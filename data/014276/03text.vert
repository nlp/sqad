<s>
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
</s>
<s>
Dálnice	dálnice	k1gFnSc1
G4	G4	k1gFnPc2
propojující	propojující	k2eAgInSc1d1
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Hong	Hong	k1gMnSc1
Kong	Kongo	k1gNnPc2
a	a	k8xC
Macau	Macao	k1gNnSc6
v	v	k7c6
Tung-kuanu	Tung-kuan	k1gInSc6
</s>
<s>
Schéma	schéma	k1gNnSc1
čínské	čínský	k2eAgFnSc2d1
dálniční	dálniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
</s>
<s>
Dálnice	dálnice	k1gFnPc1
v	v	k7c6
Čínské	čínský	k2eAgFnSc6d1
lidové	lidový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
patří	patřit	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
131	#num#	k4
000	#num#	k4
kilometry	kilometr	k1gInPc7
délky	délka	k1gFnSc2
(	(	kIx(
<g/>
stav	stav	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
druhé	druhý	k4xOgFnSc2
největší	veliký	k2eAgFnSc2d3
sítě	síť	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
Interstate	Interstate	k2eAgInSc6d1
Highway	Highway	k2eAgInSc6d1
System	System	k1gInSc6
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
dálnice	dálnice	k1gFnPc2
<g/>
,	,	kIx,
nazývané	nazývaný	k2eAgFnSc2d1
také	také	k9
National	National	k2eAgInSc1d1
Trunk	Trunk	k2eAgInSc1d1
Highway	Highwaa	k2eAgInSc1d1
System	Syst	k1gInSc1
v	v	k7c6
(	(	kIx(
<g/>
zjednodušené	zjednodušený	k2eAgFnSc6d1
čínštině	čínština	k1gFnSc6
中	中	kIx~
国	国	kIx~
高	高	kIx~
网	网	kIx~
<g/>
,	,	kIx,
v	v	k7c6
pchin-jin	pchin-jin	k1gInSc6
Zhō	Zhō	kA
Guójiā	Guójiā	kA
Gā	Gā	kA
Gō	Gō	kA
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
také	také	k9
NTHS	NTHS	kA
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nP
páteřní	páteřní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
čínské	čínský	k2eAgFnSc2d1
silniční	silniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
propojují	propojovat	k5eAaImIp3nP
velká	velký	k2eAgNnPc4d1
čínská	čínský	k2eAgNnPc4d1
města	město	k1gNnPc4
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
málo	málo	k6eAd1
hustě	hustě	k6eAd1
osídlený	osídlený	k2eAgInSc4d1
západ	západ	k1gInSc4
a	a	k8xC
také	také	k9
Čínu	Čína	k1gFnSc4
s	s	k7c7
okolními	okolní	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínské	čínský	k2eAgFnPc4d1
dálnice	dálnice	k1gFnPc4
také	také	k9
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejrychleji	rychle	k6eAd3
se	se	k3xPyFc4
rozvíjejícím	rozvíjející	k2eAgMnSc7d1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Číně	Čína	k1gFnSc6
pouze	pouze	k6eAd1
10	#num#	k4
000	#num#	k4
km	km	kA
dálnic	dálnice	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
více	hodně	k6eAd2
než	než	k8xS
ztřináctinásobil	ztřináctinásobit	k5eAaPmAgMnS,k5eAaImAgMnS
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
2050	#num#	k4
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
dosáhnout	dosáhnout	k5eAaPmF
délky	délka	k1gFnPc4
175	#num#	k4
000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Všeobecné	všeobecný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Před	před	k7c7
číslem	číslo	k1gNnSc7
dálnice	dálnice	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
píše	psát	k5eAaImIp3nS
písmeno	písmeno	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
odlišuje	odlišovat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
čínských	čínský	k2eAgFnPc2d1
dálnic	dálnice	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Písmenem	písmeno	k1gNnSc7
M	M	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nS
plánované	plánovaný	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
</s>
<s>
Písmenem	písmeno	k1gNnSc7
G	G	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nS
významné	významný	k2eAgFnPc4d1
dálnice	dálnice	k1gFnPc4
</s>
<s>
Písmenem	písmeno	k1gNnSc7
A	A	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nS
městské	městský	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
</s>
<s>
Písmenem	písmeno	k1gNnSc7
S	s	k7c7
se	s	k7c7
značí	značit	k5eAaImIp3nS
provinční	provinční	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
</s>
<s>
Písmenem	písmeno	k1gNnSc7
X	X	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nS
krajské	krajský	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
</s>
<s>
Písmenem	písmeno	k1gNnSc7
Y	Y	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nP
venkovské	venkovský	k2eAgFnPc1d1
silnice	silnice	k1gFnPc1
(	(	kIx(
<g/>
dálnice	dálnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Písmenem	písmeno	k1gNnSc7
Z	Z	kA
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nS
speciální	speciální	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
spojnice	spojnice	k1gFnSc1
dvou	dva	k4xCgFnPc2
dálnic	dálnice	k1gFnPc2
<g/>
,	,	kIx,
spojení	spojení	k1gNnSc4
města	město	k1gNnSc2
a	a	k8xC
letiště	letiště	k1gNnSc2
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
dálnice	dálnice	k1gFnPc1
mívají	mívat	k5eAaImIp3nP
počáteční	počáteční	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
za	za	k7c7
písmenem	písmeno	k1gNnSc7
nulu	nula	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
G	G	kA
<g/>
0	#num#	k4
<g/>
30	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínský	čínský	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
dálnici	dálnice	k1gFnSc4
je	být	k5eAaImIp3nS
高	高	k?
<g/>
,	,	kIx,
v	v	k7c4
pchin-jin	pchin-jin	k1gInSc4
gā	gā	k?
gō	gō	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
doslovně	doslovně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
podkladu	podklad	k1gInSc2
dálničních	dálniční	k2eAgFnPc2d1
cedulí	cedule	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
dálnicích	dálnice	k1gFnPc6
s	s	k7c7
předponou	předpona	k1gFnSc7
G-	G-	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
ostatních	ostatní	k2eAgFnPc6d1
silnicích	silnice	k1gFnPc6
nižší	nízký	k2eAgFnSc2d2
třídy	třída	k1gFnSc2
modrá	modrý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názvy	název	k1gInPc4
měst	město	k1gNnPc2
na	na	k7c6
směrovkách	směrovka	k1gFnPc6
bývají	bývat	k5eAaImIp3nP
psány	psát	k5eAaImNgInP
v	v	k7c6
angličtině	angličtina	k1gFnSc6
a	a	k8xC
ve	v	k7c6
zjednodušené	zjednodušený	k2eAgFnSc6d1
čínštině	čínština	k1gFnSc6
<g/>
,	,	kIx,
výjimkami	výjimka	k1gFnPc7
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
některý	některý	k3yIgInSc4
provincie	provincie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dálniční	dálniční	k2eAgFnPc1d1
cedule	cedule	k1gFnPc1
s	s	k7c7
rychlostními	rychlostní	k2eAgInPc7d1
limity	limit	k1gInPc7
na	na	k7c6
dálnici	dálnice	k1gFnSc6
G6	G6	k1gFnSc2
</s>
<s>
Rychlostní	rychlostní	k2eAgInPc1d1
limity	limit	k1gInPc1
</s>
<s>
Rychlostní	rychlostní	k2eAgInSc1d1
limit	limit	k1gInSc1
na	na	k7c6
čínských	čínský	k2eAgFnPc6d1
víceproudých	víceproudý	k2eAgFnPc6d1
kominikacích	kominikace	k1gFnPc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
maximálně	maximálně	k6eAd1
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
minimálně	minimálně	k6eAd1
70	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Tak	tak	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
kdy	kdy	k6eAd1
čínská	čínský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
zvýšila	zvýšit	k5eAaPmAgFnS
maximální	maximální	k2eAgFnSc4d1
povolenou	povolený	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
na	na	k7c6
dálnicích	dálnice	k1gFnPc6
ze	z	k7c2
110	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
na	na	k7c4
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Na	na	k7c6
silnicích	silnice	k1gFnPc6
mimo	mimo	k7c4
obec	obec	k1gFnSc4
je	být	k5eAaImIp3nS
max	max	kA
<g/>
.	.	kIx.
povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
80	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
v	v	k7c6
obci	obec	k1gFnSc6
pak	pak	k6eAd1
40	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Zpoplatnění	zpoplatnění	k1gNnSc1
</s>
<s>
Mýtná	mýtné	k1gNnPc1
brána	brán	k2eAgNnPc1d1
na	na	k7c6
čínské	čínský	k2eAgFnSc6d1
dálnici	dálnice	k1gFnSc6
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1
dálnice	dálnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
zpoplatněny	zpoplatnit	k5eAaPmNgFnP
klasicky	klasicky	k6eAd1
<g/>
,	,	kIx,
systémem	systém	k1gInSc7
výběru	výběr	k1gInSc2
poplatku	poplatek	k1gInSc2
v	v	k7c6
hotovosti	hotovost	k1gFnSc6
na	na	k7c6
mýtných	mýtné	k1gNnPc6
branách	brána	k1gFnPc6
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
dálnicích	dálnice	k1gFnPc6
nacházejí	nacházet	k5eAaImIp3nP
elektronické	elektronický	k2eAgMnPc4d1
mýtné	mýtný	k1gMnPc4
brány	brána	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpoplatněny	zpoplatněn	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
především	především	k9
úseky	úsek	k1gInPc1
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
jako	jako	k8xS,k8xC
Peking	Peking	k1gInSc4
<g/>
,	,	kIx,
Šanghaj	Šanghaj	k1gFnSc4
nebo	nebo	k8xC
v	v	k7c6
provincii	provincie	k1gFnSc6
Kuang-tung	Kuang-tunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
mýtného	mýtné	k1gNnSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
0,5	0,5	k4
CNY	CNY	kA
za	za	k7c4
kilometr	kilometr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálkové	dálkový	k2eAgFnPc1d1
mimoměstské	mimoměstský	k2eAgFnPc1d1
silnice	silnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
zpoplatněné	zpoplatněný	k2eAgFnPc1d1
za	za	k7c2
pomocí	pomoc	k1gFnPc2
karet	kareta	k1gFnPc2
<g/>
:	:	kIx,
při	při	k7c6
vjezdu	vjezd	k1gInSc6
na	na	k7c6
dálnici	dálnice	k1gFnSc6
řidič	řidič	k1gMnSc1
obdrží	obdržet	k5eAaPmIp3nS
kartu	karta	k1gFnSc4
a	a	k8xC
při	při	k7c6
sjezdu	sjezd	k1gInSc6
kartu	karta	k1gFnSc4
odevzdá	odevzdat	k5eAaPmIp3nS
a	a	k8xC
zaplatí	zaplatit	k5eAaPmIp3nS
za	za	k7c4
ujetou	ujetý	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytížené	vytížený	k2eAgFnPc4d1
silnice	silnice	k1gFnPc4
v	v	k7c6
Číně	Čína	k1gFnSc6
bývají	bývat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
dražší	drahý	k2eAgInPc1d2
<g/>
,	,	kIx,
nevyužívá	využívat	k5eNaImIp3nS
je	být	k5eAaImIp3nS
tedy	tedy	k9
tolik	tolik	k4xDc4,k4yIc4
řidičů	řidič	k1gMnPc2
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
se	se	k3xPyFc4
netvoří	tvořit	k5eNaImIp3nP
dopravní	dopravní	k2eAgFnSc2d1
zácpy	zácpa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byly	být	k5eAaImAgFnP
čínské	čínský	k2eAgFnPc1d1
silnice	silnice	k1gFnPc1
málo	málo	k6eAd1
využívané	využívaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
1978	#num#	k4
tvořila	tvořit	k5eAaImAgFnS
železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
54,5	54,5	k4
<g/>
%	%	kIx~
celkové	celkový	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
složky	složka	k1gFnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
silniční	silniční	k2eAgFnSc1d1
pouze	pouze	k6eAd1
2,8	2,8	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
začala	začít	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
první	první	k4xOgFnSc2
čínské	čínský	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
propojující	propojující	k2eAgNnPc1d1
města	město	k1gNnPc1
Šen-jang	Šen-janga	k1gFnPc2
a	a	k8xC
Dalian	Daliana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
začala	začít	k5eAaPmAgFnS
také	také	k9
výstavba	výstavba	k1gFnSc1
dálnice	dálnice	k1gFnSc1
Šanghaj	Šanghaj	k1gFnSc1
-	-	kIx~
Jiading	Jiading	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1988	#num#	k4
byla	být	k5eAaImAgFnS
dálnice	dálnice	k1gFnSc1
otevřená	otevřený	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
historicky	historicky	k6eAd1
první	první	k4xOgFnSc1
rychlostní	rychlostní	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
čínský	čínský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
Zhang	Zhang	k1gMnSc1
Chunxian	Chunxian	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
země	zem	k1gFnPc1
v	v	k7c6
příštích	příští	k2eAgInPc6d1
letech	let	k1gInPc6
vybuduje	vybudovat	k5eAaPmIp3nS
85	#num#	k4
000	#num#	k4
km	km	kA
dálnic	dálnice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
propojí	propojit	k5eAaPmIp3nP
všechna	všechen	k3xTgNnPc1
čínská	čínský	k2eAgNnPc1d1
města	město	k1gNnPc1
s	s	k7c7
počtem	počet	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nahradilo	nahradit	k5eAaPmAgNnS
původní	původní	k2eAgInSc4d1
návrh	návrh	k1gInSc4
dvanácti	dvanáct	k4xCc2
dálnic	dálnice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1
</s>
<s>
Značení	značení	k1gNnSc1
dálnice	dálnice	k1gFnSc2
G101	G101	k1gFnSc2
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1
dálnice	dálnice	k1gFnPc1
patří	patřit	k5eAaImIp3nP
k	k	k7c3
nejrychleji	rychle	k6eAd3
se	se	k3xPyFc4
rozvíjejícím	rozvíjející	k2eAgInSc6d1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2050	#num#	k4
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
síť	síť	k1gFnSc1
dosáhnout	dosáhnout	k5eAaPmF
délky	délka	k1gFnPc4
175	#num#	k4
000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Expressways	Expressways	k1gInSc1
of	of	k?
China	China	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dálnice	dálnice	k1gFnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
