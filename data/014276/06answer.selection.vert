<s desamb="1">
Tyto	tento	k3xDgFnPc4
dálnice	dálnice	k1gFnPc2
<g/>
,	,	kIx,
nazývané	nazývaný	k2eAgFnSc2d1
také	také	k9
National	National	k2eAgInSc1d1
Trunk	Trunk	k2eAgInSc1d1
Highway	Highwaa	k2eAgInSc1d1
System	Syst	k1gInSc1
v	v	k7c6
(	(	kIx(
<g/>
zjednodušené	zjednodušený	k2eAgFnSc6d1
čínštině	čínština	k1gFnSc6
中	中	kIx~
国	国	kIx~
高	高	kIx~
网	网	kIx~
<g/>
,	,	kIx,
v	v	k7c6
pchin-jin	pchin-jin	k1gInSc6
Zhō	Zhō	kA
Guójiā	Guójiā	kA
Gā	Gā	kA
Gō	Gō	kA
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
také	také	k9
NTHS	NTHS	kA
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nP
páteřní	páteřní	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
čínské	čínský	k2eAgFnSc2d1
silniční	silniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
propojují	propojovat	k5eAaImIp3nP
velká	velký	k2eAgNnPc4d1
čínská	čínský	k2eAgNnPc4d1
města	město	k1gNnPc4
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
málo	málo	k6eAd1
hustě	hustě	k6eAd1
osídlený	osídlený	k2eAgInSc4d1
západ	západ	k1gInSc4
a	a	k8xC
také	také	k9
Čínu	Čína	k1gFnSc4
s	s	k7c7
okolními	okolní	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>