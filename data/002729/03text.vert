<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
(	(	kIx(	(
<g/>
tibetsky	tibetsky	k6eAd1	tibetsky
ཇ	ཇ	k?	ཇ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
ག	ག	k?	ག
<g/>
ླ	ླ	k?	ླ
<g/>
ང	ང	k?	ང
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
,	,	kIx,	,
Džomolangma	Džomolangma	k1gFnSc1	Džomolangma
<g/>
;	;	kIx,	;
nepálsky	nepálsky	k6eAd1	nepálsky
स	स	k?	स
<g/>
ा	ा	k?	ा
<g/>
थ	थ	k?	थ
<g/>
ा	ा	k?	ा
<g/>
,	,	kIx,	,
Sagarmátha	Sagarmátha	k1gFnSc1	Sagarmátha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
8848	[number]	k4	8848
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
starších	starý	k2eAgInPc2d2	starší
údajů	údaj	k1gInPc2	údaj
i	i	k9	i
8850	[number]	k4	8850
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Himálaje	Himálaj	k1gFnSc2	Himálaj
kolizí	kolize	k1gFnPc2	kolize
indické	indický	k2eAgFnSc2d1	indická
a	a	k8xC	a
eurasijské	eurasijský	k2eAgFnSc2d1	eurasijská
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
britském	britský	k2eAgInSc6d1	britský
geodetovi	geodet	k1gMnSc3	geodet
George	George	k1gNnSc2	George
Everestovi	Everesta	k1gMnSc3	Everesta
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Himálají	Himálaj	k1gFnPc2	Himálaj
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
díky	díky	k7c3	díky
neustálému	neustálý	k2eAgNnSc3d1	neustálé
tlačení	tlačení	k1gNnSc3	tlačení
Indické	indický	k2eAgFnSc2d1	indická
desky	deska	k1gFnSc2	deska
na	na	k7c4	na
Eurasijskou	Eurasijský	k2eAgFnSc4d1	Eurasijská
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
i	i	k9	i
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
v	v	k7c6	v
Mahálangurském	Mahálangurský	k2eAgInSc6d1	Mahálangurský
Himálaji	Himálaj	k1gFnSc6	Himálaj
v	v	k7c6	v
nepálském	nepálský	k2eAgInSc6d1	nepálský
regionu	region	k1gInSc6	region
Khumbu	Khumb	k1gInSc2	Khumb
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
(	(	kIx(	(
<g/>
s	s	k7c7	s
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
<g/>
;	;	kIx,	;
západní	západní	k2eAgMnSc1d1	západní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgInSc1d1	jihovýchodní
vrchol	vrchol	k1gInSc1	vrchol
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nepálské	nepálský	k2eAgFnSc6d1	nepálská
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Sagarmatha	Sagarmath	k1gMnSc2	Sagarmath
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Qomolangma	Qomolangma	k1gFnSc1	Qomolangma
<g/>
.	.	kIx.	.
</s>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
a	a	k8xC	a
Tenzing	Tenzing	k1gInSc4	Tenzing
Norgay	Norgaa	k1gFnSc2	Norgaa
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Mount	Mount	k1gMnSc1	Mount
Everest	Everest	k1gInSc4	Everest
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
mnoho	mnoho	k4c1	mnoho
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
fyzicky	fyzicky	k6eAd1	fyzicky
zdatných	zdatný	k2eAgMnPc2d1	zdatný
lezců	lezec	k1gMnPc2	lezec
a	a	k8xC	a
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
není	být	k5eNaImIp3nS	být
technicky	technicky	k6eAd1	technicky
příliš	příliš	k6eAd1	příliš
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
,	,	kIx,	,
nebezpečím	bezpečit	k5eNaImIp1nS	bezpečit
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
akutní	akutní	k2eAgFnSc1d1	akutní
horská	horský	k2eAgFnSc1d1	horská
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
výkyvy	výkyv	k1gInPc1	výkyv
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Sagarmátha	Sagarmátha	k1gFnSc1	Sagarmátha
(	(	kIx(	(
<g/>
स	स	k?	स
<g/>
ा	ा	k?	ा
<g/>
थ	थ	k?	थ
<g/>
ा	ा	k?	ा
<g/>
,	,	kIx,	,
Sagaramā	Sagaramā	k1gFnSc1	Sagaramā
<g/>
,	,	kIx,	,
do	do	k7c2	do
nepálštiny	nepálština	k1gFnSc2	nepálština
převzato	převzít	k5eAaPmNgNnS	převzít
ze	z	k7c2	z
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Tvář	tvář	k1gFnSc1	tvář
nebes	nebesa	k1gNnPc2	nebesa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tibetský	tibetský	k2eAgInSc1d1	tibetský
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
Qomolangma	Qomolangma	k1gFnSc1	Qomolangma
(	(	kIx(	(
<g/>
ཇ	ཇ	k?	ཇ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
ག	ག	k?	ག
<g/>
ླ	ླ	k?	ླ
<g/>
ང	ང	k?	ང
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
,	,	kIx,	,
wylie	wylie	k1gFnPc4	wylie
Jo	jo	k9	jo
mo	mo	k?	mo
glang	glang	k1gInSc1	glang
ma	ma	k?	ma
<g/>
,	,	kIx,	,
Džo-mo-lang-ma	Džooanga	k1gFnSc1	Džo-mo-lang-ma
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Matka	matka	k1gFnSc1	matka
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkomolením	zkomolení	k1gNnSc7	zkomolení
tibetského	tibetský	k2eAgInSc2d1	tibetský
názvu	název	k1gInSc2	název
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
čínské	čínský	k2eAgNnSc1d1	čínské
珠	珠	k?	珠
(	(	kIx(	(
<g/>
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Zhū	Zhū	k1gMnSc1	Zhū
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
českým	český	k2eAgInSc7d1	český
přepisem	přepis	k1gInSc7	přepis
Ču-mu-lang-ma	Čuuanga	k1gFnSc1	Ču-mu-lang-ma
feng	feng	k1gMnSc1	feng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgInSc7d1	český
standardizovaným	standardizovaný	k2eAgInSc7d1	standardizovaný
exonymem	exonym	k1gInSc7	exonym
je	být	k5eAaImIp3nS	být
Everest	Everest	k1gInSc1	Everest
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
i	i	k9	i
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zdomácnělo	zdomácnět	k5eAaPmAgNnS	zdomácnět
jméno	jméno	k1gNnSc1	jméno
Everest	Everest	k1gInSc1	Everest
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
hoře	hoře	k1gNnSc4	hoře
dal	dát	k5eAaPmAgMnS	dát
Brit	Brit	k1gMnSc1	Brit
Sir	sir	k1gMnSc1	sir
Andrew	Andrew	k1gMnSc1	Andrew
Scott	Scott	k1gMnSc1	Scott
Waugh	Waugh	k1gMnSc1	Waugh
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
George	Georg	k1gInSc2	Georg
Everest	Everest	k1gInSc1	Everest
byl	být	k5eAaImAgInS	být
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Sira	sir	k1gMnSc2	sir
Andrew	Andrew	k1gMnSc1	Andrew
Scott	Scott	k1gMnSc1	Scott
Waugha	Waugha	k1gMnSc1	Waugha
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
slyšíme	slyšet	k5eAaImIp1nP	slyšet
nesprávnou	správný	k2eNgFnSc4d1	nesprávná
výslovnost	výslovnost	k1gFnSc4	výslovnost
[	[	kIx(	[
<g/>
ivrist	ivrist	k1gInSc4	ivrist
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
britského	britský	k2eAgInSc2d1	britský
rozhlasu	rozhlas	k1gInSc2	rozhlas
BBC	BBC	kA	BBC
Pronouncing	Pronouncing	k1gInSc4	Pronouncing
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
British	British	k1gMnSc1	British
Names	Names	k1gMnSc1	Names
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
uvádí	uvádět	k5eAaImIp3nS	uvádět
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
výslovnosti	výslovnost	k1gFnSc2	výslovnost
[	[	kIx(	[
<g/>
everest	everest	k1gInSc1	everest
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
everist	everist	k1gInSc1	everist
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
správná	správný	k2eAgFnSc1d1	správná
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
[	[	kIx(	[
<g/>
maunt	maunt	k1gMnSc1	maunt
everest	everest	k1gMnSc1	everest
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
[	[	kIx(	[
<g/>
maunt	maunt	k1gMnSc1	maunt
everist	everist	k1gMnSc1	everist
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Horolezci	horolezec	k1gMnPc1	horolezec
hovoří	hovořit	k5eAaImIp3nP	hovořit
vždy	vždy	k6eAd1	vždy
o	o	k7c6	o
[	[	kIx(	[
<g/>
everestu	everest	k1gInSc6	everest
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
R.	R.	kA	R.
Messnera	Messner	k1gMnSc2	Messner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zdolal	zdolat	k5eAaPmAgInS	zdolat
jako	jako	k8xS	jako
první	první	k4xOgFnPc1	první
všechny	všechen	k3xTgFnPc1	všechen
osmitisícovky	osmitisícovka	k1gFnPc1	osmitisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
deník	deník	k1gInSc1	deník
People	People	k1gFnSc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Daily	Dail	k1gMnPc7	Dail
publikoval	publikovat	k5eAaBmAgMnS	publikovat
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ohrazuje	ohrazovat	k5eAaImIp3nS	ohrazovat
proti	proti	k7c3	proti
přetrvávajícímu	přetrvávající	k2eAgNnSc3d1	přetrvávající
používání	používání	k1gNnSc3	používání
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
používání	používání	k1gNnSc4	používání
tibetského	tibetský	k2eAgNnSc2d1	tibetské
jména	jméno	k1gNnSc2	jméno
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Udávaná	udávaný	k2eAgFnSc1d1	udávaná
výška	výška	k1gFnSc1	výška
8	[number]	k4	8
848	[number]	k4	848
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uznaná	uznaný	k2eAgNnPc4d1	uznané
Nepálem	Nepál	k1gInSc7	Nepál
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
Radhanath	Radhanath	k1gMnSc1	Radhanath
Sikdar	Sikdar	k1gMnSc1	Sikdar
<g/>
,	,	kIx,	,
indický	indický	k2eAgMnSc1d1	indický
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
z	z	k7c2	z
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
určil	určit	k5eAaPmAgInS	určit
Everest	Everest	k1gInSc4	Everest
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
světa	svět	k1gInSc2	svět
pomocí	pomocí	k7c2	pomocí
trigonometrických	trigonometrický	k2eAgInPc2d1	trigonometrický
výpočtů	výpočet	k1gInPc2	výpočet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
měření	měření	k1gNnSc2	měření
teodolitem	teodolit	k1gInSc7	teodolit
z	z	k7c2	z
240	[number]	k4	240
km	km	kA	km
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
průzkumným	průzkumný	k2eAgNnSc7d1	průzkumné
měřením	měření	k1gNnSc7	měření
byl	být	k5eAaImAgInS	být
vrchol	vrchol	k1gInSc1	vrchol
zeměměřiči	zeměměřič	k1gMnPc1	zeměměřič
nazýván	nazývat	k5eAaImNgInS	nazývat
jménem	jméno	k1gNnSc7	jméno
Peak	Peako	k1gNnPc2	Peako
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
indičtí	indický	k2eAgMnPc1d1	indický
zeměměřiči	zeměměřič	k1gMnPc1	zeměměřič
učinili	učinit	k5eAaImAgMnP	učinit
přesnější	přesný	k2eAgNnSc4d2	přesnější
měření	měření	k1gNnSc4	měření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
přiblížilo	přiblížit	k5eAaPmAgNnS	přiblížit
dnešním	dnešní	k2eAgNnPc3d1	dnešní
měřením	měření	k1gNnPc3	měření
a	a	k8xC	a
výpočtům	výpočet	k1gInPc3	výpočet
výšky	výška	k1gFnSc2	výška
Everestu	Everest	k1gInSc2	Everest
8848	[number]	k4	8848
m.	m.	k?	m.
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
přijímána	přijímán	k2eAgFnSc1d1	přijímána
hodnota	hodnota	k1gFnSc1	hodnota
8848	[number]	k4	8848
m.	m.	k?	m.
Everest	Everest	k1gInSc1	Everest
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
díky	díky	k7c3	díky
pohybům	pohyb	k1gInPc3	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
27	[number]	k4	27
mm	mm	kA	mm
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
podle	podle	k7c2	podle
čínských	čínský	k2eAgFnPc2d1	čínská
měření	měření	k1gNnSc4	měření
z	z	k7c2	z
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
výška	výška	k1gFnSc1	výška
hory	hora	k1gFnSc2	hora
8844	[number]	k4	8844
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
celé	celý	k2eAgInPc4d1	celý
4	[number]	k4	4
metry	metr	k1gInPc4	metr
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc1	Everest
je	být	k5eAaImIp3nS	být
horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
vrcholek	vrcholek	k1gInSc1	vrcholek
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
světa	svět	k1gInSc2	svět
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
také	také	k9	také
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
ukryté	ukrytý	k2eAgNnSc1d1	ukryté
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
na	na	k7c6	na
oceánském	oceánský	k2eAgNnSc6d1	oceánské
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
takto	takto	k6eAd1	takto
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nad	nad	k7c4	nad
moře	moře	k1gNnSc4	moře
ční	čnět	k5eAaImIp3nP	čnět
pouhými	pouhý	k2eAgInPc7d1	pouhý
4	[number]	k4	4
205	[number]	k4	205
metry	metro	k1gNnPc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Chimboraza	Chimboraz	k1gMnSc2	Chimboraz
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
o	o	k7c4	o
2	[number]	k4	2
168	[number]	k4	168
metrů	metr	k1gInPc2	metr
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
Everest	Everest	k1gInSc4	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
měří	měřit	k5eAaImIp3nS	měřit
6384,4	[number]	k4	6384,4
km	km	kA	km
a	a	k8xC	a
Everest	Everest	k1gInSc1	Everest
pouze	pouze	k6eAd1	pouze
6382,3	[number]	k4	6382,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
rozdílu	rozdíl	k1gInSc2	rozdíl
je	být	k5eAaImIp3nS	být
zemská	zemský	k2eAgFnSc1d1	zemská
rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
deformuje	deformovat	k5eAaImIp3nS	deformovat
tvar	tvar	k1gInSc4	tvar
Země	zem	k1gFnSc2	zem
i	i	k8xC	i
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
standardně	standardně	k6eAd1	standardně
měří	měřit	k5eAaImIp3nS	měřit
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
není	být	k5eNaImIp3nS	být
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
6310	[number]	k4	6310
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
ani	ani	k8xC	ani
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
místo	místo	k1gNnSc4	místo
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
Challenger	Challengra	k1gFnPc2	Challengra
v	v	k7c6	v
Mariánském	mariánský	k2eAgInSc6d1	mariánský
příkopu	příkop	k1gInSc6	příkop
je	být	k5eAaImIp3nS	být
hlouběji	hluboko	k6eAd2	hluboko
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Everest	Everest	k1gInSc1	Everest
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
překonání	překonání	k1gNnSc4	překonání
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
dlouho	dlouho	k6eAd1	dlouho
odolával	odolávat	k5eAaImAgMnS	odolávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
s	s	k7c7	s
dnešními	dnešní	k2eAgFnPc7d1	dnešní
komerčními	komerční	k2eAgFnPc7d1	komerční
výpravami	výprava	k1gFnPc7	výprava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
lezou	lézt	k5eAaImIp3nP	lézt
i	i	k9	i
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
důchodci	důchodce	k1gMnPc1	důchodce
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
divně	divně	k6eAd1	divně
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgMnPc1d1	dnešní
návštěvníci	návštěvník	k1gMnPc1	návštěvník
mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
těm	ten	k3xDgInPc3	ten
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obrovské	obrovský	k2eAgFnPc4d1	obrovská
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
vyzkoušené	vyzkoušený	k2eAgFnPc4d1	vyzkoušená
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
instalovaná	instalovaný	k2eAgNnPc4d1	instalované
lana	lano	k1gNnPc4	lano
a	a	k8xC	a
žebříky	žebřík	k1gInPc4	žebřík
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc4d2	lepší
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
důvěryhodnější	důvěryhodný	k2eAgFnPc4d2	důvěryhodnější
předpovědi	předpověď	k1gFnPc4	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
Everest	Everest	k1gInSc1	Everest
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
horou	hora	k1gFnSc7	hora
i	i	k9	i
pro	pro	k7c4	pro
klasické	klasický	k2eAgFnPc4d1	klasická
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
jištěné	jištěný	k2eAgFnSc2d1	jištěná
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
expedice	expedice	k1gFnSc1	expedice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
George	Georg	k1gMnSc2	Georg
Malloryho	Mallory	k1gMnSc2	Mallory
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zejména	zejména	k9	zejména
vylepšit	vylepšit	k5eAaPmF	vylepšit
geografické	geografický	k2eAgFnPc4d1	geografická
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
na	na	k7c4	na
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
nebyla	být	k5eNaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vybavena	vybavit	k5eAaPmNgFnS	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
expedice	expedice	k1gFnSc1	expedice
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
severní	severní	k2eAgNnSc4d1	severní
sedlo	sedlo	k1gNnSc4	sedlo
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
7060	[number]	k4	7060
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
však	však	k9	však
již	již	k6eAd1	již
nepokračovala	pokračovat	k5eNaImAgFnS	pokračovat
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
velká	velký	k2eAgFnSc1d1	velká
britská	britský	k2eAgFnSc1d1	britská
expedice	expedice	k1gFnSc1	expedice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Charlese	Charles	k1gMnSc2	Charles
Granvilla	Granvill	k1gMnSc2	Granvill
Bruce	Bruce	k1gMnSc2	Bruce
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
čítala	čítat	k5eAaImAgFnS	čítat
13	[number]	k4	13
britských	britský	k2eAgMnPc2d1	britský
horolezců	horolezec	k1gMnPc2	horolezec
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
George	Georg	k1gMnSc2	Georg
Malloryho	Mallory	k1gMnSc2	Mallory
<g/>
)	)	kIx)	)
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
nepálských	nepálský	k2eAgMnPc2d1	nepálský
a	a	k8xC	a
tibetských	tibetský	k2eAgMnPc2d1	tibetský
nosičů	nosič	k1gMnPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
expedice	expedice	k1gFnSc2	expedice
byl	být	k5eAaImAgMnS	být
také	také	k9	také
britský	britský	k2eAgMnSc1d1	britský
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
George	George	k1gFnPc2	George
Finch	Finch	k1gMnSc1	Finch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
výhody	výhoda	k1gFnPc4	výhoda
využití	využití	k1gNnSc2	využití
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
přenosných	přenosný	k2eAgFnPc2d1	přenosná
kyslíkových	kyslíkový	k2eAgFnPc2d1	kyslíková
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
Mallory	Mallor	k1gMnPc4	Mallor
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
další	další	k2eAgMnPc1d1	další
horolezci	horolezec	k1gMnPc1	horolezec
a	a	k8xC	a
devět	devět	k4xCc1	devět
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pokusu	pokus	k1gInSc6	pokus
Mallory	Mallora	k1gFnSc2	Mallora
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
horolezci	horolezec	k1gMnPc1	horolezec
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
překročili	překročit	k5eAaPmAgMnP	překročit
hranici	hranice	k1gFnSc4	hranice
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
8000	[number]	k4	8000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
když	když	k8xS	když
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
225	[number]	k4	225
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
pokusu	pokus	k1gInSc2	pokus
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
nosičů	nosič	k1gInPc2	nosič
účastnili	účastnit	k5eAaImAgMnP	účastnit
Bruce	Bruec	k1gInPc4	Bruec
<g/>
,	,	kIx,	,
Finch	Finch	k1gMnSc1	Finch
a	a	k8xC	a
Gurkha	Gurkha	k1gMnSc1	Gurkha
Tejbir	Tejbir	k1gMnSc1	Tejbir
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
a	a	k8xC	a
Finch	Fincha	k1gFnPc2	Fincha
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výšky	výška	k1gFnSc2	výška
8326	[number]	k4	8326
m.	m.	k?	m.
Třetího	třetí	k4xOgInSc2	třetí
pokusu	pokus	k1gInSc2	pokus
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
Mallory	Mallor	k1gInPc4	Mallor
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finch	Finch	k1gMnSc1	Finch
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
další	další	k2eAgMnPc1d1	další
britští	britský	k2eAgMnPc1d1	britský
horolezci	horolezec	k1gMnPc1	horolezec
a	a	k8xC	a
16	[number]	k4	16
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
však	však	k9	však
strhli	strhnout	k5eAaPmAgMnP	strhnout
lavinu	lavina	k1gFnSc4	lavina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
7	[number]	k4	7
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
nepokračovali	pokračovat	k5eNaImAgMnP	pokračovat
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
expedice	expedice	k1gFnSc1	expedice
tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
expedice	expedice	k1gFnSc1	expedice
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
13	[number]	k4	13
Britů	Brit	k1gMnPc2	Brit
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bruce	Bruce	k1gFnSc2	Bruce
a	a	k8xC	a
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Malloryho	Mallory	k1gMnSc2	Mallory
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
150	[number]	k4	150
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
základních	základní	k2eAgInPc2d1	základní
táborů	tábor	k1gInPc2	tábor
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
dva	dva	k4xCgMnPc1	dva
nosiči	nosič	k1gMnPc1	nosič
na	na	k7c4	na
omrzliny	omrzlina	k1gFnPc4	omrzlina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
byl	být	k5eAaImAgInS	být
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
,	,	kIx,	,
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
pokusu	pokus	k1gInSc6	pokus
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
8573	[number]	k4	8573
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
třetí	třetí	k4xOgInSc4	třetí
pokus	pokus	k1gInSc4	pokus
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
Mallory	Mallora	k1gFnPc4	Mallora
a	a	k8xC	a
Andrew	Andrew	k1gMnSc5	Andrew
Irvine	Irvin	k1gMnSc5	Irvin
<g/>
,	,	kIx,	,
zpět	zpět	k6eAd1	zpět
se	se	k3xPyFc4	se
však	však	k9	však
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yRgFnPc4	jaký
výšky	výška	k1gFnPc4	výška
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Geolog	geolog	k1gMnSc1	geolog
výpravy	výprava	k1gFnSc2	výprava
Noel	Noel	k1gMnSc1	Noel
Odell	Odell	k1gMnSc1	Odell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
horolezce	horolezec	k1gMnSc2	horolezec
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
do	do	k7c2	do
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
viděl	vidět	k5eAaImAgMnS	vidět
stoupat	stoupat	k5eAaImF	stoupat
od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
výšvihu	výšvih	k1gInSc2	výšvih
(	(	kIx(	(
<g/>
second	second	k1gInSc1	second
step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
8605	[number]	k4	8605
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
expedici	expedice	k1gFnSc6	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
Irvinův	Irvinův	k2eAgInSc1d1	Irvinův
cepín	cepín	k1gInSc1	cepín
pod	pod	k7c7	pod
prvním	první	k4xOgInSc7	první
výšvihem	výšvih	k1gInSc7	výšvih
(	(	kIx(	(
<g/>
first	first	k1gInSc1	first
step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
8560	[number]	k4	8560
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
pak	pak	k6eAd1	pak
Malloryho	Malloryha	k1gFnSc5	Malloryha
tělo	tělo	k1gNnSc1	tělo
v	v	k7c6	v
8155	[number]	k4	8155
metrech	metr	k1gInPc6	metr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nehody	nehoda	k1gFnSc2	nehoda
oba	dva	k4xCgMnPc1	dva
horolezci	horolezec	k1gMnPc1	horolezec
již	již	k6eAd1	již
slaňovali	slaňovat	k5eAaImAgMnP	slaňovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
základního	základní	k2eAgInSc2d1	základní
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
Irvina	Irvin	k1gMnSc2	Irvin
ani	ani	k8xC	ani
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oba	dva	k4xCgMnPc1	dva
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
a	a	k8xC	a
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
mnohé	mnohé	k1gNnSc4	mnohé
objasnit	objasnit	k5eAaPmF	objasnit
<g/>
,	,	kIx,	,
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
oba	dva	k4xCgMnPc1	dva
horolezci	horolezec	k1gMnPc1	horolezec
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
horolezci	horolezec	k1gMnPc7	horolezec
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
příliš	příliš	k6eAd1	příliš
příznivců	příznivec	k1gMnPc2	příznivec
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
další	další	k2eAgFnPc1d1	další
expedice	expedice	k1gFnPc1	expedice
s	s	k7c7	s
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
však	však	k9	však
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
ani	ani	k8xC	ani
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
druhého	druhý	k4xOgInSc2	druhý
pokusu	pokus	k1gInSc2	pokus
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
expedice	expedice	k1gFnSc2	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
povolil	povolit	k5eAaPmAgInS	povolit
Nepál	Nepál	k1gInSc1	Nepál
vstup	vstup	k1gInSc4	vstup
cizinců	cizinec	k1gMnPc2	cizinec
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
otevíralo	otevírat	k5eAaImAgNnS	otevírat
další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
expedice	expedice	k1gFnSc1	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
nebyl	být	k5eNaImAgInS	být
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
průzkum	průzkum	k1gInSc1	průzkum
trasy	trasa	k1gFnSc2	trasa
výstupu	výstup	k1gInSc2	výstup
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Raymond	Raymond	k1gMnSc1	Raymond
Lambert	Lambert	k1gMnSc1	Lambert
a	a	k8xC	a
Šerpa	šerpa	k1gFnSc1	šerpa
Tenzing	Tenzing	k1gInSc4	Tenzing
Norgay	Norgaa	k1gFnSc2	Norgaa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
nadmořské	nadmořský	k2eAgFnPc4d1	nadmořská
výšky	výška	k1gFnPc4	výška
8595	[number]	k4	8595
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
nový	nový	k2eAgInSc4d1	nový
výškový	výškový	k2eAgInSc4d1	výškový
rekord	rekord	k1gInSc4	rekord
(	(	kIx(	(
<g/>
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
neznámé	známý	k2eNgFnSc2d1	neznámá
výšky	výška	k1gFnSc2	výška
dosažené	dosažený	k2eAgInPc4d1	dosažený
Mallorym	Mallorym	k1gInSc4	Mallorym
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norgay	Norga	k1gMnPc4	Norga
získal	získat	k5eAaPmAgInS	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zúročil	zúročit	k5eAaPmAgMnS	zúročit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
Novozélanďan	Novozélanďan	k1gMnSc1	Novozélanďan
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
a	a	k8xC	a
nepálský	nepálský	k2eAgInSc1d1	nepálský
Šerpa	šerpa	k1gFnSc1	šerpa
Tenzing	Tenzing	k1gInSc4	Tenzing
Norgay	Norgaa	k1gFnSc2	Norgaa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
Everestu	Everest	k1gInSc6	Everest
byla	být	k5eAaImAgFnS	být
Japonka	Japonka	k1gFnSc1	Japonka
Džunko	džunka	k1gFnSc5	džunka
Tabei	Tabe	k1gFnSc6	Tabe
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1975	[number]	k4	1975
po	po	k7c6	po
výstupu	výstup	k1gInSc6	výstup
klasickou	klasický	k2eAgFnSc7d1	klasická
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
11	[number]	k4	11
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
Tibeťanka	Tibeťanka	k1gFnSc1	Tibeťanka
Phantog	Phantoga	k1gFnPc2	Phantoga
po	po	k7c6	po
výstupu	výstup	k1gInSc6	výstup
severní	severní	k2eAgNnSc1d1	severní
cestou	cesta	k1gFnSc7	cesta
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
expedicí	expedice	k1gFnSc7	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
první	první	k4xOgFnSc7	první
Evropankou	Evropanka	k1gFnSc7	Evropanka
na	na	k7c6	na
Everestu	Everest	k1gInSc6	Everest
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
asi	asi	k9	asi
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
himálajská	himálajský	k2eAgFnSc1d1	himálajská
horolezkyně	horolezkyně	k1gFnSc1	horolezkyně
-	-	kIx~	-
Polka	Polka	k1gFnSc1	Polka
Wanda	Wanda	k1gFnSc1	Wanda
Rutkiewiczová	Rutkiewiczová	k1gFnSc1	Rutkiewiczová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
dokončila	dokončit	k5eAaPmAgFnS	dokončit
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
JV	JV	kA	JV
hřebenem	hřeben	k1gInSc7	hřeben
polská	polský	k2eAgFnSc1d1	polská
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Andrejem	Andrej	k1gMnSc7	Andrej
Zawadou	Zawadý	k2eAgFnSc4d1	Zawadý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Leszek	Leszek	k1gInSc4	Leszek
Cichy	Cicha	k1gFnSc2	Cicha
a	a	k8xC	a
Krzysztof	Krzysztof	k1gInSc4	Krzysztof
Wielicki	Wielick	k1gFnSc2	Wielick
<g/>
.	.	kIx.	.
</s>
<s>
Polští	polský	k2eAgMnPc1d1	polský
horolezci	horolezec	k1gMnPc1	horolezec
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
lezení	lezení	k1gNnSc6	lezení
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
průkopníky	průkopník	k1gMnPc4	průkopník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
čelili	čelit	k5eAaImAgMnP	čelit
mrazům	mráz	k1gInPc3	mráz
kolem	kolo	k1gNnSc7	kolo
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
prudkým	prudký	k2eAgFnPc3d1	prudká
vichřicím	vichřice	k1gFnPc3	vichřice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výstup	výstup	k1gInSc1	výstup
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
umělého	umělý	k2eAgInSc2d1	umělý
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
úspěšně	úspěšně	k6eAd1	úspěšně
zakončili	zakončit	k5eAaPmAgMnP	zakončit
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
Reinhold	Reinhold	k1gMnSc1	Reinhold
Messner	Messner	k1gMnSc1	Messner
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Habeler	Habeler	k1gMnSc1	Habeler
<g/>
.	.	kIx.	.
</s>
<s>
Messner	Messner	k1gMnSc1	Messner
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
předvedl	předvést	k5eAaPmAgMnS	předvést
první	první	k4xOgInSc4	první
kompletní	kompletní	k2eAgInSc4d1	kompletní
sólovýstup	sólovýstup	k1gInSc4	sólovýstup
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
kyslíku	kyslík	k1gInSc2	kyslík
devalvuje	devalvovat	k5eAaBmIp3nS	devalvovat
podaný	podaný	k2eAgInSc4d1	podaný
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Messnera	Messner	k1gMnSc2	Messner
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
výstupu	výstup	k1gInSc2	výstup
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
6400	[number]	k4	6400
metrů	metr	k1gInPc2	metr
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Horolezci	horolezec	k1gMnPc1	horolezec
stoupající	stoupající	k2eAgMnPc1d1	stoupající
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vypotřebování	vypotřebování	k1gNnSc2	vypotřebování
bomby	bomba	k1gFnSc2	bomba
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nejsou	být	k5eNaImIp3nP	být
aklimatizování	aklimatizování	k1gNnSc4	aklimatizování
na	na	k7c4	na
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
událost	událost	k1gFnSc1	událost
končí	končit	k5eAaImIp3nS	končit
zpravidla	zpravidla	k6eAd1	zpravidla
tragicky	tragicky	k6eAd1	tragicky
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
evidovaných	evidovaný	k2eAgMnPc2d1	evidovaný
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
je	být	k5eAaImIp3nS	být
výstupů	výstup	k1gInPc2	výstup
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
jen	jen	k9	jen
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
dvě	dva	k4xCgNnPc4	dva
procenta	procento	k1gNnPc4	procento
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
první	první	k4xOgMnPc1	první
českoslovenští	československý	k2eAgMnPc1d1	československý
horolezci	horolezec	k1gMnPc1	horolezec
-	-	kIx~	-
Slováci	Slovák	k1gMnPc1	Slovák
Zoltán	Zoltán	k1gMnSc1	Zoltán
Demján	Demján	k1gMnSc1	Demján
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Psotka	Psotka	k1gMnSc1	Psotka
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Šerpa	šerpa	k1gFnSc1	šerpa
Ang	Ang	k1gFnSc1	Ang
Rita	Rita	k1gFnSc1	Rita
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
Polské	polský	k2eAgFnSc2d1	polská
cesty	cesta	k1gFnSc2	cesta
na	na	k7c6	na
JZ	JZ	kA	JZ
pilíři	pilíř	k1gInSc6	pilíř
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Psotka	Psotka	k1gMnSc1	Psotka
však	však	k9	však
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
z	z	k7c2	z
Jižního	jižní	k2eAgNnSc2d1	jižní
sedla	sedlo	k1gNnSc2	sedlo
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
horolezec	horolezec	k1gMnSc1	horolezec
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Leopold	Leopold	k1gMnSc1	Leopold
Sulovský	Sulovský	k1gMnSc1	Sulovský
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Italem	Ital	k1gMnSc7	Ital
Battistou	Battista	k1gMnSc7	Battista
Bonalim	Bonalima	k1gFnPc2	Bonalima
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
ve	v	k7c6	v
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
části	část	k1gFnSc6	část
osm	osm	k4xCc4	osm
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgMnPc4	tři
horští	horský	k2eAgMnPc1d1	horský
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
:	:	kIx,	:
Rob	roba	k1gFnPc2	roba
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
Scott	Scott	k1gInSc1	Scott
Fisher	Fishra	k1gFnPc2	Fishra
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Harris	Harris	k1gFnSc2	Harris
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvých	mrtvý	k1gMnPc2	mrtvý
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nebýt	být	k5eNaImF	být
dočasného	dočasný	k2eAgNnSc2d1	dočasné
utišení	utišení	k1gNnSc2	utišení
bouře	bouř	k1gFnSc2	bouř
a	a	k8xC	a
výkonu	výkon	k1gInSc2	výkon
vůdce	vůdce	k1gMnSc2	vůdce
Anatolije	Anatolije	k1gMnSc2	Anatolije
Bukrejeva	Bukrejev	k1gMnSc2	Bukrejev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
částí	část	k1gFnSc7	část
kolegů	kolega	k1gMnPc2	kolega
později	pozdě	k6eAd2	pozdě
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lezl	lézt	k5eAaImAgMnS	lézt
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
nesetrval	setrvat	k5eNaPmAgMnS	setrvat
s	s	k7c7	s
výpravou	výprava	k1gMnSc7	výprava
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
dřív	dříve	k6eAd2	dříve
připravit	připravit	k5eAaPmF	připravit
pro	pro	k7c4	pro
navracející	navracející	k2eAgInSc4d1	navracející
se	se	k3xPyFc4	se
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
někteří	některý	k3yIgMnPc1	některý
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nezodpovědné	zodpovědný	k2eNgNnSc4d1	nezodpovědné
<g/>
.	.	kIx.	.
</s>
<s>
Příběhem	příběh	k1gInSc7	příběh
se	se	k3xPyFc4	se
šťastným	šťastný	k2eAgInSc7d1	šťastný
koncem	konec	k1gInSc7	konec
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
výstup	výstup	k1gInSc1	výstup
Izraelce	Izraelec	k1gMnSc2	Izraelec
Nadava	Nadava	k1gFnSc1	Nadava
Ben	Ben	k1gInSc1	Ben
Yehudy	Yehuda	k1gFnSc2	Yehuda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
horu	hora	k1gFnSc4	hora
zdolat	zdolat	k5eAaPmF	zdolat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Rekord	rekord	k1gInSc1	rekord
najmladšího	najmladší	k2eAgMnSc2d1	najmladší
Izraelce	Izraelec	k1gMnSc2	Izraelec
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
však	však	k9	však
obětoval	obětovat	k5eAaBmAgMnS	obětovat
záchraně	záchrana	k1gFnSc3	záchrana
tureckého	turecký	k2eAgMnSc2d1	turecký
horolezce	horolezec	k1gMnSc2	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
dolů	dolů	k6eAd1	dolů
jim	on	k3xPp3gMnPc3	on
nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
Nadav	nadat	k5eAaPmDgInS	nadat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
oprávněně	oprávněně	k6eAd1	oprávněně
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
učitel	učitel	k1gMnSc1	učitel
matematiky	matematika	k1gFnSc2	matematika
z	z	k7c2	z
britského	britský	k2eAgNnSc2d1	Britské
městečka	městečko	k1gNnSc2	městečko
Gainsborough	Gainsborough	k1gMnSc1	Gainsborough
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everestu	Everest	k1gInSc2	Everest
s	s	k7c7	s
cestovní	cestovní	k2eAgFnSc7d1	cestovní
kanceláří	kancelář	k1gFnSc7	kancelář
Asian	Asiany	k1gInPc2	Asiany
Trekking	Trekking	k1gInSc4	Trekking
<g/>
.	.	kIx.	.
</s>
<s>
Zdolal	zdolat	k5eAaPmAgMnS	zdolat
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cestou	cesta	k1gFnSc7	cesta
dolů	dolů	k6eAd1	dolů
ho	on	k3xPp3gMnSc4	on
opustily	opustit	k5eAaPmAgFnP	opustit
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
se	se	k3xPyFc4	se
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
ho	on	k3xPp3gNnSc2	on
míjelo	míjet	k5eAaImAgNnS	míjet
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
deseti	deset	k4xCc6	deset
hodinách	hodina	k1gFnPc6	hodina
přišla	přijít	k5eAaPmAgFnS	přijít
výprava	výprava	k1gFnSc1	výprava
natáčející	natáčející	k2eAgFnSc1d1	natáčející
dokument	dokument	k1gInSc4	dokument
pro	pro	k7c4	pro
Discovery	Discovera	k1gFnPc4	Discovera
Channel	Channela	k1gFnPc2	Channela
<g/>
.	.	kIx.	.
</s>
<s>
Zeptali	zeptat	k5eAaPmAgMnP	zeptat
se	se	k3xPyFc4	se
Davida	David	k1gMnSc2	David
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
jsi	být	k5eAaImIp2nS	být
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
"	"	kIx"	"
<g/>
Jmenuji	jmenovat	k5eAaImIp1nS	jmenovat
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
Sharp	Sharp	kA	Sharp
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
tu	tu	k6eAd1	tu
s	s	k7c7	s
Asian	Asiany	k1gInPc2	Asiany
Trekking	Trekking	k1gInSc1	Trekking
a	a	k8xC	a
jen	jen	k9	jen
se	se	k3xPyFc4	se
chci	chtít	k5eAaImIp1nS	chtít
trochu	trochu	k6eAd1	trochu
vyspat	vyspat	k5eAaPmF	vyspat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
mu	on	k3xPp3gMnSc3	on
zásobu	zásoba	k1gFnSc4	zásoba
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
ho	on	k3xPp3gMnSc4	on
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Sharp	Sharp	kA	Sharp
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
(	(	kIx(	(
<g/>
či	či	k8xC	či
sestupu	sestup	k1gInSc2	sestup
<g/>
)	)	kIx)	)
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
až	až	k9	až
2010	[number]	k4	2010
219	[number]	k4	219
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgMnPc2	dva
Čechů	Čech	k1gMnPc2	Čech
(	(	kIx(	(
<g/>
Libor	Libor	k1gMnSc1	Libor
Kozák	Kozák	k1gMnSc1	Kozák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
a	a	k8xC	a
Věslav	Věslav	k1gMnSc1	Věslav
Chrzaszcz	Chrzaszcz	k1gMnSc1	Chrzaszcz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nepálu	Nepál	k1gInSc2	Nepál
smrt	smrt	k1gFnSc4	smrt
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
na	na	k7c4	na
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
pohřešovaných	pohřešovaná	k1gFnPc2	pohřešovaná
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
Šerpa	šerpa	k1gFnSc1	šerpa
Appa	Appa	k1gFnSc1	Appa
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
výstupů	výstup	k1gInPc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladšímu	mladý	k2eAgMnSc3d3	nejmladší
lezci	lezec	k1gMnSc3	lezec
na	na	k7c6	na
Everestu	Everest	k1gInSc6	Everest
bylo	být	k5eAaImAgNnS	být
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jím	on	k3xPp3gMnSc7	on
Američan	Američan	k1gMnSc1	Američan
Jordan	Jordan	k1gMnSc1	Jordan
Romer	Romer	k1gMnSc1	Romer
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
byl	být	k5eAaImAgInS	být
Šerpa	šerpa	k1gFnSc1	šerpa
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc1	Everest
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
horolezecké	horolezecký	k2eAgFnPc4d1	horolezecká
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgNnPc4d1	severovýchodní
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
méně	málo	k6eAd2	málo
časté	častý	k2eAgFnPc4d1	častá
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
méně	málo	k6eAd2	málo
obtížná	obtížný	k2eAgFnSc1d1	obtížná
než	než	k8xS	než
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
také	také	k9	také
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
frekventovanější	frekventovaný	k2eAgNnSc1d2	frekventovanější
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
cestou	cesta	k1gFnSc7	cesta
šli	jít	k5eAaImAgMnP	jít
také	také	k9	také
Hillary	Hillara	k1gFnPc4	Hillara
a	a	k8xC	a
Tenzing	Tenzing	k1gInSc4	Tenzing
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
prvovýstupu	prvovýstup	k1gInSc2	prvovýstup
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
předurčen	předurčen	k2eAgInSc1d1	předurčen
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Tibet	Tibet	k1gInSc1	Tibet
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
uzavřen	uzavřen	k2eAgMnSc1d1	uzavřen
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
před	před	k7c7	před
letní	letní	k2eAgFnSc7d1	letní
monzunovou	monzunový	k2eAgFnSc7d1	monzunová
sezónou	sezóna	k1gFnSc7	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
snižuje	snižovat	k5eAaImIp3nS	snižovat
rychlost	rychlost	k1gFnSc4	rychlost
tzv.	tzv.	kA	tzv.
tryskového	tryskový	k2eAgNnSc2d1	tryskové
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
po	po	k7c6	po
letní	letní	k2eAgFnSc6d1	letní
monzunové	monzunový	k2eAgFnSc6d1	monzunová
sezóně	sezóna	k1gFnSc6	sezóna
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výška	výška	k1gFnSc1	výška
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
činí	činit	k5eAaImIp3nS	činit
výstupy	výstup	k1gInPc4	výstup
mnohem	mnohem	k6eAd1	mnohem
obtížnějšími	obtížný	k2eAgInPc7d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
po	po	k7c6	po
letní	letní	k2eAgFnSc6d1	letní
monzunové	monzunový	k2eAgFnSc6d1	monzunová
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1973	[number]	k4	1973
japonské	japonský	k2eAgFnSc6d1	japonská
expedici	expedice	k1gFnSc6	expedice
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
jsou	být	k5eAaImIp3nP	být
zimní	zimní	k2eAgInPc1d1	zimní
výstupy	výstup	k1gInPc1	výstup
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výstupy	výstup	k1gInPc1	výstup
v	v	k7c6	v
období	období	k1gNnSc6	období
zimního	zimní	k2eAgInSc2d1	zimní
monzunu	monzun	k1gInSc2	monzun
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
horolezci	horolezec	k1gMnPc1	horolezec
musí	muset	k5eAaImIp3nP	muset
čelit	čelit	k5eAaImF	čelit
polárním	polární	k2eAgFnPc3d1	polární
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
častým	častý	k2eAgFnPc3d1	častá
vichřicím	vichřice	k1gFnPc3	vichřice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
méně	málo	k6eAd2	málo
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
zimních	zimní	k2eAgInPc2d1	zimní
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
deset	deset	k4xCc4	deset
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
a	a	k8xC	a
výstupy	výstup	k1gInPc1	výstup
na	na	k7c6	na
Everestu	Everest	k1gInSc6	Everest
byly	být	k5eAaImAgInP	být
značně	značně	k6eAd1	značně
ovlivněny	ovlivněn	k2eAgInPc1d1	ovlivněn
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
uzavřen	uzavřen	k2eAgInSc4d1	uzavřen
Nepál	Nepál	k1gInSc4	Nepál
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
Čína	Čína	k1gFnSc1	Čína
vojensky	vojensky	k6eAd1	vojensky
obsadila	obsadit	k5eAaPmAgFnS	obsadit
a	a	k8xC	a
znepřístupnila	znepřístupnit	k5eAaPmAgFnS	znepřístupnit
Tibet	Tibet	k1gInSc4	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stěn	stěna	k1gFnPc2	stěna
Everestu	Everest	k1gInSc2	Everest
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
východní	východní	k2eAgMnSc1d1	východní
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
do	do	k7c2	do
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
-	-	kIx~	-
klasická	klasický	k2eAgFnSc1d1	klasická
jižní	jižní	k2eAgFnSc1d1	jižní
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
ledopád	ledopád	k1gInSc4	ledopád
Khumbu	Khumb	k1gInSc2	Khumb
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgNnSc1d1	jižní
sedlo	sedlo	k1gNnSc1	sedlo
(	(	kIx(	(
<g/>
7986	[number]	k4	7986
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
JV	JV	kA	JV
hřeben	hřeben	k1gInSc1	hřeben
První	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
provedla	provést	k5eAaPmAgFnS	provést
britsko-novozélandská	britskoovozélandský	k2eAgFnSc1d1	britsko-novozélandský
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
prostoupila	prostoupit	k5eAaPmAgFnS	prostoupit
ledopád	ledopád	k1gInSc4	ledopád
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
6500	[number]	k4	6500
m.	m.	k?	m.
Následovaly	následovat	k5eAaImAgFnP	následovat
dvě	dva	k4xCgFnPc4	dva
švýcarské	švýcarský	k2eAgFnPc4d1	švýcarská
výpravy	výprava	k1gFnPc4	výprava
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Raymond	Raymonda	k1gFnPc2	Raymonda
Lambert	Lambert	k1gInSc1	Lambert
a	a	k8xC	a
Tenzing	Tenzing	k1gInSc1	Tenzing
Norkej	Norkej	k1gFnPc2	Norkej
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výšky	výška	k1gFnSc2	výška
8595	[number]	k4	8595
m.	m.	k?	m.
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
jejich	jejich	k3xOp3gFnSc7	jejich
cestou	cesta	k1gFnSc7	cesta
dokončila	dokončit	k5eAaPmAgFnS	dokončit
výstup	výstup	k1gInSc4	výstup
britská	britský	k2eAgFnSc1d1	britská
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Johnem	John	k1gMnSc7	John
Huntem	hunt	k1gInSc7	hunt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ledovcem	ledovec	k1gInSc7	ledovec
Khumbu	Khumb	k1gInSc2	Khumb
a	a	k8xC	a
Jižním	jižní	k2eAgNnSc7d1	jižní
sedlem	sedlo	k1gNnSc7	sedlo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
trasa	trasa	k1gFnSc1	trasa
doslova	doslova	k6eAd1	doslova
obležena	oblehnout	k5eAaPmNgFnS	oblehnout
komerčními	komerční	k2eAgFnPc7d1	komerční
expedicemi	expedice	k1gFnPc7	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
ji	on	k3xPp3gFnSc4	on
Šerpové	Šerpová	k1gFnSc6	Šerpová
vybaví	vybavit	k5eAaPmIp3nP	vybavit
fixními	fixní	k2eAgNnPc7d1	fixní
lany	lano	k1gNnPc7	lano
a	a	k8xC	a
upraví	upravit	k5eAaPmIp3nS	upravit
cestu	cesta	k1gFnSc4	cesta
ledopádem	ledopád	k1gInSc7	ledopád
Khumbu	Khumb	k1gInSc2	Khumb
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
fixních	fixní	k2eAgNnPc2d1	fixní
lan	lano	k1gNnPc2	lano
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příznivých	příznivý	k2eAgInPc6d1	příznivý
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
stovky	stovka	k1gFnPc4	stovka
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Šerpů	Šerp	k1gInPc2	Šerp
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dopravit	dopravit	k5eAaPmF	dopravit
platícího	platící	k2eAgMnSc4d1	platící
klienta	klient	k1gMnSc4	klient
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
pokoušejících	pokoušející	k2eAgMnPc2d1	pokoušející
se	se	k3xPyFc4	se
o	o	k7c6	o
dosažení	dosažení	k1gNnSc6	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
dobrodruzi	dobrodruh	k1gMnPc1	dobrodruh
než	než	k8xS	než
horolezci	horolezec	k1gMnPc1	horolezec
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
s	s	k7c7	s
sebou	se	k3xPyFc7	se
negativní	negativní	k2eAgInPc1d1	negativní
jevy	jev	k1gInPc4	jev
od	od	k7c2	od
znečišťování	znečišťování	k1gNnSc2	znečišťování
trasy	trasa	k1gFnSc2	trasa
odpadky	odpadek	k1gInPc4	odpadek
až	až	k8xS	až
po	po	k7c4	po
krádeže	krádež	k1gFnPc4	krádež
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
vynesených	vynesený	k2eAgFnPc2d1	vynesená
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
-	-	kIx~	-
klasická	klasický	k2eAgFnSc1d1	klasická
severní	severní	k2eAgFnSc1d1	severní
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Severní	severní	k2eAgNnSc4d1	severní
sedlo	sedlo	k1gNnSc4	sedlo
(	(	kIx(	(
<g/>
7000	[number]	k4	7000
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc4d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
hřeben	hřeben	k1gInSc4	hřeben
Trasa	trasa	k1gFnSc1	trasa
prvních	první	k4xOgInPc2	první
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
Britové	Brit	k1gMnPc1	Brit
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
sedm	sedm	k4xCc4	sedm
expedic	expedice	k1gFnPc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
úspěchu	úspěch	k1gInSc3	úspěch
byly	být	k5eAaImAgFnP	být
třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
výprava	výprava	k1gFnSc1	výprava
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
a	a	k8xC	a
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
sedm	sedm	k4xCc1	sedm
horolezců	horolezec	k1gMnPc2	horolezec
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
výškový	výškový	k2eAgInSc1d1	výškový
rekord	rekord	k1gInSc1	rekord
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
překonán	překonat	k5eAaPmNgInS	překonat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
8500	[number]	k4	8500
<g/>
-	-	kIx~	-
<g/>
8600	[number]	k4	8600
m.	m.	k?	m.
Zastavily	zastavit	k5eAaPmAgFnP	zastavit
je	být	k5eAaImIp3nS	být
fyzická	fyzický	k2eAgFnSc1d1	fyzická
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
také	také	k9	také
technické	technický	k2eAgFnPc4d1	technická
obtíže	obtíž	k1gFnPc4	obtíž
tzv.	tzv.	kA	tzv.
Druhého	druhý	k4xOgInSc2	druhý
výšvihu	výšvih	k1gInSc2	výšvih
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
severní	severní	k2eAgFnSc2d1	severní
cestou	cesta	k1gFnSc7	cesta
dokončila	dokončit	k5eAaPmAgFnS	dokončit
obrovská	obrovský	k2eAgFnSc1d1	obrovská
čínská	čínský	k2eAgFnSc1d1	čínská
expedice	expedice	k1gFnSc1	expedice
čítající	čítající	k2eAgFnSc1d1	čítající
přes	přes	k7c4	přes
200	[number]	k4	200
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Číňané	Číňan	k1gMnPc1	Číňan
Wang	Wanga	k1gFnPc2	Wanga
Fu-čchou	Fu-čchý	k2eAgFnSc4d1	Fu-čchý
<g/>
,	,	kIx,	,
Čchu	Čcha	k1gFnSc4	Čcha
Ťin-chua	Ťinhu	k1gInSc2	Ťin-chu
a	a	k8xC	a
Tibeťan	Tibeťan	k1gMnSc1	Tibeťan
Gonpa	Gonp	k1gMnSc2	Gonp
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
trasa	trasa	k1gFnSc1	trasa
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
obležení	obležení	k1gNnSc6	obležení
komerčních	komerční	k2eAgFnPc2d1	komerční
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
zajištěná	zajištěný	k2eAgFnSc1d1	zajištěná
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
fixními	fixní	k2eAgNnPc7d1	fixní
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
ledopád	ledopád	k1gInSc4	ledopád
Khumbu	Khumb	k1gInSc2	Khumb
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc1d1	západní
rameno	rameno	k1gNnSc1	rameno
(	(	kIx(	(
<g/>
7200	[number]	k4	7200
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hornbeinův	Hornbeinův	k2eAgInSc1d1	Hornbeinův
kuloár	kuloár	k1gInSc1	kuloár
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
stěně	stěna	k1gFnSc6	stěna
Trasu	tras	k1gInSc2	tras
otevřela	otevřít	k5eAaPmAgFnS	otevřít
americká	americký	k2eAgFnSc1d1	americká
výprava	výprava	k1gFnSc1	výprava
vedená	vedený	k2eAgFnSc1d1	vedená
Normanem	Norman	k1gMnSc7	Norman
Dyhrenfurthem	Dyhrenfurth	k1gInSc7	Dyhrenfurth
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
současně	současně	k6eAd1	současně
postupovala	postupovat	k5eAaImAgFnS	postupovat
i	i	k9	i
klasickou	klasický	k2eAgFnSc7d1	klasická
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
trasách	trasa	k1gFnPc6	trasa
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1	prvovýstup
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
rameno	rameno	k1gNnSc4	rameno
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
William	William	k1gInSc4	William
Unsoeld	Unsoelda	k1gFnPc2	Unsoelda
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
Hornbein	Hornbein	k1gMnSc1	Hornbein
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tudy	tudy	k6eAd1	tudy
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
stoupali	stoupat	k5eAaImAgMnP	stoupat
a	a	k8xC	a
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
jen	jen	k9	jen
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sestupu	sestup	k1gInSc6	sestup
museli	muset	k5eAaImAgMnP	muset
bivakovat	bivakovat	k5eAaImF	bivakovat
v	v	k7c6	v
8400	[number]	k4	8400
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
vážné	vážný	k2eAgFnPc4d1	vážná
omrzliny	omrzlina	k1gFnPc4	omrzlina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
otevření	otevření	k1gNnSc2	otevření
nové	nový	k2eAgFnSc2d1	nová
trasy	trasa	k1gFnSc2	trasa
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
i	i	k9	i
první	první	k4xOgInSc1	první
přechod	přechod	k1gInSc1	přechod
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
-	-	kIx~	-
Bonningtonova	Bonningtonův	k2eAgFnSc1d1	Bonningtonův
cesta	cesta	k1gFnSc1	cesta
JZ	JZ	kA	JZ
stěnou	stěna	k1gFnSc7	stěna
O	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
nejstrmější	strmý	k2eAgFnSc7d3	nejstrmější
stěnou	stěna	k1gFnSc7	stěna
Everestu	Everest	k1gInSc2	Everest
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
foto	foto	k1gNnSc4	foto
v	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
Historie	historie	k1gFnSc1	historie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
šest	šest	k4xCc4	šest
výprav	výprava	k1gFnPc2	výprava
(	(	kIx(	(
<g/>
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
počasí	počasí	k1gNnSc1	počasí
a	a	k8xC	a
technické	technický	k2eAgFnPc4d1	technická
obtíže	obtíž	k1gFnPc4	obtíž
skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
8200	[number]	k4	8200
<g/>
-	-	kIx~	-
<g/>
8500	[number]	k4	8500
m	m	kA	m
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Žlutý	žlutý	k2eAgInSc4d1	žlutý
pás	pás	k1gInSc4	pás
nebo	nebo	k8xC	nebo
Rockband	Rockband	k1gInSc4	Rockband
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
<g/>
,	,	kIx,	,
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podařil	podařit	k5eAaPmAgInS	podařit
levou	levý	k2eAgFnSc7d1	levá
variantou	varianta	k1gFnSc7	varianta
britské	britský	k2eAgFnSc3d1	britská
expedici	expedice	k1gFnSc3	expedice
vedené	vedený	k2eAgFnSc3d1	vedená
Chrisem	Chris	k1gInSc7	Chris
Bonningtonem	Bonnington	k1gInSc7	Bonnington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Doug	Doug	k1gMnSc1	Doug
Scott	Scott	k1gMnSc1	Scott
a	a	k8xC	a
Dougal	Dougal	k1gMnSc1	Dougal
Haston	Haston	k1gInSc4	Haston
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Peter	Peter	k1gMnSc1	Peter
Boardman	Boardman	k1gMnSc1	Boardman
s	s	k7c7	s
Šerpou	šerpa	k1gFnSc7	šerpa
Pertembou	Pertemba	k1gFnSc7	Pertemba
a	a	k8xC	a
sólo	sólo	k2eAgMnSc1d1	sólo
Mick	Mick	k1gMnSc1	Mick
Burke	Burke	k1gNnSc2	Burke
(	(	kIx(	(
<g/>
ztratil	ztratit	k5eAaPmAgMnS	ztratit
se	se	k3xPyFc4	se
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zopakovat	zopakovat	k5eAaPmF	zopakovat
třikrát	třikrát	k6eAd1	třikrát
<g/>
:	:	kIx,	:
1988	[number]	k4	1988
slovenský	slovenský	k2eAgInSc4d1	slovenský
výstup	výstup	k1gInSc4	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Japonci	Japonec	k1gMnPc7	Japonec
(	(	kIx(	(
<g/>
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
)	)	kIx)	)
a	a	k8xC	a
1995	[number]	k4	1995
Korejci	Korejec	k1gMnPc7	Korejec
expedičním	expediční	k2eAgInSc7d1	expediční
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
-	-	kIx~	-
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
cesta	cesta	k1gFnSc1	cesta
západním	západní	k2eAgInSc7d1	západní
hřebenem	hřeben	k1gInSc7	hřeben
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
pokusila	pokusit	k5eAaPmAgFnS	pokusit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
lavině	lavina	k1gFnSc6	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1	prvovýstup
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
slovinským	slovinský	k2eAgMnSc7d1	slovinský
a	a	k8xC	a
chorvatským	chorvatský	k2eAgMnPc3d1	chorvatský
horolezcům	horolezec	k1gMnPc3	horolezec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
trasu	trasa	k1gFnSc4	trasa
na	na	k7c6	na
většinou	většinou	k6eAd1	většinou
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Andrej	Andrej	k1gMnSc1	Andrej
Stremfelj	Stremfelj	k1gMnSc1	Stremfelj
a	a	k8xC	a
Jernej	Jernej	k1gMnSc1	Jernej
Zaplotnik	Zaplotnik	k1gMnSc1	Zaplotnik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
družstvu	družstvo	k1gNnSc6	družstvo
je	být	k5eAaImIp3nS	být
následovali	následovat	k5eAaImAgMnP	následovat
Stipe	Stip	k1gMnSc5	Stip
Božić	Božić	k1gMnSc5	Božić
<g/>
,	,	kIx,	,
Stane	stanout	k5eAaPmIp3nS	stanout
Belak	Belak	k1gMnSc1	Belak
a	a	k8xC	a
Šerpa	šerpa	k1gFnSc1	šerpa
Ang	Ang	k1gMnSc2	Ang
Phu	Phu	k1gMnSc2	Phu
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabil	zabít	k5eAaPmAgMnS	zabít
pádem	pád	k1gInSc7	pád
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sestupu	sestup	k1gInSc3	sestup
obě	dva	k4xCgFnPc1	dva
družstva	družstvo	k1gNnSc2	družstvo
použila	použít	k5eAaPmAgFnS	použít
snazší	snadný	k2eAgFnSc4d2	snazší
americkou	americký	k2eAgFnSc4d1	americká
cestu	cesta	k1gFnSc4	cesta
Hornbeinovým	Hornbeinův	k2eAgInSc7d1	Hornbeinův
kuloárem	kuloár	k1gInSc7	kuloár
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jugoslávskou	jugoslávský	k2eAgFnSc4d1	jugoslávská
trasu	trasa	k1gFnSc4	trasa
křižuje	křižovat	k5eAaImIp3nS	křižovat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
7300	[number]	k4	7300
m.	m.	k?	m.
Kompletní	kompletní	k2eAgFnSc4d1	kompletní
cestu	cesta	k1gFnSc4	cesta
západním	západní	k2eAgInSc7d1	západní
hřebenem	hřeben	k1gInSc7	hřeben
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
bulharská	bulharský	k2eAgFnSc1d1	bulharská
expedice	expedice	k1gFnSc1	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
dalších	další	k2eAgFnPc2d1	další
výprav	výprava	k1gFnPc2	výprava
zvolila	zvolit	k5eAaPmAgFnS	zvolit
kombinaci	kombinace	k1gFnSc4	kombinace
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
nejobtížnější	obtížný	k2eAgFnSc4d3	nejobtížnější
části	část	k1gFnPc4	část
hřebene	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
japonská	japonský	k2eAgNnPc1d1	Japonské
diretissima	diretissima	k1gNnSc4	diretissima
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
tibetských	tibetský	k2eAgFnPc2d1	tibetská
hranic	hranice	k1gFnPc2	hranice
vylezli	vylézt	k5eAaPmAgMnP	vylézt
Japonci	Japonec	k1gMnPc1	Japonec
velmi	velmi	k6eAd1	velmi
přímou	přímý	k2eAgFnSc4d1	přímá
trasu	trasa	k1gFnSc4	trasa
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
stěně	stěna	k1gFnSc6	stěna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
8200	[number]	k4	8200
m	m	kA	m
napojuje	napojovat	k5eAaImIp3nS	napojovat
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
Hornbeinově	Hornbeinův	k2eAgInSc6d1	Hornbeinův
kuloáru	kuloár	k1gInSc6	kuloár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
stáli	stát	k5eAaImAgMnP	stát
Takaši	Takaš	k1gMnPc1	Takaš
Ozaki	Ozaki	k1gNnPc2	Ozaki
a	a	k8xC	a
Tsuneo	Tsuneo	k6eAd1	Tsuneo
Šigehiro	Šigehiro	k1gNnSc1	Šigehiro
<g/>
.	.	kIx.	.
</s>
<s>
Trasu	trasa	k1gFnSc4	trasa
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Erhard	Erharda	k1gFnPc2	Erharda
Loretan	Loretan	k1gInSc1	Loretan
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Troillet	Troillet	k1gInSc4	Troillet
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
za	za	k7c4	za
pouhých	pouhý	k2eAgFnPc2d1	pouhá
36	[number]	k4	36
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
několik	několik	k4yIc4	několik
málo	málo	k6eAd1	málo
dalších	další	k2eAgFnPc2d1	další
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
polská	polský	k2eAgFnSc1d1	polská
cesta	cesta	k1gFnSc1	cesta
jižním	jižní	k2eAgInSc7d1	jižní
pilířem	pilíř	k1gInSc7	pilíř
Cesta	cesta	k1gFnSc1	cesta
sleduje	sledovat	k5eAaImIp3nS	sledovat
pravý	pravý	k2eAgInSc4d1	pravý
okraj	okraj	k1gInSc4	okraj
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
v	v	k7c6	v
8100	[number]	k4	8100
m	m	kA	m
překonává	překonávat	k5eAaImIp3nS	překonávat
obtížný	obtížný	k2eAgInSc4d1	obtížný
skalní	skalní	k2eAgInSc4d1	skalní
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
expedice	expedice	k1gFnSc2	expedice
vedené	vedený	k2eAgFnSc2d1	vedená
Andrzejem	Andrzej	k1gInSc7	Andrzej
Zawadou	Zawada	k1gFnSc7	Zawada
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Andrzej	Andrzej	k1gFnSc2	Andrzej
Czok	Czoka	k1gFnPc2	Czoka
a	a	k8xC	a
Jerzy	Jerza	k1gFnSc2	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
československá	československý	k2eAgFnSc1d1	Československá
expedice	expedice	k1gFnSc1	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pár	pár	k4xCyI	pár
dalších	další	k2eAgFnPc2d1	další
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
Messnerova	Messnerův	k2eAgFnSc1d1	Messnerova
cesta	cesta	k1gFnSc1	cesta
Reinhold	Reinhold	k1gMnSc1	Reinhold
Messner	Messner	k1gMnSc1	Messner
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
třídenního	třídenní	k2eAgInSc2d1	třídenní
sólovýstupu	sólovýstup	k1gInSc2	sólovýstup
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
7200	[number]	k4	7200
m	m	kA	m
klasickou	klasický	k2eAgFnSc4d1	klasická
severní	severní	k2eAgFnSc4d1	severní
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
prostoupil	prostoupit	k5eAaPmAgMnS	prostoupit
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
a	a	k8xC	a
Nortonovým	Nortonový	k2eAgInSc7d1	Nortonový
kuloárem	kuloár	k1gInSc7	kuloár
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
není	být	k5eNaImIp3nS	být
často	často	k6eAd1	často
lezena	lezen	k2eAgFnSc1d1	lezena
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
lezců	lezec	k1gMnPc2	lezec
sleduje	sledovat	k5eAaImIp3nS	sledovat
raději	rád	k6eAd2	rád
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snazší	snadný	k2eAgFnSc4d2	snazší
klasickou	klasický	k2eAgFnSc4d1	klasická
severní	severní	k2eAgFnSc4d1	severní
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
-	-	kIx~	-
sovětská	sovětský	k2eAgFnSc1d1	sovětská
cesta	cesta	k1gFnSc1	cesta
JZ	JZ	kA	JZ
pilířem	pilíř	k1gInSc7	pilíř
Horolezci	horolezec	k1gMnSc6	horolezec
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
cestu	cesta	k1gFnSc4	cesta
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
Bonningtonovy	Bonningtonův	k2eAgFnSc2d1	Bonningtonův
trasy	trasa	k1gFnSc2	trasa
velmi	velmi	k6eAd1	velmi
obtížným	obtížný	k2eAgInSc7d1	obtížný
skalním	skalní	k2eAgInSc7d1	skalní
pilířem	pilíř	k1gInSc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8500	[number]	k4	8500
m	m	kA	m
se	se	k3xPyFc4	se
výstup	výstup	k1gInSc1	výstup
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
západní	západní	k2eAgInSc4d1	západní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
družstvech	družstvo	k1gNnPc6	družstvo
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
devět	devět	k4xCc1	devět
horolezců	horolezec	k1gMnPc2	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zopakován	zopakovat	k5eAaPmNgInS	zopakovat
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
cesta	cesta	k1gFnSc1	cesta
V	v	k7c6	v
stěnou	stěna	k1gFnSc7	stěna
Prvnímu	první	k4xOgInSc3	první
výstupu	výstup	k1gInSc2	výstup
východní	východní	k2eAgFnSc7d1	východní
stěnou	stěna	k1gFnSc7	stěna
předcházel	předcházet	k5eAaImAgInS	předcházet
jeden	jeden	k4xCgInSc4	jeden
také	také	k6eAd1	také
americký	americký	k2eAgInSc4d1	americký
pokus	pokus	k1gInSc4	pokus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
překonává	překonávat	k5eAaImIp3nS	překonávat
převýšení	převýšení	k1gNnSc4	převýšení
3500	[number]	k4	3500
m	m	kA	m
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
obtížné	obtížný	k2eAgFnSc2d1	obtížná
skalní	skalní	k2eAgFnSc2d1	skalní
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
lavinami	lavina	k1gFnPc7	lavina
ohrožovaný	ohrožovaný	k2eAgInSc4d1	ohrožovaný
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
dvě	dva	k4xCgFnPc1	dva
trojice	trojice	k1gFnPc1	trojice
horolezců	horolezec	k1gMnPc2	horolezec
-	-	kIx~	-
Kim	Kim	k1gMnSc1	Kim
Momb	Momb	k1gMnSc1	Momb
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc1	Lou
Reichardt	Reichardt	k1gMnSc1	Reichardt
<g/>
,	,	kIx,	,
Carlos	Carlos	k1gMnSc1	Carlos
Buhler	Buhler	k1gMnSc1	Buhler
a	a	k8xC	a
George	Georg	k1gMnSc4	Georg
Lowe	Low	k1gMnSc4	Low
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Reid	Reid	k1gMnSc1	Reid
<g/>
,	,	kIx,	,
Jay	Jay	k1gMnSc1	Jay
Cassell	Cassell	k1gMnSc1	Cassell
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zopakován	zopakovat	k5eAaPmNgInS	zopakovat
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
-	-	kIx~	-
australská	australský	k2eAgFnSc1d1	australská
cesta	cesta	k1gFnSc1	cesta
Velkým	velký	k2eAgInSc7d1	velký
(	(	kIx(	(
<g/>
Nortonovým	Nortonový	k2eAgInSc7d1	Nortonový
<g/>
)	)	kIx)	)
kuloárem	kuloár	k1gInSc7	kuloár
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
stěně	stěna	k1gFnSc6	stěna
Trasu	tras	k1gInSc2	tras
prostoupila	prostoupit	k5eAaPmAgFnS	prostoupit
malá	malý	k2eAgFnSc1d1	malá
výprava	výprava	k1gFnSc1	výprava
několika	několik	k4yIc2	několik
přátel	přítel	k1gMnPc2	přítel
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
šerpských	šerpský	k2eAgInPc2d1	šerpský
nosičů	nosič	k1gInPc2	nosič
a	a	k8xC	a
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Tim	Tim	k?	Tim
McCartney-Snape	McCartney-Snap	k1gInSc5	McCartney-Snap
a	a	k8xC	a
Greg	Greg	k1gMnSc1	Greg
Mortimer	Mortimer	k1gMnSc1	Mortimer
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
spolulezec	spolulezec	k1gMnSc1	spolulezec
Henderson	Henderson	k1gMnSc1	Henderson
musel	muset	k5eAaImAgMnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
50	[number]	k4	50
m	m	kA	m
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
-	-	kIx~	-
pilíř	pilíř	k1gInSc1	pilíř
spadající	spadající	k2eAgInSc1d1	spadající
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
ramene	rameno	k1gNnSc2	rameno
Everestu	Everest	k1gInSc2	Everest
k	k	k7c3	k
severu	sever	k1gInSc3	sever
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
výprava	výprava	k1gFnSc1	výprava
zlezla	zlézt	k5eAaPmAgFnS	zlézt
poprvé	poprvé	k6eAd1	poprvé
pilíř	pilíř	k1gInSc4	pilíř
spadající	spadající	k2eAgInPc4d1	spadající
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
ramene	rameno	k1gNnSc2	rameno
Everestu	Everest	k1gInSc2	Everest
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7250	[number]	k4	7250
m	m	kA	m
se	se	k3xPyFc4	se
trasa	trasa	k1gFnSc1	trasa
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Mike	Mike	k1gFnSc4	Mike
Congdon	Congdona	k1gFnPc2	Congdona
a	a	k8xC	a
horolezkyně	horolezkyně	k1gFnSc2	horolezkyně
Sharon	Sharon	k1gMnSc1	Sharon
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zlezla	zlézt	k5eAaPmAgFnS	zlézt
Everest	Everest	k1gInSc4	Everest
novou	nový	k2eAgFnSc7d1	nová
cestou	cesta	k1gFnSc7	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1988	[number]	k4	1988
-	-	kIx~	-
východní	východní	k2eAgFnSc7d1	východní
stěnou	stěna	k1gFnSc7	stěna
vlevo	vlevo	k6eAd1	vlevo
ve	v	k7c6	v
spádnici	spádnice	k1gFnSc6	spádnice
Jižního	jižní	k2eAgNnSc2d1	jižní
sedla	sedlo	k1gNnSc2	sedlo
Australsko-britská	australskoritský	k2eAgFnSc1d1	australsko-britský
malá	malý	k2eAgFnSc1d1	malá
výprava	výprava	k1gFnSc1	výprava
otevřela	otevřít	k5eAaPmAgFnS	otevřít
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
východní	východní	k2eAgFnSc7d1	východní
stěnou	stěna	k1gFnSc7	stěna
vlevo	vlevo	k6eAd1	vlevo
ve	v	k7c6	v
spádnici	spádnice	k1gFnSc6	spádnice
Jižního	jižní	k2eAgNnSc2d1	jižní
sedla	sedlo	k1gNnSc2	sedlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
trasa	trasa	k1gFnSc1	trasa
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
sólo	sólo	k2eAgMnSc1d1	sólo
Angličan	Angličan	k1gMnSc1	Angličan
Stephen	Stephna	k1gFnPc2	Stephna
Venables	Venables	k1gMnSc1	Venables
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
horolezci	horolezec	k1gMnPc1	horolezec
z	z	k7c2	z
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
-	-	kIx~	-
severovýchodní	severovýchodní	k2eAgInSc1d1	severovýchodní
hřeben	hřeben	k1gInSc1	hřeben
Trasu	trasa	k1gFnSc4	trasa
celým	celý	k2eAgInSc7d1	celý
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
přes	přes	k7c4	přes
Tři	tři	k4xCgFnPc4	tři
věže	věž	k1gFnPc4	věž
poprvé	poprvé	k6eAd1	poprvé
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
Britové	Brit	k1gMnPc1	Brit
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
8200	[number]	k4	8200
m.	m.	k?	m.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dva	dva	k4xCgMnPc1	dva
Britové	Brit	k1gMnPc1	Brit
prostoupili	prostoupit	k5eAaPmAgMnP	prostoupit
hřeben	hřeben	k1gInSc4	hřeben
až	až	k9	až
do	do	k7c2	do
8450	[number]	k4	8450
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojili	napojit	k5eAaPmAgMnP	napojit
do	do	k7c2	do
klasické	klasický	k2eAgFnSc2d1	klasická
severní	severní	k2eAgFnSc2d1	severní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
ale	ale	k9	ale
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
<g/>
,	,	kIx,	,
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
kompletní	kompletní	k2eAgInSc4d1	kompletní
výstup	výstup	k1gInSc4	výstup
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
dokončila	dokončit	k5eAaPmAgFnS	dokončit
velká	velký	k2eAgFnSc1d1	velká
japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hřeben	hřeben	k1gInSc4	hřeben
zabezpečila	zabezpečit	k5eAaPmAgFnS	zabezpečit
4	[number]	k4	4
km	km	kA	km
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
Šerpové	Šerp	k1gMnPc1	Šerp
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
-	-	kIx~	-
kazašská	kazašský	k2eAgFnSc1d1	kazašská
cesta	cesta	k1gFnSc1	cesta
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
stěnou	stěna	k1gFnSc7	stěna
Kazaši	Kazach	k1gMnPc1	Kazach
stoupali	stoupat	k5eAaImAgMnP	stoupat
stěnou	stěna	k1gFnSc7	stěna
mezi	mezi	k7c7	mezi
severním	severní	k2eAgInSc7d1	severní
a	a	k8xC	a
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
8000	[number]	k4	8000
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojili	napojit	k5eAaPmAgMnP	napojit
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
severní	severní	k2eAgFnSc4d1	severní
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
-	-	kIx~	-
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
mezi	mezi	k7c7	mezi
Hornbeinovým	Hornbeinův	k2eAgInSc7d1	Hornbeinův
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
kuloárem	kuloár	k1gInSc7	kuloár
Ruská	ruský	k2eAgFnSc1d1	ruská
expedice	expedice	k1gFnSc1	expedice
otevřela	otevřít	k5eAaPmAgFnS	otevřít
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
trasu	trasa	k1gFnSc4	trasa
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
mezi	mezi	k7c7	mezi
Hornbeinovým	Hornbeinův	k2eAgInSc7d1	Hornbeinův
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
kuloárem	kuloár	k1gInSc7	kuloár
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
horolezců	horolezec	k1gMnPc2	horolezec
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
družstvech	družstvo	k1gNnPc6	družstvo
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
překonává	překonávat	k5eAaImIp3nS	překonávat
dvě	dva	k4xCgFnPc4	dva
náročné	náročný	k2eAgFnPc4d1	náročná
skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
v	v	k7c4	v
7200	[number]	k4	7200
a	a	k8xC	a
8500	[number]	k4	8500
m.	m.	k?	m.
2009	[number]	k4	2009
-	-	kIx~	-
korejská	korejský	k2eAgFnSc1d1	Korejská
cesta	cesta	k1gFnSc1	cesta
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
stěnou	stěna	k1gFnSc7	stěna
Jihokorejská	jihokorejský	k2eAgFnSc1d1	jihokorejská
expedice	expedice	k1gFnSc1	expedice
dokončila	dokončit	k5eAaPmAgFnS	dokončit
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnPc1	vedoucí
výpravy	výprava	k1gFnSc2	výprava
Pak	pak	k6eAd1	pak
Jŏ	Jŏ	k1gFnSc7	Jŏ
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
třemi	tři	k4xCgInPc7	tři
horolezci	horolezec	k1gMnPc7	horolezec
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnPc4	hora
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
klasickou	klasický	k2eAgFnSc7d1	klasická
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgInPc4d1	předchozí
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
pokusy	pokus	k1gInPc4	pokus
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
popis	popis	k1gInSc1	popis
expedice	expedice	k1gFnSc1	expedice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
existuje	existovat	k5eAaImIp3nS	existovat
17	[number]	k4	17
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
variant	varianta	k1gFnPc2	varianta
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgInPc1d1	používaný
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
šest	šest	k4xCc1	šest
cest	cesta	k1gFnPc2	cesta
nebylo	být	k5eNaImAgNnS	být
od	od	k7c2	od
prvovýstupu	prvovýstup	k1gInSc2	prvovýstup
nikdy	nikdy	k6eAd1	nikdy
zopakováno	zopakován	k2eAgNnSc1d1	zopakováno
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
dva	dva	k4xCgInPc1	dva
prvovýstupy	prvovýstup	k1gInPc1	prvovýstup
byly	být	k5eAaImAgInP	být
podniknuty	podniknout	k5eAaPmNgInP	podniknout
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
kuloár	kuloár	k1gInSc1	kuloár
1984	[number]	k4	1984
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
stěna	stěna	k1gFnSc1	stěna
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Everestu	Everest	k1gInSc2	Everest
stálo	stát	k5eAaImAgNnS	stát
už	už	k9	už
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kyslíkového	kyslíkový	k2eAgInSc2d1	kyslíkový
přístroje	přístroj	k1gInSc2	přístroj
zhruba	zhruba	k6eAd1	zhruba
jen	jen	k9	jen
100	[number]	k4	100
<g/>
,	,	kIx,	,
po	po	k7c6	po
výstupu	výstup	k1gInSc2	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
Šerpů	Šerp	k1gInPc2	Šerp
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
zajištění	zajištění	k1gNnSc2	zajištění
trasy	trasa	k1gFnSc2	trasa
lany	lano	k1gNnPc7	lano
<g/>
)	)	kIx)	)
jen	jen	k9	jen
tři	tři	k4xCgMnPc4	tři
(	(	kIx(	(
<g/>
Messner	Messner	k1gInSc1	Messner
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Loretan	Loretan	k1gInSc1	Loretan
a	a	k8xC	a
Troillet	Troillet	k1gInSc1	Troillet
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
tras	trasa	k1gFnPc2	trasa
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
na	na	k7c4	na
www.explorersweb.com	www.explorersweb.com	k1gInSc4	www.explorersweb.com
Možnosti	možnost	k1gFnSc3	možnost
nových	nový	k2eAgInPc2d1	nový
prvovýstupů	prvovýstup	k1gInPc2	prvovýstup
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Logickou	logický	k2eAgFnSc7d1	logická
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Fantasy	fantas	k1gInPc7	fantas
Ridge	Ridge	k1gNnSc2	Ridge
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
východní	východní	k2eAgFnSc2d1	východní
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Pilíř	pilíř	k1gInSc1	pilíř
je	být	k5eAaImIp3nS	být
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
lavinami	lavina	k1gFnPc7	lavina
a	a	k8xC	a
napojuje	napojovat	k5eAaImIp3nS	napojovat
se	se	k3xPyFc4	se
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
hřeben	hřeben	k1gInSc4	hřeben
teprve	teprve	k6eAd1	teprve
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
nejobtížnějším	obtížný	k2eAgNnSc7d3	nejobtížnější
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
výstupovou	výstupový	k2eAgFnSc4d1	výstupová
trasu	trasa	k1gFnSc4	trasa
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
převýšením	převýšení	k1gNnSc7	převýšení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možnou	možný	k2eAgFnSc7d1	možná
trasou	trasa	k1gFnSc7	trasa
je	být	k5eAaImIp3nS	být
pravá	pravý	k2eAgFnSc1d1	pravá
část	část	k1gFnSc1	část
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
stěny	stěna	k1gFnSc2	stěna
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
Bonningtonovy	Bonningtonův	k2eAgFnSc2d1	Bonningtonův
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
prostoupit	prostoupit	k5eAaPmF	prostoupit
tři	tři	k4xCgFnPc1	tři
expedice	expedice	k1gFnPc1	expedice
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
také	také	k9	také
československá	československý	k2eAgFnSc1d1	Československá
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
dosažený	dosažený	k2eAgInSc1d1	dosažený
bod	bod	k1gInSc1	bod
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
je	být	k5eAaImIp3nS	být
8350	[number]	k4	8350
m	m	kA	m
(	(	kIx(	(
<g/>
Dougal	Dougal	k1gInSc1	Dougal
Haston	Haston	k1gInSc1	Haston
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Whillans	Whillans	k1gInSc4	Whillans
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
14	[number]	k4	14
osmitisícovek	osmitisícovka	k1gFnPc2	osmitisícovka
a	a	k8xC	a
jako	jako	k9	jako
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Asie	Asie	k1gFnSc2	Asie
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
Koruny	koruna	k1gFnSc2	koruna
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
australský	australský	k2eAgMnSc1d1	australský
horolezec	horolezec	k1gMnSc1	horolezec
Tim	Tim	k?	Tim
McCartney-Snape	McCartney-Snap	k1gInSc5	McCartney-Snap
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
vrcholu	vrchol	k1gInSc2	vrchol
zdoláním	zdolání	k1gNnSc7	zdolání
celé	celý	k2eAgFnSc2d1	celá
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
od	od	k7c2	od
0	[number]	k4	0
m	m	kA	m
až	až	k6eAd1	až
do	do	k7c2	do
samotného	samotný	k2eAgInSc2d1	samotný
vrcholu	vrchol	k1gInSc2	vrchol
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
pěšky	pěšky	k6eAd1	pěšky
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
Bengálském	bengálský	k2eAgInSc6d1	bengálský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
