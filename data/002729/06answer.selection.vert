<s>
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
(	(	kIx(	(
<g/>
tibetsky	tibetsky	k6eAd1	tibetsky
ཇ	ཇ	k?	ཇ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
ོ	ོ	k?	ོ
<g/>
་	་	k?	་
<g/>
ག	ག	k?	ག
<g/>
ླ	ླ	k?	ླ
<g/>
ང	ང	k?	ང
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
,	,	kIx,	,
Džomolangma	Džomolangma	k1gFnSc1	Džomolangma
<g/>
;	;	kIx,	;
nepálsky	nepálsky	k6eAd1	nepálsky
स	स	k?	स
<g/>
ा	ा	k?	ा
<g/>
थ	थ	k?	थ
<g/>
ा	ा	k?	ा
<g/>
,	,	kIx,	,
Sagarmátha	Sagarmátha	k1gFnSc1	Sagarmátha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
8848	[number]	k4	8848
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
starších	starý	k2eAgInPc2d2	starší
údajů	údaj	k1gInPc2	údaj
i	i	k9	i
8850	[number]	k4	8850
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
