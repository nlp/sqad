<s>
Posláním	poslání	k1gNnSc7
a	a	k8xC
hlavní	hlavní	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
činnosti	činnost	k1gFnSc2
Českého	český	k2eAgInSc2d1
institutu	institut	k1gInSc2
pro	pro	k7c4
akreditaci	akreditace	k1gFnSc4
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
prospěšné	prospěšný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
ČIA	ČIA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
poskytování	poskytování	k1gNnSc1
služeb	služba	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
akreditace	akreditace	k1gFnSc2
komerčním	komerční	k2eAgInPc3d1
subjektům	subjekt	k1gInPc3
a	a	k8xC
orgánům	orgán	k1gInPc3
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>