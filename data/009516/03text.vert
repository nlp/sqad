<p>
<s>
Honšú	Honšú	k?	Honšú
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
本	本	k?	本
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
japonský	japonský	k2eAgInSc4d1	japonský
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
Kjúšú	Kjúšú	k1gFnSc2	Kjúšú
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
východní	východní	k2eAgInPc1d1	východní
břehy	břeh	k1gInPc1	břeh
omývá	omývat	k5eAaImIp3nS	omývat
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
Japonské	japonský	k2eAgNnSc1d1	Japonské
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sedmý	sedmý	k4xOgInSc1	sedmý
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
(	(	kIx(	(
<g/>
po	po	k7c6	po
Jávě	Jáva	k1gFnSc6	Jáva
<g/>
)	)	kIx)	)
ostrov	ostrov	k1gInSc1	ostrov
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografická	geografický	k2eAgFnSc1d1	geografická
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1300	[number]	k4	1300
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
50	[number]	k4	50
až	až	k9	až
230	[number]	k4	230
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
rozlohou	rozloha	k1gFnSc7	rozloha
230	[number]	k4	230
500	[number]	k4	500
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Honšú	Honšú	k?	Honšú
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
ostrov	ostrov	k1gInSc4	ostrov
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Honšú	Honšú	k?	Honšú
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
450	[number]	k4	450
km	km	kA	km
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hornatý	hornatý	k2eAgInSc1d1	hornatý
ostrov	ostrov	k1gInSc1	ostrov
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vulkány	vulkán	k1gInPc7	vulkán
je	být	k5eAaImIp3nS	být
dějištěm	dějiště	k1gNnSc7	dějiště
častých	častý	k2eAgNnPc2d1	časté
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
Fudžisan	Fudžisan	k1gInSc1	Fudžisan
(	(	kIx(	(
<g/>
Hora	hora	k1gFnSc1	hora
Fudži	Fudž	k1gFnSc6	Fudž
<g/>
)	)	kIx)	)
s	s	k7c7	s
3776	[number]	k4	3776
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
(	(	kIx(	(
<g/>
krátkých	krátký	k2eAgFnPc2d1	krátká
<g/>
)	)	kIx)	)
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
japonské	japonský	k2eAgFnSc2d1	japonská
nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
řeky	řeka	k1gFnSc2	řeka
Šinano	Šinana	k1gFnSc5	Šinana
(	(	kIx(	(
<g/>
367	[number]	k4	367
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
98	[number]	k4	98
352	[number]	k4	352
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
89	[number]	k4	89
101	[number]	k4	101
702	[number]	k4	702
<g/>
)	)	kIx)	)
soustředěných	soustředěný	k2eAgFnPc2d1	soustředěná
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nP	patřit
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
,	,	kIx,	,
Kjóto	Kjóto	k1gNnSc1	Kjóto
<g/>
,	,	kIx,	,
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Kóbe	Kóbe	k1gNnSc1	Kóbe
<g/>
,	,	kIx,	,
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
,	,	kIx,	,
Sendai	Sendai	k1gNnSc1	Sendai
a	a	k8xC	a
Nagoja	Nagoja	k1gFnSc1	Nagoja
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
34	[number]	k4	34
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
metropole	metropol	k1gFnSc2	metropol
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
Čúgoku	Čúgok	k1gInSc2	Čúgok
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kansai	Kansae	k1gFnSc4	Kansae
(	(	kIx(	(
<g/>
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Čúgoku	Čúgok	k1gInSc2	Čúgok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čúbu	Čúba	k1gFnSc4	Čúba
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc4d1	centrální
oblast	oblast	k1gFnSc4	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kantó	Kantó	k1gMnSc1	Kantó
(	(	kIx(	(
<g/>
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tóhoku	Tóhok	k1gInSc2	Tóhok
(	(	kIx(	(
<g/>
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
Honšú	Honšú	k1gFnSc2	Honšú
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
horské	horský	k2eAgNnSc1d1	horské
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
značného	značný	k2eAgInSc2d1	značný
rozdílu	rozdíl	k1gInSc2	rozdíl
v	v	k7c6	v
podnebí	podnebí	k1gNnSc6	podnebí
mezi	mezi	k7c4	mezi
východní	východní	k2eAgFnSc4d1	východní
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc7d1	jižní
<g/>
,	,	kIx,	,
pacifickou	pacifický	k2eAgFnSc7d1	Pacifická
<g/>
)	)	kIx)	)
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Japonského	japonský	k2eAgNnSc2d1	Japonské
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Prefektury	prefektura	k1gFnPc1	prefektura
na	na	k7c6	na
Honšú	Honšú	k1gFnSc6	Honšú
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čúgoku	Čúgok	k1gInSc2	Čúgok
–	–	k?	–
Hirošima-ken	Hirošimaen	k1gInSc1	Hirošima-ken
<g/>
,	,	kIx,	,
Okajama-ken	Okajamaen	k1gInSc1	Okajama-ken
<g/>
,	,	kIx,	,
Šimane-ken	Šimaneen	k1gInSc1	Šimane-ken
<g/>
,	,	kIx,	,
Tottori-ken	Tottorien	k1gInSc1	Tottori-ken
<g/>
,	,	kIx,	,
Jamaguči-ken	Jamagučien	k1gInSc1	Jamaguči-ken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kansai	Kansai	k6eAd1	Kansai
–	–	k?	–
Hjógo-ken	Hjógona	k1gFnPc2	Hjógo-kna
<g/>
,	,	kIx,	,
Kjóto-fu	Kjóto	k1gInSc2	Kjóto-f
<g/>
,	,	kIx,	,
Mie-ken	Miena	k1gFnPc2	Mie-kna
<g/>
,	,	kIx,	,
Nara-ken	Narana	k1gFnPc2	Nara-kna
<g/>
,	,	kIx,	,
Ósaka-fu	Ósaka	k1gInSc2	Ósaka-f
<g/>
,	,	kIx,	,
Šiga-ken	Šigaen	k1gInSc1	Šiga-ken
<g/>
,	,	kIx,	,
Wakajama-ken	Wakajamaen	k1gInSc1	Wakajama-ken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čúbu	Čúbu	k6eAd1	Čúbu
–	–	k?	–
Aiči-ken	Aičien	k1gInSc1	Aiči-ken
<g/>
,	,	kIx,	,
Fukui-ken	Fukuien	k1gInSc1	Fukui-ken
<g/>
,	,	kIx,	,
Gifu-ken	Gifuen	k1gInSc1	Gifu-ken
<g/>
,	,	kIx,	,
Išikawa-ken	Išikawaen	k1gInSc1	Išikawa-ken
<g/>
,	,	kIx,	,
Nagano-ken	Naganoen	k1gInSc1	Nagano-ken
<g/>
,	,	kIx,	,
Niigata-ken	Niigataen	k1gInSc1	Niigata-ken
<g/>
,	,	kIx,	,
Tojama-ken	Tojamaen	k1gInSc1	Tojama-ken
<g/>
,	,	kIx,	,
Šizuoka-ken	Šizuokaen	k1gInSc1	Šizuoka-ken
<g/>
,	,	kIx,	,
Jamanaši-ken	Jamanašien	k1gInSc1	Jamanaši-ken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kantó	Kantó	k?	Kantó
–	–	k?	–
Čiba-ken	Čibaen	k1gInSc1	Čiba-ken
<g/>
,	,	kIx,	,
Gunma-ken	Gunmaen	k1gInSc1	Gunma-ken
<g/>
,	,	kIx,	,
Ibaraki-ken	Ibarakien	k1gInSc1	Ibaraki-ken
<g/>
,	,	kIx,	,
Kanagawa-ken	Kanagawaen	k1gInSc1	Kanagawa-ken
<g/>
,	,	kIx,	,
Saitama-ken	Saitamaen	k1gInSc1	Saitama-ken
<g/>
,	,	kIx,	,
Točigi-ken	Točigien	k1gInSc1	Točigi-ken
<g/>
,	,	kIx,	,
Tókjó-to	Tókjóo	k1gNnSc1	Tókjó-to
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tóhoku	Tóhok	k1gInSc2	Tóhok
–	–	k?	–
Akita-ken	Akitaen	k1gInSc1	Akita-ken
<g/>
,	,	kIx,	,
Aomori-ken	Aomorien	k1gInSc1	Aomori-ken
<g/>
,	,	kIx,	,
Fukušima-ken	Fukušimaen	k1gInSc1	Fukušima-ken
<g/>
,	,	kIx,	,
Iwate-ken	Iwateen	k1gInSc1	Iwate-ken
<g/>
,	,	kIx,	,
Mijagi-ken	Mijagien	k1gInSc1	Mijagi-ken
<g/>
,	,	kIx,	,
Jamagata-ken	Jamagataen	k1gInSc1	Jamagata-ken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Honshū	Honshū	k1gFnSc2	Honshū
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Honšú	Honšú	k1gFnSc2	Honšú
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
