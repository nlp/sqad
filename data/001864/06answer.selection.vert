<s>
První	první	k4xOgFnSc4	první
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jean	Jean	k1gMnSc1	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Lamarck	Lamarcko	k1gNnPc2	Lamarcko
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
lamarckismus	lamarckismus	k1gInSc1	lamarckismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnešní	dnešní	k2eAgMnPc1d1	dnešní
vědci	vědec	k1gMnPc1	vědec
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
předložil	předložit	k5eAaPmAgMnS	předložit
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
darwinismus	darwinismus	k1gInSc1	darwinismus
<g/>
,	,	kIx,	,
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spojil	spojit	k5eAaPmAgMnS	spojit
myšlenku	myšlenka	k1gFnSc4	myšlenka
postupné	postupný	k2eAgFnSc2d1	postupná
evoluce	evoluce	k1gFnSc2	evoluce
druhu	druh	k1gInSc2	druh
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
příčinou	příčina	k1gFnSc7	příčina
a	a	k8xC	a
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
