<s>
Evoluce	evoluce	k1gFnSc1	evoluce
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
biologická	biologický	k2eAgFnSc1d1	biologická
evoluce	evoluce	k1gFnSc1	evoluce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnPc4	teorie
popisující	popisující	k2eAgInSc4d1	popisující
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
a	a	k8xC	a
samovolný	samovolný	k2eAgInSc4d1	samovolný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
diverzifikuje	diverzifikovat	k5eAaImIp3nS	diverzifikovat
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podstatu	podstata	k1gFnSc4	podstata
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jean	Jean	k1gMnSc1	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Lamarck	Lamarcko	k1gNnPc2	Lamarcko
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
lamarckismus	lamarckismus	k1gInSc1	lamarckismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnešní	dnešní	k2eAgMnPc1d1	dnešní
vědci	vědec	k1gMnPc1	vědec
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
předložil	předložit	k5eAaPmAgMnS	předložit
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
darwinismus	darwinismus	k1gInSc1	darwinismus
<g/>
,	,	kIx,	,
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spojil	spojit	k5eAaPmAgMnS	spojit
myšlenku	myšlenka	k1gFnSc4	myšlenka
postupné	postupný	k2eAgFnSc2d1	postupná
evoluce	evoluce	k1gFnSc2	evoluce
druhu	druh	k1gInSc2	druh
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
příčinou	příčina	k1gFnSc7	příčina
a	a	k8xC	a
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
evoluce	evoluce	k1gFnSc2	evoluce
biologické	biologický	k2eAgFnSc2d1	biologická
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
evoluce	evoluce	k1gFnSc1	evoluce
kulturní	kulturní	k2eAgFnSc1d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
Evolucí	evoluce	k1gFnSc7	evoluce
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
chápán	chápat	k5eAaImNgInS	chápat
postupný	postupný	k2eAgInSc1d1	postupný
vývoj	vývoj	k1gInSc1	vývoj
života	život	k1gInSc2	život
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
výskytu	výskyt	k1gInSc2	výskyt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
různým	různý	k2eAgFnPc3d1	různá
formám	forma	k1gFnPc3	forma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
paleontologie	paleontologie	k1gFnSc1	paleontologie
a	a	k8xC	a
moderní	moderní	k2eAgFnPc1d1	moderní
biologické	biologický	k2eAgFnPc1d1	biologická
a	a	k8xC	a
biochemické	biochemický	k2eAgFnPc1d1	biochemická
vědy	věda	k1gFnPc1	věda
popisují	popisovat	k5eAaImIp3nP	popisovat
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
s	s	k7c7	s
narůstající	narůstající	k2eAgFnSc7d1	narůstající
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Společné	společný	k2eAgInPc1d1	společný
rysy	rys	k1gInPc1	rys
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
všech	všecek	k3xTgInPc2	všecek
žijících	žijící	k2eAgInPc2d1	žijící
organismů	organismus	k1gInPc2	organismus
včetně	včetně	k7c2	včetně
lidí	člověk	k1gMnPc2	člověk
tak	tak	k6eAd1	tak
dokladují	dokladovat	k5eAaImIp3nP	dokladovat
jejich	jejich	k3xOp3gInSc4	jejich
společný	společný	k2eAgInSc4d1	společný
prvotní	prvotní	k2eAgInSc4d1	prvotní
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
společné	společný	k2eAgFnPc4d1	společná
sady	sada	k1gFnPc4	sada
genů	gen	k1gInPc2	gen
(	(	kIx(	(
<g/>
postupné	postupný	k2eAgFnPc1d1	postupná
náhodné	náhodný	k2eAgFnPc1d1	náhodná
mutace	mutace	k1gFnPc1	mutace
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
selektovány	selektovat	k5eAaBmNgFnP	selektovat
deterministicky	deterministicky	k6eAd1	deterministicky
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
podobnost	podobnost	k1gFnSc4	podobnost
organismů	organismus	k1gInPc2	organismus
tedy	tedy	k9	tedy
odráží	odrážet	k5eAaImIp3nS	odrážet
podobnost	podobnost	k1gFnSc4	podobnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
organismy	organismus	k1gInPc1	organismus
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
evoluce	evoluce	k1gFnSc2	evoluce
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
například	například	k6eAd1	například
rychlá	rychlý	k2eAgFnSc1d1	rychlá
adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
změna	změna	k1gFnSc1	změna
zbarvení	zbarvení	k1gNnSc4	zbarvení
drsnokřídlovce	drsnokřídlovec	k1gMnSc2	drsnokřídlovec
březového	březový	k2eAgMnSc2d1	březový
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
dobře	dobře	k6eAd1	dobře
zmapovaná	zmapovaný	k2eAgFnSc1d1	zmapovaná
evoluce	evoluce	k1gFnSc1	evoluce
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc4d1	znám
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
"	"	kIx"	"
<g/>
články	článek	k1gInPc4	článek
<g/>
"	"	kIx"	"
evoluce	evoluce	k1gFnSc1	evoluce
včetně	včetně	k7c2	včetně
postupných	postupný	k2eAgFnPc2d1	postupná
změn	změna	k1gFnPc2	změna
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
evoluce	evoluce	k1gFnSc2	evoluce
pochází	pocházet	k5eAaImIp3nS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
evolutio	evolutio	k6eAd1	evolutio
(	(	kIx(	(
<g/>
rozvinutí	rozvinutí	k1gNnSc1	rozvinutí
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostalo	dostat	k5eAaPmAgNnS	dostat
skrze	skrze	k?	skrze
anglickou	anglický	k2eAgFnSc4d1	anglická
teorii	teorie	k1gFnSc4	teorie
evolution	evolution	k1gInSc1	evolution
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
-	-	kIx~	-
např.	např.	kA	např.
evoluce	evoluce	k1gFnSc1	evoluce
trhu	trh	k1gInSc2	trh
nebo	nebo	k8xC	nebo
evoluce	evoluce	k1gFnSc2	evoluce
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
spíše	spíše	k9	spíše
o	o	k7c4	o
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
chybí	chybět	k5eAaImIp3nS	chybět
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
o	o	k7c4	o
třídění	třídění	k1gNnSc4	třídění
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
mechanismů	mechanismus	k1gInPc2	mechanismus
a	a	k8xC	a
zákonitostí	zákonitost	k1gFnPc2	zákonitost
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
evolučními	evoluční	k2eAgInPc7d1	evoluční
vztahy	vztah	k1gInPc7	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
organismy	organismus	k1gInPc7	organismus
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
fylogenetika	fylogenetika	k1gFnSc1	fylogenetika
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
definoval	definovat	k5eAaBmAgMnS	definovat
v	v	k7c6	v
O	O	kA	O
původu	původ	k1gInSc2	původ
druhů	druh	k1gInPc2	druh
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
tyto	tento	k3xDgFnPc4	tento
čtyři	čtyři	k4xCgFnPc4	čtyři
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
Organismus	organismus	k1gInSc1	organismus
zplodí	zplodit	k5eAaPmIp3nS	zplodit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
za	za	k7c4	za
život	život	k1gInSc4	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgMnSc2	jeden
plodného	plodný	k2eAgMnSc2d1	plodný
potomka	potomek	k1gMnSc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
určitý	určitý	k2eAgInSc4d1	určitý
selekční	selekční	k2eAgInSc4d1	selekční
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
organismy	organismus	k1gInPc1	organismus
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
vnitrodruhová	vnitrodruhový	k2eAgFnSc1d1	vnitrodruhová
variabilita	variabilita	k1gFnSc1	variabilita
<g/>
,	,	kIx,	,
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
příslušníci	příslušník	k1gMnPc1	příslušník
téhož	týž	k3xTgInSc2	týž
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
heredita	heredita	k1gFnSc1	heredita
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
podobají	podobat	k5eAaImIp3nP	podobat
více	hodně	k6eAd2	hodně
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
než	než	k8xS	než
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
většinou	většina	k1gFnSc7	většina
cílí	cílit	k5eAaImIp3nS	cílit
právě	právě	k9	právě
na	na	k7c4	na
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
empirické	empirický	k2eAgNnSc4d1	empirické
ověření	ověření	k1gNnSc4	ověření
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
<g/>
,	,	kIx,	,
než	než	k8xS	než
pouze	pouze	k6eAd1	pouze
přirozený	přirozený	k2eAgInSc4d1	přirozený
výběr	výběr	k1gInSc4	výběr
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokroky	pokrok	k1gInPc1	pokrok
v	v	k7c6	v
paleontologii	paleontologie	k1gFnSc6	paleontologie
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
řadu	řada	k1gFnSc4	řada
důkazů	důkaz	k1gInPc2	důkaz
hromadících	hromadící	k2eAgInPc2d1	hromadící
se	s	k7c7	s
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
organizmech	organizmus	k1gInPc6	organizmus
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
delšího	dlouhý	k2eAgNnSc2d2	delší
časového	časový	k2eAgNnSc2d1	časové
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
hromadící	hromadící	k2eAgFnSc1d1	hromadící
se	se	k3xPyFc4	se
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
fyzických	fyzický	k2eAgInPc2d1	fyzický
znaků	znak	k1gInPc2	znak
i	i	k8xC	i
životních	životní	k2eAgFnPc2d1	životní
strategií	strategie	k1gFnPc2	strategie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
sledování	sledování	k1gNnSc2	sledování
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
strukturu	struktura	k1gFnSc4	struktura
"	"	kIx"	"
<g/>
stromu	strom	k1gInSc2	strom
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
a	a	k8xC	a
mapovat	mapovat	k5eAaImF	mapovat
příbuznost	příbuznost	k1gFnSc4	příbuznost
všech	všecek	k3xTgFnPc2	všecek
životních	životní	k2eAgFnPc2d1	životní
forem	forma	k1gFnPc2	forma
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
paleontologie	paleontologie	k1gFnSc1	paleontologie
začala	začít	k5eAaPmAgFnS	začít
prací	práce	k1gFnSc7	práce
George	Georg	k1gMnSc2	Georg
Cuviera	Cuvier	k1gMnSc2	Cuvier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
vrstvě	vrstva	k1gFnSc6	vrstva
sedimentů	sediment	k1gInPc2	sediment
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
specifickou	specifický	k2eAgFnSc4d1	specifická
skupinu	skupina	k1gFnSc4	skupina
fosílií	fosílie	k1gFnPc2	fosílie
(	(	kIx(	(
<g/>
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyQgNnSc7	čí
jsou	být	k5eAaImIp3nP	být
vrstvy	vrstva	k1gFnPc1	vrstva
hlubší	hluboký	k2eAgFnPc1d2	hlubší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
nalezené	nalezený	k2eAgInPc1d1	nalezený
organismy	organismus	k1gInPc1	organismus
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Cuvier	Cuvier	k1gMnSc1	Cuvier
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
starší	starý	k2eAgFnPc4d2	starší
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
že	že	k8xS	že
tudíž	tudíž	k8xC	tudíž
probíhá	probíhat	k5eAaImIp3nS	probíhat
postupný	postupný	k2eAgInSc1d1	postupný
vývoj	vývoj	k1gInSc1	vývoj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
komplexitě	komplexita	k1gFnSc3	komplexita
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
jeho	jeho	k3xOp3gFnPc1	jeho
hypotézy	hypotéza	k1gFnPc1	hypotéza
jako	jako	k8xC	jako
katastrofismus	katastrofismus	k1gInSc1	katastrofismus
(	(	kIx(	(
<g/>
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
náhlé	náhlý	k2eAgFnPc1d1	náhlá
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
geologických	geologický	k2eAgFnPc6d1	geologická
vrstvách	vrstva	k1gFnPc6	vrstva
jsou	být	k5eAaImIp3nP	být
následkem	následkem	k7c2	následkem
katastrof	katastrofa	k1gFnPc2	katastrofa
předchozích	předchozí	k2eAgNnPc2d1	předchozí
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
nacházíme	nacházet	k5eAaImIp1nP	nacházet
i	i	k9	i
nyní	nyní	k6eAd1	nyní
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
vědci	vědec	k1gMnPc7	vědec
odmítány	odmítán	k2eAgInPc1d1	odmítán
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
následně	následně	k6eAd1	následně
uniformitarismus	uniformitarismus	k1gInSc1	uniformitarismus
prosadil	prosadit	k5eAaPmAgInS	prosadit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
probíhaly	probíhat	k5eAaImAgInP	probíhat
geologické	geologický	k2eAgInPc1d1	geologický
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
stejně	stejně	k6eAd1	stejně
rychle	rychle	k6eAd1	rychle
jako	jako	k8xS	jako
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
složení	složení	k1gNnSc2	složení
fosílií	fosílie	k1gFnPc2	fosílie
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
vrstvách	vrstva	k1gFnPc6	vrstva
můžeme	moct	k5eAaImIp1nP	moct
dnes	dnes	k6eAd1	dnes
určit	určit	k5eAaPmF	určit
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
jak	jak	k6eAd1	jak
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
které	který	k3yQgInPc1	který
organismy	organismus	k1gInPc1	organismus
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
nástrojem	nástroj	k1gInSc7	nástroj
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
různé	různý	k2eAgFnPc1d1	různá
radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
datovací	datovací	k2eAgFnPc1d1	datovací
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
uhlíková	uhlíkový	k2eAgFnSc1d1	uhlíková
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
15	[number]	k4	15
dalších	další	k1gNnPc2	další
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
využívají	využívat	k5eAaImIp3nP	využívat
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
rozpad	rozpad	k1gInSc4	rozpad
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
starší	starý	k2eAgNnSc4d2	starší
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
metoda	metoda	k1gFnSc1	metoda
draslík-argon	draslíkrgon	k1gInSc1	draslík-argon
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
40	[number]	k4	40
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
1,3	[number]	k4	1,3
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
důkazu	důkaz	k1gInSc6	důkaz
evoluční	evoluční	k2eAgFnSc2d1	evoluční
myšlenky	myšlenka	k1gFnSc2	myšlenka
představují	představovat	k5eAaImIp3nP	představovat
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
přechodné	přechodný	k2eAgFnPc4d1	přechodná
fosílie	fosílie	k1gFnPc4	fosílie
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Archaeopteryx	Archaeopteryx	k1gInSc4	Archaeopteryx
(	(	kIx(	(
<g/>
jako	jako	k9	jako
pojítko	pojítko	k1gNnSc1	pojítko
organismů	organismus	k1gInPc2	organismus
s	s	k7c7	s
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
bez	bez	k7c2	bez
nich	on	k3xPp3gInPc2	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
anatomie	anatomie	k1gFnSc1	anatomie
využívá	využívat	k5eAaPmIp3nS	využívat
poznatků	poznatek	k1gInPc2	poznatek
morfologie	morfologie	k1gFnSc1	morfologie
jako	jako	k8xC	jako
způsob	způsob	k1gInSc1	způsob
růstu	růst	k1gInSc2	růst
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
sdílené	sdílený	k2eAgInPc4d1	sdílený
znaky	znak	k1gInPc4	znak
ale	ale	k8xC	ale
i	i	k9	i
etologické	etologický	k2eAgInPc1d1	etologický
poznatky	poznatek	k1gInPc1	poznatek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mapovala	mapovat	k5eAaImAgFnS	mapovat
společné	společný	k2eAgInPc4d1	společný
předky	předek	k1gInPc4	předek
geneticky	geneticky	k6eAd1	geneticky
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc1	důkaz
evoluce	evoluce	k1gFnSc2	evoluce
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
porovnávání	porovnávání	k1gNnSc1	porovnávání
naopak	naopak	k6eAd1	naopak
podobných	podobný	k2eAgFnPc2d1	podobná
nezávisle	závisle	k6eNd1	závisle
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
evolučních	evoluční	k2eAgFnPc2d1	evoluční
strategií	strategie	k1gFnPc2	strategie
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zjištění	zjištění	k1gNnSc2	zjištění
původního	původní	k2eAgInSc2d1	původní
selekčního	selekční	k2eAgInSc2d1	selekční
tlaku	tlak	k1gInSc2	tlak
-	-	kIx~	-
konvergence	konvergence	k1gFnSc1	konvergence
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
klasifikaci	klasifikace	k1gFnSc4	klasifikace
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
metod	metoda	k1gFnPc2	metoda
zjištění	zjištění	k1gNnSc2	zjištění
genetické	genetický	k2eAgFnSc2d1	genetická
přibuznosti	přibuznost	k1gFnSc2	přibuznost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
znalostmi	znalost	k1gFnPc7	znalost
taxonomie	taxonomie	k1gFnPc4	taxonomie
můžeme	moct	k5eAaImIp1nP	moct
například	například	k6eAd1	například
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
probíhala	probíhat	k5eAaImAgFnS	probíhat
speciace	speciace	k1gFnSc1	speciace
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
množství	množství	k1gNnSc1	množství
změn	změna	k1gFnPc2	změna
i	i	k8xC	i
podobností	podobnost	k1gFnPc2	podobnost
napříč	napříč	k7c7	napříč
organizmy	organizmus	k1gInPc7	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
připomínají	připomínat	k5eAaImIp3nP	připomínat
struktury	struktura	k1gFnPc1	struktura
embryí	embryo	k1gNnPc2	embryo
některé	některý	k3yIgInPc1	některý
evolučně	evolučně	k6eAd1	evolučně
předcházející	předcházející	k2eAgInPc1d1	předcházející
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Embrya	embryo	k1gNnPc1	embryo
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
i	i	k9	i
napříč	napříč	k7c7	napříč
druhy	druh	k1gInPc7	druh
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
brzká	brzký	k2eAgNnPc4d1	brzké
stádia	stádium	k1gNnPc4	stádium
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
)	)	kIx)	)
a	a	k8xC	a
i	i	k9	i
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
fázích	fáze	k1gFnPc6	fáze
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
rudimenty	rudiment	k1gInPc4	rudiment
jako	jako	k8xS	jako
faryngální	faryngální	k2eAgInSc4d1	faryngální
(	(	kIx(	(
<g/>
žaberní	žaberní	k2eAgInSc4d1	žaberní
<g/>
)	)	kIx)	)
aparát	aparát	k1gInSc4	aparát
nebo	nebo	k8xC	nebo
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
informační	informační	k2eAgFnSc4d1	informační
náročnost	náročnost	k1gFnSc4	náročnost
(	(	kIx(	(
<g/>
genovou	genový	k2eAgFnSc4d1	genová
<g/>
)	)	kIx)	)
stále	stále	k6eAd1	stále
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
přežívají	přežívat	k5eAaImIp3nP	přežívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
působící	působící	k2eAgInPc1d1	působící
selekční	selekční	k2eAgInPc1d1	selekční
tlaky	tlak	k1gInPc1	tlak
nejsou	být	k5eNaImIp3nP	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
selektovala	selektovat	k5eAaBmAgFnS	selektovat
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
forma	forma	k1gFnSc1	forma
exprese	exprese	k1gFnSc2	exprese
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
brzký	brzký	k2eAgInSc4d1	brzký
vývoj	vývoj	k1gInSc4	vývoj
embrya	embryo	k1gNnSc2	embryo
<g/>
..	..	k?	..
Biografie	biografie	k1gFnSc1	biografie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
geografické	geografický	k2eAgNnSc4d1	geografické
rozložení	rozložení	k1gNnSc4	rozložení
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
z	z	k7c2	z
biogeografie	biogeografie	k1gFnSc2	biogeografie
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
oceánské	oceánský	k2eAgFnSc2d1	oceánská
biogeografie	biogeografie	k1gFnSc2	biogeografie
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
Darwina	Darwin	k1gMnSc4	Darwin
i	i	k8xC	i
Wallace	Wallace	k1gFnPc4	Wallace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechen	všechen	k3xTgInSc4	všechen
život	život	k1gInSc4	život
sdílí	sdílet	k5eAaImIp3nP	sdílet
jednoho	jeden	k4xCgMnSc4	jeden
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
často	často	k6eAd1	často
obývají	obývat	k5eAaImIp3nP	obývat
endemiti	endemit	k5eAaPmF	endemit
(	(	kIx(	(
<g/>
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nežijí	žít	k5eNaImIp3nP	žít
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
například	například	k6eAd1	například
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
vzdálená	vzdálený	k2eAgNnPc4d1	vzdálené
místa	místo	k1gNnPc4	místo
dostali	dostat	k5eAaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
často	často	k6eAd1	často
nepřežijí	přežít	k5eNaPmIp3nP	přežít
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
biomu	biom	k1gInSc6	biom
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
podobají	podobat	k5eAaImIp3nP	podobat
geograficky	geograficky	k6eAd1	geograficky
nejbližším	blízký	k2eAgInPc3d3	Nejbližší
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
živý	živý	k2eAgInSc1d1	živý
organizmus	organizmus	k1gInSc1	organizmus
(	(	kIx(	(
<g/>
s	s	k7c7	s
možnou	možný	k2eAgFnSc7d1	možná
výjimkou	výjimka	k1gFnSc7	výjimka
RNA	RNA	kA	RNA
virů	vir	k1gInPc2	vir
<g/>
)	)	kIx)	)
přenáší	přenášet	k5eAaImIp3nS	přenášet
molekuly	molekula	k1gFnPc4	molekula
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejné	stejný	k2eAgInPc1d1	stejný
geny	gen	k1gInPc1	gen
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
podobné	podobný	k2eAgInPc1d1	podobný
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
znaky	znak	k1gInPc4	znak
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
analýze	analýza	k1gFnSc3	analýza
a	a	k8xC	a
porovnávání	porovnávání	k1gNnSc4	porovnávání
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
genofondů	genofond	k1gInPc2	genofond
organizmů	organizmus	k1gInPc2	organizmus
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
běžně	běžně	k6eAd1	běžně
také	také	k9	také
zjistit	zjistit	k5eAaPmF	zjistit
mnoho	mnoho	k4c4	mnoho
genetických	genetický	k2eAgFnPc2d1	genetická
predispozic	predispozice	k1gFnPc2	predispozice
<g/>
,	,	kIx,	,
mapovat	mapovat	k5eAaImF	mapovat
geny	gen	k1gInPc1	gen
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
různé	různý	k2eAgFnPc4d1	různá
nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
určit	určit	k5eAaPmF	určit
rodičovství	rodičovství	k1gNnSc4	rodičovství
nebo	nebo	k8xC	nebo
charakteristiky	charakteristika	k1gFnPc4	charakteristika
pachatele	pachatel	k1gMnSc2	pachatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zanechal	zanechat	k5eAaPmAgMnS	zanechat
nějakou	nějaký	k3yIgFnSc4	nějaký
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
protein	protein	k1gInSc4	protein
pro	pro	k7c4	pro
fluorescentní	fluorescentní	k2eAgFnPc4d1	fluorescentní
myši	myš	k1gFnPc4	myš
nebo	nebo	k8xC	nebo
rostliny	rostlina	k1gFnPc4	rostlina
odolávající	odolávající	k2eAgFnSc2d1	odolávající
suchu	sucho	k1gNnSc3	sucho
i	i	k8xC	i
parazitům	parazit	k1gMnPc3	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtění	šlechtění	k1gNnSc1	šlechtění
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
charakteristiky	charakteristika	k1gFnPc4	charakteristika
chovných	chovný	k2eAgNnPc2d1	chovné
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vědomě	vědomě	k6eAd1	vědomě
i	i	k9	i
nevědomě	vědomě	k6eNd1	vědomě
udržovali	udržovat	k5eAaImAgMnP	udržovat
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
plodné	plodný	k2eAgMnPc4d1	plodný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Umělý	umělý	k2eAgInSc1d1	umělý
výběr	výběr	k1gInSc1	výběr
umožnil	umožnit	k5eAaPmAgInS	umožnit
i	i	k9	i
tak	tak	k6eAd1	tak
kuriózní	kuriózní	k2eAgInPc1d1	kuriózní
znaky	znak	k1gInPc1	znak
jako	jako	k8xC	jako
plnokvěté	plnokvětý	k2eAgFnPc1d1	plnokvětá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
pomeranče	pomeranč	k1gInPc1	pomeranč
bez	bez	k7c2	bez
jader	jádro	k1gNnPc2	jádro
nebo	nebo	k8xC	nebo
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
psí	psí	k2eAgNnPc4d1	psí
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
teorií	teorie	k1gFnPc2	teorie
z	z	k7c2	z
Darwinova	Darwinův	k2eAgNnSc2d1	Darwinovo
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
dlouholetého	dlouholetý	k2eAgNnSc2d1	dlouholeté
studování	studování	k1gNnSc2	studování
umělého	umělý	k2eAgInSc2d1	umělý
výběru	výběr	k1gInSc2	výběr
na	na	k7c6	na
holubech	holub	k1gMnPc6	holub
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
<g/>
:	:	kIx,	:
kladogeneze	kladogeneze	k1gFnSc1	kladogeneze
-	-	kIx~	-
postupné	postupný	k2eAgNnSc1d1	postupné
odštěpování	odštěpování	k1gNnSc1	odštěpování
evolučních	evoluční	k2eAgFnPc2d1	evoluční
linií	linie	k1gFnPc2	linie
anageneze	anageneze	k1gFnSc2	anageneze
-	-	kIx~	-
postupné	postupný	k2eAgFnPc1d1	postupná
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
znacích	znak	k1gInPc6	znak
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
evolučních	evoluční	k2eAgFnPc2d1	evoluční
linií	linie	k1gFnPc2	linie
Podle	podle	k7c2	podle
měřítka	měřítko	k1gNnSc2	měřítko
evolučních	evoluční	k2eAgFnPc2d1	evoluční
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
:	:	kIx,	:
mikroevoluce	mikroevoluce	k1gFnPc1	mikroevoluce
-	-	kIx~	-
menší	malý	k2eAgFnPc1d2	menší
evoluční	evoluční	k2eAgFnPc1d1	evoluční
změny	změna	k1gFnPc1	změna
během	během	k7c2	během
kratšího	krátký	k2eAgNnSc2d2	kratší
časového	časový	k2eAgNnSc2d1	časové
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
změny	změna	k1gFnPc1	změna
frekvencí	frekvence	k1gFnPc2	frekvence
alel	alela	k1gFnPc2	alela
v	v	k7c6	v
populacích	populace	k1gFnPc6	populace
během	během	k7c2	během
několika	několik	k4yIc2	několik
generací	generace	k1gFnPc2	generace
makroevoluce	makroevoluce	k1gFnSc2	makroevoluce
-	-	kIx~	-
větší	veliký	k2eAgInSc4d2	veliký
<g />
.	.	kIx.	.
</s>
<s>
evoluční	evoluční	k2eAgFnPc1d1	evoluční
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
geologického	geologický	k2eAgInSc2d1	geologický
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
i	i	k8xC	i
vyšších	vysoký	k2eAgFnPc2d2	vyšší
taxonomických	taxonomický	k2eAgFnPc2d1	Taxonomická
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
řád	řád	k1gInSc4	řád
či	či	k8xC	či
kmen	kmen	k1gInSc4	kmen
Dále	daleko	k6eAd2	daleko
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
mezidruhovou	mezidruhový	k2eAgFnSc4d1	mezidruhová
kompetici	kompetik	k1gMnPc1	kompetik
-	-	kIx~	-
evoluci	evoluce	k1gFnSc3	evoluce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
určité	určitý	k2eAgFnPc1d1	určitá
mutace	mutace	k1gFnPc1	mutace
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
druhu	druh	k1gInSc2	druh
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
(	(	kIx(	(
<g/>
selekce	selekce	k1gFnSc1	selekce
rychlonohých	rychlonohý	k2eAgMnPc2d1	rychlonohý
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
)	)	kIx)	)
vnitrodruhovou	vnitrodruhový	k2eAgFnSc7d1	vnitrodruhová
<g />
.	.	kIx.	.
</s>
<s>
kompetici	kompetik	k1gMnPc1	kompetik
-	-	kIx~	-
evoluci	evoluce	k1gFnSc3	evoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
výhodná	výhodný	k2eAgFnSc1d1	výhodná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
fixace	fixace	k1gFnSc2	fixace
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
(	(	kIx(	(
<g/>
selekce	selekce	k1gFnSc1	selekce
vysokých	vysoký	k2eAgInPc2d1	vysoký
stromů	strom	k1gInPc2	strom
<g/>
)	)	kIx)	)
Mezi	mezi	k7c4	mezi
příčiny	příčina	k1gFnPc4	příčina
evolučního	evoluční	k2eAgInSc2d1	evoluční
vývoje	vývoj	k1gInSc2	vývoj
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
popsal	popsat	k5eAaPmAgMnS	popsat
již	již	k6eAd1	již
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
přirozený	přirozený	k2eAgInSc1d1	přirozený
výběr	výběr	k1gInSc1	výběr
skupinový	skupinový	k2eAgInSc1d1	skupinový
výběr	výběr	k1gInSc4	výběr
rodičovský	rodičovský	k2eAgInSc4d1	rodičovský
výběr	výběr	k1gInSc4	výběr
příbuzenecký	příbuzenecký	k2eAgInSc1d1	příbuzenecký
výběr	výběr	k1gInSc1	výběr
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
výběr	výběr	k1gInSc4	výběr
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
izolace	izolace	k1gFnSc2	izolace
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
principy	princip	k1gInPc1	princip
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
genové	genový	k2eAgFnPc4d1	genová
mutace	mutace	k1gFnPc4	mutace
genetický	genetický	k2eAgInSc4d1	genetický
drift	drift	k1gInSc4	drift
genetický	genetický	k2eAgInSc4d1	genetický
draft	draft	k1gInSc4	draft
sobecký	sobecký	k2eAgInSc1d1	sobecký
gen	gen	k1gInSc1	gen
epigenetika	epigenetik	k1gMnSc2	epigenetik
evo-devo	evoevo	k6eAd1	evo-devo
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
teorií	teorie	k1gFnPc2	teorie
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
života	život	k1gInSc2	život
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
evolucí	evoluce	k1gFnSc7	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
první	první	k4xOgFnPc1	první
myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc7	první
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
evoluční	evoluční	k2eAgFnSc7d1	evoluční
teorií	teorie	k1gFnSc7	teorie
byl	být	k5eAaImAgInS	být
lamarckismus	lamarckismus	k1gInSc1	lamarckismus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
<g/>
"	"	kIx"	"
rozumí	rozumět	k5eAaImIp3nS	rozumět
koncepce	koncepce	k1gFnSc1	koncepce
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
Darwina	Darwin	k1gMnSc4	Darwin
a	a	k8xC	a
Wallaceho	Wallace	k1gMnSc4	Wallace
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
hromadění	hromadění	k1gNnSc1	hromadění
nahodilých	nahodilý	k2eAgFnPc2d1	nahodilá
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
přirozený	přirozený	k2eAgInSc4d1	přirozený
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
formulace	formulace	k1gFnSc1	formulace
evolucionistických	evolucionistický	k2eAgInPc2d1	evolucionistický
názorů	názor	k1gInPc2	názor
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
již	již	k9	již
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
myšlenek	myšlenka	k1gFnPc2	myšlenka
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Thaléta	Thaléta	k1gMnSc1	Thaléta
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
v	v	k7c4	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
st.	st.	kA	st.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
První	první	k4xOgMnPc1	první
živočichové	živočich	k1gMnPc1	živočich
se	se	k3xPyFc4	se
zrodili	zrodit	k5eAaPmAgMnP	zrodit
ve	v	k7c6	v
vlhku	vlhko	k1gNnSc6	vlhko
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
ostnatou	ostnatý	k2eAgFnSc4d1	ostnatá
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
když	když	k8xS	když
dorůstali	dorůstat	k5eAaImAgMnP	dorůstat
<g/>
,	,	kIx,	,
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
na	na	k7c4	na
souš	souš	k1gFnSc4	souš
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
kůra	kůra	k1gFnSc1	kůra
zlomila	zlomit	k5eAaPmAgFnS	zlomit
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
ještě	ještě	k6eAd1	ještě
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
st.	st.	kA	st.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Empedoklés	Empedoklés	k1gInSc4	Empedoklés
z	z	k7c2	z
Akragantu	Akragant	k1gInSc2	Akragant
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
prvotního	prvotní	k2eAgInSc2d1	prvotní
chaosu	chaos	k1gInSc2	chaos
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
i	i	k8xC	i
živočišných	živočišný	k2eAgNnPc2d1	živočišné
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
různě	různě	k6eAd1	různě
spojovaly	spojovat	k5eAaImAgFnP	spojovat
a	a	k8xC	a
kombinovaly	kombinovat	k5eAaImAgFnP	kombinovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
normální	normální	k2eAgInPc1d1	normální
organismy	organismus	k1gInPc1	organismus
i	i	k8xC	i
podivné	podivný	k2eAgFnSc2d1	podivná
obludy	obluda	k1gFnSc2	obluda
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
životaneschopná	životaneschopný	k2eAgNnPc1d1	životaneschopný
monstra	monstrum	k1gNnPc1	monstrum
vyhynula	vyhynout	k5eAaPmAgNnP	vyhynout
a	a	k8xC	a
udržely	udržet	k5eAaPmAgInP	udržet
se	se	k3xPyFc4	se
jen	jen	k9	jen
dokonalé	dokonalý	k2eAgFnPc1d1	dokonalá
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
evolučních	evoluční	k2eAgFnPc2d1	evoluční
úvah	úvaha	k1gFnPc2	úvaha
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kladl	klást	k5eAaImAgInS	klást
absolutní	absolutní	k2eAgInSc1d1	absolutní
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
účelnost	účelnost	k1gFnSc4	účelnost
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
orgánu	orgán	k1gInSc2	orgán
či	či	k8xC	či
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
finální	finální	k2eAgFnSc1d1	finální
příčina	příčina	k1gFnSc1	příčina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
orgánu	orgán	k1gInSc2	orgán
nebo	nebo	k8xC	nebo
organismu	organismus	k1gInSc2	organismus
dává	dávat	k5eAaImIp3nS	dávat
účel	účel	k1gInSc1	účel
<g/>
,	,	kIx,	,
cíl	cíl	k1gInSc1	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aristotelovská	aristotelovský	k2eAgFnSc1d1	aristotelovská
teleologie	teleologie	k1gFnSc1	teleologie
chápala	chápat	k5eAaImAgFnS	chápat
účelnost	účelnost	k1gFnSc4	účelnost
jako	jako	k8xS	jako
primárně	primárně	k6eAd1	primárně
danou	daný	k2eAgFnSc4d1	daná
a	a	k8xC	a
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
organizaci	organizace	k1gFnSc6	organizace
živé	živý	k2eAgFnSc2d1	živá
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
Darwin	Darwin	k1gMnSc1	Darwin
dokázal	dokázat	k5eAaPmAgMnS	dokázat
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
účelnosti	účelnost	k1gFnSc2	účelnost
<g/>
.	.	kIx.	.
</s>
<s>
Arabský	arabský	k2eAgMnSc1d1	arabský
myslitel	myslitel	k1gMnSc1	myslitel
al-Džáhiz	al-Džáhiz	k1gMnSc1	al-Džáhiz
již	již	k6eAd1	již
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
st.	st.	kA	st.
n.	n.	k?	n.
l.	l.	k?	l.
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
zárodky	zárodek	k1gInPc4	zárodek
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lamarckismus	lamarckismus	k1gInSc1	lamarckismus
<g/>
.	.	kIx.	.
</s>
<s>
Lamarckismus	Lamarckismus	k1gInSc1	Lamarckismus
(	(	kIx(	(
<g/>
postulovaný	postulovaný	k2eAgMnSc1d1	postulovaný
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
operoval	operovat	k5eAaImAgMnS	operovat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
organismy	organismus	k1gInPc1	organismus
získávají	získávat	k5eAaImIp3nP	získávat
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zkušenosti	zkušenost	k1gFnSc2	zkušenost
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
pak	pak	k6eAd1	pak
zúročují	zúročovat	k5eAaImIp3nP	zúročovat
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
svého	svůj	k3xOyFgNnSc2	svůj
potomstva	potomstvo	k1gNnSc2	potomstvo
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
srna	srna	k1gFnSc1	srna
hodně	hodně	k6eAd1	hodně
běhá	běhat	k5eAaImIp3nS	běhat
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
její	její	k3xOp3gNnPc4	její
mláďata	mládě	k1gNnPc4	mládě
silné	silný	k2eAgFnPc4d1	silná
nohy	noha	k1gFnPc4	noha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lamarckismus	Lamarckismus	k1gInSc1	Lamarckismus
představoval	představovat	k5eAaImAgInS	představovat
první	první	k4xOgFnSc4	první
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
obecně	obecně	k6eAd1	obecně
přijímanou	přijímaný	k2eAgFnSc7d1	přijímaná
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
akceptují	akceptovat	k5eAaBmIp3nP	akceptovat
myšlenky	myšlenka	k1gFnPc4	myšlenka
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
nahrazen	nahradit	k5eAaPmNgInS	nahradit
darwinismem	darwinismus	k1gInSc7	darwinismus
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
biologie	biologie	k1gFnSc1	biologie
jeho	jeho	k3xOp3gFnSc2	jeho
myšlenky	myšlenka	k1gFnSc2	myšlenka
odmítá	odmítat	k5eAaImIp3nS	odmítat
buďto	buďto	k8xC	buďto
celkově	celkově	k6eAd1	celkově
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
maximálně	maximálně	k6eAd1	maximálně
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
modifikované	modifikovaný	k2eAgFnSc6d1	modifikovaná
podobě	podoba	k1gFnSc6	podoba
mohl	moct	k5eAaImAgInS	moct
omezeně	omezeně	k6eAd1	omezeně
fungovat	fungovat	k5eAaImF	fungovat
u	u	k7c2	u
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
totiž	totiž	k9	totiž
znám	znát	k5eAaImIp1nS	znát
žádný	žádný	k3yNgInSc1	žádný
funkční	funkční	k2eAgInSc1d1	funkční
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
rodič	rodič	k1gMnSc1	rodič
životní	životní	k2eAgFnSc2d1	životní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
získané	získaný	k2eAgFnSc2d1	získaná
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
poskytnout	poskytnout	k5eAaPmF	poskytnout
potomkům	potomek	k1gMnPc3	potomek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dědičné	dědičný	k2eAgFnSc2d1	dědičná
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
ani	ani	k8xC	ani
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
u	u	k7c2	u
složitějších	složitý	k2eAgInPc2d2	složitější
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
mohl	moct	k5eAaImAgMnS	moct
existovat	existovat	k5eAaImF	existovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Weismannova	Weismannův	k2eAgFnSc1d1	Weismannův
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
od	od	k7c2	od
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
jménem	jméno	k1gNnSc7	jméno
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lamarck	Lamarck	k1gMnSc1	Lamarck
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
-	-	kIx~	-
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Darwinismus	darwinismus	k1gInSc1	darwinismus
představuje	představovat	k5eAaImIp3nS	představovat
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
Charlesem	Charles	k1gMnSc7	Charles
Darwinem	Darwin	k1gMnSc7	Darwin
a	a	k8xC	a
Alfredem	Alfred	k1gMnSc7	Alfred
Wallacem	Wallace	k1gMnSc7	Wallace
<g/>
.	.	kIx.	.
</s>
<s>
Operuje	operovat	k5eAaImIp3nS	operovat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
evoluce	evoluce	k1gFnSc1	evoluce
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
drobných	drobný	k2eAgFnPc2d1	drobná
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
základě	základ	k1gInSc6	základ
selekce	selekce	k1gFnSc2	selekce
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
organismy	organismus	k1gInPc1	organismus
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
neměnné	neměnný	k2eAgFnPc1d1	neměnná
<g/>
,	,	kIx,	,
že	že	k8xS	že
rámcově	rámcově	k6eAd1	rámcově
dědí	dědit	k5eAaImIp3nP	dědit
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
dědit	dědit	k5eAaImF	dědit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získá	získat	k5eAaPmIp3nS	získat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
tuto	tento	k3xDgFnSc4	tento
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vlastnost	vlastnost	k1gFnSc4	vlastnost
předá	předat	k5eAaPmIp3nS	předat
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
:	:	kIx,	:
ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
<g/>
-li	i	k?	-li
se	s	k7c7	s
srny	srn	k1gMnPc7	srn
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musí	muset	k5eAaImIp3nS	muset
hodně	hodně	k6eAd1	hodně
běhat	běhat	k5eAaImF	běhat
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
řádí	řádit	k5eAaImIp3nS	řádit
šelmy	šelma	k1gFnPc4	šelma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozmnoží	rozmnožit	k5eAaPmIp3nS	rozmnožit
se	se	k3xPyFc4	se
jen	jen	k9	jen
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
běh	běh	k1gInSc4	běh
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
mláďata	mládě	k1gNnPc1	mládě
pak	pak	k6eAd1	pak
budou	být	k5eAaImBp3nP	být
převážně	převážně	k6eAd1	převážně
také	také	k9	také
dobří	dobrý	k2eAgMnPc1d1	dobrý
běžci	běžec	k1gMnPc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
tohoto	tento	k3xDgInSc2	tento
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
postuloval	postulovat	k5eAaImAgMnS	postulovat
Darwin	Darwin	k1gMnSc1	Darwin
ještě	ještě	k9	ještě
pojetí	pojetí	k1gNnSc4	pojetí
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
výběru	výběr	k1gInSc2	výběr
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobu	podoba	k1gFnSc4	podoba
organismu	organismus	k1gInSc2	organismus
neurčuje	určovat	k5eNaImIp3nS	určovat
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
dobře	dobře	k6eAd1	dobře
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
být	být	k5eAaImF	být
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
případné	případný	k2eAgMnPc4d1	případný
sexuální	sexuální	k2eAgMnPc4d1	sexuální
partnery	partner	k1gMnPc4	partner
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kdo	kdo	k3yQnSc1	kdo
nenajde	najít	k5eNaPmIp3nS	najít
partnera	partner	k1gMnSc4	partner
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
též	též	k9	též
nerozmnoží	rozmnožit	k5eNaPmIp3nS	rozmnožit
a	a	k8xC	a
nepředá	předat	k5eNaPmIp3nS	předat
potomkům	potomek	k1gMnPc3	potomek
své	svůj	k3xOyFgFnSc2	svůj
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Darwinismus	darwinismus	k1gInSc1	darwinismus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgFnSc7	první
evoluční	evoluční	k2eAgFnSc7d1	evoluční
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obecně	obecně	k6eAd1	obecně
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
vědecké	vědecký	k2eAgNnSc4d1	vědecké
myšlení	myšlení	k1gNnSc4	myšlení
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
též	též	k9	též
také	také	k9	také
poslední	poslední	k2eAgFnSc7d1	poslední
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
teorií	teorie	k1gFnSc7	teorie
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
jasným	jasný	k2eAgNnSc7d1	jasné
pojetím	pojetí	k1gNnSc7	pojetí
principů	princip	k1gInPc2	princip
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
dosti	dosti	k6eAd1	dosti
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
omezoval	omezovat	k5eAaImAgInS	omezovat
se	se	k3xPyFc4	se
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
i	i	k9	i
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
absence	absence	k1gFnSc1	absence
rozsáhlejšího	rozsáhlý	k2eAgNnSc2d2	rozsáhlejší
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
poznání	poznání	k1gNnSc2	poznání
života	život	k1gInSc2	život
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
genetiky	genetika	k1gFnSc2	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
znalosti	znalost	k1gFnPc1	znalost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgInPc2	tento
oborů	obor	k1gInPc2	obor
rozšiřovaly	rozšiřovat	k5eAaImAgFnP	rozšiřovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
pojatá	pojatý	k2eAgFnSc1d1	pojatá
základní	základní	k2eAgFnSc1d1	základní
myšlenka	myšlenka	k1gFnSc1	myšlenka
naprosto	naprosto	k6eAd1	naprosto
nedostačuje	dostačovat	k5eNaImIp3nS	dostačovat
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
darwinismus	darwinismus	k1gInSc4	darwinismus
modifikován	modifikován	k2eAgInSc4d1	modifikován
a	a	k8xC	a
obohacován	obohacován	k2eAgInSc4d1	obohacován
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
postuláty	postulát	k1gInPc4	postulát
a	a	k8xC	a
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
se	se	k3xPyFc4	se
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
syntézu	syntéza	k1gFnSc4	syntéza
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
poznáním	poznání	k1gNnSc7	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
nově	nova	k1gFnSc3	nova
vznikající	vznikající	k2eAgFnSc6d1	vznikající
syntéze	syntéza	k1gFnSc6	syntéza
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
evoluční	evoluční	k2eAgFnSc1d1	evoluční
syntéza	syntéza	k1gFnSc1	syntéza
je	být	k5eAaImIp3nS	být
sloučení	sloučení	k1gNnSc4	sloučení
darwinismu	darwinismus	k1gInSc2	darwinismus
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
<g/>
)	)	kIx)	)
a	a	k8xC	a
genetiky	genetika	k1gFnSc2	genetika
(	(	kIx(	(
<g/>
neměnnosti	neměnnost	k1gFnSc2	neměnnost
genu	gen	k1gInSc2	gen
<g/>
)	)	kIx)	)
v	v	k7c4	v
neodarwinismus	neodarwinismus	k1gInSc4	neodarwinismus
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
moderní	moderní	k2eAgFnSc7d1	moderní
evoluční	evoluční	k2eAgFnSc7d1	evoluční
syntézou	syntéza	k1gFnSc7	syntéza
a	a	k8xC	a
neodarwinismem	neodarwinismus	k1gInSc7	neodarwinismus
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ernst	Ernst	k1gMnSc1	Ernst
Mayr	Mayr	k1gMnSc1	Mayr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
používán	používán	k2eAgInSc1d1	používán
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
teorii	teorie	k1gFnSc4	teorie
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gInSc4	Russel
Wallace	Wallace	k1gFnSc2	Wallace
a	a	k8xC	a
August	August	k1gMnSc1	August
Weismann	Weismann	k1gMnSc1	Weismann
-	-	kIx~	-
kompletní	kompletní	k2eAgNnSc1d1	kompletní
očištění	očištění	k1gNnSc1	očištění
darwinismu	darwinismus	k1gInSc2	darwinismus
od	od	k7c2	od
lamarckismu	lamarckismus	k1gInSc2	lamarckismus
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
použití	použití	k1gNnSc1	použití
pojmu	pojem	k1gInSc2	pojem
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
adaptacionismus	adaptacionismus	k1gInSc4	adaptacionismus
(	(	kIx(	(
<g/>
panselekcionismus	panselekcionismus	k1gInSc4	panselekcionismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
převažující	převažující	k2eAgNnSc1d1	převažující
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderní	moderní	k2eAgFnSc1d1	moderní
evoluční	evoluční	k2eAgFnSc1d1	evoluční
syntéza	syntéza	k1gFnSc1	syntéza
a	a	k8xC	a
neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
jsou	být	k5eAaImIp3nP	být
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Neodarwinismus	neodarwinismus	k1gInSc1	neodarwinismus
představuje	představovat	k5eAaImIp3nS	představovat
snahu	snaha	k1gFnSc4	snaha
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
myšlenky	myšlenka	k1gFnPc4	myšlenka
darwinismu	darwinismus	k1gInSc2	darwinismus
s	s	k7c7	s
poznatky	poznatek	k1gInPc7	poznatek
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
cytologie	cytologie	k1gFnSc2	cytologie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
objevy	objev	k1gInPc7	objev
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
parazitologie	parazitologie	k1gFnSc2	parazitologie
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nelze	lze	k6eNd1	lze
dost	dost	k6eAd1	dost
přesně	přesně	k6eAd1	přesně
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
různým	různý	k2eAgMnPc3d1	různý
evolučním	evoluční	k2eAgMnPc3d1	evoluční
biologům	biolog	k1gMnPc3	biolog
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
syntézy	syntéza	k1gFnSc2	syntéza
dosti	dosti	k6eAd1	dosti
odlišné	odlišný	k2eAgInPc1d1	odlišný
koncepty	koncept	k1gInPc1	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
za	za	k7c4	za
neodarwinismus	neodarwinismus	k1gInSc4	neodarwinismus
považují	považovat	k5eAaImIp3nP	považovat
ty	ten	k3xDgInPc4	ten
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
základní	základní	k2eAgFnPc4d1	základní
myšlenky	myšlenka	k1gFnPc4	myšlenka
darwinismu	darwinismus	k1gInSc2	darwinismus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pozvolnost	pozvolnost	k1gFnSc4	pozvolnost
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
přirozený	přirozený	k2eAgInSc4d1	přirozený
výběr	výběr	k1gInSc4	výběr
skrze	skrze	k?	skrze
prospěšnost	prospěšnost	k1gFnSc1	prospěšnost
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
směřování	směřování	k1gNnSc4	směřování
evoluce	evoluce	k1gFnSc2	evoluce
ke	k	k7c3	k
složitějším	složitý	k2eAgFnPc3d2	složitější
strukturám	struktura	k1gFnPc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
překonán	překonán	k2eAgInSc1d1	překonán
<g/>
,	,	kIx,	,
nepřeberné	přeberný	k2eNgFnSc2d1	nepřeberná
moderní	moderní	k2eAgFnSc2d1	moderní
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
směry	směr	k1gInPc1	směr
pojetí	pojetí	k1gNnSc4	pojetí
evoluce	evoluce	k1gFnSc2	evoluce
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
koncepty	koncept	k1gInPc7	koncept
velmi	velmi	k6eAd1	velmi
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
skokových	skokový	k2eAgFnPc2d1	skoková
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
důležitost	důležitost	k1gFnSc4	důležitost
náhodných	náhodný	k2eAgInPc2d1	náhodný
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
genetický	genetický	k2eAgInSc4d1	genetický
drift	drift	k1gInSc4	drift
a	a	k8xC	a
efekt	efekt	k1gInSc4	efekt
hrdla	hrdlo	k1gNnSc2	hrdlo
lahve	lahev	k1gFnSc2	lahev
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
fixují	fixovat	k5eAaImIp3nP	fixovat
i	i	k9	i
neutrální	neutrální	k2eAgFnPc1d1	neutrální
nebo	nebo	k8xC	nebo
vyloženě	vyloženě	k6eAd1	vyloženě
špatné	špatný	k2eAgFnPc4d1	špatná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
mizí	mizet	k5eAaImIp3nP	mizet
ty	ten	k3xDgFnPc1	ten
dobré	dobrý	k2eAgFnPc1d1	dobrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
automatické	automatický	k2eAgNnSc4d1	automatické
směřování	směřování	k1gNnSc4	směřování
evoluce	evoluce	k1gFnSc2	evoluce
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
složitějším	složitý	k2eAgFnPc3d2	složitější
strukturám	struktura	k1gFnPc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
vědců	vědec	k1gMnPc2	vědec
vnímá	vnímat	k5eAaImIp3nS	vnímat
pojem	pojem	k1gInSc1	pojem
mnohem	mnohem	k6eAd1	mnohem
šířeji	šířej	k1gInPc7	šířej
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
neodarwinismus	neodarwinismus	k1gInSc4	neodarwinismus
modernější	moderní	k2eAgInPc1d2	modernější
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
chápaní	chápaný	k2eAgMnPc1d1	chápaný
evoluce	evoluce	k1gFnSc2	evoluce
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
faktory	faktor	k1gInPc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ty	ten	k3xDgFnPc4	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
častěji	často	k6eAd2	často
používán	používán	k2eAgInSc1d1	používán
pojem	pojem	k1gInSc1	pojem
moderní	moderní	k2eAgFnSc1d1	moderní
evoluční	evoluční	k2eAgFnSc1d1	evoluční
syntéza	syntéza	k1gFnSc1	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
námitek	námitka	k1gFnPc2	námitka
proti	proti	k7c3	proti
evoluční	evoluční	k2eAgFnSc3d1	evoluční
teorii	teorie	k1gFnSc3	teorie
bezprostředně	bezprostředně	k6eAd1	bezprostředně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
vědy	věda	k1gFnSc2	věda
jako	jako	k8xC	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odpůrci	odpůrce	k1gMnPc1	odpůrce
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
pouhá	pouhý	k2eAgFnSc1d1	pouhá
teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
vědecký	vědecký	k2eAgInSc4d1	vědecký
fakt	fakt	k1gInSc4	fakt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Wells	Wellsa	k1gFnPc2	Wellsa
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
chápaná	chápaný	k2eAgFnSc1d1	chápaná
jako	jako	k8xC	jako
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
"	"	kIx"	"
<g/>
původu	původ	k1gInSc2	původ
všech	všecek	k3xTgInPc2	všecek
organismů	organismus	k1gInPc2	organismus
ze	z	k7c2	z
společného	společný	k2eAgInSc2d1	společný
předka	předek	k1gMnSc4	předek
působením	působení	k1gNnSc7	působení
neřízených	řízený	k2eNgInPc2d1	neřízený
přírodních	přírodní	k2eAgInPc2d1	přírodní
procesů	proces	k1gInPc2	proces
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
přímém	přímý	k2eAgNnSc6d1	přímé
pozorování	pozorování	k1gNnSc6	pozorování
či	či	k8xC	či
experimentu	experiment	k1gInSc6	experiment
a	a	k8xC	a
tedy	tedy	k8xC	tedy
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
definici	definice	k1gFnSc4	definice
vědecké	vědecký	k2eAgFnSc2d1	vědecká
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hendl	Hendl	k1gMnSc1	Hendl
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
běžné	běžný	k2eAgNnSc1d1	běžné
pojetí	pojetí	k1gNnSc1	pojetí
vědecké	vědecký	k2eAgFnSc2d1	vědecká
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
atributy	atribut	k1gInPc4	atribut
přímého	přímý	k2eAgNnSc2d1	přímé
pozorování	pozorování	k1gNnSc2	pozorování
a	a	k8xC	a
experimentu	experiment	k1gInSc2	experiment
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
jako	jako	k9	jako
nezbytně	zbytně	k6eNd1	zbytně
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odpůrci	odpůrce	k1gMnPc1	odpůrce
evoluce	evoluce	k1gFnSc2	evoluce
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgInSc1	druhý
termodynamický	termodynamický	k2eAgInSc1d1	termodynamický
zákon	zákon	k1gInSc1	zákon
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
zvyšování	zvyšování	k1gNnSc4	zvyšování
uspořádanosti	uspořádanost	k1gFnSc2	uspořádanost
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
snižování	snižování	k1gNnSc1	snižování
entropie	entropie	k1gFnSc2	entropie
<g/>
)	)	kIx)	)
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
vysoce	vysoce	k6eAd1	vysoce
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
nemohly	moct	k5eNaImAgInP	moct
evolučně	evolučně	k6eAd1	evolučně
vyvinout	vyvinout	k5eAaPmF	vyvinout
z	z	k7c2	z
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
neživých	živý	k2eNgFnPc2d1	neživá
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
termodynamický	termodynamický	k2eAgInSc1d1	termodynamický
zákon	zákon	k1gInSc1	zákon
ovšem	ovšem	k9	ovšem
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
snižování	snižování	k1gNnSc4	snižování
entropie	entropie	k1gFnSc2	entropie
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
izolovaných	izolovaný	k2eAgInPc6d1	izolovaný
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
není	být	k5eNaImIp3nS	být
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
přijímá	přijímat	k5eAaImIp3nS	přijímat
sluneční	sluneční	k2eAgFnSc4d1	sluneční
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
svou	svůj	k3xOyFgFnSc4	svůj
uspořádanost	uspořádanost	k1gFnSc4	uspořádanost
(	(	kIx(	(
<g/>
snižovat	snižovat	k5eAaImF	snižovat
entropii	entropie	k1gFnSc4	entropie
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
okolního	okolní	k2eAgInSc2d1	okolní
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nejsou	být	k5eNaImIp3nP	být
izolovanými	izolovaný	k2eAgInPc7d1	izolovaný
systémy	systém	k1gInPc7	systém
ani	ani	k8xC	ani
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
a	a	k8xC	a
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
svou	svůj	k3xOyFgFnSc4	svůj
uspořádanost	uspořádanost	k1gFnSc4	uspořádanost
(	(	kIx(	(
<g/>
snižovat	snižovat	k5eAaImF	snižovat
entropii	entropie	k1gFnSc4	entropie
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nezjednodušitelná	Nezjednodušitelný	k2eAgFnSc1d1	Nezjednodušitelná
složitost	složitost	k1gFnSc1	složitost
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
argumentů	argument	k1gInPc2	argument
zastánců	zastánce	k1gMnPc2	zastánce
inteligentního	inteligentní	k2eAgInSc2d1	inteligentní
plánu	plán	k1gInSc2	plán
proti	proti	k7c3	proti
evoluční	evoluční	k2eAgFnSc3d1	evoluční
teorii	teorie	k1gFnSc3	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
představě	představa	k1gFnSc6	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
biologické	biologický	k2eAgFnPc1d1	biologická
struktury	struktura	k1gFnPc1	struktura
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
složité	složitý	k2eAgInPc1d1	složitý
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
z	z	k7c2	z
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
struktur	struktura	k1gFnPc2	struktura
přirozeným	přirozený	k2eAgInSc7d1	přirozený
výběrem	výběr	k1gInSc7	výběr
pracujícím	pracující	k1gMnSc7	pracující
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
náhodných	náhodný	k2eAgFnPc2d1	náhodná
mutací	mutace	k1gFnPc2	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
složité	složitý	k2eAgFnPc1d1	složitá
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
oko	oko	k1gNnSc1	oko
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
díky	díky	k7c3	díky
četným	četný	k2eAgFnPc3d1	četná
následným	následný	k2eAgFnPc3d1	následná
malým	malý	k2eAgFnPc3d1	malá
modifikacím	modifikace	k1gFnPc3	modifikace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přechodné	přechodný	k2eAgInPc1d1	přechodný
stupně	stupeň	k1gInPc1	stupeň
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
možný	možný	k2eAgInSc1d1	možný
způsob	způsob	k1gInSc1	způsob
evoluce	evoluce	k1gFnSc2	evoluce
oka	oko	k1gNnSc2	oko
přes	přes	k7c4	přes
funkční	funkční	k2eAgFnSc4d1	funkční
a	a	k8xC	a
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgInPc4d2	dokonalejší
mezistupně	mezistupeň	k1gInPc4	mezistupeň
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
již	již	k6eAd1	již
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
vyřešenou	vyřešený	k2eAgFnSc4d1	vyřešená
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládané	předpokládaný	k2eAgInPc1d1	předpokládaný
mezistupně	mezistupeň	k1gInPc1	mezistupeň
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
známých	známý	k2eAgMnPc2d1	známý
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
počítačové	počítačový	k2eAgInPc1d1	počítačový
modely	model	k1gInPc1	model
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
oka	oko	k1gNnSc2	oko
schopného	schopný	k2eAgNnSc2d1	schopné
ostření	ostření	k1gNnSc2	ostření
ze	z	k7c2	z
shluků	shluk	k1gInPc2	shluk
světločivných	světločivný	k2eAgFnPc2d1	světločivná
buněk	buňka	k1gFnPc2	buňka
může	moct	k5eAaImIp3nS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
během	během	k7c2	během
pouhých	pouhý	k2eAgFnPc2d1	pouhá
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
běžně	běžně	k6eAd1	běžně
uváděným	uváděný	k2eAgInSc7d1	uváděný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
evoluce	evoluce	k1gFnSc1	evoluce
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
reprodukčních	reprodukční	k2eAgInPc6d1	reprodukční
orgánech	orgán	k1gInPc6	orgán
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
neplodnosti	neplodnost	k1gFnSc3	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
by	by	kYmCp3nS	by
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
najednou	najednou	k6eAd1	najednou
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příklad	příklad	k1gInSc4	příklad
nezjednodušitelné	zjednodušitelný	k2eNgFnSc2d1	nezjednodušitelná
složitosti	složitost	k1gFnSc2	složitost
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
známé	známá	k1gFnSc2	známá
svou	svůj	k3xOyFgFnSc7	svůj
proměnlivostí	proměnlivost	k1gFnSc7	proměnlivost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
určování	určování	k1gNnSc2	určování
druhů	druh	k1gInPc2	druh
brouků	brouk	k1gMnPc2	brouk
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
morfologických	morfologický	k2eAgInPc2d1	morfologický
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
vydáních	vydání	k1gNnPc6	vydání
knih	kniha	k1gFnPc2	kniha
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
O	o	k7c4	o
původu	původa	k1gMnSc4	původa
člověka	člověk	k1gMnSc4	člověk
odvolával	odvolávat	k5eAaImAgMnS	odvolávat
i	i	k9	i
na	na	k7c4	na
kresby	kresba	k1gFnPc4	kresba
embryí	embryo	k1gNnPc2	embryo
Ernsta	Ernst	k1gMnSc2	Ernst
Haeckela	Haeckel	k1gMnSc2	Haeckel
<g/>
.	.	kIx.	.
</s>
<s>
Haeckelovy	Haeckelův	k2eAgFnPc1d1	Haeckelova
kresby	kresba	k1gFnPc1	kresba
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
myšlenek	myšlenka	k1gFnPc2	myšlenka
Karla	Karel	k1gMnSc2	Karel
Ernsta	Ernst	k1gMnSc2	Ernst
von	von	k1gInSc4	von
Baera	Baero	k1gNnSc2	Baero
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
embrya	embryo	k1gNnSc2	embryo
některých	některý	k3yIgMnPc2	některý
obratlovců	obratlovec	k1gMnPc2	obratlovec
procházejí	procházet	k5eAaImIp3nP	procházet
stadiem	stadion	k1gNnSc7	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vypadají	vypadat	k5eAaImIp3nP	vypadat
všechna	všechen	k3xTgNnPc4	všechen
dost	dost	k6eAd1	dost
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
to	ten	k3xDgNnSc4	ten
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
společného	společný	k2eAgInSc2d1	společný
původu	původ	k1gInSc2	původ
všech	všecek	k3xTgMnPc2	všecek
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Haeckel	Haeckel	k1gMnSc1	Haeckel
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
zastáncem	zastánce	k1gMnSc7	zastánce
a	a	k8xC	a
popularizátorem	popularizátor	k1gMnSc7	popularizátor
Darwinovy	Darwinův	k2eAgFnSc2d1	Darwinova
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
pronikání	pronikání	k1gNnSc3	pronikání
náboženství	náboženství	k1gNnSc2	náboženství
do	do	k7c2	do
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
rozlítil	rozlítit	k5eAaPmAgMnS	rozlítit
tak	tak	k9	tak
řadu	řada	k1gFnSc4	řada
svých	svůj	k3xOyFgMnPc2	svůj
nábožensky	nábožensky	k6eAd1	nábožensky
založených	založený	k2eAgMnPc2d1	založený
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
své	svůj	k3xOyFgFnSc2	svůj
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
knihy	kniha	k1gFnSc2	kniha
Natürliche	Natürlichus	k1gMnSc5	Natürlichus
Schöpfungsgeschichte	Schöpfungsgeschicht	k1gMnSc5	Schöpfungsgeschicht
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslovně	doslovně	k6eAd1	doslovně
"	"	kIx"	"
<g/>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
historie	historie	k1gFnSc1	historie
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgMnS	použít
Haeckel	Haeckel	k1gMnSc1	Haeckel
identický	identický	k2eAgMnSc1d1	identický
dřevořez	dřevořez	k1gInSc4	dřevořez
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
raného	raný	k2eAgInSc2d1	raný
vývoje	vývoj	k1gInSc2	vývoj
zárodků	zárodek	k1gInPc2	zárodek
tří	tři	k4xCgMnPc2	tři
různých	různý	k2eAgMnPc2d1	různý
obratlovců	obratlovec	k1gMnPc2	obratlovec
-	-	kIx~	-
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
kuřete	kuře	k1gNnSc2	kuře
a	a	k8xC	a
želvy	želva	k1gFnSc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
recenzent	recenzent	k1gMnSc1	recenzent
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
Haeckel	Haeckel	k1gMnSc1	Haeckel
se	se	k3xPyFc4	se
hájil	hájit	k5eAaImAgMnS	hájit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
takto	takto	k6eAd1	takto
raném	raný	k2eAgNnSc6d1	rané
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
žádné	žádný	k3yNgInPc4	žádný
rozdíly	rozdíl	k1gInPc4	rozdíl
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
přístrojového	přístrojový	k2eAgNnSc2d1	přístrojové
vybavení	vybavení	k1gNnSc2	vybavení
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
Haeckel	Haeckel	k1gMnSc1	Haeckel
uznal	uznat	k5eAaPmAgMnS	uznat
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
text	text	k1gInSc4	text
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
příštím	příští	k2eAgNnSc6d1	příští
vydání	vydání	k1gNnSc6	vydání
upravil	upravit	k5eAaPmAgMnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nepřestali	přestat	k5eNaPmAgMnP	přestat
toto	tento	k3xDgNnSc4	tento
Haeckelovo	Haeckelův	k2eAgNnSc4d1	Haeckelovo
pochybení	pochybení	k1gNnSc4	pochybení
připomínat	připomínat	k5eAaImF	připomínat
a	a	k8xC	a
několik	několik	k4yIc1	několik
vědců	vědec	k1gMnPc2	vědec
kritizujících	kritizující	k2eAgMnPc2d1	kritizující
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Emil	Emil	k1gMnSc1	Emil
du	du	k?	du
Bois-Reymond	Bois-Reymond	k1gMnSc1	Bois-Reymond
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Virchow	Virchow	k1gMnSc1	Virchow
nebo	nebo	k8xC	nebo
Louis	Louis	k1gMnSc1	Louis
Agassiz	Agassiz	k1gMnSc1	Agassiz
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
obvinilo	obvinit	k5eAaPmAgNnS	obvinit
Haeckela	Haeckela	k1gFnSc1	Haeckela
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jako	jako	k9	jako
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Henry	henry	k1gInSc2	henry
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
Weismann	Weismann	k1gMnSc1	Weismann
nebo	nebo	k8xC	nebo
Carl	Carl	k1gMnSc1	Carl
Gegenbaur	Gegenbaura	k1gFnPc2	Gegenbaura
však	však	k9	však
zachovali	zachovat	k5eAaPmAgMnP	zachovat
Haeckelovi	Haeckel	k1gMnSc3	Haeckel
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obviněný	k1gMnPc1	obviněný
Ernsta	Ernst	k1gMnSc2	Ernst
Haeckela	Haeckel	k1gMnSc2	Haeckel
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
oživeno	oživit	k5eAaPmNgNnS	oživit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Pennisi	Pennise	k1gFnSc4	Pennise
publikovala	publikovat	k5eAaBmAgFnS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
editoriál	editoriál	k1gInSc1	editoriál
informující	informující	k2eAgInSc1d1	informující
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
Michaela	Michael	k1gMnSc2	Michael
Richardsona	Richardson	k1gMnSc2	Richardson
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
provedli	provést	k5eAaPmAgMnP	provést
srovnání	srovnání	k1gNnSc4	srovnání
Haeckelových	Haeckelův	k2eAgFnPc2d1	Haeckelova
kreseb	kresba	k1gFnPc2	kresba
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
embryí	embryo	k1gNnPc2	embryo
stejných	stejný	k2eAgInPc2d1	stejný
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
srovnatelné	srovnatelný	k2eAgFnSc6d1	srovnatelná
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
série	série	k1gFnSc1	série
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
lišily	lišit	k5eAaImAgFnP	lišit
nejen	nejen	k6eAd1	nejen
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
Haeckelových	Haeckelův	k2eAgFnPc2d1	Haeckelova
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
usvědčovaly	usvědčovat	k5eAaImAgFnP	usvědčovat
Haeckela	Haeckel	k1gMnSc2	Haeckel
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Richardson	Richardson	k1gInSc1	Richardson
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
článku	článek	k1gInSc6	článek
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
prohlášením	prohlášení	k1gNnSc7	prohlášení
"	"	kIx"	"
<g/>
vypadá	vypadat	k5eAaPmIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
podvrhů	podvrh	k1gInPc2	podvrh
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Richardsonovy	Richardsonův	k2eAgInPc1d1	Richardsonův
závěry	závěr	k1gInPc1	závěr
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířily	šířit	k5eAaImAgInP	šířit
populárním	populární	k2eAgInSc7d1	populární
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
londýnský	londýnský	k2eAgInSc1d1	londýnský
deník	deník	k1gInSc1	deník
The	The	k1gFnPc2	The
Times	Timesa	k1gFnPc2	Timesa
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
článek	článek	k1gInSc1	článek
obviňující	obviňující	k2eAgFnSc4d1	obviňující
Haeckela	Haeckela	k1gFnSc1	Haeckela
z	z	k7c2	z
úmyslného	úmyslný	k2eAgInSc2d1	úmyslný
podvodu	podvod	k1gInSc2	podvod
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
An	An	k1gFnPc2	An
Embryonic	Embryonice	k1gFnPc2	Embryonice
Liar	Liar	k1gInSc1	Liar
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslovně	doslovně	k6eAd1	doslovně
"	"	kIx"	"
<g/>
Embryonální	embryonální	k2eAgMnSc1d1	embryonální
lhář	lhář	k1gMnSc1	lhář
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obviněním	obvinění	k1gNnSc7	obvinění
se	se	k3xPyFc4	se
ztotožnila	ztotožnit	k5eAaPmAgFnS	ztotožnit
i	i	k9	i
řada	řada	k1gFnSc1	řada
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
biologů	biolog	k1gMnPc2	biolog
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Jay	Jay	k1gMnSc1	Jay
Gould	Gould	k1gMnSc1	Gould
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Richards	Richards	k1gInSc1	Richards
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
však	však	k9	však
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Richardsonovy	Richardsonův	k2eAgFnPc1d1	Richardsonova
fotografie	fotografia	k1gFnPc1	fotografia
jsou	být	k5eAaImIp3nP	být
zavádějící	zavádějící	k2eAgFnPc1d1	zavádějící
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
zárodky	zárodek	k1gInPc1	zárodek
jsou	být	k5eAaImIp3nP	být
foceny	focen	k2eAgInPc1d1	focen
se	s	k7c7	s
žloutkovým	žloutkový	k2eAgInSc7d1	žloutkový
váčkem	váček	k1gInSc7	váček
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
(	(	kIx(	(
<g/>
Haeckelovy	Haeckelův	k2eAgFnSc2d1	Haeckelova
kresby	kresba	k1gFnSc2	kresba
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
bez	bez	k7c2	bez
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
váčku	váček	k1gInSc2	váček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Roček	Roček	k1gMnSc1	Roček
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Historie	historie	k1gFnSc1	historie
obratlovců	obratlovec	k1gMnPc2	obratlovec
však	však	k9	však
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
nijak	nijak	k6eAd1	nijak
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
či	či	k8xC	či
neplatností	neplatnost	k1gFnSc7	neplatnost
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Haeckelův	Haeckelův	k2eAgInSc1d1	Haeckelův
biogenetický	biogenetický	k2eAgInSc1d1	biogenetický
zákon	zákon	k1gInSc1	zákon
totiž	totiž	k9	totiž
sehrál	sehrát	k5eAaPmAgInS	sehrát
roli	role	k1gFnSc4	role
především	především	k9	především
jako	jako	k8xC	jako
inspirace	inspirace	k1gFnSc1	inspirace
a	a	k8xC	a
nepředstavuje	představovat	k5eNaImIp3nS	představovat
nutnou	nutný	k2eAgFnSc4d1	nutná
podmínku	podmínka	k1gFnSc4	podmínka
platnosti	platnost	k1gFnSc2	platnost
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
neplatností	neplatnost	k1gFnSc7	neplatnost
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jediný	jediný	k2eAgInSc1d1	jediný
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
embryonální	embryonální	k2eAgNnPc1d1	embryonální
stádia	stádium	k1gNnPc1	stádium
chráněna	chráněn	k2eAgFnSc1d1	chráněna
před	před	k7c7	před
evolučními	evoluční	k2eAgInPc7d1	evoluční
tlaky	tlak	k1gInPc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Fakticita	fakticita	k1gFnSc1	fakticita
fosilního	fosilní	k2eAgInSc2d1	fosilní
záznamu	záznam	k1gInSc2	záznam
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
neodarwinisty	neodarwinista	k1gMnPc7	neodarwinista
a	a	k8xC	a
kreacionisty	kreacionista	k1gMnPc7	kreacionista
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
datovací	datovací	k2eAgFnPc1d1	datovací
metody	metoda	k1gFnPc1	metoda
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
dokladů	doklad	k1gInPc2	doklad
stáří	stáří	k1gNnSc2	stáří
dnes	dnes	k6eAd1	dnes
známých	známý	k2eAgFnPc2d1	známá
fosílií	fosílie	k1gFnPc2	fosílie
<g/>
.	.	kIx.	.
</s>
<s>
Kreacionisté	Kreacionista	k1gMnPc1	Kreacionista
mnohdy	mnohdy	k6eAd1	mnohdy
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
dostatek	dostatek	k1gInSc4	dostatek
fosílií	fosílie	k1gFnPc2	fosílie
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
články	článek	k1gInPc4	článek
evolučního	evoluční	k2eAgInSc2d1	evoluční
vývoje	vývoj	k1gInSc2	vývoj
nekontinuálně	kontinuálně	k6eNd1	kontinuálně
propojeny	propojit	k5eAaPmNgInP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgMnSc1d1	evoluční
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
Marek	Marek	k1gMnSc1	Marek
Vácha	Vácha	k1gMnSc1	Vácha
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
fosílií	fosílie	k1gFnPc2	fosílie
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
necháváme	nechávat	k5eAaImIp1nP	nechávat
rozpadnout	rozpadnout	k5eAaPmF	rozpadnout
kosti	kost	k1gFnPc4	kost
na	na	k7c6	na
kompostu	kompost	k1gInSc6	kompost
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
fosílie	fosílie	k1gFnSc1	fosílie
uchovala	uchovat	k5eAaPmAgFnS	uchovat
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
živočicha	živočich	k1gMnSc2	živočich
přikryta	přikrýt	k5eAaPmNgFnS	přikrýt
vrstvou	vrstva	k1gFnSc7	vrstva
popela	popel	k1gInSc2	popel
a	a	k8xC	a
zalita	zalit	k2eAgFnSc1d1	zalita
magmatem	magma	k1gNnSc7	magma
<g/>
,	,	kIx,	,
kompletně	kompletně	k6eAd1	kompletně
zmražena	zmražen	k2eAgFnSc1d1	zmražena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
usazena	usazen	k2eAgFnSc1d1	usazena
ve	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vápničité	vápničitý	k2eAgFnSc6d1	vápničitý
propadlině	propadlina	k1gFnSc6	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
totiž	totiž	k9	totiž
byly	být	k5eAaImAgFnP	být
zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
rozloženy	rozložen	k2eAgFnPc1d1	rozložena
reducenty	reducenta	k1gFnPc1	reducenta
a	a	k8xC	a
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
bakteriemi	bakterie	k1gFnPc7	bakterie
nebo	nebo	k8xC	nebo
znehodnoceny	znehodnocen	k2eAgInPc4d1	znehodnocen
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
a	a	k8xC	a
chemickými	chemický	k2eAgInPc7d1	chemický
procesy	proces	k1gInPc7	proces
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
<g/>
,	,	kIx,	,
rozpad	rozpad	k1gInSc1	rozpad
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
mechanické	mechanický	k2eAgNnSc1d1	mechanické
narušování	narušování	k1gNnSc1	narušování
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
vlnami	vlna	k1gFnPc7	vlna
nebo	nebo	k8xC	nebo
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Fosílie	fosílie	k1gFnPc1	fosílie
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
často	často	k6eAd1	často
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
v	v	k7c6	v
sopečných	sopečný	k2eAgFnPc6d1	sopečná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ze	z	k7c2	z
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
často	často	k6eAd1	často
vyplavují	vyplavovat	k5eAaImIp3nP	vyplavovat
právě	právě	k6eAd1	právě
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
prvky	prvek	k1gInPc1	prvek
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
relativnímu	relativní	k2eAgNnSc3d1	relativní
datování	datování	k1gNnSc3	datování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programování	programování	k1gNnSc6	programování
a	a	k8xC	a
robotice	robotika	k1gFnSc6	robotika
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
evoluce	evoluce	k1gFnSc2	evoluce
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
optimalizační	optimalizační	k2eAgFnSc4d1	optimalizační
metodu	metoda	k1gFnSc4	metoda
genetického	genetický	k2eAgNnSc2d1	genetické
programování	programování	k1gNnSc2	programování
(	(	kIx(	(
<g/>
obecněji	obecně	k6eAd2	obecně
evolučního	evoluční	k2eAgNnSc2d1	evoluční
programování	programování	k1gNnSc2	programování
<g/>
)	)	kIx)	)
připomínající	připomínající	k2eAgFnSc4d1	připomínající
biologickou	biologický	k2eAgFnSc4d1	biologická
evoluci	evoluce	k1gFnSc4	evoluce
<g/>
:	:	kIx,	:
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
heuristická	heuristický	k2eAgFnSc1d1	heuristická
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fitness	fitness	k1gInSc1	fitness
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
ohodnotí	ohodnotit	k5eAaPmIp3nS	ohodnotit
chování	chování	k1gNnSc1	chování
populace	populace	k1gFnSc2	populace
robotů	robot	k1gInPc2	robot
(	(	kIx(	(
<g/>
skutečných	skutečný	k2eAgMnPc2d1	skutečný
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
emulovaných	emulovaný	k2eAgInPc2d1	emulovaný
software	software	k1gInSc4	software
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
populace	populace	k1gFnSc1	populace
křížením	křížení	k1gNnSc7	křížení
a	a	k8xC	a
náhodnou	náhodný	k2eAgFnSc7d1	náhodná
modifikací	modifikace	k1gFnSc7	modifikace
těch	ten	k3xDgMnPc2	ten
(	(	kIx(	(
<g/>
nej	nej	k?	nej
<g/>
)	)	kIx)	)
<g/>
úspěšnějších	úspěšný	k2eAgFnPc2d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
umělé	umělý	k2eAgFnSc6d1	umělá
evoluci	evoluce	k1gFnSc6	evoluce
závisí	záviset	k5eAaImIp3nS	záviset
rychlost	rychlost	k1gFnSc1	rychlost
vývoje	vývoj	k1gInSc2	vývoj
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
výkonu	výkon	k1gInSc6	výkon
použitého	použitý	k2eAgNnSc2d1	Použité
výpočetního	výpočetní	k2eAgNnSc2d1	výpočetní
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
až	až	k9	až
o	o	k7c4	o
miliardy	miliarda	k4xCgFnPc4	miliarda
generací	generace	k1gFnPc2	generace
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
podobnými	podobný	k2eAgFnPc7d1	podobná
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
jako	jako	k8xS	jako
biologická	biologický	k2eAgFnSc1d1	biologická
evoluce	evoluce	k1gFnSc1	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
využívá	využívat	k5eAaPmIp3nS	využívat
například	například	k6eAd1	například
strojové	strojový	k2eAgNnSc1d1	strojové
učení	učení	k1gNnSc1	učení
či	či	k8xC	či
genetické	genetický	k2eAgNnSc1d1	genetické
programování	programování	k1gNnSc1	programování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
někomu	někdo	k3yInSc3	někdo
připomínat	připomínat	k5eAaImF	připomínat
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
evoluci	evoluce	k1gFnSc4	evoluce
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
analogicky	analogicky	k6eAd1	analogicky
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
nebyla	být	k5eNaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
<g/>
.	.	kIx.	.
</s>
