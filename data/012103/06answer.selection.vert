<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
pramenech	pramen	k1gInPc6	pramen
GDP	GDP	kA	GDP
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Gross	Gross	k1gMnSc1	Gross
Domestic	Domestice	k1gFnPc2	Domestice
Product	Product	k1gMnSc1	Product
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
finální	finální	k2eAgFnSc1d1	finální
celková	celkový	k2eAgFnSc1d1	celková
peněžní	peněžní	k2eAgFnSc1d1	peněžní
hodnota	hodnota	k1gFnSc1	hodnota
statků	statek	k1gInPc2	statek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
za	za	k7c4	za
dané	daný	k2eAgNnSc4d1	dané
období	období	k1gNnSc4	období
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
