<p>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
pramenech	pramen	k1gInPc6	pramen
GDP	GDP	kA	GDP
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Gross	Gross	k1gMnSc1	Gross
Domestic	Domestice	k1gFnPc2	Domestice
Product	Product	k1gMnSc1	Product
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
finální	finální	k2eAgFnSc1d1	finální
celková	celkový	k2eAgFnSc1d1	celková
peněžní	peněžní	k2eAgFnSc1d1	peněžní
hodnota	hodnota	k1gFnSc1	hodnota
statků	statek	k1gInPc2	statek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
za	za	k7c4	za
dané	daný	k2eAgNnSc4d1	dané
období	období	k1gNnSc4	období
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ukazatel	ukazatel	k1gInSc1	ukazatel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
makroekonomii	makroekonomie	k1gFnSc6	makroekonomie
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
výkonnosti	výkonnost	k1gFnSc2	výkonnost
ekonomiky	ekonomika	k1gFnSc2	ekonomika
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Časovým	časový	k2eAgNnSc7d1	časové
obdobím	období	k1gNnSc7	období
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
srovnáních	srovnání	k1gNnPc6	srovnání
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
toková	tokový	k2eAgFnSc1d1	toková
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
penězích	peníze	k1gInPc6	peníze
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
produkt	produkt	k1gInSc4	produkt
přírůstek	přírůstek	k1gInSc4	přírůstek
bohatství	bohatství	k1gNnPc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
analýz	analýza	k1gFnPc2	analýza
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
HDP	HDP	kA	HDP
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
vyjádřením	vyjádření	k1gNnSc7	vyjádření
míry	míra	k1gFnSc2	míra
spotřeby	spotřeba	k1gFnSc2	spotřeba
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
produktu	produkt	k1gInSc2	produkt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
ukazatelem	ukazatel	k1gInSc7	ukazatel
vývoje	vývoj	k1gInSc2	vývoj
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
výkonnost	výkonnost	k1gFnSc4	výkonnost
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ukazatel	ukazatel	k1gInSc4	ukazatel
shrnující	shrnující	k2eAgFnSc2d1	shrnující
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odhadu	odhad	k1gInSc3	odhad
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
rozvoje	rozvoj	k1gInSc2	rozvoj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
ve	v	k7c6	v
finančním	finanční	k2eAgNnSc6d1	finanční
vyjádření	vyjádření	k1gNnSc6	vyjádření
představuje	představovat	k5eAaImIp3nS	představovat
hodnotu	hodnota	k1gFnSc4	hodnota
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
za	za	k7c4	za
sledované	sledovaný	k2eAgNnSc4d1	sledované
období	období	k1gNnSc4	období
–	–	k?	–
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
čtvrtletí	čtvrtletí	k1gNnSc4	čtvrtletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočet	výpočet	k1gInSc1	výpočet
HDP	HDP	kA	HDP
==	==	k?	==
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
lze	lze	k6eAd1	lze
standardně	standardně	k6eAd1	standardně
vypočítat	vypočítat	k5eAaPmF	vypočítat
třemi	tři	k4xCgInPc7	tři
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
metoda	metoda	k1gFnSc1	metoda
představuje	představovat	k5eAaImIp3nS	představovat
jiný	jiný	k2eAgInSc4d1	jiný
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
započítávají	započítávat	k5eAaImIp3nP	započítávat
různá	různý	k2eAgNnPc4d1	různé
hlediska	hledisko	k1gNnPc4	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
však	však	k9	však
obtížně	obtížně	k6eAd1	obtížně
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
odhady	odhad	k1gInPc4	odhad
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
věci	věc	k1gFnPc4	věc
"	"	kIx"	"
<g/>
zdarma	zdarma	k6eAd1	zdarma
<g/>
"	"	kIx"	"
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
opomíjena	opomíjet	k5eAaImNgFnS	opomíjet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
ekonomických	ekonomický	k2eAgNnPc6d1	ekonomické
srovnáních	srovnání	k1gNnPc6	srovnání
používá	používat	k5eAaImIp3nS	používat
nejčastěji	často	k6eAd3	často
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výrobková	výrobkový	k2eAgFnSc1d1	výrobková
metoda	metoda	k1gFnSc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
též	též	k9	též
zbožová	zbožový	k2eAgFnSc1d1	zbožová
nebo	nebo	k8xC	nebo
produkční	produkční	k2eAgFnSc1d1	produkční
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
hrubé	hrubý	k2eAgFnSc2d1	hrubá
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
institucionálních	institucionální	k2eAgInPc2d1	institucionální
sektorů	sektor	k1gInPc2	sektor
nebo	nebo	k8xC	nebo
odvětví	odvětví	k1gNnPc2	odvětví
a	a	k8xC	a
čistých	čistý	k2eAgFnPc2d1	čistá
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
rozvrženy	rozvrhnout	k5eAaPmNgFnP	rozvrhnout
do	do	k7c2	do
sektorů	sektor	k1gInPc2	sektor
a	a	k8xC	a
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
vyrovnávací	vyrovnávací	k2eAgFnSc1d1	vyrovnávací
položka	položka	k1gFnSc1	položka
účtu	účet	k1gInSc2	účet
výroby	výroba	k1gFnSc2	výroba
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
hospodářství	hospodářství	k1gNnSc4	hospodářství
celkem	celkem	k6eAd1	celkem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
zdrojů	zdroj	k1gInPc2	zdroj
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
užití	užití	k1gNnPc2	užití
mezispotřeba	mezispotřeb	k1gMnSc2	mezispotřeb
<g/>
.	.	kIx.	.
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
přidaná	přidaný	k2eAgFnSc1d1	přidaná
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
produkcí	produkce	k1gFnSc7	produkce
a	a	k8xC	a
mezispotřebou	mezispotřeba	k1gFnSc7	mezispotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
cenách	cena	k1gFnPc6	cena
a	a	k8xC	a
užití	užití	k1gNnSc4	užití
v	v	k7c6	v
kupních	kupní	k2eAgFnPc6d1	kupní
cenách	cena	k1gFnPc6	cena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
strana	strana	k1gFnSc1	strana
zdrojů	zdroj	k1gInPc2	zdroj
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
hospodářství	hospodářství	k1gNnSc4	hospodářství
celkem	celkem	k6eAd1	celkem
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
daně	daň	k1gFnPc4	daň
snížené	snížený	k2eAgFnPc4d1	snížená
o	o	k7c6	o
dotace	dotace	k1gFnPc4	dotace
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
=	=	kIx~	=
Produkce	produkce	k1gFnSc1	produkce
minus	minus	k1gNnSc1	minus
Mezispotřeba	Mezispotřeb	k1gMnSc2	Mezispotřeb
plus	plus	k1gNnSc2	plus
Daně	daň	k1gFnPc1	daň
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
minus	minus	k1gInSc4	minus
Dotace	dotace	k1gFnSc2	dotace
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
</s>
</p>
<p>
<s>
Výrobce	výrobce	k1gMnSc1	výrobce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nakoupí	nakoupit	k5eAaPmIp3nS	nakoupit
materiál	materiál	k1gInSc4	materiál
za	za	k7c4	za
1000	[number]	k4	1000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
pracovníkům	pracovník	k1gMnPc3	pracovník
800	[number]	k4	800
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
energie	energie	k1gFnSc1	energie
za	za	k7c4	za
200	[number]	k4	200
Kč	Kč	kA	Kč
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
výrobek	výrobek	k1gInSc1	výrobek
prodá	prodat	k5eAaPmIp3nS	prodat
za	za	k7c4	za
5000	[number]	k4	5000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
přispěje	přispět	k5eAaPmIp3nS	přispět
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
produktu	produkt	k1gInSc2	produkt
částkou	částka	k1gFnSc7	částka
3000	[number]	k4	3000
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
5000	[number]	k4	5000
−	−	k?	−
1000	[number]	k4	1000
−	−	k?	−
800	[number]	k4	800
−	−	k?	−
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obchodník	obchodník	k1gMnSc1	obchodník
nakoupí	nakoupit	k5eAaPmIp3nS	nakoupit
tento	tento	k3xDgInSc4	tento
výrobek	výrobek	k1gInSc4	výrobek
za	za	k7c4	za
5000	[number]	k4	5000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
prodá	prodat	k5eAaPmIp3nS	prodat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
7500	[number]	k4	7500
Kč	Kč	kA	Kč
a	a	k8xC	a
nespotřebuje	spotřebovat	k5eNaPmIp3nS	spotřebovat
žádný	žádný	k3yNgInSc4	žádný
další	další	k2eAgInSc4d1	další
nakupovaný	nakupovaný	k2eAgInSc4d1	nakupovaný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
energii	energie	k1gFnSc4	energie
nebo	nebo	k8xC	nebo
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
přispěje	přispět	k5eAaPmIp3nS	přispět
tak	tak	k6eAd1	tak
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
produktu	produkt	k1gInSc2	produkt
částkou	částka	k1gFnSc7	částka
2500	[number]	k4	2500
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
duplicity	duplicita	k1gFnPc4	duplicita
(	(	kIx(	(
<g/>
meziprodukty	meziprodukt	k1gInPc4	meziprodukt
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
bohužel	bohužel	k9	bohužel
těžko	těžko	k6eAd1	těžko
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výdajová	výdajový	k2eAgFnSc1d1	výdajová
metoda	metoda	k1gFnSc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
výdaj	výdaj	k1gInSc1	výdaj
za	za	k7c4	za
zboží	zboží	k1gNnSc4	zboží
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
ceně	cena	k1gFnSc3	cena
tohoto	tento	k3xDgNnSc2	tento
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
součtu	součet	k1gInSc2	součet
přidaných	přidaný	k2eAgFnPc2d1	přidaná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
produkt	produkt	k1gInSc4	produkt
měřit	měřit	k5eAaImF	měřit
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
výdajů	výdaj	k1gInPc2	výdaj
–	–	k?	–
agregátní	agregátní	k2eAgInPc4d1	agregátní
výdaje	výdaj	k1gInPc4	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
součtu	součet	k1gInSc2	součet
započítáváme	započítávat	k5eAaImIp1nP	započítávat
pouze	pouze	k6eAd1	pouze
finální	finální	k2eAgInPc1d1	finální
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
takové	takový	k3xDgInPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
vstupem	vstup	k1gInSc7	vstup
další	další	k2eAgFnSc2d1	další
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
nebudou	být	k5eNaImBp3nP	být
v	v	k7c6	v
přidané	přidaný	k2eAgFnSc6d1	přidaná
hodnotě	hodnota	k1gFnSc6	hodnota
jiného	jiný	k2eAgNnSc2d1	jiné
zboží	zboží	k1gNnSc2	zboží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgInPc4d1	osobní
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
spotřebu	spotřeba	k1gFnSc4	spotřeba
(	(	kIx(	(
<g/>
statky	statek	k1gInPc1	statek
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
užití	užití	k1gNnSc2	užití
<g/>
,	,	kIx,	,
statky	statek	k1gInPc1	statek
krátkodobého	krátkodobý	k2eAgNnSc2d1	krátkodobé
užití	užití	k1gNnSc2	užití
<g/>
,	,	kIx,	,
služby	služba	k1gFnSc2	služba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hrubé	hrubý	k2eAgFnPc1d1	hrubá
soukromé	soukromý	k2eAgFnPc1d1	soukromá
domácí	domácí	k2eAgFnPc1d1	domácí
investice	investice	k1gFnPc1	investice
(	(	kIx(	(
<g/>
fixní	fixní	k2eAgFnPc1d1	fixní
investice	investice	k1gFnPc1	investice
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc4	změna
stavu	stav	k1gInSc2	stav
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
fixní	fixní	k2eAgFnPc4d1	fixní
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
bytové	bytový	k2eAgFnSc2d1	bytová
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vládní	vládní	k2eAgInPc1d1	vládní
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
statků	statek	k1gInPc2	statek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
</s>
</p>
<p>
<s>
saldo	saldo	k1gNnSc1	saldo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
vývozů	vývoz	k1gInPc2	vývoz
–	–	k?	–
hodnota	hodnota	k1gFnSc1	hodnota
dovozů	dovoz	k1gInPc2	dovoz
<g/>
)	)	kIx)	)
<g/>
Agregátní	agregátní	k2eAgInPc4d1	agregátní
výdaje	výdaj	k1gInPc4	výdaj
=	=	kIx~	=
spotřeba	spotřeba	k1gFnSc1	spotřeba
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
+	+	kIx~	+
hrubé	hrubý	k2eAgFnPc4d1	hrubá
investice	investice	k1gFnPc4	investice
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
)	)	kIx)	)
+	+	kIx~	+
veřejné	veřejný	k2eAgInPc1d1	veřejný
výdaje	výdaj	k1gInPc1	výdaj
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
+	+	kIx~	+
čistý	čistý	k2eAgInSc1d1	čistý
vývoz	vývoz	k1gInSc1	vývoz
(	(	kIx(	(
<g/>
NX	NX	kA	NX
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Důchodová	důchodový	k2eAgFnSc1d1	důchodová
metoda	metoda	k1gFnSc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
každý	každý	k3xTgInSc1	každý
výdaj	výdaj	k1gInSc1	výdaj
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
je	být	k5eAaImIp3nS	být
něčím	něčí	k3xOyIgInSc7	něčí
důchodem	důchod	k1gInSc7	důchod
(	(	kIx(	(
<g/>
příjmem	příjem	k1gInSc7	příjem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
produkt	produkt	k1gInSc4	produkt
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
součet	součet	k1gInSc1	součet
důchodů	důchod	k1gInPc2	důchod
(	(	kIx(	(
<g/>
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
daně	daň	k1gFnPc1	daň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
prvotních	prvotní	k2eAgInPc2d1	prvotní
důchodů	důchod	k1gInPc2	důchod
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
hospodářství	hospodářství	k1gNnSc4	hospodářství
celkem	celkem	k6eAd1	celkem
<g/>
:	:	kIx,	:
náhrad	náhrada	k1gFnPc2	náhrada
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
<g/>
,	,	kIx,	,
daní	danit	k5eAaImIp3nS	danit
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
snížených	snížený	k2eAgFnPc2d1	snížená
o	o	k7c4	o
dotace	dotace	k1gFnPc4	dotace
a	a	k8xC	a
hrubého	hrubý	k2eAgInSc2d1	hrubý
provozního	provozní	k2eAgInSc2d1	provozní
přebytku	přebytek	k1gInSc2	přebytek
a	a	k8xC	a
smíšeného	smíšený	k2eAgInSc2d1	smíšený
důchodu	důchod	k1gInSc2	důchod
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
čistého	čistý	k2eAgInSc2d1	čistý
provozního	provozní	k2eAgInSc2d1	provozní
přebytku	přebytek	k1gInSc2	přebytek
a	a	k8xC	a
smíšeného	smíšený	k2eAgInSc2d1	smíšený
důchodu	důchod	k1gInSc2	důchod
a	a	k8xC	a
spotřeby	spotřeba	k1gFnSc2	spotřeba
fixního	fixní	k2eAgInSc2d1	fixní
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
=	=	kIx~	=
Náhrady	náhrada	k1gFnSc2	náhrada
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
+	+	kIx~	+
Daně	daň	k1gFnPc1	daň
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
-	-	kIx~	-
Dotace	dotace	k1gFnSc1	dotace
+	+	kIx~	+
Čistý	čistý	k2eAgInSc1d1	čistý
provozní	provozní	k2eAgInSc1d1	provozní
přebytek	přebytek	k1gInSc1	přebytek
+	+	kIx~	+
Čistý	čistý	k2eAgInSc1d1	čistý
smíšený	smíšený	k2eAgInSc1d1	smíšený
důchod	důchod	k1gInSc1	důchod
+	+	kIx~	+
Spotřeba	spotřeba	k1gFnSc1	spotřeba
fixního	fixní	k2eAgInSc2d1	fixní
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
viz	vidět	k5eAaImRp2nS	vidět
Národní	národní	k2eAgFnSc2d1	národní
důchod	důchod	k1gInSc4	důchod
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc1	variant
produktu	produkt	k1gInSc2	produkt
==	==	k?	==
</s>
</p>
<p>
<s>
čistý	čistý	k2eAgInSc1d1	čistý
produkt	produkt	k1gInSc1	produkt
=	=	kIx~	=
hrubý	hrubý	k2eAgInSc1d1	hrubý
produkt	produkt	k1gInSc1	produkt
−	−	k?	−
opotřebení	opotřebení	k1gNnSc2	opotřebení
kapitáluPřívlastek	kapitáluPřívlastka	k1gFnPc2	kapitáluPřívlastka
domácí	domácí	k1gMnPc1	domácí
<g/>
/	/	kIx~	/
<g/>
národní	národní	k2eAgInSc4d1	národní
produkt	produkt	k1gInSc4	produkt
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
produktem	produkt	k1gInSc7	produkt
vytvořeným	vytvořený	k2eAgInSc7d1	vytvořený
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
<g/>
)	)	kIx)	)
a	a	k8xC	a
produktem	produkt	k1gInSc7	produkt
vytvořeným	vytvořený	k2eAgInSc7d1	vytvořený
výrobními	výrobní	k2eAgNnPc7d1	výrobní
faktory	faktor	k1gInPc1	faktor
příslušníky	příslušník	k1gMnPc7	příslušník
určitého	určitý	k2eAgInSc2d1	určitý
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
=	=	kIx~	=
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
+	+	kIx~	+
produkt	produkt	k1gInSc1	produkt
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
−	−	k?	−
produkt	produkt	k1gInSc1	produkt
cizích	cizí	k2eAgFnPc2d1	cizí
firem	firma	k1gFnPc2	firma
v	v	k7c6	v
ČRPorovnáváme	ČRPorovnáváme	k1gFnSc6	ČRPorovnáváme
<g/>
-li	i	k?	-li
údaje	údaj	k1gInSc2	údaj
HDP	HDP	kA	HDP
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
změnu	změna	k1gFnSc4	změna
hodnoty	hodnota	k1gFnSc2	hodnota
peněz	peníze	k1gInPc2	peníze
(	(	kIx(	(
<g/>
inflaci	inflace	k1gFnSc6	inflace
případně	případně	k6eAd1	případně
deflaci	deflace	k1gFnSc4	deflace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reálný	reálný	k2eAgInSc1d1	reálný
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
očištěný	očištěný	k2eAgMnSc1d1	očištěný
o	o	k7c4	o
inflaci	inflace	k1gFnSc4	inflace
<g/>
)	)	kIx)	)
získáme	získat	k5eAaPmIp1nP	získat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nominální	nominální	k2eAgInSc1d1	nominální
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
počítaný	počítaný	k2eAgInSc1d1	počítaný
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
cenách	cena	k1gFnPc6	cena
<g/>
)	)	kIx)	)
vydělíme	vydělit	k5eAaPmIp1nP	vydělit
hodnotou	hodnota	k1gFnSc7	hodnota
deflátoru	deflátor	k1gInSc2	deflátor
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
cenového	cenový	k2eAgInSc2d1	cenový
indexu	index	k1gInSc2	index
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
změnu	změna	k1gFnSc4	změna
cen	cena	k1gFnPc2	cena
všech	všecek	k3xTgInPc2	všecek
statků	statek	k1gInPc2	statek
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
HDP	HDP	kA	HDP
vs	vs	k?	vs
<g/>
.	.	kIx.	.
bohatství	bohatství	k1gNnSc2	bohatství
==	==	k?	==
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
nutně	nutně	k6eAd1	nutně
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
kvalitu	kvalita	k1gFnSc4	kvalita
života	život	k1gInSc2	život
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
občanů	občan	k1gMnPc2	občan
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
veličinou	veličina	k1gFnSc7	veličina
jak	jak	k8xS	jak
mezi	mezi	k7c7	mezi
ekonomy	ekonom	k1gMnPc7	ekonom
<g/>
,	,	kIx,	,
tak	tak	k9	tak
mezi	mezi	k7c7	mezi
laickou	laický	k2eAgFnSc7d1	laická
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
lze	lze	k6eAd1	lze
elegantně	elegantně	k6eAd1	elegantně
jediným	jediný	k2eAgNnSc7d1	jediné
číslem	číslo	k1gNnSc7	číslo
popsat	popsat	k5eAaPmF	popsat
a	a	k8xC	a
porovnávat	porovnávat	k5eAaImF	porovnávat
stav	stav	k1gInSc4	stav
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
ekonomik	ekonomika	k1gFnPc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
musí	muset	k5eAaImIp3nS	muset
nezbytně	nezbytně	k6eAd1	nezbytně
narážet	narážet	k5eAaPmF	narážet
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
HDP	HDP	kA	HDP
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
bohatství	bohatství	k1gNnSc2	bohatství
občanů	občan	k1gMnPc2	občan
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
drobným	drobný	k2eAgNnSc7d1	drobné
rozdílům	rozdíl	k1gInPc3	rozdíl
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přikládat	přikládat	k5eAaImF	přikládat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
nahlížet	nahlížet	k5eAaImF	nahlížet
na	na	k7c4	na
meziroční	meziroční	k2eAgInSc4d1	meziroční
růst	růst	k1gInSc4	růst
či	či	k8xC	či
pokles	pokles	k1gInSc4	pokles
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
koncentrace	koncentrace	k1gFnPc4	koncentrace
na	na	k7c4	na
desetiny	desetina	k1gFnPc4	desetina
procent	procento	k1gNnPc2	procento
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šedá	šedý	k2eAgFnSc1d1	šedá
ekonomika	ekonomika	k1gFnSc1	ekonomika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
HDP	HDP	kA	HDP
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
postihnout	postihnout	k5eAaPmF	postihnout
činnosti	činnost	k1gFnPc4	činnost
šedé	šedý	k2eAgFnSc2d1	šedá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výše	výše	k1gFnSc1	výše
šedé	šedý	k2eAgFnSc2d1	šedá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
u	u	k7c2	u
zemí	zem	k1gFnPc2	zem
OECD	OECD	kA	OECD
14-16	[number]	k4	14-16
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
u	u	k7c2	u
tranzitivních	tranzitivní	k2eAgFnPc2d1	tranzitivní
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
21-30	[number]	k4	21-30
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
u	u	k7c2	u
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
35-44	[number]	k4	35-44
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
dění	dění	k1gNnSc2	dění
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
mimo	mimo	k7c4	mimo
oficiální	oficiální	k2eAgFnSc4d1	oficiální
statistiku	statistika	k1gFnSc4	statistika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nutně	nutně	k6eAd1	nutně
zkreslená	zkreslený	k2eAgFnSc1d1	zkreslená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Domácnosti	domácnost	k1gFnPc1	domácnost
===	===	k?	===
</s>
</p>
<p>
<s>
Činnosti	činnost	k1gFnPc1	činnost
domácností	domácnost	k1gFnPc2	domácnost
typicky	typicky	k6eAd1	typicky
nespadají	spadat	k5eNaPmIp3nP	spadat
do	do	k7c2	do
šedé	šedý	k2eAgFnSc2d1	šedá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
ohledu	ohled	k1gInSc6	ohled
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgFnPc2	takový
činností	činnost	k1gFnPc2	činnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vaření	vaření	k1gNnSc1	vaření
oběda	oběd	k1gInSc2	oběd
nebo	nebo	k8xC	nebo
umytí	umytí	k1gNnSc4	umytí
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
činnosti	činnost	k1gFnPc1	činnost
se	se	k3xPyFc4	se
do	do	k7c2	do
HDP	HDP	kA	HDP
opět	opět	k6eAd1	opět
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bohatství	bohatství	k1gNnSc1	bohatství
obyvatel	obyvatel	k1gMnPc2	obyvatel
nezbytně	nezbytně	k6eAd1	nezbytně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
dělat	dělat	k5eAaImF	dělat
podobné	podobný	k2eAgFnPc4d1	podobná
činnosti	činnost	k1gFnPc4	činnost
sami	sám	k3xTgMnPc1	sám
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
v	v	k7c6	v
HDP	HDP	kA	HDP
uměle	uměle	k6eAd1	uměle
podhodnoceny	podhodnotit	k5eAaPmNgFnP	podhodnotit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepeněžní	peněžní	k2eNgInPc1d1	nepeněžní
faktory	faktor	k1gInPc1	faktor
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zkoumat	zkoumat	k5eAaImF	zkoumat
nepeněžní	peněžní	k2eNgInPc4d1	nepeněžní
faktory	faktor	k1gInPc4	faktor
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc4d1	dobré
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
kriminalita	kriminalita	k1gFnSc1	kriminalita
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc1d1	fungující
instituce	instituce	k1gFnSc1	instituce
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
faktory	faktor	k1gInPc4	faktor
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ani	ani	k8xC	ani
aproximovat	aproximovat	k5eAaBmF	aproximovat
peněžně	peněžně	k6eAd1	peněžně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
HDP	HDP	kA	HDP
skryty	skrýt	k5eAaPmNgInP	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
zcela	zcela	k6eAd1	zcela
evidentně	evidentně	k6eAd1	evidentně
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
bohatství	bohatství	k1gNnSc2	bohatství
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
například	například	k6eAd1	například
při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
HDP	HDP	kA	HDP
pak	pak	k6eAd1	pak
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
znamenat	znamenat	k5eAaImF	znamenat
méně	málo	k6eAd2	málo
bohatou	bohatý	k2eAgFnSc4d1	bohatá
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
investice	investice	k1gFnSc1	investice
do	do	k7c2	do
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
HDP	HDP	kA	HDP
<g/>
[	[	kIx(	[
<g/>
Jak	jak	k6eAd1	jak
<g/>
?	?	kIx.	?
</s>
<s>
Přidat	přidat	k5eAaPmF	přidat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nehmotné	hmotný	k2eNgNnSc1d1	nehmotné
bohatství	bohatství	k1gNnSc1	bohatství
obyvatel	obyvatel	k1gMnPc2	obyvatel
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc1	cena
vs	vs	k?	vs
<g/>
.	.	kIx.	.
užitek	užitek	k1gInSc1	užitek
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
HDP	HDP	kA	HDP
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
součet	součet	k1gInSc4	součet
cen	cena	k1gFnPc2	cena
všech	všecek	k3xTgInPc2	všecek
vyprodukovaných	vyprodukovaný	k2eAgInPc2d1	vyprodukovaný
výrobků	výrobek	k1gInPc2	výrobek
za	za	k7c4	za
dané	daný	k2eAgNnSc4d1	dané
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
nicméně	nicméně	k8xC	nicméně
nevyjadřují	vyjadřovat	k5eNaImIp3nP	vyjadřovat
užitek	užitek	k1gInSc4	užitek
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
statků	statek	k1gInPc2	statek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ani	ani	k9	ani
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
ordinalitě	ordinalita	k1gFnSc3	ordinalita
sčítat	sčítat	k5eAaImF	sčítat
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
bohatství	bohatství	k1gNnSc2	bohatství
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
užitku	užitek	k1gInSc2	užitek
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
vůbec	vůbec	k9	vůbec
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
HDP	HDP	kA	HDP
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
činilo	činit	k5eAaImAgNnS	činit
světové	světový	k2eAgNnSc4d1	světové
HDP	HDP	kA	HDP
6,595	[number]	k4	6,595
<g/>
×	×	k?	×
<g/>
1013	[number]	k4	1013
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
s	s	k7c7	s
přírůstkem	přírůstek	k1gInSc7	přírůstek
5,3	[number]	k4	5,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
činilo	činit	k5eAaImAgNnS	činit
10	[number]	k4	10
200	[number]	k4	200
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
64	[number]	k4	64
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
HDP	HDP	kA	HDP
činily	činit	k5eAaImAgInP	činit
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
terciární	terciární	k2eAgInSc1d1	terciární
sektor	sektor	k1gInSc1	sektor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgInSc1d1	sekundární
sektor	sektor	k1gInSc1	sektor
<g/>
)	)	kIx)	)
a	a	k8xC	a
pouhá	pouhý	k2eAgFnSc1d1	pouhá
4	[number]	k4	4
%	%	kIx~	%
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgInS	začít
Druhý	druhý	k4xOgInSc4	druhý
svět	svět	k1gInSc4	svět
v	v	k7c6	v
HDP	HDP	kA	HDP
konvergovat	konvergovat	k5eAaImF	konvergovat
k	k	k7c3	k
Západnímu	západní	k2eAgInSc3d1	západní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
</s>
</p>
<p>
<s>
Hrubé	Hrubé	k2eAgNnSc1d1	Hrubé
národní	národní	k2eAgNnSc1d1	národní
štěstí	štěstí	k1gNnSc1	štěstí
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Makroekonomická	makroekonomický	k2eAgFnSc1d1	makroekonomická
predikce	predikce	k1gFnSc1	predikce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
</s>
</p>
<p>
<s>
Metodika	metodika	k1gFnSc1	metodika
výpočtu	výpočet	k1gInSc2	výpočet
HDP	HDP	kA	HDP
dle	dle	k7c2	dle
ČSÚ	ČSÚ	kA	ČSÚ
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnSc1	databáze
ročních	roční	k2eAgInPc2d1	roční
národních	národní	k2eAgInPc2d1	národní
účtů	účet	k1gInPc2	účet
na	na	k7c6	na
webu	web	k1gInSc6	web
ČSÚ	ČSÚ	kA	ČSÚ
</s>
</p>
<p>
<s>
Kurzy	kurz	k1gInPc1	kurz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
HDP	HDP	kA	HDP
Přehled	přehled	k1gInSc4	přehled
vývoje	vývoj	k1gInSc2	vývoj
HDP	HDP	kA	HDP
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Eurostat	Eurostat	k1gInSc1	Eurostat
Regionální	regionální	k2eAgInSc1d1	regionální
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
PPP	PPP	kA	PPP
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
trhu	trh	k1gInSc2	trh
<g/>
:	:	kIx,	:
HDP	HDP	kA	HDP
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
důchodové	důchodový	k2eAgFnSc2d1	důchodová
metody	metoda	k1gFnSc2	metoda
ČR	ČR	kA	ČR
-	-	kIx~	-
Bluenomics	Bluenomics	k1gInSc1	Bluenomics
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
výdajové	výdajový	k2eAgFnSc2d1	výdajová
metody	metoda	k1gFnSc2	metoda
ČR	ČR	kA	ČR
-	-	kIx~	-
Bluenomics	Bluenomics	k1gInSc1	Bluenomics
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
výrobkové	výrobkový	k2eAgFnSc2d1	výrobková
metody	metoda	k1gFnSc2	metoda
ČR	ČR	kA	ČR
-	-	kIx~	-
Bluenomics	Bluenomics	k1gInSc1	Bluenomics
</s>
</p>
