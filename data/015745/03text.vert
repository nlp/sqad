<s>
John	John	k1gMnSc1
Hannah	Hannah	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Hannah	Hannah	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
John	John	k1gMnSc1
David	David	k1gMnSc1
Hannah	Hannah	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1962	#num#	k4
(	(	kIx(
<g/>
59	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
East	East	k1gInSc1
Kilbride	Kilbrid	k1gInSc5
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc6
</s>
<s>
Claremont	Claremont	k1gMnSc1
High	High	k1gMnSc1
SchoolRoyal	SchoolRoyal	k1gMnSc1
Conservatoire	Conservatoir	k1gInSc5
of	of	k?
Scotland	Scotlando	k1gNnPc2
Povolání	povolání	k1gNnPc2
</s>
<s>
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
producent	producent	k1gMnSc1
a	a	k8xC
televizní	televizní	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Joanna	Joanna	k1gFnSc1
Roth	Roth	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
David	David	k1gMnSc1
Hannah	Hannah	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1962	#num#	k4
<g/>
,	,	kIx,
East	East	k1gInSc1
Kilbride	Kilbrid	k1gInSc5
<g/>
,	,	kIx,
Skotsko	Skotsko	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
Joannou	Joanný	k2eAgFnSc7d1
Roth	Roth	k1gMnSc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
dvojčata	dvojče	k1gNnPc1
–	–	k?
chlapec	chlapec	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
a	a	k8xC
dívka	dívka	k1gFnSc1
Astrid	Astrida	k1gFnPc2
(	(	kIx(
<g/>
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
prosté	prostý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
ze	z	k7c2
tří	tři	k4xCgFnPc2
dětí	dítě	k1gFnPc2
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
starší	starý	k2eAgFnPc4d2
sestry	sestra	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
nástrojař	nástrojař	k1gMnSc1
a	a	k8xC
matka	matka	k1gFnSc1
uklízečka	uklízečka	k1gFnSc1
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
původně	původně	k6eAd1
vyučený	vyučený	k2eAgMnSc1d1
elektrikář	elektrikář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyučení	vyučení	k1gNnSc6
nicméně	nicméně	k8xC
vystudoval	vystudovat	k5eAaPmAgMnS
herectví	herectví	k1gNnSc4
na	na	k7c6
Královské	královský	k2eAgFnSc6d1
skotské	skotská	k1gFnSc6
hudební	hudební	k2eAgFnSc6d1
a	a	k8xC
dramatické	dramatický	k2eAgFnSc3d1
akademii	akademie	k1gFnSc3
v	v	k7c6
Glasgowě	Glasgow	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
televizi	televize	k1gFnSc6
účinkuje	účinkovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
audiovizuálních	audiovizuální	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
významnější	významný	k2eAgFnSc4d2
přelomovou	přelomový	k2eAgFnSc4d1
roli	role	k1gFnSc4
získal	získat	k5eAaPmAgMnS
ve	v	k7c6
známém	známý	k2eAgInSc6d1
britském	britský	k2eAgInSc6d1
snímku	snímek	k1gInSc6
Čtyři	čtyři	k4xCgFnPc1
svatby	svatba	k1gFnPc1
a	a	k8xC
jeden	jeden	k4xCgInSc4
pohřeb	pohřeb	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
významnou	významný	k2eAgFnSc4d1
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
v	v	k7c6
britsko-americkém	britsko-americký	k2eAgInSc6d1
romantickém	romantický	k2eAgInSc6d1
snímku	snímek	k1gInSc6
Srdcová	srdcový	k2eAgFnSc1d1
sedma	sedma	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
(	(	kIx(
<g/>
snímek	snímek	k1gInSc1
pomáhal	pomáhat	k5eAaImAgInS
i	i	k9
spolufinancovat	spolufinancovat	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
následovaly	následovat	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
výrazné	výrazný	k2eAgFnPc1d1
a	a	k8xC
významné	významný	k2eAgFnPc1d1
role	role	k1gFnPc1
ve	v	k7c6
snímcích	snímek	k1gInPc6
Hurikán	hurikán	k1gInSc4
v	v	k7c6
ringu	ring	k1gInSc6
a	a	k8xC
Mumie	mumie	k1gFnSc1
<g/>
,	,	kIx,
seriál	seriál	k1gInSc1
Spartakus	Spartakus	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
...	...	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
John	John	k1gMnSc1
Hannah	Hannaha	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
John	John	k1gMnSc1
Hannah	Hannah	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
John	John	k1gMnSc1
Hannah	Hannah	k1gMnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
151612	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1011656817	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1946	#num#	k4
2107	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
99003657	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
5132154	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
99003657	#num#	k4
</s>
