<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Veškrnová	Veškrnová	k1gFnSc1	Veškrnová
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
manželkou	manželka	k1gFnSc7	manželka
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
a	a	k8xC	a
první	první	k4xOgFnSc7	první
dámou	dáma	k1gFnSc7	dáma
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
používá	používat	k5eAaImIp3nS	používat
jméno	jméno	k1gNnSc4	jméno
Dagmar	Dagmar	k1gFnSc2	Dagmar
Havlová-Veškrnová	Havlová-Veškrnová	k1gFnSc1	Havlová-Veškrnová
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Karla	Karel	k1gMnSc2	Karel
Veškrny	Veškrna	k1gFnSc2	Veškrna
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Markéty	Markéta	k1gFnSc2	Markéta
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1922	[number]	k4	1922
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
v	v	k7c6	v
Brně-Židenicích	Brně-Židenice	k1gFnPc6	Brně-Židenice
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
strýcem	strýc	k1gMnSc7	strýc
byl	být	k5eAaImAgMnS	být
kněz	kněz	k1gMnSc1	kněz
podzemní	podzemní	k2eAgFnSc2d1	podzemní
církve	církev	k1gFnSc2	církev
Vladimír	Vladimír	k1gMnSc1	Vladimír
Julián	Julián	k1gMnSc1	Julián
Veškrna	Veškrna	k1gFnSc1	Veškrna
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1925	[number]	k4	1925
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Husovice	Husovice	k1gFnPc1	Husovice
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
-	-	kIx~	-
Suché	Suché	k2eAgFnSc3d1	Suché
Vrbné	Vrbná	k1gFnSc3	Vrbná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
ukončila	ukončit	k5eAaPmAgFnS	ukončit
maturitou	maturita	k1gFnSc7	maturita
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
DiS.	DiS.	k1gFnSc2	DiS.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Janáčkovu	Janáčkův	k2eAgFnSc4d1	Janáčkova
akademii	akademie	k1gFnSc4	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
MgA.	MgA.	k1gFnSc2	MgA.
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
byla	být	k5eAaImAgFnS	být
stálou	stálý	k2eAgFnSc7d1	stálá
členkou	členka	k1gFnSc7	členka
Divadla	divadlo	k1gNnSc2	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
a	a	k8xC	a
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
také	také	k9	také
v	v	k7c6	v
Semaforu	Semafor	k1gInSc6	Semafor
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
signatářů	signatář	k1gMnPc2	signatář
prohlášení	prohlášení	k1gNnSc2	prohlášení
Anticharty	anticharta	k1gFnSc2	anticharta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
přerušila	přerušit	k5eAaPmAgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
po	po	k7c6	po
11	[number]	k4	11
<g/>
leté	letý	k2eAgFnSc6d1	letá
pauze	pauza	k1gFnSc6	pauza
opět	opět	k6eAd1	opět
navrátila	navrátit	k5eAaPmAgFnS	navrátit
na	na	k7c4	na
divadelní	divadelní	k2eAgFnSc4d1	divadelní
scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Israele	Israel	k1gInSc2	Israel
Horovitze	Horovitze	k1gFnSc1	Horovitze
"	"	kIx"	"
<g/>
Chvíle	chvíle	k1gFnSc1	chvíle
pravdy	pravda	k1gFnSc2	pravda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
stálou	stálý	k2eAgFnSc7d1	stálá
členkou	členka	k1gFnSc7	členka
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
nejvýraznější	výrazný	k2eAgInPc4d3	nejvýraznější
jevištní	jevištní	k2eAgInPc4d1	jevištní
výkony	výkon	k1gInPc4	výkon
patří	patřit	k5eAaImIp3nS	patřit
Jana	Jana	k1gFnSc1	Jana
v	v	k7c6	v
Anouilhově	Anouilhova	k1gFnSc6	Anouilhova
Skřivánkovi	Skřivánkův	k2eAgMnPc1d1	Skřivánkův
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
v	v	k7c6	v
Shakespearově	Shakespearův	k2eAgNnSc6d1	Shakespearovo
Zkrocení	zkrocení	k1gNnSc6	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
Lady	Lada	k1gFnSc2	Lada
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnPc4d1	titulní
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
Ionescově	Ionescův	k2eAgMnSc6d1	Ionescův
Macbethovi	Macbeth	k1gMnSc6	Macbeth
<g/>
,	,	kIx,	,
Gazdině	gazdina	k1gFnSc6	gazdina
robě	roba	k1gFnSc6	roba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Strindbergově	Strindbergův	k2eAgFnSc6d1	Strindbergova
Královně	královna	k1gFnSc6	královna
Kristině	Kristin	k2eAgFnSc6d1	Kristina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Horovitzově	Horovitzův	k2eAgFnSc6d1	Horovitzův
Chvíli	chvíle	k1gFnSc6	chvíle
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
také	také	k9	také
jako	jako	k9	jako
Ibsenova	Ibsenův	k2eAgFnSc1d1	Ibsenova
Rebeka	Rebeka	k1gFnSc1	Rebeka
nebo	nebo	k8xC	nebo
Baronka	baronka	k1gFnSc1	baronka
Castelliová	Castelliový	k2eAgFnSc1d1	Castelliový
v	v	k7c6	v
Krležově	Krležův	k2eAgInSc6d1	Krležův
Rodu	rod	k1gInSc6	rod
Glembayů	Glembay	k1gInPc2	Glembay
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
celovečerní	celovečerní	k2eAgFnSc6d1	celovečerní
veselohře	veselohra	k1gFnSc6	veselohra
Juraje	Juraj	k1gInSc2	Juraj
Herze	Herze	k1gFnSc2	Herze
Holky	holka	k1gFnPc1	holka
z	z	k7c2	z
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
komediální	komediální	k2eAgFnPc4d1	komediální
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
natočila	natočit	k5eAaBmAgFnS	natočit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
filmů	film	k1gInPc2	film
a	a	k8xC	a
250	[number]	k4	250
televizních	televizní	k2eAgFnPc2d1	televizní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
komedie	komedie	k1gFnSc1	komedie
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Odcházení	odcházení	k1gNnSc2	odcházení
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
také	také	k9	také
televize	televize	k1gFnSc1	televize
uvedla	uvést	k5eAaPmAgFnS	uvést
pokračování	pokračování	k1gNnSc4	pokračování
legendárního	legendární	k2eAgInSc2d1	legendární
seriálu	seriál	k1gInSc2	seriál
Sanitka	sanitka	k1gFnSc1	sanitka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
na	na	k7c6	na
žižkovské	žižkovský	k2eAgFnSc6d1	Žižkovská
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
předchozí	předchozí	k2eAgFnSc2d1	předchozí
choti	choť	k1gFnSc2	choť
Olgy	Olga	k1gFnSc2	Olga
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
manžela	manžel	k1gMnSc2	manžel
při	při	k7c6	při
jeho	jeho	k3xOp3gFnPc6	jeho
pracovních	pracovní	k2eAgFnPc6d1	pracovní
povinnostech	povinnost	k1gFnPc6	povinnost
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
charitě	charita	k1gFnSc3	charita
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
založili	založit	k5eAaPmAgMnP	založit
Nadaci	nadace	k1gFnSc4	nadace
Dagmar	Dagmar	k1gFnSc2	Dagmar
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havlových	Havlových	k2eAgFnSc2d1	Havlových
VIZE	vize	k1gFnSc2	vize
97	[number]	k4	97
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
majitelkou	majitelka	k1gFnSc7	majitelka
památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgFnSc2d1	chráněná
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
restaurace	restaurace	k1gFnSc2	restaurace
Terasy	terasa	k1gFnSc2	terasa
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Barrandově	Barrandov	k1gInSc6	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Nemovitost	nemovitost	k1gFnSc1	nemovitost
vložila	vložit	k5eAaPmAgFnS	vložit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
Barrandovské	barrandovský	k2eAgFnSc2d1	Barrandovská
terasy	terasa	k1gFnSc2	terasa
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
prodala	prodat	k5eAaPmAgFnS	prodat
část	část	k1gFnSc1	část
akcií	akcie	k1gFnPc2	akcie
nejmenovanému	jmenovaný	k2eNgMnSc3d1	nejmenovaný
kupci	kupec	k1gMnSc3	kupec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
má	mít	k5eAaImIp3nS	mít
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
dceru	dcera	k1gFnSc4	dcera
Ninu	Nina	k1gFnSc4	Nina
<g/>
,	,	kIx,	,
vdanou	vdaný	k2eAgFnSc4d1	vdaná
Smitovou	Smitová	k1gFnSc4	Smitová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
pražskou	pražský	k2eAgFnSc4d1	Pražská
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
justiční	justiční	k2eAgFnSc7d1	justiční
čekatelkou	čekatelka	k1gFnSc7	čekatelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
soudkyní	soudkyně	k1gFnSc7	soudkyně
Obvodního	obvodní	k2eAgInSc2d1	obvodní
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
založila	založit	k5eAaPmAgFnS	založit
nadaci	nadace	k1gFnSc4	nadace
Vize	vize	k1gFnSc1	vize
97	[number]	k4	97
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
s	s	k7c7	s
nadací	nadace	k1gFnSc7	nadace
Nadací	nadace	k1gFnSc7	nadace
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Olgy	Olga	k1gFnSc2	Olga
Havlových	Havlová	k1gFnPc2	Havlová
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Nadace	nadace	k1gFnSc1	nadace
Dagmar	Dagmar	k1gFnSc2	Dagmar
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havlových	Havlových	k2eAgFnSc2d1	Havlových
VIZE	vize	k1gFnSc2	vize
97	[number]	k4	97
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
nadaci	nadace	k1gFnSc3	nadace
je	být	k5eAaImIp3nS	být
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociální	sociální	k2eAgFnSc2d1	sociální
<g/>
,	,	kIx,	,
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
sedm	sedm	k4xCc4	sedm
nadačních	nadační	k2eAgFnPc2d1	nadační
představení	představení	k1gNnPc2	představení
Královny	královna	k1gFnSc2	královna
Kristiny	Kristina	k1gFnSc2	Kristina
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
věnovala	věnovat	k5eAaPmAgFnS	věnovat
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
domova	domov	k1gInSc2	domov
pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
lidi	člověk	k1gMnPc4	člověk
v	v	k7c6	v
povodněmi	povodeň	k1gFnPc7	povodeň
postižených	postižený	k2eAgFnPc6d1	postižená
oblastech	oblast	k1gFnPc6	oblast
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nadace	nadace	k1gFnSc2	nadace
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
prevence	prevence	k1gFnSc2	prevence
rakoviny	rakovina	k1gFnSc2	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
preventivní	preventivní	k2eAgNnSc1d1	preventivní
vyšetření	vyšetření	k1gNnSc1	vyšetření
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nad	nad	k7c4	nad
50	[number]	k4	50
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
narozenin	narozeniny	k1gFnPc2	narozeniny
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
tradiční	tradiční	k2eAgNnSc4d1	tradiční
předávání	předávání	k1gNnSc4	předávání
Ceny	cena	k1gFnSc2	cena
Nadace	nadace	k1gFnSc2	nadace
VIZE	vize	k1gFnSc2	vize
97	[number]	k4	97
významným	významný	k2eAgFnPc3d1	významná
domácím	domácí	k2eAgFnPc3d1	domácí
i	i	k8xC	i
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Fond	fond	k1gInSc1	fond
porozumění	porozumění	k1gNnSc2	porozumění
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporuje	podporovat	k5eAaImIp3nS	podporovat
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Rodiny	rodina	k1gFnSc2	rodina
pro	pro	k7c4	pro
mentálně	mentálně	k6eAd1	mentálně
postižené	postižený	k1gMnPc4	postižený
a	a	k8xC	a
Domov	domov	k1gInSc4	domov
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
založila	založit	k5eAaPmAgFnS	založit
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
tradiční	tradiční	k2eAgInSc4d1	tradiční
Mikulášský	mikulášský	k2eAgInSc4d1	mikulášský
charitativní	charitativní	k2eAgInSc4d1	charitativní
bazar	bazar	k1gInSc4	bazar
a	a	k8xC	a
přizvala	přizvat	k5eAaPmAgFnS	přizvat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
deset	deset	k4xCc4	deset
významných	významný	k2eAgFnPc2d1	významná
neziskových	ziskový	k2eNgFnPc2d1	nezisková
organizací	organizace	k1gFnPc2	organizace
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Založila	založit	k5eAaPmAgFnS	založit
botanickou	botanický	k2eAgFnSc4d1	botanická
zahradu	zahrada	k1gFnSc4	zahrada
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgFnPc1d1	vzácná
dřeviny	dřevina	k1gFnPc1	dřevina
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
do	do	k7c2	do
arboreta	arboretum	k1gNnSc2	arboretum
významné	významný	k2eAgFnSc2d1	významná
osobnosti	osobnost	k1gFnSc2	osobnost
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
i	i	k9	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
expozici	expozice	k1gFnSc4	expozice
navštívit	navštívit	k5eAaPmF	navštívit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
firmy	firma	k1gFnSc2	firma
Botanicus	Botanicus	k1gInSc1	Botanicus
v	v	k7c4	v
Ostré	ostrý	k2eAgFnPc4d1	ostrá
u	u	k7c2	u
Lysé	Lysá	k1gFnSc2	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
boje	boj	k1gInPc1	boj
proti	proti	k7c3	proti
rasové	rasový	k2eAgFnSc3d1	rasová
nesnášenlivosti	nesnášenlivost	k1gFnSc3	nesnášenlivost
a	a	k8xC	a
diskriminaci	diskriminace	k1gFnSc3	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
uděluje	udělovat	k5eAaImIp3nS	udělovat
pravidelně	pravidelně	k6eAd1	pravidelně
záštity	záštita	k1gFnSc2	záštita
kulturním	kulturní	k2eAgInPc3d1	kulturní
a	a	k8xC	a
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
JAMU	jam	k1gInSc2	jam
1968	[number]	k4	1968
–	–	k?	–
Broučci	Brouček	k1gMnPc1	Brouček
(	(	kIx(	(
<g/>
Verunka	Verunka	k1gFnSc1	Verunka
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
–	–	k?	–
Doktor	doktor	k1gMnSc1	doktor
Causy	causa	k1gFnSc2	causa
(	(	kIx(	(
<g/>
Děvče	děvče	k1gNnSc1	děvče
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
–	–	k?	–
Radúz	Radúz	k1gMnSc1	Radúz
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
(	(	kIx(	(
<g/>
Ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
–	–	k?	–
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
–	–	k?	–
Kotevní	kotevní	k2eAgNnSc1d1	kotevní
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Petrovna	Petrovna	k1gFnSc1	Petrovna
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
–	–	k?	–
První	první	k4xOgInPc4	první
<g />
.	.	kIx.	.
</s>
<s>
den	den	k1gInSc1	den
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Inge	Inge	k1gFnSc1	Inge
<g/>
)	)	kIx)	)
Státní	státní	k2eAgFnSc1d1	státní
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
1970	[number]	k4	1970
–	–	k?	–
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
1974	[number]	k4	1974
–	–	k?	–
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
Soňa	Soňa	k1gFnSc1	Soňa
Čechová	Čechová	k1gFnSc1	Čechová
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
1975	[number]	k4	1975
–	–	k?	–
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Bystrozraký	bystrozraký	k2eAgMnSc1d1	bystrozraký
(	(	kIx(	(
<g/>
zakletá	zakletý	k2eAgFnSc1d1	zakletá
princezna	princezna	k1gFnSc1	princezna
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
–	–	k?	–
Tajemný	tajemný	k2eAgInSc4d1	tajemný
<g />
.	.	kIx.	.
</s>
<s>
doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
(	(	kIx(	(
<g/>
Suzela	Suzela	k1gFnSc1	Suzela
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
–	–	k?	–
Bumbaraš	Bumbaraš	k1gMnSc1	Bumbaraš
aneb	aneb	k?	aneb
Paličák	paličák	k1gMnSc1	paličák
(	(	kIx(	(
<g/>
Varvara	Varvara	k1gFnSc1	Varvara
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
–	–	k?	–
Kterak	kterak	k6eAd1	kterak
se	se	k3xPyFc4	se
o	o	k7c4	o
princeznu	princezna	k1gFnSc4	princezna
čert	čert	k1gMnSc1	čert
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
(	(	kIx(	(
<g/>
princezna	princezna	k1gFnSc1	princezna
Cecilka	Cecilka	k1gFnSc1	Cecilka
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
–	–	k?	–
O	o	k7c6	o
Slunečníku	slunečník	k1gInSc6	slunečník
<g/>
,	,	kIx,	,
Měsíčníku	měsíčník	k1gInSc6	měsíčník
a	a	k8xC	a
Větrníku	větrník	k1gInSc6	větrník
(	(	kIx(	(
<g/>
Černovláska	černovláska	k1gFnSc1	černovláska
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
–	–	k?	–
Ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
princezna	princezna	k1gFnSc1	princezna
Elena	Elena	k1gFnSc1	Elena
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
–	–	k?	–
Zkrocení	zkrocení	k1gNnSc4	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
–	–	k?	–
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
Motýlice	motýlice	k1gFnSc1	motýlice
Iris	iris	k1gFnSc1	iris
/	/	kIx~	/
Chrobačka	chrobačka	k1gFnSc1	chrobačka
/	/	kIx~	/
Zaječice	zaječice	k1gFnSc1	zaječice
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
1979	[number]	k4	1979
–	–	k?	–
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
níže	nízce	k6eAd2	nízce
podepsaní	podepsaný	k1gMnPc1	podepsaný
(	(	kIx(	(
<g/>
Alla	Alla	k1gFnSc1	Alla
Šindinová	Šindinový	k2eAgFnSc1d1	Šindinový
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
–	–	k?	–
Lištičky	lištička	k1gFnSc2	lištička
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
–	–	k?	–
Poslední	poslední	k2eAgInSc4d1	poslední
záblesk	záblesk	k1gInSc4	záblesk
(	(	kIx(	(
<g/>
Colette	Colett	k1gMnSc5	Colett
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
–	–	k?	–
Svatba	svatba	k1gFnSc1	svatba
Krečinského	Krečinský	k2eAgInSc2d1	Krečinský
(	(	kIx(	(
<g/>
Lidočka	Lidočka	k1gFnSc1	Lidočka
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
–	–	k?	–
Sviť	svítit	k5eAaImRp2nS	svítit
<g/>
,	,	kIx,	,
sviť	svítit	k5eAaImRp2nS	svítit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hvězdo	hvězda	k1gFnSc5	hvězda
(	(	kIx(	(
<g/>
Margareta	Margareta	k1gFnSc1	Margareta
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
–	–	k?	–
Jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
přikázání	přikázání	k1gNnSc1	přikázání
(	(	kIx(	(
<g/>
Františka	Františka	k1gFnSc1	Františka
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
–	–	k?	–
Diamantoví	diamantový	k2eAgMnPc1d1	diamantový
<g />
.	.	kIx.	.
</s>
<s>
kluci	kluk	k1gMnPc1	kluk
(	(	kIx(	(
<g/>
Marta	Marta	k1gFnSc1	Marta
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
–	–	k?	–
Dalskabáty	Dalskabáta	k1gFnSc2	Dalskabáta
<g/>
,	,	kIx,	,
hříšná	hříšný	k2eAgFnSc1d1	hříšná
ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
Dorotka	Dorotka	k1gFnSc1	Dorotka
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
–	–	k?	–
Krajnice	krajnice	k1gFnSc2	krajnice
báječných	báječný	k2eAgFnPc2d1	báječná
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
Darina	Darina	k1gFnSc1	Darina
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
–	–	k?	–
Zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
Nastasja	Nastasj	k2eAgFnSc1d1	Nastasja
Filipovna	Filipovna	k1gFnSc1	Filipovna
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
–	–	k?	–
Rafani	Rafaň	k1gFnSc6	Rafaň
(	(	kIx(	(
<g/>
Marta	Marta	k1gFnSc1	Marta
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
–	–	k?	–
Spartakus	Spartakus	k1gMnSc1	Spartakus
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Varinia	Varinium	k1gNnPc1	Varinium
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
–	–	k?	–
Den	den	k1gInSc1	den
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
Ajzada	Ajzada	k1gFnSc1	Ajzada
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
–	–	k?	–
Krysař	krysař	k1gMnSc1	krysař
(	(	kIx(	(
<g/>
Lora	Lora	k1gFnSc1	Lora
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
–	–	k?	–
Jedna	jeden	k4xCgFnSc1	jeden
jaro	jaro	k1gNnSc4	jaro
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Jeanett	Jeanett	k1gInSc1	Jeanett
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
–	–	k?	–
Lov	lov	k1gInSc1	lov
divokých	divoký	k2eAgFnPc2d1	divoká
kachen	kachna	k1gFnPc2	kachna
(	(	kIx(	(
<g/>
Věra	Věra	k1gFnSc1	Věra
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
–	–	k?	–
Taková	takový	k3xDgFnSc1	takový
ženská	ženská	k1gFnSc1	ženská
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
krku	krk	k1gInSc3	krk
(	(	kIx(	(
<g/>
Lucette	Lucett	k1gInSc5	Lucett
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
–	–	k?	–
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
(	(	kIx(	(
<g/>
Hela	Hela	k1gFnSc1	Hela
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
–	–	k?	–
Hlasy	hlas	k1gInPc1	hlas
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
Yveta	Yveta	k1gFnSc1	Yveta
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
–	–	k?	–
Macbett	Macbett	k1gMnSc1	Macbett
(	(	kIx(	(
<g/>
Lady	lady	k1gFnSc1	lady
Macbett	Macbett	k1gMnSc1	Macbett
/	/	kIx~	/
Lady	lady	k1gFnSc1	lady
Duncanová	Duncanová	k1gFnSc1	Duncanová
/	/	kIx~	/
Čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
–	–	k?	–
Gazdina	gazdina	k1gFnSc1	gazdina
roba	roba	k1gFnSc1	roba
(	(	kIx(	(
<g/>
Eva	Eva	k1gFnSc1	Eva
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
krajina	krajina	k1gFnSc1	krajina
širá	širý	k2eAgFnSc1d1	širá
(	(	kIx(	(
<g/>
Adéla	Adéla	k1gFnSc1	Adéla
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
–	–	k?	–
Chvála	chvála	k1gFnSc1	chvála
prostopášnosti	prostopášnost	k1gFnSc2	prostopášnost
(	(	kIx(	(
<g/>
Smolička	Smolička	k1gFnSc1	Smolička
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
–	–	k?	–
Královna	královna	k1gFnSc1	královna
Kristina	Kristina	k1gFnSc1	Kristina
(	(	kIx(	(
<g/>
královna	královna	k1gFnSc1	královna
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
–	–	k?	–
Chvíle	chvíle	k1gFnSc2	chvíle
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
Kathleen	Kathleen	k1gInSc1	Kathleen
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
–	–	k?	–
Višňový	višňový	k2eAgInSc1d1	višňový
sad	sad	k1gInSc1	sad
(	(	kIx(	(
<g/>
Ljubov	Ljubov	k1gInSc1	Ljubov
Raněvská	Raněvský	k2eAgFnSc1d1	Raněvská
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
(	(	kIx(	(
<g/>
skotská	skotský	k2eAgFnSc1d1	skotská
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
–	–	k?	–
Cyrano	Cyrano	k1gMnSc1	Cyrano
<g/>
!!	!!	k?	!!
Cyrano	Cyrano	k1gMnSc1	Cyrano
<g/>
!!	!!	k?	!!
Cyrano	Cyrano	k1gMnSc1	Cyrano
<g/>
!!	!!	k?	!!
Postmuzikál	Postmuzikál	k1gMnSc1	Postmuzikál
(	(	kIx(	(
<g/>
Roxana	Roxana	k1gFnSc1	Roxana
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
–	–	k?	–
Rebeka	Rebeka	k1gFnSc1	Rebeka
(	(	kIx(	(
<g/>
Rosmersholm	Rosmersholm	k1gInSc1	Rosmersholm
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Rebeka	Rebeka	k1gFnSc1	Rebeka
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
–	–	k?	–
Rod	rod	k1gInSc4	rod
Glembayů	Glembay	k1gMnPc2	Glembay
(	(	kIx(	(
<g/>
Baronka	baronka	k1gFnSc1	baronka
Castelliová-Glembayová	Castelliová-Glembayová	k1gFnSc1	Castelliová-Glembayová
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
–	–	k?	–
Její	její	k3xOp3gFnSc6	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
(	(	kIx(	(
<g/>
Kostelnička	kostelnička	k1gFnSc1	kostelnička
<g/>
)	)	kIx)	)
Švandovo	Švandův	k2eAgNnSc1d1	Švandovo
divadlo	divadlo	k1gNnSc1	divadlo
2008	[number]	k4	2008
–	–	k?	–
Chvíle	chvíle	k1gFnSc2	chvíle
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
Kathleen	Kathleen	k1gInSc1	Kathleen
<g/>
)	)	kIx)	)
Studio	studio	k1gNnSc1	studio
DVA	dva	k4xCgInPc4	dva
divadlo	divadlo	k1gNnSc1	divadlo
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Chvíle	chvíle	k1gFnSc1	chvíle
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
Kathleen	Kathleen	k1gInSc1	Kathleen
<g/>
)	)	kIx)	)
Získala	získat	k5eAaPmAgFnS	získat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
za	za	k7c4	za
herecké	herecký	k2eAgInPc4d1	herecký
výkony	výkon	k1gInPc4	výkon
a	a	k8xC	a
charitativní	charitativní	k2eAgFnSc4d1	charitativní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
–	–	k?	–
Zlaté	zlatá	k1gFnSc2	zlatá
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
–	–	k?	–
Holky	holka	k1gFnSc2	holka
z	z	k7c2	z
porcelánu	porcelán	k1gInSc2	porcelán
1976	[number]	k4	1976
–	–	k?	–
Zlaté	zlatá	k1gFnSc2	zlatá
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
–	–	k?	–
Holka	holka	k1gFnSc1	holka
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
1982	[number]	k4	1982
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
MFF	MFF	kA	MFF
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
1997	[number]	k4	1997
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
stuha	stuha	k1gFnSc1	stuha
Řádu	řád	k1gInSc2	řád
renesance	renesance	k1gFnSc2	renesance
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Cordon	Cordona	k1gFnPc2	Cordona
of	of	k?	of
Al	ala	k1gFnPc2	ala
Nahda	Nahda	k1gFnSc1	Nahda
Decoration	Decoration	k1gInSc1	Decoration
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udělená	udělený	k2eAgFnSc1d1	udělená
během	během	k7c2	během
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
v	v	k7c6	v
září	září	k1gNnSc6	září
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
jako	jako	k9	jako
druhé	druhý	k4xOgNnSc4	druhý
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
2001	[number]	k4	2001
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Niky	nika	k1gFnSc2	nika
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
za	za	k7c4	za
herectví	herectví	k1gNnSc4	herectví
2003	[number]	k4	2003
–	–	k?	–
Osobnost	osobnost	k1gFnSc1	osobnost
města	město	k1gNnSc2	město
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Židenice	Židenice	k1gFnSc1	Židenice
2004	[number]	k4	2004
–	–	k?	–
American	American	k1gInSc1	American
friends	friends	k1gInSc1	friends
of	of	k?	of
the	the	k?	the
Czech	Czech	k1gInSc4	Czech
Republic	Republice	k1gFnPc2	Republice
2004	[number]	k4	2004
–	–	k?	–
Hrnec	hrnec	k1gInSc1	hrnec
smíchu	smích	k1gInSc3	smích
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
přínos	přínos	k1gInSc4	přínos
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
2005	[number]	k4	2005
–	–	k?	–
Hercova	hercův	k2eAgFnSc1d1	hercova
mise	mise	k1gFnSc1	mise
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
herecké	herecký	k2eAgNnSc4d1	herecké
mistrovství	mistrovství	k1gNnSc4	mistrovství
<g/>
,	,	kIx,	,
MFF	MFF	kA	MFF
Trenčianské	trenčianský	k2eAgFnSc2d1	Trenčianská
Teplice	teplice	k1gFnSc2	teplice
2007	[number]	k4	2007
–	–	k?	–
Cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnPc4d1	Česká
komedii	komedie	k1gFnSc4	komedie
<g/>
,	,	kIx,	,
Novoměstský	novoměstský	k2eAgInSc4d1	novoměstský
Hrnec	hrnec	k1gInSc4	hrnec
smíchu	smích	k1gInSc2	smích
2009	[number]	k4	2009
–	–	k?	–
Cena	cena	k1gFnSc1	cena
"	"	kIx"	"
<g/>
Smích	smích	k1gInSc1	smích
léčí	léč	k1gFnPc2	léč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ONKOCET	ONKOCET	kA	ONKOCET
EUROPE	EUROPE	kA	EUROPE
o.p.s.	o.p.s.	k?	o.p.s.
2013	[number]	k4	2013
–	–	k?	–
American	American	k1gInSc1	American
Friends	Friends	k1gInSc1	Friends
of	of	k?	of
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
Američtí	americký	k2eAgMnPc1d1	americký
přátelé	přítel	k1gMnPc1	přítel
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
za	za	k7c4	za
vynikající	vynikající	k2eAgNnSc4d1	vynikající
vedení	vedení	k1gNnSc4	vedení
Nadace	nadace	k1gFnSc2	nadace
VIZE	vize	k1gFnSc2	vize
97	[number]	k4	97
a	a	k8xC	a
významné	významný	k2eAgInPc1d1	významný
přínosy	přínos	k1gInPc1	přínos
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g />
.	.	kIx.	.
</s>
<s>
občanské	občanský	k2eAgFnSc3d1	občanská
společnosti	společnost	k1gFnSc3	společnost
2014	[number]	k4	2014
–	–	k?	–
Cena	cena	k1gFnSc1	cena
TýTý	TýTý	k1gFnSc1	TýTý
<g/>
,	,	kIx,	,
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
Herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
Absolutní	absolutní	k2eAgMnSc1d1	absolutní
vítěz	vítěz	k1gMnSc1	vítěz
všech	všecek	k3xTgFnPc2	všecek
kategorií	kategorie	k1gFnPc2	kategorie
2014	[number]	k4	2014
–	–	k?	–
Cena	cena	k1gFnSc1	cena
vinohradského	vinohradský	k2eAgMnSc2d1	vinohradský
diváka	divák	k1gMnSc2	divák
August	August	k1gMnSc1	August
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
sezony	sezona	k1gFnSc2	sezona
za	za	k7c4	za
Ibsenovu	Ibsenův	k2eAgFnSc4d1	Ibsenova
Rebeku	Rebeka	k1gFnSc4	Rebeka
2016	[number]	k4	2016
–	–	k?	–
Cena	cena	k1gFnSc1	cena
vinohradského	vinohradský	k2eAgMnSc2d1	vinohradský
diváka	divák	k1gMnSc2	divák
August	August	k1gMnSc1	August
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
sezony	sezona	k1gFnSc2	sezona
za	za	k7c4	za
Krležovu	Krležův	k2eAgFnSc4d1	Krležova
baronku	baronka	k1gFnSc4	baronka
Glembayovou-Castelli	Glembayovou-Castelle	k1gFnSc4	Glembayovou-Castelle
členka	členka	k1gFnSc1	členka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
charitativní	charitativní	k2eAgFnSc2d1	charitativní
organizace	organizace	k1gFnSc2	organizace
<g />
.	.	kIx.	.
</s>
<s>
Femmes	Femmes	k1gInSc1	Femmes
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
členka	členka	k1gFnSc1	členka
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
Nadace	nadace	k1gFnSc2	nadace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
RCPCA	RCPCA	kA	RCPCA
čestná	čestný	k2eAgFnSc1d1	čestná
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
čestná	čestný	k2eAgFnSc1d1	čestná
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
-	-	kIx~	-
patronka	patronka	k1gFnSc1	patronka
Českého	český	k2eAgInSc2d1	český
výboru	výbor	k1gInSc2	výbor
UNICEF	UNICEF	kA	UNICEF
členka	členka	k1gFnSc1	členka
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Konta	konto	k1gNnSc2	konto
náročných	náročný	k2eAgFnPc2d1	náročná
operací	operace	k1gFnPc2	operace
čestná	čestný	k2eAgFnSc1d1	čestná
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Společnosti	společnost	k1gFnSc2	společnost
Vítězslavy	Vítězslava	k1gFnSc2	Vítězslava
Kaprálové	kaprál	k1gMnPc1	kaprál
členka	členka	k1gFnSc1	členka
a	a	k8xC	a
spolupracovnice	spolupracovnice	k1gFnSc1	spolupracovnice
rady	rada	k1gFnSc2	rada
Počítače	počítač	k1gInSc2	počítač
<g />
.	.	kIx.	.
</s>
<s>
proti	proti	k7c3	proti
bariérám	bariéra	k1gFnPc3	bariéra
při	při	k7c6	při
Chartě	charta	k1gFnSc6	charta
77	[number]	k4	77
pro	pro	k7c4	pro
mentálně	mentálně	k6eAd1	mentálně
postižené	postižený	k1gMnPc4	postižený
čestná	čestný	k2eAgFnSc1d1	čestná
členka	členka	k1gFnSc1	členka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
diplom	diplom	k1gInSc1	diplom
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
MAU	MAU	kA	MAU
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
za	za	k7c4	za
herecké	herecký	k2eAgNnSc4d1	herecké
umění	umění	k1gNnSc4	umění
čestná	čestný	k2eAgFnSc1d1	čestná
členka	členka	k1gFnSc1	členka
organizace	organizace	k1gFnSc2	organizace
Helppes	Helppesa	k1gFnPc2	Helppesa
<g/>
,	,	kIx,	,
soustřeďující	soustřeďující	k2eAgFnPc1d1	soustřeďující
se	se	k3xPyFc4	se
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
asistenčních	asistenční	k2eAgMnPc2d1	asistenční
psů	pes	k1gMnPc2	pes
pro	pro	k7c4	pro
postižené	postižený	k2eAgMnPc4d1	postižený
spoluobčany	spoluobčan	k1gMnPc4	spoluobčan
čestná	čestný	k2eAgFnSc1d1	čestná
členka	členka	k1gFnSc1	členka
České	český	k2eAgFnSc2d1	Česká
gastroenterologické	gastroenterologický	k2eAgFnSc2d1	gastroenterologická
společnosti	společnost	k1gFnSc2	společnost
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
členka	členka	k1gFnSc1	členka
Etické	etický	k2eAgFnSc2d1	etická
<g />
.	.	kIx.	.
</s>
<s>
komise	komise	k1gFnSc1	komise
České	český	k2eAgFnSc2d1	Česká
lékařské	lékařský	k2eAgFnSc2d1	lékařská
komory	komora	k1gFnSc2	komora
členka	členka	k1gFnSc1	členka
Společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
PEN	PEN	kA	PEN
klubu	klub	k1gInSc2	klub
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
členka	členka	k1gFnSc1	členka
Board	Boarda	k1gFnPc2	Boarda
of	of	k?	of
Advisorsinstituce	Advisorsinstituce	k1gFnSc2	Advisorsinstituce
Educational	Educational	k1gFnSc2	Educational
Initiative	Initiativ	k1gInSc5	Initiativ
for	forum	k1gNnPc2	forum
Central	Central	k1gMnSc7	Central
and	and	k?	and
Eastern	Eastern	k1gInSc1	Eastern
Europe	Europ	k1gInSc5	Europ
členka	členka	k1gFnSc1	členka
výboru	výbor	k1gInSc2	výbor
International	International	k1gFnSc2	International
Committee	Committee	k1gInSc1	Committee
of	of	k?	of
Woman	Woman	k1gInSc1	Woman
Leaders	Leadersa	k1gFnPc2	Leadersa
for	forum	k1gNnPc2	forum
Mental	Mental	k1gMnSc1	Mental
Health	Health	k1gMnSc1	Health
při	při	k7c6	při
presidentské	presidentský	k2eAgFnSc6d1	presidentská
knihovně	knihovna	k1gFnSc6	knihovna
J.	J.	kA	J.
Cartera	Carter	k1gMnSc2	Carter
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
a	a	k8xC	a
členka	členka	k1gFnSc1	členka
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Knihovny	knihovna	k1gFnSc2	knihovna
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
o.p.s.	o.p.s.	k?	o.p.s.
členka	členka	k1gFnSc1	členka
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
AMU	AMU	kA	AMU
čestná	čestný	k2eAgFnSc1d1	čestná
členka	členka	k1gFnSc1	členka
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
gastrointestinální	gastrointestinální	k2eAgFnSc4d1	gastrointestinální
onkologii	onkologie	k1gFnSc4	onkologie
(	(	kIx(	(
<g/>
SGO	SGO	kA	SGO
<g/>
)	)	kIx)	)
České	český	k2eAgFnSc2d1	Česká
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
Společnosti	společnost	k1gFnSc2	společnost
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
</s>
