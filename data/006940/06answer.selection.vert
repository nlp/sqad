<s>
Pylon	pylon	k1gInSc1	pylon
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
–	–	k?	–
"	"	kIx"	"
<g/>
brána	brána	k1gFnSc1	brána
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
egyptský	egyptský	k2eAgInSc1d1	egyptský
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
bechenet	bechenet	k5eAaImF	bechenet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
věžovité	věžovitý	k2eAgFnSc2d1	věžovitá
stavby	stavba	k1gFnSc2	stavba
tvořící	tvořící	k2eAgFnSc2d1	tvořící
v	v	k7c6	v
páru	pár	k1gInSc6	pár
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
monumentální	monumentální	k2eAgInSc1d1	monumentální
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
egyptských	egyptský	k2eAgInPc2d1	egyptský
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
