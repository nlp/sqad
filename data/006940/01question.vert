<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
označení	označení	k1gNnSc1	označení
věžovité	věžovitý	k2eAgFnSc2d1	věžovitá
stavby	stavba	k1gFnSc2	stavba
tvořící	tvořící	k2eAgFnSc2d1	tvořící
v	v	k7c6	v
páru	pár	k1gInSc6	pár
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
monumentální	monumentální	k2eAgInSc1d1	monumentální
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
egyptských	egyptský	k2eAgInPc2d1	egyptský
chrámů	chrám	k1gInPc2	chrám
<g/>
?	?	kIx.	?
</s>
