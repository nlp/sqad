<s>
Pylon	pylon	k1gInSc1	pylon
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
–	–	k?	–
"	"	kIx"	"
<g/>
brána	brána	k1gFnSc1	brána
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
egyptský	egyptský	k2eAgInSc1d1	egyptský
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
bechenet	bechenet	k5eAaPmF	bechenet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
věžovité	věžovitý	k2eAgFnSc2d1	věžovitá
stavby	stavba	k1gFnSc2	stavba
tvořící	tvořící	k2eAgFnSc2d1	tvořící
v	v	k7c6	v
páru	pár	k1gInSc6	pár
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
monumentální	monumentální	k2eAgInSc1d1	monumentální
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
egyptských	egyptský	k2eAgInPc2d1	egyptský
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
lemující	lemující	k2eAgFnSc4d1	lemující
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
spojené	spojený	k2eAgNnSc4d1	spojené
nad	nad	k7c7	nad
portálem	portál	k1gInSc7	portál
mostkem	mostek	k1gInSc7	mostek
tvořily	tvořit	k5eAaImAgInP	tvořit
vnější	vnější	k2eAgFnSc4d1	vnější
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
širším	široký	k2eAgInSc7d2	širší
chrámovým	chrámový	k2eAgInSc7d1	chrámový
okrskem	okrsek	k1gInSc7	okrsek
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
chrámem	chrám	k1gInSc7	chrám
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
příbytkem	příbytek	k1gInSc7	příbytek
boha	bůh	k1gMnSc2	bůh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
pylonu	pylon	k1gInSc2	pylon
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
symboliky	symbolika	k1gFnSc2	symbolika
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
"	"	kIx"	"
<g/>
achet	achet	k1gInSc4	achet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
achet	achet	k1gInSc1	achet
je	být	k5eAaImIp3nS	být
egyptským	egyptský	k2eAgNnSc7d1	egyptské
slovem	slovo	k1gNnSc7	slovo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
obzor	obzor	k1gInSc4	obzor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
horizont	horizont	k1gInSc1	horizont
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
tajemné	tajemný	k2eAgNnSc1d1	tajemné
mystické	mystický	k2eAgNnSc1d1	mystické
místo	místo	k1gNnSc1	místo
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
vstupu	vstup	k1gInSc2	vstup
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
svět	svět	k1gInSc4	svět
mezi	mezi	k7c7	mezi
dvojicí	dvojice	k1gFnSc7	dvojice
hor.	hor.	k?	hor.
Ačkoli	ačkoli	k8xS	ačkoli
to	ten	k3xDgNnSc4	ten
nelze	lze	k6eNd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
architektonická	architektonický	k2eAgFnSc1d1	architektonická
podoba	podoba	k1gFnSc1	podoba
pylonu	pylon	k1gInSc2	pylon
jako	jako	k8xC	jako
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
světem	svět	k1gInSc7	svět
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
světem	svět	k1gInSc7	svět
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
hieroglyfické	hieroglyfický	k2eAgFnSc2d1	hieroglyfická
značky	značka	k1gFnSc2	značka
achet	acheta	k1gFnPc2	acheta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgInPc6d1	rozsáhlý
chrámových	chrámový	k2eAgInPc6d1	chrámový
komplexech	komplex	k1gInPc6	komplex
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Karnaku	Karnak	k1gInSc6	Karnak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
pylonů	pylon	k1gInPc2	pylon
znásobován	znásobován	k2eAgMnSc1d1	znásobován
i	i	k8xC	i
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
funkci	funkce	k1gFnSc4	funkce
–	–	k?	–
snad	snad	k9	snad
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
nám	my	k3xPp1nPc3	my
neznámou	známý	k2eNgFnSc7d1	neznámá
symbolikou	symbolika	k1gFnSc7	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jasně	jasně	k6eAd1	jasně
symbolické	symbolický	k2eAgFnSc6d1	symbolická
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
také	také	k9	také
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
srozumitelně	srozumitelně	k6eAd1	srozumitelně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pylony	pylon	k1gInPc7	pylon
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
Knihách	kniha	k1gFnPc6	kniha
mrtvých	mrtvý	k2eAgFnPc6d1	mrtvá
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
podsvětních	podsvětní	k2eAgFnPc6d1	podsvětní
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představují	představovat	k5eAaImIp3nP	představovat
místa	místo	k1gNnPc4	místo
zkoušek	zkouška	k1gFnPc2	zkouška
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
,	,	kIx,	,
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
ty	ten	k3xDgFnPc4	ten
části	část	k1gFnPc4	část
Duatu	Duat	k1gInSc2	Duat
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
strastiplné	strastiplný	k2eAgFnSc6d1	strastiplná
cestě	cesta	k1gFnSc6	cesta
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
bohy	bůh	k1gMnPc7	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
i	i	k9	i
fáze	fáze	k1gFnPc4	fáze
jeho	jeho	k3xOp3gFnPc4	jeho
podsvětní	podsvětní	k2eAgFnPc4d1	podsvětní
proměny	proměna	k1gFnPc4	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Pylon	pylon	k1gInSc1	pylon
má	mít	k5eAaImIp3nS	mít
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
půdorys	půdorys	k1gInSc4	půdorys
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
komolého	komolý	k2eAgInSc2d1	komolý
jehlanu	jehlan	k1gInSc2	jehlan
se	s	k7c7	s
stěnami	stěna	k1gFnPc7	stěna
zakončenými	zakončený	k2eAgFnPc7d1	zakončená
římsou	římsa	k1gFnSc7	římsa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
plný	plný	k2eAgInSc1d1	plný
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
úzké	úzký	k2eAgNnSc1d1	úzké
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
střešní	střešní	k2eAgFnSc4d1	střešní
terasu	terasa	k1gFnSc4	terasa
a	a	k8xC	a
ke	k	k7c3	k
zděnému	zděný	k2eAgInSc3d1	zděný
mostku	mostek	k1gInSc3	mostek
spojujícímu	spojující	k2eAgInSc3d1	spojující
oba	dva	k4xCgMnPc1	dva
z	z	k7c2	z
páru	pár	k1gInSc2	pár
pylonů	pylon	k1gInPc2	pylon
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
zdi	zeď	k1gFnPc1	zeď
(	(	kIx(	(
<g/>
a	a	k8xC	a
zejména	zejména	k9	zejména
zdi	zeď	k1gFnPc1	zeď
přední	přední	k2eAgFnPc1d1	přední
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zdobeny	zdoben	k2eAgInPc4d1	zdoben
náboženskými	náboženský	k2eAgInPc7d1	náboženský
a	a	k8xC	a
mytologickým	mytologický	k2eAgInSc7d1	mytologický
výjevy	výjev	k1gInPc1	výjev
a	a	k8xC	a
nápisy	nápis	k1gInPc1	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pylony	pylon	k1gInPc7	pylon
stávaly	stávat	k5eAaImAgFnP	stávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
stožáry	stožár	k1gInPc4	stožár
s	s	k7c7	s
vlajícími	vlající	k2eAgInPc7d1	vlající
úzkými	úzký	k2eAgInPc7d1	úzký
praporci	praporec	k1gInPc7	praporec
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
obelisky	obelisk	k1gInPc1	obelisk
a	a	k8xC	a
kolosální	kolosální	k2eAgFnPc1d1	kolosální
sochy	socha	k1gFnPc1	socha
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
doklad	doklad	k1gInSc1	doklad
užití	užití	k1gNnSc2	užití
pylonu	pylon	k1gInSc2	pylon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
cihlového	cihlový	k2eAgInSc2d1	cihlový
chrámu	chrám	k1gInSc2	chrám
Mentuhotepa	Mentuhotep	k1gMnSc2	Mentuhotep
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
postaveného	postavený	k2eAgMnSc4d1	postavený
ve	v	k7c6	v
Vesetu	Veset	k1gInSc6	Veset
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
nad	nad	k7c7	nad
Údolím	údolí	k1gNnSc7	údolí
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
ovšem	ovšem	k9	ovšem
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
v	v	k7c6	v
nárožních	nárožní	k2eAgFnPc6d1	nárožní
věžovitých	věžovitý	k2eAgFnPc6d1	věžovitá
stavbách	stavba	k1gFnPc6	stavba
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
dynastii	dynastie	k1gFnSc6	dynastie
součástí	součást	k1gFnPc2	součást
zádušního	zádušní	k2eAgInSc2d1	zádušní
chrámu	chrám	k1gInSc2	chrám
krále	král	k1gMnSc2	král
Niuserrea	Niuserreus	k1gMnSc2	Niuserreus
v	v	k7c6	v
Abúsíru	Abúsír	k1gInSc6	Abúsír
a	a	k8xC	a
v	v	k7c6	v
masívním	masívní	k2eAgNnSc6d1	masívní
zesílení	zesílení	k1gNnSc6	zesílení
zdiva	zdivo	k1gNnSc2	zdivo
věžovitými	věžovitý	k2eAgFnPc7d1	věžovitá
stavbami	stavba	k1gFnPc7	stavba
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
půdorysu	půdorys	k1gInSc2	půdorys
se	s	k7c7	s
skloněnými	skloněný	k2eAgFnPc7d1	skloněná
stěnami	stěna	k1gFnPc7	stěna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lemují	lemovat	k5eAaImIp3nP	lemovat
východní	východní	k2eAgInSc4d1	východní
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Džedkareova	Džedkareův	k2eAgInSc2d1	Džedkareův
zádušního	zádušní	k2eAgInSc2d1	zádušní
chrámu	chrám	k1gInSc2	chrám
v	v	k7c4	v
Sakkáře	Sakkář	k1gMnPc4	Sakkář
ze	z	k7c2	z
závěru	závěr	k1gInSc2	závěr
stejného	stejný	k2eAgNnSc2d1	stejné
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
Novou	nový	k2eAgFnSc7d1	nová
říší	říš	k1gFnSc7	říš
jsou	být	k5eAaImIp3nP	být
pylony	pylon	k1gInPc4	pylon
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
architektonického	architektonický	k2eAgInSc2d1	architektonický
kánonu	kánon	k1gInSc2	kánon
egyptského	egyptský	k2eAgInSc2d1	egyptský
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jím	on	k3xPp3gNnSc7	on
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
trvání	trvání	k1gNnSc2	trvání
kultu	kult	k1gInSc2	kult
egyptských	egyptský	k2eAgMnPc2d1	egyptský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pylon	pylon	k1gInSc1	pylon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pylon	pylon	k1gInSc4	pylon
(	(	kIx(	(
<g/>
architecture	architectur	k1gMnSc5	architectur
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
