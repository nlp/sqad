<s>
Trojiční	trojiční	k2eAgInSc1d1
sloup	sloup	k1gInSc1
</s>
<s>
Morový	morový	k2eAgInSc1d1
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1693	#num#	k4
(	(	kIx(
<g/>
dokončený	dokončený	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Trojiční	trojiční	k2eAgInSc1d1
sloup	sloup	k1gInSc1
(	(	kIx(
<g/>
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
sochařského	sochařský	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
sloupu	sloup	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
jehož	jehož	k3xOyRp3gInSc4
vršek	vršek	k1gInSc4
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
sochařské	sochařský	k2eAgNnSc1d1
ztvárnění	ztvárnění	k1gNnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
monumentálniho	monumentálni	k1gMnSc2
díla	dílo	k1gNnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
tradice	tradice	k1gFnSc2
mariánských	mariánský	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
,	,	kIx,
první	první	k4xOgInPc1
trojiční	trojiční	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
objevovat	objevovat	k5eAaImF
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yQgNnPc4,k3yIgNnPc4
umožnilo	umožnit	k5eAaPmAgNnS
široké	široký	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
po	po	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
sloup	sloup	k1gInSc1
definoval	definovat	k5eAaBmAgInS
také	také	k9
základní	základní	k2eAgFnPc4d1
ikonografické	ikonografický	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
trojičních	trojiční	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
:	:	kIx,
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
ztvárněna	ztvárnit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
a	a	k8xC
Bůh	bůh	k1gMnSc1
Syn	syn	k1gMnSc1
sedí	sedit	k5eAaImIp3nS
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
a	a	k8xC
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
poletuje	poletovat	k5eAaImIp3nS
holubice	holubice	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Žaltářová	žaltářový	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
v	v	k7c6
baroku	baroko	k1gNnSc6
stalo	stát	k5eAaPmAgNnS
populární	populární	k2eAgNnSc1d1
ztvárnění	ztvárnění	k1gNnSc1
(	(	kIx(
<g/>
další	další	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
Trůn	trůn	k1gInSc4
milosti	milost	k1gFnSc2
<g/>
,	,	kIx,
Gnadenstuhl	Gnadenstuhl	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
sloup	sloup	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
trojúhelníkovém	trojúhelníkový	k2eAgInSc6d1
půdorysu	půdorys	k1gInSc6
a	a	k8xC
navíc	navíc	k6eAd1
byl	být	k5eAaImAgInS
pojednán	pojednán	k2eAgInSc1d1
jako	jako	k8xS,k8xC
iluzivní	iluzivní	k2eAgInSc1d1
tzv.	tzv.	kA
oblakový	oblakový	k2eAgInSc1d1
sloup	sloup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
trojičních	trojiční	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
(	(	kIx(
<g/>
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
chráněný	chráněný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
památka	památka	k1gFnSc1
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloup	sloup	k1gInSc1
se	se	k3xPyFc4
stavěl	stavět	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1716	#num#	k4
až	až	k9
1754	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomoucký	olomoucký	k2eAgInSc1d1
sloup	sloup	k1gInSc1
sice	sice	k8xC
užívá	užívat	k5eAaImIp3nS
sloup	sloup	k1gInSc1
řádové	řádový	k2eAgFnSc2d1
architketury	architketura	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jinak	jinak	k6eAd1
užívá	užívat	k5eAaImIp3nS
velmi	velmi	k6eAd1
bohatou	bohatý	k2eAgFnSc4d1
sochařskou	sochařský	k2eAgFnSc4d1
výzdobu	výzdoba	k1gFnSc4
a	a	k8xC
Nejsvětější	nejsvětější	k2eAgFnSc4d1
Trojici	trojice	k1gFnSc4
stejnou	stejný	k2eAgFnSc4d1
jako	jako	k9
vídeňský	vídeňský	k2eAgMnSc1d1
(	(	kIx(
<g/>
doplněný	doplněný	k2eAgInSc1d1
o	o	k7c4
Pannu	Panna	k1gFnSc4
Marii	Maria	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c6
některých	některý	k3yIgInPc6
sloupech	sloup	k1gInPc6
se	se	k3xPyFc4
pak	pak	k6eAd1
trojiční	trojiční	k2eAgInSc1d1
a	a	k8xC
mariánský	mariánský	k2eAgInSc1d1
výjev	výjev	k1gInSc1
uzavírá	uzavírat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
podoby	podoba	k1gFnSc2
Korunování	korunování	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ikonografie	ikonografie	k1gFnSc1
</s>
<s>
ObrázekPopis	ObrázekPopis	k1gInSc1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Žaltářová	žaltářový	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Náchodě	Náchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Žaltářová	žaltářový	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
<g/>
,	,	kIx,
Kříž	kříž	k1gInSc1
umístěn	umístit	k5eAaPmNgInS
centrálně	centrálně	k6eAd1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
Trůn	trůn	k1gInSc1
milosti	milost	k1gFnSc2
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Horním	horní	k2eAgInSc6d1
Slavkově	Slavkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Trůn	trůn	k1gInSc1
milosti	milost	k1gFnSc2
<g/>
,	,	kIx,
varianta	varianta	k1gFnSc1
Pietas	Pietasa	k1gFnPc2
Domini	Domin	k1gMnPc1
(	(	kIx(
<g/>
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
drží	držet	k5eAaImIp3nS
z	z	k7c2
kříže	kříž	k1gInSc2
sejmutého	sejmutý	k2eAgMnSc2d1
Krista	Kristus	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Korunování	korunování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Marie	Marie	k1gFnSc1
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
vrcholového	vrcholový	k2eAgNnSc2d1
sousoší	sousoší	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
socha	socha	k1gFnSc1
zpracována	zpracovat	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
Korunování	korunování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Marie	Marie	k1gFnSc1
je	být	k5eAaImIp3nS
pod	pod	k7c7
hlavicí	hlavice	k1gFnSc7
sloupu	sloup	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PAVLÍČEK	Pavlíček	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čestný	čestný	k2eAgInSc1d1
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
(	(	kIx(
<g/>
1716	#num#	k4
<g/>
–	–	k?
<g/>
1754	#num#	k4
<g/>
)	)	kIx)
in	in	k?
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Umění	umění	k1gNnSc1
<g/>
:	:	kIx,
prostor	prostor	k1gInSc1
pro	pro	k7c4
život	život	k1gInSc4
a	a	k8xC
hru	hra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texty	text	k1gInPc4
<g/>
:	:	kIx,
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
diva-dlo	diva-dnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
umění	umění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
153	#num#	k4
<g/>
–	–	k?
<g/>
176	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hana	Hana	k1gFnSc1
Petříková	Petříková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblačné	oblačný	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
včeských	včeský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magisterská	magisterský	k2eAgFnSc1d1
diplomové	diplomový	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
trojiční	trojiční	k2eAgInSc4d1
sloup	sloup	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
135217	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
7554095-2	7554095-2	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
