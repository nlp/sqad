<s desamb="1">
Tento	tento	k3xDgInSc1
sloup	sloup	k1gInSc1
definoval	definovat	k5eAaBmAgInS
také	také	k9
základní	základní	k2eAgFnPc4d1
ikonografické	ikonografický	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
trojičních	trojiční	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
:	:	kIx,
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
ztvárněna	ztvárnit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
a	a	k8xC
Bůh	bůh	k1gMnSc1
Syn	syn	k1gMnSc1
sedí	sedit	k5eAaImIp3nS
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
a	a	k8xC
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
poletuje	poletovat	k5eAaImIp3nS
holubice	holubice	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Žaltářová	žaltářový	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
v	v	k7c6
baroku	baroko	k1gNnSc6
stalo	stát	k5eAaPmAgNnS
populární	populární	k2eAgNnSc1d1
ztvárnění	ztvárnění	k1gNnSc1
(	(	kIx(
<g/>
další	další	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
Trůn	trůn	k1gInSc4
milosti	milost	k1gFnSc2
<g/>
,	,	kIx,
Gnadenstuhl	Gnadenstuhl	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>