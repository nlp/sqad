<s>
Trojiční	trojiční	k2eAgInSc1d1
sloup	sloup	k1gInSc1
(	(	kIx(
<g/>
sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
sochařského	sochařský	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
sloupu	sloup	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
jehož	jehož	k3xOyRp3gInSc4
vršek	vršek	k1gInSc4
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
sochařské	sochařský	k2eAgNnSc1d1
ztvárnění	ztvárnění	k1gNnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
.	.	kIx.
</s>