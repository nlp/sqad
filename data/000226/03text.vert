<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgFnSc1d1	vládní
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
potřetí	potřetí	k4xO	potřetí
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2006	[number]	k4	2006
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
2009	[number]	k4	2009
a	a	k8xC	a
opět	opět	k6eAd1	opět
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
jako	jako	k8xS	jako
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
koaliční	koaliční	k2eAgMnSc1d1	koaliční
partner	partner	k1gMnSc1	partner
v	v	k7c6	v
kabinetu	kabinet	k1gInSc6	kabinet
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
profiluje	profilovat	k5eAaImIp3nS	profilovat
jako	jako	k9	jako
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
liberální	liberální	k2eAgFnSc1d1	liberální
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
euroskeptická	euroskeptický	k2eAgFnSc1d1	euroskeptická
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
evropské	evropský	k2eAgFnSc6d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
Konzervativci	konzervativec	k1gMnPc7	konzervativec
a	a	k8xC	a
polskou	polský	k2eAgFnSc7d1	polská
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
Spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
stran	strana	k1gFnPc2	strana
třetí	třetí	k4xOgFnSc2	třetí
největší	veliký	k2eAgFnSc2d3	veliký
evropské	evropský	k2eAgFnSc2d1	Evropská
skupiny	skupina	k1gFnSc2	skupina
Evropští	evropský	k2eAgMnPc1d1	evropský
konzervativci	konzervativec	k1gMnPc1	konzervativec
a	a	k8xC	a
reformisté	reformista	k1gMnPc1	reformista
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgNnPc7d1	hlavní
hesly	heslo	k1gNnPc7	heslo
programu	program	k1gInSc2	program
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
nízké	nízký	k2eAgFnPc1d1	nízká
daně	daň	k1gFnPc1	daň
<g/>
,	,	kIx,	,
zdravé	zdravý	k2eAgFnPc1d1	zdravá
veřejné	veřejný	k2eAgFnPc1d1	veřejná
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
nezadlužená	zadlužený	k2eNgFnSc1d1	nezadlužená
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
<g/>
,	,	kIx,	,
adresný	adresný	k2eAgInSc1d1	adresný
sociální	sociální	k2eAgInSc1d1	sociální
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
snižování	snižování	k1gNnSc1	snižování
byrokracie	byrokracie	k1gFnSc2	byrokracie
<g/>
,	,	kIx,	,
uvolnění	uvolnění	k1gNnSc4	uvolnění
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
stát	stát	k1gInSc1	stát
a	a	k8xC	a
transatlantická	transatlantický	k2eAgFnSc1d1	transatlantická
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
ODS	ODS	kA	ODS
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1991	[number]	k4	1991
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
zvolený	zvolený	k2eAgInSc4d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
předsedou	předseda	k1gMnSc7	předseda
v	v	k7c6	v
období	období	k1gNnSc6	období
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
vykonával	vykonávat	k5eAaImAgInS	vykonávat
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
třetím	třetí	k4xOgMnSc7	třetí
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
je	být	k5eAaImIp3nS	být
Alexandra	Alexandra	k1gFnSc1	Alexandra
Udženija	Udženija	k1gFnSc1	Udženija
<g/>
,	,	kIx,	,
řadovými	řadový	k2eAgFnPc7d1	řadová
místopředsedy	místopředseda	k1gMnPc7	místopředseda
Martin	Martin	k1gMnSc1	Martin
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Vystrčil	Vystrčil	k1gMnSc1	Vystrčil
a	a	k8xC	a
Drahomíra	drahomíra	k1gMnSc1	drahomíra
Miklošová	Miklošová	k1gFnSc1	Miklošová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
ODS	ODS	kA	ODS
celkem	celkem	k6eAd1	celkem
33	[number]	k4	33
916	[number]	k4	916
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
organizováni	organizovat	k5eAaBmNgMnP	organizovat
v	v	k7c4	v
1	[number]	k4	1
461	[number]	k4	461
místních	místní	k2eAgMnPc2d1	místní
<g/>
,	,	kIx,	,
92	[number]	k4	92
oblastních	oblastní	k2eAgFnPc2d1	oblastní
a	a	k8xC	a
14	[number]	k4	14
regionálních	regionální	k2eAgFnPc2d1	regionální
sdruženích	sdružení	k1gNnPc6	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2015	[number]	k4	2015
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
pod	pod	k7c4	pod
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
neoficiální	neoficiální	k2eAgFnSc4d1	neoficiální
mládežnickou	mládežnický	k2eAgFnSc4d1	mládežnická
organizaci	organizace	k1gFnSc4	organizace
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
Mladí	mladý	k2eAgMnPc1d1	mladý
konzervativci	konzervativec	k1gMnPc1	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
volebních	volební	k2eAgInPc2d1	volební
programů	program	k1gInPc2	program
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
volební	volební	k2eAgInSc1d1	volební
program	program	k1gInSc1	program
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
nazvala	nazvat	k5eAaBmAgFnS	nazvat
ODS	ODS	kA	ODS
"	"	kIx"	"
<g/>
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
lepší	dobrý	k2eAgInSc4d2	lepší
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
či	či	k8xC	či
také	také	k9	také
"	"	kIx"	"
<g/>
ODS	ODS	kA	ODS
Plus	plus	k1gInSc1	plus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kampaň	kampaň	k1gFnSc1	kampaň
následovala	následovat	k5eAaImAgFnS	následovat
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Modré	modrý	k2eAgFnSc3d1	modrá
šanci	šance	k1gFnSc3	šance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
měla	mít	k5eAaImAgFnS	mít
vymezit	vymezit	k5eAaPmF	vymezit
k	k	k7c3	k
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
koaličním	koaliční	k2eAgFnPc3d1	koaliční
vládám	vláda	k1gFnPc3	vláda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
nabídnout	nabídnout	k5eAaPmF	nabídnout
pravicové	pravicový	k2eAgFnPc4d1	pravicová
alternativy	alternativa	k1gFnPc4	alternativa
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
ODS	ODS	kA	ODS
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
opatření	opatření	k1gNnPc4	opatření
nutná	nutný	k2eAgNnPc4d1	nutné
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zastaveno	zastaven	k2eAgNnSc1d1	zastaveno
plýtvání	plýtvání	k1gNnSc1	plýtvání
penězi	peníze	k1gInPc7	peníze
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
zneužívání	zneužívání	k1gNnSc1	zneužívání
sociálních	sociální	k2eAgFnPc2d1	sociální
dávek	dávka	k1gFnPc2	dávka
a	a	k8xC	a
zadlužování	zadlužování	k1gNnSc2	zadlužování
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
snížena	snížen	k2eAgFnSc1d1	snížena
míra	míra	k1gFnSc1	míra
korupce	korupce	k1gFnSc1	korupce
<g/>
,	,	kIx,	,
posílena	posílen	k2eAgFnSc1d1	posílena
udržitelnost	udržitelnost	k1gFnSc1	udržitelnost
sociálního	sociální	k2eAgInSc2d1	sociální
a	a	k8xC	a
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zvýšena	zvýšen	k2eAgFnSc1d1	zvýšena
motivace	motivace	k1gFnSc1	motivace
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
snížena	snížen	k2eAgFnSc1d1	snížena
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
rovná	rovný	k2eAgFnSc1d1	rovná
daň	daň	k1gFnSc1	daň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poukazovala	poukazovat	k5eAaImAgFnS	poukazovat
na	na	k7c4	na
přínosy	přínos	k1gInPc4	přínos
podobného	podobný	k2eAgInSc2d1	podobný
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zavedla	zavést	k5eAaPmAgFnS	zavést
SDKÚ-DS	SDKÚ-DS	k1gFnSc1	SDKÚ-DS
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
ODS	ODS	kA	ODS
vyvolila	vyvolit	k5eAaPmAgFnS	vyvolit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
k	k	k7c3	k
následování	následování	k1gNnSc3	následování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiky	kritika	k1gFnSc2	kritika
levicových	levicový	k2eAgFnPc2d1	levicová
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
šanci	šance	k1gFnSc4	šance
jen	jen	k9	jen
pro	pro	k7c4	pro
bohatší	bohatý	k2eAgFnSc4d2	bohatší
menšinu	menšina	k1gFnSc4	menšina
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
vedoucí	vedoucí	k1gFnSc2	vedoucí
zejména	zejména	k9	zejména
k	k	k7c3	k
dalšímu	další	k2eAgMnSc3d1	další
"	"	kIx"	"
<g/>
rozevírání	rozevírání	k1gNnSc3	rozevírání
nůžek	nůžky	k1gFnPc2	nůžky
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
sociálními	sociální	k2eAgFnPc7d1	sociální
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
vtělila	vtělit	k5eAaPmAgFnS	vtělit
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
"	"	kIx"	"
<g/>
antikampaně	antikampaně	k6eAd1	antikampaně
<g/>
"	"	kIx"	"
s	s	k7c7	s
názvem	název	k1gInSc7	název
ODS	ODS	kA	ODS
Mínus	mínus	k1gInSc1	mínus
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
například	například	k6eAd1	například
zavedení	zavedení	k1gNnSc4	zavedení
regulačních	regulační	k2eAgInPc2d1	regulační
poplatků	poplatek	k1gInPc2	poplatek
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
diskuze	diskuze	k1gFnPc4	diskuze
o	o	k7c6	o
školném	školné	k1gNnSc6	školné
a	a	k8xC	a
reformách	reforma	k1gFnPc6	reforma
klíčových	klíčový	k2eAgInPc2d1	klíčový
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EDU	Eda	k1gMnSc4	Eda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společného	společný	k2eAgNnSc2d1	společné
pravicového	pravicový	k2eAgNnSc2d1	pravicové
uskupení	uskupení	k1gNnSc2	uskupení
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
evropských	evropský	k2eAgFnPc2d1	Evropská
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
liberálního	liberální	k2eAgNnSc2d1	liberální
a	a	k8xC	a
křesťansko-demokratického	křesťanskoemokratický	k2eAgNnSc2d1	křesťansko-demokratické
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
spoluzakládala	spoluzakládat	k5eAaImAgFnS	spoluzakládat
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
Konzervativci	konzervativec	k1gMnPc7	konzervativec
<g/>
)	)	kIx)	)
evropskou	evropský	k2eAgFnSc4d1	Evropská
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
Hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
evropskou	evropský	k2eAgFnSc4d1	Evropská
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
evropskou	evropský	k2eAgFnSc4d1	Evropská
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
skupinu	skupina	k1gFnSc4	skupina
Evropští	evropský	k2eAgMnPc1d1	evropský
konzervativci	konzervativec	k1gMnPc1	konzervativec
a	a	k8xC	a
reformisté	reformista	k1gMnPc1	reformista
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kongresů	kongres	k1gInPc2	kongres
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
ODS	ODS	kA	ODS
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
stanov	stanova	k1gFnPc2	stanova
kongres	kongres	k1gInSc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Schází	scházet	k5eAaImIp3nS	scházet
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sudých	sudý	k2eAgNnPc6d1	sudé
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nP	volit
vedení	vedení	k1gNnSc3	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
ODS	ODS	kA	ODS
jsou	být	k5eAaImIp3nP	být
místní	místní	k2eAgNnPc4d1	místní
sdružení	sdružení	k1gNnPc4	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
do	do	k7c2	do
oblastních	oblastní	k2eAgNnPc2d1	oblastní
sdružení	sdružení	k1gNnPc2	sdružení
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
regionálních	regionální	k2eAgInPc2d1	regionální
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
každého	každý	k3xTgNnSc2	každý
sdružení	sdružení	k1gNnSc2	sdružení
je	být	k5eAaImIp3nS	být
sněm	sněm	k1gInSc1	sněm
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc2d1	místní
<g/>
,	,	kIx,	,
oblastní	oblastní	k2eAgFnSc2d1	oblastní
a	a	k8xC	a
regionální	regionální	k2eAgFnSc2d1	regionální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
podle	podle	k7c2	podle
stanov	stanova	k1gFnPc2	stanova
aspoň	aspoň	k9	aspoň
1	[number]	k4	1
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
sněmy	sněm	k1gInPc7	sněm
řídí	řídit	k5eAaImIp3nS	řídit
činnost	činnost	k1gFnSc1	činnost
sdružení	sdružení	k1gNnSc2	sdružení
jeho	jeho	k3xOp3gFnSc1	jeho
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kongresy	kongres	k1gInPc7	kongres
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
strany	strana	k1gFnSc2	strana
výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
22	[number]	k4	22
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
zástupci	zástupce	k1gMnSc6	zástupce
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
regionálního	regionální	k2eAgNnSc2d1	regionální
sdružení	sdružení	k1gNnSc2	sdružení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
republikový	republikový	k2eAgInSc1d1	republikový
sněm	sněm	k1gInSc1	sněm
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
<g/>
,	,	kIx,	,
předsedové	předseda	k1gMnPc1	předseda
regionálních	regionální	k2eAgNnPc2d1	regionální
sdružení	sdružení	k1gNnPc2	sdružení
a	a	k8xC	a
předsedové	předseda	k1gMnPc1	předseda
oblastních	oblastní	k2eAgNnPc2d1	oblastní
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
<g/>
×	×	k?	×
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
průběžně	průběžně	k6eAd1	průběžně
řídí	řídit	k5eAaImIp3nS	řídit
činnost	činnost	k1gFnSc4	činnost
strany	strana	k1gFnSc2	strana
mezi	mezi	k7c7	mezi
zasedáními	zasedání	k1gNnPc7	zasedání
výkonné	výkonný	k2eAgFnPc4d1	výkonná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gMnPc2	její
místopředsedů	místopředseda	k1gMnPc2	místopředseda
a	a	k8xC	a
předsedů	předseda	k1gMnPc2	předseda
parlamentních	parlamentní	k2eAgInPc2d1	parlamentní
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
na	na	k7c6	na
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
subjekty	subjekt	k1gInPc4	subjekt
<g/>
:	:	kIx,	:
ODS	ODS	kA	ODS
a	a	k8xC	a
středolevé	středolevý	k2eAgNnSc1d1	středolevé
Občanské	občanský	k2eAgNnSc1d1	občanské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
OF	OF	kA	OF
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1991	[number]	k4	1991
ODS	ODS	kA	ODS
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
koaliční	koaliční	k2eAgFnSc4d1	koaliční
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Křesťanskodemokratickou	křesťanskodemokratický	k2eAgFnSc7d1	Křesťanskodemokratická
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
KDS	KDS	kA	KDS
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
koalice	koalice	k1gFnSc1	koalice
ODS-KDS	ODS-KDS	k1gFnSc1	ODS-KDS
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
zákonodárných	zákonodárný	k2eAgInPc6d1	zákonodárný
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
pak	pak	k6eAd1	pak
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
Hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pak	pak	k6eAd1	pak
s	s	k7c7	s
KDS	KDS	kA	KDS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
ODA	ODA	kA	ODA
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
Stráský	Stráský	k1gMnSc1	Stráský
<g/>
,	,	kIx,	,
premiérem	premiér	k1gMnSc7	premiér
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pak	pak	k6eAd1	pak
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
ODS	ODS	kA	ODS
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
stran	strana	k1gFnPc2	strana
nejzásadněji	zásadně	k6eAd3	zásadně
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
rozpad	rozpad	k1gInSc4	rozpad
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
iniciativě	iniciativa	k1gFnSc3	iniciativa
předsedy	předseda	k1gMnSc2	předseda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pak	pak	k6eAd1	pak
provedla	provést	k5eAaPmAgFnS	provést
ODS	ODS	kA	ODS
zásadní	zásadní	k2eAgInPc4d1	zásadní
transformační	transformační	k2eAgInPc4d1	transformační
kroky	krok	k1gInPc4	krok
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
začleňování	začleňování	k1gNnSc4	začleňování
země	zem	k1gFnSc2	zem
do	do	k7c2	do
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
struktur	struktura	k1gFnPc2	struktura
<g/>
;	;	kIx,	;
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
i	i	k8xC	i
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
se	se	k3xPyFc4	se
ODS	ODS	kA	ODS
sloučila	sloučit	k5eAaPmAgFnS	sloučit
s	s	k7c7	s
KDS	KDS	kA	KDS
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
společně	společně	k6eAd1	společně
tak	tak	k6eAd1	tak
šly	jít	k5eAaImAgInP	jít
do	do	k7c2	do
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
ODS	ODS	kA	ODS
znovu	znovu	k6eAd1	znovu
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pravicová	pravicový	k2eAgFnSc1d1	pravicová
koalice	koalice	k1gFnSc1	koalice
získala	získat	k5eAaPmAgFnS	získat
pouze	pouze	k6eAd1	pouze
99	[number]	k4	99
poslaneckých	poslanecký	k2eAgNnPc2d1	poslanecké
křesel	křeslo	k1gNnPc2	křeslo
kvůli	kvůli	k7c3	kvůli
úspěchu	úspěch	k1gInSc3	úspěch
tzv.	tzv.	kA	tzv.
nesystémových	systémový	k2eNgFnPc2d1	nesystémová
stran	strana	k1gFnPc2	strana
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
SPR-RSČ	SPR-RSČ	k1gFnSc2	SPR-RSČ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dohodnuta	dohodnout	k5eAaPmNgFnS	dohodnout
menšinová	menšinový	k2eAgFnSc1d1	menšinová
koalice	koalice	k1gFnSc1	koalice
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
tolerovaná	tolerovaný	k2eAgFnSc1d1	tolerovaná
Českou	český	k2eAgFnSc7d1	Česká
stranou	strana	k1gFnSc7	strana
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
předseda	předseda	k1gMnSc1	předseda
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1996	[number]	k4	1996
získala	získat	k5eAaPmAgFnS	získat
ODS	ODS	kA	ODS
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
32	[number]	k4	32
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
však	však	k9	však
musela	muset	k5eAaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
vzrůstajícímu	vzrůstající	k2eAgInSc3d1	vzrůstající
tlaku	tlak	k1gInSc3	tlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
způsobený	způsobený	k2eAgInSc1d1	způsobený
především	především	k9	především
zhoršením	zhoršení	k1gNnSc7	zhoršení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
koaličních	koaliční	k2eAgMnPc2d1	koaliční
partnerů	partner	k1gMnPc2	partner
a	a	k8xC	a
i	i	k9	i
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
finančními	finanční	k2eAgInPc7d1	finanční
skandály	skandál	k1gInPc7	skandál
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1997	[number]	k4	1997
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Josef	Josef	k1gMnSc1	Josef
Zieleniec	Zieleniec	k1gMnSc1	Zieleniec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
místopředsedové	místopředseda	k1gMnPc1	místopředseda
strany	strana	k1gFnSc2	strana
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Pilip	Pilip	k1gMnSc1	Pilip
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vládu	vláda	k1gFnSc4	vláda
opustili	opustit	k5eAaPmAgMnP	opustit
ministři	ministr	k1gMnPc1	ministr
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
a	a	k8xC	a
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
premiérově	premiérův	k2eAgFnSc3d1	premiérova
demisi	demise	k1gFnSc3	demise
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
ODS	ODS	kA	ODS
zpopularizovala	zpopularizovat	k5eAaPmAgFnS	zpopularizovat
označení	označení	k1gNnSc4	označení
Sarajevský	sarajevský	k2eAgInSc1d1	sarajevský
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výzva	výzva	k1gFnSc1	výzva
přišla	přijít	k5eAaPmAgFnS	přijít
během	během	k7c2	během
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
návštěvy	návštěva	k1gFnSc2	návštěva
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
středoevropských	středoevropský	k2eAgFnPc2d1	středoevropská
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
válkou	válka	k1gFnSc7	válka
zničeném	zničený	k2eAgNnSc6d1	zničené
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1997	[number]	k4	1997
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
strany	strana	k1gFnSc2	strana
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
problematickém	problematický	k2eAgNnSc6d1	problematické
období	období	k1gNnSc6	období
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
strany	strana	k1gFnSc2	strana
<g/>
;	;	kIx,	;
Pilipovo	Pilipův	k2eAgNnSc4d1	Pilipův
a	a	k8xC	a
Rumlovo	Rumlův	k2eAgNnSc4d1	Rumlovo
křídlo	křídlo	k1gNnSc4	křídlo
ji	on	k3xPp3gFnSc4	on
opustilo	opustit	k5eAaPmAgNnS	opustit
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1998	[number]	k4	1998
založilo	založit	k5eAaPmAgNnS	založit
Unii	unie	k1gFnSc4	unie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nepodílela	podílet	k5eNaImAgFnS	podílet
na	na	k7c6	na
dočasné	dočasný	k2eAgFnSc6d1	dočasná
polo-úřednické	polo-úřednický	k2eAgFnSc6d1	polo-úřednický
Tošovského	Tošovského	k2eAgFnSc6d1	Tošovského
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ODS	ODS	kA	ODS
nečekaně	nečekaně	k6eAd1	nečekaně
dobrého	dobrý	k2eAgInSc2d1	dobrý
výsledku	výsledek	k1gInSc2	výsledek
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
s	s	k7c7	s
28	[number]	k4	28
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc1	možnost
koalice	koalice	k1gFnSc1	koalice
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
stran	strana	k1gFnPc2	strana
s	s	k7c7	s
US	US	kA	US
a	a	k8xC	a
lidovci	lidovec	k1gMnPc1	lidovec
<g/>
:	:	kIx,	:
unionisté	unionista	k1gMnPc1	unionista
však	však	k9	však
vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
ČSSD	ČSSD	kA	ČSSD
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
však	však	k9	však
podle	podle	k7c2	podle
novináře	novinář	k1gMnSc2	novinář
Erika	Erik	k1gMnSc4	Erik
Taberyho	Tabery	k1gMnSc4	Tabery
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Unii	unie	k1gFnSc4	unie
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
kruté	krutý	k2eAgFnPc4d1	krutá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
strany	strana	k1gFnPc1	strana
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
jako	jako	k9	jako
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
ODS	ODS	kA	ODS
překvapivě	překvapivě	k6eAd1	překvapivě
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
tzv.	tzv.	kA	tzv.
Opoziční	opoziční	k2eAgFnSc4d1	opoziční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
předsednictví	předsednictví	k1gNnSc4	předsednictví
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
důležitá	důležitý	k2eAgNnPc1d1	důležité
správní	správní	k2eAgNnPc1d1	správní
místa	místo	k1gNnPc1	místo
pro	pro	k7c4	pro
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Občanští	občanský	k2eAgMnPc1d1	občanský
demokraté	demokrat	k1gMnPc1	demokrat
také	také	k9	také
měli	mít	k5eAaImAgMnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
schválení	schválení	k1gNnSc4	schválení
zákonů	zákon	k1gInPc2	zákon
navržených	navržený	k2eAgInPc2d1	navržený
ČSSD	ČSSD	kA	ČSSD
včetně	včetně	k7c2	včetně
reformy	reforma	k1gFnSc2	reforma
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
posílit	posílit	k5eAaPmF	posílit
většinové	většinový	k2eAgInPc4d1	většinový
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
nelibostí	nelibost	k1gFnSc7	nelibost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
(	(	kIx(	(
<g/>
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
odejděte	odejít	k5eAaPmRp2nP	odejít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vláda	vláda	k1gFnSc1	vláda
přežila	přežít	k5eAaPmAgFnS	přežít
do	do	k7c2	do
konce	konec	k1gInSc2	konec
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
2002	[number]	k4	2002
opustila	opustit	k5eAaPmAgFnS	opustit
ODS	ODS	kA	ODS
skupina	skupina	k1gFnSc1	skupina
okolo	okolo	k7c2	okolo
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
pražského	pražský	k2eAgMnSc2d1	pražský
primátora	primátor	k1gMnSc2	primátor
Jana	Jan	k1gMnSc2	Jan
Kasla	Kasl	k1gMnSc2	Kasl
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
Sdružením	sdružení	k1gNnSc7	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
SNK	SNK	kA	SNK
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ODS	ODS	kA	ODS
historicky	historicky	k6eAd1	historicky
třetího	třetí	k4xOgInSc2	třetí
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
výsledku	výsledek	k1gInSc2	výsledek
(	(	kIx(	(
<g/>
24,5	[number]	k4	24,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
dal	dát	k5eAaPmAgMnS	dát
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
;	;	kIx,	;
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
ODS	ODS	kA	ODS
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ostravský	ostravský	k2eAgMnSc1d1	ostravský
senátor	senátor	k1gMnSc1	senátor
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c4	o
11	[number]	k4	11
hlasů	hlas	k1gInPc2	hlas
porazil	porazit	k5eAaPmAgInS	porazit
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ODS	ODS	kA	ODS
vyhrávala	vyhrávat	k5eAaImAgFnS	vyhrávat
všechny	všechen	k3xTgFnPc4	všechen
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
drtivé	drtivý	k2eAgFnSc2d1	drtivá
porážky	porážka	k1gFnSc2	porážka
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
následující	následující	k2eAgNnSc4d1	následující
hlasování	hlasování	k1gNnSc4	hlasování
<g/>
:	:	kIx,	:
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
stal	stát	k5eAaPmAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
získala	získat	k5eAaPmAgFnS	získat
9	[number]	k4	9
z	z	k7c2	z
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
Senátní	senátní	k2eAgFnSc2d1	senátní
a	a	k8xC	a
krajské	krajský	k2eAgFnSc2d1	krajská
volby	volba	k1gFnSc2	volba
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
získala	získat	k5eAaPmAgFnS	získat
19	[number]	k4	19
senátorských	senátorský	k2eAgInPc2d1	senátorský
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
hejtmany	hejtman	k1gMnPc7	hejtman
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
vyjma	vyjma	k7c2	vyjma
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
premiérem	premiér	k1gMnSc7	premiér
Senátní	senátní	k2eAgFnPc4d1	senátní
a	a	k8xC	a
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc6	jenž
získala	získat	k5eAaPmAgFnS	získat
ODS	ODS	kA	ODS
většinu	většina	k1gFnSc4	většina
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
(	(	kIx(	(
<g/>
41	[number]	k4	41
z	z	k7c2	z
81	[number]	k4	81
<g/>
)	)	kIx)	)
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Senátní	senátní	k2eAgFnSc2d1	senátní
a	a	k8xC	a
krajské	krajský	k2eAgFnSc2d1	krajská
volby	volba	k1gFnSc2	volba
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
senátorské	senátorský	k2eAgInPc4d1	senátorský
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
ztratila	ztratit	k5eAaPmAgFnS	ztratit
všech	všecek	k3xTgMnPc2	všecek
12	[number]	k4	12
hejtmanů	hejtman	k1gMnPc2	hejtman
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
ODS	ODS	kA	ODS
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
opět	opět	k6eAd1	opět
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
s	s	k7c7	s
historicky	historicky	k6eAd1	historicky
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
sestavila	sestavit	k5eAaPmAgFnS	sestavit
ODS	ODS	kA	ODS
koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
a	a	k8xC	a
Stranou	strana	k1gFnSc7	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Koaliční	koaliční	k2eAgFnPc1d1	koaliční
strany	strana	k1gFnPc1	strana
však	však	k9	však
získaly	získat	k5eAaPmAgFnP	získat
pouze	pouze	k6eAd1	pouze
100	[number]	k4	100
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
neobešly	neobešla	k1gFnPc1	neobešla
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
poslanců	poslanec	k1gMnPc2	poslanec
Michala	Michal	k1gMnSc2	Michal
Pohanky	Pohanka	k1gMnSc2	Pohanka
a	a	k8xC	a
Miloše	Miloš	k1gMnSc2	Miloš
Melčáka	Melčák	k1gMnSc2	Melčák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
opustili	opustit	k5eAaPmAgMnP	opustit
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Patová	patový	k2eAgFnSc1d1	patová
situace	situace	k1gFnSc1	situace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
několika	několik	k4yIc3	několik
měsícům	měsíc	k1gInPc3	měsíc
křečovitého	křečovitý	k2eAgNnSc2d1	křečovité
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
mezi	mezi	k7c7	mezi
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
politické	politický	k2eAgFnSc3d1	politická
nestabilitě	nestabilita	k1gFnSc3	nestabilita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
tak	tak	k8xS	tak
ODS	ODS	kA	ODS
držela	držet	k5eAaImAgFnS	držet
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
ústavní	ústavní	k2eAgInSc4d1	ústavní
i	i	k8xC	i
mocenské	mocenský	k2eAgFnPc4d1	mocenská
posty	posta	k1gFnPc4	posta
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
:	:	kIx,	:
post	post	k1gInSc1	post
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
podporoval	podporovat	k5eAaImAgMnS	podporovat
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
12	[number]	k4	12
ze	z	k7c2	z
13	[number]	k4	13
krajských	krajský	k2eAgMnPc2d1	krajský
hejtmanů	hejtman	k1gMnPc2	hejtman
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgMnPc4	všechen
mimo	mimo	k6eAd1	mimo
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
primátory	primátor	k1gMnPc7	primátor
většiny	většina	k1gFnSc2	většina
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
atd.	atd.	kA	atd.
mimo	mimo	k7c4	mimo
Ostravy	Ostrava	k1gFnPc4	Ostrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
důležitý	důležitý	k2eAgInSc1d1	důležitý
post	post	k1gInSc1	post
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
nedržela	držet	k5eNaImAgFnS	držet
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
Miloslav	Miloslav	k1gMnSc1	Miloslav
Vlček	Vlček	k1gMnSc1	Vlček
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
kroky	krok	k1gInPc4	krok
vlády	vláda	k1gFnSc2	vláda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
reforma	reforma	k1gFnSc1	reforma
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
Evropského	evropský	k2eAgNnSc2d1	Evropské
předsednictví	předsednictví	k1gNnSc2	předsednictví
<g/>
,	,	kIx,	,
důchodová	důchodový	k2eAgFnSc1d1	důchodová
a	a	k8xC	a
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
prohra	prohra	k1gFnSc1	prohra
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
drtivě	drtivě	k6eAd1	drtivě
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
opoziční	opoziční	k2eAgInSc4d1	opoziční
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
pro	pro	k7c4	pro
kritiky	kritika	k1gFnPc4	kritika
Topolánkova	Topolánkův	k2eAgNnSc2d1	Topolánkovo
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
konzervativněji	konzervativně	k6eAd2	konzervativně
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rebely	rebel	k1gMnPc4	rebel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
vedením	vedení	k1gNnSc7	vedení
ODS	ODS	kA	ODS
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
zhoršení	zhoršení	k1gNnSc3	zhoršení
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
demonstrativním	demonstrativní	k2eAgInSc7d1	demonstrativní
odchodem	odchod	k1gInSc7	odchod
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
ODS	ODS	kA	ODS
a	a	k8xC	a
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
postu	post	k1gInSc2	post
čestného	čestný	k2eAgMnSc2d1	čestný
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
kongresu	kongres	k1gInSc6	kongres
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Topolánka	Topolánek	k1gMnSc4	Topolánek
vystřídat	vystřídat	k5eAaPmF	vystřídat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
strany	strana	k1gFnSc2	strana
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Topolánek	Topolánek	k1gMnSc1	Topolánek
svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
nicméně	nicméně	k8xC	nicméně
ODS	ODS	kA	ODS
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
koaliční	koaliční	k2eAgFnPc1d1	koaliční
strany	strana	k1gFnPc1	strana
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
několika	několik	k4yIc2	několik
svých	svůj	k3xOyFgMnPc2	svůj
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgFnP	zaměřit
na	na	k7c4	na
nespokojence	nespokojenec	k1gMnPc4	nespokojenec
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
nedůvěře	nedůvěra	k1gFnSc6	nedůvěra
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
kabinetu	kabinet	k1gInSc2	kabinet
vedeného	vedený	k2eAgInSc2d1	vedený
Mirkem	Mirek	k1gMnSc7	Mirek
Topolánkem	Topolánek	k1gMnSc7	Topolánek
<g/>
;	;	kIx,	;
dva	dva	k4xCgMnPc1	dva
poslanci	poslanec	k1gMnPc1	poslanec
za	za	k7c4	za
ODS	ODS	kA	ODS
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
Zelených	Zelených	k2eAgMnPc2d1	Zelených
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vládu	vláda	k1gFnSc4	vláda
nepodpořit	podpořit	k5eNaPmF	podpořit
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
ostrý	ostrý	k2eAgInSc1d1	ostrý
nesouhlas	nesouhlas	k1gInSc1	nesouhlas
s	s	k7c7	s
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
praktikami	praktika	k1gFnPc7	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
pravomoci	pravomoc	k1gFnPc1	pravomoc
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
kongresu	kongres	k1gInSc2	kongres
převzal	převzít	k5eAaPmAgInS	převzít
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pověřený	pověřený	k2eAgMnSc1d1	pověřený
místopředseda	místopředseda	k1gMnSc1	místopředseda
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
21	[number]	k4	21
<g/>
.	.	kIx.	.
kongres	kongres	k1gInSc1	kongres
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
zvolil	zvolit	k5eAaPmAgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Volbám	volba	k1gFnPc3	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
předcházela	předcházet	k5eAaImAgFnS	předcházet
turbulentní	turbulentní	k2eAgFnSc1d1	turbulentní
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
volební	volební	k2eAgFnSc1d1	volební
kampaň	kampaň	k1gFnSc1	kampaň
běžící	běžící	k2eAgFnSc1d1	běžící
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
vyslovení	vyslovení	k1gNnSc2	vyslovení
nedůvěry	nedůvěra	k1gFnPc4	nedůvěra
Topolánkově	Topolánkův	k2eAgFnSc3d1	Topolánkova
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
volby	volba	k1gFnPc1	volba
měly	mít	k5eAaImAgFnP	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
jako	jako	k9	jako
předčasné	předčasný	k2eAgFnPc4d1	předčasná
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
však	však	k9	však
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Miloše	Miloš	k1gMnSc2	Miloš
Melčáka	Melčák	k1gMnSc2	Melčák
<g/>
,	,	kIx,	,
poslance	poslanec	k1gMnSc2	poslanec
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
parlamentem	parlament	k1gInSc7	parlament
přijatý	přijatý	k2eAgInSc1d1	přijatý
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
stejnou	stejný	k2eAgFnSc7d1	stejná
cestou	cesta	k1gFnSc7	cesta
jako	jako	k8xC	jako
při	při	k7c6	při
obdobné	obdobný	k2eAgFnSc6d1	obdobná
situaci	situace	k1gFnSc6	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
sice	sice	k8xC	sice
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
široké	široký	k2eAgFnSc3d1	široká
politické	politický	k2eAgFnSc3d1	politická
shodě	shoda	k1gFnSc3	shoda
na	na	k7c6	na
změně	změna	k1gFnSc6	změna
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
možnost	možnost	k1gFnSc4	možnost
samorozpuštění	samorozpuštění	k1gNnSc2	samorozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
o	o	k7c6	o
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
však	však	k9	však
ČSSD	ČSSD	kA	ČSSD
vedená	vedený	k2eAgFnSc1d1	vedená
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
porušila	porušit	k5eAaPmAgFnS	porušit
přijaté	přijatý	k2eAgFnPc4d1	přijatá
dohody	dohoda	k1gFnPc4	dohoda
a	a	k8xC	a
neumožnila	umožnit	k5eNaPmAgFnS	umožnit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obav	obava	k1gFnPc2	obava
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
úspěch	úspěch	k1gInSc4	úspěch
konání	konání	k1gNnSc2	konání
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
tak	tak	k6eAd1	tak
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
až	až	k9	až
v	v	k7c6	v
řádném	řádný	k2eAgInSc6d1	řádný
termínu	termín	k1gInSc6	termín
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
a	a	k8xC	a
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
původně	původně	k6eAd1	původně
"	"	kIx"	"
<g/>
letní	letní	k2eAgFnSc2d1	letní
překlenovací	překlenovací	k2eAgFnSc2d1	překlenovací
<g/>
"	"	kIx"	"
vlády	vláda	k1gFnSc2	vláda
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
se	se	k3xPyFc4	se
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
volebního	volební	k2eAgMnSc2d1	volební
lídra	lídr	k1gMnSc2	lídr
předseda	předseda	k1gMnSc1	předseda
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
12	[number]	k4	12
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
i	i	k9	i
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
ustavila	ustavit	k5eAaPmAgFnS	ustavit
novým	nový	k2eAgMnSc7d1	nový
volebním	volební	k2eAgMnSc7d1	volební
lídrem	lídr	k1gMnSc7	lídr
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
skončila	skončit	k5eAaPmAgFnS	skončit
ODS	ODS	kA	ODS
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
20,22	[number]	k4	20,22
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
53	[number]	k4	53
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
za	za	k7c4	za
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
22,08	[number]	k4	22,08
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
56	[number]	k4	56
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
uzavírací	uzavírací	k2eAgFnSc4d1	uzavírací
klauzuli	klauzule	k1gFnSc4	klauzule
překročily	překročit	k5eAaPmAgFnP	překročit
dále	daleko	k6eAd2	daleko
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
41	[number]	k4	41
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
26	[number]	k4	26
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
VV	VV	kA	VV
(	(	kIx(	(
<g/>
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ODS	ODS	kA	ODS
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
nezvítězila	zvítězit	k5eNaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
prakticky	prakticky	k6eAd1	prakticky
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
koaliční	koaliční	k2eAgNnSc1d1	koaliční
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
VV	VV	kA	VV
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
reflektoval	reflektovat	k5eAaImAgMnS	reflektovat
vůli	vůle	k1gFnSc4	vůle
středopravicových	středopravicový	k2eAgFnPc2d1	středopravicová
stran	strana	k1gFnPc2	strana
vytvořit	vytvořit	k5eAaPmF	vytvořit
vládu	vláda	k1gFnSc4	vláda
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
jejich	jejich	k3xOp3gFnSc4	jejich
bezprecedentní	bezprecedentní	k2eAgFnSc4d1	bezprecedentní
118	[number]	k4	118
<g/>
mandátovou	mandátový	k2eAgFnSc7d1	Mandátová
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
vedením	vedení	k1gNnSc7	vedení
jednání	jednání	k1gNnPc2	jednání
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Koaliční	koaliční	k2eAgFnSc1d1	koaliční
smlouva	smlouva	k1gFnSc1	smlouva
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
mezi	mezi	k7c7	mezi
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
VV	VV	kA	VV
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
vládní	vládní	k2eAgInSc4d1	vládní
křesla	křeslo	k1gNnPc4	křeslo
mezi	mezi	k7c4	mezi
smluvní	smluvní	k2eAgFnPc4d1	smluvní
strany	strana	k1gFnPc4	strana
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
6	[number]	k4	6
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
(	(	kIx(	(
<g/>
premiér	premiér	k1gMnSc1	premiér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alexandra	Alexandr	k1gMnSc4	Alexandr
Vondru	Vondra	k1gMnSc4	Vondra
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
legislativní	legislativní	k2eAgFnSc2d1	legislativní
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
Drobila	drobit	k5eAaImAgFnS	drobit
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martina	Martin	k1gMnSc2	Martin
Kocourka	Kocourek	k1gMnSc2	Kocourek
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ivana	Ivan	k1gMnSc4	Ivan
Fuksu	Fuksa	k1gMnSc4	Fuksa
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
jí	on	k3xPp3gFnSc3	on
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
důvěru	důvěra	k1gFnSc4	důvěra
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
některých	některý	k3yIgInPc2	některý
ministrů	ministr	k1gMnPc2	ministr
nejmenšího	malý	k2eAgMnSc2d3	nejmenší
koaličního	koaliční	k2eAgMnSc2d1	koaliční
partnera	partner	k1gMnSc2	partner
a	a	k8xC	a
také	také	k9	také
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
na	na	k7c4	na
postu	posta	k1gFnSc4	posta
ministra	ministr	k1gMnSc2	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
stal	stát	k5eAaPmAgMnS	stát
Tomáš	Tomáš	k1gMnSc1	Tomáš
Chalupa	Chalupa	k1gMnSc1	Chalupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
pak	pak	k6eAd1	pak
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
ministra	ministr	k1gMnSc2	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Ivana	Ivan	k1gMnSc4	Ivan
Fuksu	Fuksa	k1gMnSc4	Fuksa
dřívější	dřívější	k2eAgMnSc1d1	dřívější
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Petr	Petr	k1gMnSc1	Petr
Bendl	Bendl	k1gMnSc1	Bendl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prvním	první	k4xOgNnSc7	první
kolem	kolo	k1gNnSc7	kolo
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
obhájila	obhájit	k5eAaPmAgFnS	obhájit
ODS	ODS	kA	ODS
své	svůj	k3xOyFgNnSc4	svůj
prvenství	prvenství	k1gNnSc4	prvenství
mezi	mezi	k7c7	mezi
politickými	politický	k2eAgFnPc7d1	politická
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
získaných	získaný	k2eAgInPc2d1	získaný
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
ODS	ODS	kA	ODS
tak	tak	k8xC	tak
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
112	[number]	k4	112
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgFnSc1d1	výchozí
situace	situace	k1gFnSc1	situace
před	před	k7c7	před
senátními	senátní	k2eAgFnPc7d1	senátní
volbami	volba	k1gFnPc7	volba
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ODS	ODS	kA	ODS
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
vůbec	vůbec	k9	vůbec
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
18	[number]	k4	18
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvoukolovém	dvoukolový	k2eAgNnSc6d1	dvoukolové
senátním	senátní	k2eAgNnSc6d1	senátní
klání	klání	k1gNnSc6	klání
ODS	ODS	kA	ODS
získala	získat	k5eAaPmAgFnS	získat
8	[number]	k4	8
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
když	když	k8xS	když
celkovým	celkový	k2eAgInSc7d1	celkový
ziskem	zisk	k1gInSc7	zisk
12	[number]	k4	12
mandátů	mandát	k1gInPc2	mandát
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
nezměnily	změnit	k5eNaPmAgFnP	změnit
ani	ani	k8xC	ani
doplňovací	doplňovací	k2eAgFnPc1d1	doplňovací
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
uspěla	uspět	k5eAaPmAgFnS	uspět
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
krajů	kraj	k1gInPc2	kraj
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
obsadila	obsadit	k5eAaPmAgFnS	obsadit
s	s	k7c7	s
12,28	[number]	k4	12,28
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Občanští	občanský	k2eAgMnPc1d1	občanský
demokraté	demokrat	k1gMnPc1	demokrat
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
roli	role	k1gFnSc4	role
lídra	lídr	k1gMnSc2	lídr
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
když	když	k8xS	když
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
dokázali	dokázat	k5eAaPmAgMnP	dokázat
získat	získat	k5eAaPmF	získat
jen	jen	k9	jen
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejné	stejný	k2eAgInPc4d1	stejný
dny	den	k1gInPc4	den
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
i	i	k8xC	i
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
14	[number]	k4	14
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
10	[number]	k4	10
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgInSc4d3	nejvýraznější
úspěch	úspěch	k1gInSc4	úspěch
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kuber	k1gMnSc2	Kuber
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
získal	získat	k5eAaPmAgMnS	získat
40,49	[number]	k4	40,49
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgMnPc7	tři
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Předčasným	předčasný	k2eAgFnPc3d1	předčasná
volbám	volba	k1gFnPc3	volba
konaným	konaný	k2eAgFnPc3d1	konaná
ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
předcházela	předcházet	k5eAaImAgFnS	předcházet
politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
policejní	policejní	k2eAgInSc1d1	policejní
zásah	zásah	k1gInSc1	zásah
na	na	k7c6	na
úřadu	úřad	k1gInSc6	úřad
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
demise	demise	k1gFnSc1	demise
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
premiéra	premiéra	k1gFnSc1	premiéra
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
následné	následný	k2eAgNnSc4d1	následné
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
premiérova	premiérův	k2eAgNnSc2d1	premiérovo
okolí	okolí	k1gNnSc2	okolí
nepřineslo	přinést	k5eNaPmAgNnS	přinést
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
čelila	čelit	k5eAaImAgFnS	čelit
tvrdým	tvrdá	k1gFnPc3	tvrdá
útoků	útok	k1gInPc2	útok
i	i	k8xC	i
neopodstatněným	opodstatněný	k2eNgInPc3d1	neopodstatněný
odsudkům	odsudek	k1gInPc3	odsudek
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
názorových	názorový	k2eAgMnPc2d1	názorový
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
demise	demise	k1gFnSc2	demise
se	se	k3xPyFc4	se
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
LIDEM	Lido	k1gNnSc7	Lido
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c6	na
pokračování	pokračování	k1gNnSc6	pokračování
koaliční	koaliční	k2eAgFnSc2d1	koaliční
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ale	ale	k8xC	ale
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
postupem	postup	k1gInSc7	postup
navzdory	navzdory	k7c3	navzdory
ústavním	ústavní	k2eAgFnPc3d1	ústavní
zvyklostem	zvyklost	k1gFnPc3	zvyklost
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
vládu	vláda	k1gFnSc4	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
poslanci	poslanec	k1gMnPc1	poslanec
odhlasovali	odhlasovat	k5eAaPmAgMnP	odhlasovat
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
Sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
tak	tak	k9	tak
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
předčasným	předčasný	k2eAgFnPc3d1	předčasná
volbám	volba	k1gFnPc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vedla	vést	k5eAaImAgFnS	vést
občanské	občanský	k2eAgFnSc3d1	občanská
demokraty	demokrat	k1gMnPc7	demokrat
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
na	na	k7c4	na
stranické	stranický	k2eAgFnPc4d1	stranická
funkce	funkce	k1gFnPc4	funkce
tuto	tento	k3xDgFnSc4	tento
agendu	agenda	k1gFnSc4	agenda
zastal	zastat	k5eAaPmAgMnS	zastat
úřadující	úřadující	k2eAgMnSc1d1	úřadující
předseda	předseda	k1gMnSc1	předseda
Martin	Martin	k1gMnSc1	Martin
Kuba	Kuba	k1gMnSc1	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
7,72	[number]	k4	7,72
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
stačilo	stačit	k5eAaBmAgNnS	stačit
na	na	k7c4	na
16	[number]	k4	16
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
přišli	přijít	k5eAaPmAgMnP	přijít
lidé	člověk	k1gMnPc1	člověk
k	k	k7c3	k
volbám	volba	k1gFnPc3	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
23	[number]	k4	23
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
představila	představit	k5eAaPmAgFnS	představit
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
strana	strana	k1gFnSc1	strana
vizi	vize	k1gFnSc4	vize
eurorealistické	eurorealistický	k2eAgFnSc2d1	eurorealistická
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Občanští	občanský	k2eAgMnPc1d1	občanský
demokraté	demokrat	k1gMnPc1	demokrat
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
pouhý	pouhý	k2eAgInSc4d1	pouhý
antagonismus	antagonismus	k1gInSc4	antagonismus
vůči	vůči	k7c3	vůči
EU	EU	kA	EU
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
přesouváním	přesouvání	k1gNnSc7	přesouvání
pravomocí	pravomoc	k1gFnPc2	pravomoc
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Petice	petice	k1gFnSc1	petice
pro	pro	k7c4	pro
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
000	[number]	k4	000
podpisů	podpis	k1gInPc2	podpis
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tak	tak	k6eAd1	tak
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
přání	přání	k1gNnSc4	přání
zachovat	zachovat	k5eAaPmF	zachovat
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
nepřijímat	přijímat	k5eNaImF	přijímat
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Post	posta	k1gFnPc2	posta
europoslance	europoslance	k1gFnSc2	europoslance
ohájili	ohájit	k5eAaPmAgMnP	ohájit
Jan	Jan	k1gMnSc1	Jan
Zahradil	zahradit	k5eAaPmAgMnS	zahradit
a	a	k8xC	a
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
předpovědím	předpověď	k1gFnPc3	předpověď
mnohých	mnohý	k2eAgMnPc2d1	mnohý
komentátorů	komentátor	k1gMnPc2	komentátor
ODS	ODS	kA	ODS
uspěla	uspět	k5eAaPmAgFnS	uspět
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
procentuálního	procentuální	k2eAgInSc2d1	procentuální
přepočtu	přepočet	k1gInSc2	přepočet
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
"	"	kIx"	"
<g/>
modré	modré	k1gNnSc1	modré
<g/>
"	"	kIx"	"
předstihlo	předstihnout	k5eAaPmAgNnS	předstihnout
pouze	pouze	k6eAd1	pouze
hnutí	hnutí	k1gNnPc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Občanští	občanský	k2eAgMnPc1d1	občanský
demokraté	demokrat	k1gMnPc1	demokrat
obhájili	obhájit	k5eAaPmAgMnP	obhájit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
500	[number]	k4	500
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
obcí	obec	k1gFnPc2	obec
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
250	[number]	k4	250
starostů	starosta	k1gMnPc2	starosta
zvolených	zvolený	k2eAgMnPc2d1	zvolený
za	za	k7c4	za
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
společně	společně	k6eAd1	společně
s	s	k7c7	s
komunálními	komunální	k2eAgFnPc7d1	komunální
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
hned	hned	k6eAd1	hned
7	[number]	k4	7
kandidátů	kandidát	k1gMnPc2	kandidát
občanských	občanský	k2eAgMnPc2d1	občanský
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
komoře	komora	k1gFnSc6	komora
Parlamentu	parlament	k1gInSc2	parlament
získali	získat	k5eAaPmAgMnP	získat
2	[number]	k4	2
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgNnP	mít
ODS	ODS	kA	ODS
velikou	veliký	k2eAgFnSc4d1	veliká
podporu	podpora	k1gFnSc4	podpora
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
preference	preference	k1gFnSc1	preference
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
kolem	kolem	k7c2	kolem
35	[number]	k4	35
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sarajevském	sarajevský	k2eAgInSc6d1	sarajevský
atentátu	atentát	k1gInSc6	atentát
podpora	podpora	k1gFnSc1	podpora
ODS	ODS	kA	ODS
slábla	slábnout	k5eAaImAgFnS	slábnout
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
preference	preference	k1gFnPc1	preference
začaly	začít	k5eAaPmAgFnP	začít
zvedat	zvedat	k5eAaImF	zvedat
a	a	k8xC	a
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
vlády	vláda	k1gFnSc2	vláda
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
pak	pak	k6eAd1	pak
podpora	podpora	k1gFnSc1	podpora
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přinesly	přinést	k5eAaPmAgFnP	přinést
největší	veliký	k2eAgInSc4d3	veliký
zisk	zisk	k1gInSc4	zisk
přes	přes	k7c4	přes
35	[number]	k4	35
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
preference	preference	k1gFnSc1	preference
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
vyrovnaly	vyrovnat	k5eAaBmAgInP	vyrovnat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
volby	volba	k1gFnSc2	volba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
nakonec	nakonec	k6eAd1	nakonec
straně	strana	k1gFnSc6	strana
přinesly	přinést	k5eAaPmAgInP	přinést
jen	jen	k6eAd1	jen
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
pak	pak	k6eAd1	pak
prudce	prudko	k6eAd1	prudko
slábne	slábnout	k5eAaImIp3nS	slábnout
<g/>
,	,	kIx,	,
předčasné	předčasný	k2eAgFnPc1d1	předčasná
sněmovní	sněmovní	k2eAgFnPc1d1	sněmovní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
pouze	pouze	k6eAd1	pouze
7,7	[number]	k4	7,7
%	%	kIx~	%
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
její	její	k3xOp3gFnPc1	její
preference	preference	k1gFnPc1	preference
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
kolem	kolem	k7c2	kolem
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
propad	propad	k1gInSc1	propad
preferencí	preference	k1gFnPc2	preference
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
i	i	k9	i
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Květen	květen	k1gInSc1	květen
2016	[number]	k4	2016
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
vedení	vedení	k1gNnSc1	vedení
ODS	ODS	kA	ODS
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nemá	mít	k5eNaImIp3nS	mít
ODS	ODS	kA	ODS
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
žádného	žádný	k3yNgMnSc4	žádný
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
strany	strana	k1gFnSc2	strana
16	[number]	k4	16
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
je	být	k5eAaImIp3nS	být
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjura	k1gFnSc1	Stanjura
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedkyní	místopředsedkyně	k1gFnPc2	místopředsedkyně
Jana	Jana	k1gFnSc1	Jana
Černochová	Černochová	k1gFnSc1	Černochová
a	a	k8xC	a
řadovými	řadový	k2eAgMnPc7d1	řadový
místopředsedy	místopředseda	k1gMnPc7	místopředseda
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
vedení	vedení	k1gNnSc1	vedení
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
jsou	být	k5eAaImIp3nP	být
Ivan	Ivan	k1gMnSc1	Ivan
Adamec	Adamec	k1gMnSc1	Adamec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Zahradník	Zahradník	k1gMnSc1	Zahradník
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
předsednictvu	předsednictvo	k1gNnSc6	předsednictvo
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
žádného	žádný	k3yNgMnSc4	žádný
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
má	mít	k5eAaImIp3nS	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
výborech	výbor	k1gInPc6	výbor
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
přes	přes	k7c4	přes
špatný	špatný	k2eAgInSc4d1	špatný
volební	volební	k2eAgInSc4d1	volební
výsledek	výsledek	k1gInSc4	výsledek
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
vedení	vedení	k1gNnSc2	vedení
mandátového	mandátový	k2eAgInSc2d1	mandátový
a	a	k8xC	a
imunitního	imunitní	k2eAgInSc2d1	imunitní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
výbor	výbor	k1gInSc4	výbor
vedl	vést	k5eAaImAgMnS	vést
nejdříve	dříve	k6eAd3	dříve
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
nebyla	být	k5eNaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
jí	jíst	k5eAaImIp3nS	jíst
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
přenechal	přenechat	k5eAaPmAgMnS	přenechat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Senátorský	senátorský	k2eAgInSc1d1	senátorský
klub	klub	k1gInSc1	klub
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
senátního	senátní	k2eAgInSc2d1	senátní
klubu	klub	k1gInSc2	klub
strany	strana	k1gFnSc2	strana
15	[number]	k4	15
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubera	k1gFnSc1	Kubera
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Miloš	Miloš	k1gMnSc1	Miloš
Vystrčil	Vystrčil	k1gMnSc1	Vystrčil
a	a	k8xC	a
řadovým	řadový	k2eAgMnSc7d1	řadový
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Miroslav	Miroslav	k1gMnSc1	Miroslav
Škaloud	Škaloud	k1gMnSc1	Škaloud
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
vedení	vedení	k1gNnSc1	vedení
senátního	senátní	k2eAgInSc2d1	senátní
klubu	klub	k1gInSc2	klub
jsou	být	k5eAaImIp3nP	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kladívko	kladívko	k1gNnSc4	kladívko
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Oberfalzer	Oberfalzer	k1gMnSc1	Oberfalzer
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
neoficiálně	oficiálně	k6eNd1	oficiálně
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
místopředseda	místopředseda	k1gMnSc1	místopředseda
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
prezidentský	prezidentský	k2eAgMnSc1d1	prezidentský
kandidát	kandidát	k1gMnSc1	kandidát
Přemysl	Přemysl	k1gMnSc1	Přemysl
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Zahradil	zahradit	k5eAaPmAgMnS	zahradit
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
europoslaneckého	europoslanecký	k2eAgInSc2d1	europoslanecký
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
frakce	frakce	k1gFnSc2	frakce
ECR	ECR	kA	ECR
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Aliance	aliance	k1gFnSc2	aliance
evropských	evropský	k2eAgMnPc2d1	evropský
konzervativců	konzervativec	k1gMnPc2	konzervativec
a	a	k8xC	a
reformistů	reformista	k1gMnPc2	reformista
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
ODS	ODS	kA	ODS
Kritika	kritika	k1gFnSc1	kritika
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
ideových	ideový	k2eAgFnPc2d1	ideová
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Velkých	velká	k1gFnPc2	velká
rozměrů	rozměr	k1gInPc2	rozměr
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
při	při	k7c6	při
skandálu	skandál	k1gInSc6	skandál
s	s	k7c7	s
financováním	financování	k1gNnSc7	financování
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
kritice	kritika	k1gFnSc6	kritika
ODS	ODS	kA	ODS
postavila	postavit	k5eAaPmAgFnS	postavit
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
svou	svůj	k3xOyFgFnSc7	svůj
volební	volební	k2eAgFnSc7d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
ODS	ODS	kA	ODS
Mínus	mínus	k1gInSc1	mínus
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
byla	být	k5eAaImAgFnS	být
též	též	k9	též
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
politické	politický	k2eAgFnSc2d1	politická
pravice	pravice	k1gFnSc2	pravice
za	za	k7c4	za
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
prosazování	prosazování	k1gNnSc4	prosazování
pravicových	pravicový	k2eAgFnPc2d1	pravicová
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
Topolánkově	Topolánkův	k2eAgFnSc6d1	Topolánkova
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
podpis	podpis	k1gInSc4	podpis
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
Mirkem	Mirek	k1gMnSc7	Mirek
Topolánkem	Topolánek	k1gMnSc7	Topolánek
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
prosadit	prosadit	k5eAaPmF	prosadit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
schválení	schválení	k1gNnSc3	schválení
antidiskriminačního	antidiskriminační	k2eAgInSc2d1	antidiskriminační
zákona	zákon	k1gInSc2	zákon
nebo	nebo	k8xC	nebo
uznání	uznání	k1gNnSc2	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Kosova	Kosův	k2eAgFnSc1d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kritiky	kritika	k1gFnSc2	kritika
ideologické	ideologický	k2eAgFnSc2d1	ideologická
je	být	k5eAaImIp3nS	být
ODS	ODS	kA	ODS
též	též	k6eAd1	též
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
věcné	věcný	k2eAgInPc1d1	věcný
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
korupčním	korupční	k2eAgNnSc7d1	korupční
jednáním	jednání	k1gNnSc7	jednání
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
silnou	silný	k2eAgFnSc4d1	silná
nevoli	nevole	k1gFnSc4	nevole
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
například	například	k6eAd1	například
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Aleš	Aleš	k1gMnSc1	Aleš
Řebíček	řebíček	k1gInSc1	řebíček
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jenž	k3xRgNnSc4	jenž
působení	působení	k1gNnSc4	působení
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
<g/>
"	"	kIx"	"
firma	firma	k1gFnSc1	firma
Viamont	Viamonta	k1gFnPc2	Viamonta
miliardové	miliardový	k2eAgFnSc2d1	miliardová
státní	státní	k2eAgFnSc2d1	státní
zakázky	zakázka	k1gFnSc2	zakázka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jde	jít	k5eAaImIp3nS	jít
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
firmu	firma	k1gFnSc4	firma
s	s	k7c7	s
netransparentní	transparentní	k2eNgFnSc7d1	netransparentní
vlastnickou	vlastnický	k2eAgFnSc7d1	vlastnická
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
listinné	listinný	k2eAgFnPc4d1	listinná
akcie	akcie	k1gFnPc4	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
Aleš	Aleš	k1gMnSc1	Aleš
Řebíček	řebíček	k1gInSc4	řebíček
stále	stále	k6eAd1	stále
vlastní	vlastní	k2eAgInSc4d1	vlastní
nemalý	malý	k2eNgInSc4d1	nemalý
obchodní	obchodní	k2eAgInSc4d1	obchodní
podíl	podíl	k1gInSc4	podíl
a	a	k8xC	a
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c6	o
veřejných	veřejný	k2eAgFnPc6d1	veřejná
zakázkách	zakázka	k1gFnPc6	zakázka
se	se	k3xPyFc4	se
jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
dopustil	dopustit	k5eAaPmAgMnS	dopustit
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
obohatil	obohatit	k5eAaPmAgMnS	obohatit
se	se	k3xPyFc4	se
o	o	k7c4	o
stovky	stovka	k1gFnPc4	stovka
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pavel	Pavel	k1gMnSc1	Pavel
Bém	Bém	k1gMnSc1	Bém
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
primátor	primátor	k1gMnSc1	primátor
Prahy	Praha	k1gFnSc2	Praha
Pavel	Pavel	k1gMnSc1	Pavel
Blažek	Blažek	k1gMnSc1	Blažek
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Rudolf	Rudolf	k1gMnSc1	Rudolf
Blažek	Blažek	k1gMnSc1	Blažek
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
náměstek	náměstek	k1gMnSc1	náměstek
ministra	ministr	k1gMnSc2	ministr
Petr	Petr	k1gMnSc1	Petr
Gandalovič	Gandalovič	k1gMnSc1	Gandalovič
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Tomáš	Tomáš	k1gMnSc1	Tomáš
Chalupa	Chalupa	k1gMnSc1	Chalupa
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Tomáš	Tomáš	k1gMnSc1	Tomáš
Julínek	Julínek	k1gMnSc1	Julínek
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
Václav	Václav	k1gMnSc1	Václav
<g />
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Ivan	Ivan	k1gMnSc1	Ivan
Kočárník	kočárník	k1gMnSc1	kočárník
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
Martin	Martin	k1gMnSc1	Martin
Kuba	Kuba	k1gMnSc1	Kuba
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubera	k1gFnSc1	Kubera
-	-	kIx~	-
primátor	primátor	k1gMnSc1	primátor
Teplic	Teplice	k1gFnPc2	Teplice
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
Ivan	Ivan	k1gMnSc1	Ivan
Langer	Langer	k1gMnSc1	Langer
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macek	Macek	k1gMnSc1	Macek
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
vlády	vláda	k1gFnPc1	vláda
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
-	-	kIx~	-
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Ivan	Ivan	k1gMnSc1	Ivan
Pilip	Pilip	k1gMnSc1	Pilip
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Jiří	Jiří	k1gMnSc1	Jiří
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
-	-	kIx~	-
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
Přemysl	Přemysl	k1gMnSc1	Přemysl
Sobotka	Sobotka	k1gMnSc1	Sobotka
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
prezidentský	prezidentský	k2eAgMnSc1d1	prezidentský
kandidát	kandidát	k1gMnSc1	kandidát
Jan	Jan	k1gMnSc1	Jan
Stráský	Stráský	k1gMnSc1	Stráský
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
federální	federální	k2eAgMnSc1d1	federální
premiér	premiér	k1gMnSc1	premiér
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
primátor	primátor	k1gMnSc1	primátor
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjura	k1gFnSc1	Stanjura
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
-	-	kIx~	-
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
Evžen	Evžen	k1gMnSc1	Evžen
Tošenovský	Tošenovský	k2eAgMnSc1d1	Tošenovský
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
hejtman	hejtman	k1gMnSc1	hejtman
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
David	David	k1gMnSc1	David
Vodrážka	Vodrážka	k1gMnSc1	Vodrážka
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vondra	Vondra	k1gMnSc1	Vondra
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
Jan	Jan	k1gMnSc1	Jan
Zahradil	zahradit	k5eAaPmAgMnS	zahradit
-	-	kIx~	-
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
</s>
