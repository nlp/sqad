<s>
Stožec	Stožec	k1gInSc4
(	(	kIx(
<g/>
okres	okres	k1gInSc4
Prachatice	Prachatice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Stožec	Stožec	k1gMnSc1
Křižovatka	křižovatka	k1gFnSc1
na	na	k7c6
kraji	kraj	k1gInSc6
obce	obec	k1gFnSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0315	CZ0315	k4
550523	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
Volary	Volar	k1gMnPc4
Obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
315	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihočeský	jihočeský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
31	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
205	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
104,78	104,78	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
780	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
384	#num#	k4
44	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
3	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
4	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
7	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
StožecStožec	StožecStožec	k1gMnSc1
54	#num#	k4
<g/>
,	,	kIx,
Stožec	Stožec	k1gMnSc1
(	(	kIx(
<g/>
PSČ	PSČ	kA
384	#num#	k4
44	#num#	k4
<g/>
)	)	kIx)
podatelna@stozec.cz	podatelna@stozec.cz	k1gInSc1
Starostka	starostka	k1gFnSc1
</s>
<s>
Helga	Helga	k1gFnSc1
Finiková	Finikový	k2eAgFnSc1d1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.stozec.cz	www.stozec.cz	k1gInSc1
</s>
<s>
Stožec	Stožec	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Stožec	Stožec	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Tusset	Tusset	k1gInSc1
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Studené	Studené	k2eAgFnSc2d1
Vltavy	Vltava	k1gFnSc2
zhruba	zhruba	k6eAd1
7,5	7,5	k4
km	km	kA
(	(	kIx(
<g/>
15	#num#	k4
km	km	kA
po	po	k7c6
silnici	silnice	k1gFnSc6
/	/	kIx~
10	#num#	k4
km	km	kA
po	po	k7c6
železnici	železnice	k1gFnSc6
<g/>
)	)	kIx)
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Volar	Volara	k1gFnPc2
a	a	k8xC
21	#num#	k4
km	km	kA
(	(	kIx(
<g/>
35	#num#	k4
km	km	kA
po	po	k7c6
silnici	silnice	k1gFnSc6
<g/>
)	)	kIx)
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prachatic	Prachatice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
místních	místní	k2eAgFnPc2d1
částí	část	k1gFnPc2
–	–	k?
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
<g/>
,	,	kIx,
Dobrá	dobrý	k1gFnSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
a	a	k8xC
Stožec	Stožec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
205	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stožec	Stožec	k1gMnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
obcí	obec	k1gFnPc2
s	s	k7c7
největším	veliký	k2eAgNnSc7d3
územím	území	k1gNnSc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
Stožce	Stožka	k1gFnSc6
</s>
<s>
Stožecká	Stožecký	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Přesný	přesný	k2eAgInSc4d1
rok	rok	k1gInSc4
založení	založení	k1gNnSc2
vesnice	vesnice	k1gFnSc2
Stožec	Stožec	k1gMnSc1
není	být	k5eNaImIp3nS
znám	znám	k2eAgMnSc1d1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1769	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
dřevařská	dřevařský	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
zajímavou	zajímavý	k2eAgFnSc7d1
událostí	událost	k1gFnSc7
v	v	k7c6
dějinách	dějiny	k1gFnPc6
obce	obec	k1gFnSc2
byla	být	k5eAaImAgFnS
pytlácká	pytlácký	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
pytláckou	pytlácký	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
událo	udát	k5eAaPmAgNnS
v	v	k7c6
únoru	únor	k1gInSc6
1850	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bavorští	bavorský	k2eAgMnPc1d1
pytláci	pytlák	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
lesníky	lesník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
stačili	stačit	k5eAaBmAgMnP
opevnit	opevnit	k5eAaPmF
v	v	k7c6
místním	místní	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
Pstruh	pstruh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
několik	několik	k4yIc1
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
mnoho	mnoho	k4c1
zraněných	zraněný	k1gMnPc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
objektu	objekt	k1gInSc2
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
našlo	najít	k5eAaPmAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
kulek	kulka	k1gFnPc2
zavrtaných	zavrtaný	k2eAgFnPc2d1
do	do	k7c2
trámů	trám	k1gInPc2
roubené	roubený	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
16.8	16.8	k4
<g/>
.1894	.1894	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zaznamenal	zaznamenat	k5eAaPmAgMnS
Stožec	Stožec	k1gMnSc1
své	svůj	k3xOyFgFnPc4
vrcholné	vrcholný	k2eAgFnPc4d1
období	období	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
obce	obec	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
přivedena	přiveden	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
lokálních	lokální	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vedla	vést	k5eAaImAgFnS
z	z	k7c2
Černého	Černého	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
přes	přes	k7c4
Stožec	Stožec	k1gInSc4
do	do	k7c2
Nového	Nového	k2eAgNnSc2d1
Údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
leží	ležet	k5eAaImIp3nS
těsně	těsně	k6eAd1
před	před	k7c7
státní	státní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
s	s	k7c7
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
osmikilometrová	osmikilometrový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
umožnila	umožnit	k5eAaPmAgFnS
spojení	spojení	k1gNnSc4
jihočeských	jihočeský	k2eAgFnPc2d1
a	a	k8xC
pošumavských	pošumavský	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
se	s	k7c7
sítí	síť	k1gFnSc7
drah	draha	k1gFnPc2
bavorských	bavorský	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
pozvolna	pozvolna	k6eAd1
rostl	růst	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
2733	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
dokonce	dokonce	k9
2811	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
na	na	k7c4
celou	celý	k2eAgFnSc4d1
dnešní	dnešní	k2eAgFnSc4d1
obec	obec	k1gFnSc4
včetně	včetně	k7c2
přilehlých	přilehlý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
;	;	kIx,
jen	jen	k9
v	v	k7c6
Českých	český	k2eAgInPc6d1
Žlebech	žleb	k1gInPc6
žilo	žít	k5eAaImAgNnS
1200	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zlom	zlom	k1gInSc1
nastal	nastat	k5eAaPmAgInS
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příhraničí	příhraničí	k1gNnSc1
bylo	být	k5eAaImAgNnS
vylidněno	vylidnit	k5eAaPmNgNnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
vysídlení	vysídlení	k1gNnSc2
obyvatel	obyvatel	k1gMnPc2
německé	německý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
velké	velký	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Stožci	Stožce	k1gFnSc6
zůstalo	zůstat	k5eAaPmAgNnS
pouze	pouze	k6eAd1
599	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
počet	počet	k1gInSc1
navíc	navíc	k6eAd1
dále	daleko	k6eAd2
klesal	klesat	k5eAaImAgInS
hlavně	hlavně	k9
kvůli	kvůli	k7c3
represím	represe	k1gFnPc3
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
několik	několik	k4yIc4
málo	málo	k1gNnSc4
kilometrů	kilometr	k1gInPc2
vzdálené	vzdálený	k2eAgInPc1d1
od	od	k7c2
tehdejšího	tehdejší	k2eAgNnSc2d1
„	„	k?
<g/>
Západního	západní	k2eAgNnSc2d1
<g/>
“	“	k?
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
v	v	k7c6
obci	obec	k1gFnSc6
žije	žít	k5eAaImIp3nS
asi	asi	k9
jen	jen	k9
200	#num#	k4
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
Stožec	Stožec	k1gInSc1
cca	cca	kA
120	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
osada	osada	k1gFnSc1
České	český	k2eAgFnSc2d1
Žleby	žleb	k1gInPc1
cca	cca	kA
60	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
osada	osada	k1gFnSc1
Dobrá	dobrá	k9
8	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
přihlášených	přihlášený	k2eAgMnPc2d1
k	k	k7c3
trvalému	trvalý	k2eAgInSc3d1
pobytu	pobyt	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
Stožec	Stožec	k1gInSc1
jedním	jeden	k4xCgInSc7
z	z	k7c2
oblíbených	oblíbený	k2eAgInPc2d1
turistických	turistický	k2eAgInPc2d1
cílů	cíl	k1gInPc2
na	na	k7c4
Volarsku	Volarska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
v	v	k7c6
obci	obec	k1gFnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
</s>
<s>
Kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
známá	známá	k1gFnSc1
též	též	k9
pod	pod	k7c7
názvem	název	k1gInSc7
Stožecká	Stožecký	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
dřevěná	dřevěný	k2eAgFnSc1d1
poutní	poutní	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
pod	pod	k7c7
zaniklým	zaniklý	k2eAgInSc7d1
hradem	hrad	k1gInSc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
léčivým	léčivý	k2eAgInPc3d1
účinkům	účinek	k1gInPc3
místního	místní	k2eAgInSc2d1
pramene	pramen	k1gInSc2
a	a	k8xC
zázračnému	zázračný	k2eAgInSc3d1
obrazu	obraz	k1gInSc3
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhledávána	vyhledávat	k5eAaImNgFnS
poutníky	poutník	k1gMnPc7
z	z	k7c2
Čech	Čechy	k1gFnPc2
i	i	k8xC
nedalekých	daleký	k2eNgInPc2d1
Bavor	Bavory	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1804	#num#	k4
byla	být	k5eAaImAgFnS
kaple	kaple	k1gFnSc1
přestavěna	přestavět	k5eAaPmNgFnS
a	a	k8xC
poté	poté	k6eAd1
ještě	ještě	k9
několikrát	několikrát	k6eAd1
upravována	upravován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
kaple	kaple	k1gFnSc1
odstraněna	odstranit	k5eAaPmNgFnS
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
podle	podle	k7c2
její	její	k3xOp3gFnSc2
kopie	kopie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
německé	německý	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
Philippsreut	Philippsreut	k1gInSc4
(	(	kIx(
<g/>
hraniční	hraniční	k2eAgInSc4d1
přechod	přechod	k1gInSc4
Strážný	strážný	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
Philippsreut	Philippsreut	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
kapli	kaple	k1gFnSc3
vede	vést	k5eAaImIp3nS
Křížová	Křížová	k1gFnSc1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
–	–	k?
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
Stožecké	Stožecký	k2eAgFnSc6d1
skále	skála	k1gFnSc6
pravděpodobně	pravděpodobně	k6eAd1
k	k	k7c3
ochraně	ochrana	k1gFnSc3
Zlaté	zlatý	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
<g/>
,	,	kIx,
údajně	údajně	k6eAd1
ho	on	k3xPp3gMnSc4
nechal	nechat	k5eAaPmAgMnS
vystavět	vystavět	k5eAaPmF
děkan	děkan	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zpustl	zpustnout	k5eAaPmAgInS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
více	hodně	k6eAd2
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
historii	historie	k1gFnSc6
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
dokonce	dokonce	k9
se	se	k3xPyFc4
nedochoval	dochovat	k5eNaPmAgInS
ani	ani	k8xC
název	název	k1gInSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
z	z	k7c2
něj	on	k3xPp3gInSc2
zbyly	zbýt	k5eAaPmAgInP
pouze	pouze	k6eAd1
ruiny	ruina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
Hlava	hlava	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Steinköpfelhäuser	Steinköpfelhäuser	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
zaniklá	zaniklý	k2eAgFnSc1d1
osada	osada	k1gFnSc1
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schönberg	Schönberg	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
zaniklá	zaniklý	k2eAgFnSc1d1
osada	osada	k1gFnSc1
</s>
<s>
Radvanovice	Radvanovice	k1gFnSc1
(	(	kIx(
<g/>
něm.	něm.	k?
Schillerberg	Schillerberg	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
zaniklá	zaniklý	k2eAgFnSc1d1
šumavská	šumavský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
</s>
<s>
Jelení	jelení	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Spálený	spálený	k2eAgInSc1d1
luh	luh	k1gInSc1
–	–	k?
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
</s>
<s>
Stožec	Stožec	k1gInSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Stožecká	Stožecký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
zóna	zóna	k1gFnSc1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
svahu	svah	k1gInSc6
Stožecké	Stožecký	k2eAgFnSc2d1
skály	skála	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
chráněná	chráněný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
s	s	k7c7
původním	původní	k2eAgInSc7d1
porostem	porost	k1gInSc7
klenové	klenový	k2eAgFnSc2d1
jedlobučiny	jedlobučina	k1gFnSc2
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
Boubínskému	boubínský	k2eAgInSc3d1
pralesu	prales	k1gInSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
nádherný	nádherný	k2eAgInSc1d1
výhled	výhled	k1gInSc1
do	do	k7c2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobný	obdobný	k2eAgInSc4d1
větší	veliký	k2eAgInSc4d2
komplex	komplex	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
zóny	zóna	k1gFnPc4
lze	lze	k6eAd1
najít	najít	k5eAaPmF
též	též	k9
na	na	k7c6
samotném	samotný	k2eAgInSc6d1
vrcholu	vrchol	k1gInSc6
Stožec	Stožec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Stožeckou	Stožecký	k2eAgFnSc4d1
skálu	skála	k1gFnSc4
–	–	k?
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Třístoličník	Třístoličník	k1gInSc1
–	–	k?
hraniční	hraniční	k2eAgFnSc1d1
hora	hora	k1gFnSc1
s	s	k7c7
výhledem	výhled	k1gInSc7
a	a	k8xC
turistickou	turistický	k2eAgFnSc7d1
chatou	chata	k1gFnSc7
</s>
<s>
Schwarzenberský	schwarzenberský	k2eAgInSc1d1
plavební	plavební	k2eAgInSc1d1
kanál	kanál	k1gInSc1
–	–	k?
začínající	začínající	k2eAgFnSc1d1
asi	asi	k9
5	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Stožce	Stožka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
–	–	k?
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
prochází	procházet	k5eAaImIp3nS
Zlatá	zlatý	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Soumarský	soumarský	k2eAgInSc1d1
most	most	k1gInSc1
–	–	k?
dříve	dříve	k6eAd2
historický	historický	k2eAgInSc4d1
dřevěný	dřevěný	k2eAgInSc4d1
most	most	k1gInSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
nahrazen	nahradit	k5eAaPmNgMnS
betonovou	betonový	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
výchozí	výchozí	k2eAgNnSc1d1
místo	místo	k1gNnSc1
pro	pro	k7c4
vodáckou	vodácký	k2eAgFnSc4d1
turistiku	turistika	k1gFnSc4
skrze	skrze	k?
Vltavský	vltavský	k2eAgInSc4d1
luh	luh	k1gInSc4
</s>
<s>
Lenora	Lenora	k1gFnSc1
–	–	k?
šumavská	šumavský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
se	s	k7c7
sklářskou	sklářský	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
,	,	kIx,
výchozí	výchozí	k2eAgNnSc1d1
místo	místo	k1gNnSc1
pro	pro	k7c4
vodáckou	vodácký	k2eAgFnSc4d1
turistiku	turistika	k1gFnSc4
</s>
<s>
Areál	areál	k1gInSc1
lesních	lesní	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
Medvědí	medvědí	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
</s>
<s>
Stožec	Stožec	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Stožec	Stožec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
<g/>
,	,	kIx,
Horní	horní	k2eAgInSc1d1
Cazov	Cazov	k1gInSc1
a	a	k8xC
Radvanovice	Radvanovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dobrá	dobrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
ZSJ	ZSJ	kA
<g/>
:	:	kIx,
Černý	Černý	k1gMnSc1
Kříž	Kříž	k1gMnSc1
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
<g/>
,	,	kIx,
Dobrá	dobrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Horní	horní	k2eAgInSc1d1
Cazov	Cazov	k1gInSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
Radvanovice	Radvanovice	k1gFnSc1
a	a	k8xC
Stožec	Stožec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k6eAd1
zmíněných	zmíněný	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
obce	obec	k1gFnSc2
nalézají	nalézat	k5eAaImIp3nP
zaniklé	zaniklý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Krásná	krásný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
České	český	k2eAgInPc1d1
Žleby	žleb	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Brod	Brod	k1gInSc1
(	(	kIx(
<g/>
Grasfurth	Grasfurth	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
pjd	pjd	k?
<g/>
.	.	kIx.
<g/>
ecn	ecn	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VOTOČEK	VOTOČEK	kA
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monografie	monografie	k1gFnSc1
česjkoslovenských	česjkoslovenský	k2eAgFnPc2d1
známek	známka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
234	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1950	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změnách	změna	k1gFnPc6
úředních	úřední	k2eAgInPc2d1
názvů	název	k1gInPc2
míst	místo	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Část	část	k1gFnSc1
obce	obec	k1gFnSc2
Brod	Brod	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genea	Genea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČERNÝ	Černý	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Poutní	poutní	k2eAgNnPc1d1
místa	místo	k1gNnPc1
jižních	jižní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milostné	milostný	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
<g/>
,	,	kIx,
sochy	socha	k1gFnPc1
a	a	k8xC
místa	místo	k1gNnPc1
zvláštní	zvláštní	k2eAgFnSc2d1
zbožnosti	zbožnost	k1gFnSc2
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
České	český	k2eAgFnSc2d1
Žleby	žleb	k1gInPc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Šumava	Šumava	k1gFnSc1
</s>
<s>
Mikroregion	mikroregion	k1gInSc1
Horní	horní	k2eAgFnSc1d1
Vltava	Vltava	k1gFnSc1
–	–	k?
Boubínsko	Boubínsko	k1gNnSc1
</s>
<s>
Svazek	svazek	k1gInSc1
lipenských	lipenský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
mostů	most	k1gInPc2
přes	přes	k7c4
Vltavu	Vltava	k1gFnSc4
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Číčenice	Číčenice	k1gFnSc2
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Údolí	údolí	k1gNnSc1
</s>
<s>
Železniční	železniční	k2eAgInPc1d1
hraniční	hraniční	k2eAgInPc1d1
přechody	přechod	k1gInPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Železniční	železniční	k2eAgInSc4d1
hraniční	hraniční	k2eAgInSc4d1
přechod	přechod	k1gInSc4
Stožec	Stožec	k1gMnSc1
<g/>
–	–	k?
<g/>
Haidmühle	Haidmühle	k1gMnSc1
</s>
<s>
Haidmühle	Haidmühle	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stožec	Stožec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stožec	Stožec	k1gInSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
Stožec	Stožec	k1gInSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
České	český	k2eAgFnPc1d1
ŽlebyDobráStožec	ŽlebyDobráStožec	k1gMnSc1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Černý	Černý	k1gMnSc1
KřížHorní	KřížHorní	k2eAgFnSc1d1
CazovKrásná	CazovKrásný	k2eAgFnSc1d1
HoraNové	Horanové	k2eAgFnSc1d1
ÚdolíRadvanovice	ÚdolíRadvanovice	k1gFnSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
Babice	Babice	k1gFnPc4
•	•	k?
Bohumilice	Bohumilice	k1gFnPc4
•	•	k?
Bohunice	Bohunice	k1gFnPc4
•	•	k?
Borová	borový	k2eAgFnSc1d1
Lada	Lada	k1gFnSc1
•	•	k?
Bošice	Bošice	k1gFnSc2
•	•	k?
Budkov	Budkov	k1gInSc1
•	•	k?
Buk	buk	k1gInSc1
•	•	k?
Bušanovice	Bušanovice	k1gFnSc2
•	•	k?
Čkyně	Čkyeň	k1gFnSc2
•	•	k?
Drslavice	Drslavice	k1gFnSc2
•	•	k?
Dub	Dub	k1gMnSc1
•	•	k?
Dvory	Dvůr	k1gInPc1
•	•	k?
Horní	horní	k2eAgFnSc2d1
Vltavice	Vltavice	k1gFnSc2
•	•	k?
Hracholusky	Hracholuska	k1gFnSc2
•	•	k?
Husinec	Husinec	k1gInSc1
•	•	k?
Chlumany	Chluman	k1gInPc4
•	•	k?
Chroboly	Chrobola	k1gFnSc2
•	•	k?
Chvalovice	Chvalovice	k1gFnSc2
•	•	k?
Kratušín	Kratušín	k1gInSc1
•	•	k?
Křišťanov	Křišťanov	k1gInSc1
•	•	k?
Ktiš	Ktiš	k1gInSc1
•	•	k?
Kubova	Kubův	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Kvilda	Kvildo	k1gNnSc2
•	•	k?
Lažiště	Lažiště	k1gNnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lčovice	Lčovice	k1gFnSc1
•	•	k?
Lenora	Lenora	k1gFnSc1
•	•	k?
Lhenice	Lhenice	k1gFnSc2
•	•	k?
Lipovice	Lipovice	k1gFnSc2
•	•	k?
Lužice	Lužice	k1gFnSc2
•	•	k?
Mahouš	Mahouš	k1gMnSc1
•	•	k?
Malovice	Malovice	k1gFnSc2
•	•	k?
Mičovice	Mičovice	k1gFnSc2
•	•	k?
Nebahovy	Nebahova	k1gFnSc2
•	•	k?
Němčice	Němčice	k1gFnSc2
•	•	k?
Netolice	Netolice	k1gFnPc4
•	•	k?
Nicov	Nicov	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
•	•	k?
Nové	Nová	k1gFnSc2
Hutě	huť	k1gFnSc2
•	•	k?
Olšovice	Olšovice	k1gFnSc2
•	•	k?
Pěčnov	Pěčnov	k1gInSc1
•	•	k?
Prachatice	Prachatice	k1gFnPc4
•	•	k?
Radhostice	Radhostika	k1gFnSc6
•	•	k?
Stachy	Stachy	k1gInPc1
•	•	k?
Stožec	Stožec	k1gInSc1
•	•	k?
Strážný	strážný	k2eAgInSc1d1
•	•	k?
Strunkovice	Strunkovice	k1gFnSc1
nad	nad	k7c7
Blanicí	Blanice	k1gFnSc7
•	•	k?
Svatá	svatá	k1gFnSc1
Maří	mařit	k5eAaImIp3nS
•	•	k?
Šumavské	šumavský	k2eAgFnSc2d1
Hoštice	Hoštice	k1gFnSc2
•	•	k?
Těšovice	Těšovice	k1gFnSc2
•	•	k?
Tvrzice	Tvrzice	k?
•	•	k?
Újezdec	Újezdec	k1gMnSc1
•	•	k?
Vacov	Vacov	k1gInSc1
•	•	k?
Vimperk	Vimperk	k1gInSc1
•	•	k?
Vitějovice	Vitějovice	k1gFnSc2
•	•	k?
Vlachovo	Vlachův	k2eAgNnSc1d1
Březí	březí	k1gNnSc1
•	•	k?
Volary	Volara	k1gFnSc2
•	•	k?
Vrbice	vrbice	k1gFnSc1
•	•	k?
Záblatí	Záblatí	k1gNnSc2
•	•	k?
Zábrdí	Zábrdí	k1gNnSc2
•	•	k?
Zálezly	Zálezly	k1gInPc1
•	•	k?
Zbytiny	Zbytina	k1gFnSc2
•	•	k?
Zdíkov	Zdíkov	k1gInSc1
•	•	k?
Žárovná	Žárovný	k2eAgFnSc1d1
•	•	k?
Želnava	Želnava	k1gFnSc1
•	•	k?
Žernovice	Žernovice	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
