<s>
Lucerna	lucerna	k1gFnSc1	lucerna
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Režii	režie	k1gFnSc4	režie
premiérového	premiérový	k2eAgNnSc2d1	premiérové
představení	představení	k1gNnSc2	představení
měl	mít	k5eAaImAgMnS	mít
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autorovu	autorův	k2eAgFnSc4d1	autorova
nejznámější	známý	k2eAgFnSc4d3	nejznámější
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
nejhranější	hraný	k2eAgFnSc4d3	nejhranější
původní	původní	k2eAgFnSc4d1	původní
českou	český	k2eAgFnSc4d1	Česká
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
televizní	televizní	k2eAgFnSc1d1	televizní
verze	verze	k1gFnSc1	verze
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
režii	režie	k1gFnSc4	režie
tohoto	tento	k3xDgNnSc2	tento
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
provedl	provést	k5eAaPmAgMnS	provést
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
časově	časově	k6eAd1	časově
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
historického	historický	k2eAgNnSc2d1	historické
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sedláci	sedlák	k1gMnPc1	sedlák
dožadovali	dožadovat	k5eAaImAgMnP	dožadovat
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
a	a	k8xC	a
povinné	povinný	k2eAgFnSc2d1	povinná
roboty	robota	k1gFnSc2	robota
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
krásu	krása	k1gFnSc4	krása
české	český	k2eAgFnSc2d1	Česká
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
lidovou	lidový	k2eAgFnSc4d1	lidová
slovesnost	slovesnost	k1gFnSc4	slovesnost
a	a	k8xC	a
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hold	hold	k1gInSc4	hold
moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
citu	cit	k1gInSc2	cit
prostých	prostý	k2eAgMnPc2d1	prostý
českých	český	k2eAgMnPc2d1	český
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pohádkovou	pohádkový	k2eAgFnSc4d1	pohádková
hru	hra	k1gFnSc4	hra
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
mlýně	mlýn	k1gInSc6	mlýn
<g/>
,	,	kIx,	,
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
v	v	k7c6	v
lese	les	k1gInSc6	les
mezi	mezi	k7c7	mezi
mlýnem	mlýn	k1gInSc7	mlýn
a	a	k8xC	a
lesním	lesní	k2eAgInSc7d1	lesní
zámečkem	zámeček	k1gInSc7	zámeček
a	a	k8xC	a
v	v	k7c6	v
lesním	lesní	k2eAgInSc6d1	lesní
zámečku	zámeček	k1gInSc6	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
mladší	mladý	k2eAgMnSc1d2	mladší
mlynář	mlynář	k1gMnSc1	mlynář
Libor	Libor	k1gMnSc1	Libor
společně	společně	k6eAd1	společně
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
babičkou	babička	k1gFnSc7	babička
a	a	k8xC	a
schovankou	schovanka	k1gFnSc7	schovanka
Haničkou	Hanička	k1gFnSc7	Hanička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mlýnském	mlýnský	k2eAgInSc6d1	mlýnský
náhonu	náhon	k1gInSc6	náhon
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
vodník	vodník	k1gMnSc1	vodník
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
mladé	mladý	k2eAgFnSc2d1	mladá
Haničky	Hanička	k1gFnSc2	Hanička
zamilován	zamilován	k2eAgInSc1d1	zamilován
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadil	usadit	k5eAaPmAgMnS	usadit
i	i	k9	i
starý	starý	k2eAgMnSc1d1	starý
vodník	vodník	k1gMnSc1	vodník
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
panuje	panovat	k5eAaImIp3nS	panovat
zlý	zlý	k2eAgMnSc1d1	zlý
správce	správce	k1gMnSc1	správce
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
také	také	k9	také
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
Haničce	Hanička	k1gFnSc6	Hanička
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
odvést	odvést	k5eAaPmF	odvést
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
nekalými	kalý	k2eNgInPc7d1	nekalý
pokoutními	pokoutní	k2eAgInPc7d1	pokoutní
úmysly	úmysl	k1gInPc7	úmysl
jako	jako	k8xC	jako
služku	služka	k1gFnSc4	služka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
usiluje	usilovat	k5eAaImIp3nS	usilovat
porazit	porazit	k5eAaPmF	porazit
i	i	k9	i
velkou	velký	k2eAgFnSc4d1	velká
a	a	k8xC	a
starou	starý	k2eAgFnSc4d1	stará
lípu	lípa	k1gFnSc4	lípa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
úctě	úcta	k1gFnSc6	úcta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
okamžiku	okamžik	k1gInSc6	okamžik
děje	dít	k5eAaImIp3nS	dít
však	však	k9	však
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
nečekaně	nečekaně	k6eAd1	nečekaně
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
panstvo	panstvo	k1gNnSc4	panstvo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
paní	paní	k1gFnSc2	paní
kněžny	kněžna	k1gFnSc2	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
se	se	k3xPyFc4	se
nudí	nudit	k5eAaImIp3nS	nudit
a	a	k8xC	a
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
rozptýlení	rozptýlení	k1gNnSc6	rozptýlení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
lesnímu	lesní	k2eAgInSc3d1	lesní
zámečku	zámeček	k1gInSc3	zámeček
kolem	kolem	k7c2	kolem
Liborova	Liborův	k2eAgInSc2d1	Liborův
mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Mlynář	Mlynář	k1gMnSc1	Mlynář
Libor	Libor	k1gMnSc1	Libor
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nemusí	muset	k5eNaImIp3nS	muset
robotovat	robotovat	k5eAaImF	robotovat
na	na	k7c6	na
panském	panské	k1gNnSc6	panské
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
vůči	vůči	k7c3	vůči
vrchnosti	vrchnost	k1gFnSc3	vrchnost
jednou	jednou	k6eAd1	jednou
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
prastarou	prastarý	k2eAgFnSc4d1	prastará
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
-li	i	k?	-li
vrchnost	vrchnost	k1gFnSc4	vrchnost
o	o	k7c4	o
doprovod	doprovod	k1gInSc4	doprovod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
ji	on	k3xPp3gFnSc4	on
doprovázet	doprovázet	k5eAaImF	doprovázet
přes	přes	k7c4	přes
les	les	k1gInSc4	les
a	a	k8xC	a
svítit	svítit	k5eAaImF	svítit
jí	on	k3xPp3gFnSc7	on
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
starou	starý	k2eAgFnSc7d1	stará
lucernou	lucerna	k1gFnSc7	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
mlynáři	mlynář	k1gMnPc1	mlynář
poručí	poručit	k5eAaPmIp3nP	poručit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svoji	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
splnil	splnit	k5eAaPmAgMnS	splnit
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
podvolí	podvolit	k5eAaPmIp3nS	podvolit
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
se	se	k3xPyFc4	se
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
zámečku	zámeček	k1gInSc2	zámeček
s	s	k7c7	s
mlynářem	mlynář	k1gMnSc7	mlynář
sblíží	sblížit	k5eAaPmIp3nS	sblížit
<g/>
,	,	kIx,	,
svobodomyslný	svobodomyslný	k2eAgMnSc1d1	svobodomyslný
mlynář	mlynář	k1gMnSc1	mlynář
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
velmi	velmi	k6eAd1	velmi
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
odjel	odjet	k5eAaPmAgMnS	odjet
pryč	pryč	k6eAd1	pryč
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
sympatického	sympatický	k2eAgMnSc2d1	sympatický
venkovského	venkovský	k2eAgMnSc2d1	venkovský
mlynáře	mlynář	k1gMnSc2	mlynář
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zapůsobí	zapůsobit	k5eAaPmIp3nS	zapůsobit
i	i	k9	i
krásná	krásný	k2eAgFnSc1d1	krásná
česká	český	k2eAgFnSc1d1	Česká
příroda	příroda	k1gFnSc1	příroda
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
též	též	k9	též
život	život	k1gInSc1	život
a	a	k8xC	a
city	cit	k1gInPc1	cit
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
dějovou	dějový	k2eAgFnSc4d1	dějová
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nS	tvořit
příběh	příběh	k1gInSc1	příběh
učitelského	učitelský	k2eAgMnSc2d1	učitelský
pomocníka	pomocník	k1gMnSc2	pomocník
(	(	kIx(	(
<g/>
mládence	mládenec	k1gMnSc2	mládenec
<g/>
)	)	kIx)	)
Zajíčka	Zajíček	k1gMnSc2	Zajíček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velmi	velmi	k6eAd1	velmi
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
paní	paní	k1gFnSc1	paní
kněžna	kněžna	k1gFnSc1	kněžna
vyslechla	vyslechnout	k5eAaPmAgFnS	vyslechnout
a	a	k8xC	a
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
definitivním	definitivní	k2eAgMnSc7d1	definitivní
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
sepsat	sepsat	k5eAaPmF	sepsat
supliku	supliku	k?	supliku
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
a	a	k8xC	a
společně	společně	k6eAd1	společně
partou	parta	k1gFnSc7	parta
místních	místní	k2eAgMnPc2d1	místní
lidových	lidový	k2eAgMnPc2d1	lidový
muzikantů	muzikant	k1gMnPc2	muzikant
zahrát	zahrát	k5eAaPmF	zahrát
paní	paní	k1gFnSc4	paní
kněžně	kněžna	k1gFnSc3	kněžna
kasaci	kasace	k1gFnSc3	kasace
a	a	k8xC	a
vyprosit	vyprosit	k5eAaPmF	vyprosit
si	se	k3xPyFc3	se
tak	tak	k9	tak
její	její	k3xOp3gFnSc4	její
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Muzikanti	muzikant	k1gMnPc1	muzikant
však	však	k9	však
v	v	k7c6	v
lese	les	k1gInSc6	les
zabloudí	zabloudit	k5eAaPmIp3nS	zabloudit
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Správce	správce	k1gMnSc1	správce
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
odvést	odvést	k5eAaPmF	odvést
násilím	násilí	k1gNnSc7	násilí
Haničku	Hanička	k1gFnSc4	Hanička
z	z	k7c2	z
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovšem	ovšem	k9	ovšem
pan	pan	k1gMnSc1	pan
správce	správce	k1gMnSc1	správce
také	také	k9	také
hodlá	hodlat	k5eAaImIp3nS	hodlat
porazit	porazit	k5eAaPmF	porazit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
paní	paní	k1gFnSc2	paní
kněžny	kněžna	k1gFnSc2	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Mlynář	Mlynář	k1gMnSc1	Mlynář
dovede	dovést	k5eAaPmIp3nS	dovést
paní	paní	k1gFnSc4	paní
kněžnu	kněžna	k1gFnSc4	kněžna
na	na	k7c4	na
lesní	lesní	k2eAgInSc4d1	lesní
zámeček	zámeček	k1gInSc4	zámeček
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
bránit	bránit	k5eAaImF	bránit
starou	starý	k2eAgFnSc4d1	stará
lípu	lípa	k1gFnSc4	lípa
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
muzikanti	muzikant	k1gMnPc1	muzikant
a	a	k8xC	a
učitelský	učitelský	k2eAgMnSc1d1	učitelský
pomocník	pomocník	k1gMnSc1	pomocník
Zajíček	Zajíček	k1gMnSc1	Zajíček
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
hry	hra	k1gFnSc2	hra
nakonec	nakonec	k6eAd1	nakonec
vše	všechen	k3xTgNnSc4	všechen
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
lípu	lípa	k1gFnSc4	lípa
porazit	porazit	k5eAaPmF	porazit
nenechá	nechat	k5eNaPmIp3nS	nechat
<g/>
,	,	kIx,	,
Zajíčka	Zajíček	k1gMnSc2	Zajíček
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
rozbitím	rozbití	k1gNnSc7	rozbití
lucerny	lucerna	k1gFnSc2	lucerna
zruší	zrušit	k5eAaPmIp3nS	zrušit
onu	onen	k3xDgFnSc4	onen
prastarou	prastarý	k2eAgFnSc4d1	prastará
mlynářovu	mlynářův	k2eAgFnSc4d1	mlynářova
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
Lucerny	lucerna	k1gFnSc2	lucerna
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
autora	autor	k1gMnSc2	autor
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
mlýn	mlýn	k1gInSc4	mlýn
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Hronově	Hronov	k1gInSc6	Hronov
i	i	k8xC	i
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
litomyšlském	litomyšlský	k2eAgNnSc6d1	litomyšlské
předměstí	předměstí	k1gNnSc6	předměstí
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
litomyšlského	litomyšlský	k2eAgInSc2d1	litomyšlský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zpracování	zpracování	k1gNnSc4	zpracování
tohoto	tento	k3xDgNnSc2	tento
tématu	téma	k1gNnSc2	téma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
Jirásek	Jirásek	k1gMnSc1	Jirásek
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
začaté	začatá	k1gFnSc2	začatá
r.	r.	kA	r.
1893	[number]	k4	1893
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
publikováno	publikovat	k5eAaBmNgNnS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
19	[number]	k4	19
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
Lucerny	lucerna	k1gFnSc2	lucerna
napsal	napsat	k5eAaPmAgMnS	napsat
Jirásek	Jirásek	k1gMnSc1	Jirásek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgNnP	být
vydána	vydat	k5eAaPmNgNnP	vydat
knižně	knižně	k6eAd1	knižně
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
hru	hra	k1gFnSc4	hra
nazvat	nazvat	k5eAaPmF	nazvat
"	"	kIx"	"
<g/>
Naše	náš	k3xOp1gFnSc1	náš
lípa	lípa	k1gFnSc1	lípa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Kvapila	Kvapil	k1gMnSc2	Kvapil
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
název	název	k1gInSc1	název
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
posledním	poslední	k2eAgNnSc7d1	poslední
představením	představení	k1gNnSc7	představení
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
záborem	zábor	k1gInSc7	zábor
nacistickými	nacistický	k2eAgInPc7d1	nacistický
okupačními	okupační	k2eAgInPc7d1	okupační
úřady	úřad	k1gInPc7	úřad
a	a	k8xC	a
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
touto	tento	k3xDgFnSc7	tento
hrou	hra	k1gFnSc7	hra
poválečný	poválečný	k2eAgInSc4d1	poválečný
provoz	provoz	k1gInSc4	provoz
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Cizojazyčná	cizojazyčný	k2eAgNnPc1d1	cizojazyčné
vydání	vydání	k1gNnPc1	vydání
Lucerny	lucerna	k1gFnSc2	lucerna
<g/>
:	:	kIx,	:
anglicky	anglicky	k6eAd1	anglicky
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
lanterne	lanternout	k5eAaPmIp3nS	lanternout
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Laterne	Latern	k1gInSc5	Latern
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
(	(	kIx(	(
<g/>
Lampáš	lampáš	k1gInSc1	lampáš
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
–	–	k?	–
opera	opera	k1gFnSc1	opera
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nováka	Novák	k1gMnSc2	Novák
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
Lucerna	lucerna	k1gFnSc1	lucerna
-	-	kIx~	-
němá	němý	k2eAgFnSc1d1	němá
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Lamače	lamač	k1gMnSc2	lamač
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
Lucerna	lucerna	k1gFnSc1	lucerna
-	-	kIx~	-
zvuková	zvukový	k2eAgFnSc1d1	zvuková
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Lamače	lamač	k1gMnSc2	lamač
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
Lucerna	lucerna	k1gFnSc1	lucerna
-	-	kIx~	-
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Bělky	bělka	k1gFnSc2	bělka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Ladislav	Ladislav	k1gMnSc1	Ladislav
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
Lucerna	lucerna	k1gFnSc1	lucerna
-	-	kIx~	-
československá	československý	k2eAgFnSc1d1	Československá
televizní	televizní	k2eAgFnSc1d1	televizní
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Františka	František	k1gMnSc2	František
Filipa	Filip	k1gMnSc2	Filip
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
Lucerna	lucerna	k1gFnSc1	lucerna
-	-	kIx~	-
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
režiséra	režisér	k1gMnSc2	režisér
Přemysla	Přemysl	k1gMnSc2	Přemysl
Pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Marie	Marie	k1gFnSc1	Marie
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Le	Le	k1gFnSc2	Le
Breux	Breux	k1gInSc1	Breux
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Voska	Voska	k1gMnSc1	Voska
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
učitelský	učitelský	k2eAgMnSc1d1	učitelský
mládenec	mládenec	k1gMnSc1	mládenec
<g/>
)	)	kIx)	)
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
Lucerny	lucerna	k1gFnSc2	lucerna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
421	[number]	k4	421
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
330	[number]	k4	330
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Komentované	komentovaný	k2eAgNnSc1d1	komentované
vydání	vydání	k1gNnSc1	vydání
Lucerny	lucerna	k1gFnSc2	lucerna
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
tří	tři	k4xCgNnPc2	tři
pohádkových	pohádkový	k2eAgNnPc2d1	pohádkové
dramat	drama	k1gNnPc2	drama
<g/>
;	;	kIx,	;
komentáře	komentář	k1gInPc1	komentář
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
historickým	historický	k2eAgInSc7d1	historický
kontextem	kontext	k1gInSc7	kontext
i	i	k8xC	i
žánrovou	žánrový	k2eAgFnSc7d1	žánrová
charakteristikou	charakteristika	k1gFnSc7	charakteristika
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
TRÄGER	TRÄGER	kA	TRÄGER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
52	[number]	k4	52
s.	s.	k?	s.
FETTERS	FETTERS	kA	FETTERS
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
uvedení	uvedení	k1gNnSc2	uvedení
hry	hra	k1gFnSc2	hra
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
:	:	kIx,	:
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
sborník	sborník	k1gInSc4	sborník
k	k	k7c3	k
jubileu	jubileum	k1gNnSc3	jubileum
[	[	kIx(	[
<g/>
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
v	v	k7c6	v
Hronově	Hronov	k1gInSc6	Hronov
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hronov	Hronov	k1gInSc1	Hronov
<g/>
:	:	kIx,	:
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
23	[number]	k4	23
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
777	[number]	k4	777
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
jako	jako	k8xC	jako
e-kniha	eniha	k1gFnSc1	e-kniha
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
z	z	k7c2	z
katalogu	katalog	k1gInSc2	katalog
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
http://www.akaska.cz/?page=103	[url]	k4	http://www.akaska.cz/?page=103
Lucerna	lucerna	k1gFnSc1	lucerna
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
