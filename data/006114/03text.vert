<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
stupnice	stupnice	k1gFnSc1	stupnice
jaderných	jaderný	k2eAgFnPc2d1	jaderná
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
International	International	k1gMnSc1	International
Nuclear	Nuclear	k1gMnSc1	Nuclear
Event	Event	k1gMnSc1	Event
Scale	Scale	k1gFnSc2	Scale
–	–	k?	–
INES	INES	kA	INES
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osmistupňová	osmistupňový	k2eAgFnSc1d1	osmistupňová
škála	škála	k1gFnSc1	škála
<g/>
,	,	kIx,	,
zavedená	zavedený	k2eAgFnSc1d1	zavedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
pro	pro	k7c4	pro
posuzování	posuzování	k1gNnSc4	posuzování
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
havárií	havárie	k1gFnPc2	havárie
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Zavedly	zavést	k5eAaPmAgFnP	zavést
ji	on	k3xPp3gFnSc4	on
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
zkratka	zkratka	k1gFnSc1	zkratka
MAAE	MAAE	kA	MAAE
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
IAEA	IAEA	kA	IAEA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
OECD	OECD	kA	OECD
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
/	/	kIx~	/
<g/>
NEA	NEA	kA	NEA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stupně	stupeň	k1gInSc2	stupeň
7	[number]	k4	7
až	až	k8xS	až
4	[number]	k4	4
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
havárie	havárie	k1gFnPc4	havárie
<g/>
,	,	kIx,	,
3	[number]	k4	3
až	až	k9	až
1	[number]	k4	1
jako	jako	k9	jako
nehody	nehoda	k1gFnPc1	nehoda
<g/>
,	,	kIx,	,
0	[number]	k4	0
nemá	mít	k5eNaImIp3nS	mít
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Major	major	k1gMnSc1	major
accident	accident	k1gMnSc1	accident
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
možná	možný	k2eAgFnSc1d1	možná
havárie	havárie	k1gFnSc1	havárie
Únik	únik	k1gInSc1	únik
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
z	z	k7c2	z
jaderného	jaderný	k2eAgNnSc2d1	jaderné
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
energetického	energetický	k2eAgInSc2d1	energetický
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
)	)	kIx)	)
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
směs	směs	k1gFnSc1	směs
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
štěpných	štěpný	k2eAgInPc2d1	štěpný
produktů	produkt	k1gInPc2	produkt
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
i	i	k8xC	i
krátkými	krátký	k2eAgInPc7d1	krátký
poločasy	poločas	k1gInPc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
(	(	kIx(	(
<g/>
s	s	k7c7	s
aktivitou	aktivita	k1gFnSc7	aktivita
přesahující	přesahující	k2eAgFnSc7d1	přesahující
104	[number]	k4	104
TBq	TBq	k1gFnSc1	TBq
131I	[number]	k4	131I
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
<g />
.	.	kIx.	.
</s>
<s>
podobně	podobně	k6eAd1	podobně
biologicky	biologicky	k6eAd1	biologicky
významných	významný	k2eAgInPc2d1	významný
radionuklidů	radionuklid	k1gInPc2	radionuklid
<g/>
)	)	kIx)	)
Možnost	možnost	k1gFnSc4	možnost
akutních	akutní	k2eAgInPc2d1	akutní
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
účinků	účinek	k1gInPc2	účinek
<g/>
;	;	kIx,	;
zpožděné	zpožděný	k2eAgInPc4d1	zpožděný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
účinky	účinek	k1gInPc4	účinek
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
zasažení	zasažení	k1gNnSc2	zasažení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
země	zem	k1gFnSc2	zem
Dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
důsledky	důsledek	k1gInPc1	důsledek
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Černobyl	Černobyl	k1gInSc1	Černobyl
<g/>
,	,	kIx,	,
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k9	i
<g/>
,	,	kIx,	,
Prefektura	prefektura	k1gFnSc1	prefektura
Fukušima	Fukušima	k1gFnSc1	Fukušima
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
překlasifikováno	překlasifikovat	k5eAaPmNgNnS	překlasifikovat
ze	z	k7c2	z
stupně	stupeň	k1gInSc2	stupeň
INES	INES	kA	INES
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
Japonskou	japonský	k2eAgFnSc7d1	japonská
agenturou	agentura	k1gFnSc7	agentura
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
Serious	Serious	k1gInSc1	Serious
accident	accident	k1gMnSc1	accident
Únik	únik	k1gInSc1	únik
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
s	s	k7c7	s
řádovou	řádový	k2eAgFnSc7d1	řádová
aktivitou	aktivita	k1gFnSc7	aktivita
103	[number]	k4	103
až	až	k9	až
104	[number]	k4	104
TBq	TBq	k1gFnSc1	TBq
131I	[number]	k4	131I
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
podobně	podobně	k6eAd1	podobně
biologicky	biologicky	k6eAd1	biologicky
významných	významný	k2eAgInPc2d1	významný
radionuklidů	radionuklid	k1gInPc2	radionuklid
<g/>
)	)	kIx)	)
Plné	plný	k2eAgNnSc4d1	plné
uplatnění	uplatnění	k1gNnSc4	uplatnění
opatření	opatření	k1gNnPc2	opatření
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
následků	následek	k1gInPc2	následek
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zahrnutých	zahrnutý	k2eAgInPc2d1	zahrnutý
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
havarijních	havarijní	k2eAgInPc6d1	havarijní
plánech	plán	k1gInPc6	plán
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
přepracovatelský	přepracovatelský	k2eAgInSc1d1	přepracovatelský
závod	závod	k1gInSc1	závod
<g />
.	.	kIx.	.
</s>
<s>
Majak	Majak	k1gInSc1	Majak
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Kyštym	Kyštym	k1gInSc1	Kyštym
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čeljabinská	čeljabinský	k2eAgFnSc1d1	Čeljabinská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Ruská	ruský	k2eAgFnSc1d1	ruská
SFSR	SFSR	kA	SFSR
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1957	[number]	k4	1957
Accident	Accident	k1gMnSc1	Accident
with	with	k1gMnSc1	with
off-site	offit	k1gInSc5	off-sit
risk	risk	k1gInSc1	risk
Únik	únik	k1gInSc1	únik
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
s	s	k7c7	s
aktivitou	aktivita	k1gFnSc7	aktivita
stovek	stovka	k1gFnPc2	stovka
až	až	k9	až
tisíců	tisíc	k4xCgInPc2	tisíc
TBq	TBq	k1gMnPc7	TBq
131I	[number]	k4	131I
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
podobně	podobně	k6eAd1	podobně
biologicky	biologicky	k6eAd1	biologicky
významných	významný	k2eAgInPc2d1	významný
radionuklidů	radionuklid	k1gInPc2	radionuklid
)	)	kIx)	)
Částečné	částečný	k2eAgNnSc4d1	částečné
uplatnění	uplatnění	k1gNnSc4	uplatnění
opatření	opatření	k1gNnPc2	opatření
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
následků	následek	k1gInPc2	následek
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zahrnutých	zahrnutý	k2eAgInPc2d1	zahrnutý
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
havarijních	havarijní	k2eAgInPc6d1	havarijní
plánech	plán	k1gInPc6	plán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
evakuace	evakuace	k1gFnSc2	evakuace
<g/>
,	,	kIx,	,
ukrytí	ukrytý	k2eAgMnPc1d1	ukrytý
<g/>
)	)	kIx)	)
Těžké	těžký	k2eAgNnSc1d1	těžké
poškození	poškození	k1gNnSc1	poškození
jaderného	jaderný	k2eAgNnSc2d1	jaderné
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
těžké	těžký	k2eAgNnSc1d1	těžké
poškození	poškození	k1gNnSc1	poškození
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
energetického	energetický	k2eAgInSc2d1	energetický
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
havárie	havárie	k1gFnSc1	havárie
s	s	k7c7	s
kritičností	kritičnost	k1gFnSc7	kritičnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
či	či	k8xC	či
exploze	exploze	k1gFnSc1	exploze
uvolňující	uvolňující	k2eAgFnSc1d1	uvolňující
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
uvnitř	uvnitř	k7c2	uvnitř
zařízení	zařízení	k1gNnSc2	zařízení
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
komplex	komplex	k1gInSc1	komplex
Windscale	Windscala	k1gFnSc6	Windscala
Pile	pila	k1gFnSc6	pila
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Three	Thre	k1gMnPc4	Thre
Mile	mile	k6eAd1	mile
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1979	[number]	k4	1979
Accident	Accident	k1gMnSc1	Accident
without	without	k1gMnSc1	without
off-site	offit	k1gInSc5	off-sit
risk	risk	k1gInSc1	risk
Únik	únik	k1gInSc1	únik
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
dávky	dávka	k1gFnSc2	dávka
pro	pro	k7c4	pro
nejvíc	nejvíc	k6eAd1	nejvíc
zasaženou	zasažený	k2eAgFnSc4d1	zasažená
skupinu	skupina	k1gFnSc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
několika	několik	k4yIc2	několik
mSv	mSv	k?	mSv
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
)	)	kIx)	)
Potřeba	potřeba	k6eAd1	potřeba
havarijních	havarijní	k2eAgNnPc2d1	havarijní
opatření	opatření	k1gNnPc2	opatření
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
místní	místní	k2eAgFnSc2d1	místní
kontroly	kontrola	k1gFnSc2	kontrola
potravin	potravina	k1gFnPc2	potravina
Významné	významný	k2eAgNnSc1d1	významné
poškození	poškození	k1gNnSc1	poškození
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
částečné	částečný	k2eAgNnSc1d1	částečné
tavení	tavení	k1gNnSc1	tavení
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
v	v	k7c6	v
energetickém	energetický	k2eAgInSc6d1	energetický
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
a	a	k8xC	a
srovnatelné	srovnatelný	k2eAgFnPc4d1	srovnatelná
události	událost	k1gFnPc4	událost
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
bez	bez	k7c2	bez
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
)	)	kIx)	)
Ozáření	ozáření	k1gNnSc4	ozáření
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
až	až	k9	až
<g />
.	.	kIx.	.
</s>
<s>
jednotky	jednotka	k1gFnPc1	jednotka
Sv	sv	kA	sv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
rychlého	rychlý	k2eAgNnSc2d1	rychlé
úmrtí	úmrtí	k1gNnSc2	úmrtí
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Saint	Saint	k1gMnSc1	Saint
Laurent	Laurent	k1gMnSc1	Laurent
<g/>
,	,	kIx,	,
region	region	k1gInSc1	region
Centre-Val	Centre-Val	k1gInSc1	Centre-Val
de	de	k?	de
Loire	Loir	k1gMnSc5	Loir
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1969	[number]	k4	1969
přepracovatelský	přepracovatelský	k2eAgInSc1d1	přepracovatelský
závod	závod	k1gInSc1	závod
Sellafield	Sellafielda	k1gFnPc2	Sellafielda
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Jaslovské	Jaslovský	k2eAgFnPc1d1	Jaslovský
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1977	[number]	k4	1977
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Saint	Saint	k1gMnSc1	Saint
Laurent	Laurent	k1gMnSc1	Laurent
<g/>
,	,	kIx,	,
region	region	k1gInSc1	region
Centre-Val	Centre-Val	k1gInSc1	Centre-Val
de	de	k?	de
Loire	Loir	k1gMnSc5	Loir
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
kritický	kritický	k2eAgMnSc1d1	kritický
<g />
.	.	kIx.	.
</s>
<s>
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Serious	Serious	k1gInSc1	Serious
incident	incident	k1gInSc4	incident
Únik	únik	k1gInSc1	únik
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
materiálů	materiál	k1gInPc2	materiál
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
nad	nad	k7c4	nad
povolené	povolený	k2eAgInPc4d1	povolený
limity	limit	k1gInPc4	limit
<g/>
,	,	kIx,	,
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
dávky	dávka	k1gFnSc2	dávka
pro	pro	k7c4	pro
nejvíc	nejvíc	k6eAd1	nejvíc
zasaženou	zasažený	k2eAgFnSc4d1	zasažená
skupinu	skupina	k1gFnSc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desetin	desetina	k1gFnPc2	desetina
mSv	mSv	k?	mSv
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zlomky	zlomek	k1gInPc4	zlomek
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
)	)	kIx)	)
Nejsou	být	k5eNaImIp3nP	být
nutná	nutný	k2eAgNnPc4d1	nutné
opatření	opatření	k1gNnPc4	opatření
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Událost	událost	k1gFnSc4	událost
s	s	k7c7	s
důsledkem	důsledek	k1gInSc7	důsledek
těžkého	těžký	k2eAgNnSc2d1	těžké
<g />
.	.	kIx.	.
</s>
<s>
rozšíření	rozšíření	k1gNnSc1	rozšíření
kontaminace	kontaminace	k1gFnSc2	kontaminace
(	(	kIx(	(
<g/>
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
TBq	TBq	k1gMnPc2	TBq
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
)	)	kIx)	)
uvnitř	uvnitř	k7c2	uvnitř
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
selháním	selhání	k1gNnSc7	selhání
zařízení	zařízení	k1gNnPc2	zařízení
nebo	nebo	k8xC	nebo
provozní	provozní	k2eAgFnSc7d1	provozní
poruchou	porucha	k1gFnSc7	porucha
Takové	takový	k3xDgNnSc1	takový
ozáření	ozáření	k1gNnSc1	ozáření
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
akutní	akutní	k2eAgInPc4d1	akutní
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
následky	následek	k1gInPc4	následek
Všechny	všechen	k3xTgFnPc4	všechen
nehody	nehoda	k1gFnPc4	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
další	další	k2eAgFnSc1d1	další
porucha	porucha	k1gFnSc1	porucha
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
havarijním	havarijní	k2eAgFnPc3d1	havarijní
podmínkám	podmínka	k1gFnPc3	podmínka
nebo	nebo	k8xC	nebo
situace	situace	k1gFnPc1	situace
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
systémy	systém	k1gInPc1	systém
schopné	schopný	k2eAgInPc1d1	schopný
"	"	kIx"	"
<g/>
zabránit	zabránit	k5eAaPmF	zabránit
havárii	havárie	k1gFnSc4	havárie
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
nastaly	nastat	k5eAaPmAgFnP	nastat
určité	určitý	k2eAgFnPc1d1	určitá
iniciační	iniciační	k2eAgFnPc1d1	iniciační
události	událost	k1gFnPc1	událost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Vandellò	Vandellò	k1gFnSc2	Vandellò
<g/>
,	,	kIx,	,
Katalánsko	Katalánsko	k1gNnSc1	Katalánsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušima	k1gFnSc1	Fukušima
II	II	kA	II
<g/>
,	,	kIx,	,
Prefektura	prefektura	k1gFnSc1	prefektura
Fukušima	Fukušima	k1gFnSc1	Fukušima
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
4	[number]	k4	4
<g/>
.	.	kIx.	.
blok	blok	k1gInSc1	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k9	i
<g/>
,	,	kIx,	,
Prefektura	prefektura	k1gFnSc1	prefektura
Fukušima	Fukušima	k1gFnSc1	Fukušima
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
?	?	kIx.	?
</s>
<s>
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
Incident	incident	k1gInSc1	incident
Technická	technický	k2eAgFnSc1d1	technická
porucha	porucha	k1gFnSc1	porucha
nebo	nebo	k8xC	nebo
odchylka	odchylka	k1gFnSc1	odchylka
s	s	k7c7	s
významným	významný	k2eAgNnSc7d1	významné
selháním	selhání	k1gNnSc7	selhání
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
zbývající	zbývající	k2eAgFnSc7d1	zbývající
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
hloubkovou	hloubkový	k2eAgFnSc7d1	hloubková
ochranou	ochrana	k1gFnSc7	ochrana
k	k	k7c3	k
vypořádání	vypořádání	k1gNnSc3	vypořádání
se	se	k3xPyFc4	se
s	s	k7c7	s
dodatečnými	dodatečný	k2eAgFnPc7d1	dodatečná
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
jinak	jinak	k6eAd1	jinak
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
stupněm	stupeň	k1gInSc7	stupeň
1	[number]	k4	1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
významné	významný	k2eAgInPc4d1	významný
dodatečné	dodatečný	k2eAgInPc4d1	dodatečný
organizační	organizační	k2eAgInPc4d1	organizační
nedostatky	nedostatek	k1gInPc4	nedostatek
nebo	nebo	k8xC	nebo
nedostatky	nedostatek	k1gInPc4	nedostatek
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Ozáření	ozáření	k1gNnSc2	ozáření
pracovníka	pracovník	k1gMnSc2	pracovník
překračující	překračující	k2eAgInSc1d1	překračující
povolený	povolený	k2eAgInSc1d1	povolený
roční	roční	k2eAgInSc1d1	roční
limit	limit	k1gInSc1	limit
nebo	nebo	k8xC	nebo
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
významných	významný	k2eAgNnPc2d1	významné
množství	množství	k1gNnPc2	množství
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
uvnitř	uvnitř	k6eAd1	uvnitř
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc4	ten
projekt	projekt	k1gInSc1	projekt
nepředpokládal	předpokládat	k5eNaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgInPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Mihama	Mihama	k?	Mihama
<g/>
,	,	kIx,	,
Prefektura	prefektura	k1gFnSc1	prefektura
Fukui	Fukui	k1gNnSc2	Fukui
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1991	[number]	k4	1991
Anomaly	Anomaly	k1gFnSc4	Anomaly
Technická	technický	k2eAgFnSc1d1	technická
porucha	porucha	k1gFnSc1	porucha
nebo	nebo	k8xC	nebo
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
schváleného	schválený	k2eAgInSc2d1	schválený
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
zbývající	zbývající	k2eAgFnSc7d1	zbývající
významnou	významný	k2eAgFnSc7d1	významná
hloubkovou	hloubkový	k2eAgFnSc7d1	hloubková
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poruchy	porucha	k1gFnSc2	porucha
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgFnSc2d1	lidská
chyby	chyba	k1gFnSc2	chyba
nebo	nebo	k8xC	nebo
nedostatků	nedostatek	k1gInPc2	nedostatek
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
stupnice	stupnice	k1gFnSc1	stupnice
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
provoz	provoz	k1gInSc4	provoz
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
jaderným	jaderný	k2eAgNnSc7d1	jaderné
palivem	palivo	k1gNnSc7	palivo
a	a	k8xC	a
skladování	skladování	k1gNnSc6	skladování
odpadů	odpad	k1gInPc2	odpad
Deviation	Deviation	k1gInSc1	Deviation
<g/>
,	,	kIx,	,
no	no	k9	no
<g />
.	.	kIx.	.
</s>
<s>
safety	safet	k2eAgFnPc1d1	safet
relevance	relevance	k1gFnPc1	relevance
Odchylky	odchylka	k1gFnSc2	odchylka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
porušeny	porušen	k2eAgInPc4d1	porušen
limity	limit	k1gInPc4	limit
a	a	k8xC	a
podmínky	podmínka	k1gFnPc4	podmínka
(	(	kIx(	(
<g/>
=	=	kIx~	=
LaP	lap	k1gInSc1	lap
<g/>
)	)	kIx)	)
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bezpečně	bezpečně	k6eAd1	bezpečně
zvládnuty	zvládnut	k2eAgInPc1d1	zvládnut
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
postupy	postup	k1gInPc7	postup
Mezi	mezi	k7c4	mezi
příklady	příklad	k1gInPc4	příklad
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
náhodná	náhodný	k2eAgFnSc1d1	náhodná
porucha	porucha	k1gFnSc1	porucha
v	v	k7c6	v
redundantním	redundantní	k2eAgMnSc6d1	redundantní
(	(	kIx(	(
<g/>
zdvojeném	zdvojený	k2eAgInSc6d1	zdvojený
<g/>
)	)	kIx)	)
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
průběhu	průběh	k1gInSc3	průběh
periodických	periodický	k2eAgFnPc2d1	periodická
kontrol	kontrola	k1gFnPc2	kontrola
nebo	nebo	k8xC	nebo
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
plánované	plánovaný	k2eAgNnSc4d1	plánované
rychlé	rychlý	k2eAgNnSc4d1	rychlé
odstavení	odstavení	k1gNnSc4	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
normálně	normálně	k6eAd1	normálně
<g/>
,	,	kIx,	,
neúmyslná	úmyslný	k2eNgFnSc1d1	neúmyslná
aktivace	aktivace	k1gFnSc1	aktivace
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
významných	významný	k2eAgInPc2d1	významný
následků	následek	k1gInPc2	následek
<g/>
,	,	kIx,	,
úniky	únik	k1gInPc1	únik
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
LaP	lap	k1gInSc1	lap
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc1d2	menší
rozšíření	rozšíření	k1gNnSc1	rozšíření
kontaminace	kontaminace	k1gFnSc2	kontaminace
uvnitř	uvnitř	k7c2	uvnitř
kontrolovaného	kontrolovaný	k2eAgNnSc2d1	kontrolované
pásma	pásmo	k1gNnSc2	pásmo
bez	bez	k7c2	bez
širších	široký	k2eAgInPc2d2	širší
důsledků	důsledek	k1gInPc2	důsledek
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
příručky	příručka	k1gFnSc2	příručka
INES	INES	kA	INES
</s>
