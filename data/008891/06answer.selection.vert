<s>
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
během	během	k7c2	během
transkripce	transkripce	k1gFnSc2	transkripce
DNA	DNA	kA	DNA
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
předpis	předpis	k1gInSc4	předpis
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
bílkoviny	bílkovina	k1gFnSc2	bílkovina
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
přepsané	přepsaný	k2eAgFnPc4d1	přepsaná
podle	podle	k7c2	podle
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
