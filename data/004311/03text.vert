<s>
Atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
či	či	k8xC	či
jen	jen	k9	jen
srážky	srážka	k1gFnPc1	srážka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pojem	pojem	k1gInSc4	pojem
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
hydrometeorů	hydrometeor	k1gInPc2	hydrometeor
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soustavu	soustava	k1gFnSc4	soustava
částic	částice	k1gFnPc2	částice
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
kondenzací	kondenzace	k1gFnPc2	kondenzace
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
nebo	nebo	k8xC	nebo
sublimací	sublimace	k1gFnPc2	sublimace
nebo	nebo	k8xC	nebo
podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zdvižením	zdvižení	k1gNnSc7	zdvižení
větrem	vítr	k1gInSc7	vítr
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
padají	padat	k5eAaImIp3nP	padat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
či	či	k8xC	či
kondenzují	kondenzovat	k5eAaImIp3nP	kondenzovat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
koloběhu	koloběh	k1gInSc2	koloběh
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
frekvence	frekvence	k1gFnSc1	frekvence
srážek	srážka	k1gFnPc2	srážka
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
charakteristikou	charakteristika	k1gFnSc7	charakteristika
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
provozování	provozování	k1gNnSc4	provozování
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Orografické	orografický	k2eAgFnPc1d1	orografická
–	–	k?	–
srážky	srážka	k1gFnPc1	srážka
vznikající	vznikající	k2eAgFnSc4d1	vznikající
vynuceným	vynucený	k2eAgInSc7d1	vynucený
výstupem	výstup	k1gInSc7	výstup
vzduchu	vzduch	k1gInSc2	vzduch
způsobeným	způsobený	k2eAgInSc7d1	způsobený
tvarem	tvar	k1gInSc7	tvar
terénu	terén	k1gInSc2	terén
(	(	kIx(	(
<g/>
orografií	orografie	k1gFnPc2	orografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
na	na	k7c6	na
horských	horský	k2eAgFnPc6d1	horská
překážkách	překážka	k1gFnPc6	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Konvektivní	Konvektivní	k2eAgFnPc4d1	Konvektivní
–	–	k?	–
srážky	srážka	k1gFnPc4	srážka
způsobené	způsobený	k2eAgFnPc4d1	způsobená
výstupem	výstup	k1gInSc7	výstup
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konvekce	konvekce	k1gFnSc2	konvekce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
nerovnoměrném	rovnoměrný	k2eNgNnSc6d1	nerovnoměrné
zahřívání	zahřívání	k1gNnSc6	zahřívání
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Bublina	bublina	k1gFnSc1	bublina
zahřátého	zahřátý	k2eAgInSc2d1	zahřátý
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
nahoru	nahoru	k6eAd1	nahoru
<g/>
;	;	kIx,	;
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
teplejší	teplý	k2eAgInSc4d2	teplejší
než	než	k8xS	než
okolní	okolní	k2eAgInSc4d1	okolní
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
hladiny	hladina	k1gFnSc2	hladina
kondenzace	kondenzace	k1gFnSc2	kondenzace
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
kupovité	kupovitý	k2eAgInPc1d1	kupovitý
oblaky	oblak	k1gInPc1	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
intenzivní	intenzivní	k2eAgFnSc6d1	intenzivní
konvenci	konvence	k1gFnSc6	konvence
se	se	k3xPyFc4	se
oblaka	oblaka	k1gNnPc1	oblaka
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
vertikálně	vertikálně	k6eAd1	vertikálně
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
bouřkového	bouřkový	k2eAgInSc2d1	bouřkový
oblaku	oblak	k1gInSc2	oblak
(	(	kIx(	(
<g/>
typické	typický	k2eAgNnSc1d1	typické
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cyklonální	Cyklonální	k2eAgFnPc4d1	Cyklonální
–	–	k?	–
srážky	srážka	k1gFnSc2	srážka
vznikající	vznikající	k2eAgFnSc2d1	vznikající
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
vzduchu	vzduch	k1gInSc2	vzduch
způsobeném	způsobený	k2eAgInSc6d1	způsobený
celkovým	celkový	k2eAgInSc7d1	celkový
pohybem	pohyb	k1gInSc7	pohyb
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
déšť	déšť	k1gInSc1	déšť
mrznoucí	mrznoucí	k2eAgInSc1d1	mrznoucí
déšť	déšť	k1gInSc4	déšť
mrholení	mrholení	k1gNnPc2	mrholení
mrznoucí	mrznoucí	k2eAgNnSc1d1	mrznoucí
mrholení	mrholení	k1gNnSc1	mrholení
sníh	sníh	k1gInSc4	sníh
sněhové	sněhový	k2eAgFnSc2d1	sněhová
krupky	krupka	k1gFnSc2	krupka
sněhová	sněhový	k2eAgNnPc4d1	sněhové
zrna	zrno	k1gNnPc4	zrno
krupky	krupka	k1gFnSc2	krupka
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
déšť	déšť	k1gInSc1	déšť
kroupy	kroupa	k1gFnSc2	kroupa
ledové	ledový	k2eAgFnSc2d1	ledová
jehličky	jehlička	k1gFnSc2	jehlička
rosa	rosa	k1gFnSc1	rosa
jinovatka	jinovatka	k1gFnSc1	jinovatka
námraza	námraza	k1gFnSc1	námraza
ledovka	ledovka	k1gFnSc1	ledovka
déšť	déšť	k1gInSc1	déšť
mrholení	mrholení	k1gNnSc3	mrholení
rosa	rosa	k1gFnSc1	rosa
mrznoucí	mrznoucí	k2eAgFnSc1d1	mrznoucí
déšť	déšť	k1gInSc4	déšť
mrznoucí	mrznoucí	k2eAgNnSc1d1	mrznoucí
mrholení	mrholení	k1gNnSc1	mrholení
sníh	sníh	k1gInSc4	sníh
sněhové	sněhový	k2eAgFnSc2d1	sněhová
krupky	krupka	k1gFnSc2	krupka
sněhová	sněhový	k2eAgNnPc4d1	sněhové
zrna	zrno	k1gNnPc4	zrno
zmrzlý	zmrzlý	k2eAgInSc4d1	zmrzlý
déšť	déšť	k1gInSc4	déšť
krupky	krupka	k1gFnSc2	krupka
kroupy	kroupa	k1gFnSc2	kroupa
ledové	ledový	k2eAgFnSc2d1	ledová
jehličky	jehlička	k1gFnSc2	jehlička
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
rosa	rosa	k1gFnSc1	rosa
jíní	jínit	k5eAaImIp3nS	jínit
námraza	námraza	k1gFnSc1	námraza
ledovka	ledovka	k1gFnSc1	ledovka
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
lehce	lehko	k6eAd1	lehko
nad	nad	k7c7	nad
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
Lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
intenzitu	intenzita	k1gFnSc4	intenzita
i	i	k8xC	i
prosté	prostý	k2eAgNnSc4d1	prosté
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
bývá	bývat	k5eAaImIp3nS	bývat
udáváno	udávat	k5eAaImNgNnS	udávat
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
spadlé	spadlý	k2eAgFnSc2d1	spadlá
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
(	(	kIx(	(
<g/>
1	[number]	k4	1
mm	mm	kA	mm
=	=	kIx~	=
1	[number]	k4	1
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
či	či	k8xC	či
kroupy	kroupa	k1gFnPc1	kroupa
zachycené	zachycený	k2eAgFnPc1d1	zachycená
srážkoměrem	srážkoměr	k1gInSc7	srážkoměr
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
před	před	k7c7	před
měřením	měření	k1gNnSc7	měření
nechat	nechat	k5eAaPmF	nechat
roztát	roztát	k5eAaPmF	roztát
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
úhrnu	úhrn	k1gInSc2	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hyetometr	hyetometr	k1gInSc1	hyetometr
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
nádobu	nádoba	k1gFnSc4	nádoba
s	s	k7c7	s
nálevkou	nálevka	k1gFnSc7	nálevka
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
zaznamenávající	zaznamenávající	k2eAgInSc1d1	zaznamenávající
časový	časový	k2eAgInSc4d1	časový
průběh	průběh	k1gInSc4	průběh
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
plováku	plovák	k1gInSc2	plovák
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
termínem	termín	k1gInSc7	termín
ombrograf	ombrograf	k1gInSc1	ombrograf
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
zjišťování	zjišťování	k1gNnSc4	zjišťování
množství	množství	k1gNnSc2	množství
rosy	rosa	k1gFnSc2	rosa
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
drosometr	drosometr	k1gInSc1	drosometr
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
síťky	síťka	k1gFnSc2	síťka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
vahami	váha	k1gFnPc7	váha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
intenzity	intenzita	k1gFnSc2	intenzita
srážek	srážka	k1gFnPc2	srážka
široce	široko	k6eAd1	široko
využívá	využívat	k5eAaImIp3nS	využívat
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
radarů	radar	k1gInPc2	radar
<g/>
.	.	kIx.	.
</s>
<s>
Srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
jako	jako	k8xC	jako
výška	výška	k1gFnSc1	výška
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgInS	uvádět
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
<g/>
-li	i	k?	-li
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
převládajícímu	převládající	k2eAgInSc3d1	převládající
směru	směr	k1gInSc6	směr
větrného	větrný	k2eAgNnSc2d1	větrné
proudění	proudění	k1gNnSc2	proudění
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
,	,	kIx,	,
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
dešťových	dešťový	k2eAgFnPc2d1	dešťová
<g/>
)	)	kIx)	)
na	na	k7c6	na
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
v	v	k7c6	v
závětří	závětří	k1gNnSc6	závětří
hor	hora	k1gFnPc2	hora
tak	tak	k9	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
srážkový	srážkový	k2eAgInSc1d1	srážkový
stín	stín	k1gInSc1	stín
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
takto	takto	k6eAd1	takto
orograficky	orograficky	k6eAd1	orograficky
zeslabených	zeslabený	k2eAgFnPc2d1	zeslabená
srážek	srážka	k1gFnPc2	srážka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Žatecko	Žatecko	k1gNnSc4	Žatecko
a	a	k8xC	a
Roudnicko	Roudnicko	k1gNnSc4	Roudnicko
v	v	k7c6	v
závětří	závětří	k1gNnSc6	závětří
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Českého	český	k2eAgNnSc2d1	české
středohoří	středohoří	k1gNnSc2	středohoří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
450	[number]	k4	450
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
příkladem	příklad	k1gInSc7	příklad
zesílených	zesílený	k2eAgFnPc2d1	zesílená
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
severozápadně	severozápadně	k6eAd1	severozápadně
orientované	orientovaný	k2eAgNnSc1d1	orientované
údolí	údolí	k1gNnPc1	údolí
říčky	říčka	k1gFnSc2	říčka
Smědé	Smědý	k2eAgFnSc2d1	Smědá
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměrné	průměrný	k2eAgInPc1d1	průměrný
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
kolem	kolem	k7c2	kolem
1700	[number]	k4	1700
mm	mm	kA	mm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
a	a	k8xC	a
maxima	maximum	k1gNnSc2	maximum
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
či	či	k8xC	či
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
2500	[number]	k4	2500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
inverze	inverze	k1gFnSc1	inverze
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pokles	pokles	k1gInSc1	pokles
srážkových	srážkový	k2eAgInPc2d1	srážkový
úhrnů	úhrn	k1gInPc2	úhrn
<g/>
.	.	kIx.	.
</s>
<s>
Rekordy	rekord	k1gInPc1	rekord
pozemské	pozemský	k2eAgFnSc2d1	pozemská
neživé	živý	k2eNgFnSc2d1	neživá
přírody	příroda	k1gFnSc2	příroda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
srážky	srážka	k1gFnSc2	srážka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
srážka	srážka	k1gFnSc1	srážka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
