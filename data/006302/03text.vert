<s>
Hyde	Hyde	k6eAd1	Hyde
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
komplexně	komplexně	k6eAd1	komplexně
interaktivní	interaktivní	k2eAgInSc4d1	interaktivní
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
ČT	ČT	kA	ČT
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
posílají	posílat	k5eAaImIp3nP	posílat
otázky	otázka	k1gFnPc1	otázka
diváci	divák	k1gMnPc1	divák
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
chat	chata	k1gFnPc2	chata
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
pořadu	pořad	k1gInSc2	pořad
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Twitter	Twitter	k1gMnSc1	Twitter
<g/>
,	,	kIx,	,
SMS	SMS	kA	SMS
nebo	nebo	k8xC	nebo
volají	volat	k5eAaImIp3nP	volat
po	po	k7c6	po
telefonu	telefon	k1gInSc6	telefon
či	či	k8xC	či
Skypu	Skyp	k1gInSc6	Skyp
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
diváci	divák	k1gMnPc1	divák
nahrát	nahrát	k5eAaBmF	nahrát
videodotaz	videodotaz	k1gInSc4	videodotaz
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
</s>
<s>
Zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
díky	díky	k7c3	díky
živému	živý	k2eAgInSc3d1	živý
vstupu	vstup	k1gInSc3	vstup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
redaktor	redaktor	k1gMnSc1	redaktor
pořadu	pořad	k1gInSc2	pořad
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živém	živý	k2eAgInSc6d1	živý
vstupu	vstup	k1gInSc6	vstup
také	také	k9	také
někdy	někdy	k6eAd1	někdy
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
odborníci	odborník	k1gMnPc1	odborník
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
názoroví	názorový	k2eAgMnPc1d1	názorový
oponenti	oponent	k1gMnPc1	oponent
hosta	host	k1gMnSc2	host
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc4	pořad
moderují	moderovat	k5eAaBmIp3nP	moderovat
Daniel	Daniel	k1gMnSc1	Daniel
Takáč	Takáč	k1gMnSc1	Takáč
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Klepetko	Klepetka	k1gFnSc5	Klepetka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
pořad	pořad	k1gInSc4	pořad
moderovali	moderovat	k5eAaBmAgMnP	moderovat
s	s	k7c7	s
Danielem	Daniel	k1gMnSc7	Daniel
Takáčem	Takáč	k1gInSc7	Takáč
Pavlína	Pavlína	k1gFnSc1	Pavlína
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
a	a	k8xC	a
Jaromír	Jaromír	k1gMnSc1	Jaromír
Bosák	Bosák	k1gMnSc1	Bosák
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Lukáš	Lukáš	k1gMnSc1	Lukáš
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
vysílá	vysílat	k5eAaImIp3nS	vysílat
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
hostem	host	k1gMnSc7	host
byl	být	k5eAaImAgMnS	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Eduard	Eduard	k1gMnSc1	Eduard
Janota	Janota	k1gMnSc1	Janota
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c4	na
ČT	ČT	kA	ČT
24	[number]	k4	24
každý	každý	k3xTgInSc4	každý
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
od	od	k7c2	od
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
do	do	k7c2	do
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
pořad	pořad	k1gInSc1	pořad
kritizován	kritizován	k2eAgInSc1d1	kritizován
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Meierem	Meier	k1gMnSc7	Meier
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
pokládáno	pokládat	k5eAaImNgNnS	pokládat
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
diváckých	divácký	k2eAgFnPc2d1	divácká
otázek	otázka	k1gFnPc2	otázka
i	i	k8xC	i
otázek	otázka	k1gFnPc2	otázka
z	z	k7c2	z
telefonických	telefonický	k2eAgInPc2d1	telefonický
vstupů	vstup	k1gInPc2	vstup
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořad	pořad	k1gInSc1	pořad
vysílá	vysílat	k5eAaImIp3nS	vysílat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
otázky	otázka	k1gFnSc2	otázka
redaktorů	redaktor	k1gMnPc2	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Maier	Maier	k1gInSc1	Maier
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
i	i	k9	i
výběr	výběr	k1gInSc1	výběr
otázek	otázka	k1gFnPc2	otázka
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tématu	téma	k1gNnSc6	téma
i	i	k8xC	i
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
hodnocení	hodnocení	k1gNnSc1	hodnocení
televizními	televizní	k2eAgMnPc7d1	televizní
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
apod.	apod.	kA	apod.
http://www.ceskatelevize.cz/specialy/hydepark/	[url]	k?	http://www.ceskatelevize.cz/specialy/hydepark/
</s>
