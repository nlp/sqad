<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
И	И	k?	И
М	М	k?	М
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
Tobolsk	Tobolsk	k1gInSc1	Tobolsk
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
objevitelem	objevitel	k1gMnSc7	objevitel
periodického	periodický	k2eAgInSc2d1	periodický
zákona	zákon	k1gInSc2	zákon
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
tabulku	tabulka	k1gFnSc4	tabulka
prvků	prvek	k1gInPc2	prvek
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Ruské	ruský	k2eAgFnSc2d1	ruská
chemické	chemický	k2eAgFnSc2d1	chemická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
předložil	předložit	k5eAaPmAgInS	předložit
tabulku	tabulka	k1gFnSc4	tabulka
přesnější	přesný	k2eAgFnSc4d2	přesnější
<g/>
,	,	kIx,	,
doplněnou	doplněná	k1gFnSc4	doplněná
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
soustava	soustava	k1gFnSc1	soustava
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
k	k	k7c3	k
udání	udání	k1gNnSc3	udání
vlastností	vlastnost	k1gFnPc2	vlastnost
prvků	prvek	k1gInPc2	prvek
dosud	dosud	k6eAd1	dosud
neobjevených	objevený	k2eNgInPc2d1	neobjevený
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
objevům	objev	k1gInPc3	objev
předpovězených	předpovězený	k2eAgFnPc2d1	předpovězená
prvků	prvek	k1gInPc2	prvek
patřil	patřit	k5eAaImAgInS	patřit
objev	objev	k1gInSc1	objev
eka-aluminia	ekaluminium	k1gNnSc2	eka-aluminium
(	(	kIx(	(
<g/>
gallia	gallia	k1gFnSc1	gallia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eka-boru	ekaora	k1gFnSc4	eka-bora
(	(	kIx(	(
<g/>
skandia	skandium	k1gNnSc2	skandium
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
eka-silicia	ekailicium	k1gNnSc2	eka-silicium
(	(	kIx(	(
<g/>
germania	germanium	k1gNnSc2	germanium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc3	jehož
vlastnostem	vlastnost	k1gFnPc3	vlastnost
věnoval	věnovat	k5eAaPmAgMnS	věnovat
nejvíce	nejvíce	k6eAd1	nejvíce
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Gallium	Gallium	k1gNnSc1	Gallium
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
Lécoq	Lécoq	k1gFnSc1	Lécoq
de	de	k?	de
Boisbaudran	Boisbaudran	k1gInSc4	Boisbaudran
spektrální	spektrální	k2eAgFnSc7d1	spektrální
analýzou	analýza	k1gFnSc7	analýza
ve	v	k7c6	v
sfaleritu	sfalerit	k1gInSc6	sfalerit
<g/>
,	,	kIx,	,
skandium	skandium	k1gNnSc4	skandium
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
Lars	Larsa	k1gFnPc2	Larsa
Frederik	Frederik	k1gMnSc1	Frederik
Nilson	Nilson	k1gMnSc1	Nilson
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
sloučenin	sloučenina	k1gFnPc2	sloučenina
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
germanium	germanium	k1gNnSc4	germanium
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
Clemens	Clemensa	k1gFnPc2	Clemensa
Winkler	Winkler	k1gMnSc1	Winkler
při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
nerostu	nerost	k1gInSc2	nerost
argyroditu	argyrodita	k1gFnSc4	argyrodita
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
germania	germanium	k1gNnSc2	germanium
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
triumfem	triumf	k1gInSc7	triumf
objevu	objev	k1gInSc2	objev
periodického	periodický	k2eAgInSc2d1	periodický
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Geniálnost	geniálnost	k1gFnSc1	geniálnost
jím	jíst	k5eAaImIp1nS	jíst
objeveného	objevený	k2eAgNnSc2d1	objevené
uspořádání	uspořádání	k1gNnSc2	uspořádání
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
pochopení	pochopení	k1gNnSc2	pochopení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
studium	studium	k1gNnSc4	studium
rentgenových	rentgenový	k2eAgNnPc2d1	rentgenové
spekter	spektrum	k1gNnPc2	spektrum
a	a	k8xC	a
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
formulaci	formulace	k1gFnSc6	formulace
periodického	periodický	k2eAgInSc2d1	periodický
zákona	zákon	k1gInSc2	zákon
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
<g/>
:	:	kIx,	:
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
atomová	atomový	k2eAgFnSc1d1	atomová
váha	váha	k1gFnSc1	váha
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
výrazem	výraz	k1gInSc7	výraz
atomové	atomový	k2eAgInPc4d1	atomový
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
protonové	protonový	k2eAgFnSc3d1	protonová
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byly	být	k5eAaImAgInP	být
výzkumy	výzkum	k1gInPc1	výzkum
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
a	a	k8xC	a
rentgenových	rentgenový	k2eAgNnPc2d1	rentgenové
spekter	spektrum	k1gNnPc2	spektrum
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc2	jenž
zjistil	zjistit	k5eAaPmAgMnS	zjistit
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
Henry	Henry	k1gMnSc1	Henry
Gwyn	Gwyn	k1gMnSc1	Gwyn
Jeffreys	Jeffreys	k1gInSc4	Jeffreys
Moseley	Moselea	k1gFnSc2	Moselea
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
vlnočtem	vlnočet	k1gInSc7	vlnočet
spektrální	spektrální	k2eAgFnSc2d1	spektrální
čáry	čára	k1gFnSc2	čára
K-série	Kérie	k1gFnSc2	K-série
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
pořadovým	pořadový	k2eAgNnSc7d1	pořadové
číslem	číslo	k1gNnSc7	číslo
prvku	prvek	k1gInSc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c4	na
400	[number]	k4	400
prací	práce	k1gFnPc2	práce
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
prací	práce	k1gFnPc2	práce
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
metrologie	metrologie	k1gFnSc2	metrologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
práce	práce	k1gFnSc2	práce
o	o	k7c6	o
původu	původ	k1gInSc6	původ
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
,	,	kIx,	,
o	o	k7c6	o
roztocích	roztok	k1gInPc6	roztok
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
také	také	k9	také
například	například	k6eAd1	například
předběžné	předběžný	k2eAgInPc1d1	předběžný
výpočty	výpočet	k1gInPc1	výpočet
ledoborce	ledoborec	k1gInSc2	ledoborec
Jermak	Jermak	k1gInSc1	Jermak
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
vynikající	vynikající	k2eAgFnSc4d1	vynikající
učebnici	učebnice	k1gFnSc4	učebnice
Základy	základ	k1gInPc1	základ
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
nazván	nazván	k2eAgInSc1d1	nazván
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
impaktní	impaktní	k2eAgInSc1d1	impaktní
kráter	kráter	k1gInSc1	kráter
Mendělejev	Mendělejev	k1gFnSc2	Mendělejev
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
minerál	minerál	k1gInSc1	minerál
mendelevit	mendelevit	k1gInSc1	mendelevit
a	a	k8xC	a
101	[number]	k4	101
<g/>
.	.	kIx.	.
prvek	prvek	k1gInSc1	prvek
mendelevium	mendelevium	k1gNnSc1	mendelevium
<g/>
.	.	kIx.	.
</s>
