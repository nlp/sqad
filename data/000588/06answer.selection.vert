<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
И	И	k?	И
М	М	k?	М
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
Tobolsk	Tobolsk	k1gInSc1	Tobolsk
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
