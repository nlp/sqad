<s>
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
mysteriozní	mysteriozní	k2eAgInSc1d1	mysteriozní
filmový	filmový	k2eAgInSc1d1	filmový
thriller	thriller	k1gInSc1	thriller
subžánru	subžánr	k1gInSc2	subžánr
neo-noir	neooir	k1gInSc1	neo-noir
režiséra	režisér	k1gMnSc2	režisér
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Naomi	Nao	k1gFnPc7	Nao
Wattsová	Wattsová	k1gFnSc1	Wattsová
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
Harringová	Harringový	k2eAgFnSc1d1	Harringová
a	a	k8xC	a
Justin	Justin	k1gMnSc1	Justin
Theroux	Theroux	k1gInSc1	Theroux
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
surrealisticky	surrealisticky	k6eAd1	surrealisticky
pojatý	pojatý	k2eAgInSc1d1	pojatý
snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
netvoří	tvořit	k5eNaImIp3nS	tvořit
chronologicky	chronologicky	k6eAd1	chronologicky
souvislou	souvislý	k2eAgFnSc4d1	souvislá
dějovou	dějový	k2eAgFnSc4d1	dějová
linku	linka	k1gFnSc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
mnohovrstevnatý	mnohovrstevnatý	k2eAgInSc4d1	mnohovrstevnatý
náhled	náhled	k1gInSc4	náhled
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
všední	všední	k2eAgFnSc2d1	všední
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mulholland	Mulholland	k1gInSc1	Mulholland
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
</s>
