<p>
<s>
DIR	DIR	kA	DIR
<g/>
,	,	kIx,	,
coby	coby	k?	coby
akronym	akronym	k1gInSc4	akronym
anglického	anglický	k2eAgInSc2d1	anglický
výrazu	výraz	k1gInSc2	výraz
Doing	Doing	k1gMnSc1	Doing
It	It	k1gMnSc1	It
Right	Right	k1gMnSc1	Right
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
dělej	dělat	k5eAaImRp2nS	dělat
to	ten	k3xDgNnSc1	ten
správně	správně	k6eAd1	správně
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
standardizovat	standardizovat	k5eAaBmF	standardizovat
výcvik	výcvik	k1gInSc4	výcvik
a	a	k8xC	a
výstroj	výstroj	k1gFnSc4	výstroj
potápěčů	potápěč	k1gMnPc2	potápěč
a	a	k8xC	a
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
námahu	námaha	k1gFnSc4	námaha
a	a	k8xC	a
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
filosofii	filosofie	k1gFnSc3	filosofie
jsou	být	k5eAaImIp3nP	být
výstroj	výstroj	k1gInSc1	výstroj
a	a	k8xC	a
postupy	postup	k1gInPc7	postup
DIR	DIR	kA	DIR
potápěčů	potápěč	k1gMnPc2	potápěč
bezpečnější	bezpečný	k2eAgInPc1d2	bezpečnější
při	při	k7c6	při
hloubkových	hloubkový	k2eAgInPc6d1	hloubkový
ponorech	ponor	k1gInPc6	ponor
<g/>
,	,	kIx,	,
náročných	náročný	k2eAgInPc6d1	náročný
vrakových	vrakový	k2eAgInPc6d1	vrakový
a	a	k8xC	a
jeskynních	jeskynní	k2eAgInPc6d1	jeskynní
ponorech	ponor	k1gInPc6	ponor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drobné	drobný	k2eAgFnSc6d1	drobná
obměně	obměna	k1gFnSc6	obměna
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
při	při	k7c6	při
sportovním	sportovní	k2eAgNnSc6d1	sportovní
potápění	potápění	k1gNnSc6	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
základním	základní	k2eAgNnSc7d1	základní
pravidlem	pravidlo	k1gNnSc7	pravidlo
DIR	DIR	kA	DIR
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dělej	dělat	k5eAaImRp2nS	dělat
věci	věc	k1gFnSc2	věc
pořádně	pořádně	k6eAd1	pořádně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nedělej	dělat	k5eNaImRp2nS	dělat
vůbec	vůbec	k9	vůbec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
nehody	nehoda	k1gFnPc4	nehoda
ve	v	k7c6	v
floridských	floridský	k2eAgFnPc6d1	floridská
jeskyních	jeskyně	k1gFnPc6	jeskyně
dal	dát	k5eAaPmAgMnS	dát
počátkem	počátek	k1gInSc7	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Bill	Bill	k1gMnSc1	Bill
"	"	kIx"	"
<g/>
Hogarth	Hogarth	k1gMnSc1	Hogarth
<g/>
"	"	kIx"	"
Main	Main	k1gMnSc1	Main
dohromady	dohromady	k6eAd1	dohromady
koncept	koncept	k1gInSc4	koncept
výstroje	výstroj	k1gFnSc2	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pojily	pojit	k5eAaImAgFnP	pojit
i	i	k9	i
techniky	technika	k1gFnPc1	technika
pohybu	pohyb	k1gInSc2	pohyb
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
výcvik	výcvik	k1gInSc1	výcvik
potápěčů	potápěč	k1gMnPc2	potápěč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
DIR	DIR	kA	DIR
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
kritizována	kritizován	k2eAgFnSc1d1	kritizována
díky	díky	k7c3	díky
militantnímu	militantní	k2eAgInSc3d1	militantní
přístupu	přístup	k1gInSc3	přístup
některých	některý	k3yIgMnPc2	některý
jejích	její	k3xOp3gMnPc2	její
představitelů	představitel	k1gMnPc2	představitel
<g/>
.	.	kIx.	.
</s>
<s>
DIR	DIR	kA	DIR
potápěči	potápěč	k1gMnPc1	potápěč
pak	pak	k6eAd1	pak
bývají	bývat	k5eAaImIp3nP	bývat
přirovnávání	přirovnávání	k1gNnSc4	přirovnávání
k	k	k7c3	k
fašistům	fašista	k1gMnPc3	fašista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
není	být	k5eNaImIp3nS	být
systém	systém	k1gInSc1	systém
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc2	jeho
nepochopení	nepochopení	k1gNnSc2	nepochopení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
DIR	DIR	kA	DIR
potápěčů	potápěč	k1gInPc2	potápěč
dělá	dělat	k5eAaImIp3nS	dělat
věci	věc	k1gFnSc3	věc
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
sestavenou	sestavený	k2eAgFnSc4d1	sestavená
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
DIR	DIR	kA	DIR
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
plně	plně	k6eAd1	plně
pochopili	pochopit	k5eAaPmAgMnP	pochopit
význam	význam	k1gInSc4	význam
těch	ten	k3xDgFnPc2	ten
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
konfigurace	konfigurace	k1gFnSc2	konfigurace
</s>
</p>
<p>
<s>
==	==	k?	==
Výukové	výukový	k2eAgInPc1d1	výukový
systémy	systém	k1gInPc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
striktně	striktně	k6eAd1	striktně
dodržuji	dodržovat	k5eAaImIp1nS	dodržovat
DIR	DIR	kA	DIR
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GUE	GUE	kA	GUE
–	–	k?	–
Global	globat	k5eAaImAgMnS	globat
Underwater	Underwater	k1gMnSc1	Underwater
Explorers	Explorersa	k1gFnPc2	Explorersa
</s>
</p>
<p>
<s>
ISE	ISE	kA	ISE
–	–	k?	–
Inner	Inner	k1gInSc1	Inner
Space	Space	k1gMnSc1	Space
Explorers	Explorersa	k1gFnPc2	Explorersa
</s>
</p>
<p>
<s>
UTD	UTD	kA	UTD
–	–	k?	–
Unified	Unified	k1gInSc1	Unified
Team	team	k1gInSc4	team
DivingSystémy	DivingSystém	k1gInPc7	DivingSystém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
prvky	prvek	k1gInPc4	prvek
DIR	DIR	kA	DIR
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c4	v
jejich	jejich	k3xOp3gNnSc4	jejich
dodržování	dodržování	k1gNnSc4	dodržování
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
striktní	striktní	k2eAgNnPc1d1	striktní
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgNnPc1d1	ortodoxní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANDI	ANDI	kA	ANDI
–	–	k?	–
American	American	k1gInSc1	American
Nitrox	Nitrox	k1gInSc1	Nitrox
Divers	Divers	k1gInSc1	Divers
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ANDI	ANDI	kA	ANDI
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
ANDI-EUROPE	ANDI-EUROPE	k1gFnSc1	ANDI-EUROPE
<g/>
.	.	kIx.	.
<g/>
EU	EU	kA	EU
</s>
</p>
<p>
<s>
IANTD	IANTD	kA	IANTD
–	–	k?	–
International	International	k1gFnSc1	International
Association	Association	k1gInSc1	Association
of	of	k?	of
Nitrox	Nitrox	k1gInSc1	Nitrox
and	and	k?	and
Technical	Technical	k1gFnSc2	Technical
Divers	Diversa	k1gFnPc2	Diversa
</s>
</p>
<p>
<s>
NAUI	NAUI	kA	NAUI
–	–	k?	–
National	National	k1gFnSc1	National
Association	Association	k1gInSc1	Association
of	of	k?	of
Underwater	Underwater	k1gInSc1	Underwater
Instructors	Instructors	k1gInSc1	Instructors
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
odnoži	odnož	k1gFnSc6	odnož
NTecNAUI	NTecNAUI	k1gFnSc2	NTecNAUI
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
TDI	TDI	kA	TDI
<g/>
/	/	kIx~	/
<g/>
SDI	SDI	kA	SDI
–	–	k?	–
Technical	Technical	k1gFnSc1	Technical
Diving	Diving	k1gInSc1	Diving
International	International	k1gFnSc1	International
<g/>
/	/	kIx~	/
<g/>
Scuba	Scuba	k1gFnSc1	Scuba
Diving	Diving	k1gInSc1	Diving
International	International	k1gMnSc1	International
TDISDI	TDISDI	kA	TDISDI
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
