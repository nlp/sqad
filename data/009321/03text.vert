<p>
<s>
Meteor	meteor	k1gInSc1	meteor
(	(	kIx(	(
<g/>
Malus	Malus	k1gInSc1	Malus
domestica	domestica	k1gMnSc1	domestica
Meteor	meteor	k1gInSc1	meteor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovocný	ovocný	k2eAgInSc1d1	ovocný
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
kultivar	kultivar	k1gInSc1	kultivar
druhu	druh	k1gInSc2	druh
jabloň	jabloň	k1gFnSc4	jabloň
domácí	domácí	k1gFnSc4	domácí
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgMnPc2d1	růžovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Meteor	meteor	k1gInSc1	meteor
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1	vyšlechtěná
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
odrůd	odrůda	k1gFnPc2	odrůda
Melrose	Melrosa	k1gFnSc3	Melrosa
x	x	k?	x
Megumi	Megu	k1gFnPc7	Megu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Meteor	meteor	k1gInSc1	meteor
roste	růst	k5eAaImIp3nS	růst
středně	středně	k6eAd1	středně
bujně	bujně	k6eAd1	bujně
až	až	k9	až
bujně	bujně	k6eAd1	bujně
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pozdně	pozdně	k6eAd1	pozdně
zimní	zimní	k2eAgFnSc4d1	zimní
odrůdu	odrůda	k1gFnSc4	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Habitus	habitus	k1gInSc1	habitus
Meteoru	meteor	k1gInSc2	meteor
je	být	k5eAaImIp3nS	být
rozložitý	rozložitý	k2eAgInSc1d1	rozložitý
<g/>
.	.	kIx.	.
</s>
<s>
Plodí	plodit	k5eAaImIp3nS	plodit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
na	na	k7c6	na
středně	středně	k6eAd1	středně
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
až	až	k9	až
velmi	velmi	k6eAd1	velmi
velký	velký	k2eAgInSc4d1	velký
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
dužiny	dužina	k1gFnSc2	dužina
je	být	k5eAaImIp3nS	být
zelenožlutá	zelenožlutý	k2eAgFnSc1d1	zelenožlutá
<g/>
,	,	kIx,	,
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
rozmytou	rozmytý	k2eAgFnSc7d1	rozmytá
krycí	krycí	k2eAgFnSc7d1	krycí
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dužina	dužina	k1gFnSc1	dužina
má	mít	k5eAaImIp3nS	mít
navinule	navinule	k6eAd1	navinule
sladkou	sladký	k2eAgFnSc7d1	sladká
<g/>
,	,	kIx,	,
<g/>
šťavnatou	šťavnatý	k2eAgFnSc7d1	šťavnatá
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
výbornou	výborný	k2eAgFnSc4d1	výborná
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Konzistence	konzistence	k1gFnSc1	konzistence
dužiny	dužina	k1gFnSc2	dužina
je	být	k5eAaImIp3nS	být
hrubá	hrubý	k2eAgFnSc1d1	hrubá
a	a	k8xC	a
středně	středně	k6eAd1	středně
tuhá	tuhý	k2eAgNnPc4d1	tuhé
<g/>
.	.	kIx.	.
</s>
<s>
Sklízí	sklízet	k5eAaImIp3nS	sklízet
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
a	a	k8xC	a
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Konzumně	konzumně	k6eAd1	konzumně
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
během	během	k7c2	během
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
pozdě	pozdě	k6eAd1	pozdě
plodí	plodit	k5eAaImIp3nS	plodit
<g/>
.	.	kIx.	.
</s>
<s>
Plodnost	plodnost	k1gFnSc1	plodnost
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
avšak	avšak	k8xC	avšak
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
slabě	slabě	k6eAd1	slabě
až	až	k8xS	až
bujně	bujně	k6eAd1	bujně
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
podnože	podnož	k1gInPc4	podnož
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
s	s	k7c7	s
letním	letní	k2eAgInSc7d1	letní
průklestem	průklest	k1gInSc7	průklest
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
strupovistosti	strupovistost	k1gFnSc3	strupovistost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
napadení	napadení	k1gNnSc3	napadení
padlím	padlí	k1gNnPc3	padlí
jabloňovým	jabloňový	k2eAgFnPc3d1	Jabloňová
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
odolná	odolný	k2eAgFnSc1d1	odolná
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
především	především	k9	především
do	do	k7c2	do
teplých	teplý	k2eAgFnPc2d1	teplá
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
pěstitelských	pěstitelský	k2eAgFnPc2d1	pěstitelská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
