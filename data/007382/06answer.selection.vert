<s>
Odrůda	odrůda	k1gFnSc1	odrůda
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
odrůda	odrůda	k1gFnSc1	odrůda
Aurelius	Aurelius	k1gInSc1	Aurelius
<g/>
)	)	kIx)	)
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
Ing.	ing.	kA	ing.
Josefem	Josef	k1gMnSc7	Josef
Veverkou	Veverka	k1gMnSc7	Veverka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
její	její	k3xOp3gNnSc4	její
šlechtění	šlechtění	k1gNnSc4	šlechtění
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
ve	v	k7c6	v
Šlechtitelské	šlechtitelský	k2eAgFnSc6d1	šlechtitelská
stanici	stanice	k1gFnSc6	stanice
ve	v	k7c6	v
Velkých	velká	k1gFnPc6	velká
Pavlovicích	Pavlovice	k1gFnPc6	Pavlovice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c4	v
Perné	perný	k2eAgNnSc4d1	perné
<g/>
.	.	kIx.	.
</s>
