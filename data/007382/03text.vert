<s>
Pálava	Pálava	k1gFnSc1	Pálava
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
moštová	moštový	k2eAgFnSc1d1	moštová
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
bílých	bílý	k2eAgFnPc2d1	bílá
vín	vína	k1gFnPc2	vína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Tramín	tramín	k1gInSc4	tramín
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
Müller	Müller	k1gInSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
odrůda	odrůda	k1gFnSc1	odrůda
Aurelius	Aurelius	k1gInSc1	Aurelius
<g/>
)	)	kIx)	)
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
Ing.	ing.	kA	ing.
Josefem	Josef	k1gMnSc7	Josef
Veverkou	Veverka	k1gMnSc7	Veverka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
její	její	k3xOp3gNnSc4	její
šlechtění	šlechtění	k1gNnSc4	šlechtění
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
ve	v	k7c6	v
Šlechtitelské	šlechtitelský	k2eAgFnSc6d1	šlechtitelská
stanici	stanice	k1gFnSc6	stanice
ve	v	k7c6	v
Velkých	velká	k1gFnPc6	velká
Pavlovicích	Pavlovice	k1gFnPc6	Pavlovice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c4	v
Perné	perný	k2eAgNnSc4d1	perné
<g/>
.	.	kIx.	.
</s>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
odrůda	odrůda	k1gFnSc1	odrůda
Pálava	Pálava	k1gFnSc1	Pálava
je	být	k5eAaImIp3nS	být
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
dřevitá	dřevitý	k2eAgFnSc1d1	dřevitá
pnoucí	pnoucí	k2eAgFnSc1d1	pnoucí
liána	liána	k1gFnSc1	liána
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
až	až	k9	až
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
tloušťky	tloušťka	k1gFnSc2	tloušťka
až	až	k9	až
několik	několik	k4yIc1	několik
centimetrů	centimetr	k1gInPc2	centimetr
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
světlou	světlý	k2eAgFnSc7d1	světlá
borkou	borka	k1gFnSc7	borka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
loupe	loupat	k5eAaImIp3nS	loupat
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Úponky	úponek	k1gInPc1	úponek
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
této	tento	k3xDgFnSc3	tento
rostlině	rostlina	k1gFnSc3	rostlina
pnout	pnout	k5eAaImF	pnout
se	se	k3xPyFc4	se
po	po	k7c6	po
pevných	pevný	k2eAgInPc6d1	pevný
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
bujný	bujný	k2eAgInSc1d1	bujný
až	až	k6eAd1	až
bujný	bujný	k2eAgInSc1d1	bujný
s	s	k7c7	s
polovzpřímenými	polovzpřímený	k2eAgInPc7d1	polovzpřímený
letorosty	letorost	k1gInPc7	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Včelka	včelka	k1gFnSc1	včelka
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
ochlupení	ochlupení	k1gNnSc1	ochlupení
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
bělavé	bělavý	k2eAgNnSc1d1	bělavé
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc1	okraj
jsou	být	k5eAaImIp3nP	být
slabě	slabě	k6eAd1	slabě
načervenalé	načervenalý	k2eAgFnPc1d1	načervenalá
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholek	vrcholek	k1gInSc1	vrcholek
letorostu	letorost	k1gInSc2	letorost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
ochlupený	ochlupený	k2eAgInSc1d1	ochlupený
až	až	k8xS	až
plstnatý	plstnatý	k2eAgInSc1d1	plstnatý
<g/>
,	,	kIx,	,
bělavý	bělavý	k2eAgInSc1d1	bělavý
s	s	k7c7	s
karmínově	karmínově	k6eAd1	karmínově
červeným	červený	k2eAgNnSc7d1	červené
lemováním	lemování	k1gNnSc7	lemování
okrajů	okraj	k1gInPc2	okraj
<g/>
,	,	kIx,	,
letorost	letorost	k1gFnSc1	letorost
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
zelený	zelený	k2eAgInSc1d1	zelený
se	s	k7c7	s
slabě	slabě	k6eAd1	slabě
karmínovým	karmínový	k2eAgInSc7d1	karmínový
odstínem	odstín	k1gInSc7	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Jednoleté	jednoletý	k2eAgNnSc1d1	jednoleté
réví	réví	k1gNnSc1	réví
je	být	k5eAaImIp3nS	být
tmavohnědé	tmavohnědý	k2eAgNnSc4d1	tmavohnědé
<g/>
,	,	kIx,	,
čárkované	čárkovaný	k2eAgNnSc4d1	čárkované
tečkované	tečkovaný	k2eAgNnSc4d1	tečkované
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc1d1	zimní
pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
až	až	k9	až
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc1	povrch
čepele	čepel	k1gFnSc2	čepel
je	být	k5eAaImIp3nS	být
neurčitě	určitě	k6eNd1	určitě
zvlněný	zvlněný	k2eAgMnSc1d1	zvlněný
<g/>
,	,	kIx,	,
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
<g/>
,	,	kIx,	,
tří-	tří-	k?	tří-
až	až	k8xS	až
pětilaločnatá	pětilaločnatý	k2eAgFnSc1d1	pětilaločnatý
se	se	k3xPyFc4	se
středně	středně	k6eAd1	středně
hlubokými	hluboký	k2eAgInPc7d1	hluboký
horními	horní	k2eAgInPc7d1	horní
bočními	boční	k2eAgInPc7d1	boční
výkroji	výkroj	k1gInPc7	výkroj
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
strana	strana	k1gFnSc1	strana
čepele	čepel	k1gFnSc2	čepel
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
puchýřnatá	puchýřnatý	k2eAgFnSc1d1	puchýřnatá
<g/>
,	,	kIx,	,
kožovitě	kožovitě	k6eAd1	kožovitě
vrásčitá	vrásčitý	k2eAgFnSc1d1	vrásčitá
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
zelené	zelený	k2eAgFnPc1d1	zelená
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
ochlupená	ochlupený	k2eAgFnSc1d1	ochlupená
<g/>
,	,	kIx,	,
řapík	řapík	k1gInSc1	řapík
listu	lista	k1gFnSc4	lista
krátký	krátký	k2eAgInSc1d1	krátký
až	až	k6eAd1	až
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
narůžovělý	narůžovělý	k2eAgInSc1d1	narůžovělý
<g/>
,	,	kIx,	,
řapíkový	řapíkový	k2eAgInSc1d1	řapíkový
výkroj	výkroj	k1gInSc1	výkroj
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
bez	bez	k7c2	bez
průsvitu	průsvit	k1gInSc2	průsvit
nebo	nebo	k8xC	nebo
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
eliptickým	eliptický	k2eAgInSc7d1	eliptický
průsvitem	průsvit	k1gInSc7	průsvit
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1	oboupohlavní
pětičetné	pětičetný	k2eAgInPc4d1	pětičetný
květy	květ	k1gInPc4	květ
v	v	k7c6	v
hroznovitých	hroznovitý	k2eAgNnPc6d1	hroznovité
květenstvích	květenství	k1gNnPc6	květenství
jsou	být	k5eAaImIp3nP	být
žlutozelené	žlutozelený	k2eAgInPc1d1	žlutozelený
<g/>
,	,	kIx,	,
samosprašné	samosprašný	k2eAgInPc1d1	samosprašný
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
mm	mm	kA	mm
<g/>
,	,	kIx,	,
1,1	[number]	k4	1,1
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulatá	kulatý	k2eAgFnSc1d1	kulatá
až	až	k9	až
mírně	mírně	k6eAd1	mírně
oválná	oválný	k2eAgNnPc1d1	oválné
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
červenošedá	červenošedý	k2eAgFnSc1d1	červenošedá
bobule	bobule	k1gFnSc1	bobule
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
slupka	slupka	k1gFnSc1	slupka
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
dužnina	dužnina	k1gFnSc1	dužnina
bez	bez	k7c2	bez
zbarvení	zbarvení	k1gNnPc2	zbarvení
<g/>
,	,	kIx,	,
šťavnatá	šťavnatý	k2eAgFnSc1d1	šťavnatá
<g/>
,	,	kIx,	,
aromatická	aromatický	k2eAgFnSc1d1	aromatická
<g/>
,	,	kIx,	,
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
<g/>
,	,	kIx,	,
muškátovou	muškátový	k2eAgFnSc7d1	muškátová
vůní	vůně	k1gFnSc7	vůně
a	a	k8xC	a
kořenitou	kořenitý	k2eAgFnSc7d1	kořenitá
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
je	být	k5eAaImIp3nS	být
hruškovité	hruškovitý	k2eAgNnSc1d1	hruškovité
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
zobáček	zobáček	k1gInSc1	zobáček
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Hrozen	hrozen	k1gInSc1	hrozen
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
mm	mm	kA	mm
<g/>
,	,	kIx,	,
169	[number]	k4	169
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kónický	kónický	k2eAgInSc1d1	kónický
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
křidélky	křidélko	k1gNnPc7	křidélko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
středně	středně	k6eAd1	středně
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
stopkou	stopka	k1gFnSc7	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Pálava	Pálava	k1gFnSc1	Pálava
je	být	k5eAaImIp3nS	být
pozdní	pozdní	k2eAgFnSc1d1	pozdní
moštová	moštový	k2eAgFnSc1d1	moštová
odrůda	odrůda	k1gFnSc1	odrůda
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Tramín	tramín	k1gInSc4	tramín
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
Müller	Müller	k1gInSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
Ing.	ing.	kA	ing.
Josefem	Josef	k1gMnSc7	Josef
Veverkou	Veverka	k1gMnSc7	Veverka
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Veverka	Veverka	k1gMnSc1	Veverka
zahájil	zahájit	k5eAaPmAgMnS	zahájit
šlechtění	šlechtění	k1gNnSc1	šlechtění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
ve	v	k7c6	v
Šlechtitelské	šlechtitelský	k2eAgFnSc6d1	šlechtitelská
stanici	stanice	k1gFnSc6	stanice
vinařské	vinařský	k2eAgFnSc2d1	vinařská
ve	v	k7c6	v
Velkých	velký	k2eAgFnPc6d1	velká
Pavlovicích	Pavlovice	k1gFnPc6	Pavlovice
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
musel	muset	k5eAaImAgMnS	muset
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Ing.	ing.	kA	ing.
Veverka	Veverka	k1gMnSc1	Veverka
vybral	vybrat	k5eAaPmAgMnS	vybrat
semenáč	semenáč	k1gInSc4	semenáč
č.	č.	k?	č.
0	[number]	k4	0
<g/>
104	[number]	k4	104
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vynikal	vynikat	k5eAaImAgInS	vynikat
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Klonové	Klonový	k2eAgNnSc4d1	Klonový
potomstvo	potomstvo	k1gNnSc4	potomstvo
tohoto	tento	k3xDgInSc2	tento
semenáče	semenáč	k1gInSc2	semenáč
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
sledováno	sledovat	k5eAaImNgNnS	sledovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
Šlechtitelské	šlechtitelský	k2eAgFnSc6d1	šlechtitelská
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Perné	perný	k2eAgFnSc6d1	Perná
u	u	k7c2	u
Mikulova	Mikulov	k1gInSc2	Mikulov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Státní	státní	k2eAgFnSc2d1	státní
odrůdové	odrůdový	k2eAgFnSc2d1	odrůdová
knihy	kniha	k1gFnSc2	kniha
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
odrůda	odrůda	k1gFnSc1	odrůda
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
i	i	k9	i
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
odrůd	odrůda	k1gFnPc2	odrůda
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Výsadby	výsadba	k1gFnPc1	výsadba
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mikulovské	mikulovský	k2eAgFnSc6d1	Mikulovská
a	a	k8xC	a
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
podoblasti	podoblast	k1gFnSc6	podoblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
Perná	perný	k2eAgFnSc1d1	Perná
<g/>
,	,	kIx,	,
Lechovice	Lechovice	k1gFnPc1	Lechovice
<g/>
,	,	kIx,	,
Hrušky	hruška	k1gFnPc1	hruška
a	a	k8xC	a
Čejkovice	Čejkovice	k1gFnPc1	Čejkovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Malokarpatské	malokarpatský	k2eAgFnSc6d1	Malokarpatská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
pěstována	pěstován	k2eAgFnSc1d1	pěstována
na	na	k7c4	na
0,5	[number]	k4	0,5
%	%	kIx~	%
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
na	na	k7c4	na
1,1	[number]	k4	1,1
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
plochy	plocha	k1gFnSc2	plocha
vinohradů	vinohrad	k1gInPc2	vinohrad
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
124	[number]	k4	124
hektarů	hektar	k1gInPc2	hektar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
stáří	stáří	k1gNnSc1	stáří
vinic	vinice	k1gFnPc2	vinice
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
činilo	činit	k5eAaImAgNnS	činit
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
a	a	k8xC	a
osazené	osazený	k2eAgFnPc1d1	osazená
plochy	plocha	k1gFnPc1	plocha
vinic	vinice	k1gFnPc2	vinice
mají	mít	k5eAaImIp3nP	mít
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Udržovateli	udržovatel	k1gMnPc7	udržovatel
odrůdy	odrůda	k1gFnSc2	odrůda
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
Ampelos-Šlechtitelská	Ampelos-Šlechtitelský	k2eAgFnSc1d1	Ampelos-Šlechtitelský
stanice	stanice	k1gFnSc1	stanice
vinařská	vinařský	k2eAgFnSc1d1	vinařská
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Alois	Alois	k1gMnSc1	Alois
Tománek	Tománek	k1gMnSc1	Tománek
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Miloš	Miloš	k1gMnSc1	Miloš
Michlovský	michlovský	k2eAgMnSc1d1	michlovský
a	a	k8xC	a
ŠSV	ŠSV	kA	ŠSV
Velké	velký	k2eAgFnPc4d1	velká
Pavlovice	Pavlovice	k1gFnPc4	Pavlovice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
odrůdy	odrůda	k1gFnSc2	odrůda
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Pálava	Pálava	k1gFnSc1	Pálava
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
používaná	používaný	k2eAgNnPc1d1	používané
synonyma	synonymum	k1gNnPc1	synonymum
odrůdy	odrůda	k1gFnSc2	odrůda
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Nema	Nema	k1gFnSc1	Nema
<g/>
,	,	kIx,	,
Veverka	veverka	k1gFnSc1	veverka
<g/>
,	,	kIx,	,
TČ	tč	kA	tč
x	x	k?	x
MT	MT	kA	MT
0	[number]	k4	0
<g/>
104	[number]	k4	104
<g/>
.	.	kIx.	.
</s>
<s>
Réví	réví	k1gNnSc1	réví
pomaleji	pomale	k6eAd2	pomale
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
zimním	zimní	k2eAgInPc3d1	zimní
mrazům	mráz	k1gInPc3	mráz
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
průměrně	průměrně	k6eAd1	průměrně
odolná	odolný	k2eAgFnSc1d1	odolná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rané	raný	k2eAgNnSc4d1	rané
rašení	rašení	k1gNnSc4	rašení
je	být	k5eAaImIp3nS	být
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
pozdními	pozdní	k2eAgInPc7d1	pozdní
jarními	jarní	k2eAgInPc7d1	jarní
mrazy	mráz	k1gInPc7	mráz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
méně	málo	k6eAd2	málo
příznivých	příznivý	k2eAgFnPc6d1	příznivá
podmínkách	podmínka	k1gFnPc6	podmínka
trpí	trpět	k5eAaImIp3nP	trpět
vadnutím	vadnutí	k1gNnSc7	vadnutí
třapiny	třapina	k1gFnSc2	třapina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
doporučovány	doporučovat	k5eAaImNgFnP	doporučovat
podnože	podnož	k1gFnPc1	podnož
K	K	kA	K
5	[number]	k4	5
<g/>
BB	BB	kA	BB
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlubších	hluboký	k2eAgFnPc6d2	hlubší
půdách	půda	k1gFnPc6	půda
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
SO-4	SO-4	k1gFnSc1	SO-4
a	a	k8xC	a
T	T	kA	T
5	[number]	k4	5
<g/>
C.	C.	kA	C.
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c6	na
rýnskohesenském	rýnskohesenský	k2eAgNnSc6d1	rýnskohesenský
vedení	vedení	k1gNnSc6	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vysoké	vysoký	k2eAgNnSc1d1	vysoké
vedení	vedení	k1gNnSc1	vedení
s	s	k7c7	s
řezem	řez	k1gInSc7	řez
na	na	k7c4	na
tažně	tažeň	k1gInPc4	tažeň
<g/>
,	,	kIx,	,
se	s	k7c7	s
zatížením	zatížení	k1gNnSc7	zatížení
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
oček	očko	k1gNnPc2	očko
na	na	k7c4	na
1	[number]	k4	1
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
pravidelnější	pravidelní	k2eAgInPc1d2	pravidelní
výnosy	výnos	k1gInPc1	výnos
než	než	k8xS	než
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
výnos	výnos	k1gInSc1	výnos
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
<g/>
.	.	kIx.	.
cukernatost	cukernatost	k1gFnSc1	cukernatost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
19	[number]	k4	19
až	až	k9	až
23	[number]	k4	23
°	°	k?	°
<g/>
NM	NM	kA	NM
<g/>
.	.	kIx.	.
</s>
<s>
Hrozny	hrozen	k1gInPc1	hrozen
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
často	často	k6eAd1	často
přezrát	přezrát	k5eAaPmF	přezrát
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
přípravy	příprava	k1gFnSc2	příprava
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
jakosti	jakost	k1gFnSc6	jakost
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
a	a	k8xC	a
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
kyselin	kyselina	k1gFnPc2	kyselina
ve	v	k7c6	v
víně	víno	k1gNnSc6	víno
činí	činit	k5eAaImIp3nS	činit
6	[number]	k4	6
až	až	k9	až
11	[number]	k4	11
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
kyselin	kyselina	k1gFnPc2	kyselina
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
rajónech	rajón	k1gInPc6	rajón
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelnost	pravidelnost	k1gFnSc1	pravidelnost
výnosů	výnos	k1gInPc2	výnos
je	být	k5eAaImIp3nS	být
narušována	narušovat	k5eAaImNgFnS	narušovat
jarními	jarní	k2eAgInPc7d1	jarní
a	a	k8xC	a
zimními	zimní	k2eAgInPc7d1	zimní
mrazy	mráz	k1gInPc7	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
rašení	rašení	k1gNnSc2	rašení
oček	očko	k1gNnPc2	očko
je	být	k5eAaImIp3nS	být
raná	raný	k2eAgFnSc1d1	raná
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvete	kvést	k5eAaImIp3nS	kvést
středně	středně	k6eAd1	středně
pozdně	pozdně	k6eAd1	pozdně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Zaměkání	zaměkání	k1gNnSc1	zaměkání
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
sklizňová	sklizňový	k2eAgFnSc1d1	sklizňová
zralost	zralost	k1gFnSc1	zralost
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
na	na	k7c4	na
padlí	padlí	k1gNnSc4	padlí
révové	révový	k2eAgFnSc2d1	révová
(	(	kIx(	(
<g/>
Uncinula	Uncinula	k1gFnSc2	Uncinula
necator	necator	k1gInSc1	necator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
keře	keř	k1gInPc1	keř
jsou	být	k5eAaImIp3nP	být
dosti	dosti	k6eAd1	dosti
husté	hustý	k2eAgInPc1d1	hustý
(	(	kIx(	(
<g/>
zálistky	zálistek	k1gInPc1	zálistek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatními	ostatní	k2eAgFnPc7d1	ostatní
houbovými	houbový	k2eAgFnPc7d1	houbová
chorobami	choroba	k1gFnPc7	choroba
je	být	k5eAaImIp3nS	být
napadána	napadán	k2eAgNnPc4d1	napadáno
průměrně	průměrně	k6eAd1	průměrně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
s	s	k7c7	s
nepříznivým	příznivý	k2eNgNnSc7d1	nepříznivé
podzimním	podzimní	k2eAgNnSc7d1	podzimní
počasím	počasí	k1gNnSc7	počasí
hrozny	hrozen	k1gInPc4	hrozen
napadá	napadat	k5eAaImIp3nS	napadat
plíseň	plíseň	k1gFnSc1	plíseň
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
Botrytis	Botrytis	k1gInSc1	Botrytis
cinerea	cinere	k1gInSc2	cinere
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
mezích	mez	k1gFnPc6	mez
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgInPc4d2	vyšší
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dřevo	dřevo	k1gNnSc1	dřevo
pomaleji	pomale	k6eAd2	pomale
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
jí	on	k3xPp3gFnSc7	on
středně	středně	k6eAd1	středně
těžké	těžký	k2eAgFnPc1d1	těžká
<g/>
,	,	kIx,	,
výživné	výživný	k2eAgFnPc1d1	výživná
<g/>
,	,	kIx,	,
záhřevné	záhřevný	k2eAgFnPc1d1	záhřevná
půdy	půda	k1gFnPc1	půda
dostatečně	dostatečně	k6eAd1	dostatečně
zásobené	zásobený	k2eAgFnPc1d1	zásobená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchých	suchý	k2eAgNnPc6d1	suché
stanovištích	stanoviště	k1gNnPc6	stanoviště
bývají	bývat	k5eAaImIp3nP	bývat
hrozny	hrozen	k1gInPc1	hrozen
řídké	řídký	k2eAgInPc1d1	řídký
<g/>
,	,	kIx,	,
sklizně	sklizeň	k1gFnPc1	sklizeň
nižší	nízký	k2eAgFnPc1d2	nižší
a	a	k8xC	a
kolísavé	kolísavý	k2eAgFnPc1d1	kolísavá
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
nejkvalitnějších	kvalitní	k2eAgFnPc2d3	nejkvalitnější
<g/>
,	,	kIx,	,
teplých	teplý	k2eAgFnPc2d1	teplá
a	a	k8xC	a
bezmrazových	bezmrazový	k2eAgFnPc2d1	bezmrazový
poloh	poloha	k1gFnPc2	poloha
na	na	k7c4	na
jižně	jižně	k6eAd1	jižně
exponované	exponovaný	k2eAgInPc4d1	exponovaný
svahy	svah	k1gInPc4	svah
<g/>
.	.	kIx.	.
</s>
<s>
Pálava	Pálava	k1gFnSc1	Pálava
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jakostních	jakostní	k2eAgFnPc2d1	jakostní
a	a	k8xC	a
výběrových	výběrový	k2eAgFnPc2d1	výběrová
vín	vína	k1gFnPc2	vína
až	až	k9	až
po	po	k7c4	po
kategorie	kategorie	k1gFnPc4	kategorie
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
a	a	k8xC	a
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
aromatickou	aromatický	k2eAgFnSc4d1	aromatická
odrůdu	odrůda	k1gFnSc4	odrůda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
více	hodně	k6eAd2	hodně
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
zpracování	zpracování	k1gNnSc2	zpracování
hroznů	hrozen	k1gInPc2	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
zvolit	zvolit	k5eAaPmF	zvolit
přísně	přísně	k6eAd1	přísně
reduktivní	reduktivní	k2eAgFnSc4d1	reduktivní
technologii	technologie	k1gFnSc4	technologie
výroby	výroba	k1gFnSc2	výroba
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
svěžích	svěží	k2eAgMnPc2d1	svěží
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
pitelných	pitelný	k2eAgNnPc2d1	pitelné
vín	víno	k1gNnPc2	víno
(	(	kIx(	(
<g/>
jakostní	jakostní	k2eAgNnPc1d1	jakostní
vína	víno	k1gNnPc1	víno
a	a	k8xC	a
nižší	nízký	k2eAgInPc1d2	nižší
přívlastky	přívlastek	k1gInPc1	přívlastek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
macerace	macerace	k1gFnPc4	macerace
rmutu	rmut	k1gInSc2	rmut
za	za	k7c2	za
řízené	řízený	k2eAgFnSc2d1	řízená
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgInPc1d2	vyšší
přívlastky	přívlastek	k1gInPc1	přívlastek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
aromatických	aromatický	k2eAgFnPc2d1	aromatická
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
jsou	být	k5eAaImIp3nP	být
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
a	a	k8xC	a
plnější	plný	k2eAgFnSc1d2	plnější
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
aroma	aroma	k1gNnSc7	aroma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ponechat	ponechat	k5eAaPmF	ponechat
vyšší	vysoký	k2eAgInSc4d2	vyšší
obsah	obsah	k1gInSc4	obsah
zbytkového	zbytkový	k2eAgInSc2d1	zbytkový
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvýrazní	zvýraznit	k5eAaPmIp3nS	zvýraznit
aromatický	aromatický	k2eAgInSc4d1	aromatický
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
chuťovou	chuťový	k2eAgFnSc4d1	chuťová
plnost	plnost	k1gFnSc4	plnost
<g/>
.	.	kIx.	.
</s>
<s>
Výběrová	výběrový	k2eAgNnPc1d1	výběrové
vína	víno	k1gNnPc1	víno
jsou	být	k5eAaImIp3nP	být
vhodná	vhodný	k2eAgNnPc1d1	vhodné
i	i	k8xC	i
na	na	k7c4	na
střednědobou	střednědobý	k2eAgFnSc4d1	střednědobá
archivaci	archivace	k1gFnSc4	archivace
<g/>
.	.	kIx.	.
</s>
<s>
Typový	typový	k2eAgInSc1d1	typový
charakter	charakter	k1gInSc1	charakter
vín	víno	k1gNnPc2	víno
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
vínům	víno	k1gNnPc3	víno
odrůdy	odrůda	k1gFnSc2	odrůda
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kořenitá	kořenitý	k2eAgFnSc1d1	kořenitá
plnost	plnost	k1gFnSc1	plnost
bývá	bývat	k5eAaImIp3nS	bývat
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
acidita	acidita	k1gFnSc1	acidita
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Aciditu	acidita	k1gFnSc4	acidita
je	být	k5eAaImIp3nS	být
každopádně	každopádně	k6eAd1	každopádně
nutné	nutný	k2eAgNnSc1d1	nutné
zachovat	zachovat	k5eAaPmF	zachovat
pro	pro	k7c4	pro
příjemné	příjemný	k2eAgNnSc4d1	příjemné
vyznění	vyznění	k1gNnSc4	vyznění
<g/>
.	.	kIx.	.
</s>
<s>
Vína	víno	k1gNnPc1	víno
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
zlatožluté	zlatožlutý	k2eAgFnPc1d1	zlatožlutá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vůni	vůně	k1gFnSc6	vůně
jsou	být	k5eAaImIp3nP	být
aromatické	aromatický	k2eAgFnPc1d1	aromatická
látky	látka	k1gFnPc1	látka
Tramínu	tramín	k1gInSc2	tramín
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc4d1	doplněná
o	o	k7c4	o
vanilkové	vanilkový	k2eAgInPc4d1	vanilkový
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
<g/>
,	,	kIx,	,
dlouhotrvající	dlouhotrvající	k2eAgFnSc1d1	dlouhotrvající
<g/>
,	,	kIx,	,
jemnější	jemný	k2eAgFnSc1d2	jemnější
harmonie	harmonie	k1gFnSc1	harmonie
než	než	k8xS	než
u	u	k7c2	u
Tramínu	tramín	k1gInSc2	tramín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vůni	vůně	k1gFnSc6	vůně
a	a	k8xC	a
chuti	chuť	k1gFnSc6	chuť
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
koření	koření	k1gNnSc4	koření
<g/>
,	,	kIx,	,
muškát	muškát	k1gInSc4	muškát
<g/>
,	,	kIx,	,
vanilku	vanilka	k1gFnSc4	vanilka
<g/>
,	,	kIx,	,
mandarinky	mandarinka	k1gFnPc1	mandarinka
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Tramín	tramín	k1gInSc1	tramín
k	k	k7c3	k
silněji	silně	k6eAd2	silně
kořeněným	kořeněný	k2eAgNnPc3d1	kořeněné
jídlům	jídlo	k1gNnPc3	jídlo
<g/>
,	,	kIx,	,
k	k	k7c3	k
paštikám	paštika	k1gFnPc3	paštika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
sladkým	sladký	k2eAgFnPc3d1	sladká
úpravám	úprava	k1gFnPc3	úprava
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
k	k	k7c3	k
jemným	jemný	k2eAgInPc3d1	jemný
ovčím	ovčí	k2eAgInPc3d1	ovčí
či	či	k8xC	či
kozím	kozí	k2eAgInPc3d1	kozí
sýrům	sýr	k1gInPc3	sýr
<g/>
,	,	kIx,	,
chipsům	chips	k1gInPc3	chips
a	a	k8xC	a
olivám	oliva	k1gFnPc3	oliva
<g/>
.	.	kIx.	.
</s>
