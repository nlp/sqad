<p>
<s>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ernesta	Ernest	k1gMnSc2	Ernest
Hemingwaye	Hemingway	k1gMnSc2	Hemingway
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaBmAgInS	napsat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
kubánském	kubánský	k2eAgMnSc6d1	kubánský
rybáři	rybář	k1gMnSc6	rybář
Santiagovi	Santiag	k1gMnSc6	Santiag
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
rybařit	rybařit	k5eAaImF	rybařit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
prolomil	prolomit	k5eAaPmAgMnS	prolomit
své	svůj	k3xOyFgNnSc4	svůj
osmdesátičtyřdenní	osmdesátičtyřdenní	k2eAgNnSc4d1	osmdesátičtyřdenní
smolné	smolný	k2eAgNnSc4d1	smolné
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nechytil	chytit	k5eNaPmAgMnS	chytit
žádnou	žádný	k3yNgFnSc4	žádný
rybu	ryba	k1gFnSc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
září	září	k1gNnSc6	září
1952	[number]	k4	1952
se	se	k3xPyFc4	se
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
prodalo	prodat	k5eAaPmAgNnS	prodat
5,2	[number]	k4	5,2
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hemingway	Hemingwaa	k1gFnPc1	Hemingwaa
žil	žíla	k1gFnPc2	žíla
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
postava	postava	k1gFnSc1	postava
Santiaga	Santiago	k1gNnSc2	Santiago
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
žádného	žádný	k3yNgMnSc4	žádný
skutečného	skutečný	k2eAgMnSc4d1	skutečný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisci	životopisec	k1gMnPc1	životopisec
však	však	k9	však
přesto	přesto	k8xC	přesto
za	za	k7c4	za
předobraz	předobraz	k1gInSc4	předobraz
Santiaga	Santiago	k1gNnSc2	Santiago
považují	považovat	k5eAaImIp3nP	považovat
kubánského	kubánský	k2eAgMnSc4d1	kubánský
rybáře	rybář	k1gMnSc4	rybář
a	a	k8xC	a
přítele	přítel	k1gMnSc4	přítel
Hemingwaye	Hemingwaye	k1gFnSc7	Hemingwaye
Gregoria	Gregorium	k1gNnSc2	Gregorium
Fuentese	Fuentese	k1gFnPc4	Fuentese
z	z	k7c2	z
malého	malý	k2eAgInSc2d1	malý
přístavu	přístav	k1gInSc2	přístav
Cojímar	Cojímara	k1gFnPc2	Cojímara
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Havany	Havana	k1gFnSc2	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
na	na	k7c6	na
Lanzarote	Lanzarot	k1gMnSc5	Lanzarot
a	a	k8xC	a
jako	jako	k9	jako
šestiletý	šestiletý	k2eAgMnSc1d1	šestiletý
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
kapitánem	kapitán	k1gMnSc7	kapitán
Hemingwayovy	Hemingwayův	k2eAgFnSc2d1	Hemingwayova
jachty	jachta	k1gFnSc2	jachta
Pilar	Pilara	k1gFnPc2	Pilara
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
v	v	k7c6	v
Cojímaru	Cojímar	k1gInSc6	Cojímar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
za	za	k7c4	za
novelu	novela	k1gFnSc4	novela
dostal	dostat	k5eAaPmAgMnS	dostat
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgInSc7d1	poslední
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ji	on	k3xPp3gFnSc4	on
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Tympanum	Tympanum	k1gNnSc4	Tympanum
vydalo	vydat	k5eAaPmAgNnS	vydat
jako	jako	k8xC	jako
audioknihu	audioknih	k1gInSc2	audioknih
načtenou	načtený	k2eAgFnSc4d1	načtená
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Mrkvičkou	mrkvička	k1gFnSc7	mrkvička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Stařičkého	stařičký	k2eAgMnSc2d1	stařičký
kubánského	kubánský	k2eAgMnSc2d1	kubánský
rybáře	rybář	k1gMnSc2	rybář
Santiaga	Santiago	k1gNnSc2	Santiago
už	už	k9	už
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
84	[number]	k4	84
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
provází	provázet	k5eAaImIp3nS	provázet
smůla	smůla	k1gFnSc1	smůla
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepovedlo	povést	k5eNaPmAgNnS	povést
chytit	chytit	k5eAaPmF	chytit
žádnou	žádný	k3yNgFnSc4	žádný
větší	veliký	k2eAgFnSc4d2	veliký
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInPc3	jeho
neúspěchům	neúspěch	k1gInPc3	neúspěch
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
ho	on	k3xPp3gInSc2	on
musel	muset	k5eAaImAgInS	muset
opustit	opustit	k5eAaPmF	opustit
jeho	jeho	k3xOp3gMnSc1	jeho
mladý	mladý	k2eAgMnSc1d1	mladý
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
poručil	poručit	k5eAaPmAgMnS	poručit
jezdit	jezdit	k5eAaImF	jezdit
rybařit	rybařit	k5eAaImF	rybařit
s	s	k7c7	s
úspěšnějšími	úspěšný	k2eAgMnPc7d2	úspěšnější
rybáři	rybář	k1gMnPc7	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Stařec	stařec	k1gMnSc1	stařec
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
jako	jako	k9	jako
podivného	podivný	k2eAgNnSc2d1	podivné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
když	když	k8xS	když
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
starý	starý	k2eAgInSc1d1	starý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc1	jeho
oči	oko	k1gNnPc1	oko
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
jiskru	jiskra	k1gFnSc4	jiskra
veselosti	veselost	k1gFnSc2	veselost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Santiago	Santiago	k1gNnSc1	Santiago
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
prolomit	prolomit	k5eAaPmF	prolomit
svoje	svůj	k3xOyFgNnSc4	svůj
smolné	smolný	k2eAgNnSc4d1	smolné
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
rybáři	rybář	k1gMnPc1	rybář
<g/>
,	,	kIx,	,
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
časně	časně	k6eAd1	časně
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neplánuje	plánovat	k5eNaImIp3nS	plánovat
lovit	lovit	k5eAaImF	lovit
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
úsvitu	úsvit	k1gInSc2	úsvit
žene	hnát	k5eAaImIp3nS	hnát
loďku	loďka	k1gFnSc4	loďka
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
i	i	k9	i
nejbližší	blízký	k2eAgInPc1d3	Nejbližší
čluny	člun	k1gInPc1	člun
nevidí	vidět	k5eNaImIp3nP	vidět
jako	jako	k9	jako
malé	malý	k2eAgFnPc1d1	malá
tečky	tečka	k1gFnPc1	tečka
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
ponoří	ponořit	k5eAaPmIp3nP	ponořit
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
návnady	návnada	k1gFnSc2	návnada
<g/>
,	,	kIx,	,
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
však	však	k9	však
kromě	kromě	k7c2	kromě
malého	malý	k2eAgMnSc2d1	malý
tuňáka	tuňák	k1gMnSc2	tuňák
žádná	žádný	k3yNgFnSc1	žádný
ryba	ryba	k1gFnSc1	ryba
ani	ani	k9	ani
nedotkne	dotknout	k5eNaPmIp3nS	dotknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stařec	stařec	k1gMnSc1	stařec
už	už	k6eAd1	už
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
když	když	k8xS	když
vtom	vtom	k6eAd1	vtom
se	se	k3xPyFc4	se
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
udici	udice	k1gFnSc4	udice
chytne	chytnout	k5eAaPmIp3nS	chytnout
ryba	ryba	k1gFnSc1	ryba
takovou	takový	k3xDgFnSc7	takový
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
že	že	k8xS	že
málem	málem	k6eAd1	málem
chatrnou	chatrný	k2eAgFnSc4d1	chatrná
loďku	loďka	k1gFnSc4	loďka
překotí	překotit	k5eAaPmIp3nS	překotit
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
rybář	rybář	k1gMnSc1	rybář
hned	hned	k6eAd1	hned
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nejde	jít	k5eNaImIp3nS	jít
zabít	zabít	k5eAaPmF	zabít
jednoduše	jednoduše	k6eAd1	jednoduše
–	–	k?	–
naváže	navázat	k5eAaPmIp3nS	navázat
tedy	tedy	k9	tedy
na	na	k7c4	na
udici	udice	k1gFnSc4	udice
záložní	záložní	k2eAgInPc4d1	záložní
kotouče	kotouč	k1gInPc4	kotouč
šňůry	šňůra	k1gFnSc2	šňůra
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usilovně	usilovně	k6eAd1	usilovně
táhne	táhnout	k5eAaImIp3nS	táhnout
loďku	loďka	k1gFnSc4	loďka
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
unaví	unavit	k5eAaPmIp3nP	unavit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
harpunovat	harpunovat	k5eAaBmF	harpunovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
nemůže	moct	k5eNaImIp3nS	moct
přivázat	přivázat	k5eAaPmF	přivázat
šňůru	šňůra	k1gFnSc4	šňůra
k	k	k7c3	k
loďce	loďka	k1gFnSc3	loďka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nepřetrhla	přetrhnout	k5eNaPmAgFnS	přetrhnout
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
šňůru	šňůra	k1gFnSc4	šňůra
drží	držet	k5eAaImIp3nS	držet
svýma	svůj	k3xOyFgFnPc7	svůj
zjizvenýma	zjizvený	k2eAgFnPc7d1	zjizvená
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
po	po	k7c6	po
pár	pár	k4xCyI	pár
hodinách	hodina	k1gFnPc6	hodina
unavený	unavený	k2eAgMnSc1d1	unavený
a	a	k8xC	a
vysílený	vysílený	k2eAgMnSc1d1	vysílený
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začíná	začínat	k5eAaImIp3nS	začínat
padat	padat	k5eAaImF	padat
tma	tma	k1gFnSc1	tma
<g/>
,	,	kIx,	,
ryba	ryba	k1gFnSc1	ryba
stále	stále	k6eAd1	stále
táhne	táhnout	k5eAaImIp3nS	táhnout
stejnou	stejný	k2eAgFnSc7d1	stejná
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
stařec	stařec	k1gMnSc1	stařec
začíná	začínat	k5eAaImIp3nS	začínat
pomalu	pomalu	k6eAd1	pomalu
upadat	upadat	k5eAaPmF	upadat
do	do	k7c2	do
mdlob	mdloba	k1gFnPc2	mdloba
<g/>
.	.	kIx.	.
</s>
<s>
Sní	snít	k5eAaImIp3nP	snít
tedy	tedy	k9	tedy
tuňáka	tuňák	k1gMnSc4	tuňák
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
chytil	chytit	k5eAaPmAgMnS	chytit
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
a	a	k8xC	a
doplní	doplnit	k5eAaPmIp3nS	doplnit
síly	síla	k1gFnSc2	síla
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ale	ale	k9	ale
nemá	mít	k5eNaImIp3nS	mít
nazbyt	nazbyt	k6eAd1	nazbyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souboj	souboj	k1gInSc1	souboj
starce	stařec	k1gMnSc2	stařec
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
ryba	ryba	k1gFnSc1	ryba
ale	ale	k9	ale
nemůže	moct	k5eNaImIp3nS	moct
táhnout	táhnout	k5eAaImF	táhnout
loďku	loďka	k1gFnSc4	loďka
věčně	věčně	k6eAd1	věčně
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
když	když	k8xS	když
začne	začít	k5eAaPmIp3nS	začít
pod	pod	k7c7	pod
loďkou	loďka	k1gFnSc7	loďka
kroužit	kroužit	k5eAaImF	kroužit
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
háku	hák	k1gInSc3	hák
<g/>
,	,	kIx,	,
svitne	svitnout	k5eAaPmIp3nS	svitnout
konečně	konečně	k6eAd1	konečně
starci	stařec	k1gMnPc1	stařec
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
a	a	k8xC	a
vysilujícím	vysilující	k2eAgInSc6d1	vysilující
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
starci	stařec	k1gMnPc1	stařec
konečně	konečně	k6eAd1	konečně
podaří	podařit	k5eAaPmIp3nP	podařit
rybu	ryba	k1gFnSc4	ryba
zabít	zabít	k5eAaPmF	zabít
harpunou	harpuna	k1gFnSc7	harpuna
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
její	její	k3xOp3gInPc4	její
ohromné	ohromný	k2eAgInPc4d1	ohromný
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
ihned	ihned	k6eAd1	ihned
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
chytil	chytit	k5eAaPmAgMnS	chytit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
jejím	její	k3xOp3gInPc3	její
rozměrům	rozměr	k1gInPc3	rozměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
rybu	ryba	k1gFnSc4	ryba
naložit	naložit	k5eAaPmF	naložit
na	na	k7c4	na
loďku	loďka	k1gFnSc4	loďka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
přiváže	přivázat	k5eAaPmIp3nS	přivázat
k	k	k7c3	k
boku	bok	k1gInSc3	bok
<g/>
,	,	kIx,	,
nastaví	nastavět	k5eAaBmIp3nS	nastavět
plachtu	plachta	k1gFnSc4	plachta
a	a	k8xC	a
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
doufá	doufat	k5eAaImIp3nS	doufat
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
Havanu	Havana	k1gFnSc4	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Stařec	stařec	k1gMnSc1	stařec
má	mít	k5eAaImIp3nS	mít
konečně	konečně	k6eAd1	konečně
příležitost	příležitost	k1gFnSc4	příležitost
si	se	k3xPyFc3	se
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nadlouho	nadlouho	k6eAd1	nadlouho
–	–	k?	–
po	po	k7c6	po
pár	pár	k4xCyI	pár
hodinách	hodina	k1gFnPc6	hodina
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
Santiagovu	Santiagův	k2eAgFnSc4d1	Santiagova
rybu	ryba	k1gFnSc4	ryba
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
přivábeni	přiváben	k2eAgMnPc1d1	přiváben
vůni	vůně	k1gFnSc4	vůně
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Stařec	stařec	k1gMnSc1	stařec
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevzdává	vzdávat	k5eNaImIp3nS	vzdávat
a	a	k8xC	a
oba	dva	k4xCgMnPc4	dva
žraloky	žralok	k1gMnPc4	žralok
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
ryby	ryba	k1gFnSc2	ryba
ukousnout	ukousnout	k5eAaPmF	ukousnout
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
harpunou	harpuna	k1gFnSc7	harpuna
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nebyli	být	k5eNaImAgMnP	být
poslední	poslední	k2eAgMnPc1d1	poslední
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
potkal	potkat	k5eAaPmAgMnS	potkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
po	po	k7c6	po
pár	pár	k4xCyI	pár
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
ryba	ryba	k1gFnSc1	ryba
opět	opět	k6eAd1	opět
stane	stanout	k5eAaPmIp3nS	stanout
terčem	terč	k1gInSc7	terč
útoku	útok	k1gInSc2	útok
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
starci	stařec	k1gMnPc7	stařec
opět	opět	k6eAd1	opět
povede	povést	k5eAaPmIp3nS	povést
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
ztratí	ztratit	k5eAaPmIp3nP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
harpunu	harpuna	k1gFnSc4	harpuna
<g/>
.	.	kIx.	.
</s>
<s>
Nemíní	mínit	k5eNaImIp3nS	mínit
se	se	k3xPyFc4	se
však	však	k9	však
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
k	k	k7c3	k
pádlu	pádlo	k1gNnSc3	pádlo
přiváže	přivázat	k5eAaPmIp3nS	přivázat
nůž	nůž	k1gInSc1	nůž
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
útok	útok	k1gInSc1	útok
dalších	další	k2eAgMnPc2d1	další
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
přijde	přijít	k5eAaPmIp3nS	přijít
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
a	a	k8xC	a
Santiago	Santiago	k1gNnSc1	Santiago
svou	svůj	k3xOyFgFnSc4	svůj
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
už	už	k6eAd1	už
polovinu	polovina	k1gFnSc4	polovina
sežrali	sežrat	k5eAaPmAgMnP	sežrat
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
usilovně	usilovně	k6eAd1	usilovně
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Nůž	nůž	k1gInSc1	nůž
však	však	k9	však
není	být	k5eNaImIp3nS	být
stavěný	stavěný	k2eAgMnSc1d1	stavěný
na	na	k7c4	na
prolamování	prolamování	k1gNnPc4	prolamování
žraločích	žraločí	k2eAgFnPc2d1	žraločí
lebek	lebka	k1gFnPc2	lebka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zlomí	zlomit	k5eAaPmIp3nS	zlomit
a	a	k8xC	a
stařec	stařec	k1gMnSc1	stařec
je	být	k5eAaImIp3nS	být
najednou	najednou	k6eAd1	najednou
bezmocný	bezmocný	k2eAgMnSc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc4d1	další
žraloky	žralok	k1gMnPc4	žralok
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
umlátit	umlátit	k5eAaPmF	umlátit
kyjem	kyj	k1gInSc7	kyj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
usmrcování	usmrcování	k1gNnSc4	usmrcování
ulovených	ulovený	k2eAgFnPc2d1	ulovená
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
větrným	větrný	k2eAgInPc3d1	větrný
mlýnům	mlýn	k1gInPc3	mlýn
–	–	k?	–
žralokům	žralok	k1gMnPc3	žralok
starcovy	starcův	k2eAgInPc1d1	starcův
zoufalé	zoufalý	k2eAgInPc1d1	zoufalý
údery	úder	k1gInPc1	úder
nepůsobí	působit	k5eNaImIp3nP	působit
žádnou	žádný	k3yNgFnSc4	žádný
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
z	z	k7c2	z
ryby	ryba	k1gFnSc2	ryba
ohlodají	ohlodat	k5eAaPmIp3nP	ohlodat
poslední	poslední	k2eAgInPc4d1	poslední
kousky	kousek	k1gInPc4	kousek
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pár	pár	k4xCyI	pár
hodinách	hodina	k1gFnPc6	hodina
Santiago	Santiago	k1gNnSc1	Santiago
konečně	konečně	k6eAd1	konečně
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
noční	noční	k2eAgFnSc2d1	noční
Havany	Havana	k1gFnSc2	Havana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
kostrou	kostra	k1gFnSc7	kostra
obrovské	obrovský	k2eAgFnSc2d1	obrovská
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Dobelhá	dobelhat	k5eAaPmIp3nS	dobelhat
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
chatrče	chatrč	k1gFnSc2	chatrč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
okamžitě	okamžitě	k6eAd1	okamžitě
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
hlubokého	hluboký	k2eAgInSc2d1	hluboký
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
ho	on	k3xPp3gInSc4	on
probere	probrat	k5eAaPmIp3nS	probrat
až	až	k9	až
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
strach	strach	k1gInSc1	strach
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nesmírně	smírně	k6eNd1	smírně
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
posteli	postel	k1gFnSc6	postel
vystlané	vystlaný	k2eAgNnSc1d1	vystlané
novinami	novina	k1gFnPc7	novina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
brečí	brečet	k5eAaImIp3nS	brečet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mu	on	k3xPp3gMnSc3	on
stařec	stařec	k1gMnSc1	stařec
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
uplynulých	uplynulý	k2eAgInPc2d1	uplynulý
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
starcovy	starcův	k2eAgFnSc2d1	starcova
loďky	loďka	k1gFnSc2	loďka
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
shromáždí	shromáždit	k5eAaPmIp3nS	shromáždit
dav	dav	k1gInSc1	dav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
kostru	kostra	k1gFnSc4	kostra
obrovské	obrovský	k2eAgFnSc2d1	obrovská
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
povaluje	povalovat	k5eAaImIp3nS	povalovat
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jak	jak	k8xS	jak
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
splynutí	splynutí	k1gNnSc1	splynutí
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
(	(	kIx(	(
<g/>
stařec	stařec	k1gMnSc1	stařec
s	s	k7c7	s
rybou	ryba	k1gFnSc7	ryba
-	-	kIx~	-
vnímá	vnímat	k5eAaImIp3nS	vnímat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sobě	se	k3xPyFc3	se
rovnou	rovnou	k6eAd1	rovnou
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
úžasnou	úžasný	k2eAgFnSc4d1	úžasná
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
starce	stařec	k1gMnSc2	stařec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
lovení	lovení	k1gNnSc6	lovení
nepolevuje	polevovat	k5eNaImIp3nS	polevovat
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
upadá	upadat	k5eAaPmIp3nS	upadat
do	do	k7c2	do
mdlob	mdloba	k1gFnPc2	mdloba
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odhodlanost	odhodlanost	k1gFnSc1	odhodlanost
nás	my	k3xPp1nPc4	my
vždy	vždy	k6eAd1	vždy
dovede	dovést	k5eAaPmIp3nS	dovést
k	k	k7c3	k
vytyčenému	vytyčený	k2eAgInSc3d1	vytyčený
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
-	-	kIx~	-
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Henry	Henry	k1gMnSc1	Henry
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Zinnemann	Zinnemann	k1gMnSc1	Zinnemann
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Sturges	Sturges	k1gMnSc1	Sturges
<g/>
;	;	kIx,	;
trvání	trvání	k1gNnSc1	trvání
<g/>
:	:	kIx,	:
86	[number]	k4	86
minut	minuta	k1gFnPc2	minuta
</s>
</p>
<p>
<s>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
britský	britský	k2eAgInSc1d1	britský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
<g/>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jud	judo	k1gNnPc2	judo
Taylor	Taylora	k1gFnPc2	Taylora
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
25	[number]	k4	25
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
;	;	kIx,	;
trvání	trvání	k1gNnSc2	trvání
<g/>
:	:	kIx,	:
93	[number]	k4	93
minut	minuta	k1gFnPc2	minuta
</s>
</p>
<p>
<s>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
japonský	japonský	k2eAgInSc1d1	japonský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
Petrov	Petrov	k1gInSc1	Petrov
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
;	;	kIx,	;
trvání	trvání	k1gNnSc2	trvání
<g/>
:	:	kIx,	:
22	[number]	k4	22
minut	minuta	k1gFnPc2	minuta
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
1	[number]	k4	1
<g/>
/	/	kIx~	/
A-	A-	k1gMnPc2	A-
<g/>
L.	L.	kA	L.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
948	[number]	k4	948
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
475	[number]	k4	475
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
generace	generace	k1gFnSc1	generace
</s>
</p>
