<s>
Stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ag	Ag	k1gFnSc2	Ag
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Argentum	argentum	k1gNnSc4	argentum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
kov	kov	k1gInSc4	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
