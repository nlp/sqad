<p>
<s>
Mořčák	Mořčák	k1gInSc1	Mořčák
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Dicentrarchus	Dicentrarchus	k1gInSc1	Dicentrarchus
labrax	labrax	k1gInSc1	labrax
<g/>
;	;	kIx,	;
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dravá	dravý	k2eAgFnSc1d1	dravá
mořská	mořský	k2eAgFnSc1d1	mořská
ostnoploutvá	ostnoploutvý	k2eAgFnSc1d1	ostnoploutvá
ryba	ryba	k1gFnSc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
je	být	k5eAaImIp3nS	být
ryba	ryba	k1gFnSc1	ryba
známá	známý	k2eAgFnSc1d1	známá
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
mořčák	mořčák	k1gInSc1	mořčák
chutný	chutný	k2eAgMnSc1d1	chutný
či	či	k8xC	či
mořský	mořský	k2eAgMnSc1d1	mořský
vlk	vlk	k1gMnSc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
chovaných	chovaný	k2eAgFnPc2d1	chovaná
v	v	k7c6	v
akvakultuře	akvakultura	k1gFnSc6	akvakultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnPc4	délka
až	až	k9	až
100	[number]	k4	100
cm	cm	kA	cm
<g/>
,	,	kIx,	,
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
10	[number]	k4	10
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
cykloidní	cykloidní	k2eAgFnPc4d1	cykloidní
šupiny	šupina	k1gFnPc4	šupina
<g/>
,	,	kIx,	,
ústa	ústa	k1gNnPc1	ústa
jsou	být	k5eAaImIp3nP	být
široká	široký	k2eAgNnPc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
velkými	velký	k2eAgFnPc7d1	velká
<g/>
,	,	kIx,	,
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
původu	původ	k1gInSc6	původ
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
od	od	k7c2	od
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnSc2d1	šedá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
nebo	nebo	k8xC	nebo
zelené	zelená	k1gFnSc2	zelená
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc4	břicho
mají	mít	k5eAaImIp3nP	mít
bílé	bílý	k2eAgInPc1d1	bílý
či	či	k8xC	či
světle	světle	k6eAd1	světle
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Boky	boka	k1gFnPc1	boka
jsou	být	k5eAaImIp3nP	být
stříbrno-modré	stříbrnoodrý	k2eAgFnPc1d1	stříbrno-modrá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bledě	bledě	k6eAd1	bledě
zlaté	zlatý	k2eAgFnPc4d1	zlatá
nebo	nebo	k8xC	nebo
bronzové	bronzový	k2eAgFnPc4d1	bronzová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
bývá	bývat	k5eAaImIp3nS	bývat
světlejší	světlý	k2eAgInSc1d2	světlejší
než	než	k8xS	než
starší	starý	k2eAgFnPc1d2	starší
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
tmavé	tmavý	k2eAgFnPc4d1	tmavá
skvrny	skvrna	k1gFnPc4	skvrna
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Normálně	normálně	k6eAd1	normálně
tyto	tento	k3xDgFnPc1	tento
skvrny	skvrna	k1gFnPc1	skvrna
po	po	k7c6	po
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
hřbetní	hřbetní	k2eAgFnPc4d1	hřbetní
ploutve	ploutev	k1gFnPc4	ploutev
<g/>
;	;	kIx,	;
první	první	k4xOgFnPc4	první
s	s	k7c7	s
8	[number]	k4	8
až	až	k8xS	až
10	[number]	k4	10
trny	trn	k1gInPc7	trn
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
s	s	k7c7	s
1	[number]	k4	1
trnem	trn	k1gInSc7	trn
a	a	k8xC	a
12	[number]	k4	12
nebo	nebo	k8xC	nebo
13	[number]	k4	13
měkkými	měkký	k2eAgInPc7d1	měkký
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Řitní	řitní	k2eAgFnSc1d1	řitní
ploutev	ploutev	k1gFnSc1	ploutev
se	s	k7c7	s
3	[number]	k4	3
trny	trn	k1gInPc7	trn
a	a	k8xC	a
10	[number]	k4	10
až	až	k8xS	až
12	[number]	k4	12
měkkými	měkký	k2eAgInPc7d1	měkký
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Mořčák	Mořčák	k1gMnSc1	Mořčák
evropský	evropský	k2eAgMnSc1d1	evropský
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
východního	východní	k2eAgInSc2d1	východní
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
od	od	k7c2	od
severního	severní	k2eAgNnSc2d1	severní
Norska	Norsko	k1gNnSc2	Norsko
až	až	k9	až
po	po	k7c4	po
Maroko	Maroko	k1gNnSc4	Maroko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Kapverdské	kapverdský	k2eAgInPc4d1	kapverdský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
do	do	k7c2	do
Senegalu	Senegal	k1gInSc2	Senegal
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
i	i	k9	i
ze	z	k7c2	z
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
v	v	k7c6	v
Baltském	baltský	k2eAgInSc6d1	baltský
<g/>
,	,	kIx,	,
Barentsově	barentsově	k6eAd1	barentsově
<g/>
,	,	kIx,	,
Bílém	bílý	k2eAgMnSc6d1	bílý
a	a	k8xC	a
Kaspickém	kaspický	k2eAgNnSc6d1	Kaspické
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
vody	voda	k1gFnPc4	voda
do	do	k7c2	do
100	[number]	k4	100
m	m	kA	m
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžněji	běžně	k6eAd2	běžně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
či	či	k8xC	či
v	v	k7c6	v
lagunách	laguna	k1gFnPc6	laguna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
chladnějším	chladný	k2eAgNnSc6d2	chladnější
počasí	počasí	k1gNnSc6	počasí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snáší	snášet	k5eAaImIp3nP	snášet
velké	velký	k2eAgInPc4d1	velký
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
5-28	[number]	k4	5-28
°	°	k?	°
<g/>
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dravý	dravý	k2eAgInSc1d1	dravý
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
převážně	převážně	k6eAd1	převážně
malými	malý	k2eAgFnPc7d1	malá
pelagickými	pelagický	k2eAgFnPc7d1	pelagická
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sardinky	sardinka	k1gFnPc4	sardinka
a	a	k8xC	a
smáčci	smáček	k1gMnPc1	smáček
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
korýši	korýš	k1gMnPc1	korýš
a	a	k8xC	a
chobotnicemi	chobotnice	k1gFnPc7	chobotnice
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
spíš	spíš	k9	spíš
bezobratlé	bezobratlý	k2eAgFnPc1d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oportunistický	oportunistický	k2eAgMnSc1d1	oportunistický
predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
druhem	druh	k1gInSc7	druh
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
sezónně	sezónně	k6eAd1	sezónně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
různé	různý	k2eAgFnPc4d1	různá
strategie	strategie	k1gFnPc4	strategie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
lov	lov	k1gInSc4	lov
kořisti	kořist	k1gFnSc2	kořist
co	co	k9	co
nejefektivnější	efektivní	k2eAgFnSc1d3	nejefektivnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tření	tření	k1gNnSc1	tření
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jikry	jikra	k1gFnPc1	jikra
jsou	být	k5eAaImIp3nP	být
pelagické	pelagický	k2eAgFnPc1d1	pelagická
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
mořčák	mořčák	k1gInSc1	mořčák
prvně	prvně	k?	prvně
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
reprodukce	reprodukce	k1gFnSc2	reprodukce
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
samci	samec	k1gMnPc1	samec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4-7	[number]	k4	4-7
let	léto	k1gNnPc2	léto
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
5-8	[number]	k4	5-8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytírá	vytírat	k5eAaImIp3nS	vytírat
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vytřít	vytřít	k5eAaPmF	vytřít
i	i	k9	i
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
embrya	embryo	k1gNnSc2	embryo
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
dny	den	k1gInPc4	den
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
13-14	[number]	k4	13-14
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
larvy	larva	k1gFnSc2	larva
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
dní	den	k1gInPc2	den
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
19	[number]	k4	19
°	°	k?	°
C.	C.	kA	C.
Velikost	velikost	k1gFnSc1	velikost
jiker	jikra	k1gFnPc2	jikra
je	být	k5eAaImIp3nS	být
1,1	[number]	k4	1,1
<g/>
-	-	kIx~	-
<g/>
1,5	[number]	k4	1,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Mořčák	Mořčák	k1gInSc1	Mořčák
evropský	evropský	k2eAgInSc1d1	evropský
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněná	ceněný	k2eAgFnSc1d1	ceněná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
ryba	ryba	k1gFnSc1	ryba
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgInSc7	první
mořským	mořský	k2eAgInSc7d1	mořský
druhem	druh	k1gInSc7	druh
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
lososovitých	lososovitý	k2eAgFnPc2d1	lososovitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
komerčně	komerčně	k6eAd1	komerčně
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
komerčním	komerční	k2eAgInSc7d1	komerční
rybím	rybí	k2eAgInSc7d1	rybí
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
středomořských	středomořský	k2eAgFnPc6d1	středomořská
oblastech	oblast	k1gFnPc6	oblast
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
chován	chovat	k5eAaImNgInS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
producenty	producent	k1gMnPc7	producent
jsou	být	k5eAaImIp3nP	být
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HANEL	HANEL	kA	HANEL
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
;	;	kIx,	;
ANDRESKA	ANDRESKA	kA	ANDRESKA
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
evropských	evropský	k2eAgFnPc2d1	Evropská
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
ilustracích	ilustrace	k1gFnPc6	ilustrace
Květoslava	Květoslava	k1gFnSc1	Květoslava
Híska	Híska	k1gFnSc1	Híska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Artia	Artius	k1gMnSc2	Artius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7442	[number]	k4	7442
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
21	[number]	k4	21
<g/>
,	,	kIx,	,
37	[number]	k4	37
<g/>
,	,	kIx,	,
93	[number]	k4	93
<g/>
,	,	kIx,	,
96	[number]	k4	96
<g/>
,	,	kIx,	,
248	[number]	k4	248
<g/>
,	,	kIx,	,
249	[number]	k4	249
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stickney	Stickney	k1gInPc1	Stickney
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
R.	R.	kA	R.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopedia	Encyclopedium	k1gNnPc1	Encyclopedium
of	of	k?	of
Aquaculture	Aquacultur	k1gInSc5	Aquacultur
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Wiley	Wilea	k1gFnSc2	Wilea
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Canada	Canada	k1gFnSc1	Canada
<g/>
.	.	kIx.	.
1063	[number]	k4	1063
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mořčák	mořčák	k1gMnSc1	mořčák
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
mořčák	mořčák	k1gInSc4	mořčák
evropský	evropský	k2eAgInSc4d1	evropský
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Dicentrarchus	Dicentrarchus	k1gInSc1	Dicentrarchus
labrax	labrax	k1gInSc1	labrax
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Dicentrarchus	Dicentrarchus	k1gInSc1	Dicentrarchus
labrax	labrax	k1gInSc1	labrax
:	:	kIx,	:
fisheries	fisheries	k1gInSc1	fisheries
<g/>
,	,	kIx,	,
aquaculture	aquacultur	k1gMnSc5	aquacultur
<g/>
,	,	kIx,	,
gamefish	gamefish	k1gInSc1	gamefish
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Rainer	Rainra	k1gFnPc2	Rainra
Froese	Froese	k1gFnSc2	Froese
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Pauly	Paula	k1gFnSc2	Paula
<g/>
.	.	kIx.	.
</s>
<s>
FishBase	FishBase	k6eAd1	FishBase
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mořčák	Mořčák	k1gMnSc1	Mořčák
evropský	evropský	k2eAgMnSc1d1	evropský
na	na	k7c4	na
Europa	Europ	k1gMnSc4	Europ
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
</s>
</p>
<p>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
na	na	k7c4	na
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
