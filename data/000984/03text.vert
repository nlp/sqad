<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Silvestr	Silvestr	k1gMnSc1	Silvestr
I.	I.	kA	I.
(	(	kIx(	(
<g/>
také	také	k9	také
Sylvestr	Sylvestr	k1gInSc1	Sylvestr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
33	[number]	k4	33
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
314	[number]	k4	314
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
335	[number]	k4	335
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
pontifikátu	pontifikát	k1gInSc2	pontifikát
si	se	k3xPyFc3	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
upevnila	upevnit	k5eAaPmAgFnS	upevnit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nemusela	muset	k5eNaImAgFnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
osmým	osmý	k4xOgInPc3	osmý
nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
papežem	papež	k1gMnSc7	papež
(	(	kIx(	(
<g/>
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
1	[number]	k4	1
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
papežem	papež	k1gMnSc7	papež
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
roku	rok	k1gInSc2	rok
narození	narození	k1gNnSc2	narození
cca	cca	kA	cca
292	[number]	k4	292
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
pontificalis	pontificalis	k1gFnPc2	pontificalis
byl	být	k5eAaImAgInS	být
narozen	narodit	k5eAaPmNgInS	narodit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Rufus	Rufus	k1gMnSc1	Rufus
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
Vita	vit	k2eAgFnSc1d1	Vita
beati	beať	k1gFnSc2	beať
Sylvestri	Sylvestr	k1gFnSc2	Sylvestr
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Justa	Justa	k1gFnSc1	Justa
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
křest	křest	k1gInSc4	křest
jako	jako	k8xC	jako
mladík	mladík	k1gInSc4	mladík
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Diokleciána	Dioklecián	k1gMnSc2	Dioklecián
se	se	k3xPyFc4	se
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
na	na	k7c4	na
Monte	Mont	k1gInSc5	Mont
Soracte	Soract	k1gInSc5	Soract
nedaleko	nedaleko	k7c2	nedaleko
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolením	zvolení	k1gNnPc3	zvolení
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Silvestr	Silvestr	k1gMnSc1	Silvestr
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
silva	silvo	k1gNnSc2	silvo
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
les	les	k1gInSc1	les
a	a	k8xC	a
které	který	k3yRgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
obyvatele	obyvatel	k1gMnPc4	obyvatel
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
měl	mít	k5eAaImAgMnS	mít
římského	římský	k2eAgMnSc4d1	římský
císaře	císař	k1gMnSc4	císař
Konstantina	Konstantin	k1gMnSc4	Konstantin
Velikého	veliký	k2eAgInSc2d1	veliký
vyléčit	vyléčit	k5eAaPmF	vyléčit
z	z	k7c2	z
lepry	lepra	k1gFnSc2	lepra
a	a	k8xC	a
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
darem	dar	k1gInSc7	dar
tzv.	tzv.	kA	tzv.
Patrimonium	patrimonium	k1gNnSc1	patrimonium
Petri	Petri	k1gNnSc1	Petri
<g/>
,	,	kIx,	,
pozemek	pozemek	k1gInSc1	pozemek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
papeže	papež	k1gMnSc2	papež
Miltiada	Miltiada	k1gFnSc1	Miltiada
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
314	[number]	k4	314
(	(	kIx(	(
<g/>
přesný	přesný	k2eAgInSc4d1	přesný
den	den	k1gInSc4	den
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgInSc1d1	jistý
<g/>
)	)	kIx)	)
zvolen	zvolit	k5eAaPmNgMnS	zvolit
biskupem	biskup	k1gMnSc7	biskup
římským	římský	k2eAgMnSc7d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
Velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
Constantinus	Constantinus	k1gInSc1	Constantinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
křesťanem	křesťan	k1gMnSc7	křesťan
a	a	k8xC	a
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
313	[number]	k4	313
povolil	povolit	k5eAaPmAgInS	povolit
takzvaným	takzvaný	k2eAgInSc7d1	takzvaný
Milánským	milánský	k2eAgInSc7d1	milánský
ediktem	edikt	k1gInSc7	edikt
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
výrazně	výrazně	k6eAd1	výrazně
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
lze	lze	k6eAd1	lze
litovat	litovat	k5eAaImF	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
málo	málo	k4c1	málo
hodnověrných	hodnověrný	k2eAgInPc2d1	hodnověrný
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
čerpány	čerpán	k2eAgInPc1d1	čerpán
z	z	k7c2	z
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
Vita	vit	k2eAgNnPc4d1	Vito
beati	beati	k1gNnPc4	beati
Sylvestri	Sylvestr	k1gFnSc2	Sylvestr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
<g/>
,	,	kIx,	,
syrské	syrský	k2eAgFnSc6d1	Syrská
a	a	k8xC	a
latinské	latinský	k2eAgFnSc6d1	Latinská
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
z	z	k7c2	z
apokryfního	apokryfní	k2eAgInSc2d1	apokryfní
Constitutum	Constitutum	k1gNnSc4	Constitutum
Sylvestri	Sylvestre	k1gFnSc4	Sylvestre
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgMnSc4d1	pocházející
z	z	k7c2	z
let	léto	k1gNnPc2	léto
501	[number]	k4	501
až	až	k6eAd1	až
508	[number]	k4	508
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
připisován	připisovat	k5eAaImNgInS	připisovat
údajnému	údajný	k2eAgInSc3d1	údajný
římskému	římský	k2eAgInSc3d1	římský
koncilu	koncil	k1gInSc3	koncil
o	o	k7c6	o
kterém	který	k3yRgNnSc6	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
jiných	jiný	k2eAgFnPc2d1	jiná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
ze	z	k7c2	z
spisu	spis	k1gInSc2	spis
Donatio	Donatio	k6eAd1	Donatio
Constantini	Constantin	k2eAgMnPc1d1	Constantin
(	(	kIx(	(
<g/>
Konstantinova	Konstantinův	k2eAgFnSc1d1	Konstantinova
donace	donace	k1gFnSc1	donace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
týká	týkat	k5eAaImIp3nS	týkat
okolností	okolnost	k1gFnSc7	okolnost
údajného	údajný	k2eAgInSc2d1	údajný
císařova	císařův	k2eAgInSc2d1	císařův
křtu	křest	k1gInSc2	křest
a	a	k8xC	a
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
přenesl	přenést	k5eAaPmAgInS	přenést
sídlo	sídlo	k1gNnSc4	sídlo
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
předal	předat	k5eAaPmAgMnS	předat
Silvestrovi	Silvestr	k1gMnSc3	Silvestr
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
nástupcům	nástupce	k1gMnPc3	nástupce
světskou	světský	k2eAgFnSc4d1	světská
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
i	i	k8xC	i
celým	celý	k2eAgInSc7d1	celý
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
nároky	nárok	k1gInPc4	nárok
papežů	papež	k1gMnPc2	papež
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spis	spis	k1gInSc1	spis
je	být	k5eAaImIp3nS	být
však	však	k9	však
zcela	zcela	k6eAd1	zcela
určitě	určitě	k6eAd1	určitě
falsifikátem	falsifikát	k1gInSc7	falsifikát
pocházejícím	pocházející	k2eAgInSc7d1	pocházející
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Papeži	Papež	k1gMnSc3	Papež
Silvestrovi	Silvestr	k1gMnSc3	Silvestr
se	se	k3xPyFc4	se
nevyhnuly	vyhnout	k5eNaPmAgInP	vyhnout
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k9	již
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnSc2	desetiletí
ohrožovaly	ohrožovat	k5eAaImAgInP	ohrožovat
jednotu	jednota	k1gFnSc4	jednota
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
donatistů	donatista	k1gMnPc2	donatista
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
v	v	k7c6	v
článku	článek	k1gInSc6	článek
o	o	k7c6	o
papeži	papež	k1gMnSc6	papež
Miltiadovi	Miltiada	k1gMnSc6	Miltiada
šlo	jít	k5eAaImAgNnS	jít
zejména	zejména	k9	zejména
o	o	k7c6	o
ariánství	ariánství	k1gNnSc6	ariánství
<g/>
.	.	kIx.	.
</s>
<s>
Ariánci	Ariánek	k1gMnPc1	Ariánek
byli	být	k5eAaImAgMnP	být
stoupenci	stoupenec	k1gMnPc1	stoupenec
alexandrijského	alexandrijský	k2eAgMnSc2d1	alexandrijský
kněze	kněz	k1gMnSc2	kněz
Areia	Areius	k1gMnSc2	Areius
(	(	kIx(	(
<g/>
260	[number]	k4	260
až	až	k9	až
336	[number]	k4	336
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
318	[number]	k4	318
začal	začít	k5eAaPmAgMnS	začít
popírat	popírat	k5eAaImF	popírat
božství	božství	k1gNnSc4	božství
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
není	být	k5eNaImIp3nS	být
stejné	stejný	k2eAgFnPc4d1	stejná
podstaty	podstata	k1gFnPc4	podstata
jako	jako	k8xS	jako
Bůh	bůh	k1gMnSc1	bůh
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
vznešeným	vznešený	k2eAgMnSc7d1	vznešený
tvorem	tvor	k1gMnSc7	tvor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Bohem	bůh	k1gMnSc7	bůh
přijatým	přijatý	k2eAgMnSc7d1	přijatý
za	za	k7c4	za
jeho	jeho	k3xOp3gMnSc4	jeho
Syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
biskup	biskup	k1gMnSc1	biskup
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
názory	názor	k1gInPc4	názor
svého	svůj	k3xOyFgNnSc2	svůj
podřízeného	podřízený	k2eAgNnSc2d1	podřízené
<g/>
,	,	kIx,	,
popularita	popularita	k1gFnSc1	popularita
Areia	Areius	k1gMnSc2	Areius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
kazatelem	kazatel	k1gMnSc7	kazatel
<g/>
,	,	kIx,	,
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
že	že	k8xS	že
spory	spor	k1gInPc1	spor
povedou	povést	k5eAaPmIp3nP	povést
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
říše	říš	k1gFnSc2	říš
svolal	svolat	k5eAaPmAgInS	svolat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
325	[number]	k4	325
do	do	k7c2	do
Niceji	Nicej	k1gMnPc7	Nicej
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
první	první	k4xOgInSc1	první
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
častý	častý	k2eAgInSc1d1	častý
omyl	omyl	k1gInSc1	omyl
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
sněm	sněm	k1gInSc1	sněm
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
nicejským	nicejský	k2eAgNnSc7d1	nicejské
vyznáním	vyznání	k1gNnSc7	vyznání
víry	víra	k1gFnSc2	víra
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
je	být	k5eAaImIp3nS	být
jedné	jeden	k4xCgFnSc2	jeden
podstaty	podstata	k1gFnSc2	podstata
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
Otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
nicejského	nicejský	k2eAgInSc2d1	nicejský
koncilu	koncil	k1gInSc2	koncil
sám	sám	k3xTgMnSc1	sám
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
pouze	pouze	k6eAd1	pouze
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
presbytery	presbyter	k1gMnPc4	presbyter
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
diákony	diákon	k1gMnPc4	diákon
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tím	ten	k3xDgNnSc7	ten
papež	papež	k1gMnSc1	papež
dával	dávat	k5eAaImAgInS	dávat
najevo	najevo	k6eAd1	najevo
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncil	koncil	k1gInSc4	koncil
svolal	svolat	k5eAaPmAgMnS	svolat
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
možná	možná	k9	možná
tím	ten	k3xDgNnSc7	ten
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
některým	některý	k3yIgFnPc3	některý
císařovým	císařův	k2eAgFnPc3d1	císařova
praktikám	praktika	k1gFnPc3	praktika
(	(	kIx(	(
<g/>
nechal	nechat	k5eAaPmAgMnS	nechat
zavraždit	zavraždit	k5eAaPmF	zavraždit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Crispa	Crisp	k1gMnSc4	Crisp
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
několik	několik	k4yIc4	několik
konkubín	konkubína	k1gFnPc2	konkubína
a	a	k8xC	a
křest	křest	k1gInSc4	křest
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
proto	proto	k6eAd1	proto
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
jeho	jeho	k3xOp3gNnPc2	jeho
umírání	umírání	k1gNnPc2	umírání
odepírán	odepírat	k5eAaImNgMnS	odepírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicejský	Nicejský	k2eAgInSc1d1	Nicejský
koncil	koncil	k1gInSc1	koncil
definoval	definovat	k5eAaBmAgInS	definovat
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
dogmata	dogma	k1gNnPc4	dogma
(	(	kIx(	(
<g/>
závaznou	závazný	k2eAgFnSc4d1	závazná
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
,	,	kIx,	,
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
interpretaci	interpretace	k1gFnSc4	interpretace
bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
-	-	kIx~	-
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
ariánství	ariánství	k1gNnPc2	ariánství
a	a	k8xC	a
nestoriánství	nestoriánství	k1gNnPc2	nestoriánství
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc4	koncil
formuloval	formulovat	k5eAaImAgMnS	formulovat
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
tzv.	tzv.	kA	tzv.
Nicaeum	Nicaeum	k1gNnSc4	Nicaeum
(	(	kIx(	(
<g/>
starokřesťanské	starokřesťanský	k2eAgNnSc4d1	starokřesťanské
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
i	i	k9	i
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
slaveny	slaven	k2eAgFnPc4d1	slavena
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
termín	termín	k1gInSc4	termín
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
první	první	k4xOgFnSc1	první
neděle	neděle	k1gFnSc1	neděle
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
jarním	jarní	k2eAgInSc6d1	jarní
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
330	[number]	k4	330
povýšil	povýšit	k5eAaPmAgMnS	povýšit
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
Byzantion	Byzantion	k1gNnSc4	Byzantion
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
celé	celý	k2eAgFnSc2d1	celá
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
Konstantinopolí	Konstantinopolí	k1gNnSc1	Konstantinopolí
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
předurčil	předurčit	k5eAaPmAgInS	předurčit
úpadek	úpadek	k1gInSc1	úpadek
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
nadále	nadále	k6eAd1	nadále
pouze	pouze	k6eAd1	pouze
centrem	centrum	k1gNnSc7	centrum
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
světská	světský	k2eAgFnSc1d1	světská
moc	moc	k1gFnSc1	moc
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vážné	vážný	k2eAgInPc4d1	vážný
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
konstantinopolský	konstantinopolský	k2eAgMnSc1d1	konstantinopolský
patriarcha	patriarcha	k1gMnSc1	patriarcha
nepatřil	patřit	k5eNaImAgInS	patřit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c4	mezi
přední	přední	k2eAgInPc4d1	přední
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
biskupy	biskup	k1gInPc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Silvestr	Silvestr	k1gMnSc1	Silvestr
pak	pak	k6eAd1	pak
provedl	provést	k5eAaPmAgMnS	provést
úpravu	úprava	k1gFnSc4	úprava
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
řadu	řada	k1gFnSc4	řada
liturgických	liturgický	k2eAgNnPc2d1	liturgické
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
přikázal	přikázat	k5eAaPmAgMnS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
napříště	napříště	k6eAd1	napříště
oltáře	oltář	k1gInSc2	oltář
byly	být	k5eAaImAgInP	být
místo	místo	k6eAd1	místo
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
stavěny	stavit	k5eAaImNgInP	stavit
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Oltáře	Oltář	k1gInPc1	Oltář
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
hedvábím	hedvábí	k1gNnSc7	hedvábí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
plátnem	plátno	k1gNnSc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Stanovil	stanovit	k5eAaPmAgMnS	stanovit
přesné	přesný	k2eAgInPc4d1	přesný
stupně	stupeň	k1gInPc4	stupeň
svěcení	svěcení	k1gNnSc2	svěcení
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgMnS	spojovat
i	i	k8xC	i
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
římské	římský	k2eAgFnSc2d1	římská
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
také	také	k9	také
nechal	nechat	k5eAaPmAgMnS	nechat
sestavit	sestavit	k5eAaPmF	sestavit
první	první	k4xOgFnSc4	první
římskou	římský	k2eAgFnSc4d1	římská
martyrologii	martyrologie	k1gFnSc4	martyrologie
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
tradice	tradice	k1gFnSc2	tradice
tento	tento	k3xDgMnSc1	tento
papež	papež	k1gMnSc1	papež
zavedl	zavést	k5eAaPmAgMnS	zavést
slavení	slavení	k1gNnSc4	slavení
neděle	neděle	k1gFnSc2	neděle
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
dies	dies	k1gInSc1	dies
Dominica	Dominic	k1gInSc2	Dominic
<g/>
"	"	kIx"	"
-	-	kIx~	-
den	den	k1gInSc4	den
Páně	páně	k2eAgInSc4d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pramenech	pramen	k1gInPc6	pramen
je	být	k5eAaImIp3nS	být
mylně	mylně	k6eAd1	mylně
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
při	při	k7c6	při
liturgii	liturgie	k1gFnSc6	liturgie
papežskou	papežský	k2eAgFnSc4d1	Papežská
korunu	koruna	k1gFnSc4	koruna
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tiáru	tiára	k1gFnSc4	tiára
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kuželovitá	kuželovitý	k2eAgFnSc1d1	kuželovitá
čepice	čepice	k1gFnSc1	čepice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
korunu	koruna	k1gFnSc4	koruna
dostal	dostat	k5eAaPmAgMnS	dostat
papež	papež	k1gMnSc1	papež
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
koruny	koruna	k1gFnPc1	koruna
přibyly	přibýt	k5eAaPmAgFnP	přibýt
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Silvestrova	Silvestrův	k2eAgInSc2d1	Silvestrův
pontifikátu	pontifikát	k1gInSc2	pontifikát
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
definitivní	definitivní	k2eAgFnSc3d1	definitivní
proměně	proměna	k1gFnSc3	proměna
pohanského	pohanský	k2eAgInSc2d1	pohanský
Říma	Řím	k1gInSc2	Řím
na	na	k7c4	na
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
císaře	císař	k1gMnSc2	císař
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
postaveny	postavit	k5eAaPmNgFnP	postavit
čtyři	čtyři	k4xCgFnPc1	čtyři
slavné	slavný	k2eAgFnPc1d1	slavná
baziliky	bazilika	k1gFnPc1	bazilika
<g/>
:	:	kIx,	:
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Lateráně	Laterán	k1gInSc6	Laterán
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Pavla	Pavla	k1gFnSc1	Pavla
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
katakombami	katakomby	k1gFnPc7	katakomby
svaté	svatý	k2eAgFnSc2d1	svatá
Priscilly	Priscilla	k1gFnSc2	Priscilla
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgInSc1	sám
pohřben	pohřben	k2eAgInSc1d1	pohřben
<g/>
.	.	kIx.	.
</s>
<s>
Silvestr	Silvestr	k1gMnSc1	Silvestr
zemřel	zemřít	k5eAaPmAgMnS	zemřít
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
335	[number]	k4	335
po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
pontifikátu	pontifikát	k1gInSc6	pontifikát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
21	[number]	k4	21
let	léto	k1gNnPc2	léto
a	a	k8xC	a
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
papež	papež	k1gMnSc1	papež
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
chrámě	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Priscilly	Priscilla	k1gMnSc2	Priscilla
na	na	k7c6	na
Via	via	k7c4	via
Salaria	Salarium	k1gNnPc4	Salarium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
pochováni	pochovat	k5eAaPmNgMnP	pochovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
papežové	papež	k1gMnPc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hrob	hrob	k1gInSc1	hrob
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
archeolog	archeolog	k1gMnSc1	archeolog
De	De	k?	De
Rossi	Rosse	k1gFnSc4	Rosse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
už	už	k6eAd1	už
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
chrámě	chrám	k1gInSc6	chrám
San	San	k1gFnPc2	San
Silvestro	Silvestro	k1gNnSc1	Silvestro
in	in	k?	in
Capite	Capit	k1gMnSc5	Capit
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přenést	přenést	k5eAaPmF	přenést
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
Památku	památka	k1gFnSc4	památka
svatého	svatý	k2eAgMnSc4d1	svatý
Silvestra	Silvestr	k1gMnSc4	Silvestr
uctívá	uctívat	k5eAaImIp3nS	uctívat
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
církev	církev	k1gFnSc1	církev
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
jeho	jeho	k3xOp3gInSc2	jeho
svátku	svátek	k1gInSc2	svátek
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
i	i	k9	i
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
končí	končit	k5eAaImIp3nS	končit
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
a	a	k8xC	a
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
očekáváme	očekávat	k5eAaImIp1nP	očekávat
příchod	příchod	k1gInSc4	příchod
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
pontifikát	pontifikát	k1gInSc4	pontifikát
Silvestra	Silvestr	k1gInSc2	Silvestr
I.	I.	kA	I.
označuje	označovat	k5eAaImIp3nS	označovat
konec	konec	k1gInSc1	konec
éry	éra	k1gFnSc2	éra
pronásledování	pronásledování	k1gNnSc2	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Silvestr	Silvestr	k1gMnSc1	Silvestr
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
uctívaných	uctívaný	k2eAgMnPc2d1	uctívaný
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
kult	kult	k1gInSc1	kult
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
patronem	patron	k1gInSc7	patron
domácího	domácí	k2eAgNnSc2d1	domácí
zvířectva	zvířectvo	k1gNnSc2	zvířectvo
a	a	k8xC	a
dobré	dobrý	k2eAgFnSc2d1	dobrá
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
v	v	k7c6	v
papežském	papežský	k2eAgInSc6d1	papežský
ornátu	ornát	k1gInSc6	ornát
<g/>
,	,	kIx,	,
knihou	kniha	k1gFnSc7	kniha
a	a	k8xC	a
mušlí	mušle	k1gFnSc7	mušle
nebo	nebo	k8xC	nebo
býkem	býk	k1gMnSc7	býk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
zasvěceno	zasvěcen	k2eAgNnSc1d1	zasvěceno
papežské	papežský	k2eAgNnSc1d1	papežské
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Silvestra	Silvestr	k1gMnSc2	Silvestr
<g/>
.	.	kIx.	.
</s>
