<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
En	En	k1gMnSc1	En
attendant	attendant	k1gMnSc1	attendant
Godot	Godot	k1gMnSc1	Godot
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
Godot	Godot	k1gInSc1	Godot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
klasické	klasický	k2eAgNnSc4d1	klasické
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
Samuela	Samuel	k1gMnSc2	Samuel
Becketta	Beckett	k1gMnSc2	Beckett
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaBmAgMnS	napsat
hru	hra	k1gFnSc4	hra
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
uvedena	uveden	k2eAgFnSc1d1	uvedena
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
v	v	k7c6	v
Théâtre	Théâtr	k1gInSc5	Théâtr
de	de	k?	de
Babylone	Babylon	k1gInSc5	Babylon
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
uvedení	uvedení	k1gNnSc4	uvedení
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
jí	jíst	k5eAaImIp3nS	jíst
autor	autor	k1gMnSc1	autor
sám	sám	k3xTgInSc4	sám
přeložil	přeložit	k5eAaPmAgMnS	přeložit
a	a	k8xC	a
opatřil	opatřit	k5eAaPmAgMnS	opatřit
podtitulem	podtitul	k1gInSc7	podtitul
tragikomedie	tragikomedie	k1gFnSc2	tragikomedie
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
dějství	dějství	k1gNnPc2	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
dva	dva	k4xCgMnPc1	dva
tuláci	tulák	k1gMnPc1	tulák
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
Estragon	estragon	k1gInSc4	estragon
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
chaotického	chaotický	k2eAgInSc2d1	chaotický
dialogu	dialog	k1gInSc2	dialog
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jen	jen	k9	jen
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
že	že	k8xS	že
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
jakéhosi	jakýsi	k3yIgMnSc4	jakýsi
Godota	Godot	k1gMnSc4	Godot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
změnit	změnit	k5eAaPmF	změnit
jejich	jejich	k3xOp3gInPc4	jejich
životní	životní	k2eAgInPc4d1	životní
osudy	osud	k1gInPc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
nekonečný	konečný	k2eNgInSc1d1	nekonečný
rozhovor	rozhovor	k1gInSc1	rozhovor
je	být	k5eAaImIp3nS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
příchodem	příchod	k1gInSc7	příchod
pána	pán	k1gMnSc2	pán
Pozza	Pozz	k1gMnSc2	Pozz
s	s	k7c7	s
bičem	bič	k1gInSc7	bič
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
sluhy	sluha	k1gMnSc2	sluha
Luckyho	Luckyho	k?	Luckyho
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
otrok	otrok	k1gMnSc1	otrok
veden	veden	k2eAgMnSc1d1	veden
na	na	k7c6	na
provazu	provaz	k1gInSc6	provaz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
vychloubačnou	vychloubačný	k2eAgFnSc4d1	vychloubačná
<g/>
,	,	kIx,	,
až	až	k9	až
sadistickou	sadistický	k2eAgFnSc4d1	sadistická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
otrok	otrok	k1gMnSc1	otrok
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
bytostí	bytost	k1gFnPc2	bytost
zcela	zcela	k6eAd1	zcela
poníženou	ponížený	k2eAgFnSc7d1	ponížená
<g/>
.	.	kIx.	.
</s>
<s>
Tuláci	tulák	k1gMnPc1	tulák
navazují	navazovat	k5eAaImIp3nP	navazovat
s	s	k7c7	s
Pozzem	Pozz	k1gInSc7	Pozz
absurdní	absurdní	k2eAgInSc1d1	absurdní
rozhovor	rozhovor	k1gInSc1	rozhovor
a	a	k8xC	a
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
odchodu	odchod	k1gInSc6	odchod
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
chlapec	chlapec	k1gMnSc1	chlapec
oznamující	oznamující	k2eAgFnSc2d1	oznamující
<g/>
,	,	kIx,	,
že	že	k8xS	že
pan	pan	k1gMnSc1	pan
Godot	Godot	k1gMnSc1	Godot
dnes	dnes	k6eAd1	dnes
nepřijde	přijít	k5eNaPmIp3nS	přijít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
určitě	určitě	k6eAd1	určitě
dostaví	dostavit	k5eAaPmIp3nP	dostavit
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
<s>
Odchodem	odchod	k1gInSc7	odchod
chlapce	chlapec	k1gMnSc2	chlapec
končí	končit	k5eAaImIp3nS	končit
první	první	k4xOgNnSc1	první
dějství	dějství	k1gNnSc1	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
dějství	dějství	k1gNnSc1	dějství
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
takřka	takřka	k6eAd1	takřka
opakováním	opakování	k1gNnSc7	opakování
dějství	dějství	k1gNnSc2	dějství
prvého	prvý	k4xOgNnSc2	prvý
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
brutální	brutální	k2eAgMnSc1d1	brutální
pán	pán	k1gMnSc1	pán
Pozzo	Pozza	k1gFnSc5	Pozza
přichází	přicházet	k5eAaImIp3nS	přicházet
slepý	slepý	k2eAgMnSc1d1	slepý
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
Luckym	Luckym	k?	Luckym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
němý	němý	k2eAgInSc1d1	němý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgNnSc2	druhý
dějství	dějství	k1gNnSc2	dějství
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevuje	objevovat	k5eAaImIp3nS	objevovat
chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
pan	pan	k1gMnSc1	pan
Godot	Godot	k1gMnSc1	Godot
nedorazí	dorazit	k5eNaPmIp3nS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Estragon	estragon	k1gInSc1	estragon
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
tedy	tedy	k9	tedy
odcházejí	odcházet	k5eAaImIp3nP	odcházet
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijdou	přijít	k5eAaPmIp3nP	přijít
opět	opět	k6eAd1	opět
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
<s>
Chtějí	chtít	k5eAaImIp3nP	chtít
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nehnou	hnout	k5eNaPmIp3nP	hnout
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
významových	významový	k2eAgFnPc2d1	významová
rovin	rovina	k1gFnPc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Chápejme	chápat	k5eAaImRp1nP	chápat
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
tragické	tragický	k2eAgNnSc1d1	tragické
podobenství	podobenství	k1gNnSc1	podobenství
absurdity	absurdita	k1gFnSc2	absurdita
lidského	lidský	k2eAgInSc2d1	lidský
údělu	úděl	k1gInSc2	úděl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
možným	možný	k2eAgInSc7d1	možný
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
očekávání	očekávání	k1gNnSc1	očekávání
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
Godot	Godota	k1gFnPc2	Godota
připomíná	připomínat	k5eAaImIp3nS	připomínat
anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
Boha	bůh	k1gMnSc4	bůh
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
God	God	k1gFnSc1	God
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
úděl	úděl	k1gInSc4	úděl
ulehčit	ulehčit	k5eAaPmF	ulehčit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zabíjet	zabíjet	k5eAaImF	zabíjet
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
vymezen	vymezit	k5eAaPmNgInS	vymezit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zabíjet	zabíjet	k5eAaImF	zabíjet
jej	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
a	a	k8xC	a
nutno	nutno	k6eAd1	nutno
jakkoliv	jakkoliv	k6eAd1	jakkoliv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
protagonisté	protagonista	k1gMnPc1	protagonista
si	se	k3xPyFc3	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
slova	slovo	k1gNnPc4	slovo
jako	jako	k8xC	jako
pingpongové	pingpongový	k2eAgInPc4d1	pingpongový
míčky	míček	k1gInPc4	míček
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
druhému	druhý	k4xOgInSc3	druhý
něco	něco	k3yInSc4	něco
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
zaplnit	zaplnit	k5eAaPmF	zaplnit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
ubíjejícího	ubíjející	k2eAgInSc2d1	ubíjející
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
o	o	k7c6	o
rozbíjení	rozbíjení	k1gNnSc6	rozbíjení
sdělné	sdělný	k2eAgFnSc2d1	sdělná
funkce	funkce	k1gFnSc2	funkce
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hru	hra	k1gFnSc4	hra
o	o	k7c6	o
čekání	čekání	k1gNnSc6	čekání
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
Estragon	estragon	k1gInSc1	estragon
jsou	být	k5eAaImIp3nP	být
tuláci	tulák	k1gMnPc1	tulák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snad	snad	k9	snad
už	už	k6eAd1	už
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
stále	stále	k6eAd1	stále
na	na	k7c6	na
týchž	týž	k3xTgNnPc6	týž
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
přitom	přitom	k6eAd1	přitom
putují	putovat	k5eAaImIp3nP	putovat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
tuláci	tulák	k1gMnPc1	tulák
redukovaní	redukovaný	k2eAgMnPc1d1	redukovaný
na	na	k7c4	na
nepatrné	patrný	k2eNgNnSc4d1	nepatrné
množství	množství	k1gNnSc4	množství
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
tři	tři	k4xCgFnPc4	tři
potřeby	potřeba	k1gFnPc4	potřeba
morální	morální	k2eAgInPc1d1	morální
-	-	kIx~	-
nezůstat	zůstat	k5eNaPmF	zůstat
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
a	a	k8xC	a
zabíjet	zabíjet	k5eAaImF	zabíjet
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pozzo	Pozza	k1gFnSc5	Pozza
a	a	k8xC	a
Lucky	Lucka	k1gFnPc1	Lucka
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přivázáni	přivázán	k2eAgMnPc1d1	přivázán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zcela	zcela	k6eAd1	zcela
materiálně	materiálně	k6eAd1	materiálně
<g/>
,	,	kIx,	,
provazem	provaz	k1gInSc7	provaz
a	a	k8xC	a
smyčkou	smyčka	k1gFnSc7	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
vztahem	vztah	k1gInSc7	vztah
pána	pán	k1gMnSc2	pán
a	a	k8xC	a
otroka	otrok	k1gMnSc2	otrok
vzájemně	vzájemně	k6eAd1	vzájemně
připoutaných	připoutaný	k2eAgFnPc2d1	připoutaná
zištnými	zištný	k2eAgInPc7d1	zištný
zájmy	zájem	k1gInPc7	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
pak	pak	k6eAd1	pak
spolu	spolu	k6eAd1	spolu
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
snad	snad	k9	snad
i	i	k9	i
ze	z	k7c2	z
zvyku	zvyk	k1gInSc2	zvyk
a	a	k8xC	a
společně	společně	k6eAd1	společně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
fyzicky	fyzicky	k6eAd1	fyzicky
zchátrají	zchátrat	k5eAaPmIp3nP	zchátrat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
nabytém	nabytý	k2eAgNnSc6d1	nabyté
zmrzačení	zmrzačení	k1gNnSc6	zmrzačení
navzájem	navzájem	k6eAd1	navzájem
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
dějství	dějství	k1gNnSc6	dějství
Lucky	lucky	k6eAd1	lucky
tancuje	tancovat	k5eAaImIp3nS	tancovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
Pozzo	Pozza	k1gFnSc5	Pozza
píská	pískat	k5eAaImIp3nS	pískat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mu	on	k3xPp3gMnSc3	on
nepíská	pískat	k5eNaImIp3nS	pískat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
Lucky	lucky	k6eAd1	lucky
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pískal	pískat	k5eAaImAgMnS	pískat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
pískal	pískat	k5eAaImAgMnS	pískat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dějství	dějství	k1gNnSc6	dějství
druhém	druhý	k4xOgNnSc6	druhý
sice	sice	k8xC	sice
Pozzo	Pozza	k1gFnSc5	Pozza
stále	stále	k6eAd1	stále
vede	vést	k5eAaImIp3nS	vést
Luckyho	Luckyho	k?	Luckyho
na	na	k7c6	na
provaze	provaz	k1gInSc6	provaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
Lucky	lucky	k6eAd1	lucky
táhne	táhnout	k5eAaImIp3nS	táhnout
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
potud	potud	k6eAd1	potud
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
schopen	schopen	k2eAgMnSc1d1	schopen
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jejich	jejich	k3xOp3gNnSc4	jejich
fyzické	fyzický	k2eAgNnSc4d1	fyzické
zmrzačení	zmrzačení	k1gNnSc4	zmrzačení
je	být	k5eAaImIp3nS	být
tragickou	tragický	k2eAgFnSc7d1	tragická
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
Estragon	estragon	k1gInSc1	estragon
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
scházejí	scházet	k5eAaImIp3nP	scházet
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zase	zase	k9	zase
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
každý	každý	k3xTgInSc4	každý
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zase	zase	k9	zase
vracejí	vracet	k5eAaImIp3nP	vracet
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Zajisté	zajisté	k9	zajisté
mají	mít	k5eAaImIp3nP	mít
někdy	někdy	k6eAd1	někdy
jeden	jeden	k4xCgInSc4	jeden
druhého	druhý	k4xOgMnSc4	druhý
dost	dost	k6eAd1	dost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
bez	bez	k7c2	bez
sebe	se	k3xPyFc2	se
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
Estragon	estragon	k1gInSc1	estragon
jsou	být	k5eAaImIp3nP	být
tuláci	tulák	k1gMnPc1	tulák
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
klauni	klaun	k1gMnPc1	klaun
<g/>
,	,	kIx,	,
parodují	parodovat	k5eAaImIp3nP	parodovat
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
Pozza	Pozza	k1gFnSc1	Pozza
s	s	k7c7	s
Luckym	Luckym	k?	Luckym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jejich	jejich	k3xOp3gInSc2	jejich
dialogu	dialog	k1gInSc2	dialog
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
skutečně	skutečně	k6eAd1	skutečně
sdělnou	sdělný	k2eAgFnSc4d1	sdělná
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vyřčena	vyřčen	k2eAgNnPc1d1	vyřčeno
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaplnila	zaplnit	k5eAaPmAgFnS	zaplnit
mlčení	mlčení	k1gNnSc4	mlčení
<g/>
.	.	kIx.	.
</s>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c6	na
Godota	Godot	k1gMnSc2	Godot
se	se	k3xPyFc4	se
k	k	k7c3	k
divákovi	divák	k1gMnSc3	divák
neobrací	obracet	k5eNaImIp3nS	obracet
s	s	k7c7	s
jednoznačnou	jednoznačný	k2eAgFnSc7d1	jednoznačná
a	a	k8xC	a
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
odpovědí	odpověď	k1gFnSc7	odpověď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nP	by
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
autor	autor	k1gMnSc1	autor
předem	předem	k6eAd1	předem
zašifroval	zašifrovat	k5eAaPmAgMnS	zašifrovat
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
naléhá	naléhat	k5eAaImIp3nS	naléhat
sérií	série	k1gFnSc7	série
krutých	krutý	k2eAgFnPc2d1	krutá
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
divák	divák	k1gMnSc1	divák
může	moct	k5eAaImIp3nS	moct
pokoušet	pokoušet	k5eAaImF	pokoušet
hledat	hledat	k5eAaImF	hledat
odpověď	odpověď	k1gFnSc4	odpověď
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Krutost	krutost	k1gFnSc1	krutost
dramatu	drama	k1gNnSc2	drama
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrhá	vrhat	k5eAaImIp3nS	vrhat
divákovi	divák	k1gMnSc3	divák
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
otázky	otázka	k1gFnSc2	otázka
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
Godot	Godot	k1gInSc1	Godot
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Play	play	k0	play
of	of	k?	of
the	the	k?	the
Week	Week	k1gInSc1	Week
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alan	Alan	k1gMnSc1	Alan
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
.	.	kIx.	.
</s>
<s>
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
Godot	Godot	k1gInSc1	Godot
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irský	irský	k2eAgInSc1d1	irský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Michael	Michael	k1gMnSc1	Michael
Lindsay-Hogg	Lindsay-Hogg	k1gMnSc1	Lindsay-Hogg
<g/>
.	.	kIx.	.
</s>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
<g/>
,	,	kIx,	,
Dilia	Dilius	k1gMnSc4	Dilius
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Větrné	větrný	k2eAgInPc1d1	větrný
mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2005	[number]	k4	2005
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc4	divadlo
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
