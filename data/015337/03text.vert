<s>
Kochankowie	Kochankowie	k1gFnSc1
z	z	k7c2
Marony	Marona	k1gFnSc2
</s>
<s>
Kochankowie	Kochankowie	k1gFnSc1
z	z	k7c2
Marony	Marona	k1gFnSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Kochankowie	Kochankowie	k1gFnSc1
z	z	k7c2
Marony	Marona	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
polština	polština	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
107	#num#	k4
minut	minuta	k1gFnPc2
Žánr	žánr	k1gInSc4
</s>
<s>
drama	drama	k1gNnSc1
Námět	námět	k1gInSc1
</s>
<s>
Jarosław	Jarosław	k?
Iwaszkiewicz	Iwaszkiewicz	k1gInSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Izabella	Izabella	k1gMnSc1
CywińskaCezary	CywińskaCezara	k1gFnSc2
Harasimowicz	Harasimowicz	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Izabella	Izabella	k1gFnSc1
Cywińska	Cywińsk	k1gInSc2
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
GruszkaKrzysztof	GruszkaKrzysztof	k1gMnSc1
ZawadzkiŁukasz	ZawadzkiŁukasz	k1gMnSc1
Simlat	Simle	k1gNnPc2
Hudba	hudba	k1gFnSc1
</s>
<s>
Jerzy	Jerza	k1gFnPc1
Satanowski	Satanowsk	k1gFnSc2
Kamera	kamera	k1gFnSc1
</s>
<s>
Marcin	Marcin	k2eAgMnSc1d1
Koszałka	Koszałka	k1gMnSc1
Kostýmy	kostým	k1gInPc4
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Biedrzycka	Biedrzycka	k1gFnSc1
Střih	střih	k1gInSc4
</s>
<s>
Anna	Anna	k1gFnSc1
Wagner	Wagner	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2006	#num#	k4
Kochankowie	Kochankowie	k1gFnSc1
z	z	k7c2
Marony	Marona	k1gFnSc2
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
Kinoboxu	Kinobox	k1gInSc2
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kochankowie	Kochankowie	k1gFnSc1
z	z	k7c2
Marony	Marona	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
Milenci	milenec	k1gMnPc1
z	z	k7c2
Marony	Marona	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polský	polský	k2eAgInSc1d1
hraný	hraný	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
režírovala	režírovat	k5eAaImAgFnS
Izabella	Izabella	k1gFnSc1
Cywińska	Cywińska	k1gFnSc1
podle	podle	k7c2
vlastního	vlastní	k2eAgInSc2d1
scénáře	scénář	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
povídky	povídka	k1gFnSc2
Jarosława	Jarosławus	k1gMnSc2
Iwaszkiewicza	Iwaszkiewicz	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
natočen	natočit	k5eAaBmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
vztah	vztah	k1gInSc1
tří	tři	k4xCgFnPc2
osob	osoba	k1gFnPc2
na	na	k7c6
polském	polský	k2eAgInSc6d1
venkově	venkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ola	Ola	k1gFnSc1
je	být	k5eAaImIp3nS
mladá	mladý	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
učitelka	učitelka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
bydlí	bydlet	k5eAaImIp3nS
v	v	k7c6
malé	malý	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
Marona	Maron	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoho	jeden	k4xCgInSc2
podzimního	podzimní	k2eAgInSc2d1
dne	den	k1gInSc2
zde	zde	k6eAd1
potká	potkat	k5eAaPmIp3nS
Janka	Janka	k1gFnSc1
a	a	k8xC
Areka	areka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janek	Janek	k1gMnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
umístěn	umístit	k5eAaPmNgInS
do	do	k7c2
sanatoria	sanatorium	k1gNnSc2
na	na	k7c4
léčení	léčení	k1gNnSc4
tuberkulózy	tuberkulóza	k1gFnSc2
a	a	k8xC
Arek	areka	k1gFnPc2
za	za	k7c7
ním	on	k3xPp3gInSc7
dojíždí	dojíždět	k5eAaImIp3nP
na	na	k7c6
motorce	motorka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janek	Janek	k1gMnSc1
ovšem	ovšem	k9
ze	z	k7c2
sanatoria	sanatorium	k1gNnSc2
neustále	neustále	k6eAd1
utíká	utíkat	k5eAaImIp3nS
<g/>
,	,	kIx,
navzdory	navzdory	k6eAd1
varování	varování	k1gNnSc4
Areka	areka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jinak	jinak	k6eAd1
bude	být	k5eAaImBp3nS
z	z	k7c2
léčebny	léčebna	k1gFnSc2
vyloučen	vyloučit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc4
nicméně	nicméně	k8xC
často	často	k6eAd1
navštěvují	navštěvovat	k5eAaImIp3nP
Olu	Ola	k1gFnSc4
ve	v	k7c6
vsi	ves	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
ní	on	k3xPp3gFnSc7
a	a	k8xC
Jankem	Janek	k1gMnSc7
se	se	k3xPyFc4
vytvoří	vytvořit	k5eAaPmIp3nS
vztah	vztah	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arek	areka	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
přitahován	přitahován	k2eAgInSc1d1
k	k	k7c3
Jankovi	Janek	k1gMnSc3
<g/>
,	,	kIx,
na	na	k7c4
ni	on	k3xPp3gFnSc4
žárlí	žárlit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janek	Janek	k1gMnSc1
je	být	k5eAaImIp3nS
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgInPc3
útěkům	útěk	k1gInPc3
skutečně	skutečně	k6eAd1
ze	z	k7c2
sanatoria	sanatorium	k1gNnSc2
vyloučen	vyloučen	k2eAgMnSc1d1
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
Olou	Ola	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
propuštěna	propustit	k5eAaPmNgFnS
ze	z	k7c2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
bydlí	bydlet	k5eAaImIp3nS
u	u	k7c2
sousedky	sousedka	k1gFnSc2
Gulbińské	Gulbińská	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Jankovi	Jankův	k2eAgMnPc1d1
přitíží	přitížit	k5eAaPmIp3nP
<g/>
,	,	kIx,
přijíždí	přijíždět	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
se	s	k7c7
čtyřletým	čtyřletý	k2eAgMnSc7d1
synem	syn	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Gruszka	Gruszka	k1gFnSc1
</s>
<s>
Ola	Ola	k1gFnSc1
</s>
<s>
Krzysztof	Krzysztof	k1gInSc1
Zawadzki	Zawadzk	k1gFnSc2
</s>
<s>
Janek	Janek	k1gMnSc1
</s>
<s>
Łukasz	Łukasz	k1gInSc1
Simlat	Simle	k1gNnPc2
</s>
<s>
Arek	areka	k1gFnPc2
</s>
<s>
Ewa	Ewa	k?
Kasprzyk	Kasprzyk	k1gInSc1
</s>
<s>
Eufrozyna	Eufrozyna	k6eAd1
</s>
<s>
Danuta	Danut	k2eAgNnPc1d1
Stenka	Stenko	k1gNnPc1
</s>
<s>
Hornowa	Hornowa	k6eAd1
</s>
<s>
Jadwiga	Jadwiga	k1gFnSc1
Jankowska-Cieślak	Jankowska-Cieślak	k1gInSc1
</s>
<s>
Gulbińska	Gulbińska	k1gFnSc1
</s>
<s>
Tomasz	Tomasz	k1gMnSc1
Sapryk	Sapryk	k1gMnSc1
</s>
<s>
rybář	rybář	k1gMnSc1
</s>
<s>
Małgorzata	Małgorzat	k2eAgFnSc1d1
Zawadzka	Zawadzka	k1gFnSc1
</s>
<s>
Jankova	Jankův	k2eAgFnSc1d1
žena	žena	k1gFnSc1
Barbara	Barbara	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Festival	festival	k1gInSc1
polských	polský	k2eAgMnPc2d1
filmů	film	k1gInPc2
ve	v	k7c6
Gdyni	Gdyně	k1gFnSc6
<g/>
:	:	kIx,
cena	cena	k1gFnSc1
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
(	(	kIx(
<g/>
Karolina	Karolinum	k1gNnSc2
Gruszka	Gruszka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
polská	polský	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Orły	Orła	k1gFnSc2
<g/>
:	:	kIx,
nominace	nominace	k1gFnSc2
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
nejlepší	dobrý	k2eAgFnSc2d3
scénografie	scénografie	k1gFnSc2
(	(	kIx(
<g/>
Jacek	Jacek	k1gInSc1
Osadowski	Osadowsk	k1gFnSc2
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgInPc1d3
kostýmy	kostým	k1gInPc1
(	(	kIx(
<g/>
Magdalena	Magdalena	k1gFnSc1
Biedrzycka	Biedrzycka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgInSc1d3
zvuk	zvuk	k1gInSc1
(	(	kIx(
<g/>
Nikodem	Nikod	k1gInSc7
Wołk-Łaniewski	Wołk-Łaniewski	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgInSc1d3
střih	střih	k1gInSc1
(	(	kIx(
<g/>
Anna	Anna	k1gFnSc1
Wagner	Wagner	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
(	(	kIx(
<g/>
Karolina	Karolinum	k1gNnSc2
Gruszka	Gruszka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
na	na	k7c4
cenu	cena	k1gFnSc4
Zbigniewa	Zbigniewum	k1gNnSc2
Cybulského	Cybulský	k2eAgNnSc2d1
(	(	kIx(
<g/>
Karolina	Karolinum	k1gNnSc2
Gruszka	Gruszka	k1gFnSc1
a	a	k8xC
Łukasz	Łukasz	k1gInSc1
Simlat	Simle	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
