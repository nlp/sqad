<s desamb="1">
V	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
hantecu	hantecus	k1gInSc6
se	se	k3xPyFc4
tramvaji	tramvaj	k1gFnSc3
říká	říkat	k5eAaImIp3nS
šalina	šalina	k1gFnSc1
(	(	kIx(
<g/>
zkomolením	zkomolení	k1gNnSc7
německého	německý	k2eAgNnSc2d1
slovního	slovní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
elektrische	elektrische	k1gFnSc1
Linie	linie	k1gFnSc1
[	[	kIx(
<g/>
elektryše	elektryše	k1gFnSc1
línye	línye	k1gFnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
ze	z	k7c2
slova	slovo	k1gNnSc2
schallen	schallen	k1gInSc1
=	=	kIx~
znít	znít	k5eAaImF
<g/>
,	,	kIx,
ozývat	ozývat	k5eAaImF
se	se	k3xPyFc4
<g/>
,	,	kIx,
rozléhat	rozléhat	k5eAaImF
se	se	k3xPyFc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
častým	častý	k2eAgNnSc7d1
užíváním	užívání	k1gNnSc7
výstražného	výstražný	k2eAgInSc2d1
zvonku	zvonek	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
nebo	nebo	k8xC
šmirgl	šmirgl	k?
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
se	se	k3xPyFc4
běžně	běžně	k6eAd1
užívá	užívat	k5eAaImIp3nS
výraz	výraz	k1gInSc4
tramvajka	tramvajka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>