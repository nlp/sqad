<s>
Tramvaj	tramvaj	k1gFnSc1
</s>
<s>
Vozy	vůz	k1gInPc1
Tatra	Tatra	k1gFnSc1
T3	T3	k1gFnPc2
jsou	být	k5eAaImIp3nP
nejpočetnějším	početní	k2eAgInSc7d3
typem	typ	k1gInSc7
tramvaje	tramvaj	k1gFnSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souprava	souprava	k1gFnSc1
dvou	dva	k4xCgInPc2
motorových	motorový	k2eAgMnPc2d1
vozů	vůz	k1gInPc2
varianty	varianta	k1gFnSc2
Tatra	Tatra	k1gFnSc1
T3SUCS	T3SUCS	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1
Konstal	Konstal	k1gFnSc1
105	#num#	k4
<g/>
N	N	kA
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
v	v	k7c6
polském	polský	k2eAgInSc6d1
Štětíně	Štětín	k1gInSc6
</s>
<s>
Speciálně	speciálně	k6eAd1
upravený	upravený	k2eAgInSc1d1
pražský	pražský	k2eAgInSc1d1
pracovní	pracovní	k2eAgInSc1d1
vůz	vůz	k1gInSc1
–	–	k?
mazací	mazací	k2eAgFnSc4d1
tramvaj	tramvaj	k1gFnSc4
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
CAF	CAF	kA
Urbos	Urbos	k1gInSc4
3	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1
je	být	k5eAaImIp3nS
kolejové	kolejový	k2eAgNnSc4d1
vozidlo	vozidlo	k1gNnSc4
nebo	nebo	k8xC
vlak	vlak	k1gInSc4
tramvajové	tramvajový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
určené	určený	k2eAgInPc1d1
pro	pro	k7c4
provoz	provoz	k1gInSc4
v	v	k7c6
městských	městský	k2eAgFnPc6d1
ulicích	ulice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
tento	tento	k3xDgInSc1
název	název	k1gInSc1
označoval	označovat	k5eAaImAgInS
samotnou	samotný	k2eAgFnSc4d1
kolejovou	kolejový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
metonymickým	metonymický	k2eAgInSc7d1
přenosem	přenos	k1gInSc7
změnil	změnit	k5eAaPmAgInS
význam	význam	k1gInSc4
na	na	k7c4
označení	označení	k1gNnSc4
kolejového	kolejový	k2eAgNnSc2d1
vozidla	vozidlo	k1gNnSc2
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
tramway	tramwaa	k1gFnSc2
(	(	kIx(
<g/>
tram	tram	k1gFnPc7
označovalo	označovat	k5eAaImAgNnS
trám	trám	k1gInSc4
<g/>
,	,	kIx,
kolej	kolej	k1gFnSc4
nebo	nebo	k8xC
důlní	důlní	k2eAgInSc4d1
vozík	vozík	k1gInSc4
<g/>
,	,	kIx,
way	way	k?
cestu	cesta	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
užívalo	užívat	k5eAaImAgNnS
u	u	k7c2
důlních	důlní	k2eAgFnPc2d1
a	a	k8xC
průmyslových	průmyslový	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
tram	tram	k1gFnPc2
pro	pro	k7c4
kolej	kolej	k1gFnSc4
pravděpodobně	pravděpodobně	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
saských	saský	k2eAgInPc6d1
dolech	dol	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
podlahy	podlaha	k1gFnPc4
štol	štola	k1gFnPc2
a	a	k8xC
chodeb	chodba	k1gFnPc2
kladly	klást	k5eAaImAgInP
trámy	trám	k1gInPc1
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
přepravy	přeprava	k1gFnSc2
důlních	důlní	k2eAgInPc2d1
vozíků	vozík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
trámů	trám	k1gInPc2
byla	být	k5eAaImAgNnP
posléze	posléze	k6eAd1
vyhloubena	vyhlouben	k2eAgNnPc1d1
drážka	drážka	k1gNnPc1
a	a	k8xC
vozíky	vozík	k1gInPc1
byly	být	k5eAaImAgInP
opatřeny	opatřit	k5eAaPmNgInP
kolíkem	kolík	k1gInSc7
klouzajícím	klouzající	k2eAgInSc7d1
v	v	k7c6
drážce	drážka	k1gFnSc6
pro	pro	k7c4
bezpečné	bezpečný	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
po	po	k7c6
trati	trať	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vznikl	vzniknout	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
předchůdců	předchůdce	k1gMnPc2
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
se	se	k3xPyFc4
ustálil	ustálit	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
koněspřežného	koněspřežný	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
přenesl	přenést	k5eAaPmAgInS
se	se	k3xPyFc4
však	však	k9
i	i	k9
na	na	k7c4
parní	parní	k2eAgFnSc4d1
či	či	k8xC
jinou	jiný	k2eAgFnSc4d1
trakci	trakce	k1gFnSc4
a	a	k8xC
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
s	s	k7c7
elektrickou	elektrický	k2eAgFnSc7d1
trakcí	trakce	k1gFnSc7
s	s	k7c7
vrchním	vrchní	k2eAgNnSc7d1
trolejovým	trolejový	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
tramvaj	tramvaj	k1gFnSc1
(	(	kIx(
<g/>
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
vozidlo	vozidlo	k1gNnSc1
<g/>
)	)	kIx)
nazývala	nazývat	k5eAaImAgNnP
pouliční	pouliční	k2eAgFnSc1d1
nebo	nebo	k8xC
elektrická	elektrický	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
též	též	k9
elektrika	elektrika	k1gFnSc1
(	(	kIx(
<g/>
srov.	srov.	kA
električka	električka	k1gFnSc1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
spisovné	spisovný	k2eAgFnSc6d1
slovenštině	slovenština	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
hantecu	hantecus	k1gInSc6
se	se	k3xPyFc4
tramvaji	tramvaj	k1gFnSc3
říká	říkat	k5eAaImIp3nS
šalina	šalina	k1gFnSc1
(	(	kIx(
<g/>
zkomolením	zkomolení	k1gNnSc7
německého	německý	k2eAgNnSc2d1
slovního	slovní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
elektrische	elektrische	k1gFnSc1
Linie	linie	k1gFnSc1
[	[	kIx(
<g/>
elektryše	elektryše	k1gFnSc1
línye	línye	k1gFnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
ze	z	k7c2
slova	slovo	k1gNnSc2
schallen	schallen	k1gInSc1
=	=	kIx~
znít	znít	k5eAaImF
<g/>
,	,	kIx,
ozývat	ozývat	k5eAaImF
se	se	k3xPyFc4
<g/>
,	,	kIx,
rozléhat	rozléhat	k5eAaImF
se	se	k3xPyFc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
častým	častý	k2eAgNnSc7d1
užíváním	užívání	k1gNnSc7
výstražného	výstražný	k2eAgInSc2d1
zvonku	zvonek	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
nebo	nebo	k8xC
šmirgl	šmirgl	k?
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
se	se	k3xPyFc4
běžně	běžně	k6eAd1
užívá	užívat	k5eAaImIp3nS
výraz	výraz	k1gInSc4
tramvajka	tramvajka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výhody	výhoda	k1gFnPc1
a	a	k8xC
nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
Koněspřežná	koněspřežný	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
Gdaňsku	Gdaňsk	k1gInSc6
</s>
<s>
Parní	parní	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
užívaná	užívaný	k2eAgFnSc1d1
na	na	k7c6
lince	linka	k1gFnSc6
z	z	k7c2
Paříže	Paříž	k1gFnSc2
do	do	k7c2
Saint-Germain	Saint-Germaina	k1gFnPc2
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
z	z	k7c2
továrny	továrna	k1gFnSc2
Františka	František	k1gMnSc2
Ringhoffera	Ringhoffer	k1gMnSc2
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
tyčový	tyčový	k2eAgInSc1d1
sběrač	sběrač	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
vzhledem	vzhledem	k7c3
ke	k	k7c3
změnám	změna	k1gFnPc3
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
trolejové	trolejový	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
v	v	k7c6
dobách	doba	k1gFnPc6
rozšiřování	rozšiřování	k1gNnSc2
tramvají	tramvaj	k1gFnPc2
typu	typ	k1gInSc2
T	T	kA
<g/>
,	,	kIx,
nahrazen	nahrazen	k2eAgInSc1d1
pantografovým	pantografový	k2eAgInSc7d1
</s>
<s>
Výhody	výhoda	k1gFnPc1
</s>
<s>
větší	veliký	k2eAgFnSc1d2
přepravní	přepravní	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
oproti	oproti	k7c3
autobusu	autobus	k1gInSc3
(	(	kIx(
<g/>
i	i	k8xC
metrobusu	metrobus	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
trolejbusu	trolejbus	k1gInSc2
</s>
<s>
tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vedena	vést	k5eAaImNgFnS
jak	jak	k8xC,k8xS
po	po	k7c6
běžné	běžný	k2eAgFnSc6d1
uliční	uliční	k2eAgFnSc6d1
vozovce	vozovka	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
na	na	k7c6
samostatném	samostatný	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
současně	současně	k6eAd1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
i	i	k9
jako	jako	k8xC,k8xS
meziměstský	meziměstský	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
vnější	vnější	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
u	u	k7c2
městských	městský	k2eAgFnPc2d1
konurbací	konurbace	k1gFnPc2
(	(	kIx(
<g/>
Liberec	Liberec	k1gInSc1
–	–	k?
Jablonec	Jablonec	k1gInSc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
–	–	k?
Litvínov	Litvínov	k1gInSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
–	–	k?
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
městská	městský	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
relativně	relativně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
průchodnost	průchodnost	k1gFnSc4
hustou	hustý	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
<g/>
,	,	kIx,
byť	byť	k8xS
nižší	nízký	k2eAgFnSc1d2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
autobusy	autobus	k1gInPc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
tramvaj	tramvaj	k1gFnSc1
provozována	provozován	k2eAgFnSc1d1
na	na	k7c6
samostatném	samostatný	k2eAgInSc6d1
nebo	nebo	k8xC
odděleném	oddělený	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
závislá	závislý	k2eAgFnSc1d1
na	na	k7c4
okolní	okolní	k2eAgFnSc4d1
dopravní	dopravní	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
(	(	kIx(
<g/>
nezdržují	zdržovat	k5eNaImIp3nP
ji	on	k3xPp3gFnSc4
auta	auto	k1gNnPc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
tramvaj	tramvaj	k1gFnSc1
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
rychlodrážní	rychlodrážní	k2eAgInSc4d1
provoz	provoz	k1gInSc4
(	(	kIx(
<g/>
rychlá	rychlý	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
podzemní	podzemní	k2eAgInSc1d1
provoz	provoz	k1gInSc1
(	(	kIx(
<g/>
podpovrchová	podpovrchový	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
současný	současný	k2eAgInSc1d1
provoz	provoz	k1gInSc1
městské	městský	k2eAgFnSc2d1
<g/>
,	,	kIx,
rychlé	rychlý	k2eAgFnSc2d1
a	a	k8xC
podpovrchové	podpovrchový	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
jedné	jeden	k4xCgFnSc2
sítě	síť	k1gFnSc2
</s>
<s>
tramvaj	tramvaj	k1gFnSc1
lze	lze	k6eAd1
nejsnáze	snadno	k6eAd3
preferovat	preferovat	k5eAaImF
v	v	k7c6
rámci	rámec	k1gInSc6
podpory	podpora	k1gFnSc2
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
signálních	signální	k2eAgInPc6d1
plánech	plán	k1gInPc6
světelné	světelný	k2eAgFnSc2d1
signalizace	signalizace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
nižší	nízký	k2eAgFnSc1d2
vnitřní	vnitřní	k2eAgFnSc1d1
hlučnost	hlučnost	k1gFnSc1
přepravy	přeprava	k1gFnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
s	s	k7c7
nezávislým	závislý	k2eNgInSc7d1
pohonem	pohon	k1gInSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
lze	lze	k6eAd1
snáze	snadno	k6eAd2
vyřešit	vyřešit	k5eAaPmF
požadavek	požadavek	k1gInSc4
na	na	k7c4
nízkou	nízký	k2eAgFnSc4d1
podlahu	podlaha	k1gFnSc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
tramvaj	tramvaj	k1gFnSc1
je	být	k5eAaImIp3nS
přátelštější	přátelský	k2eAgFnSc1d2
k	k	k7c3
tělesně	tělesně	k6eAd1
postiženým	postižený	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kolejová	kolejový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
má	mít	k5eAaImIp3nS
menší	malý	k2eAgInSc4d2
valivý	valivý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
oproti	oproti	k7c3
pohonu	pohon	k1gInSc3
spalovacím	spalovací	k2eAgInSc7d1
motorem	motor	k1gInSc7
nižší	nízký	k2eAgNnSc4d2
znečištění	znečištění	k1gNnSc4
emisemi	emise	k1gFnPc7
spalování	spalování	k1gNnSc1
v	v	k7c6
místě	místo	k1gNnSc6
provozu	provoz	k1gInSc2
</s>
<s>
větší	veliký	k2eAgFnSc1d2
energetická	energetický	k2eAgFnSc1d1
účinnost	účinnost	k1gFnSc1
(	(	kIx(
<g/>
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
energie	energie	k1gFnSc1
vyrobí	vyrobit	k5eAaPmIp3nS
spálením	spálení	k1gNnSc7
fosilních	fosilní	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
,	,	kIx,
energetická	energetický	k2eAgFnSc1d1
účinnost	účinnost	k1gFnSc1
systému	systém	k1gInSc2
palivo-elektrárna-dálkové	palivo-elektrárna-dálek	k1gMnPc1
vedení-měnírna-trolejové	vedení-měnírna-trolejový	k2eAgInPc1d1
vedení-elektromotor	vedení-elektromotor	k1gInSc1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
energetická	energetický	k2eAgFnSc1d1
účinnost	účinnost	k1gFnSc1
systému	systém	k1gInSc2
palivo-spalovací	palivo-spalovací	k2eAgInSc1d1
motor	motor	k1gInSc1
autobusu	autobus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pokud	pokud	k8xS
vozové	vozový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
stojí	stát	k5eAaImIp3nP
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
zastávkách	zastávka	k1gFnPc6
a	a	k8xC
na	na	k7c6
křižovatkách	křižovatka	k1gFnPc6
<g/>
,	,	kIx,
nevznikají	vznikat	k5eNaImIp3nP
prakticky	prakticky	k6eAd1
žádné	žádný	k3yNgFnPc1
energetické	energetický	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
úspora	úspora	k1gFnSc1
energie	energie	k1gFnSc2
při	při	k7c6
brzdění	brzdění	k1gNnSc6
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
část	část	k1gFnSc1
pohybové	pohybový	k2eAgFnSc2d1
energie	energie	k1gFnSc2
mění	měnit	k5eAaImIp3nS
v	v	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
a	a	k8xC
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
napájecí	napájecí	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
rekuperace	rekuperace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
elektromagnetické	elektromagnetický	k2eAgFnPc1d1
brzdy	brzda	k1gFnPc1
šetří	šetřit	k5eAaImIp3nP
mechanické	mechanický	k2eAgFnPc1d1
brzdy	brzda	k1gFnPc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
snižují	snižovat	k5eAaImIp3nP
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
účinnější	účinný	k2eAgMnPc1d2
ve	v	k7c6
sněhu	sníh	k1gInSc6
a	a	k8xC
náledí	náledí	k1gNnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vyšší	vysoký	k2eAgFnSc4d2
spolehlivost	spolehlivost	k1gFnSc4
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
za	za	k7c2
sněhu	sníh	k1gInSc2
a	a	k8xC
náledí	náledí	k1gNnSc1
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
méně	málo	k6eAd2
spolehlivým	spolehlivý	k2eAgInSc7d1
autobusem	autobus	k1gInSc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
jednodušší	jednoduchý	k2eAgNnSc1d2
řízení	řízení	k1gNnSc1
(	(	kIx(
<g/>
řidič	řidič	k1gMnSc1
netočí	točit	k5eNaImIp3nS
volantem	volant	k1gInSc7
<g/>
,	,	kIx,
neparkuje	parkovat	k5eNaImIp3nS
<g/>
,	,	kIx,
nepředjíždí	předjíždět	k5eNaImIp3nS
atd.	atd.	kA
<g/>
)	)	kIx)
klade	klást	k5eAaImIp3nS
menší	malý	k2eAgInPc4d2
nároky	nárok	k1gInPc4
na	na	k7c4
řidiče	řidič	k1gMnPc4
<g/>
,	,	kIx,
díky	díky	k7c3
odděleným	oddělený	k2eAgFnPc3d1
kabinám	kabina	k1gFnPc3
pro	pro	k7c4
řidiče	řidič	k1gMnPc4
jsou	být	k5eAaImIp3nP
méně	málo	k6eAd2
obtěžováni	obtěžovat	k5eAaImNgMnP
cestujícími	cestující	k1gMnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
klimatizace	klimatizace	k1gFnSc1
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
bezpečnější	bezpečný	k2eAgInSc1d2
prostředek	prostředek	k1gInSc1
dopravy	doprava	k1gFnSc2
v	v	k7c6
prostředí	prostředí	k1gNnSc6
pěší	pěší	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
autobusovou	autobusový	k2eAgFnSc7d1
dopravou	doprava	k1gFnSc7
díky	díky	k7c3
ochranným	ochranný	k2eAgInPc3d1
prostředkům	prostředek	k1gInPc3
<g/>
,	,	kIx,
znemožňujícím	znemožňující	k2eAgNnSc7d1
pád	pád	k1gInSc4
osob	osoba	k1gFnPc2
pod	pod	k7c4
vozidlo	vozidlo	k1gNnSc4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vyšší	vysoký	k2eAgFnSc1d2
bezpečnost	bezpečnost	k1gFnSc1
cestujících	cestující	k1gMnPc2
uvnitř	uvnitř	k7c2
tramvaje	tramvaj	k1gFnSc2
díky	díky	k7c3
její	její	k3xOp3gFnSc3
masivní	masivní	k2eAgFnSc3d1
konstrukci	konstrukce	k1gFnSc3
i	i	k8xC
směrovému	směrový	k2eAgNnSc3d1
vedení	vedení	k1gNnSc3
na	na	k7c6
kolejnicích	kolejnice	k1gFnPc6
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
ale	ale	k8xC
naopak	naopak	k6eAd1
může	moct	k5eAaImIp3nS
ohrozit	ohrozit	k5eAaPmF
bezpečnost	bezpečnost	k1gFnSc4
ostatních	ostatní	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
provozu	provoz	k1gInSc2
na	na	k7c6
pozemních	pozemní	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
řidič	řidič	k1gMnSc1
se	se	k3xPyFc4
nemůže	moct	k5eNaImIp3nS
vyhnout	vyhnout	k5eAaPmF
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nižší	nízký	k2eAgInPc1d2
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
v	v	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
cestujícího	cestující	k1gMnSc4
(	(	kIx(
<g/>
bez	bez	k7c2
započtení	započtení	k1gNnSc2
investičních	investiční	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
možnost	možnost	k1gFnSc1
kombinace	kombinace	k1gFnSc1
s	s	k7c7
provozem	provoz	k1gInSc7
po	po	k7c6
železnici	železnice	k1gFnSc6
(	(	kIx(
<g/>
tram-train	tram-train	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
větší	veliký	k2eAgNnSc4d2
pohodlí	pohodlí	k1gNnSc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
autobusem	autobus	k1gInSc7
i	i	k8xC
trolejbusem	trolejbus	k1gInSc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
prokázaný	prokázaný	k2eAgInSc1d1
pozitivní	pozitivní	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
počet	počet	k1gInSc4
cestujících	cestující	k2eAgMnPc2d1
–	–	k?
kdykoli	kdykoli	k6eAd1
se	se	k3xPyFc4
tramvaje	tramvaj	k1gFnPc1
zrušily	zrušit	k5eAaPmAgFnP
<g/>
,	,	kIx,
cestujících	cestující	k1gMnPc2
ubylo	ubýt	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
obnovily	obnovit	k5eAaPmAgFnP
<g/>
,	,	kIx,
pasažérů	pasažér	k1gMnPc2
přibylo	přibýt	k5eAaPmAgNnS
(	(	kIx(
<g/>
například	například	k6eAd1
po	po	k7c6
jejich	jejich	k3xOp3gNnSc6
zrušení	zrušení	k1gNnSc6
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
začala	začít	k5eAaPmAgFnS
jezdit	jezdit	k5eAaImF
auty	auto	k1gNnPc7
<g/>
,	,	kIx,
po	po	k7c6
jejich	jejich	k3xOp3gNnSc6
obnovení	obnovení	k1gNnSc6
se	se	k3xPyFc4
do	do	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
počet	počet	k1gInSc1
cestujících	cestující	k1gMnPc2
v	v	k7c6
MHD	MHD	kA
zvedl	zvednout	k5eAaPmAgMnS
o	o	k7c4
43	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
lepší	dobrý	k2eAgFnSc1d2
viditelnost	viditelnost	k1gFnSc1
<g/>
:	:	kIx,
díky	díky	k7c3
kolejím	kolej	k1gFnPc3
a	a	k8xC
troleji	trolej	k1gFnSc6
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
snadněji	snadno	k6eAd2
všimnou	všimnout	k5eAaPmIp3nP
tramvaje	tramvaj	k1gFnSc2
než	než	k8xS
autobusu	autobus	k1gInSc2
</s>
<s>
nižší	nízký	k2eAgInPc1d2
náklady	náklad	k1gInPc1
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
vozidel	vozidlo	k1gNnPc2
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
vlídnost	vlídnost	k1gFnSc1
k	k	k7c3
chodcům	chodec	k1gMnPc3
<g/>
:	:	kIx,
na	na	k7c6
pěších	pěší	k2eAgFnPc6d1
zónách	zóna	k1gFnPc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
díky	díky	k7c3
kolejím	kolej	k1gFnPc3
předpovědět	předpovědět	k5eAaPmF
trasa	trasa	k1gFnSc1
vozidla	vozidlo	k1gNnSc2
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
autobusu	autobus	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
rozšiřování	rozšiřování	k1gNnSc1
zeleně	zeleň	k1gFnSc2
ve	v	k7c6
městech	město	k1gNnPc6
–	–	k?
pokud	pokud	k8xS
trať	trať	k1gFnSc1
vede	vést	k5eAaImIp3nS
mimo	mimo	k7c4
vozovku	vozovka	k1gFnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
ji	on	k3xPp3gFnSc4
zatravnit	zatravnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeleň	zeleň	k1gFnSc1
pak	pak	k6eAd1
působí	působit	k5eAaImIp3nS
přátelsky	přátelsky	k6eAd1
a	a	k8xC
esteticky	esteticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
špatná	špatný	k2eAgFnSc1d1
manévrovatelnost	manévrovatelnost	k1gFnSc1
<g/>
,	,	kIx,
omezená	omezený	k2eAgFnSc1d1
kolejemi	kolej	k1gFnPc7
a	a	k8xC
delší	dlouhý	k2eAgFnSc7d2
brzdnou	brzdný	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramvaj	tramvaj	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
objet	objet	k5eAaPmF
vadný	vadný	k2eAgInSc4d1
vlak	vlak	k1gInSc4
nebo	nebo	k8xC
jinou	jiný	k2eAgFnSc4d1
překážku	překážka	k1gFnSc4
<g/>
,	,	kIx,
omezená	omezený	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
odklonů	odklon	k1gInPc2
a	a	k8xC
objížděk	objížďka	k1gFnPc2
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgNnSc4d2
riziko	riziko	k1gNnSc4
střetů	střet	k1gInPc2
s	s	k7c7
vozidly	vozidlo	k1gNnPc7
<g/>
,	,	kIx,
chodci	chodec	k1gMnPc7
a	a	k8xC
překážkami	překážka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
větší	veliký	k2eAgFnSc1d2
hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
setrvačnost	setrvačnost	k1gFnSc1
působí	působit	k5eAaImIp3nS
delší	dlouhý	k2eAgFnPc4d2
brzdné	brzdný	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
(	(	kIx(
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
při	při	k7c6
použití	použití	k1gNnSc6
magnetické	magnetický	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
velmi	velmi	k6eAd1
malá	malý	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c6
tramvajových	tramvajový	k2eAgFnPc6d1
křižovatkách	křižovatka	k1gFnPc6
a	a	k8xC
v	v	k7c6
ostrých	ostrý	k2eAgFnPc6d1
zatáčkách	zatáčka	k1gFnPc6
<g/>
,	,	kIx,
autobusy	autobus	k1gInPc1
<g/>
,	,	kIx,
trolejbusy	trolejbus	k1gInPc1
a	a	k8xC
auta	auto	k1gNnPc1
je	on	k3xPp3gNnSc4
dokážou	dokázat	k5eAaPmIp3nP
projet	projet	k5eAaPmF
rychleji	rychle	k6eAd2
</s>
<s>
riziko	riziko	k1gNnSc1
smyku	smyk	k1gInSc2
a	a	k8xC
skluzu	skluz	k1gInSc2
dané	daný	k2eAgFnSc2d1
kontaktem	kontakt	k1gInSc7
kov	kov	k1gInSc1
na	na	k7c4
kov	kov	k1gInSc4
mezi	mezi	k7c7
kolem	kolo	k1gNnSc7
a	a	k8xC
kolejnicí	kolejnice	k1gFnSc7
</s>
<s>
větší	veliký	k2eAgFnSc1d2
hlučnost	hlučnost	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
obloucích	oblouk	k1gInPc6
</s>
<s>
větší	veliký	k2eAgNnSc4d2
ohrožování	ohrožování	k1gNnSc4
staveb	stavba	k1gFnPc2
vibracemi	vibrace	k1gFnPc7
</s>
<s>
citlivost	citlivost	k1gFnSc1
na	na	k7c4
nedostatečnou	dostatečný	k2eNgFnSc4d1
údržbu	údržba	k1gFnSc4
a	a	k8xC
provozní	provozní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
na	na	k7c6
špatném	špatný	k2eAgInSc6d1
asfaltovém	asfaltový	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
lze	lze	k6eAd1
jezdit	jezdit	k5eAaImF
vyšší	vysoký	k2eAgFnSc7d2
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
neudržované	udržovaný	k2eNgFnPc4d1
koleje	kolej	k1gFnPc4
nedovolí	dovolit	k5eNaPmIp3nS
jet	jet	k5eAaImF
plnou	plný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
některé	některý	k3yIgFnPc1
tramvajové	tramvajový	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
jsou	být	k5eAaImIp3nP
ve	v	k7c6
špatném	špatný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
jen	jen	k9
asi	asi	k9
20	#num#	k4
až	až	k9
30	#num#	k4
km	km	kA
za	za	k7c4
hodinu	hodina	k1gFnSc4
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
při	při	k7c6
kvalitní	kvalitní	k2eAgFnSc6d1
údržbě	údržba	k1gFnSc6
se	se	k3xPyFc4
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
50	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
nahoru	nahoru	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
argument	argument	k1gInSc1
pro	pro	k7c4
likvidaci	likvidace	k1gFnSc4
tramvajových	tramvajový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
tedy	tedy	k9
<g/>
:	:	kIx,
nedostatek	nedostatek	k1gInSc1
financí	finance	k1gFnPc2
na	na	k7c4
provoz	provoz	k1gInSc4
<g/>
→	→	k?
<g/>
špatná	špatný	k2eAgFnSc1d1
údržba	údržba	k1gFnSc1
<g/>
→	→	k?
<g/>
nižší	nízký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
a	a	k8xC
zastaralá	zastaralý	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
<g/>
→	→	k?
<g/>
úbytek	úbytek	k1gInSc4
cestujících	cestující	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
přesun	přesun	k1gInSc4
do	do	k7c2
aut	auto	k1gNnPc2
<g/>
→	→	k?
<g/>
likvidace	likvidace	k1gFnSc2
tramvajové	tramvajový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
závislá	závislý	k2eAgFnSc1d1
trakce	trakce	k1gFnSc1
–	–	k?
zastavení	zastavení	k1gNnSc1
provozu	provoz	k1gInSc2
při	při	k7c6
výpadku	výpadek	k1gInSc6
napájení	napájení	k1gNnSc2
</s>
<s>
zadrátování	zadrátování	k1gNnSc1
veřejného	veřejný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
zohyzdění	zohyzdění	k1gNnSc4
výhledu	výhled	k1gInSc2
na	na	k7c4
stavby	stavba	k1gFnPc4
a	a	k8xC
městská	městský	k2eAgNnPc1d1
panoramata	panorama	k1gNnPc1
<g/>
,	,	kIx,
ztížení	ztížení	k1gNnSc1
průjezdu	průjezd	k1gInSc2
vysokých	vysoký	k2eAgNnPc2d1
silničních	silniční	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
riziko	riziko	k1gNnSc1
poškození	poškození	k1gNnSc2
trolejového	trolejový	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
</s>
<s>
nerentabilnost	nerentabilnost	k1gFnSc1
tramvaje	tramvaj	k1gFnSc2
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
nízkých	nízký	k2eAgInPc2d1
přepravních	přepravní	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nízká	nízký	k2eAgFnSc1d1
provozní	provozní	k2eAgFnSc1d1
a	a	k8xC
prostorová	prostorový	k2eAgFnSc1d1
flexibilita	flexibilita	k1gFnSc1
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
závislost	závislost	k1gFnSc4
na	na	k7c6
tramvajovém	tramvajový	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
tramvaj	tramvaj	k1gFnSc1
s	s	k7c7
jednosměrnými	jednosměrný	k2eAgInPc7d1
vozy	vůz	k1gInPc7
vyžaduje	vyžadovat	k5eAaImIp3nS
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
smyčková	smyčkový	k2eAgNnPc4d1
obratiště	obratiště	k1gNnPc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
obousměrnost	obousměrnost	k1gFnSc1
vozů	vůz	k1gInPc2
výrazně	výrazně	k6eAd1
komplikuje	komplikovat	k5eAaBmIp3nS
využití	využití	k1gNnSc4
vnitřního	vnitřní	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
tramvaje	tramvaj	k1gFnSc2
<g/>
,	,	kIx,
snižuje	snižovat	k5eAaImIp3nS
se	se	k3xPyFc4
počet	počet	k1gInSc1
míst	místo	k1gNnPc2
pro	pro	k7c4
sezení	sezení	k1gNnSc4
</s>
<s>
nízká	nízký	k2eAgFnSc1d1
stoupavost	stoupavost	k1gFnSc1
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
do	do	k7c2
7	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
česká	český	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
tramvajového	tramvajový	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
uprostřed	uprostřed	k7c2
vozovky	vozovka	k1gFnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
cestující	cestující	k1gMnSc1
na	na	k7c4
zastávku	zastávka	k1gFnSc4
přecházeli	přecházet	k5eAaImAgMnP
(	(	kIx(
<g/>
tramvaj	tramvaj	k1gFnSc1
nezastavuje	zastavovat	k5eNaImIp3nS
na	na	k7c6
hraně	hrana	k1gFnSc6
chodníku	chodník	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
vysoké	vysoký	k2eAgInPc1d1
nárazové	nárazový	k2eAgInPc1d1
náklady	náklad	k1gInPc1
na	na	k7c4
pořízení	pořízení	k1gNnSc4
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
(	(	kIx(
<g/>
nezbytnost	nezbytnost	k1gFnSc1
výstavby	výstavba	k1gFnSc2
kolejí	kolej	k1gFnPc2
a	a	k8xC
trolejové	trolejový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
měníren	měnírna	k1gFnPc2
<g/>
,	,	kIx,
dep	depo	k1gNnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
způsobují	způsobovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
rozvíjí	rozvíjet	k5eAaImIp3nS
spíše	spíše	k9
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
již	již	k6eAd1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
než	než	k8xS
na	na	k7c6
nových	nový	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
neumožňuje	umožňovat	k5eNaImIp3nS
nebo	nebo	k8xC
aspoň	aspoň	k9
komplikuje	komplikovat	k5eAaBmIp3nS
současné	současný	k2eAgNnSc4d1
působení	působení	k1gNnSc4
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
dopravců	dopravce	k1gMnPc2
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Karl	Karl	k1gMnSc1
Bulla	bulla	k1gFnSc1
<g/>
:	:	kIx,
Tramvaj	tramvaj	k1gFnSc1
na	na	k7c6
zamrzlé	zamrzlý	k2eAgFnSc6d1
řece	řeka	k1gFnSc6
Něvě	Něva	k1gFnSc3
</s>
<s>
Vzdálenějšími	vzdálený	k2eAgMnPc7d2
předchůdci	předchůdce	k1gMnPc7
tramvají	tramvaj	k1gFnPc2
byly	být	k5eAaImAgInP
kolejové	kolejový	k2eAgInPc1d1
vozíky	vozík	k1gInPc1
v	v	k7c6
dolech	dol	k1gInPc6
a	a	k8xC
továrnách	továrna	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
dřevěnými	dřevěný	k2eAgFnPc7d1
nebo	nebo	k8xC
litinovými	litinový	k2eAgFnPc7d1
kolejemi	kolej	k1gFnPc7
s	s	k7c7
vodicím	vodicí	k2eAgInSc7d1
žlábkem	žlábek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
i	i	k8xC
rychlost	rychlost	k1gFnSc1
vozíků	vozík	k1gInPc2
omezovala	omezovat	k5eAaImAgFnS
rychlost	rychlost	k1gFnSc1
člověka	člověk	k1gMnSc2
nebo	nebo	k8xC
tažných	tažný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
velikost	velikost	k1gFnSc1
omezovaly	omezovat	k5eAaImAgFnP
i	i	k9
nedostatečné	dostatečný	k2eNgFnPc1d1
brzdy	brzda	k1gFnPc1
a	a	k8xC
slabé	slabý	k2eAgFnPc1d1
<g/>
,	,	kIx,
křehké	křehký	k2eAgFnPc1d1
koleje	kolej	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramvaji	tramvaj	k1gFnSc6
jako	jako	k8xC,k8xS
prostředku	prostředek	k1gInSc2
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
předcházely	předcházet	k5eAaImAgInP
městské	městský	k2eAgInPc1d1
dostavníky	dostavník	k1gInPc1
(	(	kIx(
<g/>
omnibusy	omnibus	k1gInPc1
<g/>
)	)	kIx)
s	s	k7c7
koňským	koňský	k2eAgInSc7d1
potahem	potah	k1gInSc7
a	a	k8xC
pravidelným	pravidelný	k2eAgInSc7d1
jízdním	jízdní	k2eAgInSc7d1
řádem	řád	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
linka	linka	k1gFnSc1
koněspřežné	koněspřežný	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
osob	osoba	k1gFnPc2
na	na	k7c6
kolejích	kolej	k1gFnPc6
vznikla	vzniknout	k5eAaPmAgFnS
z	z	k7c2
linky	linka	k1gFnSc2
na	na	k7c4
dopravu	doprava	k1gFnSc4
kamene	kámen	k1gInSc2
u	u	k7c2
Swansea	Swanse	k1gInSc2
(	(	kIx(
<g/>
Wales	Wales	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zahájila	zahájit	k5eAaPmAgFnS
provoz	provoz	k1gInSc4
roku	rok	k1gInSc2
1807	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
linky	linka	k1gFnPc1
vznikaly	vznikat	k5eAaImAgFnP
ojediněle	ojediněle	k6eAd1
a	a	k8xC
byly	být	k5eAaImAgFnP
považovány	považován	k2eAgFnPc1d1
spíše	spíše	k9
za	za	k7c4
atrakci	atrakce	k1gFnSc4
a	a	k8xC
technický	technický	k2eAgInSc4d1
výstřelek	výstřelek	k1gInSc4
až	až	k6eAd1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vniklo	vniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
železničních	železniční	k2eAgFnPc2d1
linek	linka	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
trati	trať	k1gFnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
Linec	Linec	k1gInSc1
a	a	k8xC
Norimberk	Norimberk	k1gInSc1
–	–	k?
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
skutečné	skutečný	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
s	s	k7c7
rozšiřováním	rozšiřování	k1gNnSc7
měst	město	k1gNnPc2
až	až	k8xS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
USA	USA	kA
(	(	kIx(
<g/>
linka	linka	k1gFnSc1
New	New	k1gMnSc1
York	York	k1gInSc1
–	–	k?
Haarlem	Haarl	k1gInSc7
1835	#num#	k4
<g/>
,	,	kIx,
Baltimore	Baltimore	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Orleans	Orleans	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
začínaly	začínat	k5eAaImAgFnP
s	s	k7c7
koněspřežným	koněspřežný	k2eAgInSc7d1
provozem	provoz	k1gInSc7
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
dálkových	dálkový	k2eAgFnPc6d1
(	(	kIx(
<g/>
železničních	železniční	k2eAgFnPc6d1
<g/>
)	)	kIx)
dráhách	dráha	k1gFnPc6
již	již	k6eAd1
byli	být	k5eAaImAgMnP
koně	kůň	k1gMnPc1
víceméně	víceméně	k9
vytlačeni	vytlačit	k5eAaPmNgMnP
parní	parní	k2eAgFnSc7d1
trakcí	trakce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
prvních	první	k4xOgInPc2
tramvajových	tramvajový	k2eAgMnPc2d1
vozů	vůz	k1gInPc2
vycházela	vycházet	k5eAaImAgFnS
z	z	k7c2
tehdejších	tehdejší	k2eAgInPc2d1
železničních	železniční	k2eAgInPc2d1
vagónů	vagón	k1gInPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
ve	v	k7c6
zmenšené	zmenšený	k2eAgFnSc6d1
a	a	k8xC
odlehčené	odlehčený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožňovala	umožňovat	k5eAaImAgFnS
tažení	tažení	k1gNnSc4
koňmi	kůň	k1gMnPc7
a	a	k8xC
projíždění	projíždění	k1gNnSc1
ostrými	ostrý	k2eAgInPc7d1
oblouky	oblouk	k1gInPc7
v	v	k7c6
městských	městský	k2eAgFnPc6d1
ulicích	ulice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koněspřežné	koněspřežný	k2eAgFnPc1d1
tramvajové	tramvajový	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
byly	být	k5eAaImAgFnP
poměrně	poměrně	k6eAd1
brzy	brzy	k6eAd1
nahrazeny	nahradit	k5eAaPmNgFnP
tramvajemi	tramvaj	k1gFnPc7
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
ale	ale	k8xC
dosti	dosti	k6eAd1
dlouho	dlouho	k6eAd1
hledal	hledat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
parních	parní	k2eAgFnPc2d1
tramvají	tramvaj	k1gFnPc2
se	se	k3xPyFc4
stavěly	stavět	k5eAaImAgFnP
tramvaje	tramvaj	k1gFnPc1
poháněné	poháněný	k2eAgFnPc1d1
stlačeným	stlačený	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
<g/>
,	,	kIx,
spalovacím	spalovací	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
,	,	kIx,
tažené	tažený	k2eAgNnSc1d1
podzemním	podzemní	k2eAgNnSc7d1
lanem	lano	k1gNnSc7
a	a	k8xC
konečně	konečně	k6eAd1
s	s	k7c7
elektrickým	elektrický	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrická	elektrický	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
má	mít	k5eAaImIp3nS
řadu	řada	k1gFnSc4
nesporných	sporný	k2eNgFnPc2d1
výhod	výhoda	k1gFnPc2
<g/>
,	,	kIx,
technický	technický	k2eAgInSc1d1
problém	problém	k1gInSc1
však	však	k9
představoval	představovat	k5eAaImAgInS
odběr	odběr	k1gInSc1
elektrické	elektrický	k2eAgFnSc2d1
energii	energie	k1gFnSc3
z	z	k7c2
nadzemního	nadzemní	k2eAgNnSc2d1
trolejového	trolejový	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
o	o	k7c6
vysokém	vysoký	k2eAgNnSc6d1
napětí	napětí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
koncem	konec	k1gInSc7
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
hledaly	hledat	k5eAaImAgFnP
i	i	k9
jiné	jiný	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
přívodu	přívod	k1gInSc2
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
akumulátorový	akumulátorový	k2eAgInSc4d1
<g/>
,	,	kIx,
boční	boční	k2eAgInSc4d1
<g/>
,	,	kIx,
spodní	spodní	k2eAgInSc4d1
přívod	přívod	k1gInSc4
z	z	k7c2
provozní	provozní	k2eAgFnSc2d1
kolejnice	kolejnice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusy	pokus	k1gInPc1
zejména	zejména	k9
se	s	k7c7
spodním	spodní	k2eAgInSc7d1
přívodem	přívod	k1gInSc7
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
odstranil	odstranit	k5eAaPmAgInS
nevzhledná	vzhledný	k2eNgNnPc4d1
trolejová	trolejový	k2eAgNnPc4d1
vedení	vedení	k1gNnPc4
se	se	k3xPyFc4
stále	stále	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
rozmach	rozmach	k1gInSc1
elektrické	elektrický	k2eAgFnSc2d1
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
nastal	nastat	k5eAaPmAgInS
od	od	k7c2
poslední	poslední	k2eAgFnSc2d1
třetiny	třetina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
technickým	technický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
větších	veliký	k2eAgInPc2d2
<g/>
,	,	kIx,
úspornějších	úsporný	k2eAgInPc2d2
a	a	k8xC
spolehlivějších	spolehlivý	k2eAgInPc2d2
autobusů	autobus	k1gInPc2
se	se	k3xPyFc4
téměř	téměř	k6eAd1
zastavil	zastavit	k5eAaPmAgInS
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američtí	americký	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
na	na	k7c4
to	ten	k3xDgNnSc4
reagovali	reagovat	k5eAaBmAgMnP
velkým	velký	k2eAgInSc7d1
projektem	projekt	k1gInSc7
technického	technický	k2eAgNnSc2d1
sjednocování	sjednocování	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
umožnil	umožnit	k5eAaPmAgInS
hromadnou	hromadný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
a	a	k8xC
snížení	snížení	k1gNnSc4
cen	cena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
PCC	PCC	kA
sice	sice	k8xC
zásadně	zásadně	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
konstrukci	konstrukce	k1gFnSc4
tramvají	tramvaj	k1gFnPc2
až	až	k9
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
rozkvět	rozkvět	k1gInSc1
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
nastal	nastat	k5eAaPmAgInS
až	až	k9
ke	k	k7c3
konci	konec	k1gInSc3
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinami	příčina	k1gFnPc7
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
technického	technický	k2eAgInSc2d1
pokroku	pokrok	k1gInSc2
tramvají	tramvaj	k1gFnPc2
čistší	čistý	k2eAgInSc4d2
provoz	provoz	k1gInSc4
bez	bez	k7c2
znečišťování	znečišťování	k1gNnSc2
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
menší	malý	k2eAgInSc4d2
hluk	hluk	k1gInSc4
i	i	k8xC
odpor	odpor	k1gInSc4
proti	proti	k7c3
zadrátovaným	zadrátovaný	k2eAgNnPc3d1
centrům	centrum	k1gNnPc3
hlavně	hlavně	k9
historických	historický	k2eAgNnPc2d1
evropských	evropský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tramvaje	tramvaj	k1gFnPc1
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
hlavně	hlavně	k9
na	na	k7c4
periferie	periferie	k1gFnPc4
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
méně	málo	k6eAd2
do	do	k7c2
jejich	jejich	k3xOp3gNnPc2
center	centrum	k1gNnPc2
<g/>
..	..	k?
</s>
<s>
Kloubové	kloubový	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
</s>
<s>
Kloubové	kloubový	k2eAgFnPc1d1
či	či	k8xC
článkové	článkový	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
jsou	být	k5eAaImIp3nP
dvou	dva	k4xCgMnPc6
nebo	nebo	k8xC
vícedílné	vícedílný	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
kapacita	kapacita	k1gFnSc1
než	než	k8xS
u	u	k7c2
klasických	klasický	k2eAgInPc2d1
jednodílných	jednodílný	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
s	s	k7c7
vlečným	vlečný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
díly	díl	k1gInPc1
kloubové	kloubový	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
přechodovým	přechodový	k2eAgInSc7d1
spojem	spoj	k1gInSc7
–	–	k?
kloubem	kloub	k1gInSc7
a	a	k8xC
přechodovým	přechodový	k2eAgInSc7d1
měchem	měch	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
vnější	vnější	k2eAgInSc1d1
plášť	plášť	k1gInSc1
přechodového	přechodový	k2eAgInSc2d1
spoje	spoj	k1gInSc2
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
díly	díl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
tramvajové	tramvajový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
vícečlánkové	vícečlánkový	k2eAgInPc1d1
<g/>
,	,	kIx,
salón	salón	k1gInSc4
pro	pro	k7c4
cestující	cestující	k1gMnPc4
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
společný	společný	k2eAgInSc1d1
a	a	k8xC
průchozí	průchozí	k2eAgInSc1d1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
docíleno	docílen	k2eAgNnSc1d1
vyšší	vysoký	k2eAgFnPc4d2
přepravní	přepravní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
soupravy	souprava	k1gFnSc2
i	i	k8xC
úspory	úspora	k1gFnSc2
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
nemožnost	nemožnost	k1gFnSc1
část	část	k1gFnSc1
soupravy	souprava	k1gFnSc2
v	v	k7c6
provozu	provoz	k1gInSc6
odpojit	odpojit	k5eAaPmF
a	a	k8xC
tím	ten	k3xDgNnSc7
flexibilně	flexibilně	k6eAd1
reagovat	reagovat	k5eAaBmF
na	na	k7c4
aktuální	aktuální	k2eAgFnSc4d1
nižší	nízký	k2eAgFnSc4d2
potřebu	potřeba	k1gFnSc4
přepravní	přepravní	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyráběla	vyrábět	k5eAaImAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
kloubové	kloubový	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
ČKD	ČKD	kA
Tatra	Tatra	k1gFnSc1
<g/>
,	,	kIx,
vůbec	vůbec	k9
prvním	první	k4xOgInSc7
typem	typ	k1gInSc7
byla	být	k5eAaImAgFnS
Tatra	Tatra	k1gFnSc1
K1	K1	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgInPc3d1
typům	typ	k1gInPc3
z	z	k7c2
ČKD	ČKD	kA
patří	patřit	k5eAaImIp3nS
Tatra	Tatra	k1gFnSc1
K	k	k7c3
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
K	k	k7c3
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
KT	KT	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
KT	KT	kA
<g/>
8	#num#	k4
<g/>
D	D	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
RT	RT	kA
<g/>
6	#num#	k4
<g/>
N	N	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
RT	RT	kA
<g/>
8	#num#	k4
<g/>
D	D	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
RT6S	RT6S	k1gFnSc1
a	a	k8xC
Tatra	Tatra	k1gFnSc1
KT	KT	kA
<g/>
8	#num#	k4
<g/>
D	D	kA
<g/>
5	#num#	k4
<g/>
N.	N.	kA
Od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyrábí	vyrábět	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
kloubové	kloubový	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
Škoda	Škoda	k1gMnSc1
Transportation	Transportation	k1gInSc1
<g/>
,	,	kIx,
Inekon	Inekon	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
Pars	Pars	k1gInSc1
nova	novum	k1gNnSc2
a	a	k8xC
Aliance	aliance	k1gFnSc2
TW	TW	kA
Team	team	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Rozchod	rozchod	k1gInSc1
kolejí	kolej	k1gFnPc2
</s>
<s>
V	v	k7c6
mnoha	mnoho	k4c6
městech	město	k1gNnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
menších	malý	k2eAgFnPc6d2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
období	období	k1gNnSc6
rozkvětu	rozkvět	k1gInSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
zvolen	zvolen	k2eAgInSc1d1
menší	malý	k2eAgInSc1d2
rozchod	rozchod	k1gInSc1
kolejí	kolej	k1gFnPc2
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
1000	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
ale	ale	k8xC
nároky	nárok	k1gInPc1
na	na	k7c4
přepravní	přepravní	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
rostly	růst	k5eAaImAgFnP
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	s	k7c7
standardem	standard	k1gInSc7
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
1435	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
na	na	k7c6
železnici	železnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
tramvajové	tramvajový	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
s	s	k7c7
menším	malý	k2eAgInSc7d2
rozchodem	rozchod	k1gInSc7
v	v	k7c6
bohatších	bohatý	k2eAgFnPc6d2
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přestavěny	přestavět	k5eAaPmNgFnP
na	na	k7c4
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Tramvajová	tramvajový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
českých	český	k2eAgFnPc2d1
tramvají	tramvaj	k1gFnPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
tramvají	tramvaj	k1gFnPc2
vyrobených	vyrobený	k2eAgFnPc2d1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Ringhofferovy	Ringhofferův	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
První	první	k4xOgFnPc4
české	český	k2eAgFnPc4d1
tramvaje	tramvaj	k1gFnPc4
vyrobila	vyrobit	k5eAaPmAgFnS
továrna	továrna	k1gFnSc1
Františka	František	k1gMnSc2
Ringhoffera	Ringhoffer	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
pro	pro	k7c4
pražskou	pražský	k2eAgFnSc4d1
koňku	koňka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
otevřené	otevřený	k2eAgInPc4d1
vlečné	vlečný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vyráběly	vyrábět	k5eAaImAgInP
Ringhofferovy	Ringhofferův	k2eAgInPc1d1
závody	závod	k1gInPc1
i	i	k8xC
elektrické	elektrický	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
–	–	k?
pražský	pražský	k2eAgInSc1d1
vozový	vozový	k2eAgInSc1d1
park	park	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
tvořen	tvořit	k5eAaImNgInS
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
vozy	vůz	k1gInPc1
od	od	k7c2
Ringhoffera	Ringhoffero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
znárodněna	znárodnit	k5eAaPmNgFnS
a	a	k8xC
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c6
Vagónku	vagónek	k1gInSc6
Tatra	Tatra	k1gFnSc1
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
podniku	podnik	k1gInSc2
ČKD	ČKD	kA
<g/>
.	.	kIx.
</s>
<s>
Královopolská	královopolský	k2eAgFnSc1d1
strojírna	strojírna	k1gFnSc1
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
ze	z	k7c2
série	série	k1gFnSc2
74	#num#	k4
<g/>
–	–	k?
<g/>
103	#num#	k4
<g/>
,	,	kIx,
114	#num#	k4
<g/>
–	–	k?
<g/>
149	#num#	k4
<g/>
,	,	kIx,
151	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
,	,	kIx,
400	#num#	k4
<g/>
–	–	k?
<g/>
405	#num#	k4
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1
souprava	souprava	k1gFnSc1
tramvaje	tramvaj	k1gFnSc2
4MT	4MT	k4
s	s	k7c7
vlečným	vlečný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
z	z	k7c2
Královopolské	královopolský	k2eAgFnSc2d1
strojírny	strojírna	k1gFnSc2
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
výroby	výroba	k1gFnSc2
</s>
<s>
Počet	počet	k1gInSc1
vyrobených	vyrobený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
</s>
<s>
Dodávky	dodávka	k1gFnPc1
</s>
<s>
série	série	k1gFnSc1
1	#num#	k4
<g/>
II	II	kA
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
II	II	kA
</s>
<s>
1903	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
6	#num#	k4
<g/>
II	II	kA
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
II	II	kA
</s>
<s>
1904	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
18	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
</s>
<s>
1904	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
série	série	k1gFnSc1
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
kropicí	kropicí	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
28	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
série	série	k1gFnSc1
74	#num#	k4
<g/>
–	–	k?
<g/>
103	#num#	k4
<g/>
,	,	kIx,
114	#num#	k4
<g/>
–	–	k?
<g/>
149,151	149,151	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
,	,	kIx,
400	#num#	k4
<g/>
–	–	k?
<g/>
405	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
251	#num#	k4
<g/>
–	–	k?
<g/>
260	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1928	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
50	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1931	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Most	most	k1gInSc1
</s>
<s>
série	série	k1gFnSc1
46	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
série	série	k1gFnSc1
261	#num#	k4
<g/>
–	–	k?
<g/>
296	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
série	série	k1gFnSc1
81	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
,	,	kIx,
186	#num#	k4
<g/>
–	–	k?
<g/>
220	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1942	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
série	série	k1gFnSc1
36	#num#	k4
<g/>
–	–	k?
<g/>
65	#num#	k4
</s>
<s>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
série	série	k1gFnSc1
40	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1950	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
série	série	k1gFnSc1
297	#num#	k4
<g/>
–	–	k?
<g/>
326	#num#	k4
(	(	kIx(
<g/>
vlečné	vlečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1950	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
MT	MT	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
∑	∑	k?
</s>
<s>
1903	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
</s>
<s>
274	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
ČKD	ČKD	kA
Tatra	Tatra	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tramvaje	tramvaj	k1gFnSc2
Tatra	Tatra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Škoda	škoda	k1gFnSc1
Transportation	Transportation	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tramvaje	tramvaj	k1gFnSc2
Škoda	škoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Inekon	Inekon	k1gNnSc1
Trams	Tramsa	k1gFnPc2
</s>
<s>
Ostravský	ostravský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
Inekon	Inekon	k1gNnSc1
01	#num#	k4
Trio	trio	k1gNnSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
výroby	výroba	k1gFnSc2
</s>
<s>
Vyrobeno	vyrobit	k5eAaPmNgNnS
vozů	vůz	k1gInPc2
</s>
<s>
Dodávky	dodávka	k1gFnPc1
</s>
<s>
Inekon	Inekon	k1gMnSc1
01	#num#	k4
Trio	trio	k1gNnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
12	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Inekon	Inekon	k1gMnSc1
12	#num#	k4
Trio	trio	k1gNnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
9	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Portland	Portland	k1gInSc1
<g/>
,	,	kIx,
Seattle	Seattle	k1gFnSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
</s>
<s>
Т	Т	k?
Inekon	Inekon	k1gInSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
8	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Sofie	Sofie	k1gFnSc1
</s>
<s>
Aliance	aliance	k1gFnSc1
TW	TW	kA
Team	team	k1gInSc4
(	(	kIx(
<g/>
KOS	kosit	k5eAaImRp2nS
Krnov	Krnov	k1gInSc1
&	&	k?
Pragoimex	Pragoimex	k1gInSc1
&	&	k?
VKV	VKV	kA
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
VarioLF	VarioLF	k?
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Ostravský	ostravský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
VarioLF	VarioLF	k1gFnSc1
<g/>
3	#num#	k4
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
výroby	výroba	k1gFnSc2
</s>
<s>
Počet	počet	k1gInSc1
vyrobených	vyrobený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
</s>
<s>
Dodávky	dodávka	k1gFnPc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.	R.	kA
<g/>
EV	Eva	k1gFnPc2
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
</s>
<s>
5	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.	R.	kA
<g/>
PV	PV	kA
</s>
<s>
2003-2008	2003-2008	k4
</s>
<s>
83	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Osijek	Osijka	k1gFnPc2
<g/>
,	,	kIx,
Volgograd	Volgograd	k1gInSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.	R.	kA
<g/>
PLF	PLF	kA
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
65	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.	R.	kA
<g/>
SLF	SLF	kA
</s>
<s>
od	od	k7c2
2008	#num#	k4
</s>
<s>
11	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
</s>
<s>
od	od	k7c2
2004	#num#	k4
</s>
<s>
165	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
–	–	k?
Litvínov	Litvínov	k1gInSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
Taškent	Taškent	k1gInSc1
</s>
<s>
VarioLF	VarioLF	k?
plus	plus	k1gInSc1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
8	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Most	most	k1gInSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
plus	plus	k1gInSc1
<g/>
/	/	kIx~
<g/>
o	o	k7c6
</s>
<s>
2013	#num#	k4
</s>
<s>
14	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
<g/>
2	#num#	k4
</s>
<s>
od	od	k7c2
2007	#num#	k4
</s>
<s>
35	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
<g/>
2	#num#	k4
plus	plus	k1gInSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
47	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
</s>
<s>
VarioLF	VarioLF	k?
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
IN	IN	kA
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
4	#num#	k4
kusy	kus	k1gInPc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
<g/>
3	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
</s>
<s>
2	#num#	k4
kusy	kus	k1gInPc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
VarioLF	VarioLF	k?
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
3	#num#	k4
kusy	kus	k1gInPc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
EVO1	EVO1	k4
</s>
<s>
od	od	k7c2
2015	#num#	k4
</s>
<s>
6	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Most	most	k1gInSc1
–	–	k?
Litvínov	Litvínov	k1gInSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
EVO	Eva	k1gFnSc5
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
o	o	k7c6
</s>
<s>
2018	#num#	k4
</s>
<s>
3	#num#	k4
kusy	kus	k1gInPc1
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
EVO2	EVO2	k4
</s>
<s>
od	od	k7c2
2012	#num#	k4
</s>
<s>
16	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
–	–	k?
Litvínov	Litvínov	k1gInSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
VV60LF	VV60LF	k4
(	(	kIx(
<g/>
vlečný	vlečný	k2eAgInSc1d1
vůz	vůz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
</s>
<s>
6	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1
vůz	vůz	k1gInSc1
K3R-N	K3R-N	k1gFnSc2
</s>
<s>
Pars	Pars	k6eAd1
nova	nova	k1gFnSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
výroby	výroba	k1gFnSc2
</s>
<s>
Vyrobeno	vyrobit	k5eAaPmNgNnS
vozů	vůz	k1gInPc2
</s>
<s>
Dodávky	dodávka	k1gFnPc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
K3R-N	K3R-N	k1gFnSc2
</s>
<s>
2006	#num#	k4
</s>
<s>
2	#num#	k4
kusy	kus	k1gInPc1
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
K2S	K2S	k1gFnSc2
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
9	#num#	k4
kusů	kus	k1gInPc2
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Tramvaje	tramvaj	k1gFnPc1
této	tento	k3xDgFnSc2
konstrukce	konstrukce	k1gFnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
vyráběny	vyrábět	k5eAaImNgInP
rovněž	rovněž	k9
v	v	k7c6
Továrně	továrna	k1gFnSc6
na	na	k7c4
vozy	vůz	k1gInPc4
v	v	k7c6
Kolíně	Kolín	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
počtu	počet	k1gInSc6
10	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vlečné	vlečný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
této	tento	k3xDgFnSc2
konstrukce	konstrukce	k1gFnSc2
byly	být	k5eAaImAgFnP
původně	původně	k6eAd1
vyráběny	vyrábět	k5eAaImNgFnP
v	v	k7c6
Ringhofferových	Ringhofferův	k2eAgInPc6d1
závodech	závod	k1gInPc6
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
podniku	podnik	k1gInSc6
Tatra-Vagónka	Tatra-Vagónka	k1gFnSc1
Kolín	Kolín	k1gInSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
poté	poté	k6eAd1
převzala	převzít	k5eAaPmAgFnS
výrobu	výroba	k1gFnSc4
Královopolská	královopolský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dalších	další	k2eAgInPc2d1
5	#num#	k4
vozů	vůz	k1gInPc2
vyrobil	vyrobit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
vlastními	vlastní	k2eAgFnPc7d1
silami	síla	k1gFnPc7
brněnský	brněnský	k2eAgInSc4d1
DP	DP	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KONEČNÁ	Konečná	k1gFnSc1
<g/>
,	,	kIx,
H.	H.	kA
Jako	jako	k8xC,k8xS
třetí	třetí	k4xOgInPc4
–	–	k?
Otisky	otisk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
90	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Karla	Karla	k1gFnSc1
Hofmannová	Hofmannová	k1gFnSc1
<g/>
:	:	kIx,
Víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
onomastika	onomastika	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Přece	přece	k8xC
věda	věda	k1gFnSc1
o	o	k7c6
vlastních	vlastní	k2eAgNnPc6d1
jménech	jméno	k1gNnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
lingvistou	lingvista	k1gMnSc7
(	(	kIx(
<g/>
dialektologem	dialektolog	k1gMnSc7
a	a	k8xC
onomastikem	onomastik	k1gMnSc7
<g/>
)	)	kIx)
Rudolfem	Rudolf	k1gMnSc7
Šrámkem	Šrámek	k1gMnSc7
<g/>
,	,	kIx,
In	In	k1gFnSc1
KAM	kam	k6eAd1
<g/>
,	,	kIx,
Společensko-informační	společensko-informační	k2eAgInSc4d1
magazín	magazín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Turistické	turistický	k2eAgNnSc1d1
a	a	k8xC
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
39	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
MUNI	MUNI	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
VĚTVIČKA	větvička	k1gFnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvežiznive	Krvežizniev	k1gFnPc1
bestyje	bestýt	k5eAaPmIp3nS
v	v	k7c6
tramvajce	tramvajka	k1gFnSc6
čislo	čislo	k1gNnSc1
5	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-02---	2011-02---	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
WWW	WWW	kA
<g/>
:	:	kIx,
<	<	kIx(
<g/>
http://www.ostravak.info/25-2-2011-krveziznive-bestyje-v-tramvajce-cislo-5/	http://www.ostravak.info/25-2-2011-krveziznive-bestyje-v-tramvajce-cislo-5/	k4
Archivováno	archivován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
>	>	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
19	#num#	k4
20	#num#	k4
↑	↑	k?
http://archiv.ihned.cz/c1-20581020-maji-trolejbusy-budoucnost	http://archiv.ihned.cz/c1-20581020-maji-trolejbusy-budoucnost	k1gFnSc1
<g/>
↑	↑	k?
http://dopravni.net/mhd/6809/strasburk/	http://dopravni.net/mhd/6809/strasburk/	k4
<g/>
↑	↑	k?
http://koroptew.blogspot.cz/2010/09/petrohradska-verejna-doprava.html	http://koroptew.blogspot.cz/2010/09/petrohradska-verejna-doprava.html	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LOSOS	losos	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
tramvají	tramvaj	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NADAS	NADAS	kA
-	-	kIx~
Nakladatelství	nakladatelství	k1gNnSc1
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
397	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Knižnice	knižnice	k1gFnSc2
silniční	silniční	k2eAgFnSc2d1
a	a	k8xC
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
LOSOS	losos	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Jiří	Jiří	k1gMnSc1
Bouda	Bouda	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
296	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
OKO	oko	k1gNnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
60	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
JANSA	Jansa	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozidla	vozidlo	k1gNnPc1
elektrické	elektrický	k2eAgFnSc2d1
trakce	trakce	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NADAS	NADAS	kA
-	-	kIx~
Nakladatelství	nakladatelství	k1gNnSc1
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
371	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Knižnice	knižnice	k1gFnSc1
nové	nový	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
a	a	k8xC
technologie	technologie	k1gFnSc2
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Koňka	koňka	k1gFnSc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc4
měst	město	k1gNnPc2
s	s	k7c7
tramvajovým	tramvajový	k2eAgInSc7d1
provozem	provoz	k1gInSc7
</s>
<s>
Typová	typový	k2eAgNnPc4d1
označení	označení	k1gNnPc4
tramvají	tramvaj	k1gFnPc2
Tatra	Tatra	k1gFnSc1
</s>
<s>
Šlapací	šlapací	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
Sylvestra	Sylvestr	k1gMnSc2
Krnky	Krnka	k1gMnSc2
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Křižík	Křižík	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tramvaj	tramvaj	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
tramvaj	tramvaj	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Porsche	Porsche	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
se	se	k3xPyFc4
sveze	svézt	k5eAaPmIp3nS
opravdu	opravdu	k6eAd1
každý	každý	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
tramvaj	tramvaj	k1gFnSc1
nejen	nejen	k6eAd1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Předpisy	předpis	k1gInPc1
<g/>
,	,	kIx,
návěsti	návěst	k1gFnPc1
<g/>
,	,	kIx,
vozidla	vozidlo	k1gNnPc1
<g/>
,	,	kIx,
tram	tram	k1gInSc1
<g/>
.	.	kIx.
<g/>
webzdarma	webzdarma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Veřejná	veřejný	k2eAgFnSc1d1
a	a	k8xC
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
silniční	silniční	k2eAgFnSc2d1
</s>
<s>
omnibus	omnibus	k1gInSc1
•	•	k?
autobusová	autobusový	k2eAgFnSc1d1
(	(	kIx(
<g/>
autobus	autobus	k1gInSc1
•	•	k?
metrobus	metrobus	k1gMnSc1
•	•	k?
Ekobus	Ekobus	k1gMnSc1
•	•	k?
cyklobus	cyklobus	k1gMnSc1
•	•	k?
skibus	skibus	k1gMnSc1
•	•	k?
radiobus	radiobus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
trolejbus	trolejbus	k1gInSc1
kolejová	kolejový	k2eAgFnSc5d1
</s>
<s>
tramvajová	tramvajový	k2eAgFnSc1d1
</s>
<s>
tramvaj	tramvaj	k1gFnSc1
(	(	kIx(
<g/>
koňská	koňský	k2eAgFnSc1d1
•	•	k?
parní	parní	k2eAgFnSc1d1
•	•	k?
plynová	plynový	k2eAgFnSc1d1
•	•	k?
pneumatická	pneumatický	k2eAgFnSc1d1
•	•	k?
dieselová	dieselový	k2eAgFnSc1d1
•	•	k?
elektrická	elektrický	k2eAgFnSc1d1
•	•	k?
vodíková	vodíkový	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
rychlodrážní	rychlodrážní	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
•	•	k?
vlakotramvaj	vlakotramvaj	k1gFnSc1
•	•	k?
podpovrchová	podpovrchový	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
</s>
<s>
metro	metro	k1gNnSc1
(	(	kIx(
<g/>
lehké	lehký	k2eAgNnSc1d1
<g/>
)	)	kIx)
•	•	k?
meziměstská	meziměstský	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
•	•	k?
příměstská	příměstský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
(	(	kIx(
<g/>
Esko	eska	k1gFnSc5
•	•	k?
S-Bahn	S-Bahn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
vodní	vodní	k2eAgFnSc1d1
</s>
<s>
městský	městský	k2eAgInSc1d1
přívoz	přívoz	k1gInSc1
•	•	k?
vodní	vodní	k2eAgFnSc4d1
tramvaj	tramvaj	k1gFnSc4
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
•	•	k?
linková	linkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
příměstská	příměstský	k2eAgFnSc1d1
•	•	k?
soukromá	soukromý	k2eAgFnSc1d1
•	•	k?
regionální	regionální	k2eAgFnSc1d1
•	•	k?
kyvadlová	kyvadlový	k2eAgFnSc1d1
•	•	k?
náhradní	náhradní	k2eAgInPc1d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4057884-7	4057884-7	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13703	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85128595	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85128595	#num#	k4
</s>
