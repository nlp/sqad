<p>
<s>
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
PČR	PČR	kA	PČR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
sbor	sbor	k1gInSc4	sbor
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
přeměnou	přeměna	k1gFnSc7	přeměna
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
československé	československý	k2eAgFnSc2d1	Československá
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Sboru	sbor	k1gInSc2	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dnem	den	k1gInSc7	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
zákona	zákon	k1gInSc2	zákon
ČNR	ČNR	kA	ČNR
č.	č.	k?	č.
283	[number]	k4	283
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
Policii	policie	k1gFnSc6	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
upravena	upravit	k5eAaPmNgFnS	upravit
novým	nový	k2eAgInSc7d1	nový
zákonem	zákon	k1gInSc7	zákon
(	(	kIx(	(
<g/>
č.	č.	k?	č.
273	[number]	k4	273
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
realizuje	realizovat	k5eAaBmIp3nS	realizovat
reformu	reforma	k1gFnSc4	reforma
policie	policie	k1gFnSc2	policie
navrženou	navržený	k2eAgFnSc4d1	navržená
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Ivanem	Ivan	k1gMnSc7	Ivan
Langerem	Langer	k1gMnSc7	Langer
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
záměrem	záměr	k1gInSc7	záměr
je	být	k5eAaImIp3nS	být
především	především	k9	především
policii	policie	k1gFnSc4	policie
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
úkoly	úkol	k1gInPc4	úkol
při	při	k7c6	při
zajištění	zajištění	k1gNnSc4	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
odbřemenit	odbřemenit	k5eAaImF	odbřemenit
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
působností	působnost	k1gFnPc2	působnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
úzce	úzko	k6eAd1	úzko
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Policisté	policista	k1gMnPc1	policista
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
pracovním	pracovní	k2eAgInSc6d1	pracovní
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
státní	státní	k2eAgInPc4d1	státní
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
PČR	PČR	kA	PČR
39	[number]	k4	39
500	[number]	k4	500
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úkoly	úkol	k1gInPc4	úkol
==	==	k?	==
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
plní	plnit	k5eAaImIp3nS	plnit
zejména	zejména	k9	zejména
tyto	tento	k3xDgInPc4	tento
úkoly	úkol	k1gInPc4	úkol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
chrání	chránit	k5eAaImIp3nS	chránit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
</s>
</p>
<p>
<s>
spolupůsobí	spolupůsobit	k5eAaImIp3nP	spolupůsobit
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
porušen	porušen	k2eAgInSc1d1	porušen
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
obnovení	obnovení	k1gNnSc3	obnovení
</s>
</p>
<p>
<s>
vede	vést	k5eAaImIp3nS	vést
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
</s>
</p>
<p>
<s>
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
jejich	jejich	k3xOp3gMnPc4	jejich
pachatele	pachatel	k1gMnPc4	pachatel
a	a	k8xC	a
realizuje	realizovat	k5eAaBmIp3nS	realizovat
opatření	opatření	k1gNnSc1	opatření
při	při	k7c6	při
předcházení	předcházení	k1gNnSc6	předcházení
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
o	o	k7c6	o
trestných	trestný	k2eAgInPc6d1	trestný
činech	čin	k1gInPc6	čin
–	–	k?	–
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
policejní	policejní	k2eAgInSc1d1	policejní
orgán	orgán	k1gInSc1	orgán
(	(	kIx(	(
<g/>
odhalování	odhalování	k1gNnSc2	odhalování
a	a	k8xC	a
prověřování	prověřování	k1gNnSc2	prověřování
skutečností	skutečnost	k1gFnPc2	skutečnost
nasvědčující	nasvědčující	k2eAgFnSc2d1	nasvědčující
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
trestný	trestný	k2eAgInSc1d1	trestný
čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
pachatelem	pachatel	k1gMnSc7	pachatel
je	být	k5eAaImIp3nS	být
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
či	či	k8xC	či
občanský	občanský	k2eAgMnSc1d1	občanský
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
,	,	kIx,	,
však	však	k9	však
provádí	provádět	k5eAaImIp3nS	provádět
Generální	generální	k2eAgFnSc1d1	generální
inspekce	inspekce	k1gFnSc1	inspekce
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ve	v	k7c6	v
vymezeném	vymezený	k2eAgInSc6d1	vymezený
rozsahu	rozsah	k1gInSc6	rozsah
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ochranu	ochrana	k1gFnSc4	ochrana
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
</s>
</p>
<p>
<s>
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ochranu	ochrana	k1gFnSc4	ochrana
ústavních	ústavní	k2eAgMnPc2d1	ústavní
činitelů	činitel	k1gMnPc2	činitel
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
chráněných	chráněný	k2eAgFnPc2d1	chráněná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
poskytována	poskytován	k2eAgFnSc1d1	poskytována
osobní	osobní	k2eAgFnSc1d1	osobní
ochrana	ochrana	k1gFnSc1	ochrana
podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
dohod	dohoda	k1gFnPc2	dohoda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
služba	služba	k1gFnSc1	služba
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ochranu	ochrana	k1gFnSc4	ochrana
zastupitelských	zastupitelský	k2eAgInPc2d1	zastupitelský
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
sídelních	sídelní	k2eAgInPc2d1	sídelní
objektů	objekt	k1gInPc2	objekt
Parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
významu	význam	k1gInSc2	význam
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
určených	určený	k2eAgInPc2d1	určený
vládou	vláda	k1gFnSc7	vláda
</s>
</p>
<p>
<s>
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
plynulost	plynulost	k1gFnSc4	plynulost
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
spolupůsobí	spolupůsobit	k5eAaImIp3nP	spolupůsobit
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
řízení	řízení	k1gNnSc6	řízení
</s>
</p>
<p>
<s>
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
pojištění	pojištění	k1gNnSc4	pojištění
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
škodu	škoda	k1gFnSc4	škoda
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
provozem	provoz	k1gInSc7	provoz
vozidla	vozidlo	k1gNnPc1	vozidlo
</s>
</p>
<p>
<s>
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
přestupky	přestupek	k1gInPc4	přestupek
</s>
</p>
<p>
<s>
projednává	projednávat	k5eAaImIp3nS	projednávat
některé	některý	k3yIgInPc4	některý
přestupky	přestupek	k1gInPc4	přestupek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vede	vést	k5eAaImIp3nS	vést
evidence	evidence	k1gFnSc1	evidence
a	a	k8xC	a
statistiky	statistika	k1gFnPc1	statistika
potřebné	potřebný	k2eAgFnPc1d1	potřebná
pro	pro	k7c4	pro
plnění	plnění	k1gNnSc4	plnění
svých	svůj	k3xOyFgInPc2	svůj
úkolů	úkol	k1gInPc2	úkol
</s>
</p>
<p>
<s>
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
celostátní	celostátní	k2eAgNnSc4d1	celostátní
pátrání	pátrání	k1gNnSc4	pátrání
</s>
</p>
<p>
<s>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyrozumění	vyrozumění	k1gNnSc2	vyrozumění
orgánu	orgán	k1gInSc2	orgán
Vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
služby	služba	k1gFnSc2	služba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
úkony	úkon	k1gInPc1	úkon
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
bezprostředním	bezprostřední	k2eAgNnSc7d1	bezprostřední
pronásledováním	pronásledování	k1gNnSc7	pronásledování
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
uprchly	uprchnout	k5eAaPmAgInP	uprchnout
z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
vazby	vazba	k1gFnSc2	vazba
nebo	nebo	k8xC	nebo
z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
</s>
</p>
<p>
<s>
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
svěřence	svěřenec	k1gMnPc4	svěřenec
s	s	k7c7	s
nařízenou	nařízený	k2eAgFnSc7d1	nařízená
ústavní	ústavní	k2eAgFnSc7d1	ústavní
nebo	nebo	k8xC	nebo
uloženou	uložený	k2eAgFnSc7d1	uložená
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
výchovou	výchova	k1gFnSc7	výchova
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
<g/>
,	,	kIx,	,
a	a	k8xC	a
spolupůsobí	spolupůsobit	k5eAaImIp3nP	spolupůsobit
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pohotovostní	pohotovostní	k2eAgFnSc4d1	pohotovostní
ochranu	ochrana	k1gFnSc4	ochrana
jaderných	jaderný	k2eAgNnPc2d1	jaderné
zařízení	zařízení	k1gNnPc2	zařízení
určených	určený	k2eAgInPc2d1	určený
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
ochraně	ochrana	k1gFnSc6	ochrana
jaderného	jaderný	k2eAgInSc2d1	jaderný
materiálu	materiál	k1gInSc2	materiál
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
přepravě	přeprava	k1gFnSc6	přeprava
</s>
</p>
<p>
<s>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
zabezpečování	zabezpečování	k1gNnSc6	zabezpečování
místních	místní	k2eAgFnPc2d1	místní
záležitostí	záležitost	k1gFnPc2	záležitost
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
</s>
</p>
<p>
<s>
plní	plnit	k5eAaImIp3nS	plnit
úkoly	úkol	k1gInPc4	úkol
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
==	==	k?	==
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
je	být	k5eAaImIp3nS	být
podřízena	podřídit	k5eAaPmNgFnS	podřídit
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Policii	policie	k1gFnSc3	policie
tvoří	tvořit	k5eAaImIp3nS	tvořit
Policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
<g/>
,	,	kIx,	,
útvary	útvar	k1gInPc1	útvar
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
útvary	útvar	k1gInPc1	útvar
s	s	k7c7	s
územně	územně	k6eAd1	územně
vymezenou	vymezený	k2eAgFnSc7d1	vymezená
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
===	===	k?	===
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
policie	policie	k1gFnSc2	policie
řídí	řídit	k5eAaImIp3nS	řídit
Policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
policejní	policejní	k2eAgMnSc1d1	policejní
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
představeným	představený	k1gMnSc7	představený
všech	všecek	k3xTgMnPc2	všecek
policistů	policista	k1gMnPc2	policista
a	a	k8xC	a
kterého	který	k3yRgMnSc4	který
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
(	(	kIx(	(
<g/>
způsob	způsob	k1gInSc4	způsob
jeho	on	k3xPp3gNnSc2	on
odvolání	odvolání	k1gNnSc2	odvolání
není	být	k5eNaImIp3nS	být
současnou	současný	k2eAgFnSc7d1	současná
právní	právní	k2eAgFnSc7d1	právní
úpravou	úprava	k1gFnSc7	úprava
řešen	řešit	k5eAaImNgMnS	řešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Policejnímu	policejní	k2eAgMnSc3d1	policejní
prezidentovi	prezident	k1gMnSc3	prezident
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prezidia	prezidium	k1gNnSc2	prezidium
podřízeni	podřízen	k2eAgMnPc1d1	podřízen
jeho	jeho	k3xOp3gMnPc1	jeho
náměstci	náměstek	k1gMnPc1	náměstek
<g/>
:	:	kIx,	:
první	první	k4xOgMnSc1	první
náměstek	náměstek	k1gMnSc1	náměstek
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
náměstek	náměstek	k1gMnSc1	náměstek
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
(	(	kIx(	(
<g/>
SKPV	SKPV	kA	SKPV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náměstek	náměstek	k1gMnSc1	náměstek
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
policejním	policejní	k2eAgMnSc7d1	policejní
prezidentem	prezident	k1gMnSc7	prezident
Jan	Jan	k1gMnSc1	Jan
Švejdar	Švejdar	k1gMnSc1	Švejdar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
Janem	Jan	k1gMnSc7	Jan
Hamáčkem	Hamáček	k1gMnSc7	Hamáček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Celostátní	celostátní	k2eAgInPc4d1	celostátní
útvary	útvar	k1gInPc4	útvar
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
služeb	služba	k1gFnPc2	služba
stojí	stát	k5eAaImIp3nP	stát
ředitelé	ředitel	k1gMnPc1	ředitel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
<g/>
.	.	kIx.	.
</s>
<s>
Ředitele	ředitel	k1gMnSc4	ředitel
Útvaru	útvar	k1gInSc2	útvar
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
prezidenta	prezident	k1gMnSc2	prezident
ČR	ČR	kA	ČR
policejní	policejní	k2eAgMnSc1d1	policejní
prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
policii	policie	k1gFnSc6	policie
působí	působit	k5eAaImIp3nP	působit
tyto	tento	k3xDgInPc4	tento
útvary	útvar	k1gInPc4	útvar
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
působností	působnost	k1gFnSc7	působnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kriminalistický	kriminalistický	k2eAgInSc1d1	kriminalistický
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
služba	služba	k1gFnSc1	služba
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
protidrogová	protidrogový	k2eAgFnSc1d1	protidrogová
centrála	centrála	k1gFnSc1	centrála
SKPV	SKPV	kA	SKPV
</s>
</p>
<p>
<s>
Pyrotechnická	pyrotechnický	k2eAgFnSc1d1	pyrotechnická
služba	služba	k1gFnSc1	služba
</s>
</p>
<p>
<s>
Ředitelství	ředitelství	k1gNnSc1	ředitelství
služby	služba	k1gFnSc2	služba
cizinecké	cizinecký	k2eAgFnSc2d1	cizinecká
policie	policie	k1gFnSc2	policie
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
dokumentace	dokumentace	k1gFnSc2	dokumentace
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
zločinů	zločin	k1gInPc2	zločin
komunismu	komunismus	k1gInSc2	komunismus
SKPV	SKPV	kA	SKPV
</s>
</p>
<p>
<s>
Útvar	útvar	k1gInSc1	útvar
policejního	policejní	k2eAgNnSc2d1	policejní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
služební	služební	k2eAgFnSc2d1	služební
přípravy	příprava	k1gFnSc2	příprava
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
centrála	centrála	k1gFnSc1	centrála
proti	proti	k7c3	proti
organizovanému	organizovaný	k2eAgInSc3d1	organizovaný
zločinu	zločin	k1gInSc3	zločin
SKPV	SKPV	kA	SKPV
</s>
</p>
<p>
<s>
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
služba	služba	k1gFnSc1	služba
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Útvar	útvar	k1gInSc1	útvar
rychlého	rychlý	k2eAgNnSc2d1	rychlé
nasazení	nasazení	k1gNnSc2	nasazení
</s>
</p>
<p>
<s>
Útvar	útvar	k1gInSc1	útvar
speciálních	speciální	k2eAgFnPc2d1	speciální
činností	činnost	k1gFnPc2	činnost
SKPV	SKPV	kA	SKPV
</s>
</p>
<p>
<s>
Útvar	útvar	k1gInSc1	útvar
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
činností	činnost	k1gFnPc2	činnost
SKPV	SKPV	kA	SKPV
</s>
</p>
<p>
<s>
===	===	k?	===
Útvary	útvar	k1gInPc1	útvar
s	s	k7c7	s
územně	územně	k6eAd1	územně
vymezenou	vymezený	k2eAgFnSc7d1	vymezená
působností	působnost	k1gFnSc7	působnost
===	===	k?	===
</s>
</p>
<p>
<s>
Útvary	útvar	k1gInPc1	útvar
s	s	k7c7	s
územně	územně	k6eAd1	územně
vymezenou	vymezený	k2eAgFnSc7d1	vymezená
působností	působnost	k1gFnSc7	působnost
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
krajská	krajský	k2eAgNnPc1d1	krajské
ředitelství	ředitelství	k1gNnPc1	ředitelství
(	(	kIx(	(
<g/>
a	a	k8xC	a
útvary	útvar	k1gInPc1	útvar
zřízené	zřízený	k2eAgInPc1d1	zřízený
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
rámci	rámec	k1gInSc6	rámec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
policie	policie	k1gFnSc2	policie
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
krajePolicejní	krajePolicejný	k2eAgMnPc1d1	krajePolicejný
prezident	prezident	k1gMnSc1	prezident
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každého	každý	k3xTgNnSc2	každý
krajského	krajský	k2eAgNnSc2d1	krajské
ředitelství	ředitelství	k1gNnSc2	ředitelství
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
jeho	on	k3xPp3gMnSc2	on
ředitele	ředitel	k1gMnSc2	ředitel
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
okresů	okres	k1gInPc2	okres
územní	územní	k2eAgInPc1d1	územní
odbory	odbor	k1gInPc1	odbor
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
okresní	okresní	k2eAgNnSc4d1	okresní
ředitelství	ředitelství	k1gNnSc4	ředitelství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
Plzni	Plzeň	k1gFnSc6	Plzeň
ale	ale	k8xC	ale
místo	místo	k7c2	místo
nich	on	k3xPp3gNnPc2	on
existují	existovat	k5eAaImIp3nP	existovat
městská	městský	k2eAgNnPc1d1	Městské
ředitelství	ředitelství	k1gNnPc1	ředitelství
a	a	k8xC	a
v	v	k7c4	v
hl.	hl.	k?	hl.
m.	m.	k?	m.
Praze	Praha	k1gFnSc6	Praha
čtyři	čtyři	k4xCgNnPc4	čtyři
obvodní	obvodní	k2eAgNnPc4d1	obvodní
ředitelství	ředitelství	k1gNnPc4	ředitelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgInPc2	tento
odborů	odbor	k1gInPc2	odbor
a	a	k8xC	a
ředitelství	ředitelství	k1gNnSc2	ředitelství
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zřizovány	zřizován	k2eAgFnPc1d1	zřizována
jako	jako	k8xC	jako
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgInSc4d2	nižší
článek	článek	k1gInSc4	článek
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
obvodní	obvodní	k2eAgFnSc1d1	obvodní
a	a	k8xC	a
místní	místní	k2eAgNnSc1d1	místní
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgFnSc7d1	organizační
složkou	složka	k1gFnSc7	složka
státu	stát	k1gInSc2	stát
vystupující	vystupující	k2eAgFnSc1d1	vystupující
za	za	k7c4	za
Policii	policie	k1gFnSc4	policie
ČR	ČR	kA	ČR
navenek	navenek	k6eAd1	navenek
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
jen	jen	k6eAd1	jen
krajská	krajský	k2eAgNnPc4d1	krajské
ředitelství	ředitelství	k1gNnPc4	ředitelství
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
nižší	nízký	k2eAgInPc1d2	nižší
útvary	útvar	k1gInPc1	útvar
s	s	k7c7	s
územně	územně	k6eAd1	územně
vymezenou	vymezený	k2eAgFnSc7d1	vymezená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
samostatně	samostatně	k6eAd1	samostatně
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
činnosti	činnost	k1gFnPc1	činnost
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc1d1	určený
interními	interní	k2eAgInPc7d1	interní
předpisy	předpis	k1gInPc7	předpis
(	(	kIx(	(
<g/>
např.	např.	kA	např.
závaznými	závazný	k2eAgMnPc7d1	závazný
pokyny	pokyn	k1gInPc1	pokyn
policejního	policejní	k2eAgMnSc2d1	policejní
prezidenta	prezident	k1gMnSc2	prezident
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc7	jejich
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
organizačními	organizační	k2eAgFnPc7d1	organizační
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Služební	služební	k2eAgInPc1d1	služební
zákroky	zákrok	k1gInPc1	zákrok
a	a	k8xC	a
úkony	úkon	k1gInPc1	úkon
policistů	policista	k1gMnPc2	policista
==	==	k?	==
</s>
</p>
<p>
<s>
Policisté	policista	k1gMnPc1	policista
realizují	realizovat	k5eAaBmIp3nP	realizovat
úkoly	úkol	k1gInPc4	úkol
policie	policie	k1gFnSc2	policie
prováděním	provádění	k1gNnSc7	provádění
služebních	služební	k2eAgInPc2d1	služební
zákroků	zákrok	k1gInPc2	zákrok
a	a	k8xC	a
úkonů	úkon	k1gInPc2	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
dbát	dbát	k5eAaImF	dbát
cti	čest	k1gFnSc3	čest
<g/>
,	,	kIx,	,
vážnosti	vážnost	k1gFnSc3	vážnost
a	a	k8xC	a
důstojnosti	důstojnost	k1gFnSc3	důstojnost
osob	osoba	k1gFnPc2	osoba
i	i	k8xC	i
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
a	a	k8xC	a
nepřipustit	připustit	k5eNaPmF	připustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
osobám	osoba	k1gFnPc3	osoba
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
činností	činnost	k1gFnSc7	činnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
bezdůvodná	bezdůvodný	k2eAgFnSc1d1	bezdůvodná
újma	újma	k1gFnSc1	újma
a	a	k8xC	a
případný	případný	k2eAgInSc1d1	případný
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
překročil	překročit	k5eAaPmAgMnS	překročit
míru	míra	k1gFnSc4	míra
nezbytnou	zbytný	k2eNgFnSc4d1	zbytný
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
účelu	účel	k1gInSc2	účel
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
služebním	služební	k2eAgInSc7d1	služební
zákrokem	zákrok	k1gInSc7	zákrok
nebo	nebo	k8xC	nebo
služebním	služební	k2eAgInSc7d1	služební
úkonem	úkon	k1gInSc7	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Úkonem	úkon	k1gInSc7	úkon
se	se	k3xPyFc4	se
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
policii	policie	k1gFnSc6	policie
rozumí	rozumět	k5eAaImIp3nS	rozumět
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
různé	různý	k2eAgNnSc1d1	různé
jednání	jednání	k1gNnSc1	jednání
policisty	policista	k1gMnSc2	policista
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákrokem	zákrok	k1gInSc7	zákrok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
zákona	zákon	k1gInSc2	zákon
rozumí	rozumět	k5eAaImIp3nS	rozumět
úkon	úkon	k1gInSc1	úkon
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
vynucování	vynucování	k1gNnSc3	vynucování
splnění	splnění	k1gNnSc1	splnění
právní	právní	k2eAgFnSc2d1	právní
povinnosti	povinnost	k1gFnSc2	povinnost
nebo	nebo	k8xC	nebo
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
ochraně	ochrana	k1gFnSc3	ochrana
práv	práv	k2eAgMnSc1d1	práv
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
síly	síla	k1gFnSc2	síla
nebo	nebo	k8xC	nebo
hrozby	hrozba	k1gFnPc1	hrozba
jejího	její	k3xOp3gNnSc2	její
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
povinen	povinen	k2eAgMnSc1d1	povinen
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
okolnosti	okolnost	k1gFnPc1	okolnost
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
,	,	kIx,	,
použít	použít	k5eAaPmF	použít
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
výzvy	výzva	k1gFnPc4	výzva
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
začátku	začátek	k1gInSc6	začátek
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Jménem	jméno	k1gNnSc7	jméno
zákona	zákon	k1gInSc2	zákon
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
uposlechnout	uposlechnout	k5eAaPmF	uposlechnout
výzvy	výzva	k1gFnSc2	výzva
zakročujícího	zakročující	k2eAgMnSc2d1	zakročující
policisty	policista	k1gMnSc2	policista
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
své	svůj	k3xOyFgFnSc2	svůj
pravomoci	pravomoc	k1gFnSc2	pravomoc
musí	muset	k5eAaImIp3nS	muset
prokázat	prokázat	k5eAaPmF	prokázat
svou	svůj	k3xOyFgFnSc4	svůj
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
policii	policie	k1gFnSc3	policie
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
okolnosti	okolnost	k1gFnPc1	okolnost
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
nebo	nebo	k8xC	nebo
služebního	služební	k2eAgInSc2d1	služební
úkonu	úkon	k1gInSc2	úkon
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
služebním	služební	k2eAgInSc7d1	služební
stejnokrojem	stejnokroj	k1gInSc7	stejnokroj
s	s	k7c7	s
identifikačním	identifikační	k2eAgNnSc7d1	identifikační
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
služebním	služební	k2eAgInSc7d1	služební
průkazem	průkaz	k1gInSc7	průkaz
<g/>
,	,	kIx,	,
odznakem	odznak	k1gInSc7	odznak
služby	služba	k1gFnSc2	služba
kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
nebo	nebo	k8xC	nebo
ústním	ústní	k2eAgNnSc7d1	ústní
prohlášením	prohlášení	k1gNnSc7	prohlášení
"	"	kIx"	"
<g/>
policie	policie	k1gFnSc2	policie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgNnSc7	takový
ústním	ústní	k2eAgNnSc7d1	ústní
prohlášením	prohlášení	k1gNnSc7	prohlášení
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
policista	policista	k1gMnSc1	policista
svou	svůj	k3xOyFgFnSc4	svůj
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
policii	policie	k1gFnSc3	policie
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
okolnosti	okolnost	k1gFnPc1	okolnost
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
tuto	tento	k3xDgFnSc4	tento
příslušnost	příslušnost	k1gFnSc4	příslušnost
prokázat	prokázat	k5eAaPmF	prokázat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Služebním	služební	k2eAgInSc7d1	služební
stejnokrojem	stejnokroj	k1gInSc7	stejnokroj
<g/>
,	,	kIx,	,
služebním	služební	k2eAgInSc7d1	služební
průkazem	průkaz	k1gInSc7	průkaz
nebo	nebo	k8xC	nebo
odznakem	odznak	k1gInSc7	odznak
služby	služba	k1gFnSc2	služba
kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
se	se	k3xPyFc4	se
policista	policista	k1gMnSc1	policista
prokáže	prokázat	k5eAaPmIp3nS	prokázat
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
to	ten	k3xDgNnSc1	ten
okolnosti	okolnost	k1gFnPc1	okolnost
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
nebo	nebo	k8xC	nebo
služebního	služební	k2eAgInSc2d1	služební
úkonu	úkon	k1gInSc2	úkon
dovolí	dovolit	k5eAaPmIp3nS	dovolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oprávnění	oprávnění	k1gNnSc1	oprávnění
policisty	policista	k1gMnSc2	policista
==	==	k?	==
</s>
</p>
<p>
<s>
Policista	policista	k1gMnSc1	policista
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
služebního	služební	k2eAgInSc2d1	služební
zákroku	zákrok	k1gInSc2	zákrok
či	či	k8xC	či
úkonů	úkon	k1gInPc2	úkon
má	mít	k5eAaImIp3nS	mít
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
Policii	policie	k1gFnSc6	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
řadu	řad	k1gInSc2	řad
oprávnění	oprávnění	k1gNnPc2	oprávnění
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
od	od	k7c2	od
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
požadovat	požadovat	k5eAaImF	požadovat
prokázání	prokázání	k1gNnSc4	prokázání
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
může	moct	k5eAaImIp3nS	moct
zajistit	zajistit	k5eAaPmF	zajistit
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
policejní	policejní	k2eAgFnSc6d1	policejní
cele	cela	k1gFnSc6	cela
až	až	k9	až
na	na	k7c4	na
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
cizince	cizinec	k1gMnSc2	cizinec
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c6	o
správním	správní	k2eAgNnSc6d1	správní
vyhoštění	vyhoštění	k1gNnSc6	vyhoštění
až	až	k9	až
na	na	k7c4	na
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
a	a	k8xC	a
agresivní	agresivní	k2eAgFnSc4d1	agresivní
osobu	osoba	k1gFnSc4	osoba
omezit	omezit	k5eAaPmF	omezit
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
připoutáním	připoutání	k1gNnSc7	připoutání
ke	k	k7c3	k
vhodnému	vhodný	k2eAgInSc3d1	vhodný
předmětu	předmět	k1gInSc3	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
odebrat	odebrat	k5eAaPmF	odebrat
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
provést	provést	k5eAaPmF	provést
prohlídku	prohlídka	k1gFnSc4	prohlídka
dopravního	dopravní	k2eAgInSc2d1	dopravní
prostředku	prostředek	k1gInSc2	prostředek
<g/>
,	,	kIx,	,
zakázat	zakázat	k5eAaPmF	zakázat
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
určené	určený	k2eAgNnSc4d1	určené
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
provozovny	provozovna	k1gFnSc2	provozovna
i	i	k9	i
do	do	k7c2	do
obydlí	obydlí	k1gNnSc2	obydlí
osoby	osoba	k1gFnSc2	osoba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
splnění	splnění	k1gNnSc2	splnění
zákonných	zákonný	k2eAgFnPc2d1	zákonná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
důvodné	důvodný	k2eAgFnSc2d1	důvodná
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ohrožen	ohrožen	k2eAgInSc4d1	ohrožen
život	život	k1gInSc4	život
nebo	nebo	k8xC	nebo
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
-li	i	k?	-li
větší	veliký	k2eAgFnSc1d2	veliký
škoda	škoda	k1gFnSc1	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
důvodné	důvodný	k2eAgNnSc4d1	důvodné
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
nachází	nacházet	k5eAaImIp3nS	nacházet
osoba	osoba	k1gFnSc1	osoba
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
,	,	kIx,	,
policista	policista	k1gMnSc1	policista
může	moct	k5eAaImIp3nS	moct
otevřít	otevřít	k5eAaPmF	otevřít
byt	byt	k1gInSc4	byt
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
Policii	policie	k1gFnSc4	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
také	také	k9	také
upravuje	upravovat	k5eAaImIp3nS	upravovat
oprávnění	oprávnění	k1gNnSc4	oprávnění
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
operativně	operativně	k6eAd1	operativně
pátracích	pátrací	k2eAgInPc2d1	pátrací
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
trestním	trestní	k2eAgNnSc7d1	trestní
řízením	řízení	k1gNnSc7	řízení
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgInPc1d1	krycí
doklady	doklad	k1gInPc1	doklad
<g/>
,	,	kIx,	,
konspirativní	konspirativní	k2eAgInPc1d1	konspirativní
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
zabezpečovací	zabezpečovací	k2eAgFnSc1d1	zabezpečovací
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc1	využití
informátora	informátor	k1gMnSc2	informátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
ochrany	ochrana	k1gFnSc2	ochrana
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
<g/>
,	,	kIx,	,
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
je	být	k5eAaImIp3nS	být
policista	policista	k1gMnSc1	policista
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
použít	použít	k5eAaPmF	použít
donucovací	donucovací	k2eAgInPc4d1	donucovací
prostředky	prostředek	k1gInPc4	prostředek
proti	proti	k7c3	proti
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chráněné	chráněný	k2eAgInPc4d1	chráněný
statky	statek	k1gInPc4	statek
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zákonem	zákon	k1gInSc7	zákon
vymezených	vymezený	k2eAgInPc6d1	vymezený
případech	případ	k1gInPc6	případ
má	mít	k5eAaImIp3nS	mít
policista	policista	k1gMnSc1	policista
oprávnění	oprávnění	k1gNnSc4	oprávnění
použít	použít	k5eAaPmF	použít
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Policejní	policejní	k2eAgFnSc2d1	policejní
školy	škola	k1gFnSc2	škola
==	==	k?	==
</s>
</p>
<p>
<s>
Policejní	policejní	k2eAgFnSc1d1	policejní
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
:	:	kIx,	:
Policejní	policejní	k2eAgFnSc1d1	policejní
akademie	akademie	k1gFnSc1	akademie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Policejní	policejní	k2eAgFnSc2d1	policejní
vyšší	vysoký	k2eAgFnSc2d2	vyšší
odborné	odborný	k2eAgFnSc2d1	odborná
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
policejní	policejní	k2eAgFnSc2d1	policejní
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
:	:	kIx,	:
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
policejní	policejní	k2eAgFnSc1d1	policejní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
policejní	policejní	k2eAgFnSc1d1	policejní
škola	škola	k1gFnSc1	škola
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
policejní	policejní	k2eAgFnSc1d1	policejní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
policejní	policejní	k2eAgFnSc1d1	policejní
škola	škola	k1gFnSc1	škola
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
Holešově	Holešov	k1gInSc6	Holešov
</s>
</p>
<p>
<s>
==	==	k?	==
Policejní	policejní	k2eAgNnPc1d1	policejní
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
značení	značení	k1gNnSc1	značení
==	==	k?	==
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
celobílý	celobílý	k2eAgInSc1d1	celobílý
nátěr	nátěr	k1gInSc1	nátěr
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
podélný	podélný	k2eAgInSc1d1	podélný
zelený	zelený	k2eAgInSc1d1	zelený
pruh	pruh	k1gInSc1	pruh
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
bočnici	bočnice	k1gFnSc6	bočnice
vozu	vůz	k1gInSc2	vůz
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
výšky	výška	k1gFnSc2	výška
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
nápis	nápis	k1gInSc1	nápis
POLICIE	policie	k1gFnSc2	policie
na	na	k7c6	na
boku	bok	k1gInSc6	bok
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
celobarevný	celobarevný	k2eAgInSc1d1	celobarevný
kruhový	kruhový	k2eAgInSc1d1	kruhový
policejní	policejní	k2eAgInSc1d1	policejní
znak	znak	k1gInSc1	znak
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
kapotě	kapota	k1gFnSc6	kapota
vozu	vůz	k1gInSc2	vůz
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
celostříbrný	celostříbrný	k2eAgInSc1d1	celostříbrný
nátěr	nátěr	k1gInSc1	nátěr
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
podélný	podélný	k2eAgInSc1d1	podélný
modrý	modrý	k2eAgInSc1d1	modrý
retroreflexní	retroreflexní	k2eAgInSc1d1	retroreflexní
pruh	pruh	k1gInSc1	pruh
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
bočnici	bočnice	k1gFnSc6	bočnice
vozu	vůz	k1gInSc2	vůz
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
výšky	výška	k1gFnSc2	výška
vozu	vůz	k1gInSc2	vůz
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
nápisy	nápis	k1gInPc7	nápis
"	"	kIx"	"
<g/>
POMÁHAT	pomáhat	k5eAaImF	pomáhat
A	a	k8xC	a
CHRÁNIT	chránit	k5eAaImF	chránit
<g/>
"	"	kIx"	"
+	+	kIx~	+
číslem	číslo	k1gNnSc7	číslo
policejní	policejní	k2eAgFnSc2d1	policejní
nouzové	nouzový	k2eAgFnSc2d1	nouzová
linky	linka	k1gFnSc2	linka
158	[number]	k4	158
<g/>
,	,	kIx,	,
šikmé	šikmý	k2eAgInPc4d1	šikmý
žluté	žlutý	k2eAgInPc4d1	žlutý
a	a	k8xC	a
modré	modrý	k2eAgInPc4d1	modrý
retroreflexní	retroreflexní	k2eAgInPc4d1	retroreflexní
pruhy	pruh	k1gInPc4	pruh
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
a	a	k8xC	a
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
délky	délka	k1gFnPc4	délka
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
nápis	nápis	k1gInSc1	nápis
POLICIE	policie	k1gFnSc2	policie
na	na	k7c6	na
boku	bok	k1gInSc6	bok
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
celobarevný	celobarevný	k2eAgInSc1d1	celobarevný
kruhový	kruhový	k2eAgInSc1d1	kruhový
policejní	policejní	k2eAgInSc1d1	policejní
znak	znak	k1gInSc1	znak
+	+	kIx~	+
černý	černý	k2eAgInSc1d1	černý
nápis	nápis	k1gInSc1	nápis
POLICIE	policie	k1gFnSc2	policie
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
kapotě	kapota	k1gFnSc6	kapota
vozu	vůz	k1gInSc2	vůz
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
nové	nový	k2eAgNnSc1d1	nové
logo	logo	k1gNnSc1	logo
tísňové	tísňový	k2eAgFnSc2d1	tísňová
linky	linka	k1gFnSc2	linka
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
studentka	studentka	k1gFnSc1	studentka
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Albína	Albín	k1gMnSc4	Albín
Bráfa	Bráf	k1gMnSc4	Bráf
a	a	k8xC	a
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
státní	státní	k2eAgFnSc2d1	státní
jazykové	jazykový	k2eAgFnSc2d1	jazyková
zkoušky	zkouška	k1gFnSc2	zkouška
Třebíč	Třebíč	k1gFnSc1	Třebíč
Andrea	Andrea	k1gFnSc1	Andrea
Jelínková	Jelínková	k1gFnSc1	Jelínková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FOJTÍK	Fojtík	k1gMnSc1	Fojtík
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgInPc1d1	policejní
vrtulníky	vrtulník	k1gInPc1	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
870	[number]	k4	870
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DLOUHÝ	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
;	;	kIx,	;
MINAŘÍK	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgNnSc1d1	policejní
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
;	;	kIx,	;
Včera	včera	k6eAd1	včera
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
THEMIS	Themis	k1gFnSc1	Themis
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Tiskárny	tiskárna	k1gFnSc2	tiskárna
MV	MV	kA	MV
<g/>
,	,	kIx,	,
p.	p.	k?	p.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
56	[number]	k4	56
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85821	[number]	k4	85821
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOJTÍK	Fojtík	k1gMnSc1	Fojtík
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulníky	vrtulník	k1gInPc1	vrtulník
v	v	k7c6	v
integrovaném	integrovaný	k2eAgInSc6d1	integrovaný
záchranném	záchranný	k2eAgInSc6d1	záchranný
systému	systém	k1gInSc6	systém
<g/>
;	;	kIx,	;
Policejní	policejní	k2eAgNnSc1d1	policejní
nasazení	nasazení	k1gNnSc1	nasazení
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
Jakub	Jakub	k1gMnSc1	Jakub
Fojtík	Fojtík	k1gMnSc1	Fojtík
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Září	září	k1gNnSc1	září
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
;	;	kIx,	;
OHERA	OHERA	kA	OHERA
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
policistů	policista	k1gMnPc2	policista
některých	některý	k3yIgInPc2	některý
celorepublikových	celorepublikový	k2eAgInPc2d1	celorepublikový
útvarů	útvar	k1gInPc2	útvar
<g/>
;	;	kIx,	;
Oddělení	oddělení	k1gNnSc1	oddělení
cíleného	cílený	k2eAgNnSc2d1	cílené
pátrání	pátrání	k1gNnSc2	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
archiv	archiv	k1gInSc1	archiv
Oddělení	oddělení	k1gNnSc2	oddělení
cíleného	cílený	k2eAgNnSc2d1	cílené
pátrání	pátrání	k1gNnSc2	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
38	[number]	k4	38
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
policistů	policista	k1gMnPc2	policista
některých	některý	k3yIgInPc2	některý
celorepublikových	celorepublikový	k2eAgInPc2d1	celorepublikový
útvarů	útvar	k1gInPc2	útvar
<g/>
;	;	kIx,	;
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Září	září	k1gNnSc1	září
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
policistů	policista	k1gMnPc2	policista
některých	některý	k3yIgInPc2	některý
celorepublikových	celorepublikový	k2eAgInPc2d1	celorepublikový
útvarů	útvar	k1gInPc2	útvar
<g/>
;	;	kIx,	;
Národní	národní	k2eAgFnSc1d1	národní
protidrogová	protidrogový	k2eAgFnSc1d1	protidrogová
centrála	centrála	k1gFnSc1	centrála
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Listopad	listopad	k1gInSc1	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
policistů	policista	k1gMnPc2	policista
některých	některý	k3yIgInPc2	některý
celorepublikových	celorepublikový	k2eAgInPc2d1	celorepublikový
útvarů	útvar	k1gInPc2	útvar
<g/>
;	;	kIx,	;
Kriminalistický	kriminalistický	k2eAgInSc1d1	kriminalistický
ústav	ústav	k1gInSc1	ústav
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc4	fotografia
Kriminalistický	kriminalistický	k2eAgInSc1d1	kriminalistický
ústav	ústav	k1gInSc1	ústav
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Prosinec	prosinec	k1gInSc1	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Orgány	orgán	k1gInPc1	orgán
činné	činný	k2eAgInPc1d1	činný
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
</s>
</p>
<p>
<s>
Policejní	policejní	k2eAgInSc1d1	policejní
orgán	orgán	k1gInSc1	orgán
</s>
</p>
<p>
<s>
Hodnosti	hodnost	k1gFnPc1	hodnost
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Obecní	obecní	k2eAgFnSc1d1	obecní
policie	policie	k1gFnSc1	policie
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
Policii	policie	k1gFnSc4	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
útvarech	útvar	k1gInPc6	útvar
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc6d1	ostatní
složkách	složka	k1gFnPc6	složka
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
server	server	k1gInSc1	server
o	o	k7c6	o
Policii	policie	k1gFnSc6	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnPc4d1	pracovní
činnosti	činnost	k1gFnPc4	činnost
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
