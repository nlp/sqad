<s>
Bovinní	bovinní	k2eAgFnSc1d1	bovinní
virová	virový	k2eAgFnSc1d1	virová
diarea	diarea	k1gFnSc1	diarea
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
BVD	BVD	kA	BVD
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
též	též	k9	též
jako	jako	k9	jako
BVD-MD	BVD-MD	k1gFnSc4	BVD-MD
=	=	kIx~	=
Bovine	Bovin	k1gMnSc5	Bovin
virus	virus	k1gInSc1	virus
diarrhoea-Mucosal	diarrhoea-Mucosat	k5eAaImAgInS	diarrhoea-Mucosat
disease	disease	k6eAd1	disease
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgMnPc2d1	další
zejména	zejména	k6eAd1	zejména
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
světadílech	světadíl	k1gInPc6	světadíl
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
většinou	většinou	k6eAd1	většinou
enzooticky	enzooticky	k6eAd1	enzooticky
a	a	k8xC	a
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
průjmem	průjem	k1gInSc7	průjem
<g/>
,	,	kIx,	,
potraty	potrat	k1gInPc4	potrat
<g/>
,	,	kIx,	,
porody	porod	k1gInPc4	porod
telat	tele	k1gNnPc2	tele
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
životaschopností	životaschopnost	k1gFnSc7	životaschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Zhoubná	zhoubný	k2eAgFnSc1d1	zhoubná
forma	forma	k1gFnSc1	forma
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
slizniční	slizniční	k2eAgFnSc1d1	slizniční
choroba	choroba	k1gFnSc1	choroba
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
četnými	četný	k2eAgFnPc7d1	četná
erozemi	eroze	k1gFnPc7	eroze
na	na	k7c6	na
sliznicích	sliznice	k1gFnPc6	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc4d1	ústní
<g/>
,	,	kIx,	,
zákalem	zákal	k1gInSc7	zákal
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
,	,	kIx,	,
krvavým	krvavý	k2eAgInSc7d1	krvavý
průjmem	průjem	k1gInSc7	průjem
a	a	k8xC	a
úhynem	úhyn	k1gInSc7	úhyn
i	i	k8xC	i
dospělého	dospělý	k2eAgInSc2d1	dospělý
skotu	skot	k1gInSc2	skot
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
nakažené	nakažený	k2eAgNnSc1d1	nakažené
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
sekundárně	sekundárně	k6eAd1	sekundárně
pak	pak	k6eAd1	pak
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
činí	činit	k5eAaImIp3nS	činit
2-14	[number]	k4	2-14
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgMnSc1d1	akutní
až	až	k8xS	až
chronický	chronický	k2eAgInSc1d1	chronický
<g/>
.	.	kIx.	.
</s>
<s>
Zkratky	zkratka	k1gFnPc1	zkratka
BVD	BVD	kA	BVD
a	a	k8xC	a
MD	MD	kA	MD
v	v	k7c6	v
názvu	název	k1gInSc6	název
nemoci	nemoc	k1gFnSc2	nemoc
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
:	:	kIx,	:
BVD	BVD	kA	BVD
-	-	kIx~	-
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
hypersalivace	hypersalivace	k1gFnSc1	hypersalivace
<g/>
,	,	kIx,	,
hlenohnisavý	hlenohnisavý	k2eAgInSc1d1	hlenohnisavý
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
útržky	útržek	k1gInPc7	útržek
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
erozena	erozen	k2eAgFnSc1d1	erozen
mulci	mulec	k1gInPc7	mulec
a	a	k8xC	a
sliznici	sliznice	k1gFnSc4	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc4d1	ústní
<g/>
,	,	kIx,	,
u	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
imunodeficience	imunodeficience	k1gFnSc2	imunodeficience
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
potraty	potrat	k1gInPc1	potrat
MD	MD	kA	MD
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
výraznější	výrazný	k2eAgFnPc1d2	výraznější
změny	změna	k1gFnPc1	změna
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
dut	dut	k0	dut
<g/>
.	.	kIx.	.
ústní	ústní	k2eAgInSc4d1	ústní
než	než	k8xS	než
u	u	k7c2	u
BDV	BDV	kA	BDV
+	+	kIx~	+
zákal	zákal	k1gInSc1	zákal
rohovky	rohovka	k1gFnSc2	rohovka
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
<g/>
,	,	kIx,	,
výplach	výplach	k1gInSc1	výplach
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
-	-	kIx~	-
desinfekční	desinfekční	k2eAgFnSc2d1	desinfekční
a	a	k8xC	a
adstringenční	adstringenční	k2eAgFnSc2d1	adstringenční
<g/>
,	,	kIx,	,
při	při	k7c6	při
horečce	horečka	k1gFnSc6	horečka
antipyretika	antipyretikum	k1gNnSc2	antipyretikum
<g/>
,	,	kIx,	,
obstipantia	obstipantium	k1gNnSc2	obstipantium
<g/>
,	,	kIx,	,
dieta	dieta	k1gFnSc1	dieta
<g/>
,	,	kIx,	,
infuze	infuze	k1gFnSc1	infuze
<g/>
,	,	kIx,	,
antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
ŠTĚRBA	štěrba	k1gFnSc1	štěrba
O.	O.	kA	O.
Virové	virový	k2eAgFnSc2d1	virová
choroby	choroba	k1gFnSc2	choroba
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
epizootologie	epizootologie	k1gFnSc2	epizootologie
<g/>
,	,	kIx,	,
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
</s>
