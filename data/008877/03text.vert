<p>
<s>
Chryzopras	chryzopras	k1gInSc1	chryzopras
(	(	kIx(	(
<g/>
též	též	k9	též
chrysopras	chrysopras	k1gInSc1	chrysopras
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chybně	chybně	k6eAd1	chybně
chryzopas	chryzopasa	k1gFnPc2	chryzopasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zeleně	zeleně	k6eAd1	zeleně
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
průsvitná	průsvitný	k2eAgFnSc1d1	průsvitná
odrůda	odrůda	k1gFnSc1	odrůda
minerálu	minerál	k1gInSc2	minerál
chalcedonu	chalcedon	k1gInSc2	chalcedon
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
odrůd	odrůda	k1gFnPc2	odrůda
chalcedonu	chalcedon	k1gInSc2	chalcedon
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
ceněn	ceněn	k2eAgInSc1d1	ceněn
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
chrysos	chrysos	k1gInSc1	chrysos
=	=	kIx~	=
zlatý	zlatý	k2eAgInSc1d1	zlatý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
odrůdy	odrůda	k1gFnPc4	odrůda
chalcedonu	chalcedon	k1gInSc2	chalcedon
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
chryzopras	chryzopras	k1gInSc1	chryzopras
poblíž	poblíž	k7c2	poblíž
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
za	za	k7c2	za
relativně	relativně	k6eAd1	relativně
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
asi	asi	k9	asi
120	[number]	k4	120
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc4d1	typická
jablečně	jablečně	k6eAd1	jablečně
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
žlutozelené	žlutozelený	k2eAgFnSc2d1	žlutozelená
po	po	k7c4	po
trávově	trávově	k6eAd1	trávově
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
)	)	kIx)	)
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vodnaté	vodnatý	k2eAgInPc1d1	vodnatý
křemičitany	křemičitan	k1gInPc1	křemičitan
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
oxidy	oxid	k1gInPc1	oxid
<g/>
)	)	kIx)	)
niklu	nikl	k1gInSc2	nikl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kerolit	kerolit	k5eAaPmF	kerolit
<g/>
,	,	kIx,	,
pimelit	pimelit	k5eAaImF	pimelit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
slunečním	sluneční	k2eAgNnSc6d1	sluneční
světle	světlo	k1gNnSc6	světlo
blednout	blednout	k5eAaImF	blednout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Zeleně	zeleně	k6eAd1	zeleně
zbarvené	zbarvený	k2eAgInPc1d1	zbarvený
kameny	kámen	k1gInPc1	kámen
patřily	patřit	k5eAaImAgInP	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
a	a	k8xC	a
nejoblíbenějším	oblíbený	k2eAgFnPc3d3	nejoblíbenější
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Chryzopras	chryzopras	k1gInSc1	chryzopras
byl	být	k5eAaImAgInS	být
používaný	používaný	k2eAgInSc1d1	používaný
již	již	k6eAd1	již
Řeky	Řek	k1gMnPc4	Řek
a	a	k8xC	a
Římany	Říman	k1gMnPc4	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgFnSc2d1	výrazná
obliby	obliba	k1gFnSc2	obliba
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
obzvlášť	obzvlášť	k6eAd1	obzvlášť
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jím	on	k3xPp3gInSc7	on
nechal	nechat	k5eAaPmAgMnS	nechat
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
vyzdobit	vyzdobit	k5eAaPmF	vyzdobit
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
kapli	kaple	k1gFnSc4	kaple
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
a	a	k8xC	a
také	také	k9	také
kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc1d2	novější
je	být	k5eAaImIp3nS	být
výzdoba	výzdoba	k1gFnSc1	výzdoba
zámku	zámek	k1gInSc2	zámek
Sanssouci	Sanssouci	k1gNnSc2	Sanssouci
v	v	k7c6	v
PostupimiPoužívá	PostupimiPoužívá	k1gFnSc6	PostupimiPoužívá
se	se	k3xPyFc4	se
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
do	do	k7c2	do
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
náušnic	náušnice	k1gFnPc2	náušnice
a	a	k8xC	a
broží	brož	k1gFnPc2	brož
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
menších	malý	k2eAgInPc2d2	menší
dekroativních	dekroativní	k2eAgInPc2d1	dekroativní
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
těžítek	těžítko	k1gNnPc2	těžítko
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
tamblováním	tamblování	k1gNnSc7	tamblování
(	(	kIx(	(
<g/>
lesklé	lesklý	k2eAgInPc1d1	lesklý
valounky	valounek	k1gInPc1	valounek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
se	se	k3xPyFc4	se
na	na	k7c4	na
korálky	korálek	k1gInPc4	korálek
<g/>
,	,	kIx,	,
čočkovce	čočkovec	k1gInPc4	čočkovec
a	a	k8xC	a
kameje	kamea	k1gFnPc4	kamea
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kámen	kámen	k1gInSc1	kámen
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
bývá	bývat	k5eAaImIp3nS	bývat
doporučován	doporučovat	k5eAaImNgInS	doporučovat
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
narozené	narozený	k2eAgFnPc4d1	narozená
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
raka	rak	k1gMnSc2	rak
<g/>
,	,	kIx,	,
štíra	štír	k1gMnSc2	štír
<g/>
,	,	kIx,	,
střelce	střelec	k1gMnSc2	střelec
a	a	k8xC	a
kozoroha	kozoroh	k1gMnSc2	kozoroh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
pěkný	pěkný	k2eAgInSc1d1	pěkný
šperkařský	šperkařský	k2eAgInSc1d1	šperkařský
materiál	materiál	k1gInSc1	materiál
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
dolů	dol	k1gInPc2	dol
polského	polský	k2eAgNnSc2d1	polské
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
za	za	k7c4	za
kvalitnější	kvalitní	k2eAgInSc4d2	kvalitnější
považován	považován	k2eAgInSc4d1	považován
kámen	kámen	k1gInSc4	kámen
z	z	k7c2	z
Queenslandu	Queensland	k1gInSc2	Queensland
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lokality	lokalita	k1gFnSc2	lokalita
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Chryzopras	chryzopras	k1gInSc1	chryzopras
uvádí	uvádět	k5eAaImIp3nS	uvádět
Oswald	Oswald	k1gInSc4	Oswald
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Křemže	Křemž	k1gFnSc2	Křemž
a	a	k8xC	a
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ornici	ornice	k1gFnSc6	ornice
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
modrozelených	modrozelený	k2eAgFnPc2d1	modrozelená
<g/>
,	,	kIx,	,
žlutozelených	žlutozelený	k2eAgFnPc2d1	žlutozelená
a	a	k8xC	a
vzácněji	vzácně	k6eAd2	vzácně
jablečně	jablečně	k6eAd1	jablečně
zelených	zelený	k2eAgInPc6d1	zelený
hlízovitých	hlízovitý	k2eAgInPc6d1	hlízovitý
útvarech	útvar	k1gInPc6	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
druhotně	druhotně	k6eAd1	druhotně
rozkladem	rozklad	k1gInSc7	rozklad
hadců	hadec	k1gInPc2	hadec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
složení	složení	k1gNnSc1	složení
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
88,75	[number]	k4	88,75
%	%	kIx~	%
SiO	SiO	k1gFnSc2	SiO
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
1,48	[number]	k4	1,48
%	%	kIx~	%
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
0,95	[number]	k4	0,95
%	%	kIx~	%
FeO	FeO	k1gFnSc1	FeO
</s>
</p>
<p>
<s>
0,92	[number]	k4	0,92
%	%	kIx~	%
MgO	MgO	k1gFnSc1	MgO
</s>
</p>
<p>
<s>
0,44	[number]	k4	0,44
%	%	kIx~	%
CaO	CaO	k1gFnSc1	CaO
</s>
</p>
<p>
<s>
0,37	[number]	k4	0,37
%	%	kIx~	%
MnO	MnO	k1gFnSc1	MnO
</s>
</p>
<p>
<s>
---------	---------	k?	---------
</s>
</p>
<p>
<s>
92,91	[number]	k4	92,91
%	%	kIx~	%
</s>
</p>
<p>
<s>
7,09	[number]	k4	7,09
%	%	kIx~	%
ztráta	ztráta	k1gFnSc1	ztráta
žíháním	žíhání	k1gNnSc7	žíhání
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
---------	---------	k?	---------
</s>
</p>
<p>
<s>
100	[number]	k4	100
%	%	kIx~	%
<g/>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
zbarvení	zbarvení	k1gNnSc1	zbarvení
přičítá	přičítat	k5eAaImIp3nS	přičítat
Oswald	Oswald	k1gInSc4	Oswald
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
žlutavé	žlutavý	k2eAgFnSc2d1	žlutavá
arzenu	arzen	k1gInSc6	arzen
As	as	k1gNnSc1	as
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ještě	ještě	k6eAd1	ještě
následující	následující	k2eAgFnPc4d1	následující
lokality	lokalita	k1gFnPc4	lokalita
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čistá	čistá	k1gFnSc1	čistá
u	u	k7c2	u
Horek	horka	k1gFnPc2	horka
</s>
</p>
<p>
<s>
Drahotín	Drahotín	k1gMnSc1	Drahotín
</s>
</p>
<p>
<s>
Jizera	Jizera	k1gFnSc1	Jizera
</s>
</p>
<p>
<s>
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
louky	louka	k1gFnPc1	louka
</s>
</p>
<p>
<s>
Kořenov	Kořenov	k1gInSc1	Kořenov
</s>
</p>
<p>
<s>
Kozákov	Kozákov	k1gInSc1	Kozákov
</s>
</p>
<p>
<s>
Levínská	Levínský	k2eAgFnSc1d1	Levínská
Olešnice	Olešnice	k1gFnSc1	Olešnice
</s>
</p>
<p>
<s>
Liběchov	Liběchov	k1gInSc1	Liběchov
</s>
</p>
<p>
<s>
Mumlava	Mumlava	k1gFnSc1	Mumlava
</s>
</p>
<p>
<s>
Rovné	rovný	k2eAgFnPc1d1	rovná
(	(	kIx(	(
<g/>
Podm	Podm	k1gInSc1	Podm
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třebovle	Třebovle	k6eAd1	Třebovle
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
naleziště	naleziště	k1gNnSc1	naleziště
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zabkovic	Zabkovice	k1gFnPc2	Zabkovice
(	(	kIx(	(
<g/>
Ząbkowice	Ząbkowice	k1gFnSc1	Ząbkowice
Śląskie	Śląskie	k1gFnSc1	Śląskie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Frankenstein	Frankenstein	k2eAgMnSc1d1	Frankenstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Szkłary	Szkłara	k1gFnSc2	Szkłara
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kozmice	Kozmika	k1gFnSc3	Kozmika
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Koźmice	Koźmika	k1gFnSc3	Koźmika
Wielkie	Wielkie	k1gFnSc2	Wielkie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
:	:	kIx,	:
Sarakul	Sarakul	k1gInSc1	Sarakul
–	–	k?	–
Baldy	balda	k1gFnSc2	balda
</s>
</p>
<p>
<s>
Ural	Ural	k1gInSc1	Ural
</s>
</p>
<p>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
:	:	kIx,	:
Tulare	Tular	k1gMnSc5	Tular
County	Count	k1gMnPc7	Count
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
:	:	kIx,	:
Goiás	Goiás	k1gInSc1	Goiás
</s>
</p>
<p>
<s>
Queensland	Queensland	k1gInSc1	Queensland
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc1d1	podobný
nerosty	nerost	k1gInPc1	nerost
==	==	k?	==
</s>
</p>
<p>
<s>
prasem	prasem	k1gInSc1	prasem
–	–	k?	–
sytě	sytě	k6eAd1	sytě
tmavozeleně	tmavozeleně	k6eAd1	tmavozeleně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
křemen	křemen	k1gInSc4	křemen
či	či	k8xC	či
směs	směs	k1gFnSc4	směs
křemene	křemen	k1gInSc2	křemen
a	a	k8xC	a
chalcedonu	chalcedon	k1gInSc2	chalcedon
</s>
</p>
<p>
<s>
plazma	plazma	k1gFnSc1	plazma
–	–	k?	–
tmavozelené	tmavozelený	k2eAgNnSc4d1	tmavozelené
zbarvení	zbarvení	k1gNnSc4	zbarvení
dané	daný	k2eAgInPc1d1	daný
chloritickými	chloritický	k2eAgFnPc7d1	chloritický
příměsemi	příměse	k1gFnPc7	příměse
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozkladem	rozklad	k1gInSc7	rozklad
hadců	hadec	k1gInPc2	hadec
</s>
</p>
<p>
<s>
heliotrop	heliotrop	k1gInSc1	heliotrop
–	–	k?	–
tmavozelený	tmavozelený	k2eAgInSc1d1	tmavozelený
s	s	k7c7	s
červenými	červený	k2eAgFnPc7d1	červená
či	či	k8xC	či
hnědými	hnědý	k2eAgFnPc7d1	hnědá
tečkami	tečka	k1gFnPc7	tečka
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chalcedon	chalcedon	k1gInSc1	chalcedon
</s>
</p>
<p>
<s>
Polodrahokam	polodrahokam	k1gInSc1	polodrahokam
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chryzopras	chryzopras	k1gInSc1	chryzopras
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
