<s>
Glyceroltrinitrát	Glyceroltrinitrát	k1gInSc1	Glyceroltrinitrát
(	(	kIx(	(
<g/>
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
(	(	kIx(	(
<g/>
tri	tri	k?	tri
<g/>
)	)	kIx)	)
<g/>
nitrát	nitrát	k1gInSc1	nitrát
glycerolu	glycerol	k1gInSc2	glycerol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
trojnásobný	trojnásobný	k2eAgInSc1d1	trojnásobný
ester	ester	k1gInSc1	ester
alkoholu	alkohol	k1gInSc2	alkohol
glycerolu	glycerol	k1gInSc2	glycerol
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
až	až	k8xS	až
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
explozivně	explozivně	k6eAd1	explozivně
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c4	za
uvolnění	uvolnění	k1gNnSc4	uvolnění
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
ho	on	k3xPp3gNnSc4	on
italský	italský	k2eAgMnSc1d1	italský
chemik	chemik	k1gMnSc1	chemik
Ascanio	Ascanio	k6eAd1	Ascanio
Sobrero	Sobrero	k1gNnSc1	Sobrero
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
základní	základní	k2eAgFnSc1d1	základní
složka	složka	k1gFnSc1	složka
dynamitu	dynamit	k1gInSc2	dynamit
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
sloučenin	sloučenina	k1gFnPc2	sloučenina
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
trhavin	trhavina	k1gFnPc2	trhavina
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
častou	častý	k2eAgFnSc7d1	častá
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
různých	různý	k2eAgFnPc2d1	různá
plastických	plastický	k2eAgFnPc2d1	plastická
trhavin	trhavina	k1gFnPc2	trhavina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
nalezl	naleznout	k5eAaPmAgInS	naleznout
nitroglycerin	nitroglycerin	k1gInSc4	nitroglycerin
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
pro	pro	k7c4	pro
roztažení	roztažení	k1gNnSc4	roztažení
věnčitých	věnčitý	k2eAgFnPc2d1	věnčitá
(	(	kIx(	(
koronárních	koronární	k2eAgFnPc2d1	koronární
<g/>
)	)	kIx)	)
tepen	tepna	k1gFnPc2	tepna
při	při	k7c6	při
záchvatech	záchvat	k1gInPc6	záchvat
anginy	angina	k1gFnSc2	angina
pectoris	pectoris	k1gFnSc2	pectoris
a	a	k8xC	a
pro	pro	k7c4	pro
snižování	snižování	k1gNnSc4	snižování
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
nitroglycerin	nitroglycerin	k1gInSc4	nitroglycerin
přesným	přesný	k2eAgInSc7d1	přesný
názvem	název	k1gInSc7	název
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
-tri-nitro-oxy-propan	riitroxyropana	k1gFnPc2	-tri-nitro-oxy-propana
<g/>
,	,	kIx,	,
správně	správně	k6eAd1	správně
též	též	k9	též
glyceroltrinitrát	glyceroltrinitrát	k1gInSc1	glyceroltrinitrát
o	o	k7c6	o
sumárním	sumární	k2eAgInSc6d1	sumární
vzorci	vzorec	k1gInSc6	vzorec
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
N	N	kA	N
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
227,087	[number]	k4	227,087
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc1d1	čistý
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
technický	technický	k2eAgInSc1d1	technický
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
mírně	mírně	k6eAd1	mírně
nažloutlé	nažloutlý	k2eAgFnPc4d1	nažloutlá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
13,2	[number]	k4	13,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
50	[number]	k4	50
-	-	kIx~	-
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
dochází	docházet	k5eAaImIp3nS	docházet
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
k	k	k7c3	k
rozkladu	rozklad	k1gInSc2	rozklad
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
při	při	k7c6	při
sníženém	snížený	k2eAgInSc6d1	snížený
tlaku	tlak	k1gInSc6	tlak
(	(	kIx(	(
<g/>
cca	cca	kA	cca
70	[number]	k4	70
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
uváděn	uvádět	k5eAaImNgInS	uvádět
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
180	[number]	k4	180
°	°	k?	°
<g/>
C.	C.	kA	C.
Hustota	hustota	k1gFnSc1	hustota
kapalného	kapalný	k2eAgInSc2d1	kapalný
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1,60	[number]	k4	1,60
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
hustotu	hustota	k1gFnSc4	hustota
1,735	[number]	k4	1,735
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
však	však	k9	však
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
běžných	běžný	k2eAgNnPc2d1	běžné
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
od	od	k7c2	od
alkoholů	alkohol	k1gInPc2	alkohol
po	po	k7c4	po
nepolární	polární	k2eNgInPc4d1	nepolární
alifatické	alifatický	k2eAgInPc4d1	alifatický
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
i	i	k9	i
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgNnSc1d1	sírové
<g/>
,	,	kIx,	,
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kyselina	kyselina	k1gFnSc1	kyselina
však	však	k9	však
působí	působit	k5eAaImIp3nS	působit
jeho	jeho	k3xOp3gFnSc4	jeho
pomalou	pomalý	k2eAgFnSc4d1	pomalá
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
pyrotechnické	pyrotechnický	k2eAgInPc4d1	pyrotechnický
účely	účel	k1gInPc4	účel
značně	značně	k6eAd1	značně
rizikový	rizikový	k2eAgMnSc1d1	rizikový
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
stabilita	stabilita	k1gFnSc1	stabilita
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
vůči	vůči	k7c3	vůči
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
samovolně	samovolně	k6eAd1	samovolně
explodovat	explodovat	k5eAaBmF	explodovat
působením	působení	k1gNnSc7	působení
i	i	k8xC	i
poměrně	poměrně	k6eAd1	poměrně
slabých	slabý	k2eAgInPc2d1	slabý
mechanických	mechanický	k2eAgInPc2d1	mechanický
i	i	k8xC	i
termických	termický	k2eAgInPc2d1	termický
podnětů	podnět	k1gInPc2	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
transport	transport	k1gInSc1	transport
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
poměrně	poměrně	k6eAd1	poměrně
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
veřejných	veřejný	k2eAgFnPc6d1	veřejná
komunikacích	komunikace	k1gFnPc6	komunikace
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
rozmachu	rozmach	k1gInSc3	rozmach
pyrotechnického	pyrotechnický	k2eAgNnSc2d1	pyrotechnické
využití	využití	k1gNnSc2	využití
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
dynamitu	dynamit	k1gInSc2	dynamit
<g/>
,	,	kIx,	,
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
kombinovat	kombinovat	k5eAaImF	kombinovat
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
explozivní	explozivní	k2eAgFnPc4d1	explozivní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
a	a	k8xC	a
transportní	transportní	k2eAgFnSc7d1	transportní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
tyto	tento	k3xDgFnPc4	tento
základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Energie	energie	k1gFnSc1	energie
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
:	:	kIx,	:
6	[number]	k4	6
060	[number]	k4	060
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
1	[number]	k4	1
450	[number]	k4	450
kcal	kcala	k1gFnPc2	kcala
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
Detonační	detonační	k2eAgFnSc4d1	detonační
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
:	:	kIx,	:
7	[number]	k4	7
700	[number]	k4	700
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
pro	pro	k7c4	pro
kapalinu	kapalina	k1gFnSc4	kapalina
a	a	k8xC	a
až	až	k9	až
9	[number]	k4	9
000	[number]	k4	000
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
pro	pro	k7c4	pro
pevný	pevný	k2eAgInSc4d1	pevný
nitroglycerin	nitroglycerin	k1gInSc4	nitroglycerin
Objem	objem	k1gInSc1	objem
spalných	spalný	k2eAgInPc2d1	spalný
plynů	plyn	k1gInPc2	plyn
<g/>
:	:	kIx,	:
715	[number]	k4	715
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
Teplota	teplota	k1gFnSc1	teplota
exploze	exploze	k1gFnSc1	exploze
<g/>
:	:	kIx,	:
3	[number]	k4	3
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
Nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
značně	značně	k6eAd1	značně
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
již	již	k6eAd1	již
dávky	dávka	k1gFnPc1	dávka
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
miligramů	miligram	k1gInPc2	miligram
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
smrt	smrt	k1gFnSc4	smrt
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
účinkem	účinek	k1gInSc7	účinek
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organizmus	organizmus	k1gInSc4	organizmus
je	být	k5eAaImIp3nS	být
rychlé	rychlý	k2eAgNnSc4d1	rychlé
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
tepen	tepna	k1gFnPc2	tepna
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
pokles	pokles	k1gInSc1	pokles
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
otrava	otrava	k1gFnSc1	otrava
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
bolestmi	bolest	k1gFnPc7	bolest
<g/>
,	,	kIx,	,
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
zmodráním	zmodrání	k1gNnSc7	zmodrání
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
zraku	zrak	k1gInSc2	zrak
a	a	k8xC	a
otoky	otok	k1gInPc1	otok
<g/>
.	.	kIx.	.
</s>
<s>
Postiženého	postižený	k1gMnSc4	postižený
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přemístit	přemístit	k5eAaPmF	přemístit
na	na	k7c4	na
čerstvý	čerstvý	k2eAgInSc4d1	čerstvý
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
podávat	podávat	k5eAaImF	podávat
silnou	silný	k2eAgFnSc4d1	silná
kávu	káva	k1gFnSc4	káva
nebo	nebo	k8xC	nebo
léky	lék	k1gInPc4	lék
proti	proti	k7c3	proti
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organizmus	organizmus	k1gInSc4	organizmus
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
individuální	individuální	k2eAgMnSc1d1	individuální
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
dlouhodobějším	dlouhodobý	k2eAgInSc6d2	dlouhodobější
styku	styk	k1gInSc6	styk
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
látkou	látka	k1gFnSc7	látka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přivykání	přivykání	k1gNnSc3	přivykání
na	na	k7c4	na
zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
dávky	dávka	k1gFnPc4	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
nitroglycerinem	nitroglycerin	k1gInSc7	nitroglycerin
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
chronické	chronický	k2eAgFnSc2d1	chronická
otravy	otrava	k1gFnSc2	otrava
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
z	z	k7c2	z
toxikologického	toxikologický	k2eAgNnSc2d1	Toxikologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
jed	jed	k1gInSc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnSc3	schopnost
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
rychle	rychle	k6eAd1	rychle
cévy	céva	k1gFnPc4	céva
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
snížení	snížení	k1gNnSc3	snížení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
chorobami	choroba	k1gFnPc7	choroba
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
vysokým	vysoký	k2eAgInSc7d1	vysoký
krevním	krevní	k2eAgInSc7d1	krevní
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zpozorování	zpozorování	k1gNnSc6	zpozorování
nástupu	nástup	k1gInSc2	nástup
srdečních	srdeční	k2eAgInPc2d1	srdeční
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
pacient	pacient	k1gMnSc1	pacient
zasunout	zasunout	k5eAaPmF	zasunout
tabletku	tabletka	k1gFnSc4	tabletka
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
0,1	[number]	k4	0,1
mg	mg	kA	mg
<g/>
)	)	kIx)	)
pod	pod	k7c4	pod
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
ji	on	k3xPp3gFnSc4	on
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
preparáty	preparát	k1gInPc1	preparát
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
podáván	podáván	k2eAgInSc1d1	podáván
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
1	[number]	k4	1
<g/>
%	%	kIx~	%
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
dávka	dávka	k1gFnSc1	dávka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nemá	mít	k5eNaImIp3nS	mít
překročit	překročit	k5eAaPmF	překročit
několik	několik	k4yIc4	několik
kapek	kapka	k1gFnPc2	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
se	se	k3xPyFc4	se
též	též	k9	též
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
diagnostice	diagnostika	k1gFnSc3	diagnostika
vasovagalnich	vasovagalnicha	k1gFnPc2	vasovagalnicha
synkop	synkopa	k1gFnPc2	synkopa
při	při	k7c6	při
HUT	HUT	kA	HUT
testu	test	k1gInSc6	test
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
masového	masový	k2eAgNnSc2d1	masové
rozšíření	rozšíření	k1gNnSc2	rozšíření
výroby	výroba	k1gFnSc2	výroba
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
preparace	preparace	k1gFnSc1	preparace
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
poměrně	poměrně	k6eAd1	poměrně
mírných	mírný	k2eAgNnPc2d1	mírné
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
kontrolovatelných	kontrolovatelný	k2eAgFnPc2d1	kontrolovatelná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
žádná	žádný	k3yNgFnSc1	žádný
vysoce	vysoce	k6eAd1	vysoce
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
drahá	drahý	k2eAgNnPc1d1	drahé
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
vstupní	vstupní	k2eAgFnPc1d1	vstupní
suroviny	surovina	k1gFnPc1	surovina
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
dostupné	dostupný	k2eAgFnPc1d1	dostupná
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
levné	levný	k2eAgNnSc1d1	levné
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
reakcí	reakce	k1gFnSc7	reakce
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nitrace	nitrace	k1gFnSc1	nitrace
trojsytného	trojsytný	k2eAgInSc2d1	trojsytný
alkoholu	alkohol	k1gInSc2	alkohol
glycerolu	glycerol	k1gInSc2	glycerol
působením	působení	k1gNnSc7	působení
směsi	směs	k1gFnSc2	směs
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
a	a	k8xC	a
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
prostředí	prostředí	k1gNnSc1	prostředí
40	[number]	k4	40
-	-	kIx~	-
60	[number]	k4	60
%	%	kIx~	%
H2SO4	H2SO4	k1gMnPc2	H2SO4
a	a	k8xC	a
30	[number]	k4	30
-	-	kIx~	-
40	[number]	k4	40
%	%	kIx~	%
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dávkují	dávkovat	k5eAaImIp3nP	dávkovat
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
glycerolu	glycerol	k1gInSc2	glycerol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nitrace	nitrace	k1gFnSc2	nitrace
toluenu	toluen	k1gInSc2	toluen
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
TNT	TNT	kA	TNT
probíhá	probíhat	k5eAaImIp3nS	probíhat
nitrace	nitrace	k1gFnPc4	nitrace
i	i	k9	i
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
menších	malý	k2eAgFnPc2d2	menší
množství	množství	k1gNnPc2	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nitrace	nitrace	k1gFnSc1	nitrace
glycerolu	glycerol	k1gInSc2	glycerol
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
značně	značně	k6eAd1	značně
exotermní	exotermní	k2eAgFnSc1d1	exotermní
(	(	kIx(	(
<g/>
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
reakčního	reakční	k2eAgNnSc2d1	reakční
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
120	[number]	k4	120
-	-	kIx~	-
170	[number]	k4	170
kcal	kcalum	k1gNnPc2	kcalum
na	na	k7c4	na
1	[number]	k4	1
kg	kg	kA	kg
glycerolu	glycerol	k1gInSc6	glycerol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
podmínkou	podmínka	k1gFnSc7	podmínka
zvládnutí	zvládnutí	k1gNnSc2	zvládnutí
reakce	reakce	k1gFnSc2	reakce
dobré	dobrý	k2eAgNnSc1d1	dobré
chlazení	chlazení	k1gNnSc1	chlazení
reakční	reakční	k2eAgFnSc2d1	reakční
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
15	[number]	k4	15
-	-	kIx~	-
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Druhým	druhý	k4xOgInSc7	druhý
nezbytným	zbytný	k2eNgInSc7d1	zbytný
požadavkem	požadavek	k1gInSc7	požadavek
je	být	k5eAaImIp3nS	být
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
promíchávání	promíchávání	k1gNnSc1	promíchávání
směsi	směs	k1gFnSc2	směs
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lokálním	lokální	k2eAgNnSc6d1	lokální
nahromadění	nahromadění	k1gNnSc6	nahromadění
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
glycerolu	glycerol	k1gInSc2	glycerol
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
bouřlivé	bouřlivý	k2eAgFnSc3d1	bouřlivá
reakci	reakce	k1gFnSc3	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
explozi	exploze	k1gFnSc4	exploze
celé	celá	k1gFnSc2	celá
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
nitrátorech	nitrátor	k1gInPc6	nitrátor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
velké	velký	k2eAgFnPc4d1	velká
nádoby	nádoba	k1gFnPc4	nádoba
vyložené	vyložený	k2eAgFnPc4d1	vyložená
olovem	olovo	k1gNnSc7	olovo
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
zhotovené	zhotovený	k2eAgInPc1d1	zhotovený
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
materiál	materiál	k1gInSc4	materiál
hovoří	hovořit	k5eAaImIp3nS	hovořit
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
olovo	olovo	k1gNnSc1	olovo
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
při	při	k7c6	při
případné	případný	k2eAgFnSc6d1	případná
explozivní	explozivní	k2eAgFnSc6d1	explozivní
havárii	havárie	k1gFnSc6	havárie
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
rozlétávání	rozlétávání	k1gNnSc3	rozlétávání
ostrých	ostrý	k2eAgInPc2d1	ostrý
kovových	kovový	k2eAgInPc2d1	kovový
úlomků	úlomek	k1gInPc2	úlomek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
pasivováno	pasivovat	k5eAaBmNgNnS	pasivovat
přítomnou	přítomný	k2eAgFnSc7d1	přítomná
H2SO4	H2SO4	k1gFnSc7	H2SO4
a	a	k8xC	a
stěny	stěn	k1gInPc1	stěn
reakční	reakční	k2eAgFnSc2d1	reakční
nádoby	nádoba	k1gFnSc2	nádoba
nejsou	být	k5eNaImIp3nP	být
směsí	směs	k1gFnSc7	směs
kyselin	kyselina	k1gFnPc2	kyselina
naleptávány	naleptávat	k5eAaImNgInP	naleptávat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
každý	každý	k3xTgInSc1	každý
nitrátor	nitrátor	k1gInSc1	nitrátor
vybaven	vybavit	k5eAaPmNgInS	vybavit
na	na	k7c6	na
dně	dna	k1gFnSc6	dna
velkým	velký	k2eAgInSc7d1	velký
výpustním	výpustní	k2eAgInSc7d1	výpustní
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
nádrže	nádrž	k1gFnSc2	nádrž
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnPc4	směs
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
prudké	prudký	k2eAgFnSc3d1	prudká
akceleraci	akcelerace	k1gFnSc3	akcelerace
nitrace	nitrace	k1gFnSc2	nitrace
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgNnSc1d1	provázené
silným	silný	k2eAgInSc7d1	silný
vývojem	vývoj	k1gInSc7	vývoj
hnědých	hnědý	k2eAgInPc2d1	hnědý
nitrózních	nitrózní	k2eAgInPc2d1	nitrózní
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
bezprostřední	bezprostřední	k2eAgNnSc1d1	bezprostřední
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
obsluha	obsluha	k1gFnSc1	obsluha
možnost	možnost	k1gFnSc4	možnost
celý	celý	k2eAgInSc4d1	celý
obsah	obsah	k1gInSc4	obsah
nitrátoru	nitrátor	k1gInSc2	nitrátor
vypustit	vypustit	k5eAaPmF	vypustit
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
do	do	k7c2	do
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
nitrace	nitrace	k1gFnSc2	nitrace
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
20	[number]	k4	20
-	-	kIx~	-
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
přidá	přidat	k5eAaPmIp3nS	přidat
další	další	k2eAgFnSc1d1	další
vychlazená	vychlazený	k2eAgFnSc1d1	vychlazená
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
oddělí	oddělit	k5eAaPmIp3nS	oddělit
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
promývá	promývat	k5eAaImIp3nS	promývat
roztokem	roztok	k1gInSc7	roztok
sody	soda	k1gFnSc2	soda
(	(	kIx(	(
<g/>
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
odstranily	odstranit	k5eAaPmAgInP	odstranit
zbytky	zbytek	k1gInPc1	zbytek
nitračních	nitrační	k2eAgFnPc2d1	nitrační
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
