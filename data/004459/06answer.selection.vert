<s>
Sídlo	sídlo	k1gNnSc1	sídlo
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
největší	veliký	k2eAgInSc4d3	veliký
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
v	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
a	a	k8xC	a
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
