<p>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
je	být	k5eAaImIp3nS	být
příkrý	příkrý	k2eAgInSc1d1	příkrý
až	až	k8xS	až
svislý	svislý	k2eAgInSc1d1	svislý
stupeň	stupeň	k1gInSc1	stupeň
v	v	k7c6	v
říčním	říční	k2eAgNnSc6d1	říční
korytě	koryto	k1gNnSc6	koryto
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
přepadá	přepadat	k5eAaImIp3nS	přepadat
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
vzniku	vznik	k1gInSc2	vznik
vodopádu	vodopád	k1gInSc2	vodopád
jsou	být	k5eAaImIp3nP	být
strukturně	strukturně	k6eAd1	strukturně
geologické	geologický	k2eAgFnPc1d1	geologická
(	(	kIx(	(
<g/>
odolné	odolný	k2eAgFnPc1d1	odolná
horniny	hornina	k1gFnPc1	hornina
spočívají	spočívat	k5eAaImIp3nP	spočívat
na	na	k7c4	na
měkkých	měkký	k2eAgInPc2d1	měkký
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc4	síť
puklin	puklina	k1gFnPc2	puklina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
(	(	kIx(	(
<g/>
visutá	visutý	k2eAgNnPc4d1	visuté
boční	boční	k2eAgNnPc4d1	boční
údolí	údolí	k1gNnPc4	údolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
však	však	k9	však
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
proudění	proudění	k1gNnSc1	proudění
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
kaskády	kaskáda	k1gFnPc1	kaskáda
–	–	k?	–
nízké	nízký	k2eAgInPc4d1	nízký
vodopády	vodopád	k1gInPc4	vodopád
</s>
</p>
<p>
<s>
katarakty	katarakta	k1gFnPc1	katarakta
–	–	k?	–
vodopády	vodopád	k1gInPc7	vodopád
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
přepadovým	přepadový	k2eAgInSc7d1	přepadový
lemem	lem	k1gInSc7	lem
</s>
</p>
<p>
<s>
peřeje	peřej	k1gInPc1	peřej
–	–	k?	–
nízké	nízký	k2eAgInPc1d1	nízký
vodopády	vodopád	k1gInPc1	vodopád
stupňovitě	stupňovitě	k6eAd1	stupňovitě
za	za	k7c7	za
sebou	se	k3xPyFc7	se
uspořádané	uspořádaný	k2eAgNnSc4d1	uspořádané
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vodopádem	vodopád	k1gInSc7	vodopád
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
buďto	buďto	k8xC	buďto
Salto	salto	k1gNnSc4	salto
Ángel	Ángela	k1gFnPc2	Ángela
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
(	(	kIx(	(
<g/>
979	[number]	k4	979
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vodopád	vodopád	k1gInSc1	vodopád
Tugela	Tugel	k1gMnSc2	Tugel
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
948	[number]	k4	948
m	m	kA	m
či	či	k8xC	či
983	[number]	k4	983
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
systém	systém	k1gInSc1	systém
vodopádů	vodopád	k1gInPc2	vodopád
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
přepadový	přepadový	k2eAgInSc1d1	přepadový
lem	lem	k1gInSc1	lem
a	a	k8xC	a
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
150	[number]	k4	150
až	až	k9	až
270	[number]	k4	270
samostatných	samostatný	k2eAgInPc2d1	samostatný
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
vodopády	vodopád	k1gInPc4	vodopád
Iguaçu	Iguaçus	k1gInSc2	Iguaçus
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Argentinou	Argentina	k1gFnSc7	Argentina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vodopádem	vodopád	k1gInSc7	vodopád
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
Pančavský	Pančavský	k2eAgInSc1d1	Pančavský
vodopád	vodopád	k1gInSc1	vodopád
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
148	[number]	k4	148
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vodopádů	vodopád	k1gInPc2	vodopád
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejmohutnějších	mohutný	k2eAgInPc2d3	nejmohutnější
vodopádů	vodopád	k1gInPc2	vodopád
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodopád	vodopád	k1gInSc1	vodopád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vodopád	vodopád	k1gInSc1	vodopád
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
