<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
nadnárodní	nadnárodní	k2eAgInSc4d1	nadnárodní
orgán	orgán	k1gInSc4	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
a	a	k8xC	a
hájící	hájící	k2eAgInPc1d1	hájící
zájmy	zájem	k1gInPc1	zájem
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
