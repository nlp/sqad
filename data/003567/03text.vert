<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
nadnárodní	nadnárodní	k2eAgInSc4d1	nadnárodní
orgán	orgán	k1gInSc4	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
a	a	k8xC	a
hájící	hájící	k2eAgInPc1d1	hájící
zájmy	zájem	k1gInPc1	zájem
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
jako	jako	k9	jako
kolegium	kolegium	k1gNnSc1	kolegium
komisařů	komisař	k1gMnPc2	komisař
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
kolegium	kolegium	k1gNnSc1	kolegium
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
administrativním	administrativní	k2eAgInSc7d1	administrativní
aparátem	aparát	k1gInSc7	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
takřka	takřka	k6eAd1	takřka
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
administrativní	administrativní	k2eAgInSc4d1	administrativní
a	a	k8xC	a
expertní	expertní	k2eAgInSc4d1	expertní
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
ústřední	ústřední	k2eAgFnSc7d1	ústřední
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc1	palác
Berlaymont	Berlaymonta	k1gFnPc2	Berlaymonta
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
dislokovány	dislokovat	k5eAaBmNgFnP	dislokovat
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Člení	členit	k5eAaImIp3nS	členit
se	se	k3xPyFc4	se
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
generálních	generální	k2eAgNnPc2d1	generální
ředitelství	ředitelství	k1gNnPc2	ředitelství
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
především	především	k9	především
"	"	kIx"	"
<g/>
strážkyní	strážkyně	k1gFnSc7	strážkyně
smluv	smlouva	k1gFnPc2	smlouva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
že	že	k8xS	že
dbá	dbát	k5eAaImIp3nS	dbát
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
smluv	smlouva	k1gFnPc2	smlouva
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
z	z	k7c2	z
úřední	úřední	k2eAgFnSc2d1	úřední
povinnosti	povinnost	k1gFnSc2	povinnost
podává	podávat	k5eAaImIp3nS	podávat
žaloby	žaloba	k1gFnSc2	žaloba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zjištěného	zjištěný	k2eAgNnSc2d1	zjištěné
porušení	porušení	k1gNnSc2	porušení
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
pravomocí	pravomoc	k1gFnSc7	pravomoc
je	být	k5eAaImIp3nS	být
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
předkládat	předkládat	k5eAaImF	předkládat
návrhy	návrh	k1gInPc4	návrh
legislativních	legislativní	k2eAgInPc2d1	legislativní
předpisů	předpis	k1gInPc2	předpis
má	mít	k5eAaImIp3nS	mít
výhradně	výhradně	k6eAd1	výhradně
Komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
pravomoci	pravomoc	k1gFnPc1	pravomoc
jsou	být	k5eAaImIp3nP	být
vydávání	vydávání	k1gNnSc4	vydávání
doporučení	doporučení	k1gNnPc2	doporučení
a	a	k8xC	a
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
,	,	kIx,	,
Komise	komise	k1gFnSc1	komise
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
i	i	k9	i
pravomoci	pravomoc	k1gFnPc4	pravomoc
v	v	k7c6	v
přenesené	přenesený	k2eAgFnSc6d1	přenesená
působnosti	působnost	k1gFnSc6	působnost
(	(	kIx(	(
<g/>
delegovaná	delegovaný	k2eAgFnSc1d1	delegovaná
legislativní	legislativní	k2eAgFnSc1d1	legislativní
pravomoc	pravomoc	k1gFnSc1	pravomoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
navenek	navenek	k6eAd1	navenek
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
udržování	udržování	k1gNnSc2	udržování
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
a	a	k8xC	a
sjednávání	sjednávání	k1gNnSc2	sjednávání
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
z	z	k7c2	z
převážné	převážný	k2eAgFnSc2d1	převážná
části	část	k1gFnSc2	část
rozpočet	rozpočet	k1gInSc1	rozpočet
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Kolegium	kolegium	k1gNnSc1	kolegium
komisařů	komisař	k1gMnPc2	komisař
zasedá	zasedat	k5eAaImIp3nS	zasedat
jedenkrát	jedenkrát	k6eAd1	jedenkrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
kolegium	kolegium	k1gNnSc1	kolegium
komisařů	komisař	k1gMnPc2	komisař
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
z	z	k7c2	z
komisařů	komisař	k1gMnPc2	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
navrhováni	navrhován	k2eAgMnPc1d1	navrhován
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
,	,	kIx,	,
nesmějí	smát	k5eNaImIp3nP	smát
přijímat	přijímat	k5eAaImF	přijímat
instrukce	instrukce	k1gFnPc4	instrukce
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
od	od	k7c2	od
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
prosazovat	prosazovat	k5eAaImF	prosazovat
výhradně	výhradně	k6eAd1	výhradně
zájmy	zájem	k1gInPc4	zájem
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
sedm	sedm	k4xCc1	sedm
místopředsedů	místopředseda	k1gMnPc2	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
předseda	předseda	k1gMnSc1	předseda
navržený	navržený	k2eAgInSc1d1	navržený
Evropskou	evropský	k2eAgFnSc7d1	Evropská
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
následně	následně	k6eAd1	následně
schválený	schválený	k2eAgInSc1d1	schválený
volbou	volba	k1gFnSc7	volba
Evropským	evropský	k2eAgInSc7d1	evropský
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
musí	muset	k5eAaImIp3nS	muset
získat	získat	k5eAaPmF	získat
prostou	prostý	k2eAgFnSc4d1	prostá
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
(	(	kIx(	(
<g/>
komisaři	komisar	k1gMnPc1	komisar
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
Radou	rada	k1gFnSc7	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
komisařům	komisař	k1gMnPc3	komisař
resorty	resort	k1gInPc4	resort
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
komisaři	komisar	k1gMnPc1	komisar
předstupují	předstupovat	k5eAaImIp3nP	předstupovat
před	před	k7c4	před
výbory	výbor	k1gInPc4	výbor
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
poslanci	poslanec	k1gMnPc7	poslanec
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
jejich	jejich	k3xOp3gFnSc1	jejich
kompetentnost	kompetentnost	k1gFnSc1	kompetentnost
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
komise	komise	k1gFnSc1	komise
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
schválena	schválit	k5eAaPmNgFnS	schválit
volbou	volba	k1gFnSc7	volba
Evropským	evropský	k2eAgInSc7d1	evropský
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
komisaře	komisař	k1gMnSc2	komisař
i	i	k8xC	i
předsedu	předseda	k1gMnSc4	předseda
komise	komise	k1gFnSc2	komise
interpelovat	interpelovat	k5eAaBmF	interpelovat
a	a	k8xC	a
interpelovaní	interpelovaný	k2eAgMnPc1d1	interpelovaný
mají	mít	k5eAaImIp3nP	mít
povinnost	povinnost	k1gFnSc4	povinnost
písemně	písemně	k6eAd1	písemně
nebo	nebo	k8xC	nebo
osobně	osobně	k6eAd1	osobně
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyhrazeno	vyhrazen	k2eAgNnSc1d1	vyhrazeno
každé	každý	k3xTgNnSc4	každý
úterý	úterý	k1gNnSc4	úterý
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
musí	muset	k5eAaImIp3nS	muset
parlamentu	parlament	k1gInSc2	parlament
pravidelně	pravidelně	k6eAd1	pravidelně
předkládat	předkládat	k5eAaImF	předkládat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
svojí	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
komise	komise	k1gFnSc1	komise
odvolána	odvolán	k2eAgFnSc1d1	odvolána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
Evropským	evropský	k2eAgInSc7d1	evropský
parlamentem	parlament	k1gInSc7	parlament
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
jeho	jeho	k3xOp3gMnPc2	jeho
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc4	počet
komisařů	komisař	k1gMnPc2	komisař
prodělal	prodělat	k5eAaPmAgMnS	prodělat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
několik	několik	k4yIc4	několik
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
deseti	deset	k4xCc2	deset
nových	nový	k2eAgInPc2d1	nový
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
měla	mít	k5eAaImAgFnS	mít
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
20	[number]	k4	20
komisařů	komisař	k1gMnPc2	komisař
<g/>
:	:	kIx,	:
Pět	pět	k4xCc4	pět
největších	veliký	k2eAgInPc2d3	veliký
států	stát	k1gInPc2	stát
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
-	-	kIx~	-
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
EK	EK	kA	EK
po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
komisařích	komisař	k1gMnPc6	komisař
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgFnPc2d1	zbylá
10	[number]	k4	10
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
nových	nový	k2eAgFnPc2d1	nová
zemí	zem	k1gFnPc2	zem
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
komisařů	komisař	k1gMnPc2	komisař
na	na	k7c4	na
30	[number]	k4	30
(	(	kIx(	(
<g/>
přibylo	přibýt	k5eAaPmAgNnS	přibýt
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
komisaři	komisař	k1gMnSc6	komisař
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
nové	nový	k2eAgFnSc2d1	nová
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
vlády	vláda	k1gFnPc4	vláda
nová	nový	k2eAgFnSc1d1	nová
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
komisařů	komisař	k1gMnPc2	komisař
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
25	[number]	k4	25
-	-	kIx~	-
každá	každý	k3xTgFnSc1	každý
členská	členský	k2eAgFnSc1d1	členská
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
komisaře	komisař	k1gMnSc4	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
Komise	komise	k1gFnSc1	komise
celkem	celek	k1gInSc7	celek
27	[number]	k4	27
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
po	po	k7c6	po
přistoupení	přistoupení	k1gNnSc6	přistoupení
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
čítá	čítat	k5eAaImIp3nS	čítat
Komise	komise	k1gFnSc1	komise
28	[number]	k4	28
komisařů	komisař	k1gMnPc2	komisař
(	(	kIx(	(
<g/>
po	po	k7c6	po
přistoupení	přistoupení	k1gNnSc6	přistoupení
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
Komise	komise	k1gFnSc2	komise
původně	původně	k6eAd1	původně
odpovídat	odpovídat	k5eAaImF	odpovídat
dvěma	dva	k4xCgInPc7	dva
třetinám	třetina	k1gFnPc3	třetina
počtu	počet	k1gInSc3	počet
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Komise	komise	k1gFnSc1	komise
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
počtu	počet	k1gInSc3	počet
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Komisař	komisař	k1gMnSc1	komisař
spravuje	spravovat	k5eAaImIp3nS	spravovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc4	několik
resortů	resort	k1gInPc2	resort
(	(	kIx(	(
<g/>
generálních	generální	k2eAgNnPc2d1	generální
ředitelství	ředitelství	k1gNnPc2	ředitelství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaných	označovaný	k2eAgInPc2d1	označovaný
zkratkou	zkratka	k1gFnSc7	zkratka
DG	dg	kA	dg
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
GŘ	GŘ	kA	GŘ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgNnSc1d1	generální
ředitelství	ředitelství	k1gNnSc1	ředitelství
jsou	být	k5eAaImIp3nP	být
odborné	odborný	k2eAgInPc1d1	odborný
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
unijní	unijní	k2eAgFnSc2d1	unijní
politiky	politika	k1gFnSc2	politika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
daně	daň	k1gFnPc1	daň
a	a	k8xC	a
cla	clo	k1gNnPc1	clo
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelství	ředitelství	k1gNnPc1	ředitelství
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
překladatele	překladatel	k1gMnPc4	překladatel
<g/>
,	,	kIx,	,
tlumočníky	tlumočník	k1gMnPc4	tlumočník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
odborníky	odborník	k1gMnPc4	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
odborné	odborný	k2eAgInPc4d1	odborný
posudky	posudek	k1gInPc4	posudek
a	a	k8xC	a
analýzy	analýza	k1gFnPc4	analýza
legislativních	legislativní	k2eAgInPc2d1	legislativní
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
Komise	komise	k1gFnSc1	komise
chystá	chystat	k5eAaImIp3nS	chystat
předložit	předložit	k5eAaPmF	předložit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
ředitelství	ředitelství	k1gNnSc2	ředitelství
je	být	k5eAaImIp3nS	být
i	i	k9	i
Generální	generální	k2eAgInSc1d1	generální
sekretariát	sekretariát	k1gInSc1	sekretariát
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
každodenní	každodenní	k2eAgInSc4d1	každodenní
chod	chod	k1gInSc4	chod
Komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
také	také	k9	také
kontrolu	kontrola	k1gFnSc4	kontrola
uplatňování	uplatňování	k1gNnSc2	uplatňování
evropských	evropský	k2eAgInPc2d1	evropský
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgNnSc1d1	generální
ředitelství	ředitelství	k1gNnSc1	ředitelství
mají	mít	k5eAaImIp3nP	mít
strukturu	struktura	k1gFnSc4	struktura
podobnou	podobný	k2eAgFnSc4d1	podobná
ministerstvům	ministerstvo	k1gNnPc3	ministerstvo
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nP	stát
generální	generální	k2eAgMnSc1d1	generální
ředitelé	ředitel	k1gMnPc1	ředitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
kariérními	kariérní	k2eAgMnPc7d1	kariérní
úředníky	úředník	k1gMnPc7	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Junckerova	Junckerův	k2eAgFnSc1d1	Junckerův
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Junckerova	Junckerův	k2eAgFnSc1d1	Junckerův
komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
úřadující	úřadující	k2eAgFnSc1d1	úřadující
komise	komise	k1gFnSc1	komise
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
mandát	mandát	k1gInSc1	mandát
vyprší	vypršet	k5eAaPmIp3nS	vypršet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
politik	politik	k1gMnSc1	politik
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Juncker	Juncker	k1gMnSc1	Juncker
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
zástupci	zástupce	k1gMnSc6	zástupce
vyslal	vyslat	k5eAaPmAgMnS	vyslat
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
osmi	osm	k4xCc2	osm
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Portugalce	Portugalec	k1gMnPc4	Portugalec
Josého	José	k1gNnSc2	José
Manuela	Manuela	k1gFnSc1	Manuela
Barrosa	Barrosa	k1gFnSc1	Barrosa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
předsedy	předseda	k1gMnSc2	předseda
má	mít	k5eAaImIp3nS	mít
komise	komise	k1gFnSc1	komise
sedm	sedm	k4xCc4	sedm
místopředsedů	místopředseda	k1gMnPc2	místopředseda
<g/>
.	.	kIx.	.
</s>
