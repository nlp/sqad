<p>
<s>
Izotop	izotop	k1gInSc1	izotop
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nuklid	nuklid	k1gInSc4	nuklid
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
souboru	soubor	k1gInSc2	soubor
nuklidů	nuklid	k1gInPc2	nuklid
jednoho	jeden	k4xCgInSc2	jeden
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
atomů	atom	k1gInPc2	atom
izotopů	izotop	k1gInPc2	izotop
jednoho	jeden	k4xCgInSc2	jeden
prvku	prvek	k1gInSc2	prvek
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
počet	počet	k1gInSc4	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
tedy	tedy	k9	tedy
stejné	stejný	k2eAgNnSc4d1	stejné
atomové	atomový	k2eAgNnSc4d1	atomové
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
rozdílné	rozdílný	k2eAgNnSc4d1	rozdílné
hmotové	hmotový	k2eAgNnSc4d1	hmotové
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
předpony	předpona	k1gFnSc2	předpona
iso-	iso-	k?	iso-
(	(	kIx(	(
<g/>
stejno-	stejno-	k?	stejno-
<g/>
)	)	kIx)	)
a	a	k8xC	a
topos	topos	k1gInSc1	topos
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc1	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izotopy	izotop	k1gInPc1	izotop
téhož	týž	k3xTgInSc2	týž
prvku	prvek	k1gInSc2	prvek
mají	mít	k5eAaImIp3nP	mít
prakticky	prakticky	k6eAd1	prakticky
totožné	totožný	k2eAgFnPc1d1	totožná
chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
;	;	kIx,	;
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžší	těžký	k2eAgInPc1d2	těžší
izotopy	izotop	k1gInPc1	izotop
reagují	reagovat	k5eAaBmIp3nP	reagovat
poněkud	poněkud	k6eAd1	poněkud
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgInSc1d3	nejvýraznější
u	u	k7c2	u
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
nukleonů	nukleon	k1gInPc2	nukleon
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgInSc1d1	relativní
rozdíl	rozdíl	k1gInSc1	rozdíl
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInSc1d2	menší
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
obvykle	obvykle	k6eAd1	obvykle
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
izotopů	izotop	k1gInPc2	izotop
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jejich	jejich	k3xOp3gFnSc2	jejich
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
tedy	tedy	k9	tedy
hustoty	hustota	k1gFnSc2	hustota
jejich	jejich	k3xOp3gFnPc2	jejich
sloučenin	sloučenina	k1gFnPc2	sloučenina
bývá	bývat	k5eAaImIp3nS	bývat
nejčastější	častý	k2eAgFnSc7d3	nejčastější
odlišností	odlišnost	k1gFnSc7	odlišnost
mezi	mezi	k7c7	mezi
izotopy	izotop	k1gInPc1	izotop
jejich	jejich	k3xOp3gFnSc4	jejich
stálost	stálost	k1gFnSc4	stálost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
izotopy	izotop	k1gInPc1	izotop
(	(	kIx(	(
<g/>
vzdalující	vzdalující	k2eAgNnSc4d1	vzdalující
se	se	k3xPyFc4	se
od	od	k7c2	od
ideálního	ideální	k2eAgInSc2d1	ideální
středního	střední	k2eAgInSc2d1	střední
poměru	poměr	k1gInSc2	poměr
počtu	počet	k1gInSc2	počet
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
protonů	proton	k1gInPc2	proton
na	na	k7c4	na
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
nejsou	být	k5eNaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
podléhají	podléhat	k5eAaImIp3nP	podléhat
radioaktivnímu	radioaktivní	k2eAgInSc3d1	radioaktivní
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
izotopy	izotop	k1gInPc1	izotop
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
přirozené	přirozený	k2eAgInPc1d1	přirozený
vznikající	vznikající	k2eAgInPc1d1	vznikající
jadernými	jaderný	k2eAgFnPc7d1	jaderná
reakcemi	reakce	k1gFnPc7	reakce
samovolně	samovolně	k6eAd1	samovolně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgInPc1d1	umělý
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInPc1d1	vznikající
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Odlišnosti	odlišnost	k1gFnPc1	odlišnost
lze	lze	k6eAd1	lze
také	také	k9	také
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc6	var
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
isotopy	isotop	k1gInPc7	isotop
téhož	týž	k3xTgInSc2	týž
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
H2O	H2O	k1gFnSc2	H2O
má	mít	k5eAaImIp3nS	mít
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
voda	voda	k1gFnSc1	voda
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
za	za	k7c2	za
stejných	stejný	k2eAgFnPc2d1	stejná
podmínek	podmínka	k1gFnPc2	podmínka
kvůli	kvůli	k7c3	kvůli
těžší	těžký	k2eAgFnSc3d2	těžší
molekule	molekula	k1gFnSc3	molekula
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
o	o	k7c4	o
1,5	[number]	k4	1,5
°	°	k?	°
<g/>
C	C	kA	C
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
izotopy	izotop	k1gInPc1	izotop
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různý	různý	k2eAgInSc4d1	různý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Deuterium	deuterium	k1gNnSc1	deuterium
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
kvůli	kvůli	k7c3	kvůli
odlišné	odlišný	k2eAgFnSc3d1	odlišná
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
tvaru	tvar	k1gInSc3	tvar
molekul	molekula	k1gFnPc2	molekula
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
izotopů	izotop	k1gInPc2	izotop
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Izotopy	izotop	k1gInPc1	izotop
vodíku	vodík	k1gInSc2	vodík
===	===	k?	===
</s>
</p>
<p>
<s>
11	[number]	k4	11
H	H	kA	H
–	–	k?	–
protium	protium	k1gNnSc1	protium
(	(	kIx(	(
<g/>
běžný	běžný	k2eAgInSc1d1	běžný
vodík	vodík	k1gInSc1	vodík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
21	[number]	k4	21
H	H	kA	H
–	–	k?	–
deuterium	deuterium	k1gNnSc1	deuterium
<g/>
,	,	kIx,	,
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
21	[number]	k4	21
D	D	kA	D
</s>
</p>
<p>
<s>
31	[number]	k4	31
H	H	kA	H
–	–	k?	–
tritium	tritium	k1gNnSc1	tritium
<g/>
,	,	kIx,	,
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
31	[number]	k4	31
T	T	kA	T
</s>
</p>
<p>
<s>
===	===	k?	===
Izotopy	izotop	k1gInPc1	izotop
uhlíku	uhlík	k1gInSc2	uhlík
===	===	k?	===
</s>
</p>
<p>
<s>
126	[number]	k4	126
C	C	kA	C
–	–	k?	–
uhlík	uhlík	k1gInSc1	uhlík
12	[number]	k4	12
</s>
</p>
<p>
<s>
136	[number]	k4	136
C	C	kA	C
–	–	k?	–
uhlík	uhlík	k1gInSc1	uhlík
13	[number]	k4	13
<g/>
,	,	kIx,	,
využívaný	využívaný	k2eAgInSc1d1	využívaný
v	v	k7c6	v
NMR	NMR	kA	NMR
spekroskopii	spekroskopie	k1gFnSc6	spekroskopie
</s>
</p>
<p>
<s>
146	[number]	k4	146
C	C	kA	C
–	–	k?	–
uhlík	uhlík	k1gInSc1	uhlík
14	[number]	k4	14
(	(	kIx(	(
<g/>
radiouhlík	radiouhlík	k1gInSc1	radiouhlík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Izotopomer	Izotopomer	k1gMnSc1	Izotopomer
</s>
</p>
<p>
<s>
Nuklid	Nuklid	k1gInSc1	Nuklid
</s>
</p>
<p>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
izotopu	izotop	k1gInSc2	izotop
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
izotop	izotop	k1gInSc1	izotop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
izotop	izotop	k1gInSc1	izotop
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
