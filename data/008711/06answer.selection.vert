<s>
Jádra	jádro	k1gNnPc1	jádro
atomů	atom	k1gInPc2	atom
izotopů	izotop	k1gInPc2	izotop
jednoho	jeden	k4xCgInSc2	jeden
prvku	prvek	k1gInSc2	prvek
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
počet	počet	k1gInSc4	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
