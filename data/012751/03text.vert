<p>
<s>
Dichotomický	dichotomický	k2eAgInSc1d1	dichotomický
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
principiálně	principiálně	k6eAd1	principiálně
nabývá	nabývat	k5eAaImIp3nS	nabývat
pouze	pouze	k6eAd1	pouze
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
ano	ano	k9	ano
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
znaků	znak	k1gInPc2	znak
==	==	k?	==
</s>
</p>
<p>
<s>
uspěl	uspět	k5eAaPmAgMnS	uspět
×	×	k?	×
neuspěl	uspět	k5eNaPmAgMnS	uspět
</s>
</p>
<p>
<s>
muž	muž	k1gMnSc1	muž
×	×	k?	×
žena	žena	k1gFnSc1	žena
</s>
</p>
<p>
<s>
ano	ano	k9	ano
×	×	k?	×
ne	ne	k9	ne
</s>
</p>
<p>
<s>
pravák	pravák	k1gMnSc1	pravák
×	×	k?	×
levák	levák	k1gMnSc1	levák
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dichotomie	dichotomie	k1gFnSc1	dichotomie
</s>
</p>
<p>
<s>
Disjunktní	disjunktní	k2eAgFnPc1d1	disjunktní
množiny	množina	k1gFnPc1	množina
</s>
</p>
<p>
<s>
Binární	binární	k2eAgFnSc1d1	binární
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Kontingenční	Kontingenční	k2eAgFnSc1d1	Kontingenční
tabulka	tabulka	k1gFnSc1	tabulka
</s>
</p>
