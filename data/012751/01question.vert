<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
principiálně	principiálně	k6eAd1	principiálně
nabývá	nabývat	k5eAaImIp3nS	nabývat
pouze	pouze	k6eAd1	pouze
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
ano	ano	k9	ano
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
?	?	kIx.	?
</s>
