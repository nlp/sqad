<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
římské	římský	k2eAgFnSc6d1	římská
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
planetu	planeta	k1gFnSc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
ženě	žena	k1gFnSc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc2d1	hrubá
skladby	skladba	k1gFnSc2	skladba
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
planetou	planeta	k1gFnSc7	planeta
<g/>
"	"	kIx"	"
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Eliptická	eliptický	k2eAgFnSc1d1	eliptická
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Venuše	Venuše	k1gFnSc2	Venuše
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejmenší	malý	k2eAgFnSc4d3	nejmenší
výstřednost	výstřednost	k1gFnSc4	výstřednost
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,007	[number]	k4	0,007
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
224,7	[number]	k4	224,7
pozemských	pozemský	k2eAgInPc2d1	pozemský
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
blíže	blíž	k1gFnSc2	blíž
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
úhlová	úhlový	k2eAgFnSc1d1	úhlová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nemůže	moct	k5eNaImIp3nS	moct
překročit	překročit	k5eAaPmF	překročit
určitou	určitý	k2eAgFnSc4d1	určitá
mez	mez	k1gFnSc4	mez
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
elongace	elongace	k1gFnSc1	elongace
je	být	k5eAaImIp3nS	být
47,8	[number]	k4	47,8
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
před	před	k7c7	před
úsvitem	úsvit	k1gInSc7	úsvit
nebo	nebo	k8xC	nebo
po	po	k7c6	po
soumraku	soumrak	k1gInSc6	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
večernice	večernice	k1gFnSc1	večernice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zdaleka	zdaleka	k6eAd1	zdaleka
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
bodový	bodový	k2eAgInSc4d1	bodový
přírodní	přírodní	k2eAgInSc4d1	přírodní
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
magnituda	magnituda	k1gFnSc1	magnituda
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
a	a	k8xC	a
Měsíci	měsíc	k1gInSc3	měsíc
nejjasnějším	jasný	k2eAgInSc7d3	nejjasnější
zdrojem	zdroj	k1gInSc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
lze	lze	k6eAd1	lze
Venuši	Venuše	k1gFnSc4	Venuše
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
vrstvou	vrstva	k1gFnSc7	vrstva
husté	hustý	k2eAgFnSc2d1	hustá
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
spatřit	spatřit	k5eAaPmF	spatřit
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
spekulací	spekulace	k1gFnPc2	spekulace
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
přetrvávajících	přetrvávající	k2eAgInPc2d1	přetrvávající
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
pomocí	pomocí	k7c2	pomocí
přistávacích	přistávací	k2eAgInPc2d1	přistávací
modulů	modul	k1gInPc2	modul
a	a	k8xC	a
radarového	radarový	k2eAgNnSc2d1	radarové
mapování	mapování	k1gNnSc2	mapování
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
atmosféru	atmosféra	k1gFnSc4	atmosféra
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
cyklu	cyklus	k1gInSc2	cyklus
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
navázání	navázání	k1gNnSc2	navázání
do	do	k7c2	do
hornin	hornina	k1gFnPc2	hornina
či	či	k8xC	či
na	na	k7c4	na
biomasu	biomasa	k1gFnSc4	biomasa
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
enormnímu	enormní	k2eAgInSc3d1	enormní
nárůstu	nárůst	k1gInSc3	nárůst
až	až	k6eAd1	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ohřál	ohřát	k5eAaPmAgInS	ohřát
planetu	planeta	k1gFnSc4	planeta
na	na	k7c4	na
teploty	teplota	k1gFnPc4	teplota
znemožňující	znemožňující	k2eAgInSc1d1	znemožňující
výskyt	výskyt	k1gInSc1	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
učinil	učinit	k5eAaImAgMnS	učinit
z	z	k7c2	z
Venuše	Venuše	k1gFnSc2	Venuše
suchý	suchý	k2eAgInSc4d1	suchý
a	a	k8xC	a
prašný	prašný	k2eAgInSc4d1	prašný
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
oceány	oceán	k1gInPc4	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	Voda	k1gMnSc1	Voda
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
narůstající	narůstající	k2eAgFnSc2d1	narůstající
teploty	teplota	k1gFnSc2	teplota
vypařila	vypařit	k5eAaPmAgFnS	vypařit
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
vodní	vodní	k2eAgFnPc1d1	vodní
molekuly	molekula	k1gFnPc1	molekula
střetly	střetnout	k5eAaPmAgFnP	střetnout
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozpadu	rozpad	k1gInSc3	rozpad
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
volných	volný	k2eAgFnPc2d1	volná
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tlak	tlak	k1gInSc4	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
přibližně	přibližně	k6eAd1	přibližně
92	[number]	k4	92
<g/>
násobku	násobek	k1gInSc2	násobek
tlaku	tlak	k1gInSc2	tlak
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
starým	starý	k2eAgMnPc3d1	starý
Babyloňanům	Babyloňan	k1gMnPc3	Babyloňan
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jasné	jasný	k2eAgFnSc3d1	jasná
viditelnosti	viditelnost	k1gFnSc3	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
nálezy	nález	k1gInPc1	nález
dokládající	dokládající	k2eAgFnSc1d1	dokládající
její	její	k3xOp3gNnSc4	její
pozorování	pozorování	k1gNnSc4	pozorování
z	z	k7c2	z
archeologické	archeologický	k2eAgFnSc2d1	archeologická
lokality	lokalita	k1gFnSc2	lokalita
Makotřasy	Makotřasy	k1gInPc4	Makotřasy
z	z	k7c2	z
období	období	k1gNnSc2	období
asi	asi	k9	asi
2700	[number]	k4	2700
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
držící	držící	k2eAgNnSc1d1	držící
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
:	:	kIx,	:
kruh	kruh	k1gInSc1	kruh
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
křížem	kříž	k1gInSc7	kříž
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
<g/>
:	:	kIx,	:
♀	♀	k?	♀
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Venuše	Venuše	k1gFnSc2	Venuše
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
až	až	k6eAd1	až
díky	díky	k7c3	díky
radaru	radar	k1gInSc3	radar
a	a	k8xC	a
kosmickým	kosmický	k2eAgFnPc3d1	kosmická
sondám	sonda	k1gFnPc3	sonda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
přistání	přistání	k1gNnSc4	přistání
provedla	provést	k5eAaPmAgFnS	provést
sonda	sonda	k1gFnSc1	sonda
Veněra	Veněra	k1gFnSc1	Veněra
7	[number]	k4	7
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zhotovila	zhotovit	k5eAaPmAgFnS	zhotovit
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Magellan	Magellana	k1gFnPc2	Magellana
detailní	detailní	k2eAgFnSc4d1	detailní
mapu	mapa	k1gFnSc4	mapa
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výzkumy	výzkum	k1gInPc1	výzkum
přinesly	přinést	k5eAaPmAgInP	přinést
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
silné	silný	k2eAgFnSc6d1	silná
sopečné	sopečný	k2eAgFnSc6d1	sopečná
aktivitě	aktivita	k1gFnSc6	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
domněnkám	domněnka	k1gFnPc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
nachází	nacházet	k5eAaImIp3nS	nacházet
aktivní	aktivní	k2eAgInSc1d1	aktivní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
měření	měření	k1gNnSc4	měření
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
snímků	snímek	k1gInPc2	snímek
ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
žádné	žádný	k3yNgInPc1	žádný
doklady	doklad	k1gInPc1	doklad
lávových	lávový	k2eAgInPc2d1	lávový
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
pocházely	pocházet	k5eAaImAgInP	pocházet
z	z	k7c2	z
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
bylo	být	k5eAaImAgNnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
kráterů	kráter	k1gInPc2	kráter
naznačující	naznačující	k2eAgFnSc2d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
o	o	k7c4	o
stáří	stáří	k1gNnSc4	stáří
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,6	[number]	k4	4,6
či	či	k8xC	či
4,5	[number]	k4	4,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
akrecí	akrece	k1gFnPc2	akrece
z	z	k7c2	z
pracho-plynného	pracholynný	k2eAgInSc2d1	pracho-plynný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
rodící	rodící	k2eAgFnSc2d1	rodící
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Srážkami	srážka	k1gFnPc7	srážka
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
formovat	formovat	k5eAaImF	formovat
malá	malý	k2eAgNnPc1d1	malé
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
další	další	k2eAgFnSc1d1	další
částice	částice	k1gFnSc1	částice
a	a	k8xC	a
okolní	okolní	k2eAgInSc1d1	okolní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
první	první	k4xOgFnPc1	první
planetesimály	planetesimála	k1gFnPc1	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
srážely	srážet	k5eAaImAgInP	srážet
a	a	k8xC	a
formovaly	formovat	k5eAaImAgInP	formovat
větší	veliký	k2eAgFnPc4d2	veliký
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgInPc1	čtyři
terestrické	terestrický	k2eAgInPc1d1	terestrický
protoplanety	protoplanet	k1gInPc1	protoplanet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zformování	zformování	k1gNnSc1	zformování
protoplanety	protoplanet	k1gInPc7	protoplanet
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
bombardování	bombardování	k1gNnSc3	bombardování
povrchu	povrch	k1gInSc2	povrch
zbylým	zbylý	k2eAgInSc7d1	zbylý
materiálem	materiál	k1gInSc7	materiál
ze	z	k7c2	z
vzniku	vznik	k1gInSc2	vznik
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	on	k3xPp3gInSc2	on
neustálé	neustálý	k2eAgNnSc1d1	neustálé
přetváření	přetváření	k1gNnSc1	přetváření
a	a	k8xC	a
přetavování	přetavování	k1gNnSc1	přetavování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
roztaven	roztavit	k5eAaPmNgInS	roztavit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
společně	společně	k6eAd1	společně
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
diferenciací	diferenciace	k1gFnSc7	diferenciace
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
kumulována	kumulován	k2eAgFnSc1d1	kumulována
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
a	a	k8xC	a
tektonických	tektonický	k2eAgInPc2d1	tektonický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Země	zem	k1gFnSc2	zem
pevný	pevný	k2eAgInSc4d1	pevný
kamenitý	kamenitý	k2eAgInSc4d1	kamenitý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc3	hmotnost
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
sestra	sestra	k1gFnSc1	sestra
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
sesterská	sesterský	k2eAgFnSc1d1	sesterská
planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
650	[number]	k4	650
km	km	kA	km
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
její	její	k3xOp3gFnSc1	její
hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
81,5	[number]	k4	81,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
pozemských	pozemský	k2eAgInPc2d1	pozemský
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
panují	panovat	k5eAaImIp3nP	panovat
extrémní	extrémní	k2eAgFnPc1d1	extrémní
podmínky	podmínka	k1gFnPc1	podmínka
způsobené	způsobený	k2eAgFnPc1d1	způsobená
silným	silný	k2eAgInSc7d1	silný
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
efektem	efekt	k1gInSc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
96,5	[number]	k4	96,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
změřit	změřit	k5eAaPmF	změřit
šíření	šíření	k1gNnSc4	šíření
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
skrz	skrz	k7c4	skrz
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
vrstvy	vrstva	k1gFnPc4	vrstva
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
znalosti	znalost	k1gFnSc2	znalost
momentu	moment	k1gInSc2	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zjistit	zjistit	k5eAaPmF	zjistit
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
složení	složení	k1gNnSc6	složení
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podobnost	podobnost	k1gFnSc1	podobnost
rozměru	rozměr	k1gInSc2	rozměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Venuše	Venuše	k1gFnSc2	Venuše
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
si	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
i	i	k9	i
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
také	také	k9	také
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
pevné	pevný	k2eAgFnSc2d1	pevná
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
odvozovat	odvozovat	k5eAaImF	odvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
menší	malý	k2eAgFnSc1d2	menší
bude	být	k5eAaImBp3nS	být
i	i	k9	i
její	její	k3xOp3gInSc4	její
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
také	také	k9	také
nepodařilo	podařit	k5eNaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
důkazy	důkaz	k1gInPc4	důkaz
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc2	Venuše
tak	tak	k8xC	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Merkurem	Merkur	k1gInSc7	Merkur
má	mít	k5eAaImIp3nS	mít
nejspíše	nejspíše	k9	nejspíše
litosféru	litosféra	k1gFnSc4	litosféra
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
jednou	jeden	k4xCgFnSc7	jeden
kompaktní	kompaktní	k2eAgFnSc7d1	kompaktní
litosférickou	litosférický	k2eAgFnSc7d1	litosférická
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
silnou	silný	k2eAgFnSc4d1	silná
litosféru	litosféra	k1gFnSc4	litosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
průniku	průnik	k1gInSc3	průnik
chocholů	chochol	k1gInPc2	chochol
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
nastartování	nastartování	k1gNnSc1	nastartování
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
pozemskému	pozemský	k2eAgNnSc3d1	pozemské
jádru	jádro	k1gNnSc3	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
tvořen	tvořit	k5eAaImNgInS	tvořit
částečně	částečně	k6eAd1	částečně
tekutým	tekutý	k2eAgNnSc7d1	tekuté
železným	železný	k2eAgNnSc7d1	železné
jádrem	jádro	k1gNnSc7	jádro
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
6	[number]	k4	6
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
obklopeným	obklopený	k2eAgInSc7d1	obklopený
roztaveným	roztavený	k2eAgInSc7d1	roztavený
kamenným	kamenný	k2eAgInSc7d1	kamenný
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
tvoří	tvořit	k5eAaImIp3nS	tvořit
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
obaly	obal	k1gInPc4	obal
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
pláště	plášť	k1gInSc2	plášť
leží	ležet	k5eAaImIp3nS	ležet
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
okolo	okolo	k7c2	okolo
2	[number]	k4	2
840	[number]	k4	840
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
ani	ani	k8xC	ani
teplota	teplota	k1gFnSc1	teplota
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
planety	planeta	k1gFnSc2	planeta
nejsou	být	k5eNaImIp3nP	být
podrobně	podrobně	k6eAd1	podrobně
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
a	a	k8xC	a
známy	znám	k2eAgInPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
bohatě	bohatě	k6eAd1	bohatě
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
zejména	zejména	k9	zejména
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vázané	vázaný	k2eAgFnPc1d1	vázaná
se	se	k3xPyFc4	se
sírou	síra	k1gFnSc7	síra
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
pláště	plášť	k1gInSc2	plášť
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
teploty	teplota	k1gFnPc1	teplota
okolo	okolo	k7c2	okolo
3	[number]	k4	3
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
k	k	k7c3	k
4	[number]	k4	4
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Venuše	Venuše	k1gFnSc2	Venuše
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Země	země	k1gFnSc1	země
překonala	překonat	k5eAaPmAgFnS	překonat
gravitační	gravitační	k2eAgFnSc4d1	gravitační
diferenciaci	diferenciace	k1gFnSc4	diferenciace
<g/>
,	,	kIx,	,
období	období	k1gNnPc4	období
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zformování	zformování	k1gNnSc6	zformování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těžší	těžký	k2eAgInPc1d2	těžší
prvky	prvek	k1gInPc1	prvek
klesaly	klesat	k5eAaImAgFnP	klesat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lehčí	lehký	k2eAgInPc4d2	lehčí
prvky	prvek	k1gInPc4	prvek
stoupaly	stoupat	k5eAaImAgFnP	stoupat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
diferenciace	diferenciace	k1gFnSc2	diferenciace
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
sekundární	sekundární	k2eAgFnSc2d1	sekundární
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
měření	měření	k1gNnSc2	měření
sondy	sonda	k1gFnSc2	sonda
Magellan	Magellana	k1gFnPc2	Magellana
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůra	kůra	k1gFnSc1	kůra
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
neuvolňuje	uvolňovat	k5eNaImIp3nS	uvolňovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
energii	energie	k1gFnSc4	energie
pohyby	pohyb	k1gInPc1	pohyb
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
jako	jako	k8xC	jako
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
masivní	masivní	k2eAgFnSc4d1	masivní
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
zalévá	zalévat	k5eAaImIp3nS	zalévat
čerstvou	čerstvý	k2eAgFnSc7d1	čerstvá
lávou	láva	k1gFnSc7	láva
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podporuje	podporovat	k5eAaImIp3nS	podporovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgInPc1d3	nejstarší
geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
staré	stará	k1gFnPc1	stará
pouze	pouze	k6eAd1	pouze
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
mladšího	mladý	k2eAgNnSc2d2	mladší
data	datum	k1gNnSc2	datum
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
geologických	geologický	k2eAgNnPc6d1	geologické
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc4	pozorování
povrchu	povrch	k1gInSc2	povrch
zařízením	zařízení	k1gNnSc7	zařízení
VIRTIS	VIRTIS	kA	VIRTIS
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
Venus	Venus	k1gInSc1	Venus
Express	express	k1gInSc1	express
přineslo	přinést	k5eAaPmAgNnS	přinést
poznatky	poznatek	k1gInPc4	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
byla	být	k5eAaImAgFnS	být
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
2,5	[number]	k4	2,5
miliónu	milión	k4xCgInSc2	milión
až	až	k6eAd1	až
250	[number]	k4	250
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Povrch	povrch	k7c2wR	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
husté	hustý	k2eAgFnSc3d1	hustá
atmosféře	atmosféra	k1gFnSc3	atmosféra
obklopující	obklopující	k2eAgFnSc4d1	obklopující
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
nemožný	možný	k2eNgInSc1d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmapování	zmapování	k1gNnSc4	zmapování
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
radarové	radarový	k2eAgFnPc1d1	radarová
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
atmosférou	atmosféra	k1gFnSc7	atmosféra
proniknout	proniknout	k5eAaPmF	proniknout
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
80	[number]	k4	80
%	%	kIx~	%
tvořen	tvořit	k5eAaImNgInS	tvořit
lávovými	lávový	k2eAgFnPc7d1	lávová
planinami	planina	k1gFnPc7	planina
<g/>
.	.	kIx.	.
</s>
<s>
Venušin	Venušin	k2eAgInSc1d1	Venušin
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
"	"	kIx"	"
<g/>
kontinentálními	kontinentální	k2eAgFnPc7d1	kontinentální
<g/>
"	"	kIx"	"
vrchovinami	vrchovina	k1gFnPc7	vrchovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zdvíhají	zdvíhat	k5eAaImIp3nP	zdvíhat
z	z	k7c2	z
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
okolních	okolní	k2eAgFnPc2d1	okolní
plání	pláň	k1gFnPc2	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Výšky	výška	k1gFnPc1	výška
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
střednímu	střední	k2eAgInSc3d1	střední
poloměru	poloměr	k1gInSc3	poloměr
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
výchozí	výchozí	k2eAgFnSc4d1	výchozí
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
vrchoviny	vrchovina	k1gFnSc2	vrchovina
Ishtar	Ishtar	k1gMnSc1	Ishtar
Terra	Terra	k1gMnSc1	Terra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
největší	veliký	k2eAgFnSc2d3	veliký
hory	hora	k1gFnSc2	hora
Venuše	Venuše	k1gFnSc2	Venuše
Maxwell	maxwell	k1gInSc1	maxwell
Montes	Montes	k1gInSc1	Montes
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
masiv	masiv	k1gInSc1	masiv
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
2	[number]	k4	2
km	km	kA	km
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
Mount	Mount	k1gMnSc1	Mount
Everest	Everest	k1gInSc4	Everest
<g/>
,	,	kIx,	,
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tedy	tedy	k9	tedy
výšky	výška	k1gFnPc4	výška
okolo	okolo	k7c2	okolo
11	[number]	k4	11
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pohořími	pohoří	k1gNnPc7	pohoří
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Lakshmi	Laksh	k1gFnPc7	Laksh
Planum	Planum	k1gInSc1	Planum
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Ishtar	Ishtara	k1gFnPc2	Ishtara
Terra	Terra	k1gFnSc1	Terra
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
pozemská	pozemský	k2eAgFnSc1d1	pozemská
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
okolo	okolo	k7c2	okolo
8,5	[number]	k4	8,5
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
oblast	oblast	k1gFnSc1	oblast
Aphrodite	Aphrodit	k1gInSc5	Aphrodit
Terra	Terr	k1gInSc2	Terr
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
15	[number]	k4	15
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
velikostí	velikost	k1gFnPc2	velikost
rovná	rovnat	k5eAaImIp3nS	rovnat
přibližně	přibližně	k6eAd1	přibližně
Jižní	jižní	k2eAgFnSc3d1	jižní
Americe	Amerika	k1gFnSc3	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
vrchovinami	vrchovina	k1gFnPc7	vrchovina
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
širokých	široký	k2eAgFnPc2d1	široká
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
jako	jako	k8xC	jako
například	například	k6eAd1	například
Atalanta	Atalant	k1gMnSc4	Atalant
Planitia	Planitius	k1gMnSc4	Planitius
<g/>
,	,	kIx,	,
Guinevere	Guinever	k1gInSc5	Guinever
Planitia	Planitia	k1gFnSc1	Planitia
a	a	k8xC	a
Niobe	Niobe	k1gFnSc1	Niobe
Planitia	Planitia	k1gFnSc1	Planitia
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
využívají	využívat	k5eAaPmIp3nP	využívat
ženská	ženský	k2eAgNnPc4d1	ženské
jména	jméno	k1gNnPc4	jméno
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
vyjma	vyjma	k7c4	vyjma
Maxwell	maxwell	k1gInSc4	maxwell
Montes	Montesa	k1gFnPc2	Montesa
<g/>
,	,	kIx,	,
Alpha	Alpha	k1gFnSc1	Alpha
a	a	k8xC	a
Beta	beta	k1gNnSc1	beta
Regio	Regio	k6eAd1	Regio
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
přijata	přijmout	k5eAaPmNgNnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
Venuše	Venuše	k1gFnSc2	Venuše
velmi	velmi	k6eAd1	velmi
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brzdí	brzdit	k5eAaImIp3nS	brzdit
dopadající	dopadající	k2eAgNnPc4d1	dopadající
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
nenachází	nacházet	k5eNaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
žádné	žádný	k3yNgInPc1	žádný
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
3	[number]	k4	3
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
mělké	mělký	k2eAgFnPc1d1	mělká
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nízká	nízký	k2eAgFnSc1d1	nízká
hloubka	hloubka	k1gFnSc1	hloubka
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
intenzivní	intenzivní	k2eAgFnSc6d1	intenzivní
erozi	eroze	k1gFnSc6	eroze
nebo	nebo	k8xC	nebo
o	o	k7c6	o
silných	silný	k2eAgInPc6d1	silný
endogenních	endogenní	k2eAgInPc6d1	endogenní
pochodech	pochod	k1gInPc6	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
také	také	k9	také
zlomy	zlom	k1gInPc4	zlom
značných	značný	k2eAgInPc2d1	značný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
tektonické	tektonický	k2eAgFnSc6d1	tektonická
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
nedávno	nedávno	k6eAd1	nedávno
utuhlá	utuhlý	k2eAgFnSc1d1	utuhlá
vrstva	vrstva	k1gFnSc1	vrstva
čedičové	čedičový	k2eAgFnSc2d1	čedičová
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
zřídka	zřídka	k6eAd1	zřídka
porušena	porušit	k5eAaPmNgFnS	porušit
meteorickými	meteorický	k2eAgInPc7d1	meteorický
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
impaktních	impaktní	k2eAgFnPc2d1	impaktní
poruch	porucha	k1gFnPc2	porucha
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
značně	značně	k6eAd1	značně
přeměněn	přeměnit	k5eAaPmNgMnS	přeměnit
vlivem	vlivem	k7c2	vlivem
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Kartografické	kartografický	k2eAgFnPc4d1	kartografická
souřadnice	souřadnice	k1gFnPc4	souřadnice
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
jsou	být	k5eAaImIp3nP	být
vztažené	vztažený	k2eAgFnPc1d1	vztažená
k	k	k7c3	k
nultému	nultý	k4xOgInSc3	nultý
poledníku	poledník	k1gInSc3	poledník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prochází	procházet	k5eAaImIp3nS	procházet
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odráží	odrážet	k5eAaImIp3nP	odrážet
radarové	radarový	k2eAgInPc1d1	radarový
signály	signál	k1gInPc1	signál
a	a	k8xC	a
který	který	k3yQgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
uprostřed	uprostřed	k7c2	uprostřed
oválného	oválný	k2eAgInSc2d1	oválný
objektu	objekt	k1gInSc2	objekt
Eve	Eve	k1gFnSc2	Eve
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Alpha	Alph	k1gMnSc2	Alph
Regio	Regio	k6eAd1	Regio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
sopek	sopka	k1gFnPc2	sopka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sif	Sif	k1gFnSc1	Sif
Mons	Mons	k1gInSc1	Mons
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
povedlo	povést	k5eAaPmAgNnS	povést
již	již	k6eAd1	již
objevit	objevit	k5eAaPmF	objevit
přes	přes	k7c4	přes
1	[number]	k4	1
600	[number]	k4	600
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
konečné	konečný	k2eAgNnSc1d1	konečné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
rozlišení	rozlišení	k1gNnSc4	rozlišení
snímků	snímek	k1gInPc2	snímek
porvchu	porvch	k1gInSc2	porvch
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mají	mít	k5eAaImIp3nP	mít
vědci	vědec	k1gMnPc1	vědec
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
167	[number]	k4	167
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
základny	základna	k1gFnSc2	základna
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
podobná	podobný	k2eAgFnSc1d1	podobná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
sopek	sopka	k1gFnPc2	sopka
oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
není	být	k5eNaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
silnějším	silný	k2eAgInSc7d2	silnější
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyšším	vysoký	k2eAgNnSc7d2	vyšší
stářím	stáří	k1gNnSc7	stáří
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
kůře	kůra	k1gFnSc6	kůra
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
pravidelně	pravidelně	k6eAd1	pravidelně
znovuvytvářena	znovuvytvářet	k5eAaImNgFnS	znovuvytvářet
a	a	k8xC	a
pohřbívána	pohřbívat	k5eAaImNgFnS	pohřbívat
vlivem	vlivem	k7c2	vlivem
subdukce	subdukce	k1gFnSc2	subdukce
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgNnSc4d1	střední
stáří	stáří	k1gNnSc4	stáří
pozemské	pozemský	k2eAgFnSc2d1	pozemská
kůry	kůra	k1gFnSc2	kůra
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
stará	starý	k2eAgFnSc1d1	stará
až	až	k9	až
okolo	okolo	k7c2	okolo
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozeznat	rozeznat	k5eAaPmF	rozeznat
okolo	okolo	k7c2	okolo
tisíce	tisíc	k4xCgInSc2	tisíc
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
distribuovány	distribuovat	k5eAaBmNgFnP	distribuovat
nahodile	nahodile	k6eAd1	nahodile
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
či	či	k8xC	či
Měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
stádiu	stádium	k1gNnSc6	stádium
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
různém	různý	k2eAgNnSc6d1	různé
stáří	stáří	k1gNnSc6	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
jsou	být	k5eAaImIp3nP	být
starší	starý	k2eAgInPc1d2	starší
krátery	kráter	k1gInPc1	kráter
zahlazovány	zahlazován	k2eAgInPc1d1	zahlazován
novějšími	nový	k2eAgInPc7d2	novější
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
činností	činnost	k1gFnSc7	činnost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
krátery	kráter	k1gInPc1	kráter
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
85	[number]	k4	85
%	%	kIx~	%
případů	případ	k1gInPc2	případ
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
dokonalém	dokonalý	k2eAgInSc6d1	dokonalý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
stav	stav	k1gInSc1	stav
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
kráterů	kráter	k1gInPc2	kráter
podporují	podporovat	k5eAaImIp3nP	podporovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
celkovém	celkový	k2eAgNnSc6d1	celkové
překrytí	překrytí	k1gNnSc6	překrytí
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
novým	nový	k2eAgInSc7d1	nový
sopečným	sopečný	k2eAgInSc7d1	sopečný
materiálem	materiál	k1gInSc7	materiál
před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
přetváří	přetvářet	k5eAaImIp3nS	přetvářet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
unikající	unikající	k2eAgNnSc4d1	unikající
teplo	teplo	k1gNnSc4	teplo
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
k	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
procesu	proces	k1gInSc3	proces
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
teplo	teplo	k6eAd1	teplo
cyklicky	cyklicky	k6eAd1	cyklicky
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
masivní	masivní	k2eAgFnSc7d1	masivní
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměru	průměr	k1gInSc3	průměr
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
280	[number]	k4	280
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
krátery	kráter	k1gInPc1	kráter
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
to	ten	k3xDgNnSc1	ten
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
brzdí	brzdit	k5eAaImIp3nS	brzdit
malá	malý	k2eAgNnPc4d1	malé
prolétající	prolétající	k2eAgNnPc4d1	prolétající
tělesa	těleso	k1gNnPc4	těleso
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
již	již	k6eAd1	již
nemají	mít	k5eNaImIp3nP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
schopnou	schopný	k2eAgFnSc7d1	schopná
vytvořit	vytvořit	k5eAaPmF	vytvořit
impaktní	impaktní	k2eAgInSc4d1	impaktní
kráter	kráter	k1gInSc4	kráter
<g/>
.	.	kIx.	.
či	či	k8xC	či
zničení	zničení	k1gNnSc1	zničení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
Venus	Venus	k1gInSc4	Venus
Orbiter	Orbitra	k1gFnPc2	Orbitra
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
slabší	slabý	k2eAgInPc4d2	slabší
a	a	k8xC	a
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
pozemské	pozemský	k2eAgInPc4d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
není	být	k5eNaImIp3nS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Venuše	Venuše	k1gFnSc2	Venuše
indikované	indikovaný	k2eAgFnSc2d1	indikovaná
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
při	při	k7c6	při
interakci	interakce	k1gFnSc6	interakce
ionosféry	ionosféra	k1gFnSc2	ionosféra
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nemá	mít	k5eNaImIp3nS	mít
Venuše	Venuše	k1gFnSc2	Venuše
dvojpólové	dvojpólový	k2eAgNnSc1d1	dvojpólové
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
generované	generovaný	k2eAgNnSc1d1	generované
jádrem	jádro	k1gNnSc7	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
Venuše	Venuše	k1gFnSc2	Venuše
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
vzniku	vznik	k1gInSc3	vznik
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
podobné	podobný	k2eAgNnSc4d1	podobné
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
tekutým	tekutý	k2eAgInSc7d1	tekutý
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
probíhání	probíhání	k1gNnSc4	probíhání
termochemické	termochemický	k2eAgFnSc2d1	termochemická
konvekce	konvekce	k1gFnSc2	konvekce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
generování	generování	k1gNnSc4	generování
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
pole	pole	k1gFnSc1	pole
indukovaného	indukovaný	k2eAgInSc2d1	indukovaný
jádrem	jádro	k1gNnSc7	jádro
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počáteční	počáteční	k2eAgNnSc4d1	počáteční
teplo	teplo	k1gNnSc4	teplo
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
společně	společně	k6eAd1	společně
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
vznikajícím	vznikající	k2eAgNnSc7d1	vznikající
při	při	k7c6	při
radioaktivním	radioaktivní	k2eAgInSc6d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
nestačilo	stačit	k5eNaBmAgNnS	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
udrželo	udržet	k5eAaPmAgNnS	udržet
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
teplota	teplota	k1gFnSc1	teplota
jádra	jádro	k1gNnSc2	jádro
příliš	příliš	k6eAd1	příliš
nízká	nízký	k2eAgFnSc1d1	nízká
na	na	k7c6	na
termální	termální	k2eAgFnSc6d1	termální
konvekci	konvekce	k1gFnSc6	konvekce
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
vnitřně	vnitřně	k6eAd1	vnitřně
buzené	buzený	k2eAgFnSc2d1	buzená
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Venuše	Venuše	k1gFnSc2	Venuše
malým	malý	k2eAgInSc7d1	malý
tepelným	tepelný	k2eAgInSc7d1	tepelný
tokem	tok	k1gInSc7	tok
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
generovaného	generovaný	k2eAgNnSc2d1	generované
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
oblastech	oblast	k1gFnPc6	oblast
planety	planeta	k1gFnSc2	planeta
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
chráněna	chránit	k5eAaImNgFnS	chránit
proti	proti	k7c3	proti
dopadajícím	dopadající	k2eAgFnPc3d1	dopadající
částicím	částice	k1gFnPc3	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
radiaci	radiace	k1gFnSc4	radiace
přicházející	přicházející	k2eAgMnPc1d1	přicházející
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xC	jako
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
zmiňované	zmiňovaný	k2eAgFnPc1d1	zmiňovaná
planety	planeta	k1gFnPc1	planeta
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
Indukovaná	indukovaný	k2eAgFnSc1d1	indukovaná
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
vznikající	vznikající	k2eAgFnSc1d1	vznikající
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
částic	částice	k1gFnPc2	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
je	být	k5eAaImIp3nS	být
zformována	zformovat	k5eAaPmNgFnS	zformovat
do	do	k7c2	do
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
chvostu	chvost	k1gInSc2	chvost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
až	až	k9	až
12	[number]	k4	12
poloměrů	poloměr	k1gInPc2	poloměr
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
pak	pak	k6eAd1	pak
vzniká	vznikat	k5eAaImIp3nS	vznikat
rázová	rázový	k2eAgFnSc1d1	rázová
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
dynama	dynamo	k1gNnSc2	dynamo
generující	generující	k2eAgFnSc2d1	generující
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pole	pole	k1gFnSc2	pole
byla	být	k5eAaImAgFnS	být
velkým	velký	k2eAgNnSc7d1	velké
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
při	při	k7c6	při
shodné	shodný	k2eAgFnSc6d1	shodná
velikosti	velikost	k1gFnSc6	velikost
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
také	také	k9	také
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
dynamo	dynamo	k1gNnSc4	dynamo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
dynama	dynamo	k1gNnSc2	dynamo
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
splnit	splnit	k5eAaPmF	splnit
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
vodivou	vodivý	k2eAgFnSc4d1	vodivá
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
,	,	kIx,	,
rotaci	rotace	k1gFnSc4	rotace
a	a	k8xC	a
konvekci	konvekce	k1gFnSc4	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
dynama	dynamo	k1gNnSc2	dynamo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jádro	jádro	k1gNnSc1	jádro
vodivé	vodivý	k2eAgNnSc1d1	vodivé
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejspíš	nejspíš	k9	nejspíš
i	i	k9	i
případ	případ	k1gInSc4	případ
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc2	rotace
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dle	dle	k7c2	dle
modelů	model	k1gInPc2	model
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Venuše	Venuše	k1gFnSc2	Venuše
dostatečná	dostatečná	k1gFnSc1	dostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
poznatky	poznatek	k1gInPc1	poznatek
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
absence	absence	k1gFnSc2	absence
dynama	dynamo	k1gNnSc2	dynamo
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
Venuše	Venuše	k1gFnSc2	Venuše
bude	být	k5eAaImBp3nS	být
chybějící	chybějící	k2eAgFnSc1d1	chybějící
konvekce	konvekce	k1gFnSc1	konvekce
mezi	mezi	k7c7	mezi
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
korespondovalo	korespondovat	k5eAaImAgNnS	korespondovat
i	i	k9	i
s	s	k7c7	s
poznatky	poznatek	k1gInPc7	poznatek
o	o	k7c6	o
chybějící	chybějící	k2eAgFnSc6d1	chybějící
deskové	deskový	k2eAgFnSc6d1	desková
tektonice	tektonika	k1gFnSc6	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
mezi	mezi	k7c7	mezi
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
a	a	k8xC	a
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
nepatrný	nepatrný	k2eAgInSc1d1	nepatrný
a	a	k8xC	a
tedy	tedy	k9	tedy
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
dynama	dynamo	k1gNnSc2	dynamo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
atmosféry	atmosféra	k1gFnSc2	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
měřeních	měření	k1gNnPc6	měření
uskutečněných	uskutečněný	k2eAgFnPc2d1	uskutečněná
sondami	sonda	k1gFnPc7	sonda
typu	typ	k1gInSc2	typ
Veněra	Veněra	k1gFnSc1	Veněra
<g/>
,	,	kIx,	,
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
Pioneer-Venus	Pioneer-Venus	k1gInSc1	Pioneer-Venus
<g/>
,	,	kIx,	,
pozemskými	pozemský	k2eAgNnPc7d1	pozemské
pozorováními	pozorování	k1gNnPc7	pozorování
a	a	k8xC	a
teoretickými	teoretický	k2eAgInPc7d1	teoretický
modely	model	k1gInPc7	model
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
odhadovat	odhadovat	k5eAaImF	odhadovat
chování	chování	k1gNnSc4	chování
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
hustou	hustý	k2eAgFnSc7d1	hustá
vrstvou	vrstva	k1gFnSc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
silného	silný	k2eAgInSc2d1	silný
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
dokonce	dokonce	k9	dokonce
až	až	k9	až
o	o	k7c4	o
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Silný	silný	k2eAgInSc1d1	silný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
teplejší	teplý	k2eAgInSc1d2	teplejší
než	než	k8xS	než
Slunci	slunce	k1gNnSc6	slunce
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
<g />
.	.	kIx.	.
</s>
<s>
planeta	planeta	k1gFnSc1	planeta
Merkur	Merkur	k1gInSc1	Merkur
i	i	k8xC	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dvakrát	dvakrát	k6eAd1	dvakrát
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
pouze	pouze	k6eAd1	pouze
25	[number]	k4	25
%	%	kIx~	%
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
2	[number]	k4	2
613,9	[number]	k4	613,9
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
0	[number]	k4	0
<g/>
71,1	[number]	k4	71,1
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
tepelné	tepelný	k2eAgFnSc2d1	tepelná
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
a	a	k8xC	a
proudění	proudění	k1gNnSc2	proudění
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc6d1	noční
straně	strana	k1gFnSc6	strana
Venuše	Venuše	k1gFnSc2	Venuše
výrazně	výrazně	k6eAd1	výrazně
neliší	lišit	k5eNaImIp3nS	lišit
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
pomalá	pomalý	k2eAgFnSc1d1	pomalá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
panují	panovat	k5eAaImIp3nP	panovat
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obkrouží	obkroužit	k5eAaPmIp3nP	obkroužit
planetu	planeta	k1gFnSc4	planeta
přibližně	přibližně	k6eAd1	přibližně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
4	[number]	k4	4
pozemské	pozemský	k2eAgInPc4d1	pozemský
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
tepla	teplo	k1gNnSc2	teplo
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
8	[number]	k4	8
MPa	MPa	k1gFnPc2	MPa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozorování	pozorování	k1gNnSc2	pozorování
planety	planeta	k1gFnSc2	planeta
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
i	i	k9	i
elektrické	elektrický	k2eAgInPc4d1	elektrický
výboje	výboj	k1gInPc4	výboj
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
blesků	blesk	k1gInPc2	blesk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejspíš	nejspíš	k9	nejspíš
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1	elektrický
výboje	výboj	k1gInPc1	výboj
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
byly	být	k5eAaImAgFnP	být
předpovězeny	předpovědit	k5eAaPmNgFnP	předpovědit
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokud	dokud	k6eAd1	dokud
nebyly	být	k5eNaImAgInP	být
prvně	prvně	k?	prvně
pozorovány	pozorován	k2eAgInPc1d1	pozorován
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
sondou	sonda	k1gFnSc7	sonda
Veněra	Veněra	k1gFnSc1	Veněra
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
teorii	teorie	k1gFnSc6	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
2006	[number]	k4	2006
až	až	k9	až
2007	[number]	k4	2007
provedla	provést	k5eAaPmAgFnS	provést
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Venus	Venus	k1gInSc4	Venus
Express	express	k1gInSc1	express
řadu	řad	k1gInSc2	řad
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jasně	jasně	k6eAd1	jasně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
existenci	existence	k1gFnSc4	existence
elektrických	elektrický	k2eAgInPc2d1	elektrický
výbojů	výboj	k1gInPc2	výboj
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
vanou	vanout	k5eAaImIp3nP	vanout
silné	silný	k2eAgInPc1d1	silný
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
360	[number]	k4	360
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
vane	vanout	k5eAaImIp3nS	vanout
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
slabý	slabý	k2eAgInSc1d1	slabý
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
rychlost	rychlost	k1gFnSc1	rychlost
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
ale	ale	k9	ale
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
tyto	tento	k3xDgInPc1	tento
slabé	slabý	k2eAgInPc1d1	slabý
větry	vítr	k1gInPc1	vítr
značnou	značný	k2eAgFnSc4d1	značná
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
velkou	velký	k2eAgFnSc7d1	velká
energií	energie	k1gFnSc7	energie
na	na	k7c4	na
případnou	případný	k2eAgFnSc4d1	případná
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
objevila	objevit	k5eAaPmAgFnS	objevit
Venus	Venus	k1gInSc1	Venus
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
existuje	existovat	k5eAaImIp3nS	existovat
obrovský	obrovský	k2eAgInSc4d1	obrovský
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
vír	vír	k1gInSc4	vír
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
okolo	okolo	k7c2	okolo
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
koróna	koróna	k1gFnSc1	koróna
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
300	[number]	k4	300
km	km	kA	km
nachází	nacházet	k5eAaImIp3nS	nacházet
atmosféra	atmosféra	k1gFnSc1	atmosféra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
héliem	hélium	k1gNnSc7	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
je	být	k5eAaImIp3nS	být
směsice	směsice	k1gFnSc1	směsice
vzduchu	vzduch	k1gInSc2	vzduch
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
okolo	okolo	k6eAd1	okolo
96,5	[number]	k4	96,5
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
3,5	[number]	k4	3,5
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
0,1	[number]	k4	0,1
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
několika	několik	k4yIc7	několik
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
Venuše	Venuše	k1gFnSc1	Venuše
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
velmi	velmi	k6eAd1	velmi
podobat	podobat	k5eAaImF	podobat
pozemské	pozemský	k2eAgFnPc1d1	pozemská
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypaření	vypaření	k1gNnSc3	vypaření
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
navýšila	navýšit	k5eAaPmAgFnS	navýšit
obsah	obsah	k1gInSc4	obsah
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
na	na	k7c4	na
kritickou	kritický	k2eAgFnSc4d1	kritická
hranici	hranice	k1gFnSc4	hranice
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
k	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
extrémně	extrémně	k6eAd1	extrémně
horkému	horký	k2eAgInSc3d1	horký
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
velmi	velmi	k6eAd1	velmi
oslabené	oslabený	k2eAgInPc1d1	oslabený
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
musí	muset	k5eAaImIp3nS	muset
překonávat	překonávat	k5eAaImF	překonávat
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
síly	síla	k1gFnPc4	síla
Slunce	slunce	k1gNnSc2	slunce
při	při	k7c6	při
zamračené	zamračený	k2eAgFnSc6d1	zamračená
obloze	obloha	k1gFnSc6	obloha
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
oblačnost	oblačnost	k1gFnSc1	oblačnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
až	až	k9	až
70	[number]	k4	70
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
odráží	odrážet	k5eAaImIp3nS	odrážet
okolo	okolo	k7c2	okolo
60	[number]	k4	60
%	%	kIx~	%
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zabraňování	zabraňování	k1gNnSc3	zabraňování
ještě	ještě	k6eAd1	ještě
silnějšího	silný	k2eAgNnSc2d2	silnější
ohřívání	ohřívání	k1gNnSc2	ohřívání
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
svědomí	svědomí	k1gNnSc4	svědomí
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bolometrické	bolometrický	k2eAgNnSc1d1	bolometrický
albedo	albedo	k1gNnSc1	albedo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
a	a	k8xC	a
albedo	albedo	k1gNnSc1	albedo
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
oblačnosti	oblačnost	k1gFnSc2	oblačnost
Venuše	Venuše	k1gFnSc2	Venuše
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotný	samotný	k2eAgInSc1d1	samotný
povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
osvětlený	osvětlený	k2eAgInSc4d1	osvětlený
než	než	k8xS	než
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
i	i	k9	i
méně	málo	k6eAd2	málo
zahříván	zahříván	k2eAgInSc1d1	zahříván
slunečním	sluneční	k2eAgNnSc7d1	sluneční
teplem	teplo	k1gNnSc7	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
není	být	k5eNaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
panují	panovat	k5eAaImIp3nP	panovat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tímto	tento	k3xDgInSc7	tento
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
CO2	CO2	k1gFnSc2	CO2
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
silný	silný	k2eAgInSc1d1	silný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Mraky	mrak	k1gInPc1	mrak
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgMnPc4d1	tvořený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
a	a	k8xC	a
kapiček	kapička	k1gFnPc2	kapička
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
planetu	planeta	k1gFnSc4	planeta
a	a	k8xC	a
skrývají	skrývat	k5eAaImIp3nP	skrývat
lidskému	lidský	k2eAgNnSc3d1	lidské
oku	oko	k1gNnSc6	oko
veškeré	veškerý	k3xTgInPc4	veškerý
detaily	detail	k1gInPc4	detail
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
mraků	mrak	k1gInPc2	mrak
mají	mít	k5eAaImIp3nP	mít
teplotu	teplota	k1gFnSc4	teplota
přibližně	přibližně	k6eAd1	přibližně
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
příznivá	příznivý	k2eAgFnSc1d1	příznivá
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
kolem	kolem	k7c2	kolem
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozemských	pozemský	k2eAgNnPc2d1	pozemské
měření	měření	k1gNnPc2	měření
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
300	[number]	k4	300
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
určila	určit	k5eAaPmAgFnS	určit
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
464	[number]	k4	464
°	°	k?	°
<g/>
C.	C.	kA	C.
Minimální	minimální	k2eAgFnSc4d1	minimální
teplotu	teplota	k1gFnSc4	teplota
mají	mít	k5eAaImIp3nP	mít
právě	právě	k9	právě
vrcholky	vrcholek	k1gInPc1	vrcholek
mraků	mrak	k1gInPc2	mrak
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nikdy	nikdy	k6eAd1	nikdy
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c7	pod
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Povrch	povrch	k7c2wR	povrch
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnSc6d1	stejná
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
nepanují	panovat	k5eNaImIp3nP	panovat
výraznější	výrazný	k2eAgInPc4d2	výraznější
výkyvy	výkyv	k1gInPc4	výkyv
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
skloněna	skloněn	k2eAgFnSc1d1	skloněna
jen	jen	k9	jen
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
střídání	střídání	k1gNnSc1	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
teplotní	teplotní	k2eAgInPc1d1	teplotní
výkyvy	výkyv	k1gInPc1	výkyv
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
výraznější	výrazný	k2eAgFnSc1d2	výraznější
změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
nastává	nastávat	k5eAaImIp3nS	nastávat
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
sonda	sonda	k1gFnSc1	sonda
Magellan	Magellana	k1gFnPc2	Magellana
nasnímala	nasnímat	k5eAaPmAgFnS	nasnímat
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
vysoce	vysoce	k6eAd1	vysoce
reflektivní	reflektivní	k2eAgFnSc4d1	reflektivní
bílou	bílý	k2eAgFnSc4d1	bílá
látku	látka	k1gFnSc4	látka
vykazující	vykazující	k2eAgFnSc4d1	vykazující
výraznou	výrazný	k2eAgFnSc4d1	výrazná
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
pozemským	pozemský	k2eAgInSc7d1	pozemský
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
podobnými	podobný	k2eAgInPc7d1	podobný
procesy	proces	k1gInPc7	proces
jako	jako	k8xC	jako
vzniká	vznikat	k5eAaImIp3nS	vznikat
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
za	za	k7c2	za
značně	značně	k6eAd1	značně
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
plynného	plynný	k2eAgNnSc2d1	plynné
skupenství	skupenství	k1gNnSc2	skupenství
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
oblastech	oblast	k1gFnPc6	oblast
pak	pak	k6eAd1	pak
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
skupenství	skupenství	k1gNnSc2	skupenství
padá	padat	k5eAaImIp3nS	padat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
složení	složení	k1gNnSc4	složení
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
neznámé	známý	k2eNgNnSc1d1	neznámé
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
možných	možný	k2eAgNnPc2d1	možné
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
od	od	k7c2	od
telluru	tellur	k1gInSc2	tellur
až	až	k9	až
po	po	k7c4	po
sloučeniny	sloučenina	k1gFnPc4	sloučenina
sulfidů	sulfid	k1gInPc2	sulfid
např.	např.	kA	např.
galenitu	galenit	k1gInSc6	galenit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Venuše	Venuše	k1gFnSc1	Venuše
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
zcela	zcela	k6eAd1	zcela
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
částicemi	částice	k1gFnPc7	částice
dopadajícími	dopadající	k2eAgFnPc7d1	dopadající
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
podobné	podobný	k2eAgNnSc4d1	podobné
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombardování	bombardování	k1gNnSc1	bombardování
slunečními	sluneční	k2eAgFnPc7d1	sluneční
částicemi	částice	k1gFnPc7	částice
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
vodních	vodní	k2eAgFnPc2d1	vodní
molekul	molekula	k1gFnPc2	molekula
na	na	k7c4	na
atomy	atom	k1gInPc4	atom
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nízké	nízký	k2eAgFnSc2d1	nízká
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vodíku	vodík	k1gInSc2	vodík
mohl	moct	k5eAaImAgInS	moct
následně	následně	k6eAd1	následně
snadno	snadno	k6eAd1	snadno
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
těžší	těžký	k2eAgInSc4d2	těžší
kyslík	kyslík	k1gInSc4	kyslík
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
následně	následně	k6eAd1	následně
nejspíše	nejspíše	k9	nejspíše
reagoval	reagovat	k5eAaBmAgMnS	reagovat
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
navázání	navázání	k1gNnSc3	navázání
a	a	k8xC	a
postupnému	postupný	k2eAgNnSc3d1	postupné
vymizení	vymizení	k1gNnSc3	vymizení
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
deutéria	deutérium	k1gNnSc2	deutérium
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
suchu	sucho	k1gNnSc3	sucho
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
těžší	těžký	k2eAgMnSc1d2	těžší
a	a	k8xC	a
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
než	než	k8xS	než
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc4	vznik
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
útesů	útes	k1gInPc2	útes
s	s	k7c7	s
prudšími	prudký	k2eAgInPc7d2	prudší
svahy	svah	k1gInPc7	svah
a	a	k8xC	a
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
okolo	okolo	k7c2	okolo
108	[number]	k4	108
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
224,65	[number]	k4	224,65
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
po	po	k7c6	po
eliptické	eliptický	k2eAgFnSc6d1	eliptická
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nejvíce	hodně	k6eAd3	hodně
blízká	blízký	k2eAgFnSc1d1	blízká
kruhové	kruhový	k2eAgInPc1d1	kruhový
s	s	k7c7	s
excentricitou	excentricita	k1gFnSc7	excentricita
dráhy	dráha	k1gFnSc2	dráha
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
0,01	[number]	k4	0,01
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pohybu	pohyb	k1gInSc2	pohyb
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
Venuše	Venuše	k1gFnSc1	Venuše
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
každých	každý	k3xTgInPc2	každý
584	[number]	k4	584
dní	den	k1gInPc2	den
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
41	[number]	k4	41
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Venuše	Venuše	k1gFnSc1	Venuše
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vlastní	vlastní	k2eAgInSc4d1	vlastní
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
obíhá	obíhat	k5eAaImIp3nS	obíhat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
tzv.	tzv.	kA	tzv.
kvazisatelit	kvazisatelit	k5eAaImF	kvazisatelit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
planetku	planetka	k1gFnSc4	planetka
2002	[number]	k4	2002
VE	v	k7c4	v
<g/>
68	[number]	k4	68
<g/>
,	,	kIx,	,
objevenou	objevený	k2eAgFnSc4d1	objevená
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
kombinaci	kombinace	k1gFnSc3	kombinace
tvaru	tvar	k1gInSc2	tvar
její	její	k3xOp3gFnSc2	její
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
shodné	shodný	k2eAgFnSc2d1	shodná
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
planetka	planetka	k1gFnSc1	planetka
obíhala	obíhat	k5eAaImAgFnS	obíhat
retrográdně	retrográdně	k6eAd1	retrográdně
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nové	nový	k2eAgFnSc2d1	nová
studie	studie	k1gFnSc2	studie
Alexe	Alex	k1gMnSc5	Alex
Alemiho	Alemi	k1gMnSc2	Alemi
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Stevensona	Stevenson	k1gMnSc2	Stevenson
z	z	k7c2	z
California	Californium	k1gNnSc2	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc4	technolog
měla	mít	k5eAaImAgFnS	mít
Venuše	Venuše	k1gFnSc1	Venuše
před	před	k7c7	před
miliardou	miliarda	k4xCgFnSc7	miliarda
let	léto	k1gNnPc2	léto
nejspíše	nejspíše	k9	nejspíše
jeden	jeden	k4xCgInSc4	jeden
velký	velký	k2eAgInSc4d1	velký
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
důsledkem	důsledek	k1gInSc7	důsledek
obrovského	obrovský	k2eAgInSc2d1	obrovský
impaktu	impakt	k1gInSc2	impakt
cizího	cizí	k2eAgNnSc2d1	cizí
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dalšího	další	k2eAgInSc2d1	další
velkého	velký	k2eAgInSc2d1	velký
impaktu	impakt	k1gInSc2	impakt
po	po	k7c6	po
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionech	milion	k4xCgInPc6	milion
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
rotace	rotace	k1gFnSc2	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
ve	v	k7c6	v
ztrátě	ztráta	k1gFnSc6	ztráta
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
nejpomaleji	pomale	k6eAd3	pomale
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
jednou	jednou	k6eAd1	jednou
za	za	k7c2	za
243,16	[number]	k4	243,16
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pomalou	pomalý	k2eAgFnSc4d1	pomalá
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rotuje	rotovat	k5eAaImIp3nS	rotovat
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
namísto	namísto	k7c2	namísto
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
planeta	planeta	k1gFnSc1	planeta
rotuje	rotovat	k5eAaImIp3nS	rotovat
zpětně	zpětně	k6eAd1	zpětně
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
následek	následek	k1gInSc4	následek
slapového	slapový	k2eAgNnSc2d1	slapové
působení	působení	k1gNnSc2	působení
její	její	k3xOp3gFnSc2	její
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnSc2d1	hmotná
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
zpětné	zpětný	k2eAgFnSc2d1	zpětná
rotace	rotace	k1gFnSc2	rotace
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
rotace	rotace	k1gFnSc1	rotace
Venuše	Venuše	k1gFnSc2	Venuše
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
synchronizovaná	synchronizovaný	k2eAgFnSc1d1	synchronizovaná
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
přiblížení	přiblížení	k1gNnSc2	přiblížení
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
otáčí	otáčet	k5eAaImIp3nS	otáčet
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc7d1	stejná
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
Venušinu	Venušin	k2eAgFnSc4d1	Venušina
rotaci	rotace	k1gFnSc4	rotace
kdykoliv	kdykoliv	k6eAd1	kdykoliv
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
dostanou	dostat	k5eAaPmIp3nP	dostat
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
shodu	shoda	k1gFnSc4	shoda
náhod	náhoda	k1gFnPc2	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
je	být	k5eAaImIp3nS	být
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc4	den
kratší	krátký	k2eAgInSc4d2	kratší
než	než	k8xS	než
siderický	siderický	k2eAgInSc4d1	siderický
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Slunce	slunce	k1gNnSc2	slunce
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
východ	východ	k1gInSc1	východ
Slunce	slunce	k1gNnSc2	slunce
objeví	objevit	k5eAaPmIp3nS	objevit
každých	každý	k3xTgNnPc2	každý
116,75	[number]	k4	116,75
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pozorování	pozorování	k1gNnSc2	pozorování
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
nejjasnějším	jasný	k2eAgInSc7d3	nejjasnější
objektem	objekt	k1gInSc7	objekt
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
uvnitř	uvnitř	k7c2	uvnitř
zemské	zemský	k2eAgFnSc2d1	zemská
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
vzdálit	vzdálit	k5eAaPmF	vzdálit
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
47	[number]	k4	47
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
-	-	kIx~	-
magnitudy	magnitudy	k6eAd1	magnitudy
do	do	k7c2	do
-4,4	-4,4	k4	-4,4
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
osvětlených	osvětlený	k2eAgNnPc2d1	osvětlené
25	[number]	k4	25
%	%	kIx~	%
jejího	její	k3xOp3gInSc2	její
kotouče	kotouč	k1gInSc2	kotouč
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
zpravidla	zpravidla	k6eAd1	zpravidla
dochází	docházet	k5eAaImIp3nS	docházet
37	[number]	k4	37
dní	den	k1gInPc2	den
před	před	k7c7	před
dolní	dolní	k2eAgFnSc7d1	dolní
konjunkcí	konjunkce	k1gFnSc7	konjunkce
na	na	k7c6	na
večerní	večerní	k2eAgFnSc6d1	večerní
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
37	[number]	k4	37
dní	den	k1gInPc2	den
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
na	na	k7c6	na
ranní	ranní	k2eAgFnSc6d1	ranní
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
až	až	k9	až
15	[number]	k4	15
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
Sirius	Sirius	k1gMnSc1	Sirius
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
nejvíc	hodně	k6eAd3	hodně
vychýlí	vychýlit	k5eAaPmIp3nS	vychýlit
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
dní	den	k1gInPc2	den
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
dolní	dolní	k2eAgFnSc6d1	dolní
konjunkcí	konjunkce	k1gFnSc7	konjunkce
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poloviční	poloviční	k2eAgFnSc6d1	poloviční
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dvou	dva	k4xCgInPc6	dva
intervalech	interval	k1gInPc6	interval
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
viditelná	viditelný	k2eAgFnSc1d1	viditelná
i	i	k9	i
za	za	k7c2	za
plného	plný	k2eAgNnSc2d1	plné
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
přesně	přesně	k6eAd1	přesně
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
podívat	podívat	k5eAaImF	podívat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
i	i	k8xC	i
Venuše	Venuše	k1gFnPc1	Venuše
se	se	k3xPyFc4	se
na	na	k7c6	na
svojí	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
při	při	k7c6	při
pozorování	pozorování	k1gNnPc2	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
zdánlivě	zdánlivě	k6eAd1	zdánlivě
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
pak	pak	k6eAd1	pak
postupuje	postupovat	k5eAaImIp3nS	postupovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Perioda	perioda	k1gFnSc1	perioda
opačného	opačný	k2eAgInSc2d1	opačný
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
dní	den	k1gInPc2	den
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
dolní	dolní	k2eAgFnSc6d1	dolní
konjunkci	konjunkce	k1gFnSc6	konjunkce
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pohybu	pohyb	k1gInSc2	pohyb
přechází	přecházet	k5eAaImIp3nS	přecházet
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výzkum	výzkum	k1gInSc1	výzkum
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
automatická	automatický	k2eAgFnSc1d1	automatická
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
a	a	k8xC	a
současně	současně	k6eAd1	současně
první	první	k4xOgFnSc1	první
meziplanetární	meziplanetární	k2eAgFnSc1d1	meziplanetární
sonda	sonda	k1gFnSc1	sonda
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
byla	být	k5eAaImAgFnS	být
Veněra	Veněra	k1gFnSc1	Veněra
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
vyslána	vyslán	k2eAgFnSc1d1	vyslána
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sonda	sonda	k1gFnSc1	sonda
z	z	k7c2	z
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
sovětského	sovětský	k2eAgInSc2d1	sovětský
programu	program	k1gInSc2	program
Veněra	Veněra	k1gFnSc1	Veněra
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
dopadovou	dopadový	k2eAgFnSc4d1	dopadová
trajektorii	trajektorie	k1gFnSc4	trajektorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	se	k3xPyFc4	se
sondou	sonda	k1gFnSc7	sonda
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
dnech	den	k1gInPc6	den
ztracen	ztracen	k2eAgInSc4d1	ztracen
rádiový	rádiový	k2eAgInSc4d1	rádiový
kontakt	kontakt	k1gInSc4	kontakt
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
dráhy	dráha	k1gFnSc2	dráha
sondy	sonda	k1gFnSc2	sonda
vypočítáno	vypočítat	k5eAaPmNgNnS	vypočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proletěla	proletět	k5eAaPmAgFnS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
průběh	průběh	k1gInSc1	průběh
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
začátek	začátek	k1gInSc1	začátek
amerického	americký	k2eAgInSc2d1	americký
průzkumného	průzkumný	k2eAgInSc2d1	průzkumný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
startu	start	k1gInSc2	start
byla	být	k5eAaImAgFnS	být
ztracena	ztracen	k2eAgFnSc1d1	ztracena
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
2	[number]	k4	2
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
velikého	veliký	k2eAgInSc2d1	veliký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
108	[number]	k4	108
dnech	den	k1gInPc6	den
doletěla	doletět	k5eAaPmAgFnS	doletět
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
první	první	k4xOgFnSc7	první
lidskou	lidský	k2eAgFnSc7d1	lidská
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Mariner	Mariner	k1gInSc1	Mariner
2	[number]	k4	2
proletěl	proletět	k5eAaPmAgInS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
34	[number]	k4	34
833	[number]	k4	833
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
mikrovlnného	mikrovlnný	k2eAgInSc2d1	mikrovlnný
a	a	k8xC	a
infračerveného	infračervený	k2eAgInSc2d1	infračervený
radiometru	radiometr	k1gInSc2	radiometr
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
svrchní	svrchní	k2eAgFnPc4d1	svrchní
oblasti	oblast	k1gFnPc4	oblast
mračen	mračno	k1gNnPc2	mračno
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
chladná	chladný	k2eAgNnPc1d1	chladné
<g/>
,	,	kIx,	,
a	a	k8xC	a
povrch	povrch	k1gInSc4	povrch
s	s	k7c7	s
extrémní	extrémní	k2eAgFnSc7d1	extrémní
teplotou	teplota	k1gFnSc7	teplota
okolo	okolo	k7c2	okolo
425	[number]	k4	425
°	°	k?	°
<g/>
C.	C.	kA	C.
Měření	měření	k1gNnSc4	měření
sondy	sonda	k1gFnSc2	sonda
tak	tak	k6eAd1	tak
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
dřívější	dřívější	k2eAgInPc4d1	dřívější
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
horký	horký	k2eAgMnSc1d1	horký
a	a	k8xC	a
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
příhodné	příhodný	k2eAgFnPc4d1	příhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
sondy	sonda	k1gFnSc2	sonda
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
současně	současně	k6eAd1	současně
odhadnout	odhadnout	k5eAaPmF	odhadnout
hmotnost	hmotnost	k1gFnSc4	hmotnost
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
změřit	změřit	k5eAaPmF	změřit
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
proletěla	proletět	k5eAaPmAgFnS	proletět
atmosférou	atmosféra	k1gFnSc7	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Veněra	Veněra	k1gFnSc1	Veněra
3	[number]	k4	3
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
-	-	kIx~	-
sonda	sonda	k1gFnSc1	sonda
se	se	k3xPyFc4	se
zřítila	zřítit	k5eAaPmAgFnS	zřítit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poruchu	porucha	k1gFnSc4	porucha
komunikačního	komunikační	k2eAgInSc2d1	komunikační
systému	systém	k1gInSc2	systém
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
odeslat	odeslat	k5eAaPmF	odeslat
žádná	žádný	k3yNgNnPc4	žádný
data	datum	k1gNnPc4	datum
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
Venuše	Venuše	k1gFnSc2	Venuše
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Veněra	Veněra	k1gFnSc1	Veněra
4	[number]	k4	4
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
odeslala	odeslat	k5eAaPmAgFnS	odeslat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
Veněry	Veněra	k1gFnSc2	Veněra
4	[number]	k4	4
současně	současně	k6eAd1	současně
vyvrátilo	vyvrátit	k5eAaPmAgNnS	vyvrátit
předchozí	předchozí	k2eAgNnSc4d1	předchozí
teplotní	teplotní	k2eAgNnSc4d1	teplotní
měření	měření	k1gNnSc4	měření
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc4	Mariner
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
zjistila	zjistit	k5eAaPmAgFnS	zjistit
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
složení	složení	k1gNnSc2	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
90	[number]	k4	90
až	až	k9	až
95	[number]	k4	95
%	%	kIx~	%
tvořena	tvořit	k5eAaImNgFnS	tvořit
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
byla	být	k5eAaImAgFnS	být
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
sovětští	sovětský	k2eAgMnPc1d1	sovětský
konstruktéři	konstruktér	k1gMnPc1	konstruktér
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
sonda	sonda	k1gFnSc1	sonda
rozměrnější	rozměrný	k2eAgFnSc1d2	rozměrnější
padák	padák	k1gInSc1	padák
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
silněji	silně	k6eAd2	silně
brzděna	brzděn	k2eAgFnSc1d1	brzděna
a	a	k8xC	a
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
padala	padat	k5eAaImAgFnS	padat
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
baterie	baterie	k1gFnSc1	baterie
vyprázdnila	vyprázdnit	k5eAaPmAgFnS	vyprázdnit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
sondy	sonda	k1gFnSc2	sonda
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
signálu	signál	k1gInSc2	signál
vysílala	vysílat	k5eAaImAgFnS	vysílat
sonda	sonda	k1gFnSc1	sonda
93	[number]	k4	93
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
telemetrie	telemetrie	k1gFnSc1	telemetrie
sondy	sonda	k1gFnSc2	sonda
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolní	okolní	k2eAgInSc4d1	okolní
tlak	tlak	k1gInSc4	tlak
kolem	kolem	k7c2	kolem
sondy	sonda	k1gFnSc2	sonda
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
baru	bar	k1gInSc2	bar
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
24,96	[number]	k4	24,96
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
-	-	kIx~	-
Mariner	Mariner	k1gInSc1	Mariner
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proletěl	proletět	k5eAaPmAgInS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
000	[number]	k4	000
km	km	kA	km
nad	nad	k7c4	nad
vrcholky	vrcholek	k1gInPc4	vrcholek
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Mariner	Mariner	k1gInSc4	Mariner
5	[number]	k4	5
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
připravována	připravován	k2eAgFnSc1d1	připravována
jako	jako	k8xS	jako
záložní	záložní	k2eAgFnSc1d1	záložní
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
sonda	sonda	k1gFnSc1	sonda
Marineru	Mariner	k1gMnSc3	Mariner
4	[number]	k4	4
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
předchozí	předchozí	k2eAgFnSc2d1	předchozí
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc4	Mariner
2	[number]	k4	2
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
její	její	k3xOp3gInSc1	její
cíl	cíl	k1gInSc1	cíl
změněn	změnit	k5eAaPmNgInS	změnit
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
také	také	k9	také
vyslána	vyslat	k5eAaPmNgFnS	vyslat
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
vybavení	vybavení	k1gNnSc1	vybavení
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
lepším	dobrý	k2eAgInSc6d2	lepší
technickém	technický	k2eAgInSc6d1	technický
stupni	stupeň	k1gInSc6	stupeň
s	s	k7c7	s
citlivější	citlivý	k2eAgFnSc7d2	citlivější
aparaturou	aparatura	k1gFnSc7	aparatura
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předchozí	předchozí	k2eAgFnSc2d1	předchozí
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc1	Mariner
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
získat	získat	k5eAaPmF	získat
lepší	dobrý	k2eAgNnPc4d2	lepší
vědecká	vědecký	k2eAgNnPc4d1	vědecké
data	datum	k1gNnPc4	datum
ohledně	ohledně	k7c2	ohledně
složení	složení	k1gNnSc2	složení
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
téměř	téměř	k6eAd1	téměř
současnému	současný	k2eAgInSc3d1	současný
příletu	přílet	k1gInSc3	přílet
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc1	Mariner
5	[number]	k4	5
a	a	k8xC	a
Veněry	Veněr	k1gInPc1	Veněr
4	[number]	k4	4
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
porovnat	porovnat	k5eAaPmF	porovnat
získaná	získaný	k2eAgNnPc4d1	získané
data	datum	k1gNnPc4	datum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sovětsko-americké	sovětskomerický	k2eAgFnSc2d1	sovětsko-americká
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
náznak	náznak	k1gInSc1	náznak
budoucí	budoucí	k2eAgFnSc2d1	budoucí
kooperace	kooperace	k1gFnSc2	kooperace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
kosmického	kosmický	k2eAgInSc2d1	kosmický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
konstrukcí	konstrukce	k1gFnSc7	konstrukce
sondy	sonda	k1gFnSc2	sonda
Veněry	Veněra	k1gFnSc2	Veněra
4	[number]	k4	4
a	a	k8xC	a
novými	nový	k2eAgNnPc7d1	nové
daty	datum	k1gNnPc7	datum
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
novou	nový	k2eAgFnSc4d1	nová
dvojici	dvojice	k1gFnSc4	dvojice
stejných	stejný	k2eAgFnPc2d1	stejná
sond	sonda	k1gFnPc2	sonda
Veněra	Veněra	k1gFnSc1	Veněra
5	[number]	k4	5
a	a	k8xC	a
Veněra	Veněra	k1gFnSc1	Veněra
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Sondy	sonda	k1gFnPc1	sonda
byly	být	k5eAaImAgFnP	být
vyslány	vyslat	k5eAaPmNgFnP	vyslat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
po	po	k7c6	po
sobě	se	k3xPyFc3	se
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
příletu	přílet	k1gInSc2	přílet
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sond	sonda	k1gFnPc2	sonda
byl	být	k5eAaImAgInS	být
posílen	posílit	k5eAaPmNgInS	posílit
jejich	jejich	k3xOp3gInSc1	jejich
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odolal	odolat	k5eAaPmAgMnS	odolat
atmosférickému	atmosférický	k2eAgMnSc3d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
25	[number]	k4	25
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
padáku	padák	k1gInSc2	padák
umožňující	umožňující	k2eAgInSc4d1	umožňující
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
sestup	sestup	k1gInSc4	sestup
skrz	skrz	k7c4	skrz
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
model	model	k1gInSc4	model
Venuše	Venuše	k1gFnSc2	Venuše
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
panují	panovat	k5eAaImIp3nP	panovat
tlaky	tlak	k1gInPc1	tlak
mezi	mezi	k7c7	mezi
75	[number]	k4	75
až	až	k9	až
100	[number]	k4	100
atmosférami	atmosféra	k1gFnPc7	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nepředpokládalo	předpokládat	k5eNaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
sondy	sonda	k1gFnPc1	sonda
mohly	moct	k5eAaImAgFnP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
sondy	sonda	k1gFnSc2	sonda
vysílaly	vysílat	k5eAaImAgInP	vysílat
data	datum	k1gNnPc4	datum
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
dvě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
přestaly	přestat	k5eAaPmAgFnP	přestat
vysílat	vysílat	k5eAaImF	vysílat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vlivem	vlivem	k7c2	vlivem
extrémního	extrémní	k2eAgInSc2d1	extrémní
tlaku	tlak	k1gInSc2	tlak
zničeny	zničit	k5eAaPmNgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Veněra	Veněra	k1gFnSc1	Veněra
7	[number]	k4	7
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
povrchu	povrch	k1gInSc3	povrch
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
cílem	cíl	k1gInSc7	cíl
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgInP	provést
i	i	k9	i
konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
úpravy	úprava	k1gFnPc1	úprava
na	na	k7c6	na
přistávacím	přistávací	k2eAgInSc6d1	přistávací
modulu	modul	k1gInSc6	modul
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
schopný	schopný	k2eAgMnSc1d1	schopný
přežít	přežít	k5eAaPmF	přežít
tlak	tlak	k1gInSc4	tlak
180	[number]	k4	180
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
aparatura	aparatura	k1gFnSc1	aparatura
podchlazena	podchlazen	k2eAgFnSc1d1	podchlazena
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
kvůli	kvůli	k7c3	kvůli
prodloužení	prodloužení	k1gNnSc3	prodloužení
její	její	k3xOp3gFnSc2	její
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
započala	započnout	k5eAaPmAgFnS	započnout
svůj	svůj	k3xOyFgInSc4	svůj
sestup	sestup	k1gInSc4	sestup
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využila	využít	k5eAaPmAgFnS	využít
tepelný	tepelný	k2eAgInSc4d1	tepelný
štít	štít	k1gInSc4	štít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
následně	následně	k6eAd1	následně
otevřela	otevřít	k5eAaPmAgFnS	otevřít
speciálně	speciálně	k6eAd1	speciálně
upravený	upravený	k2eAgInSc4d1	upravený
padák	padák	k1gInSc4	padák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
umožnit	umožnit	k5eAaPmF	umožnit
rychlý	rychlý	k2eAgInSc1d1	rychlý
průlet	průlet	k1gInSc1	průlet
skrz	skrz	k7c4	skrz
atmosféru	atmosféra	k1gFnSc4	atmosféra
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
agresivního	agresivní	k2eAgNnSc2d1	agresivní
prostředí	prostředí	k1gNnSc2	prostředí
okolní	okolní	k2eAgFnSc2d1	okolní
atmosféry	atmosféra	k1gFnSc2	atmosféra
ale	ale	k8xC	ale
padák	padák	k1gInSc1	padák
nezůstal	zůstat	k5eNaPmAgInS	zůstat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
a	a	k8xC	a
tak	tak	k6eAd1	tak
sonda	sonda	k1gFnSc1	sonda
několik	několik	k4yIc4	několik
posledních	poslední	k2eAgInPc2d1	poslední
metrů	metr	k1gInPc2	metr
padala	padat	k5eAaImAgFnS	padat
volným	volný	k2eAgInSc7d1	volný
pádem	pád	k1gInSc7	pád
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
dopad	dopad	k1gInSc1	dopad
částečně	částečně	k6eAd1	částečně
poškodil	poškodit	k5eAaPmAgInS	poškodit
sondu	sonda	k1gFnSc4	sonda
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
chvíli	chvíle	k1gFnSc6	chvíle
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
zachycení	zachycení	k1gNnSc4	zachycení
jejího	její	k3xOp3gInSc2	její
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
analyzování	analyzování	k1gNnSc2	analyzování
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
šumu	šum	k1gInSc2	šum
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vědci	vědec	k1gMnPc1	vědec
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
objevit	objevit	k5eAaPmF	objevit
slabý	slabý	k2eAgInSc4d1	slabý
signál	signál	k1gInSc4	signál
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydržel	vydržet	k5eAaPmAgMnS	vydržet
23	[number]	k4	23
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
první	první	k4xOgFnSc4	první
telemetrii	telemetrie	k1gFnSc4	telemetrie
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
povrchu	povrch	k1gInSc2	povrch
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
program	program	k1gInSc4	program
Veněra	Veněra	k1gFnSc1	Veněra
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
vysláním	vyslání	k1gNnSc7	vyslání
sondy	sonda	k1gFnSc2	sonda
Veněra	Veněra	k1gFnSc1	Veněra
8	[number]	k4	8
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vysílala	vysílat	k5eAaImAgFnS	vysílat
data	datum	k1gNnPc1	datum
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
sondy	sonda	k1gFnPc1	sonda
Veněra	Veněra	k1gFnSc1	Veněra
9	[number]	k4	9
a	a	k8xC	a
Veněra	Veněra	k1gFnSc1	Veněra
10	[number]	k4	10
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zaslaly	zaslat	k5eAaPmAgInP	zaslat
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
první	první	k4xOgFnSc2	první
snímky	snímka	k1gFnSc2	snímka
povrchu	povrch	k1gInSc2	povrch
Venuše	Venuše	k1gFnSc2	Venuše
ukazující	ukazující	k2eAgFnSc4d1	ukazující
nehostinnou	hostinný	k2eNgFnSc4d1	nehostinná
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
různá	různý	k2eAgNnPc4d1	různé
místa	místo	k1gNnPc4	místo
přistání	přistání	k1gNnSc2	přistání
zachytila	zachytit	k5eAaPmAgFnS	zachytit
zcela	zcela	k6eAd1	zcela
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
krajinu	krajina	k1gFnSc4	krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
míst	místo	k1gNnPc2	místo
modulů	modul	k1gInPc2	modul
-	-	kIx~	-
Veněra	Veněra	k1gFnSc1	Veněra
9	[number]	k4	9
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
o	o	k7c6	o
sklonu	sklon	k1gInSc6	sklon
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
°	°	k?	°
obklopeného	obklopený	k2eAgMnSc2d1	obklopený
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
velkými	velký	k2eAgInPc7d1	velký
balvany	balvan	k1gInPc7	balvan
<g/>
;	;	kIx,	;
Veněra	Veněra	k1gFnSc1	Veněra
10	[number]	k4	10
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
krajinu	krajina	k1gFnSc4	krajina
připomínající	připomínající	k2eAgFnSc2d1	připomínající
čedičové	čedičový	k2eAgFnSc2d1	čedičová
desky	deska	k1gFnSc2	deska
se	s	k7c7	s
zvětralým	zvětralý	k2eAgInSc7d1	zvětralý
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
sondu	sonda	k1gFnSc4	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
primárně	primárně	k6eAd1	primárně
směřovala	směřovat	k5eAaImAgFnS	směřovat
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
u	u	k7c2	u
Venuše	Venuše	k1gFnSc2	Venuše
provedla	provést	k5eAaPmAgFnS	provést
pouze	pouze	k6eAd1	pouze
gravitační	gravitační	k2eAgInSc4d1	gravitační
manévr	manévr	k1gInSc4	manévr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1974	[number]	k4	1974
sonda	sonda	k1gFnSc1	sonda
pořídila	pořídit	k5eAaPmAgFnS	pořídit
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
planety	planeta	k1gFnSc2	planeta
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
pouhých	pouhý	k2eAgInPc2d1	pouhý
5	[number]	k4	5
790	[number]	k4	790
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc4	fotografia
představují	představovat	k5eAaImIp3nP	představovat
záběry	záběr	k1gInPc1	záběr
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgFnSc3d2	lepší
kvalitě	kvalita	k1gFnSc3	kvalita
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelné	viditelný	k2eAgFnSc2d1	viditelná
části	část	k1gFnSc2	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
poslala	poslat	k5eAaPmAgFnS	poslat
NASA	NASA	kA	NASA
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
dvě	dva	k4xCgFnPc4	dva
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
Pioneer	Pioneer	kA	Pioneer
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
mise	mise	k1gFnSc1	mise
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
dopravována	dopravovat	k5eAaImNgFnS	dopravovat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
:	:	kIx,	:
Orbiter	Orbiter	k1gInSc1	Orbiter
(	(	kIx(	(
<g/>
oběžnice	oběžnice	k1gFnSc1	oběžnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Multiprobe	Multiprob	k1gInSc5	Multiprob
(	(	kIx(	(
<g/>
multisonda	multisonda	k1gFnSc1	multisonda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
Pioneer	Pioneer	kA	Pioneer
Venus	Venus	k1gInSc1	Venus
Multiprobe	Multiprob	k1gInSc5	Multiprob
nesla	nést	k5eAaImAgFnS	nést
jednu	jeden	k4xCgFnSc4	jeden
velkou	velká	k1gFnSc4	velká
a	a	k8xC	a
3	[number]	k4	3
malé	malý	k2eAgFnSc2d1	malá
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
sondy	sonda	k1gFnSc2	sonda
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
velká	velký	k2eAgFnSc1d1	velká
sonda	sonda	k1gFnSc1	sonda
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
tři	tři	k4xCgFnPc4	tři
menší	malý	k2eAgFnPc4d2	menší
sondy	sonda	k1gFnPc4	sonda
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
sondy	sonda	k1gFnPc1	sonda
do	do	k7c2	do
Venušiny	Venušin	k2eAgFnSc2d1	Venušina
atmosféry	atmosféra	k1gFnSc2	atmosféra
následovány	následovat	k5eAaImNgFnP	následovat
přenosovým	přenosový	k2eAgNnSc7d1	přenosové
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
neočekávalo	očekávat	k5eNaImAgNnS	očekávat
přežití	přežití	k1gNnSc1	přežití
po	po	k7c6	po
sestupu	sestup	k1gInSc6	sestup
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
ještě	ještě	k9	ještě
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1978	[number]	k4	1978
přešel	přejít	k5eAaPmAgMnS	přejít
Pioneer	Pioneer	kA	Pioneer
Venus	Venus	k1gMnSc1	Venus
Orbiter	Orbiter	k1gMnSc1	Orbiter
na	na	k7c4	na
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
17	[number]	k4	17
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
mu	on	k3xPp3gNnSc3	on
nedošlo	dojít	k5eNaPmAgNnS	dojít
palivo	palivo	k1gNnSc1	palivo
stabilizující	stabilizující	k2eAgNnSc1d1	stabilizující
jeho	jeho	k3xOp3gFnSc4	jeho
orbitu	orbita	k1gFnSc4	orbita
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1992	[number]	k4	1992
zničen	zničit	k5eAaPmNgInS	zničit
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
dorazily	dorazit	k5eAaPmAgInP	dorazit
poslední	poslední	k2eAgInPc1d1	poslední
4	[number]	k4	4
sondy	sonda	k1gFnSc2	sonda
z	z	k7c2	z
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
programu	program	k1gInSc2	program
Veněra	Veněra	k1gFnSc1	Veněra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sond	sonda	k1gFnPc2	sonda
Veněra	Veněra	k1gFnSc1	Veněra
11	[number]	k4	11
a	a	k8xC	a
Veněra	Veněra	k1gFnSc1	Veněra
12	[number]	k4	12
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
elektrické	elektrický	k2eAgInPc4d1	elektrický
výboje	výboj	k1gInPc4	výboj
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
blesků	blesk	k1gInPc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
sondy	sonda	k1gFnPc1	sonda
Veněra	Veněra	k1gFnSc1	Veněra
13	[number]	k4	13
a	a	k8xC	a
Veněra	Veněra	k1gFnSc1	Veněra
14	[number]	k4	14
provedly	provést	k5eAaPmAgFnP	provést
přistání	přistání	k1gNnSc4	přistání
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
zaslaly	zaslat	k5eAaPmAgInP	zaslat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
první	první	k4xOgFnSc2	první
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňované	zmiňovaný	k2eAgFnPc1d1	zmiňovaná
sondy	sonda	k1gFnPc1	sonda
současně	současně	k6eAd1	současně
prozkoumaly	prozkoumat	k5eAaPmAgFnP	prozkoumat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
fluorescence	fluorescence	k1gFnSc2	fluorescence
vzorky	vzorek	k1gInPc1	vzorek
zeminy	zemina	k1gFnSc2	zemina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
hodnoty	hodnota	k1gFnPc4	hodnota
podobné	podobný	k2eAgFnPc4d1	podobná
čedičové	čedičový	k2eAgFnSc2d1	čedičová
hornině	hornina	k1gFnSc6	hornina
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
draslík	draslík	k1gInSc4	draslík
<g/>
.	.	kIx.	.
</s>
<s>
Zakončením	zakončení	k1gNnSc7	zakončení
programu	program	k1gInSc2	program
Veněra	Veněra	k1gFnSc1	Veněra
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vyslání	vyslání	k1gNnSc1	vyslání
sond	sonda	k1gFnPc2	sonda
Veněra	Veněra	k1gFnSc1	Veněra
15	[number]	k4	15
a	a	k8xC	a
Veněra	Veněra	k1gFnSc1	Veněra
16	[number]	k4	16
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
navedeny	navést	k5eAaPmNgInP	navést
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
orbitu	orbita	k1gFnSc4	orbita
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
radarového	radarový	k2eAgNnSc2d1	radarové
mapování	mapování	k1gNnSc2	mapování
severní	severní	k2eAgFnSc2d1	severní
třetiny	třetina	k1gFnSc2	třetina
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
využili	využít	k5eAaPmAgMnP	využít
sovětští	sovětský	k2eAgMnPc1d1	sovětský
konstruktéři	konstruktér	k1gMnPc1	konstruktér
možnosti	možnost	k1gFnPc4	možnost
zkombinovat	zkombinovat	k5eAaPmF	zkombinovat
misi	mise	k1gFnSc4	mise
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
s	s	k7c7	s
průzkumem	průzkum	k1gInSc7	průzkum
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prolétala	prolétat	k5eAaPmAgFnS	prolétat
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
z	z	k7c2	z
programu	program	k1gInSc2	program
Vega	Veg	k1gInSc2	Veg
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
doletěly	doletět	k5eAaPmAgFnP	doletět
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
po	po	k7c6	po
devítiměsíčním	devítiměsíční	k2eAgInSc6d1	devítiměsíční
výzkumu	výzkum	k1gInSc6	výzkum
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
odpoutaly	odpoutat	k5eAaPmAgFnP	odpoutat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kometě	kometa	k1gFnSc3	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Magellan	Magellany	k1gInPc2	Magellany
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
provést	provést	k5eAaPmF	provést
podrobné	podrobný	k2eAgNnSc4d1	podrobné
zmapování	zmapování	k1gNnSc4	zmapování
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
<s>
Pořízené	pořízený	k2eAgInPc1d1	pořízený
snímky	snímek	k1gInPc1	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
byly	být	k5eAaImAgFnP	být
fotografovány	fotografovat	k5eAaImNgFnP	fotografovat
během	během	k7c2	během
mise	mise	k1gFnSc2	mise
trvající	trvající	k2eAgFnSc2d1	trvající
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
mise	mise	k1gFnSc2	mise
zcela	zcela	k6eAd1	zcela
překonala	překonat	k5eAaPmAgFnS	překonat
očekávání	očekávání	k1gNnSc3	očekávání
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
přes	přes	k7c4	přes
98	[number]	k4	98
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
pomocí	pomocí	k7c2	pomocí
radaru	radar	k1gInSc2	radar
a	a	k8xC	a
zmapovat	zmapovat	k5eAaPmF	zmapovat
95	[number]	k4	95
%	%	kIx~	%
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
sondy	sonda	k1gFnSc2	sonda
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
byla	být	k5eAaImAgFnS	být
navedena	navést	k5eAaPmNgFnS	navést
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
ještě	ještě	k9	ještě
sondami	sonda	k1gFnPc7	sonda
Galileo	Galilea	k1gFnSc5	Galilea
a	a	k8xC	a
Cassini	Cassin	k2eAgMnPc1d1	Cassin
během	během	k7c2	během
průletů	průlet	k1gInPc2	průlet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sekundární	sekundární	k2eAgInPc4d1	sekundární
cíle	cíl	k1gInPc4	cíl
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výzkumů	výzkum	k1gInPc2	výzkum
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
sond	sonda	k1gFnPc2	sonda
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
tělesům	těleso	k1gNnPc3	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
proletěla	proletět	k5eAaPmAgFnS	proletět
kolem	kolem	k6eAd1	kolem
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
během	během	k7c2	během
korekce	korekce	k1gFnSc2	korekce
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
Merkuru	Merkur	k1gInSc3	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
obíhá	obíhat	k5eAaImIp3nS	obíhat
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Venus	Venus	k1gMnSc1	Venus
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
orbitu	orbita	k1gFnSc4	orbita
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
atmosféry	atmosféra	k1gFnSc2	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
mraků	mrak	k1gInPc2	mrak
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
prostředí	prostředí	k1gNnSc2	prostředí
planetární	planetární	k2eAgFnSc2d1	planetární
fyziky	fyzika	k1gFnSc2	fyzika
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
povrchové	povrchový	k2eAgFnSc2d1	povrchová
charakteristiky	charakteristika	k1gFnSc2	charakteristika
a	a	k8xC	a
měření	měření	k1gNnSc2	měření
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
na	na	k7c4	na
500	[number]	k4	500
pozemských	pozemský	k2eAgInPc2d1	pozemský
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
venušské	venušský	k2eAgInPc4d1	venušský
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úspěchem	úspěch	k1gInSc7	úspěch
Venus	Venus	k1gInSc1	Venus
Express	express	k1gInSc4	express
bylo	být	k5eAaImAgNnS	být
objevení	objevení	k1gNnSc1	objevení
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
cirkumpolárního	cirkumpolární	k2eAgInSc2d1	cirkumpolární
víru	vír	k1gInSc2	vír
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Venuše	Venuše	k1gFnSc2	Venuše
sonda	sonda	k1gFnSc1	sonda
Akacuki	Akacuk	k1gFnSc2	Akacuk
japonské	japonský	k2eAgFnSc2d1	japonská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
přístroje	přístroj	k1gInPc1	přístroj
jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
připravuje	připravovat	k5eAaImIp3nS	připravovat
misi	mise	k1gFnSc4	mise
BepiColombo	BepiColomba	k1gFnSc5	BepiColomba
k	k	k7c3	k
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
proletět	proletět	k5eAaPmF	proletět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
a	a	k8xC	a
2020	[number]	k4	2020
dvakrát	dvakrát	k6eAd1	dvakrát
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
Merkuru	Merkur	k1gInSc2	Merkur
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zaparkovat	zaparkovat	k5eAaPmF	zaparkovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2024	[number]	k4	2024
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
existence	existence	k1gFnSc1	existence
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
naznačující	naznačující	k2eAgFnSc2d1	naznačující
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
životu	život	k1gInSc3	život
stále	stále	k6eAd1	stále
přívětivé	přívětivý	k2eAgFnSc2d1	přívětivá
teploty	teplota	k1gFnSc2	teplota
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
častým	častý	k2eAgFnPc3d1	častá
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
vyspělého	vyspělý	k2eAgInSc2d1	vyspělý
života	život	k1gInSc2	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Proctor	Proctor	k1gMnSc1	Proctor
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
kratší	krátký	k2eAgFnSc3d2	kratší
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
Venuše	Venuše	k1gFnSc2	Venuše
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
stačí	stačit	k5eAaBmIp3nS	stačit
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
neobyvatelné	obyvatelný	k2eNgFnSc2d1	neobyvatelná
bytostmi	bytost	k1gFnPc7	bytost
podobnými	podobný	k2eAgFnPc7d1	podobná
pozemským	pozemský	k2eAgInPc3d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
blízkosti	blízkost	k1gFnPc1	blízkost
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
teploty	teplota	k1gFnSc2	teplota
nesnesitelné	snesitelný	k2eNgFnSc2d1	nesnesitelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mírných	mírný	k2eAgInPc6d1	mírný
a	a	k8xC	a
chladných	chladný	k2eAgInPc6d1	chladný
pásech	pás	k1gInPc6	pás
mohou	moct	k5eAaImIp3nP	moct
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
existovat	existovat	k5eAaImF	existovat
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
nám	my	k3xPp1nPc3	my
dobře	dobře	k6eAd1	dobře
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
<g/>
...	...	k?	...
Nenacházím	nacházet	k5eNaImIp1nS	nacházet
žádný	žádný	k3yNgInSc4	žádný
důvod	důvod	k1gInSc4	důvod
<g/>
...	...	k?	...
zamítnout	zamítnout	k5eAaPmF	zamítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
plná	plný	k2eAgNnPc4d1	plné
stvoření	stvoření	k1gNnPc4	stvoření
tak	tak	k9	tak
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
naopak	naopak	k6eAd1	naopak
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
skleníkovému	skleníkový	k2eAgInSc3d1	skleníkový
efektu	efekt	k1gInSc3	efekt
a	a	k8xC	a
povrchovým	povrchový	k2eAgFnPc3d1	povrchová
teplotám	teplota	k1gFnPc3	teplota
okolo	okolo	k7c2	okolo
450	[number]	k4	450
°	°	k?	°
<g/>
C	C	kA	C
nelze	lze	k6eNd1	lze
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
života	život	k1gInSc2	život
pozemského	pozemský	k2eAgInSc2d1	pozemský
typu	typ	k1gInSc2	typ
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
však	však	k9	však
Dirk	Dirk	k1gInSc1	Dirk
Schulze-Makuch	Schulze-Makuch	k1gMnSc1	Schulze-Makuch
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Irwin	Irwin	k1gMnSc1	Irwin
z	z	k7c2	z
texaské	texaský	k2eAgFnSc2d1	texaská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
El	Ela	k1gFnPc2	Ela
Paso	Paso	k6eAd1	Paso
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
životě	život	k1gInSc6	život
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
Venušině	Venušin	k2eAgInSc6d1	Venušin
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
oblacích	oblak	k1gInPc6	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
Veněra	Veněra	k1gFnSc1	Veněra
<g/>
,	,	kIx,	,
Pioneer	Pioneer	kA	Pioneer
Venus	Venus	k1gInSc4	Venus
a	a	k8xC	a
Magellan	Magellan	k1gInSc4	Magellan
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
vodních	vodní	k2eAgFnPc2d1	vodní
kapek	kapka	k1gFnPc2	kapka
ve	v	k7c6	v
venušských	venušský	k2eAgInPc6d1	venušský
mracích	mrak	k1gInPc6	mrak
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
přítomností	přítomnost	k1gFnSc7	přítomnost
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
ze	z	k7c2	z
zejména	zejména	k9	zejména
o	o	k7c4	o
současnou	současný	k2eAgFnSc4d1	současná
přítomnost	přítomnost	k1gFnSc4	přítomnost
sulfanu	sulfan	k1gInSc2	sulfan
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
dvou	dva	k4xCgNnPc2	dva
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
navzájem	navzájem	k6eAd1	navzájem
reagují	reagovat	k5eAaBmIp3nP	reagovat
a	a	k8xC	a
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc1	nějaký
jev	jev	k1gInSc1	jev
nedoplňuje	doplňovat	k5eNaImIp3nS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Poukázali	poukázat	k5eAaPmAgMnP	poukázat
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgNnSc4d1	nízké
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgNnSc2d1	uhelnatý
navzdory	navzdory	k7c3	navzdory
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
záření	záření	k1gNnSc3	záření
a	a	k8xC	a
bleskům	blesk	k1gInPc3	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
vznášejících	vznášející	k2eAgMnPc2d1	vznášející
se	se	k3xPyFc4	se
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nS	by
využívaly	využívat	k5eAaPmAgFnP	využívat
metabolizmu	metabolizmus	k1gInSc3	metabolizmus
podobného	podobný	k2eAgMnSc4d1	podobný
některým	některý	k3yIgInPc3	některý
raným	raný	k2eAgInPc3d1	raný
pozemským	pozemský	k2eAgInPc3d1	pozemský
organizmům	organizmus	k1gInPc3	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
život	život	k1gInSc1	život
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
nebyl	být	k5eNaImAgInS	být
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
složení	složení	k1gNnSc1	složení
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
čtyřmi	čtyři	k4xCgNnPc7	čtyři
miliardami	miliarda	k4xCgFnPc7	miliarda
let	let	k1gInSc4	let
Slunce	slunce	k1gNnSc1	slunce
vyzařovalo	vyzařovat	k5eAaImAgNnS	vyzařovat
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
méně	málo	k6eAd2	málo
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Země	země	k1gFnSc1	země
i	i	k8xC	i
Mars	Mars	k1gInSc4	Mars
byly	být	k5eAaImAgInP	být
zamrzlé	zamrzlý	k2eAgInPc1d1	zamrzlý
světy	svět	k1gInPc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
nejspíše	nejspíše	k9	nejspíše
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
panovaly	panovat	k5eAaImAgFnP	panovat
optimální	optimální	k2eAgFnPc1d1	optimální
teploty	teplota	k1gFnPc1	teplota
umožňující	umožňující	k2eAgFnSc4d1	umožňující
existenci	existence	k1gFnSc4	existence
oceánů	oceán	k1gInPc2	oceán
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tedy	tedy	k9	tedy
potenciální	potenciální	k2eAgNnSc1d1	potenciální
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
změny	změna	k1gFnSc2	změna
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
teoreticky	teoreticky	k6eAd1	teoreticky
mohl	moct	k5eAaImAgInS	moct
život	život	k1gInSc4	život
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
těmto	tento	k3xDgFnPc3	tento
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
obyvatelné	obyvatelný	k2eAgFnSc2d1	obyvatelná
zóny	zóna	k1gFnSc2	zóna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
extrémním	extrémní	k2eAgFnPc3d1	extrémní
podmínkám	podmínka	k1gFnPc3	podmínka
panujícím	panující	k2eAgFnPc3d1	panující
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
současným	současný	k2eAgInSc7d1	současný
stupněm	stupeň	k1gInSc7	stupeň
technologie	technologie	k1gFnSc2	technologie
trvale	trvale	k6eAd1	trvale
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Teoretické	teoretický	k2eAgFnPc1d1	teoretická
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
trvalé	trvalá	k1gFnPc4	trvalá
lidské	lidský	k2eAgFnSc3d1	lidská
posádce	posádka	k1gFnSc3	posádka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
"	"	kIx"	"
<g/>
plovoucích	plovoucí	k2eAgNnPc2d1	plovoucí
měst	město	k1gNnPc2	město
<g/>
"	"	kIx"	"
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
husté	hustý	k2eAgFnSc2d1	hustá
venušské	venušský	k2eAgFnSc2d1	venušský
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
panujících	panující	k2eAgFnPc6d1	panující
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
50	[number]	k4	50
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
úrovně	úroveň	k1gFnPc4	úroveň
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
podmínkám	podmínka	k1gFnPc3	podmínka
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
provedení	provedení	k1gNnSc3	provedení
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
využití	využití	k1gNnSc4	využití
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
zařízení	zařízení	k1gNnSc2	zařízení
lehčího	lehký	k2eAgNnSc2d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
tzv.	tzv.	kA	tzv.
aerostat	aerostat	k1gInSc1	aerostat
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bude	být	k5eAaImBp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
setrvat	setrvat	k5eAaPmF	setrvat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
nebezpečného	bezpečný	k2eNgNnSc2d1	nebezpečné
množství	množství	k1gNnSc2	množství
těkavých	těkavý	k2eAgFnPc2d1	těkavá
kyselin	kyselina	k1gFnPc2	kyselina
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
bude	být	k5eAaImBp3nS	být
klást	klást	k5eAaImF	klást
množství	množství	k1gNnSc1	množství
překážek	překážka	k1gFnPc2	překážka
a	a	k8xC	a
vystupovat	vystupovat	k5eAaImF	vystupovat
proti	proti	k7c3	proti
krátkodobému	krátkodobý	k2eAgNnSc3d1	krátkodobé
osídlení	osídlení	k1gNnSc3	osídlení
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
úvahy	úvaha	k1gFnPc1	úvaha
jdou	jít	k5eAaImIp3nP	jít
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Venuše	Venuše	k1gFnSc1	Venuše
měla	mít	k5eAaImAgFnS	mít
odstínit	odstínit	k5eAaPmF	odstínit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
speciálním	speciální	k2eAgInSc7d1	speciální
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
Venuše	Venuše	k1gFnSc1	Venuše
přestala	přestat	k5eAaPmAgFnS	přestat
přijímat	přijímat	k5eAaImF	přijímat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
ji	on	k3xPp3gFnSc4	on
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
ochlazování	ochlazování	k1gNnSc3	ochlazování
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
ochlazení	ochlazení	k1gNnSc1	ochlazení
atmosféry	atmosféra	k1gFnSc2	atmosféra
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
vypršela	vypršet	k5eAaPmAgFnS	vypršet
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
ztenčení	ztenčení	k1gNnSc3	ztenčení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
případnou	případný	k2eAgFnSc4d1	případná
terraformaci	terraformace	k1gFnSc4	terraformace
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
počeštěné	počeštěný	k2eAgNnSc4d1	počeštěné
jméno	jméno	k1gNnSc4	jméno
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
původně	původně	k6eAd1	původně
půvab	půvab	k1gInSc4	půvab
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
vděk	vděk	k1gInSc4	vděk
a	a	k8xC	a
vnady	vnada	k1gFnPc4	vnada
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
večer	večer	k6eAd1	večer
po	po	k7c6	po
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
ráno	ráno	k6eAd1	ráno
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
východem	východ	k1gInSc7	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgInPc1d1	mnohý
národy	národ	k1gInPc1	národ
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
Venuši	Venuše	k1gFnSc4	Venuše
dvě	dva	k4xCgNnPc1	dva
pojmenování	pojmenování	k1gNnPc2	pojmenování
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
planeta	planeta	k1gFnSc1	planeta
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
nazývali	nazývat	k5eAaImAgMnP	nazývat
Venuši	Venuše	k1gFnSc4	Venuše
Hesperos	Hesperosa	k1gFnPc2	Hesperosa
večer	večer	k6eAd1	večer
anebo	anebo	k8xC	anebo
ráno	ráno	k6eAd1	ráno
Fósforos	Fósforosa	k1gFnPc2	Fósforosa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
podobné	podobný	k2eAgNnSc1d1	podobné
dvojité	dvojitý	k2eAgNnSc1d1	dvojité
pojmenování	pojmenování	k1gNnSc1	pojmenování
-	-	kIx~	-
Večernice	večernice	k1gFnSc1	večernice
anebo	anebo	k8xC	anebo
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Venus	Venus	k1gInSc1	Venus
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
používalo	používat	k5eAaImAgNnS	používat
pro	pro	k7c4	pro
staroitalskou	staroitalský	k2eAgFnSc4d1	staroitalská
bohyni	bohyně	k1gFnSc4	bohyně
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
probouzející	probouzející	k2eAgFnSc4d1	probouzející
se	se	k3xPyFc4	se
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
až	až	k9	až
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
i	i	k9	i
bohyni	bohyně	k1gFnSc4	bohyně
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
po	po	k7c6	po
první	první	k4xOgFnSc6	první
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
ke	k	k7c3	k
ztotožnění	ztotožnění	k1gNnSc3	ztotožnění
římské	římský	k2eAgFnSc2d1	římská
bohyně	bohyně	k1gFnSc2	bohyně
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
bohyní	bohyně	k1gFnSc7	bohyně
lásky	láska	k1gFnSc2	láska
Afroditou	Afrodita	k1gFnSc7	Afrodita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Venuše	Venuše	k1gFnSc1	Venuše
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
božskými	božský	k2eAgInPc7d1	božský
principy	princip	k1gInPc7	princip
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
asociována	asociován	k2eAgFnSc1d1	asociována
se	s	k7c7	s
sumerskou	sumerský	k2eAgFnSc7d1	sumerská
bohyní	bohyně	k1gFnSc7	bohyně
Inanou	Inaný	k2eAgFnSc7d1	Inaný
<g/>
,	,	kIx,	,
akkadskou	akkadský	k2eAgFnSc7d1	akkadská
Ištar	Ištar	k1gInSc4	Ištar
<g/>
,	,	kIx,	,
asyrskou	asyrský	k2eAgFnSc7d1	Asyrská
Mylitou	Mylita	k1gFnSc7	Mylita
<g/>
,	,	kIx,	,
syrskou	syrský	k2eAgFnSc7d1	Syrská
Astarté	Astartý	k2eAgFnPc4d1	Astartý
<g/>
,	,	kIx,	,
fénickou	fénický	k2eAgFnSc7d1	fénická
Astarot	Astarot	k1gInSc4	Astarot
<g/>
,	,	kIx,	,
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
Aštoret	Aštoret	k1gInSc4	Aštoret
<g/>
,	,	kIx,	,
indickou	indický	k2eAgFnSc7d1	indická
Šukrou	Šukra	k1gFnSc7	Šukra
<g/>
,	,	kIx,	,
germánskou	germánský	k2eAgFnSc7d1	germánská
Freyou	Freya	k1gFnSc7	Freya
<g/>
,	,	kIx,	,
řeckou	řecký	k2eAgFnSc7d1	řecká
Afroditou	Afrodita	k1gFnSc7	Afrodita
a	a	k8xC	a
římskou	římska	k1gFnSc7	římska
Vénus	Vénus	k1gMnSc1	Vénus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
babylónské	babylónský	k2eAgFnSc2d1	Babylónská
astrologie	astrologie	k1gFnSc2	astrologie
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
tradičních	tradiční	k2eAgNnPc2d1	tradiční
7	[number]	k4	7
planetárních	planetární	k2eAgMnPc2d1	planetární
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
chronokratorů	chronokrator	k1gInPc2	chronokrator
(	(	kIx(	(
<g/>
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
pár	pár	k4xCyI	pár
světel	světlo	k1gNnPc2	světlo
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
planety	planeta	k1gFnPc1	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sedmidenního	sedmidenní	k2eAgInSc2d1	sedmidenní
týdne	týden	k1gInSc2	týden
vládne	vládnout	k5eAaImIp3nS	vládnout
Venuše	Venuše	k1gFnSc1	Venuše
pátku	pátek	k1gInSc2	pátek
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
pojmenování	pojmenování	k1gNnSc1	pojmenování
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
dies	dies	k6eAd1	dies
Veneris	Veneris	k1gFnPc2	Veneris
<g/>
,	,	kIx,	,
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
šukravána	šukraván	k2eAgFnSc1d1	šukraván
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pythagorových	Pythagorův	k2eAgFnPc2d1	Pythagorova
kosmologických	kosmologický	k2eAgFnPc2d1	kosmologická
představ	představa	k1gFnPc2	představa
Země	zem	k1gFnSc2	zem
obalené	obalený	k2eAgFnSc2d1	obalená
7	[number]	k4	7
otáčejícími	otáčející	k2eAgFnPc7d1	otáčející
se	se	k3xPyFc4	se
planetárními	planetární	k2eAgFnPc7d1	planetární
sférami	sféra	k1gFnPc7	sféra
<g/>
,	,	kIx,	,
vydávajícími	vydávající	k2eAgFnPc7d1	vydávající
tzv.	tzv.	kA	tzv.
hudbu	hudba	k1gFnSc4	hudba
sfér	sféra	k1gFnPc2	sféra
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
starší	starý	k2eAgFnPc1d2	starší
sedmitónové	sedmitónový	k2eAgFnPc1d1	sedmitónový
hudební	hudební	k2eAgFnPc1d1	hudební
stupnice	stupnice	k1gFnPc1	stupnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
Venuši	Venuše	k1gFnSc6	Venuše
náležel	náležet	k5eAaImAgInS	náležet
tón	tón	k1gInSc1	tón
H.	H.	kA	H.
Z	z	k7c2	z
lidských	lidský	k2eAgInPc2d1	lidský
smyslů	smysl	k1gInPc2	smysl
připadla	připadnout	k5eAaPmAgFnS	připadnout
Venuši	Venuše	k1gFnSc4	Venuše
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
z	z	k7c2	z
kovů	kov	k1gInPc2	kov
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
blahodárnou	blahodárný	k2eAgFnSc4d1	blahodárná
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
malým	malý	k2eAgMnSc7d1	malý
dobrodějem	dobroděj	k1gMnSc7	dobroděj
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Malým	malý	k2eAgNnSc7d1	malé
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
asociována	asociovat	k5eAaBmNgFnS	asociovat
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
astrologické	astrologický	k2eAgFnSc6d1	astrologická
tradici	tradice	k1gFnSc6	tradice
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
ptolemaiovském	ptolemaiovský	k2eAgInSc6d1	ptolemaiovský
systému	systém	k1gInSc6	systém
vládne	vládnout	k5eAaImIp3nS	vládnout
Venuše	Venuše	k1gFnSc1	Venuše
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
její	její	k3xOp3gInSc1	její
denní	denní	k2eAgInSc1d1	denní
dům	dům	k1gInSc1	dům
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vzdušnému	vzdušný	k2eAgNnSc3d1	vzdušné
znamení	znamení	k1gNnSc3	znamení
Vah	Váhy	k1gFnPc2	Váhy
a	a	k8xC	a
noční	noční	k2eAgFnSc2d1	noční
zemskému	zemský	k2eAgInSc3d1	zemský
znamení	znamení	k1gNnSc6	znamení
Býka	býk	k1gMnSc4	býk
<g/>
.	.	kIx.	.
</s>
<s>
Ničí	ničit	k5eAaImIp3nS	ničit
ji	on	k3xPp3gFnSc4	on
protilehlá	protilehlý	k2eAgFnSc1d1	protilehlá
(	(	kIx(	(
<g/>
exilová	exilový	k2eAgFnSc1d1	exilová
<g/>
)	)	kIx)	)
znamení	znamení	k1gNnSc1	znamení
Berana	Beran	k1gMnSc2	Beran
a	a	k8xC	a
Štíra	štír	k1gMnSc2	štír
<g/>
,	,	kIx,	,
domy	dům	k1gInPc1	dům
jejího	její	k3xOp3gNnSc2	její
konkurenta	konkurent	k1gMnSc4	konkurent
Marta	Marta	k1gFnSc1	Marta
<g/>
.	.	kIx.	.
</s>
<s>
Povýšení	povýšení	k1gNnSc1	povýšení
zažívá	zažívat	k5eAaImIp3nS	zažívat
v	v	k7c6	v
spirituálně	spirituálně	k6eAd1	spirituálně
sounáležitých	sounáležitý	k2eAgFnPc6d1	sounáležitý
Rybách	Ryby	k1gFnPc6	Ryby
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
rozumové	rozumový	k2eAgFnSc6d1	rozumová
a	a	k8xC	a
kritické	kritický	k2eAgFnSc6d1	kritická
Panně	Panna	k1gFnSc6	Panna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
inspirovány	inspirovat	k5eAaBmNgFnP	inspirovat
skutečnými	skutečný	k2eAgFnPc7d1	skutečná
astronomickými	astronomický	k2eAgFnPc7d1	astronomická
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
5	[number]	k4	5
synodických	synodický	k2eAgInPc2d1	synodický
oběhů	oběh	k1gInPc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
koná	konat	k5eAaImIp3nS	konat
Venuše	Venuše	k1gFnSc1	Venuše
z	z	k7c2	z
geocentrického	geocentrický	k2eAgInSc2d1	geocentrický
pohledu	pohled	k1gInSc2	pohled
5	[number]	k4	5
retrográdních	retrográdní	k2eAgInPc2d1	retrográdní
pohybů	pohyb	k1gInPc2	pohyb
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
posloupnost	posloupnost	k1gFnSc1	posloupnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
vrcholy	vrchol	k1gInPc4	vrchol
pěticípé	pěticípý	k2eAgFnSc2d1	pěticípá
hvězdy	hvězda	k1gFnSc2	hvězda
kreslené	kreslený	k2eAgFnSc2d1	kreslená
jedním	jeden	k4xCgInSc7	jeden
tahem	tah	k1gInSc7	tah
-	-	kIx~	-
pentagramu	pentagram	k1gInSc2	pentagram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
očích	oko	k1gNnPc6	oko
hermetiků	hermetik	k1gMnPc2	hermetik
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
pentagram	pentagram	k1gInSc4	pentagram
postavený	postavený	k2eAgInSc4d1	postavený
špicí	špice	k1gFnSc7	špice
vzhůru	vzhůru	k6eAd1	vzhůru
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
duchovno	duchovno	k1gNnSc1	duchovno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postaven	postaven	k2eAgMnSc1d1	postaven
obráceně	obráceně	k6eAd1	obráceně
značí	značit	k5eAaImIp3nS	značit
býčí	býčí	k2eAgFnSc4d1	býčí
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
hmotné	hmotný	k2eAgInPc1d1	hmotný
statky	statek	k1gInPc1	statek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
denních	denní	k2eAgInPc2d1	denní
a	a	k8xC	a
nočních	noční	k2eAgInPc2d1	noční
domů	dům	k1gInPc2	dům
planet	planeta	k1gFnPc2	planeta
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
i	i	k9	i
od	od	k7c2	od
dvojjediné	dvojjediný	k2eAgFnSc2d1	dvojjediná
podstaty	podstata	k1gFnSc2	podstata
Venuše	Venuše	k1gFnSc2	Venuše
coby	coby	k?	coby
Jitřenky	Jitřenka	k1gFnSc2	Jitřenka
<g/>
/	/	kIx~	/
<g/>
Večernice	večernice	k1gFnSc1	večernice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
astrologii	astrologie	k1gFnSc6	astrologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
planetárních	planetární	k2eAgMnPc2d1	planetární
vládců	vládce	k1gMnPc2	vládce
9	[number]	k4	9
(	(	kIx(	(
<g/>
k	k	k7c3	k
tradičním	tradiční	k2eAgFnPc3d1	tradiční
sedmi	sedm	k4xCc2	sedm
je	být	k5eAaImIp3nS	být
přidán	přidán	k2eAgInSc4d1	přidán
vzestupný	vzestupný	k2eAgInSc4d1	vzestupný
a	a	k8xC	a
sestupný	sestupný	k2eAgInSc4d1	sestupný
měsíční	měsíční	k2eAgInSc4d1	měsíční
uzel	uzel	k1gInSc4	uzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vládne	vládnout	k5eAaImIp3nS	vládnout
Venuše	Venuše	k1gFnSc1	Venuše
třem	tři	k4xCgFnPc3	tři
lunárním	lunární	k2eAgInPc3d1	lunární
zvířetníkovým	zvířetníkový	k2eAgInPc3d1	zvířetníkový
domům	dům	k1gInPc3	dům
(	(	kIx(	(
<g/>
nakšatrám	nakšatrat	k5eAaPmIp1nS	nakšatrat
<g/>
)	)	kIx)	)
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
Bharaní	Bharaní	k1gNnSc4	Bharaní
<g/>
,	,	kIx,	,
Púrvaphálguní	Púrvaphálguní	k1gMnSc1	Púrvaphálguní
a	a	k8xC	a
Púrvášádha	Púrvášádha	k1gMnSc1	Púrvášádha
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
astrologie	astrologie	k1gFnSc1	astrologie
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
babylónských	babylónský	k2eAgInPc2d1	babylónský
kořenů	kořen	k1gInPc2	kořen
zná	znát	k5eAaImIp3nS	znát
Venuši	Venuše	k1gFnSc4	Venuše
s	s	k7c7	s
Lunou	luna	k1gFnSc7	luna
jako	jako	k9	jako
jediné	jediný	k2eAgFnPc1d1	jediná
dvě	dva	k4xCgFnPc1	dva
ženské	ženská	k1gFnPc1	ženská
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc2	Venuše
však	však	k9	však
oproti	oproti	k7c3	oproti
Luně	luna	k1gFnSc3	luna
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
spíše	spíše	k9	spíše
smyslovou	smyslový	k2eAgFnSc4d1	smyslová
<g/>
,	,	kIx,	,
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
a	a	k8xC	a
citovou	citový	k2eAgFnSc4d1	citová
stránku	stránka	k1gFnSc4	stránka
ženství	ženství	k1gNnSc2	ženství
<g/>
,	,	kIx,	,
prvotní	prvotní	k2eAgFnSc4d1	prvotní
fázi	fáze	k1gFnSc4	fáze
ženského	ženský	k2eAgInSc2d1	ženský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
pannu	panna	k1gFnSc4	panna
nebo	nebo	k8xC	nebo
milenku	milenka	k1gFnSc4	milenka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
naplnění	naplnění	k1gNnSc4	naplnění
jejího	její	k3xOp3gNnSc2	její
mateřského	mateřský	k2eAgNnSc2d1	mateřské
poslání	poslání	k1gNnSc2	poslání
teprve	teprve	k6eAd1	teprve
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
astrologický	astrologický	k2eAgInSc1d1	astrologický
symbol	symbol	k1gInSc1	symbol
Venuše	Venuše	k1gFnSc2	Venuše
♀	♀	k?	♀
chápán	chápat	k5eAaImNgMnS	chápat
jako	jako	k8xS	jako
glyf	glyf	k1gMnSc1	glyf
zrcátka	zrcátko	k1gNnSc2	zrcátko
nebo	nebo	k8xC	nebo
náhrdelníku	náhrdelník	k1gInSc2	náhrdelník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hlubší	hluboký	k2eAgFnSc1d2	hlubší
symbolika	symbolika	k1gFnSc1	symbolika
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
nadřazení	nadřazení	k1gNnSc6	nadřazení
kruhu	kruh	k1gInSc2	kruh
ducha	duch	k1gMnSc2	duch
nad	nad	k7c7	nad
křížem	kříž	k1gInSc7	kříž
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Venuše	Venuše	k1gFnSc1	Venuše
ovládá	ovládat	k5eAaImIp3nS	ovládat
jak	jak	k6eAd1	jak
duchovní	duchovní	k2eAgNnSc1d1	duchovní
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
materiální	materiální	k2eAgFnPc1d1	materiální
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
značí	značit	k5eAaImIp3nS	značit
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
svůdnost	svůdnost	k1gFnSc4	svůdnost
<g/>
,	,	kIx,	,
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
,	,	kIx,	,
laskavost	laskavost	k1gFnSc4	laskavost
<g/>
,	,	kIx,	,
společenskost	společenskost	k1gFnSc4	společenskost
<g/>
,	,	kIx,	,
toleranci	tolerance	k1gFnSc4	tolerance
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
kompromisů	kompromis	k1gInPc2	kompromis
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
aspektovaná	aspektovaný	k2eAgFnSc1d1	aspektovaná
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
promiskuitě	promiskuita	k1gFnSc3	promiskuita
<g/>
,	,	kIx,	,
lascivnosti	lascivnost	k1gFnSc6	lascivnost
<g/>
,	,	kIx,	,
chlípnosti	chlípnost	k1gFnSc6	chlípnost
<g/>
,	,	kIx,	,
bezstarostnosti	bezstarostnost	k1gFnSc6	bezstarostnost
a	a	k8xC	a
povrchnosti	povrchnost	k1gFnSc6	povrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Libuje	libovat	k5eAaImIp3nS	libovat
si	se	k3xPyFc3	se
v	v	k7c6	v
kráse	krása	k1gFnSc6	krása
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
harmonii	harmonie	k1gFnSc6	harmonie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dobré	dobrý	k2eAgFnSc2d1	dobrá
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nižší	nízký	k2eAgFnSc6d2	nižší
podobě	podoba	k1gFnSc6	podoba
svádí	svádět	k5eAaImIp3nS	svádět
k	k	k7c3	k
pohodlí	pohodlí	k1gNnSc3	pohodlí
<g/>
,	,	kIx,	,
snadnému	snadný	k2eAgInSc3d1	snadný
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
řivosti	řivost	k1gFnSc5	řivost
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
pod	pod	k7c7	pod
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
vlivem	vliv	k1gInSc7	vliv
Venuše	Venuše	k1gFnSc2	Venuše
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
inklinovat	inklinovat	k5eAaImF	inklinovat
k	k	k7c3	k
povoláním	povolání	k1gNnPc3	povolání
souvisejícím	související	k2eAgFnPc3d1	související
s	s	k7c7	s
krásou	krása	k1gFnSc7	krása
<g/>
,	,	kIx,	,
módou	móda	k1gFnSc7	móda
<g/>
,	,	kIx,	,
uměním	umění	k1gNnSc7	umění
nebo	nebo	k8xC	nebo
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
nemohl	moct	k5eNaImAgMnS	moct
člověk	člověk	k1gMnSc1	člověk
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgMnSc4d1	jiný
než	než	k8xS	než
hustá	hustý	k2eAgNnPc1d1	husté
mračna	mračno	k1gNnPc1	mračno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podněcovalo	podněcovat	k5eAaImAgNnS	podněcovat
představivost	představivost	k1gFnSc4	představivost
mnohých	mnohý	k2eAgMnPc2d1	mnohý
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
dávalo	dávat	k5eAaImAgNnS	dávat
jim	on	k3xPp3gMnPc3	on
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
ve	v	k7c6	v
spekulacích	spekulace	k1gFnPc6	spekulace
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
panujících	panující	k2eAgFnPc2d1	panující
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
pozorování	pozorování	k1gNnSc1	pozorování
planety	planeta	k1gFnSc2	planeta
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
námětech	námět	k1gInPc6	námět
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
klima	klima	k1gNnSc1	klima
teplejší	teplý	k2eAgNnSc1d2	teplejší
než	než	k8xS	než
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
panovala	panovat	k5eAaImAgFnS	panovat
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
mohli	moct	k5eAaImAgMnP	moct
trvale	trvale	k6eAd1	trvale
obývat	obývat	k5eAaImF	obývat
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Venuši	Venuše	k1gFnSc4	Venuše
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
sci-fi	scii	k1gFnSc2	sci-fi
nastal	nastat	k5eAaPmAgInS	nastat
mezi	mezi	k7c4	mezi
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
lety	let	k1gInPc7	let
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vědci	vědec	k1gMnPc1	vědec
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
odhadnout	odhadnout	k5eAaPmF	odhadnout
některé	některý	k3yIgFnPc4	některý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zde	zde	k6eAd1	zde
panovala	panovat	k5eAaImAgFnS	panovat
značná	značný	k2eAgFnSc1d1	značná
nejistota	nejistota	k1gFnSc1	nejistota
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgInSc4d1	poskytující
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaPmAgMnS	napsat
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gInSc2	Ric
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
sérii	série	k1gFnSc4	série
dobrodružných	dobrodružný	k2eAgFnPc2d1	dobrodružná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
Venuši	Venuše	k1gFnSc6	Venuše
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známý	známý	k2eAgMnSc1d1	známý
Robert	Robert	k1gMnSc1	Robert
Heinlein	Heinlein	k1gMnSc1	Heinlein
se	se	k3xPyFc4	se
sérii	série	k1gFnSc4	série
příběhů	příběh	k1gInPc2	příběh
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
Future	Futur	k1gMnSc5	Futur
History	Histor	k1gInPc1	Histor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
inspirované	inspirovaný	k2eAgFnSc2d1	inspirovaná
chemikem	chemik	k1gMnSc7	chemik
Svantem	Svant	k1gMnSc7	Svant
Arrheniem	Arrhenium	k1gNnSc7	Arrhenium
předpovídající	předpovídající	k2eAgInSc1d1	předpovídající
souvislý	souvislý	k2eAgInSc1d1	souvislý
déšť	déšť	k1gInSc1	déšť
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
povídky	povídka	k1gFnPc4	povídka
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Rain	Rain	k1gMnSc1	Rain
a	a	k8xC	a
All	All	k1gMnSc1	All
Summer	Summer	k1gMnSc1	Summer
in	in	k?	in
a	a	k8xC	a
Day	Day	k1gMnSc1	Day
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
i	i	k9	i
další	další	k2eAgMnSc1d1	další
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ray	Ray	k1gFnSc2	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vyšla	vyjít	k5eAaPmAgFnS	vyjít
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
Perelanda	Perelanda	k1gFnSc1	Perelanda
od	od	k7c2	od
Cliva	Clivo	k1gNnSc2	Clivo
Lewise	Lewise	k1gFnSc2	Lewise
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
pak	pak	k9	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gInSc4	Starr
and	and	k?	and
the	the	k?	the
Oceans	Oceans	k1gInSc1	Oceans
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
popisovala	popisovat	k5eAaImAgFnS	popisovat
Venuši	Venuše	k1gFnSc4	Venuše
jako	jako	k8xS	jako
vodní	vodní	k2eAgInSc4d1	vodní
svět	svět	k1gInSc4	svět
s	s	k7c7	s
obrovským	obrovský	k2eAgInSc7d1	obrovský
oceánem	oceán	k1gInSc7	oceán
plným	plný	k2eAgNnSc7d1	plné
exotického	exotický	k2eAgInSc2d1	exotický
mořského	mořský	k2eAgInSc2d1	mořský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
náměty	námět	k1gInPc1	námět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
popisovaly	popisovat	k5eAaImAgFnP	popisovat
Venuši	Venuše	k1gFnSc4	Venuše
jako	jako	k8xC	jako
suchý	suchý	k2eAgInSc4d1	suchý
a	a	k8xC	a
prašný	prašný	k2eAgInSc4d1	prašný
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
příběhy	příběh	k1gInPc1	příběh
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c6	o
dílo	dílo	k1gNnSc4	dílo
The	The	k1gFnSc2	The
Big	Big	k1gMnSc1	Big
Rain	Rain	k1gMnSc1	Rain
od	od	k7c2	od
Poula	Poul	k1gMnSc2	Poul
Andersona	Anderson	k1gMnSc2	Anderson
či	či	k8xC	či
novela	novela	k1gFnSc1	novela
The	The	k1gFnSc1	The
Space	Space	k1gFnSc1	Space
Merchants	Merchants	k1gInSc4	Merchants
od	od	k7c2	od
Fredericka	Fredericko	k1gNnSc2	Fredericko
Pohla	Pohl	k1gMnSc2	Pohl
a	a	k8xC	a
Cyrila	Cyril	k1gMnSc2	Cyril
M.	M.	kA	M.
Kornblutha	Kornbluth	k1gMnSc2	Kornbluth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
napsal	napsat	k5eAaBmAgMnS	napsat
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Stanisław	Stanisław	k1gMnSc1	Stanisław
Lem	lem	k1gInSc4	lem
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
sci-fi	scii	k1gFnSc7	sci-fi
román	román	k1gInSc1	román
Astronauti	astronaut	k1gMnPc1	astronaut
<g/>
,	,	kIx,	,
situovaný	situovaný	k2eAgMnSc1d1	situovaný
částečně	částečně	k6eAd1	částečně
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
poplatný	poplatný	k2eAgInSc1d1	poplatný
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
-	-	kIx~	-
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
optimistické	optimistický	k2eAgFnSc6d1	optimistická
komunistické	komunistický	k2eAgFnSc6d1	komunistická
budoucnosti	budoucnost	k1gFnSc6	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mementem	memento	k1gNnSc7	memento
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
jaderné	jaderný	k2eAgInPc4d1	jaderný
válce	válec	k1gInPc4	válec
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
psaní	psaní	k1gNnSc2	psaní
románu	román	k1gInSc2	román
již	již	k6eAd1	již
probíhala	probíhat	k5eAaImAgFnS	probíhat
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
sondy	sonda	k1gFnPc1	sonda
přinesly	přinést	k5eAaPmAgFnP	přinést
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
Venuši	Venuše	k1gFnSc6	Venuše
jako	jako	k9	jako
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
a	a	k8xC	a
nehostinném	hostinný	k2eNgNnSc6d1	nehostinné
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
velký	velký	k2eAgInSc4d1	velký
úpadek	úpadek	k1gInSc4	úpadek
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Venuši	Venuše	k1gFnSc4	Venuše
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
sci-fi	scii	k1gFnSc2	sci-fi
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
literatury	literatura	k1gFnSc2	literatura
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
na	na	k7c6	na
Venuši	Venuše	k1gFnSc6	Venuše
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Planeta	planeta	k1gFnSc1	planeta
nachových	nachový	k2eAgFnPc2d1	nachová
mračen	mračna	k1gFnPc2	mračna
od	od	k7c2	od
bratrů	bratr	k1gMnPc2	bratr
Strugackých	Strugackých	k2eAgFnSc2d1	Strugackých
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnSc2d1	vydaná
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
narůstajícími	narůstající	k2eAgFnPc7d1	narůstající
vědomostmi	vědomost	k1gFnPc7	vědomost
o	o	k7c6	o
Venuši	Venuše	k1gFnSc6	Venuše
se	se	k3xPyFc4	se
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
mezi	mezi	k7c4	mezi
autory	autor	k1gMnPc4	autor
sci-fi	scii	k1gFnSc2	sci-fi
navracet	navracet	k5eAaImF	navracet
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
námětem	námět	k1gInSc7	námět
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
osídlení	osídlení	k1gNnSc2	osídlení
a	a	k8xC	a
terraformace	terraformace	k1gFnSc2	terraformace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
britský	britský	k2eAgMnSc1d1	britský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Arthur	Arthura	k1gFnPc2	Arthura
C.	C.	kA	C.
Clarke	Clark	k1gFnSc2	Clark
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
3001	[number]	k4	3001
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgFnSc1d1	poslední
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
popisuje	popisovat	k5eAaImIp3nS	popisovat
snahu	snaha	k1gFnSc4	snaha
lidstva	lidstvo	k1gNnSc2	lidstvo
ochladit	ochladit	k5eAaPmF	ochladit
Venuši	Venuše	k1gFnSc4	Venuše
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ledových	ledový	k2eAgNnPc2d1	ledové
jader	jádro	k1gNnPc2	jádro
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
námět	námět	k1gInSc1	námět
teraformace	teraformace	k1gFnSc2	teraformace
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
příběhů	příběh	k1gInPc2	příběh
jako	jako	k8xC	jako
např.	např.	kA	např.
ve	v	k7c6	v
Star	star	k1gFnSc6	star
Treku	Treka	k1gFnSc4	Treka
či	či	k8xC	či
Exosquad	Exosquad	k1gInSc4	Exosquad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
život	život	k1gInSc4	život
podobný	podobný	k2eAgInSc4d1	podobný
tomu	ten	k3xDgNnSc3	ten
pozemskému	pozemský	k2eAgInSc3d1	pozemský
v	v	k7c6	v
minulých	minulý	k2eAgFnPc6d1	minulá
geologických	geologický	k2eAgFnPc6d1	geologická
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
představa	představa	k1gFnSc1	představa
bytostí	bytost	k1gFnPc2	bytost
podobných	podobný	k2eAgFnPc2d1	podobná
pozemským	pozemský	k2eAgMnPc3d1	pozemský
druhohorním	druhohorní	k2eAgMnPc3d1	druhohorní
dinosaurům	dinosaurus	k1gMnPc3	dinosaurus
<g/>
.	.	kIx.	.
</s>
