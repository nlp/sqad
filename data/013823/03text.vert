<s>
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
</s>
<s>
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
,	,	kIx,
a.s.	a.s.	k?
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1993	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Fond	fond	k1gInSc1
národního	národní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1
Mýto	mýto	k1gNnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnSc1d1
74	#num#	k4
<g/>
,	,	kIx,
Vysoké	vysoký	k2eAgNnSc1d1
Mýto	mýto	k1gNnSc1
<g/>
,	,	kIx,
566	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
sídla	sídlo	k1gNnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
22,01	22,01	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
13,27	13,27	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
automobilový	automobilový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
autobus	autobus	k1gInSc1
Obrat	obrat	k1gInSc1
</s>
<s>
23	#num#	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
21,7	21,7	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
2,6	2,6	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
2,6	2,6	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
2,1	2,1	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
2,2	2,2	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgNnPc1d1
aktiva	aktivum	k1gNnPc1
</s>
<s>
16,8	16,8	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
14,8	14,8	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
10,7	10,7	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
9,3	9,3	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
2	#num#	k4
598	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
CNH	CNH	kA
Industrial	Industrial	k1gMnSc1
Majitel	majitel	k1gMnSc1
</s>
<s>
Iveco	Iveco	k1gMnSc1
France	Franc	k1gMnSc2
(	(	kIx(
<g/>
98,0	98,0	k4
%	%	kIx~
<g/>
)	)	kIx)
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
www.iveco.com/czech/spolecnost/pages/o-spole%C4%8Dnosti.aspx	www.iveco.com/czech/spolecnost/pages/o-spole%C4%8Dnosti.aspx	k1gInSc1
IČO	IČO	kA
</s>
<s>
48171131	#num#	k4
LEI	lei	k1gInSc2
</s>
<s>
5493008MZ3KHZRE8F512	5493008MZ3KHZRE8F512	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Meziměstský	meziměstský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
Irisbus	Irisbus	k1gInSc1
Crossway	Crosswaa	k1gFnSc2
v	v	k7c6
Peci	Pec	k1gFnSc6
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
</s>
<s>
Městský	městský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
Iveco	Iveco	k1gNnSc1
Urbanway	Urbanwaa	k1gFnSc2
12M	12M	k4
</s>
<s>
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
firma	firma	k1gFnSc1
vyrábějící	vyrábějící	k2eAgInPc4d1
autobusy	autobus	k1gInPc7
ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1993	#num#	k4
privatizací	privatizace	k1gFnPc2
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
Karosa	karosa	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
jehož	jehož	k3xOyRp3gInSc1
název	název	k1gInSc1
již	již	k6eAd1
jako	jako	k9
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nesla	nést	k5eAaImAgFnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1999	#num#	k4
se	se	k3xPyFc4
Karosa	karosa	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
celoevropského	celoevropský	k2eAgInSc2d1
holdingu	holding	k1gInSc2
Irisbus	Irisbus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
založily	založit	k5eAaPmAgFnP
firmy	firma	k1gFnPc1
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
vlastnící	vlastnící	k2eAgFnSc4d1
Karosu	karosa	k1gFnSc4
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
)	)	kIx)
a	a	k8xC
Iveco	Iveco	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
celý	celý	k2eAgInSc1d1
Irisbus	Irisbus	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
převzalo	převzít	k5eAaPmAgNnS
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
značku	značka	k1gFnSc4
nadále	nadále	k6eAd1
využívalo	využívat	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
podniku	podnik	k1gInSc2
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
koncern	koncern	k1gInSc1
Iveco	Iveco	k6eAd1
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
autobusy	autobus	k1gInPc1
jednotnou	jednotný	k2eAgFnSc4d1
obchodní	obchodní	k2eAgFnSc4d1
značku	značka	k1gFnSc4
Iveco	Iveco	k6eAd1
Bus	bus	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
předchozí	předchozí	k2eAgInSc4d1
Irisbus	Irisbus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc1
Republic	Republic	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
vyrobilo	vyrobit	k5eAaPmAgNnS
2526	#num#	k4
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
o	o	k7c4
494	#num#	k4
méně	málo	k6eAd2
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Ovšem	ovšem	k9
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
vyrobeno	vyrobit	k5eAaPmNgNnS
rekordních	rekordní	k2eAgInPc2d1
3165	#num#	k4
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1
byly	být	k5eAaImAgInP
z	z	k7c2
90	#num#	k4
%	%	kIx~
určeny	určit	k5eAaPmNgInP
na	na	k7c4
vývoz	vývoz	k1gInSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Především	především	k9
díky	díky	k7c3
někdejší	někdejší	k2eAgFnSc3d1
Karose	karosa	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
vyrábí	vyrábět	k5eAaImIp3nS
v	v	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
milion	milion	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
autobusů	autobus	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgFnSc1d3
evropská	evropský	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
autobusů	autobus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modely	model	k1gInPc1
</s>
<s>
Karosa	karosa	k1gFnSc1
od	od	k7c2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vyráběla	vyrábět	k5eAaImAgFnS
modelovou	modelový	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
900	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
modernizací	modernizace	k1gFnSc7
řady	řada	k1gFnSc2
700	#num#	k4
vyvíjené	vyvíjený	k2eAgFnSc2d1
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
řady	řada	k1gFnSc2
900	#num#	k4
byly	být	k5eAaImAgFnP
vyrobeny	vyrobit	k5eAaPmNgInP
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
naopak	naopak	k6eAd1
vyvíjena	vyvíjet	k5eAaImNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
dálkový	dálkový	k2eAgInSc1d1
model	model	k1gInSc1
Irisbus	Irisbus	k1gInSc4
Arway	Arwaa	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
školní	školní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Irisbus	Irisbus	k1gMnSc1
Récréo	Récréo	k1gMnSc1
a	a	k8xC
příměstský	příměstský	k2eAgMnSc1d1
a	a	k8xC
meziměstský	meziměstský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
Irisbus	Irisbus	k1gInSc4
Crossway	Crosswaa	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
roku	rok	k1gInSc2
2007	#num#	k4
přibyla	přibýt	k5eAaPmAgFnS
i	i	k9
částečně	částečně	k6eAd1
nízkopodlažní	nízkopodlažní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Irisbus	Irisbus	k1gMnSc1
Crossway	Crosswaa	k1gFnSc2
LE	LE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
byly	být	k5eAaImAgFnP
ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
vyráběny	vyráběn	k2eAgInPc1d1
i	i	k8xC
nízkopodlažní	nízkopodlažní	k2eAgInPc1d1
vozy	vůz	k1gInPc1
Citelis	Citelis	k1gFnSc1
10.5	10.5	k4
<g/>
M	M	kA
a	a	k8xC
Citelis	Citelis	k1gFnSc1
12	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
produkce	produkce	k1gFnSc1
sem	sem	k6eAd1
byla	být	k5eAaImAgFnS
převedena	převést	k5eAaPmNgFnS
z	z	k7c2
italského	italský	k2eAgInSc2d1
závodu	závod	k1gInSc2
ve	v	k7c6
Valle	Vall	k1gInSc6
Ufita	Ufit	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Firma	firma	k1gFnSc1
Iveco	Iveco	k6eAd1
Czech	Czech	k1gInSc4
Republic	Republice	k1gFnPc2
vyráběla	vyrábět	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
městské	městský	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
Urbanway	Urbanwa	k2eAgInPc1d1
a	a	k8xC
meziměstské	meziměstský	k2eAgInPc1d1
modely	model	k1gInPc1
Crossway	Crosswa	k2eAgInPc1d1
a	a	k8xC
Crossway	Crosswa	k2eAgInPc1d1
LE	LE	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Veřejný	veřejný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
a	a	k8xC
Sbírka	sbírka	k1gFnSc1
listin	listina	k1gFnPc2
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpis	výpis	k1gInSc1
z	z	k7c2
obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SRB	Srb	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karosa	karosa	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Iveco	Iveco	k1gNnSc1
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autobusovenoviny	Autobusovenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2007-01-15	2007-01-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Iveco	Iveco	k6eAd1
Bus	bus	k1gInSc4
<g/>
:	:	kIx,
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
značky	značka	k1gFnSc2
určený	určený	k2eAgInSc1d1
hromadné	hromadný	k2eAgFnSc3d1
přepravě	přeprava	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Busportal	Busportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-05-24	2013-05-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
loni	loni	k6eAd1
vyrobili	vyrobit	k5eAaPmAgMnP
2526	#num#	k4
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
1400	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
exportováno	exportovat	k5eAaBmNgNnS
do	do	k7c2
Francie	Francie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auto	auto	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-02-03	2010-02-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Iveco	Iveco	k1gMnSc1
Czech	Czech	k1gMnSc1
Republic	Republice	k1gFnPc2
vyrobilo	vyrobit	k5eAaPmAgNnS
rekordní	rekordní	k2eAgInSc4d1
počet	počet	k1gInSc4
autobusů	autobus	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denik	Denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-01-21	2014-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ekonom	ekonom	k1gMnSc1
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnPc1d1
Noviny	novina	k1gFnPc1
IHNED	ihned	k6eAd1
-	-	kIx~
Karosa	karosa	k1gFnSc1
a	a	k8xC
Živnostenská	živnostenský	k2eAgFnSc1d1
banka	banka	k1gFnSc1
mají	mít	k5eAaImIp3nP
společný	společný	k2eAgInSc4d1
osud	osud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aneb	Aneb	k?
když	když	k8xS
firma	firma	k1gFnSc1
přijde	přijít	k5eAaPmIp3nS
o	o	k7c4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Česko	Česko	k1gNnSc1
je	být	k5eAaImIp3nS
autobusová	autobusový	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalá	bývalý	k2eAgFnSc1d1
Karosa	karosa	k1gFnSc1
jede	jet	k5eAaImIp3nS
naplno	naplno	k6eAd1
<g/>
↑	↑	k?
SRB	Srb	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnSc2
z	z	k7c2
Vysokého	vysoký	k2eAgNnSc2d1
Mýta	mýto	k1gNnSc2
<g/>
:	:	kIx,
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
konec	konec	k1gInSc1
řady	řada	k1gFnSc2
900	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autobusovenoviny	Autobusovenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2007-01-13	2007-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VIŠO	VIŠO	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HINČICA	HINČICA	kA
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
111	#num#	k4
111	#num#	k4
<g/>
.	.	kIx.
autobus	autobus	k1gInSc1
opustil	opustit	k5eAaPmAgInS
výrobní	výrobní	k2eAgFnSc4d1
linku	linka	k1gFnSc4
ve	v	k7c6
Vysokém	vysoký	k2eAgNnSc6d1
Mýtě	mýto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československý	československý	k2eAgMnSc1d1
dopravák	dopravák	k1gMnSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PECÁK	pecák	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
je	být	k5eAaImIp3nS
autobusová	autobusový	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalá	bývalý	k2eAgFnSc1d1
Karosa	karosa	k1gFnSc1
jede	jet	k5eAaImIp3nS
naplno	naplno	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-06-04	2014-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
SOR	SOR	kA
Libchavy	Libchava	k1gFnPc1
</s>
<s>
TEDOM	TEDOM	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1547	#num#	k4
4565	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129097154	#num#	k4
</s>
