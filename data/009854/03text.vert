<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brazílie	Brazílie	k1gFnSc2	Brazílie
počínají	počínat	k5eAaImIp3nP	počínat
příchodem	příchod	k1gInSc7	příchod
prvních	první	k4xOgFnPc2	první
obyvatel	obyvatel	k1gMnSc1	obyvatel
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Evropané	Evropan	k1gMnPc1	Evropan
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kolonizoval	kolonizovat	k5eAaBmAgMnS	kolonizovat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
území	území	k1gNnSc4	území
Brazilské	brazilský	k2eAgFnSc2d1	brazilská
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
jihoamerickém	jihoamerický	k2eAgInSc6d1	jihoamerický
kontinentu	kontinent	k1gInSc6	kontinent
byl	být	k5eAaImAgMnS	být
Pedro	Pedro	k1gNnSc4	Pedro
Álvares	Álvares	k1gMnSc1	Álvares
Cabral	Cabral	k1gMnSc1	Cabral
(	(	kIx(	(
<g/>
1467	[number]	k4	1467
<g/>
–	–	k?	–
<g/>
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1500	[number]	k4	1500
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Portugalského	portugalský	k2eAgNnSc2d1	portugalské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
součástí	součást	k1gFnPc2	součást
portugalské	portugalský	k2eAgFnSc2d1	portugalská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
území	území	k1gNnSc1	území
kolonie	kolonie	k1gFnSc2	kolonie
tvořilo	tvořit	k5eAaImAgNnS	tvořit
15	[number]	k4	15
kapitanátů	kapitanát	k1gInPc2	kapitanát
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
Atlantiku	Atlantik	k1gInSc2	Atlantik
východně	východně	k6eAd1	východně
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
stanovené	stanovený	k2eAgInPc1d1	stanovený
Tordesillaskou	Tordesillaský	k2eAgFnSc7d1	Tordesillaská
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1494	[number]	k4	1494
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
46	[number]	k4	46
<g/>
.	.	kIx.	.
poledník	poledník	k1gInSc1	poledník
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
oddělovala	oddělovat	k5eAaImAgFnS	oddělovat
portugalské	portugalský	k2eAgFnPc4d1	portugalská
domény	doména	k1gFnPc4	doména
na	na	k7c6	na
východě	východ	k1gInSc6	východ
od	od	k7c2	od
španělských	španělský	k2eAgInPc2d1	španělský
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgFnPc2d1	následující
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
Brazílie	Brazílie	k1gFnSc2	Brazílie
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
západně	západně	k6eAd1	západně
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Amazonky	Amazonka	k1gFnSc2	Amazonka
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
vnitrozemských	vnitrozemský	k2eAgFnPc2d1	vnitrozemská
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
ustálily	ustálit	k5eAaPmAgFnP	ustálit
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
portugalský	portugalský	k2eAgInSc1d1	portugalský
dvůr	dvůr	k1gInSc1	dvůr
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zde	zde	k6eAd1	zde
vládl	vládnout	k5eAaImAgInS	vládnout
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
kolonie	kolonie	k1gFnSc1	kolonie
a	a	k8xC	a
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Algarve	Algarev	k1gFnSc2	Algarev
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1822	[number]	k4	1822
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
ustanovení	ustanovení	k1gNnSc6	ustanovení
Brazilského	brazilský	k2eAgNnSc2d1	brazilské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
první	první	k4xOgFnSc3	první
Brazilské	brazilský	k2eAgFnSc3d1	brazilská
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
přijata	přijat	k2eAgFnSc1d1	přijata
ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
vyhlášeny	vyhlášen	k2eAgInPc1d1	vyhlášen
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
brazilské	brazilský	k2eAgInPc1d1	brazilský
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
prošla	projít	k5eAaPmAgFnS	projít
dvěma	dva	k4xCgNnPc7	dva
obdobími	období	k1gNnPc7	období
diktatur	diktatura	k1gFnPc2	diktatura
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
během	během	k7c2	během
Vargasovy	Vargasův	k2eAgFnSc2d1	Vargasův
éry	éra	k1gFnSc2	éra
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
a	a	k8xC	a
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgInPc1	druhý
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
junty	junta	k1gFnSc2	junta
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předkoloniální	Předkoloniální	k2eAgNnSc4d1	Předkoloniální
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Domorodé	domorodý	k2eAgNnSc1d1	domorodé
osídlení	osídlení	k1gNnSc1	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
portugalských	portugalský	k2eAgMnPc2d1	portugalský
průzkumníků	průzkumník	k1gMnPc2	průzkumník
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
zde	zde	k6eAd1	zde
žily	žít	k5eAaImAgFnP	žít
stovky	stovka	k1gFnPc1	stovka
různých	různý	k2eAgFnPc2d1	různá
odnoží	odnož	k1gFnPc2	odnož
kmenů	kmen	k1gInPc2	kmen
Jiquabu	Jiquab	k1gInSc2	Jiquab
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstarší	starý	k2eAgInPc4d3	nejstarší
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
před	před	k7c4	před
10	[number]	k4	10
000	[number]	k4	000
lety	let	k1gInPc7	let
na	na	k7c6	na
vysočinách	vysočina	k1gFnPc6	vysočina
Minas	Minas	k1gMnSc1	Minas
Gerais	Gerais	k1gFnSc1	Gerais
<g/>
.	.	kIx.	.
</s>
<s>
Datování	datování	k1gNnSc4	datování
příchodu	příchod	k1gInSc2	příchod
prvních	první	k4xOgMnPc2	první
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
Portugalci	Portugalec	k1gMnPc1	Portugalec
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
Indiáni	Indián	k1gMnPc1	Indián
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
índios	índios	k1gInSc1	índios
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c4	mezi
archeology	archeolog	k1gMnPc4	archeolog
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgFnSc1d3	nejstarší
keramika	keramika	k1gFnSc1	keramika
nalezená	nalezený	k2eAgFnSc1d1	nalezená
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
byla	být	k5eAaImAgFnS	být
vykopána	vykopat	k5eAaPmNgFnS	vykopat
v	v	k7c6	v
amazonské	amazonský	k2eAgFnSc6d1	Amazonská
oblasti	oblast	k1gFnSc6	oblast
Brazílie	Brazílie	k1gFnSc2	Brazílie
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Santarém	Santarý	k2eAgNnSc6d1	Santarý
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
stáří	stáří	k1gNnSc4	stáří
určila	určit	k5eAaPmAgFnS	určit
radiokarbonová	radiokarbonový	k2eAgFnSc1d1	radiokarbonová
metoda	metoda	k1gFnSc1	metoda
na	na	k7c4	na
8000	[number]	k4	8000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
tak	tak	k9	tak
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
vyvracející	vyvracející	k2eAgInSc1d1	vyvracející
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
tropického	tropický	k2eAgInSc2d1	tropický
lesa	les	k1gInSc2	les
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
příliš	příliš	k6eAd1	příliš
málo	málo	k4c4	málo
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
komplexní	komplexní	k2eAgFnSc1d1	komplexní
prehistorická	prehistorický	k2eAgFnSc1d1	prehistorická
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejuznávanějšího	uznávaný	k2eAgInSc2d3	nejuznávanější
názoru	názor	k1gInSc2	názor
antropologů	antropolog	k1gMnPc2	antropolog
<g/>
,	,	kIx,	,
lingvistů	lingvista	k1gMnPc2	lingvista
a	a	k8xC	a
genetiků	genetik	k1gMnPc2	genetik
byly	být	k5eAaImAgInP	být
rané	raný	k2eAgInPc1d1	raný
brazilské	brazilský	k2eAgInPc1d1	brazilský
kmeny	kmen	k1gInPc1	kmen
součástí	součást	k1gFnSc7	součást
první	první	k4xOgFnSc2	první
vlny	vlna	k1gFnSc2	vlna
migrujících	migrující	k2eAgMnPc2d1	migrující
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Beringovu	Beringův	k2eAgFnSc4d1	Beringova
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
námořní	námořní	k2eAgFnSc7d1	námořní
cestou	cesta	k1gFnSc7	cesta
podél	podél	k7c2	podél
Pacifického	pacifický	k2eAgNnSc2d1	pacifické
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kombinací	kombinace	k1gFnSc7	kombinace
obojího	obojí	k4xRgMnSc2	obojí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andy	Anda	k1gFnPc1	Anda
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
pohoří	pohoří	k1gNnPc1	pohoří
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
ustanovily	ustanovit	k5eAaPmAgInP	ustanovit
poměrně	poměrně	k6eAd1	poměrně
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kulturní	kulturní	k2eAgFnSc4d1	kulturní
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
usedlými	usedlý	k2eAgFnPc7d1	usedlá
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
civilizacemi	civilizace	k1gFnPc7	civilizace
západního	západní	k2eAgInSc2d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
polokočovnými	polokočovný	k2eAgInPc7d1	polokočovný
kmeny	kmen	k1gInPc7	kmen
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nikdy	nikdy	k6eAd1	nikdy
nevytvořily	vytvořit	k5eNaPmAgInP	vytvořit
písemné	písemný	k2eAgInPc1d1	písemný
záznamy	záznam	k1gInPc1	záznam
ani	ani	k8xC	ani
trvalé	trvalý	k2eAgFnPc1d1	trvalá
monumentální	monumentální	k2eAgFnPc1d1	monumentální
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgMnSc4	ten
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
Brazílie	Brazílie	k1gFnSc2	Brazílie
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1500	[number]	k4	1500
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
především	především	k9	především
keramika	keramika	k1gFnSc1	keramika
<g/>
)	)	kIx)	)
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
složité	složitý	k2eAgInPc1d1	složitý
vzorce	vzorec	k1gInPc1	vzorec
kulturního	kulturní	k2eAgInSc2d1	kulturní
vývoje	vývoj	k1gInSc2	vývoj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
migrace	migrace	k1gFnSc2	migrace
a	a	k8xC	a
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
kvazistátních	kvazistátní	k2eAgFnPc2d1	kvazistátní
federací	federace	k1gFnPc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Brazílie	Brazílie	k1gFnSc2	Brazílie
asi	asi	k9	asi
2000	[number]	k4	2000
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tradičně	tradičně	k6eAd1	tradičně
tvořily	tvořit	k5eAaImAgInP	tvořit
převážně	převážně	k6eAd1	převážně
polokočovné	polokočovný	k2eAgInPc1d1	polokočovný
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
živily	živit	k5eAaImAgFnP	živit
lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
,	,	kIx,	,
sběrem	sběr	k1gInSc7	sběr
a	a	k8xC	a
migračním	migrační	k2eAgNnSc7d1	migrační
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
dorazili	dorazit	k5eAaPmAgMnP	dorazit
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
domorodci	domorodec	k1gMnPc1	domorodec
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
podél	podél	k7c2	podél
větších	veliký	k2eAgFnPc2d2	veliký
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
a	a	k8xC	a
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
těžbu	těžba	k1gFnSc4	těžba
sapanu	sapan	k1gInSc2	sapan
<g/>
,	,	kIx,	,
poskytujícího	poskytující	k2eAgInSc2d1	poskytující
ceněné	ceněný	k2eAgNnSc4d1	ceněné
červené	červený	k2eAgNnSc4d1	červené
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
Portugalce	Portugalec	k1gMnPc4	Portugalec
o	o	k7c4	o
potřebnosti	potřebnost	k1gFnPc4	potřebnost
christianizace	christianizace	k1gFnSc2	christianizace
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Španělé	Španěl	k1gMnPc1	Španěl
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
jihoamerických	jihoamerický	k2eAgFnPc6d1	jihoamerická
državách	država	k1gFnPc6	država
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nevědomky	nevědomky	k6eAd1	nevědomky
přinášeli	přinášet	k5eAaImAgMnP	přinášet
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgMnPc3	jenž
domorodci	domorodec	k1gMnPc1	domorodec
neměli	mít	k5eNaImAgMnP	mít
imunitu	imunita	k1gFnSc4	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Spalničky	spalničky	k1gFnPc1	spalničky
<g/>
,	,	kIx,	,
neštovice	neštovice	k1gFnPc1	neštovice
<g/>
,	,	kIx,	,
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
kapavka	kapavka	k1gFnSc1	kapavka
a	a	k8xC	a
chřipka	chřipka	k1gFnSc1	chřipka
zabily	zabít	k5eAaPmAgFnP	zabít
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc1	nemoc
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířily	šířit	k5eAaImAgFnP	šířit
po	po	k7c6	po
domorodých	domorodý	k2eAgFnPc6d1	domorodá
obchodních	obchodní	k2eAgFnPc6d1	obchodní
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celé	celý	k2eAgInPc1d1	celý
kmeny	kmen	k1gInPc1	kmen
byly	být	k5eAaImAgInP	být
vyhlazeny	vyhlazen	k2eAgInPc4d1	vyhlazen
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
Evropany	Evropan	k1gMnPc7	Evropan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kultura	kultura	k1gFnSc1	kultura
Marajoara	Marajoara	k1gFnSc1	Marajoara
===	===	k?	===
</s>
</p>
<p>
<s>
Kultura	kultura	k1gFnSc1	kultura
Marajoara	Marajoara	k1gFnSc1	Marajoara
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Marajó	Marajó	k1gFnSc2	Marajó
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
sofistikovaná	sofistikovaný	k2eAgFnSc1d1	sofistikovaná
keramika	keramika	k1gFnSc1	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgInPc1d1	nalezený
předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
zdobené	zdobený	k2eAgInPc1d1	zdobený
složitou	složitý	k2eAgFnSc7d1	složitá
malbou	malba	k1gFnSc7	malba
a	a	k8xC	a
řezbou	řezba	k1gFnSc7	řezba
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
živočichy	živočich	k1gMnPc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
první	první	k4xOgInSc4	první
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marajó	Marajó	k1gFnSc6	Marajó
existovala	existovat	k5eAaImAgFnS	existovat
složitěji	složitě	k6eAd2	složitě
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
mohylových	mohylový	k2eAgFnPc2d1	Mohylová
staveb	stavba	k1gFnPc2	stavba
dále	daleko	k6eAd2	daleko
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
ostrově	ostrov	k1gInSc6	ostrov
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
lidnatá	lidnatý	k2eAgFnSc1d1	lidnatá
<g/>
,	,	kIx,	,
komplexní	komplexní	k2eAgFnSc1d1	komplexní
a	a	k8xC	a
sofistikovaná	sofistikovaný	k2eAgNnPc4d1	sofistikované
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
takováto	takovýto	k3xDgNnPc1	takovýto
sídliště	sídliště	k1gNnPc1	sídliště
dokázala	dokázat	k5eAaPmAgNnP	dokázat
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
zemní	zemní	k2eAgFnPc1d1	zemní
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
<g/>
Nicméně	nicméně	k8xC	nicméně
rozsah	rozsah	k1gInSc1	rozsah
Marajoarské	Marajoarský	k2eAgFnSc2d1	Marajoarský
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
stupeň	stupeň	k1gInSc1	stupeň
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
hospodaření	hospodaření	k1gNnSc2	hospodaření
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
předmětem	předmět	k1gInSc7	předmět
pochybností	pochybnost	k1gFnPc2	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
badatelka	badatelka	k1gFnSc1	badatelka
Betty	Betty	k1gFnSc1	Betty
Meggersová	Meggersová	k1gFnSc1	Meggersová
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
raných	raný	k2eAgInPc6d1	raný
výzkumných	výzkumný	k2eAgInPc6d1	výzkumný
pracích	prak	k1gInPc6	prak
z	z	k7c2	z
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
kultury	kultura	k1gFnSc2	kultura
Marajoara	Marajoara	k1gFnSc1	Marajoara
migrovali	migrovat	k5eAaImAgMnP	migrovat
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vědců	vědec	k1gMnPc2	vědec
zastávalo	zastávat	k5eAaImAgNnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andy	Anda	k1gFnPc1	Anda
byly	být	k5eAaImAgFnP	být
osídleny	osídlit	k5eAaPmNgFnP	osídlit
paleoindiánskými	paleoindiánský	k2eAgFnPc7d1	paleoindiánský
migranty	migrant	k1gMnPc7	migrant
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
lovci	lovec	k1gMnPc1	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
archeoložka	archeoložka	k1gFnSc1	archeoložka
Anna	Anna	k1gFnSc1	Anna
Curtenius	Curtenius	k1gInSc4	Curtenius
Rooseveltová	Rooseveltová	k1gFnSc1	Rooseveltová
vedla	vést	k5eAaImAgFnS	vést
vykopávky	vykopávka	k1gFnSc2	vykopávka
a	a	k8xC	a
geofyzikální	geofyzikální	k2eAgInPc1d1	geofyzikální
průzkumy	průzkum	k1gInPc1	průzkum
mohyly	mohyla	k1gFnSc2	mohyla
Teso	Teso	k1gMnSc1	Teso
dos	dos	k?	dos
Bichos	Bichos	k1gMnSc1	Bichos
<g/>
.	.	kIx.	.
</s>
<s>
Došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohyly	mohyla	k1gFnPc4	mohyla
stavěla	stavět	k5eAaImAgFnS	stavět
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
<g/>
Předkolumbovská	předkolumbovský	k2eAgFnSc1d1	předkolumbovská
kultura	kultura	k1gFnSc1	kultura
Marajó	Marajó	k1gFnSc1	Marajó
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
sociální	sociální	k2eAgFnSc4d1	sociální
stratifikaci	stratifikace	k1gFnSc4	stratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
populace	populace	k1gFnSc1	populace
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
možná	možná	k9	možná
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Domorodci	domorodec	k1gMnPc1	domorodec
amazonského	amazonský	k2eAgInSc2d1	amazonský
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovládali	ovládat	k5eAaImAgMnP	ovládat
kultivaci	kultivace	k1gFnSc4	kultivace
půdy	půda	k1gFnSc2	půda
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
Terra	Terra	k1gFnSc1	Terra
preta	preta	k1gFnSc1	preta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
velkoplošné	velkoplošný	k2eAgNnSc4d1	velkoplošné
zemědělství	zemědělství	k1gNnSc4	zemědělství
potřebné	potřebný	k2eAgNnSc4d1	potřebné
k	k	k7c3	k
existenci	existence	k1gFnSc3	existence
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
složitých	složitý	k2eAgFnPc2d1	složitá
společenských	společenský	k2eAgFnPc2d1	společenská
a	a	k8xC	a
vládních	vládní	k2eAgFnPc2d1	vládní
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
náčelnictví	náčelnictví	k1gNnSc1	náčelnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Raná	raný	k2eAgFnSc1d1	raná
Brazílie	Brazílie	k1gFnSc1	Brazílie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
kolonizace	kolonizace	k1gFnSc1	kolonizace
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Brazílie	Brazílie	k1gFnSc2	Brazílie
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
široce	široko	k6eAd1	široko
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
názoru	názor	k1gInSc2	názor
o	o	k7c4	o
prvenství	prvenství	k1gNnSc4	prvenství
Pedra	Pedr	k1gMnSc2	Pedr
Cabrala	Cabral	k1gMnSc2	Cabral
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
teorie	teorie	k1gFnPc1	teorie
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
Brazílii	Brazílie	k1gFnSc3	Brazílie
objevil	objevit	k5eAaPmAgInS	objevit
Duarte	Duart	k1gInSc5	Duart
Pacheco	Pacheco	k6eAd1	Pacheco
Pereira	Pereiro	k1gNnPc1	Pereiro
mezi	mezi	k7c7	mezi
listopadem	listopad	k1gInSc7	listopad
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
1498	[number]	k4	1498
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
verze	verze	k1gFnPc1	verze
připisují	připisovat	k5eAaImIp3nP	připisovat
první	první	k4xOgInSc4	první
kontakt	kontakt	k1gInSc4	kontakt
Vicentu	Vicent	k1gMnSc3	Vicent
Yáñ	Yáñ	k1gMnSc3	Yáñ
Pinzónovi	Pinzón	k1gMnSc3	Pinzón
<g/>
,	,	kIx,	,
španělskému	španělský	k2eAgMnSc3d1	španělský
mořeplavci	mořeplavec	k1gMnSc3	mořeplavec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
objevitelské	objevitelský	k2eAgFnSc6d1	objevitelská
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
údajně	údajně	k6eAd1	údajně
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
Pernambuco	Pernambuco	k6eAd1	Pernambuco
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
území	území	k1gNnSc4	území
nárokovat	nárokovat	k5eAaImF	nárokovat
pro	pro	k7c4	pro
Španělskou	španělský	k2eAgFnSc4d1	španělská
korunu	koruna	k1gFnSc4	koruna
kvůli	kvůli	k7c3	kvůli
smlouvě	smlouva	k1gFnSc3	smlouva
z	z	k7c2	z
Tordesillas	Tordesillasa	k1gFnPc2	Tordesillasa
<g/>
.	.	kIx.	.
</s>
<s>
Příjezdem	příjezd	k1gInSc7	příjezd
portugalské	portugalský	k2eAgFnSc2d1	portugalská
flotily	flotila	k1gFnSc2	flotila
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Pedra	Pedr	k1gMnSc2	Pedr
Álvarese	Álvarese	k1gFnSc2	Álvarese
Cabrala	Cabral	k1gMnSc2	Cabral
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
se	se	k3xPyFc4	se
Brazílie	Brazílie	k1gFnSc1	Brazílie
stala	stát	k5eAaPmAgFnS	stát
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
doménou	doména	k1gFnSc7	doména
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
domorodci	domorodec	k1gMnPc7	domorodec
používajícími	používající	k2eAgMnPc7d1	používající
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
rozdělenými	rozdělená	k1gFnPc7	rozdělená
do	do	k7c2	do
několika	několik	k4yIc2	několik
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnozí	mnohý	k2eAgMnPc1d1	mnohý
sdíleli	sdílet	k5eAaImAgMnP	sdílet
stejnou	stejný	k2eAgFnSc4d1	stejná
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
Tupi-Guarani	Tupi-Guaran	k1gMnPc1	Tupi-Guaran
a	a	k8xC	a
vedli	vést	k5eAaImAgMnP	vést
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
evropanů	evropan	k1gMnPc2	evropan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
vývozním	vývozní	k2eAgInSc7d1	vývozní
artiklem	artikl	k1gInSc7	artikl
druh	druh	k1gInSc1	druh
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
kolonisty	kolonista	k1gMnPc7	kolonista
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
pau-Brasil	pau-Brasit	k5eAaImAgMnS	pau-Brasit
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
odvozený	odvozený	k2eAgInSc1d1	odvozený
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgNnSc1d1	znamenající
dřevo	dřevo	k1gNnSc1	dřevo
červené	červená	k1gFnSc2	červená
jako	jako	k8xS	jako
uhlíky	uhlík	k1gInPc4	uhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgNnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
strom	strom	k1gInSc4	strom
sapan	sapan	k1gInSc1	sapan
ježatý	ježatý	k2eAgInSc1d1	ježatý
(	(	kIx(	(
<g/>
Caesalpinia	Caesalpinium	k1gNnSc2	Caesalpinium
echinata	echinata	k1gFnSc1	echinata
<g/>
)	)	kIx)	)
jehož	jehož	k3xOyRp3gInSc1	jehož
kmen	kmen	k1gInSc1	kmen
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ceněné	ceněný	k2eAgNnSc4d1	ceněné
červené	červený	k2eAgNnSc4d1	červené
barvivo	barvivo	k1gNnSc4	barvivo
a	a	k8xC	a
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
vymýcen	vymýtit	k5eAaPmNgInS	vymýtit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
exploatace	exploatace	k1gFnSc2	exploatace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
Brazílii	Brazílie	k1gFnSc6	Brazílie
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
vysoce	vysoce	k6eAd1	vysoce
lukrativnímu	lukrativní	k2eAgInSc3d1	lukrativní
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
Východní	východní	k2eAgFnSc7d1	východní
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
nezájmu	nezájem	k1gInSc2	nezájem
využívali	využívat	k5eAaImAgMnP	využívat
cizí	cizí	k2eAgMnPc1d1	cizí
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
piráti	pirát	k1gMnPc1	pirát
a	a	k8xC	a
korzáři	korzár	k1gMnPc1	korzár
k	k	k7c3	k
ilegální	ilegální	k2eAgFnSc3d1	ilegální
těžbě	těžba	k1gFnSc3	těžba
cenného	cenný	k2eAgInSc2d1	cenný
sapanu	sapan	k1gInSc2	sapan
v	v	k7c6	v
portugalských	portugalský	k2eAgFnPc6d1	portugalská
državách	država	k1gFnPc6	država
<g/>
.	.	kIx.	.
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
koruna	koruna	k1gFnSc1	koruna
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pokusila	pokusit	k5eAaPmAgFnS	pokusit
vytvořit	vytvořit	k5eAaPmF	vytvořit
systém	systém	k1gInSc4	systém
umožňující	umožňující	k2eAgInSc4d1	umožňující
efektivní	efektivní	k2eAgFnSc4d1	efektivní
správu	správa	k1gFnSc4	správa
Brazílie	Brazílie	k1gFnSc2	Brazílie
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
státních	státní	k2eAgInPc2d1	státní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
systému	systém	k1gInSc2	systém
dědičných	dědičný	k2eAgInPc2d1	dědičný
kapitanátů	kapitanát	k1gInPc2	kapitanát
(	(	kIx(	(
<g/>
capitanías	capitanías	k1gMnSc1	capitanías
hereditarias	hereditarias	k1gMnSc1	hereditarias
nebo	nebo	k8xC	nebo
též	též	k9	též
capitanías	capitanías	k1gMnSc1	capitanías
donatárias	donatárias	k1gMnSc1	donatárias
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pásy	pás	k1gInPc4	pás
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
poskytnutých	poskytnutý	k2eAgFnPc2d1	poskytnutá
portugalským	portugalský	k2eAgMnPc3d1	portugalský
šlechticům	šlechtic	k1gMnPc3	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
měli	mít	k5eAaImAgMnP	mít
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
okupaci	okupace	k1gFnSc4	okupace
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
s	s	k7c7	s
přímou	přímý	k2eAgFnSc7d1	přímá
zodpovědností	zodpovědnost	k1gFnSc7	zodpovědnost
vůči	vůči	k7c3	vůči
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
celkově	celkově	k6eAd1	celkově
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
obsadit	obsadit	k5eAaPmF	obsadit
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
kapitanáty	kapitanát	k1gInPc4	kapitanát
<g/>
:	:	kIx,	:
Pernambuco	Pernambuca	k1gMnSc5	Pernambuca
<g/>
,	,	kIx,	,
Sã	Sã	k1gMnSc5	Sã
Vicente	Vicent	k1gMnSc5	Vicent
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ilhéus	Ilhéus	k1gInSc1	Ilhéus
a	a	k8xC	a
Porto	porto	k1gNnSc1	porto
Seguro	Segura	k1gFnSc5	Segura
<g/>
.	.	kIx.	.
</s>
<s>
Kapitanáty	kapitanát	k1gInPc1	kapitanát
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vrátily	vrátit	k5eAaPmAgInP	vrátit
Koruně	koruna	k1gFnSc3	koruna
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
státy	stát	k1gInPc7	stát
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1549	[number]	k4	1549
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
všech	všecek	k3xTgInPc2	všecek
kapitanátů	kapitanát	k1gInPc2	kapitanát
kolonie	kolonie	k1gFnSc2	kolonie
Brazílie	Brazílie	k1gFnSc2	Brazílie
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
město	město	k1gNnSc1	město
Sã	Sã	k1gFnSc2	Sã
Salvador	Salvador	k1gInSc1	Salvador
da	da	k?	da
Bahía	Bahí	k1gInSc2	Bahí
de	de	k?	de
Todos	Todos	k1gInSc1	Todos
os	osa	k1gFnPc2	osa
Santos	Santos	k1gInSc1	Santos
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Salvador	Salvador	k1gInSc1	Salvador
<g/>
)	)	kIx)	)
a	a	k8xC	a
zřízen	zřízen	k2eAgInSc1d1	zřízen
úřad	úřad	k1gInSc1	úřad
generálního	generální	k2eAgMnSc2d1	generální
guvernéra	guvernér	k1gMnSc2	guvernér
(	(	kIx(	(
<g/>
vicekrále	vicekrál	k1gMnSc2	vicekrál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Domorodé	domorodý	k2eAgFnSc2d1	domorodá
vzpoury	vzpoura	k1gFnSc2	vzpoura
===	===	k?	===
</s>
</p>
<p>
<s>
Konfederace	konfederace	k1gFnSc1	konfederace
Tamoyo	Tamoyo	k6eAd1	Tamoyo
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
Confederaçã	Confederaçã	k1gMnSc1	Confederaçã
dos	dos	k?	dos
Tamoios	Tamoios	k1gMnSc1	Tamoios
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc4d1	trvající
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1554	[number]	k4	1554
do	do	k7c2	do
1567	[number]	k4	1567
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
aliancí	aliance	k1gFnSc7	aliance
domorodých	domorodý	k2eAgMnPc2d1	domorodý
náčelníků	náčelník	k1gMnPc2	náčelník
kmenů	kmen	k1gInPc2	kmen
sídlících	sídlící	k2eAgInPc2d1	sídlící
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
od	od	k7c2	od
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Santos	Santos	k1gInSc1	Santos
až	až	k9	až
k	k	k7c3	k
Riu	Riu	k1gFnSc3	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc6	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
této	tento	k3xDgFnSc2	tento
poměrně	poměrně	k6eAd1	poměrně
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
aliance	aliance	k1gFnSc2	aliance
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
kmeny	kmen	k1gInPc7	kmen
byl	být	k5eAaImAgMnS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
zotročování	zotročování	k1gNnSc4	zotročování
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
masakry	masakr	k1gInPc4	masakr
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
Tupinambové	Tupinamb	k1gMnPc1	Tupinamb
trpěli	trpět	k5eAaImAgMnP	trpět
od	od	k7c2	od
raných	raný	k2eAgMnPc2d1	raný
portugalských	portugalský	k2eAgMnPc2d1	portugalský
objevitelů	objevitel	k1gMnPc2	objevitel
a	a	k8xC	a
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Tamuya	Tamuya	k1gFnSc1	Tamuya
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Tupi	Tup	k1gFnSc2	Tup
"	"	kIx"	"
<g/>
stařešina	stařešina	k1gFnSc1	stařešina
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Náčelníci	náčelník	k1gMnPc1	náčelník
kmenů	kmen	k1gInPc2	kmen
zvolili	zvolit	k5eAaPmAgMnP	zvolit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Cunhambebea	Cunhambebea	k1gMnSc1	Cunhambebea
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vůdce	vůdce	k1gMnSc4	vůdce
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
náčelníky	náčelník	k1gMnPc7	náčelník
Pindobuçú	Pindobuçú	k1gFnSc2	Pindobuçú
<g/>
,	,	kIx,	,
Koakira	Koakira	k1gMnSc1	Koakira
<g/>
,	,	kIx,	,
Araraí	Araraí	k1gMnSc1	Araraí
a	a	k8xC	a
Aimberê	Aimberê	k1gMnSc1	Aimberê
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Portugalcům	Portugalec	k1gMnPc3	Portugalec
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věk	věk	k1gInSc1	věk
cukru	cukr	k1gInSc2	cukr
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
základem	základ	k1gInSc7	základ
brazilské	brazilský	k2eAgFnSc2d1	brazilská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
stalo	stát	k5eAaPmAgNnS	stát
plantážní	plantážní	k2eAgNnSc1d1	plantážní
pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
export	export	k1gInSc4	export
cukru	cukr	k1gInSc2	cukr
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
plantáže	plantáž	k1gFnPc1	plantáž
zvané	zvaný	k2eAgFnPc1d1	zvaná
engenhos	engenhos	k1gInSc4	engenhos
se	se	k3xPyFc4	se
rozkládaly	rozkládat	k5eAaImAgFnP	rozkládat
podél	podél	k7c2	podél
severovýchodního	severovýchodní	k2eAgNnSc2d1	severovýchodní
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
brazilské	brazilský	k2eAgNnSc1d1	brazilské
Nordeste	Nordest	k1gInSc5	Nordest
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
osadníci	osadník	k1gMnPc1	osadník
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
zotročit	zotročit	k5eAaPmF	zotročit
domorodce	domorodec	k1gMnPc4	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
bylo	být	k5eAaImAgNnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
plantážního	plantážní	k2eAgInSc2d1	plantážní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zavedeného	zavedený	k2eAgMnSc2d1	zavedený
na	na	k7c6	na
atlantických	atlantický	k2eAgInPc6d1	atlantický
ostrovech	ostrov	k1gInPc6	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Sã	Sã	k1gFnSc1	Sã
Tomé	Tomá	k1gFnSc2	Tomá
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
nucenou	nucený	k2eAgFnSc4d1	nucená
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kapitálové	kapitálový	k2eAgFnPc4d1	kapitálová
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
pracovních	pracovní	k2eAgNnPc2d1	pracovní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Extenzivní	extenzivní	k2eAgNnSc1d1	extenzivní
pěstování	pěstování	k1gNnSc1	pěstování
třtiny	třtina	k1gFnSc2	třtina
pro	pro	k7c4	pro
cukr	cukr	k1gInSc4	cukr
určený	určený	k2eAgInSc4d1	určený
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
plochy	plocha	k1gFnPc4	plocha
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
získána	získat	k5eAaPmNgFnS	získat
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
bez	bez	k7c2	bez
většího	veliký	k2eAgInSc2d2	veliký
odporu	odpor	k1gInSc2	odpor
domorodého	domorodý	k2eAgNnSc2d1	domorodé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1570	[number]	k4	1570
produkce	produkce	k1gFnSc1	produkce
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
produkci	produkce	k1gFnSc4	produkce
atlantických	atlantický	k2eAgInPc2d1	atlantický
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
již	již	k9	již
byla	být	k5eAaImAgFnS	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
průzkumy	průzkum	k1gInPc1	průzkum
brazilského	brazilský	k2eAgNnSc2d1	brazilské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
převážně	převážně	k6eAd1	převážně
prováděli	provádět	k5eAaImAgMnP	provádět
polovojenští	polovojenský	k2eAgMnPc1d1	polovojenský
dobrodruhové	dobrodruh	k1gMnPc1	dobrodruh
<g/>
,	,	kIx,	,
bandeirantes	bandeirantes	k1gMnSc1	bandeirantes
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pronikali	pronikat	k5eAaImAgMnP	pronikat
do	do	k7c2	do
džungle	džungle	k1gFnSc2	džungle
kvůli	kvůli	k7c3	kvůli
hledání	hledání	k1gNnSc3	hledání
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
domorodých	domorodý	k2eAgMnPc2d1	domorodý
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
však	však	k9	však
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
trvale	trvale	k6eAd1	trvale
zotročovat	zotročovat	k5eAaImF	zotročovat
domorodce	domorodec	k1gMnPc4	domorodec
a	a	k8xC	a
portugalští	portugalský	k2eAgMnPc1d1	portugalský
plantážníci	plantážník	k1gMnPc1	plantážník
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
začali	začít	k5eAaPmAgMnP	začít
brzy	brzy	k6eAd1	brzy
dovážet	dovážet	k5eAaImF	dovážet
miliony	milion	k4xCgInPc4	milion
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
otroků	otrok	k1gMnPc2	otrok
na	na	k7c6	na
třtinových	třtinový	k2eAgFnPc6d1	třtinová
plantážích	plantáž	k1gFnPc6	plantáž
a	a	k8xC	a
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
zlata	zlato	k1gNnSc2	zlato
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
nevhodným	vhodný	k2eNgFnPc3d1	nevhodná
životním	životní	k2eAgFnPc3d1	životní
podmínkám	podmínka	k1gFnPc3	podmínka
se	se	k3xPyFc4	se
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
jejich	jejich	k3xOp3gFnSc1	jejich
populace	populace	k1gFnSc1	populace
přirozeně	přirozeně	k6eAd1	přirozeně
obnovovat	obnovovat	k5eAaImF	obnovovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Afričané	Afričan	k1gMnPc1	Afričan
stali	stát	k5eAaPmAgMnP	stát
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
brazilského	brazilský	k2eAgNnSc2d1	brazilské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
zrušením	zrušení	k1gNnSc7	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
mísit	mísit	k5eAaImF	mísit
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
brazilskou	brazilský	k2eAgFnSc7d1	brazilská
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Afričtí	africký	k2eAgMnPc1d1	africký
otroci	otrok	k1gMnPc1	otrok
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
nuceně	nuceně	k6eAd1	nuceně
křtěni	křtěn	k2eAgMnPc1d1	křtěn
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
však	však	k8xC	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
u	u	k7c2	u
svých	svůj	k3xOyFgNnPc2	svůj
tradičních	tradiční	k2eAgNnPc2d1	tradiční
afrických	africký	k2eAgNnPc2d1	africké
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
základu	základ	k1gInSc2	základ
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
současné	současný	k2eAgInPc1d1	současný
afro-brazilské	afrorazilský	k2eAgInPc1d1	afro-brazilský
synkretické	synkretický	k2eAgInPc1d1	synkretický
kulty	kult	k1gInPc1	kult
umbanda	umbando	k1gNnSc2	umbando
a	a	k8xC	a
candomblé	candomblý	k2eAgFnSc2d1	candomblý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
150	[number]	k4	150
let	léto	k1gNnPc2	léto
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
zlákané	zlákaný	k2eAgInPc1d1	zlákaný
obrovskými	obrovský	k2eAgInPc7d1	obrovský
přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
a	a	k8xC	a
nevyužitou	využitý	k2eNgFnSc7d1	nevyužitá
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
zřídit	zřídit	k5eAaPmF	zřídit
kolonie	kolonie	k1gFnSc1	kolonie
v	v	k7c6	v
několika	několik	k4yIc6	několik
částech	část	k1gFnPc6	část
brazilského	brazilský	k2eAgNnSc2d1	brazilské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
ignorujíce	ignorovat	k5eAaImSgFnP	ignorovat
papežskou	papežský	k2eAgFnSc4d1	Papežská
bulu	bula	k1gFnSc4	bula
Inter	Inter	k1gInSc1	Inter
caetera	caeter	k1gMnSc2	caeter
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
z	z	k7c2	z
Tordesillas	Tordesillasa	k1gFnPc2	Tordesillasa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
Nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
mezi	mezi	k7c4	mezi
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
kolonisté	kolonista	k1gMnPc1	kolonista
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nedlouho	dlouho	k6eNd1	dlouho
trvající	trvající	k2eAgMnPc1d1	trvající
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Rio	Rio	k1gMnSc2	Rio
de	de	k?	de
Janeira	Janeira	k1gMnSc1	Janeira
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1555	[number]	k4	1555
až	až	k9	až
1567	[number]	k4	1567
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Antarktická	antarktický	k2eAgFnSc1d1	antarktická
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
France	Franc	k1gMnSc2	Franc
Antarctique	Antarctiqu	k1gMnSc2	Antarctiqu
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Sã	Sã	k1gFnPc3	Sã
Luís	Luís	k1gInSc4	Luís
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1612	[number]	k4	1612
až	až	k6eAd1	až
1614	[number]	k4	1614
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
France	Franc	k1gMnSc2	Franc
Équinoxiale	Équinoxiala	k1gFnSc3	Équinoxiala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
přišli	přijít	k5eAaPmAgMnP	přijít
brzy	brzy	k6eAd1	brzy
také	také	k9	také
Jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
Sã	Sã	k1gFnSc4	Sã
Paulo	Paula	k1gFnSc5	Paula
a	a	k8xC	a
křtili	křtít	k5eAaImAgMnP	křtít
tamější	tamější	k2eAgMnPc4d1	tamější
domorodce	domorodec	k1gMnPc4	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
domorodí	domorodý	k2eAgMnPc1d1	domorodý
spojenci	spojenec	k1gMnPc1	spojenec
jezuitů	jezuita	k1gMnPc2	jezuita
pak	pak	k9	pak
Portugalcům	Portugalec	k1gMnPc3	Portugalec
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
vyhnat	vyhnat	k5eAaPmF	vyhnat
Francouzy	Francouzy	k1gInPc4	Francouzy
<g/>
.	.	kIx.	.
</s>
<s>
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
kolonii	kolonie	k1gFnSc4	kolonie
France	Franc	k1gMnSc2	Franc
Antarctique	Antarctiqu	k1gMnSc2	Antarctiqu
zničil	zničit	k5eAaPmAgInS	zničit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1567	[number]	k4	1567
třetí	třetí	k4xOgMnSc1	třetí
brazilský	brazilský	k2eAgMnSc1d1	brazilský
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Mem	Mem	k1gMnSc1	Mem
de	de	k?	de
Sá	Sá	k1gMnSc1	Sá
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
synovcem	synovec	k1gMnSc7	synovec
Estáciem	Estácium	k1gNnSc7	Estácium
de	de	k?	de
Sá	Sá	k1gFnSc1	Sá
poté	poté	k6eAd1	poté
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1567	[number]	k4	1567
založil	založit	k5eAaPmAgMnS	založit
osadu	osada	k1gFnSc4	osada
Rio	Rio	k1gFnSc3	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větším	veliký	k2eAgInSc7d2	veliký
zdrojem	zdroj	k1gInSc7	zdroj
potíží	potíž	k1gFnPc2	potíž
pro	pro	k7c4	pro
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vpád	vpád	k1gInSc4	vpád
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
delší	dlouhý	k2eAgNnSc4d2	delší
trvání	trvání	k1gNnSc4	trvání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
počátkem	počátek	k1gInSc7	počátek
byly	být	k5eAaImAgInP	být
drancující	drancující	k2eAgInPc1d1	drancující
nájezdy	nájezd	k1gInPc1	nájezd
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
korzárů	korzár	k1gMnPc2	korzár
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1604	[number]	k4	1604
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
provincii	provincie	k1gFnSc4	provincie
Bahía	Bahíum	k1gNnSc2	Bahíum
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
krátkodobě	krátkodobě	k6eAd1	krátkodobě
obsadili	obsadit	k5eAaPmAgMnP	obsadit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Salvador	Salvador	k1gInSc4	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1630	[number]	k4	1630
až	až	k8xS	až
1654	[number]	k4	1654
se	se	k3xPyFc4	se
již	již	k6eAd1	již
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
natrvalo	natrvalo	k6eAd1	natrvalo
uchytili	uchytit	k5eAaPmAgMnP	uchytit
a	a	k8xC	a
ovládali	ovládat	k5eAaImAgMnP	ovládat
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
úsek	úsek	k1gInSc4	úsek
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
však	však	k9	však
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Zabrali	zabrat	k5eAaPmAgMnP	zabrat
také	také	k9	také
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
plantáží	plantáž	k1gFnPc2	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kolonisté	kolonista	k1gMnPc1	kolonista
Nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
Západoindické	západoindický	k2eAgFnSc2d1	Západoindická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
byli	být	k5eAaImAgMnP	být
neustále	neustále	k6eAd1	neustále
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
obležení	obležení	k1gNnSc2	obležení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k7c3	navzdory
přítomnosti	přítomnost	k1gFnSc3	přítomnost
guvernéra	guvernér	k1gMnSc2	guvernér
Jana	Jan	k1gMnSc2	Jan
Mořice	Mořic	k1gMnSc2	Mořic
z	z	k7c2	z
Nassau-Singen	Nassau-Singen	k1gInSc4	Nassau-Singen
<g/>
,	,	kIx,	,
sídlícího	sídlící	k2eAgNnSc2d1	sídlící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Recife	Recif	k1gMnSc5	Recif
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
otevřené	otevřený	k2eAgFnSc2d1	otevřená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nezdařené	zdařený	k2eNgInPc1d1	nezdařený
pokusy	pokus	k1gInPc1	pokus
zanechaly	zanechat	k5eAaPmAgInP	zanechat
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
a	a	k8xC	a
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
etnického	etnický	k2eAgInSc2d1	etnický
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
síly	síl	k1gInPc1	síl
portugalských	portugalský	k2eAgMnPc2d1	portugalský
Brazilců	Brazilec	k1gMnPc2	Brazilec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
domorodých	domorodý	k2eAgMnPc2d1	domorodý
a	a	k8xC	a
afrobrazilských	afrobrazilský	k2eAgMnPc2d1	afrobrazilský
spojenců	spojenec	k1gMnPc2	spojenec
sice	sice	k8xC	sice
nakonec	nakonec	k6eAd1	nakonec
Nizozemce	Nizozemec	k1gMnPc4	Nizozemec
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
vypudily	vypudit	k5eAaPmAgInP	vypudit
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Angličany	Angličan	k1gMnPc7	Angličan
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
brazilský	brazilský	k2eAgInSc4d1	brazilský
model	model	k1gInSc4	model
plantážní	plantážní	k2eAgFnSc2d1	plantážní
produkce	produkce	k1gFnSc2	produkce
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
državách	država	k1gFnPc6	država
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
konkurence	konkurence	k1gFnSc1	konkurence
zapříčinily	zapříčinit	k5eAaPmAgInP	zapříčinit
pokles	pokles	k1gInSc4	pokles
cen	cena	k1gFnPc2	cena
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
brazilský	brazilský	k2eAgInSc1d1	brazilský
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
</s>
<s>
Zotavování	zotavování	k1gNnSc1	zotavování
Brazílie	Brazílie	k1gFnSc2	Brazílie
z	z	k7c2	z
holandského	holandský	k2eAgInSc2d1	holandský
vpádu	vpád	k1gInSc2	vpád
bylo	být	k5eAaImAgNnS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
následkem	následkem	k7c2	následkem
bojů	boj	k1gInPc2	boj
třtinové	třtinový	k2eAgFnSc2d1	třtinová
plantáže	plantáž	k1gFnSc2	plantáž
značně	značně	k6eAd1	značně
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Bahia	Bahium	k1gNnSc2	Bahium
se	se	k3xPyFc4	se
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
tabák	tabák	k1gInSc1	tabák
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
africký	africký	k2eAgInSc4d1	africký
exportní	exportní	k2eAgInSc4d1	exportní
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tabák	tabák	k1gInSc1	tabák
máčený	máčený	k2eAgInSc1d1	máčený
v	v	k7c6	v
melase	melasa	k1gFnSc6	melasa
(	(	kIx(	(
<g/>
vedlejším	vedlejší	k2eAgInSc6d1	vedlejší
produktu	produkt	k1gInSc6	produkt
výroby	výroba	k1gFnSc2	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
směňován	směňovat	k5eAaImNgInS	směňovat
za	za	k7c4	za
africké	africký	k2eAgMnPc4d1	africký
otroky	otrok	k1gMnPc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc1	hospodářství
Brazílie	Brazílie	k1gFnSc2	Brazílie
bylo	být	k5eAaImAgNnS	být
koncentrováno	koncentrovat	k5eAaBmNgNnS	koncentrovat
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Holandská	holandský	k2eAgFnSc1d1	holandská
invaze	invaze	k1gFnSc1	invaze
podtrhla	podtrhnout	k5eAaPmAgFnS	podtrhnout
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
Brazílie	Brazílie	k1gFnSc2	Brazílie
vůči	vůči	k7c3	vůči
cizím	cizí	k2eAgMnPc3d1	cizí
vpádům	vpád	k1gInPc3	vpád
<g/>
.	.	kIx.	.
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
koruna	koruna	k1gFnSc1	koruna
reagovala	reagovat	k5eAaBmAgFnS	reagovat
budováním	budování	k1gNnSc7	budování
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
pevností	pevnost	k1gFnPc2	pevnost
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
námořních	námořní	k2eAgFnPc2d1	námořní
hlídkových	hlídkový	k2eAgFnPc2d1	hlídková
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzpoury	vzpoura	k1gFnPc1	vzpoura
otroků	otrok	k1gMnPc2	otrok
===	===	k?	===
</s>
</p>
<p>
<s>
Vzpoury	vzpoura	k1gFnPc1	vzpoura
otroků	otrok	k1gMnPc2	otrok
byly	být	k5eAaImAgFnP	být
časté	častý	k2eAgFnPc1d1	častá
až	až	k9	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
z	z	k7c2	z
povstání	povstání	k1gNnSc2	povstání
vedl	vést	k5eAaImAgMnS	vést
Zumbi	Zumb	k1gMnSc3	Zumb
dos	dos	k?	dos
Palmares	Palmares	k1gMnSc1	Palmares
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
založil	založit	k5eAaPmAgInS	založit
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Quilombo	quilombo	k1gNnSc4	quilombo
dos	dos	k?	dos
Palmares	Palmares	k1gInSc1	Palmares
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
republikou	republika	k1gFnSc7	republika
maronů	maron	k1gInPc2	maron
<g/>
,	,	kIx,	,
uprchlých	uprchlý	k2eAgInPc2d1	uprchlý
z	z	k7c2	z
portugalských	portugalský	k2eAgFnPc2d1	portugalská
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
oblastí	oblast	k1gFnPc2	oblast
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
snad	snad	k9	snad
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Pernambuca	Pernambuc	k1gInSc2	Pernambuc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vrcholu	vrchol	k1gInSc6	vrchol
mělo	mít	k5eAaImAgNnS	mít
Palmares	Palmares	k1gInSc4	Palmares
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
opakovaným	opakovaný	k2eAgInPc3d1	opakovaný
útokům	útok	k1gInPc3	útok
portugalské	portugalský	k2eAgFnSc2d1	portugalská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
moci	moc	k1gFnSc2	moc
používali	používat	k5eAaImAgMnP	používat
bojovníci	bojovník	k1gMnPc1	bojovník
Palmares	Palmaresa	k1gFnPc2	Palmaresa
capoeiru	capoeir	k1gInSc2	capoeir
<g/>
,	,	kIx,	,
formu	forma	k1gFnSc4	forma
bojového	bojový	k2eAgNnSc2d1	bojové
umění	umění	k1gNnSc2	umění
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
africkými	africký	k2eAgMnPc7d1	africký
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Afričan	Afričan	k1gMnSc1	Afričan
známý	známý	k2eAgMnSc1d1	známý
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
Zumbi	Zumb	k1gFnSc3	Zumb
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
v	v	k7c6	v
Palmares	Palmares	k1gMnSc1	Palmares
jako	jako	k8xC	jako
svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
let	léto	k1gNnPc2	léto
ho	on	k3xPp3gInSc4	on
chytili	chytit	k5eAaPmAgMnP	chytit
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
předali	předat	k5eAaPmAgMnP	předat
misionáři	misionář	k1gMnPc1	misionář
<g/>
,	,	kIx,	,
páteru	páter	k1gMnSc6	páter
Antôniovi	Antônius	k1gMnSc6	Antônius
Melovi	Mela	k1gMnSc6	Mela
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
jako	jako	k9	jako
Francisco	Francisco	k1gMnSc1	Francisco
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
portugalštině	portugalština	k1gFnSc3	portugalština
a	a	k8xC	a
latině	latina	k1gFnSc3	latina
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
s	s	k7c7	s
každodenními	každodenní	k2eAgFnPc7d1	každodenní
mšemi	mše	k1gFnPc7	mše
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
snahy	snaha	k1gFnPc4	snaha
o	o	k7c6	o
"	"	kIx"	"
<g/>
zcivilizování	zcivilizování	k1gNnSc6	zcivilizování
<g/>
"	"	kIx"	"
Zumbi	Zumbi	k1gNnSc1	Zumbi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
a	a	k8xC	a
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodiště	rodiště	k1gNnSc2	rodiště
<g/>
.	.	kIx.	.
</s>
<s>
Zumbi	Zumbi	k6eAd1	Zumbi
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svou	svůj	k3xOyFgFnSc7	svůj
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
zdatností	zdatnost	k1gFnSc7	zdatnost
a	a	k8xC	a
vychytralostí	vychytralost	k1gFnSc7	vychytralost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
dvacátém	dvacátý	k4xOgInSc6	dvacátý
roce	rok	k1gInSc6	rok
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
vojenským	vojenský	k2eAgMnSc7d1	vojenský
stratégem	stratég	k1gMnSc7	stratég
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1678	[number]	k4	1678
se	se	k3xPyFc4	se
Pedro	Pedro	k1gNnSc1	Pedro
Almeida	Almeida	k1gFnSc1	Almeida
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
kapitanátu	kapitanát	k1gInSc2	kapitanát
Pernambuco	Pernambuco	k1gMnSc1	Pernambuco
<g/>
,	,	kIx,	,
unavený	unavený	k2eAgMnSc1d1	unavený
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Palmares	Palmares	k1gInSc1	Palmares
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
vůdce	vůdce	k1gMnPc4	vůdce
Ganga	Ganga	k1gFnSc1	Ganga
Zumbu	Zumb	k1gInSc2	Zumb
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Almeida	Almeid	k1gMnSc4	Almeid
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
svobodu	svoboda	k1gFnSc4	svoboda
všem	všecek	k3xTgMnPc3	všecek
uprchlým	uprchlý	k2eAgMnPc3d1	uprchlý
otrokům	otrok	k1gMnPc3	otrok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Palmares	Palmares	k1gInSc1	Palmares
podřídí	podřídit	k5eAaPmIp3nS	podřídit
portugalským	portugalský	k2eAgInPc3d1	portugalský
úřadům	úřad	k1gInPc3	úřad
<g/>
,	,	kIx,	,
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
Gangu	Ganga	k1gFnSc4	Ganga
Zumbovi	Zumba	k1gMnSc3	Zumba
zamlouval	zamlouvat	k5eAaImAgMnS	zamlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
Zumbi	Zumbi	k1gNnSc1	Zumbi
Portugalcům	Portugalec	k1gMnPc3	Portugalec
nedůvěřoval	důvěřovat	k5eNaImAgInS	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
svobodu	svoboda	k1gFnSc4	svoboda
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
Palmares	Palmares	k1gInSc4	Palmares
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
Afričané	Afričan	k1gMnPc1	Afričan
měli	mít	k5eAaImAgMnP	mít
zůstat	zůstat	k5eAaPmF	zůstat
zotročeni	zotročen	k2eAgMnPc1d1	zotročen
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Almeidinovy	Almeidinův	k2eAgInPc4d1	Almeidinův
návrhy	návrh	k1gInPc4	návrh
a	a	k8xC	a
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
vedení	vedení	k1gNnSc1	vedení
Ganga	Ganga	k1gFnSc1	Ganga
Zumby	Zumba	k1gMnSc2	Zumba
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
slibem	slib	k1gInSc7	slib
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
vůči	vůči	k7c3	vůči
portugalskému	portugalský	k2eAgInSc3d1	portugalský
útlaku	útlak	k1gInSc3	útlak
se	se	k3xPyFc4	se
Zumbi	Zumbe	k1gFnSc4	Zumbe
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
vůdcem	vůdce	k1gMnSc7	vůdce
Palmares	Palmaresa	k1gFnPc2	Palmaresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patnáct	patnáct	k4xCc1	patnáct
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
Zumbi	Zumbi	k1gNnSc4	Zumbi
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
Palmares	Palmaresa	k1gFnPc2	Palmaresa
<g/>
,	,	kIx,	,
portugalští	portugalský	k2eAgMnPc1d1	portugalský
vojenští	vojenský	k2eAgMnPc1d1	vojenský
velitelé	velitel	k1gMnPc1	velitel
Domingos	Domingosa	k1gFnPc2	Domingosa
Jorge	Jorg	k1gMnSc4	Jorg
Velho	Vel	k1gMnSc4	Vel
a	a	k8xC	a
Vieira	Vieir	k1gMnSc4	Vieir
de	de	k?	de
Melo	Melo	k?	Melo
provedli	provést	k5eAaPmAgMnP	provést
na	na	k7c4	na
quilombo	quilombo	k1gNnSc4	quilombo
útok	útok	k1gInSc4	útok
podporovaný	podporovaný	k2eAgInSc4d1	podporovaný
dělostřelectvem	dělostřelectvo	k1gNnSc7	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1694	[number]	k4	1694
<g/>
,	,	kIx,	,
po	po	k7c6	po
67	[number]	k4	67
letech	léto	k1gNnPc6	léto
neustálých	neustálý	k2eAgInPc2d1	neustálý
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
cafuzos	cafuzosa	k1gFnPc2	cafuzosa
(	(	kIx(	(
<g/>
marony	marona	k1gFnSc2	marona
<g/>
)	)	kIx)	)
z	z	k7c2	z
Palmares	Palmaresa	k1gFnPc2	Palmaresa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Portugalcům	Portugalec	k1gMnPc3	Portugalec
podařilo	podařit	k5eAaPmAgNnS	podařit
zničit	zničit	k5eAaPmF	zničit
Cerca	Cercum	k1gNnPc4	Cercum
do	do	k7c2	do
Macaco	Macaco	k6eAd1	Macaco
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Bojovníci	bojovník	k1gMnPc1	bojovník
Palmares	Palmaresa	k1gFnPc2	Palmaresa
nebyli	být	k5eNaImAgMnP	být
pro	pro	k7c4	pro
portugalské	portugalský	k2eAgNnSc4d1	portugalské
dělostřelectvo	dělostřelectvo	k1gNnSc4	dělostřelectvo
žádným	žádný	k3yNgMnSc7	žádný
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
;	;	kIx,	;
republika	republika	k1gFnSc1	republika
padla	padnout	k5eAaPmAgFnS	padnout
a	a	k8xC	a
Zumbi	Zumb	k1gFnSc2	Zumb
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
před	před	k7c7	před
Portugalci	Portugalec	k1gMnPc7	Portugalec
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zrazen	zradit	k5eAaPmNgMnS	zradit
<g/>
,	,	kIx,	,
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1695	[number]	k4	1695
<g/>
,	,	kIx,	,
chycen	chycen	k2eAgMnSc1d1	chycen
a	a	k8xC	a
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
sťat	stnout	k5eAaPmNgMnS	stnout
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
převezli	převézt	k5eAaPmAgMnP	převézt
Zumbiho	Zumbi	k1gMnSc4	Zumbi
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
Recife	Recif	k1gMnSc5	Recif
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
vystavili	vystavit	k5eAaPmAgMnP	vystavit
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
praça	praça	k1gMnSc1	praça
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
legendu	legenda	k1gFnSc4	legenda
o	o	k7c4	o
Zumbiho	Zumbi	k1gMnSc4	Zumbi
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc3d1	populární
mezi	mezi	k7c7	mezi
africkými	africký	k2eAgMnPc7d1	africký
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
varování	varování	k1gNnSc1	varování
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
napodobit	napodobit	k5eAaPmF	napodobit
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
starých	starý	k2eAgMnPc2d1	starý
quilombů	quilomb	k1gMnPc2	quilomb
přežívaly	přežívat	k5eAaImAgFnP	přežívat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ještě	ještě	k6eAd1	ještě
dalších	další	k2eAgMnPc2d1	další
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
a	a	k8xC	a
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
horečka	horečka	k1gFnSc1	horečka
===	===	k?	===
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
velké	velký	k2eAgNnSc4d1	velké
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomika	ekonomika	k1gFnSc1	ekonomika
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
válek	válka	k1gFnPc2	válka
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
nálezu	nález	k1gInSc2	nález
zlata	zlato	k1gNnSc2	zlato
proudili	proudit	k5eAaPmAgMnP	proudit
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
domácího	domácí	k2eAgNnSc2d1	domácí
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oblast	oblast	k1gFnSc1	oblast
brazilského	brazilský	k2eAgNnSc2d1	brazilské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zlato	zlato	k1gNnSc1	zlato
dobývalo	dobývat	k5eAaImAgNnS	dobývat
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
označení	označení	k1gNnSc4	označení
Minas	Minasa	k1gFnPc2	Minasa
Gerais	Gerais	k1gFnPc2	Gerais
(	(	kIx(	(
<g/>
Centrální	centrální	k2eAgInPc1d1	centrální
nebo	nebo	k8xC	nebo
Hlavní	hlavní	k2eAgInPc1d1	hlavní
doly	dol	k1gInPc1	dol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
aktivitou	aktivita	k1gFnSc7	aktivita
koloniální	koloniální	k2eAgFnSc2d1	koloniální
Brazílie	Brazílie	k1gFnSc2	Brazílie
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
se	se	k3xPyFc4	se
zlato	zlato	k1gNnSc1	zlato
používalo	používat	k5eAaImAgNnS	používat
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
platbě	platba	k1gFnSc3	platba
za	za	k7c4	za
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zboží	zboží	k1gNnSc4	zboží
(	(	kIx(	(
<g/>
textilie	textilie	k1gFnPc1	textilie
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
)	)	kIx)	)
dovážené	dovážený	k2eAgInPc1d1	dovážený
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc2	Jan
V.	V.	kA	V.
<g/>
,	,	kIx,	,
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
monumentálních	monumentální	k2eAgFnPc2d1	monumentální
barokních	barokní	k2eAgFnPc2d1	barokní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc4	klášter
Mafra	Mafr	k1gInSc2	Mafr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
zlatých	zlatý	k2eAgInPc6d1	zlatý
dolech	dol	k1gInPc6	dol
se	se	k3xPyFc4	se
využívali	využívat	k5eAaImAgMnP	využívat
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
způsobil	způsobit	k5eAaPmAgInS	způsobit
velký	velký	k2eAgInSc1d1	velký
příliv	příliv	k1gInSc1	příliv
evropských	evropský	k2eAgMnPc2d1	evropský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
vláda	vláda	k1gFnSc1	vláda
dostala	dostat	k5eAaPmAgFnS	dostat
těžbu	těžba	k1gFnSc4	těžba
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
přivézt	přivézt	k5eAaPmF	přivézt
úředníky	úředník	k1gMnPc4	úředník
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
konfliktními	konfliktní	k2eAgFnPc7d1	konfliktní
povinnostmi	povinnost	k1gFnPc7	povinnost
a	a	k8xC	a
jurisdikcemi	jurisdikce	k1gFnPc7	jurisdikce
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
tohoto	tento	k3xDgNnSc2	tento
vysoce	vysoce	k6eAd1	vysoce
lukrativního	lukrativní	k2eAgNnSc2d1	lukrativní
odvětví	odvětví	k1gNnSc2	odvětví
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
mimo	mimo	k7c4	mimo
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Brazilské	brazilský	k2eAgFnSc2d1	brazilská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
aktivitu	aktivita	k1gFnSc4	aktivita
Britové	Brit	k1gMnPc1	Brit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
otevřela	otevřít	k5eAaPmAgFnS	otevřít
Brity	Brit	k1gMnPc4	Brit
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
důlní	důlní	k2eAgFnSc1d1	důlní
společnost	společnost	k1gFnSc1	společnost
Saint	Saint	k1gMnSc1	Saint
John	John	k1gMnSc1	John
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
El	Ela	k1gFnPc2	Ela
Rey	Rea	k1gFnSc2	Rea
Mining	Mining	k1gInSc1	Mining
Company	Compana	k1gFnSc2	Compana
největší	veliký	k2eAgInSc4d3	veliký
zlatý	zlatý	k2eAgInSc4d1	zlatý
důl	důl	k1gInSc4	důl
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
přinesli	přinést	k5eAaPmAgMnP	přinést
moderní	moderní	k2eAgFnPc4d1	moderní
techniky	technika	k1gFnPc4	technika
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
know-how	knowow	k?	know-how
<g/>
.	.	kIx.	.
</s>
<s>
Důl	důl	k1gInSc1	důl
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nova	novum	k1gNnSc2	novum
Lima	limo	k1gNnSc2	limo
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
rudu	ruda	k1gFnSc4	ruda
celých	celý	k2eAgInPc2d1	celý
125	[number]	k4	125
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
ložisek	ložisko	k1gNnPc2	ložisko
diamantů	diamant	k1gInPc2	diamant
kolem	kolem	k7c2	kolem
vesnice	vesnice	k1gFnSc2	vesnice
Tijuco	Tijuco	k6eAd1	Tijuco
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
Vila	vít	k5eAaImAgFnS	vít
do	do	k7c2	do
Príncipe	Príncip	k1gMnSc5	Príncip
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
drahokamy	drahokam	k1gInPc1	drahokam
zaplavily	zaplavit	k5eAaPmAgInP	zaplavit
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
koruna	koruna	k1gFnSc1	koruna
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dostala	dostat	k5eAaPmAgFnS	dostat
těžbu	těžba	k1gFnSc4	těžba
v	v	k7c6	v
Diamantině	Diamantina	k1gFnSc6	Diamantina
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
systému	systém	k1gInSc2	systém
poskytování	poskytování	k1gNnSc2	poskytování
těžebních	těžební	k2eAgNnPc2d1	těžební
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
si	se	k3xPyFc3	se
ponechala	ponechat	k5eAaPmAgFnS	ponechat
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
<g/>
.	.	kIx.	.
<g/>
Dolování	dolování	k1gNnSc1	dolování
stimulovalo	stimulovat	k5eAaImAgNnS	stimulovat
regionální	regionální	k2eAgInSc4d1	regionální
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
samotné	samotný	k2eAgFnSc3d1	samotná
těžbě	těžba	k1gFnSc3	těžba
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
posílením	posílení	k1gNnSc7	posílení
produkce	produkce	k1gFnSc2	produkce
potravin	potravina	k1gFnPc2	potravina
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
důležitější	důležitý	k2eAgMnSc1d2	důležitější
byl	být	k5eAaImAgMnS	být
nárůst	nárůst	k1gInSc4	nárůst
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
obchodních	obchodní	k2eAgFnPc2d1	obchodní
komunit	komunita	k1gFnPc2	komunita
v	v	k7c6	v
přístavních	přístavní	k2eAgNnPc6d1	přístavní
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
Portugalci	Portugalec	k1gMnPc1	Portugalec
ovládali	ovládat	k5eAaImAgMnP	ovládat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nesměla	smět	k5eNaImAgFnS	smět
vytvářet	vytvářet	k5eAaImF	vytvářet
výrobní	výrobní	k2eAgFnPc4d1	výrobní
kapacity	kapacita	k1gFnPc4	kapacita
na	na	k7c4	na
zboží	zboží	k1gNnSc4	zboží
již	již	k6eAd1	již
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
entrepôt	entrepôt	k5eAaPmF	entrepôt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pouze	pouze	k6eAd1	pouze
prostředník	prostředník	k1gInSc4	prostředník
pro	pro	k7c4	pro
zboží	zboží	k1gNnSc4	zboží
dovážené	dovážený	k2eAgNnSc4d1	dovážené
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
vyvezeno	vyvézt	k5eAaPmNgNnS	vyvézt
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
cizími	cizí	k2eAgInPc7d1	cizí
státy	stát	k1gInPc7	stát
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
holandským	holandský	k2eAgInSc7d1	holandský
vpádem	vpád	k1gInSc7	vpád
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
brazilského	brazilský	k2eAgInSc2d1	brazilský
vývozu	vývoz	k1gInSc2	vývoz
přepravována	přepravovat	k5eAaImNgFnS	přepravovat
na	na	k7c6	na
nizozemských	nizozemský	k2eAgFnPc6d1	nizozemská
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Americké	americký	k2eAgFnSc6d1	americká
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
v	v	k7c6	v
brazilských	brazilský	k2eAgInPc6d1	brazilský
přístavech	přístav	k1gInPc6	přístav
začaly	začít	k5eAaPmAgFnP	začít
běžně	běžně	k6eAd1	běžně
objevovat	objevovat	k5eAaImF	objevovat
americké	americký	k2eAgFnPc4d1	americká
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
před	před	k7c7	před
Napoleonem	napoleon	k1gInSc7	napoleon
portugalský	portugalský	k2eAgInSc1d1	portugalský
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
aktů	akt	k1gInPc2	akt
monarchy	monarcha	k1gMnSc2	monarcha
bylo	být	k5eAaImAgNnS	být
otevření	otevření	k1gNnSc1	otevření
brazilských	brazilský	k2eAgInPc2d1	brazilský
přístavů	přístav	k1gInPc2	přístav
cizím	cizí	k2eAgFnPc3d1	cizí
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástup	nástup	k1gInSc1	nástup
Getúlia	Getúlius	k1gMnSc2	Getúlius
Vargase	Vargasa	k1gFnSc6	Vargasa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
==	==	k?	==
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
před	před	k7c7	před
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
poměry	poměr	k1gInPc4	poměr
relativně	relativně	k6eAd1	relativně
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
občasně	občasně	k6eAd1	občasně
propukala	propukat	k5eAaImAgFnS	propukat
povstání	povstání	k1gNnSc4	povstání
chudých	chudý	k2eAgMnPc2d1	chudý
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vláda	vláda	k1gFnSc1	vláda
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
potlačovala	potlačovat	k5eAaImAgFnS	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
Velké	velká	k1gFnSc2	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c6	na
Brazílii	Brazílie	k1gFnSc6	Brazílie
silně	silně	k6eAd1	silně
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Exporty	export	k1gInPc4	export
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
tvořila	tvořit	k5eAaImAgFnS	tvořit
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
březen	březen	k1gInSc4	březen
1930	[number]	k4	1930
byly	být	k5eAaImAgFnP	být
vypsány	vypsán	k2eAgFnPc1d1	vypsána
federální	federální	k2eAgFnPc1d1	federální
a	a	k8xC	a
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
kandidát	kandidát	k1gMnSc1	kandidát
konzervativního	konzervativní	k2eAgNnSc2d1	konzervativní
uskupení	uskupení	k1gNnSc2	uskupení
ze	z	k7c2	z
Sao	Sao	k1gMnSc1	Sao
Paula	Paul	k1gMnSc2	Paul
Júlio	Júlio	k1gMnSc1	Júlio
Prestes	Prestes	k1gMnSc1	Prestes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
podporu	podpora	k1gFnSc4	podpora
statkářů	statkář	k1gMnPc2	statkář
a	a	k8xC	a
oligarchie	oligarchie	k1gFnSc1	oligarchie
ze	z	k7c2	z
Sao	Sao	k1gMnSc2	Sao
Paula	Paul	k1gMnSc2	Paul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opozicí	opozice	k1gFnSc7	opozice
byla	být	k5eAaImAgFnS	být
Liberální	liberální	k2eAgFnSc1d1	liberální
aliance	aliance	k1gFnSc1	aliance
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Getúliem	Getúlium	k1gNnSc7	Getúlium
Vargasem	Vargas	k1gMnSc7	Vargas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
podnikatele	podnikatel	k1gMnPc4	podnikatel
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vargas	Vargas	k1gInSc1	Vargas
sliboval	slibovat	k5eAaImAgInS	slibovat
parcelaci	parcelace	k1gFnSc4	parcelace
velkostatků	velkostatek	k1gInPc2	velkostatek
a	a	k8xC	a
znárodnění	znárodnění	k1gNnSc2	znárodnění
některých	některý	k3yIgInPc2	některý
velkých	velký	k2eAgInPc2d1	velký
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Vargas	Vargas	k1gMnSc1	Vargas
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
volby	volba	k1gFnPc4	volba
za	za	k7c4	za
zfalšované	zfalšovaný	k2eAgFnPc4d1	zfalšovaná
<g/>
,	,	kIx,	,
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgMnPc1d2	nižší
příslušníci	příslušník	k1gMnPc1	příslušník
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
důstojníci	důstojník	k1gMnPc1	důstojník
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Vargase	Vargas	k1gMnSc5	Vargas
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
padla	padnout	k5eAaImAgFnS	padnout
a	a	k8xC	a
moci	moct	k5eAaImF	moct
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Vargas	Vargas	k1gMnSc1	Vargas
donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
předala	předat	k5eAaPmAgFnS	předat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vargas	Vargas	k1gInSc1	Vargas
následně	následně	k6eAd1	následně
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
federální	federální	k2eAgInSc1d1	federální
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgInS	soustředit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vládě	vláda	k1gFnSc6	vláda
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
i	i	k8xC	i
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zakročil	zakročit	k5eAaPmAgInS	zakročit
proti	proti	k7c3	proti
levici	levice	k1gFnSc3	levice
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
odborech	odbor	k1gInPc6	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Zrušil	zrušit	k5eAaPmAgMnS	zrušit
cla	clo	k1gNnPc4	clo
mezi	mezi	k7c7	mezi
brazilskými	brazilský	k2eAgInPc7d1	brazilský
státy	stát	k1gInPc7	stát
a	a	k8xC	a
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
cla	clo	k1gNnPc4	clo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
v	v	k7c6	v
Brazilské	brazilský	k2eAgFnSc6d1	brazilská
historii	historie	k1gFnSc6	historie
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příchodem	příchod	k1gInSc7	příchod
Vargase	Vargas	k1gInSc6	Vargas
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
střídající	střídající	k2eAgFnSc1d1	střídající
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
brazilské	brazilský	k2eAgFnPc1d1	brazilská
"	"	kIx"	"
<g/>
kávové	kávový	k2eAgFnPc1d1	kávová
oligarchie	oligarchie	k1gFnPc1	oligarchie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zájmem	zájem	k1gInSc7	zájem
těchto	tento	k3xDgMnPc2	tento
latifundistů	latifundista	k1gMnPc2	latifundista
bylo	být	k5eAaImAgNnS	být
zachování	zachování	k1gNnSc4	zachování
stávajícího	stávající	k2eAgInSc2d1	stávající
pořádku	pořádek	k1gInSc2	pořádek
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gInPc4	jejich
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
statky	statek	k1gInPc4	statek
obhospodařovala	obhospodařovat	k5eAaImAgFnS	obhospodařovat
masa	masa	k1gFnSc1	masa
nemajetných	majetný	k2eNgMnPc2d1	nemajetný
rolníků	rolník	k1gMnPc2	rolník
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
příjmem	příjem	k1gInSc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Latifundisté	latifundista	k1gMnPc1	latifundista
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
většině	většina	k1gFnSc6	většina
nejevili	jevit	k5eNaImAgMnP	jevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Vargas	Vargas	k1gMnSc1	Vargas
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
zájmy	zájem	k1gInPc4	zájem
rodící	rodící	k2eAgInPc4d1	rodící
se	se	k3xPyFc4	se
střední	střední	k2eAgFnSc2d1	střední
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
tak	tak	k6eAd1	tak
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamenal	znamenat	k5eAaImAgInS	znamenat
brazilskou	brazilský	k2eAgFnSc4d1	brazilská
buržoazní	buržoazní	k2eAgFnSc4d1	buržoazní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
o	o	k7c6	o
století	století	k1gNnSc6	století
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
díky	díky	k7c3	díky
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
Brazílie	Brazílie	k1gFnSc1	Brazílie
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
rychlejšího	rychlý	k2eAgInSc2d2	rychlejší
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
==	==	k?	==
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
Getúlio	Getúlio	k1gMnSc1	Getúlio
Vargas	Vargas	k1gMnSc1	Vargas
ujal	ujmout	k5eAaPmAgMnS	ujmout
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
spoustu	spousta	k1gFnSc4	spousta
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
urychlit	urychlit	k5eAaPmF	urychlit
industrializace	industrializace	k1gFnPc4	industrializace
<g/>
,	,	kIx,	,
posílit	posílit	k5eAaPmF	posílit
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
a	a	k8xC	a
znárodnit	znárodnit	k5eAaPmF	znárodnit
některé	některý	k3yIgInPc4	některý
sektory	sektor	k1gInPc4	sektor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
doly	dol	k1gInPc1	dol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reformy	reforma	k1gFnPc1	reforma
však	však	k9	však
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
oligarchů	oligarch	k1gMnPc2	oligarch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Vargas	Vargas	k1gInSc1	Vargas
zavedl	zavést	k5eAaPmAgInS	zavést
tzv.	tzv.	kA	tzv.
Organický	organický	k2eAgInSc1d1	organický
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
Lei	lei	k1gInSc1	lei
Orgánica	Orgánic	k1gInSc2	Orgánic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zrušil	zrušit	k5eAaPmAgMnS	zrušit
ústavu	ústava	k1gFnSc4	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
federální	federální	k2eAgInSc1d1	federální
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
zákonné	zákonný	k2eAgFnSc2d1	zákonná
rady	rada	k1gFnSc2	rada
států	stát	k1gInPc2	stát
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
proces	proces	k1gInSc1	proces
vyvlastňování	vyvlastňování	k1gNnSc6	vyvlastňování
půdy	půda	k1gFnSc2	půda
oligarchů	oligarch	k1gMnPc2	oligarch
a	a	k8xC	a
přerozdělení	přerozdělení	k1gNnSc4	přerozdělení
mezi	mezi	k7c4	mezi
chudinu	chudina	k1gFnSc4	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
zavedena	zaveden	k2eAgFnSc1d1	zavedena
8	[number]	k4	8
<g/>
-hodinová	odinový	k2eAgFnSc1d1	-hodinový
pracovní	pracovní	k2eAgFnSc1d1	pracovní
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
vlivu	vliv	k1gInSc2	vliv
katolicismu	katolicismus	k1gInSc2	katolicismus
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
nebo	nebo	k8xC	nebo
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
Vargas	Vargas	k1gMnSc1	Vargas
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
sblížení	sblížení	k1gNnSc6	sblížení
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
i	i	k8xC	i
Velkou	velká	k1gFnSc7	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
však	však	k9	však
provádění	provádění	k1gNnSc4	provádění
těchto	tento	k3xDgFnPc2	tento
reforem	reforma	k1gFnPc2	reforma
znemožňovala	znemožňovat	k5eAaImAgFnS	znemožňovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
brazilský	brazilský	k2eAgInSc4d1	brazilský
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
zejména	zejména	k9	zejména
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc4	kakao
<g/>
,	,	kIx,	,
bavlnu	bavlna	k1gFnSc4	bavlna
a	a	k8xC	a
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
bylo	být	k5eAaImAgNnS	být
nezaměstnaných	zaměstnaný	k2eNgInPc2d1	nezaměstnaný
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
postihla	postihnout	k5eAaPmAgFnS	postihnout
všechny	všechen	k3xTgFnPc4	všechen
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
radikalizovat	radikalizovat	k5eAaBmF	radikalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pořádáno	pořádat	k5eAaImNgNnS	pořádat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
armáda	armáda	k1gFnSc1	armáda
nebyla	být	k5eNaImAgFnS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc1	situace
opozice	opozice	k1gFnSc2	opozice
usoudila	usoudit	k5eAaPmAgFnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
převzít	převzít	k5eAaPmF	převzít
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
revoluci	revoluce	k1gFnSc6	revoluce
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
Sao	Sao	k1gFnSc6	Sao
Paolu	Paol	k1gInSc2	Paol
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svrhnout	svrhnout	k5eAaPmF	svrhnout
federální	federální	k2eAgFnSc4d1	federální
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
nastolit	nastolit	k5eAaPmF	nastolit
ústavní	ústavní	k2eAgInSc4d1	ústavní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
značnou	značný	k2eAgFnSc4d1	značná
podporu	podpora	k1gFnSc4	podpora
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
Sao	Sao	k1gFnSc2	Sao
Paolo	Paolo	k1gNnSc4	Paolo
nikdo	nikdo	k3yNnSc1	nikdo
nepřidal	přidat	k5eNaPmAgMnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
Sao	Sao	k1gFnSc1	Sao
Paola	Paola	k1gFnSc1	Paola
byla	být	k5eAaImAgFnS	být
8,5	[number]	k4	8,5
tisíce	tisíc	k4xCgInSc2	tisíc
mužů	muž	k1gMnPc2	muž
proti	proti	k7c3	proti
provládní	provládní	k2eAgFnSc3d1	provládní
přesile	přesila	k1gFnSc3	přesila
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc1	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
federální	federální	k2eAgFnSc4d1	federální
přesilu	přesila	k1gFnSc4	přesila
trvaly	trvat	k5eAaImAgInP	trvat
boje	boj	k1gInPc1	boj
až	až	k9	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
Sao	Sao	k1gMnSc1	Sao
Paolo	Paolo	k1gNnSc4	Paolo
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Dopady	dopad	k1gInPc1	dopad
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
dvojí	dvojí	k4xRgInPc1	dvojí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
Vargas	Vargas	k1gInSc1	Vargas
okamžitě	okamžitě	k6eAd1	okamžitě
provedl	provést	k5eAaPmAgInS	provést
čistky	čistka	k1gFnPc4	čistka
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
si	se	k3xPyFc3	se
uvědom	uvědomit	k5eAaPmRp2nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opozice	opozice	k1gFnSc1	opozice
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
více	hodně	k6eAd2	hodně
respektovat	respektovat	k5eAaImF	respektovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
mírně	mírně	k6eAd1	mírně
demokratizovala	demokratizovat	k5eAaBmAgFnS	demokratizovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc1	povstání
ze	z	k7c2	z
Sao	Sao	k1gMnSc2	Sao
Paola	Paol	k1gMnSc2	Paol
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nenabourala	nabourat	k5eNaPmAgFnS	nabourat
republikánské	republikánský	k2eAgInPc4d1	republikánský
rysy	rys	k1gInPc4	rys
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc4d1	sociální
řád	řád	k1gInSc4	řád
budovala	budovat	k5eAaImAgFnS	budovat
podle	podle	k7c2	podle
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
bude	být	k5eAaImBp3nS	být
významný	významný	k2eAgInSc1d1	významný
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vargas	Vargas	k1gMnSc1	Vargas
začne	začít	k5eAaPmIp3nS	začít
zavádět	zavádět	k5eAaImF	zavádět
korporativistické	korporativistický	k2eAgInPc4d1	korporativistický
rysy	rys	k1gInPc4	rys
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
nepřímo	přímo	k6eNd1	přímo
parlamentem	parlament	k1gInSc7	parlament
zvolen	zvolit	k5eAaPmNgInS	zvolit
opět	opět	k6eAd1	opět
Vargas	Vargas	k1gInSc1	Vargas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
budou	být	k5eAaImBp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
přímé	přímý	k2eAgFnPc1d1	přímá
a	a	k8xC	a
demokratické	demokratický	k2eAgFnPc1d1	demokratická
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
brazilském	brazilský	k2eAgNnSc6d1	brazilské
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
však	však	k9	však
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
existoval	existovat	k5eAaImAgInS	existovat
systém	systém	k1gInSc1	systém
cambã	cambã	k?	cambã
(	(	kIx(	(
<g/>
polo-nevolnický	poloevolnický	k2eAgInSc1d1	polo-nevolnický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
rolník	rolník	k1gMnSc1	rolník
za	za	k7c4	za
pronájem	pronájem	k1gInSc4	pronájem
robotoval	robotovat	k5eAaImAgMnS	robotovat
pro	pro	k7c4	pro
statkáře	statkář	k1gMnSc4	statkář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
krizi	krize	k1gFnSc3	krize
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
začala	začít	k5eAaPmAgFnS	začít
vyostřovat	vyostřovat	k5eAaImF	vyostřovat
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnPc1	demonstrace
nebyly	být	k5eNaImAgFnP	být
jen	jen	k9	jen
levicové	levicový	k2eAgFnPc1d1	levicová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
fašistické	fašistický	k2eAgInPc1d1	fašistický
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
levicové	levicový	k2eAgFnPc1d1	levicová
síly	síla	k1gFnPc1	síla
představily	představit	k5eAaPmAgFnP	představit
veřejnosti	veřejnost	k1gFnPc4	veřejnost
tzv.	tzv.	kA	tzv.
Národně	národně	k6eAd1	národně
osvobozenecký	osvobozenecký	k2eAgInSc1d1	osvobozenecký
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
ANL	ANL	kA	ANL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
požadoval	požadovat	k5eAaImAgInS	požadovat
levicové	levicový	k2eAgFnPc4d1	levicová
reformy	reforma	k1gFnPc4	reforma
jako	jako	k8xC	jako
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
znárodnit	znárodnit	k5eAaPmF	znárodnit
cizí	cizí	k2eAgFnPc4d1	cizí
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
neplatit	platit	k5eNaImF	platit
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
zaručit	zaručit	k5eAaPmF	zaručit
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
v	v	k7c6	v
ANL	ANL	kA	ANL
posilovalo	posilovat	k5eAaImAgNnS	posilovat
komunistické	komunistický	k2eAgNnSc1d1	komunistické
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1935	[number]	k4	1935
komunisté	komunista	k1gMnPc1	komunista
připravují	připravovat	k5eAaImIp3nP	připravovat
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
započalo	započnout	k5eAaPmAgNnS	započnout
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
listopadu	listopad	k1gInSc2	listopad
1935	[number]	k4	1935
v	v	k7c6	v
Recife	Recif	k1gInSc5	Recif
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
listopadu	listopad	k1gInSc2	listopad
revoluce	revoluce	k1gFnSc2	revoluce
propukla	propuknout	k5eAaPmAgFnS	propuknout
i	i	k9	i
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Jaineru	Jainer	k1gMnSc3	Jainer
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vláda	vláda	k1gFnSc1	vláda
rychle	rychle	k6eAd1	rychle
zakročila	zakročit	k5eAaPmAgFnS	zakročit
<g/>
.	.	kIx.	.
</s>
<s>
Přišla	přijít	k5eAaPmAgFnS	přijít
nevídaná	vídaný	k2eNgFnSc1d1	nevídaná
vlna	vlna	k1gFnSc1	vlna
perzekucí	perzekuce	k1gFnPc2	perzekuce
komunistů	komunista	k1gMnPc2	komunista
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
byli	být	k5eAaImAgMnP	být
zatknuti	zatknout	k5eAaPmNgMnP	zatknout
všichni	všechen	k3xTgMnPc1	všechen
poslanci	poslanec	k1gMnPc1	poslanec
ANL	ANL	kA	ANL
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
válečný	válečný	k2eAgInSc1d1	válečný
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Vargas	Vargas	k1gMnSc1	Vargas
mezitím	mezitím	k6eAd1	mezitím
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
posiloval	posilovat	k5eAaImAgMnS	posilovat
nacionalistické	nacionalistický	k2eAgFnPc4d1	nacionalistická
nálady	nálada	k1gFnPc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Blížily	blížit	k5eAaImAgFnP	blížit
se	se	k3xPyFc4	se
slibované	slibovaný	k2eAgFnPc1d1	slibovaná
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Vargas	Vargas	k1gMnSc1	Vargas
nechtěl	chtít	k5eNaImAgMnS	chtít
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
o	o	k7c4	o
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
pokusil	pokusit	k5eAaPmAgMnS	pokusit
fašista	fašista	k1gMnSc1	fašista
O.	O.	kA	O.
M.	M.	kA	M.
Filho	Fil	k1gMnSc2	Fil
<g/>
.	.	kIx.	.
</s>
<s>
Aféra	aféra	k1gFnSc1	aféra
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
zmedializovala	zmedializovat	k5eAaBmAgFnS	zmedializovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
vyjímečný	vyjímečný	k?	vyjímečný
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Vargas	Vargas	k1gMnSc1	Vargas
tak	tak	k6eAd1	tak
vzal	vzít	k5eAaPmAgMnS	vzít
moc	moc	k6eAd1	moc
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třetí	třetí	k4xOgFnSc1	třetí
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
1937-1945	[number]	k4	1937-1945
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
History	Histor	k1gInPc4	Histor
of	of	k?	of
Brazil	Brazil	k1gFnSc2	Brazil
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Geschichte	Geschicht	k1gInSc5	Geschicht
Brasiliens	Brasiliensa	k1gFnPc2	Brasiliensa
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
460	[number]	k4	460
s.	s.	k?	s.
<g/>
,	,	kIx,	,
80-7106-261-8	[number]	k4	80-7106-261-8
</s>
</p>
<p>
<s>
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Stručné	stručný	k2eAgFnSc2d1	stručná
historie	historie	k1gFnSc2	historie
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Libri	Libr	k1gFnSc2	Libr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
DE	DE	k?	DE
CAMINHA	CAMINHA	kA	CAMINHA
<g/>
,	,	kIx,	,
Pero	pero	k1gNnSc1	pero
Vaz	vaz	k1gInSc1	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
králi	král	k1gMnSc3	král
Manuelovi	Manuel	k1gMnSc3	Manuel
o	o	k7c6	o
nalezení	nalezení	k1gNnSc6	nalezení
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Scriptorium	Scriptorium	k1gNnSc1	Scriptorium
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86197	[number]	k4	86197
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
