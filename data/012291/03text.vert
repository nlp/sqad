<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc4	turnaj
hostila	hostit	k5eAaImAgFnS	hostit
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
československá	československý	k2eAgNnPc1d1	Československé
města	město	k1gNnPc1	město
Frýdek-Místek	Frýdek-Místka	k1gFnPc2	Frýdek-Místka
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
a	a	k8xC	a
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
hokejisté	hokejista	k1gMnPc1	hokejista
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finálová	finálový	k2eAgFnSc1d1	finálová
skupina	skupina	k1gFnSc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
výsledky	výsledek	k1gInPc1	výsledek
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
započetly	započíst	k5eAaPmAgFnP	započíst
postupujícím	postupující	k2eAgNnSc7d1	postupující
i	i	k8xC	i
do	do	k7c2	do
finálové	finálový	k2eAgFnSc2d1	finálová
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
bylo	být	k5eAaImAgNnS	být
konečným	konečný	k2eAgInSc7d1	konečný
výsledkem	výsledek	k1gInSc7	výsledek
mužstev	mužstvo	k1gNnPc2	mužstvo
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
-	-	kIx~	-
Polsko	Polsko	k1gNnSc1	Polsko
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
po	po	k7c6	po
pr	pr	k0	pr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
sestoupilo	sestoupit	k5eAaPmAgNnS	sestoupit
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turnajová	turnajový	k2eAgNnPc4d1	turnajové
ocenění	ocenění	k1gNnPc4	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Produktivita	produktivita	k1gFnSc1	produktivita
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Mistři	mistr	k1gMnPc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
ČSSR	ČSSR	kA	ČSSR
==	==	k?	==
</s>
</p>
<p>
<s>
Brankáři	brankář	k1gMnPc1	brankář
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Turek	Turek	k1gMnSc1	Turek
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Horyna	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Orct	Orct	k1gMnSc1	Orct
</s>
</p>
<p>
<s>
Obránci	obránce	k1gMnPc1	obránce
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Vykoukal	Vykoukal	k1gMnSc1	Vykoukal
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Šmehlík	Šmehlík	k1gMnSc1	Šmehlík
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Varholík	Varholík	k1gMnSc1	Varholík
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Bakula	bakula	k1gFnSc1	bakula
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Stanček	Stanček	k1gMnSc1	Stanček
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc1	Pavol
Sýkorčin	sýkorčin	k2eAgInSc1d1	sýkorčin
<g/>
,	,	kIx,	,
Matej	Matej	k1gInSc1	Matej
Bukna	Bukno	k1gNnSc2	Bukno
</s>
</p>
<p>
<s>
Útočníci	útočník	k1gMnPc1	útočník
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Reichel	Reichel	k1gMnSc1	Reichel
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Rob	roba	k1gFnPc2	roba
<g/>
,	,	kIx,	,
Pavol	Pavola	k1gFnPc2	Pavola
Zůbek	Zůbky	k1gFnPc2	Zůbky
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Zůbek	Zůbek	k1gMnSc1	Zůbek
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Kontšek	Kontšek	k1gMnSc1	Kontšek
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Karabín	Karabín	k1gMnSc1	Karabín
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Bareš	Bareš	k1gMnSc1	Bareš
<g/>
,	,	kIx,	,
Marián	Marián	k1gMnSc1	Marián
Uharček	Uharček	k1gMnSc1	Uharček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vimmer	Vimmer	k1gMnSc1	Vimmer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nižší	nízký	k2eAgFnSc2d2	nižší
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
B	B	kA	B
skupina	skupina	k1gFnSc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
B	B	kA	B
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
Briançonu	Briançon	k1gInSc6	Briançon
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
postup	postup	k1gInSc4	postup
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
1989	[number]	k4	1989
si	se	k3xPyFc3	se
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
Západní	západní	k2eAgMnPc1d1	západní
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
Britové	Brit	k1gMnPc1	Brit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SRN	SRN	kA	SRN
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
===	===	k?	===
C	C	kA	C
skupina	skupina	k1gFnSc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
C	C	kA	C
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastiánu	Sebastián	k1gMnSc6	Sebastián
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
jej	on	k3xPp3gNnSc4	on
Bulhaři	Bulhar	k1gMnPc1	Bulhar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
</s>
</p>
