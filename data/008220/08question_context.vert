<s>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1564	[number]	k4	1564
–	–	k?	–
zemřel	zemřít	k5eAaPmAgInS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
anglicky	anglicky	k6eAd1	anglicky
píšícího	píšící	k2eAgMnSc2d1	píšící
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
nejpřednějšího	přední	k2eAgMnSc2d3	nejpřednější
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
anglickým	anglický	k2eAgMnSc7d1	anglický
národním	národní	k2eAgMnSc7d1	národní
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
"	"	kIx"	"
<g/>
bardem	bard	k1gMnSc7	bard
z	z	k7c2	z
Avonu	Avon	k1gInSc2	Avon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
připsaných	připsaný	k2eAgNnPc2d1	připsané
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
asi	asi	k9	asi
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
154	[number]	k4	154
sonetů	sonet	k1gInPc2	sonet
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
epické	epický	k2eAgFnPc4d1	epická
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
nejistého	jistý	k2eNgNnSc2d1	nejisté
autorství	autorství	k1gNnSc2	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
hlavních	hlavní	k2eAgInPc2d1	hlavní
živých	živý	k2eAgInPc2d1	živý
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
častěji	často	k6eAd2	často
než	než	k8xS	než
hry	hra	k1gFnSc2	hra
jakéhokoli	jakýkoli	k3yIgMnSc2	jakýkoli
jiného	jiný	k2eAgMnSc2d1	jiný
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
nad	nad	k7c7	nad
Avonou	Avona	k1gFnSc7	Avona
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Warwickshire	Warwickshir	k1gMnSc5	Warwickshir
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Anne	Anne	k1gFnSc7	Anne
Hathawayovou	Hathawayový	k2eAgFnSc7d1	Hathawayová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
dceru	dcera	k1gFnSc4	dcera
Susannu	Susann	k1gInSc2	Susann
a	a	k8xC	a
dvojčata	dvojče	k1gNnPc1	dvojče
syna	syn	k1gMnSc2	syn
Hamneta	Hamnet	k1gMnSc2	Hamnet
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Judith	Juditha	k1gFnPc2	Juditha
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1585	[number]	k4	1585
a	a	k8xC	a
1592	[number]	k4	1592
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
částečný	částečný	k2eAgMnSc1d1	částečný
vlastník	vlastník	k1gMnSc1	vlastník
herecké	herecký	k2eAgFnSc2d1	herecká
společnosti	společnost	k1gFnSc2	společnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
(	(	kIx(	(
<g/>
Lord	lord	k1gMnSc1	lord
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
49	[number]	k4	49
let	léto	k1gNnPc2	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
do	do	k7c2	do
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
záznamů	záznam	k1gInPc2	záznam
ze	z	k7c2	z
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
značné	značný	k2eAgFnPc4d1	značná
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
takových	takový	k3xDgFnPc6	takový
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jeho	on	k3xPp3gInSc4	on
fyzický	fyzický	k2eAgInSc4d1	fyzický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgNnSc4d1	sexuální
zaměření	zaměření	k1gNnSc4	zaměření
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zda	zda	k8xS	zda
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jemu	on	k3xPp3gMnSc3	on
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
napsány	napsán	k2eAgInPc1d1	napsán
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
známých	známý	k2eAgFnPc2d1	známá
prací	práce	k1gFnPc2	práce
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1589	[number]	k4	1589
a	a	k8xC	a
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
rané	raný	k2eAgFnPc1d1	raná
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
komedie	komedie	k1gFnPc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
období	období	k1gNnSc6	období
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
mnoho	mnoho	k6eAd1	mnoho
her	hra	k1gFnPc2	hra
na	na	k7c4	na
historické	historický	k2eAgInPc4d1	historický
náměty	námět	k1gInPc4	námět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
anglických	anglický	k2eAgFnPc2d1	anglická
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
např.	např.	kA	např.
velké	velký	k2eAgNnSc1d1	velké
drama	drama	k1gNnSc1	drama
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
asi	asi	k9	asi
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
psal	psát	k5eAaImAgInS	psát
své	svůj	k3xOyFgFnPc4	svůj
slavné	slavný	k2eAgFnPc4d1	slavná
tragédie	tragédie	k1gFnPc4	tragédie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
a	a	k8xC	a
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgMnPc4d1	považován
za	za	k7c4	za
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
díla	dílo	k1gNnPc4	dílo
anglickojazyčné	anglickojazyčný	k2eAgFnSc2d1	anglickojazyčná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>

