<s>
Otesánek	Otesánek	k1gMnSc1	Otesánek
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
pohádky	pohádka	k1gFnSc2	pohádka
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
jako	jako	k8xC	jako
surrealistický	surrealistický	k2eAgInSc1d1	surrealistický
horor	horor	k1gInSc1	horor
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obdržel	obdržet	k5eAaPmAgMnS	obdržet
tři	tři	k4xCgMnPc4	tři
České	český	k2eAgInPc4d1	český
lvy	lev	k1gInPc7	lev
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
bezdětných	bezdětný	k2eAgMnPc6d1	bezdětný
manželech	manžel	k1gMnPc6	manžel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
pořídí	pořídit	k5eAaPmIp3nP	pořídit
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
(	(	kIx(	(
<g/>
otesanou	otesaný	k2eAgFnSc4d1	otesaná
<g/>
)	)	kIx)	)
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
imitace	imitace	k1gFnSc1	imitace
obživne	obživnout	k5eAaPmIp3nS	obživnout
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
jíst	jíst	k5eAaImF	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
sní	sníst	k5eAaPmIp3nS	sníst
všechny	všechen	k3xTgFnPc4	všechen
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
začnou	začít	k5eAaPmIp3nP	začít
vyvářet	vyvářet	k5eAaImF	vyvářet
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
servírují	servírovat	k5eAaBmIp3nP	servírovat
i	i	k9	i
pošťáka	pošťák	k1gMnSc4	pošťák
a	a	k8xC	a
sousedy	soused	k1gMnPc4	soused
<g/>
.	.	kIx.	.
</s>
<s>
Otesánka	Otesánek	k1gMnSc4	Otesánek
uklidí	uklidit	k5eAaPmIp3nS	uklidit
do	do	k7c2	do
sklepa	sklep	k1gInSc2	sklep
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požírá	požírat	k5eAaImIp3nS	požírat
další	další	k2eAgMnPc4d1	další
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
za	za	k7c7	za
Otesánkem	otesánek	k1gInSc7	otesánek
do	do	k7c2	do
sklepa	sklep	k1gInSc2	sklep
vydává	vydávat	k5eAaImIp3nS	vydávat
babka	babka	k1gFnSc1	babka
s	s	k7c7	s
motykou	motyka	k1gFnSc7	motyka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
filmu	film	k1gInSc3	film
i	i	k9	i
k	k	k7c3	k
pohádce	pohádka	k1gFnSc3	pohádka
samé	samý	k3xTgInPc4	samý
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
podal	podat	k5eAaPmAgMnS	podat
výklad	výklad	k1gInSc4	výklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Otesánek	Otesánek	k1gMnSc1	Otesánek
představuje	představovat	k5eAaImIp3nS	představovat
"	"	kIx"	"
<g/>
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
iracionální	iracionální	k2eAgFnSc4d1	iracionální
část	část	k1gFnSc4	část
našeho	náš	k3xOp1gInSc2	náš
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jsme	být	k5eAaImIp1nP	být
svými	svůj	k3xOyFgMnPc7	svůj
smysly	smysl	k1gInPc4	smysl
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
v	v	k7c4	v
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
organizací	organizace	k1gFnSc7	organizace
našeho	náš	k3xOp1gInSc2	náš
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
)	)	kIx)	)
vypudili	vypudit	k5eAaPmAgMnP	vypudit
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažíme	snažit	k5eAaImIp1nP	snažit
nějak	nějak	k6eAd1	nějak
racionálně	racionálně	k6eAd1	racionálně
zpacifikovat	zpacifikovat	k5eAaPmF	zpacifikovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Otesánek	Otesánek	k1gMnSc1	Otesánek
stále	stále	k6eAd1	stále
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
a	a	k8xC	a
požírá	požírat	k5eAaImIp3nS	požírat
nás	my	k3xPp1nPc4	my
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trest	trest	k1gInSc1	trest
za	za	k7c4	za
zpackanou	zpackaný	k2eAgFnSc4d1	zpackaná
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
II	II	kA	II
<g/>
.	.	kIx.	.
kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Růžička	Růžička	k1gMnSc1	Růžička
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Carl	Carl	k1gMnSc1	Carl
Maria	Maria	k1gFnSc1	Maria
von	von	k1gInSc4	von
Weber	weber	k1gInSc1	weber
...	...	k?	...
předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Čarostřelec	čarostřelec	k1gMnSc1	čarostřelec
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Ivo	Ivo	k1gMnSc1	Ivo
Špalj	Špalj	k1gFnSc1	Špalj
Kostýmy	kostým	k1gInPc4	kostým
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Švankmajerová	Švankmajerová	k1gFnSc1	Švankmajerová
Výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
rekvizit	rekvizita	k1gFnPc2	rekvizita
a	a	k8xC	a
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Švankmajerová	Švankmajerová	k1gFnSc1	Švankmajerová
(	(	kIx(	(
<g/>
kresleného	kreslený	k2eAgInSc2d1	kreslený
filmu	film	k1gInSc2	film
a	a	k8xC	a
kostýmů	kostým	k1gInPc2	kostým
<g/>
)	)	kIx)	)
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Kallista	Kallista	k1gMnSc1	Kallista
Zástupce	zástupce	k1gMnSc1	zástupce
ved	ved	k?	ved
<g/>
.	.	kIx.	.
produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
Věra	Věra	k1gFnSc1	Věra
Ferdová	Ferdová	k1gFnSc1	Ferdová
Produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Kallista	Kallista	k1gMnSc1	Kallista
Koproducent	koproducent	k1gMnSc1	koproducent
<g/>
:	:	kIx,	:
Keith	Keith	k1gInSc1	Keith
Griffiths	Griffiths	k1gInSc1	Griffiths
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Uldrichová	Uldrichová	k1gFnSc1	Uldrichová
Asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc2	režie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Martin	Martin	k2eAgInSc4d1	Martin
Kublák	Kublák	k1gInSc4	Kublák
Pomocná	pomocný	k2eAgFnSc1d1	pomocná
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Kublák	Kublák	k1gMnSc1	Kublák
<g/>
,	,	kIx,	,
Tijn	Tijn	k1gMnSc1	Tijn
Po	Po	kA	Po
Asistent	asistent	k1gMnSc1	asistent
kamery	kamera	k1gFnSc2	kamera
<g/>
:	:	kIx,	:
Peter	Peter	k1gMnSc1	Peter
Nečas	Nečas	k1gMnSc1	Nečas
Zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
efekty	efekt	k1gInPc4	efekt
(	(	kIx(	(
<g/>
optické	optický	k2eAgInPc4d1	optický
triky	trik	k1gInPc4	trik
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Kryml	Kryml	k1gMnSc1	Kryml
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Haták	Haták	k1gMnSc1	Haták
Animace	animace	k1gFnSc1	animace
<g/>
:	:	kIx,	:
Bedřich	Bedřich	k1gMnSc1	Bedřich
Glazer	Glazer	k1gMnSc1	Glazer
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kublák	Kublák	k1gMnSc1	Kublák
Odborný	odborný	k2eAgMnSc1d1	odborný
poradce	poradce	k1gMnSc1	poradce
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g />
.	.	kIx.	.
</s>
<s>
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
Kukal	kukat	k5eAaImAgMnS	kukat
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
animace	animace	k1gFnSc2	animace
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Stejskal	Stejskal	k1gMnSc1	Stejskal
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
Athanor	Athanor	k1gInSc1	Athanor
<g/>
,	,	kIx,	,
Barrandov	Barrandov	k1gInSc1	Barrandov
Biografia	Biografia	k1gFnSc1	Biografia
<g/>
,	,	kIx,	,
Channel	Channel	k1gInSc1	Channel
Four	Foura	k1gFnPc2	Foura
Films	Films	k1gInSc1	Films
<g/>
,	,	kIx,	,
Ilumination	Ilumination	k1gInSc1	Ilumination
Films	Films	k1gInSc1	Films
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
fond	fond	k1gInSc1	fond
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Kretschmerová	Kretschmerová	k1gFnSc1	Kretschmerová
...	...	k?	...
matka	matka	k1gFnSc1	matka
Alžbětky	Alžbětka	k1gFnSc2	Alžbětka
paní	paní	k1gFnSc1	paní
Štádlerová	Štádlerová	k1gFnSc1	Štádlerová
<g />
.	.	kIx.	.
</s>
<s>
Veronika	Veronika	k1gFnSc1	Veronika
Žilková	Žilková	k1gFnSc1	Žilková
...	...	k?	...
Božena	Božena	k1gFnSc1	Božena
Horáková	Horáková	k1gFnSc1	Horáková
Jan	Jan	k1gMnSc1	Jan
Hartl	Hartl	k1gMnSc1	Hartl
...	...	k?	...
Karel	Karel	k1gMnSc1	Karel
Horák	Horák	k1gMnSc1	Horák
Pavel	Pavel	k1gMnSc1	Pavel
Nový	Nový	k1gMnSc1	Nový
...	...	k?	...
František	František	k1gMnSc1	František
Štádler	Štádler	k1gMnSc1	Štádler
Kristina	Kristina	k1gFnSc1	Kristina
Adamcová	Adamcová	k1gFnSc1	Adamcová
...	...	k?	...
Alžbětka	Alžbětka	k1gFnSc1	Alžbětka
Dagmar	Dagmar	k1gFnSc1	Dagmar
Stříbrná	stříbrná	k1gFnSc1	stříbrná
...	...	k?	...
paní	paní	k1gFnSc1	paní
správcová	správcová	k1gFnSc1	správcová
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kozák	Kozák	k1gMnSc1	Kozák
...	...	k?	...
pan	pan	k1gMnSc1	pan
Žlábek	žlábek	k1gInSc4	žlábek
Gustav	Gustav	k1gMnSc1	Gustav
Vondráček	Vondráček	k1gMnSc1	Vondráček
...	...	k?	...
pošťák	pošťák	k1gMnSc1	pošťák
Mládek	Mládek	k1gMnSc1	Mládek
Arnošt	Arnošt	k1gMnSc1	Arnošt
Goldflam	Goldflam	k1gInSc1	Goldflam
...	...	k?	...
gynekolog	gynekolog	k1gMnSc1	gynekolog
Jitka	Jitka	k1gFnSc1	Jitka
Smutná	Smutná	k1gFnSc1	Smutná
...	...	k?	...
sociální	sociální	k2eAgFnSc2d1	sociální
pracovnice	pracovnice	k1gFnSc2	pracovnice
Bulánková	Bulánková	k1gFnSc1	Bulánková
Jiří	Jiří	k1gMnSc1	Jiří
Lábus	Lábus	k1gMnSc1	Lábus
...	...	k?	...
policista	policista	k1gMnSc1	policista
Radek	Radek	k1gMnSc1	Radek
Holub	Holub	k1gMnSc1	Holub
<g />
.	.	kIx.	.
</s>
<s>
...	...	k?	...
mladý	mladý	k2eAgMnSc1d1	mladý
pošťák	pošťák	k1gMnSc1	pošťák
Jan	Jan	k1gMnSc1	Jan
Jiráň	Jiráň	k1gFnSc4	Jiráň
...	...	k?	...
Karlův	Karlův	k2eAgMnSc1d1	Karlův
kolega	kolega	k1gMnSc1	kolega
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Palusga	Palusga	k1gFnSc1	Palusga
...	...	k?	...
uniformovaný	uniformovaný	k2eAgMnSc1d1	uniformovaný
policista	policista	k1gMnSc1	policista
František	František	k1gMnSc1	František
Polata	Polata	k1gFnSc1	Polata
...	...	k?	...
tajný	tajný	k2eAgMnSc1d1	tajný
policista	policista	k1gMnSc1	policista
Václav	Václav	k1gMnSc1	Václav
Ježek	Ježek	k1gMnSc1	Ježek
...	...	k?	...
tajný	tajný	k2eAgMnSc1d1	tajný
policista	policista	k1gMnSc1	policista
Marie	Maria	k1gFnSc2	Maria
Marešová	Marešová	k1gFnSc1	Marešová
...	...	k?	...
role	role	k1gFnSc1	role
neurčena	určen	k2eNgFnSc1d1	neurčena
Anna	Anna	k1gFnSc1	Anna
Wetlinská	Wetlinský	k2eAgFnSc1d1	Wetlinská
...	...	k?	...
role	role	k1gFnSc1	role
neurčena	určen	k2eNgFnSc1d1	neurčena
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bernatský	Bernatský	k2eAgMnSc1d1	Bernatský
...	...	k?	...
role	role	k1gFnPc1	role
neurčena	určen	k2eNgFnSc1d1	neurčena
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
...	...	k?	...
role	role	k1gFnSc1	role
neurčena	určen	k2eNgFnSc1d1	neurčena
Jiří	Jiří	k1gMnSc1	Jiří
Macháček	Macháček	k1gMnSc1	Macháček
...	...	k?	...
role	role	k1gFnSc1	role
neurčena	určen	k2eNgFnSc1d1	neurčena
Otesánek	otesánek	k1gInSc1	otesánek
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Otesánek	otesánek	k1gInSc1	otesánek
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Otesánek	Otesánek	k1gMnSc1	Otesánek
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
</s>
