<s>
Otesánek	Otesánek	k1gMnSc1	Otesánek
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
pohádky	pohádka	k1gFnSc2	pohádka
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
jako	jako	k8xS	jako
surrealistický	surrealistický	k2eAgInSc1d1	surrealistický
horor	horor	k1gInSc1	horor
<g/>
.	.	kIx.	.
</s>
