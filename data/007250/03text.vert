<s>
Zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Fractura	Fractura	k1gFnSc1	Fractura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
kontinuity	kontinuita	k1gFnSc2	kontinuita
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Zlomeniny	zlomenina	k1gFnPc4	zlomenina
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
Infrakce	infrakce	k1gFnPc4	infrakce
(	(	kIx(	(
<g/>
částečné	částečný	k2eAgNnSc4d1	částečné
nalomení	nalomení	k1gNnSc4	nalomení
kosti	kost	k1gFnSc2	kost
<g/>
)	)	kIx)	)
Fisura	fisura	k1gFnSc1	fisura
(	(	kIx(	(
<g/>
trhlina	trhlina	k1gFnSc1	trhlina
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
kostech	kost	k1gFnPc6	kost
nebo	nebo	k8xC	nebo
na	na	k7c6	na
lebečních	lebeční	k2eAgFnPc6d1	lebeční
kostech	kost	k1gFnPc6	kost
<g/>
)	)	kIx)	)
Epifyzeolýza	Epifyzeolýza	k1gFnSc1	Epifyzeolýza
(	(	kIx(	(
<g/>
traumatické	traumatický	k2eAgNnSc4d1	traumatické
porušení	porušení	k1gNnSc4	porušení
kontinuity	kontinuita	k1gFnSc2	kontinuita
růstové	růstový	k2eAgFnSc2d1	růstová
ploténky	ploténka	k1gFnSc2	ploténka
<g/>
)	)	kIx)	)
Luxační	luxační	k2eAgFnSc2d1	luxační
zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
doprovázená	doprovázený	k2eAgFnSc5d1	doprovázená
vykloubením	vykloubení	k1gNnSc7	vykloubení
kloubu	kloub	k1gInSc2	kloub
<g/>
)	)	kIx)	)
Kompresivní	kompresivní	k2eAgFnSc4d1	kompresivní
zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
typická	typický	k2eAgNnPc4d1	typické
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
bederních	bederní	k2eAgInPc2d1	bederní
obratlů	obratel	k1gInPc2	obratel
při	při	k7c6	při
tlakové	tlakový	k2eAgFnSc6d1	tlaková
zátěži	zátěž	k1gFnSc6	zátěž
<g/>
)	)	kIx)	)
Impresivní	impresivní	k2eAgFnSc1d1	impresivní
zlomenina	zlomenina	k1gFnSc1	zlomenina
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
kostní	kostní	k2eAgFnSc1d1	kostní
plocha	plocha	k1gFnSc1	plocha
vmáčknuta	vmáčknut	k2eAgFnSc1d1	vmáčknuta
do	do	k7c2	do
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
)	)	kIx)	)
Subperiostální	Subperiostální	k2eAgFnSc1d1	Subperiostální
zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
porušen	porušen	k2eAgInSc1d1	porušen
periost	periost	k1gInSc1	periost
<g/>
)	)	kIx)	)
Patologická	patologický	k2eAgFnSc1d1	patologická
zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
předchozího	předchozí	k2eAgNnSc2d1	předchozí
poškození	poškození	k1gNnSc2	poškození
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
například	například	k6eAd1	například
cystou	cysta	k1gFnSc7	cysta
nebo	nebo	k8xC	nebo
nádorem	nádor	k1gInSc7	nádor
<g/>
)	)	kIx)	)
Zlomenina	zlomenina	k1gFnSc1	zlomenina
s	s	k7c7	s
dislokací	dislokace	k1gFnSc7	dislokace
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
dislokace	dislokace	k1gFnSc2	dislokace
(	(	kIx(	(
<g/>
s	s	k7c7	s
posunutím	posunutí	k1gNnSc7	posunutí
kostních	kostní	k2eAgInPc2d1	kostní
úlomků	úlomek	k1gInPc2	úlomek
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gInSc2	jejich
posunu	posun	k1gInSc2	posun
<g/>
)	)	kIx)	)
Únavová	únavový	k2eAgFnSc1d1	únavová
zlomenina	zlomenina	k1gFnSc1	zlomenina
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zjevného	zjevný	k2eAgNnSc2d1	zjevné
vyvolávacího	vyvolávací	k2eAgNnSc2d1	vyvolávací
traumatu	trauma	k1gNnSc2	trauma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zlomeniny	zlomenina	k1gFnPc4	zlomenina
zánártních	zánártní	k2eAgFnPc2d1	zánártní
kostí	kost	k1gFnPc2	kost
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
pochodech	pochod	k1gInPc6	pochod
<g/>
)	)	kIx)	)
Přímá	přímý	k2eAgFnSc1d1	přímá
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
působení	působení	k1gNnSc2	působení
síly	síla	k1gFnSc2	síla
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Nepřímá	přímý	k2eNgNnPc1d1	nepřímé
(	(	kIx(	(
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
od	od	k7c2	od
působení	působení	k1gNnSc2	působení
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
kožního	kožní	k2eAgInSc2d1	kožní
krytu	kryt	k1gInSc2	kryt
<g/>
)	)	kIx)	)
Zavřená	zavřený	k2eAgFnSc1d1	zavřená
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
porušen	porušen	k2eAgInSc1d1	porušen
kožní	kožní	k2eAgInSc1d1	kožní
kryt	kryt	k1gInSc1	kryt
<g/>
)	)	kIx)	)
Příčná	příčný	k2eAgFnSc1d1	příčná
Spirálovitá	spirálovitý	k2eAgFnSc1d1	spirálovitá
Šikmá	šikmý	k2eAgFnSc1d1	šikmá
Tříštivá	tříštivý	k2eAgFnSc1d1	tříštivá
Úhlová	úhlový	k2eAgFnSc1d1	úhlová
<g/>
,	,	kIx,	,
rotační	rotační	k2eAgFnSc1d1	rotační
<g/>
,	,	kIx,	,
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
se	s	k7c7	s
zkrácením	zkrácení	k1gNnSc7	zkrácení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
s	s	k7c7	s
odtažením	odtažení	k1gNnSc7	odtažení
Jisté	jistý	k2eAgNnSc1d1	jisté
-	-	kIx~	-
patologická	patologický	k2eAgFnSc1d1	patologická
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
<g/>
,	,	kIx,	,
krepitace	krepitace	k1gFnSc1	krepitace
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
deformace	deformace	k1gFnSc1	deformace
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
RTG	RTG	kA	RTG
nález	nález	k1gInSc4	nález
Nejisté	jistý	k2eNgFnPc1d1	nejistá
-	-	kIx~	-
otok	otok	k1gInSc1	otok
<g/>
,	,	kIx,	,
bolestivost	bolestivost	k1gFnSc1	bolestivost
<g/>
,	,	kIx,	,
deformace	deformace	k1gFnSc1	deformace
<g/>
,	,	kIx,	,
porucha	porucha	k1gFnSc1	porucha
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
hematom	hematom	k1gInSc1	hematom
Repozice	repozice	k1gFnSc2	repozice
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
(	(	kIx(	(
<g/>
repozicí	repozice	k1gFnSc7	repozice
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
napravení	napravení	k1gNnSc1	napravení
dislokace	dislokace	k1gFnSc2	dislokace
kostních	kostní	k2eAgInPc2d1	kostní
úlomků	úlomek	k1gInPc2	úlomek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Imobilizace	imobilizace	k1gFnSc2	imobilizace
neboli	neboli	k8xC	neboli
znehybnění	znehybnění	k1gNnSc2	znehybnění
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
způsob	způsob	k1gInSc1	způsob
léčení	léčení	k1gNnSc2	léčení
zlomenin	zlomenina	k1gFnPc2	zlomenina
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
klasickou	klasický	k2eAgFnSc7d1	klasická
sádrovou	sádrový	k2eAgFnSc7d1	sádrová
dlahou	dlaha	k1gFnSc7	dlaha
nebo	nebo	k8xC	nebo
modernější	moderní	k2eAgInSc1d2	modernější
plastový	plastový	k2eAgInSc1d1	plastový
obvaz	obvaz	k1gInSc1	obvaz
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
postačí	postačit	k5eAaPmIp3nS	postačit
znehybnění	znehybnění	k1gNnSc4	znehybnění
končetiny	končetina	k1gFnSc2	končetina
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
obvazem	obvaz	k1gInSc7	obvaz
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zlomenina	zlomenina	k1gFnSc1	zlomenina
klíční	klíční	k2eAgFnSc2d1	klíční
kosti	kost	k1gFnSc2	kost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
klidový	klidový	k2eAgInSc4d1	klidový
režim	režim	k1gInSc4	režim
a	a	k8xC	a
léky	lék	k1gInPc4	lék
tlumící	tlumící	k2eAgFnSc4d1	tlumící
bolest	bolest	k1gFnSc4	bolest
<g/>
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nekomplikovaná	komplikovaný	k2eNgFnSc1d1	nekomplikovaná
zlomenina	zlomenina	k1gFnSc1	zlomenina
jednoho	jeden	k4xCgInSc2	jeden
žebra	žebro	k1gNnPc1	žebro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klidový	klidový	k2eAgInSc1d1	klidový
režim	režim	k1gInSc1	režim
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
korzetu	korzet	k1gInSc6	korzet
je	být	k5eAaImIp3nS	být
podstatou	podstata	k1gFnSc7	podstata
léčby	léčba	k1gFnSc2	léčba
nekomplikovaných	komplikovaný	k2eNgFnPc2d1	nekomplikovaná
zlomenin	zlomenina	k1gFnPc2	zlomenina
bederních	bederní	k2eAgInPc2d1	bederní
a	a	k8xC	a
hrudních	hrudní	k2eAgInPc2d1	hrudní
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Osteosynthesa	Osteosynthesa	k1gFnSc1	Osteosynthesa
znamená	znamenat	k5eAaImIp3nS	znamenat
operační	operační	k2eAgNnSc4d1	operační
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
nejčastěji	často	k6eAd3	často
kovovým	kovový	k2eAgInSc7d1	kovový
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc4	nějaký
druh	druh	k1gInSc4	druh
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
buď	buď	k8xC	buď
vyjme	vyjmout	k5eAaPmIp3nS	vyjmout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ponechá	ponechat	k5eAaPmIp3nS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgMnPc2d1	jednoduchý
cerkláží	cerklážet	k5eAaPmIp3nS	cerklážet
ocelovými	ocelový	k2eAgInPc7d1	ocelový
dráty	drát	k1gInPc7	drát
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
šrouby	šroub	k1gInPc4	šroub
doplněné	doplněný	k2eAgInPc4d1	doplněný
dlahami	dlaha	k1gFnPc7	dlaha
až	až	k9	až
po	po	k7c4	po
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
systémy	systém	k1gInPc4	systém
hřebování	hřebování	k1gNnPc2	hřebování
s	s	k7c7	s
předvrtáváním	předvrtávání	k1gNnSc7	předvrtávání
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
(	(	kIx(	(
<g/>
hřeb	hřeb	k1gInSc1	hřeb
znamená	znamenat	k5eAaImIp3nS	znamenat
navlečení	navlečení	k1gNnPc4	navlečení
nejčastěji	často	k6eAd3	často
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
duté	dutý	k2eAgFnPc1d1	dutá
kosti	kost	k1gFnPc1	kost
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocelový	ocelový	k2eAgInSc4d1	ocelový
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zafixován	zafixován	k2eAgInSc4d1	zafixován
šrouby	šroub	k1gInPc4	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	s	k7c7	s
šrouby	šroub	k1gInPc7	šroub
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
zpravidla	zpravidla	k6eAd1	zpravidla
distálním	distální	k2eAgInSc6d1	distální
(	(	kIx(	(
<g/>
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
<g/>
)	)	kIx)	)
konci	konec	k1gInSc6	konec
vyjmou	vyjmout	k5eAaPmIp3nP	vyjmout
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
dynamizace	dynamizace	k1gFnSc1	dynamizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zevní	zevní	k2eAgFnSc1d1	zevní
fixace	fixace	k1gFnSc1	fixace
je	být	k5eAaImIp3nS	být
navrtání	navrtání	k1gNnSc4	navrtání
speciálních	speciální	k2eAgInPc2d1	speciální
hřebů	hřeb	k1gInPc2	hřeb
někdy	někdy	k6eAd1	někdy
se	s	k7c7	s
závity	závit	k1gInPc7	závit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
do	do	k7c2	do
kostních	kostní	k2eAgInPc2d1	kostní
úlomků	úlomek	k1gInPc2	úlomek
skrze	skrze	k?	skrze
kůži	kůže	k1gFnSc3	kůže
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
upevnění	upevnění	k1gNnSc2	upevnění
vně	vně	k6eAd1	vně
končetiny	končetina	k1gFnPc1	končetina
do	do	k7c2	do
zevního	zevní	k2eAgInSc2d1	zevní
fixatéru	fixatér	k1gInSc2	fixatér
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
představíte	představit	k5eAaPmIp2nP	představit
jako	jako	k8xS	jako
stavebnici	stavebnice	k1gFnSc4	stavebnice
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
zafixujete	zafixovat	k5eAaPmIp2nP	zafixovat
hřeby	hřeb	k1gInPc4	hřeb
a	a	k8xC	a
můžete	moct	k5eAaImIp2nP	moct
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
různě	různě	k6eAd1	různě
manipulovat	manipulovat	k5eAaImF	manipulovat
a	a	k8xC	a
ve	v	k7c6	v
vhodné	vhodný	k2eAgFnSc6d1	vhodná
poloze	poloha	k1gFnSc6	poloha
to	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
zafixujete	zafixovat	k5eAaPmIp2nP	zafixovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
tuzemských	tuzemský	k2eAgInPc2d1	tuzemský
i	i	k8xC	i
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
operačních	operační	k2eAgFnPc2d1	operační
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Náhrady	náhrada	k1gFnPc4	náhrada
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
například	například	k6eAd1	například
náhrada	náhrada	k1gFnSc1	náhrada
horního	horní	k2eAgInSc2d1	horní
konce	konec	k1gInSc2	konec
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zlomení	zlomení	k1gNnSc6	zlomení
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
CKP	CKP	kA	CKP
neboli	neboli	k8xC	neboli
cervikokapitální	cervikokapitální	k2eAgFnSc1d1	cervikokapitální
prothesa	prothesa	k1gFnSc1	prothesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
náhrada	náhrada	k1gFnSc1	náhrada
téhož	týž	k3xTgInSc2	týž
včetně	včetně	k7c2	včetně
kloubní	kloubní	k2eAgFnSc2d1	kloubní
jamky	jamka	k1gFnSc2	jamka
(	(	kIx(	(
<g/>
TEP	tep	k1gInSc1	tep
neboli	neboli	k8xC	neboli
totální	totální	k2eAgFnSc1d1	totální
endoprothesa	endoprothesa	k1gFnSc1	endoprothesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
zlomeniny	zlomenina	k1gFnPc4	zlomenina
je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přítomno	přítomen	k2eAgNnSc1d1	přítomno
i	i	k8xC	i
značné	značný	k2eAgNnSc1d1	značné
opotřebení	opotřebení	k1gNnSc4	opotřebení
chrupavek	chrupavka	k1gFnPc2	chrupavka
(	(	kIx(	(
<g/>
artróza	artróza	k1gFnSc1	artróza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
řešení	řešení	k1gNnSc2	řešení
i	i	k8xC	i
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
náhrady	náhrada	k1gFnPc4	náhrada
nejenom	nejenom	k6eAd1	nejenom
kyčle	kyčel	k1gFnPc4	kyčel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
kolena	koleno	k1gNnSc2	koleno
<g/>
,	,	kIx,	,
hlezna	hlezno	k1gNnSc2	hlezno
<g/>
,	,	kIx,	,
ramene	rameno	k1gNnSc2	rameno
a	a	k8xC	a
lokte	loket	k1gInSc2	loket
<g/>
.	.	kIx.	.
</s>
<s>
Klouby	kloub	k1gInPc1	kloub
na	na	k7c6	na
prstech	prst	k1gInPc6	prst
lze	lze	k6eAd1	lze
nahradit	nahradit	k5eAaPmF	nahradit
implantáty	implantát	k1gInPc4	implantát
z	z	k7c2	z
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
silikonu	silikon	k1gInSc2	silikon
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
nahradit	nahradit	k5eAaPmF	nahradit
těla	tělo	k1gNnPc4	tělo
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
část	část	k1gFnSc1	část
lebečních	lebeční	k2eAgFnPc2d1	lebeční
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
náhrady	náhrada	k1gFnPc4	náhrada
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
řadit	řadit	k5eAaImF	řadit
i	i	k9	i
transplantace	transplantace	k1gFnSc1	transplantace
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Klidový	klidový	k2eAgInSc1d1	klidový
režim	režim	k1gInSc1	režim
následovaný	následovaný	k2eAgInSc1d1	následovaný
časnou	časný	k2eAgFnSc7d1	časná
rehabilitací	rehabilitace	k1gFnSc7	rehabilitace
je	být	k5eAaImIp3nS	být
metodou	metoda	k1gFnSc7	metoda
volby	volba	k1gFnSc2	volba
u	u	k7c2	u
značně	značně	k6eAd1	značně
nemocných	nemocný	k1gMnPc2	nemocný
a	a	k8xC	a
starých	starý	k2eAgMnPc2d1	starý
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
nepřežili	přežít	k5eNaPmAgMnP	přežít
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc2d1	uvedený
operačních	operační	k2eAgInPc2d1	operační
zákroků	zákrok	k1gInPc2	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
jim	on	k3xPp3gFnPc3	on
sice	sice	k8xC	sice
přinese	přinést	k5eAaPmIp3nS	přinést
invaliditu	invalidita	k1gFnSc4	invalidita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dává	dávat	k5eAaImIp3nS	dávat
jim	on	k3xPp3gFnPc3	on
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
)	)	kIx)	)
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Amputace	amputace	k1gFnSc1	amputace
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
to	ten	k3xDgNnSc1	ten
zní	znět	k5eAaImIp3nS	znět
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
amputace	amputace	k1gFnSc2	amputace
značně	značně	k6eAd1	značně
zdevastované	zdevastovaný	k2eAgFnSc2d1	zdevastovaná
končetiny	končetina	k1gFnSc2	končetina
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
metodou	metoda	k1gFnSc7	metoda
nežli	nežli	k8xS	nežli
zdlouhavé	zdlouhavý	k2eAgNnSc1d1	zdlouhavé
<g/>
,	,	kIx,	,
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
značně	značně	k6eAd1	značně
nákladné	nákladný	k2eAgInPc1d1	nákladný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vyléčení	vyléčení	k1gNnSc4	vyléčení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c4	v
nefunkční	funkční	k2eNgFnSc4d1	nefunkční
nehybnou	hybný	k2eNgFnSc4d1	nehybná
a	a	k8xC	a
překážející	překážející	k2eAgFnSc4d1	překážející
končetinu	končetina	k1gFnSc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
přísně	přísně	k6eAd1	přísně
individuálně	individuálně	k6eAd1	individuálně
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
vhodnější	vhodný	k2eAgFnSc4d2	vhodnější
končetinu	končetina	k1gFnSc4	končetina
časně	časně	k6eAd1	časně
amputovat	amputovat	k5eAaBmF	amputovat
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
protézou	protéza	k1gFnSc7	protéza
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
výjimečné	výjimečný	k2eAgFnPc4d1	výjimečná
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
živelní	živelní	k2eAgFnPc4d1	živelní
pohromy	pohroma	k1gFnPc4	pohroma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
dosti	dosti	k6eAd1	dosti
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
ideální	ideální	k2eAgFnSc4d1	ideální
léčbu	léčba	k1gFnSc4	léčba
všech	všecek	k3xTgMnPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
sem	sem	k6eAd1	sem
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
amputaci	amputace	k1gFnSc4	amputace
například	například	k6eAd1	například
hlavičky	hlavička	k1gFnSc2	hlavička
vřetenní	vřetenní	k2eAgFnPc4d1	vřetenní
kosti	kost	k1gFnPc4	kost
u	u	k7c2	u
jejího	její	k3xOp3gNnSc2	její
roztříštění	roztříštění	k1gNnSc2	roztříštění
a	a	k8xC	a
značné	značný	k2eAgFnSc6d1	značná
dislokaci	dislokace	k1gFnSc6	dislokace
úlomků	úlomek	k1gInPc2	úlomek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
ideální	ideální	k2eAgMnSc1d1	ideální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přinese	přinést	k5eAaPmIp3nS	přinést
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
lepší	dobrý	k2eAgInSc4d2	lepší
funkční	funkční	k2eAgInSc4d1	funkční
výsledek	výsledek	k1gInSc4	výsledek
než	než	k8xS	než
jiné	jiný	k2eAgInPc4d1	jiný
operační	operační	k2eAgInPc4d1	operační
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
