<s>
Kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
předpis	předpis	k1gInSc4	předpis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
převádějí	převádět	k5eAaImIp3nP	převádět
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
reprezentace	reprezentace	k1gFnSc2	reprezentace
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gInSc2	jejich
přenosu	přenos	k1gInSc2	přenos
či	či	k8xC	či
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
nebo	nebo	k8xC	nebo
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
kód	kód	k1gInSc1	kód
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
sama	sám	k3xTgFnSc1	sám
zakódovaná	zakódovaný	k2eAgFnSc1d1	zakódovaná
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Kódování	kódování	k1gNnSc1	kódování
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
převodu	převod	k1gInSc2	převod
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
symboly	symbol	k1gInPc4	symbol
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
dekódování	dekódování	k1gNnSc1	dekódování
je	být	k5eAaImIp3nS	být
opačný	opačný	k2eAgInSc4d1	opačný
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
písmena	písmeno	k1gNnPc4	písmeno
na	na	k7c4	na
sekvence	sekvence	k1gFnPc4	sekvence
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
při	při	k7c6	při
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
přenosu	přenos	k1gInSc6	přenos
či	či	k8xC	či
záznamu	záznam	k1gInSc2	záznam
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvolit	zvolit	k5eAaPmF	zvolit
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
reprezentaci	reprezentace	k1gFnSc4	reprezentace
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
specifické	specifický	k2eAgInPc4d1	specifický
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
obvykle	obvykle	k6eAd1	obvykle
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
stručnost	stručnost	k1gFnSc1	stručnost
(	(	kIx(	(
<g/>
komprese	komprese	k1gFnSc1	komprese
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
detekce	detekce	k1gFnSc2	detekce
a	a	k8xC	a
opravy	oprava	k1gFnSc2	oprava
chyb	chyba	k1gFnPc2	chyba
záznamu	záznam	k1gInSc2	záznam
či	či	k8xC	či
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
samoopravný	samoopravný	k2eAgInSc1d1	samoopravný
kód	kód	k1gInSc1	kód
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
kódování	kódování	k1gNnSc2	kódování
resp.	resp.	kA	resp.
dekódování	dekódování	k1gNnSc1	dekódování
<g/>
,	,	kIx,	,
technická	technický	k2eAgFnSc1d1	technická
vhodnost	vhodnost	k1gFnSc1	vhodnost
pro	pro	k7c4	pro
přenosové	přenosový	k2eAgFnPc4d1	přenosová
či	či	k8xC	či
záznamové	záznamový	k2eAgNnSc1d1	záznamové
médium	médium	k1gNnSc1	médium
(	(	kIx(	(
<g/>
linkový	linkový	k2eAgInSc1d1	linkový
kód	kód	k1gInSc1	kód
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
kódování	kódování	k1gNnSc2	kódování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
utajení	utajení	k1gNnSc1	utajení
smyslu	smysl	k1gInSc2	smysl
zprávy	zpráva	k1gFnSc2	zpráva
před	před	k7c7	před
nezasvěceným	zasvěcený	k2eNgMnSc7d1	nezasvěcený
příjemcem	příjemce	k1gMnSc7	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
šifrování	šifrování	k1gNnSc1	šifrování
<g/>
,	,	kIx,	,
kódy	kód	k1gInPc1	kód
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
a	a	k8xC	a
například	například	k6eAd1	například
substituce	substituce	k1gFnSc1	substituce
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
slov	slovo	k1gNnPc2	slovo
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
poskytnout	poskytnout	k5eAaPmF	poskytnout
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
takových	takový	k3xDgInPc2	takový
kódů	kód	k1gInPc2	kód
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
mezi	mezi	k7c7	mezi
zločinci	zločinec	k1gMnPc7	zločinec
nebo	nebo	k8xC	nebo
v	v	k7c4	v
historii	historie	k1gFnSc4	historie
při	při	k7c6	při
válečných	válečný	k2eAgFnPc6d1	válečná
operacích	operace	k1gFnPc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
Čárový	čárový	k2eAgInSc1d1	čárový
kód	kód	k1gInSc1	kód
Genetický	genetický	k2eAgInSc1d1	genetický
kód	kód	k1gInSc4	kód
Linkový	linkový	k2eAgInSc4d1	linkový
kód	kód	k1gInSc4	kód
Kódování	kódování	k1gNnSc2	kódování
znaků	znak	k1gInPc2	znak
Strojový	strojový	k2eAgInSc4d1	strojový
kód	kód	k1gInSc4	kód
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kód	kód	k1gInSc4	kód
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
kód	kód	k1gInSc4	kód
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
