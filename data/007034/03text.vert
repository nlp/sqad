<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Okinawu	Okinawa	k1gFnSc4	Okinawa
<g/>
,	,	kIx,	,
vybojovaná	vybojovaný	k2eAgFnSc1d1	vybojovaná
na	na	k7c6	na
japonském	japonský	k2eAgInSc6d1	japonský
ostrově	ostrov	k1gInSc6	ostrov
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
největší	veliký	k2eAgFnSc4d3	veliký
obojživelnou	obojživelný	k2eAgFnSc4d1	obojživelná
operaci	operace	k1gFnSc4	operace
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
operace	operace	k1gFnSc1	operace
nesla	nést	k5eAaImAgFnS	nést
kódové	kódový	k2eAgNnSc4d1	kódové
označení	označení	k1gNnSc4	označení
Iceberg	Iceberg	k1gInSc4	Iceberg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Ledovec	ledovec	k1gInSc1	ledovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvalo	k1gNnPc1	trvalo
od	od	k7c2	od
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
do	do	k7c2	do
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Šalamounovy	Šalamounův	k2eAgInPc4d1	Šalamounův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
obsazení	obsazení	k1gNnSc4	obsazení
ostrova	ostrov	k1gInSc2	ostrov
Bougainville	Bougainville	k1gNnSc2	Bougainville
<g/>
,	,	kIx,	,
Gilbertových	Gilbertův	k2eAgInPc2d1	Gilbertův
a	a	k8xC	a
Marshalových	Marshalův	k2eAgInPc2d1	Marshalův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
Marian	Mariana	k1gFnPc2	Mariana
a	a	k8xC	a
po	po	k7c6	po
krvavé	krvavý	k2eAgFnSc6d1	krvavá
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Iwodžimu	Iwodžim	k1gInSc6	Iwodžim
si	se	k3xPyFc3	se
Američané	Američan	k1gMnPc1	Američan
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobytí	dobytí	k1gNnSc4	dobytí
Japonska	Japonsko	k1gNnSc2	Japonsko
bude	být	k5eAaImBp3nS	být
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
Iwodžimy	Iwodžima	k1gFnSc2	Iwodžima
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
km2	km2	k4	km2
je	on	k3xPp3gNnSc4	on
stálo	stát	k5eAaImAgNnS	stát
6	[number]	k4	6
825	[number]	k4	825
padlých	padlý	k1gMnPc2	padlý
a	a	k8xC	a
nezvěstných	zvěstný	k2eNgMnPc2d1	nezvěstný
a	a	k8xC	a
20	[number]	k4	20
000	[number]	k4	000
raněných	raněný	k1gMnPc2	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
evidentní	evidentní	k2eAgNnSc1d1	evidentní
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonský	japonský	k2eAgInSc1d1	japonský
odpor	odpor	k1gInSc1	odpor
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
silnějším	silný	k2eAgNnSc7d2	silnější
se	s	k7c7	s
zkracující	zkracující	k2eAgFnSc7d1	zkracující
se	se	k3xPyFc4	se
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
mateřských	mateřský	k2eAgInPc2d1	mateřský
japonských	japonský	k2eAgInPc2d1	japonský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1	Japonské
velení	velení	k1gNnSc1	velení
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
většiny	většina	k1gFnSc2	většina
zkušených	zkušený	k2eAgMnPc2d1	zkušený
letců	letec	k1gMnPc2	letec
chápalo	chápat	k5eAaImAgNnS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
americkým	americký	k2eAgFnPc3d1	americká
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
rovném	rovný	k2eAgInSc6d1	rovný
boji	boj	k1gInSc6	boj
a	a	k8xC	a
proto	proto	k8xC	proto
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
v	v	k7c6	v
maximální	maximální	k2eAgFnSc6d1	maximální
míře	míra	k1gFnSc6	míra
využívat	využívat	k5eAaImF	využívat
taktiku	taktika	k1gFnSc4	taktika
sebevražedných	sebevražedný	k2eAgInPc2d1	sebevražedný
útoků	útok	k1gInPc2	útok
kamikaze	kamikaze	k1gMnPc2	kamikaze
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
způsobit	způsobit	k5eAaPmF	způsobit
nepříteli	nepřítel	k1gMnSc3	nepřítel
co	co	k9	co
největší	veliký	k2eAgFnPc4d3	veliký
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
otřást	otřást	k5eAaPmF	otřást
jeho	jeho	k3xOp3gFnSc7	jeho
ochotou	ochota	k1gFnSc7	ochota
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
tyto	tento	k3xDgFnPc4	tento
snahy	snaha	k1gFnPc4	snaha
registrovali	registrovat	k5eAaBmAgMnP	registrovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
<g/>
,	,	kIx,	,
sami	sám	k3xTgMnPc1	sám
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přinutit	přinutit	k5eAaPmF	přinutit
Japonce	Japonec	k1gMnPc4	Japonec
k	k	k7c3	k
jednáním	jednání	k1gNnPc3	jednání
o	o	k7c6	o
míru	mír	k1gInSc6	mír
těžkými	těžký	k2eAgInPc7d1	těžký
nálety	nálet	k1gInPc7	nálet
na	na	k7c4	na
japonská	japonský	k2eAgNnPc4d1	Japonské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
1945	[number]	k4	1945
jejich	jejich	k3xOp3gInPc1	jejich
bombardéry	bombardér	k1gInPc1	bombardér
naložené	naložený	k2eAgInPc1d1	naložený
zápalnými	zápalný	k2eAgFnPc7d1	zápalná
bombami	bomba	k1gFnPc7	bomba
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
připravily	připravit	k5eAaPmAgFnP	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
silné	silný	k2eAgInPc1d1	silný
útoky	útok	k1gInPc1	útok
postihly	postihnout	k5eAaPmAgInP	postihnout
i	i	k9	i
další	další	k2eAgNnPc1d1	další
japonská	japonský	k2eAgNnPc1d1	Japonské
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
počítal	počítat	k5eAaImAgInS	počítat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
ostrova	ostrov	k1gInSc2	ostrov
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ostrov	ostrov	k1gInSc1	ostrov
ležící	ležící	k2eAgInSc1d1	ležící
asi	asi	k9	asi
550	[number]	k4	550
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
hlavních	hlavní	k2eAgInPc2d1	hlavní
japonských	japonský	k2eAgInPc2d1	japonský
ostrovů	ostrov	k1gInPc2	ostrov
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
sotva	sotva	k6eAd1	sotva
30	[number]	k4	30
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výhodné	výhodný	k2eAgFnSc3d1	výhodná
poloze	poloha	k1gFnSc3	poloha
představoval	představovat	k5eAaImAgInS	představovat
důležitý	důležitý	k2eAgInSc1d1	důležitý
odrazový	odrazový	k2eAgInSc1d1	odrazový
můstek	můstek	k1gInSc1	můstek
k	k	k7c3	k
samotnému	samotný	k2eAgInSc3d1	samotný
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
bránit	bránit	k5eAaImF	bránit
kolem	kolem	k7c2	kolem
60	[number]	k4	60
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
32	[number]	k4	32
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
62	[number]	k4	62
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
44	[number]	k4	44
<g/>
.	.	kIx.	.
samostatné	samostatný	k2eAgFnSc2d1	samostatná
brigády	brigáda	k1gFnSc2	brigáda
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
velel	velet	k5eAaImAgMnS	velet
generálporučík	generálporučík	k1gMnSc1	generálporučík
Micuru	Micur	k1gInSc2	Micur
Ušidžima	Ušidžima	k1gFnSc1	Ušidžima
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
zástupce	zástupce	k1gMnSc1	zástupce
generál	generál	k1gMnSc1	generál
Isamu	Isam	k1gInSc2	Isam
Čó	Čó	k1gMnSc1	Čó
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
štábu	štáb	k1gInSc2	štáb
Hiromoči	Hiromoč	k1gInSc3	Hiromoč
Jahara	Jahar	k1gMnSc2	Jahar
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
větší	veliký	k2eAgNnSc1d2	veliký
uskupení	uskupení	k1gNnSc1	uskupení
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
velel	velet	k5eAaImAgInS	velet
menším	malý	k2eAgMnSc7d2	menší
jednotkám	jednotka	k1gFnPc3	jednotka
generál	generál	k1gMnSc1	generál
Takehido	Takehida	k1gFnSc5	Takehida
Udo	Udo	k1gMnSc1	Udo
<g/>
.	.	kIx.	.
</s>
<s>
Ušidžima	Ušidžim	k1gMnSc4	Ušidžim
dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
nemá	mít	k5eNaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
udržet	udržet	k5eAaPmF	udržet
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
soustředil	soustředit	k5eAaPmAgMnS	soustředit
svoji	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
kolem	kolem	k7c2	kolem
hradu	hrad	k1gInSc2	hrad
Šuri	Šur	k1gFnSc2	Šur
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgFnSc2d1	středověká
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
příkrých	příkrý	k2eAgInPc6d1	příkrý
hřebenech	hřeben	k1gInPc6	hřeben
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přesile	přesila	k1gFnSc3	přesila
Spojenců	spojenec	k1gMnPc2	spojenec
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
žádnou	žádný	k3yNgFnSc4	žádný
šanci	šance	k1gFnSc4	šance
odvrátit	odvrátit	k5eAaPmF	odvrátit
svoji	svůj	k3xOyFgFnSc4	svůj
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
pevně	pevně	k6eAd1	pevně
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
vytrvat	vytrvat	k5eAaPmF	vytrvat
do	do	k7c2	do
posledního	poslední	k2eAgMnSc2d1	poslední
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
bojovat	bojovat	k5eAaImF	bojovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Američané	Američan	k1gMnPc1	Američan
draze	draha	k1gFnSc6	draha
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
za	za	k7c4	za
dobytí	dobytí	k1gNnSc4	dobytí
každého	každý	k3xTgInSc2	každý
kousku	kousek	k1gInSc2	kousek
jeho	on	k3xPp3gNnSc2	on
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
bráněná	bráněný	k2eAgFnSc1d1	bráněná
japonská	japonský	k2eAgFnSc1d1	japonská
linie	linie	k1gFnSc1	linie
měla	mít	k5eAaImAgFnS	mít
jediné	jediný	k2eAgNnSc4d1	jediné
slabé	slabý	k2eAgNnSc4d1	slabé
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
měli	mít	k5eAaImAgMnP	mít
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
svoje	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
čas	čas	k1gInSc1	čas
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
bunkrů	bunkr	k1gInPc2	bunkr
a	a	k8xC	a
záložních	záložní	k2eAgFnPc2d1	záložní
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
též	též	k9	též
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předešlými	předešlý	k2eAgFnPc7d1	předešlá
bitvami	bitva	k1gFnPc7	bitva
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
)	)	kIx)	)
podstatně	podstatně	k6eAd1	podstatně
víc	hodně	k6eAd2	hodně
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgMnPc4	který
měli	mít	k5eAaImAgMnP	mít
i	i	k8xC	i
dostatek	dostatek	k1gInSc4	dostatek
munice	munice	k1gFnSc2	munice
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgNnSc1d1	americké
pozemní	pozemní	k2eAgNnSc1d1	pozemní
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
vylodit	vylodit	k5eAaPmF	vylodit
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálporučíka	generálporučík	k1gMnSc2	generálporučík
Simona	Simon	k1gMnSc2	Simon
B.	B.	kA	B.
Bucknera	Buckner	k1gMnSc2	Buckner
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yIgFnSc7	který
spadaly	spadat	k5eAaPmAgInP	spadat
dva	dva	k4xCgInPc1	dva
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
obojživelný	obojživelný	k2eAgInSc1d1	obojživelný
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
<s>
XXIV	XXIV	kA	XXIV
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
77	[number]	k4	77
<g/>
.	.	kIx.	.
a	a	k8xC	a
99	[number]	k4	99
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgFnSc2d1	pěší
divize	divize	k1gFnSc2	divize
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgFnPc1d1	námořní
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podporovaly	podporovat	k5eAaImAgFnP	podporovat
vylodění	vylodění	k1gNnSc3	vylodění
čítaly	čítat	k5eAaImAgFnP	čítat
asi	asi	k9	asi
1300	[number]	k4	1300
plavidel	plavidlo	k1gNnPc2	plavidlo
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
čtyř	čtyři	k4xCgFnPc2	čtyři
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
byla	být	k5eAaImAgNnP	být
i	i	k8xC	i
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc1	force
57	[number]	k4	57
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
britskými	britský	k2eAgInPc7d1	britský
<g/>
,	,	kIx,	,
australskými	australský	k2eAgInPc7d1	australský
<g/>
,	,	kIx,	,
kanadskými	kanadský	k2eAgFnPc7d1	kanadská
a	a	k8xC	a
novozélandskými	novozélandský	k2eAgFnPc7d1	novozélandská
loděmi	loď	k1gFnPc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Bitvě	bitva	k1gFnSc3	bitva
samotné	samotný	k2eAgFnSc2d1	samotná
předcházely	předcházet	k5eAaImAgFnP	předcházet
menší	malý	k2eAgInPc4d2	menší
rušivé	rušivý	k2eAgInPc4d1	rušivý
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
obranná	obranný	k2eAgNnPc4d1	obranné
zařízení	zařízení	k1gNnPc4	zařízení
na	na	k7c6	na
Okinawě	Okinawa	k1gFnSc6	Okinawa
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
si	se	k3xPyFc3	se
také	také	k9	také
zajistili	zajistit	k5eAaPmAgMnP	zajistit
nedaleké	daleký	k2eNgNnSc4d1	nedaleké
souostroví	souostroví	k1gNnSc4	souostroví
Kerama	Keramum	k1gNnSc2	Keramum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
mohli	moct	k5eAaImAgMnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
podpůrnou	podpůrný	k2eAgFnSc4d1	podpůrná
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Japonci	Japonec	k1gMnPc1	Japonec
budou	být	k5eAaImBp3nP	být
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
bránit	bránit	k5eAaImF	bránit
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
před	před	k7c7	před
invazí	invaze	k1gFnSc7	invaze
bylo	být	k5eAaImAgNnS	být
mohutně	mohutně	k6eAd1	mohutně
bombardováno	bombardován	k2eAgNnSc1d1	bombardováno
pobřeží	pobřeží	k1gNnSc1	pobřeží
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pláže	pláž	k1gFnSc2	pláž
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
vylodění	vylodění	k1gNnSc1	vylodění
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
vylodily	vylodit	k5eAaPmAgFnP	vylodit
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
první	první	k4xOgFnSc2	první
americké	americký	k2eAgFnSc2d1	americká
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
nezaznamenaly	zaznamenat	k5eNaPmAgFnP	zaznamenat
významnější	významný	k2eAgFnPc4d2	významnější
známky	známka	k1gFnPc4	známka
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Okinawě	Okinawa	k1gFnSc6	Okinawa
60	[number]	k4	60
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
předmostí	předmostí	k1gNnSc1	předmostí
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
13	[number]	k4	13
km	km	kA	km
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
5	[number]	k4	5
km	km	kA	km
hluboké	hluboký	k2eAgNnSc1d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
byla	být	k5eAaImAgFnS	být
dobyta	dobyt	k2eAgFnSc1d1	dobyta
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
už	už	k6eAd1	už
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
zlomit	zlomit	k5eAaPmF	zlomit
vytrvalý	vytrvalý	k2eAgInSc4d1	vytrvalý
odpor	odpor	k1gInSc4	odpor
Ušidžimových	Ušidžimův	k2eAgNnPc2d1	Ušidžimův
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ušidžima	Ušidžima	k1gFnSc1	Ušidžima
zpočátku	zpočátku	k6eAd1	zpočátku
dal	dát	k5eAaPmAgInS	dát
na	na	k7c4	na
Čóovu	Čóova	k1gFnSc4	Čóova
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
proti	proti	k7c3	proti
invazním	invazní	k2eAgNnPc3d1	invazní
vojskům	vojsko	k1gNnPc3	vojsko
silné	silný	k2eAgInPc1d1	silný
protiútoky	protiútok	k1gInPc1	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgInP	ukázat
jako	jako	k9	jako
málo	málo	k6eAd1	málo
efektivní	efektivní	k2eAgFnPc1d1	efektivní
a	a	k8xC	a
způsobily	způsobit	k5eAaPmAgFnP	způsobit
jen	jen	k9	jen
těžké	těžký	k2eAgFnPc1d1	těžká
ztráty	ztráta	k1gFnPc1	ztráta
japonských	japonský	k2eAgFnPc2d1	japonská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
více	hodně	k6eAd2	hodně
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
taktiku	taktika	k1gFnSc4	taktika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
používal	používat	k5eAaImAgMnS	používat
Jahara	Jahara	k1gFnSc1	Jahara
<g/>
.	.	kIx.	.
</s>
<s>
Opírali	opírat	k5eAaImAgMnP	opírat
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
o	o	k7c4	o
dobře	dobře	k6eAd1	dobře
opevněná	opevněný	k2eAgNnPc4d1	opevněné
postavení	postavení	k1gNnPc4	postavení
protkaná	protkaný	k2eAgFnSc1d1	protkaná
tunely	tunel	k1gInPc7	tunel
a	a	k8xC	a
zákopy	zákop	k1gInPc7	zákop
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
americké	americký	k2eAgFnSc2d1	americká
jednotky	jednotka	k1gFnSc2	jednotka
úporně	úporně	k6eAd1	úporně
útočily	útočit	k5eAaImAgInP	útočit
na	na	k7c4	na
obrannou	obranný	k2eAgFnSc4d1	obranná
linii	linie	k1gFnSc4	linie
s	s	k7c7	s
pevností	pevnost	k1gFnSc7	pevnost
v	v	k7c6	v
městě	město	k1gNnSc6	město
Šuri	Šur	k1gFnSc2	Šur
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
vyvýšených	vyvýšený	k2eAgNnPc2d1	vyvýšené
postavení	postavení	k1gNnPc2	postavení
byli	být	k5eAaImAgMnP	být
Japonci	Japonec	k1gMnPc1	Japonec
schopni	schopen	k2eAgMnPc1d1	schopen
obratně	obratně	k6eAd1	obratně
řídit	řídit	k5eAaImF	řídit
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1	dělostřelecká
palbu	palba	k1gFnSc4	palba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
neustále	neustále	k6eAd1	neustále
ztěžovali	ztěžovat	k5eAaImAgMnP	ztěžovat
americké	americký	k2eAgInPc4d1	americký
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pokrývat	pokrývat	k5eAaImF	pokrývat
oblast	oblast	k1gFnSc4	oblast
kulometnou	kulometný	k2eAgFnSc7d1	kulometná
palbou	palba	k1gFnSc7	palba
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
ztráty	ztráta	k1gFnPc1	ztráta
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
značné	značný	k2eAgFnPc4d1	značná
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
byly	být	k5eAaImAgInP	být
menší	malý	k2eAgInPc1d2	menší
japonské	japonský	k2eAgInPc1d1	japonský
protiútoky	protiútok	k1gInPc1	protiútok
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
ztracené	ztracený	k2eAgFnPc4d1	ztracená
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
začalo	začít	k5eAaPmAgNnS	začít
hrozit	hrozit	k5eAaImF	hrozit
odříznutí	odříznutí	k1gNnSc1	odříznutí
města	město	k1gNnSc2	město
Šuri	Šur	k1gFnSc2	Šur
americkými	americký	k2eAgInPc7d1	americký
obchvatnými	obchvatný	k2eAgInPc7d1	obchvatný
útoky	útok	k1gInPc7	útok
<g/>
,	,	kIx,	,
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Ušidžima	Ušidžima	k1gNnSc4	Ušidžima
stáhnout	stáhnout	k5eAaPmF	stáhnout
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
pásma	pásmo	k1gNnSc2	pásmo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Šuri	Šuri	k6eAd1	Šuri
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
vojákům	voják	k1gMnPc3	voják
generála	generál	k1gMnSc2	generál
del	del	k?	del
Valleho	Valle	k1gMnSc2	Valle
až	až	k6eAd1	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
obsazení	obsazení	k1gNnSc1	obsazení
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
významným	významný	k2eAgMnSc7d1	významný
psychologickým	psychologický	k2eAgInSc7d1	psychologický
zvratem	zvrat	k1gInSc7	zvrat
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Američané	Američan	k1gMnPc1	Američan
vylodění	vyloděný	k2eAgMnPc1d1	vyloděný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
poloostrova	poloostrov	k1gInSc2	poloostrov
Oroku	Orok	k1gInSc2	Orok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
stále	stále	k6eAd1	stále
nedobyté	dobytý	k2eNgNnSc4d1	nedobyté
japonské	japonský	k2eAgNnSc4d1	Japonské
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bojovaly	bojovat	k5eAaImAgFnP	bojovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jaedžu-Dake	Jaedžu-Dake	k1gFnSc6	Jaedžu-Dake
<g/>
,	,	kIx,	,
Juza-Dake	Juza-Dake	k1gFnSc6	Juza-Dake
a	a	k8xC	a
Kunaši	Kunaše	k1gFnSc6	Kunaše
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
poslední	poslední	k2eAgFnSc7d1	poslední
linií	linie	k1gFnSc7	linie
japonské	japonský	k2eAgFnSc2d1	japonská
obrany	obrana	k1gFnSc2	obrana
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
obránci	obránce	k1gMnPc1	obránce
účinně	účinně	k6eAd1	účinně
využívali	využívat	k5eAaImAgMnP	využívat
příhodný	příhodný	k2eAgInSc4d1	příhodný
terén	terén	k1gInSc4	terén
a	a	k8xC	a
početné	početný	k2eAgFnPc4d1	početná
jeskyně	jeskyně	k1gFnPc4	jeskyně
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
doslova	doslova	k6eAd1	doslova
sebevražedný	sebevražedný	k2eAgMnSc1d1	sebevražedný
a	a	k8xC	a
neotřáslo	otřást	k5eNaPmAgNnS	otřást
jím	on	k3xPp3gMnSc7	on
ani	ani	k8xC	ani
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
nasazení	nasazení	k1gNnSc1	nasazení
plamenometů	plamenomet	k1gInPc2	plamenomet
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Američané	Američan	k1gMnPc1	Američan
hojně	hojně	k6eAd1	hojně
používali	používat	k5eAaImAgMnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
odpor	odpor	k1gInSc1	odpor
navíc	navíc	k6eAd1	navíc
podporoval	podporovat	k5eAaImAgInS	podporovat
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ztěžoval	ztěžovat	k5eAaImAgInS	ztěžovat
pohyb	pohyb	k1gInSc1	pohyb
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Hřeben	hřeben	k1gInSc1	hřeben
Jaedžu-Dake	Jaedžu-Dak	k1gInSc2	Jaedžu-Dak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
označovali	označovat	k5eAaImAgMnP	označovat
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
Apple	Apple	kA	Apple
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgFnPc4d1	japonská
pozice	pozice	k1gFnPc4	pozice
Američané	Američan	k1gMnPc1	Američan
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
přemohli	přemoct	k5eAaPmAgMnP	přemoct
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
spáchal	spáchat	k5eAaPmAgInS	spáchat
Ušidžima	Ušidžima	k1gNnSc4	Ušidžima
i	i	k8xC	i
Čó	Čó	k1gFnSc4	Čó
seppuku	seppuk	k1gInSc2	seppuk
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
velitelstvím	velitelství	k1gNnSc7	velitelství
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
museli	muset	k5eAaImAgMnP	muset
čelit	čelit	k5eAaImF	čelit
velkým	velký	k2eAgInPc3d1	velký
náletům	nálet	k1gInPc3	nálet
kamikaze	kamikaze	k1gMnPc2	kamikaze
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
do	do	k7c2	do
boje	boj	k1gInSc2	boj
nasadili	nasadit	k5eAaPmAgMnP	nasadit
obrovský	obrovský	k2eAgInSc4d1	obrovský
počet	počet	k1gInSc4	počet
svých	svůj	k3xOyFgNnPc2	svůj
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
potopili	potopit	k5eAaPmAgMnP	potopit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
2	[number]	k4	2
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgNnPc2d2	menší
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Soustavným	soustavný	k2eAgInPc3d1	soustavný
útokům	útok	k1gInPc3	útok
kamikaze	kamikaze	k1gMnPc2	kamikaze
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
čelit	čelit	k5eAaImF	čelit
tzv.	tzv.	kA	tzv.
radarovou	radarový	k2eAgFnSc7d1	radarová
clonou	clona	k1gFnSc7	clona
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
nasadili	nasadit	k5eAaPmAgMnP	nasadit
torpédoborce	torpédoborec	k1gInSc2	torpédoborec
vybavené	vybavený	k2eAgInPc4d1	vybavený
radary	radar	k1gInPc4	radar
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nezabránilo	zabránit	k5eNaPmAgNnS	zabránit
útokům	útok	k1gInPc3	útok
a	a	k8xC	a
namísto	namísto	k7c2	namísto
námořníků	námořník	k1gMnPc2	námořník
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
umírali	umírat	k5eAaImAgMnP	umírat
námořníci	námořník	k1gMnPc1	námořník
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
japonské	japonský	k2eAgFnSc2d1	japonská
operace	operace	k1gFnSc2	operace
Ten-gó	Tenó	k1gFnSc2	Ten-gó
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
americkými	americký	k2eAgInPc7d1	americký
palubními	palubní	k2eAgInPc7d1	palubní
letouny	letoun	k1gInPc7	letoun
potopena	potopen	k2eAgNnPc4d1	potopeno
i	i	k8xC	i
Jamato	Jamat	k2eAgNnSc4d1	Jamato
–	–	k?	–
největší	veliký	k2eAgFnSc4d3	veliký
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Piloti	pilot	k1gMnPc1	pilot
kamikaze	kamikaze	k1gMnSc4	kamikaze
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
10	[number]	k4	10
masových	masový	k2eAgInPc2d1	masový
útoků	útok	k1gInPc2	útok
Kikusui	Kikusu	k1gFnSc2	Kikusu
s	s	k7c7	s
50	[number]	k4	50
–	–	k?	–
300	[number]	k4	300
letadly	letadlo	k1gNnPc7	letadlo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poškodily	poškodit	k5eAaPmAgInP	poškodit
mnoho	mnoho	k4c4	mnoho
bitevních	bitevní	k2eAgInPc2d1	bitevní
a	a	k8xC	a
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
jim	on	k3xPp3gMnPc3	on
padla	padnout	k5eAaPmAgFnS	padnout
i	i	k9	i
ostřílená	ostřílený	k2eAgFnSc1d1	ostřílená
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc2	Enterprise
a	a	k8xC	a
novější	nový	k2eAgFnSc2d2	novější
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodě	loď	k1gFnSc2	loď
USS	USS	kA	USS
Hancock	Hancock	k1gInSc1	Hancock
a	a	k8xC	a
USS	USS	kA	USS
Bunker	Bunker	k1gMnSc1	Bunker
Hill	Hill	k1gMnSc1	Hill
–	–	k?	–
všechny	všechen	k3xTgFnPc1	všechen
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
poškození	poškození	k1gNnSc4	poškození
různého	různý	k2eAgInSc2d1	různý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Spruancova	Spruancův	k2eAgFnSc1d1	Spruancův
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
Bunker	Bunkra	k1gFnPc2	Bunkra
Hill	Hilla	k1gFnPc2	Hilla
ztratila	ztratit	k5eAaPmAgFnS	ztratit
396	[number]	k4	396
mužů	muž	k1gMnPc2	muž
své	svůj	k3xOyFgFnSc2	svůj
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
82	[number]	k4	82
dní	den	k1gInPc2	den
trvající	trvající	k2eAgInSc4d1	trvající
pozemní	pozemní	k2eAgInSc4d1	pozemní
boje	boj	k1gInPc4	boj
o	o	k7c4	o
Okinawu	Okinawa	k1gFnSc4	Okinawa
znamenaly	znamenat	k5eAaImAgFnP	znamenat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
ztráty	ztráta	k1gFnPc1	ztráta
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
70	[number]	k4	70
000	[number]	k4	000
–	–	k?	–
140	[number]	k4	140
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měla	mít	k5eAaImAgFnS	mít
japonská	japonský	k2eAgFnSc1d1	japonská
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
systematicky	systematicky	k6eAd1	systematicky
přesvědčovala	přesvědčovat	k5eAaImAgFnS	přesvědčovat
japonské	japonský	k2eAgNnSc4d1	Japonské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
jsou	být	k5eAaImIp3nP	být
krvelační	krvelačný	k2eAgMnPc1d1	krvelačný
barbaři	barbar	k1gMnPc1	barbar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
budou	být	k5eAaImBp3nP	být
jednat	jednat	k5eAaImF	jednat
maximálně	maximálně	k6eAd1	maximálně
krutě	krutě	k6eAd1	krutě
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
civilistů	civilista	k1gMnPc2	civilista
proto	proto	k8xC	proto
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
zabili	zabít	k5eAaPmAgMnP	zabít
své	svůj	k3xOyFgMnPc4	svůj
rodinné	rodinný	k2eAgMnPc4d1	rodinný
příslušníky	příslušník	k1gMnPc4	příslušník
jen	jen	k9	jen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Američanům	Američan	k1gMnPc3	Američan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
bitvy	bitva	k1gFnSc2	bitva
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
4000	[number]	k4	4000
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
vojáci	voják	k1gMnPc1	voják
vystrašení	vystrašený	k2eAgMnPc1d1	vystrašený
a	a	k8xC	a
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
120	[number]	k4	120
000	[number]	k4	000
nasazených	nasazený	k2eAgMnPc2d1	nasazený
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
asi	asi	k9	asi
110	[number]	k4	110
000	[number]	k4	000
padlo	padnout	k5eAaPmAgNnS	padnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
následovalo	následovat	k5eAaImAgNnS	následovat
své	svůj	k3xOyFgMnPc4	svůj
velitele	velitel	k1gMnPc4	velitel
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
,	,	kIx,	,
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1	japonský
historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
západních	západní	k2eAgMnPc2d1	západní
nedomnívají	domnívat	k5eNaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
takový	takový	k3xDgInSc4	takový
počet	počet	k1gInSc4	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
mezi	mezi	k7c4	mezi
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
byli	být	k5eAaImAgMnP	být
přinuceni	přinutit	k5eAaPmNgMnP	přinutit
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k8xC	i
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
některými	některý	k3yIgMnPc7	některý
fanatiky	fanatik	k1gMnPc7	fanatik
<g/>
.	.	kIx.	.
</s>
<s>
Domnívají	domnívat	k5eAaImIp3nP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
o	o	k7c4	o
vraždy	vražda	k1gFnPc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
72	[number]	k4	72
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
12	[number]	k4	12
513	[number]	k4	513
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
padlo	padnout	k5eAaPmAgNnS	padnout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
nezvěstných	zvěstný	k2eNgNnPc2d1	nezvěstné
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těžkých	těžký	k2eAgInPc2d1	těžký
bojů	boj	k1gInPc2	boj
duševně	duševně	k6eAd1	duševně
zhroutilo	zhroutit	k5eAaPmAgNnS	zhroutit
či	či	k8xC	či
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
a	a	k8xC	a
těžkému	těžký	k2eAgInSc3d1	těžký
stresu	stres	k1gInSc3	stres
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yRgFnSc6	jaký
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dělo	dít	k5eAaBmAgNnS	dít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
taženími	tažení	k1gNnPc7	tažení
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
jasně	jasně	k6eAd1	jasně
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
tvrdost	tvrdost	k1gFnSc4	tvrdost
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
padl	padnout	k5eAaImAgMnS	padnout
při	při	k7c6	při
inspekci	inspekce	k1gFnSc6	inspekce
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
linii	linie	k1gFnSc6	linie
i	i	k9	i
generál	generál	k1gMnSc1	generál
Simon	Simon	k1gMnSc1	Simon
Buckner	Buckner	k1gMnSc1	Buckner
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejvýše	vysoce	k6eAd3	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
americkým	americký	k2eAgMnSc7d1	americký
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zahynul	zahynout	k5eAaPmAgMnS	zahynout
pod	pod	k7c7	pod
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
Claudius	Claudius	k1gMnSc1	Claudius
M.	M.	kA	M.
Easley	Easley	k1gInPc1	Easley
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
mělo	mít	k5eAaImAgNnS	mít
následovat	následovat	k5eAaImF	následovat
vylodění	vylodění	k1gNnSc1	vylodění
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnPc1	příprava
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
uskutečnění	uskutečnění	k1gNnSc6	uskutečnění
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
analytici	analytik	k1gMnPc1	analytik
však	však	k9	však
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
bránili	bránit	k5eAaImAgMnP	bránit
Japonci	Japonec	k1gMnPc1	Japonec
Okinawu	Okinawa	k1gFnSc4	Okinawa
jen	jen	k6eAd1	jen
omezenými	omezený	k2eAgFnPc7d1	omezená
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
přesto	přesto	k8xC	přesto
způsobili	způsobit	k5eAaPmAgMnP	způsobit
Američanům	Američan	k1gMnPc3	Američan
ztráty	ztráta	k1gFnSc2	ztráta
jako	jako	k8xC	jako
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Guadalcanal	Guadalcanal	k1gFnSc4	Guadalcanal
a	a	k8xC	a
Iwodžimu	Iwodžima	k1gFnSc4	Iwodžima
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
odpor	odpor	k1gInSc4	odpor
na	na	k7c6	na
japonské	japonský	k2eAgFnSc6d1	japonská
pevnině	pevnina	k1gFnSc6	pevnina
si	se	k3xPyFc3	se
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
životy	život	k1gInPc4	život
nejen	nejen	k6eAd1	nejen
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
ale	ale	k8xC	ale
i	i	k9	i
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
přinutil	přinutit	k5eAaPmAgMnS	přinutit
Japonsko	Japonsko	k1gNnSc4	Japonsko
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
pomocí	pomocí	k7c2	pomocí
nové	nový	k2eAgFnSc2d1	nová
atomové	atomový	k2eAgFnSc2d1	atomová
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
