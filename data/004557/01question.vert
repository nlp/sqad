<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dvojlist	dvojlist	k1gInSc1	dvojlist
(	(	kIx(	(
<g/>
pevnějšího	pevný	k2eAgInSc2d2	pevnější
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
knižní	knižní	k2eAgInSc1d1	knižní
blok	blok	k1gInSc1	blok
s	s	k7c7	s
deskami	deska	k1gFnPc7	deska
knihy	kniha	k1gFnSc2	kniha
<g/>
?	?	kIx.	?
</s>
