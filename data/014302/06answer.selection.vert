<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
FIS	FIS	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gFnSc1
Internationale	Internationale	k2eAgFnSc1d1
de	de	k7c2
Ski	ski	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
lyžařských	lyžařský	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
.	.	kIx.
</s>