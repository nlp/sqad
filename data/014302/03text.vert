<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
FIS	FIS	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1924	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgInSc1d1
Účel	účel	k1gInSc1
</s>
<s>
Lyžování	lyžování	k1gNnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Oberhofen	Oberhofen	k2eAgInSc4d1
am	am	k?
Thunersee	Thunersee	k1gInSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
45,84	45,84	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
22,3	22,3	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
celosvětová	celosvětový	k2eAgFnSc1d1
Úřední	úřední	k2eAgFnSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
ruština	ruština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
128	#num#	k4
(	(	kIx(
<g/>
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Gian-Franco	Gian-Franco	k1gNnSc1
Kasper	Kaspra	k1gFnPc2
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Přidružení	přidružený	k2eAgMnPc1d1
</s>
<s>
SportAccord	SportAccord	k1gMnSc1
<g/>
,	,	kIx,
MOV	MOV	kA
<g/>
,	,	kIx,
AOIWF	AOIWF	kA
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.fis-ski.com	www.fis-ski.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
FIS	FIS	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gFnSc1
Internationale	Internationale	k2eAgFnSc1d1
de	de	k7c2
Ski	ski	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
lyžařských	lyžařský	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1924	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
francouzském	francouzský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Chamonix	Chamonix	k1gNnSc1
národními	národní	k2eAgFnPc7d1
reprezentacemi	reprezentace	k1gFnPc7
14	#num#	k4
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gInSc7
členem	člen	k1gInSc7
128	#num#	k4
národních	národní	k2eAgNnPc2d1
lyžařských	lyžařský	k2eAgNnPc2d1
sdružení	sdružení	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Oberhofen	Oberhofno	k1gNnPc2
am	am	k?
Thunersee	Thunerse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lyžařské	lyžařský	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Federace	federace	k1gFnSc1
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
soutěže	soutěž	k1gFnPc4
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
a	a	k8xC
zadává	zadávat	k5eAaImIp3nS
organizování	organizování	k1gNnSc3
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
následujících	následující	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
Alpské	alpský	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Alpské	alpský	k2eAgFnSc2d1
kombinace	kombinace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Sjezd	sjezd	k1gInSc1
</s>
<s>
Superobří	superobří	k2eAgInSc1d1
slalom	slalom	k1gInSc1
</s>
<s>
Obří	obří	k2eAgInSc1d1
slalom	slalom	k1gInSc1
</s>
<s>
Slalom	slalom	k1gInSc1
</s>
<s>
Severské	severský	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
</s>
<s>
Běh	běh	k1gInSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
</s>
<s>
Skoky	skok	k1gInPc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
</s>
<s>
Severská	severský	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
</s>
<s>
Telemarkové	Telemarkový	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
</s>
<s>
Freestylové	Freestylový	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
</s>
<s>
Jízda	jízda	k1gFnSc1
v	v	k7c6
boulích	boule	k1gFnPc6
</s>
<s>
Akrobatické	akrobatický	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
</s>
<s>
Skikros	Skikrosa	k1gFnPc2
</s>
<s>
U-rampa	U-rampa	k1gFnSc1
</s>
<s>
Jízda	jízda	k1gFnSc1
na	na	k7c6
Snowboardu	snowboard	k1gInSc6
</s>
<s>
Alpské	alpský	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
</s>
<s>
Freestyle	Freestyl	k1gInSc5
snowboarding	snowboarding	k1gInSc1
</s>
<s>
Snowboardcross	Snowboardcross	k6eAd1
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
a	a	k8xC
extrémní	extrémní	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Lyžování	lyžování	k1gNnSc1
na	na	k7c6
trávě	tráva	k1gFnSc6
</s>
<s>
Kolečkové	kolečkový	k2eAgFnPc1d1
lyže	lyže	k1gFnPc1
</s>
<s>
Rychlostní	rychlostní	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
</s>
<s>
Výjimkou	výjimka	k1gFnSc7
mezi	mezi	k7c7
lyžařskými	lyžařský	k2eAgInPc7d1
sporty	sport	k1gInPc7
je	být	k5eAaImIp3nS
biatlon	biatlon	k1gInSc1
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
nepodléhá	podléhat	k5eNaImIp3nS
FIS	fis	k1gNnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
řídí	řídit	k5eAaImIp3nS
jej	on	k3xPp3gInSc4
vlastní	vlastní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
biatlonová	biatlonový	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
IBU	IBU	kA
<g/>
,	,	kIx,
International	International	k1gFnSc1
Biathlon	Biathlon	k1gInSc1
Union	union	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
oddělila	oddělit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
skialpinismus	skialpinismus	k1gInSc1
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
federaci	federace	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
skialpinistická	skialpinistický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Prezidenti	prezident	k1gMnPc1
FIS	fis	k1gNnSc2
</s>
<s>
PrezidentObdobíNárodnost	PrezidentObdobíNárodnost	k1gFnSc1
</s>
<s>
Ivar	Ivar	k1gInSc1
Holmquist	Holmquist	k1gInSc1
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
Švédsko	Švédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Nicolai	Nicolai	k6eAd1
Ramm	Ramm	k1gInSc1
Östgaard	Östgaard	k1gInSc1
<g/>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
Norsko	Norsko	k1gNnSc4
Norsko	Norsko	k1gNnSc1
</s>
<s>
Marc	Marc	k1gInSc1
Hodler	Hodler	k1gInSc1
<g/>
1951	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Gian-Franco	Gian-Franco	k6eAd1
Kasperod	Kasperod	k1gInSc1
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
FIS	fis	k1gNnSc1
pořádá	pořádat	k5eAaImIp3nS
následující	následující	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
alpském	alpský	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
akrobatickém	akrobatický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
a	a	k8xC
snowboardingu	snowboarding	k1gInSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
na	na	k7c6
lyžích	lyže	k1gFnPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
International	International	k1gFnSc2
Ski	ski	k1gFnSc2
Federation	Federation	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Inside	Insid	k1gInSc5
FIS	fis	k1gNnSc2
-	-	kIx~
About	About	k1gInSc1
FIS	FIS	kA
<g/>
:	:	kIx,
National	National	k1gMnSc1
Ski	ski	k1gFnSc2
Assocations	Assocations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Francouzská	francouzský	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
FIS	fis	k1gNnSc2
je	být	k5eAaImIp3nS
užívána	užíván	k2eAgFnSc1d1
ve	v	k7c6
všech	všecek	k3xTgInPc6
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federace	federace	k1gFnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgInPc4
"	"	kIx"
<g/>
úřední	úřední	k2eAgInPc4d1
<g/>
"	"	kIx"
jazyky	jazyk	k1gInPc4
-	-	kIx~
angličtinu	angličtina	k1gFnSc4
<g/>
,	,	kIx,
francouzštinu	francouzština	k1gFnSc4
a	a	k8xC
němčinu	němčina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KÖSSL	KÖSSL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
KRÁTKÝ	Krátký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
II	II	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Rozvoj	rozvoj	k1gInSc1
sportovního	sportovní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
105	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Svaz	svaz	k1gInSc1
lyžařů	lyžař	k1gMnPc2
ČR	ČR	kA
–	–	k?
člen	člen	k1gMnSc1
ČOV	ČOV	kA
<g/>
,	,	kIx,
ČUS	ČUS	k?
a	a	k8xC
FIS	FIS	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
strany	strana	k1gFnPc1
FIS	fis	k1gNnSc2
</s>
<s>
FIS	fis	k1gNnSc1
na	na	k7c6
Youtube	Youtub	k1gInSc5
</s>
<s>
Svaz	svaz	k1gInSc1
lyžařů	lyžař	k1gMnPc2
ČR	ČR	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ASOIF	ASOIF	kA
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
letních	letní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BWF	BWF	kA
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIBA	FIBA	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
AIBA	AIBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UCI	UCI	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIFA	FIFA	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIG	FIG	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
IHF	IHF	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ISAF	ISAF	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FEI	FEI	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
IJF	IJF	kA
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICF	ICF	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
WA	WA	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UIPM	UIPM	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgInSc4d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIH	FIH	kA
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WR	WR	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ISSF	ISSF	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ITTF	ITTF	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIE	FIE	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WTF	WTF	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ITF	ITF	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITU	ITU	kA
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FISA	FISA	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
FIVB	FIVB	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWF	IWF	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
UWW	UWW	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
AIOWF	AIOWF	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IBU	IBU	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBSF	IBSF	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISU	ISU	kA
(	(	kIx(
<g/>
bruslení	bruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IIHF	IIHF	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIS	FIS	kA
(	(	kIx(
<g/>
lyžařské	lyžařský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
sáňkařský	sáňkařský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
ARISF	ARISF	kA
(	(	kIx(
<g/>
37	#num#	k4
<g/>
)	)	kIx)
<g/>
Další	další	k2eAgFnSc1d1
federace	federace	k1gFnSc1
uznané	uznaný	k2eAgFnSc2d1
MOV	MOV	kA
</s>
<s>
IFAF	IFAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIB	FIB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WBSC	WBSC	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc1
a	a	k8xC
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WB	WB	kA
(	(	kIx(
<g/>
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WBF	WBF	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFF	IFF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIAA	UIAA	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICU	ICU	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WKF	WKF	kA
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICC	ICC	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIRS	FIRS	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CMSB	CMSB	kA
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WCBS	WCBS	kA
(	(	kIx(
<g/>
kulečník	kulečník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAI	FAI	kA
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIM	FIM	kA
(	(	kIx(
<g/>
motocyklový	motocyklový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMA	IFMA	kA
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IFNA	IFNA	kA
(	(	kIx(
<g/>
netball	netball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IOF	IOF	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIPV	FIPV	kA
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CMAS	CMAS	kA
(	(	kIx(
<g/>
podvodní	podvodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIP	FIP	kA
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
TWIF	TWIF	kA
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISMF	ISMF	kA
(	(	kIx(
<g/>
skialpinismus	skialpinismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFSC	IFSC	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSF	WSF	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFS	IFS	kA
(	(	kIx(
<g/>
sumo	suma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
ISA	ISA	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIDE	FIDE	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
WDSF	WDSF	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WFDF	WFDF	kA
(	(	kIx(
<g/>
ultimate	ultimat	k1gInSc5
frisbee	frisbeus	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
IWWF	IWWF	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
a	a	k8xC
wakeboarding	wakeboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIM	UIM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWUF	IWUF	kA
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
ILSF	ILSF	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ve	v	k7c6
SportAccordu	SportAccordo	k1gNnSc6
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IAF	IAF	kA
(	(	kIx(
<g/>
aikido	aikida	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FMJD	FMJD	kA
(	(	kIx(
<g/>
dáma	dáma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
IDBF	IDBF	kA
(	(	kIx(
<g/>
dračí	dračí	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
JJIF	JJIF	kA
(	(	kIx(
<g/>
džú-džucu	džú-džucu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
IFA	IFA	kA
(	(	kIx(
<g/>
faustball	faustball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
go	go	k?
<g/>
)	)	kIx)
</s>
<s>
IFI	IFI	kA
(	(	kIx(
<g/>
ice	ice	k?
stock	stock	k1gInSc1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIK	fik	k0
(	(	kIx(
<g/>
kendó	kendó	k?
<g/>
)	)	kIx)
</s>
<s>
WAKO	WAKO	kA
(	(	kIx(
<g/>
kickboxing	kickboxing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBB	IFBB	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
a	a	k8xC
fitness	fitness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WMF	WMF	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISFF	ISFF	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICSF	ICSF	kA
(	(	kIx(
<g/>
rybolovná	rybolovný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIAS	FIAS	kA
(	(	kIx(
<g/>
sambo	samba	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FISav	FISav	k1gInSc4
(	(	kIx(
<g/>
savate	savat	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ISTAF	ISTAF	kA
(	(	kIx(
<g/>
sepak	sepak	k1gMnSc1
takraw	takraw	k?
<g/>
)	)	kIx)
</s>
<s>
IPF	IPF	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISTF	ISTF	kA
(	(	kIx(
<g/>
soft	soft	k?
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CIPS	CIPS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WDF	WDF	kA
(	(	kIx(
<g/>
šipky	šipka	k1gFnPc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
federace	federace	k1gFnPc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WAF	WAF	kA
(	(	kIx(
<g/>
armwrestling	armwrestling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ARI	ARI	kA
(	(	kIx(
<g/>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBA	iba	k6eAd1
(	(	kIx(
<g/>
bodyboarding	bodyboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PBA	PBA	kA
(	(	kIx(
<g/>
bowls	bowls	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBA	IFBA	kA
(	(	kIx(
<g/>
broomball	broomball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IKAEF	IKAEF	kA
(	(	kIx(
<g/>
eskrima	eskrim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
WFA	WFA	kA
(	(	kIx(
<g/>
footbag	footbag	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ISBHF	ISBHF	kA
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
kroket	kroket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
kabaddi	kabaddit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
IMMAF	IMMAF	kA
(	(	kIx(
<g/>
MMA	MMA	kA
<g/>
)	)	kIx)
</s>
<s>
IFP	IFP	kA
(	(	kIx(
<g/>
poker	poker	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IPSC	IPSC	kA
(	(	kIx(
<g/>
practical	practicat	k5eAaPmAgInS
shooting	shooting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IQA	IQA	kA
(	(	kIx(
<g/>
mudlovský	mudlovský	k2eAgInSc1d1
famfrpál	famfrpál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMAR	IFMAR	kA
(	(	kIx(
<g/>
závody	závod	k1gInPc1
automobilových	automobilový	k2eAgInPc2d1
modelů	model	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
FIFTA	FIFTA	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
rogaining	rogaining	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
RLIF	RLIF	kA
(	(	kIx(
<g/>
třináctkové	třináctkový	k2eAgNnSc1d1
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSSA	WSSA	kA
(	(	kIx(
<g/>
sport	sport	k1gInSc1
stacking	stacking	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITPF	ITPF	kA
(	(	kIx(
<g/>
Tent	tent	k1gInSc1
pegging	pegging	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIT	fit	k2eAgInSc1d1
(	(	kIx(
<g/>
touch	touch	k1gInSc1
rugby	rugby	k1gNnSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Světových	světový	k2eAgFnPc2d1
her	hra	k1gFnPc2
•	•	k?
SportAccord	SportAccord	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
83519	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2046286-4	2046286-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2286	#num#	k4
7025	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003145346	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
133186546	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003145346	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
