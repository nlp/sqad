<p>
<s>
Brož	brož	k1gFnSc1	brož
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
broche	broch	k1gInSc2	broch
–	–	k?	–
vřeteno	vřeteno	k1gNnSc1	vřeteno
<g/>
,	,	kIx,	,
kolík	kolík	k1gInSc1	kolík
<g/>
,	,	kIx,	,
rožeň	rožeň	k1gInSc1	rožeň
či	či	k8xC	či
sponka	sponka	k1gFnSc1	sponka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mužský	mužský	k2eAgInSc4d1	mužský
či	či	k8xC	či
ženský	ženský	k2eAgInSc4d1	ženský
šperk	šperk	k1gInSc4	šperk
<g/>
,	,	kIx,	,
připínaný	připínaný	k2eAgInSc4d1	připínaný
na	na	k7c4	na
oděv	oděv	k1gInSc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
spon	spona	k1gFnPc2	spona
<g/>
,	,	kIx,	,
agraf	agrafa	k1gFnPc2	agrafa
či	či	k8xC	či
fibulí	fibule	k1gFnPc2	fibule
nemá	mít	k5eNaImIp3nS	mít
brož	brož	k1gFnSc4	brož
nosnou	nosný	k2eAgFnSc4d1	nosná
=	=	kIx~	=
spínací	spínací	k2eAgFnSc4d1	spínací
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
účel	účel	k1gInSc1	účel
dekorační	dekorační	k2eAgInSc1d1	dekorační
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
estetický	estetický	k2eAgInSc1d1	estetický
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
spony	spona	k1gFnPc4	spona
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
broží	brož	k1gFnPc2	brož
k	k	k7c3	k
připnutí	připnutí	k1gNnSc3	připnutí
na	na	k7c4	na
oděv	oděv	k1gInSc4	oděv
převážně	převážně	k6eAd1	převážně
jehlici	jehlice	k1gFnSc4	jehlice
či	či	k8xC	či
zavírací	zavírací	k2eAgInSc4d1	zavírací
(	(	kIx(	(
<g/>
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
<g/>
)	)	kIx)	)
špendlík	špendlík	k1gInSc4	špendlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
spony	spona	k1gFnPc1	spona
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
šperkům	šperk	k1gInPc3	šperk
užívaným	užívaný	k2eAgInPc3d1	užívaný
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
po	po	k7c4	po
novověk	novověk	k1gInSc4	novověk
<g/>
,	,	kIx,	,
brože	brož	k1gFnPc1	brož
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
až	až	k9	až
na	na	k7c6	na
luxusní	luxusní	k2eAgFnSc6d1	luxusní
garderobě	garderoba	k1gFnSc6	garderoba
doby	doba	k1gFnSc2	doba
gotické	gotický	k2eAgFnSc2d1	gotická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
baroka	baroko	k1gNnSc2	baroko
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
broží	brož	k1gFnPc2	brož
bývá	bývat	k5eAaImIp3nS	bývat
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
dvorskou	dvorský	k2eAgFnSc7d1	dvorská
módou	móda	k1gFnSc7	móda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
francouzské	francouzský	k2eAgFnSc2d1	francouzská
milostnice	milostnice	k1gFnSc2	milostnice
<g/>
,	,	kIx,	,
šlechtičny	šlechtična	k1gFnSc2	šlechtična
Marie	Maria	k1gFnSc2	Maria
Chantal	Chantal	k1gMnSc1	Chantal
<g/>
,	,	kIx,	,
markýzy	markýza	k1gFnPc1	markýza
ze	z	k7c2	z
Sévigné	Sévigná	k1gFnSc2	Sévigná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
Brož	Brož	k1gMnSc1	Brož
a	a	k8xC	a
la	la	k1gNnSc1	la
Sévigné	Sévigná	k1gFnSc2	Sévigná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
korpusu	korpus	k1gInSc6	korpus
má	mít	k5eAaImIp3nS	mít
symetricky	symetricky	k6eAd1	symetricky
zavěšené	zavěšený	k2eAgFnPc4d1	zavěšená
tři	tři	k4xCgFnPc4	tři
přívěsky	přívěska	k1gFnPc4	přívěska
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kapkovité	kapkovitý	k2eAgInPc1d1	kapkovitý
barokní	barokní	k2eAgInPc1d1	barokní
perly	perl	k1gInPc1	perl
na	na	k7c6	na
očku	očko	k1gNnSc6	očko
či	či	k8xC	či
řetízku	řetízek	k1gInSc6	řetízek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
mívá	mívat	k5eAaImIp3nS	mívat
kosočtverečný	kosočtverečný	k2eAgInSc4d1	kosočtverečný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
dlaždicovitě	dlaždicovitě	k6eAd1	dlaždicovitě
(	(	kIx(	(
<g/>
v	v	k7c6	v
pavé	pavá	k1gFnSc6	pavá
<g/>
)	)	kIx)	)
osazené	osazený	k2eAgInPc1d1	osazený
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hmotnost	hmotnost	k1gFnSc4	hmotnost
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
přišit	přišít	k5eAaPmNgInS	přišít
doprostřed	doprostřed	k7c2	doprostřed
živůtku	živůtek	k1gInSc2	živůtek
společenských	společenský	k2eAgInPc2d1	společenský
šatů	šat	k1gInPc2	šat
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
pod	pod	k7c4	pod
dekolt	dekolt	k1gInSc4	dekolt
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
drahokamy	drahokam	k1gInPc7	drahokam
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
sklem	sklo	k1gNnSc7	sklo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
štrasu	štras	k1gInSc2	štras
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
typ	typ	k1gInSc1	typ
má	mít	k5eAaImIp3nS	mít
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
geometrický	geometrický	k2eAgInSc1d1	geometrický
tvar	tvar	k1gInSc1	tvar
podélný	podélný	k2eAgInSc1d1	podélný
<g/>
,	,	kIx,	,
příčný	příčný	k2eAgInSc1d1	příčný
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgFnSc1d1	kruhová
rozeta	rozeta	k1gFnSc1	rozeta
<g/>
,	,	kIx,	,
terč	terč	k1gInSc1	terč
<g/>
,	,	kIx,	,
či	či	k8xC	či
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
figurky	figurka	k1gFnSc2	figurka
nebo	nebo	k8xC	nebo
hlavičky	hlavička	k1gFnSc2	hlavička
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
brož	brož	k1gFnSc4	brož
lze	lze	k6eAd1	lze
připnout	připnout	k5eAaPmF	připnout
kamkoliv	kamkoliv	k6eAd1	kamkoliv
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
polovinu	polovina	k1gFnSc4	polovina
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
na	na	k7c4	na
košili	košile	k1gFnSc4	košile
<g/>
,	,	kIx,	,
živůtek	živůtek	k1gInSc4	živůtek
šatů	šat	k1gInPc2	šat
nebo	nebo	k8xC	nebo
klopu	klopa	k1gFnSc4	klopa
saka	sako	k1gNnSc2	sako
či	či	k8xC	či
kabátu	kabát	k1gInSc2	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgFnPc1d1	gotická
brože	brož	k1gFnPc1	brož
se	se	k3xPyFc4	se
často	často	k6eAd1	často
přivěšovaly	přivěšovat	k5eAaImAgFnP	přivěšovat
na	na	k7c4	na
baret	baret	k1gInSc4	baret
či	či	k8xC	či
na	na	k7c4	na
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dana	Dana	k1gFnSc1	Dana
Stehlíková	Stehlíková	k1gFnSc1	Stehlíková
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
zlatnictví	zlatnictví	k1gNnSc2	zlatnictví
<g/>
,	,	kIx,	,
stříbrnictví	stříbrnictví	k1gNnSc2	stříbrnictví
a	a	k8xC	a
klenotnictví	klenotnictví	k1gNnSc2	klenotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
s.	s.	k?	s.
69-70	[number]	k4	69-70
a	a	k8xC	a
zejména	zejména	k9	zejména
s.	s.	k?	s.
445	[number]	k4	445
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
brož	brož	k1gFnSc1	brož
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
