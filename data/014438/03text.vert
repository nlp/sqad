<s>
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
(	(	kIx(
<g/>
píseň	píseň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
</s>
<s>
Skladba	skladba	k1gFnSc1
od	od	k7c2
Lucie	Lucie	k1gFnSc2
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
Z	z	k7c2
alba	album	k1gNnSc2
</s>
<s>
Missariel	Missariel	k1gMnSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
1992	#num#	k4
</s>
<s>
Žánr	žánr	k1gInSc1
</s>
<s>
pop	pop	k1gMnSc1
music	music	k1gMnSc1
</s>
<s>
Skladatel	skladatel	k1gMnSc1
</s>
<s>
Prince	princ	k1gMnSc2
</s>
<s>
Textař	textař	k1gMnSc1
</s>
<s>
Gabriela	Gabriela	k1gFnSc1
Osvaldová	Osvaldová	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
popová	popový	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
z	z	k7c2
alba	album	k1gNnSc2
Missariel	Missariel	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
české	český	k2eAgFnSc2d1
zpěvačky	zpěvačka	k1gFnSc2
Lucie	Lucie	k1gFnSc2
Bílé	bílé	k1gNnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
názpívala	názpívat	k5eAaPmAgFnS,k5eAaImAgFnS
společně	společně	k6eAd1
s	s	k7c7
Ilonou	Ilona	k1gFnSc7
Csákovou	Csáková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
skladby	skladba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
původně	původně	k6eAd1
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
Alphabet	Alphabet	k1gInSc4
St.	st.	kA
a	a	k8xC
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
v	v	k7c6
albu	album	k1gNnSc6
Lovesexy	Lovesex	k1gInPc4
,	,	kIx,
je	on	k3xPp3gInPc4
americký	americký	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
se	se	k3xPyFc4
za	za	k7c2
autora	autor	k1gMnSc2
vydával	vydávat	k5eAaImAgMnS,k5eAaPmAgMnS
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
po	po	k7c6
smrti	smrt	k1gFnSc6
Prince	princ	k1gMnSc2
ale	ale	k8xC
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
autorem	autor	k1gMnSc7
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
i	i	k9
na	na	k7c6
reedici	reedice	k1gFnSc6
desky	deska	k1gFnSc2
Missariel	Missariela	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Text	text	k1gInSc4
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
Gabriela	Gabriela	k1gFnSc1
Osvaldová	Osvaldová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Skladba	skladba	k1gFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejznámějších	známý	k2eAgFnPc2d3
skladeb	skladba	k1gFnPc2
Lucie	Lucie	k1gFnSc2
Bílé	bílé	k1gNnSc1
<g/>
,	,	kIx,
album	album	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
zařazeno	zařadit	k5eAaPmNgNnS
mezi	mezi	k7c4
10	#num#	k4
nejlepších	dobrý	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
desek	deska	k1gFnPc2
popové	popový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Videoklip	videoklip	k1gInSc1
</s>
<s>
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
vyšlo	vyjít	k5eAaPmAgNnS
album	album	k1gNnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
také	také	k9
čtyřminutový	čtyřminutový	k2eAgInSc1d1
videoklip	videoklip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystupují	vystupovat	k5eAaImIp3nP
zde	zde	k6eAd1
Lucie	Lucie	k1gFnSc1
Bílá	bílý	k2eAgFnSc1d1
s	s	k7c7
Ilonou	Ilona	k1gFnSc7
Csákovou	Csákův	k2eAgFnSc7d1
jako	jako	k8xS,k8xC
školačky	školačka	k1gFnSc2
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
Lou	Lou	k1gMnSc1
Fanánek	Fanánek	k1gMnSc1
Hagen	Hagen	k2eAgMnSc1d1
v	v	k7c6
uniformě	uniforma	k1gFnSc6
strážníka	strážník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
vášnivě	vášnivě	k6eAd1
líbá	líbat	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
kolegou	kolega	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
diváckého	divácký	k2eAgNnSc2d1
hlasování	hlasování	k1gNnSc2
i	i	k8xC
odborné	odborný	k2eAgFnSc2d1
poroty	porota	k1gFnSc2
byl	být	k5eAaImAgInS
videoklip	videoklip	k1gInSc1
vyhlášen	vyhlásit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
nejlepší	dobrý	k2eAgMnSc1d3
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byl	být	k5eAaImAgInS
diváky	divák	k1gMnPc7
zvolen	zvolit	k5eAaPmNgInS
za	za	k7c4
třetí	třetí	k4xOgInSc4
nejlepší	dobrý	k2eAgInSc4d3
český	český	k2eAgInSc4d1
klip	klip	k1gInSc4
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
se	se	k3xPyFc4
nesměl	smět	k5eNaImAgMnS
vysílat	vysílat	k5eAaImF
v	v	k7c6
televizi	televize	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgMnS
označen	označit	k5eAaPmNgMnS
za	za	k7c4
zvrácený	zvrácený	k2eAgInSc4d1
a	a	k8xC
neetický	etický	k2eNgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Proč	proč	k6eAd1
Lucie	Lucie	k1gFnSc1
Bílá	bílý	k2eAgFnSc1d1
vděčí	vděčit	k5eAaImIp3nS
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
Princovi	princ	k1gMnSc3
a	a	k8xC
měla	mít	k5eAaImAgNnP
by	by	kYmCp3nP
mu	on	k3xPp3gNnSc3
alespoň	alespoň	k9
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
poděkovat	poděkovat	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cool	Cool	k1gMnSc1
magazin	magazin	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ukradená	ukradený	k2eAgFnSc1d1
písnička	písnička	k1gFnSc1
XII	XII	kA
<g/>
.	.	kIx.
–	–	k?
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lucie	Lucie	k1gFnSc1
Bílá	bílý	k2eAgFnSc1d1
–	–	k?
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
–	–	k?
Hudební	hudební	k2eAgMnPc1d1
videokli	videoknout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
CSFD	CSFD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Právě	právě	k6eAd1
uplynulo	uplynout	k5eAaPmAgNnS
20	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
natáčení	natáčení	k1gNnSc2
videoklipu	videoklip	k1gInSc2
Láska	láska	k1gFnSc1
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ahaonline	Ahaonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
