<s>
Astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
α	α	k?	α
z	z	k7c2	z
ά	ά	k?	ά
(	(	kIx(	(
<g/>
astron	astron	k1gMnSc1	astron
<g/>
)	)	kIx)	)
–	–	k?	–
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
λ	λ	k?	λ
(	(	kIx(	(
<g/>
logos	logos	k1gInSc1	logos
<g/>
)	)	kIx)	)
–	–	k?	–
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
hvězdopravectví	hvězdopravectví	k1gNnSc4	hvězdopravectví
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
hermetický	hermetický	k2eAgInSc1d1	hermetický
obor	obor	k1gInSc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jejími	její	k3xOp3gMnPc7	její
stoupenci	stoupenec	k1gMnPc7	stoupenec
předpokládaných	předpokládaný	k2eAgFnPc2d1	předpokládaná
souvislostí	souvislost	k1gFnPc2	souvislost
mezi	mezi	k7c7	mezi
děním	dění	k1gNnSc7	dění
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
charakterizovaným	charakterizovaný	k2eAgMnSc7d1	charakterizovaný
zejména	zejména	k9	zejména
pohybem	pohyb	k1gInSc7	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
souvislostí	souvislost	k1gFnPc2	souvislost
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
vlivy	vliv	k1gInPc4	vliv
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Činí	činit	k5eAaImIp3nS	činit
tak	tak	k9	tak
zejména	zejména	k9	zejména
formou	forma	k1gFnSc7	forma
hledání	hledání	k1gNnSc1	hledání
analogií	analogie	k1gFnPc2	analogie
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
dějů	děj	k1gInPc2	děj
minulých	minulý	k2eAgFnPc2d1	minulá
a	a	k8xC	a
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
<g/>
,	,	kIx,	,
předvídání	předvídání	k1gNnSc1	předvídání
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
dění	dění	k1gNnSc2	dění
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
,	,	kIx,	,
hodnocení	hodnocení	k1gNnSc6	hodnocení
nebeských	nebeský	k2eAgInPc2d1	nebeský
vlivů	vliv	k1gInPc2	vliv
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
psychiku	psychika	k1gFnSc4	psychika
a	a	k8xC	a
fyzické	fyzický	k2eAgNnSc4d1	fyzické
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
na	na	k7c4	na
světové	světový	k2eAgNnSc4d1	světové
dění	dění	k1gNnSc4	dění
<g/>
,	,	kIx,	,
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc4d1	politická
souvislosti	souvislost	k1gFnPc4	souvislost
atd.	atd.	kA	atd.
Rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
oblastí	oblast	k1gFnSc7	oblast
činnosti	činnost	k1gFnSc2	činnost
astrologie	astrologie	k1gFnSc2	astrologie
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
souvztažností	souvztažnost	k1gFnPc2	souvztažnost
mezi	mezi	k7c7	mezi
děním	dění	k1gNnSc7	dění
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
jak	jak	k8xS	jak
jedince	jedinko	k6eAd1	jedinko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
celého	celý	k2eAgNnSc2d1	celé
lidského	lidský	k2eAgNnSc2d1	lidské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
tohoto	tento	k3xDgNnSc2	tento
zkoumání	zkoumání	k1gNnSc2	zkoumání
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
astrologové	astrolog	k1gMnPc1	astrolog
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
různých	různý	k2eAgInPc6d1	různý
civilizačních	civilizační	k2eAgInPc6d1	civilizační
okruzích	okruh	k1gInPc6	okruh
řadu	řad	k1gInSc2	řad
samostatných	samostatný	k2eAgInPc2d1	samostatný
symbolických	symbolický	k2eAgInPc2d1	symbolický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
tradičních	tradiční	k2eAgFnPc2d1	tradiční
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pochopení	pochopení	k1gNnSc4	pochopení
vztahů	vztah	k1gInPc2	vztah
nebeských	nebeský	k2eAgInPc2d1	nebeský
a	a	k8xC	a
pozemských	pozemský	k2eAgInPc2d1	pozemský
dějů	děj	k1gInPc2	děj
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
alchymií	alchymie	k1gFnSc7	alchymie
a	a	k8xC	a
magií	magie	k1gFnSc7	magie
je	být	k5eAaImIp3nS	být
západní	západní	k2eAgFnSc1d1	západní
astrologie	astrologie	k1gFnSc1	astrologie
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
mezi	mezi	k7c4	mezi
hermetismus	hermetismus	k1gInSc4	hermetismus
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
základním	základní	k2eAgInSc6d1	základní
hermetickém	hermetický	k2eAgInSc6d1	hermetický
axiomu	axiom	k1gInSc6	axiom
o	o	k7c6	o
jednotě	jednota	k1gFnSc6	jednota
makrokosmu	makrokosmos	k1gInSc2	makrokosmos
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
)	)	kIx)	)
a	a	k8xC	a
mikrokosmu	mikrokosmos	k1gInSc2	mikrokosmos
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
primárních	primární	k2eAgInPc2d1	primární
astrologických	astrologický	k2eAgInPc2d1	astrologický
poznatků	poznatek	k1gInPc2	poznatek
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Astrologie	astrologie	k1gFnSc1	astrologie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
stol.	stol.	k?	stol.
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
komunitou	komunita	k1gFnSc7	komunita
odmítána	odmítán	k2eAgFnSc1d1	odmítána
jako	jako	k8xC	jako
nevědecká	vědecký	k2eNgFnSc1d1	nevědecká
či	či	k8xC	či
pseudovědecká	pseudovědecký	k2eAgFnSc1d1	pseudovědecká
<g/>
.	.	kIx.	.
</s>
<s>
Astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
chápe	chápat	k5eAaImIp3nS	chápat
většina	většina	k1gFnSc1	většina
astrologů	astrolog	k1gMnPc2	astrolog
západní	západní	k2eAgFnSc2d1	západní
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
synchronicity	synchronicita	k1gFnSc2	synchronicita
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
kauzality	kauzalita	k1gFnSc2	kauzalita
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vztah	vztah	k1gInSc1	vztah
postavení	postavení	k1gNnSc2	postavení
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
k	k	k7c3	k
jevům	jev	k1gInPc3	jev
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
nezabývá	zabývat	k5eNaImIp3nS	zabývat
příčinami	příčina	k1gFnPc7	příčina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
<g/>
)	)	kIx)	)
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
není	být	k5eNaImIp3nS	být
deterministická	deterministický	k2eAgFnSc1d1	deterministická
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
sklony	sklon	k1gInPc4	sklon
a	a	k8xC	a
tendence	tendence	k1gFnPc4	tendence
<g/>
,	,	kIx,	,
neurčuje	určovat	k5eNaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
musí	muset	k5eAaImIp3nS	muset
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výstupy	výstup	k1gInPc1	výstup
astrologie	astrologie	k1gFnSc2	astrologie
nejsou	být	k5eNaImIp3nP	být
exaktní	exaktní	k2eAgFnPc1d1	exaktní
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
empirické	empirický	k2eAgFnPc1d1	empirická
<g/>
.	.	kIx.	.
</s>
<s>
Astrologie	astrologie	k1gFnSc1	astrologie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
spíše	spíše	k9	spíše
na	na	k7c6	na
zkušenostech	zkušenost	k1gFnPc6	zkušenost
a	a	k8xC	a
tradici	tradice	k1gFnSc6	tradice
než	než	k8xS	než
na	na	k7c6	na
měřitelných	měřitelný	k2eAgFnPc6d1	měřitelná
a	a	k8xC	a
statisticky	statisticky	k6eAd1	statisticky
zpracovatelných	zpracovatelný	k2eAgInPc6d1	zpracovatelný
výzkumech	výzkum	k1gInPc6	výzkum
podle	podle	k7c2	podle
kritérií	kritérion	k1gNnPc2	kritérion
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
hermeneutikou	hermeneutika	k1gFnSc7	hermeneutika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
interpretačním	interpretační	k2eAgNnSc7d1	interpretační
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ji	on	k3xPp3gFnSc4	on
nutno	nutno	k6eAd1	nutno
řadit	řadit	k5eAaImF	řadit
mezi	mezi	k7c4	mezi
esoterické	esoterický	k2eAgFnPc4d1	esoterická
nauky	nauka	k1gFnPc4	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
oblastí	oblast	k1gFnSc7	oblast
astrologického	astrologický	k2eAgNnSc2d1	astrologické
zkoumání	zkoumání	k1gNnSc2	zkoumání
je	být	k5eAaImIp3nS	být
popis	popis	k1gInSc1	popis
vztahu	vztah	k1gInSc2	vztah
postavení	postavení	k1gNnSc4	postavení
astrologických	astrologický	k2eAgFnPc2d1	astrologická
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
narození	narození	k1gNnSc2	narození
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
pomocí	pomocí	k7c2	pomocí
astrologického	astrologický	k2eAgInSc2d1	astrologický
diagramu	diagram	k1gInSc2	diagram
zvaného	zvaný	k2eAgInSc2d1	zvaný
horoskop	horoskop	k1gInSc1	horoskop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
astronomických	astronomický	k2eAgFnPc2d1	astronomická
planet	planeta	k1gFnPc2	planeta
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
označovány	označovat	k5eAaImNgFnP	označovat
i	i	k9	i
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
tradice	tradice	k1gFnSc1	tradice
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
horoskopu	horoskop	k1gInSc2	horoskop
také	také	k9	také
postavení	postavení	k1gNnSc4	postavení
mnoha	mnoho	k4c2	mnoho
skutečných	skutečný	k2eAgInPc2d1	skutečný
astronomických	astronomický	k2eAgInPc2d1	astronomický
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
mlhoviny	mlhovina	k1gFnPc1	mlhovina
<g/>
,	,	kIx,	,
asteroidy	asteroida	k1gFnPc1	asteroida
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k9	i
řadu	řada	k1gFnSc4	řada
exaktně	exaktně	k6eAd1	exaktně
vypočtených	vypočtený	k2eAgInPc2d1	vypočtený
astronomických	astronomický	k2eAgInPc2d1	astronomický
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ascendent	ascendent	k1gMnSc1	ascendent
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
lunární	lunární	k2eAgInPc4d1	lunární
uzly	uzel	k1gInPc4	uzel
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bod	bod	k1gInSc1	bod
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
někteří	některý	k3yIgMnPc1	některý
astrologové	astrolog	k1gMnPc1	astrolog
vztah	vztah	k1gInSc4	vztah
postavení	postavení	k1gNnSc4	postavení
astrologických	astrologický	k2eAgFnPc2d1	astrologická
planet	planeta	k1gFnPc2	planeta
na	na	k7c4	na
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
neživé	živý	k2eNgFnPc1d1	neživá
věci	věc	k1gFnPc1	věc
(	(	kIx(	(
<g/>
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
pozemské	pozemský	k2eAgFnPc1d1	pozemská
události	událost	k1gFnPc1	událost
(	(	kIx(	(
<g/>
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
katastrofy	katastrofa	k1gFnPc1	katastrofa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
různě	různě	k6eAd1	různě
kladené	kladený	k2eAgFnPc4d1	kladená
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
oblastem	oblast	k1gFnPc3	oblast
astrologie	astrologie	k1gFnSc2	astrologie
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
specializované	specializovaný	k2eAgInPc1d1	specializovaný
astrologické	astrologický	k2eAgInPc1d1	astrologický
obory	obor	k1gInPc1	obor
jako	jako	k8xC	jako
mundánní	mundánní	k2eAgFnSc1d1	mundánní
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
meteoastrologie	meteoastrologie	k1gFnSc1	meteoastrologie
<g/>
,	,	kIx,	,
horární	horární	k2eAgFnSc1d1	horární
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
astromedicína	astromedicína	k1gFnSc1	astromedicína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologickém	astrologický	k2eAgNnSc6d1	astrologické
zkoumání	zkoumání	k1gNnSc6	zkoumání
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k8xC	jak
horoskop	horoskop	k1gInSc4	horoskop
sestavený	sestavený	k2eAgInSc4d1	sestavený
pro	pro	k7c4	pro
okamžik	okamžik	k1gInSc4	okamžik
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
či	či	k8xC	či
vzniku	vznik	k1gInSc3	vznik
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nativní	nativní	k2eAgInSc1d1	nativní
horoskop	horoskop	k1gInSc1	horoskop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
postavení	postavení	k1gNnSc1	postavení
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
vůči	vůči	k7c3	vůči
jejich	jejich	k3xOp3gFnSc3	jejich
pozici	pozice	k1gFnSc3	pozice
v	v	k7c6	v
nativním	nativní	k2eAgInSc6d1	nativní
horoskopu	horoskop	k1gInSc6	horoskop
(	(	kIx(	(
<g/>
tranzity	tranzit	k1gInPc4	tranzit
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zprofanoval	zprofanovat	k5eAaPmAgInS	zprofanovat
způsob	způsob	k1gInSc1	způsob
lidové	lidový	k2eAgFnSc2d1	lidová
tvorby	tvorba	k1gFnSc2	tvorba
horoskopů	horoskop	k1gInPc2	horoskop
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
zejména	zejména	k9	zejména
zbulvarizovaným	zbulvarizovaný	k2eAgInSc7d1	zbulvarizovaný
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
předpovědí	předpověď	k1gFnPc2	předpověď
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
pro	pro	k7c4	pro
čtenáře	čtenář	k1gMnPc4	čtenář
narozené	narozený	k2eAgNnSc4d1	narozené
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
obdobích	období	k1gNnPc6	období
roku	rok	k1gInSc2	rok
formálně	formálně	k6eAd1	formálně
totožných	totožný	k2eAgFnPc2d1	totožná
s	s	k7c7	s
astrologickými	astrologický	k2eAgNnPc7d1	astrologické
znameními	znamení	k1gNnPc7	znamení
Zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lidová	lidový	k2eAgFnSc1d1	lidová
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
prováděna	provádět	k5eAaImNgFnS	provádět
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
astrologické	astrologický	k2eAgFnSc2d1	astrologická
souvislosti	souvislost	k1gFnSc2	souvislost
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
podobný	podobný	k2eAgInSc1d1	podobný
způsob	způsob	k1gInSc1	způsob
symboliky	symbolika	k1gFnSc2	symbolika
a	a	k8xC	a
pseudoastrologický	pseudoastrologický	k2eAgInSc4d1	pseudoastrologický
slovník	slovník	k1gInSc4	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
principů	princip	k1gInPc2	princip
astrologických	astrologický	k2eAgInPc2d1	astrologický
výzkumů	výzkum	k1gInPc2	výzkum
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
mizivou	mizivý	k2eAgFnSc4d1	mizivá
vypovídací	vypovídací	k2eAgFnSc4d1	vypovídací
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
předpovědím	předpověď	k1gFnPc3	předpověď
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
prvků	prvek	k1gInPc2	prvek
skutečného	skutečný	k2eAgInSc2d1	skutečný
astrologického	astrologický	k2eAgInSc2d1	astrologický
horoskopu	horoskop	k1gInSc2	horoskop
-	-	kIx~	-
postavení	postavení	k1gNnSc4	postavení
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
nativním	nativní	k2eAgInSc6d1	nativní
horoskopu	horoskop	k1gInSc6	horoskop
-	-	kIx~	-
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
jeho	jeho	k3xOp3gInPc1	jeho
prvky	prvek	k1gInPc1	prvek
úplně	úplně	k6eAd1	úplně
pomíjí	pomíjet	k5eAaImIp3nP	pomíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
samostatných	samostatný	k2eAgInPc2d1	samostatný
astrologických	astrologický	k2eAgInPc2d1	astrologický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
ze	z	k7c2	z
společných	společný	k2eAgInPc2d1	společný
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
jevů	jev	k1gInPc2	jev
zpravidla	zpravidla	k6eAd1	zpravidla
vlastní	vlastní	k2eAgFnSc4d1	vlastní
symboliku	symbolika	k1gFnSc4	symbolika
i	i	k8xC	i
způsob	způsob	k1gInSc4	způsob
její	její	k3xOp3gFnSc2	její
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
další	další	k2eAgInPc4d1	další
astrologické	astrologický	k2eAgInPc4d1	astrologický
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
lišící	lišící	k2eAgMnSc1d1	lišící
jak	jak	k8xC	jak
ve	v	k7c6	v
východiscích	východisko	k1gNnPc6	východisko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
cílech	cíl	k1gInPc6	cíl
svého	svůj	k3xOyFgNnSc2	svůj
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Babylónská	babylónský	k2eAgFnSc1d1	Babylónská
astrologie	astrologie	k1gFnSc1	astrologie
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
astrologie	astrologie	k1gFnSc2	astrologie
Helénská	helénský	k2eAgFnSc1d1	helénská
astrologie	astrologie	k1gFnSc1	astrologie
Perská	perský	k2eAgFnSc1d1	perská
astrologie	astrologie	k1gFnSc2	astrologie
Arabská	arabský	k2eAgFnSc1d1	arabská
astrologie	astrologie	k1gFnSc1	astrologie
Védská	védský	k2eAgFnSc1d1	védská
astrologie	astrologie	k1gFnSc2	astrologie
Čínská	čínský	k2eAgFnSc1d1	čínská
astrologie	astrologie	k1gFnSc1	astrologie
Mayská	mayský	k2eAgFnSc1d1	mayská
astrologie	astrologie	k1gFnSc2	astrologie
Aztécká	aztécký	k2eAgFnSc1d1	aztécká
astrologie	astrologie	k1gFnSc1	astrologie
Západní	západní	k2eAgFnSc2d1	západní
astrologie	astrologie	k1gFnSc2	astrologie
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Védská	védský	k2eAgFnSc1d1	védská
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkomplexnějších	komplexní	k2eAgInPc2d3	nejkomplexnější
a	a	k8xC	a
nejpřesnějších	přesný	k2eAgInPc2d3	nejpřesnější
astrologických	astrologický	k2eAgInPc2d1	astrologický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
disciplíny	disciplína	k1gFnPc1	disciplína
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xC	jako
gola	gol	k2eAgFnSc1d1	gola
(	(	kIx(	(
<g/>
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ganita	ganita	k1gFnSc1	ganita
(	(	kIx(	(
<g/>
výpočty	výpočet	k1gInPc1	výpočet
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
džátaka	džátak	k1gMnSc4	džátak
(	(	kIx(	(
<g/>
nativní	nativní	k2eAgFnSc2d1	nativní
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muhúrta	muhúrta	k1gFnSc1	muhúrta
(	(	kIx(	(
<g/>
vybírání	vybírání	k1gNnSc1	vybírání
vhodného	vhodný	k2eAgInSc2d1	vhodný
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prašna	prašno	k1gNnSc2	prašno
(	(	kIx(	(
<g/>
zodpovídání	zodpovídání	k1gNnSc1	zodpovídání
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
otázky	otázka	k1gFnSc2	otázka
<g/>
)	)	kIx)	)
a	a	k8xC	a
nimitta	nimitta	k1gFnSc1	nimitta
(	(	kIx(	(
<g/>
výklad	výklad	k1gInSc1	výklad
specifických	specifický	k2eAgNnPc2d1	specifické
znamení	znamení	k1gNnPc2	znamení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
starobylé	starobylý	k2eAgFnSc6d1	starobylá
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
astrology	astrolog	k1gMnPc4	astrolog
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
používána	používán	k2eAgFnSc1d1	používána
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
pojetí	pojetí	k1gNnSc6	pojetí
guru-žák	guru-žák	k1gMnSc1	guru-žák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgFnPc2d3	nejuznávanější
autorit	autorita	k1gFnPc2	autorita
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
mudrc	mudrc	k1gMnSc1	mudrc
Parášara	Parášara	k1gFnSc1	Parášara
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
klasického	klasický	k2eAgNnSc2d1	klasické
díla	dílo	k1gNnSc2	dílo
Parášara	Parášara	k1gFnSc1	Parášara
Hora	hora	k1gFnSc1	hora
Šástra	šástra	k1gFnSc1	šástra
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
siderický	siderický	k2eAgInSc4d1	siderický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
tropický	tropický	k2eAgInSc4d1	tropický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Babylónská	babylónský	k2eAgFnSc1d1	Babylónská
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Astrologie	astrologie	k1gFnSc1	astrologie
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
civilizacích	civilizace	k1gFnPc6	civilizace
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zprvu	zprvu	k6eAd1	zprvu
tvořila	tvořit	k5eAaImAgFnS	tvořit
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
náboženských	náboženský	k2eAgInPc2d1	náboženský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Babylónský	babylónský	k2eAgInSc1d1	babylónský
astrologický	astrologický	k2eAgInSc1d1	astrologický
systém	systém	k1gInSc1	systém
pracoval	pracovat	k5eAaImAgInS	pracovat
s	s	k7c7	s
pěti	pět	k4xCc7	pět
postavami	postava	k1gFnPc7	postava
babylónských	babylónský	k2eAgMnPc2d1	babylónský
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
Marduk	Marduk	k1gMnSc1	Marduk
<g/>
,	,	kIx,	,
Ištar	Ištar	k1gMnSc1	Ištar
<g/>
,	,	kIx,	,
Ninibe	Ninib	k1gMnSc5	Ninib
<g/>
,	,	kIx,	,
Nabu	Nabum	k1gNnSc6	Nabum
<g/>
,	,	kIx,	,
Nergal	Nergal	k1gMnSc1	Nergal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
božstva	božstvo	k1gNnPc1	božstvo
byla	být	k5eAaImAgNnP	být
později	pozdě	k6eAd2	pozdě
identifikována	identifikovat	k5eAaBmNgFnS	identifikovat
s	s	k7c7	s
helénským	helénský	k2eAgInSc7d1	helénský
pantheonem	pantheon	k1gInSc7	pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
zlomky	zlomek	k1gInPc1	zlomek
babylónských	babylónský	k2eAgMnPc2d1	babylónský
nábožensko-věšteckých	náboženskoěštecký	k2eAgMnPc2d1	nábožensko-věštecký
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgNnPc2d1	zvané
omen	omen	k1gNnPc2	omen
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
znamení	znamení	k1gNnSc1	znamení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnohé	mnohé	k1gNnSc1	mnohé
mají	mít	k5eAaImIp3nP	mít
astrologické	astrologický	k2eAgInPc1d1	astrologický
náměty	námět	k1gInPc1	námět
(	(	kIx(	(
<g/>
jiné	jiná	k1gFnPc1	jiná
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
zabývají	zabývat	k5eAaImIp3nP	zabývat
např.	např.	kA	např.
i	i	k8xC	i
věštěním	věštění	k1gNnSc7	věštění
z	z	k7c2	z
vnitřností	vnitřnost	k1gFnPc2	vnitřnost
obětních	obětní	k2eAgNnPc2d1	obětní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Babylónští	babylónský	k2eAgMnPc1d1	babylónský
kněží-astrologové	kněžístrolog	k1gMnPc1	kněží-astrolog
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
roční	roční	k2eAgFnSc4d1	roční
pouť	pouť	k1gFnSc4	pouť
Slunce	slunce	k1gNnSc2	slunce
zvěrokruhem	zvěrokruh	k1gInSc7	zvěrokruh
mezi	mezi	k7c4	mezi
dvanáct	dvanáct	k4xCc4	dvanáct
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
či	či	k8xC	či
spíše	spíše	k9	spíše
astrologická	astrologický	k2eAgNnPc4d1	astrologické
znamení	znamení	k1gNnPc4	znamení
po	po	k7c6	po
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Babylónským	babylónský	k2eAgMnPc3d1	babylónský
astrologům	astrolog	k1gMnPc3	astrolog
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
také	také	k9	také
sestavení	sestavení	k1gNnSc4	sestavení
prvních	první	k4xOgInPc2	první
horoskopů	horoskop	k1gInPc2	horoskop
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
horoskop	horoskop	k1gInSc1	horoskop
je	být	k5eAaImIp3nS	být
diagram	diagram	k1gInSc4	diagram
z	z	k7c2	z
r.	r.	kA	r.
410	[number]	k4	410
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
horoskop	horoskop	k1gInSc4	horoskop
syna	syn	k1gMnSc2	syn
babylónského	babylónský	k2eAgMnSc2d1	babylónský
panovníka	panovník	k1gMnSc2	panovník
Šuma	šuma	k1gFnSc1	šuma
Usa	Usa	k1gMnSc1	Usa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
410	[number]	k4	410
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Také	také	k9	také
astrologická	astrologický	k2eAgNnPc4d1	astrologické
pozorování	pozorování	k1gNnPc4	pozorování
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
jako	jako	k9	jako
nedílná	dílný	k2eNgFnSc1d1	nedílná
součást	součást	k1gFnSc1	součást
náboženských	náboženský	k2eAgNnPc2d1	náboženské
mystérií	mystérium	k1gNnPc2	mystérium
pěstovaných	pěstovaný	k2eAgNnPc2d1	pěstované
v	v	k7c6	v
chrámových	chrámový	k2eAgNnPc6d1	chrámové
centrech	centrum	k1gNnPc6	centrum
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
indicie	indicie	k1gFnPc1	indicie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
interpretace	interpretace	k1gFnSc1	interpretace
značek	značka	k1gFnPc2	značka
na	na	k7c6	na
stropní	stropní	k2eAgFnSc6d1	stropní
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
mapě	mapa	k1gFnSc6	mapa
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Dendeře	Dendera	k1gFnSc6	Dendera
<g/>
)	)	kIx)	)
svědčí	svědčit	k5eAaImIp3nS	svědčit
dokonce	dokonce	k9	dokonce
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
egyptská	egyptský	k2eAgFnSc1d1	egyptská
tradice	tradice	k1gFnSc1	tradice
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
starší	starý	k2eAgMnPc4d2	starší
než	než	k8xS	než
tradice	tradice	k1gFnSc1	tradice
babylónská	babylónský	k2eAgFnSc1d1	Babylónská
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
sahat	sahat	k5eAaImF	sahat
až	až	k9	až
před	před	k7c7	před
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
astrologii	astrologie	k1gFnSc6	astrologie
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
nečeru	črat	k5eNaPmIp1nS	črat
(	(	kIx(	(
<g/>
egyptskými	egyptský	k2eAgMnPc7d1	egyptský
"	"	kIx"	"
<g/>
bohy	bůh	k1gMnPc7	bůh
<g/>
"	"	kIx"	"
se	s	k7c7	s
zoomorfními	zoomorfní	k2eAgInPc7d1	zoomorfní
rysy	rys	k1gInPc7	rys
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohá	mnohý	k2eAgNnPc4d1	mnohé
astrologická	astrologický	k2eAgNnPc4d1	astrologické
pozorování	pozorování	k1gNnPc4	pozorování
byla	být	k5eAaImAgFnS	být
zakódována	zakódován	k2eAgFnSc1d1	zakódována
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
veřejně	veřejně	k6eAd1	veřejně
šířených	šířený	k2eAgInPc2d1	šířený
náboženských	náboženský	k2eAgInPc2d1	náboženský
mýtů	mýtus	k1gInPc2	mýtus
předávaných	předávaný	k2eAgFnPc2d1	předávaná
ústní	ústní	k2eAgFnSc7d1	ústní
tradicí	tradice	k1gFnSc7	tradice
i	i	k8xC	i
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
astrologická	astrologický	k2eAgNnPc1d1	astrologické
znamení	znamení	k1gNnPc1	znamení
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
podobu	podoba	k1gFnSc4	podoba
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
astrologii	astrologie	k1gFnSc4	astrologie
také	také	k9	také
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
a	a	k8xC	a
stavitelství	stavitelství	k1gNnSc6	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
starověké	starověký	k2eAgFnPc1d1	starověká
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
jsou	být	k5eAaImIp3nP	být
orientovány	orientovat	k5eAaBmNgInP	orientovat
podle	podle	k7c2	podle
soudobého	soudobý	k2eAgNnSc2d1	soudobé
postavení	postavení	k1gNnSc2	postavení
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
sloužily	sloužit	k5eAaImAgInP	sloužit
často	často	k6eAd1	často
také	také	k9	také
jako	jako	k9	jako
astronomické	astronomický	k2eAgFnPc4d1	astronomická
pozorovatelny	pozorovatelna	k1gFnPc4	pozorovatelna
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
rozměrech	rozměr	k1gInPc6	rozměr
jsou	být	k5eAaImIp3nP	být
rozpoznávány	rozpoznáván	k2eAgInPc1d1	rozpoznáván
astrologické	astrologický	k2eAgInPc1d1	astrologický
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Helénská	helénský	k2eAgFnSc1d1	helénská
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Helénská	helénský	k2eAgFnSc1d1	helénská
astrologie	astrologie	k1gFnSc1	astrologie
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
egyptské	egyptský	k2eAgFnSc6d1	egyptská
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
částečně	částečně	k6eAd1	částečně
absorbovala	absorbovat	k5eAaBmAgFnS	absorbovat
a	a	k8xC	a
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
i	i	k8xC	i
zdroje	zdroj	k1gInPc4	zdroj
babylónské	babylónský	k2eAgFnSc2d1	Babylónská
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
astrologie	astrologie	k1gFnSc2	astrologie
chaldejské	chaldejský	k2eAgFnSc2d1	Chaldejská
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
helénský	helénský	k2eAgInSc1d1	helénský
spis	spis	k1gInSc1	spis
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Corpus	corpus	k1gInSc1	corpus
Hermeticum	Hermeticum	k1gNnSc1	Hermeticum
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
tradici	tradice	k1gFnSc4	tradice
přímo	přímo	k6eAd1	přímo
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
řeckých	řecký	k2eAgMnPc2d1	řecký
filosofů	filosof	k1gMnPc2	filosof
a	a	k8xC	a
astrologů	astrolog	k1gMnPc2	astrolog
studovalo	studovat	k5eAaImAgNnS	studovat
v	v	k7c6	v
egyptských	egyptský	k2eAgInPc6d1	egyptský
chrámech	chrám	k1gInPc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
helénskou	helénský	k2eAgFnSc4d1	helénská
astrologii	astrologie	k1gFnSc4	astrologie
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
astrologické	astrologický	k2eAgFnPc4d1	astrologická
nauky	nauka	k1gFnPc4	nauka
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnSc6	středomoří
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
rokem	rok	k1gInSc7	rok
500	[number]	k4	500
n.	n.	k?	n.
l.	l.	k?	l.
Astrologie	astrologie	k1gFnSc1	astrologie
nicméně	nicméně	k8xC	nicméně
pronikala	pronikat	k5eAaImAgFnS	pronikat
do	do	k7c2	do
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
básník	básník	k1gMnSc1	básník
Hesiodos	Hesiodos	k1gMnSc1	Hesiodos
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
didaktické	didaktický	k2eAgFnSc6d1	didaktická
básni	báseň	k1gFnSc6	báseň
Práce	práce	k1gFnSc1	práce
a	a	k8xC	a
dni	den	k1gInPc1	den
pomocí	pomocí	k7c2	pomocí
postavení	postavení	k1gNnSc2	postavení
hvězd	hvězda	k1gFnPc2	hvězda
podává	podávat	k5eAaImIp3nS	podávat
termíny	termín	k1gInPc4	termín
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
také	také	k9	také
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
dobu	doba	k1gFnSc4	doba
k	k	k7c3	k
početí	početí	k1gNnSc3	početí
syna	syn	k1gMnSc2	syn
či	či	k8xC	či
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
názory	názor	k1gInPc1	názor
šly	jít	k5eAaImAgInP	jít
ještě	ještě	k9	ještě
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
hlásaly	hlásat	k5eAaImAgFnP	hlásat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavení	postavení	k1gNnSc1	postavení
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
zvířetníkových	zvířetníkový	k2eAgNnPc6d1	zvířetníkové
souhvězdích	souhvězdí	k1gNnPc6	souhvězdí
při	při	k7c6	při
zrození	zrození	k1gNnSc6	zrození
člověka	člověk	k1gMnSc2	člověk
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
celý	celý	k2eAgInSc4d1	celý
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
náhle	náhle	k6eAd1	náhle
prosadil	prosadit	k5eAaPmAgInS	prosadit
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
tvorby	tvorba	k1gFnSc2	tvorba
horoskopů	horoskop	k1gInPc2	horoskop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
původní	původní	k2eAgInSc1d1	původní
systém	systém	k1gInSc1	systém
babylónský	babylónský	k2eAgInSc1d1	babylónský
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
náhle	náhle	k6eAd1	náhle
a	a	k8xC	a
ustálil	ustálit	k5eAaPmAgMnS	ustálit
se	se	k3xPyFc4	se
během	během	k7c2	během
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
závěrům	závěr	k1gInPc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dílem	dílo	k1gNnSc7	dílo
jedince	jedinko	k6eAd1	jedinko
(	(	kIx(	(
<g/>
za	za	k7c2	za
něhož	jenž	k3xRgNnSc2	jenž
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
legendárního	legendární	k2eAgMnSc4d1	legendární
Herma	Hermes	k1gMnSc4	Hermes
Trismegista	Trismegist	k1gMnSc4	Trismegist
<g/>
)	)	kIx)	)
či	či	k8xC	či
neznámé	známý	k2eNgFnSc2d1	neznámá
malé	malý	k2eAgFnSc2d1	malá
astrologické	astrologický	k2eAgFnSc2d1	astrologická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
ustálil	ustálit	k5eAaPmAgInS	ustálit
sestavování	sestavování	k1gNnSc4	sestavování
horoskopů	horoskop	k1gInPc2	horoskop
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
zavedla	zavést	k5eAaPmAgFnS	zavést
systém	systém	k1gInSc4	systém
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
aspekty	aspekt	k1gInPc4	aspekt
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
sedm	sedm	k4xCc4	sedm
klasických	klasický	k2eAgFnPc2d1	klasická
astrologických	astrologický	k2eAgFnPc2d1	astrologická
planet	planeta	k1gFnPc2	planeta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
V	v	k7c6	v
helénském	helénský	k2eAgNnSc6d1	helénské
období	období	k1gNnSc6	období
astrologie	astrologie	k1gFnSc2	astrologie
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
astrologická	astrologický	k2eAgFnSc1d1	astrologická
terminologie	terminologie	k1gFnSc1	terminologie
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
stěžejní	stěžejní	k2eAgNnPc1d1	stěžejní
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
čerpala	čerpat	k5eAaImAgFnS	čerpat
pozdější	pozdní	k2eAgFnSc1d2	pozdější
arabská	arabský	k2eAgFnSc1d1	arabská
astrologie	astrologie	k1gFnSc1	astrologie
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc1	překlad
řeckých	řecký	k2eAgInPc2d1	řecký
textů	text	k1gInPc2	text
do	do	k7c2	do
sanskrtu	sanskrt	k1gInSc2	sanskrt
dokonce	dokonce	k9	dokonce
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
terminologii	terminologie	k1gFnSc4	terminologie
staré	starý	k2eAgFnSc2d1	stará
indické	indický	k2eAgFnSc2d1	indická
tradiční	tradiční	k2eAgFnSc2d1	tradiční
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
astrology	astrolog	k1gMnPc7	astrolog
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
zejména	zejména	k9	zejména
Klaudia	Klaudium	k1gNnSc2	Klaudium
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
"	"	kIx"	"
<g/>
Čtyř	čtyři	k4xCgNnPc2	čtyři
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
astrologii	astrologie	k1gFnSc4	astrologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tetrabiblos	Tetrabiblos	k1gInSc1	Tetrabiblos
<g/>
)	)	kIx)	)
a	a	k8xC	a
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
katalogu	katalog	k1gInSc2	katalog
Almagest	Almagest	k1gFnSc1	Almagest
<g/>
,	,	kIx,	,
či	či	k8xC	či
Héfaistióna	Héfaistióna	k1gFnSc1	Héfaistióna
Thébského	thébský	k2eAgInSc2d1	thébský
a	a	k8xC	a
Vettia	Vettius	k1gMnSc2	Vettius
Valense	Valens	k1gMnSc2	Valens
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
významné	významný	k2eAgFnSc2d1	významná
Antologie	antologie	k1gFnSc2	antologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Západní	západní	k2eAgFnSc2d1	západní
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
postupně	postupně	k6eAd1	postupně
astrologické	astrologický	k2eAgFnSc2d1	astrologická
znalosti	znalost	k1gFnSc2	znalost
helénského	helénský	k2eAgNnSc2d1	helénské
období	období	k1gNnSc2	období
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
vymizely	vymizet	k5eAaPmAgFnP	vymizet
a	a	k8xC	a
zachovávaly	zachovávat	k5eAaImAgFnP	zachovávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc1	některý
astrologické	astrologický	k2eAgInPc1d1	astrologický
prvky	prvek	k1gInPc1	prvek
pohanských	pohanský	k2eAgInPc2d1	pohanský
náboženských	náboženský	k2eAgInPc2d1	náboženský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Starý	starý	k2eAgInSc4d1	starý
kontinent	kontinent	k1gInSc4	kontinent
se	se	k3xPyFc4	se
astrologie	astrologie	k1gFnSc1	astrologie
začala	začít	k5eAaPmAgFnS	začít
vracet	vracet	k5eAaImF	vracet
až	až	k9	až
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
arabské	arabský	k2eAgFnSc2d1	arabská
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
působením	působení	k1gNnSc7	působení
arabských	arabský	k2eAgFnPc2d1	arabská
univerzit	univerzita	k1gFnPc2	univerzita
na	na	k7c4	na
Araby	Arab	k1gMnPc4	Arab
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
územích	území	k1gNnPc6	území
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
musela	muset	k5eAaImAgFnS	muset
astrologie	astrologie	k1gFnSc1	astrologie
kvůli	kvůli	k7c3	kvůli
silným	silný	k2eAgInPc3d1	silný
arabským	arabský	k2eAgInPc3d1	arabský
vlivům	vliv	k1gInPc3	vliv
nejprve	nejprve	k6eAd1	nejprve
vyřešit	vyřešit	k5eAaPmF	vyřešit
spor	spor	k1gInSc4	spor
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
uznali	uznat	k5eAaPmAgMnP	uznat
především	především	k9	především
dva	dva	k4xCgMnPc1	dva
významní	významný	k2eAgMnPc1d1	významný
teologové	teolog	k1gMnPc1	teolog
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
-	-	kIx~	-
Albertus	Albertus	k1gMnSc1	Albertus
Magnus	Magnus	k1gMnSc1	Magnus
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Boloni	Boloňa	k1gFnSc6	Boloňa
přitom	přitom	k6eAd1	přitom
existovala	existovat	k5eAaImAgFnS	existovat
od	od	k7c2	od
r.	r.	kA	r.
1125	[number]	k4	1125
významná	významný	k2eAgFnSc1d1	významná
astrologická	astrologický	k2eAgFnSc1d1	astrologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Velkého	velký	k2eAgInSc2d1	velký
rozvoje	rozvoj	k1gInSc2	rozvoj
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
astrologie	astrologie	k1gFnSc1	astrologie
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Středověkou	středověký	k2eAgFnSc4d1	středověká
astrologii	astrologie	k1gFnSc4	astrologie
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
zprostředkovaných	zprostředkovaný	k2eAgInPc6d1	zprostředkovaný
překladech	překlad	k1gInPc6	překlad
klasických	klasický	k2eAgInPc2d1	klasický
helénských	helénský	k2eAgInPc2d1	helénský
astrologických	astrologický	k2eAgInPc2d1	astrologický
spisů	spis	k1gInPc2	spis
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obohatily	obohatit	k5eAaPmAgInP	obohatit
přímé	přímý	k2eAgInPc1d1	přímý
překlady	překlad	k1gInPc1	překlad
ze	z	k7c2	z
znovuobjevených	znovuobjevený	k2eAgInPc2d1	znovuobjevený
řeckých	řecký	k2eAgInPc2d1	řecký
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Astrologií	astrologie	k1gFnPc2	astrologie
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
i	i	k9	i
několik	několik	k4yIc1	několik
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
astrologii	astrologie	k1gFnSc4	astrologie
opět	opět	k6eAd1	opět
potírala	potírat	k5eAaImAgFnS	potírat
a	a	k8xC	a
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
osobnostem	osobnost	k1gFnPc3	osobnost
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
astrologií	astrologie	k1gFnSc7	astrologie
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
francouzského	francouzský	k2eAgMnSc4d1	francouzský
astrologa	astrolog	k1gMnSc4	astrolog
Michela	Michel	k1gMnSc4	Michel
Nostradama	Nostradam	k1gMnSc4	Nostradam
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
známá	známý	k2eAgNnPc1d1	známé
zakódovaná	zakódovaný	k2eAgNnPc1d1	zakódované
proroctví	proroctví	k1gNnPc1	proroctví
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Astrologii	astrologie	k1gFnSc3	astrologie
se	se	k3xPyFc4	se
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
i	i	k8xC	i
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
výsledky	výsledek	k1gInPc7	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
z	z	k7c2	z
astrologie	astrologie	k1gFnSc2	astrologie
vyčleňovat	vyčleňovat	k5eAaImF	vyčleňovat
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgInPc7d1	významný
a	a	k8xC	a
úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
astrology	astrolog	k1gMnPc4	astrolog
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
astrologických	astrologický	k2eAgInPc2d1	astrologický
výsledků	výsledek	k1gInPc2	výsledek
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc6	Galile
či	či	k8xC	či
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
působící	působící	k2eAgFnSc2d1	působící
Tycho	Tyc	k1gMnSc2	Tyc
de	de	k?	de
Brahe	Brah	k1gMnSc2	Brah
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
získávali	získávat	k5eAaImAgMnP	získávat
astrologové	astrolog	k1gMnPc1	astrolog
i	i	k8xC	i
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mocné	mocný	k2eAgMnPc4d1	mocný
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
pověrčivý	pověrčivý	k2eAgMnSc1d1	pověrčivý
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
dával	dávat	k5eAaImAgMnS	dávat
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
svého	svůj	k3xOyFgMnSc2	svůj
dvorního	dvorní	k2eAgMnSc2d1	dvorní
astrologa	astrolog	k1gMnSc2	astrolog
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Seniho	Seni	k1gMnSc2	Seni
<g/>
.	.	kIx.	.
</s>
<s>
Astrologii	astrologie	k1gFnSc4	astrologie
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ještě	ještě	k9	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
fyzik	fyzika	k1gFnPc2	fyzika
Isaac	Isaac	k1gFnSc1	Isaac
Newton	newton	k1gInSc1	newton
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
udržovalo	udržovat	k5eAaImAgNnS	udržovat
západní	západní	k2eAgFnSc4d1	západní
astrologickou	astrologický	k2eAgFnSc4d1	astrologická
tradici	tradice	k1gFnSc4	tradice
především	především	k9	především
hnutí	hnutí	k1gNnPc1	hnutí
rosekruciánů	rosekrucián	k1gMnPc2	rosekrucián
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
úlohu	úloha	k1gFnSc4	úloha
udržovatele	udržovatel	k1gMnSc2	udržovatel
a	a	k8xC	a
pokračovatele	pokračovatel	k1gMnSc2	pokračovatel
astrologických	astrologický	k2eAgInPc2d1	astrologický
výzkumů	výzkum	k1gInPc2	výzkum
později	pozdě	k6eAd2	pozdě
převzalo	převzít	k5eAaPmAgNnS	převzít
svobodné	svobodný	k2eAgNnSc4d1	svobodné
zednářství	zednářství	k1gNnSc4	zednářství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Značné	značný	k2eAgFnSc2d1	značná
obliby	obliba	k1gFnSc2	obliba
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Západní	západní	k2eAgFnSc1d1	západní
astrologie	astrologie	k1gFnSc1	astrologie
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
opustila	opustit	k5eAaPmAgFnS	opustit
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
společnosti	společnost	k1gFnPc4	společnost
zednářských	zednářský	k2eAgFnPc2d1	zednářská
lóží	lóže	k1gFnPc2	lóže
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velký	velký	k2eAgInSc1d1	velký
rozruch	rozruch	k1gInSc1	rozruch
do	do	k7c2	do
astrologického	astrologický	k2eAgNnSc2d1	astrologické
zkoumání	zkoumání	k1gNnSc2	zkoumání
vnesl	vnést	k5eAaPmAgMnS	vnést
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
tradiční	tradiční	k2eAgInSc1d1	tradiční
astrologický	astrologický	k2eAgInSc1d1	astrologický
aparát	aparát	k1gInSc1	aparát
propojit	propojit	k5eAaPmF	propojit
s	s	k7c7	s
psychologickými	psychologický	k2eAgInPc7d1	psychologický
fenomény	fenomén	k1gInPc7	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
řada	řada	k1gFnSc1	řada
astrologů	astrolog	k1gMnPc2	astrolog
<g/>
,	,	kIx,	,
pracujících	pracující	k1gMnPc2	pracující
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
postupy	postup	k1gInPc7	postup
ptolmajovské	ptolmajovský	k2eAgFnSc2d1	ptolmajovský
astrologie	astrologie	k1gFnSc2	astrologie
jeho	jeho	k3xOp3gFnSc2	jeho
závěry	závěra	k1gFnSc2	závěra
odmítá	odmítat	k5eAaImIp3nS	odmítat
jako	jako	k9	jako
dezinterpretaci	dezinterpretace	k1gFnSc3	dezinterpretace
staré	starý	k2eAgFnPc4d1	stará
tradiční	tradiční	k2eAgFnPc4d1	tradiční
nauky	nauka	k1gFnPc4	nauka
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
psychologicko-astrologický	psychologickostrologický	k2eAgInSc4d1	psychologicko-astrologický
výzkum	výzkum	k1gInSc4	výzkum
jak	jak	k8xS	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
psychologů	psycholog	k1gMnPc2	psycholog
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
astrologů	astrolog	k1gMnPc2	astrolog
<g/>
.	.	kIx.	.
</s>
<s>
Bádání	bádání	k1gNnSc1	bádání
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
psychologie	psychologie	k1gFnSc2	psychologie
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
astrologů	astrolog	k1gMnPc2	astrolog
opustila	opustit	k5eAaPmAgFnS	opustit
tradiční	tradiční	k2eAgNnPc4d1	tradiční
astrologická	astrologický	k2eAgNnPc4d1	astrologické
východiska	východisko	k1gNnPc4	východisko
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
psychologizujících	psychologizující	k2eAgFnPc2d1	psychologizující
astrologických	astrologický	k2eAgFnPc2d1	astrologická
teorií	teorie	k1gFnPc2	teorie
revidující	revidující	k2eAgFnSc2d1	revidující
některá	některý	k3yIgNnPc1	některý
stará	starý	k2eAgNnPc1d1	staré
pojetí	pojetí	k1gNnPc1	pojetí
(	(	kIx(	(
<g/>
Hamburská	hamburský	k2eAgFnSc1d1	hamburská
astrologická	astrologický	k2eAgFnSc1d1	astrologická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Humanistická	humanistický	k2eAgFnSc1d1	humanistická
a	a	k8xC	a
transpersonální	transpersonální	k2eAgFnSc1d1	transpersonální
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
Kosmobiologie	Kosmobiologie	k1gFnSc1	Kosmobiologie
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Astrologie	astrologie	k1gFnSc1	astrologie
prožila	prožít	k5eAaPmAgFnS	prožít
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
století	století	k1gNnSc2	století
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
zájmu	zájem	k1gInSc2	zájem
laické	laický	k2eAgFnSc3d1	laická
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
masových	masový	k2eAgInPc2d1	masový
sdělovacích	sdělovací	k2eAgInPc2d1	sdělovací
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
internet	internet	k1gInSc1	internet
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k8xC	i
rozvoji	rozvoj	k1gInSc3	rozvoj
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
sestavit	sestavit	k5eAaPmF	sestavit
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
horoskop	horoskop	k1gInSc1	horoskop
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
astrologie	astrologie	k1gFnPc1	astrologie
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
silné	silný	k2eAgFnSc3d1	silná
zednářské	zednářský	k2eAgFnSc3d1	zednářská
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
pseudorosekruciánské	pseudorosekruciánský	k2eAgFnSc3d1	pseudorosekruciánský
esoterické	esoterický	k2eAgFnSc3d1	esoterická
tradici	tradice	k1gFnSc3	tradice
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
také	také	k9	také
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
astrologický	astrologický	k2eAgInSc1d1	astrologický
boom	boom	k1gInSc1	boom
<g/>
"	"	kIx"	"
také	také	k9	také
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
hnutí	hnutí	k1gNnSc1	hnutí
New	New	k1gFnSc2	New
Age	Age	k1gFnPc2	Age
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
starou	starý	k2eAgFnSc4d1	stará
západní	západní	k2eAgFnSc4d1	západní
tradici	tradice	k1gFnSc4	tradice
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
násilně	násilně	k6eAd1	násilně
a	a	k8xC	a
protichůdně	protichůdně	k6eAd1	protichůdně
<g/>
)	)	kIx)	)
propojilo	propojit	k5eAaPmAgNnS	propojit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
orientálními	orientální	k2eAgInPc7d1	orientální
<g/>
,	,	kIx,	,
indiánskými	indiánský	k2eAgInPc7d1	indiánský
a	a	k8xC	a
pohanskými	pohanský	k2eAgInPc7d1	pohanský
astrologickými	astrologický	k2eAgInPc7d1	astrologický
systémy	systém	k1gInPc7	systém
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
poněkud	poněkud	k6eAd1	poněkud
nepřehlednou	přehledný	k2eNgFnSc4d1	nepřehledná
změť	změť	k1gFnSc4	změť
astrologického	astrologický	k2eAgInSc2d1	astrologický
symbolismu	symbolismus	k1gInSc2	symbolismus
a	a	k8xC	a
interpretačních	interpretační	k2eAgFnPc2d1	interpretační
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velký	velký	k2eAgInSc1d1	velký
nárůst	nárůst	k1gInSc1	nárůst
zájmu	zájem	k1gInSc2	zájem
o	o	k7c6	o
astrologii	astrologie	k1gFnSc6	astrologie
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
profanaci	profanace	k1gFnSc3	profanace
a	a	k8xC	a
častému	častý	k2eAgNnSc3d1	časté
mediálnímu	mediální	k2eAgNnSc3d1	mediální
zbulvarizování	zbulvarizování	k1gNnSc3	zbulvarizování
původních	původní	k2eAgFnPc2d1	původní
astrologických	astrologický	k2eAgFnPc2d1	astrologická
nauk	nauka	k1gFnPc2	nauka
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
Oblasti	oblast	k1gFnPc1	oblast
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
astrologická	astrologický	k2eAgFnSc1d1	astrologická
tradice	tradice	k1gFnSc1	tradice
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
pronikat	pronikat	k5eAaImF	pronikat
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Jistého	jistý	k2eAgNnSc2d1	jisté
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
uznání	uznání	k1gNnSc2	uznání
se	se	k3xPyFc4	se
astrologii	astrologie	k1gFnSc3	astrologie
dostalo	dostat	k5eAaPmAgNnS	dostat
zejména	zejména	k9	zejména
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
lucemburských	lucemburský	k2eAgMnPc2d1	lucemburský
císařů	císař	k1gMnPc2	císař
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
následníka	následník	k1gMnSc2	následník
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Astrologie	astrologie	k1gFnSc1	astrologie
se	se	k3xPyFc4	se
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc6d1	založená
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
astrologii	astrologie	k1gFnSc6	astrologie
se	se	k3xPyFc4	se
prokazatelně	prokazatelně	k6eAd1	prokazatelně
věnoval	věnovat	k5eAaImAgMnS	věnovat
její	její	k3xOp3gMnSc1	její
druhý	druhý	k4xOgMnSc1	druhý
rektor	rektor	k1gMnSc1	rektor
Jan	Jan	k1gMnSc1	Jan
Šindel	šindel	k1gInSc1	šindel
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
astrolábu	astroláb	k1gInSc2	astroláb
Staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Astrologické	astrologický	k2eAgInPc1d1	astrologický
poznatky	poznatek	k1gInPc1	poznatek
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
odrážely	odrážet	k5eAaImAgInP	odrážet
i	i	k9	i
v	v	k7c6	v
architektonické	architektonický	k2eAgFnSc6d1	architektonická
výzdobě	výzdoba	k1gFnSc6	výzdoba
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
gotických	gotický	k2eAgFnPc2d1	gotická
staveb	stavba	k1gFnPc2	stavba
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
knihovna	knihovna	k1gFnSc1	knihovna
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
několik	několik	k4yIc4	několik
velmi	velmi	k6eAd1	velmi
významných	významný	k2eAgInPc2d1	významný
astrologických	astrologický	k2eAgInPc2d1	astrologický
spisů	spis	k1gInPc2	spis
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
Alfonsinských	Alfonsinský	k2eAgFnPc2d1	Alfonsinský
tabulek	tabulka	k1gFnPc2	tabulka
a	a	k8xC	a
Ptolemaiova	Ptolemaiův	k2eAgInSc2d1	Ptolemaiův
spisu	spis	k1gInSc2	spis
Tetrabiblos	Tetrabiblosa	k1gFnPc2	Tetrabiblosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
zachovaly	zachovat	k5eAaPmAgInP	zachovat
tři	tři	k4xCgInPc1	tři
sborníky	sborník	k1gInPc1	sborník
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgNnSc2d1	další
období	období	k1gNnSc2	období
velkého	velký	k2eAgInSc2d1	velký
rozmachu	rozmach	k1gInSc2	rozmach
se	se	k3xPyFc4	se
astrologie	astrologie	k1gFnSc1	astrologie
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
dočkala	dočkat	k5eAaPmAgFnS	dočkat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vyhlášeného	vyhlášený	k2eAgMnSc4d1	vyhlášený
mecenáše	mecenáš	k1gMnSc4	mecenáš
všech	všecek	k3xTgInPc2	všecek
provozovatelů	provozovatel	k1gMnPc2	provozovatel
okultních	okultní	k2eAgFnPc2d1	okultní
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
císařských	císařský	k2eAgMnPc2d1	císařský
astrologů	astrolog	k1gMnPc2	astrolog
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
u	u	k7c2	u
pražského	pražský	k2eAgInSc2d1	pražský
dvora	dvůr	k1gInSc2	dvůr
působili	působit	k5eAaImAgMnP	působit
i	i	k9	i
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brah	k1gMnSc4	Brah
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
působili	působit	k5eAaImAgMnP	působit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
proslulí	proslulý	k2eAgMnPc1d1	proslulý
učenci	učenec	k1gMnPc1	učenec
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
astrologií	astrologie	k1gFnSc7	astrologie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
John	John	k1gMnSc1	John
Dee	Dee	k1gMnSc1	Dee
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
okruhu	okruh	k1gInSc3	okruh
pražských	pražský	k2eAgMnPc2d1	pražský
rudolfinských	rudolfinský	k2eAgMnPc2d1	rudolfinský
zasvěcenců	zasvěcenec	k1gMnPc2	zasvěcenec
i	i	k8xC	i
krátké	krátký	k2eAgFnSc6d1	krátká
epizodě	epizoda	k1gFnSc6	epizoda
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc4d1	falcký
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
stopy	stop	k1gInPc4	stop
založení	založení	k1gNnSc2	založení
rosekruciánského	rosekruciánský	k2eAgNnSc2d1	rosekruciánský
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
astrologické	astrologický	k2eAgFnSc2d1	astrologická
tradice	tradice	k1gFnSc2	tradice
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postarali	postarat	k5eAaPmAgMnP	postarat
zejména	zejména	k9	zejména
členové	člen	k1gMnPc1	člen
prvorepublikového	prvorepublikový	k2eAgNnSc2d1	prvorepublikové
sdružení	sdružení	k1gNnSc2	sdružení
hermetiků	hermetik	k1gMnPc2	hermetik
Universalie	universalie	k1gFnPc1	universalie
<g/>
.	.	kIx.	.
</s>
<s>
Astrologií	astrologie	k1gFnSc7	astrologie
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
především	především	k9	především
Otakar	Otakar	k1gMnSc1	Otakar
Griese	Griese	k1gFnSc2	Griese
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Kefer	Kefer	k1gMnSc1	Kefer
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
astrologie	astrologie	k1gFnSc1	astrologie
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
potírána	potírán	k2eAgFnSc1d1	potírána
a	a	k8xC	a
pronásledována	pronásledován	k2eAgFnSc1d1	pronásledována
jako	jako	k8xS	jako
učení	učení	k1gNnSc1	učení
neslučující	slučující	k2eNgMnSc1d1	neslučující
se	se	k3xPyFc4	se
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
marxisticko-leninským	marxistickoeninský	k2eAgInSc7d1	marxisticko-leninský
světonázorem	světonázor	k1gInSc7	světonázor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
českou	český	k2eAgFnSc4d1	Česká
astrologii	astrologie	k1gFnSc4	astrologie
například	například	k6eAd1	například
Jindřich	Jindřich	k1gMnSc1	Jindřich
Šob	Šob	k1gMnSc1	Šob
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Schob	Schoba	k1gFnPc2	Schoba
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Šimandl	Šimandl	k1gMnSc1	Šimandl
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Vratislav	Vratislav	k1gMnSc1	Vratislav
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
–	–	k?	–
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
pokračovateli	pokračovatel	k1gMnPc7	pokračovatel
vyniká	vynikat	k5eAaImIp3nS	vynikat
Milan	Milan	k1gMnSc1	Milan
Špůrek	Špůrek	k1gMnSc1	Špůrek
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Astrologická	astrologický	k2eAgFnSc1d1	astrologická
veřejnost	veřejnost	k1gFnSc1	veřejnost
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
prvorepublikovou	prvorepublikový	k2eAgFnSc4d1	prvorepubliková
tradici	tradice	k1gFnSc4	tradice
založením	založení	k1gNnSc7	založení
Astrologické	astrologický	k2eAgFnSc2d1	astrologická
společnosti	společnost	k1gFnSc2	společnost
ČR	ČR	kA	ČR
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
v	v	k7c4	v
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Pavel	Pavel	k1gMnSc1	Pavel
Turnovský	turnovský	k2eAgMnSc1d1	turnovský
<g/>
.	.	kIx.	.
</s>
<s>
Meziválečnou	meziválečný	k2eAgFnSc4d1	meziválečná
generaci	generace	k1gFnSc4	generace
astrologů	astrolog	k1gMnPc2	astrolog
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
kolegy	kolega	k1gMnPc7	kolega
spojila	spojit	k5eAaPmAgNnP	spojit
osobnost	osobnost	k1gFnSc4	osobnost
Josefa	Josef	k1gMnSc2	Josef
Danzera	Danzer	k1gMnSc2	Danzer
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
člena	člen	k1gMnSc4	člen
Universalie	universalie	k1gFnPc4	universalie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
astrolog	astrolog	k1gMnSc1	astrolog
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Vanda	Vanda	k1gFnSc1	Vanda
Jindrová	Jindrová	k1gFnSc1	Jindrová
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kalfiřt	Kalfiřt	k1gMnSc1	Kalfiřt
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Kobal	Kobal	k1gMnSc1	Kobal
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Koudela	Koudela	k1gMnSc1	Koudela
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Špůrek	Špůrek	k1gMnSc1	Špůrek
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Štěpánová	Štěpánová	k1gFnSc1	Štěpánová
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Turnovský	turnovský	k2eAgMnSc1d1	turnovský
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vydává	vydávat	k5eAaImIp3nS	vydávat
revue	revue	k1gFnSc7	revue
Konstelace	konstelace	k1gFnSc2	konstelace
Astrologie	astrologie	k1gFnSc2	astrologie
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
základním	základní	k2eAgInPc3d1	základní
principům	princip	k1gInPc3	princip
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
Principy	princip	k1gInPc4	princip
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
)	)	kIx)	)
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
kontroverzí	kontroverze	k1gFnPc2	kontroverze
se	s	k7c7	s
zastánci	zastánce	k1gMnPc7	zastánce
jiného	jiný	k2eAgInSc2d1	jiný
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
kontextu	kontext	k1gInSc6	kontext
musela	muset	k5eAaImAgFnS	muset
evropská	evropský	k2eAgFnSc1d1	Evropská
tradiční	tradiční	k2eAgFnSc1d1	tradiční
astrologie	astrologie	k1gFnSc1	astrologie
nejprve	nejprve	k6eAd1	nejprve
řešit	řešit	k5eAaImF	řešit
spory	spor	k1gInPc4	spor
s	s	k7c7	s
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
teologií	teologie	k1gFnSc7	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
věštění	věštění	k1gNnSc4	věštění
či	či	k8xC	či
předpovídání	předpovídání	k1gNnSc4	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
odmítali	odmítat	k5eAaImAgMnP	odmítat
již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoliv	jakýkoliv	k3yIgFnPc1	jakýkoliv
formy	forma	k1gFnPc1	forma
věštění	věštění	k1gNnSc2	věštění
<g/>
,	,	kIx,	,
mrakopravectví	mrakopravectví	k1gNnSc2	mrakopravectví
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
okultních	okultní	k2eAgFnPc2d1	okultní
praktik	praktika	k1gFnPc2	praktika
jsou	být	k5eAaImIp3nP	být
odmítány	odmítat	k5eAaImNgInP	odmítat
již	již	k6eAd1	již
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
Dt	Dt	k1gFnSc1	Dt
18,10	[number]	k4	18,10
<g/>
;	;	kIx,	;
Jer	jer	k1gInSc1	jer
29,8	[number]	k4	29,8
<g/>
)	)	kIx)	)
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
církevních	církevní	k2eAgFnPc2d1	církevní
autorit	autorita	k1gFnPc2	autorita
–	–	k?	–
Augustinus	Augustinus	k1gMnSc1	Augustinus
(	(	kIx(	(
<g/>
354	[number]	k4	354
<g/>
–	–	k?	–
<g/>
430	[number]	k4	430
<g/>
)	)	kIx)	)
–	–	k?	–
astrologii	astrologie	k1gFnSc4	astrologie
důrazně	důrazně	k6eAd1	důrazně
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
astrology	astrolog	k1gMnPc4	astrolog
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c7	za
šarlatány	šarlatán	k1gMnPc7	šarlatán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
"	"	kIx"	"
<g/>
svou	svůj	k3xOyFgFnSc4	svůj
službu	služba	k1gFnSc4	služba
prodávají	prodávat	k5eAaImIp3nP	prodávat
nevědomým	vědomý	k2eNgMnPc3d1	nevědomý
lidem	člověk	k1gMnPc3	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
např.	např.	kA	např.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
pověrčivosti	pověrčivost	k1gFnSc2	pověrčivost
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
ani	ani	k9	ani
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
horoskopy	horoskop	k1gInPc4	horoskop
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
narození	narození	k1gNnSc2	narození
<g/>
...	...	k?	...
Neboť	neboť	k8xC	neboť
i	i	k9	i
když	když	k8xS	když
skutečně	skutečně	k6eAd1	skutečně
určují	určovat	k5eAaImIp3nP	určovat
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
zjistí	zjistit	k5eAaPmIp3nS	zjistit
pozici	pozice	k1gFnSc4	pozice
hvězd	hvězda	k1gFnPc2	hvězda
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
něčího	něčí	k3xOyIgNnSc2	něčí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
zásadně	zásadně	k6eAd1	zásadně
se	se	k3xPyFc4	se
mýlí	mýlit	k5eAaImIp3nP	mýlit
<g/>
,	,	kIx,	,
když	když	k8xS	když
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
<g />
.	.	kIx.	.
</s>
<s>
údajů	údaj	k1gInPc2	údaj
snaží	snažit	k5eAaImIp3nS	snažit
předpovědět	předpovědět	k5eAaPmF	předpovědět
naše	náš	k3xOp1gNnSc4	náš
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
důsledky	důsledek	k1gInPc4	důsledek
<g/>
...	...	k?	...
Chtít	chtít	k5eAaImF	chtít
ale	ale	k8xC	ale
z	z	k7c2	z
takových	takový	k3xDgNnPc2	takový
zjištění	zjištění	k1gNnPc2	zjištění
předpovídat	předpovídat	k5eAaImF	předpovídat
mravní	mravní	k2eAgFnPc4d1	mravní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnPc4	jednání
a	a	k8xC	a
události	událost	k1gFnPc4	událost
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
šílený	šílený	k2eAgInSc1d1	šílený
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nahlédli	nahlédnout	k5eAaPmAgMnP	nahlédnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
takových	takový	k3xDgFnPc2	takový
věcí	věc	k1gFnPc2	věc
zříci	zříct	k5eAaPmF	zříct
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
pověru	pověra	k1gFnSc4	pověra
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
paradox	paradox	k1gInSc1	paradox
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
ač	ač	k8xS	ač
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
rozmení	rozmení	k1gNnSc6	rozmení
kratším	krátký	k2eAgNnSc6d2	kratší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
poloha	poloha	k1gFnSc1	poloha
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Augustinus	Augustinus	k1gInSc1	Augustinus
také	také	k9	také
důsledně	důsledně	k6eAd1	důsledně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
hvězdáři	hvězdář	k1gMnPc7	hvězdář
jako	jako	k8xC	jako
vědeckými	vědecký	k2eAgMnPc7d1	vědecký
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
hvězdopravců	hvězdopravec	k1gMnPc2	hvězdopravec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
érách	éra	k1gFnPc6	éra
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
střídala	střídat	k5eAaImAgFnS	střídat
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
astrologie	astrologie	k1gFnSc1	astrologie
dominantní	dominantní	k2eAgFnSc1d1	dominantní
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
tolerována	tolerovat	k5eAaImNgFnS	tolerovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
astrologii	astrologie	k1gFnSc4	astrologie
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
věnovala	věnovat	k5eAaImAgFnS	věnovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
katolických	katolický	k2eAgMnPc2d1	katolický
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
učenců	učenec	k1gMnPc2	učenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
astrologie	astrologie	k1gFnSc1	astrologie
církví	církev	k1gFnPc2	církev
potírána	potírat	k5eAaImNgFnS	potírat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
stanovisko	stanovisko	k1gNnSc1	stanovisko
jasně	jasně	k6eAd1	jasně
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
Katechismus	katechismus	k1gInSc1	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
odst	odst	k1gMnSc1	odst
<g/>
.	.	kIx.	.
2116	[number]	k4	2116
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odmítat	odmítat	k5eAaImF	odmítat
všechny	všechen	k3xTgInPc4	všechen
způsoby	způsob	k1gInPc4	způsob
věštění	věštění	k1gNnSc2	věštění
<g/>
:	:	kIx,	:
vzývání	vzývání	k1gNnSc1	vzývání
satana	satan	k1gMnSc2	satan
nebo	nebo	k8xC	nebo
zlých	zlý	k2eAgMnPc2d1	zlý
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
mrtvých	mrtvý	k1gMnPc2	mrtvý
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
praktiky	praktika	k1gFnPc4	praktika
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
neprávem	neprávo	k1gNnSc7	neprávo
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
<g/>
"	"	kIx"	"
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Uchylovat	uchylovat	k5eAaImF	uchylovat
se	se	k3xPyFc4	se
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
k	k	k7c3	k
horoskopům	horoskop	k1gInPc3	horoskop
<g/>
,	,	kIx,	,
k	k	k7c3	k
astrologii	astrologie	k1gFnSc3	astrologie
<g/>
,	,	kIx,	,
k	k	k7c3	k
hádání	hádání	k1gNnSc3	hádání
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
k	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
předtuch	předtucha	k1gFnPc2	předtucha
a	a	k8xC	a
věšteb	věštba	k1gFnPc2	věštba
<g/>
,	,	kIx,	,
k	k	k7c3	k
jevům	jev	k1gInPc3	jev
jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
<g/>
,	,	kIx,	,
ptát	ptát	k5eAaImF	ptát
se	se	k3xPyFc4	se
věštců	věštec	k1gMnPc2	věštec
(	(	kIx(	(
<g/>
médií	médium	k1gNnPc2	médium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
všem	všecek	k3xTgNnSc6	všecek
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
vůle	vůle	k1gFnSc1	vůle
mít	mít	k5eAaImF	mít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
časem	čas	k1gInSc7	čas
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
dějinami	dějiny	k1gFnPc7	dějiny
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
nad	nad	k7c7	nad
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
touha	touha	k1gFnSc1	touha
naklonit	naklonit	k5eAaPmF	naklonit
skryté	skrytý	k2eAgFnPc4d1	skrytá
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
ustavení	ustavení	k1gNnSc2	ustavení
vědy	věda	k1gFnSc2	věda
jako	jako	k8xC	jako
racionalistického	racionalistický	k2eAgInSc2d1	racionalistický
systému	systém	k1gInSc2	systém
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
opakovatelnosti	opakovatelnost	k1gFnSc6	opakovatelnost
a	a	k8xC	a
přenositelnosti	přenositelnost	k1gFnPc4	přenositelnost
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
exaktnosti	exaktnost	k1gFnSc2	exaktnost
<g/>
,	,	kIx,	,
měřitelnosti	měřitelnost	k1gFnSc2	měřitelnost
a	a	k8xC	a
statistické	statistický	k2eAgFnSc2d1	statistická
zpracovatelnosti	zpracovatelnost	k1gFnSc2	zpracovatelnost
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
astrologie	astrologie	k1gFnSc1	astrologie
vystavena	vystaven	k2eAgFnSc1d1	vystavena
oponentuře	oponentura	k1gFnSc3	oponentura
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vědecké	vědecký	k2eAgFnSc2d1	vědecká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgNnPc4	tento
kritéria	kritérion	k1gNnPc4	kritérion
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
principům	princip	k1gInPc3	princip
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
Principy	princip	k1gInPc4	princip
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
)	)	kIx)	)
naplnit	naplnit	k5eAaPmF	naplnit
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
řada	řada	k1gFnSc1	řada
studií	studie	k1gFnPc2	studie
<g/>
[	[	kIx(	[
<g/>
čí	čí	k3xOyRgInPc4	čí
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
vytkly	vytknout	k5eAaPmAgFnP	vytknout
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
potvrdit	potvrdit	k5eAaPmF	potvrdit
či	či	k8xC	či
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
astrologická	astrologický	k2eAgNnPc4d1	astrologické
východiska	východisko	k1gNnPc4	východisko
statistickou	statistický	k2eAgFnSc7d1	statistická
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
těchto	tento	k3xDgInPc2	tento
experimentů	experiment	k1gInPc2	experiment
stál	stát	k5eAaImAgInS	stát
zejména	zejména	k9	zejména
francouzský	francouzský	k2eAgMnSc1d1	francouzský
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Michel	Michel	k1gInSc4	Michel
Gauquelin	Gauquelin	k2eAgInSc4d1	Gauquelin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
největší	veliký	k2eAgInSc1d3	veliký
rozruch	rozruch	k1gInSc1	rozruch
způsobil	způsobit	k5eAaPmAgInS	způsobit
publikací	publikace	k1gFnSc7	publikace
tzv.	tzv.	kA	tzv.
Martického	Martický	k2eAgInSc2d1	Martický
efektu	efekt	k1gInSc2	efekt
-	-	kIx~	-
významných	významný	k2eAgFnPc2d1	významná
statistických	statistický	k2eAgFnPc2d1	statistická
odchylek	odchylka	k1gFnPc2	odchylka
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
Marsu	Mars	k1gInSc2	Mars
v	v	k7c6	v
nativním	nativní	k2eAgInSc6d1	nativní
horoskopu	horoskop	k1gInSc6	horoskop
významných	významný	k2eAgMnPc2d1	významný
sportovců	sportovec	k1gMnPc2	sportovec
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
však	však	k9	však
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
například	například	k6eAd1	například
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
podobného	podobný	k2eAgInSc2d1	podobný
výsledku	výsledek	k1gInSc2	výsledek
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
náhodných	náhodný	k2eAgNnPc6d1	náhodné
datech	datum	k1gNnPc6	datum
byla	být	k5eAaImAgFnS	být
25	[number]	k4	25
<g/>
procentní	procentní	k2eAgInPc1d1	procentní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobných	podobný	k2eAgInPc6d1	podobný
výzkumech	výzkum	k1gInPc6	výzkum
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
psycholog	psycholog	k1gMnSc1	psycholog
Hans	Hans	k1gMnSc1	Hans
Eysenck	Eysenck	k1gMnSc1	Eysenck
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
psychologicko-statistické	psychologickotatistický	k2eAgFnPc1d1	psychologicko-statistický
studie	studie	k1gFnPc1	studie
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
autorů	autor	k1gMnPc2	autor
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
introverzí	introverze	k1gFnSc7	introverze
a	a	k8xC	a
extraverzí	extraverze	k1gFnSc7	extraverze
s	s	k7c7	s
astrologickými	astrologický	k2eAgInPc7d1	astrologický
vlivy	vliv	k1gInPc7	vliv
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
či	či	k8xC	či
emoční	emoční	k2eAgFnSc7d1	emoční
labilitou	labilita	k1gFnSc7	labilita
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
narozených	narozený	k2eAgFnPc2d1	narozená
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
vodních	vodní	k2eAgNnPc6d1	vodní
znameních	znamení	k1gNnPc6	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
podobné	podobný	k2eAgFnPc1d1	podobná
studie	studie	k1gFnPc1	studie
však	však	k8xC	však
podobné	podobný	k2eAgFnPc1d1	podobná
statisticky	statisticky	k6eAd1	statisticky
významné	významný	k2eAgFnPc1d1	významná
korelace	korelace	k1gFnPc1	korelace
mezi	mezi	k7c7	mezi
astrologickými	astrologický	k2eAgInPc7d1	astrologický
a	a	k8xC	a
psychologickými	psychologický	k2eAgInPc7d1	psychologický
fenomény	fenomén	k1gInPc7	fenomén
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
studie	studie	k1gFnPc1	studie
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
odborných	odborný	k2eAgFnPc2d1	odborná
disputací	disputace	k1gFnPc2	disputace
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podobné	podobný	k2eAgInPc4d1	podobný
statistické	statistický	k2eAgInPc4d1	statistický
experimenty	experiment	k1gInPc4	experiment
nicméně	nicméně	k8xC	nicméně
astrologové	astrolog	k1gMnPc1	astrolog
nahlížejí	nahlížet	k5eAaImIp3nP	nahlížet
převážně	převážně	k6eAd1	převážně
skepticky	skepticky	k6eAd1	skepticky
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výzkumy	výzkum	k1gInPc1	výzkum
totiž	totiž	k9	totiž
zpravidla	zpravidla	k6eAd1	zpravidla
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
ukazatelů	ukazatel	k1gMnPc2	ukazatel
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
astrologie	astrologie	k1gFnSc1	astrologie
dospívá	dospívat	k5eAaImIp3nS	dospívat
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
závěrům	závěr	k1gInPc3	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
astrologického	astrologický	k2eAgInSc2d1	astrologický
výzkumu	výzkum	k1gInSc2	výzkum
je	být	k5eAaImIp3nS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
možných	možný	k2eAgFnPc2d1	možná
kombinací	kombinace	k1gFnPc2	kombinace
těchto	tento	k3xDgMnPc2	tento
ukazatelů	ukazatel	k1gMnPc2	ukazatel
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnSc2	jejich
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
v	v	k7c6	v
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
případě	případ	k1gInSc6	případ
<g/>
)	)	kIx)	)
a	a	k8xC	a
posouzení	posouzení	k1gNnSc4	posouzení
jejich	jejich	k3xOp3gFnSc2	jejich
významnosti	významnost	k1gFnSc2	významnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Francise	Francise	k1gFnSc1	Francise
Bacona	Bacona	k1gFnSc1	Bacona
a	a	k8xC	a
vědecké	vědecký	k2eAgFnPc1d1	vědecká
revoluce	revoluce	k1gFnPc1	revoluce
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyčleňovat	vyčleňovat	k5eAaImF	vyčleňovat
nové	nový	k2eAgFnPc4d1	nová
vědecké	vědecký	k2eAgFnPc4d1	vědecká
disciplíny	disciplína	k1gFnPc4	disciplína
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
obory	obor	k1gInPc1	obor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
,	,	kIx,	,
meteorologie	meteorologie	k1gFnSc1	meteorologie
..	..	k?	..
<g/>
)	)	kIx)	)
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
metody	metoda	k1gFnPc1	metoda
systematického	systematický	k2eAgInSc2d1	systematický
empirického	empirický	k2eAgInSc2d1	empirický
výzkumu	výzkum	k1gInSc2	výzkum
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
experimentálních	experimentální	k2eAgNnPc6d1	experimentální
pozorováních	pozorování	k1gNnPc6	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
astrologie	astrologie	k1gFnPc1	astrologie
a	a	k8xC	a
astronomie	astronomie	k1gFnPc1	astronomie
začaly	začít	k5eAaPmAgFnP	začít
rozcházet	rozcházet	k5eAaImF	rozcházet
<g/>
;	;	kIx,	;
astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
přírodní	přírodní	k2eAgFnSc7d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
astrologie	astrologie	k1gFnSc1	astrologie
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
viděna	vidět	k5eAaImNgFnS	vidět
přírodovědci	přírodovědec	k1gMnSc3	přírodovědec
jako	jako	k8xS	jako
okultní	okultní	k2eAgFnSc1d1	okultní
věda	věda	k1gFnSc1	věda
nebo	nebo	k8xC	nebo
pověra	pověra	k1gFnSc1	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Odloučení	odloučení	k1gNnSc1	odloučení
astrologie	astrologie	k1gFnSc2	astrologie
od	od	k7c2	od
vědy	věda	k1gFnSc2	věda
bylo	být	k5eAaImAgNnS	být
urychleno	urychlit	k5eAaPmNgNnS	urychlit
v	v	k7c6	v
osmnáctém	osmnáctý	k4xOgMnSc6	osmnáctý
a	a	k8xC	a
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
moderních	moderní	k2eAgMnPc2d1	moderní
vědců	vědec	k1gMnPc2	vědec
astrologii	astrologie	k1gFnSc4	astrologie
odmítá	odmítat	k5eAaImIp3nS	odmítat
jako	jako	k9	jako
nevědeckou	vědecký	k2eNgFnSc4d1	nevědecká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
základní	základní	k2eAgFnPc4d1	základní
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
neexistují	existovat	k5eNaImIp3nP	existovat
experimentální	experimentální	k2eAgNnPc4d1	experimentální
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
by	by	kYmCp3nP	by
působení	působení	k1gNnSc4	působení
planet	planeta	k1gFnPc2	planeta
prokazovala	prokazovat	k5eAaImAgFnS	prokazovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nevědeckou	vědecký	k2eNgFnSc4d1	nevědecká
ji	on	k3xPp3gFnSc4	on
označili	označit	k5eAaPmAgMnP	označit
například	například	k6eAd1	například
fyzik	fyzik	k1gMnSc1	fyzik
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
či	či	k8xC	či
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
<g/>
.	.	kIx.	.
<g/>
Andrew	Andrew	k1gFnSc1	Andrew
Fraknoi	Frakno	k1gFnSc2	Frakno
z	z	k7c2	z
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pseudovědu	pseudověda	k1gFnSc4	pseudověda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
the	the	k?	the
American	American	k1gInSc1	American
Humanist	Humanist	k1gMnSc1	Humanist
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
humanistická	humanistický	k2eAgFnSc1d1	humanistická
asociace	asociace	k1gFnSc1	asociace
<g/>
)	)	kIx)	)
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvěra	důvěra	k1gFnSc1	důvěra
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
víru	víra	k1gFnSc4	víra
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
vědecký	vědecký	k2eAgInSc4d1	vědecký
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
vskutku	vskutku	k9	vskutku
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgInPc1d1	silný
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
SISYFOS	Sisyfos	k1gMnSc1	Sisyfos
astrologii	astrologie	k1gFnSc3	astrologie
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
za	za	k7c4	za
pseudovědu	pseudověda	k1gFnSc4	pseudověda
<g/>
.	.	kIx.	.
</s>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
astrologové	astrolog	k1gMnPc1	astrolog
se	se	k3xPyFc4	se
k	k	k7c3	k
vědě	věda	k1gFnSc3	věda
a	a	k8xC	a
aplikaci	aplikace	k1gFnSc3	aplikace
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
metod	metoda	k1gFnPc2	metoda
staví	stavit	k5eAaBmIp3nS	stavit
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
astrologie	astrologie	k1gFnSc1	astrologie
očividně	očividně	k6eAd1	očividně
využívá	využívat	k5eAaPmIp3nS	využívat
některých	některý	k3yIgFnPc2	některý
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pozorování	pozorování	k1gNnSc1	pozorování
a	a	k8xC	a
popisu	popis	k1gInSc2	popis
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
formulace	formulace	k1gFnPc1	formulace
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc1	stanovení
hypotéz	hypotéza	k1gFnPc2	hypotéza
a	a	k8xC	a
předvídání	předvídání	k1gNnPc2	předvídání
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgFnSc7d3	nejčastější
výhradou	výhrada	k1gFnSc7	výhrada
vědy	věda	k1gFnSc2	věda
vůči	vůči	k7c3	vůči
astrologii	astrologie	k1gFnSc3	astrologie
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
ověřování	ověřování	k1gNnSc1	ověřování
souladu	soulad	k1gInSc2	soulad
hypotéz	hypotéza	k1gFnPc2	hypotéza
a	a	k8xC	a
předvídání	předvídání	k1gNnSc1	předvídání
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ji	on	k3xPp3gFnSc4	on
někteří	některý	k3yIgMnPc1	některý
astrologové	astrolog	k1gMnPc1	astrolog
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
považují	považovat	k5eAaImIp3nP	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
větve	větev	k1gFnPc1	větev
moderní	moderní	k2eAgFnSc2d1	moderní
astrologie	astrologie	k1gFnSc2	astrologie
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
astrology	astrolog	k1gMnPc7	astrolog
ztotožňovány	ztotožňován	k2eAgInPc1d1	ztotožňován
s	s	k7c7	s
psychologizujícími	psychologizující	k2eAgInPc7d1	psychologizující
a	a	k8xC	a
humanistickými	humanistický	k2eAgInPc7d1	humanistický
vědními	vědní	k2eAgInPc7d1	vědní
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
souvztažnosti	souvztažnost	k1gFnPc4	souvztažnost
mezi	mezi	k7c7	mezi
děním	dění	k1gNnSc7	dění
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
psychologickými	psychologický	k2eAgFnPc7d1	psychologická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc3	jeho
vzorci	vzorec	k1gInSc3	vzorec
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tyto	tento	k3xDgInPc4	tento
vzorce	vzorec	k1gInPc4	vzorec
vhodně	vhodně	k6eAd1	vhodně
kultivovat	kultivovat	k5eAaImF	kultivovat
<g/>
.	.	kIx.	.
</s>
<s>
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
Héfaistión	Héfaistión	k1gMnSc1	Héfaistión
Thébský	thébský	k2eAgInSc4d1	thébský
Dorotheus	Dorotheus	k1gInSc4	Dorotheus
Sidónský	Sidónský	k2eAgMnSc1d1	Sidónský
Vettius	Vettius	k1gMnSc1	Vettius
Valens	Valensa	k1gFnPc2	Valensa
Julius	Julius	k1gMnSc1	Julius
Firmicus	Firmicus	k1gMnSc1	Firmicus
Maternus	Maternus	k1gMnSc1	Maternus
Albumasar	Albumasar	k1gMnSc1	Albumasar
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilee	k1gFnSc4	Galilee
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brahe	k1gNnSc7	Brahe
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
Marsilio	Marsilio	k1gMnSc1	Marsilio
Ficino	Ficino	k1gNnSc1	Ficino
Jan	Jan	k1gMnSc1	Jan
Šindel	šindel	k1gInSc1	šindel
Nostradamus	Nostradamus	k1gMnSc1	Nostradamus
Otakar	Otakar	k1gMnSc1	Otakar
Griese	Griese	k1gFnSc2	Griese
Jan	Jan	k1gMnSc1	Jan
Kefer	Kefer	k1gMnSc1	Kefer
Mauritius	Mauritius	k1gInSc4	Mauritius
Knauer	Knauer	k1gInSc1	Knauer
William	William	k1gInSc1	William
Lilly	Lilla	k1gMnSc2	Lilla
Gerolamo	Gerolama	k1gFnSc5	Gerolama
Cardano	Cardana	k1gFnSc5	Cardana
Cyril	Cyril	k1gMnSc1	Cyril
Fagan	Fagan	k?	Fagan
Garth	Garth	k1gMnSc1	Garth
Allen	Allen	k1gMnSc1	Allen
Karel	Karel	k1gMnSc1	Karel
Weinfurter	Weinfurter	k1gMnSc1	Weinfurter
Horoskop	horoskop	k1gInSc4	horoskop
Hermetismus	hermetismus	k1gInSc1	hermetismus
Astronomie	astronomie	k1gFnSc2	astronomie
Sideralistická	Sideralistický	k2eAgFnSc1d1	Sideralistický
astrologie	astrologie	k1gFnSc1	astrologie
Bulvární	bulvární	k2eAgFnSc2d1	bulvární
astrologie	astrologie	k1gFnSc2	astrologie
Astrofocus	Astrofocus	k1gInSc1	Astrofocus
Forerův	Forerův	k2eAgInSc1d1	Forerův
efekt	efekt	k1gInSc4	efekt
Horární	Horární	k2eAgInSc4d1	Horární
<g />
.	.	kIx.	.
</s>
<s>
astrologie	astrologie	k1gFnSc1	astrologie
Galerie	galerie	k1gFnSc1	galerie
astrologie	astrologie	k1gFnSc1	astrologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
astrologie	astrologie	k1gFnSc2	astrologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Česká	český	k2eAgFnSc1d1	Česká
astrologická	astrologický	k2eAgFnSc1d1	astrologická
stránka	stránka	k1gFnSc1	stránka
-	-	kIx~	-
Humanistická	humanistický	k2eAgFnSc1d1	humanistická
a	a	k8xC	a
transpersonální	transpersonální	k2eAgFnSc1d1	transpersonální
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Astrologie	astrologie	k1gFnSc1	astrologie
panna	panna	k1gFnSc1	panna
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
ezoteriky	ezoterika	k1gFnSc2	ezoterika
a	a	k8xC	a
duchovna	duchovno	k1gNnSc2	duchovno
PhDr.	PhDr.	kA	PhDr.
Martin	Martin	k1gInSc1	Martin
Křivka	křivka	k1gFnSc1	křivka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
<g/>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
Astrologie	astrologie	k1gFnSc1	astrologie
historie	historie	k1gFnSc1	historie
astrologie	astrologie	k1gFnSc1	astrologie
a	a	k8xC	a
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
astrologie	astrologie	k1gFnSc1	astrologie
<g/>
,	,	kIx,	,
inext	inext	k1gInSc1	inext
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Texty	text	k1gInPc1	text
k	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
souvislostem	souvislost	k1gFnPc3	souvislost
astrologie	astrologie	k1gFnSc2	astrologie
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
<g/>
části	část	k1gFnSc3	část
Setkání	setkání	k1gNnSc2	setkání
nad	nad	k7c7	nad
astrologií	astrologie	k1gFnSc7	astrologie
(	(	kIx(	(
<g/>
pohled	pohled	k1gInSc1	pohled
věřících	věřící	k1gMnPc2	věřící
na	na	k7c4	na
astrologii	astrologie	k1gFnSc4	astrologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
G.	G.	kA	G.
Schiaparelli	Schiaparell	k1gMnPc1	Schiaparell
<g/>
:	:	kIx,	:
Astronomie	astronomie	k1gFnSc1	astronomie
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
etf	etf	k?	etf
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
cuni	cuni	k1gNnSc1	cuni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
stránky	stránka	k1gFnPc4	stránka
Evangelické	evangelický	k2eAgFnSc2d1	evangelická
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Kralovy	Kralův	k2eAgFnSc2d1	Kralova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
Astrologická	astrologický	k2eAgFnSc1d1	astrologická
společnost	společnost	k1gFnSc1	společnost
ČR-	ČR-	k1gFnSc2	ČR-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Astrologické	astrologický	k2eAgFnSc2d1	astrologická
společnosti	společnost	k1gFnSc2	společnost
ČR	ČR	kA	ČR
Astrolexikon	Astrolexikon	k1gInSc1	Astrolexikon
-	-	kIx~	-
Astrologický	astrologický	k2eAgInSc1d1	astrologický
slovník	slovník	k1gInSc1	slovník
Claudios	Claudios	k1gMnSc1	Claudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
Tetrabiblos	Tetrabiblos	k1gMnSc1	Tetrabiblos
ceskaastrologie	ceskaastrologie	k1gFnSc1	ceskaastrologie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Vojta	Vojta	k1gMnSc1	Vojta
Roučka	Roučka	k1gMnSc1	Roučka
<g/>
:	:	kIx,	:
Kurz	Kurz	k1gMnSc1	Kurz
astrologie	astrologie	k1gFnSc2	astrologie
I	I	kA	I
<g/>
,	,	kIx,	,
<g/>
II	II	kA	II
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
III	III	kA	III
<g/>
,	,	kIx,	,
ceskaastrologie	ceskaastrologie	k1gFnSc1	ceskaastrologie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Petr	Petr	k1gMnSc1	Petr
Kubala	Kubala	k1gMnSc1	Kubala
<g/>
:	:	kIx,	:
<g/>
Astrologové	astrolog	k1gMnPc1	astrolog
<g/>
,	,	kIx,	,
ufologové	ufolog	k1gMnPc1	ufolog
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
šarlatáni	šarlatán	k1gMnPc1	šarlatán
I	i	k9	i
Instantní	instantní	k2eAgFnPc4d1	instantní
astronomické	astronomický	k2eAgFnPc4d1	astronomická
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ian	ian	k?	ian
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Heřt	Heřt	k1gMnSc1	Heřt
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
astrologie	astrologie	k1gFnSc1	astrologie
nefunguje	fungovat	k5eNaImIp3nS	fungovat
sysifos	sysifos	k1gInSc4	sysifos
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Heslo	heslo	k1gNnSc4	heslo
astrologie	astrologie	k1gFnSc2	astrologie
ve	v	k7c6	v
skeptickém	skeptický	k2eAgInSc6d1	skeptický
slovníku	slovník	k1gInSc6	slovník
Spolek	spolek	k1gInSc1	spolek
Sisifos	Sisifos	k1gInSc1	Sisifos
<g/>
,	,	kIx,	,
sysifos	sysifos	k1gInSc1	sysifos
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
