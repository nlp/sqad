<s>
Hlaholice	hlaholice	k1gFnSc1	hlaholice
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neužívané	užívaný	k2eNgNnSc4d1	neužívané
slovanské	slovanský	k2eAgNnSc4d1	slovanské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroslověnského	staroslověnský	k2eAgNnSc2d1	staroslověnské
slovesa	sloveso	k1gNnSc2	sloveso
glagolati	glagolat	k5eAaPmF	glagolat
-	-	kIx~	-
hovořit	hovořit	k5eAaImF	hovořit
nebo	nebo	k8xC	nebo
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
glagolъ	glagolъ	k?	glagolъ
-	-	kIx~	-
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
tvořilo	tvořit	k5eAaImAgNnS	tvořit
základ	základ	k1gInSc4	základ
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nejdéle	dlouho	k6eAd3	dlouho
udrželo	udržet	k5eAaPmAgNnS	udržet
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Slované	Slovan	k1gMnPc1	Slovan
přešli	přejít	k5eAaPmAgMnP	přejít
buď	buď	k8xC	buď
k	k	k7c3	k
latince	latinka	k1gFnSc3	latinka
nebo	nebo	k8xC	nebo
cyrilici	cyrilice	k1gFnSc6	cyrilice
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlaholici	hlaholice	k1gFnSc6	hlaholice
nezávislý	závislý	k2eNgInSc1d1	nezávislý
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlaholice	hlaholice	k1gFnSc1	hlaholice
byla	být	k5eAaImAgFnS	být
odsunuta	odsunout	k5eAaPmNgFnS	odsunout
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Hlaholici	hlaholice	k1gFnSc4	hlaholice
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Konstantin	Konstantin	k1gMnSc1	Konstantin
ze	z	k7c2	z
Soluně	Soluň	k1gFnSc2	Soluň
(	(	kIx(	(
<g/>
svatý	svatý	k2eAgMnSc1d1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
863	[number]	k4	863
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
mise	mise	k1gFnSc1	mise
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
(	(	kIx(	(
<g/>
žádost	žádost	k1gFnSc4	žádost
moravského	moravský	k2eAgMnSc4d1	moravský
vládce	vládce	k1gMnSc4	vládce
Rastislava	Rastislav	k1gMnSc4	Rastislav
u	u	k7c2	u
východořímského	východořímský	k2eAgMnSc2d1	východořímský
císaře	císař	k1gMnSc2	císař
Michala	Michal	k1gMnSc2	Michal
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
písmo	písmo	k1gNnSc4	písmo
pro	pro	k7c4	pro
přijatelný	přijatelný	k2eAgInSc4d1	přijatelný
zápis	zápis	k1gInSc4	zápis
slovanštiny	slovanština	k1gFnSc2	slovanština
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
překládat	překládat	k5eAaImF	překládat
bohoslužebné	bohoslužebný	k2eAgInPc4d1	bohoslužebný
i	i	k8xC	i
jiné	jiný	k2eAgInPc4d1	jiný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1	hlaholice
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
41	[number]	k4	41
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
pro	pro	k7c4	pro
hlásky	hlásek	k1gInPc4	hlásek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
přejatých	přejatý	k2eAgFnPc2d1	přejatá
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
24	[number]	k4	24
grafémů	grafém	k1gInPc2	grafém
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
středověkých	středověký	k2eAgInPc2d1	středověký
kurzivních	kurzivní	k2eAgInPc2d1	kurzivní
minuskulních	minuskulní	k2eAgInPc2d1	minuskulní
znaků	znak	k1gInPc2	znak
řeckého	řecký	k2eAgNnSc2d1	řecké
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
ostatních	ostatní	k2eAgInPc2d1	ostatní
znaků	znak	k1gInPc2	znak
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
písmen	písmeno	k1gNnPc2	písmeno
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
a	a	k8xC	a
samaritánského	samaritánský	k2eAgNnSc2d1	samaritánské
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
Konstantin	Konstantin	k1gMnSc1	Konstantin
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
předchozí	předchozí	k2eAgFnSc6d1	předchozí
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Chazarům	Chazar	k1gMnPc3	Chazar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
abecedou	abeceda	k1gFnSc7	abeceda
koptskou	koptský	k2eAgFnSc7d1	koptská
<g/>
,	,	kIx,	,
gruzínskou	gruzínský	k2eAgFnSc7d1	gruzínská
a	a	k8xC	a
s	s	k7c7	s
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
a	a	k8xC	a
neuznávaná	uznávaný	k2eNgFnSc1d1	neuznávaná
<g/>
,	,	kIx,	,
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlaholice	hlaholice	k1gFnSc1	hlaholice
má	mít	k5eAaImIp3nS	mít
základ	základ	k1gInSc4	základ
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
(	(	kIx(	(
<g/>
diskutovaném	diskutovaný	k2eAgInSc6d1	diskutovaný
<g/>
)	)	kIx)	)
runovém	runový	k2eAgNnSc6d1	runové
písmu	písmo	k1gNnSc6	písmo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
črty	črt	k1gInPc1	črt
a	a	k8xC	a
vruby	vrub	k1gInPc1	vrub
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
a	a	k8xC	a
také	také	k9	také
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
runy	runa	k1gFnPc1	runa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
germánské	germánský	k2eAgFnPc1d1	germánská
runy	runa	k1gFnPc1	runa
<g/>
,	,	kIx,	,
v	v	k7c6	v
předkřesťanské	předkřesťanský	k2eAgFnSc6d1	předkřesťanská
době	doba	k1gFnSc6	doba
užíváno	užívat	k5eAaImNgNnS	užívat
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1	hlaholice
byla	být	k5eAaImAgFnS	být
každopádně	každopádně	k6eAd1	každopádně
sestavena	sestavit	k5eAaPmNgFnS	sestavit
jako	jako	k8xS	jako
vědomý	vědomý	k2eAgInSc4d1	vědomý
celek	celek	k1gInSc4	celek
s	s	k7c7	s
návazností	návaznost	k1gFnSc7	návaznost
mnoha	mnoho	k4c2	mnoho
písmen	písmeno	k1gNnPc2	písmeno
a	a	k8xC	a
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
grafickou	grafický	k2eAgFnSc7d1	grafická
úpravou	úprava	k1gFnSc7	úprava
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
ornamentálního	ornamentální	k2eAgInSc2d1	ornamentální
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
dříve	dříve	k6eAd2	dříve
neměli	mít	k5eNaImAgMnP	mít
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čítali	čítat	k5eAaImAgMnP	čítat
a	a	k8xC	a
hádali	hádat	k5eAaImAgMnP	hádat
pomocí	pomocí	k7c2	pomocí
črt	črta	k1gFnPc2	črta
a	a	k8xC	a
vrubů	vrub	k1gInPc2	vrub
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byli	být	k5eAaImAgMnP	být
pohané	pohan	k1gMnPc1	pohan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pokřtili	pokřtít	k5eAaPmAgMnP	pokřtít
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
řeč	řeč	k1gFnSc4	řeč
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
latinskými	latinský	k2eAgNnPc7d1	latinské
i	i	k8xC	i
řeckými	řecký	k2eAgNnPc7d1	řecké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
psát	psát	k5eAaImF	psát
správně	správně	k6eAd1	správně
řeckými	řecký	k2eAgNnPc7d1	řecké
písmeny	písmeno	k1gNnPc7	písmeno
bogъ	bogъ	k?	bogъ
nebo	nebo	k8xC	nebo
životъ	životъ	k?	životъ
nebo	nebo	k8xC	nebo
dzělo	dzělo	k1gNnSc1	dzělo
nebo	nebo	k8xC	nebo
crъ	crъ	k?	crъ
nebo	nebo	k8xC	nebo
člověkъ	člověkъ	k?	člověkъ
nebo	nebo	k8xC	nebo
širota	širot	k1gMnSc2	širot
nebo	nebo	k8xC	nebo
ščedroty	ščedrota	k1gFnSc2	ščedrota
nebo	nebo	k8xC	nebo
junostь	junostь	k?	junostь
nebo	nebo	k8xC	nebo
ǫ	ǫ	k?	ǫ
nebo	nebo	k8xC	nebo
językъ	językъ	k?	językъ
nebo	nebo	k8xC	nebo
ědъ	ědъ	k?	ědъ
a	a	k8xC	a
jiná	jiný	k2eAgFnSc1d1	jiná
jim	on	k3xPp3gMnPc3	on
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
mnohá	mnohý	k2eAgNnPc4d1	mnohé
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
však	však	k9	však
lidumilný	lidumilný	k2eAgMnSc1d1	lidumilný
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vše	všechen	k3xTgNnSc4	všechen
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
,	,	kIx,	,
neponechal	ponechat	k5eNaPmAgMnS	ponechat
lidské	lidský	k2eAgNnSc4d1	lidské
plémě	plémě	k1gNnSc4	plémě
slovanské	slovanský	k2eAgNnSc4d1	slovanské
bez	bez	k7c2	bez
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přiváděje	přivádět	k5eAaImSgInS	přivádět
celé	celý	k2eAgFnSc6d1	celá
k	k	k7c3	k
vědění	vědění	k1gNnSc3	vědění
a	a	k8xC	a
spasení	spasení	k1gNnSc3	spasení
<g/>
,	,	kIx,	,
smiloval	smilovat	k5eAaPmAgMnS	smilovat
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
plemenem	plemeno	k1gNnSc7	plemeno
slovanským	slovanský	k2eAgNnSc7d1	slovanské
a	a	k8xC	a
seslal	seslat	k5eAaPmAgMnS	seslat
mu	on	k3xPp3gMnSc3	on
svatého	svatý	k2eAgMnSc2d1	svatý
Konstantina	Konstantin	k1gMnSc2	Konstantin
Filozofa	filozof	k1gMnSc2	filozof
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Kyril	Kyril	k1gMnSc1	Kyril
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
spravedlivého	spravedlivý	k2eAgMnSc2d1	spravedlivý
a	a	k8xC	a
pravého	pravý	k2eAgNnSc2d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ten	ten	k3xDgMnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gNnPc4	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
třicet	třicet	k4xCc4	třicet
a	a	k8xC	a
osm	osm	k4xCc4	osm
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc4	některý
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
řeckých	řecký	k2eAgNnPc2d1	řecké
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
slovanského	slovanský	k2eAgInSc2d1	slovanský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
počav	počnout	k5eAaPmDgInS	počnout
podle	podle	k7c2	podle
řečtiny	řečtina	k1gFnPc1	řečtina
prvním	první	k4xOgMnSc6	první
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
totiž	totiž	k9	totiž
začínají	začínat	k5eAaImIp3nP	začínat
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
azъ	azъ	k?	azъ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
azъ	azъ	k?	azъ
začíná	začínat	k5eAaImIp3nS	začínat
obojí	oboj	k1gFnSc7	oboj
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
učinili	učinit	k5eAaPmAgMnP	učinit
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
napodobili	napodobit	k5eAaPmAgMnP	napodobit
židovská	židovský	k2eAgNnPc4d1	Židovské
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
on	on	k3xPp3gMnSc1	on
řecká	řecký	k2eAgNnPc1d1	řecké
<g/>
...	...	k?	...
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
hlaholice	hlaholice	k1gFnSc1	hlaholice
používala	používat	k5eAaImAgFnS	používat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
863	[number]	k4	863
<g/>
-	-	kIx~	-
<g/>
886	[number]	k4	886
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgInPc4d1	oficiální
státní	státní	k2eAgInPc4d1	státní
a	a	k8xC	a
bohoslužebné	bohoslužebný	k2eAgInPc4d1	bohoslužebný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
se	se	k3xPyFc4	se
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
akademii	akademie	k1gFnSc6	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
Konstantin	Konstantin	k1gMnSc1	Konstantin
nebo	nebo	k8xC	nebo
Metoděj	Metoděj	k1gMnSc1	Metoděj
a	a	k8xC	a
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
vychováváni	vychováván	k2eAgMnPc1d1	vychováván
jejich	jejich	k3xOp3gMnPc1	jejich
učedníci	učedník	k1gMnPc1	učedník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
886	[number]	k4	886
byli	být	k5eAaImAgMnP	být
nitranským	nitranský	k2eAgInSc7d1	nitranský
biskupem	biskup	k1gInSc7	biskup
Wichingem	Wiching	k1gInSc7	Wiching
kněží	kněz	k1gMnPc2	kněz
slovanského	slovanský	k2eAgInSc2d1	slovanský
obřadu	obřad	k1gInSc2	obřad
vyhnáni	vyhnán	k2eAgMnPc1d1	vyhnán
a	a	k8xC	a
hlaholice	hlaholice	k1gFnPc1	hlaholice
se	se	k3xPyFc4	se
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
přestala	přestat	k5eAaPmAgFnS	přestat
užívat	užívat	k5eAaImF	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Několika	několik	k4yIc3	několik
z	z	k7c2	z
vyhnaných	vyhnaný	k2eAgMnPc2d1	vyhnaný
kněží	kněz	k1gMnPc2	kněz
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
bulharský	bulharský	k2eAgMnSc1d1	bulharský
chán	chán	k1gMnSc1	chán
Boris	Boris	k1gMnSc1	Boris
I.	I.	kA	I.
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgInS	pověřit
je	být	k5eAaImIp3nS	být
výukou	výuka	k1gFnSc7	výuka
bulharských	bulharský	k2eAgMnPc2d1	bulharský
kněží	kněz	k1gMnPc2	kněz
<g/>
;	;	kIx,	;
založil	založit	k5eAaPmAgInS	založit
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
dvě	dva	k4xCgFnPc4	dva
akademie	akademie	k1gFnPc4	akademie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ohridu	Ohrid	k1gInSc6	Ohrid
a	a	k8xC	a
Preslavi	Preslaev	k1gFnSc6	Preslaev
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
totiž	totiž	k9	totiž
přijalo	přijmout	k5eAaPmAgNnS	přijmout
křesťanství	křesťanství	k1gNnSc1	křesťanství
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
a	a	k8xC	a
Boris	Boris	k1gMnSc1	Boris
viděl	vidět	k5eAaImAgMnS	vidět
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
bohoslužebném	bohoslužebný	k2eAgInSc6d1	bohoslužebný
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
písmu	písmo	k1gNnSc6	písmo
záruku	záruka	k1gFnSc4	záruka
nezávislosti	nezávislost	k1gFnSc2	nezávislost
bulharské	bulharský	k2eAgFnSc2d1	bulharská
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
cařihradském	cařihradský	k2eAgInSc6d1	cařihradský
patriarchátu	patriarchát	k1gInSc6	patriarchát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bulharských	bulharský	k2eAgFnPc2d1	bulharská
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
hlaholice	hlaholice	k1gFnSc1	hlaholice
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
Balkánu	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
hranatá	hranatý	k2eAgFnSc1d1	hranatá
varianta	varianta	k1gFnSc1	varianta
písma	písmo	k1gNnSc2	písmo
vedle	vedle	k7c2	vedle
staršího	starý	k2eAgInSc2d2	starší
oblého	oblý	k2eAgInSc2d1	oblý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1248	[number]	k4	1248
dokonce	dokonce	k9	dokonce
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
IV	IV	kA	IV
<g/>
.	.	kIx.	.
dal	dát	k5eAaPmAgInS	dát
Chorvatům	Chorvat	k1gMnPc3	Chorvat
ojedinělé	ojedinělý	k2eAgNnSc4d1	ojedinělé
privilegium	privilegium	k1gNnSc4	privilegium
užívat	užívat	k5eAaImF	užívat
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
hlaholice	hlaholice	k1gFnSc1	hlaholice
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
se	se	k3xPyFc4	se
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hlaholice	hlaholice	k1gFnSc2	hlaholice
užívala	užívat	k5eAaImAgFnS	užívat
ještě	ještě	k9	ještě
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
na	na	k7c6	na
Kyjevské	kyjevský	k2eAgFnSc6d1	Kyjevská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
hranatá	hranatý	k2eAgFnSc1d1	hranatá
podoba	podoba	k1gFnSc1	podoba
hlaholice	hlaholice	k1gFnSc1	hlaholice
<g/>
,	,	kIx,	,
když	když	k8xS	když
tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
r.	r.	kA	r.
1347	[number]	k4	1347
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jeho	jeho	k3xOp3gFnSc2	jeho
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
znovuzavedení	znovuzavedení	k1gNnSc4	znovuzavedení
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
liturgie	liturgie	k1gFnSc2	liturgie
povoláni	povolán	k2eAgMnPc1d1	povolán
chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
mniši	mnich	k1gMnPc1	mnich
(	(	kIx(	(
<g/>
glagoljaši	glagoljaš	k1gMnPc1	glagoljaš
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
založen	založit	k5eAaPmNgInS	založit
klášter	klášter	k1gInSc1	klášter
sv.	sv.	kA	sv.
Jeronýma	Jeroným	k1gMnSc2	Jeroným
a	a	k8xC	a
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
-	-	kIx~	-
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
klášter	klášter	k1gInSc4	klášter
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
Emauzy	Emauzy	k1gInPc4	Emauzy
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
zdejším	zdejší	k2eAgNnSc6d1	zdejší
skriptoriu	skriptorium	k1gNnSc6	skriptorium
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mj.	mj.	kA	mj.
i	i	k8xC	i
hlaholská	hlaholský	k2eAgFnSc1d1	hlaholská
část	část	k1gFnSc1	část
Remešského	remešský	k2eAgInSc2d1	remešský
evangeliáře	evangeliář	k1gInSc2	evangeliář
<g/>
,	,	kIx,	,
korunovačního	korunovační	k2eAgInSc2d1	korunovační
kodexu	kodex	k1gInSc2	kodex
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pražských	pražský	k2eAgInPc2d1	pražský
Emauz	Emauzy	k1gInPc2	Emauzy
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1390	[number]	k4	1390
<g/>
)	)	kIx)	)
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellem	Jagell	k1gMnSc7	Jagell
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
ženou	hnát	k5eAaImIp3nP	hnát
Hedvikou	Hedvika	k1gFnSc7	Hedvika
povoláni	povolán	k2eAgMnPc1d1	povolán
mniši	mnich	k1gMnPc1	mnich
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
nově	nově	k6eAd1	nově
založenému	založený	k2eAgInSc3d1	založený
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
krakovském	krakovský	k2eAgNnSc6d1	Krakovské
předměstí	předměstí	k1gNnSc6	předměstí
Kleparz	Kleparz	k1gInSc4	Kleparz
<g/>
;	;	kIx,	;
slovanský-hlaholský	slovanskýlaholský	k2eAgInSc4d1	slovanský-hlaholský
ráz	ráz	k1gInSc4	ráz
této	tento	k3xDgFnSc2	tento
fundace	fundace	k1gFnSc2	fundace
pak	pak	k6eAd1	pak
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
přehled	přehled	k1gInSc1	přehled
písmen	písmeno	k1gNnPc2	písmeno
hlaholice	hlaholice	k1gFnSc2	hlaholice
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
oblé	oblý	k2eAgFnSc6d1	oblá
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc6d2	starší
<g/>
)	)	kIx)	)
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
přidávat	přidávat	k5eAaImF	přidávat
písmenům	písmeno	k1gNnPc3	písmeno
i	i	k9	i
číselný	číselný	k2eAgInSc4d1	číselný
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
samostatné	samostatný	k2eAgInPc4d1	samostatný
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
číslice	číslice	k1gFnPc4	číslice
neexistovaly	existovat	k5eNaImAgInP	existovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
i	i	k9	i
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
zobrazení	zobrazení	k1gNnSc4	zobrazení
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
sloupci	sloupec	k1gInSc6	sloupec
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
unicodový	unicodový	k2eAgInSc4d1	unicodový
font	font	k1gInSc4	font
s	s	k7c7	s
hlaholicí	hlaholice	k1gFnSc7	hlaholice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odtud	odtud	k6eAd1	odtud
(	(	kIx(	(
<g/>
font	font	k1gInSc1	font
Dilyana	Dilyana	k1gFnSc1	Dilyana
<g/>
.	.	kIx.	.
<g/>
ttf	ttf	k?	ttf
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hlaholici	hlaholice	k1gFnSc4	hlaholice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
unicode	unicod	k1gInSc5	unicod
rezervován	rezervován	k2eAgInSc1d1	rezervován
blok	blok	k1gInSc1	blok
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
C	C	kA	C
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
..	..	k?	..
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
F	F	kA	F
(	(	kIx(	(
<g/>
Glagolitic	Glagolitice	k1gFnPc2	Glagolitice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1372	[number]	k4	1372
založil	založit	k5eAaPmAgMnS	založit
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Slovanech	Slovan	k1gInPc6	Slovan
Emauzský	emauzský	k2eAgInSc4d1	emauzský
klášter	klášter	k1gInSc4	klášter
a	a	k8xC	a
povolal	povolat	k5eAaPmAgMnS	povolat
do	do	k7c2	do
něho	on	k3xPp3gInSc2	on
benediktiny-hlaholity	benediktinylaholit	k1gInPc1	benediktiny-hlaholit
z	z	k7c2	z
Charvátska	Charvátsko	k1gNnSc2	Charvátsko
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
psalo	psát	k5eAaImAgNnS	psát
charvátskou	charvátský	k2eAgFnSc7d1	Charvátská
hlaholicí	hlaholice	k1gFnSc7	hlaholice
hranatého	hranatý	k2eAgInSc2d1	hranatý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaPmNgFnS	napsat
například	například	k6eAd1	například
Bible	bible	k1gFnSc1	bible
hlaholská	hlaholský	k2eAgFnSc1d1	hlaholská
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
psány	psát	k5eAaImNgInP	psát
nejenom	nejenom	k6eAd1	nejenom
spisy	spis	k1gInPc1	spis
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
hlaholice	hlaholice	k1gFnSc2	hlaholice
se	se	k3xPyFc4	se
přepisovaly	přepisovat	k5eAaImAgInP	přepisovat
i	i	k9	i
některé	některý	k3yIgInPc1	některý
staročeské	staročeský	k2eAgInPc1d1	staročeský
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
commons	commons	k6eAd1	commons
<g/>
:	:	kIx,	:
<g/>
Category	Categor	k1gInPc1	Categor
<g/>
:	:	kIx,	:
<g/>
Angular	Angular	k1gInSc1	Angular
Glagolitic	Glagolitice	k1gFnPc2	Glagolitice
letters	letters	k6eAd1	letters
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hlaholského	hlaholský	k2eAgNnSc2d1	hlaholské
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
ligatur	ligatura	k1gFnPc2	ligatura
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejenom	nejenom	k6eAd1	nejenom
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
čtyř	čtyři	k4xCgNnPc2	čtyři
písmen	písmeno	k1gNnPc2	písmeno
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
charvátskohlaholském	charvátskohlaholský	k2eAgNnSc6d1	charvátskohlaholský
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
například	například	k6eAd1	například
Bible	bible	k1gFnSc1	bible
hlaholská	hlaholský	k2eAgFnSc1d1	hlaholská
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
ligatury	ligatura	k1gFnSc2	ligatura
vznikaly	vznikat	k5eAaImAgInP	vznikat
aa	aa	k?	aa
<g/>
,	,	kIx,	,
ga	ga	k?	ga
<g/>
,	,	kIx,	,
za	za	k7c7	za
<g/>
,	,	kIx,	,
da	da	k?	da
vd	vd	k?	vd
<g/>
,	,	kIx,	,
vr	vr	k0	vr
<g/>
,	,	kIx,	,
vrz	vrz	k1gInSc1	vrz
<g/>
,	,	kIx,	,
vrt	vrt	k1gInSc1	vrt
<g/>
,	,	kIx,	,
vt	vt	k?	vt
<g/>
,	,	kIx,	,
vtv	vtv	k?	vtv
<g/>
,	,	kIx,	,
vz	vz	k?	vz
<g/>
,	,	kIx,	,
vzv	vzv	k?	vzv
<g/>
,	,	kIx,	,
vzd	vzd	k?	vzd
<g/>
,	,	kIx,	,
vp	vp	k?	vp
<g/>
,	,	kIx,	,
vl	vl	k?	vl
dv	dv	k?	dv
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dž	dž	k?	dž
el	ela	k1gFnPc2	ela
<g/>
,	,	kIx,	,
mžt	mžt	k?	mžt
zd	zd	k?	zd
<g/>
,	,	kIx,	,
zl	zl	k?	zl
<g/>
,	,	kIx,	,
zr	zr	k?	zr
<g/>
,	,	kIx,	,
zv	zv	k?	zv
<g/>
,	,	kIx,	,
zt	zt	k?	zt
ml	ml	kA	ml
<g/>
,	,	kIx,	,
mž	mž	k?	mž
<g/>
,	,	kIx,	,
mč	mč	k?	mč
lv	lv	k?	lv
<g/>
,	,	kIx,	,
ll	ll	k?	ll
pl	pl	k?	pl
<g/>
,	,	kIx,	,
przd	przd	k1gMnSc1	przd
<g/>
,	,	kIx,	,
pv	pv	k?	pv
<g/>
,	,	kIx,	,
prvd	prvd	k1gMnSc1	prvd
tr	tr	k?	tr
<g/>
,	,	kIx,	,
tv	tv	k?	tv
<g/>
,	,	kIx,	,
tl	tl	k?	tl
<g/>
,	,	kIx,	,
tvrd	tvrdo	k1gNnPc2	tvrdo
<g/>
,	,	kIx,	,
tz	tz	k?	tz
chv	chv	k?	chv
jud	judo	k1gNnPc2	judo
<g/>
,	,	kIx,	,
juda	judo	k1gNnSc2	judo
nd	nd	k?	nd
<g/>
,	,	kIx,	,
ng	ng	k?	ng
Tyto	tento	k3xDgFnPc1	tento
ligatury	ligatura	k1gFnPc1	ligatura
mohou	moct	k5eAaImIp3nP	moct
dnes	dnes	k6eAd1	dnes
lehce	lehko	k6eAd1	lehko
komplikovat	komplikovat	k5eAaBmF	komplikovat
četbu	četba	k1gFnSc4	četba
textu	text	k1gInSc2	text
čtenáři	čtenář	k1gMnSc3	čtenář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gNnPc4	on
není	být	k5eNaImIp3nS	být
zvyklý	zvyklý	k2eAgInSc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Fucic	Fucic	k1gMnSc1	Fucic
<g/>
,	,	kIx,	,
Branko	branka	k1gFnSc5	branka
<g/>
:	:	kIx,	:
Glagoljski	Glagoljsk	k1gMnSc3	Glagoljsk
natpisi	natpis	k1gMnSc3	natpis
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
In	In	k1gFnSc1	In
<g/>
:	:	kIx,	:
Djela	Djela	k1gFnSc1	Djela
Jugoslavenske	Jugoslavensk	k1gFnSc2	Jugoslavensk
Akademije	Akademije	k1gFnSc2	Akademije
Znanosti	Znanost	k1gFnSc2	Znanost
i	i	k8xC	i
Umjetnosti	Umjetnost	k1gFnSc2	Umjetnost
<g/>
,	,	kIx,	,
knjiga	knjiga	k1gFnSc1	knjiga
57	[number]	k4	57
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zagreb	Zagreb	k1gInSc1	Zagreb
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
420	[number]	k4	420
p.	p.	k?	p.
Fullerton	Fullerton	k1gInSc1	Fullerton
<g/>
,	,	kIx,	,
Sharon	Sharon	k1gInSc1	Sharon
Golke	Golke	k1gFnSc1	Golke
<g/>
:	:	kIx,	:
Paleographic	Paleographic	k1gMnSc1	Paleographic
Methods	Methodsa	k1gFnPc2	Methodsa
Used	Used	k1gMnSc1	Used
in	in	k?	in
Dating	Dating	k1gInSc1	Dating
Cyrillic	Cyrillice	k1gFnPc2	Cyrillice
and	and	k?	and
Glagolitic	Glagolitice	k1gFnPc2	Glagolitice
Slavic	slavice	k1gFnPc2	slavice
Manuscripts	Manuscripts	k1gInSc1	Manuscripts
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
Slavic	slavice	k1gFnPc2	slavice
Papers	Papers	k1gInSc1	Papers
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
93	[number]	k4	93
p.	p.	k?	p.
Gosev	Gosva	k1gFnPc2	Gosva
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
:	:	kIx,	:
Rilszki	Rilszki	k1gNnSc1	Rilszki
glagolicseszki	glagolicseszk	k1gFnSc2	glagolicseszk
lisztove	lisztov	k1gInSc5	lisztov
<g/>
.	.	kIx.	.
</s>
<s>
Szofia	Szofia	k1gFnSc1	Szofia
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
130	[number]	k4	130
p.	p.	k?	p.
V.	V.	kA	V.
Jagic	Jagic	k1gMnSc1	Jagic
Glagolitica	Glagolitica	k1gMnSc1	Glagolitica
<g/>
.	.	kIx.	.
</s>
<s>
Würdigung	Würdigung	k1gMnSc1	Würdigung
neuentdeckter	neuentdeckter	k1gMnSc1	neuentdeckter
Fragmente	fragment	k1gInSc5	fragment
<g/>
,	,	kIx,	,
Wien	Wiena	k1gFnPc2	Wiena
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
Miklas	Miklas	k1gMnSc1	Miklas
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Glagolitica	Glagolitica	k1gFnSc1	Glagolitica
<g/>
:	:	kIx,	:
zum	zum	k?	zum
Ursprung	Ursprung	k1gInSc1	Ursprung
der	drát	k5eAaImRp2nS	drát
slavischen	slavischna	k1gFnPc2	slavischna
Schriftkultur	Schriftkultura	k1gFnPc2	Schriftkultura
<g/>
,	,	kIx,	,
Wien	Wiena	k1gFnPc2	Wiena
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Steller	Steller	k1gMnSc1	Steller
<g/>
,	,	kIx,	,
Lea-Katharina	Lea-Katharina	k1gMnSc1	Lea-Katharina
<g/>
:	:	kIx,	:
A	a	k8xC	a
glagolita	glagolita	k1gMnSc1	glagolita
írás	írása	k1gFnPc2	írása
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
B.	B.	kA	B.
<g/>
Virághalmy	Virághalma	k1gFnSc2	Virághalma
<g/>
,	,	kIx,	,
Lea	Lea	k1gFnSc1	Lea
<g/>
:	:	kIx,	:
Paleográfiai	Paleográfia	k1gFnPc1	Paleográfia
kalandozások	kalandozások	k1gInSc4	kalandozások
<g/>
.	.	kIx.	.
</s>
<s>
Szentendre	Szentendr	k1gInSc5	Szentendr
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
963-450-922-3	[number]	k4	963-450-922-3
Vais	Vais	k1gMnSc1	Vais
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
:	:	kIx,	:
Abecedarivm	Abecedarivm	k1gMnSc1	Abecedarivm
Palaeoslovenicvm	Palaeoslovenicvm	k1gMnSc1	Palaeoslovenicvm
in	in	k?	in
usum	usum	k1gInSc1	usum
glagolitarum	glagolitarum	k1gInSc1	glagolitarum
<g/>
.	.	kIx.	.
</s>
<s>
Veglae	Veglaat	k5eAaPmIp3nS	Veglaat
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Krk	krk	k1gInSc4	krk
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
XXXVI	XXXVI	kA	XXXVI
<g/>
,	,	kIx,	,
74	[number]	k4	74
p.	p.	k?	p.
Vajs	Vajs	k1gInSc1	Vajs
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
:	:	kIx,	:
Rukověť	rukověť	k1gFnSc1	rukověť
hlaholské	hlaholský	k2eAgFnSc2d1	hlaholská
paleografie	paleografie	k1gFnSc2	paleografie
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc4	uvedení
do	do	k7c2	do
knižního	knižní	k2eAgNnSc2d1	knižní
písma	písmo	k1gNnSc2	písmo
hlaholského	hlaholský	k2eAgNnSc2d1	hlaholské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
178	[number]	k4	178
p	p	k?	p
<g/>
,	,	kIx,	,
LIV	LIV	kA	LIV
<g/>
.	.	kIx.	.
tab	tab	kA	tab
<g/>
.	.	kIx.	.
Cyrilice	cyrilice	k1gFnSc2	cyrilice
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
runy	runa	k1gFnPc1	runa
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hlaholice	hlaholice	k1gFnSc2	hlaholice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kyjevské	kyjevský	k2eAgInPc1d1	kyjevský
listy	list	k1gInPc1	list
-	-	kIx~	-
ukázka	ukázka	k1gFnSc1	ukázka
hlaholského	hlaholský	k2eAgInSc2d1	hlaholský
textu	text	k1gInSc2	text
Misal	Misal	k1gMnSc1	Misal
1483	[number]	k4	1483
-	-	kIx~	-
hrvatski	hrvatski	k6eAd1	hrvatski
prvotisak	prvotisak	k1gMnSc1	prvotisak
Vrbnički	Vrbničk	k1gFnSc2	Vrbničk
statut	statut	k1gInSc1	statut
1380	[number]	k4	1380
<g/>
/	/	kIx~	/
<g/>
1527	[number]	k4	1527
Istarski	Istarski	k1gNnSc7	Istarski
razvod	razvoda	k1gFnPc2	razvoda
1325	[number]	k4	1325
<g/>
/	/	kIx~	/
<g/>
1546	[number]	k4	1546
</s>
