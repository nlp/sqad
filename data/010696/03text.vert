<p>
<s>
Transportní	transportní	k2eAgFnSc1d1	transportní
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
čtvrté	čtvrtý	k4xOgFnSc2	čtvrtý
vrstvy	vrstva	k1gFnSc2	vrstva
modelu	model	k1gInSc2	model
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
OSI	OSI	kA	OSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
originále	originál	k1gInSc6	originál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
transport	transport	k1gInSc1	transport
layer	layra	k1gFnPc2	layra
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
adresovat	adresovat	k5eAaBmF	adresovat
přímo	přímo	k6eAd1	přímo
aplikace	aplikace	k1gFnPc4	aplikace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
protokolech	protokol	k1gInPc6	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
pomocí	pomocí	k7c2	pomocí
čísel	číslo	k1gNnPc2	číslo
portů	port	k1gInPc2	port
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
transparentní	transparentní	k2eAgMnSc1d1	transparentní
<g/>
,	,	kIx,	,
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
s	s	k7c7	s
požadovanou	požadovaný	k2eAgFnSc7d1	požadovaná
kvalitou	kvalita	k1gFnSc7	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
různé	různý	k2eAgFnPc4d1	různá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
přenosových	přenosový	k2eAgFnPc2d1	přenosová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nP	provádět
převod	převod	k1gInSc4	převod
transportních	transportní	k2eAgFnPc2d1	transportní
adres	adresa	k1gFnPc2	adresa
na	na	k7c6	na
síťové	síťový	k2eAgFnSc6d1	síťová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestará	starat	k5eNaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c6	o
směrování	směrování	k1gNnSc6	směrování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Analýza	analýza	k1gFnSc1	analýza
==	==	k?	==
</s>
</p>
<p>
<s>
Transportní	transportní	k2eAgFnSc1d1	transportní
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
doručení	doručení	k1gNnSc4	doručení
dat	datum	k1gNnPc2	datum
k	k	k7c3	k
příslušnému	příslušný	k2eAgInSc3d1	příslušný
aplikačnímu	aplikační	k2eAgInSc3d1	aplikační
procesu	proces	k1gInSc3	proces
na	na	k7c6	na
hostitelském	hostitelský	k2eAgInSc6d1	hostitelský
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
statistické	statistický	k2eAgNnSc4d1	statistické
multiplexování	multiplexování	k1gNnSc4	multiplexování
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
aplikačních	aplikační	k2eAgInPc2d1	aplikační
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vytváření	vytváření	k1gNnSc4	vytváření
packetů	packet	k1gInPc2	packet
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
přidávání	přidávání	k1gNnSc1	přidávání
zdrojových	zdrojový	k2eAgNnPc2d1	zdrojové
a	a	k8xC	a
cílových	cílový	k2eAgNnPc2d1	cílové
čísel	číslo	k1gNnPc2	číslo
portů	port	k1gInPc2	port
do	do	k7c2	do
hlavičky	hlavička	k1gFnSc2	hlavička
každého	každý	k3xTgInSc2	každý
packetu	packet	k1gInSc2	packet
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
z	z	k7c2	z
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zdrojovou	zdrojový	k2eAgFnSc7d1	zdrojová
adresou	adresa	k1gFnSc7	adresa
<g/>
,	,	kIx,	,
cílovou	cílový	k2eAgFnSc7d1	cílová
adresou	adresa	k1gFnSc7	adresa
a	a	k8xC	a
čísly	číslo	k1gNnPc7	číslo
portů	port	k1gInPc2	port
tvoří	tvořit	k5eAaImIp3nS	tvořit
síťový	síťový	k2eAgInSc4d1	síťový
socket	socket	k1gInSc4	socket
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
identifikační	identifikační	k2eAgFnSc1d1	identifikační
adresa	adresa	k1gFnSc1	adresa
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modelu	model	k1gInSc6	model
OSI	OSI	kA	OSI
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
podporována	podporovat	k5eAaImNgFnS	podporovat
Relační	relační	k2eAgFnSc7d1	relační
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
protokoly	protokol	k1gInPc1	protokol
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
TCP	TCP	kA	TCP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
UDP	UDP	kA	UDP
<g/>
,	,	kIx,	,
podporují	podporovat	k5eAaImIp3nP	podporovat
virtuální	virtuální	k2eAgInPc4d1	virtuální
okruhy	okruh	k1gInPc4	okruh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
poskytování	poskytování	k1gNnSc4	poskytování
spojované	spojovaný	k2eAgFnSc2d1	spojovaná
komunikace	komunikace	k1gFnSc2	komunikace
přes	přes	k7c4	přes
základní	základní	k2eAgFnSc4d1	základní
packetově	packetově	k6eAd1	packetově
orientovanou	orientovaný	k2eAgFnSc4d1	orientovaná
síť	síť	k1gFnSc4	síť
datagramu	datagram	k1gInSc2	datagram
<g/>
.	.	kIx.	.
</s>
<s>
Bytový	bytový	k2eAgInSc1d1	bytový
proud	proud	k1gInSc1	proud
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
doručován	doručován	k2eAgInSc1d1	doručován
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
packetovou	packetový	k2eAgFnSc4d1	packetový
komunikaci	komunikace	k1gFnSc4	komunikace
pro	pro	k7c4	pro
aplikační	aplikační	k2eAgInPc4d1	aplikační
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
patří	patřit	k5eAaImIp3nS	patřit
navázání	navázání	k1gNnSc4	navázání
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc2	rozdělení
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
proudu	proud	k1gInSc2	proud
dat	datum	k1gNnPc2	datum
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
packetů	packet	k1gInPc2	packet
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
segmenty	segment	k1gInPc7	segment
<g/>
,	,	kIx,	,
číslování	číslování	k1gNnSc1	číslování
segmentů	segment	k1gInPc2	segment
a	a	k8xC	a
přeorganizování	přeorganizování	k1gNnPc2	přeorganizování
"	"	kIx"	"
<g/>
neseřazených	seřazený	k2eNgMnPc2d1	seřazený
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
out-of-order	outfrder	k1gInSc1	out-of-order
<g/>
)	)	kIx)	)
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
protokoly	protokol	k1gInPc1	protokol
transportních	transportní	k2eAgFnPc2d1	transportní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
TCP	TCP	kA	TCP
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
UDP	UDP	kA	UDP
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
spolehlivou	spolehlivý	k2eAgFnSc7d1	spolehlivá
koncovou	koncový	k2eAgFnSc7d1	koncová
(	(	kIx(	(
<g/>
end-to-end	endonda	k1gFnPc2	end-to-enda
<g/>
)	)	kIx)	)
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
detekce	detekce	k1gFnSc1	detekce
a	a	k8xC	a
oprava	oprava	k1gFnSc1	oprava
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
opakováním	opakování	k1gNnSc7	opakování
(	(	kIx(	(
<g/>
ARQ	ARQ	kA	ARQ
protokol	protokol	k1gInSc1	protokol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
ARQ	ARQ	kA	ARQ
protokol	protokol	k1gInSc4	protokol
také	také	k6eAd1	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
kontrolu	kontrola	k1gFnSc4	kontrola
toku	tok	k1gInSc2	tok
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
flow	flow	k?	flow
control	control	k1gInSc1	control
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zkombinována	zkombinovat	k5eAaPmNgFnS	zkombinovat
s	s	k7c7	s
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
vyvarování	vyvarování	k1gNnSc4	vyvarování
se	se	k3xPyFc4	se
zahlcenosti	zahlcenost	k1gFnSc2	zahlcenost
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UDP	UDP	kA	UDP
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
protokol	protokol	k1gInSc4	protokol
a	a	k8xC	a
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
virtuální	virtuální	k2eAgInPc4d1	virtuální
okruhy	okruh	k1gInPc4	okruh
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nS	muset
delegovat	delegovat	k5eAaBmF	delegovat
aplikační	aplikační	k2eAgInPc4d1	aplikační
programy	program	k1gInPc4	program
pro	pro	k7c4	pro
obstarávání	obstarávání	k1gNnSc4	obstarávání
těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
UDP	UDP	kA	UDP
packety	packet	k1gInPc1	packet
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
než	než	k8xS	než
segmenty	segment	k1gInPc1	segment
nazývají	nazývat	k5eAaImIp3nP	nazývat
datagramy	datagram	k1gInPc4	datagram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TCP	TCP	kA	TCP
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
HTTP	HTTP	kA	HTTP
pro	pro	k7c4	pro
prohlížení	prohlížení	k1gNnSc4	prohlížení
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
posílání	posílání	k1gNnSc2	posílání
emailů	email	k1gInPc2	email
<g/>
.	.	kIx.	.
</s>
<s>
UDP	UDP	kA	UDP
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
multicasting	multicasting	k1gInSc4	multicasting
(	(	kIx(	(
<g/>
zasílání	zasílání	k1gNnSc1	zasílání
dat	datum	k1gNnPc2	datum
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednomu	jeden	k4xCgNnSc3	jeden
síťovému	síťový	k2eAgNnSc3d1	síťové
připojení	připojení	k1gNnSc3	připojení
<g/>
)	)	kIx)	)
a	a	k8xC	a
broadcasting	broadcasting	k1gInSc1	broadcasting
(	(	kIx(	(
<g/>
zasílání	zasílání	k1gNnSc1	zasílání
dat	datum	k1gNnPc2	datum
všem	všecek	k3xTgNnPc3	všecek
síťovým	síťový	k2eAgNnPc3d1	síťové
připojením	připojení	k1gNnPc3	připojení
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
síti	síť	k1gFnSc6	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
opakované	opakovaný	k2eAgInPc1d1	opakovaný
přenosy	přenos	k1gInPc1	přenos
nejsou	být	k5eNaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
UDP	UDP	kA	UDP
typicky	typicky	k6eAd1	typicky
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
propustnost	propustnost	k1gFnSc4	propustnost
a	a	k8xC	a
kratší	krátký	k2eAgFnSc4d2	kratší
odezvu	odezva	k1gFnSc4	odezva
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
multimédií	multimédium	k1gNnPc2	multimédium
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
občasná	občasný	k2eAgFnSc1d1	občasná
ztráta	ztráta	k1gFnSc1	ztráta
packetů	packet	k1gMnPc2	packet
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
akceptována	akceptován	k2eAgFnSc1d1	akceptována
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
pro	pro	k7c4	pro
IPTV	IPTV	kA	IPTV
a	a	k8xC	a
IP	IP	kA	IP
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
online	onlinout	k5eAaPmIp3nS	onlinout
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nejsou	být	k5eNaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
IP	IP	kA	IP
protokolech	protokol	k1gInPc6	protokol
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
X	X	kA	X
<g/>
.25	.25	k4	.25
<g/>
,	,	kIx,	,
Frame	Fram	k1gInSc5	Fram
Relay	Relaum	k1gNnPc7	Relaum
a	a	k8xC	a
ATM	ATM	kA	ATM
(	(	kIx(	(
<g/>
Asynchronous	Asynchronous	k1gInSc1	Asynchronous
Transfer	transfer	k1gInSc1	transfer
Mode	modus	k1gInSc5	modus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
orientované	orientovaný	k2eAgNnSc1d1	orientované
připojení	připojení	k1gNnSc1	připojení
spíše	spíše	k9	spíše
realizováno	realizovat	k5eAaBmNgNnS	realizovat
v	v	k7c6	v
sítové	sítový	k2eAgFnSc6d1	sítová
nebo	nebo	k8xC	nebo
v	v	k7c6	v
linkové	linkový	k2eAgFnSc6d1	Linková
(	(	kIx(	(
<g/>
spojové	spojový	k2eAgFnSc6d1	spojová
<g/>
)	)	kIx)	)
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
transportní	transportní	k2eAgFnSc6d1	transportní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
X	X	kA	X
<g/>
.25	.25	k4	.25
<g/>
,	,	kIx,	,
v	v	k7c6	v
modemové	modemový	k2eAgFnSc6d1	modemová
telefonní	telefonní	k2eAgFnSc6d1	telefonní
síti	síť	k1gFnSc6	síť
a	a	k8xC	a
v	v	k7c6	v
bezdrátových	bezdrátový	k2eAgInPc6d1	bezdrátový
komunikačních	komunikační	k2eAgInPc6d1	komunikační
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
uzel	uzel	k1gInSc1	uzel
<g/>
–	–	k?	–
<g/>
uzel	uzel	k1gInSc1	uzel
komunikace	komunikace	k1gFnSc2	komunikace
implementována	implementován	k2eAgFnSc1d1	implementována
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
vrstvách	vrstva	k1gFnPc6	vrstva
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifikace	specifikace	k1gFnSc1	specifikace
propojovacího	propojovací	k2eAgInSc2d1	propojovací
protokolu	protokol	k1gInSc2	protokol
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
OSI	OSI	kA	OSI
definuje	definovat	k5eAaBmIp3nS	definovat
pět	pět	k4xCc4	pět
tříd	třída	k1gFnPc2	třída
transportních	transportní	k2eAgInPc2d1	transportní
protokolů	protokol	k1gInPc2	protokol
<g/>
:	:	kIx,	:
počínaje	počínaje	k7c7	počínaje
TP	TP	kA	TP
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgFnSc4d1	poskytující
minimální	minimální	k2eAgFnSc4d1	minimální
obnovu	obnova	k1gFnSc4	obnova
při	při	k7c6	při
chybě	chyba	k1gFnSc6	chyba
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
TP	TP	kA	TP
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
speciálně	speciálně	k6eAd1	speciálně
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
spolehlivé	spolehlivý	k2eAgFnPc4d1	spolehlivá
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
AEP	AEP	kA	AEP
</s>
</p>
<p>
<s>
AMTP	AMTP	kA	AMTP
</s>
</p>
<p>
<s>
AppleTalk	AppleTalk	k6eAd1	AppleTalk
Transaction	Transaction	k1gInSc1	Transaction
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CUDP	CUDP	kA	CUDP
</s>
</p>
<p>
<s>
IL	IL	kA	IL
</s>
</p>
<p>
<s>
NBP	NBP	kA	NBP
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
NetBEUI	NetBEUI	k?	NetBEUI
</s>
</p>
<p>
<s>
RTMP	RTMP	kA	RTMP
</s>
</p>
<p>
<s>
SMB	SMB	kA	SMB
</s>
</p>
<p>
<s>
IPX	IPX	kA	IPX
<g/>
/	/	kIx~	/
<g/>
SPX	SPX	kA	SPX
</s>
</p>
<p>
<s>
TCP	TCP	kA	TCP
</s>
</p>
<p>
<s>
UDP	UDP	kA	UDP
</s>
</p>
<p>
<s>
SCTP	SCTP	kA	SCTP
</s>
</p>
<p>
<s>
RTP	RTP	kA	RTP
</s>
</p>
<p>
<s>
==	==	k?	==
Porovnání	porovnání	k1gNnSc1	porovnání
protokolů	protokol	k1gInPc2	protokol
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Porovnání	porovnání	k1gNnSc1	porovnání
transportních	transportní	k2eAgInPc2d1	transportní
protokolů	protokol	k1gInPc2	protokol
OSI	OSI	kA	OSI
==	==	k?	==
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
8073	[number]	k4	8073
<g/>
/	/	kIx~	/
<g/>
ITU-T	ITU-T	k1gFnPc2	ITU-T
doporučení	doporučení	k1gNnSc2	doporučení
X	X	kA	X
<g/>
.224	.224	k4	.224
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
pět	pět	k4xCc4	pět
tříd	třída	k1gFnPc2	třída
spojovaných	spojovaný	k2eAgInPc2d1	spojovaný
protokolů	protokol	k1gInPc2	protokol
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
označovaných	označovaný	k2eAgInPc2d1	označovaný
jako	jako	k8xC	jako
transportní	transportní	k2eAgInPc1d1	transportní
protokol	protokol	k1gInSc1	protokol
třídy	třída	k1gFnSc2	třída
0	[number]	k4	0
(	(	kIx(	(
<g/>
TP	TP	kA	TP
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
až	až	k9	až
4	[number]	k4	4
(	(	kIx(	(
<g/>
TP	TP	kA	TP
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
TP0	TP0	k4	TP0
neprovádí	provádět	k5eNaImIp3nS	provádět
žádnou	žádný	k3yNgFnSc4	žádný
opravu	oprava	k1gFnSc4	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
nad	nad	k7c7	nad
síťovou	síťový	k2eAgFnSc7d1	síťová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
bezchybná	bezchybný	k2eAgNnPc4d1	bezchybné
spojení	spojení	k1gNnPc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
TP4	TP4	k4	TP4
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
podobá	podobat	k5eAaImIp3nS	podobat
TCP	TCP	kA	TCP
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
TCP	TCP	kA	TCP
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
funkce	funkce	k1gFnSc1	funkce
jako	jako	k8xC	jako
nenásilné	násilný	k2eNgNnSc1d1	nenásilné
zavření	zavření	k1gNnSc1	zavření
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
OSI	OSI	kA	OSI
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
relační	relační	k2eAgFnSc2d1	relační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
třídy	třída	k1gFnPc1	třída
spojovaných	spojovaný	k2eAgInPc2d1	spojovaný
protokolů	protokol	k1gInPc2	protokol
OSI	OSI	kA	OSI
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
přednostní	přednostní	k2eAgInSc4d1	přednostní
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
hranice	hranice	k1gFnPc4	hranice
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgFnPc1d1	detailní
charakteristiky	charakteristika	k1gFnPc1	charakteristika
tříd	třída	k1gFnPc2	třída
jsou	být	k5eAaImIp3nP	být
přehledně	přehledně	k6eAd1	přehledně
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
OSI	OSI	kA	OSI
definuje	definovat	k5eAaBmIp3nS	definovat
i	i	k9	i
nespojovaný	spojovaný	k2eNgInSc1d1	nespojovaný
transportní	transportní	k2eAgInSc1d1	transportní
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
specifikovaný	specifikovaný	k2eAgInSc1d1	specifikovaný
standardem	standard	k1gInSc7	standard
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
8602	[number]	k4	8602
a	a	k8xC	a
ITU-T	ITU-T	k1gMnSc3	ITU-T
doporučením	doporučení	k1gNnSc7	doporučení
X	X	kA	X
<g/>
.234	.234	k4	.234
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
