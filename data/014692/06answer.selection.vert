<s>
Generátor	generátor	k1gInSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
(	(	kIx(
<g/>
EMP	EMP	kA
generátor	generátor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
generuje	generovat	k5eAaImIp3nS
silné	silný	k2eAgNnSc4d1
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1
má	mít	k5eAaImIp3nS
několik	několik	k4yIc4
důsledků	důsledek	k1gInPc2
<g/>
.	.	kIx.
</s>