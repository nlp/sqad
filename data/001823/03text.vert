<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ɡ	ɡ	k?	ɡ
apɔ	apɔ	k?	apɔ
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Albert	Albert	k1gMnSc1	Albert
Wladimir	Wladimir	k1gMnSc1	Wladimir
Alexandre	Alexandr	k1gInSc5	Alexandr
Apollinare	Apollinar	k1gMnSc5	Apollinar
de	de	k?	de
Kostrowitzky	Kostrowitzka	k1gFnPc4	Kostrowitzka
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1880	[number]	k4	1880
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
anarchista	anarchista	k1gMnSc1	anarchista
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Angelica	Angelic	k2eAgFnSc1d1	Angelica
Kostrowicka	Kostrowicka	k1gFnSc1	Kostrowicka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
šlechtična	šlechtična	k1gFnSc1	šlechtična
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
blízko	blízko	k7c2	blízko
Nowogródek	Nowogródka	k1gFnPc2	Nowogródka
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
dohadů	dohad	k1gInPc2	dohad
to	ten	k3xDgNnSc1	ten
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
Francesco	Francesco	k1gMnSc1	Francesco
Flugi	Flug	k1gFnSc2	Flug
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aspermont	Aspermont	k1gMnSc1	Aspermont
<g/>
,	,	kIx,	,
švýcarsko-italský	švýcarskotalský	k2eAgMnSc1d1	švýcarsko-italský
aristokrat	aristokrat	k1gMnSc1	aristokrat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
Apollinairova	Apollinairův	k2eAgInSc2d1	Apollinairův
života	život	k1gInSc2	život
záhy	záhy	k6eAd1	záhy
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
La	la	k1gNnSc4	la
Santé	Santý	k2eAgNnSc4d1	Santé
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
krádeže	krádež	k1gFnSc2	krádež
fénických	fénický	k2eAgFnPc2d1	fénická
sošek	soška	k1gFnPc2	soška
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
aféry	aféra	k1gFnPc4	aféra
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
krádeže	krádež	k1gFnSc2	krádež
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
u	u	k7c2	u
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
baterie	baterie	k1gFnSc1	baterie
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
seržant	seržant	k1gMnSc1	seržant
a	a	k8xC	a
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
rota	rota	k1gFnSc1	rota
u	u	k7c2	u
96	[number]	k4	96
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
podporučík	podporučík	k1gMnSc1	podporučík
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
raněn	ranit	k5eAaPmNgMnS	ranit
a	a	k8xC	a
trepanován	trepanovat	k5eAaBmNgInS	trepanovat
pod	pod	k7c7	pod
chloroformem	chloroform	k1gInSc7	chloroform
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rekonvalescenci	rekonvalescence	k1gFnSc6	rekonvalescence
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
u	u	k7c2	u
odboru	odbor	k1gInSc2	odbor
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
poručíka	poručík	k1gMnSc2	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
během	během	k7c2	během
pandemie	pandemie	k1gFnSc2	pandemie
na	na	k7c4	na
následky	následek	k1gInPc4	následek
viru	vir	k1gInSc2	vir
tzv.	tzv.	kA	tzv.
španělské	španělský	k2eAgFnSc2d1	španělská
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
několika	několik	k4yIc2	několik
časopisů	časopis	k1gInPc2	časopis
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ezopova	Ezopův	k2eAgFnSc1d1	Ezopova
hostina	hostina	k1gFnSc1	hostina
<g/>
,	,	kIx,	,
Pařížské	pařížský	k2eAgInPc1d1	pařížský
večery	večer	k1gInPc1	večer
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnSc1d1	moderní
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
Imoralistická	imoralistický	k2eAgFnSc1d1	imoralistický
revue	revue	k1gFnSc1	revue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
členů	člen	k1gMnPc2	člen
Montparnasské	Montparnasský	k2eAgFnSc2d1	Montparnasský
umělecké	umělecký	k2eAgFnSc2d1	umělecká
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
byli	být	k5eAaImAgMnP	být
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Salmon	Salmon	k1gMnSc1	Salmon
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Laurencin	Laurencin	k1gMnSc1	Laurencin
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Derain	Derain	k1gMnSc1	Derain
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
Cendrars	Cendrars	k1gInSc1	Cendrars
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
Reverdy	Reverd	k1gInPc7	Reverd
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Cocteau	Cocteaus	k1gInSc2	Cocteaus
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
Satie	Satie	k1gFnSc1	Satie
<g/>
,	,	kIx,	,
Ossip	Ossip	k1gInSc1	Ossip
Zadkine	Zadkin	k1gInSc5	Zadkin
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Chagall	Chagall	k1gMnSc1	Chagall
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
Duchamp	Duchamp	k1gMnSc1	Duchamp
<g/>
,	,	kIx,	,
Giorgio	Giorgio	k1gMnSc1	Giorgio
de	de	k?	de
Chirico	Chirico	k1gMnSc1	Chirico
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
portrétoval	portrétovat	k5eAaImAgMnS	portrétovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
francouzské	francouzský	k2eAgFnSc2d1	francouzská
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
propagoval	propagovat	k5eAaImAgMnS	propagovat
avantgardní	avantgardní	k2eAgNnSc4d1	avantgardní
malířství	malířství	k1gNnSc4	malířství
(	(	kIx(	(
<g/>
působil	působit	k5eAaImAgInS	působit
i	i	k9	i
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
kritik	kritik	k1gMnSc1	kritik
<g/>
)	)	kIx)	)
-	-	kIx~	-
především	především	k9	především
kubismus	kubismus	k1gInSc1	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc2	jeho
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
Alkoholy	alkohol	k1gInPc1	alkohol
a	a	k8xC	a
Kaligramy	Kaligram	k1gInPc1	Kaligram
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
mezníkem	mezník	k1gInSc7	mezník
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
francouzské	francouzský	k2eAgFnSc2d1	francouzská
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
vytyčily	vytyčit	k5eAaPmAgFnP	vytyčit
cestu	cesta	k1gFnSc4	cesta
modernímu	moderní	k2eAgNnSc3d1	moderní
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
skutečnosti	skutečnost	k1gFnSc2	skutečnost
a	a	k8xC	a
z	z	k7c2	z
nových	nový	k2eAgInPc2d1	nový
životních	životní	k2eAgInPc2d1	životní
pocitů	pocit	k1gInPc2	pocit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
básních	báseň	k1gFnPc6	báseň
spojil	spojit	k5eAaPmAgMnS	spojit
tradici	tradice	k1gFnSc4	tradice
francouzských	francouzský	k2eAgInPc2d1	francouzský
šansonů	šanson	k1gInPc2	šanson
<g/>
,	,	kIx,	,
německých	německý	k2eAgFnPc2d1	německá
romantických	romantický	k2eAgFnPc2d1	romantická
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
okultní	okultní	k2eAgFnSc2d1	okultní
inspirace	inspirace	k1gFnSc2	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
krásy	krása	k1gFnSc2	krása
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgInS	vrátit
poezii	poezie	k1gFnSc4	poezie
její	její	k3xOp3gInSc1	její
smysl	smysl	k1gInSc1	smysl
pro	pro	k7c4	pro
tajemno	tajemno	k1gNnSc4	tajemno
<g/>
,	,	kIx,	,
obohatil	obohatit	k5eAaPmAgMnS	obohatit
verše	verš	k1gInSc2	verš
kultem	kult	k1gInSc7	kult
každodenní	každodenní	k2eAgFnSc2d1	každodenní
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
estetického	estetický	k2eAgInSc2d1	estetický
dynamismu	dynamismus	k1gInSc2	dynamismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
volné	volný	k2eAgFnSc2d1	volná
asociace	asociace	k1gFnSc2	asociace
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
otevírající	otevírající	k2eAgFnSc4d1	otevírající
cestu	cesta	k1gFnSc4	cesta
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hroutí	hroutit	k5eAaImIp3nS	hroutit
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
hranice	hranice	k1gFnSc1	hranice
světa	svět	k1gInSc2	svět
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
prolínají	prolínat	k5eAaImIp3nP	prolínat
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
a	a	k8xC	a
minulost	minulost	k1gFnSc1	minulost
<g/>
,	,	kIx,	,
sen	sen	k1gInSc1	sen
a	a	k8xC	a
realita	realita	k1gFnSc1	realita
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
nového	nový	k2eAgInSc2d1	nový
lyrismu	lyrismus	k1gInSc2	lyrismus
své	svůj	k3xOyFgFnSc2	svůj
poezie	poezie	k1gFnSc2	poezie
usiloval	usilovat	k5eAaImAgInS	usilovat
zejména	zejména	k9	zejména
o	o	k7c4	o
zlidštění	zlidštění	k1gNnSc4	zlidštění
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
osvobození	osvobození	k1gNnSc2	osvobození
z	z	k7c2	z
konvencí	konvence	k1gFnPc2	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
verše	verš	k1gInPc1	verš
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
strhující	strhující	k2eAgFnSc7d1	strhující
oslavou	oslava	k1gFnSc7	oslava
života	život	k1gInSc2	život
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dnešními	dnešní	k2eAgMnPc7d1	dnešní
literárními	literární	k2eAgMnPc7d1	literární
vědci	vědec	k1gMnPc7	vědec
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
označovány	označovat	k5eAaImNgFnP	označovat
za	za	k7c4	za
kubofuturistické	kubofuturistický	k2eAgInPc4d1	kubofuturistický
-	-	kIx~	-
spojují	spojovat	k5eAaImIp3nP	spojovat
totiž	totiž	k9	totiž
prvky	prvek	k1gInPc1	prvek
kubismu	kubismus	k1gInSc2	kubismus
a	a	k8xC	a
futurismu	futurismus	k1gInSc2	futurismus
<g/>
:	:	kIx,	:
uvolnění	uvolnění	k1gNnSc3	uvolnění
struktury	struktura	k1gFnSc2	struktura
básně	báseň	k1gFnSc2	báseň
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
však	však	k9	však
její	její	k3xOp3gNnSc4	její
úplné	úplný	k2eAgNnSc4d1	úplné
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc1	odstranění
interpunkce	interpunkce	k1gFnSc2	interpunkce
<g/>
,	,	kIx,	,
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
,	,	kIx,	,
polytematičnost	polytematičnost	k1gFnSc1	polytematičnost
<g/>
,	,	kIx,	,
spojování	spojování	k1gNnSc1	spojování
představ	představa	k1gFnPc2	představa
na	na	k7c6	na
základě	základ	k1gInSc6	základ
asociací	asociace	k1gFnPc2	asociace
<g/>
...	...	k?	...
Pásmo	pásmo	k1gNnSc1	pásmo
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlé	Rychlé	k2eAgNnSc1d1	Rychlé
přemisťování	přemisťování	k1gNnSc1	přemisťování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
dynamiku	dynamika	k1gFnSc4	dynamika
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
splývají	splývat	k5eAaImIp3nP	splývat
v	v	k7c4	v
jediné	jediné	k1gNnSc4	jediné
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
samostatné	samostatný	k2eAgFnPc4d1	samostatná
významové	významový	k2eAgFnPc4d1	významová
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
má	mít	k5eAaImIp3nS	mít
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
stránka	stránka	k1gFnSc1	stránka
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc4d1	typický
pro	pro	k7c4	pro
dílo	dílo	k1gNnSc4	dílo
jsou	být	k5eAaImIp3nP	být
metafory	metafora	k1gFnSc2	metafora
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Wolkera	Wolker	k1gMnSc2	Wolker
(	(	kIx(	(
<g/>
Svatý	svatý	k2eAgInSc1d1	svatý
kopeček	kopeček	k1gInSc1	kopeček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Nezvala	Nezval	k1gMnSc2	Nezval
(	(	kIx(	(
<g/>
Edison	Edison	k1gMnSc1	Edison
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Biebla	Biebl	k1gMnSc2	Biebl
(	(	kIx(	(
<g/>
Nový	nový	k2eAgMnSc1d1	nový
Ikaros	Ikaros	k1gMnSc1	Ikaros
<g/>
)	)	kIx)	)
<g/>
...	...	k?	...
Alkoholy	alkohol	k1gInPc1	alkohol
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyvrcholení	vyvrcholení	k1gNnSc3	vyvrcholení
jeho	jeho	k3xOp3gFnSc2	jeho
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
nového	nový	k2eAgInSc2d1	nový
básnického	básnický	k2eAgInSc2d1	básnický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc2	vyjádření
rytmu	rytmus	k1gInSc2	rytmus
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kaligramy	Kaligram	k1gInPc1	Kaligram
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Bestiář	bestiář	k1gInSc1	bestiář
aneb	aneb	k?	aneb
Orfeův	Orfeův	k2eAgInSc1d1	Orfeův
průvod	průvod	k1gInSc1	průvod
Život	život	k1gInSc1	život
zasvětit	zasvětit	k5eAaPmF	zasvětit
lásce	láska	k1gFnSc3	láska
Je	být	k5eAaImIp3nS	být
Básně	báseň	k1gFnSc2	báseň
pro	pro	k7c4	pro
Lou	Lou	k1gFnSc4	Lou
Něžný	něžný	k2eAgMnSc1d1	něžný
jako	jako	k8xS	jako
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
Nalezené	nalezený	k2eAgFnSc2d1	nalezená
básně	báseň	k1gFnSc2	báseň
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
prutů	prut	k1gInPc2	prut
aneb	aneb	k?	aneb
rychtářovy	rychtářův	k2eAgFnPc1d1	Rychtářova
lásky	láska	k1gFnPc1	láska
Hrdinské	hrdinský	k2eAgFnSc2d1	hrdinská
činy	čina	k1gFnSc2	čina
mladého	mladý	k2eAgMnSc2d1	mladý
donchuana	donchuan	k1gMnSc2	donchuan
Prsy	prs	k1gInPc4	prs
Tiréziovy	Tiréziův	k2eAgInPc4d1	Tiréziův
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
název	název	k1gInSc1	název
surrealismus	surrealismus	k1gInSc1	surrealismus
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
podtitul	podtitul	k1gInSc4	podtitul
surrealistické	surrealistický	k2eAgNnSc4d1	surrealistické
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
než	než	k8xS	než
o	o	k7c4	o
drama	drama	k1gNnSc4	drama
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
libreto	libreto	k1gNnSc4	libreto
nebo	nebo	k8xC	nebo
scénář	scénář	k1gInSc1	scénář
pro	pro	k7c4	pro
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
pomíjí	pomíjet	k5eAaImIp3nS	pomíjet
všechny	všechen	k3xTgInPc4	všechen
stavební	stavební	k2eAgInPc4d1	stavební
prvky	prvek	k1gInPc4	prvek
tradičního	tradiční	k2eAgNnSc2d1	tradiční
dramatu	drama	k1gNnSc2	drama
(	(	kIx(	(
<g/>
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
charaktery	charakter	k1gInPc1	charakter
<g/>
,	,	kIx,	,
dramatický	dramatický	k2eAgInSc4d1	dramatický
konflikt	konflikt	k1gInSc4	konflikt
<g/>
)	)	kIx)	)
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
programovým	programový	k2eAgNnSc7d1	programové
dílem	dílo	k1gNnSc7	dílo
divadelní	divadelní	k2eAgFnSc2d1	divadelní
avantgardy	avantgarda	k1gFnSc2	avantgarda
a	a	k8xC	a
předchůdcem	předchůdce	k1gMnSc7	předchůdce
absurdního	absurdní	k2eAgNnSc2d1	absurdní
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrdinka	hrdinka	k1gFnSc1	hrdinka
je	být	k5eAaImIp3nS	být
feministka	feministka	k1gFnSc1	feministka
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odmítá	odmítat	k5eAaImIp3nS	odmítat
úděl	úděl	k1gInSc4	úděl
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
odhodí	odhodit	k5eAaPmIp3nS	odhodit
prsa	prsa	k1gNnPc4	prsa
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
mužem	muž	k1gMnSc7	muž
Tiréziem	Tirézius	k1gMnSc7	Tirézius
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
manžel	manžel	k1gMnSc1	manžel
zplodí	zplodit	k5eAaPmIp3nS	zplodit
během	během	k7c2	během
dne	den	k1gInSc2	den
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Apollinaire	Apollinair	k1gMnSc5	Apollinair
velmi	velmi	k6eAd1	velmi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
překládali	překládat	k5eAaImAgMnP	překládat
mj.	mj.	kA	mj.
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	zvát	k5eNaImAgMnS	zvát
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
Seznam	seznam	k1gInSc4	seznam
francouzských	francouzský	k2eAgMnPc2d1	francouzský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Surrealismus	surrealismus	k1gInSc1	surrealismus
Dadaismus	dadaismus	k1gInSc4	dadaismus
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
na	na	k7c4	na
projektu	projekt	k1gInSc2	projekt
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
Osoba	osoba	k1gFnSc1	osoba
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Osobní	osobní	k2eAgFnSc1d1	osobní
recitace	recitace	k1gFnSc1	recitace
básně	báseň	k1gFnSc2	báseň
Sous	Sous	k1gInSc1	Sous
le	le	k?	le
Pont	Pont	k1gInSc4	Pont
Mirabeu	Mirabea	k1gMnSc4	Mirabea
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
mp	mp	k?	mp
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
