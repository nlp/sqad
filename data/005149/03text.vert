<s>
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
je	být	k5eAaImIp3nS	být
plutoid	plutoid	k1gInSc4	plutoid
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
SDO	SDO	kA	SDO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pozorování	pozorování	k1gNnSc1	pozorování
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přesně	přesně	k6eAd1	přesně
stanovit	stanovit	k5eAaPmF	stanovit
jeho	jeho	k3xOp3gFnSc4	jeho
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgNnSc4d1	velké
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
průměr	průměr	k1gInSc1	průměr
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
upřesněn	upřesnit	k5eAaPmNgInS	upřesnit
na	na	k7c6	na
2326	[number]	k4	2326
±	±	k?	±
12	[number]	k4	12
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc4d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
znalostí	znalost	k1gFnPc2	znalost
průměr	průměr	k1gInSc1	průměr
2370	[number]	k4	2370
±	±	k?	±
20	[number]	k4	20
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Eris	Eris	k1gFnSc1	Eris
momentálně	momentálně	k6eAd1	momentálně
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
známé	známý	k2eAgNnSc4d1	známé
těleso	těleso	k1gNnSc4	těleso
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
tělesem	těleso	k1gNnSc7	těleso
nejhmotnějším	hmotný	k2eAgInPc3d3	hmotný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c4	o
27	[number]	k4	27
%	%	kIx~	%
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
objeviteli	objevitel	k1gMnSc6	objevitel
provizorně	provizorně	k6eAd1	provizorně
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Xena	Xenum	k1gNnSc2	Xenum
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2005	[number]	k4	2005
oznámili	oznámit	k5eAaPmAgMnP	oznámit
objevitelé	objevitel	k1gMnPc1	objevitel
Xeny	Xena	k1gFnSc2	Xena
<g/>
,	,	kIx,	,
že	že	k8xS	že
planetka	planetka	k1gFnSc1	planetka
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
pracovně	pracovně	k6eAd1	pracovně
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Gabrielle	Gabrielle	k1gInSc4	Gabrielle
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
postavy	postava	k1gFnSc2	postava
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yIgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
předběžné	předběžný	k2eAgNnSc4d1	předběžné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
diskusi	diskuse	k1gFnSc6	diskuse
na	na	k7c4	na
XXVI	XXVI	kA	XXVI
<g/>
.	.	kIx.	.
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
schválena	schválen	k2eAgFnSc1d1	schválena
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Plutem	Pluto	k1gMnSc7	Pluto
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
kategorie	kategorie	k1gFnSc2	kategorie
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Katalogové	katalogový	k2eAgNnSc1d1	Katalogové
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
definitivní	definitivní	k2eAgNnSc1d1	definitivní
jméno	jméno	k1gNnSc1	jméno
Eris	Eris	k1gFnSc1	Eris
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
a	a	k8xC	a
publikováno	publikovat	k5eAaBmNgNnS	publikovat
nomenklaturní	nomenklaturní	k2eAgFnSc7d1	nomenklaturní
komisí	komise	k1gFnSc7	komise
IAU	IAU	kA	IAU
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
dostal	dostat	k5eAaPmAgMnS	dostat
definitivní	definitivní	k2eAgNnSc4d1	definitivní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
I	i	k9	i
a	a	k8xC	a
název	název	k1gInSc1	název
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
zavedla	zavést	k5eAaPmAgFnS	zavést
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
novou	nový	k2eAgFnSc4d1	nová
kategorii	kategorie	k1gFnSc4	kategorie
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
–	–	k?	–
plutoidy	plutoida	k1gFnSc2	plutoida
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
Eris	Eris	k1gFnSc1	Eris
i	i	k8xC	i
Pluto	plut	k2eAgNnSc1d1	Pluto
zařazeny	zařazen	k2eAgInPc1d1	zařazen
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
Michael	Michaela	k1gFnPc2	Michaela
E.	E.	kA	E.
Brown	Brown	k1gMnSc1	Brown
z	z	k7c2	z
California	Californium	k1gNnSc2	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
Chadwick	Chadwick	k1gInSc4	Chadwick
A.	A.	kA	A.
Trujillo	Trujillo	k1gNnSc1	Trujillo
z	z	k7c2	z
Gemini	Gemin	k2eAgMnPc1d1	Gemin
Observatory	Observator	k1gMnPc7	Observator
a	a	k8xC	a
David	David	k1gMnSc1	David
L.	L.	kA	L.
Rabinowitz	Rabinowitz	k1gMnSc1	Rabinowitz
z	z	k7c2	z
Yale	Yale	k1gNnSc2	Yale
University	universita	k1gFnSc2	universita
planetku	planetka	k1gFnSc4	planetka
poprvé	poprvé	k6eAd1	poprvé
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
na	na	k7c6	na
Observatoři	observatoř	k1gFnSc6	observatoř
Palomar	Palomara	k1gFnPc2	Palomara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
1,2	[number]	k4	1,2
<g/>
m	m	kA	m
dalekohledu	dalekohled	k1gInSc2	dalekohled
SOT	sot	k1gInSc1	sot
(	(	kIx(	(
<g/>
Samuel	Samuel	k1gMnSc1	Samuel
Oschin	Oschin	k1gMnSc1	Oschin
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vybaveného	vybavený	k2eAgInSc2d1	vybavený
CCD	CCD	kA	CCD
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
enormní	enormní	k2eAgFnSc3d1	enormní
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
objektu	objekt	k1gInSc2	objekt
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
pohyb	pohyb	k1gInSc4	pohyb
této	tento	k3xDgFnSc2	tento
planetky	planetka	k1gFnSc2	planetka
o	o	k7c4	o
magnitudě	magnitudě	k6eAd1	magnitudě
18,8	[number]	k4	18,8
<g/>
m	m	kA	m
tak	tak	k6eAd1	tak
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nedala	dát	k5eNaPmAgFnS	dát
stanovit	stanovit	k5eAaPmF	stanovit
přesně	přesně	k6eAd1	přesně
její	její	k3xOp3gFnSc1	její
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zdařilo	zdařit	k5eAaPmAgNnS	zdařit
až	až	k9	až
po	po	k7c6	po
nových	nový	k2eAgNnPc6d1	nové
pozorováních	pozorování	k1gNnPc6	pozorování
<g/>
,	,	kIx,	,
uskutečněných	uskutečněný	k2eAgInPc2d1	uskutečněný
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Cerro	Cerro	k1gNnSc1	Cerro
Tololo	Tolola	k1gFnSc5	Tolola
dalekohledem	dalekohled	k1gInSc7	dalekohled
SMARTS	SMARTS	kA	SMARTS
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
1,3	[number]	k4	1,3
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
objevitelů	objevitel	k1gMnPc2	objevitel
podílela	podílet	k5eAaImAgFnS	podílet
ještě	ještě	k9	ještě
Suzanne	Suzann	k1gInSc5	Suzann
W.	W.	kA	W.
Tourtellotte	Tourtellott	k1gInSc5	Tourtellott
z	z	k7c2	z
Yale	Yale	k1gFnSc7	Yale
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídka	prohlídka	k1gFnSc1	prohlídka
archívů	archív	k1gInPc2	archív
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgInSc1d3	nejstarší
snímek	snímek	k1gInSc1	snímek
tohoto	tento	k3xDgInSc2	tento
objektu	objekt	k1gInSc2	objekt
pořídili	pořídit	k5eAaPmAgMnP	pořídit
již	již	k6eAd1	již
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1989	[number]	k4	1989
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Siding	Siding	k1gInSc1	Siding
Springs	Springsa	k1gFnPc2	Springsa
Schmidtovou	Schmidtův	k2eAgFnSc7d1	Schmidtova
komorou	komora	k1gFnSc7	komora
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
1,2	[number]	k4	1,2
m.	m.	k?	m.
Další	další	k2eAgInPc4d1	další
archivní	archivní	k2eAgInPc4d1	archivní
snímky	snímek	k1gInPc4	snímek
z	z	k7c2	z
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Palomar	Palomara	k1gFnPc2	Palomara
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
velikosti	velikost	k1gFnSc2	velikost
této	tento	k3xDgFnSc2	tento
planetky	planetka	k1gFnSc2	planetka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zpřesněných	zpřesněný	k2eAgInPc2d1	zpřesněný
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
okamžité	okamžitý	k2eAgFnSc6d1	okamžitá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
97	[number]	k4	97
AU	au	k0	au
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
ohlášen	ohlášet	k5eAaImNgInS	ohlášet
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
byl	být	k5eAaImAgInS	být
ohlášen	ohlášet	k5eAaImNgInS	ohlášet
i	i	k9	i
objev	objev	k1gInSc1	objev
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zmatkům	zmatek	k1gInPc3	zmatek
ve	v	k7c6	v
zprávách	zpráva	k1gFnPc6	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
planetka	planetka	k1gFnSc1	planetka
Haumea	Haumea	k1gFnSc1	Haumea
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
2003	[number]	k4	2003
EL	Ela	k1gFnPc2	Ela
<g/>
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Sedna	Seden	k2eAgFnSc1d1	Sedna
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
je	být	k5eAaImIp3nS	být
Makemake	Makemake	k1gNnSc1	Makemake
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
2005	[number]	k4	2005
FY	fy	kA	fy
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Sedna	Seden	k2eAgFnSc1d1	Sedna
<g/>
.	.	kIx.	.
</s>
<s>
Eris	Eris	k1gFnSc1	Eris
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
557,15	[number]	k4	557,15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Afélium	afélium	k1gNnSc1	afélium
má	mít	k5eAaImIp3nS	mít
97,610	[number]	k4	97,610
<g/>
3	[number]	k4	3
AU	au	k0	au
<g/>
,	,	kIx,	,
perihélium	perihélium	k1gNnSc4	perihélium
pak	pak	k6eAd1	pak
37,807	[number]	k4	37,807
<g/>
9	[number]	k4	9
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
96,7	[number]	k4	96,7
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
po	po	k7c4	po
projití	projití	k1gNnSc4	projití
posledním	poslední	k2eAgNnSc7d1	poslední
aféliem	afélium	k1gNnSc7	afélium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Perihéliem	perihélium	k1gNnSc7	perihélium
projde	projít	k5eAaPmIp3nS	projít
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2256	[number]	k4	2256
a	a	k8xC	a
2258	[number]	k4	2258
<g/>
.	.	kIx.	.
</s>
<s>
Eris	Eris	k1gFnSc1	Eris
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
objekty	objekt	k1gInPc7	objekt
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc4d1	skutečná
velikost	velikost	k1gFnSc4	velikost
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
určit	určit	k5eAaPmF	určit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
porovnáním	porovnání	k1gNnSc7	porovnání
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
,	,	kIx,	,
ukazujících	ukazující	k2eAgInPc2d1	ukazující
totožný	totožný	k2eAgInSc4d1	totožný
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
zákryt	zákryt	k1gInSc4	zákryt
Plutoidem	Plutoid	k1gInSc7	Plutoid
Eris	Eris	k1gFnSc1	Eris
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
určený	určený	k2eAgInSc1d1	určený
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
2326	[number]	k4	2326
±	±	k?	±
12	[number]	k4	12
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
dřívější	dřívější	k2eAgInPc1d1	dřívější
odhady	odhad	k1gInPc1	odhad
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jiných	jiný	k2eAgFnPc2d1	jiná
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
jasnost	jasnost	k1gFnSc1	jasnost
objektů	objekt	k1gInPc2	objekt
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
albedo	albedo	k1gNnSc1	albedo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
známe	znát	k5eAaImIp1nP	znát
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
albedo	albedo	k1gNnSc4	albedo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
z	z	k7c2	z
hodnoty	hodnota	k1gFnSc2	hodnota
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
tělesa	těleso	k1gNnSc2	těleso
odvodit	odvodit	k5eAaPmF	odvodit
jeho	on	k3xPp3gInSc4	on
průměr	průměr	k1gInSc4	průměr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čím	co	k3yInSc7	co
vyšší	vysoký	k2eAgNnSc1d2	vyšší
albedo	albedo	k1gNnSc1	albedo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
nižší	nízký	k2eAgFnSc1d2	nižší
průměr	průměr	k1gInSc1	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
přesné	přesný	k2eAgNnSc1d1	přesné
albedo	albedo	k1gNnSc1	albedo
tělesa	těleso	k1gNnSc2	těleso
2003	[number]	k4	2003
UB313	UB313	k1gMnPc7	UB313
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
skutečná	skutečný	k2eAgFnSc1d1	skutečná
velikost	velikost	k1gFnSc1	velikost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
jasnosti	jasnost	k1gFnSc2	jasnost
určena	určit	k5eAaPmNgFnS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
astronomové	astronom	k1gMnPc1	astronom
spočítali	spočítat	k5eAaPmAgMnP	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
těleso	těleso	k1gNnSc1	těleso
odráželo	odrážet	k5eAaImAgNnS	odrážet
všechno	všechen	k3xTgNnSc4	všechen
přijaté	přijatý	k2eAgNnSc4d1	přijaté
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
maximálnímu	maximální	k2eAgMnSc3d1	maximální
albedu	albed	k1gMnSc3	albed
1,0	[number]	k4	1,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
albedo	albedo	k1gNnSc1	albedo
bude	být	k5eAaImBp3nS	být
určitě	určitě	k6eAd1	určitě
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Reálněji	reálně	k6eAd2	reálně
lze	lze	k6eAd1	lze
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
velmi	velmi	k6eAd1	velmi
čistým	čistý	k2eAgInSc7d1	čistý
ledem	led	k1gInSc7	led
a	a	k8xC	a
tedy	tedy	k9	tedy
velice	velice	k6eAd1	velice
jasný	jasný	k2eAgInSc1d1	jasný
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
až	až	k8xS	až
0,50	[number]	k4	0,50
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
průměr	průměr	k1gInSc1	průměr
nejméně	málo	k6eAd3	málo
3150	[number]	k4	3150
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
tmavý	tmavý	k2eAgInSc1d1	tmavý
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc4	albedo
jen	jen	k6eAd1	jen
0,05	[number]	k4	0,05
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
dokonce	dokonce	k9	dokonce
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
9900	[number]	k4	9900
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
být	být	k5eAaImF	být
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
metanovým	metanův	k2eAgInSc7d1	metanův
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
povrchu	povrch	k1gInSc3	povrch
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
albedo	albedo	k1gNnSc1	albedo
mohlo	moct	k5eAaImAgNnS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
0,3	[number]	k4	0,3
a	a	k8xC	a
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
nejpravděpodobnější	pravděpodobný	k2eAgFnSc1d3	nejpravděpodobnější
hodnota	hodnota	k1gFnSc1	hodnota
jeho	jeho	k3xOp3gInSc2	jeho
průměru	průměr	k1gInSc2	průměr
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
4000	[number]	k4	4000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
odporuje	odporovat	k5eAaImIp3nS	odporovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
nebyl	být	k5eNaImAgInS	být
detekován	detekovat	k5eAaImNgInS	detekovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
horní	horní	k2eAgInSc1d1	horní
limit	limit	k1gInSc1	limit
průměru	průměr	k1gInSc2	průměr
3000	[number]	k4	3000
km	km	kA	km
a	a	k8xC	a
albedo	albedo	k1gNnSc4	albedo
nejméně	málo	k6eAd3	málo
0,55	[number]	k4	0,55
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
zveřejněných	zveřejněný	k2eAgFnPc2d1	zveřejněná
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
má	mít	k5eAaImIp3nS	mít
objekt	objekt	k1gInSc4	objekt
2003	[number]	k4	2003
UB313	UB313	k1gFnPc2	UB313
na	na	k7c6	na
základě	základ	k1gInSc6	základ
měření	měření	k1gNnSc2	měření
tepelného	tepelný	k2eAgNnSc2d1	tepelné
vyzařování	vyzařování	k1gNnSc2	vyzařování
průměr	průměr	k1gInSc4	průměr
3000	[number]	k4	3000
km	km	kA	km
±	±	k?	±
<g/>
300	[number]	k4	300
km	km	kA	km
±	±	k?	±
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
odchylka	odchylka	k1gFnSc1	odchylka
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
metodou	metoda	k1gFnSc7	metoda
měření	měření	k1gNnSc2	měření
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
neznámé	známý	k2eNgFnSc3d1	neznámá
orientaci	orientace	k1gFnSc3	orientace
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
rychlosti	rychlost	k1gFnSc2	rychlost
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
23	[number]	k4	23
až	až	k9	až
27	[number]	k4	27
K.	K.	kA	K.
Měření	měření	k1gNnSc2	měření
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
1,2	[number]	k4	1,2
mm	mm	kA	mm
(	(	kIx(	(
<g/>
okraj	okraj	k1gInSc4	okraj
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
spektra	spektrum	k1gNnSc2	spektrum
<g/>
)	)	kIx)	)
při	při	k7c6	při
které	který	k3yQgFnSc6	který
jasnost	jasnost	k1gFnSc1	jasnost
objektu	objekt	k1gInSc2	objekt
závisí	záviset	k5eAaImIp3nS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
(	(	kIx(	(
<g/>
HST	HST	kA	HST
<g/>
)	)	kIx)	)
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
2400	[number]	k4	2400
<g/>
±	±	k?	±
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k4c4	málo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
Pluta	Pluto	k1gNnSc2	Pluto
(	(	kIx(	(
<g/>
2370	[number]	k4	2370
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Albedo	Albedo	k1gNnSc1	Albedo
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
odhadováno	odhadován	k2eAgNnSc1d1	odhadováno
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
0,86	[number]	k4	0,86
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
těleso	těleso	k1gNnSc4	těleso
s	s	k7c7	s
nejodrazivějším	odrazivý	k2eAgInSc7d3	odrazivý
povrchem	povrch	k1gInSc7	povrch
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
měsíce	měsíc	k1gInSc2	měsíc
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
odhady	odhad	k1gInPc1	odhad
na	na	k7c6	na
základě	základ	k1gInSc6	základ
infračervených	infračervený	k2eAgNnPc2d1	infračervené
měření	měření	k1gNnPc2	měření
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
planetka	planetka	k1gFnSc1	planetka
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
svým	svůj	k3xOyFgInSc7	svůj
pólem	pól	k1gInSc7	pól
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rovnovážnou	rovnovážný	k2eAgFnSc4d1	rovnovážná
teplotu	teplota	k1gFnSc4	teplota
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
tohoto	tento	k3xDgInSc2	tento
předpokladu	předpoklad	k1gInSc2	předpoklad
by	by	kYmCp3nS	by
průměr	průměr	k1gInSc4	průměr
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
infračervených	infračervený	k2eAgNnPc2d1	infračervené
měření	měření	k1gNnPc2	měření
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgNnPc1d2	menší
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
2500	[number]	k4	2500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgNnPc1	první
spektroskopická	spektroskopický	k2eAgNnPc1d1	spektroskopické
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
,	,	kIx,	,
uskutečněná	uskutečněný	k2eAgNnPc1d1	uskutečněné
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
8	[number]	k4	8
<g/>
m	m	kA	m
dalekohledem	dalekohled	k1gInSc7	dalekohled
GNT	GNT	kA	GNT
(	(	kIx(	(
<g/>
Gemini	Gemin	k2eAgMnPc1d1	Gemin
North	North	k1gMnSc1	North
Telescope	Telescop	k1gInSc5	Telescop
<g/>
)	)	kIx)	)
na	na	k7c6	na
Havajských	havajský	k2eAgInPc6d1	havajský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
tohoto	tento	k3xDgNnSc2	tento
tělesa	těleso	k1gNnSc2	těleso
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pokryt	pokryt	k1gInSc4	pokryt
ledy	led	k1gInPc7	led
<g/>
,	,	kIx,	,
tvořenými	tvořený	k2eAgInPc7d1	tvořený
zkondenzovanými	zkondenzovaný	k2eAgInPc7d1	zkondenzovaný
plyny	plyn	k1gInPc7	plyn
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
povrchu	povrch	k1gInSc3	povrch
planety	planeta	k1gFnSc2	planeta
Pluto	Pluto	k1gMnSc1	Pluto
a	a	k8xC	a
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
měsíce	měsíc	k1gInSc2	měsíc
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
však	však	k9	však
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
tak	tak	k6eAd1	tak
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
albedem	albed	k1gInSc7	albed
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nejpodrobnější	podrobný	k2eAgFnPc1d3	nejpodrobnější
spektroskopické	spektroskopický	k2eAgFnPc1d1	spektroskopická
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
uskutečněné	uskutečněný	k2eAgFnPc1d1	uskutečněná
4,2	[number]	k4	4,2
<g/>
m	m	kA	m
dalekohledem	dalekohled	k1gInSc7	dalekohled
WHT	WHT	kA	WHT
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Herschel	Herschela	k1gFnPc2	Herschela
Telescope	Telescop	k1gInSc5	Telescop
<g/>
)	)	kIx)	)
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
Roque	Roque	k1gInSc1	Roque
de	de	k?	de
los	los	k1gInSc1	los
Muchachos	Muchachos	k1gInSc1	Muchachos
<g/>
"	"	kIx"	"
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
La	la	k1gNnSc2	la
Palma	palma	k1gFnSc1	palma
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
od	od	k7c2	od
350	[number]	k4	350
do	do	k7c2	do
950	[number]	k4	950
nm	nm	k?	nm
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchní	vrchní	k2eAgFnSc4d1	vrchní
vrstvu	vrstva	k1gFnSc4	vrstva
tvoří	tvořit	k5eAaImIp3nP	tvořit
tuhý	tuhý	k2eAgInSc4d1	tuhý
metan	metan	k1gInSc4	metan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
pevného	pevný	k2eAgInSc2d1	pevný
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
spekter	spektrum	k1gNnPc2	spektrum
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlubší	hluboký	k2eAgFnPc1d2	hlubší
vrstvy	vrstva	k1gFnPc1	vrstva
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
čistší	čistý	k2eAgInSc4d2	čistší
metan	metan	k1gInSc4	metan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
povrchové	povrchový	k2eAgFnPc1d1	povrchová
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
znečištěny	znečištěn	k2eAgMnPc4d1	znečištěn
pevným	pevný	k2eAgInSc7d1	pevný
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgInPc2d1	poslední
dvou	dva	k4xCgInPc2	dva
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
planetka	planetka	k1gFnSc1	planetka
vzdalovala	vzdalovat	k5eAaImAgFnS	vzdalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
k	k	k7c3	k
aféliu	afélium	k1gNnSc3	afélium
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
kondenzovaly	kondenzovat	k5eAaImAgInP	kondenzovat
i	i	k9	i
méně	málo	k6eAd2	málo
těkavé	těkavý	k2eAgInPc1d1	těkavý
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
právě	právě	k9	právě
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
těkavý	těkavý	k2eAgInSc1d1	těkavý
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
na	na	k7c6	na
Plutu	Pluto	k1gNnSc6	Pluto
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
slabé	slabý	k2eAgFnSc3d1	slabá
intenzitě	intenzita	k1gFnSc3	intenzita
záření	záření	k1gNnSc2	záření
Eris	Eris	k1gFnSc1	Eris
a	a	k8xC	a
tedy	tedy	k9	tedy
obtížnějšímu	obtížný	k2eAgNnSc3d2	obtížnější
vyhodnocování	vyhodnocování	k1gNnSc3	vyhodnocování
spekter	spektrum	k1gNnPc2	spektrum
nebyl	být	k5eNaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Pluto	Pluto	k1gNnSc1	Pluto
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
velká	velký	k2eAgNnPc1d1	velké
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
přítomnosti	přítomnost	k1gFnSc3	přítomnost
složitých	složitý	k2eAgFnPc2d1	složitá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
tholinů	tholin	k1gInPc2	tholin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
růst	růst	k1gInSc1	růst
absorpce	absorpce	k1gFnSc2	absorpce
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
krátkovlnné	krátkovlnný	k2eAgFnSc3d1	krátkovlnná
části	část	k1gFnSc3	část
spektra	spektrum	k1gNnSc2	spektrum
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
tělesa	těleso	k1gNnSc2	těleso
přibližně	přibližně	k6eAd1	přibližně
poloviční	poloviční	k2eAgMnSc1d1	poloviční
než	než	k8xS	než
u	u	k7c2	u
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
i	i	k9	i
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
Eris	Eris	k1gFnSc1	Eris
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tholiny	tholin	k1gInPc1	tholin
vznikající	vznikající	k2eAgInPc1d1	vznikající
především	především	k6eAd1	především
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
perihelu	perihel	k1gInSc2	perihel
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
překryty	překrýt	k5eAaPmNgFnP	překrýt
silnou	silný	k2eAgFnSc7d1	silná
vrstvou	vrstva	k1gFnSc7	vrstva
později	pozdě	k6eAd2	pozdě
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
metanového	metanový	k2eAgInSc2d1	metanový
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dysnomia	Dysnomius	k1gMnSc2	Dysnomius
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
nalezl	nalézt	k5eAaBmAgMnS	nalézt
M.	M.	kA	M.
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
California	Californium	k1gNnPc1	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
se	s	k7c7	s
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
přibližně	přibližně	k6eAd1	přibližně
šedesátkrát	šedesátkrát	k6eAd1	šedesátkrát
méně	málo	k6eAd2	málo
jasného	jasný	k2eAgMnSc4d1	jasný
průvodce	průvodce	k1gMnSc4	průvodce
této	tento	k3xDgFnSc2	tento
planetky	planetka	k1gFnSc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Snímkování	snímkování	k1gNnSc1	snímkování
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
2,1	[number]	k4	2,1
μ	μ	k?	μ
<g/>
)	)	kIx)	)
kamerou	kamera	k1gFnSc7	kamera
NIRC2	NIRC2	k1gFnSc2	NIRC2
(	(	kIx(	(
<g/>
Near	Near	k1gInSc1	Near
Infra	Infr	k1gMnSc2	Infr
Red	Red	k1gMnSc2	Red
Camera	Camer	k1gMnSc2	Camer
2	[number]	k4	2
<g/>
)	)	kIx)	)
spřaženou	spřažený	k2eAgFnSc4d1	spřažená
s	s	k7c7	s
nově	nově	k6eAd1	nově
testovaným	testovaný	k2eAgInSc7d1	testovaný
systémem	systém	k1gInSc7	systém
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
LGS	LGS	kA	LGS
AO	AO	kA	AO
(	(	kIx(	(
<g/>
Laser	laser	k1gInSc1	laser
Guide	Guid	k1gInSc5	Guid
Star	Star	kA	Star
Adaptive	Adaptiv	k1gInSc5	Adaptiv
Optics	Opticsa	k1gFnPc2	Opticsa
<g/>
)	)	kIx)	)
na	na	k7c6	na
dalekohledu	dalekohled	k1gInSc6	dalekohled
Keck	Kecka	k1gFnPc2	Kecka
II	II	kA	II
observatoře	observatoř	k1gFnSc2	observatoř
Keck	Kecka	k1gFnPc2	Kecka
Observatory	Observator	k1gInPc1	Observator
na	na	k7c4	na
Mauna	Mauen	k2eAgMnSc4d1	Mauen
Kea	Kea	k1gMnSc4	Kea
na	na	k7c6	na
Havajských	havajský	k2eAgInPc6d1	havajský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
dostal	dostat	k5eAaPmAgInS	dostat
předběžné	předběžný	k2eAgNnSc4d1	předběžné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
)	)	kIx)	)
1	[number]	k4	1
a	a	k8xC	a
pracovní	pracovní	k2eAgNnSc4d1	pracovní
jméno	jméno	k1gNnSc4	jméno
Gabrielle	Gabrielle	k1gFnSc2	Gabrielle
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
průvodkyně	průvodkyně	k1gFnSc2	průvodkyně
princezny	princezna	k1gFnSc2	princezna
Xeny	Xena	k1gFnSc2	Xena
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
<g/>
.	.	kIx.	.
</s>
<s>
Rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
průměr	průměr	k1gInSc1	průměr
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
250	[number]	k4	250
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
kruhová	kruhový	k2eAgFnSc1d1	kruhová
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Eris	Eris	k1gFnSc1	Eris
37	[number]	k4	37
350	[number]	k4	350
±	±	k?	±
140	[number]	k4	140
km	km	kA	km
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
jednoho	jeden	k4xCgInSc2	jeden
oběhu	oběh	k1gInSc2	oběh
byla	být	k5eAaImAgNnP	být
určena	určit	k5eAaPmNgNnP	určit
na	na	k7c6	na
15,774	[number]	k4	15,774
±	±	k?	±
0,002	[number]	k4	0,002
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planetka	planetka	k1gFnSc1	planetka
Eris	Eris	k1gFnSc1	Eris
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
řecké	řecký	k2eAgFnSc2d1	řecká
bohyně	bohyně	k1gFnSc2	bohyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
personifikací	personifikace	k1gFnSc7	personifikace
sváru	svár	k1gInSc2	svár
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
měsíc	měsíc	k1gInSc1	měsíc
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
je	být	k5eAaImIp3nS	být
nazván	nazvat	k5eAaBmNgInS	nazvat
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc2	bohyně
anarchie	anarchie	k1gFnSc2	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
jména	jméno	k1gNnPc1	jméno
zřejmě	zřejmě	k6eAd1	zřejmě
odrážejí	odrážet	k5eAaImIp3nP	odrážet
názor	názor	k1gInSc4	názor
objevitelů	objevitel	k1gMnPc2	objevitel
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
nim	on	k3xPp3gFnPc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bouřlivým	bouřlivý	k2eAgFnPc3d1	bouřlivá
diskusím	diskuse	k1gFnPc3	diskuse
ohledně	ohledně	k7c2	ohledně
pojmu	pojem	k1gInSc2	pojem
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
mnoho	mnoho	k4c4	mnoho
pří	pře	k1gFnPc2	pře
v	v	k7c6	v
astronomické	astronomický	k2eAgFnSc6d1	astronomická
i	i	k8xC	i
neastronomické	astronomický	k2eNgFnSc6d1	astronomický
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eris	Eris	k1gFnSc1	Eris
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
MPEC	MPEC	kA	MPEC
2005-O41	[number]	k4	2005-O41
:	:	kIx,	:
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
,	,	kIx,	,
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Electronic	Electronice	k1gFnPc2	Electronice
Circular	Circular	k1gInSc1	Circular
<g/>
,	,	kIx,	,
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
July	Jula	k1gFnSc2	Jula
29	[number]	k4	29
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
Astronomers	Astronomers	k1gInSc1	Astronomers
at	at	k?	at
Palomar	Palomar	k1gInSc1	Palomar
Observatory	Observator	k1gInPc7	Observator
Discover	Discover	k1gMnSc1	Discover
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
10	[number]	k4	10
<g/>
th	th	k?	th
Planet	planeta	k1gFnPc2	planeta
Beyond	Beyond	k1gMnSc1	Beyond
Pluto	Pluto	k1gMnSc1	Pluto
–	–	k?	–
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
aktuality	aktualita	k1gFnSc2	aktualita
na	na	k7c6	na
webu	web	k1gInSc6	web
Mike	Mik	k1gMnSc2	Mik
Browna	Brown	k1gMnSc2	Brown
<g/>
,	,	kIx,	,
Caltech	Calt	k1gInPc6	Calt
University	universita	k1gFnSc2	universita
–	–	k?	–
astronomové	astronom	k1gMnPc1	astronom
Mike	Mik	k1gFnSc2	Mik
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
Caltech	Calt	k1gInPc6	Calt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chad	Chad	k1gInSc1	Chad
Trujillo	Trujillo	k1gNnSc1	Trujillo
(	(	kIx(	(
<g/>
Gemini	Gemin	k1gMnPc1	Gemin
Observatory	Observator	k1gMnPc7	Observator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
David	David	k1gMnSc1	David
Rabinowitz	Rabinowitz	k1gMnSc1	Rabinowitz
(	(	kIx(	(
<g/>
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
Trans-Neptunian	Trans-Neptunian	k1gMnSc1	Trans-Neptunian
Object	Object	k1gInSc4	Object
2003	[number]	k4	2003
UB313	UB313	k1gMnSc1	UB313
vying	vying	k1gMnSc1	vying
with	with	k1gMnSc1	with
Pluto	Pluto	k1gMnSc1	Pluto
in	in	k?	in
Size	Size	k1gFnSc1	Size
<g/>
.	.	kIx.	.
-	-	kIx~	-
IAU	IAU	kA	IAU
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
Press	Press	k1gInSc1	Press
Release	Releasa	k1gFnSc3	Releasa
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mike	Mike	k1gNnPc1	Mike
Brown	Browna	k1gFnPc2	Browna
<g/>
,	,	kIx,	,
Caltech	Calt	k1gInPc6	Calt
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
July	Jula	k1gFnSc2	Jula
29	[number]	k4	29
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
Zehnter	Zehnter	k1gInSc1	Zehnter
Planet	planeta	k1gFnPc2	planeta
des	des	k1gNnSc2	des
Sonnensystems	Sonnensystemsa	k1gFnPc2	Sonnensystemsa
entdeckt	entdeckt	k1gInSc1	entdeckt
<g/>
,	,	kIx,	,
NZZ	NZZ	kA	NZZ
<g/>
.	.	kIx.	.
<g/>
ch	ch	k0	ch
<g/>
,	,	kIx,	,
30.7	[number]	k4	30.7
<g/>
.2005	.2005	k4	.2005
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
<g/>
)	)	kIx)	)
Asteroid	asteroid	k1gInSc1	asteroid
2003	[number]	k4	2003
UB313	UB313	k1gFnSc2	UB313
ist	ist	k?	ist
grösser	grösser	k1gMnSc1	grösser
als	als	k?	als
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
,	,	kIx,	,
ist	ist	k?	ist
der	drát	k5eAaImRp2nS	drát
zehnte	zehnt	k1gInSc5	zehnt
Planet	planeta	k1gFnPc2	planeta
entdeckt	entdeckt	k1gInSc1	entdeckt
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
.	.	kIx.	.
<g/>
info	info	k1gNnSc1	info
<g/>
,	,	kIx,	,
30.7	[number]	k4	30.7
<g/>
.2005	.2005	k4	.2005
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
<g/>
)	)	kIx)	)
A	a	k9	a
tenth	tenth	k1gInSc1	tenth
planet	planeta	k1gFnPc2	planeta
/	/	kIx~	/
W.	W.	kA	W.
R.	R.	kA	R.
Johnston	Johnston	k1gInSc1	Johnston
<g/>
,	,	kIx,	,
8.9	[number]	k4	8.9
<g/>
.2005	.2005	k4	.2005
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
BROWN	BROWN	kA	BROWN
<g/>
,	,	kIx,	,
M.	M.	kA	M.
E.	E.	kA	E.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Satellites	Satellites	k1gInSc1	Satellites
of	of	k?	of
the	the	k?	the
Largest	Largest	k1gMnSc1	Largest
Kuiper	Kuiper	k1gMnSc1	Kuiper
Belt	Belt	k2eAgInSc4d1	Belt
Objects	Objects	k1gInSc4	Objects
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Astrophysical	Astrophysical	k1gMnSc1	Astrophysical
Journal	Journal	k1gMnSc1	Journal
<g/>
,	,	kIx,	,
639	[number]	k4	639
<g/>
:	:	kIx,	:
<g/>
L	L	kA	L
<g/>
43	[number]	k4	43
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
46	[number]	k4	46
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
March	March	k1gInSc1	March
1	[number]	k4	1
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
LICANDRO	LICANDRO	kA	LICANDRO
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
GRUNDY	GRUNDY	kA	GRUNDY
<g/>
,	,	kIx,	,
W.	W.	kA	W.
M.	M.	kA	M.
<g/>
;	;	kIx,	;
PINILLA-ALONSO	PINILLA-ALONSO	k1gMnSc1	PINILLA-ALONSO
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
;	;	kIx,	;
LEISY	LEISY	kA	LEISY
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Visible	Visible	k1gFnSc1	Visible
spectroscopy	spectroscopa	k1gFnSc2	spectroscopa
of	of	k?	of
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
:	:	kIx,	:
Evidence	evidence	k1gFnSc2	evidence
for	forum	k1gNnPc2	forum
N2	N2	k1gMnSc7	N2
ice	ice	k?	ice
on	on	k3xPp3gMnSc1	on
the	the	k?	the
surface	surface	k1gFnSc2	surface
of	of	k?	of
the	the	k?	the
largest	largest	k1gFnSc4	largest
TNO	TNO	kA	TNO
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
Astronomy	astronom	k1gMnPc4	astronom
&	&	k?	&
Astrophysics	Astrophysics	k1gInSc4	Astrophysics
<g/>
.	.	kIx.	.
-	-	kIx~	-
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
