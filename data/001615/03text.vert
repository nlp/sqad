<s>
Max	Max	k1gMnSc1	Max
Born	Born	k1gMnSc1	Born
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Breslau	Breslaa	k1gFnSc4	Breslaa
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Göttingen	Göttingen	k1gInSc1	Göttingen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
W.	W.	kA	W.
Bothem	Both	k1gInSc7	Both
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
zásadní	zásadní	k2eAgInSc4d1	zásadní
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
statistickou	statistický	k2eAgFnSc4d1	statistická
interpretaci	interpretace	k1gFnSc4	interpretace
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
dědečkem	dědeček	k1gMnSc7	dědeček
britské	britský	k2eAgFnSc2d1	britská
herečky	herečka	k1gFnSc2	herečka
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Olivie	Olivie	k1gFnSc2	Olivie
Newton-John	Newton-Johna	k1gFnPc2	Newton-Johna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
výroku	výrok	k1gInSc2	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studiem	studio	k1gNnSc7	studio
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
stane	stanout	k5eAaPmIp3nS	stanout
ateistou	ateista	k1gMnSc7	ateista
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dost	dost	k6eAd1	dost
hloupí	hloupý	k2eAgMnPc1d1	hloupý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
