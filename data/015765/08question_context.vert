<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
tedy	tedy	k9
nad	nad	k7c4
očekávání	očekávání	k1gNnPc4
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
němčině	němčina	k1gFnSc6
známa	známo	k1gNnSc2
jako	jako	k8xC,k8xS
Kreuzzug	Kreuzzug	k1gFnPc1
Friedrichs	Friedrichs	k1gFnPc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tj.	tj.	kA
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
římsko-německého	římsko-německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>