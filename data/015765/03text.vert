<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Císař	Císař	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
spolu	spolu	k6eAd1
s	s	k7c7
Ludvíkem	Ludvík	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Durynským	durynský	k2eAgNnSc7d1
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
,	,	kIx,
mozaika	mozaika	k1gFnSc1
z	z	k7c2
komnaty	komnata	k1gFnSc2
ve	v	k7c6
Wartburgu	Wartburg	k1gInSc6
o	o	k7c6
životě	život	k1gInSc6
Alžběty	Alžběta	k1gFnSc2
Uherské	uherský	k2eAgFnSc2d1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
Palestina	Palestina	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
úspěšné	úspěšný	k2eAgNnSc1d1
vyjednání	vyjednání	k1gNnSc1
předání	předání	k1gNnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
území	území	k1gNnPc2
do	do	k7c2
křižáckých	křižácký	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
Nazaret	Nazaret	k1gInSc1
<g/>
,	,	kIx,
Sidón	Sidón	k1gInSc1
<g/>
,	,	kIx,
Jaffa	Jaffa	k1gFnSc1
a	a	k8xC
Betlém	Betlém	k1gInSc1
vráceny	vrátit	k5eAaPmNgFnP
křižákům	křižák	k1gMnPc3
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
Sicilské	sicilský	k2eAgNnSc4d1
království	království	k1gNnSc4
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
někteří	některý	k3yIgMnPc1
Angličanépodpora	Angličanépodpor	k1gMnSc2
<g/>
:	:	kIx,
Řád	řád	k1gInSc4
templářů	templář	k1gMnPc2
Řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
Antiochijské	antiochijský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Egyptský	egyptský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
Ajjúbovců	Ajjúbovec	k1gInPc2
Damašský	damašský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
Ajjúbovců	Ajjúbovec	k1gMnPc2
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heřman	Heřman	k1gMnSc1
ze	z	k7c2
Salzy	Salza	k1gFnSc2
Ludvík	Ludvík	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Durynský	durynský	k2eAgInSc1d1
Odo	Odo	k1gFnPc7
z	z	k7c2
Montbéliardu	Montbéliard	k1gInSc2
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Limburský	limburský	k2eAgMnSc1d1
Matyáš	Matyáš	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lotrinský	lotrinský	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Filangieri	Filangier	k1gFnSc2
Petr	Petr	k1gMnSc1
des	des	k1gNnSc2
Roches	Roches	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Briwere	Briwer	k1gInSc5
Jindřich	Jindřich	k1gMnSc1
Kyperský	kyperský	k2eAgInSc1d1
Bohemund	Bohemund	k1gInSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
z	z	k7c2
Ibelinu	Ibelin	k1gInSc2
Waltr	Waltr	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
z	z	k7c2
Cesareje	Cesarej	k1gInSc2
Balian	Balian	k1gMnSc1
Grenier	Grenier	k1gMnSc1
</s>
<s>
al-Kámil	al-Kámit	k5eAaPmAgMnS
An-Násir	An-Násir	k1gMnSc1
Dawúd	Dawúd	k1gMnSc1
</s>
<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
němčině	němčina	k1gFnSc6
známa	známo	k1gNnSc2
jako	jako	k8xC,k8xS
Kreuzzug	Kreuzzug	k1gFnPc1
Friedrichs	Friedrichs	k1gFnPc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tj.	tj.	kA
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
římsko-německého	římsko-německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufského	Štaufský	k2eAgMnSc2d1
<g/>
,	,	kIx,
uskutečněné	uskutečněný	k2eAgNnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1228	#num#	k4
–	–	k?
1229	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
se	se	k3xPyFc4
císař	císař	k1gMnSc1
zavázal	zavázat	k5eAaPmAgMnS
již	již	k6eAd1
roku	rok	k1gInSc2
1215	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
Egypta	Egypt	k1gInSc2
k	k	k7c3
páté	pátý	k4xOgFnSc3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
se	se	k3xPyFc4
osobně	osobně	k6eAd1
nakonec	nakonec	k6eAd1
nedostavil	dostavit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitropolitické	vnitropolitický	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
a	a	k8xC
spory	spor	k1gInPc4
s	s	k7c7
papeži	papež	k1gMnPc7
jej	on	k3xPp3gMnSc4
zdržovaly	zdržovat	k5eAaImAgInP
na	na	k7c4
Sicílii	Sicílie	k1gFnSc4
<g/>
,	,	kIx,
až	až	k6eAd1
nakonec	nakonec	k6eAd1
roku	rok	k1gInSc2
1225	#num#	k4
přislíbil	přislíbit	k5eAaPmAgMnS
papeži	papež	k1gMnSc3
Honoriu	Honorium	k1gNnSc3
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
vytáhne	vytáhnout	k5eAaPmIp3nS
na	na	k7c4
Východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Německé	německý	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
v	v	k7c6
srpnu	srpen	k1gInSc6
1227	#num#	k4
skutečně	skutečně	k6eAd1
ze	z	k7c2
Sicílie	Sicílie	k1gFnSc2
vyrazilo	vyrazit	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
Fridrich	Fridrich	k1gMnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
nemoci	nemoc	k1gFnSc3
po	po	k7c6
cestě	cesta	k1gFnSc6
zdržel	zdržet	k5eAaPmAgMnS
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
jej	on	k3xPp3gNnSc4
Řehoř	Řehoř	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
Fridricha	Fridrich	k1gMnSc4
podezříval	podezřívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
výpravu	výprava	k1gFnSc4
chce	chtít	k5eAaImIp3nS
opět	opět	k6eAd1
odložit	odložit	k5eAaPmF
<g/>
,	,	kIx,
exkomunikoval	exkomunikovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
papežův	papežův	k2eAgInSc1d1
verdikt	verdikt	k1gInSc1
ignoroval	ignorovat	k5eAaImAgInS
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
uzdravil	uzdravit	k5eAaPmAgMnS
<g/>
,	,	kIx,
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císařova	Císařův	k2eAgFnSc1d1
exkomunikace	exkomunikace	k1gFnSc1
však	však	k9
měla	mít	k5eAaImAgFnS
na	na	k7c4
výpravu	výprava	k1gFnSc4
neblahé	blahý	k2eNgInPc4d1
následky	následek	k1gInPc4
<g/>
;	;	kIx,
obrovská	obrovský	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
posléze	posléze	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
následovat	následovat	k5eAaImF
exkomunikovaného	exkomunikovaný	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
a	a	k8xC
rovněž	rovněž	k9
palestinští	palestinský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
<g/>
,	,	kIx,
templáři	templář	k1gMnPc1
a	a	k8xC
johanité	johanita	k1gMnPc1
od	od	k7c2
Fridricha	Fridrich	k1gMnSc2
dali	dát	k5eAaPmAgMnP
ruce	ruka	k1gFnSc3
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
zůstal	zůstat	k5eAaPmAgInS
stát	stát	k5eAaImF,k5eAaPmF
věrně	věrně	k6eAd1
při	při	k7c6
císaři	císař	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Fridrichovým	Fridrichův	k2eAgInSc7d1
protějškem	protějšek	k1gInSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
egyptský	egyptský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
al-Kámil	al-Kámit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
exkomunikaci	exkomunikace	k1gFnSc3
nacházel	nacházet	k5eAaImAgMnS
ve	v	k7c6
složitém	složitý	k2eAgNnSc6d1
postavení	postavení	k1gNnSc6
vůči	vůči	k7c3
domácím	domácí	k2eAgMnPc3d1
baronům	baron	k1gMnPc3
<g/>
,	,	kIx,
nechtěl	chtít	k5eNaImAgMnS
v	v	k7c6
Palestině	Palestina	k1gFnSc6
vést	vést	k5eAaImF
dlouhou	dlouhý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
a	a	k8xC
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
diplomacii	diplomacie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Kámil	Al-Kámil	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
napjaté	napjatý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
příbuznými	příbuzná	k1gFnPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
vládli	vládnout	k5eAaImAgMnP
Sýrii	Sýrie	k1gFnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
jednání	jednání	k1gNnSc4
přístupný	přístupný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrichovi	Fridrich	k1gMnSc6
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
pro	pro	k7c4
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1
království	království	k1gNnSc4
získat	získat	k5eAaPmF
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
území	území	k1gNnPc4
v	v	k7c6
palestinském	palestinský	k2eAgNnSc6d1
vnitrozemí	vnitrozemí	k1gNnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
samotného	samotný	k2eAgInSc2d1
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Fridrich	Fridrich	k1gMnSc1
sám	sám	k3xTgMnSc1
korunoval	korunovat	k5eAaBmAgMnS
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
desetileté	desetiletý	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
úspěchy	úspěch	k1gInPc1
však	však	k9
nenašly	najít	k5eNaPmAgInP
odezvu	odezva	k1gFnSc4
ani	ani	k9
u	u	k7c2
papeže	papež	k1gMnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
u	u	k7c2
křižáckých	křižácký	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
Palestinu	Palestina	k1gFnSc4
roku	rok	k1gInSc2
1229	#num#	k4
a	a	k8xC
křižácké	křižácký	k2eAgNnSc1d1
Zámoří	zámoří	k1gNnSc1
upadlo	upadnout	k5eAaPmAgNnS
do	do	k7c2
chaosu	chaos	k1gInSc2
občanských	občanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
a	a	k8xC
když	když	k8xS
vypršelo	vypršet	k5eAaPmAgNnS
příměří	příměří	k1gNnSc1
vyjednané	vyjednaný	k2eAgNnSc1d1
Fridrichem	Fridrich	k1gMnSc7
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
rychle	rychle	k6eAd1
galilejské	galilejský	k2eAgNnSc4d1
vnitrozemí	vnitrozemí	k1gNnSc4
dobyli	dobýt	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
i	i	k9
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
křižáci	křižák	k1gMnPc1
už	už	k6eAd1
nikdy	nikdy	k6eAd1
nedobyli	dobýt	k5eNaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
výpravou	výprava	k1gFnSc7
</s>
<s>
Císař	Císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Římské	římský	k2eAgFnSc2d1
Friedrich	Friedrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
zúčastnit	zúčastnit	k5eAaPmF
již	již	k6eAd1
předchozí	předchozí	k2eAgFnPc4d1
páté	pátá	k1gFnPc4
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc1
na	na	k7c6
ní	on	k3xPp3gFnSc6
mu	on	k3xPp3gMnSc3
však	však	k9
byla	být	k5eAaImAgFnS
zakázána	zakázat	k5eAaPmNgFnS
nejprve	nejprve	k6eAd1
papežem	papež	k1gMnSc7
Inocentem	Inocent	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
Honoriem	Honorium	k1gNnSc7
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc4
papežové	papež	k1gMnPc1
se	se	k3xPyFc4
báli	bát	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Friedrich	Friedrich	k1gMnSc1
mohl	moct	k5eAaImAgInS
získal	získat	k5eAaPmAgInS
příliš	příliš	k6eAd1
mnoho	mnoho	k6eAd1
moci	moct	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
Friedrich	Friedrich	k1gMnSc1
rozhodnul	rozhodnout	k5eAaPmAgMnS
zorganizovat	zorganizovat	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
bez	bez	k7c2
povolení	povolení	k1gNnPc2
papeže	papež	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
příležitost	příležitost	k1gFnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
naskytla	naskytnout	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1220	#num#	k4
korunován	korunován	k2eAgInSc1d1
císařem	císař	k1gMnSc7
a	a	k8xC
upevnil	upevnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
ve	v	k7c6
vzpurných	vzpurný	k2eAgNnPc6d1
italských	italský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1225	#num#	k4
si	se	k3xPyFc3
Friedrich	Friedrich	k1gMnSc1
bere	brát	k5eAaImIp3nS
za	za	k7c4
manželku	manželka	k1gFnSc4
Jolandu	Jolanda	k1gFnSc4
Jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
(	(	kIx(
<g/>
dceru	dcera	k1gFnSc4
Jana	Jan	k1gMnSc2
z	z	k7c2
Briennu	Brienn	k1gInSc2
a	a	k8xC
Marie	Maria	k1gFnSc2
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatbou	svatba	k1gFnSc7
s	s	k7c7
touto	tento	k3xDgFnSc7
dědičkou	dědička	k1gFnSc7
jeruzalémského	jeruzalémský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
si	se	k3xPyFc3
nyní	nyní	k6eAd1
na	na	k7c4
něj	on	k3xPp3gMnSc4
mohl	moct	k5eAaImAgMnS
císař	císař	k1gMnSc1
činit	činit	k5eAaImF
dobře	dobře	k6eAd1
podložený	podložený	k2eAgInSc4d1
nárok	nárok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
stagnující	stagnující	k2eAgNnSc4d1
království	království	k1gNnSc4
zachránit	zachránit	k5eAaPmF
<g/>
,	,	kIx,
dobýt	dobýt	k5eAaPmF
Turky	Turek	k1gMnPc4
držený	držený	k2eAgInSc4d1
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
dát	dát	k5eAaPmF
se	se	k3xPyFc4
korunovat	korunovat	k5eAaBmF
jeho	jeho	k3xOp3gMnSc7
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Cesta	cesta	k1gFnSc1
šesté	šestý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Novým	nový	k2eAgMnSc7d1
papežem	papež	k1gMnSc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1227	#num#	k4
stal	stát	k5eAaPmAgMnS
Řehoř	Řehoř	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
vyplul	vyplout	k5eAaPmAgMnS
z	z	k7c2
italského	italský	k2eAgNnSc2d1
Brindisi	Brindisi	k1gNnSc2
i	i	k8xC
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
armádou	armáda	k1gFnSc7
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Akkonu	Akkon	k1gInSc2
tam	tam	k6eAd1
jeho	jeho	k3xOp3gFnSc4
výpravu	výprava	k1gFnSc4
zastihla	zastihnout	k5eAaPmAgFnS
morová	morový	k2eAgFnSc1d1
epidemie	epidemie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
donutilo	donutit	k5eAaPmAgNnS
císaře	císař	k1gMnSc4
k	k	k7c3
návratu	návrat	k1gInSc3
zpět	zpět	k6eAd1
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
toho	ten	k3xDgMnSc4
rychle	rychle	k6eAd1
využil	využít	k5eAaPmAgMnS
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Friedrich	Friedrich	k1gMnSc1
zbaběle	zbaběle	k6eAd1
zanechal	zanechat	k5eAaPmAgMnS
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
ho	on	k3xPp3gMnSc4
exkomunikoval	exkomunikovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
upevnil	upevnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
ignoroval	ignorovat	k5eAaImAgMnS
papeže	papež	k1gMnPc4
a	a	k8xC
i	i	k9
přes	přes	k7c4
jeho	jeho	k3xOp3gInPc4
protesty	protest	k1gInPc4
vyplul	vyplout	k5eAaPmAgMnS
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
plavbě	plavba	k1gFnSc6
se	se	k3xPyFc4
tentokrát	tentokrát	k6eAd1
zastavil	zastavit	k5eAaPmAgInS
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozporu	rozpor	k1gInSc3
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
kyperským	kyperský	k2eAgMnSc7d1
šlechticem	šlechtic	k1gMnSc7
Janem	Jan	k1gMnSc7
z	z	k7c2
Ibelinu	Ibelin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Janovi	Jan	k1gMnSc6
odebral	odebrat	k5eAaPmAgInS
jeho	jeho	k3xOp3gNnSc4
lenní	lenní	k2eAgNnSc1d1
panství	panství	k1gNnSc1
na	na	k7c6
pevnině	pevnina	k1gFnSc6
–	–	k?
město	město	k1gNnSc1
Bejrút	Bejrút	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
Friedrichovi	Friedrich	k1gMnSc6
znepřátelilo	znepřátelit	k5eAaPmAgNnS
významný	významný	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
rod	rod	k1gInSc4
Ibelinů	Ibelin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1228	#num#	k4
císař	císař	k1gMnSc1
doplul	doplout	k5eAaPmAgMnS
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
království	království	k1gNnSc2
panovaly	panovat	k5eAaImAgInP
na	na	k7c4
nového	nový	k2eAgMnSc4d1
krále	král	k1gMnSc4
rozdílné	rozdílný	k2eAgInPc1d1
názory	názor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friedrich	Friedrich	k1gMnSc1
byl	být	k5eAaImAgMnS
podporován	podporován	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
armádou	armáda	k1gFnSc7
a	a	k8xC
také	také	k9
Řádem	řád	k1gInSc7
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
působícím	působící	k2eAgInSc7d1
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
proti	proti	k7c3
novému	nový	k2eAgMnSc3d1
vladaři	vladař	k1gMnSc3
se	se	k3xPyFc4
stavělo	stavět	k5eAaImAgNnS
duchovenstvo	duchovenstvo	k1gNnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
patriarchou	patriarcha	k1gMnSc7
Geraldem	Gerald	k1gMnSc7
z	z	k7c2
Lausange	Lausang	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
měl	mít	k5eAaImAgMnS
od	od	k7c2
papeže	papež	k1gMnSc2
svůj	svůj	k3xOyFgInSc4
postoj	postoj	k1gInSc1
jasně	jasně	k6eAd1
přikázaný	přikázaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
jakmile	jakmile	k8xS
dorazily	dorazit	k5eAaPmAgInP
zprávy	zpráva	k1gFnPc4
o	o	k7c6
císařově	císařův	k2eAgFnSc6d1
exkomunikaci	exkomunikace	k1gFnSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
nakloněno	nakloněn	k2eAgNnSc4d1
i	i	k8xC
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
prostých	prostý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgMnSc1d1
bylo	být	k5eAaImAgNnS
postavení	postavení	k1gNnSc1
templářů	templář	k1gMnPc2
a	a	k8xC
johanitů	johanita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
řády	řád	k1gInPc4
sice	sice	k8xC
neposkytovaly	poskytovat	k5eNaImAgFnP
Friedrichovi	Friedrich	k1gMnSc6
přímou	přímý	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
podporovaly	podporovat	k5eAaImAgInP
ho	on	k3xPp3gNnSc4
však	však	k9
materiálně	materiálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémští	jeruzalémský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
ho	on	k3xPp3gNnSc4
zpočátku	zpočátku	k6eAd1
nadšeně	nadšeně	k6eAd1
vítali	vítat	k5eAaImAgMnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
se	se	k3xPyFc4
dozvěděli	dozvědět	k5eAaPmAgMnP
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
zacházení	zacházení	k1gNnSc4
s	s	k7c7
majetkem	majetek	k1gInSc7
Jana	Jan	k1gMnSc2
z	z	k7c2
Ibelinu	Ibelin	k1gInSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
více	hodně	k6eAd2
ostražití	ostražitý	k2eAgMnPc1d1
a	a	k8xC
nedůvěřiví	důvěřivý	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
sultán	sultán	k1gMnSc1
al-Kámil	al-Kámit	k5eAaPmAgMnS
</s>
<s>
Císař	Císař	k1gMnSc1
shromáždil	shromáždit	k5eAaPmAgMnS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
svou	svůj	k3xOyFgFnSc4
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
armáda	armáda	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
bojovaly	bojovat	k5eAaImAgFnP
v	v	k7c6
předchozích	předchozí	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
<g/>
,	,	kIx,
jen	jen	k6eAd1
pouhou	pouhý	k2eAgFnSc7d1
hrstkou	hrstka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friedrichovi	Friedrich	k1gMnSc3
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
úspěchu	úspěch	k1gInSc2
může	moct	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
jedině	jedině	k6eAd1
vyjednáváním	vyjednávání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postup	k1gInSc7
se	se	k3xPyFc4
svým	svůj	k3xOyFgNnSc7
vojskem	vojsko	k1gNnSc7
směrem	směr	k1gInSc7
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
Jeruzalému	Jeruzalém	k1gInSc3
chtěl	chtít	k5eAaImAgInS
vyděsit	vyděsit	k5eAaPmF
sultána	sultán	k1gMnSc4
Al-Kámila	Al-Kámila	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jak	jak	k6eAd1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
by	by	kYmCp3nS
poté	poté	k6eAd1
přistoupil	přistoupit	k5eAaPmAgInS
na	na	k7c4
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
křesťanům	křesťan	k1gMnPc3
vydala	vydat	k5eAaPmAgFnS
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
pás	pás	k1gInSc4
pobřeží	pobřeží	k1gNnSc2
kolem	kolem	k7c2
něj	on	k3xPp3gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Sultán	sultán	k1gMnSc1
byl	být	k5eAaImAgMnS
tímto	tento	k3xDgInSc7
jeho	jeho	k3xOp3gInSc7
troufalým	troufalý	k2eAgInSc7d1
pokusem	pokus	k1gInSc7
zaskočen	zaskočit	k5eAaPmNgInS
a	a	k8xC
zastrašen	zastrašit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
byl	být	k5eAaImAgInS
právě	právě	k6eAd1
zaměstnán	zaměstnán	k2eAgInSc1d1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
vojskem	vojsko	k1gNnSc7
v	v	k7c6
Damašku	Damašek	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
potlačoval	potlačovat	k5eAaImAgMnS
vzpouru	vzpoura	k1gFnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
k	k	k7c3
smlouvě	smlouva	k1gFnSc3
přistoupil	přistoupit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1229	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
padly	padnout	k5eAaPmAgFnP,k5eAaImAgFnP
do	do	k7c2
Friedrichových	Friedrichových	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
také	také	k9
města	město	k1gNnSc2
Jaffa	Jaffa	k1gFnSc1
<g/>
,	,	kIx,
Sidon	Sidon	k1gInSc1
<g/>
,	,	kIx,
Nazaret	Nazaret	k1gInSc1
a	a	k8xC
Betlém	Betlém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimskou	muslimský	k2eAgFnSc7d1
podmínkou	podmínka	k1gFnSc7
ve	v	k7c6
smlouvě	smlouva	k1gFnSc6
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
bude	být	k5eAaImBp3nS
ponechána	ponechat	k5eAaPmNgFnS
Chrámová	chrámový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
s	s	k7c7
mešitou	mešita	k1gFnSc7
Al-Aksá	Al-Aksý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
muslimských	muslimský	k2eAgFnPc6d1
rukou	ruka	k1gFnPc6
zůstaly	zůstat	k5eAaPmAgInP
hrady	hrad	k1gInPc1
v	v	k7c6
Zajordánsku	Zajordánsko	k1gNnSc6
a	a	k8xC
navíc	navíc	k6eAd1
Jeruzalém	Jeruzalém	k1gInSc1
měl	mít	k5eAaImAgInS
silně	silně	k6eAd1
poškozené	poškozený	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
ještě	ještě	k9
po	po	k7c6
minulé	minulý	k2eAgFnSc6d1
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
ihned	ihned	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
vypršení	vypršení	k1gNnSc6
domluveného	domluvený	k2eAgNnSc2d1
příměří	příměří	k1gNnSc2
svaté	svatý	k2eAgNnSc1d1
město	město	k1gNnSc1
znovu	znovu	k6eAd1
padne	padnout	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
rukou	ruka	k1gFnPc2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Nicméně	nicméně	k8xC
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1229	#num#	k4
vjel	vjet	k5eAaPmAgMnS
Friedrich	Friedrich	k1gMnSc1
slavnostně	slavnostně	k6eAd1
do	do	k7c2
svatého	svatý	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
korunovat	korunovat	k5eAaBmF
na	na	k7c4
krále	král	k1gMnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémský	jeruzalémský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
Gerald	Gerald	k1gMnSc1
z	z	k7c2
Lausagne	Lausagn	k1gInSc5
tuto	tento	k3xDgFnSc4
korunovaci	korunovace	k1gFnSc4
okamžitě	okamžitě	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
za	za	k7c4
neplatnou	platný	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1228	#num#	k4
<g/>
,	,	kIx,
však	však	k9
již	již	k6eAd1
po	po	k7c6
otci	otec	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
právoplatný	právoplatný	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
výpravy	výprava	k1gFnSc2
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
po	po	k7c6
šesté	šestý	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
</s>
<s>
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
tedy	tedy	k9
nad	nad	k7c4
očekávání	očekávání	k1gNnPc4
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgInSc2
boje	boj	k1gInSc2
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
křesťanům	křesťan	k1gMnPc3
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jen	jen	k9
dočasně	dočasně	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
rukou	ruka	k1gFnPc2
svaté	svatý	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friedrich	Friedrich	k1gMnSc1
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
bez	bez	k7c2
papežského	papežský	k2eAgNnSc2d1
svolení	svolení	k1gNnSc2
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
při	při	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
značně	značně	k6eAd1
podlomil	podlomit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
autoritu	autorita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dohodnutý	dohodnutý	k2eAgInSc1d1
mír	mír	k1gInSc1
trval	trvat	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1239	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
uplynutí	uplynutí	k1gNnSc6
byl	být	k5eAaImAgInS
Jeruzalém	Jeruzalém	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1244	#num#	k4
obsazen	obsadit	k5eAaPmNgInS
chórezmskými	chórezmský	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
ve	v	k7c6
službách	služba	k1gFnPc6
Turků	Turek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šestá	šestý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc1
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgFnPc3d1
Turkům	Turkovi	k1gRnPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
779432	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4139114-7	4139114-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034387	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034387	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
