<p>
<s>
Škola	škola	k1gFnSc1	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Bradavice	bradavice	k1gFnSc1	bradavice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Hogwarts	Hogwarts	k1gInSc1	Hogwarts
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Witchcraft	Witchcraft	k1gMnSc1	Witchcraft
and	and	k?	and
Wizardry	Wizardr	k1gInPc1	Wizardr
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Hogwarts	Hogwarts	k1gInSc1	Hogwarts
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kouzelnická	kouzelnický	k2eAgFnSc1d1	kouzelnická
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíci	tisíc	k4xCgInPc7	tisíc
lety	let	k1gInPc7	let
založena	založit	k5eAaPmNgFnS	založit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
největšími	veliký	k2eAgFnPc7d3	veliký
čarodějkami	čarodějka	k1gFnPc7	čarodějka
a	a	k8xC	a
čaroději	čaroděj	k1gMnPc7	čaroděj
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
–	–	k?	–
Godrikem	Godrik	k1gMnSc7	Godrik
Nebelvírem	Nebelvír	k1gMnSc7	Nebelvír
<g/>
,	,	kIx,	,
Helgou	Helga	k1gFnSc7	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
,	,	kIx,	,
Rowenou	Rowená	k1gFnSc4	Rowená
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
a	a	k8xC	a
Salazarem	Salazar	k1gMnSc7	Salazar
Zmijozelem	Zmijozel	k1gMnSc7	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
čtyři	čtyři	k4xCgFnPc1	čtyři
koleje	kolej	k1gFnPc1	kolej
–	–	k?	–
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
<g/>
,	,	kIx,	,
Mrzimor	Mrzimor	k1gMnSc1	Mrzimor
<g/>
,	,	kIx,	,
Havraspár	Havraspár	k1gMnSc1	Havraspár
a	a	k8xC	a
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Heslem	heslo	k1gNnSc7	heslo
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
Draco	Draco	k6eAd1	Draco
dormiens	dormiens	k6eAd1	dormiens
nunquam	nunquam	k6eAd1	nunquam
titillandus	titillandus	k1gInSc1	titillandus
–	–	k?	–
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
nelechtej	lechtat	k5eNaImRp2nS	lechtat
spícího	spící	k2eAgMnSc4d1	spící
draka	drak	k1gMnSc4	drak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
hymna	hymna	k1gFnSc1	hymna
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
někde	někde	k6eAd1	někde
ve	v	k7c6	v
Skotské	skotský	k2eAgFnSc6d1	skotská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Mudlům	Mudl	k1gMnPc3	Mudl
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
kouzlu	kouzlo	k1gNnSc3	kouzlo
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
zřícenina	zřícenina	k1gFnSc1	zřícenina
a	a	k8xC	a
bojí	bát	k5eAaImIp3nS	bát
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
dostanou	dostat	k5eAaPmIp3nP	dostat
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odjede	odjet	k5eAaPmIp3nS	odjet
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
z	z	k7c2	z
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
nádraží	nádraží	k1gNnSc2	nádraží
King	King	k1gInSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
vždy	vždy	k6eAd1	vždy
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
z	z	k7c2	z
nástupiště	nástupiště	k1gNnSc2	nástupiště
9	[number]	k4	9
a	a	k8xC	a
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koleje	kolej	k1gFnPc1	kolej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zařazování	zařazování	k1gNnPc4	zařazování
do	do	k7c2	do
kolejí	kolej	k1gFnPc2	kolej
===	===	k?	===
</s>
</p>
<p>
<s>
Žáci	Žák	k1gMnPc1	Žák
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
kolejí	kolej	k1gFnPc2	kolej
zařazováni	zařazovat	k5eAaImNgMnP	zařazovat
první	první	k4xOgInSc4	první
večer	večer	k1gInSc4	večer
před	před	k7c7	před
Slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
hostinou	hostina	k1gFnSc7	hostina
hned	hned	k6eAd1	hned
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnSc1	zástupce
<g/>
/	/	kIx~	/
<g/>
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
ředitele	ředitel	k1gMnSc2	ředitel
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
postupně	postupně	k6eAd1	postupně
jména	jméno	k1gNnPc4	jméno
nových	nový	k2eAgMnPc2d1	nový
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
sedne	sednout	k5eAaPmIp3nS	sednout
na	na	k7c4	na
stoličku	stolička	k1gFnSc4	stolička
a	a	k8xC	a
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
položen	položit	k5eAaPmNgInS	položit
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yQgFnSc2	jaký
koleje	kolej	k1gFnSc2	kolej
žák	žák	k1gMnSc1	žák
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Havraspár	Havraspár	k1gInSc4	Havraspár
===	===	k?	===
</s>
</p>
<p>
<s>
Havraspár	Havraspár	k1gInSc1	Havraspár
(	(	kIx(	(
<g/>
v	v	k7c6	v
originálu	originál	k1gInSc6	originál
Ravenclaw	Ravenclaw	k1gFnSc2	Ravenclaw
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc1	spojení
slov	slovo	k1gNnPc2	slovo
raven	ravno	k1gNnPc2	ravno
–	–	k?	–
krkavec	krkavec	k1gMnSc1	krkavec
či	či	k8xC	či
havran	havran	k1gMnSc1	havran
a	a	k8xC	a
claw	claw	k?	claw
–	–	k?	–
pařát	pařát	k1gInSc1	pařát
<g/>
,	,	kIx,	,
spár	spár	k1gInSc1	spár
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Rowenou	Rowena	k1gFnSc7	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
.	.	kIx.	.
</s>
<s>
Kolejními	kolejní	k2eAgFnPc7d1	kolejní
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
bronzová	bronzový	k2eAgFnSc1d1	bronzová
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmech	film	k1gInPc6	film
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
erbovním	erbovní	k2eAgNnSc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
koleje	kolej	k1gFnSc2	kolej
je	být	k5eAaImIp3nS	být
Filius	Filius	k1gInSc1	Filius
Kratiknot	kratiknot	k1gInSc1	kratiknot
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
profesorem	profesor	k1gMnSc7	profesor
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
formulí	formule	k1gFnPc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Kolejním	kolejní	k2eAgMnSc7d1	kolejní
duchem	duch	k1gMnSc7	duch
je	být	k5eAaImIp3nS	být
Šedá	šedý	k2eAgFnSc1d1	šedá
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dcera	dcera	k1gFnSc1	dcera
Roweny	Rowena	k1gFnSc2	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Helena	Helena	k1gFnSc1	Helena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
.	.	kIx.	.
</s>
<s>
Památkou	památka	k1gFnSc7	památka
po	po	k7c6	po
Roweně	Rowena	k1gFnSc6	Rowena
je	být	k5eAaImIp3nS	být
diadém	diadém	k1gInSc4	diadém
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
který	který	k3yQgInSc4	který
ukradla	ukradnout	k5eAaPmAgFnS	ukradnout
Helena	Helena	k1gFnSc1	Helena
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
)	)	kIx)	)
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
udělal	udělat	k5eAaPmAgMnS	udělat
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
svůj	svůj	k3xOyFgInSc4	svůj
viteál	viteál	k1gInSc4	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
diadém	diadém	k1gInSc1	diadém
zničen	zničen	k2eAgInSc1d1	zničen
zložárem	zložár	k1gMnSc7	zložár
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
i	i	k9	i
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
Voldemortovy	Voldemortův	k2eAgFnSc2d1	Voldemortova
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Havraspárská	Havraspárský	k2eAgFnSc1d1	Havraspárská
společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
věži	věž	k1gFnSc6	věž
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Vchodem	vchod	k1gInSc7	vchod
jsou	být	k5eAaImIp3nP	být
dveře	dveře	k1gFnPc4	dveře
bez	bez	k7c2	bez
kliky	klika	k1gFnSc2	klika
s	s	k7c7	s
klepadlem	klepadlo	k1gNnSc7	klepadlo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
havrana	havran	k1gMnSc2	havran
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
klepnutí	klepnutí	k1gNnSc6	klepnutí
vydá	vydat	k5eAaPmIp3nS	vydat
klepadlo	klepadlo	k1gNnSc4	klepadlo
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
zodpoví	zodpovědět	k5eAaPmIp3nS	zodpovědět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
klepadlu	klepadlo	k1gNnSc3	klepadlo
zdála	zdát	k5eAaImAgFnS	zdát
moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
se	se	k3xPyFc4	se
otevřou	otevřít	k5eAaPmIp3nP	otevřít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ne	ne	k9	ne
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
počkat	počkat	k5eAaPmF	počkat
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
uspokojivě	uspokojivě	k6eAd1	uspokojivě
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
Havraspáru	Havraspár	k1gInSc2	Havraspár
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
vzdušnější	vzdušný	k2eAgFnSc1d2	vzdušnější
než	než	k8xS	než
nebelvírská	belvírský	k2eNgFnSc1d1	nebelvírská
<g/>
.	.	kIx.	.
</s>
<s>
Rowena	Rowena	k1gFnSc1	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
Moudrého	moudrý	k2eAgInSc2d1	moudrý
klobouku	klobouk	k1gInSc2	klobouk
"	"	kIx"	"
<g/>
zrozená	zrozený	k2eAgFnSc1d1	zrozená
v	v	k7c6	v
lůně	lůno	k1gNnSc6	lůno
hor	hora	k1gFnPc2	hora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
snad	snad	k9	snad
mohlo	moct	k5eAaImAgNnS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
byla	být	k5eAaImAgFnS	být
obdařena	obdařit	k5eAaPmNgFnS	obdařit
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
inteligencí	inteligence	k1gFnSc7	inteligence
a	a	k8xC	a
neuvěřitelnou	uvěřitelný	k2eNgFnSc7d1	neuvěřitelná
kreativitou	kreativita	k1gFnSc7	kreativita
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
pohyblivé	pohyblivý	k2eAgFnPc4d1	pohyblivá
bradavické	bradavická	k1gFnPc4	bradavická
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Rowena	Rowena	k1gFnSc1	Rowena
měla	mít	k5eAaImAgFnS	mít
minimálně	minimálně	k6eAd1	minimálně
jedno	jeden	k4xCgNnSc4	jeden
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
dceru	dcera	k1gFnSc4	dcera
Helenu	Helena	k1gFnSc4	Helena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
.	.	kIx.	.
</s>
<s>
Šedou	šedý	k2eAgFnSc4d1	šedá
dámu	dáma	k1gFnSc4	dáma
můžete	moct	k5eAaImIp2nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
7	[number]	k4	7
díle	díl	k1gInSc6	díl
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
-	-	kIx~	-
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mrzimor	Mrzimor	k1gInSc4	Mrzimor
===	===	k?	===
</s>
</p>
<p>
<s>
Mrzimor	Mrzimor	k1gInSc1	Mrzimor
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Hufflepuff	Hufflepuff	k1gInSc1	Hufflepuff
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Helgou	Helga	k1gFnSc7	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
<s>
Kolejními	kolejní	k2eAgFnPc7d1	kolejní
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
pšenici	pšenice	k1gFnSc4	pšenice
a	a	k8xC	a
černá	černat	k5eAaImIp3nS	černat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Erbovním	erbovní	k2eAgNnSc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
jezevec	jezevec	k1gMnSc1	jezevec
<g/>
.	.	kIx.	.
</s>
<s>
Mrzimorským	Mrzimorský	k2eAgInSc7d1	Mrzimorský
elementem	element	k1gInSc7	element
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Mrzimorští	Mrzimorský	k2eAgMnPc1d1	Mrzimorský
jsou	být	k5eAaImIp3nP	být
loajální	loajální	k2eAgMnPc1d1	loajální
<g/>
,	,	kIx,	,
praví	pravý	k2eAgMnPc1d1	pravý
<g/>
,	,	kIx,	,
féroví	férový	k2eAgMnPc1d1	férový
a	a	k8xC	a
pracovití	pracovitý	k2eAgMnPc1d1	pracovitý
<g/>
.	.	kIx.	.
</s>
<s>
Kolejním	kolejní	k2eAgMnSc7d1	kolejní
duchem	duch	k1gMnSc7	duch
je	být	k5eAaImIp3nS	být
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
mnich	mnich	k1gMnSc1	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelkou	ředitelka	k1gFnSc7	ředitelka
koleje	kolej	k1gFnSc2	kolej
je	být	k5eAaImIp3nS	být
profesorka	profesorka	k1gFnSc1	profesorka
Pomona	Pomona	k1gFnSc1	Pomona
Prýtová	Prýtová	k1gFnSc1	Prýtová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
profesorkou	profesorka	k1gFnSc7	profesorka
bylinkářství	bylinkářství	k1gNnSc2	bylinkářství
<g/>
.	.	kIx.	.
</s>
<s>
Mrzimor	Mrzimor	k1gInSc1	Mrzimor
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
zakladatelky	zakladatelka	k1gFnSc2	zakladatelka
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bývala	bývat	k5eAaImAgFnS	bývat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
čarodějek	čarodějka	k1gFnPc2	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
dědiců	dědic	k1gMnPc2	dědic
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
žádný	žádný	k1gMnSc1	žádný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
Hepziba	Hepziba	k1gFnSc1	Hepziba
Smithová	Smithová	k1gFnSc1	Smithová
<g/>
,	,	kIx,	,
coby	coby	k?	coby
dědička	dědička	k1gFnSc1	dědička
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zabil	zabít	k5eAaPmAgMnS	zabít
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
neboli	neboli	k8xC	neboli
Tom	Tom	k1gMnSc1	Tom
Raddle	Raddle	k1gMnSc1	Raddle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
Helze	Helze	k1gFnPc4	Helze
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
,	,	kIx,	,
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
viteál	viteát	k5eAaPmAgMnS	viteát
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
místnosti	místnost	k1gFnSc6	místnost
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
je	být	k5eAaImIp3nS	být
ukryt	ukrýt	k5eAaPmNgInS	ukrýt
v	v	k7c6	v
hromadě	hromada	k1gFnSc6	hromada
velkých	velký	k2eAgInPc2d1	velký
sudů	sud	k1gInPc2	sud
v	v	k7c6	v
koutě	kout	k1gInSc6	kout
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
chodby	chodba	k1gFnSc2	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poklepat	poklepat	k5eAaPmF	poklepat
na	na	k7c4	na
hlaveň	hlaveň	k1gFnSc4	hlaveň
druhého	druhý	k4xOgInSc2	druhý
ze	z	k7c2	z
spodu	spod	k1gInSc6	spod
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
"	"	kIx"	"
<g/>
Helga	Helga	k1gFnSc1	Helga
Hufflepuff	Hufflepuff	k1gMnSc1	Hufflepuff
<g/>
"	"	kIx"	"
a	a	k8xC	a
víko	víko	k1gNnSc1	víko
se	se	k3xPyFc4	se
otevře	otevřít	k5eAaPmIp3nS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Mrzimor	Mrzimor	k1gInSc1	Mrzimor
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
odpuzující	odpuzující	k2eAgNnSc4d1	odpuzující
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
případné	případný	k2eAgMnPc4d1	případný
vetřelce	vetřelec	k1gMnPc4	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
špatné	špatný	k2eAgNnSc1d1	špatné
víko	víko	k1gNnSc1	víko
poklepáno	poklepán	k2eAgNnSc1d1	poklepáno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rytmus	rytmus	k1gInSc1	rytmus
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nelegální	legální	k2eNgMnSc1d1	nelegální
účastník	účastník	k1gMnSc1	účastník
polit	polít	k5eAaPmNgMnS	polít
octem	ocet	k1gInSc7	ocet
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
zdobena	zdoben	k2eAgFnSc1d1	zdobena
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
sem	sem	k6eAd1	sem
dává	dávat	k5eAaImIp3nS	dávat
profesorka	profesorka	k1gFnSc1	profesorka
Prýtová	Prýtová	k1gFnSc1	Prýtová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Mrzimoru	Mrzimora	k1gFnSc4	Mrzimora
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gMnSc2	Malfoa
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
díle	dílo	k1gNnSc6	dílo
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Hagrid	Hagrid	k1gInSc1	Hagrid
Harrymu	Harrym	k1gInSc2	Harrym
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
Mrzimoru	Mrzimor	k1gInSc6	Mrzimor
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
kolej	kolej	k1gFnSc1	kolej
hlupáků	hlupák	k1gMnPc2	hlupák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
nejméně	málo	k6eAd3	málo
černokněžníků	černokněžník	k1gMnPc2	černokněžník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
kolejí	kolej	k1gFnPc2	kolej
Mrzimor	Mrzimor	k1gMnSc1	Mrzimor
vychází	vycházet	k5eAaImIp3nS	vycházet
nejlépe	dobře	k6eAd3	dobře
s	s	k7c7	s
Nebelvírem	Nebelvír	k1gInSc7	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
studoval	studovat	k5eAaImAgMnS	studovat
např.	např.	kA	např.
Cedric	Cedric	k1gMnSc1	Cedric
Diggory	Diggora	k1gFnSc2	Diggora
<g/>
,	,	kIx,	,
Mlok	mlok	k1gMnSc1	mlok
Scamander	Scamander	k1gMnSc1	Scamander
nebo	nebo	k8xC	nebo
Nymfadora	Nymfadora	k1gFnSc1	Nymfadora
Tonksová	Tonksová	k1gFnSc1	Tonksová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nebelvír	Nebelvír	k1gInSc4	Nebelvír
===	===	k?	===
</s>
</p>
<p>
<s>
Nebelvír	Nebelvír	k1gInSc1	Nebelvír
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Gryffindor	Gryffindor	k1gInSc1	Gryffindor
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Godrikem	Godrik	k1gMnSc7	Godrik
Nebelvírem	Nebelvír	k1gMnSc7	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Kolejními	kolejní	k2eAgFnPc7d1	kolejní
barvami	barva	k1gFnPc7	barva
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
jsou	být	k5eAaImIp3nP	být
rudá	rudý	k2eAgFnSc1d1	rudá
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Erbovním	erbovní	k2eAgNnSc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Kolejním	kolejní	k2eAgMnSc7d1	kolejní
duchem	duch	k1gMnSc7	duch
je	být	k5eAaImIp3nS	být
Sir	sir	k1gMnSc1	sir
Nicholas	Nicholas	k1gMnSc1	Nicholas
de	de	k?	de
Mimsy-Porpington	Mimsy-Porpington	k1gInSc1	Mimsy-Porpington
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Skoro	skoro	k6eAd1	skoro
bezhlavý	bezhlavý	k2eAgInSc1d1	bezhlavý
Nick	Nick	k1gInSc1	Nick
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelkou	ředitelka	k1gFnSc7	ředitelka
koleje	kolej	k1gFnSc2	kolej
je	být	k5eAaImIp3nS	být
Minerva	Minerva	k1gFnSc1	Minerva
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
zástupkyní	zástupkyně	k1gFnSc7	zástupkyně
ředitele	ředitel	k1gMnSc2	ředitel
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
ředitelkou	ředitelka	k1gFnSc7	ředitelka
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesorkou	profesorka	k1gFnSc7	profesorka
přeměňování	přeměňování	k1gNnSc2	přeměňování
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
střeží	střežit	k5eAaImIp3nS	střežit
portrét	portrét	k1gInSc4	portrét
Buclaté	buclatý	k2eAgFnSc2d1	buclatá
dámy	dáma	k1gFnSc2	dáma
a	a	k8xC	a
heslo	heslo	k1gNnSc1	heslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Nebelvírská	Nebelvírský	k2eAgFnSc1d1	Nebelvírská
společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
útulná	útulný	k2eAgFnSc1d1	útulná
<g/>
.	.	kIx.	.
</s>
<s>
Nebelvír	Nebelvír	k1gInSc1	Nebelvír
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
přítelem	přítel	k1gMnSc7	přítel
Salazara	Salazar	k1gMnSc2	Salazar
Zmijozela	Zmijozela	k1gMnSc2	Zmijozela
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
cesty	cesta	k1gFnPc1	cesta
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
o	o	k7c6	o
vedení	vedení	k1gNnSc6	vedení
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc3	budoucnost
školy	škola	k1gFnSc2	škola
rozešly	rozejít	k5eAaPmAgFnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
dědiců	dědic	k1gMnPc2	dědic
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgMnSc1	žádný
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Harry	Harr	k1gInPc1	Harr
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Neville	Neville	k1gFnSc1	Neville
Longbottom	Longbottom	k1gInSc1	Longbottom
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
narodili	narodit	k5eAaPmAgMnP	narodit
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
znamení	znamení	k1gNnSc4	znamení
lva	lev	k1gInSc2	lev
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Nejmagičtější	magický	k2eAgMnSc1d3	magický
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Památkou	památka	k1gFnSc7	památka
na	na	k7c4	na
Godrika	Godrik	k1gMnSc4	Godrik
Nebelvíra	Nebelvír	k1gMnSc4	Nebelvír
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
posetý	posetý	k2eAgInSc1d1	posetý
rubíny	rubín	k1gInPc4	rubín
(	(	kIx(	(
<g/>
Nebelvírův	Nebelvírův	k2eAgInSc1d1	Nebelvírův
meč	meč	k1gInSc1	meč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
skřeti	skřet	k1gMnPc1	skřet
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zjeví	zjevit	k5eAaPmIp3nS	zjevit
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
z	z	k7c2	z
pochvy	pochva	k1gFnSc2	pochva
klobouku	klobouk	k1gInSc2	klobouk
jen	jen	k9	jen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
a	a	k8xC	a
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
-li	i	k?	-li
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
-li	i	k?	-li
hrdinství	hrdinství	k1gNnSc4	hrdinství
a	a	k8xC	a
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
skřeti	skřet	k1gMnPc1	skřet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgNnSc1d1	unikátní
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odolává	odolávat	k5eAaImIp3nS	odolávat
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
mohlo	moct	k5eAaImAgNnS	moct
ublížit	ublížit	k5eAaPmF	ublížit
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gNnSc4	on
poškodit	poškodit	k5eAaPmF	poškodit
a	a	k8xC	a
nasává	nasávat	k5eAaImIp3nS	nasávat
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
posilňuje	posilňovat	k5eAaImIp3nS	posilňovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
ředitelově	ředitelův	k2eAgFnSc6d1	ředitelova
pracovně	pracovna	k1gFnSc6	pracovna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
předmět	předmět	k1gInSc1	předmět
po	po	k7c6	po
zakladatelích	zakladatel	k1gMnPc6	zakladatel
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgMnS	stát
viteál	viteál	k1gMnSc1	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
studovali	studovat	k5eAaImAgMnP	studovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Neville	Neville	k1gNnSc4	Neville
Longbottom	Longbottom	k1gInSc4	Longbottom
<g/>
,	,	kIx,	,
Weasleyovi	Weasleyův	k2eAgMnPc1d1	Weasleyův
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
Hermiona	Hermiona	k1gFnSc1	Hermiona
Grangerová	Grangerová	k1gFnSc1	Grangerová
nebo	nebo	k8xC	nebo
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zmijozel	Zmijozel	k1gMnPc5	Zmijozel
===	===	k?	===
</s>
</p>
<p>
<s>
Zmijozel	Zmijozet	k5eAaPmAgInS	Zmijozet
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Slytherin	Slytherin	k1gInSc1	Slytherin
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Salazarem	Salazar	k1gMnSc7	Salazar
Zmijozelem	Zmijozel	k1gMnSc7	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Salazar	Salazar	k1gMnSc1	Salazar
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
vybírali	vybírat	k5eAaImAgMnP	vybírat
jen	jen	k9	jen
z	z	k7c2	z
čistokrevných	čistokrevný	k2eAgFnPc2d1	čistokrevná
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozkolu	rozkol	k1gInSc3	rozkol
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
třemi	tři	k4xCgInPc7	tři
zakladateli	zakladatel	k1gMnPc7	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bradavice	bradavice	k1gFnSc2	bradavice
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
zanechal	zanechat	k5eAaPmAgMnS	zanechat
však	však	k9	však
po	po	k7c6	po
sobě	se	k3xPyFc3	se
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
střežil	střežit	k5eAaImAgMnS	střežit
Bazilišek	bazilišek	k1gMnSc1	bazilišek
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
směl	smět	k5eAaImAgMnS	smět
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
otevřít	otevřít	k5eAaPmF	otevřít
jen	jen	k9	jen
jeho	jeho	k3xOp3gMnSc1	jeho
dědic	dědic	k1gMnSc1	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgNnPc1d1	Rowlingové
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
portugalským	portugalský	k2eAgMnSc7d1	portugalský
diktátorem	diktátor	k1gMnSc7	diktátor
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1932	[number]	k4	1932
–	–	k?	–
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Antóniu	Antónium	k1gNnSc6	Antónium
de	de	k?	de
Oliveira	Oliveir	k1gInSc2	Oliveir
Salazarovi	Salazar	k1gMnSc3	Salazar
<g/>
.	.	kIx.	.
</s>
<s>
Kolejními	kolejní	k2eAgFnPc7d1	kolejní
barvami	barva	k1gFnPc7	barva
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
jsou	být	k5eAaImIp3nP	být
zelená	zelený	k2eAgNnPc1d1	zelené
a	a	k8xC	a
stříbrná	stříbrný	k2eAgNnPc1d1	stříbrné
<g/>
.	.	kIx.	.
</s>
<s>
Erbovním	erbovní	k2eAgNnSc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
had	had	k1gMnSc1	had
<g/>
.	.	kIx.	.
</s>
<s>
Kolejním	kolejní	k2eAgMnSc7d1	kolejní
duchem	duch	k1gMnSc7	duch
je	být	k5eAaImIp3nS	být
Krvavý	krvavý	k2eAgMnSc1d1	krvavý
baron	baron	k1gMnSc1	baron
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
duchem	duch	k1gMnSc7	duch
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
navždy	navždy	k6eAd1	navždy
být	být	k5eAaImF	být
s	s	k7c7	s
milovanou	milovaný	k2eAgFnSc7d1	milovaná
Helenou	Helena	k1gFnSc7	Helena
Ravenclaw	Ravenclaw	k1gFnSc2	Ravenclaw
(	(	kIx(	(
<g/>
Šedá	šedý	k2eAgFnSc1d1	šedá
dáma	dáma	k1gFnSc1	dáma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
i	i	k9	i
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
zmijozelské	zmijozelský	k2eAgFnSc2d1	zmijozelská
koleje	kolej	k1gFnSc2	kolej
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
převzal	převzít	k5eAaPmAgMnS	převzít
ředitelské	ředitelský	k2eAgNnSc4d1	ředitelské
místo	místo	k1gNnSc4	místo
Horácio	Horácio	k1gMnSc1	Horácio
Křiklan	Křiklan	k1gMnSc1	Křiklan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
koleje	kolej	k1gFnSc2	kolej
již	již	k9	již
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
než	než	k8xS	než
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Zmijozel	Zmijozet	k5eAaPmAgMnS	Zmijozet
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
kolej	kolej	k1gFnSc1	kolej
zlých	zlý	k2eAgMnPc2d1	zlý
čarodějů	čaroděj	k1gMnPc2	čaroděj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
ze	z	k7c2	z
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
jsou	být	k5eAaImIp3nP	být
zlí	zlý	k2eAgMnPc1d1	zlý
–	–	k?	–
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gMnPc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ctižádost	ctižádost	k1gFnSc4	ctižádost
a	a	k8xC	a
"	"	kIx"	"
<g/>
nebýt	být	k5eNaImF	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
když	když	k8xS	když
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
neplyne	plynout	k5eNaImIp3nS	plynout
zisk	zisk	k1gInSc1	zisk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
žáci	žák	k1gMnPc1	žák
Zmijozelu	Zmijozela	k1gFnSc4	Zmijozela
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
potomky	potomek	k1gMnPc7	potomek
čistokrevných	čistokrevný	k2eAgFnPc2d1	čistokrevná
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
není	být	k5eNaImIp3nS	být
často	často	k6eAd1	často
dodržováno	dodržován	k2eAgNnSc1d1	dodržováno
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
je	být	k5eAaImIp3nS	být
ukryt	ukryt	k2eAgInSc1d1	ukryt
za	za	k7c7	za
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
a	a	k8xC	a
otevírá	otevírat	k5eAaImIp3nS	otevírat
se	se	k3xPyFc4	se
heslem	heslo	k1gNnSc7	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Místnost	místnost	k1gFnSc1	místnost
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
umístěná	umístěný	k2eAgFnSc1d1	umístěná
pod	pod	k7c7	pod
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
koleji	kolej	k1gFnSc6	kolej
byl	být	k5eAaImAgInS	být
bývalým	bývalý	k2eAgMnSc7d1	bývalý
žákem	žák	k1gMnSc7	žák
i	i	k8xC	i
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Rojvol	Rojvola	k1gFnPc2	Rojvola
Raddle	Raddle	k1gMnSc1	Raddle
neboli	neboli	k8xC	neboli
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Studovali	studovat	k5eAaImAgMnP	studovat
zde	zde	k6eAd1	zde
také	také	k9	také
Draco	Draco	k6eAd1	Draco
Malfoy	Malfo	k2eAgFnPc1d1	Malfo
<g/>
,	,	kIx,	,
Leta	Let	k2eAgFnSc1d1	Leta
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
<g/>
,	,	kIx,	,
Pansy	Pansa	k1gFnPc1	Pansa
Paarkinsonová	Paarkinsonová	k1gFnSc1	Paarkinsonová
<g/>
,	,	kIx,	,
Dolores	Dolores	k1gInSc1	Dolores
Umbridgeová	Umbridgeová	k1gFnSc1	Umbridgeová
<g/>
,	,	kIx,	,
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
Belatrix	Belatrix	k1gInSc1	Belatrix
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
nebo	nebo	k8xC	nebo
Horácio	Horácio	k1gMnSc1	Horácio
Křiklan	Křiklan	k1gMnSc1	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
koleji	kolej	k1gFnSc6	kolej
jelikož	jelikož	k8xS	jelikož
umí	umět	k5eAaImIp3nS	umět
mluvit	mluvit	k5eAaImF	mluvit
s	s	k7c7	s
hady	had	k1gMnPc7	had
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
==	==	k?	==
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
studenti	student	k1gMnPc1	student
skládají	skládat	k5eAaImIp3nP	skládat
zkoušky	zkouška	k1gFnPc4	zkouška
NKÚ	NKÚ	kA	NKÚ
(	(	kIx(	(
<g/>
náležitá	náležitý	k2eAgFnSc1d1	náležitá
kouzelnická	kouzelnický	k2eAgFnSc1d1	kouzelnická
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
)	)	kIx)	)
a	a	k8xC	a
vybírají	vybírat	k5eAaImIp3nP	vybírat
si	se	k3xPyFc3	se
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
OVCE	ovce	k1gFnPc4	ovce
(	(	kIx(	(
<g/>
ohavně	ohavně	k6eAd1	ohavně
vyčerpávající	vyčerpávající	k2eAgInPc1d1	vyčerpávající
celočarodějné	celočarodějný	k2eAgInPc1d1	celočarodějný
exameny	examen	k1gInPc1	examen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
skládají	skládat	k5eAaImIp3nP	skládat
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
7	[number]	k4	7
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
předmětů	předmět	k1gInPc2	předmět
všelijak	všelijak	k6eAd1	všelijak
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
budoucím	budoucí	k2eAgNnSc7d1	budoucí
povoláním	povolání	k1gNnSc7	povolání
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c6	na
povinné	povinný	k2eAgFnSc6d1	povinná
(	(	kIx(	(
<g/>
studenti	student	k1gMnPc1	student
je	on	k3xPp3gMnPc4	on
musí	muset	k5eAaImIp3nP	muset
studovat	studovat	k5eAaImF	studovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
až	až	k9	až
do	do	k7c2	do
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
a	a	k8xC	a
volitelné	volitelný	k2eAgNnSc1d1	volitelné
(	(	kIx(	(
<g/>
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
další	další	k2eAgInSc1d1	další
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc4	dva
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
pak	pak	k6eAd1	pak
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
z	z	k7c2	z
povinných	povinný	k2eAgMnPc2d1	povinný
skládají	skládat	k5eAaImIp3nP	skládat
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povinnými	povinný	k2eAgInPc7d1	povinný
předměty	předmět	k1gInPc7	předmět
jsou	být	k5eAaImIp3nP	být
přeměňování	přeměňování	k1gNnSc1	přeměňování
<g/>
,	,	kIx,	,
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
bylinkářství	bylinkářství	k1gNnSc4	bylinkářství
<g/>
,	,	kIx,	,
kouzelné	kouzelný	k2eAgFnPc4d1	kouzelná
formule	formule	k1gFnPc4	formule
<g/>
,	,	kIx,	,
lektvary	lektvar	k1gInPc4	lektvar
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc4	dějiny
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
mají	mít	k5eAaImIp3nP	mít
studenti	student	k1gMnPc1	student
navíc	navíc	k6eAd1	navíc
létání	létání	k1gNnSc4	létání
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
od	od	k7c2	od
třetího	třetí	k4xOgNnSc2	třetí
ročníku	ročník	k1gInSc6	ročník
si	se	k3xPyFc3	se
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
předmětů	předmět	k1gInPc2	předmět
jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
<g/>
,	,	kIx,	,
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
věštění	věštění	k1gNnSc4	věštění
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
mudlů	mudl	k1gInPc2	mudl
a	a	k8xC	a
starodávné	starodávný	k2eAgFnPc1d1	starodávná
runy	runa	k1gFnPc1	runa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dovršení	dovršení	k1gNnSc6	dovršení
plnoletosti	plnoletost	k1gFnSc2	plnoletost
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
studenti	student	k1gMnPc1	student
zapsat	zapsat	k5eAaPmF	zapsat
do	do	k7c2	do
kurzů	kurz	k1gInPc2	kurz
přemisťování	přemisťování	k1gNnSc2	přemisťování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
ročníku	ročník	k1gInSc6	ročník
se	se	k3xPyFc4	se
také	také	k9	také
vybírají	vybírat	k5eAaImIp3nP	vybírat
prefekti	prefekt	k1gMnPc1	prefekt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
primus	primus	k1gInSc4	primus
<g/>
.	.	kIx.	.
</s>
<s>
Prefekti	prefekt	k1gMnPc1	prefekt
a	a	k8xC	a
primusové	primus	k1gMnPc1	primus
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ti	ten	k3xDgMnPc1	ten
nejzdatnější	zdatný	k2eAgMnPc1d3	nejzdatnější
žáci	žák	k1gMnPc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
učitelům	učitel	k1gMnPc3	učitel
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
strhávat	strhávat	k5eAaImF	strhávat
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studentský	studentský	k2eAgInSc4d1	studentský
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
začíná	začínat	k5eAaImIp3nS	začínat
snídaní	snídaně	k1gFnPc2	snídaně
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
sedí	sedit	k5eAaImIp3nP	sedit
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
své	svůj	k3xOyFgFnSc2	svůj
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
jíst	jíst	k5eAaImF	jíst
a	a	k8xC	a
socializovat	socializovat	k5eAaBmF	socializovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dodělávat	dodělávat	k5eAaImF	dodělávat
domací	domací	k2eAgInPc4d1	domací
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
či	či	k8xC	či
ředitelka	ředitelka	k1gFnSc1	ředitelka
jí	jíst	k5eAaImIp3nS	jíst
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
profesory	profesor	k1gMnPc7	profesor
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
pro	pro	k7c4	pro
profesory	profesor	k1gMnPc4	profesor
na	na	k7c6	na
konci	konec	k1gInSc6	konec
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
snídaně	snídaně	k1gFnSc2	snídaně
nosí	nosit	k5eAaImIp3nS	nosit
sovy	sova	k1gFnPc4	sova
studentům	student	k1gMnPc3	student
poštu	pošta	k1gFnSc4	pošta
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
výtisků	výtisk	k1gInPc2	výtisk
Denního	denní	k2eAgMnSc2d1	denní
věštce	věštec	k1gMnSc2	věštec
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
nebo	nebo	k8xC	nebo
balíčků	balíček	k1gInPc2	balíček
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
hlásí	hlásit	k5eAaImIp3nP	hlásit
zvonek	zvonek	k1gInSc4	zvonek
začátek	začátek	k1gInSc1	začátek
první	první	k4xOgFnSc2	první
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopoledne	dopoledne	k1gNnPc1	dopoledne
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
přestávkou	přestávka	k1gFnSc7	přestávka
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
učeben	učebna	k1gFnPc2	učebna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
hodiny	hodina	k1gFnSc2	hodina
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
od	od	k7c2	od
13	[number]	k4	13
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
odpoledne	odpoledne	k6eAd1	odpoledne
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
dalšími	další	k2eAgFnPc7d1	další
hodinami	hodina	k1gFnPc7	hodina
opět	opět	k6eAd1	opět
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
trvají	trvat	k5eAaImIp3nP	trvat
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
občasné	občasný	k2eAgFnPc1d1	občasná
dvojité	dvojitý	k2eAgFnPc1d1	dvojitá
hodiny	hodina	k1gFnPc1	hodina
trvají	trvat	k5eAaImIp3nP	trvat
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vyučování	vyučování	k1gNnSc1	vyučování
končí	končit	k5eAaImIp3nS	končit
kolem	kolem	k7c2	kolem
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Žáci	Žák	k1gMnPc1	Žák
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
mívají	mívat	k5eAaImIp3nP	mívat
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
odpoledne	odpoledne	k6eAd1	odpoledne
volno	volno	k6eAd1	volno
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
šestého	šestý	k4xOgNnSc2	šestý
a	a	k8xC	a
sedmého	sedmý	k4xOgInSc2	sedmý
ročníku	ročník	k1gInSc2	ročník
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
volných	volný	k2eAgFnPc2d1	volná
hodin	hodina	k1gFnPc2	hodina
během	během	k7c2	během
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
podávána	podávat	k5eAaImNgFnS	podávat
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
studenti	student	k1gMnPc1	student
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
společenských	společenský	k2eAgFnPc2d1	společenská
místností	místnost	k1gFnPc2	místnost
svých	svůj	k3xOyFgFnPc2	svůj
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
astronomie	astronomie	k1gFnSc2	astronomie
probíhají	probíhat	k5eAaImIp3nP	probíhat
pozdě	pozdě	k6eAd1	pozdě
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
Astronomické	astronomický	k2eAgFnSc6d1	astronomická
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
koleje	kolej	k1gFnPc1	kolej
mají	mít	k5eAaImIp3nP	mít
tajné	tajný	k2eAgInPc4d1	tajný
vchody	vchod	k1gInPc4	vchod
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
známé	známý	k2eAgNnSc1d1	známé
jen	jen	k9	jen
členům	člen	k1gInPc3	člen
koleje	kolej	k1gFnSc2	kolej
a	a	k8xC	a
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
znát	znát	k5eAaImF	znát
heslo	heslo	k1gNnSc4	heslo
(	(	kIx(	(
<g/>
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
<g/>
,	,	kIx,	,
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uhádnout	uhádnout	k5eAaPmF	uhádnout
hádanku	hádanka	k1gFnSc4	hádanka
(	(	kIx(	(
<g/>
Havraspár	Havraspár	k1gInSc4	Havraspár
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
provést	provést	k5eAaPmF	provést
rituál	rituál	k1gInSc4	rituál
(	(	kIx(	(
<g/>
Mrzimor	Mrzimor	k1gInSc4	Mrzimor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
s	s	k7c7	s
křesly	křeslo	k1gNnPc7	křeslo
a	a	k8xC	a
sedačkami	sedačka	k1gFnPc7	sedačka
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
a	a	k8xC	a
stoly	stol	k1gInPc4	stol
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
úloh	úloha	k1gFnPc2	úloha
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
krb	krb	k1gInSc1	krb
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yRgInSc2	který
mohou	moct	k5eAaImIp3nP	moct
studenti	student	k1gMnPc1	student
posedávat	posedávat	k5eAaImF	posedávat
a	a	k8xC	a
relaxovat	relaxovat	k5eAaBmF	relaxovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společenských	společenský	k2eAgFnPc6d1	společenská
místnostech	místnost	k1gFnPc6	místnost
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
strategických	strategický	k2eAgNnPc6d1	strategické
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
visí	viset	k5eAaImIp3nS	viset
nástěnky	nástěnka	k1gFnPc1	nástěnka
na	na	k7c4	na
důležitá	důležitý	k2eAgNnPc4d1	důležité
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
mohou	moct	k5eAaImIp3nP	moct
přejít	přejít	k5eAaPmF	přejít
studenti	student	k1gMnPc1	student
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
ložnice	ložnice	k1gFnSc2	ložnice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
přespávají	přespávat	k5eAaImIp3nP	přespávat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dívčí	dívčí	k2eAgFnSc1d1	dívčí
ložnice	ložnice	k1gFnSc1	ložnice
je	být	k5eAaImIp3nS	být
očarována	očarovat	k5eAaPmNgFnS	očarovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nemohli	moct	k5eNaImAgMnP	moct
vstupovat	vstupovat	k5eAaImF	vstupovat
chlapci	chlapec	k1gMnPc1	chlapec
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
dívky	dívka	k1gFnPc1	dívka
mohou	moct	k5eAaImIp3nP	moct
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
ložnice	ložnice	k1gFnSc2	ložnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
žák	žák	k1gMnSc1	žák
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
postel	postel	k1gFnSc4	postel
s	s	k7c7	s
nebesy	nebesa	k1gNnPc7	nebesa
s	s	k7c7	s
povlečením	povlečení	k1gNnSc7	povlečení
a	a	k8xC	a
závěsy	závěs	k1gInPc7	závěs
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
každé	každý	k3xTgFnSc2	každý
postele	postel	k1gFnSc2	postel
stojí	stát	k5eAaImIp3nS	stát
noční	noční	k2eAgInSc4d1	noční
stolek	stolek	k1gInSc4	stolek
a	a	k8xC	a
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ložnici	ložnice	k1gFnSc6	ložnice
je	být	k5eAaImIp3nS	být
také	také	k9	také
trvale	trvale	k6eAd1	trvale
džbán	džbán	k1gInSc4	džbán
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
sklenice	sklenice	k1gFnSc1	sklenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
určených	určený	k2eAgInPc6d1	určený
víkendech	víkend	k1gInPc6	víkend
mohou	moct	k5eAaImIp3nP	moct
studenti	student	k1gMnPc1	student
třetích	třetí	k4xOgFnPc2	třetí
a	a	k8xC	a
vyšších	vysoký	k2eAgInPc2d2	vyšší
ročníků	ročník	k1gInPc2	ročník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
podepsané	podepsaný	k2eAgNnSc4d1	podepsané
povolení	povolení	k1gNnSc4	povolení
<g/>
,	,	kIx,	,
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
blízkou	blízký	k2eAgFnSc4d1	blízká
čarodějnickou	čarodějnický	k2eAgFnSc4d1	čarodějnická
vesnici	vesnice	k1gFnSc4	vesnice
Prasinky	Prasinka	k1gFnSc2	Prasinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
procházet	procházet	k5eAaImF	procházet
nebo	nebo	k8xC	nebo
využít	využít	k5eAaPmF	využít
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
hospod	hospod	k?	hospod
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Prasinkách	Prasinka	k1gFnPc6	Prasinka
patří	patřit	k5eAaImIp3nS	patřit
Medový	medový	k2eAgInSc1d1	medový
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
Taškářovy	taškářův	k2eAgInPc1d1	taškářův
žertovné	žertovný	k2eAgInPc1d1	žertovný
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
hostinec	hostinec	k1gInSc1	hostinec
U	u	k7c2	u
Tří	tři	k4xCgNnPc2	tři
košťat	koště	k1gNnPc2	koště
a	a	k8xC	a
Chroptící	chroptící	k2eAgFnSc2d1	chroptící
chýše	chýš	k1gFnSc2	chýš
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
straší	strašit	k5eAaImIp3nS	strašit
nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předměty	předmět	k1gInPc1	předmět
a	a	k8xC	a
profesoři	profesor	k1gMnPc1	profesor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přeměňování	přeměňování	k1gNnSc2	přeměňování
===	===	k?	===
</s>
</p>
<p>
<s>
Přeměňování	přeměňování	k1gNnSc1	přeměňování
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc1	umění
měnit	měnit	k5eAaImF	měnit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Studenty	student	k1gMnPc4	student
učí	učit	k5eAaImIp3nP	učit
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
věci	věc	k1gFnPc4	věc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
učebnici	učebnice	k1gFnSc6	učebnice
<g/>
)	)	kIx)	)
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
věci	věc	k1gFnSc6	věc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
krysu	krysa	k1gFnSc4	krysa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předmět	předmět	k1gInSc1	předmět
učí	učit	k5eAaImIp3nS	učit
Harryho	Harry	k1gMnSc4	Harry
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
profesorka	profesorka	k1gFnSc1	profesorka
Minerva	Minerva	k1gFnSc1	Minerva
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
díle	dílo	k1gNnSc6	dílo
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předmět	předmět	k1gInSc1	předmět
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
již	již	k6eAd1	již
39	[number]	k4	39
let	léto	k1gNnPc2	léto
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
je	být	k5eAaImIp3nS	být
zvěromág	zvěromág	k1gInSc4	zvěromág
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
se	se	k3xPyFc4	se
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c4	v
kočku	kočka	k1gFnSc4	kočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Bradavic	bradavice	k1gFnPc2	bradavice
Armando	Armanda	k1gFnSc5	Armanda
Dippet	Dippet	k1gInSc4	Dippet
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgMnS	učit
přeměňování	přeměňování	k1gNnSc4	přeměňování
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
učí	učit	k5eAaImIp3nP	učit
obranné	obranný	k2eAgFnSc2d1	obranná
techniky	technika	k1gFnSc2	technika
před	před	k7c7	před
černou	černý	k2eAgFnSc7d1	černá
magií	magie	k1gFnSc7	magie
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
se	se	k3xPyFc4	se
před	před	k7c7	před
zlými	zlý	k2eAgNnPc7d1	zlé
stvořeními	stvoření	k1gNnPc7	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
praxe	praxe	k1gFnSc1	praxe
v	v	k7c6	v
obranných	obranný	k2eAgNnPc6d1	obranné
kouzlech	kouzlo	k1gNnPc6	kouzlo
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
kletbách	kletba	k1gFnPc6	kletba
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
setkat	setkat	k5eAaPmF	setkat
kouzelníci	kouzelník	k1gMnPc1	kouzelník
mimo	mimo	k7c4	mimo
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
předmětu	předmět	k1gInSc2	předmět
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
,	,	kIx,	,
během	během	k7c2	během
Harryho	Harry	k1gMnSc2	Harry
studia	studio	k1gNnSc2	studio
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
post	post	k1gInSc4	post
profesora	profesor	k1gMnSc2	profesor
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
je	být	k5eAaImIp3nS	být
zakletý	zakletý	k2eAgInSc1d1	zakletý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
žádal	žádat	k5eAaImAgMnS	žádat
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
jednou	jednou	k6eAd1	jednou
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Voldemortově	Voldemortův	k2eAgFnSc6d1	Voldemortova
smrti	smrt	k1gFnSc6	smrt
kletba	kletba	k1gFnSc1	kletba
zmizela	zmizet	k5eAaPmAgFnS	zmizet
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
kapitolou	kapitola	k1gFnSc7	kapitola
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
epilogem	epilog	k1gInSc7	epilog
předmět	předmět	k1gInSc1	předmět
učila	učit	k5eAaImAgFnS	učit
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Voldemortova	Voldemortův	k2eAgNnSc2d1	Voldemortovo
studia	studio	k1gNnSc2	studio
učila	učít	k5eAaPmAgFnS	učít
obranu	obrana	k1gFnSc4	obrana
profesorka	profesorka	k1gFnSc1	profesorka
Galatea	Galatea	k1gFnSc1	Galatea
Merrythoughtová	Merrythoughtová	k1gFnSc1	Merrythoughtová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
odchodu	odchod	k1gInSc6	odchod
se	se	k3xPyFc4	se
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
ucházel	ucházet	k5eAaImAgMnS	ucházet
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Brumbál	brumbál	k1gMnSc1	brumbál
ho	on	k3xPp3gInSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
éra	éra	k1gFnSc1	éra
učitelů	učitel	k1gMnPc2	učitel
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Harryho	Harry	k1gMnSc2	Harry
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
na	na	k7c6	na
postu	post	k1gInSc6	post
profesora	profesor	k1gMnSc2	profesor
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
postupně	postupně	k6eAd1	postupně
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Quirinus	Quirinus	k1gMnSc1	Quirinus
Quirrell	Quirrell	k1gMnSc1	Quirrell
<g/>
,	,	kIx,	,
koktavý	koktavý	k2eAgMnSc1d1	koktavý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
turbanem	turban	k1gInSc7	turban
okolo	okolo	k7c2	okolo
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
,	,	kIx,	,
Zlatoslav	Zlatoslav	k1gMnSc1	Zlatoslav
Lockhart	Lockhart	k1gInSc1	Lockhart
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
bojovník	bojovník	k1gMnSc1	bojovník
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
magií	magie	k1gFnSc7	magie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
činech	čin	k1gInPc6	čin
lhal	lhát	k5eAaImAgMnS	lhát
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byl	být	k5eAaImAgMnS	být
<g />
.	.	kIx.	.
</s>
<s>
zbabělý	zbabělý	k2eAgMnSc1d1	zbabělý
<g/>
,	,	kIx,	,
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
<g/>
,	,	kIx,	,
dávný	dávný	k2eAgMnSc1d1	dávný
přítel	přítel	k1gMnSc1	přítel
Jamese	Jamese	k1gFnSc2	Jamese
Pottera	Potter	k1gMnSc2	Potter
a	a	k8xC	a
snad	snad	k9	snad
nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
profesor	profesor	k1gMnSc1	profesor
tohoto	tento	k3xDgInSc2	tento
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
Alastor	Alastor	k1gMnSc1	Alastor
Moody	Mooda	k1gFnSc2	Mooda
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bystrozor	bystrozor	k1gMnSc1	bystrozor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
přestrojený	přestrojený	k2eAgInSc1d1	přestrojený
smrtijed	smrtijed	k1gInSc1	smrtijed
Barty	Barta	k1gFnSc2	Barta
Skrk	Skrka	k1gFnPc2	Skrka
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Dolores	Dolores	k1gMnSc1	Dolores
Umbridgeová	Umbridgeová	k1gFnSc1	Umbridgeová
<g/>
,	,	kIx,	,
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
profesorka	profesorka	k1gFnSc1	profesorka
dosazená	dosazený	k2eAgFnSc1d1	dosazená
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
místo	místo	k1gNnSc4	místo
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
letech	let	k1gInPc6	let
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
Amycus	Amycus	k1gMnSc1	Amycus
Carrow	Carrow	k1gMnSc1	Carrow
<g/>
,	,	kIx,	,
smrtijed	smrtijed	k1gInSc1	smrtijed
dosazený	dosazený	k2eAgInSc1d1	dosazený
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Styl	styl	k1gInSc1	styl
učení	učení	k1gNnSc2	učení
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
profesorů	profesor	k1gMnPc2	profesor
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
během	během	k7c2	během
Lockhartova	Lockhartův	k2eAgNnSc2d1	Lockhartův
působení	působení	k1gNnSc2	působení
studenti	student	k1gMnPc1	student
pouze	pouze	k6eAd1	pouze
předčítali	předčítat	k5eAaImAgMnP	předčítat
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lupinově	lupinově	k6eAd1	lupinově
a	a	k8xC	a
Moodyho	Moody	k1gMnSc2	Moody
éře	éra	k1gFnSc6	éra
se	se	k3xPyFc4	se
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
na	na	k7c6	na
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
u	u	k7c2	u
Lupina	lupina	k1gFnSc1	lupina
učil	učít	k5eAaPmAgMnS	učít
bránit	bránit	k5eAaImF	bránit
nebezpečným	bezpečný	k2eNgMnPc3d1	nebezpečný
tvorům	tvor	k1gMnPc3	tvor
<g/>
,	,	kIx,	,
u	u	k7c2	u
Moodyho	Moody	k1gMnSc2	Moody
kletbám	kletba	k1gFnPc3	kletba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Umbridgeové	Umbridgeová	k1gFnPc4	Umbridgeová
naopak	naopak	k6eAd1	naopak
praktické	praktický	k2eAgNnSc1d1	praktické
cvičení	cvičení	k1gNnSc1	cvičení
úplně	úplně	k6eAd1	úplně
vymizelo	vymizet	k5eAaPmAgNnS	vymizet
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
pouze	pouze	k6eAd1	pouze
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
tzv.	tzv.	kA	tzv.
Brumbálovy	brumbálův	k2eAgFnSc2d1	Brumbálova
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
studentské	studentský	k2eAgFnSc2d1	studentská
skupiny	skupina	k1gFnSc2	skupina
vedené	vedený	k2eAgInPc1d1	vedený
Harrym	Harrym	k1gInSc1	Harrym
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studenta	student	k1gMnSc4	student
učila	učít	k5eAaPmAgFnS	učít
obranná	obranný	k2eAgNnPc4d1	obranné
kouzla	kouzlo	k1gNnPc4	kouzlo
i	i	k8xC	i
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
učil	učít	k5eAaPmAgInS	učít
Harryho	Harry	k1gMnSc4	Harry
ročník	ročník	k1gInSc4	ročník
zejména	zejména	k9	zejména
užívání	užívání	k1gNnSc4	užívání
kouzel	kouzlo	k1gNnPc2	kouzlo
bez	bez	k7c2	bez
vyslovení	vyslovení	k1gNnSc2	vyslovení
kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
formule	formule	k1gFnSc2	formule
–	–	k?	–
neverbální	verbální	k2eNgInSc1d1	neverbální
zaklínadla	zaklínadlo	k1gNnSc2	zaklínadlo
a	a	k8xC	a
nutno	nutno	k6eAd1	nutno
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snapeovy	Snapeův	k2eAgFnPc1d1	Snapeova
metody	metoda	k1gFnPc1	metoda
poněkud	poněkud	k6eAd1	poněkud
pozvedly	pozvednout	k5eAaPmAgFnP	pozvednout
úroveň	úroveň	k1gFnSc4	úroveň
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
působení	působení	k1gNnSc2	působení
Amycuse	Amycuse	k1gFnSc2	Amycuse
Carrowa	Carrow	k2eAgFnSc1d1	Carrow
se	se	k3xPyFc4	se
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
změnila	změnit	k5eAaPmAgFnS	změnit
ve	v	k7c4	v
vyučování	vyučování	k1gNnSc4	vyučování
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnPc1d2	starší
studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
měli	mít	k5eAaImAgMnP	mít
trénovat	trénovat	k5eAaImF	trénovat
kletby	kletba	k1gFnPc4	kletba
na	na	k7c6	na
prvňácích	prvňák	k1gMnPc6	prvňák
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Bylinkářství	bylinkářství	k1gNnSc2	bylinkářství
===	===	k?	===
</s>
</p>
<p>
<s>
Bylinkářství	bylinkářství	k1gNnSc1	bylinkářství
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kouzelnými	kouzelný	k2eAgFnPc7d1	kouzelná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
pečováním	pečování	k1gNnSc7	pečování
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
využitím	využití	k1gNnSc7	využití
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
rostliny	rostlina	k1gFnPc1	rostlina
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
a	a	k8xC	a
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc4d1	různý
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
je	být	k5eAaImIp3nS	být
profesorka	profesorka	k1gFnSc1	profesorka
Pomona	Pomona	k1gFnSc1	Pomona
Prýtová	Prýtová	k1gFnSc1	Prýtová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
také	také	k9	také
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
generaci	generace	k1gFnSc4	generace
Harryho	Harry	k1gMnSc2	Harry
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
bylinkářství	bylinkářství	k1gNnSc6	bylinkářství
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
Neville	Nevilla	k1gFnSc3	Nevilla
Longbottom	Longbottom	k1gInSc4	Longbottom
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc4	Harry
spolužák	spolužák	k1gMnSc1	spolužák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
formule	formule	k1gFnSc2	formule
===	===	k?	===
</s>
</p>
<p>
<s>
Kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
formule	formule	k1gFnSc2	formule
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
malý	malý	k2eAgMnSc1d1	malý
profesor	profesor	k1gMnSc1	profesor
Filius	Filius	k1gMnSc1	Filius
Kratiknot	kratiknot	k1gInSc4	kratiknot
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
stoh	stoh	k1gInSc4	stoh
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgMnS	vidět
na	na	k7c4	na
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kouzelných	kouzelný	k2eAgFnPc6d1	kouzelná
formulích	formule	k1gFnPc6	formule
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
učí	učit	k5eAaImIp3nP	učit
kouzla	kouzlo	k1gNnPc4	kouzlo
jako	jako	k8xC	jako
Lumos	Lumos	k1gInSc4	Lumos
(	(	kIx(	(
<g/>
z	z	k7c2	z
hůlky	hůlka	k1gFnSc2	hůlka
vytryskne	vytrysknout	k5eAaPmIp3nS	vytrysknout
světlo	světlo	k1gNnSc1	světlo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Alohomora	Alohomora	k1gFnSc1	Alohomora
(	(	kIx(	(
<g/>
otevře	otevřít	k5eAaPmIp3nS	otevřít
zamčený	zamčený	k2eAgInSc4d1	zamčený
zámek	zámek	k1gInSc4	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
formulí	formule	k1gFnPc2	formule
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přivolávací	přivolávací	k2eAgNnSc4d1	přivolávací
kouzlo	kouzlo	k1gNnSc4	kouzlo
Accio	Accio	k6eAd1	Accio
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
accire	accir	k1gInSc5	accir
(	(	kIx(	(
<g/>
volat	volat	k5eAaImF	volat
či	či	k8xC	či
přivolávat	přivolávat	k5eAaImF	přivolávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lektvary	lektvar	k1gInPc4	lektvar
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lektvarech	lektvar	k1gInPc6	lektvar
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
učí	učit	k5eAaImIp3nP	učit
namíchat	namíchat	k5eAaBmF	namíchat
různé	různý	k2eAgInPc4d1	různý
lektvary	lektvar	k1gInPc4	lektvar
s	s	k7c7	s
kouzelnými	kouzelný	k2eAgInPc7d1	kouzelný
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
používání	používání	k1gNnSc1	používání
správných	správný	k2eAgFnPc2d1	správná
surovin	surovina	k1gFnPc2	surovina
ve	v	k7c6	v
správném	správný	k2eAgNnSc6d1	správné
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
ve	v	k7c6	v
správném	správný	k2eAgInSc6d1	správný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
lektvar	lektvar	k1gInSc4	lektvar
uvařit	uvařit	k5eAaPmF	uvařit
mudla	mudla	k6eAd1	mudla
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lektvary	lektvar	k1gInPc1	lektvar
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
jako	jako	k9	jako
mudlům	mudl	k1gMnPc3	mudl
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
přijde	přijít	k5eAaPmIp3nS	přijít
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
míchat	míchat	k5eAaImF	míchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Harryho	Harry	k1gMnSc2	Harry
studia	studio	k1gNnSc2	studio
lektvary	lektvar	k1gInPc7	lektvar
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gMnSc5	Snap
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
do	do	k7c2	do
pátého	pátý	k4xOgInSc2	pátý
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Snape	Snap	k1gInSc5	Snap
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
díle	dílo	k1gNnSc6	dílo
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
ho	on	k3xPp3gMnSc4	on
Horácio	Horácio	k1gMnSc1	Horácio
Křiklan	Křiklan	k1gMnSc1	Křiklan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lektvary	lektvar	k1gInPc4	lektvar
učil	učít	k5eAaPmAgInS	učít
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
mnohdy	mnohdy	k6eAd1	mnohdy
najdete	najít	k5eAaPmIp2nP	najít
i	i	k9	i
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
některý	některý	k3yIgInSc1	některý
lektvar	lektvar	k1gInSc1	lektvar
připravit	připravit	k5eAaPmF	připravit
–	–	k?	–
Severus	Severus	k1gInSc4	Severus
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Doušek	Doušek	k1gMnSc1	Doušek
živé	živý	k2eAgFnSc2d1	živá
smrti	smrt	k1gFnSc2	smrt
vyrobíte	vyrobit	k5eAaPmIp2nP	vyrobit
pomocí	pomocí	k7c2	pomocí
Rozdrceného	rozdrcený	k2eAgInSc2d1	rozdrcený
kořene	kořen	k1gInSc2	kořen
Asfodelu	asfodel	k1gInSc2	asfodel
a	a	k8xC	a
výluhu	výluh	k1gInSc2	výluh
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dějiny	dějiny	k1gFnPc1	dějiny
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgInSc7d1	jediný
předmětem	předmět	k1gInSc7	předmět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
duch	duch	k1gMnSc1	duch
–	–	k?	–
profesor	profesor	k1gMnSc1	profesor
Cuthbert	Cuthbert	k1gMnSc1	Cuthbert
Binns	Binns	k1gInSc4	Binns
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
prý	prý	k9	prý
"	"	kIx"	"
<g/>
kdysi	kdysi	k6eAd1	kdysi
usnul	usnout	k5eAaPmAgMnS	usnout
před	před	k7c7	před
krbem	krb	k1gInSc7	krb
ve	v	k7c6	v
sborovně	sborovně	k6eAd1	sborovně
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
tam	tam	k6eAd1	tam
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluví	mluvit	k5eAaImIp3nS	mluvit
monotónním	monotónní	k2eAgInSc7d1	monotónní
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejnudnějšího	nudný	k2eAgMnSc4d3	nejnudnější
učitele	učitel	k1gMnSc4	učitel
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
probíraná	probíraný	k2eAgNnPc4d1	probírané
témata	téma	k1gNnPc4	téma
předmětu	předmět	k1gInSc2	předmět
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
vzpoury	vzpoura	k1gFnSc2	vzpoura
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
s	s	k7c7	s
obry	obr	k1gMnPc7	obr
nebo	nebo	k8xC	nebo
původ	původ	k1gMnSc1	původ
čarodějnického	čarodějnický	k2eAgNnSc2d1	čarodějnické
utajení	utajení	k1gNnSc2	utajení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Astronomie	astronomie	k1gFnSc1	astronomie
===	===	k?	===
</s>
</p>
<p>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
předmětem	předmět	k1gInSc7	předmět
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgInSc2	svůj
přímého	přímý	k2eAgInSc2d1	přímý
protějška	protějšek	k1gInSc2	protějšek
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předmět	předmět	k1gInSc1	předmět
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
profesorka	profesorka	k1gFnSc1	profesorka
Sinistrová	Sinistrový	k2eAgFnSc1d1	Sinistrový
na	na	k7c6	na
astronomické	astronomický	k2eAgFnSc6d1	astronomická
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
věži	věž	k1gFnSc6	věž
Bradavického	Bradavický	k2eAgInSc2d1	Bradavický
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
předmětu	předmět	k1gInSc2	předmět
mohou	moct	k5eAaImIp3nP	moct
studenti	student	k1gMnPc1	student
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgFnPc4	svůj
koleje	kolej	k1gFnPc4	kolej
po	po	k7c6	po
večerce	večerka	k1gFnSc6	večerka
<g/>
.	.	kIx.	.
</s>
<s>
Žáci	Žák	k1gMnPc1	Žák
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
učí	učit	k5eAaImIp3nP	učit
poznávat	poznávat	k5eAaImF	poznávat
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
i	i	k8xC	i
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Létání	létání	k1gNnPc4	létání
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
===	===	k?	===
</s>
</p>
<p>
<s>
Létání	létání	k1gNnSc1	létání
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
a	a	k8xC	a
žáci	žák	k1gMnPc1	žák
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
madam	madam	k1gFnSc2	madam
Hoochové	Hoochová	k1gFnSc2	Hoochová
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nP	učit
létat	létat	k5eAaImF	létat
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
ročníku	ročník	k1gInSc6	ročník
je	být	k5eAaImIp3nS	být
studentům	student	k1gMnPc3	student
školy	škola	k1gFnSc2	škola
umožněno	umožnit	k5eAaPmNgNnS	umožnit
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
kolejního	kolejní	k2eAgInSc2d1	kolejní
famfrpálového	famfrpálový	k2eAgInSc2d1	famfrpálový
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Žáci	Žák	k1gMnPc1	Žák
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
učí	učit	k5eAaImIp3nS	učit
létání	létání	k1gNnSc4	létání
absolutně	absolutně	k6eAd1	absolutně
od	od	k7c2	od
základů	základ	k1gInPc2	základ
–	–	k?	–
od	od	k7c2	od
přejmutí	přejmutí	k1gNnSc2	přejmutí
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
koštětem	koště	k1gNnSc7	koště
přes	přes	k7c4	přes
vzlétnutí	vzlétnutí	k1gNnSc4	vzlétnutí
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
až	až	k9	až
po	po	k7c6	po
zatáčení	zatáčení	k1gNnSc6	zatáčení
<g/>
,	,	kIx,	,
zrychlování	zrychlování	k1gNnSc1	zrychlování
a	a	k8xC	a
brždění	bržděný	k2eAgMnPc1d1	bržděný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Péče	péče	k1gFnSc1	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c4	v
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
učí	učit	k5eAaImIp3nP	učit
starat	starat	k5eAaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
různá	různý	k2eAgNnPc4d1	různé
magická	magický	k2eAgNnPc4d1	magické
stvoření	stvoření	k1gNnPc4	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
mimo	mimo	k7c4	mimo
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Harryho	Harry	k1gMnSc2	Harry
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ale	ale	k9	ale
Harry	Harra	k1gFnSc2	Harra
tento	tento	k3xDgInSc1	tento
předmět	předmět	k1gInSc1	předmět
nestuduje	studovat	k5eNaImIp3nS	studovat
<g/>
,	,	kIx,	,
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
předmět	předmět	k1gInSc1	předmět
profesor	profesor	k1gMnSc1	profesor
Silvanus	Silvanus	k1gMnSc1	Silvanus
Kettleburn	Kettleburn	k1gMnSc1	Kettleburn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
odejde	odejít	k5eAaPmIp3nS	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
věnovat	věnovat	k5eAaPmF	věnovat
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
zbývající	zbývající	k2eAgFnPc4d1	zbývající
končetiny	končetina	k1gFnPc4	končetina
<g/>
"	"	kIx"	"
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
vyučování	vyučování	k1gNnSc1	vyučování
ujme	ujmout	k5eAaPmIp3nS	ujmout
Rubeus	Rubeus	k1gInSc4	Rubeus
Hagrid	Hagrida	k1gFnPc2	Hagrida
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
odvolá	odvolat	k5eAaPmIp3nS	odvolat
Umbridgeová	Umbridgeový	k2eAgFnSc1d1	Umbridgeová
<g/>
,	,	kIx,	,
bradavická	bradavický	k2eAgFnSc1d1	bradavická
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vyšetřovatelka	vyšetřovatelka	k1gFnSc1	vyšetřovatelka
<g/>
,	,	kIx,	,
zastupován	zastupován	k2eAgInSc4d1	zastupován
profesorkou	profesorka	k1gFnSc7	profesorka
Červotočkovou	Červotočkův	k2eAgFnSc7d1	Červotočkův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
jasnovidectví	jasnovidectví	k1gNnSc6	jasnovidectví
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
učí	učit	k5eAaImIp3nP	učit
předpovídat	předpovídat	k5eAaImF	předpovídat
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
věštěním	věštění	k1gNnSc7	věštění
z	z	k7c2	z
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
čajových	čajový	k2eAgInPc2d1	čajový
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
z	z	k7c2	z
kříšťálové	kříšťálový	k2eAgFnSc2d1	kříšťálová
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Učí	učit	k5eAaImIp3nS	učit
jej	on	k3xPp3gMnSc4	on
profesorka	profesorka	k1gFnSc1	profesorka
Sibylla	Sibylla	k1gFnSc1	Sibylla
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studenty	student	k1gMnPc4	student
neustále	neustále	k6eAd1	neustále
děsí	děsit	k5eAaImIp3nP	děsit
zvěstmi	zvěst	k1gFnPc7	zvěst
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Profesorka	profesorka	k1gFnSc1	profesorka
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jasnovidectví	jasnovidectví	k1gNnSc1	jasnovidectví
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejnepřesnějších	přesný	k2eNgNnPc2d3	přesný
odvětví	odvětví	k1gNnPc2	odvětví
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
zastánci	zastánce	k1gMnPc1	zastánce
jasnovidectví	jasnovidectví	k1gNnSc1	jasnovidectví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
profesorky	profesorka	k1gFnSc2	profesorka
Trelawneyové	Trelawneyová	k1gFnSc2	Trelawneyová
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
dar	dar	k1gInSc1	dar
–	–	k?	–
tzv.	tzv.	kA	tzv.
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
profesorka	profesorka	k1gFnSc1	profesorka
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
odvolána	odvolat	k5eAaPmNgFnS	odvolat
vrchní	vrchní	k2eAgFnSc7d1	vrchní
vyšetřovatelkou	vyšetřovatelka	k1gFnSc7	vyšetřovatelka
Umbridgeovou	Umbridgeův	k2eAgFnSc7d1	Umbridgeův
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
předmět	předmět	k1gInSc1	předmět
kentaur	kentaur	k1gMnSc1	kentaur
Firenze	Firenze	k1gFnSc1	Firenze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Umbridgeové	Umbridgeová	k1gFnSc2	Umbridgeová
se	se	k3xPyFc4	se
Firenze	Firenze	k1gFnSc1	Firenze
a	a	k8xC	a
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
o	o	k7c4	o
předmět	předmět	k1gInSc4	předmět
dělí	dělit	k5eAaImIp3nP	dělit
<g/>
.	.	kIx.	.
</s>
<s>
Učebna	učebna	k1gFnSc1	učebna
jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
žebříkem	žebřík	k1gInSc7	žebřík
<g/>
.	.	kIx.	.
</s>
<s>
Kentaurova	kentaurův	k2eAgFnSc1d1	kentaurův
učebna	učebna	k1gFnSc1	učebna
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
mýtina	mýtina	k1gFnSc1	mýtina
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
spodních	spodní	k2eAgNnPc6d1	spodní
patrech	patro	k1gNnPc6	patro
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věštění	věštění	k1gNnSc1	věštění
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Věštění	věštěný	k2eAgMnPc1d1	věštěný
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
numerologie	numerologie	k1gFnSc1	numerologie
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
magickými	magický	k2eAgFnPc7d1	magická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harra	k1gFnPc1	Harra
tento	tento	k3xDgInSc4	tento
předmět	předmět	k1gInSc4	předmět
nestuduje	studovat	k5eNaImIp3nS	studovat
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
předmět	předmět	k1gInSc1	předmět
Hermiony	Hermion	k1gInPc1	Hermion
<g/>
.	.	kIx.	.
</s>
<s>
Vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
ho	on	k3xPp3gMnSc4	on
profesorka	profesorka	k1gFnSc1	profesorka
Vectorová	Vectorový	k2eAgFnSc1d1	Vectorová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studia	studio	k1gNnSc2	studio
mudlů	mudl	k1gInPc2	mudl
===	===	k?	===
</s>
</p>
<p>
<s>
Studia	studio	k1gNnSc2	studio
mudlů	mudl	k1gMnPc2	mudl
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
studiem	studio	k1gNnSc7	studio
nekouzelnického	kouzelnický	k2eNgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
čarodějové	čaroděj	k1gMnPc1	čaroděj
dokázali	dokázat	k5eAaPmAgMnP	dokázat
krýt	krýt	k5eAaImF	krýt
v	v	k7c6	v
mudlovském	mudlovský	k2eAgInSc6d1	mudlovský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
předmět	předmět	k1gInSc4	předmět
učil	učit	k5eAaImAgMnS	učit
Quirinus	Quirinus	k1gMnSc1	Quirinus
Quirrell	Quirrell	k1gMnSc1	Quirrell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ovšem	ovšem	k9	ovšem
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
učitele	učitel	k1gMnSc2	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
Charita	charita	k1gFnSc1	charita
Burbageová	Burbageová	k1gFnSc1	Burbageová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemře	zemřít	k5eAaPmIp3nS	zemřít
rukou	ruka	k1gFnSc7	ruka
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
v	v	k7c6	v
první	první	k4xOgFnSc6	první
kapitole	kapitola	k1gFnSc6	kapitola
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
předmětu	předmět	k1gInSc6	předmět
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
mudly	mudl	k1gMnPc4	mudl
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
školy	škola	k1gFnSc2	škola
smrtijedy	smrtijeda	k1gMnSc2	smrtijeda
se	se	k3xPyFc4	se
vyučování	vyučování	k1gNnSc2	vyučování
ujala	ujmout	k5eAaPmAgFnS	ujmout
Alekto	Alekto	k1gNnSc4	Alekto
Carrowová	Carrowová	k1gFnSc1	Carrowová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žáky	žák	k1gMnPc4	žák
učila	učít	k5eAaPmAgFnS	učít
o	o	k7c6	o
zkaženosti	zkaženost	k1gFnSc6	zkaženost
mudlů	mudl	k1gInPc2	mudl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starodávné	starodávný	k2eAgInPc1d1	starodávný
runy	run	k1gInPc1	run
===	===	k?	===
</s>
</p>
<p>
<s>
Starodávné	starodávný	k2eAgFnPc1d1	starodávná
runy	runa	k1gFnPc1	runa
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
teoretický	teoretický	k2eAgInSc1d1	teoretický
předmět	předmět	k1gInSc1	předmět
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
luštěním	luštění	k1gNnSc7	luštění
starodávných	starodávný	k2eAgFnPc2d1	starodávná
run	runa	k1gFnPc2	runa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
postav	postava	k1gFnPc2	postava
série	série	k1gFnSc2	série
jej	on	k3xPp3gMnSc4	on
studuje	studovat	k5eAaImIp3nS	studovat
pouze	pouze	k6eAd1	pouze
Hermiona	Hermiona	k1gFnSc1	Hermiona
<g/>
.	.	kIx.	.
</s>
<s>
Učí	učit	k5eAaImIp3nS	učit
ho	on	k3xPp3gMnSc4	on
profesorka	profesorka	k1gFnSc1	profesorka
Bathsheda	Bathsheda	k1gMnSc1	Bathsheda
Babbling	Babbling	k1gInSc1	Babbling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
===	===	k?	===
</s>
</p>
<p>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
je	být	k5eAaImIp3nS	být
místnost	místnost	k1gFnSc4	místnost
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
pod	pod	k7c7	pod
bradavickým	bradavický	k2eAgInSc7d1	bradavický
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děj	děj	k1gInSc4	děj
knihy	kniha	k1gFnSc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
komnatu	komnata	k1gFnSc4	komnata
postavil	postavit	k5eAaPmAgMnS	postavit
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Bradavic	bradavice	k1gFnPc2	bradavice
Salazar	Salazar	k1gMnSc1	Salazar
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
neshodl	shodnout	k5eNaPmAgInS	shodnout
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
zakladateli	zakladatel	k1gMnPc7	zakladatel
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
přijímání	přijímání	k1gNnSc2	přijímání
studentů	student	k1gMnPc2	student
mudlovského	mudlovský	k2eAgInSc2d1	mudlovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odchodem	odchod	k1gInSc7	odchod
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
v	v	k7c6	v
komnatě	komnata	k1gFnSc6	komnata
baziliška	bazilišek	k1gMnSc2	bazilišek
–	–	k?	–
obřího	obří	k2eAgMnSc2d1	obří
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Zmijozelova	Zmijozelův	k2eAgMnSc2d1	Zmijozelův
dědice	dědic	k1gMnSc2	dědic
pomoci	pomoc	k1gFnSc2	pomoc
vyčistit	vyčistit	k5eAaPmF	vyčistit
školu	škola	k1gFnSc4	škola
od	od	k7c2	od
mudlovských	mudlovský	k2eAgMnPc2d1	mudlovský
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Komnata	komnata	k1gFnSc1	komnata
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Bradavic	bradavice	k1gFnPc2	bradavice
otevřela	otevřít	k5eAaPmAgFnS	otevřít
třikrát	třikrát	k6eAd1	třikrát
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Tom	Tom	k1gMnSc1	Tom
Raddle	Raddle	k1gMnSc1	Raddle
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
komnatu	komnata	k1gFnSc4	komnata
otevře	otevřít	k5eAaPmIp3nS	otevřít
Ginny	Ginn	k1gMnPc4	Ginn
Weasleyová	Weasleyová	k1gFnSc1	Weasleyová
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Voldemortových	Voldemortův	k2eAgMnPc2d1	Voldemortův
viteálů	viteál	k1gMnPc2	viteál
–	–	k?	–
Raddleova	Raddleův	k2eAgInSc2d1	Raddleův
deníku	deník	k1gInSc2	deník
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
ji	on	k3xPp3gFnSc4	on
otevřel	otevřít	k5eAaPmAgInS	otevřít
Ronald	Ronald	k1gInSc1	Ronald
Weasley	Weaslea	k1gFnSc2	Weaslea
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
Grangerovou	Grangerův	k2eAgFnSc7d1	Grangerova
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
šli	jít	k5eAaImAgMnP	jít
pro	pro	k7c4	pro
zub	zub	k1gInSc4	zub
Baziliška	bazilišek	k1gMnSc2	bazilišek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
zničit	zničit	k5eAaPmF	zničit
další	další	k2eAgFnPc4d1	další
z	z	k7c2	z
Voldemortových	Voldemortův	k2eAgInPc2d1	Voldemortův
viteálů	viteál	k1gInPc2	viteál
-	-	kIx~	-
Pohár	pohár	k1gInSc1	pohár
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
otevření	otevření	k1gNnSc6	otevření
komnaty	komnata	k1gFnSc2	komnata
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Ufňukaná	ufňukaný	k2eAgFnSc1d1	ufňukaná
Uršula	Uršula	k1gFnSc1	Uršula
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
umývárně	umývárna	k1gFnSc6	umývárna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
Tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Uršulin	Uršulin	k2eAgMnSc1d1	Uršulin
duch	duch	k1gMnSc1	duch
tuto	tento	k3xDgFnSc4	tento
umývárnu	umývárna	k1gFnSc4	umývárna
pak	pak	k6eAd1	pak
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
obývá	obývat	k5eAaImIp3nS	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
pomocí	pomocí	k7c2	pomocí
hadího	hadí	k2eAgInSc2d1	hadí
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
dostal	dostat	k5eAaPmAgMnS	dostat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
Harry	Harra	k1gFnSc2	Harra
<g/>
,	,	kIx,	,
když	když	k8xS	když
šel	jít	k5eAaImAgMnS	jít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
Lockhartem	Lockhart	k1gInSc7	Lockhart
osvobodit	osvobodit	k5eAaPmF	osvobodit
Ginny	Ginn	k1gInPc4	Ginn
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vstoupit	vstoupit	k5eAaPmF	vstoupit
i	i	k9	i
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
když	když	k8xS	když
pouze	pouze	k6eAd1	pouze
napodobil	napodobit	k5eAaPmAgMnS	napodobit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dříve	dříve	k6eAd2	dříve
řekl	říct	k5eAaPmAgMnS	říct
Harry	Harr	k1gInPc7	Harr
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
se	se	k3xPyFc4	se
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
vydal	vydat	k5eAaPmAgMnS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
baziliškovy	baziliškův	k2eAgInPc4d1	baziliškův
zuby	zub	k1gInPc4	zub
–	–	k?	–
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
viteálů	viteál	k1gInPc2	viteál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komnata	komnata	k1gFnSc1	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
===	===	k?	===
</s>
</p>
<p>
<s>
Komnata	komnata	k1gFnSc1	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
skrytá	skrytý	k2eAgFnSc1d1	skrytá
místnost	místnost	k1gFnSc1	místnost
v	v	k7c6	v
bradavickém	bradavický	k2eAgMnSc6d1	bradavický
sedmém	sedmý	k4xOgNnSc6	sedmý
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
někdo	někdo	k3yInSc1	někdo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
při	při	k7c6	při
vánočním	vánoční	k2eAgInSc6d1	vánoční
plese	ples	k1gInSc6	ples
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jednou	jeden	k4xCgFnSc7	jeden
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
záchod	záchod	k1gInSc4	záchod
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
s	s	k7c7	s
výstavou	výstava	k1gFnSc7	výstava
nočníků	nočník	k1gInPc2	nočník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
lépe	dobře	k6eAd2	dobře
prohlédnul	prohlédnout	k5eAaPmAgInS	prohlédnout
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
ji	on	k3xPp3gFnSc4	on
nemohl	moct	k5eNaImAgMnS	moct
nalézt	nalézt	k5eAaBmF	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	dílo	k1gNnSc6	dílo
ji	on	k3xPp3gFnSc4	on
využijí	využít	k5eAaPmIp3nP	využít
Harry	Harr	k1gInPc1	Harr
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
cvičebnu	cvičebna	k1gFnSc4	cvičebna
pro	pro	k7c4	pro
Brumbálovu	brumbálův	k2eAgFnSc4d1	Brumbálova
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
ji	on	k3xPp3gFnSc4	on
Dobby	Dobb	k1gMnPc4	Dobb
používal	používat	k5eAaImAgInS	používat
<g/>
,	,	kIx,	,
když	když	k8xS	když
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
zotavit	zotavit	k5eAaPmF	zotavit
opilou	opilý	k2eAgFnSc4d1	opilá
Winky	Winka	k1gFnPc4	Winka
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
a	a	k8xC	a
George	George	k1gFnSc1	George
Weasleyovi	Weasleyův	k2eAgMnPc1d1	Weasleyův
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
zase	zase	k9	zase
dostali	dostat	k5eAaPmAgMnP	dostat
<g/>
,	,	kIx,	,
když	když	k8xS	když
utíkali	utíkat	k5eAaImAgMnP	utíkat
před	před	k7c7	před
Filchem	Filch	k1gInSc7	Filch
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zjevila	zjevit	k5eAaPmAgFnS	zjevit
jako	jako	k9	jako
přístěnek	přístěnek	k1gInSc4	přístěnek
na	na	k7c4	na
košťata	koště	k1gNnPc4	koště
<g/>
.	.	kIx.	.
</s>
<s>
Profesorka	profesorka	k1gFnSc1	profesorka
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
tam	tam	k6eAd1	tam
schovávala	schovávat	k5eAaImAgFnS	schovávat
prázdné	prázdný	k2eAgFnPc4d1	prázdná
lahve	lahev	k1gFnPc4	lahev
od	od	k7c2	od
sherry	sherry	k1gNnSc2	sherry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
díle	dílo	k1gNnSc6	dílo
ji	on	k3xPp3gFnSc4	on
Harry	Harro	k1gNnPc7	Harro
využije	využít	k5eAaPmIp3nS	využít
k	k	k7c3	k
úkrytu	úkryt	k1gInSc3	úkryt
své	své	k1gNnSc4	své
učebnice	učebnice	k1gFnPc4	učebnice
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kdysi	kdysi	k6eAd1	kdysi
patřila	patřit	k5eAaImAgFnS	patřit
princi	princa	k1gFnSc2	princa
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uvedeno	uvést	k5eAaPmNgNnS	uvést
kouzlo	kouzlo	k1gNnSc4	kouzlo
sectumsempra	sectumsempro	k1gNnSc2	sectumsempro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
použil	použít	k5eAaPmAgMnS	použít
proti	proti	k7c3	proti
Dracovi	Draec	k1gMnSc3	Draec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Komnatě	komnata	k1gFnSc6	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
ukryta	ukrýt	k5eAaPmNgFnS	ukrýt
také	také	k9	také
rozplývavá	rozplývavý	k2eAgFnSc1d1	rozplývavá
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
opravuje	opravovat	k5eAaImIp3nS	opravovat
Draco	Draco	k1gMnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pomůže	pomoct	k5eAaPmIp3nS	pomoct
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
smrtijedům	smrtijed	k1gMnPc3	smrtijed
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pak	pak	k6eAd1	pak
zapříčiní	zapříčinit	k5eAaPmIp3nP	zapříčinit
smrt	smrt	k1gFnSc4	smrt
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
poslouží	posloužit	k5eAaPmIp3nS	posloužit
Komnata	komnata	k1gFnSc1	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
jako	jako	k8xC	jako
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
vzpírající	vzpírající	k2eAgMnPc4d1	vzpírající
se	se	k3xPyFc4	se
praktikám	praktika	k1gFnPc3	praktika
smrtijedů	smrtijed	k1gMnPc2	smrtijed
Carrowových	Carrowová	k1gFnPc2	Carrowová
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
učí	učit	k5eAaImIp3nP	učit
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
smrtijedů	smrtijed	k1gMnPc2	smrtijed
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
černou	černý	k2eAgFnSc4d1	černá
magii	magie	k1gFnSc4	magie
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
)	)	kIx)	)
a	a	k8xC	a
studia	studio	k1gNnPc1	studio
mudlů	mudl	k1gMnPc2	mudl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k6eAd1	také
nalezen	naleznout	k5eAaPmNgInS	naleznout
havraspárský	havraspárský	k2eAgInSc1d1	havraspárský
diadém	diadém	k1gInSc1	diadém
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
Voldemortových	Voldemortův	k2eAgInPc2d1	Voldemortův
viteálů	viteál	k1gInPc2	viteál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
síň	síň	k1gFnSc1	síň
===	===	k?	===
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
síň	síň	k1gFnSc1	síň
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
místnost	místnost	k1gFnSc1	místnost
s	s	k7c7	s
kouzelným	kouzelný	k2eAgInSc7d1	kouzelný
stropem	strop	k1gInSc7	strop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
venkovní	venkovní	k2eAgNnSc4d1	venkovní
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc4	čtyři
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
stoly	stol	k1gInPc4	stol
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
koleje	kolej	k1gFnPc4	kolej
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
<g/>
,	,	kIx,	,
Mrzimor	Mrzimor	k1gMnSc1	Mrzimor
<g/>
,	,	kIx,	,
Havraspár	Havraspár	k1gMnSc1	Havraspár
<g/>
,	,	kIx,	,
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
stůl	stůl	k1gInSc4	stůl
pro	pro	k7c4	pro
profesory	profesor	k1gMnPc4	profesor
<g/>
,	,	kIx,	,
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
školy	škola	k1gFnSc2	škola
či	či	k8xC	či
příležitostné	příležitostný	k2eAgMnPc4d1	příležitostný
hosty	host	k1gMnPc4	host
<g/>
.	.	kIx.	.
</s>
<s>
Síň	síň	k1gFnSc1	síň
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
k	k	k7c3	k
hostinám	hostina	k1gFnPc3	hostina
<g/>
,	,	kIx,	,
slavnostem	slavnost	k1gFnPc3	slavnost
<g/>
,	,	kIx,	,
skládání	skládání	k1gNnSc1	skládání
zkoušek	zkouška	k1gFnPc2	zkouška
NKÚ	NKÚ	kA	NKÚ
a	a	k8xC	a
OVCE	ovce	k1gFnSc2	ovce
a	a	k8xC	a
pro	pro	k7c4	pro
hodiny	hodina	k1gFnPc4	hodina
přemisťování	přemisťování	k1gNnSc2	přemisťování
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
ani	ani	k8xC	ani
školních	školní	k2eAgInPc2d1	školní
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nelze	lze	k6eNd1	lze
přemístit	přemístit	k5eAaPmF	přemístit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
však	však	k9	však
v	v	k7c4	v
soboty	sobota	k1gFnPc4	sobota
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
kurzů	kurz	k1gInPc2	kurz
přemisťování	přemisťování	k1gNnSc1	přemisťování
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
síň	síň	k1gFnSc4	síň
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
byl	být	k5eAaImAgInS	být
Harry	Harra	k1gFnSc2	Harra
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
díle	dílo	k1gNnSc6	dílo
byl	být	k5eAaImAgInS	být
organizován	organizován	k2eAgInSc1d1	organizován
soubojnický	soubojnický	k2eAgInSc1d1	soubojnický
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	dílo	k1gNnSc6	dílo
zde	zde	k6eAd1	zde
spali	spát	k5eAaImAgMnP	spát
bradavičtí	bradavický	k2eAgMnPc1d1	bradavický
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
roznesla	roznést	k5eAaPmAgFnS	roznést
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
dostal	dostat	k5eAaPmAgMnS	dostat
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
díle	dílo	k1gNnSc6	dílo
zahájen	zahájen	k2eAgInSc4d1	zahájen
turnaj	turnaj	k1gInSc4	turnaj
Tří	tři	k4xCgMnPc2	tři
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
,	,	kIx,	,
ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
tří	tři	k4xCgMnPc2	tři
šampionů	šampion	k1gMnPc2	šampion
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgInSc1d1	vánoční
ples	ples	k1gInSc1	ples
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
díle	dílo	k1gNnSc6	dílo
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
doneseni	donesen	k2eAgMnPc1d1	donesen
všichni	všechen	k3xTgMnPc1	všechen
mrtví	mrtvý	k1gMnPc1	mrtvý
po	po	k7c6	po
Bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
poletuje	poletovat	k5eAaImIp3nS	poletovat
mnoho	mnoho	k4c1	mnoho
duchů	duch	k1gMnPc2	duch
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kolejní	kolejní	k2eAgFnSc1d1	kolejní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Školní	školní	k2eAgInSc1d1	školní
rok	rok	k1gInSc1	rok
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
většina	většina	k1gFnSc1	většina
studentů	student	k1gMnPc2	student
jezdí	jezdit	k5eAaImIp3nS	jezdit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školy	škola	k1gFnSc2	škola
jsou	být	k5eAaImIp3nP	být
dopravováni	dopravován	k2eAgMnPc1d1	dopravován
Bradavickým	Bradavický	k2eAgInSc7d1	Bradavický
expresem	expres	k1gInSc7	expres
z	z	k7c2	z
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
nádraží	nádraží	k1gNnSc2	nádraží
King	King	k1gInSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
z	z	k7c2	z
nástupiště	nástupiště	k1gNnSc2	nástupiště
devět	devět	k4xCc4	devět
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
přijede	přijet	k5eAaPmIp3nS	přijet
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
v	v	k7c6	v
Prasinkách	Prasinka	k1gFnPc6	Prasinka
a	a	k8xC	a
odtamtud	odtamtud	k6eAd1	odtamtud
jsou	být	k5eAaImIp3nP	být
studenti	student	k1gMnPc1	student
dopraveni	dopravit	k5eAaPmNgMnP	dopravit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
buď	buď	k8xC	buď
lodičkami	lodička	k1gFnPc7	lodička
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
s	s	k7c7	s
Hagridem	Hagrid	k1gInSc7	Hagrid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kočáry	kočár	k1gInPc7	kočár
taženými	tažený	k2eAgInPc7d1	tažený
testrály	testrál	k1gInPc7	testrál
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
hradě	hrad	k1gInSc6	hrad
plném	plný	k2eAgInSc6d1	plný
tajných	tajný	k2eAgFnPc2d1	tajná
chodeb	chodba	k1gFnPc2	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kolej	kolej	k1gFnSc1	kolej
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
společenskou	společenský	k2eAgFnSc4d1	společenská
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
<g/>
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
a	a	k8xC	a
Havraspár	Havraspár	k1gMnSc1	Havraspár
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
věžích	věž	k1gFnPc6	věž
<g/>
,	,	kIx,	,
<g/>
Mrzimor	Mrzimor	k1gInSc1	Mrzimor
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
(	(	kIx(	(
<g/>
místnost	místnost	k1gFnSc1	místnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
blízko	blízko	k7c2	blízko
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zmijozel	Zmijozel	k1gFnSc1	Zmijozel
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
<g/>
.	.	kIx.	.
<g/>
Studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
scházejí	scházet	k5eAaImIp3nP	scházet
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
k	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
<g/>
,	,	kIx,	,
obědu	oběd	k1gInSc3	oběd
a	a	k8xC	a
večeři	večeře	k1gFnSc3	večeře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
místnosti	místnost	k1gFnSc6	místnost
se	se	k3xPyFc4	se
také	také	k9	také
konají	konat	k5eAaImIp3nP	konat
zahajovací	zahajovací	k2eAgInPc1d1	zahajovací
a	a	k8xC	a
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
ceremoniály	ceremoniál	k1gInPc1	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
školní	školní	k2eAgInPc1d1	školní
pozemky	pozemek	k1gInPc1	pozemek
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
Zapovězený	zapovězený	k2eAgInSc4d1	zapovězený
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgNnSc4d1	Černé
jezero	jezero	k1gNnSc4	jezero
a	a	k8xC	a
Famfrpálové	Famfrpálový	k2eAgNnSc4d1	Famfrpálové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Bradavické	Bradavický	k2eAgFnSc2d1	Bradavická
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
ředitel	ředitel	k1gMnSc1	ředitel
dosazený	dosazený	k2eAgInSc1d1	dosazený
správní	správní	k2eAgFnSc7d1	správní
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kolej	kolej	k1gFnSc1	kolej
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
také	také	k9	také
zástupcem	zástupce	k1gMnSc7	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
známá	známý	k2eAgFnSc1d1	známá
sestava	sestava	k1gFnSc1	sestava
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hogwarts	Hogwartsa	k1gFnPc2	Hogwartsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
995	[number]	k4	995
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
898	[number]	k4	898
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
951	[number]	k4	951
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
994	[number]	k4	994
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1294	[number]	k4	1294
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1819	[number]	k4	1819
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROWLINGOVÁ	ROWLINGOVÁ	kA	ROWLINGOVÁ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2122	[number]	k4	2122
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Škola	škola	k1gFnSc1	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bradavice	bradavice	k1gFnSc1	bradavice
na	na	k7c4	na
Harry	Harra	k1gFnPc4	Harra
Potter	Potter	k1gInSc4	Potter
Wiki	Wiki	k1gNnSc1	Wiki
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bradavice	bradavice	k1gFnSc1	bradavice
na	na	k7c6	na
lexikonu	lexikon	k1gInSc6	lexikon
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
</s>
</p>
