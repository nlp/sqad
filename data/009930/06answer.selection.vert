<s>
Vápník	vápník	k1gInSc1	vápník
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Calcium	Calcium	k1gNnSc1	Calcium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
