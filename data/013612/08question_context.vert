<s>
Karakul	Karakul	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
,	,	kIx,
z	z	k7c2
turkického	turkický	k2eAgInSc2d1
černé	černý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezodtoké	bezodtoký	k2eAgNnSc1d1
meteoritické	meteoritický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Pamíru	Pamír	k1gInSc2
Horním	horní	k2eAgInSc6d1
Badachšánu	Badachšán	k1gInSc6
v	v	k7c6
Tádžikistánu	Tádžikistán	k1gInSc6
<g/>
.	.	kIx.
</s>