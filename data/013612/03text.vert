<s>
Karakul	Karakul	k1gInSc1
</s>
<s>
Karakul	Karakul	k1gInSc1
Satelitní	satelitní	k2eAgInSc4d1
snímek	snímek	k1gInSc4
jezeraPoloha	jezeraPoloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
Tádžikistán	Tádžikistán	k1gInSc1
Provincie	provincie	k1gFnSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
Badachšán	Badachšán	k1gInSc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
73	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
380	#num#	k4
km²	km²	k?
Délka	délka	k1gFnSc1
</s>
<s>
33	#num#	k4
km	km	kA
Max	max	kA
<g/>
.	.	kIx.
hloubka	hloubka	k1gFnSc1
</s>
<s>
236	#num#	k4
m	m	kA
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
meteoritické	meteoritický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
3914	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Karadžilga	Karadžilga	k1gFnSc1
<g/>
,	,	kIx,
Karaart	Karaart	k1gInSc1
<g/>
,	,	kIx,
Muzkol	Muzkol	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karakul	Karakul	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
,	,	kIx,
z	z	k7c2
turkického	turkický	k2eAgInSc2d1
černé	černý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezodtoké	bezodtoký	k2eAgNnSc1d1
meteoritické	meteoritický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Pamíru	Pamír	k1gInSc2
Horním	horní	k2eAgInSc6d1
Badachšánu	Badachšán	k1gInSc6
v	v	k7c6
Tádžikistánu	Tádžikistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
380	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
33	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
částečně	částečně	k6eAd1
naplňuje	naplňovat	k5eAaImIp3nS
impaktní	impaktní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
o	o	k7c6
průměru	průměr	k1gInSc6
52	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
je	být	k5eAaImIp3nS
hluboké	hluboký	k2eAgNnSc1d1
maximálně	maximálně	k6eAd1
236	#num#	k4
m	m	kA
a	a	k8xC
ve	v	k7c4
východní	východní	k2eAgFnSc4d1
22,5	22,5	k4
m.	m.	kA
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
3914	#num#	k4
m.	m.	kA
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS
asi	asi	k9
před	před	k7c7
5	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
obklopeno	obklopit	k5eAaPmNgNnS
vysokými	vysoký	k2eAgFnPc7d1
horami	hora	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
spojené	spojený	k2eAgFnPc4d1
úzkými	úzký	k2eAgInPc7d1
průlivy	průliv	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Největší	veliký	k2eAgInPc1d3
přítoky	přítok	k1gInPc1
jsou	být	k5eAaImIp3nP
Karadžilga	Karadžilg	k1gMnSc4
<g/>
,	,	kIx,
Karaart	Karaart	k1gInSc4
a	a	k8xC
Muzkol	Muzkol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
vody	voda	k1gFnSc2
</s>
<s>
Voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
slaná	slaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průzračnost	průzračnost	k1gFnSc1
je	být	k5eAaImIp3nS
až	až	k9
9	#num#	k4
m.	m.	k?
Jezero	jezero	k1gNnSc1
je	být	k5eAaImIp3nS
pokryté	pokrytý	k2eAgNnSc1d1
ledem	led	k1gInSc7
od	od	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
do	do	k7c2
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplota	teplota	k1gFnSc1
vody	voda	k1gFnSc2
v	v	k7c6
létě	léto	k1gNnSc6
je	být	k5eAaImIp3nS
12	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
К	К	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karakul	Karakul	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
