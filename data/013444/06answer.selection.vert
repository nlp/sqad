<s>
Issyk-kul	Issykul	k1gInSc1	Issyk-kul
(	(	kIx(	(
<g/>
kyrgyzsky	kyrgyzsky	k6eAd1	kyrgyzsky
Ы	Ы	k?	Ы
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
И	И	k?	И
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
teplé	teplý	k2eAgNnSc1d1	teplé
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezodtoké	bezodtoký	k2eAgNnSc1d1	bezodtoké
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ťan-šanu	Ťan-šan	k1gInSc2	Ťan-šan
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
oblasti	oblast	k1gFnSc6	oblast
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
<g/>
.	.	kIx.	.
</s>
