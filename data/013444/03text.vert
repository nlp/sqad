<p>
<s>
Issyk-kul	Issykul	k1gInSc1	Issyk-kul
(	(	kIx(	(
<g/>
kyrgyzsky	kyrgyzsky	k6eAd1	kyrgyzsky
Ы	Ы	k?	Ы
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
И	И	k?	И
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
teplé	teplý	k2eAgNnSc1d1	teplé
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezodtoké	bezodtoký	k2eAgNnSc1d1	bezodtoké
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ťan-šanu	Ťan-šan	k1gInSc2	Ťan-šan
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
oblasti	oblast	k1gFnSc6	oblast
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
horských	horský	k2eAgNnPc2d1	horské
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
mezihorské	mezihorský	k2eAgFnSc6d1	Mezihorská
Issykkulské	Issykkulský	k2eAgFnSc6d1	Issykkulský
kotlině	kotlina	k1gFnSc6	kotlina
mezi	mezi	k7c4	mezi
hřbety	hřbet	k1gInPc4	hřbet
Kungej-Alatau	Kungej-Alataus	k1gInSc2	Kungej-Alataus
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Terskej-Alatau	Terskej-Alataus	k1gInSc6	Terskej-Alataus
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
6236	[number]	k4	6236
km2	km2	k4	km2
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
6	[number]	k4	6
330	[number]	k4	330
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
178	[number]	k4	178
km	km	kA	km
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
široké	široký	k2eAgFnPc1d1	široká
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hloubku	hloubka	k1gFnSc4	hloubka
má	mít	k5eAaImIp3nS	mít
278	[number]	k4	278
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
668	[number]	k4	668
m	m	kA	m
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
až	až	k9	až
702	[number]	k4	702
m	m	kA	m
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
1738	[number]	k4	1738
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
jezera	jezero	k1gNnSc2	jezero
Issyk	Issyka	k1gFnPc2	Issyka
kul	kula	k1gFnPc2	kula
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
21	[number]	k4	21
900	[number]	k4	900
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1608	[number]	k4	1608
m.	m.	k?	m.
Po	po	k7c6	po
jezeře	jezero	k1gNnSc6	jezero
Titicaca	Titicacum	k1gNnSc2	Titicacum
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
horské	horský	k2eAgNnSc4d1	horské
jezero	jezero	k1gNnSc4	jezero
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
a	a	k8xC	a
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
oblast	oblast	k1gFnSc1	oblast
centrálního	centrální	k2eAgInSc2d1	centrální
Ťan-šanu	Ťan-šan	k1gInSc2	Ťan-šan
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
4	[number]	k4	4
311	[number]	k4	311
588	[number]	k4	588
ha	ha	kA	ha
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
členité	členitý	k2eAgNnSc1d1	členité
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgInPc1d1	hluboký
zálivy	záliv	k1gInPc1	záliv
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
(	(	kIx(	(
<g/>
Tjupský	Tjupský	k2eAgInSc4d1	Tjupský
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
Džergalanský	Džergalanský	k2eAgInSc4d1	Džergalanský
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
Pokrovká	Pokrovký	k2eAgFnSc1d1	Pokrovký
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
597	[number]	k4	597
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
písečné	písečný	k2eAgFnPc4d1	písečná
pláže	pláž	k1gFnPc4	pláž
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
jílové	jílový	k2eAgInPc4d1	jílový
a	a	k8xC	a
oblázkové	oblázkový	k2eAgInPc4d1	oblázkový
břehy	břeh	k1gInPc4	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
ústí	ústit	k5eAaImIp3nS	ústit
50	[number]	k4	50
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
celkový	celkový	k2eAgInSc1d1	celkový
roční	roční	k2eAgInSc1d1	roční
přítok	přítok	k1gInSc1	přítok
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnPc1	řeka
Džergalan	Džergalana	k1gFnPc2	Džergalana
(	(	kIx(	(
<g/>
střední	střední	k2eAgInSc4d1	střední
průtok	průtok	k1gInSc4	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
22	[number]	k4	22
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tjup	Tjup	k1gMnSc1	Tjup
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
vlévá	vlévat	k5eAaImIp3nS	vlévat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Ču	Ču	k1gFnSc1	Ču
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
západním	západní	k2eAgInSc7d1	západní
okrajem	okraj	k1gInSc7	okraj
kotliny	kotlina	k1gFnSc2	kotlina
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
s	s	k7c7	s
jezerem	jezero	k1gNnSc7	jezero
spojená	spojený	k2eAgNnPc4d1	spojené
<g/>
.	.	kIx.	.
</s>
<s>
Odtok	odtok	k1gInSc1	odtok
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Ču	Ču	k1gFnSc2	Ču
existoval	existovat	k5eAaImAgInS	existovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
hladina	hladina	k1gFnSc1	hladina
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
m	m	kA	m
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
taková	takový	k3xDgFnSc1	takový
epocha	epocha	k1gFnSc1	epocha
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
je	být	k5eAaImIp3nS	být
smíšený	smíšený	k2eAgInSc1d1	smíšený
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
sněhový	sněhový	k2eAgMnSc1d1	sněhový
<g/>
,	,	kIx,	,
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
stavy	stav	k1gInPc7	stav
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
změn	změna	k1gFnPc2	změna
výšky	výška	k1gFnSc2	výška
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgInPc2d1	způsobený
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
jezerní	jezerní	k2eAgInPc4d1	jezerní
terasy	teras	k1gInPc4	teras
vysoké	vysoká	k1gFnSc2	vysoká
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
a	a	k8xC	a
podvodní	podvodní	k2eAgFnSc2d1	podvodní
rozvaliny	rozvalina	k1gFnSc2	rozvalina
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
do	do	k7c2	do
8	[number]	k4	8
m.	m.	k?	m.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgInPc2d1	poslední
dvou	dva	k4xCgInPc2	dva
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
snižuje	snižovat	k5eAaImIp3nS	snižovat
od	od	k7c2	od
r.	r.	kA	r.
1886	[number]	k4	1886
o	o	k7c4	o
4	[number]	k4	4
m	m	kA	m
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdroj	zdroj	k1gInSc4	zdroj
<g/>
;	;	kIx,	;
až	až	k9	až
o	o	k7c4	o
7	[number]	k4	7
m	m	kA	m
<g/>
)	)	kIx)	)
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
pokles	pokles	k1gInSc1	pokles
není	být	k5eNaImIp3nS	být
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
rychlost	rychlost	k1gFnSc1	rychlost
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
neotektonickým	otektonický	k2eNgInSc7d1	otektonický
vzestupem	vzestup	k1gInSc7	vzestup
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
vytvoření	vytvoření	k1gNnSc1	vytvoření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teras	terasa	k1gFnPc2	terasa
(	(	kIx(	(
<g/>
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
jezerní	jezerní	k2eAgFnSc2d1	jezerní
kotliny	kotlina	k1gFnSc2	kotlina
je	být	k5eAaImIp3nS	být
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
16	[number]	k4	16
až	až	k9	až
17	[number]	k4	17
°	°	k?	°
<g/>
С	С	k?	С
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
na	na	k7c6	na
západě	západ	k1gInSc6	západ
-2	-2	k4	-2
až	až	k9	až
-3	-3	k4	-3
°	°	k?	°
<g/>
С	С	k?	С
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
-4	-4	k4	-4
až	až	k9	až
-7	-7	k4	-7
°	°	k?	°
<g/>
С	С	k?	С
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
250	[number]	k4	250
mm	mm	kA	mm
(	(	kIx(	(
<g/>
na	na	k7c6	na
západě	západ	k1gInSc6	západ
110	[number]	k4	110
mm	mm	kA	mm
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
470	[number]	k4	470
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpar	odpar	k1gInSc1	odpar
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
700	[number]	k4	700
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vanou	vanout	k5eAaImIp3nP	vanout
silné	silný	k2eAgInPc1d1	silný
větry	vítr	k1gInPc1	vítr
(	(	kIx(	(
<g/>
až	až	k9	až
30-40	[number]	k4	30-40
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
náhlé	náhlý	k2eAgFnPc4d1	náhlá
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
vlny	vlna	k1gFnPc4	vlna
vysoké	vysoká	k1gFnSc2	vysoká
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
m.	m.	k?	m.
Roční	roční	k2eAgFnSc1d1	roční
amplituda	amplituda	k1gFnSc1	amplituda
kolísání	kolísání	k1gNnSc2	kolísání
hladiny	hladina	k1gFnSc2	hladina
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
srpnu	srpen	k1gInSc3	srpen
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc6d2	nižší
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
°	°	k?	°
<g/>
С	С	k?	С
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
19	[number]	k4	19
až	až	k9	až
24	[number]	k4	24
°	°	k?	°
<g/>
С	С	k?	С
<g/>
,	,	kIx,	,
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
100	[number]	k4	100
m	m	kA	m
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
teplota	teplota	k1gFnSc1	teplota
3,5	[number]	k4	3,5
až	až	k9	až
4	[number]	k4	4
°	°	k?	°
<g/>
С	С	k?	С
Led	led	k1gInSc1	led
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zálivech	záliv	k1gInPc6	záliv
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
zimách	zima	k1gFnPc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průzračná	průzračný	k2eAgFnSc1d1	průzračná
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
12	[number]	k4	12
m.	m.	k?	m.
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
slanosti	slanost	k1gFnSc2	slanost
5,8	[number]	k4	5,8
‰	‰	k?	‰
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
na	na	k7c6	na
pití	pití	k1gNnSc6	pití
ani	ani	k8xC	ani
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jezero	jezero	k1gNnSc1	jezero
nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Issyk-kulské	Issykulský	k2eAgFnSc6d1	Issyk-kulský
oblasti	oblast	k1gFnSc6	oblast
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
na	na	k7c4	na
-25	-25	k4	-25
°	°	k?	°
<g/>
C	C	kA	C
i	i	k9	i
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flóra	Flóra	k1gFnSc1	Flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zálivech	záliv	k1gInPc6	záliv
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
30	[number]	k4	30
m	m	kA	m
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
souvislý	souvislý	k2eAgInSc1d1	souvislý
porost	porost	k1gInSc1	porost
řas	řasa	k1gFnPc2	řasa
parožnatek	parožnatka	k1gFnPc2	parožnatka
a	a	k8xC	a
rdestů	rdest	k1gInPc2	rdest
<g/>
,	,	kIx,	,
dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
převážně	převážně	k6eAd1	převážně
hlínou	hlína	k1gFnSc7	hlína
a	a	k8xC	a
jílem	jíl	k1gInSc7	jíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
je	být	k5eAaImIp3nS	být
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
zpracování	zpracování	k1gNnSc4	zpracování
jelců	jelec	k1gMnPc2	jelec
<g/>
,	,	kIx,	,
holých	holý	k2eAgMnPc2d1	holý
osmanů	osman	k1gMnPc2	osman
<g/>
,	,	kIx,	,
kaprů	kapr	k1gMnPc2	kapr
a	a	k8xC	a
marinek	marinka	k1gFnPc2	marinka
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
až	až	k9	až
4	[number]	k4	4
tisíce	tisíc	k4xCgInSc2	tisíc
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zálivech	záliv	k1gInPc6	záliv
na	na	k7c6	na
západě	západ	k1gInSc6	západ
jezera	jezero	k1gNnSc2	jezero
každoročně	každoročně	k6eAd1	každoročně
zimuje	zimovat	k5eAaImIp3nS	zimovat
20	[number]	k4	20
až	až	k9	až
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
převážně	převážně	k6eAd1	převážně
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
poláci	polák	k1gMnPc1	polák
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc1	kachna
<g/>
,	,	kIx,	,
lysky	lyska	k1gFnPc1	lyska
a	a	k8xC	a
labutě	labuť	k1gFnPc1	labuť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
r	r	kA	r
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
Issykkulská	Issykkulský	k2eAgFnSc1d1	Issykkulský
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
místní	místní	k2eAgFnSc1d1	místní
osobní	osobní	k2eAgFnSc1d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc1d1	nákladní
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Balykčy	Balykč	k2eAgInPc1d1	Balykč
(	(	kIx(	(
<g/>
Rybačje	Rybačj	k1gInPc1	Rybačj
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
balyk	balyk	k6eAd1	balyk
je	být	k5eAaImIp3nS	být
kyrgyzsky	kyrgyzsky	k6eAd1	kyrgyzsky
ryba	ryba	k1gFnSc1	ryba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pristaň	Pristaň	k1gFnSc1	Pristaň
Prževalsk	Prževalsk	k1gInSc1	Prževalsk
(	(	kIx(	(
<g/>
Přístav	přístav	k1gInSc1	přístav
Prževalsk	Prževalsk	k1gInSc1	Prževalsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
druhého	druhý	k4xOgNnSc2	druhý
je	on	k3xPp3gInPc4	on
mohyla	mohyla	k1gFnSc1	mohyla
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
ruského	ruský	k2eAgMnSc2d1	ruský
cestovatele	cestovatel	k1gMnSc2	cestovatel
N.	N.	kA	N.
M.	M.	kA	M.
Prževalského	Prževalský	k2eAgInSc2d1	Prževalský
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
obydlených	obydlený	k2eAgNnPc2d1	obydlené
míst	místo	k1gNnPc2	místo
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
obživou	obživa	k1gFnSc7	obživa
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
(	(	kIx(	(
<g/>
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
sadovnictví	sadovnictví	k1gNnSc2	sadovnictví
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
.	.	kIx.	.
7	[number]	k4	7
km	km	kA	km
od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
město	město	k1gNnSc1	město
Karakol	Karakola	k1gFnPc2	Karakola
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Prževalsk	Prževalsk	k1gInSc1	Prževalsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
jsou	být	k5eAaImIp3nP	být
klimatické	klimatický	k2eAgFnPc1d1	klimatická
lázně	lázeň	k1gFnPc1	lázeň
Čolpon-Ata	Čolpon-Ato	k1gNnSc2	Čolpon-Ato
a	a	k8xC	a
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
Tamga	Tamg	k1gMnSc2	Tamg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
prázdninové	prázdninový	k2eAgInPc1d1	prázdninový
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
tábory	tábor	k1gInPc1	tábor
a	a	k8xC	a
chaty	chata	k1gFnPc1	chata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
И	И	k?	И
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
↑	↑	k?	↑
a	a	k8xC	a
b	b	k?	b
Lake	Lake	k1gInSc1	Lake
Issyk-Kul	Issyk-Kul	k1gInSc1	Issyk-Kul
-	-	kIx~	-
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
-	-	kIx~	-
Asia	Asia	k1gFnSc1	Asia
-	-	kIx~	-
Destinations	Destinations	k1gInSc1	Destinations
|	|	kIx~	|
Bradt	Bradt	k2eAgMnSc1d1	Bradt
Travel	Travel	k1gMnSc1	Travel
Guides	Guides	k1gMnSc1	Guides
<g/>
.	.	kIx.	.
www.bradtguides.com	www.bradtguides.com	k1gInSc1	www.bradtguides.com
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
↑	↑	k?	↑
Issyk	Issyk	k1gMnSc1	Issyk
Kul	kout	k5eAaImAgMnS	kout
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UNESCO	Unesco	k1gNnSc1	Unesco
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Berg	Berg	k1gInSc1	Berg
L.	L.	kA	L.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Hydrologický	hydrologický	k2eAgInSc1d1	hydrologický
výzkum	výzkum	k1gInSc1	výzkum
na	na	k7c4	na
Issyk-kulu	Issykula	k1gFnSc4	Issyk-kula
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Б	Б	k?	Б
Л	Л	k?	Л
С	С	k?	С
<g/>
,	,	kIx,	,
Г	Г	k?	Г
и	и	k?	и
н	н	k?	н
И	И	k?	И
в	в	k?	в
1928	[number]	k4	1928
г	г	k?	г
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Vládního	vládní	k2eAgInSc2d1	vládní
hydrologického	hydrologický	k2eAgInSc2d1	hydrologický
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
28	[number]	k4	28
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
И	И	k?	И
<g/>
.	.	kIx.	.
Г	Г	k?	Г
<g/>
.	.	kIx.	.
Г	Г	k?	Г
и	и	k?	и
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
No	no	k9	no
28	[number]	k4	28
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Sapožnikov	Sapožnikov	k1gInSc1	Sapožnikov
D.	D.	kA	D.
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Viselkina	Viselkina	k1gMnSc1	Viselkina
M.	M.	kA	M.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Současné	současný	k2eAgFnPc1d1	současná
usazeniny	usazenina	k1gFnPc1	usazenina
jezera	jezero	k1gNnSc2	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
zálivů	záliv	k1gInPc2	záliv
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
С	С	k?	С
Д	Д	k?	Д
Г	Г	k?	Г
<g/>
,	,	kIx,	,
В	В	k?	В
М	М	k?	М
А	А	k?	А
<g/>
,	,	kIx,	,
С	С	k?	С
о	о	k?	о
о	о	k?	о
И	И	k?	И
и	и	k?	и
е	е	k?	е
з	з	k?	з
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Korotajev	Korotajev	k1gMnSc1	Korotajev
V.	V.	kA	V.
N.	N.	kA	N.
<g/>
,	,	kIx,	,
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
zóna	zóna	k1gFnSc1	zóna
jezera	jezero	k1gNnSc2	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
<g/>
,	,	kIx,	,
Frunze	Frunze	k1gNnSc7	Frunze
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
В	В	k?	В
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Б	Б	k?	Б
з	з	k?	з
о	о	k?	о
И	И	k?	И
<g/>
,	,	kIx,	,
Ф	Ф	k?	Ф
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
