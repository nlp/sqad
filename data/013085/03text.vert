<p>
<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
relikvie	relikvie	k1gFnSc2	relikvie
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Deathly	Deathly	k1gFnSc2	Deathly
Hallows	Hallowsa	k1gFnPc2	Hallowsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmá	sedmý	k4xOgFnSc1	sedmý
kniha	kniha	k1gFnSc1	kniha
ze	z	k7c2	z
série	série	k1gFnSc2	série
Joanne	Joann	k1gInSc5	Joann
Rowlingové	Rowlingový	k2eAgFnSc6d1	Rowlingová
o	o	k7c6	o
čarodějnickém	čarodějnický	k2eAgMnSc6d1	čarodějnický
učni	učeň	k1gMnSc6	učeň
Harrym	Harrym	k1gInSc1	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
otázek	otázka	k1gFnPc2	otázka
nastolených	nastolený	k2eAgFnPc2d1	nastolená
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
dílech	díl	k1gInPc6	díl
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
zde	zde	k6eAd1	zde
také	také	k9	také
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
bitva	bitva	k1gFnSc1	bitva
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
proti	proti	k7c3	proti
černému	černý	k2eAgMnSc3d1	černý
mágovi	mág	k1gMnSc3	mág
lordu	lord	k1gMnSc3	lord
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
zlomila	zlomit	k5eAaPmAgFnS	zlomit
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
výtisků	výtisk	k1gInPc2	výtisk
v	v	k7c4	v
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
–	–	k?	–
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
koupilo	koupit	k5eAaPmAgNnS	koupit
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlad	překlad	k1gInSc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Pavel	Pavel	k1gMnSc1	Pavel
Medek	Medek	k1gMnSc1	Medek
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
však	však	k9	však
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
anglického	anglický	k2eAgNnSc2d1	anglické
vydání	vydání	k1gNnSc2	vydání
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
(	(	kIx(	(
<g/>
a	a	k8xC	a
nelegální	legální	k2eNgInSc1d1	nelegální
<g/>
)	)	kIx)	)
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
skupina	skupina	k1gFnSc1	skupina
fanoušků	fanoušek	k1gMnPc2	fanoušek
(	(	kIx(	(
<g/>
označovala	označovat	k5eAaImAgFnS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Fénixův	fénixův	k2eAgInSc1d1	fénixův
tým	tým	k1gInSc1	tým
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
dokončila	dokončit	k5eAaPmAgFnS	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
překlad	překlad	k1gInSc4	překlad
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
anglické	anglický	k2eAgFnSc2d1	anglická
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
díl	díl	k1gInSc1	díl
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
Albatros	albatros	k1gMnSc1	albatros
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c6	na
papíru	papír	k1gInSc6	papír
s	s	k7c7	s
certifikátem	certifikát	k1gInSc7	certifikát
FSC	FSC	kA	FSC
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
požadavku	požadavek	k1gInSc2	požadavek
Joanne	Joann	k1gInSc5	Joann
Rowlingové	Rowlingový	k2eAgNnSc1d1	Rowlingové
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
její	její	k3xOp3gFnPc1	její
knihy	kniha	k1gFnPc1	kniha
vycházely	vycházet	k5eAaImAgFnP	vycházet
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
šetrném	šetrný	k2eAgInSc6d1	šetrný
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
již	již	k9	již
několik	několik	k4yIc1	několik
knih	kniha	k1gFnPc2	kniha
série	série	k1gFnSc2	série
na	na	k7c6	na
certifikovaném	certifikovaný	k2eAgInSc6d1	certifikovaný
papíře	papír	k1gInSc6	papír
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
i	i	k8xC	i
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc4	překlad
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
ekologickém	ekologický	k2eAgInSc6d1	ekologický
papíře	papír	k1gInSc6	papír
již	již	k6eAd1	již
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
takovouto	takovýto	k3xDgFnSc4	takovýto
publikaci	publikace	k1gFnSc4	publikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
příběhu	příběh	k1gInSc2	příběh
Harry	Harra	k1gFnSc2	Harra
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
svou	svůj	k3xOyFgFnSc4	svůj
tetu	teta	k1gFnSc4	teta
<g/>
,	,	kIx,	,
strýce	strýc	k1gMnSc4	strýc
a	a	k8xC	a
bratrance	bratranec	k1gMnSc4	bratranec
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
půjdou	jít	k5eAaImIp3nP	jít
smrtijedi	smrtijed	k1gMnPc1	smrtijed
hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
Harry	Harra	k1gFnPc1	Harra
dovrší	dovršit	k5eAaPmIp3nP	dovršit
kouzelnické	kouzelnický	k2eAgFnPc1d1	kouzelnická
plnoletosti	plnoletost	k1gFnPc1	plnoletost
(	(	kIx(	(
<g/>
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyprší	vypršet	k5eAaPmIp3nS	vypršet
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gNnSc4	on
chránila	chránit	k5eAaImAgFnS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc4d1	samotný
Harry	Harr	k1gInPc4	Harr
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
od	od	k7c2	od
Dursleyů	Dursley	k1gMnPc2	Dursley
k	k	k7c3	k
Weasleyům	Weasley	k1gMnPc3	Weasley
<g/>
.	.	kIx.	.
</s>
<s>
Přesun	přesun	k1gInSc1	přesun
organizuje	organizovat	k5eAaBmIp3nS	organizovat
Fénixův	fénixův	k2eAgInSc4d1	fénixův
řád	řád	k1gInSc4	řád
a	a	k8xC	a
celé	celý	k2eAgFnSc6d1	celá
akci	akce	k1gFnSc6	akce
velí	velet	k5eAaImIp3nS	velet
bystrozor	bystrozor	k1gInSc1	bystrozor
Alastor	Alastor	k1gInSc4	Alastor
Moody	Mooda	k1gFnSc2	Mooda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
nemohou	moct	k5eNaImIp3nP	moct
použít	použít	k5eAaPmF	použít
přemístění	přemístění	k1gNnSc4	přemístění
<g/>
,	,	kIx,	,
přenášedlo	přenášedlo	k1gNnSc4	přenášedlo
ani	ani	k8xC	ani
letaxovou	letaxový	k2eAgFnSc4d1	letaxová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
Voldemort	Voldemort	k1gInSc1	Voldemort
plně	plně	k6eAd1	plně
neovládá	ovládat	k5eNaImIp3nS	ovládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
řadu	řada	k1gFnSc4	řada
informátorů	informátor	k1gMnPc2	informátor
<g/>
,	,	kIx,	,
kouzelníků	kouzelník	k1gMnPc2	kouzelník
pod	pod	k7c7	pod
kletbou	kletba	k1gFnSc7	kletba
Imperius	Imperius	k1gInSc1	Imperius
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
by	by	kYmCp3nS	by
vyzrazení	vyzrazení	k1gNnSc1	vyzrazení
<g/>
.	.	kIx.	.
</s>
<s>
Přesun	přesun	k1gInSc1	přesun
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
riskantně	riskantně	k6eAd1	riskantně
proveden	provést	k5eAaPmNgInS	provést
na	na	k7c6	na
košťatech	koště	k1gNnPc6	koště
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmatení	zmatení	k1gNnSc4	zmatení
případných	případný	k2eAgMnPc2d1	případný
pronásledovatelů	pronásledovatel	k1gMnPc2	pronásledovatel
vypije	vypít	k5eAaPmIp3nS	vypít
dalších	další	k2eAgNnPc2d1	další
šest	šest	k4xCc4	šest
Harryho	Harryha	k1gFnSc5	Harryha
přátel	přítel	k1gMnPc2	přítel
mnoholičný	mnoholičný	k2eAgInSc4d1	mnoholičný
lektvar	lektvar	k1gInSc4	lektvar
a	a	k8xC	a
promění	proměnit	k5eAaPmIp3nS	proměnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c4	v
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
honičce	honička	k1gFnSc6	honička
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
(	(	kIx(	(
<g/>
kterým	který	k3yIgFnPc3	který
předtím	předtím	k6eAd1	předtím
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
prozradí	prozradit	k5eAaPmIp3nP	prozradit
datum	datum	k1gNnSc4	datum
přesunu	přesun	k1gInSc2	přesun
<g/>
)	)	kIx)	)
zahyne	zahynout	k5eAaPmIp3nS	zahynout
Alastor	Alastor	k1gMnSc1	Alastor
Moody	Mooda	k1gFnSc2	Mooda
a	a	k8xC	a
Harryho	Harry	k1gMnSc2	Harry
sova	sova	k1gFnSc1	sova
Hedvika	Hedvika	k1gFnSc1	Hedvika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Weasleyů	Weasley	k1gMnPc2	Weasley
Harry	Harra	k1gFnSc2	Harra
smutně	smutně	k6eAd1	smutně
oslaví	oslavit	k5eAaPmIp3nP	oslavit
své	svůj	k3xOyFgFnPc4	svůj
sedmnácté	sedmnáctý	k4xOgFnPc4	sedmnáctý
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
Hermionou	Hermiona	k1gFnSc7	Hermiona
dostanou	dostat	k5eAaPmIp3nP	dostat
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
odkázal	odkázat	k5eAaPmAgMnS	odkázat
Brumbál	brumbál	k1gMnSc1	brumbál
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
nespolupracoval	spolupracovat	k5eNaImAgMnS	spolupracovat
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
jim	on	k3xPp3gMnPc3	on
prozradit	prozradit	k5eAaPmF	prozradit
nic	nic	k3yNnSc1	nic
o	o	k7c6	o
viteálech	viteál	k1gInPc6	viteál
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
svém	svůj	k3xOyFgInSc6	svůj
plánu	plán	k1gInSc6	plán
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
pána	pán	k1gMnSc2	pán
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
předměty	předmět	k1gInPc4	předmět
dlouho	dlouho	k6eAd1	dlouho
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
najdou	najít	k5eAaPmIp3nP	najít
nějakou	nějaký	k3yIgFnSc4	nějaký
tajnou	tajný	k2eAgFnSc4d1	tajná
zbraň	zbraň	k1gFnSc4	zbraň
nebo	nebo	k8xC	nebo
indicii	indicie	k1gFnSc4	indicie
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
nalezení	nalezení	k1gNnSc3	nalezení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
museli	muset	k5eAaImAgMnP	muset
dědictví	dědictví	k1gNnSc4	dědictví
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
dostala	dostat	k5eAaPmAgFnS	dostat
knížku	knížka	k1gFnSc4	knížka
bajek	bajka	k1gFnPc2	bajka
<g/>
,	,	kIx,	,
Ron	Ron	k1gMnSc1	Ron
zatemňovač	zatemňovač	k1gMnSc1	zatemňovač
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
zlatonku	zlatonek	k1gInSc2	zlatonek
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chytil	chytit	k5eAaPmAgMnS	chytit
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
zápasu	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dostat	dostat	k5eAaPmF	dostat
také	také	k9	také
meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k8xC	ale
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
nevydalo	vydat	k5eNaPmAgNnS	vydat
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
škole	škola	k1gFnSc3	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
Brumbálovi	brumbálův	k2eAgMnPc1d1	brumbálův
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ho	on	k3xPp3gInSc4	on
ani	ani	k8xC	ani
nemůže	moct	k5eNaImIp3nS	moct
nikomu	nikdo	k3yNnSc3	nikdo
odkázat	odkázat	k5eAaPmF	odkázat
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
poté	poté	k6eAd1	poté
mluví	mluvit	k5eAaImIp3nS	mluvit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Harrym	Harrymum	k1gNnPc2	Harrymum
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
přimět	přimět	k5eAaPmF	přimět
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
svatby	svatba	k1gFnPc1	svatba
Billa	Bill	k1gMnSc2	Bill
a	a	k8xC	a
Fleur	Fleura	k1gFnPc2	Fleura
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
je	být	k5eAaImIp3nS	být
maskován	maskovat	k5eAaBmNgInS	maskovat
pomocí	pomocí	k7c2	pomocí
mnoholičného	mnoholičný	k2eAgInSc2d1	mnoholičný
lektvaru	lektvar	k1gInSc2	lektvar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
svědkem	svědek	k1gMnSc7	svědek
roztržky	roztržka	k1gFnSc2	roztržka
mezi	mezi	k7c7	mezi
Krumem	Krum	k1gMnSc7	Krum
a	a	k8xC	a
Xenofiliusem	Xenofilius	k1gMnSc7	Xenofilius
Láskorádem	Láskorád	k1gMnSc7	Láskorád
-	-	kIx~	-
otcem	otec	k1gMnSc7	otec
Lenky	Lenka	k1gFnPc1	Lenka
Láskorádové	Láskorád	k1gMnPc1	Láskorád
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vyslechne	vyslechnout	k5eAaPmIp3nS	vyslechnout
pomluvy	pomluva	k1gFnPc4	pomluva
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Brumbála	brumbál	k1gMnSc2	brumbál
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
Ronovy	Ronův	k2eAgFnSc2d1	Ronova
tety	teta	k1gFnSc2	teta
Muriel	Muriela	k1gFnPc2	Muriela
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	době	k6eAd1	době
přerušena	přerušit	k5eAaPmNgFnS	přerušit
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
pádu	pád	k1gInSc6	pád
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc3	smrt
ministra	ministr	k1gMnSc2	ministr
a	a	k8xC	a
příchodem	příchod	k1gInSc7	příchod
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ron	ron	k1gInSc1	ron
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
Harry	Harr	k1gInPc1	Harr
utečou	utéct	k5eAaPmIp3nP	utéct
a	a	k8xC	a
uchýlí	uchýlit	k5eAaPmIp3nP	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
kavárny	kavárna	k1gFnSc2	kavárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
vypátráni	vypátrán	k2eAgMnPc1d1	vypátrán
Smrtijedy	Smrtijed	k1gMnPc4	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
najdou	najít	k5eAaPmIp3nP	najít
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Blackovy	Blackův	k2eAgFnSc2d1	Blackova
rodiny	rodina	k1gFnSc2	rodina
na	na	k7c6	na
Grimmauldově	Grimmauldův	k2eAgNnSc6d1	Grimmauldovo
náměstí	náměstí	k1gNnSc6	náměstí
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
jako	jako	k9	jako
opravdu	opravdu	k6eAd1	opravdu
velice	velice	k6eAd1	velice
schopná	schopný	k2eAgFnSc1d1	schopná
a	a	k8xC	a
předvídavá	předvídavý	k2eAgFnSc1d1	předvídavá
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
věci	věc	k1gFnPc4	věc
k	k	k7c3	k
pátrání	pátrání	k1gNnSc3	pátrání
po	po	k7c6	po
viteálech	viteál	k1gInPc6	viteál
včetně	včetně	k7c2	včetně
hromady	hromada	k1gFnSc2	hromada
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
stanu	stan	k1gInSc2	stan
<g/>
,	,	kIx,	,
lektvarů	lektvar	k1gInPc2	lektvar
apod.	apod.	kA	apod.
naloží	naložit	k5eAaPmIp3nS	naložit
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
očarované	očarovaný	k2eAgFnSc2d1	očarovaná
korálkové	korálkový	k2eAgFnSc2d1	korálková
kabelky	kabelka	k1gFnSc2	kabelka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
najdou	najít	k5eAaPmIp3nP	najít
domácího	domácí	k2eAgMnSc4d1	domácí
skřítka	skřítek	k1gMnSc4	skřítek
Kráturu	Krátura	k1gFnSc4	Krátura
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Regulus	Regulus	k1gMnSc1	Regulus
Arcturus	Arcturus	k1gMnSc1	Arcturus
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Siriusův	Siriusův	k2eAgMnSc1d1	Siriusův
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
vzali	vzít	k5eAaPmAgMnP	vzít
Voldemortův	Voldemortův	k2eAgInSc4d1	Voldemortův
viteál	viteál	k1gInSc4	viteál
z	z	k7c2	z
podzemního	podzemní	k2eAgNnSc2d1	podzemní
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčný	dotyčný	k2eAgInSc1d1	dotyčný
medailon	medailon	k1gInSc1	medailon
ale	ale	k8xC	ale
v	v	k7c6	v
domě	dům	k1gInSc6	dům
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
přikáže	přikázat	k5eAaPmIp3nS	přikázat
Kráturovi	Krátur	k1gMnSc3	Krátur
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
Mundunguse	Mundunguse	k1gFnPc4	Mundunguse
Fletchera	Fletchero	k1gNnSc2	Fletchero
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
podezírá	podezírat	k5eAaImIp3nS	podezírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
medailon	medailon	k1gInSc4	medailon
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fletcher	Fletchra	k1gFnPc2	Fletchra
je	být	k5eAaImIp3nS	být
přivlečen	přivléct	k5eAaPmNgInS	přivléct
Kráturou	Krátura	k1gFnSc7	Krátura
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
triu	trio	k1gNnSc3	trio
<g/>
,	,	kIx,	,
že	že	k8xS	že
medailon	medailon	k1gInSc1	medailon
dal	dát	k5eAaPmAgInS	dát
jako	jako	k8xC	jako
úplatek	úplatek	k1gInSc1	úplatek
kouzelnici	kouzelnice	k1gFnSc4	kouzelnice
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
popisu	popis	k1gInSc2	popis
Dolores	Dolores	k1gInSc4	Dolores
Umbridgeové	Umbridgeové	k2eAgInSc4d1	Umbridgeové
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
vydají	vydat	k5eAaPmIp3nP	vydat
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
už	už	k9	už
pod	pod	k7c7	pod
úplnou	úplný	k2eAgFnSc7d1	úplná
kontrolou	kontrola	k1gFnSc7	kontrola
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
výslechy	výslech	k1gInPc1	výslech
a	a	k8xC	a
utlačováním	utlačování	k1gNnSc7	utlačování
kouzelníků	kouzelník	k1gMnPc2	kouzelník
mudlovského	mudlovský	k2eAgInSc2d1	mudlovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přestrojení	přestrojení	k1gNnSc3	přestrojení
použijí	použít	k5eAaPmIp3nP	použít
mnoholičný	mnoholičný	k2eAgInSc4d1	mnoholičný
lektvar	lektvar	k1gInSc4	lektvar
<g/>
.	.	kIx.	.
</s>
<s>
Čekají	čekat	k5eAaImIp3nP	čekat
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
<g/>
,	,	kIx,	,
omráčí	omráčit	k5eAaPmIp3nS	omráčit
dva	dva	k4xCgMnPc4	dva
kouzelníky	kouzelník	k1gMnPc4	kouzelník
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
čarodějku	čarodějka	k1gFnSc4	čarodějka
a	a	k8xC	a
díky	dík	k1gInPc7	dík
lektvaru	lektvar	k1gInSc2	lektvar
změní	změnit	k5eAaPmIp3nP	změnit
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proniknutí	proniknutí	k1gNnSc6	proniknutí
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
plán	plán	k1gInSc1	plán
ale	ale	k9	ale
hroutí	hroutit	k5eAaImIp3nS	hroutit
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
kouzelníka	kouzelník	k1gMnSc4	kouzelník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
doprovodit	doprovodit	k5eAaPmF	doprovodit
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
–	–	k?	–
mudlovskou	mudlovský	k2eAgFnSc4d1	mudlovská
čarodějku	čarodějka	k1gFnSc4	čarodějka
–	–	k?	–
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
přestrojený	přestrojený	k2eAgMnSc1d1	přestrojený
Ron	Ron	k1gMnSc1	Ron
nestihl	stihnout	k5eNaPmAgMnS	stihnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
likvidovat	likvidovat	k5eAaBmF	likvidovat
deštivé	deštivý	k2eAgNnSc4d1	deštivé
kouzlo	kouzlo	k1gNnSc4	kouzlo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
od	od	k7c2	od
ostatních	ostatní	k1gNnPc2	ostatní
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
drobnou	drobný	k2eAgFnSc4d1	drobná
čarodějku	čarodějka	k1gFnSc4	čarodějka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
asistovat	asistovat	k5eAaImF	asistovat
u	u	k7c2	u
výslechu	výslech	k1gInSc2	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
vede	vést	k5eAaImIp3nS	vést
Umbridgeová	Umbridgeová	k1gFnSc1	Umbridgeová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
odvede	odvést	k5eAaPmIp3nS	odvést
do	do	k7c2	do
výslechové	výslechový	k2eAgFnSc2d1	výslechová
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nP	změnit
v	v	k7c4	v
postavu	postava	k1gFnSc4	postava
mohutného	mohutný	k2eAgMnSc2d1	mohutný
Smrtijeda	Smrtijed	k1gMnSc2	Smrtijed
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
budí	budit	k5eAaImIp3nS	budit
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
pracovníků	pracovník	k1gMnPc2	pracovník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
respekt	respekt	k1gInSc1	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
neviditelnému	viditelný	k2eNgInSc3d1	neviditelný
plášti	plášť	k1gInSc3	plášť
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
Dolores	Doloresa	k1gFnPc2	Doloresa
Umbridgeové	Umbridgeová	k1gFnSc2	Umbridgeová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hledá	hledat	k5eAaImIp3nS	hledat
medailon	medailon	k1gInSc1	medailon
<g/>
.	.	kIx.	.
</s>
<s>
Všimne	všimnout	k5eAaPmIp3nS	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
dveřích	dveře	k1gFnPc6	dveře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
otvoru	otvor	k1gInSc6	otvor
pro	pro	k7c4	pro
kukátko	kukátko	k1gNnSc4	kukátko
čarodějné	čarodějný	k2eAgNnSc1d1	čarodějné
oko	oko	k1gNnSc1	oko
zabitého	zabitý	k1gMnSc2	zabitý
bystrozora	bystrozor	k1gMnSc2	bystrozor
Moodyho	Moody	k1gMnSc2	Moody
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
vzít	vzít	k5eAaPmF	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukáže	ukázat	k5eAaPmIp3nS	ukázat
jako	jako	k9	jako
veliká	veliký	k2eAgFnSc1d1	veliká
chyba	chyba	k1gFnSc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
potkává	potkávat	k5eAaImIp3nS	potkávat
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
společně	společně	k6eAd1	společně
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
výslechové	výslechový	k2eAgFnSc3d1	výslechová
místnosti	místnost	k1gFnSc3	místnost
pro	pro	k7c4	pro
Hermionu	Hermion	k1gInSc2	Hermion
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
omráčí	omráčet	k5eAaImIp3nS	omráčet
Umbridgeovou	Umbridgeův	k2eAgFnSc7d1	Umbridgeův
a	a	k8xC	a
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
viteál	viteál	k1gInSc4	viteál
za	za	k7c4	za
kopii	kopie	k1gFnSc4	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
prchají	prchat	k5eAaImIp3nP	prchat
i	i	k9	i
s	s	k7c7	s
osvobozenou	osvobozený	k2eAgFnSc7d1	osvobozená
vyslýchanou	vyslýchaný	k2eAgFnSc7d1	vyslýchaná
čarodějkou	čarodějka	k1gFnSc7	čarodějka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
ale	ale	k9	ale
o	o	k7c6	o
vetřelcích	vetřelec	k1gMnPc6	vetřelec
ví	vědět	k5eAaImIp3nS	vědět
celé	celý	k2eAgNnSc4d1	celé
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
a	a	k8xC	a
Harry	Harra	k1gFnPc4	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
jen	jen	k9	jen
tak	tak	k6eAd1	tak
tak	tak	k6eAd1	tak
unikají	unikat	k5eAaImIp3nP	unikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přemístění	přemístění	k1gNnSc6	přemístění
do	do	k7c2	do
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Grimmauldově	Grimmauldův	k2eAgNnSc6d1	Grimmauldovo
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
Hermiony	Hermion	k1gInPc1	Hermion
chytí	chytit	k5eAaPmIp3nP	chytit
jeden	jeden	k4xCgInSc4	jeden
Smrtijed	Smrtijed	k1gInSc4	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
domu	dům	k1gInSc2	dům
narušena	narušen	k2eAgFnSc1d1	narušena
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
pohotově	pohotově	k6eAd1	pohotově
přemístí	přemístit	k5eAaPmIp3nS	přemístit
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
ale	ale	k9	ale
nastane	nastat	k5eAaPmIp3nS	nastat
nehoda	nehoda	k1gFnSc1	nehoda
a	a	k8xC	a
u	u	k7c2	u
Rona	Ron	k1gMnSc2	Ron
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odštěpu	odštěp	k1gInSc3	odštěp
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
ho	on	k3xPp3gMnSc4	on
rychle	rychle	k6eAd1	rychle
ošetří	ošetřit	k5eAaPmIp3nP	ošetřit
pomocí	pomocí	k7c2	pomocí
tinktury	tinktura	k1gFnSc2	tinktura
z	z	k7c2	z
třemdavy	třemdava	k1gFnSc2	třemdava
a	a	k8xC	a
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
zabezpečí	zabezpečit	k5eAaPmIp3nS	zabezpečit
místo	místo	k7c2	místo
úkrytu	úkryt	k1gInSc2	úkryt
ochrannými	ochranný	k2eAgNnPc7d1	ochranné
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
stále	stále	k6eAd1	stále
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
a	a	k8xC	a
stanovat	stanovat	k5eAaImF	stanovat
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Tíživá	tíživý	k2eAgFnSc1d1	tíživá
situace	situace	k1gFnSc1	situace
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
podporována	podporovat	k5eAaImNgFnS	podporovat
mocí	moc	k1gFnSc7	moc
viteálu	viteál	k1gInSc2	viteál
<g/>
)	)	kIx)	)
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c4	v
hádky	hádka	k1gFnPc4	hádka
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
Harryho	Harry	k1gMnSc4	Harry
a	a	k8xC	a
Hermionu	Hermion	k1gInSc3	Hermion
opustí	opustit	k5eAaPmIp3nP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
dva	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
navštívit	navštívit	k5eAaPmF	navštívit
Godrikův	Godrikův	k2eAgInSc4d1	Godrikův
důl	důl	k1gInSc4	důl
–	–	k?	–
Harryho	Harry	k1gMnSc2	Harry
rodiště	rodiště	k1gNnSc1	rodiště
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Harry	Harra	k1gFnSc2	Harra
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
jizvě	jizva	k1gFnSc3	jizva
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Najdou	najít	k5eAaPmIp3nP	najít
tady	tady	k6eAd1	tady
hrob	hrob	k1gInSc4	hrob
Harryho	Harry	k1gMnSc2	Harry
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
poblíž	poblíž	k6eAd1	poblíž
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
hrob	hrob	k1gInSc1	hrob
kouzelníka	kouzelník	k1gMnSc2	kouzelník
Ignota	Ignot	k1gMnSc2	Ignot
Peverella	Peverell	k1gMnSc2	Peverell
s	s	k7c7	s
tajemným	tajemný	k2eAgInSc7d1	tajemný
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Hermiona	Hermiona	k1gFnSc1	Hermiona
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
bajek	bajka	k1gFnPc2	bajka
od	od	k7c2	od
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
pohádal	pohádat	k5eAaPmAgMnS	pohádat
Viktor	Viktor	k1gMnSc1	Viktor
Krum	Krum	k1gMnSc1	Krum
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
Lenky	Lenka	k1gFnSc2	Lenka
Láskorádové	Láskorádový	k2eAgFnSc2d1	Láskorádová
Xenofiliusem	Xenofilius	k1gInSc7	Xenofilius
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
taky	taky	k9	taky
pomník	pomník	k1gInSc4	pomník
slavného	slavný	k2eAgNnSc2d1	slavné
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
Voldemortem	Voldemort	k1gInSc7	Voldemort
a	a	k8xC	a
rozbořený	rozbořený	k2eAgInSc1d1	rozbořený
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Voldemort	Voldemort	k1gInSc1	Voldemort
zavraždil	zavraždit	k5eAaPmAgInS	zavraždit
jeho	jeho	k3xOp3gMnPc4	jeho
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potkávají	potkávat	k5eAaImIp3nP	potkávat
starou	starý	k2eAgFnSc4d1	stará
čarodějku	čarodějka	k1gFnSc4	čarodějka
Batyldu	Batyld	k1gInSc2	Batyld
Bagshotovou	Bagshotův	k2eAgFnSc7d1	Bagshotův
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
si	se	k3xPyFc3	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
rozhovor	rozhovor	k1gInSc4	rozhovor
ze	z	k7c2	z
svatby	svatba	k1gFnSc2	svatba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ronova	Ronův	k2eAgFnSc1d1	Ronova
teta	teta	k1gFnSc1	teta
Muriel	Muriela	k1gFnPc2	Muriela
pomlouvala	pomlouvat	k5eAaImAgFnS	pomlouvat
Brumbála	brumbál	k1gMnSc4	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovala	zmiňovat	k5eAaImAgFnS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
Batyldě	Batylda	k1gFnSc6	Batylda
jako	jako	k8xC	jako
o	o	k7c6	o
rodinné	rodinný	k2eAgFnSc6d1	rodinná
přítelkyni	přítelkyně	k1gFnSc6	přítelkyně
Brumbálových	brumbálův	k2eAgFnPc2d1	Brumbálova
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vzbudí	vzbudit	k5eAaPmIp3nP	vzbudit
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
otupí	otupit	k5eAaPmIp3nS	otupit
jeho	jeho	k3xOp3gFnSc4	jeho
obezřetnost	obezřetnost	k1gFnSc4	obezřetnost
<g/>
.	.	kIx.	.
</s>
<s>
Batylda	Batylda	k1gFnSc1	Batylda
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
podkroví	podkroví	k1gNnSc2	podkroví
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzápětí	vzápětí	k6eAd1	vzápětí
změní	změnit	k5eAaPmIp3nS	změnit
ve	v	k7c4	v
Voldemortova	Voldemortův	k2eAgMnSc4d1	Voldemortův
hada	had	k1gMnSc4	had
Naginiho	Nagini	k1gMnSc4	Nagini
a	a	k8xC	a
napadne	napadnout	k5eAaPmIp3nS	napadnout
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zásahu	zásah	k1gInSc3	zásah
Hermiony	Hermion	k1gInPc7	Hermion
oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
Voldemortovi	Voldemortův	k2eAgMnPc1d1	Voldemortův
uniknou	uniknout	k5eAaPmIp3nP	uniknout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
ale	ale	k9	ale
zlomí	zlomit	k5eAaPmIp3nS	zlomit
Harryho	Harry	k1gMnSc4	Harry
kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
hůlka	hůlka	k1gFnSc1	hůlka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
jisté	jistý	k2eAgNnSc1d1	jisté
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
svou	svůj	k3xOyFgFnSc7	svůj
a	a	k8xC	a
Voldemortovou	Voldemortový	k2eAgFnSc7d1	Voldemortový
myslí	mysl	k1gFnSc7	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
vidění	vidění	k1gNnSc3	vidění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Voldemortem	Voldemort	k1gInSc7	Voldemort
a	a	k8xC	a
vraždí	vraždit	k5eAaImIp3nP	vraždit
další	další	k2eAgMnPc4d1	další
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
kouzelníky	kouzelník	k1gMnPc4	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Cítí	cítit	k5eAaImIp3nP	cítit
nesnesitelné	snesitelný	k2eNgFnPc1d1	nesnesitelná
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
vycítí	vycítit	k5eAaPmIp3nS	vycítit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pán	pán	k1gMnSc1	pán
zla	zlo	k1gNnSc2	zlo
rozzuřený	rozzuřený	k2eAgInSc1d1	rozzuřený
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
dále	daleko	k6eAd2	daleko
stanují	stanovat	k5eAaImIp3nP	stanovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
lesích	les	k1gInPc6	les
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
přesunují	přesunovat	k5eAaImIp3nP	přesunovat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
jednou	jednou	k6eAd1	jednou
při	při	k7c6	při
hlídkování	hlídkování	k1gNnSc6	hlídkování
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Patrona	patrona	k1gFnSc1	patrona
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
laně	laň	k1gFnSc2	laň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gNnSc4	on
zavede	zavést	k5eAaPmIp3nS	zavést
k	k	k7c3	k
zamrzlému	zamrzlý	k2eAgNnSc3d1	zamrzlé
jezírku	jezírko	k1gNnSc3	jezírko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
dně	dno	k1gNnSc6	dno
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
artefaktů	artefakt	k1gInPc2	artefakt
schopných	schopný	k2eAgMnPc2d1	schopný
zničit	zničit	k5eAaPmF	zničit
viteál	viteál	k1gInSc4	viteál
–	–	k?	–
medailon	medailon	k1gInSc1	medailon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
Harryho	Harry	k1gMnSc2	Harry
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
meč	meč	k1gInSc4	meč
z	z	k7c2	z
jezírka	jezírko	k1gNnSc2	jezírko
málem	málem	k6eAd1	málem
uškrtí	uškrtit	k5eAaPmIp3nS	uškrtit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
brzo	brzo	k6eAd1	brzo
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
okamžiku	okamžik	k1gInSc6	okamžik
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
Harry	Harr	k1gInPc4	Harr
zachráněn	zachránit	k5eAaPmNgInS	zachránit
Ronem	Ron	k1gMnSc7	Ron
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zčistajasna	zčistajasna	k6eAd1	zčistajasna
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
Rona	Ron	k1gMnSc2	Ron
přiměje	přimět	k5eAaPmIp3nS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viteál	viteál	k1gInSc4	viteál
zničil	zničit	k5eAaPmAgMnS	zničit
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zdráhá	zdráhat	k5eAaImIp3nS	zdráhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Medailon	medailon	k1gInSc1	medailon
se	se	k3xPyFc4	se
kouzlem	kouzlo	k1gNnSc7	kouzlo
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Vyčaruje	vyčarovat	k5eAaPmIp3nS	vyčarovat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
Ron	ron	k1gInSc4	ron
bojí	bát	k5eAaImIp3nS	bát
–	–	k?	–
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
milovanou	milovaný	k2eAgFnSc4d1	milovaná
Hermionu	Hermion	k1gInSc6	Hermion
v	v	k7c6	v
objetí	objetí	k1gNnSc6	objetí
Harryho	Harry	k1gMnSc2	Harry
<g/>
,	,	kIx,	,
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnPc1	rodič
nechtěli	chtít	k5eNaImAgMnP	chtít
a	a	k8xC	a
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
mečem	meč	k1gInSc7	meč
medailon	medailon	k1gInSc1	medailon
rozsekne	rozseknout	k5eAaPmIp3nS	rozseknout
a	a	k8xC	a
nadobro	nadobro	k6eAd1	nadobro
jej	on	k3xPp3gMnSc4	on
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Relikvie	relikvie	k1gFnSc2	relikvie
smrti	smrt	k1gFnSc2	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Opět	opět	k6eAd1	opět
sjednocené	sjednocený	k2eAgNnSc1d1	sjednocené
trio	trio	k1gNnSc1	trio
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
navštívit	navštívit	k5eAaPmF	navštívit
otce	otka	k1gFnSc3	otka
Lenky	Lenka	k1gFnSc2	Lenka
Láskorádové	Láskorádový	k2eAgFnSc2d1	Láskorádová
Xenophiliuse	Xenophiliuse	k1gFnSc2	Xenophiliuse
Láskoráda	Láskoráda	k1gFnSc1	Láskoráda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
víc	hodně	k6eAd2	hodně
o	o	k7c6	o
symbolu	symbol	k1gInSc6	symbol
na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
symbol	symbol	k1gInSc4	symbol
Relikvií	relikvie	k1gFnPc2	relikvie
smrti	smrt	k1gFnSc2	smrt
–	–	k?	–
neporazitelné	porazitelný	k2eNgFnSc2d1	neporazitelná
kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
schopného	schopný	k2eAgMnSc2d1	schopný
vzkřísit	vzkřísit	k5eAaPmF	vzkřísit
mrtvé	mrtvý	k2eAgInPc4d1	mrtvý
<g/>
,	,	kIx,	,
a	a	k8xC	a
neviditelného	viditelný	k2eNgInSc2d1	neviditelný
pláště	plášť	k1gInSc2	plášť
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
Harry	Harra	k1gFnSc2	Harra
již	již	k9	již
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc4	jejich
rozhovor	rozhovor	k1gInSc4	rozhovor
ukončí	ukončit	k5eAaPmIp3nP	ukončit
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
Xenofilius	Xenofilius	k1gInSc1	Xenofilius
Láskorád	Láskoráda	k1gFnPc2	Láskoráda
přivolal	přivolat	k5eAaPmAgInS	přivolat
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
dopadení	dopadení	k1gNnSc6	dopadení
Harryho	Harry	k1gMnSc2	Harry
vrátí	vrátit	k5eAaPmIp3nS	vrátit
Lenku	Lenka	k1gFnSc4	Lenka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
unesli	unést	k5eAaPmAgMnP	unést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Triu	trio	k1gNnSc3	trio
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podaří	podařit	k5eAaPmIp3nS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
Harryho	Harry	k1gMnSc2	Harry
nepozorností	nepozornost	k1gFnPc2	nepozornost
jsou	být	k5eAaImIp3nP	být
brzy	brzy	k6eAd1	brzy
potom	potom	k6eAd1	potom
zajati	zajat	k2eAgMnPc1d1	zajat
a	a	k8xC	a
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
v	v	k7c4	v
Malfoy	Malfoy	k1gInPc4	Malfoy
Manor	Manora	k1gFnPc2	Manora
<g/>
,	,	kIx,	,
domě	dům	k1gInSc6	dům
Malfoyů	Malfoy	k1gMnPc2	Malfoy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
sklepní	sklepní	k2eAgFnSc6d1	sklepní
kobce	kobka	k1gFnSc6	kobka
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
Lenkou	Lenka	k1gFnSc7	Lenka
Láskorádovou	Láskorádový	k2eAgFnSc7d1	Láskorádová
<g/>
,	,	kIx,	,
výrobcem	výrobce	k1gMnSc7	výrobce
hůlek	hůlka	k1gFnPc2	hůlka
Ollivanderem	Ollivander	k1gMnSc7	Ollivander
a	a	k8xC	a
skřetem	skřet	k1gMnSc7	skřet
Griphookem	Griphook	k1gMnSc7	Griphook
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
uniknout	uniknout	k5eAaPmF	uniknout
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
skřítka	skřítek	k1gMnSc2	skřítek
Dobbyho	Dobby	k1gMnSc2	Dobby
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zabit	zabit	k2eAgInSc4d1	zabit
Bellatrix	Bellatrix	k1gInSc4	Bellatrix
Lestrangeovou	Lestrangeův	k2eAgFnSc7d1	Lestrangeův
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Dracem	Drace	k1gMnSc7	Drace
Malfoyem	Malfoy	k1gMnSc7	Malfoy
získat	získat	k5eAaPmF	získat
jeho	jeho	k3xOp3gFnSc4	jeho
hůlku	hůlka	k1gFnSc4	hůlka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
najdou	najít	k5eAaPmIp3nP	najít
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
Lasturové	lasturový	k2eAgFnSc6d1	lasturová
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
domě	dům	k1gInSc6	dům
patřícím	patřící	k2eAgInSc6d1	patřící
Billovi	Bill	k1gMnSc3	Bill
a	a	k8xC	a
Fleur	Fleura	k1gFnPc2	Fleura
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
Ollivandera	Ollivandero	k1gNnSc2	Ollivandero
<g/>
,	,	kIx,	,
že	že	k8xS	že
neporazitelná	porazitelný	k2eNgFnSc1d1	neporazitelná
hůlka	hůlka	k1gFnSc1	hůlka
skutečně	skutečně	k6eAd1	skutečně
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
Voldemort	Voldemort	k1gInSc4	Voldemort
pase	pase	k1gFnPc1	pase
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
pojistit	pojistit	k5eAaPmF	pojistit
si	se	k3xPyFc3	se
Harryho	Harry	k1gMnSc4	Harry
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
posledním	poslední	k2eAgMnSc7d1	poslední
majitelem	majitel	k1gMnSc7	majitel
hůlky	hůlka	k1gFnSc2	hůlka
byl	být	k5eAaImAgMnS	být
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
dát	dát	k5eAaPmF	dát
před	před	k7c7	před
hůlkou	hůlka	k1gFnSc7	hůlka
přednost	přednost	k1gFnSc4	přednost
hledání	hledání	k1gNnSc2	hledání
viteálů	viteál	k1gInPc2	viteál
<g/>
,	,	kIx,	,
a	a	k8xC	a
Voldemort	Voldemort	k1gInSc4	Voldemort
hůlku	hůlka	k1gFnSc4	hůlka
úspěšně	úspěšně	k6eAd1	úspěšně
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
z	z	k7c2	z
Brumbálova	brumbálův	k2eAgInSc2d1	brumbálův
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
s	s	k7c7	s
Griphookem	Griphook	k1gInSc7	Griphook
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Nebelvírův	Nebelvírův	k2eAgInSc4d1	Nebelvírův
meč	meč	k1gInSc4	meč
pomůže	pomoct	k5eAaPmIp3nS	pomoct
vloupat	vloupat	k5eAaPmF	vloupat
se	se	k3xPyFc4	se
do	do	k7c2	do
banky	banka	k1gFnSc2	banka
Gringottových	Gringottových	k2eAgFnSc2d1	Gringottových
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
Lestrangeových	Lestrangeův	k2eAgInPc2d1	Lestrangeův
další	další	k2eAgInSc4d1	další
Voldemortův	Voldemortův	k2eAgInSc4d1	Voldemortův
viteál	viteál	k1gInSc4	viteál
–	–	k?	–
pohár	pohár	k1gInSc1	pohár
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
banky	banka	k1gFnSc2	banka
utíkají	utíkat	k5eAaImIp3nP	utíkat
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
draka	drak	k1gMnSc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
konečně	konečně	k6eAd1	konečně
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
viteály	viteála	k1gFnPc1	viteála
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
jejich	jejich	k3xOp3gNnSc4	jejich
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
vše	všechen	k3xTgNnSc1	všechen
vidí	vidět	k5eAaImIp3nS	vidět
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc1d1	další
viteál	viteál	k1gInSc1	viteál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bradavice	bradavice	k1gFnSc2	bradavice
===	===	k?	===
</s>
</p>
<p>
<s>
Trojice	trojice	k1gFnSc1	trojice
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
s	s	k7c7	s
Aberforthem	Aberforth	k1gInSc7	Aberforth
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
bratrem	bratr	k1gMnSc7	bratr
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jim	on	k3xPp3gMnPc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Brumbálově	brumbálův	k2eAgFnSc6d1	Brumbálova
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
černokněžníkem	černokněžník	k1gMnSc7	černokněžník
Grindewaldem	Grindewald	k1gMnSc7	Grindewald
a	a	k8xC	a
smrti	smrt	k1gFnSc3	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
"	"	kIx"	"
<g/>
Brumbálovy	brumbálův	k2eAgFnSc2d1	Brumbálova
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
a	a	k8xC	a
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dalším	další	k2eAgInSc7d1	další
viteálem	viteál	k1gInSc7	viteál
je	být	k5eAaImIp3nS	být
diadém	diadém	k1gInSc1	diadém
(	(	kIx(	(
<g/>
zdobená	zdobený	k2eAgFnSc1d1	zdobená
čelenka	čelenka	k1gFnSc1	čelenka
<g/>
)	)	kIx)	)
Roweny	Rowena	k1gFnPc1	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
zubů	zub	k1gInPc2	zub
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
baziliška	bazilišek	k1gMnSc2	bazilišek
zničí	zničit	k5eAaPmIp3nS	zničit
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
políbí	políbit	k5eAaPmIp3nP	políbit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
vydají	vydat	k5eAaPmIp3nP	vydat
do	do	k7c2	do
Komnaty	komnata	k1gFnSc2	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Voldemort	Voldemort	k1gInSc1	Voldemort
v	v	k7c6	v
hromadě	hromada	k1gFnSc6	hromada
schoval	schovat	k5eAaPmAgMnS	schovat
Roweninu	Rowenin	k2eAgFnSc4d1	Rowenin
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komnatě	komnata	k1gFnSc6	komnata
se	se	k3xPyFc4	se
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
Malfoyem	Malfoy	k1gMnSc7	Malfoy
<g/>
,	,	kIx,	,
Crabbem	Crabb	k1gMnSc7	Crabb
a	a	k8xC	a
Goylem	Goyl	k1gMnSc7	Goyl
<g/>
;	;	kIx,	;
Crabbe	Crabb	k1gInSc5	Crabb
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
sešle	sešle	k6eAd1	sešle
pomocí	pomoc	k1gFnSc7	pomoc
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
kouzlo	kouzlo	k1gNnSc1	kouzlo
Zložár	Zložár	k1gInSc1	Zložár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zničí	zničit	k5eAaPmIp3nS	zničit
vše	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
včetně	včetně	k7c2	včetně
viteálu	viteál	k1gInSc2	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
poté	poté	k6eAd1	poté
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
ostatním	ostatní	k2eAgMnPc3d1	ostatní
<g/>
,	,	kIx,	,
že	že	k8xS	že
viteály	viteála	k1gFnSc2	viteála
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
ničit	ničit	k5eAaImF	ničit
i	i	k9	i
černou	černý	k2eAgFnSc7d1	černá
magií	magie	k1gFnSc7	magie
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
neodvážila	odvážit	k5eNaPmAgFnS	odvážit
<g/>
.	.	kIx.	.
</s>
<s>
Trio	trio	k1gNnSc1	trio
unikne	uniknout	k5eAaPmIp3nS	uniknout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Malfoyem	Malfoy	k1gMnSc7	Malfoy
a	a	k8xC	a
Goylem	Goyl	k1gMnSc7	Goyl
<g/>
,	,	kIx,	,
Crabbe	Crabb	k1gInSc5	Crabb
padne	padnout	k5eAaImIp3nS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
ohni	oheň	k1gInSc3	oheň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
zároveň	zároveň	k6eAd1	zároveň
umírá	umírat	k5eAaImIp3nS	umírat
Fred	Fred	k1gMnSc1	Fred
Weasley	Weaslea	k1gFnSc2	Weaslea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harry	Harr	k1gInPc1	Harr
vycítí	vycítit	k5eAaPmIp3nP	vycítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
je	být	k5eAaImIp3nS	být
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
hadem	had	k1gMnSc7	had
v	v	k7c6	v
Chroptící	chroptící	k2eAgFnSc6d1	chroptící
chýši	chýš	k1gFnSc6	chýš
<g/>
.	.	kIx.	.
</s>
<s>
Proplíží	proplížit	k5eAaPmIp3nS	proplížit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tajnou	tajný	k2eAgFnSc7d1	tajná
chodbou	chodba	k1gFnSc7	chodba
právě	právě	k6eAd1	právě
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyslechl	vyslechnout	k5eAaPmAgMnS	vyslechnout
jeho	on	k3xPp3gInSc4	on
rozhovor	rozhovor	k1gInSc4	rozhovor
se	s	k7c7	s
Snapem	Snap	k1gInSc7	Snap
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
Voldemort	Voldemort	k1gInSc1	Voldemort
zabije	zabít	k5eAaPmIp3nS	zabít
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
neporazitelná	porazitelný	k2eNgFnSc1d1	neporazitelná
bezová	bezový	k2eAgFnSc1d1	Bezová
hůlka	hůlka	k1gFnSc1	hůlka
skutečně	skutečně	k6eAd1	skutečně
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
až	až	k9	až
dosud	dosud	k6eAd1	dosud
mu	on	k3xPp3gMnSc3	on
nepředvedla	předvést	k5eNaPmAgFnS	předvést
svou	svůj	k3xOyFgFnSc4	svůj
plnou	plný	k2eAgFnSc4d1	plná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Umírající	umírající	k2eAgInSc1d1	umírající
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
předá	předat	k5eAaPmIp3nS	předat
Harrymu	Harrym	k1gInSc3	Harrym
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zabití	zabití	k1gNnSc1	zabití
Brumbála	brumbál	k1gMnSc4	brumbál
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
viteálů	viteál	k1gInPc2	viteál
zraněn	zranit	k5eAaPmNgInS	zranit
černou	černý	k2eAgFnSc7d1	černá
magií	magie	k1gFnSc7	magie
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
silného	silný	k2eAgNnSc2d1	silné
kouzla	kouzlo	k1gNnSc2	kouzlo
navlékl	navléknout	k5eAaPmAgInS	navléknout
Voldemortův	Voldemortův	k2eAgInSc1d1	Voldemortův
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
pomalu	pomalu	k6eAd1	pomalu
umíral	umírat	k5eAaImAgMnS	umírat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nikdo	nikdo	k3yNnSc1	nikdo
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
a	a	k8xC	a
Snapea	Snapea	k1gMnSc1	Snapea
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
blízká	blízký	k2eAgFnSc1d1	blízká
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
nevyhnutelná	vyhnutelný	k2eNgFnSc1d1	nevyhnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgInS	využít
proto	proto	k8xC	proto
se	s	k7c7	s
Snapem	Snap	k1gInSc7	Snap
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc1	fakt
a	a	k8xC	a
sehráli	sehrát	k5eAaPmAgMnP	sehrát
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgNnSc6	který
nikdo	nikdo	k3yNnSc1	nikdo
nepochyboval	pochybovat	k5eNaImAgMnS	pochybovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
je	on	k3xPp3gInPc4	on
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
i	i	k9	i
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
Snapem	Snap	k1gInSc7	Snap
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
Lilly	Lilla	k1gFnSc2	Lilla
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
<g/>
,	,	kIx,	,
prosil	prosít	k5eAaPmAgMnS	prosít
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
vraždě	vražda	k1gFnSc6	vražda
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
při	při	k7c6	při
Voldemortově	Voldemortův	k2eAgInSc6d1	Voldemortův
vražedném	vražedný	k2eAgInSc6d1	vražedný
pokusu	pokus	k1gInSc6	pokus
přešla	přejít	k5eAaPmAgFnS	přejít
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
neplánovaným	plánovaný	k2eNgMnSc7d1	neplánovaný
viteálem	viteál	k1gMnSc7	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
Voldemorta	Voldemorta	k1gFnSc1	Voldemorta
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
zničit	zničit	k5eAaPmF	zničit
i	i	k9	i
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
odevzdaně	odevzdaně	k6eAd1	odevzdaně
odchází	odcházet	k5eAaImIp3nP	odcházet
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
a	a	k8xC	a
v	v	k7c6	v
dědictví	dědictví	k1gNnSc6	dědictví
od	od	k7c2	od
Brumbála	brumbál	k1gMnSc2	brumbál
objevuje	objevovat	k5eAaImIp3nS	objevovat
poslední	poslední	k2eAgFnSc4d1	poslední
Relikvii	relikvie	k1gFnSc4	relikvie
smrti	smrt	k1gFnSc2	smrt
–	–	k?	–
kámen	kámen	k1gInSc1	kámen
schopný	schopný	k2eAgInSc1d1	schopný
přivolat	přivolat	k5eAaPmF	přivolat
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
Siriuse	Siriuse	k1gFnSc1	Siriuse
Blacka	Blacka	k1gFnSc1	Blacka
a	a	k8xC	a
Lupina	lupina	k1gFnSc1	lupina
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
zahynul	zahynout	k5eAaPmAgMnS	zahynout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tonksovou	Tonksová	k1gFnSc7	Tonksová
v	v	k7c6	v
předcházející	předcházející	k2eAgFnSc6d1	předcházející
bitvě	bitva	k1gFnSc6	bitva
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
dodají	dodat	k5eAaPmIp3nP	dodat
odvahu	odvaha	k1gFnSc4	odvaha
se	se	k3xPyFc4	se
beze	beze	k7c2	beze
zbraně	zbraň	k1gFnSc2	zbraň
střetnout	střetnout	k5eAaPmF	střetnout
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Voldemortově	Voldemortův	k2eAgFnSc6d1	Voldemortova
smrtící	smrtící	k2eAgFnSc6d1	smrtící
kletbě	kletba	k1gFnSc6	kletba
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
probouzí	probouzet	k5eAaImIp3nP	probouzet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
podobném	podobný	k2eAgInSc6d1	podobný
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
svým	svůj	k3xOyFgNnSc7	svůj
kouzlem	kouzlo	k1gNnSc7	kouzlo
zabil	zabít	k5eAaPmAgInS	zabít
jen	jen	k6eAd1	jen
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
duše	duše	k1gFnSc2	duše
v	v	k7c4	v
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokončit	dokončit	k5eAaPmF	dokončit
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
probouzí	probouzet	k5eAaImIp3nS	probouzet
<g/>
,	,	kIx,	,
a	a	k8xC	a
předstíraje	předstírat	k5eAaImSgMnS	předstírat
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vynesen	vynesen	k2eAgMnSc1d1	vynesen
Smrtijedy	Smrtijed	k1gMnPc4	Smrtijed
před	před	k7c4	před
Bradavice	bradavice	k1gFnPc4	bradavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obránci	obránce	k1gMnPc1	obránce
Bradavic	bradavice	k1gFnPc2	bradavice
jsou	být	k5eAaImIp3nP	být
zdrceni	zdrcen	k2eAgMnPc1d1	zdrcen
<g/>
,	,	kIx,	,
Neville	Neville	k1gFnSc1	Neville
se	se	k3xPyFc4	se
ale	ale	k9	ale
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
postaví	postavit	k5eAaPmIp3nS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
potupit	potupit	k5eAaPmF	potupit
a	a	k8xC	a
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
mu	on	k3xPp3gMnSc3	on
zapálený	zapálený	k2eAgInSc1d1	zapálený
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
ovšem	ovšem	k9	ovšem
Neville	Neville	k1gNnSc2	Neville
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Harry	Harra	k1gFnPc4	Harra
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dílu	díl	k1gInSc6	díl
<g/>
)	)	kIx)	)
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
Nebelvírův	Nebelvírův	k2eAgInSc1d1	Nebelvírův
meč	meč	k1gInSc1	meč
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
Voldemortova	Voldemortův	k2eAgMnSc4d1	Voldemortův
hada	had	k1gMnSc4	had
–	–	k?	–
poslední	poslední	k2eAgInSc4d1	poslední
viteál	viteál	k1gInSc4	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozproudí	rozproudit	k5eAaPmIp3nS	rozproudit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
naposledy	naposledy	k6eAd1	naposledy
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
Voldemorta	Voldemorta	k1gFnSc1	Voldemorta
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
Brumbála	brumbál	k1gMnSc4	brumbál
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neporazil	porazit	k5eNaPmAgMnS	porazit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gNnSc1	jeho
zabití	zabití	k1gNnSc1	zabití
bylo	být	k5eAaImAgNnS	být
dohodnuté	dohodnutý	k2eAgNnSc1d1	dohodnuté
<g/>
,	,	kIx,	,
a	a	k8xC	a
Voldemort	Voldemort	k1gInSc1	Voldemort
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
skutečným	skutečný	k2eAgMnSc7d1	skutečný
pánem	pán	k1gMnSc7	pán
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
pokusí	pokusit	k5eAaPmIp3nP	pokusit
Harryho	Harry	k1gMnSc4	Harry
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
smrtící	smrtící	k2eAgNnSc4d1	smrtící
kouzlo	kouzlo	k1gNnSc4	kouzlo
se	se	k3xPyFc4	se
ale	ale	k9	ale
obrátí	obrátit	k5eAaPmIp3nS	obrátit
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
příběhu	příběh	k1gInSc2	příběh
vypravují	vypravovat	k5eAaImIp3nP	vypravovat
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Ginny	Ginna	k1gFnPc1	Ginna
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
své	svůj	k3xOyFgFnPc4	svůj
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Jamese	Jamese	k1gFnPc4	Jamese
<g/>
,	,	kIx,	,
Albuse	Albuse	k1gFnPc4	Albuse
Severuse	Severuse	k1gFnSc2	Severuse
a	a	k8xC	a
Lilly	Lilla	k1gFnSc2	Lilla
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
a	a	k8xC	a
Hermiona	Hermion	k1gMnSc4	Hermion
mají	mít	k5eAaImIp3nP	mít
děti	dítě	k1gFnPc1	dítě
dvě	dva	k4xCgFnPc4	dva
<g/>
:	:	kIx,	:
Rose	Ros	k1gMnSc4	Ros
a	a	k8xC	a
Huga	Hugo	k1gMnSc4	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Teddymu	Teddym	k1gInSc3	Teddym
Lupinovi	Lupin	k1gMnSc6	Lupin
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
let	léto	k1gNnPc2	léto
a	a	k8xC	a
již	jenž	k3xRgFnSc4	jenž
školu	škola	k1gFnSc4	škola
vyšel	vyjít	k5eAaPmAgInS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
se	se	k3xPyFc4	se
v	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
dítětem	dítě	k1gNnSc7	dítě
(	(	kIx(	(
<g/>
Scorpius	Scorpius	k1gInSc1	Scorpius
Malfoy	Malfoa	k1gFnSc2	Malfoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neville	Neville	k1gFnSc1	Neville
Longbottom	Longbottom	k1gInSc1	Longbottom
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plány	plán	k1gInPc7	plán
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vehementně	vehementně	k6eAd1	vehementně
bránila	bránit	k5eAaImAgFnS	bránit
dalšímu	další	k2eAgNnSc3d1	další
pokračování	pokračování	k1gNnSc3	pokračování
<g/>
,	,	kIx,	,
letos	letos	k6eAd1	letos
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sedmý	sedmý	k4xOgInSc1	sedmý
díl	díl	k1gInSc1	díl
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
posledním	poslední	k2eAgMnPc3d1	poslední
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
po	po	k7c6	po
vyřčení	vyřčení	k1gNnSc6	vyřčení
tento	tento	k3xDgInSc1	tento
nápad	nápad	k1gInSc4	nápad
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc4	důvod
nakonec	nakonec	k6eAd1	nakonec
podala	podat	k5eAaPmAgFnS	podat
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harr	k1gInPc4	Harr
Potter	Potter	k1gInSc1	Potter
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
nechat	nechat	k5eAaPmF	nechat
jen	jen	k9	jen
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
také	také	k9	také
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánuje	plánovat	k5eAaImIp3nS	plánovat
napsat	napsat	k5eAaPmF	napsat
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
postavách	postava	k1gFnPc6	postava
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
o	o	k7c6	o
použitých	použitý	k2eAgNnPc6d1	Použité
kouzlech	kouzlo	k1gNnPc6	kouzlo
a	a	k8xC	a
místních	místní	k2eAgInPc6d1	místní
jménech	jméno	k1gNnPc6	jméno
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
osmý	osmý	k4xOgInSc4	osmý
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
autorčiných	autorčin	k2eAgFnPc2d1	autorčina
a	a	k8xC	a
Harryho	Harry	k1gMnSc2	Harry
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
the	the	k?	the
cursed	cursed	k1gMnSc1	cursed
child	child	k1gMnSc1	child
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
prokleté	prokletý	k2eAgNnSc1d1	prokleté
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
o	o	k7c6	o
dětech	dítě	k1gFnPc6	dítě
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dospělým	dospělý	k2eAgMnSc7d1	dospělý
a	a	k8xC	a
skvělým	skvělý	k2eAgMnPc3d1	skvělý
vedoucím	vedoucí	k1gMnPc3	vedoucí
oddělení	oddělení	k1gNnSc4	oddělení
bystrozorů	bystrozor	k1gInPc2	bystrozor
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c4	na
prkna	prkno	k1gNnPc4	prkno
Brodwaye	Brodway	k1gFnSc2	Brodway
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
