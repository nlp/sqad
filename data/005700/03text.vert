<s>
Tóra	tóra	k1gFnSc1	tóra
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ת	ת	k?	ת
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
Zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc1	učení
a	a	k8xC	a
v	v	k7c6	v
nejužším	úzký	k2eAgInSc6d3	nejužší
smyslu	smysl	k1gInSc6	smysl
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
TaNaChu	TaNaCh	k1gInSc2	TaNaCh
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgNnPc2	tento
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
Pentateuch	pentateuch	k1gInSc1	pentateuch
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chamiša	Chamiša	k1gMnSc1	Chamiša
chumšej	chumšet	k5eAaPmRp2nS	chumšet
Tora	Tor	k1gMnSc2	Tor
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
"	"	kIx"	"
<g/>
Pět	pět	k4xCc1	pět
pětin	pětina	k1gFnPc2	pětina
Tóry	tóra	k1gFnSc2	tóra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
Pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zavádějící	zavádějící	k2eAgInSc4d1	zavádějící
název	název	k1gInSc4	název
pocházející	pocházející	k2eAgInPc4d1	pocházející
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tóra	tóra	k1gFnSc1	tóra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
ב	ב	k?	ב
Be-rešit	Beešit	k1gFnSc1	Be-rešit
–	–	k?	–
Genesis	Genesis	k1gFnSc1	Genesis
ש	ש	k?	ש
Šemot	Šemot	k1gInSc1	Šemot
–	–	k?	–
Exodus	Exodus	k1gInSc1	Exodus
ו	ו	k?	ו
Va-jikra	Vaikr	k1gInSc2	Va-jikr
–	–	k?	–
Leviticus	Leviticus	k1gMnSc1	Leviticus
ב	ב	k?	ב
Ba-midbar	Baidbar	k1gMnSc1	Ba-midbar
–	–	k?	–
Numeri	Numer	k1gFnSc2	Numer
ד	ד	k?	ד
Devarim	Devarim	k1gInSc1	Devarim
–	–	k?	–
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
pak	pak	k6eAd1	pak
slovo	slovo	k1gNnSc4	slovo
Tóra	tóra	k1gFnSc1	tóra
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nejen	nejen	k6eAd1	nejen
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
Tanachu	Tanach	k1gInSc2	Tanach
a	a	k8xC	a
Ústní	ústní	k2eAgInSc1d1	ústní
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgFnSc1d1	tradiční
interpretace	interpretace	k1gFnSc1	interpretace
textu	text	k1gInSc2	text
Tóry	tóra	k1gFnSc2	tóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tóra	tóra	k1gFnSc1	tóra
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Pentateuchu	pentateuch	k1gInSc2	pentateuch
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
dokumentem	dokument	k1gInSc7	dokument
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
samotné	samotný	k2eAgNnSc1d1	samotné
má	mít	k5eAaImIp3nS	mít
kořen	kořen	k1gInSc4	kořen
v	v	k7c6	v
hebrejském	hebrejský	k2eAgInSc6d1	hebrejský
י	י	k?	י
"	"	kIx"	"
<g/>
jara	jaro	k1gNnSc2	jaro
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
učit	učit	k5eAaImF	učit
či	či	k8xC	či
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tóře	tóra	k1gFnSc6	tóra
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
nejen	nejen	k6eAd1	nejen
univerzální	univerzální	k2eAgInPc1d1	univerzální
příběhy	příběh	k1gInPc1	příběh
Adama	Adam	k1gMnSc2	Adam
<g/>
,	,	kIx,	,
Noeho	Noe	k1gMnSc2	Noe
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc4	příběh
židovských	židovský	k2eAgMnPc2d1	židovský
praotců	praotec	k1gMnPc2	praotec
Avrahama	Avrahama	k1gFnSc1	Avrahama
(	(	kIx(	(
<g/>
Abrahám	Abrahám	k1gMnSc1	Abrahám
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jicchaka	Jicchak	k1gMnSc2	Jicchak
(	(	kIx(	(
<g/>
Izák	Izák	k1gMnSc1	Izák
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
akova	akov	k1gMnSc2	akov
(	(	kIx(	(
<g/>
Jákob	Jákob	k1gMnSc1	Jákob
<g/>
,	,	kIx,	,
též	též	k9	též
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc7d1	další
jménem	jméno	k1gNnSc7	jméno
Jisra	Jisro	k1gNnSc2	Jisro
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proroka	prorok	k1gMnSc4	prorok
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
613	[number]	k4	613
micvot	micvota	k1gFnPc2	micvota
(	(	kIx(	(
<g/>
židovských	židovský	k2eAgNnPc2d1	Židovské
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
právotvorný	právotvorný	k2eAgInSc4d1	právotvorný
<g/>
.	.	kIx.	.
</s>
<s>
Tóra	tóra	k1gFnSc1	tóra
tak	tak	k6eAd1	tak
dala	dát	k5eAaPmAgFnS	dát
základ	základ	k1gInSc4	základ
celému	celý	k2eAgNnSc3d1	celé
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
židovskému	židovský	k2eAgNnSc3d1	Židovské
právu	právo	k1gNnSc3	právo
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ה	ה	k?	ה
halacha	halacha	k1gMnSc1	halacha
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
Pentateuchu	pentateuch	k1gInSc2	pentateuch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
židovské	židovský	k2eAgFnSc2d1	židovská
tradice	tradice	k1gFnSc2	tradice
byly	být	k5eAaImAgFnP	být
knihy	kniha	k1gFnPc1	kniha
Tóry	tóra	k1gFnSc2	tóra
věnovány	věnovat	k5eAaImNgFnP	věnovat
Bohem	bůh	k1gMnSc7	bůh
Mojžíšovi	Mojžíš	k1gMnSc6	Mojžíš
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1220	[number]	k4	1220
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
exodu	exodus	k1gInSc6	exodus
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
věnování	věnování	k1gNnSc3	věnování
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
v	v	k7c6	v
období	období	k1gNnSc6	období
raného	raný	k2eAgInSc2d1	raný
rabínského	rabínský	k2eAgInSc2d1	rabínský
anebo	anebo	k8xC	anebo
tanaitského	tanaitský	k2eAgInSc2d1	tanaitský
judaismu	judaismus	k1gInSc2	judaismus
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
počátek	počátek	k1gInSc4	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
považovala	považovat	k5eAaImAgFnS	považovat
věnování	věnování	k1gNnSc4	věnování
Tóry	tóra	k1gFnSc2	tóra
za	za	k7c4	za
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Sinaj	Sinaj	k1gInSc1	Sinaj
obdržel	obdržet	k5eAaPmAgInS	obdržet
celou	celý	k2eAgFnSc4d1	celá
Tóru	tóra	k1gFnSc4	tóra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jediném	jediný	k2eAgInSc6d1	jediný
okamžiku	okamžik	k1gInSc6	okamžik
byla	být	k5eAaImAgFnS	být
zjevena	zjeven	k2eAgFnSc1d1	zjevena
celá	celý	k2eAgFnSc1d1	celá
historie	historie	k1gFnSc1	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
Stvoření	stvoření	k1gNnSc1	stvoření
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
verzí	verze	k1gFnPc2	verze
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
na	na	k7c6	na
Sinaji	Sinaj	k1gInSc6	Sinaj
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pouze	pouze	k6eAd1	pouze
Deset	deset	k4xCc4	deset
přikázání	přikázání	k1gNnPc2	přikázání
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
zákony	zákon	k1gInPc1	zákon
byly	být	k5eAaImAgInP	být
zjevovány	zjevovat	k5eAaImNgInP	zjevovat
postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
putování	putování	k1gNnSc4	putování
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
to	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgInS	být
hlas	hlas	k1gInSc1	hlas
Boží	boží	k2eAgInSc1d1	boží
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Mojžíšovi	Mojžíš	k1gMnSc3	Mojžíš
diktoval	diktovat	k5eAaImAgMnS	diktovat
obsah	obsah	k1gInSc4	obsah
Tóry	tóra	k1gFnSc2	tóra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
Druhého	druhý	k4xOgMnSc2	druhý
chrámu	chrám	k1gInSc2	chrám
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
obdržel	obdržet	k5eAaPmAgMnS	obdržet
nejen	nejen	k6eAd1	nejen
celou	celý	k2eAgFnSc4d1	celá
psanou	psaný	k2eAgFnSc4d1	psaná
Tóru	tóra	k1gFnSc4	tóra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Ústní	ústní	k2eAgInSc4d1	ústní
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
osvícenství	osvícenství	k1gNnSc2	osvícenství
začaly	začít	k5eAaPmAgInP	začít
ujímat	ujímat	k5eAaImF	ujímat
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
Tóry	tóra	k1gFnSc2	tóra
sestaven	sestavit	k5eAaPmNgInS	sestavit
z	z	k7c2	z
několika	několik	k4yIc2	několik
předcházejících	předcházející	k2eAgInPc2d1	předcházející
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
hypotézu	hypotéza	k1gFnSc4	hypotéza
pramenů	pramen	k1gInPc2	pramen
či	či	k8xC	či
dokumentární	dokumentární	k2eAgFnSc4d1	dokumentární
hypotézu	hypotéza	k1gFnSc4	hypotéza
(	(	kIx(	(
<g/>
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
někdy	někdy	k6eAd1	někdy
podle	podle	k7c2	podle
předpokládných	předpokládný	k2eAgInPc2d1	předpokládný
pramenů	pramen	k1gInPc2	pramen
"	"	kIx"	"
<g/>
Jahvisty	Jahvist	k1gInPc4	Jahvist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Elohisty	Elohist	k1gInPc4	Elohist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kněžského	kněžský	k2eAgInSc2d1	kněžský
spisu	spis	k1gInSc2	spis
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Priesterschrift	Priesterschrift	k2eAgMnSc1d1	Priesterschrift
<g/>
,	,	kIx,	,
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Deuteronomisty	Deuteronomista	k1gMnPc4	Deuteronomista
zkratkou	zkratka	k1gFnSc7	zkratka
"	"	kIx"	"
<g/>
JEPD	JEPD	kA	JEPD
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
podobě	podoba	k1gFnSc6	podoba
ji	on	k3xPp3gFnSc4	on
formuloval	formulovat	k5eAaImAgMnS	formulovat
Julius	Julius	k1gMnSc1	Julius
Wellhausen	Wellhausna	k1gFnPc2	Wellhausna
<g/>
,	,	kIx,	,
hypotéza	hypotéza	k1gFnSc1	hypotéza
ale	ale	k8xC	ale
prošla	projít	k5eAaPmAgFnS	projít
mnoha	mnoho	k4c7	mnoho
modifikacemi	modifikace	k1gFnPc7	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
společně	společně	k6eAd1	společně
sdíleným	sdílený	k2eAgNnPc3d1	sdílené
východiskům	východisko	k1gNnPc3	východisko
moderního	moderní	k2eAgNnSc2d1	moderní
biblického	biblický	k2eAgNnSc2d1	biblické
bádání	bádání	k1gNnSc2	bádání
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
Tóry	tóra	k1gFnSc2	tóra
kompromisem	kompromis	k1gInSc7	kompromis
kněžských	kněžský	k2eAgInPc2d1	kněžský
a	a	k8xC	a
laických	laický	k2eAgInPc2d1	laický
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
těchto	tento	k3xDgFnPc2	tento
tradic	tradice	k1gFnPc2	tradice
do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
známe	znát	k5eAaImIp1nP	znát
jako	jako	k9	jako
Tóru	tóra	k1gFnSc4	tóra
či	či	k8xC	či
Pentateuch	pentateuch	k1gInSc4	pentateuch
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Peršanů	peršan	k1gInPc2	peršan
od	od	k7c2	od
konce	konec	k1gInSc2	konec
6	[number]	k4	6
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tóra	tóra	k1gFnSc1	tóra
je	být	k5eAaImIp3nS	být
souvislým	souvislý	k2eAgNnSc7d1	souvislé
vyprávěním	vyprávění	k1gNnSc7	vyprávění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
stvořením	stvoření	k1gNnSc7	stvoření
světa	svět	k1gInSc2	svět
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
Židé	Žid	k1gMnPc1	Žid
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Kanaánu	Kanaán	k1gInSc2	Kanaán
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
řecké	řecký	k2eAgInPc1d1	řecký
názvy	název	k1gInPc1	název
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
Exodus	Exodus	k1gInSc1	Exodus
<g/>
,	,	kIx,	,
Leviticus	Leviticus	k1gInSc1	Leviticus
<g/>
,	,	kIx,	,
Numeri	Numeri	k1gNnSc1	Numeri
a	a	k8xC	a
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
<g/>
)	)	kIx)	)
popisují	popisovat	k5eAaImIp3nP	popisovat
zhruba	zhruba	k6eAd1	zhruba
obsah	obsah	k1gInSc4	obsah
každé	každý	k3xTgFnSc2	každý
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
tradici	tradice	k1gFnSc6	tradice
bývaly	bývat	k5eAaImAgFnP	bývat
knihy	kniha	k1gFnPc1	kniha
označovány	označovat	k5eAaImNgFnP	označovat
podle	podle	k7c2	podle
počátečních	počáteční	k2eAgNnPc2d1	počáteční
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Genesis	Genesis	k1gFnSc1	Genesis
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
<g/>
,	,	kIx,	,
Be-rešit	Beešit	k1gFnSc1	Be-rešit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Začíná	začínat	k5eAaImIp3nS	začínat
popisem	popis	k1gInSc7	popis
stvoření	stvoření	k1gNnSc2	stvoření
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
genesis	genesis	k1gFnSc1	genesis
<g/>
)	)	kIx)	)
světa	svět	k1gInSc2	svět
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
praotcích	praotec	k1gMnPc6	praotec
izraelského	izraelský	k2eAgInSc2d1	izraelský
národa	národ	k1gInSc2	národ
–	–	k?	–
Abrahámovi	Abrahámův	k2eAgMnPc1d1	Abrahámův
a	a	k8xC	a
Sáře	Sára	k1gFnSc6	Sára
<g/>
,	,	kIx,	,
Izákovi	Izák	k1gMnSc3	Izák
a	a	k8xC	a
Rebece	Rebeka	k1gFnSc3	Rebeka
<g/>
,	,	kIx,	,
Jákobovi	Jákob	k1gMnSc6	Jákob
<g/>
,	,	kIx,	,
Ráchel	Ráchel	k1gFnPc6	Ráchel
<g/>
,	,	kIx,	,
Lei	lei	k1gInPc6	lei
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc6	jejich
potomcích	potomek	k1gMnPc6	potomek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
izraelský	izraelský	k2eAgInSc4d1	izraelský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
Jákobova	Jákobův	k2eAgMnSc2d1	Jákobův
syna	syn	k1gMnSc2	syn
Josefa	Josef	k1gMnSc2	Josef
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Exodus	Exodus	k1gInSc1	Exodus
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ש	ש	k?	ש
<g/>
,	,	kIx,	,
Šemot	Šemot	k1gInSc1	Šemot
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jména	jméno	k1gNnSc2	jméno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc4	život
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
egyptském	egyptský	k2eAgNnSc6d1	egyptské
otroctví	otroctví	k1gNnSc6	otroctví
<g/>
,	,	kIx,	,
narození	narození	k1gNnSc6	narození
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vysvobození	vysvobození	k1gNnSc4	vysvobození
a	a	k8xC	a
odchod	odchod	k1gInSc4	odchod
(	(	kIx(	(
<g/>
exodus	exodus	k1gInSc4	exodus
<g/>
)	)	kIx)	)
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Předání	předání	k1gNnSc1	předání
Desatera	desatero	k1gNnSc2	desatero
Bohem	bůh	k1gMnSc7	bůh
Mojžíšovi	Mojžíš	k1gMnSc3	Mojžíš
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Sinaj	Sinaj	k1gInSc4	Sinaj
<g/>
.	.	kIx.	.
</s>
<s>
Leviticus	Leviticus	k1gInSc1	Leviticus
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ו	ו	k?	ו
<g/>
,	,	kIx,	,
Va-jikra	Vaikra	k1gFnSc1	Va-jikra
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
A	a	k8xC	a
zavolal	zavolat	k5eAaPmAgMnS	zavolat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
mnoho	mnoho	k4c1	mnoho
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
613	[number]	k4	613
micvot	micvota	k1gFnPc2	micvota
<g/>
.	.	kIx.	.
</s>
<s>
Popisované	popisovaný	k2eAgInPc1d1	popisovaný
zákony	zákon	k1gInPc1	zákon
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
především	především	k6eAd1	především
chrámových	chrámový	k2eAgInPc2d1	chrámový
rituálů	rituál	k1gInPc2	rituál
a	a	k8xC	a
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
izraelitský	izraelitský	k2eAgInSc1d1	izraelitský
kmen	kmen	k1gInSc1	kmen
Levitů	levit	k1gMnPc2	levit
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
i	i	k9	i
řecké	řecký	k2eAgNnSc4d1	řecké
a	a	k8xC	a
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Numeri	Numeri	k1gNnSc1	Numeri
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
<g/>
,	,	kIx,	,
Ba-midbar	Baidbar	k1gInSc1	Ba-midbar
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Popis	popis	k1gInSc1	popis
putování	putování	k1gNnSc4	putování
Izraelitů	izraelita	k1gMnPc2	izraelita
pouští	poušť	k1gFnPc2	poušť
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
narativních	narativní	k2eAgFnPc2d1	narativní
pasáží	pasáž	k1gFnPc2	pasáž
popisující	popisující	k2eAgFnSc2d1	popisující
obtíže	obtíž	k1gFnSc2	obtíž
a	a	k8xC	a
přestoupení	přestoupení	k1gNnPc4	přestoupení
Izraelitů	izraelita	k1gMnPc2	izraelita
proti	proti	k7c3	proti
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
numeri	numeri	k1gNnSc2	numeri
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
kniha	kniha	k1gFnSc1	kniha
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příběh	příběh	k1gInSc1	příběh
vzpoury	vzpoura	k1gFnSc2	vzpoura
proti	proti	k7c3	proti
Mojžíšovi	Mojžíš	k1gMnSc3	Mojžíš
<g/>
.	.	kIx.	.
</s>
<s>
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ד	ד	k?	ד
<g/>
,	,	kIx,	,
Devarim	Devarim	k1gInSc1	Devarim
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Slova	slovo	k1gNnPc4	slovo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Dostalo	dostat	k5eAaPmAgNnS	dostat
své	svůj	k3xOyFgNnSc1	svůj
pojmenování	pojmenování	k1gNnSc1	pojmenování
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
opakuje	opakovat	k5eAaImIp3nS	opakovat
zákony	zákon	k1gInPc4	zákon
předané	předaný	k2eAgFnSc2d1	předaná
Bohem	bůh	k1gMnSc7	bůh
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
řeč	řeč	k1gFnSc4	řeč
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
národu	národ	k1gInSc3	národ
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
připravují	připravovat	k5eAaImIp3nP	připravovat
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
kapitole	kapitola	k1gFnSc6	kapitola
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Šema	Šem	k1gInSc2	Šem
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgNnSc4d1	Židovské
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Tóra	tóra	k1gFnSc1	tóra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
tradici	tradice	k1gFnSc6	tradice
otevřena	otevřít	k5eAaPmNgFnS	otevřít
interpretaci	interpretace	k1gFnSc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
rabínského	rabínský	k2eAgInSc2d1	rabínský
judaismu	judaismus	k1gInSc2	judaismus
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
Tóry	tóra	k1gFnSc2	tóra
podroben	podrobit	k5eAaPmNgInS	podrobit
agadické	agadický	k2eAgFnSc6d1	agadický
<g/>
,	,	kIx,	,
halachické	halachický	k2eAgFnSc6d1	halachická
<g/>
,	,	kIx,	,
gramatické	gramatický	k2eAgFnSc6d1	gramatická
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgFnSc6d1	filozofická
a	a	k8xC	a
mystické	mystický	k2eAgFnSc6d1	mystická
interpretaci	interpretace	k1gFnSc6	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nikdy	nikdy	k6eAd1	nikdy
nekončící	končící	k2eNgFnSc1d1	nekončící
interpretace	interpretace	k1gFnSc1	interpretace
Tóry	tóra	k1gFnSc2	tóra
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
názorové	názorový	k2eAgNnSc4d1	názorové
bohatství	bohatství	k1gNnSc4	bohatství
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Doslovnému	doslovný	k2eAgInSc3d1	doslovný
výkladu	výklad	k1gInSc3	výklad
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedává	dávat	k5eNaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
výklady	výklad	k1gInPc7	výklad
<g/>
.	.	kIx.	.
</s>
<s>
Kabala	kabala	k1gFnSc1	kabala
a	a	k8xC	a
chasidismus	chasidismus	k1gInSc1	chasidismus
někdy	někdy	k6eAd1	někdy
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
předávání	předávání	k1gNnSc2	předávání
Tóry	tóra	k1gFnSc2	tóra
nikdy	nikdy	k6eAd1	nikdy
nekončí	končit	k5eNaImIp3nS	končit
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zopakován	zopakovat	k5eAaPmNgInS	zopakovat
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
