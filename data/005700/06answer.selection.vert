<s>
Tóra	tóra	k1gFnSc1	tóra
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ת	ת	k?	ת
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
Zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc1	učení
a	a	k8xC	a
v	v	k7c6	v
nejužším	úzký	k2eAgInSc6d3	nejužší
smyslu	smysl	k1gInSc6	smysl
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
TaNaChu	TaNaCh	k1gInSc2	TaNaCh
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
