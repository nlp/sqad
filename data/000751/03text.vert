<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgInSc1d1	francouzský
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Louis	Louis	k1gMnSc1	Louis
IX	IX	kA	IX
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1214	[number]	k4	1214
Poissy	Poissa	k1gFnSc2	Poissa
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1270	[number]	k4	1270
Tunis	Tunis	k1gInSc1	Tunis
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1226	[number]	k4	1226
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Kapetovců	Kapetovec	k1gInPc2	Kapetovec
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
dvou	dva	k4xCgFnPc6	dva
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
však	však	k9	však
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1298	[number]	k4	1298
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
příkladný	příkladný	k2eAgInSc4d1	příkladný
život	život	k1gInSc4	život
plný	plný	k2eAgInSc4d1	plný
odříkání	odříkání	k1gNnSc4	odříkání
svatořečen	svatořečen	k2eAgMnSc1d1	svatořečen
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
z	z	k7c2	z
početného	početný	k2eAgNnSc2d1	početné
potomstva	potomstvo	k1gNnSc2	potomstvo
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Blanky	Blanka	k1gFnSc2	Blanka
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
kastilského	kastilský	k2eAgMnSc4d1	kastilský
krále	král	k1gMnSc4	král
Alfonse	Alfons	k1gMnSc2	Alfons
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgNnSc7	druhý
jménem	jméno	k1gNnSc7	jméno
známým	známý	k1gMnSc7	známý
synem	syn	k1gMnSc7	syn
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1209	[number]	k4	1209
<g/>
,	,	kIx,	,
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	let	k1gInPc6	let
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
čtyřletý	čtyřletý	k2eAgMnSc1d1	čtyřletý
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1218	[number]	k4	1218
stal	stát	k5eAaPmAgMnS	stát
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dobových	dobový	k2eAgInPc2d1	dobový
svědectví	svědectví	k1gNnSc4	svědectví
princ	princa	k1gFnPc2	princa
zdědil	zdědit	k5eAaPmAgInS	zdědit
plavé	plavý	k2eAgInPc4d1	plavý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
jemný	jemný	k2eAgInSc4d1	jemný
vzhled	vzhled	k1gInSc4	vzhled
po	po	k7c6	po
rodu	rod	k1gInSc6	rod
z	z	k7c2	z
Hainaut	Hainaut	k1gInSc4	Hainaut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
mu	on	k3xPp3gMnSc3	on
především	především	k6eAd1	především
matka	matka	k1gFnSc1	matka
dopřávala	dopřávat	k5eAaImAgFnS	dopřávat
pečlivou	pečlivý	k2eAgFnSc4d1	pečlivá
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
trávil	trávit	k5eAaImAgInS	trávit
také	také	k9	také
s	s	k7c7	s
dědečkem	dědeček	k1gMnSc7	dědeček
Filipem	Filip	k1gMnSc7	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bouvines	Bouvinesa	k1gFnPc2	Bouvinesa
přenechal	přenechat	k5eAaPmAgInS	přenechat
válčení	válčení	k1gNnPc4	válčení
i	i	k8xC	i
správu	správa	k1gFnSc4	správa
království	království	k1gNnSc2	království
synovi	syn	k1gMnSc3	syn
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
zkušeným	zkušený	k2eAgMnPc3d1	zkušený
královským	královský	k2eAgMnPc3d1	královský
rádcům	rádce	k1gMnPc3	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Ludvík	Ludvík	k1gMnSc1	Ludvík
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1226	[number]	k4	1226
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
albigenským	albigenští	k1gMnPc3	albigenští
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
úplavici	úplavice	k1gFnSc4	úplavice
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
teprve	teprve	k6eAd1	teprve
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelý	zemřelý	k2eAgMnSc1d1	zemřelý
král	král	k1gMnSc1	král
byl	být	k5eAaImAgInS	být
pohřben	pohřben	k2eAgInSc4d1	pohřben
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
o	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
korunován	korunovat	k5eAaBmNgMnS	korunovat
na	na	k7c4	na
krále	král	k1gMnSc4	král
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
svému	své	k1gNnSc3	své
věku	věk	k1gInSc2	věk
však	však	k9	však
nemohl	moct	k5eNaImAgInS	moct
samostatně	samostatně	k6eAd1	samostatně
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Remeše	Remeš	k1gFnSc2	Remeš
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
při	při	k7c6	při
zastávce	zastávka	k1gFnSc6	zastávka
v	v	k7c4	v
Soissons	Soissons	k1gInSc4	Soissons
pasován	pasovat	k5eAaBmNgMnS	pasovat
na	na	k7c4	na
rytíře	rytíř	k1gMnSc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
ujala	ujmout	k5eAaPmAgFnS	ujmout
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
hájení	hájení	k1gNnSc2	hájení
práv	právo	k1gNnPc2	právo
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správě	správa	k1gFnSc6	správa
království	království	k1gNnSc3	království
Blanka	Blanka	k1gFnSc1	Blanka
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
mohla	moct	k5eAaImAgFnS	moct
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
skupiny	skupina	k1gFnSc2	skupina
věrných	věrný	k2eAgInPc2d1	věrný
kastelánských	kastelánský	k2eAgInPc2d1	kastelánský
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
okruhu	okruh	k1gInSc2	okruh
pocházeli	pocházet	k5eAaImAgMnP	pocházet
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
v	v	k7c4	v
Sens	Sens	k1gInSc4	Sens
Gautier	Gautier	k1gMnSc1	Gautier
Cornut	Cornut	k1gMnSc1	Cornut
<g/>
,	,	kIx,	,
královský	královský	k2eAgMnSc1d1	královský
konstábl	konstábl	k1gMnSc1	konstábl
Matouš	Matouš	k1gMnSc1	Matouš
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Montmorency	Montmorenca	k1gFnSc2	Montmorenca
nebo	nebo	k8xC	nebo
královský	královský	k2eAgMnSc1d1	královský
maršálek	maršálek	k1gMnSc1	maršálek
Jean	Jean	k1gMnSc1	Jean
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Clément	Clément	k1gMnSc1	Clément
<g/>
.	.	kIx.	.
</s>
<s>
Podporovali	podporovat	k5eAaImAgMnP	podporovat
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
senliský	senliský	k2eAgMnSc1d1	senliský
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
kancléř	kancléř	k1gMnSc1	kancléř
Guérin	Guérin	k1gInSc1	Guérin
a	a	k8xC	a
komoří	komoří	k1gMnSc1	komoří
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
z	z	k7c2	z
Roye	Roy	k1gMnSc2	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
královna	královna	k1gFnSc1	královna
omezila	omezit	k5eAaPmAgFnS	omezit
rizika	riziko	k1gNnPc4	riziko
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
případnými	případný	k2eAgFnPc7d1	případná
ambicemi	ambice	k1gFnPc7	ambice
Filipa	Filip	k1gMnSc2	Filip
Hurepela	Hurepel	k1gMnSc2	Hurepel
<g/>
,	,	kIx,	,
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
svého	své	k1gNnSc2	své
zemřelého	zemřelý	k1gMnSc2	zemřelý
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
mu	on	k3xPp3gMnSc3	on
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
hned	hned	k6eAd1	hned
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
korunovaci	korunovace	k1gFnSc6	korunovace
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
podnětu	podnět	k1gInSc2	podnět
dva	dva	k4xCgInPc4	dva
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnPc6	jeho
panstvích	panství	k1gNnPc6	panství
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
saintpolské	saintpolský	k2eAgNnSc1d1	saintpolský
hrabství	hrabství	k1gNnSc1	hrabství
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1227	[number]	k4	1227
obdržel	obdržet	k5eAaPmAgMnS	obdržet
i	i	k9	i
doživotní	doživotní	k2eAgFnSc4d1	doživotní
rentu	renta	k1gFnSc4	renta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
6000	[number]	k4	6000
tourských	tourský	k2eAgFnPc2d1	tourský
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Hurepel	Hurepel	k1gMnSc1	Hurepel
se	se	k3xPyFc4	se
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
musel	muset	k5eAaImAgMnS	muset
zavázat	zavázat	k5eAaPmF	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
vznášet	vznášet	k5eAaImF	vznášet
žádné	žádný	k3yNgInPc4	žádný
další	další	k2eAgInPc4d1	další
požadavky	požadavek	k1gInPc4	požadavek
ohledně	ohledně	k7c2	ohledně
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1227	[number]	k4	1227
král	král	k1gMnSc1	král
nechal	nechat	k5eAaPmAgMnS	nechat
propustit	propustit	k5eAaPmF	propustit
flanderského	flanderský	k2eAgMnSc4d1	flanderský
hraběte	hrabě	k1gMnSc4	hrabě
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
držen	držet	k5eAaImNgMnS	držet
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
od	od	k7c2	od
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Bouvines	Bouvinesa	k1gFnPc2	Bouvinesa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
propuštění	propuštění	k1gNnSc6	propuštění
složil	složit	k5eAaPmAgMnS	složit
králi	král	k1gMnSc6	král
výkupné	výkupné	k1gNnSc4	výkupné
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
záruky	záruka	k1gFnPc4	záruka
vhodného	vhodný	k2eAgNnSc2d1	vhodné
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
vždy	vždy	k6eAd1	vždy
věrně	věrně	k6eAd1	věrně
sloužil	sloužit	k5eAaImAgMnS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
mladému	mladý	k2eAgMnSc3d1	mladý
králi	král	k1gMnSc3	král
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koalice	koalice	k1gFnSc1	koalice
velmožů	velmož	k1gMnPc2	velmož
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
od	od	k7c2	od
posílení	posílení	k1gNnSc2	posílení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
očekávali	očekávat	k5eAaImAgMnP	očekávat
újmu	újma	k1gFnSc4	újma
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
postavení	postavení	k1gNnSc6	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
především	především	k9	především
bretaňský	bretaňský	k2eAgMnSc1d1	bretaňský
vévoda	vévoda	k1gMnSc1	vévoda
Petr	Petr	k1gMnSc1	Petr
Mauclerc	Mauclerc	k1gFnSc1	Mauclerc
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
ze	z	k7c2	z
Champagne	Champagn	k1gInSc5	Champagn
Theobald	Theobalda	k1gFnPc2	Theobalda
V.	V.	kA	V.
a	a	k8xC	a
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Marche	March	k1gFnSc2	March
Hugo	Hugo	k1gMnSc1	Hugo
X.	X.	kA	X.
z	z	k7c2	z
Lusignanu	Lusignan	k1gInSc2	Lusignan
<g/>
.	.	kIx.	.
</s>
<s>
Spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
znovuzískání	znovuzískání	k1gNnSc6	znovuzískání
ztracených	ztracený	k2eAgNnPc2d1	ztracené
území	území	k1gNnPc2	území
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
vzbouřencům	vzbouřenec	k1gMnPc3	vzbouřenec
sliboval	slibovat	k5eAaImAgMnS	slibovat
různé	různý	k2eAgFnPc4d1	různá
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Královně	královna	k1gFnSc3	královna
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
papežského	papežský	k2eAgMnSc2d1	papežský
legáta	legát	k1gMnSc2	legát
shromáždit	shromáždit	k5eAaPmF	shromáždit
velké	velký	k2eAgNnSc4d1	velké
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
vytáhla	vytáhnout	k5eAaPmAgFnS	vytáhnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Theobald	Theobald	k1gInSc1	Theobald
ze	z	k7c2	z
Champagne	Champagn	k1gInSc5	Champagn
nepohodl	pohodnout	k5eNaPmAgMnS	pohodnout
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pro	pro	k7c4	pro
vzbouřence	vzbouřenka	k1gFnSc6	vzbouřenka
nepříznivé	příznivý	k2eNgFnSc3d1	nepříznivá
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
Petr	Petr	k1gMnSc1	Petr
Mauclerc	Mauclerc	k1gInSc1	Mauclerc
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Lusignanu	Lusignan	k1gInSc2	Lusignan
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1227	[number]	k4	1227
poddali	poddat	k5eAaPmAgMnP	poddat
královně	královna	k1gFnSc3	královna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnPc1	příležitost
byly	být	k5eAaImAgFnP	být
domluveny	domluven	k2eAgInPc4d1	domluven
sňatky	sňatek	k1gInPc4	sňatek
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
příslušníky	příslušník	k1gMnPc4	příslušník
rodů	rod	k1gInPc2	rod
z	z	k7c2	z
Lusignanu	Lusignan	k1gInSc2	Lusignan
a	a	k8xC	a
z	z	k7c2	z
Dreux	Dreux	k1gInSc4	Dreux
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
příměří	příměří	k1gNnSc1	příměří
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Zklidnění	zklidnění	k1gNnSc1	zklidnění
situace	situace	k1gFnSc2	situace
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
zdánlivé	zdánlivý	k2eAgNnSc1d1	zdánlivé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1227	[number]	k4	1227
se	se	k3xPyFc4	se
francouzští	francouzský	k2eAgMnPc1d1	francouzský
baroni	baron	k1gMnPc1	baron
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c4	v
Corbeil	Corbeil	k1gInSc4	Corbeil
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zmocní	zmocnit	k5eAaPmIp3nS	zmocnit
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
čela	čelo	k1gNnSc2	čelo
si	se	k3xPyFc3	se
postavili	postavit	k5eAaPmAgMnP	postavit
Filipa	Filip	k1gMnSc4	Filip
Hurepela	Hurepel	k1gMnSc4	Hurepel
a	a	k8xC	a
Petra	Petr	k1gMnSc4	Petr
Mauclerka	Mauclerka	k1gFnSc1	Mauclerka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
baronů	baron	k1gMnPc2	baron
nebyla	být	k5eNaImAgFnS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
vzpoura	vzpoura	k1gFnSc1	vzpoura
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaha	snaha	k1gFnSc1	snaha
zbavit	zbavit	k5eAaPmF	zbavit
vlivu	vliv	k1gInSc2	vliv
královnu	královna	k1gFnSc4	královna
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgMnPc1d1	přední
šlechtici	šlechtic	k1gMnPc1	šlechtic
by	by	kYmCp3nP	by
pak	pak	k9	pak
jménem	jméno	k1gNnSc7	jméno
krále	král	k1gMnSc2	král
mohli	moct	k5eAaImAgMnP	moct
vládnout	vládnout	k5eAaImF	vládnout
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Orléans	Orléansa	k1gFnPc2	Orléansa
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
v	v	k7c4	v
Montlhéry	Montlhéra	k1gFnPc4	Montlhéra
zastaven	zastaven	k2eAgInSc1d1	zastaven
baronským	baronský	k2eAgNnSc7d1	baronský
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
jej	on	k3xPp3gInSc4	on
až	až	k6eAd1	až
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
pařížský	pařížský	k2eAgInSc4d1	pařížský
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1227	[number]	k4	1227
<g/>
-	-	kIx~	-
<g/>
1229	[number]	k4	1229
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
královská	královský	k2eAgFnSc1d1	královská
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Humberta	Humbert	k1gMnSc2	Humbert
z	z	k7c2	z
Beaujeu	Beaujeus	k1gInSc2	Beaujeus
v	v	k7c6	v
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
proti	proti	k7c3	proti
albigenským	albigenští	k1gMnPc3	albigenští
zahájené	zahájený	k2eAgFnSc2d1	zahájená
Ludvíkovým	Ludvíkův	k2eAgMnSc7d1	Ludvíkův
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
tažení	tažení	k1gNnSc2	tažení
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
smlouvou	smlouva	k1gFnSc7	smlouva
podepsanou	podepsaný	k2eAgFnSc7d1	podepsaná
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1229	[number]	k4	1229
<g/>
.	.	kIx.	.
</s>
<s>
Toulouskému	toulouský	k2eAgMnSc3d1	toulouský
hraběti	hrabě	k1gMnSc3	hrabě
Raimondu	Raimond	k1gMnSc3	Raimond
VII	VII	kA	VII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
dědička	dědička	k1gFnSc1	dědička
Jana	Jana	k1gFnSc1	Jana
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
provdat	provdat	k5eAaPmF	provdat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Ludvíkových	Ludvíkových	k2eAgMnPc2d1	Ludvíkových
bratrů	bratr	k1gMnPc2	bratr
a	a	k8xC	a
přinést	přinést	k5eAaPmF	přinést
mu	on	k3xPp3gMnSc3	on
věnem	věno	k1gNnSc7	věno
Toulouse	Toulouse	k1gInSc1	Toulouse
a	a	k8xC	a
toulouský	toulouský	k2eAgInSc1d1	toulouský
kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Raimond	Raimond	k1gMnSc1	Raimond
neměl	mít	k5eNaImAgMnS	mít
další	další	k2eAgMnPc4d1	další
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
Jana	Jana	k1gFnSc1	Jana
zdědit	zdědit	k5eAaPmF	zdědit
i	i	k9	i
ostatní	ostatní	k2eAgMnSc1d1	ostatní
jeho	on	k3xPp3gNnSc2	on
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
Jana	Jana	k1gFnSc1	Jana
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
celé	celý	k2eAgNnSc4d1	celé
jejich	jejich	k3xOp3gNnSc4	jejich
panství	panství	k1gNnSc4	panství
stát	stát	k5eAaImF	stát
součástí	součást	k1gFnSc7	součást
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
smlouvou	smlouva	k1gFnSc7	smlouva
získal	získat	k5eAaPmAgMnS	získat
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
albigenského	albigenský	k2eAgInSc2d1	albigenský
kraje	kraj	k1gInSc2	kraj
včetně	včetně	k7c2	včetně
města	město	k1gNnSc2	město
Albi	Alb	k1gFnSc2	Alb
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
sedm	sedm	k4xCc4	sedm
hradů	hrad	k1gInPc2	hrad
včetně	včetně	k7c2	včetně
citadely	citadela	k1gFnSc2	citadela
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1229	[number]	k4	1229
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Ludvík	Ludvík	k1gMnSc1	Ludvík
smlouvu	smlouva	k1gFnSc4	smlouva
i	i	k9	i
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
vzbouřencem	vzbouřenec	k1gMnSc7	vzbouřenec
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
Raimundem	Raimund	k1gMnSc7	Raimund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trencavelem	Trencavel	k1gInSc7	Trencavel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
získal	získat	k5eAaPmAgMnS	získat
Ludvík	Ludvík	k1gMnSc1	Ludvík
Carcassonne	Carcassonn	k1gInSc5	Carcassonn
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
korunní	korunní	k2eAgNnSc1d1	korunní
území	území	k1gNnSc1	území
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
až	až	k9	až
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
zde	zde	k6eAd1	zde
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgInS	založit
přístav	přístav	k1gInSc1	přístav
Aigues-Mortes	Aigues-Mortes	k1gInSc1	Aigues-Mortes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1229	[number]	k4	1229
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
založil	založit	k5eAaPmAgInS	založit
Klášter	klášter	k1gInSc1	klášter
Royaumont	Royaumonto	k1gNnPc2	Royaumonto
<g/>
.	.	kIx.	.
</s>
<s>
Plnil	plnit	k5eAaImAgMnS	plnit
tak	tak	k9	tak
vůli	vůle	k1gFnSc4	vůle
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
odkázal	odkázat	k5eAaPmAgInS	odkázat
vekou	veka	k1gFnSc7	veka
sumu	suma	k1gFnSc4	suma
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
kláštera	klášter	k1gInSc2	klášter
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
fundací	fundace	k1gFnSc7	fundace
řeholních	řeholní	k2eAgMnPc2d1	řeholní
kanovníků	kanovník	k1gMnPc2	kanovník
z	z	k7c2	z
pařížského	pařížský	k2eAgNnSc2d1	pařížské
opatství	opatství	k1gNnSc2	opatství
svatého	svatý	k2eAgMnSc2d1	svatý
Viktora	Viktor	k1gMnSc2	Viktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
převedli	převést	k5eAaPmAgMnP	převést
fundaci	fundace	k1gFnSc4	fundace
na	na	k7c4	na
cisterciácký	cisterciácký	k2eAgInSc4d1	cisterciácký
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
nového	nový	k2eAgInSc2d1	nový
kláštera	klášter	k1gInSc2	klášter
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
osobně	osobně	k6eAd1	osobně
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1233	[number]	k4	1233
<g/>
-	-	kIx~	-
<g/>
1234	[number]	k4	1234
Ludvík	Ludvík	k1gMnSc1	Ludvík
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vyřešit	vyřešit	k5eAaPmF	vyřešit
spor	spor	k1gInSc4	spor
o	o	k7c4	o
hrabství	hrabství	k1gNnSc4	hrabství
Champagne	Champagn	k1gMnSc5	Champagn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
hrabství	hrabství	k1gNnSc6	hrabství
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
Alice	Alice	k1gFnSc1	Alice
<g/>
,	,	kIx,	,
kyperská	kyperský	k2eAgFnSc1d1	Kyperská
královna	královna	k1gFnSc1	královna
a	a	k8xC	a
sestřenice	sestřenice	k1gFnSc1	sestřenice
hraběte	hrabě	k1gMnSc2	hrabě
Theobalda	Theobald	k1gMnSc2	Theobald
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1234	[number]	k4	1234
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Alice	Alice	k1gFnSc1	Alice
vzdala	vzdát	k5eAaPmAgFnS	vzdát
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
hrabství	hrabství	k1gNnSc6	hrabství
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c7	za
40	[number]	k4	40
000	[number]	k4	000
tourských	tourský	k2eAgFnPc2d1	tourský
liber	libra	k1gFnPc2	libra
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
a	a	k8xC	a
roční	roční	k2eAgFnSc4d1	roční
rentu	renta	k1gFnSc4	renta
2000	[number]	k4	2000
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Theobald	Theobald	k1gInSc1	Theobald
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
tak	tak	k9	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
částku	částka	k1gFnSc4	částka
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
Ludvíka	Ludvík	k1gMnSc4	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
uhradil	uhradit	k5eAaPmAgMnS	uhradit
Alici	Alice	k1gFnSc4	Alice
všechny	všechen	k3xTgFnPc4	všechen
pohledávky	pohledávka	k1gFnPc4	pohledávka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
se	se	k3xPyFc4	se
Theobald	Theobalda	k1gFnPc2	Theobalda
v	v	k7c4	v
králův	králův	k2eAgInSc4d1	králův
prospěch	prospěch	k1gInSc4	prospěch
vzdal	vzdát	k5eAaPmAgInS	vzdát
lenní	lenní	k2eAgFnPc4d1	lenní
držby	držba	k1gFnPc4	držba
hrabství	hrabství	k1gNnSc2	hrabství
Blois	Blois	k1gFnSc2	Blois
<g/>
,	,	kIx,	,
Chartres	Chartres	k1gMnSc1	Chartres
<g/>
,	,	kIx,	,
Sancerre	Sancerr	k1gInSc5	Sancerr
a	a	k8xC	a
châteaudunského	châteaudunského	k2eAgNnPc2d1	châteaudunského
vikomství	vikomství	k1gNnPc2	vikomství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1233	[number]	k4	1233
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
stále	stále	k6eAd1	stále
svobodný	svobodný	k2eAgInSc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Vina	vina	k1gFnSc1	vina
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
kladena	kladen	k2eAgFnSc1d1	kladena
jeho	jeho	k3xOp3gFnSc3	jeho
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prý	prý	k9	prý
bránila	bránit	k5eAaImAgFnS	bránit
jeho	jeho	k3xOp3gInSc3	jeho
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
oslaben	oslabit	k5eAaPmNgInS	oslabit
její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
Ludvíkovou	Ludvíková	k1gFnSc4	Ludvíková
vyvolenou	vyvolená	k1gFnSc4	vyvolená
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teprve	teprve	k6eAd1	teprve
třináctiletá	třináctiletý	k2eAgFnSc1d1	třináctiletá
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
provensálského	provensálský	k2eAgMnSc2d1	provensálský
hraběte	hrabě	k1gMnSc2	hrabě
Ramona	Ramona	k1gFnSc1	Ramona
Berenguera	Berenguera	k1gFnSc1	Berenguera
V.	V.	kA	V.
a	a	k8xC	a
Beatrix	Beatrix	k1gInSc1	Beatrix
Savojské	savojský	k2eAgFnSc2d1	Savojská
<g/>
.	.	kIx.	.
</s>
<s>
Snoubenci	snoubenec	k1gMnPc1	snoubenec
byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
příbuzní	příbuzný	k1gMnPc1	příbuzný
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dispensem	dispens	k1gInSc7	dispens
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1234	[number]	k4	1234
je	být	k5eAaImIp3nS	být
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
této	tento	k3xDgFnSc2	tento
překážky	překážka	k1gFnSc2	překážka
zbavil	zbavit	k5eAaPmAgMnS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1234	[number]	k4	1234
v	v	k7c4	v
Sens	Sens	k1gInSc4	Sens
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
věno	věno	k1gNnSc4	věno
vyplatil	vyplatit	k5eAaPmAgMnS	vyplatit
nevěstě	nevěsta	k1gFnSc6	nevěsta
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
8000	[number]	k4	8000
marek	marka	k1gFnPc2	marka
stříbra	stříbro	k1gNnSc2	stříbro
později	pozdě	k6eAd2	pozdě
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
o	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
2000	[number]	k4	2000
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
novomanželka	novomanželka	k1gFnSc1	novomanželka
korunována	korunovat	k5eAaBmNgFnS	korunovat
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
sňatek	sňatek	k1gInSc4	sňatek
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
částky	částka	k1gFnPc1	částka
2526	[number]	k4	2526
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
či	či	k8xC	či
nejpozději	pozdě	k6eAd3	pozdě
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
ujal	ujmout	k5eAaPmAgMnS	ujmout
samostatné	samostatný	k2eAgFnPc4d1	samostatná
vlády	vláda	k1gFnPc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
však	však	k9	však
Blanka	Blanka	k1gFnSc1	Blanka
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
zachovala	zachovat	k5eAaPmAgFnS	zachovat
autoritu	autorita	k1gFnSc4	autorita
a	a	k8xC	a
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
zamilováni	zamilován	k2eAgMnPc1d1	zamilován
a	a	k8xC	a
králova	králův	k2eAgFnSc1d1	králova
matka	matka	k1gFnSc1	matka
neviděla	vidět	k5eNaImAgFnS	vidět
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
tráví	trávit	k5eAaImIp3nS	trávit
tolik	tolik	k6eAd1	tolik
času	čas	k1gInSc2	čas
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgInS	starat
o	o	k7c4	o
správu	správa	k1gFnSc4	správa
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
novomanželům	novomanžel	k1gMnPc3	novomanžel
narodilo	narodit	k5eAaPmAgNnS	narodit
až	až	k9	až
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Ludvík	Ludvík	k1gMnSc1	Ludvík
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
velmi	velmi	k6eAd1	velmi
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyrovnal	vyrovnat	k5eNaPmAgMnS	vyrovnat
vlivu	vliv	k1gInSc2	vliv
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
nebo	nebo	k8xC	nebo
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
získal	získat	k5eAaPmAgMnS	získat
Ludvík	Ludvík	k1gMnSc1	Ludvík
od	od	k7c2	od
konstantinopolského	konstantinopolský	k2eAgMnSc2d1	konstantinopolský
císaře	císař	k1gMnSc2	císař
Balduina	Balduino	k1gNnSc2	Balduino
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
trnovou	trnový	k2eAgFnSc4d1	Trnová
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nechal	nechat	k5eAaPmAgInS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1239	[number]	k4	1239
převézt	převézt	k5eAaPmF	převézt
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
za	za	k7c4	za
trnovou	trnový	k2eAgFnSc4d1	Trnová
korunu	koruna	k1gFnSc4	koruna
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
10	[number]	k4	10
000	[number]	k4	000
pařížských	pařížský	k2eAgInPc2d1	pařížský
livrů	livr	k1gInPc2	livr
a	a	k8xC	a
doprava	doprava	k1gFnSc1	doprava
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
stála	stát	k5eAaImAgFnS	stát
dalších	další	k2eAgInPc2d1	další
2000	[number]	k4	2000
livrů	livr	k1gInPc2	livr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
získanou	získaný	k2eAgFnSc4d1	získaná
cennou	cenný	k2eAgFnSc4d1	cenná
relikvii	relikvie	k1gFnSc4	relikvie
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
novou	nový	k2eAgFnSc4d1	nová
kapli	kaple	k1gFnSc4	kaple
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k9	jako
Sainte-Chapelle	Sainte-Chapelle	k1gFnSc1	Sainte-Chapelle
(	(	kIx(	(
<g/>
Svatá	svatý	k2eAgFnSc1d1	svatá
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
čase	čas	k1gInSc6	čas
včetně	včetně	k7c2	včetně
výzdoby	výzdoba	k1gFnSc2	výzdoba
mozaikových	mozaikový	k2eAgNnPc2d1	mozaikové
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
byla	být	k5eAaImAgFnS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1248	[number]	k4	1248
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
kaple	kaple	k1gFnSc1	kaple
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
se	se	k3xPyFc4	se
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
kolekci	kolekce	k1gFnSc4	kolekce
svatých	svatý	k2eAgInPc2d1	svatý
ostatků	ostatek	k1gInPc2	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sehrála	sehrát	k5eAaPmAgFnS	sehrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
kapetovského	kapetovský	k2eAgInSc2d1	kapetovský
monarchistického	monarchistický	k2eAgInSc2d1	monarchistický
kultu	kult	k1gInSc2	kult
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
velké	velký	k2eAgInPc4d1	velký
fragmenty	fragment	k1gInPc4	fragment
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Crux	Crux	k1gInSc1	Crux
triumphalis	triumphalis	k1gInSc1	triumphalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železný	železný	k2eAgInSc1d1	železný
hrot	hrot	k1gInSc1	hrot
kopí	kopí	k1gNnPc2	kopí
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
byl	být	k5eAaImAgInS	být
proboden	probodnout	k5eAaPmNgInS	probodnout
bok	bok	k1gInSc4	bok
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
Kristova	Kristův	k2eAgInSc2d1	Kristův
rubáše	rubáš	k1gInSc2	rubáš
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
z	z	k7c2	z
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
a	a	k8xC	a
také	také	k9	také
různé	různý	k2eAgInPc4d1	různý
jiné	jiný	k2eAgInPc4d1	jiný
ostatky	ostatek	k1gInPc4	ostatek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
lebky	lebka	k1gFnPc1	lebka
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Blažeje	Blažej	k1gMnSc2	Blažej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
plnoletosti	plnoletost	k1gFnSc2	plnoletost
svých	svůj	k3xOyFgInPc2	svůj
mladších	mladý	k2eAgInPc2d2	mladší
bratří	bratr	k1gMnPc2	bratr
Ludvík	Ludvík	k1gMnSc1	Ludvík
naplnil	naplnit	k5eAaPmAgMnS	naplnit
otcovu	otcův	k2eAgFnSc4d1	otcova
závěť	závěť	k1gFnSc4	závěť
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
jejich	jejich	k3xOp3gNnSc2	jejich
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
se	se	k3xPyFc4	se
Robert	Robert	k1gMnSc1	Robert
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1237	[number]	k4	1237
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1241	[number]	k4	1241
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1246	[number]	k4	1246
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc4d1	Anjý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
bratři	bratr	k1gMnPc1	bratr
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
pasováni	pasován	k2eAgMnPc1d1	pasován
na	na	k7c4	na
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sedmá	sedmý	k4xOgFnSc1	sedmý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1244	[number]	k4	1244
Ludvík	Ludvík	k1gMnSc1	Ludvík
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
úplavici	úplavice	k1gFnSc6	úplavice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
královo	králův	k2eAgNnSc1d1	královo
okolí	okolí	k1gNnSc1	okolí
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
obávat	obávat	k5eAaImF	obávat
nejhoršího	zlý	k2eAgMnSc4d3	nejhorší
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Ludvík	Ludvík	k1gMnSc1	Ludvík
v	v	k7c6	v
předtuše	předtucha	k1gFnSc6	předtucha
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zemřel	zemřít	k5eAaPmAgMnS	zemřít
smířen	smířen	k2eAgMnSc1d1	smířen
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
dva	dva	k4xCgMnPc4	dva
smírčí	smírčí	k1gMnPc4	smírčí
soudce	soudce	k1gMnPc4	soudce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
urovnat	urovnat	k5eAaPmF	urovnat
jeho	jeho	k3xOp3gInPc4	jeho
spory	spor	k1gInPc4	spor
s	s	k7c7	s
kapitulou	kapitula	k1gFnSc7	kapitula
katedrály	katedrála	k1gFnSc2	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
modlitby	modlitba	k1gFnPc1	modlitba
a	a	k8xC	a
procesí	procesí	k1gNnPc1	procesí
za	za	k7c4	za
královo	králův	k2eAgNnSc4d1	královo
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
dokonce	dokonce	k9	dokonce
nechala	nechat	k5eAaPmAgFnS	nechat
do	do	k7c2	do
Pontoise	Pontoise	k1gFnSc2	Pontoise
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ludvík	Ludvík	k1gMnSc1	Ludvík
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
,	,	kIx,	,
přivézt	přivézt	k5eAaPmF	přivézt
relikvie	relikvie	k1gFnPc4	relikvie
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
Ludvík	Ludvík	k1gMnSc1	Ludvík
dotkl	dotknout	k5eAaPmAgInS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzdravení	uzdravení	k1gNnSc6	uzdravení
z	z	k7c2	z
vážné	vážný	k2eAgFnSc2d1	vážná
nemoci	nemoc	k1gFnSc2	nemoc
král	král	k1gMnSc1	král
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
uspořádat	uspořádat	k5eAaPmF	uspořádat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkovo	Ludvíkův	k2eAgNnSc1d1	Ludvíkovo
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
se	se	k3xPyFc4	se
nesetkalo	setkat	k5eNaPmAgNnS	setkat
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
i	i	k8xC	i
pařížský	pařížský	k2eAgMnSc1d1	pařížský
biskup	biskup	k1gMnSc1	biskup
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Auvergne	Auvergn	k1gInSc5	Auvergn
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
uzdraveného	uzdravený	k2eAgMnSc4d1	uzdravený
Ludvíka	Ludvík	k1gMnSc4	Ludvík
odvrátit	odvrátit	k5eAaPmF	odvrátit
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
záměru	záměr	k1gInSc2	záměr
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
složil	složit	k5eAaPmAgMnS	složit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nevládl	vládnout	k5eNaImAgMnS	vládnout
všemi	všecek	k3xTgNnPc7	všecek
svými	svůj	k3xOyFgFnPc7	svůj
duševními	duševní	k2eAgFnPc7d1	duševní
schopnostmi	schopnost	k1gFnPc7	schopnost
není	být	k5eNaImIp3nS	být
platný	platný	k2eAgMnSc1d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Rozmluvit	rozmluvit	k5eAaPmF	rozmluvit
králi	král	k1gMnSc3	král
jeho	jeho	k3xOp3gNnPc2	jeho
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ne	ne	k9	ne
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Ludvík	Ludvík	k1gMnSc1	Ludvík
přijal	přijmout	k5eAaPmAgMnS	přijmout
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
papež	papež	k1gMnSc1	papež
s	s	k7c7	s
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
organizací	organizace	k1gFnSc7	organizace
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
legát	legát	k1gMnSc1	legát
Odo	Odo	k1gMnSc1	Odo
ze	z	k7c2	z
Châteauroux	Châteauroux	k1gInSc1	Châteauroux
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
poněkud	poněkud	k6eAd1	poněkud
komplikovalo	komplikovat	k5eAaBmAgNnS	komplikovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
hlásána	hlásán	k2eAgFnSc1d1	hlásána
i	i	k8xC	i
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Fridrichovi	Fridrich	k1gMnSc3	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1245	[number]	k4	1245
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
svolal	svolat	k5eAaPmAgMnS	svolat
shromáždění	shromáždění	k1gNnPc4	shromáždění
baronů	baron	k1gMnPc2	baron
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
jejich	jejich	k3xOp3gInSc4	jejich
souhlas	souhlas	k1gInSc4	souhlas
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1248	[number]	k4	1248
svolal	svolat	k5eAaPmAgInS	svolat
shromáždění	shromáždění	k1gNnSc4	shromáždění
opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
baroni	baron	k1gMnPc1	baron
složili	složit	k5eAaPmAgMnP	složit
lenní	lenní	k2eAgInSc4d1	lenní
hold	hold	k1gInSc4	hold
jeho	jeho	k3xOp3gFnPc3	jeho
dětem	dítě	k1gFnPc3	dítě
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
z	z	k7c2	z
výpravy	výprava	k1gFnSc2	výprava
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
vojskem	vojsko	k1gNnSc7	vojsko
nalodil	nalodit	k5eAaPmAgInS	nalodit
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1248	[number]	k4	1248
v	v	k7c4	v
Aigues-Mortes	Aigues-Mortes	k1gInSc4	Aigues-Mortes
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
i	i	k9	i
skoro	skoro	k6eAd1	skoro
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
jeho	jeho	k3xOp3gNnSc2	jeho
vojska	vojsko	k1gNnSc2	vojsko
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
2500	[number]	k4	2500
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
plavby	plavba	k1gFnSc2	plavba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
za	za	k7c4	za
shromaždiště	shromaždiště	k1gNnSc4	shromaždiště
celého	celý	k2eAgNnSc2d1	celé
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Limassolu	Limassol	k1gInSc2	Limassol
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
Ludvík	Ludvík	k1gMnSc1	Ludvík
dorazil	dorazit	k5eAaPmAgMnS	dorazit
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květno	k1gNnSc2	květno
vyplulo	vyplout	k5eAaPmAgNnS	vyplout
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
vedené	vedený	k2eAgNnSc1d1	vedené
křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
Kypru	Kypr	k1gInSc2	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
počasí	počasí	k1gNnSc2	počasí
dorazila	dorazit	k5eAaPmAgFnS	dorazit
flotila	flotila	k1gFnSc1	flotila
k	k	k7c3	k
Damiettě	Damietta	k1gFnSc3	Damietta
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Časně	časně	k6eAd1	časně
ráno	ráno	k6eAd1	ráno
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začali	začít	k5eAaPmAgMnP	začít
křižáci	křižák	k1gMnPc1	křižák
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
střelců	střelec	k1gMnPc2	střelec
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
s	s	k7c7	s
vyloďováním	vyloďování	k1gNnSc7	vyloďování
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
získat	získat	k5eAaPmF	získat
předmostí	předmostí	k1gNnSc4	předmostí
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
as-Sálih	as-Sálih	k1gMnSc1	as-Sálih
zatím	zatím	k6eAd1	zatím
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
armádou	armáda	k1gFnSc7	armáda
vyčkával	vyčkávat	k5eAaImAgInS	vyčkávat
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Nilu	Nil	k1gInSc2	Nil
za	za	k7c4	za
Damiettou	Damiettá	k1gFnSc4	Damiettá
<g/>
.	.	kIx.	.
</s>
<s>
Obránci	obránce	k1gMnPc1	obránce
Damietty	Damietta	k1gFnSc2	Damietta
tak	tak	k9	tak
město	město	k1gNnSc4	město
raději	rád	k6eAd2	rád
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nP	by
riskovali	riskovat	k5eAaBmAgMnP	riskovat
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
boji	boj	k1gInSc6	boj
nebo	nebo	k8xC	nebo
hladem	hlad	k1gInSc7	hlad
při	při	k7c6	při
obležení	obležení	k1gNnSc6	obležení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Damietta	Damietta	k1gFnSc1	Damietta
tak	tak	k6eAd1	tak
padla	padnout	k5eAaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
překvapených	překvapený	k2eAgMnPc2d1	překvapený
útočníků	útočník	k1gMnPc2	útočník
po	po	k7c6	po
pouhých	pouhý	k2eAgNnPc6d1	pouhé
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
boje	boj	k1gInPc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
ve	v	k7c6	v
městě	město	k1gNnSc6	město
získali	získat	k5eAaPmAgMnP	získat
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
válečného	válečný	k2eAgInSc2d1	válečný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
53	[number]	k4	53
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
od	od	k7c2	od
páté	pátá	k1gFnSc2	pátá
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
pouhého	pouhý	k2eAgNnSc2d1	pouhé
jediného	jediné	k1gNnSc2	jediné
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
města	město	k1gNnSc2	město
nařídil	nařídit	k5eAaPmAgInS	nařídit
Ludvík	Ludvík	k1gMnSc1	Ludvík
zvýšit	zvýšit	k5eAaPmF	zvýšit
opevnění	opevnění	k1gNnSc4	opevnění
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
opravit	opravit	k5eAaPmF	opravit
příkopy	příkop	k1gInPc4	příkop
a	a	k8xC	a
posílit	posílit	k5eAaPmF	posílit
zdi	zeď	k1gFnSc3	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
také	také	k9	také
obnovit	obnovit	k5eAaPmF	obnovit
kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
mešitu	mešita	k1gFnSc4	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
i	i	k9	i
o	o	k7c4	o
kolonizaci	kolonizace	k1gFnSc4	kolonizace
dobytých	dobytý	k2eAgNnPc2d1	dobyté
území	území	k1gNnPc2	území
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Zdržení	zdržení	k1gNnSc1	zdržení
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
neumožnilo	umožnit	k5eNaPmAgNnS	umožnit
králi	král	k1gMnSc3	král
zformovat	zformovat	k5eAaPmF	zformovat
armádu	armáda	k1gFnSc4	armáda
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
na	na	k7c4	na
jih	jih	k1gInSc4	jih
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
nilských	nilský	k2eAgFnPc2d1	nilská
záplav	záplava	k1gFnPc2	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgFnPc1d1	živá
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
neúspěch	neúspěch	k1gInSc4	neúspěch
páté	pátý	k4xOgFnSc2	pátý
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
v	v	k7c6	v
dobytém	dobytý	k2eAgNnSc6d1	dobyté
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Získaného	získaný	k2eAgInSc2d1	získaný
času	čas	k1gInSc2	čas
využil	využít	k5eAaPmAgMnS	využít
Ludvík	Ludvík	k1gMnSc1	Ludvík
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
posíleno	posílit	k5eAaPmNgNnS	posílit
dalšími	další	k2eAgInPc7d1	další
oddíly	oddíl	k1gInPc7	oddíl
včetně	včetně	k7c2	včetně
oddílu	oddíl	k1gInSc2	oddíl
jeho	jeho	k3xOp3gMnSc2	jeho
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Alfonse	Alfons	k1gMnSc2	Alfons
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
začínalo	začínat	k5eAaImAgNnS	začínat
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
období	období	k1gNnSc1	období
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ležení	ležení	k1gNnSc6	ležení
u	u	k7c2	u
Damietty	Damietta	k1gFnSc2	Damietta
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
křižáci	křižák	k1gMnPc1	křižák
dva	dva	k4xCgInPc4	dva
možné	možný	k2eAgInPc4d1	možný
scénáře	scénář	k1gInPc4	scénář
dalšího	další	k2eAgInSc2d1	další
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
část	část	k1gFnSc1	část
vojska	vojsko	k1gNnSc2	vojsko
chtěla	chtít	k5eAaImAgFnS	chtít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
relativně	relativně	k6eAd1	relativně
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Alexandrii	Alexandrie	k1gFnSc4	Alexandrie
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Robertem	Robert	k1gMnSc7	Robert
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
chtěli	chtít	k5eAaImAgMnP	chtít
zaútočit	zaútočit	k5eAaPmF	zaútočit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
Káhiru	Káhira	k1gFnSc4	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
kdo	kdo	k3yRnSc1	kdo
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mu	on	k3xPp3gMnSc3	on
nejprve	nejprve	k6eAd1	nejprve
roztít	roztít	k5eAaPmF	roztít
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
byl	být	k5eAaImAgInS	být
Ludvíkův	Ludvíkův	k2eAgInSc1d1	Ludvíkův
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
finančních	finanční	k2eAgInPc6d1	finanční
zdrojích	zdroj	k1gInPc6	zdroj
závisela	záviset	k5eAaImAgFnS	záviset
většina	většina	k1gFnSc1	většina
křižáckých	křižácký	k2eAgMnPc2d1	křižácký
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
dalšího	další	k2eAgInSc2d1	další
křižáckého	křižácký	k2eAgInSc2d1	křižácký
postupu	postup	k1gInSc2	postup
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
města	město	k1gNnSc2	město
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Ludvík	Ludvík	k1gMnSc1	Ludvík
v	v	k7c6	v
Damiettě	Damietta	k1gFnSc6	Damietta
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
a	a	k8xC	a
stálou	stálý	k2eAgFnSc4d1	stálá
kapitulu	kapitula	k1gFnSc4	kapitula
katedrálních	katedrální	k2eAgMnPc2d1	katedrální
kanovníků	kanovník	k1gMnPc2	kanovník
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1249	[number]	k4	1249
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
Damietty	Damietta	k1gFnSc2	Damietta
ke	k	k7c3	k
Káhiře	Káhira	k1gFnSc3	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
postupovalo	postupovat	k5eAaImAgNnS	postupovat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
denně	denně	k6eAd1	denně
neurazilo	urazit	k5eNaPmAgNnS	urazit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
vojsko	vojsko	k1gNnSc1	vojsko
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
početná	početný	k2eAgFnSc1d1	početná
flotila	flotila	k1gFnSc1	flotila
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
na	na	k7c6	na
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pomalého	pomalý	k2eAgInSc2d1	pomalý
křižáckého	křižácký	k2eAgInSc2d1	křižácký
postupu	postup	k1gInSc2	postup
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
u	u	k7c2	u
al-Mansúry	al-Mansúra	k1gFnSc2	al-Mansúra
egyptský	egyptský	k2eAgMnSc1d1	egyptský
sultán	sultán	k1gMnSc1	sultán
as-Sálih	as-Sálih	k1gMnSc1	as-Sálih
Ajjúb	Ajjúb	k1gMnSc1	Ajjúb
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
přešla	přejít	k5eAaPmAgFnS	přejít
reálná	reálný	k2eAgFnSc1d1	reálná
moc	moc	k1gFnSc1	moc
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
vdovu	vdova	k1gFnSc4	vdova
Šadžar	Šadžar	k1gMnSc1	Šadžar
ad-Durr	ad-Durr	k1gMnSc1	ad-Durr
a	a	k8xC	a
Fachr	Fachr	k1gMnSc1	Fachr
ad-Dína	ad-Dín	k1gInSc2	ad-Dín
<g/>
.	.	kIx.	.
</s>
<s>
Sultánovu	sultánův	k2eAgMnSc3d1	sultánův
dědici	dědic	k1gMnSc3	dědic
al-Mu	al-M	k1gMnSc3	al-M
<g/>
'	'	kIx"	'
<g/>
azzamovi	azzam	k1gMnSc3	azzam
Túránšáhovi	Túránšáh	k1gMnSc3	Túránšáh
totiž	totiž	k9	totiž
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zastavit	zastavit	k5eAaPmF	zastavit
křižácký	křižácký	k2eAgInSc4d1	křižácký
postup	postup	k1gInSc4	postup
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
utkali	utkat	k5eAaPmAgMnP	utkat
s	s	k7c7	s
templářským	templářský	k2eAgInSc7d1	templářský
předvojem	předvoj	k1gInSc7	předvoj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
dorazili	dorazit	k5eAaPmAgMnP	dorazit
křižáci	křižák	k1gMnPc1	křižák
k	k	k7c3	k
nilskému	nilský	k2eAgInSc3d1	nilský
kanálu	kanál	k1gInSc2	kanál
Bahr	Bahra	k1gFnPc2	Bahra
as-Saghír	as-Saghír	k1gInSc1	as-Saghír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
egyptskému	egyptský	k2eAgInSc3d1	egyptský
táboru	tábor	k1gInSc3	tábor
postavili	postavit	k5eAaPmAgMnP	postavit
vlastní	vlastní	k2eAgInSc4d1	vlastní
opevněný	opevněný	k2eAgInSc4d1	opevněný
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
útočila	útočit	k5eAaImAgFnS	útočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rameno	rameno	k1gNnSc1	rameno
Nilu	Nil	k1gInSc2	Nil
bránilo	bránit	k5eAaImAgNnS	bránit
uskutečnění	uskutečnění	k1gNnSc1	uskutečnění
velké	velký	k2eAgFnSc2d1	velká
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
změnilo	změnit	k5eAaPmAgNnS	změnit
až	až	k9	až
několik	několik	k4yIc1	několik
egyptských	egyptský	k2eAgMnPc2d1	egyptský
zběhů	zběh	k1gMnPc2	zběh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
křižáky	křižák	k1gInPc4	křižák
informovali	informovat	k5eAaBmAgMnP	informovat
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
hlubokého	hluboký	k2eAgInSc2d1	hluboký
brodu	brod	k1gInSc2	brod
níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgFnSc4d1	trvající
poziční	poziční	k2eAgFnSc4d1	poziční
válku	válka	k1gFnSc4	válka
neměl	mít	k5eNaImAgInS	mít
dostatek	dostatek	k1gInSc1	dostatek
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
útok	útok	k1gInSc1	útok
přes	přes	k7c4	přes
brod	brod	k1gInSc4	brod
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
vody	voda	k1gFnSc2	voda
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
přebrodění	přebrodění	k1gNnSc4	přebrodění
jen	jen	k8xS	jen
jezdcům	jezdec	k1gMnPc3	jezdec
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Rytíři	rytíř	k1gMnPc1	rytíř
měli	mít	k5eAaImAgMnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
protější	protější	k2eAgInSc4d1	protější
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
měla	mít	k5eAaImAgFnS	mít
pěchota	pěchota	k1gFnSc1	pěchota
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Předsunutý	předsunutý	k2eAgInSc1d1	předsunutý
oddíl	oddíl	k1gInSc1	oddíl
pověřený	pověřený	k2eAgInSc1d1	pověřený
úkolem	úkol	k1gInSc7	úkol
zajištění	zajištění	k1gNnSc2	zajištění
protějšího	protější	k2eAgInSc2d1	protější
břehu	břeh	k1gInSc2	břeh
vedl	vést	k5eAaImAgMnS	vést
králův	králův	k2eAgMnSc1d1	králův
bratr	bratr	k1gMnSc1	bratr
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
posílen	posílit	k5eAaPmNgInS	posílit
o	o	k7c4	o
templáře	templář	k1gMnPc4	templář
a	a	k8xC	a
johanity	johanita	k1gMnPc4	johanita
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
oddíl	oddíl	k1gInSc1	oddíl
vedený	vedený	k2eAgInSc1d1	vedený
Vilémem	Vilém	k1gMnSc7	Vilém
Longespéem	Longespé	k1gMnSc7	Longespé
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
rameno	rameno	k1gNnSc4	rameno
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
nevyčkal	vyčkat	k5eNaPmAgMnS	vyčkat
na	na	k7c4	na
Ludvíka	Ludvík	k1gMnSc4	Ludvík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
egyptský	egyptský	k2eAgInSc4d1	egyptský
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastihl	zastihnout	k5eAaPmAgInS	zastihnout
zcela	zcela	k6eAd1	zcela
nepřipravené	připravený	k2eNgMnPc4d1	nepřipravený
obránce	obránce	k1gMnPc4	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
útoku	útok	k1gInSc2	útok
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
velitel	velitel	k1gMnSc1	velitel
egyptských	egyptský	k2eAgNnPc2d1	egyptské
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
faktický	faktický	k2eAgMnSc1d1	faktický
vládce	vládce	k1gMnSc1	vládce
Egypta	Egypt	k1gInSc2	Egypt
Fachr	Fachr	k1gMnSc1	Fachr
ad-Dín	ad-Dín	k1gMnSc1	ad-Dín
Júsuf	Júsuf	k1gMnSc1	Júsuf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
zaskočen	zaskočit	k5eAaPmNgInS	zaskočit
při	při	k7c6	při
ranní	ranní	k2eAgFnSc6d1	ranní
očistě	očista	k1gFnSc6	očista
<g/>
.	.	kIx.	.
</s>
<s>
Zaskočení	zaskočený	k2eAgMnPc1d1	zaskočený
Egypťané	Egypťan	k1gMnPc1	Egypťan
hledali	hledat	k5eAaImAgMnP	hledat
záchranu	záchrana	k1gFnSc4	záchrana
v	v	k7c6	v
al-Mansuře	al-Mansura	k1gFnSc6	al-Mansura
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
nadšení	nadšený	k2eAgMnPc1d1	nadšený
úspěchem	úspěch	k1gInSc7	úspěch
nevyčkali	vyčkat	k5eNaPmAgMnP	vyčkat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
shromáždí	shromáždit	k5eAaPmIp3nS	shromáždit
celé	celý	k2eAgNnSc1d1	celé
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
a	a	k8xC	a
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
nepřátel	nepřítel	k1gMnPc2	nepřítel
do	do	k7c2	do
al-Mansúry	al-Mansúra	k1gFnSc2	al-Mansúra
<g/>
.	.	kIx.	.
</s>
<s>
Úzké	úzké	k1gInPc4	úzké
a	a	k8xC	a
spletité	spletitý	k2eAgFnPc4d1	spletitá
městské	městský	k2eAgFnPc4d1	městská
uličky	ulička	k1gFnPc4	ulička
však	však	k9	však
útočící	útočící	k2eAgNnSc1d1	útočící
jízdní	jízdní	k2eAgNnSc1d1	jízdní
vojsko	vojsko	k1gNnSc1	vojsko
zbavily	zbavit	k5eAaPmAgInP	zbavit
všech	všecek	k3xTgFnPc2	všecek
výhod	výhoda	k1gFnPc2	výhoda
a	a	k8xC	a
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
triumf	triumf	k1gInSc1	triumf
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
masakr	masakr	k1gInSc4	masakr
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
i	i	k9	i
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
jízdním	jízdní	k2eAgInSc7d1	jízdní
oddílem	oddíl	k1gInSc7	oddíl
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
opakovaným	opakovaný	k2eAgMnPc3d1	opakovaný
egyptským	egyptský	k2eAgMnPc3d1	egyptský
protiútokům	protiútok	k1gInPc3	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
se	se	k3xPyFc4	se
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
vojsku	vojsko	k1gNnSc3	vojsko
podařilo	podařit	k5eAaPmAgNnS	podařit
opanovat	opanovat	k5eAaPmF	opanovat
celé	celý	k2eAgNnSc4d1	celé
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
sice	sice	k8xC	sice
byli	být	k5eAaImAgMnP	být
poražení	poražený	k1gMnPc1	poražený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
vojsko	vojsko	k1gNnSc1	vojsko
nebylo	být	k5eNaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
a	a	k8xC	a
al-Mansúra	al-Mansúro	k1gNnPc1	al-Mansúro
nebyla	být	k5eNaImAgNnP	být
dobyta	dobýt	k5eAaPmNgNnP	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
tak	tak	k6eAd1	tak
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
stále	stále	k6eAd1	stále
blokována	blokován	k2eAgFnSc1d1	blokována
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
egyptského	egyptský	k2eAgInSc2d1	egyptský
tábora	tábor	k1gInSc2	tábor
u	u	k7c2	u
al-Mansúry	al-Mansúra	k1gFnSc2	al-Mansúra
nový	nový	k2eAgMnSc1d1	nový
sultán	sultán	k1gMnSc1	sultán
Turanšáh	Turanšáh	k1gMnSc1	Turanšáh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupujícím	postupující	k2eAgInSc7d1	postupující
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc1	postavení
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
vojska	vojsko	k1gNnSc2	vojsko
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
muslimské	muslimský	k2eAgNnSc1d1	muslimské
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
doplňováno	doplňovat	k5eAaImNgNnS	doplňovat
posilami	posila	k1gFnPc7	posila
a	a	k8xC	a
válečným	válečný	k2eAgInSc7d1	válečný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
křižákům	křižák	k1gMnPc3	křižák
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
nedostávat	dostávat	k5eNaImF	dostávat
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
je	on	k3xPp3gFnPc4	on
trápit	trápit	k5eAaImF	trápit
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
především	především	k9	především
kurděje	kurděje	k1gFnPc1	kurděje
a	a	k8xC	a
úplavice	úplavice	k1gFnPc1	úplavice
<g/>
.	.	kIx.	.
</s>
<s>
Egypťanům	Egypťan	k1gMnPc3	Egypťan
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
podařilo	podařit	k5eAaPmAgNnS	podařit
přetáhnout	přetáhnout	k5eAaPmF	přetáhnout
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
část	část	k1gFnSc1	část
člunů	člun	k1gInPc2	člun
níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
křižáckého	křižácký	k2eAgInSc2d1	křižácký
tábora	tábor	k1gInSc2	tábor
a	a	k8xC	a
odříznout	odříznout	k5eAaPmF	odříznout
tak	tak	k6eAd1	tak
křižácký	křižácký	k2eAgInSc4d1	křižácký
tábor	tábor	k1gInSc4	tábor
od	od	k7c2	od
zásob	zásoba	k1gFnPc2	zásoba
v	v	k7c6	v
Damiettě	Damietta	k1gFnSc6	Damietta
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršující	zhoršující	k2eAgFnPc1d1	zhoršující
se	se	k3xPyFc4	se
podmínky	podmínka	k1gFnPc1	podmínka
vedly	vést	k5eAaImAgFnP	vést
Ludvíka	Ludvík	k1gMnSc4	Ludvík
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
kanálu	kanál	k1gInSc2	kanál
Bahr	Bahr	k1gMnSc1	Bahr
as-Saghír	as-Saghír	k1gMnSc1	as-Saghír
<g/>
.	.	kIx.	.
</s>
<s>
Probíhající	probíhající	k2eAgNnPc1d1	probíhající
diplomatická	diplomatický	k2eAgNnPc1d1	diplomatické
jednání	jednání	k1gNnPc1	jednání
mezi	mezi	k7c7	mezi
křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
Egypťany	Egypťan	k1gMnPc7	Egypťan
o	o	k7c6	o
výměně	výměna	k1gFnSc6	výměna
Damietty	Damietta	k1gFnSc2	Damietta
za	za	k7c4	za
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
také	také	k9	také
nevedla	vést	k5eNaImAgFnS	vést
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
postavení	postavení	k1gNnSc1	postavení
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
proto	proto	k8xC	proto
protahoval	protahovat	k5eAaImAgMnS	protahovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
nabízel	nabízet	k5eAaImAgMnS	nabízet
nepřijatelné	přijatelný	k2eNgFnPc4d1	nepřijatelná
podmínky	podmínka	k1gFnPc4	podmínka
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1250	[number]	k4	1250
se	se	k3xPyFc4	se
nemocný	nemocný	k1gMnSc1	nemocný
Ludvík	Ludvík	k1gMnSc1	Ludvík
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
k	k	k7c3	k
definitivnímu	definitivní	k2eAgInSc3d1	definitivní
ústupu	ústup	k1gInSc3	ústup
k	k	k7c3	k
Damiettě	Damietta	k1gFnSc3	Damietta
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
neponechal	ponechat	k5eNaPmAgMnS	ponechat
nic	nic	k6eAd1	nic
náhodě	náhoda	k1gFnSc3	náhoda
a	a	k8xC	a
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
chtěl	chtít	k5eAaImAgMnS	chtít
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
k	k	k7c3	k
Damiettě	Damietta	k1gFnSc3	Damietta
<g/>
.	.	kIx.	.
</s>
<s>
Soustředěné	soustředěný	k2eAgInPc1d1	soustředěný
muslimské	muslimský	k2eAgInPc1d1	muslimský
útoky	útok	k1gInPc1	útok
<g/>
,	,	kIx,	,
vyčerpání	vyčerpání	k1gNnPc1	vyčerpání
křižáků	křižák	k1gInPc2	křižák
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc1	jejich
upadající	upadající	k2eAgFnSc1d1	upadající
morálka	morálka	k1gFnSc1	morálka
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
ústupu	ústup	k1gInSc2	ústup
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
praktickému	praktický	k2eAgInSc3d1	praktický
rozpadu	rozpad	k1gInSc3	rozpad
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
křižácké	křižácký	k2eAgFnSc2d1	křižácká
armády	armáda	k1gFnSc2	armáda
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
buď	buď	k8xC	buď
zmasakrována	zmasakrován	k2eAgFnSc1d1	zmasakrován
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malému	malý	k2eAgNnSc3d1	malé
množství	množství	k1gNnSc3	množství
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
až	až	k9	až
k	k	k7c3	k
Damiettě	Damietta	k1gFnSc3	Damietta
<g/>
.	.	kIx.	.
</s>
<s>
Kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
i	i	k9	i
samotný	samotný	k2eAgMnSc1d1	samotný
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
zajetí	zajetí	k1gNnSc6	zajetí
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
doprovodem	doprovod	k1gInSc7	doprovod
v	v	k7c6	v
řetězech	řetěz	k1gInPc6	řetěz
převezen	převezen	k2eAgMnSc1d1	převezen
do	do	k7c2	do
al-Mansúry	al-Mansúra	k1gFnSc2	al-Mansúra
<g/>
.	.	kIx.	.
</s>
<s>
Zajetí	zajetí	k1gNnSc1	zajetí
krále	král	k1gMnSc2	král
a	a	k8xC	a
rozbití	rozbití	k1gNnSc6	rozbití
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
znamenalo	znamenat	k5eAaImAgNnS	znamenat
absolutní	absolutní	k2eAgNnSc1d1	absolutní
muslimské	muslimský	k2eAgNnSc1d1	muslimské
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zajetí	zajetí	k1gNnSc6	zajetí
krále	král	k1gMnSc2	král
hodlali	hodlat	k5eAaImAgMnP	hodlat
obránci	obránce	k1gMnPc1	obránce
Damietty	Damietta	k1gMnSc2	Damietta
město	město	k1gNnSc1	město
okamžitě	okamžitě	k6eAd1	okamžitě
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
záměru	záměra	k1gFnSc4	záměra
zabránila	zabránit	k5eAaPmAgFnS	zabránit
královna	královna	k1gFnSc1	královna
apelem	apel	k1gInSc7	apel
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
slibem	slib	k1gInSc7	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
všechny	všechen	k3xTgInPc1	všechen
jejich	jejich	k3xOp3gInPc1	jejich
náklady	náklad	k1gInPc1	náklad
včetně	včetně	k7c2	včetně
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
částku	částka	k1gFnSc4	částka
360	[number]	k4	360
000	[number]	k4	000
tourských	tourský	k2eAgInPc2d1	tourský
livrů	livr	k1gInPc2	livr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
královně	královna	k1gFnSc3	královna
podařilo	podařit	k5eAaPmAgNnS	podařit
Damiettu	Damietta	k1gMnSc4	Damietta
udržet	udržet	k5eAaPmF	udržet
<g/>
,	,	kIx,	,
hrálo	hrát	k5eAaImAgNnS	hrát
později	pozdě	k6eAd2	pozdě
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
o	o	k7c6	o
Ludvíkově	Ludvíkův	k2eAgNnSc6d1	Ludvíkovo
propuštění	propuštění	k1gNnSc6	propuštění
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Provedený	provedený	k2eAgInSc1d1	provedený
převrat	převrat	k1gInSc1	převrat
nic	nic	k3yNnSc4	nic
nezměnil	změnit	k5eNaPmAgInS	změnit
na	na	k7c6	na
dojednané	dojednaný	k2eAgFnSc6d1	dojednaná
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
propuštěn	propustit	k5eAaPmNgInS	propustit
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
a	a	k8xC	a
Damietta	Damietta	k1gFnSc1	Damietta
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
muslimům	muslim	k1gMnPc3	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
byla	být	k5eAaImAgFnS	být
zaplacena	zaplacen	k2eAgFnSc1d1	zaplacena
polovina	polovina	k1gFnSc1	polovina
výkupného	výkupné	k1gNnSc2	výkupné
za	za	k7c4	za
zajaté	zajatý	k2eAgInPc4d1	zajatý
křižáky	křižák	k1gInPc4	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
shromažďování	shromažďování	k1gNnSc6	shromažďování
a	a	k8xC	a
počítání	počítání	k1gNnSc6	počítání
výkupného	výkupné	k1gNnSc2	výkupné
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chybí	chybět	k5eAaImIp3nS	chybět
ještě	ještě	k9	ještě
30	[number]	k4	30
000	[number]	k4	000
livrů	livr	k1gInPc2	livr
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
Jeana	Jean	k1gMnSc2	Jean
de	de	k?	de
Joinville	Joinvill	k1gMnSc2	Joinvill
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
chybějící	chybějící	k2eAgFnSc1d1	chybějící
částka	částka	k1gFnSc1	částka
vypůjčila	vypůjčit	k5eAaPmAgFnS	vypůjčit
od	od	k7c2	od
templářů	templář	k1gMnPc2	templář
<g/>
.	.	kIx.	.
</s>
<s>
Templářský	templářský	k2eAgMnSc1d1	templářský
komtur	komtur	k1gMnSc1	komtur
Štěpán	Štěpán	k1gMnSc1	Štěpán
z	z	k7c2	z
Otricourtu	Otricourt	k1gInSc2	Otricourt
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
řád	řád	k1gInSc1	řád
nemůže	moct	k5eNaImIp3nS	moct
půjčovat	půjčovat	k5eAaImF	půjčovat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
svěřili	svěřit	k5eAaPmAgMnP	svěřit
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Joinville	Joinville	k6eAd1	Joinville
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
potřebné	potřebný	k2eAgInPc4d1	potřebný
peníze	peníz	k1gInPc4	peníz
až	až	k9	až
pod	pod	k7c7	pod
pohrůžkou	pohrůžka	k1gFnSc7	pohrůžka
použití	použití	k1gNnSc2	použití
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
poloviny	polovina	k1gFnSc2	polovina
výkupného	výkupné	k1gNnSc2	výkupné
a	a	k8xC	a
po	po	k7c4	po
propuštění	propuštění	k1gNnSc4	propuštění
všech	všecek	k3xTgMnPc2	všecek
významných	významný	k2eAgMnPc2d1	významný
zajatců	zajatec	k1gMnPc2	zajatec
odplul	odplout	k5eAaPmAgMnS	odplout
Ludvík	Ludvík	k1gMnSc1	Ludvík
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
do	do	k7c2	do
Akkonu	Akkon	k1gInSc2	Akkon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přistál	přistát	k5eAaPmAgInS	přistát
12	[number]	k4	12
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
svolal	svolat	k5eAaPmAgMnS	svolat
Ludvík	Ludvík	k1gMnSc1	Ludvík
v	v	k7c6	v
Akkonu	Akkon	k1gInSc6	Akkon
poradu	porada	k1gFnSc4	porada
svých	svůj	k3xOyFgMnPc2	svůj
leníků	leník	k1gMnPc2	leník
o	o	k7c6	o
budoucích	budoucí	k2eAgInPc6d1	budoucí
plánech	plán	k1gInPc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
totiž	totiž	k9	totiž
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
vyzývala	vyzývat	k5eAaImAgFnS	vyzývat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
k	k	k7c3	k
urychlenému	urychlený	k2eAgInSc3d1	urychlený
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1	Ludvíkův
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
flanderský	flanderský	k2eAgMnSc1d1	flanderský
hrabě	hrabě	k1gMnSc1	hrabě
radili	radit	k5eAaImAgMnP	radit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Ludvík	Ludvík	k1gMnSc1	Ludvík
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Královi	Králův	k2eAgMnPc1d1	Králův
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
vrátit	vrátit	k5eAaPmF	vrátit
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
král	král	k1gMnSc1	král
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
setrvání	setrvání	k1gNnSc4	setrvání
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Královi	Králův	k2eAgMnPc1d1	Králův
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
králem	král	k1gMnSc7	král
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
jen	jen	k9	jen
asi	asi	k9	asi
1400	[number]	k4	1400
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
Ludvíkova	Ludvíkův	k2eAgNnSc2d1	Ludvíkovo
setrvání	setrvání	k1gNnSc2	setrvání
ve	v	k7c6	v
Svaté	svatá	k1gFnSc6	svatá
zemí	zem	k1gFnPc2	zem
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
zbožnost	zbožnost	k1gFnSc1	zbožnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
snaha	snaha	k1gFnSc1	snaha
napravit	napravit	k5eAaPmF	napravit
neúspěchem	neúspěch	k1gInSc7	neúspěch
pošramocenou	pošramocený	k2eAgFnSc4d1	pošramocená
pověst	pověst	k1gFnSc4	pověst
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
Ludvík	Ludvík	k1gMnSc1	Ludvík
vynaložil	vynaložit	k5eAaPmAgMnS	vynaložit
velké	velký	k2eAgFnSc2d1	velká
částky	částka	k1gFnSc2	částka
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
opevnění	opevnění	k1gNnSc2	opevnění
Jaffy	Jaffa	k1gFnSc2	Jaffa
<g/>
,	,	kIx,	,
Caesareje	Caesarej	k1gInSc2	Caesarej
<g/>
,	,	kIx,	,
Sidonu	Sidon	k1gInSc2	Sidon
a	a	k8xC	a
Akkonu	Akkon	k1gInSc2	Akkon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jaffě	Jaffa	k1gFnSc6	Jaffa
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Ludvíka	Ludvík	k1gMnSc4	Ludvík
i	i	k9	i
nový	nový	k2eAgMnSc1d1	nový
antiochijský	antiochijský	k2eAgMnSc1d1	antiochijský
kníže	kníže	k1gMnSc1	kníže
Bohemund	Bohemund	k1gMnSc1	Bohemund
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jemu	on	k3xPp3gMnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Ludvík	Ludvík	k1gMnSc1	Ludvík
potřebné	potřebný	k2eAgFnPc4d1	potřebná
finance	finance	k1gFnPc4	finance
na	na	k7c4	na
posílení	posílení	k1gNnSc4	posílení
opevnění	opevnění	k1gNnSc2	opevnění
Antiochie	Antiochie	k1gFnSc2	Antiochie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
propuštění	propuštění	k1gNnSc4	propuštění
všech	všecek	k3xTgInPc2	všecek
křižáků	křižák	k1gInPc2	křižák
držených	držený	k2eAgInPc2d1	držený
v	v	k7c6	v
egyptském	egyptský	k2eAgNnSc6d1	egyptské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zajatců	zajatec	k1gMnPc2	zajatec
získal	získat	k5eAaPmAgMnS	získat
Ludvík	Ludvík	k1gMnSc1	Ludvík
od	od	k7c2	od
vůdce	vůdce	k1gMnSc2	vůdce
egyptských	egyptský	k2eAgMnPc2d1	egyptský
mameluků	mameluk	k1gMnPc2	mameluk
darem	dar	k1gInSc7	dar
i	i	k9	i
zebru	zebra	k1gFnSc4	zebra
a	a	k8xC	a
slona	slon	k1gMnSc4	slon
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
matka	matka	k1gFnSc1	matka
Blanka	Blanka	k1gFnSc1	Blanka
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
práva	právo	k1gNnSc2	právo
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1253	[number]	k4	1253
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
rok	rok	k1gInSc4	rok
než	než	k8xS	než
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
zanechal	zanechat	k5eAaPmAgMnS	zanechat
asi	asi	k9	asi
stovku	stovka	k1gFnSc4	stovka
rytířů	rytíř	k1gMnPc2	rytíř
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Geoffroye	Geoffroy	k1gInSc2	Geoffroy
ze	z	k7c2	z
Sergines	Serginesa	k1gFnPc2	Serginesa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
stálého	stálý	k2eAgNnSc2d1	stálé
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
francouzského	francouzský	k2eAgInSc2d1	francouzský
regimentu	regiment	k1gInSc2	regiment
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
křižáckých	křižácký	k2eAgInPc2d1	křižácký
států	stát	k1gInPc2	stát
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
státy	stát	k1gInPc4	stát
mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgFnPc4d2	významnější
než	než	k8xS	než
občasné	občasný	k2eAgFnPc4d1	občasná
křížové	křížový	k2eAgFnPc4d1	křížová
výpravy	výprava	k1gFnPc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
nejlépe	dobře	k6eAd3	dobře
naplánovanou	naplánovaný	k2eAgFnSc7d1	naplánovaná
a	a	k8xC	a
zorganizovanou	zorganizovaný	k2eAgFnSc7d1	zorganizovaná
křížovou	křížový	k2eAgFnSc7d1	křížová
výpravou	výprava	k1gFnSc7	výprava
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
neúspěch	neúspěch	k1gInSc1	neúspěch
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
však	však	k9	však
nevedl	vést	k5eNaImAgMnS	vést
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
nadšení	nadšení	k1gNnSc2	nadšení
pro	pro	k7c4	pro
křižáckou	křižácký	k2eAgFnSc4d1	křižácká
věc	věc	k1gFnSc4	věc
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1254	[number]	k4	1254
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
dětmi	dítě	k1gFnPc7	dítě
narozenými	narozený	k2eAgInPc7d1	narozený
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
(	(	kIx(	(
<g/>
Janem	Jan	k1gMnSc7	Jan
Tristanem	Tristan	k1gInSc7	Tristan
<g/>
,	,	kIx,	,
Petrem	Petr	k1gMnSc7	Petr
a	a	k8xC	a
Blankou	Blanka	k1gFnSc7	Blanka
<g/>
)	)	kIx)	)
nalodil	nalodit	k5eAaPmAgMnS	nalodit
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
domů	dům	k1gInPc2	dům
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
trvající	trvající	k2eAgFnSc3d1	trvající
plavbě	plavba	k1gFnSc3	plavba
se	se	k3xPyFc4	se
vylodil	vylodit	k5eAaPmAgMnS	vylodit
na	na	k7c6	na
evropském	evropský	k2eAgNnSc6d1	Evropské
pobřeží	pobřeží	k1gNnSc6	pobřeží
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c4	v
Salins	Salins	k1gInSc4	Salins
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Hyè	Hyè	k1gMnSc1	Hyè
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Jeana	Jean	k1gMnSc2	Jean
de	de	k?	de
Joinville	Joinvill	k1gMnSc2	Joinvill
do	do	k7c2	do
Aix-en-Provence	Aixn-Provence	k1gFnSc2	Aix-en-Provence
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
pouť	pouť	k1gFnSc4	pouť
ke	k	k7c3	k
svaté	svatá	k1gFnSc3	svatá
Máří	Máří	k?	Máří
Magdaléně	Magdaléna	k1gFnSc3	Magdaléna
de	de	k?	de
Sainte-Beaume	Sainte-Beaum	k1gInSc5	Sainte-Beaum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
Beaucaire	Beaucair	k1gMnSc5	Beaucair
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
s	s	k7c7	s
Joinvillem	Joinvill	k1gInSc7	Joinvill
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
území	území	k1gNnSc6	území
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
přes	přes	k7c4	přes
Aigues-Mortes	Aigues-Mortes	k1gInSc4	Aigues-Mortes
<g/>
,	,	kIx,	,
Saint-Gilles	Saint-Gilles	k1gMnSc1	Saint-Gilles
<g/>
,	,	kIx,	,
Nîmes	Nîmes	k1gMnSc1	Nîmes
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Puy	Puy	k1gMnSc1	Puy
<g/>
,	,	kIx,	,
Clermont	Clermont	k1gMnSc1	Clermont
<g/>
,	,	kIx,	,
do	do	k7c2	do
baziliky	bazilika	k1gFnSc2	bazilika
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uložil	uložit	k5eAaPmAgInS	uložit
korouhev	korouhev	k1gFnSc4	korouhev
a	a	k8xC	a
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1254	[number]	k4	1254
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Poznamenán	poznamenán	k2eAgInSc1d1	poznamenán
prožitým	prožitý	k2eAgNnSc7d1	prožité
utrpením	utrpení	k1gNnSc7	utrpení
a	a	k8xC	a
rozčarováním	rozčarování	k1gNnSc7	rozčarování
z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
stal	stát	k5eAaPmAgMnS	stát
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zbožnějším	zbožní	k2eAgInSc7d2	zbožnější
a	a	k8xC	a
pokornějším	pokorný	k2eAgInSc7d2	pokornější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
přísnou	přísný	k2eAgFnSc4d1	přísná
morálku	morálka	k1gFnSc4	morálka
<g/>
,	,	kIx,	,
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
prostituci	prostituce	k1gFnSc3	prostituce
<g/>
,	,	kIx,	,
hazardní	hazardní	k2eAgMnPc4d1	hazardní
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
království	království	k1gNnSc6	království
vysílal	vysílat	k5eAaImAgInS	vysílat
kontrolory	kontrolor	k1gMnPc4	kontrolor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odstranili	odstranit	k5eAaPmAgMnP	odstranit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
příčilo	příčit	k5eAaImAgNnS	příčit
Božím	boží	k2eAgNnSc7d1	boží
přikázáním	přikázání	k1gNnSc7	přikázání
<g/>
.	.	kIx.	.
</s>
<s>
Prostý	prostý	k2eAgInSc1d1	prostý
šat	šat	k1gInSc1	šat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Ludvík	Ludvík	k1gMnSc1	Ludvík
oblékl	obléct	k5eAaPmAgMnS	obléct
jako	jako	k9	jako
účastník	účastník	k1gMnSc1	účastník
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
odkládal	odkládat	k5eAaImAgInS	odkládat
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
a	a	k8xC	a
v	v	k7c6	v
pití	pití	k1gNnSc6	pití
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
střídmější	střídmý	k2eAgMnSc1d2	střídmější
než	než	k8xS	než
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prostého	prostý	k2eAgInSc2d1	prostý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Ludvík	Ludvík	k1gMnSc1	Ludvík
postupně	postupně	k6eAd1	postupně
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
přísné	přísný	k2eAgFnSc3d1	přísná
střídmosti	střídmost	k1gFnSc3	střídmost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
jeho	jeho	k3xOp3gFnSc2	jeho
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkou	myšlenka	k1gFnSc7	myšlenka
platnou	platný	k2eAgFnSc7d1	platná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
mravního	mravní	k2eAgInSc2d1	mravní
a	a	k8xC	a
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
vystupňování	vystupňování	k1gNnSc3	vystupňování
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Aragonií	Aragonie	k1gFnSc7	Aragonie
<g/>
,	,	kIx,	,
když	když	k8xS	když
aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
opětovně	opětovně	k6eAd1	opětovně
vznesl	vznést	k5eAaPmAgMnS	vznést
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
město	město	k1gNnSc4	město
Millau	Millaus	k1gInSc2	Millaus
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc2	hrabství
Foix	Foix	k1gInSc1	Foix
a	a	k8xC	a
Gévaudan	Gévaudan	k1gInSc1	Gévaudan
a	a	k8xC	a
vikomství	vikomství	k1gNnSc1	vikomství
Fenouilledè	Fenouilledè	k1gFnSc2	Fenouilledè
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
však	však	k9	však
dali	dát	k5eAaPmAgMnP	dát
přednost	přednost	k1gFnSc4	přednost
mírovému	mírový	k2eAgNnSc3d1	Mírové
urovnání	urovnání	k1gNnSc3	urovnání
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1255	[number]	k4	1255
určili	určit	k5eAaPmAgMnP	určit
dva	dva	k4xCgInPc4	dva
církevní	církevní	k2eAgMnPc4d1	církevní
smírčí	smírčí	k2eAgMnPc4d1	smírčí
soudce	soudce	k1gMnPc4	soudce
(	(	kIx(	(
<g/>
Francouze	Francouz	k1gMnSc4	Francouz
a	a	k8xC	a
Aragonce	Aragonec	k1gMnSc4	Aragonec
<g/>
)	)	kIx)	)
a	a	k8xC	a
přijali	přijmout	k5eAaPmAgMnP	přijmout
jejich	jejich	k3xOp3gInSc4	jejich
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
podepsání	podepsání	k1gNnSc1	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c4	v
Corbeil	Corbeil	k1gInSc4	Corbeil
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1258	[number]	k4	1258
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1253	[number]	k4	1253
<g/>
-	-	kIx~	-
<g/>
1254	[number]	k4	1254
potlačil	potlačit	k5eAaPmAgMnS	potlačit
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
šlechtickou	šlechtický	k2eAgFnSc4d1	šlechtická
vzpouru	vzpoura	k1gFnSc4	vzpoura
v	v	k7c6	v
Gaskoňsku	Gaskoňsko	k1gNnSc6	Gaskoňsko
<g/>
,	,	kIx,	,
vracel	vracet	k5eAaImAgInS	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Ludvíkovo	Ludvíkův	k2eAgNnSc4d1	Ludvíkovo
pozvání	pozvání	k1gNnSc4	pozvání
strávil	strávit	k5eAaPmAgMnS	strávit
Vánoce	Vánoce	k1gFnPc4	Vánoce
roku	rok	k1gInSc2	rok
1254	[number]	k4	1254
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
čtyř	čtyři	k4xCgMnPc2	čtyři
dcer	dcera	k1gFnPc2	dcera
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
provensálského	provensálský	k2eAgMnSc4d1	provensálský
hraběte	hrabě	k1gMnSc4	hrabě
<g/>
:	:	kIx,	:
francouzské	francouzský	k2eAgFnSc2d1	francouzská
královny	královna	k1gFnSc2	královna
Markéty	Markéta	k1gFnSc2	Markéta
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
,	,	kIx,	,
Sanchy	Sancha	k1gFnPc1	Sancha
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Richarda	Richard	k1gMnSc2	Richard
z	z	k7c2	z
Cornwallu	Cornwall	k1gInSc2	Cornwall
a	a	k8xC	a
Beatrice	Beatrice	k1gFnSc2	Beatrice
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Karla	Karel	k1gMnSc2	Karel
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
oba	dva	k4xCgMnPc1	dva
králové	král	k1gMnPc1	král
pocítili	pocítit	k5eAaPmAgMnP	pocítit
sympatie	sympatie	k1gFnPc4	sympatie
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
dokonce	dokonce	k9	dokonce
daroval	darovat	k5eAaPmAgMnS	darovat
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
slona	slon	k1gMnSc4	slon
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
přivezl	přivézt	k5eAaPmAgMnS	přivézt
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1258	[number]	k4	1258
byla	být	k5eAaImAgNnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
a	a	k8xC	a
náročných	náročný	k2eAgInPc6d1	náročný
jednáních	jednání	k1gNnPc6	jednání
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
předal	předat	k5eAaPmAgMnS	předat
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
bývalé	bývalý	k2eAgFnSc2d1	bývalá
državy	država	k1gFnSc2	država
jeho	jeho	k3xOp3gMnPc2	jeho
předků	předek	k1gMnPc2	předek
-	-	kIx~	-
Limousin	Limousin	k1gMnSc1	Limousin
<g/>
,	,	kIx,	,
Périgord	Périgord	k1gMnSc1	Périgord
a	a	k8xC	a
Saintonge	Saintonge	k1gFnSc1	Saintonge
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Normandii	Normandie	k1gFnSc4	Normandie
<g/>
,	,	kIx,	,
Anjou	Anjá	k1gFnSc4	Anjá
a	a	k8xC	a
Poitou	Poitá	k1gFnSc4	Poitá
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
državy	država	k1gFnPc4	država
Akvitánii	Akvitánie	k1gFnSc4	Akvitánie
a	a	k8xC	a
Gaskoňsko	Gaskoňsko	k1gNnSc4	Gaskoňsko
přijal	přijmout	k5eAaPmAgMnS	přijmout
od	od	k7c2	od
Ludvíka	Ludvík	k1gMnSc2	Ludvík
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
za	za	k7c2	za
velkolepých	velkolepý	k2eAgFnPc2d1	velkolepá
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1259	[number]	k4	1259
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
zažilo	zažít	k5eAaPmAgNnS	zažít
celých	celý	k2eAgNnPc2d1	celé
patnáct	patnáct	k4xCc1	patnáct
let	léto	k1gNnPc2	léto
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1262	[number]	k4	1262
<g/>
-	-	kIx~	-
<g/>
1270	[number]	k4	1270
<g/>
)	)	kIx)	)
provedl	provést	k5eAaPmAgMnS	provést
Ludvík	Ludvík	k1gMnSc1	Ludvík
několik	několik	k4yIc1	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
mincovních	mincovní	k2eAgFnPc2d1	mincovní
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Nařízením	nařízení	k1gNnSc7	nařízení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1262	[number]	k4	1262
zavedl	zavést	k5eAaPmAgMnS	zavést
královské	královský	k2eAgFnPc4d1	královská
mince	mince	k1gFnPc4	mince
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc4d1	jediné
oběživo	oběživo	k1gNnSc4	oběživo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
jejich	jejich	k3xOp3gNnSc4	jejich
napodobování	napodobování	k1gNnSc4	napodobování
a	a	k8xC	a
padělání	padělání	k1gNnSc4	padělání
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
měly	mít	k5eAaImAgFnP	mít
mince	mince	k1gFnPc1	mince
šlechticů	šlechtic	k1gMnPc2	šlechtic
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
ražby	ražba	k1gFnSc2	ražba
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
oběh	oběh	k1gInSc1	oběh
však	však	k9	však
byl	být	k5eAaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
jen	jen	k9	jen
na	na	k7c4	na
jejich	jejich	k3xOp3gNnPc4	jejich
vlastní	vlastní	k2eAgNnPc4d1	vlastní
panství	panství	k1gNnPc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
nařízení	nařízení	k1gNnPc4	nařízení
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1262	[number]	k4	1262
<g/>
-	-	kIx~	-
<g/>
1265	[number]	k4	1265
žádala	žádat	k5eAaImAgFnS	žádat
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
královských	královský	k2eAgMnPc2d1	královský
poddaných	poddaný	k1gMnPc2	poddaný
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudou	být	k5eNaImBp3nP	být
používat	používat	k5eAaImF	používat
anglické	anglický	k2eAgFnSc2d1	anglická
mince	mince	k1gFnSc2	mince
esterliny	esterlina	k1gFnSc2	esterlina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejzazší	zadní	k2eAgInSc1d3	nejzazší
termín	termín	k1gInSc1	termín
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
srpen	srpen	k1gInSc1	srpen
1266	[number]	k4	1266
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1266	[number]	k4	1266
byla	být	k5eAaImAgFnS	být
dalším	další	k2eAgNnSc7d1	další
nařízením	nařízení	k1gNnSc7	nařízení
obnovena	obnovit	k5eAaPmNgFnS	obnovit
ražba	ražba	k1gFnSc1	ražba
pařížského	pařížský	k2eAgInSc2d1	pařížský
denáru	denár	k1gInSc2	denár
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
stanovena	stanovit	k5eAaPmNgFnS	stanovit
jeho	jeho	k3xOp3gFnSc1	jeho
váha	váha	k1gFnSc1	váha
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
drahého	drahý	k2eAgInSc2d1	drahý
kovu	kov	k1gInSc2	kov
v	v	k7c6	v
něm.	něm.	k?	něm.
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
i	i	k9	i
ražba	ražba	k1gFnSc1	ražba
nové	nový	k2eAgFnSc2d1	nová
mince	mince	k1gFnSc2	mince
tourského	tourský	k2eAgInSc2d1	tourský
groše	groš	k1gInSc2	groš
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1266	[number]	k4	1266
<g/>
-	-	kIx~	-
<g/>
1270	[number]	k4	1270
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgInS	vydat
nařízení	nařízení	k1gNnSc4	nařízení
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
zavedl	zavést	k5eAaPmAgMnS	zavést
novou	nový	k2eAgFnSc4d1	nová
minci	mince	k1gFnSc4	mince
zlatý	zlatý	k2eAgInSc4d1	zlatý
denár	denár	k1gInSc4	denár
se	s	k7c7	s
štítem	štít	k1gInSc7	štít
(	(	kIx(	(
<g/>
deniers	deniers	k1gInSc1	deniers
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
or	or	k?	or
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
écu	écu	k?	écu
<g/>
)	)	kIx)	)
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
zkráceně	zkráceně	k6eAd1	zkráceně
écu	écu	k?	écu
<g/>
.	.	kIx.	.
</s>
<s>
Ražba	ražba	k1gFnSc1	ražba
zlatých	zlatý	k2eAgFnPc2d1	zlatá
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Evropě	Evropa	k1gFnSc6	Evropa
patřila	patřit	k5eAaImAgFnS	patřit
spíše	spíše	k9	spíše
k	k	k7c3	k
výjimkám	výjimka	k1gFnPc3	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
obnovený	obnovený	k2eAgInSc1d1	obnovený
pařížský	pařížský	k2eAgInSc1d1	pařížský
denár	denár	k1gInSc1	denár
a	a	k8xC	a
écu	écu	k?	écu
se	se	k3xPyFc4	se
neujaly	ujmout	k5eNaPmAgFnP	ujmout
<g/>
,	,	kIx,	,
tourský	tourský	k2eAgInSc1d1	tourský
groš	groš	k1gInSc1	groš
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
i	i	k9	i
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osmá	osmý	k4xOgFnSc1	osmý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1266	[number]	k4	1266
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
chatrného	chatrný	k2eAgNnSc2d1	chatrné
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
křížové	křížový	k2eAgFnSc3d1	křížová
výpravě	výprava	k1gFnSc3	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1267	[number]	k4	1267
při	při	k7c6	při
rituálu	rituál	k1gInSc6	rituál
v	v	k7c6	v
Sainte-Chapelle	Sainte-Chapell	k1gInSc6	Sainte-Chapell
přijal	přijmout	k5eAaPmAgInS	přijmout
kříž	kříž	k1gInSc1	kříž
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
třemi	tři	k4xCgMnPc7	tři
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
rodinnými	rodinný	k2eAgMnPc7d1	rodinný
příslušníky	příslušník	k1gMnPc7	příslušník
a	a	k8xC	a
předními	přední	k2eAgMnPc7d1	přední
představiteli	představitel	k1gMnPc7	představitel
francouzské	francouzský	k2eAgFnSc2d1	francouzská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
Ludvíkovu	Ludvíkův	k2eAgFnSc4d1	Ludvíkova
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dostatečně	dostatečně	k6eAd1	dostatečně
zkoordinovat	zkoordinovat	k5eAaPmF	zkoordinovat
postup	postup	k1gInSc4	postup
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
křižáckých	křižácký	k2eAgNnPc2d1	křižácké
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
už	už	k6eAd1	už
červnu	červen	k1gInSc6	červen
1269	[number]	k4	1269
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
lodí	loď	k1gFnPc2	loď
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
zničena	zničit	k5eAaPmNgFnS	zničit
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
flotila	flotila	k1gFnSc1	flotila
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1270	[number]	k4	1270
z	z	k7c2	z
Aigues	Aiguesa	k1gFnPc2	Aiguesa
Mortes	Mortesa	k1gFnPc2	Mortesa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Cagliari	Cagliar	k1gFnSc2	Cagliar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
příjezd	příjezd	k1gInSc4	příjezd
dalších	další	k2eAgInPc2d1	další
křižáků	křižák	k1gInPc2	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
Ludvík	Ludvík	k1gMnSc1	Ludvík
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
je	být	k5eAaImIp3nS	být
Tunis	Tunis	k1gInSc1	Tunis
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
Tunisu	Tunis	k1gInSc2	Tunis
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Ludvíka	Ludvík	k1gMnSc4	Ludvík
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
dosažitelný	dosažitelný	k2eAgMnSc1d1	dosažitelný
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
dobytí	dobytí	k1gNnSc4	dobytí
města	město	k1gNnSc2	město
by	by	kYmCp3nP	by
velmi	velmi	k6eAd1	velmi
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
Ludvíkovu	Ludvíkův	k2eAgMnSc3d1	Ludvíkův
bratrovi	bratr	k1gMnSc3	bratr
a	a	k8xC	a
sicilskému	sicilský	k2eAgMnSc3d1	sicilský
králi	král	k1gMnSc3	král
Karlovi	Karel	k1gMnSc3	Karel
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ve	v	k7c6	v
výběru	výběr	k1gInSc6	výběr
cíle	cíl	k1gInSc2	cíl
kruciáty	kruciáta	k1gFnSc2	kruciáta
mohla	moct	k5eAaImAgFnS	moct
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
soudobá	soudobý	k2eAgFnSc1d1	soudobá
neznalost	neznalost	k1gFnSc1	neznalost
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
současníky	současník	k1gMnPc7	současník
zřejmě	zřejmě	k6eAd1	zřejmě
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tunis	Tunis	k1gInSc1	Tunis
leží	ležet	k5eAaImIp3nS	ležet
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
Egyptu	Egypt	k1gInSc3	Egypt
než	než	k8xS	než
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgInSc4d2	pozdější
útok	útok	k1gInSc4	útok
na	na	k7c4	na
egyptského	egyptský	k2eAgMnSc4d1	egyptský
sultána	sultán	k1gMnSc4	sultán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Tunis	Tunis	k1gInSc4	Tunis
mohlo	moct	k5eAaImAgNnS	moct
ale	ale	k8xC	ale
Ludvíka	Ludvík	k1gMnSc4	Ludvík
vést	vést	k5eAaImF	vést
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zpozdil	zpozdit	k5eAaPmAgInS	zpozdit
mongolský	mongolský	k2eAgInSc1d1	mongolský
útok	útok	k1gInSc1	útok
proti	proti	k7c3	proti
mamlúkům	mamlúek	k1gMnPc3	mamlúek
(	(	kIx(	(
<g/>
s	s	k7c7	s
nimž	jenž	k3xRgFnPc3	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
Ludvík	Ludvík	k1gMnSc1	Ludvík
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
koordinovat	koordinovat	k5eAaBmF	koordinovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
král	král	k1gMnSc1	král
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
shromážděné	shromážděný	k2eAgFnPc4d1	shromážděná
síly	síla	k1gFnPc4	síla
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
jiné	jiná	k1gFnSc3	jiná
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
neplánované	plánovaný	k2eNgFnSc6d1	neplánovaná
<g/>
)	)	kIx)	)
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tunisu	Tunis	k1gInSc6	Tunis
se	se	k3xPyFc4	se
francouzské	francouzský	k2eAgNnSc1d1	francouzské
vojsko	vojsko	k1gNnSc1	vojsko
vylodilo	vylodit	k5eAaPmAgNnS	vylodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
o	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Kartágu	Kartágo	k1gNnSc3	Kartágo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
tábora	tábor	k1gInSc2	tábor
včetně	včetně	k7c2	včetně
lepšího	dobrý	k2eAgInSc2d2	lepší
zdroje	zdroj	k1gInSc2	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Ludvík	Ludvík	k1gMnSc1	Ludvík
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
teprve	teprve	k6eAd1	teprve
začal	začít	k5eAaPmAgMnS	začít
vyzbrojovat	vyzbrojovat	k5eAaImF	vyzbrojovat
svou	svůj	k3xOyFgFnSc4	svůj
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
tyfus	tyfus	k1gInSc1	tyfus
nebo	nebo	k8xC	nebo
úplavice	úplavice	k1gFnSc1	úplavice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
následky	následek	k1gInPc4	následek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
králův	králův	k2eAgMnSc1d1	králův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
i	i	k9	i
Ludvík	Ludvík	k1gMnSc1	Ludvík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
následníkem	následník	k1gMnSc7	následník
Filipem	Filip	k1gMnSc7	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
zasažený	zasažený	k2eAgInSc1d1	zasažený
ztrátou	ztráta	k1gFnSc7	ztráta
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnSc3	nemoc
posléze	posléze	k6eAd1	posléze
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
princ	princ	k1gMnSc1	princ
Filip	Filip	k1gMnSc1	Filip
a	a	k8xC	a
vůdcem	vůdce	k1gMnSc7	vůdce
výpravy	výprava	k1gMnSc2	výprava
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
chlapcově	chlapcův	k2eAgNnSc6d1	chlapcovo
mládí	mládí	k1gNnSc1	mládí
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
<g/>
.	.	kIx.	.
</s>
<s>
Karlovi	Karel	k1gMnSc3	Karel
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyjednat	vyjednat	k5eAaPmF	vyjednat
s	s	k7c7	s
emírem	emír	k1gMnSc7	emír
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
pro	pro	k7c4	pro
Francouze	Francouz	k1gMnSc4	Francouz
výprava	výprava	k1gFnSc1	výprava
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
vraceli	vracet	k5eAaImAgMnP	vracet
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
do	do	k7c2	do
Trapani	Trapaň	k1gFnSc3	Trapaň
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
flotilu	flotila	k1gFnSc4	flotila
vichřice	vichřice	k1gFnSc2	vichřice
a	a	k8xC	a
výprava	výprava	k1gFnSc1	výprava
ztratila	ztratit	k5eAaPmAgFnS	ztratit
na	na	k7c4	na
čtyřicet	čtyřicet	k4xCc4	čtyřicet
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
s	s	k7c7	s
bídou	bída	k1gFnSc7	bída
zachránil	zachránit	k5eAaPmAgInS	zachránit
holý	holý	k2eAgInSc1d1	holý
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Vyděšení	vyděšený	k2eAgMnPc1d1	vyděšený
cestovatelé	cestovatel	k1gMnPc1	cestovatel
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
raději	rád	k6eAd2	rád
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
přesto	přesto	k6eAd1	přesto
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
jen	jen	k9	jen
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnSc3	nemoc
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
navarrský	navarrský	k2eAgMnSc1d1	navarrský
král	král	k1gMnSc1	král
Theobald	Theobald	k1gMnSc1	Theobald
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Isabela	Isabela	k1gFnSc1	Isabela
(	(	kIx(	(
<g/>
Ludvíkova	Ludvíkův	k2eAgFnSc1d1	Ludvíkova
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
zemřela	zemřít	k5eAaPmAgFnS	zemřít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Mladou	mladý	k2eAgFnSc4d1	mladá
královnu	královna	k1gFnSc4	královna
Francie	Francie	k1gFnSc2	Francie
Isabelu	Isabela	k1gFnSc4	Isabela
shodil	shodit	k5eAaPmAgMnS	shodit
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
předčasnému	předčasný	k2eAgInSc3d1	předčasný
porodu	porod	k1gInSc3	porod
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
chlapce	chlapec	k1gMnSc2	chlapec
a	a	k8xC	a
Isabelině	Isabelin	k2eAgFnSc3d1	Isabelina
smrti	smrt	k1gFnSc3	smrt
o	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Srpen	srpen	k1gInSc1	srpen
roku	rok	k1gInSc2	rok
1271	[number]	k4	1271
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
osudným	osudný	k2eAgInSc7d1	osudný
pro	pro	k7c4	pro
Ludvíkova	Ludvíkův	k2eAgMnSc4d1	Ludvíkův
bratra	bratr	k1gMnSc4	bratr
Alfonse	Alfons	k1gMnSc4	Alfons
z	z	k7c2	z
Poitiers	Poitiersa	k1gFnPc2	Poitiersa
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
Janu	Jan	k1gMnSc3	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Richarda	Richard	k1gMnSc2	Richard
Cornwallského	Cornwallský	k2eAgInSc2d1	Cornwallský
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Almainu	Almain	k1gInSc2	Almain
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
modlitby	modlitba	k1gFnSc2	modlitba
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
zákeřně	zákeřně	k6eAd1	zákeřně
přepaden	přepaden	k2eAgInSc4d1	přepaden
a	a	k8xC	a
ubodán	ubodán	k2eAgInSc4d1	ubodán
Guyem	Guyem	k1gInSc4	Guyem
a	a	k8xC	a
Simonem	Simon	k1gMnSc7	Simon
<g/>
,	,	kIx,	,
syny	syn	k1gMnPc7	syn
Simona	Simona	k1gFnSc1	Simona
z	z	k7c2	z
Montfortu	Montfort	k1gInSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
msta	msta	k1gFnSc1	msta
za	za	k7c4	za
opuštění	opuštění	k1gNnSc4	opuštění
řad	řada	k1gFnPc2	řada
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Průvod	průvod	k1gInSc1	průvod
s	s	k7c7	s
rakví	rakev	k1gFnSc7	rakev
plnou	plný	k2eAgFnSc7d1	plná
kostí	kost	k1gFnSc7	kost
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
krále	král	k1gMnSc4	král
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
až	až	k9	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1271	[number]	k4	1271
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
francouzským	francouzský	k2eAgMnSc7d1	francouzský
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
anonymní	anonymní	k2eAgMnSc1d1	anonymní
básník	básník	k1gMnSc1	básník
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
získal	získat	k5eAaPmAgMnS	získat
Ludvík	Ludvík	k1gMnSc1	Ludvík
pověst	pověst	k1gFnSc4	pověst
svatosti	svatost	k1gFnSc2	svatost
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
zbožnost	zbožnost	k1gFnSc4	zbožnost
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
činy	čin	k1gInPc4	čin
vykonané	vykonaný	k2eAgInPc4d1	vykonaný
pro	pro	k7c4	pro
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
jednání	jednání	k1gNnSc4	jednání
"	"	kIx"	"
<g/>
svatého	svatý	k2eAgMnSc2d1	svatý
krále	král	k1gMnSc2	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
iniciativu	iniciativa	k1gFnSc4	iniciativa
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
svatořečení	svatořečení	k1gNnSc2	svatořečení
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
X.	X.	kA	X.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1272	[number]	k4	1272
napsal	napsat	k5eAaBmAgMnS	napsat
zpovědníkovi	zpovědník	k1gMnSc3	zpovědník
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Geoffroyovi	Geoffroyův	k2eAgMnPc1d1	Geoffroyův
z	z	k7c2	z
Beaulieu	Beaulieus	k1gInSc2	Beaulieus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zaslal	zaslat	k5eAaPmAgMnS	zaslat
co	co	k3yQnSc4	co
nejvíce	hodně	k6eAd3	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
papež	papež	k1gMnSc1	papež
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
pravý	pravý	k2eAgInSc4d1	pravý
vzor	vzor	k1gInSc4	vzor
po	po	k7c4	po
všechny	všechen	k3xTgMnPc4	všechen
křesťanské	křesťanský	k2eAgMnPc4d1	křesťanský
panovníky	panovník	k1gMnPc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
dopis	dopis	k1gInSc4	dopis
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Geoffroy	Geoffroa	k1gMnSc2	Geoffroa
z	z	k7c2	z
Beaulieu	Beaulieus	k1gInSc2	Beaulieus
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1274	[number]	k4	1274
první	první	k4xOgInSc1	první
životopis	životopis	k1gInSc1	životopis
zemřelého	zemřelý	k1gMnSc2	zemřelý
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
je	být	k5eAaImIp3nS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
hoden	hoden	k2eAgMnSc1d1	hoden
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
svatořečení	svatořečení	k1gNnSc2	svatořečení
<g/>
.	.	kIx.	.
</s>
<s>
Šetření	šetření	k1gNnSc1	šetření
o	o	k7c6	o
svatosti	svatost	k1gFnSc6	svatost
krále	král	k1gMnSc2	král
trvalo	trvalo	k1gNnSc4	trvalo
řadu	řad	k1gInSc2	řad
let	léto	k1gNnPc2	léto
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1281	[number]	k4	1281
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
oficiální	oficiální	k2eAgNnSc1d1	oficiální
řízení	řízení	k1gNnSc1	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něho	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
přes	přes	k7c4	přes
330	[number]	k4	330
svědectví	svědectví	k1gNnSc2	svědectví
o	o	k7c6	o
zázracích	zázrak	k1gInPc6	zázrak
a	a	k8xC	a
shromážděno	shromáždit	k5eAaPmNgNnS	shromáždit
38	[number]	k4	38
výpovědí	výpověď	k1gFnPc2	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
proces	proces	k1gInSc1	proces
táhl	táhnout	k5eAaImAgInS	táhnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1297	[number]	k4	1297
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
svatořečen	svatořečen	k2eAgInSc4d1	svatořečen
papežem	papež	k1gMnSc7	papež
Bonifácem	Bonifác	k1gMnSc7	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatořečení	svatořečení	k1gNnSc6	svatořečení
se	se	k3xPyFc4	se
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgInS	nechat
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1306	[number]	k4	1306
dopravit	dopravit	k5eAaPmF	dopravit
Ludvíkovu	Ludvíkův	k2eAgFnSc4d1	Ludvíkova
lebku	lebka	k1gFnSc4	lebka
do	do	k7c2	do
Sainte-Chapelle	Sainte-Chapelle	k1gFnSc2	Sainte-Chapelle
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
další	další	k2eAgInPc1d1	další
ostatky	ostatek	k1gInPc1	ostatek
pak	pak	k8xC	pak
rozesílal	rozesílat	k5eAaImAgInS	rozesílat
kostelům	kostel	k1gInPc3	kostel
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1298	[number]	k4	1298
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
obřad	obřad	k1gInSc1	obřad
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgFnP	být
kosti	kost	k1gFnPc1	kost
svatého	svatý	k2eAgMnSc2d1	svatý
Ludvíka	Ludvík	k1gMnSc2	Ludvík
exhumovány	exhumovat	k5eAaBmNgFnP	exhumovat
a	a	k8xC	a
uloženy	uložit	k5eAaPmNgFnP	uložit
do	do	k7c2	do
relikviáře	relikviář	k1gInSc2	relikviář
za	za	k7c7	za
oltářem	oltář	k1gInSc7	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
těžkostí	těžkost	k1gFnPc2	těžkost
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
podařilo	podařit	k5eAaPmAgNnS	podařit
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
na	na	k7c4	na
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
královská	královský	k2eAgNnPc4d1	královské
léna	léno	k1gNnPc4	léno
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
jeho	jeho	k3xOp3gFnSc2	jeho
korunní	korunní	k2eAgFnSc2d1	korunní
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
všude	všude	k6eAd1	všude
napomáhal	napomáhat	k5eAaBmAgMnS	napomáhat
vymáhání	vymáhání	k1gNnSc4	vymáhání
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc1	dodržování
míru	mír	k1gInSc2	mír
a	a	k8xC	a
ochraňoval	ochraňovat	k5eAaImAgMnS	ochraňovat
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
jednotu	jednota	k1gFnSc4	jednota
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
francouzského	francouzský	k2eAgInSc2d1	francouzský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
přijal	přijmout	k5eAaPmAgMnS	přijmout
Ludvík	Ludvík	k1gMnSc1	Ludvík
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
asketický	asketický	k2eAgInSc4d1	asketický
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
kritiku	kritika	k1gFnSc4	kritika
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
domnívaly	domnívat	k5eAaImAgFnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
neslučitelný	slučitelný	k2eNgInSc1d1	neslučitelný
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
úmrtí	úmrtí	k1gNnPc2	úmrtí
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
zemí	zem	k1gFnSc7	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
