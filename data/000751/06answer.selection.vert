<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
dvou	dva	k4xCgFnPc6	dva
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
však	však	k9	však
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1298	[number]	k4	1298
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
příkladný	příkladný	k2eAgInSc4d1	příkladný
život	život	k1gInSc4	život
plný	plný	k2eAgInSc4d1	plný
odříkání	odříkání	k1gNnSc4	odříkání
svatořečen	svatořečen	k2eAgMnSc1d1	svatořečen
<g/>
.	.	kIx.	.
</s>
