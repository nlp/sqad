<p>
<s>
Šedesát	šedesát	k4xCc1	šedesát
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
složené	složený	k2eAgNnSc4d1	složené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
LX	LX	kA	LX
<g/>
.	.	kIx.	.
</s>
<s>
Tutéž	týž	k3xTgFnSc4	týž
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmeno	písmeno	k1gNnSc1	písmeno
samech	samech	k1gInSc1	samech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synonymní	synonymní	k2eAgInSc1d1	synonymní
archaický	archaický	k2eAgInSc1d1	archaický
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
šedesát	šedesát	k4xCc4	šedesát
je	být	k5eAaImIp3nS	být
kopa	kopa	k1gFnSc1	kopa
<g/>
.	.	kIx.	.
</s>
<s>
Kopa	kopa	k1gFnSc1	kopa
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
zastaralými	zastaralý	k2eAgInPc7d1	zastaralý
množstevními	množstevní	k2eAgNnPc7d1	množstevní
označeními	označení	k1gNnPc7	označení
dobře	dobře	k6eAd1	dobře
přepočitatelnou	přepočitatelný	k2eAgFnSc4d1	přepočitatelný
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
kopa	kopa	k1gFnSc1	kopa
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
tuctů	tucet	k1gInPc2	tucet
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgInPc4	čtyři
mandele	mandel	k1gInPc4	mandel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemie	chemie	k1gFnSc1	chemie
==	==	k?	==
</s>
</p>
<p>
<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
prvku	prvek	k1gInSc2	prvek
neodym	neodym	k1gInSc1	neodym
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematika	matematika	k1gFnSc1	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
planární	planární	k2eAgFnSc6d1	planární
geometrii	geometrie	k1gFnSc6	geometrie
má	mít	k5eAaImIp3nS	mít
stupeň	stupeň	k1gInSc1	stupeň
<g/>
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
60	[number]	k4	60
(	(	kIx(	(
<g/>
úhlových	úhlový	k2eAgFnPc2d1	úhlová
<g/>
)	)	kIx)	)
minut	minuta	k1gFnPc2	minuta
<g/>
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
minuta	minuta	k1gFnSc1	minuta
60	[number]	k4	60
(	(	kIx(	(
<g/>
úhlových	úhlový	k2eAgFnPc2d1	úhlová
<g/>
)	)	kIx)	)
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
při	při	k7c6	při
vrcholech	vrchol	k1gInPc6	vrchol
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
jsou	být	k5eAaImIp3nP	být
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzika	fyzika	k1gFnSc1	fyzika
==	==	k?	==
</s>
</p>
<p>
<s>
měření	měření	k1gNnSc1	měření
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technika	technika	k1gFnSc1	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
označení	označení	k1gNnSc6	označení
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
číslo	číslo	k1gNnSc1	číslo
60	[number]	k4	60
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Tank	tank	k1gInSc1	tank
T-	T-	k1gFnSc2	T-
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
hydroplán	hydroplán	k1gInSc1	hydroplán
Heinkel	Heinkela	k1gFnPc2	Heinkela
He	he	k0	he
60	[number]	k4	60
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc1	vrtulník
UH-60	UH-60	k1gMnSc1	UH-60
Black	Black	k1gMnSc1	Black
Hawk	Hawk	k1gMnSc1	Hawk
či	či	k8xC	či
bombardér	bombardér	k1gMnSc1	bombardér
Convair	Convair	k1gMnSc1	Convair
YB-	YB-	k1gMnSc1	YB-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
60	[number]	k4	60
<g/>
.	.	kIx.	.
mise	mise	k1gFnSc2	mise
raketoplánu	raketoplán	k1gInSc2	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
STS-	STS-	k1gFnSc1	STS-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kopa	kopa	k1gFnSc1	kopa
grošů	groš	k1gInPc2	groš
byla	být	k5eAaImAgFnS	být
peněžní	peněžní	k2eAgFnSc1d1	peněžní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
60	[number]	k4	60
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
více	hodně	k6eAd2	hodně
grošů	groš	k1gInPc2	groš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
významným	významný	k2eAgNnSc7d1	významné
obdobím	období	k1gNnSc7	období
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interstate	Interstat	k1gMnSc5	Interstat
60	[number]	k4	60
je	být	k5eAaImIp3nS	být
komediální	komediální	k2eAgFnSc1d1	komediální
roadmovie	roadmovie	k1gFnSc1	roadmovie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
remake	remake	k1gFnSc1	remake
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
sprinterská	sprinterský	k2eAgFnSc1d1	sprinterská
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
s	s	k7c7	s
překážkami	překážka	k1gFnPc7	překážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
==	==	k?	==
</s>
</p>
<p>
<s>
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1	kvalifikovaná
většina	většina	k1gFnSc1	většina
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
ústavním	ústavní	k2eAgInSc6d1	ústavní
řádu	řád	k1gInSc6	řád
ČR	ČR	kA	ČR
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
zvolených	zvolený	k2eAgMnPc2d1	zvolený
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
kopa	kopa	k1gFnSc1	kopa
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
kopa	kopa	k1gFnSc1	kopa
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původně	původně	k6eAd1	původně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
kopání	kopání	k1gNnSc2	kopání
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
hromadu	hromada	k1gFnSc4	hromada
nakopané	nakopaný	k2eAgFnSc2d1	nakopaná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
význam	význam	k1gInSc1	význam
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
hromadu	hromada	k1gFnSc4	hromada
obecně	obecně	k6eAd1	obecně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
přesný	přesný	k2eAgInSc4d1	přesný
číselný	číselný	k2eAgInSc4d1	číselný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
60	[number]	k4	60
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
šedesát	šedesát	k4xCc1	šedesát
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kopa	kopa	k1gFnSc1	kopa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
