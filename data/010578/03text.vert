<p>
<s>
Pierce	Pierce	k1gMnSc1	Pierce
Brendan	Brendan	k1gMnSc1	Brendan
Brosnan	Brosnan	k1gMnSc1	Brosnan
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
Drogheda	Droghed	k1gMnSc2	Droghed
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc1	hrabství
Louth	Loutha	k1gFnPc2	Loutha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
irský	irský	k2eAgMnSc1d1	irský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
širší	široký	k2eAgFnSc2d2	širší
veřejnosti	veřejnost	k1gFnSc2	veřejnost
především	především	k9	především
jako	jako	k8xC	jako
představitel	představitel	k1gMnSc1	představitel
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bonda	k1gMnSc1	Bonda
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
filmech	film	k1gInPc6	film
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
žil	žíla	k1gFnPc2	žíla
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
čestným	čestný	k2eAgMnSc7d1	čestný
držitelem	držitel	k1gMnSc7	držitel
Řádu	řád	k1gInSc2	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
/	/	kIx~	/
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Good	Good	k1gMnSc1	Good
Friday	Fridaa	k1gFnSc2	Fridaa
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profesionálové-	Profesionálové-	k?	Profesionálové-
Nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
sporty	sport	k1gInPc4	sport
/	/	kIx~	/
The	The	k1gFnSc1	The
Professionals-	Professionals-	k1gFnSc2	Professionals-
Blood	Blooda	k1gFnPc2	Blooda
Sports	Sports	k1gInSc1	Sports
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozbité	rozbitý	k2eAgNnSc1d1	rozbité
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
/	/	kIx~	/
The	The	k1gMnSc1	The
Mirror	Mirror	k1gMnSc1	Mirror
Crack	Crack	k1gMnSc1	Crack
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nomads	Nomads	k1gInSc1	Nomads
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taffin	Taffin	k1gInSc1	Taffin
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
protokol	protokol	k1gInSc1	protokol
/	/	kIx~	/
The	The	k1gFnSc1	The
Fourth	Fourth	k1gMnSc1	Fourth
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podvodníci	podvodník	k1gMnPc1	podvodník
/	/	kIx~	/
The	The	k1gFnSc1	The
Deceivers	Deceiversa	k1gFnPc2	Deceiversa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Panský	panský	k2eAgInSc1d1	panský
dům	dům	k1gInSc1	dům
/	/	kIx~	/
Noble	Noble	k1gInSc1	Noble
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
80	[number]	k4	80
dní	den	k1gInPc2	den
/	/	kIx~	/
Around	Around	k1gInSc1	Around
the	the	k?	the
World	World	k1gInSc1	World
in	in	k?	in
80	[number]	k4	80
Days	Days	k1gInSc1	Days
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Johnson	Johnson	k1gMnSc1	Johnson
/	/	kIx~	/
Mister	mister	k1gMnSc1	mister
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trávníkář	Trávníkář	k1gMnSc1	Trávníkář
/	/	kIx~	/
The	The	k1gMnSc1	The
Lawnmower	Lawnmower	k1gMnSc1	Lawnmower
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Exploze	exploze	k1gFnSc1	exploze
/	/	kIx~	/
Live	Live	k1gFnSc1	Live
Wire	Wire	k1gFnSc1	Wire
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mrs	Mrs	k?	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Doubtfire	Doubtfir	k1gMnSc5	Doubtfir
-	-	kIx~	-
táta	táta	k1gMnSc1	táta
v	v	k7c6	v
sukni	sukně	k1gFnSc6	sukně
/	/	kIx~	/
Mrs	Mrs	k1gFnPc6	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Doubtfire	Doubtfir	k1gMnSc5	Doubtfir
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlak	vlak	k1gInSc1	vlak
smrti	smrt	k1gFnSc2	smrt
/	/	kIx~	/
Death	Death	k1gMnSc1	Death
Train	Train	k1gMnSc1	Train
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milostná	milostný	k2eAgFnSc1d1	milostná
aféra	aféra	k1gFnSc1	aféra
/	/	kIx~	/
Love	lov	k1gInSc5	lov
Affair	Affair	k1gMnSc1	Affair
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
/	/	kIx~	/
Night	Night	k2eAgInSc1d1	Night
Watch	Watch	k1gInSc1	Watch
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
oko	oko	k1gNnSc1	oko
/	/	kIx~	/
GoldenEye	GoldenEye	k1gNnSc1	GoldenEye
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mars	Mars	k1gInSc1	Mars
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
!	!	kIx.	!
</s>
<s>
/	/	kIx~	/
Mars	Mars	k1gInSc1	Mars
Attacks	Attacksa	k1gFnPc2	Attacksa
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
tváře	tvář	k1gFnPc1	tvář
lásky	láska	k1gFnSc2	láska
/	/	kIx~	/
The	The	k1gMnSc1	The
Mirror	Mirror	k1gMnSc1	Mirror
Has	hasit	k5eAaImRp2nS	hasit
Two	Two	k1gMnSc1	Two
Faces	Faces	k1gMnSc1	Faces
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Robinson	Robinson	k1gMnSc1	Robinson
Crusoe	Cruso	k1gFnSc2	Cruso
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zítřek	zítřek	k1gInSc1	zítřek
nikdy	nikdy	k6eAd1	nikdy
neumírá	umírat	k5eNaImIp3nS	umírat
/	/	kIx~	/
Tomorrow	Tomorrow	k1gFnSc1	Tomorrow
Never	Never	k1gMnSc1	Never
Dies	Dies	k1gInSc1	Dies
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozpoutané	rozpoutaný	k2eAgNnSc1d1	rozpoutané
peklo	peklo	k1gNnSc1	peklo
/	/	kIx~	/
Dante	Dante	k1gMnSc1	Dante
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Peak	Peaka	k1gFnPc2	Peaka
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
meč	meč	k1gInSc1	meč
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c6	na
Camelot	Camelot	k1gMnSc1	Camelot
/	/	kIx~	/
The	The	k1gMnSc1	The
Magic	Magic	k1gMnSc1	Magic
Sword	Sword	k1gMnSc1	Sword
<g/>
:	:	kIx,	:
Quest	Quest	k1gInSc1	Quest
for	forum	k1gNnPc2	forum
Camelot	Camelot	k1gInSc1	Camelot
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indián	Indián	k1gMnSc1	Indián
/	/	kIx~	/
Grey	Gre	k2eAgFnPc1d1	Gre
Owl	Owl	k1gFnPc1	Owl
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
nestačí	stačit	k5eNaBmIp3nS	stačit
/	/	kIx~	/
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
Is	Is	k1gMnSc1	Is
Not	nota	k1gFnPc2	nota
Enough	Enough	k1gMnSc1	Enough
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aféra	aféra	k1gFnSc1	aféra
Thomase	Thomas	k1gMnSc2	Thomas
Crowna	Crown	k1gMnSc2	Crown
/	/	kIx~	/
The	The	k1gMnSc1	The
Thomas	Thomas	k1gMnSc1	Thomas
Crown	Crown	k1gMnSc1	Crown
Affair	Affair	k1gMnSc1	Affair
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Agent	agent	k1gMnSc1	agent
z	z	k7c2	z
Panamy	Panama	k1gFnSc2	Panama
/	/	kIx~	/
The	The	k1gMnSc1	The
Tailor	tailor	k1gMnSc1	tailor
of	of	k?	of
Panama	Panama	k1gFnSc1	Panama
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
neumírej	umírat	k5eNaImRp2nS	umírat
/	/	kIx~	/
Die	Die	k1gFnSc6	Die
Another	Anothra	k1gFnPc2	Anothra
Day	Day	k1gFnPc2	Day
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evelyn	Evelyn	k1gNnSc1	Evelyn
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
setmí	setmět	k5eAaPmIp3nS	setmět
/	/	kIx~	/
After	After	k1gInSc1	After
the	the	k?	the
Sunset	Sunset	k1gInSc1	Sunset
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
/	/	kIx~	/
Laws	Laws	k1gInSc1	Laws
of	of	k?	of
Attraction	Attraction	k1gInSc1	Attraction
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Matador	matador	k1gMnSc1	matador
/	/	kIx~	/
The	The	k1gMnSc1	The
Matador	matador	k1gMnSc1	matador
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
/	/	kIx~	/
Seraphim	Seraphim	k1gMnSc1	Seraphim
Falls	Fallsa	k1gFnPc2	Fallsa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Topkapi	Topkapi	k1gNnSc1	Topkapi
Affair	Affair	k1gMnSc1	Affair
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Butterfly	butterfly	k1gInSc1	butterfly
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Wheel	Whelo	k1gNnPc2	Whelo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Married	Married	k1gInSc1	Married
Life	Lif	k1gFnSc2	Lif
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Caitlin	Caitlin	k1gInSc1	Caitlin
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Big	Big	k1gFnSc1	Big
Biazarro	Biazarro	k1gNnSc4	Biazarro
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Percy	Perca	k1gMnSc2	Perca
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
:	:	kIx,	:
Zloděj	zloděj	k1gMnSc1	zloděj
blesku	blesk	k1gInSc2	blesk
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nezapomeň	zapomenout	k5eNaPmRp2nS	zapomenout
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
/	/	kIx~	/
The	The	k1gMnSc1	The
Ghost	Ghost	k1gMnSc1	Ghost
Writer	Writer	k1gMnSc1	Writer
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
Here	Here	k6eAd1	Here
We	We	k1gMnSc1	We
Go	Go	k1gMnSc1	Go
Again	Again	k1gMnSc1	Again
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pierce	Pierec	k1gInSc2	Pierec
Brosnan	Brosnana	k1gFnPc2	Brosnana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pierce	Pierec	k1gInPc4	Pierec
Brosnan	Brosnana	k1gFnPc2	Brosnana
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Pierce	Pierce	k1gMnSc1	Pierce
Brosnan	Brosnan	k1gMnSc1	Brosnan
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pierce	Pierce	k1gMnSc1	Pierce
Brosnan	Brosnan	k1gMnSc1	Brosnan
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pierce	Pierce	k1gMnSc1	Pierce
Brosnan	Brosnan	k1gMnSc1	Brosnan
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
