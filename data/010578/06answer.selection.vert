<s>
Pierce	Pierce	k1gMnSc1	Pierce
Brendan	Brendan	k1gMnSc1	Brendan
Brosnan	Brosnan	k1gMnSc1	Brosnan
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
Drogheda	Droghed	k1gMnSc2	Droghed
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc1	hrabství
Louth	Loutha	k1gFnPc2	Loutha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
irský	irský	k2eAgMnSc1d1	irský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
širší	široký	k2eAgFnSc2d2	širší
veřejnosti	veřejnost	k1gFnSc2	veřejnost
především	především	k9	především
jako	jako	k8xC	jako
představitel	představitel	k1gMnSc1	představitel
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bonda	k1gMnSc1	Bonda
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
filmech	film	k1gInPc6	film
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
