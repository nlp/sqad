<s>
Tellur	tellur	k1gInSc1	tellur
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Te	Te	k1gFnSc1	Te
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Tellurium	tellurium	k1gNnSc1	tellurium
je	být	k5eAaImIp3nS	být
polokovový	polokovový	k2eAgInSc4d1	polokovový
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklý	lesklý	k2eAgInSc4d1	lesklý
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
polovodičové	polovodičový	k2eAgFnSc6d1	polovodičová
technice	technika	k1gFnSc6	technika
a	a	k8xC	a
metalurgii	metalurgie	k1gFnSc6	metalurgie
<g/>
.	.	kIx.	.
</s>
