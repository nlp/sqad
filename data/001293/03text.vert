<s>
Tellur	tellur	k1gInSc1	tellur
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Te	Te	k1gFnSc1	Te
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Tellurium	tellurium	k1gNnSc1	tellurium
je	být	k5eAaImIp3nS	být
polokovový	polokovový	k2eAgInSc4d1	polokovový
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklý	lesklý	k2eAgInSc4d1	lesklý
prvek	prvek	k1gInSc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
polovodičové	polovodičový	k2eAgFnSc6d1	polovodičová
technice	technika	k1gFnSc6	technika
a	a	k8xC	a
metalurgii	metalurgie	k1gFnSc6	metalurgie
<g/>
.	.	kIx.	.
</s>
<s>
Tellur	tellur	k1gInSc1	tellur
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
vzácný	vzácný	k2eAgInSc4d1	vzácný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
Franzem	Franz	k1gMnSc7	Franz
Josephem	Joseph	k1gInSc7	Joseph
Müllerem	Müller	k1gInSc7	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
patří	patřit	k5eAaImIp3nS	patřit
spíše	spíše	k9	spíše
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
i	i	k8xC	i
kyseliny	kyselina	k1gFnPc1	kyselina
telluru	tellur	k1gInSc2	tellur
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
chemicky	chemicky	k6eAd1	chemicky
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
síru	síra	k1gFnSc4	síra
nebo	nebo	k8xC	nebo
selen	selen	k1gInSc4	selen
<g/>
.	.	kIx.	.
</s>
<s>
Tellur	tellur	k1gInSc1	tellur
obvykle	obvykle	k6eAd1	obvykle
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
selen	selen	k1gInSc4	selen
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
rudách	ruda	k1gFnPc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
afinitu	afinita	k1gFnSc4	afinita
ke	k	k7c3	k
zlatu	zlato	k1gNnSc3	zlato
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zlatých	zlatý	k2eAgNnPc6d1	Zlaté
ložiscích	ložisko	k1gNnPc6	ložisko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
například	například	k6eAd1	například
tellurid	tellurid	k1gInSc4	tellurid
zlata	zlato	k1gNnSc2	zlato
calaverit	calaverit	k1gInSc1	calaverit
AuTe	aut	k1gInSc5	aut
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
tellurid	tellurid	k1gInSc1	tellurid
olova	olovo	k1gNnSc2	olovo
altait	altait	k1gInSc1	altait
PbTe	PbTe	k1gFnSc4	PbTe
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
tellur	tellur	k1gInSc1	tellur
získává	získávat	k5eAaImIp3nS	získávat
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
anodových	anodový	k2eAgInPc2d1	anodový
kalů	kal	k1gInPc2	kal
po	po	k7c6	po
elektrolytické	elektrolytický	k2eAgFnSc6d1	elektrolytická
výrobě	výroba	k1gFnSc6	výroba
mědi	měď	k1gFnSc2	měď
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c6	po
rafinaci	rafinace	k1gFnSc6	rafinace
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgNnSc1d1	relativní
zastoupení	zastoupení	k1gNnSc1	zastoupení
telluru	tellur	k1gInSc2	tellur
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,001	[number]	k4	0,001
<g/>
-	-	kIx~	-
<g/>
0,005	[number]	k4	0,005
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
tak	tak	k6eAd1	tak
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgFnPc1d1	současná
analytické	analytický	k2eAgFnPc1d1	analytická
techniky	technika	k1gFnPc1	technika
nebyly	být	k5eNaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
jeho	jeho	k3xOp3gInSc6	jeho
množství	množství	k1gNnSc1	množství
spolehlivě	spolehlivě	k6eAd1	spolehlivě
změřit	změřit	k5eAaPmF	změřit
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
tellur	tellur	k1gInSc1	tellur
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
stálý	stálý	k2eAgInSc1d1	stálý
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklý	lesklý	k2eAgInSc1d1	lesklý
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
křehký	křehký	k2eAgInSc1d1	křehký
polokov	polokov	k1gInSc1	polokov
<g/>
.	.	kIx.	.
</s>
<s>
Dosti	dosti	k6eAd1	dosti
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
tellur	tellur	k1gInSc1	tellur
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Te	Te	k1gFnSc2	Te
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc1	Te
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc1	Te
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
Te	Te	k1gFnSc1	Te
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
slouží	sloužit	k5eAaImIp3nS	sloužit
tellur	tellur	k1gInSc4	tellur
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
mikrolegur	mikrolegura	k1gFnPc2	mikrolegura
ke	k	k7c3	k
zlepšování	zlepšování	k1gNnSc3	zlepšování
mechanických	mechanický	k2eAgFnPc2d1	mechanická
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
koncentrace	koncentrace	k1gFnPc1	koncentrace
telluru	tellur	k1gInSc2	tellur
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
pevnost	pevnost	k1gFnSc4	pevnost
slitin	slitina	k1gFnPc2	slitina
olova	olovo	k1gNnSc2	olovo
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Přídavky	přídavka	k1gFnPc1	přídavka
telluru	tellur	k1gInSc2	tellur
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
nerezových	rezový	k2eNgFnPc2d1	nerezová
ocelí	ocel	k1gFnPc2	ocel
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
jejich	jejich	k3xOp3gFnSc4	jejich
snazší	snadný	k2eAgFnSc4d2	snazší
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
opracovatelnost	opracovatelnost	k1gFnSc4	opracovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Telurid	telurid	k1gInSc1	telurid
gallia	gallium	k1gNnSc2	gallium
nalézá	nalézat	k5eAaImIp3nS	nalézat
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
polovodičovém	polovodičový	k2eAgInSc6d1	polovodičový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
některých	některý	k3yIgNnPc2	některý
termoelektrických	termoelektrický	k2eAgNnPc2d1	termoelektrické
zařízení	zařízení	k1gNnPc2	zařízení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tellurid	tellurid	k1gInSc1	tellurid
bismutu	bismut	k1gInSc2	bismut
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
tellurem	tellur	k1gInSc7	tellur
barveno	barven	k2eAgNnSc4d1	barveno
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
perspektivní	perspektivní	k2eAgNnSc1d1	perspektivní
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
použití	použití	k1gNnSc1	použití
sloučenin	sloučenina	k1gFnPc2	sloučenina
telluru	tellur	k1gInSc2	tellur
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
fotočlánků	fotočlánek	k1gInPc2	fotočlánek
<g/>
.	.	kIx.	.
</s>
<s>
Fotočlánky	fotočlánek	k1gInPc1	fotočlánek
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
telluridu	tellurid	k1gInSc2	tellurid
kadmia	kadmium	k1gNnSc2	kadmium
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
k	k	k7c3	k
nejlevnějším	levný	k2eAgMnPc3d3	Nejlevnější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
telluridů	tellurid	k1gInPc2	tellurid
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
záznamové	záznamový	k2eAgFnPc4d1	záznamová
vrstvy	vrstva	k1gFnPc4	vrstva
v	v	k7c6	v
přepisovatelných	přepisovatelný	k2eAgInPc6d1	přepisovatelný
optických	optický	k2eAgInPc6d1	optický
discích	disk	k1gInPc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
patří	patřit	k5eAaImIp3nS	patřit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
telluru	tellur	k1gInSc2	tellur
mezi	mezi	k7c4	mezi
toxické	toxický	k2eAgFnPc4d1	toxická
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
provozech	provoz	k1gInPc6	provoz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
zvýšených	zvýšený	k2eAgFnPc6d1	zvýšená
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zachovávat	zachovávat	k5eAaImF	zachovávat
přísné	přísný	k2eAgInPc4d1	přísný
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
předpisy	předpis	k1gInPc4	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zvláště	zvláště	k6eAd1	zvláště
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
je	být	k5eAaImIp3nS	být
pokládáno	pokládán	k2eAgNnSc1d1	pokládáno
vdechování	vdechování	k1gNnSc1	vdechování
aerosolů	aerosol	k1gInPc2	aerosol
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
telluru	tellur	k1gInSc2	tellur
<g/>
.	.	kIx.	.
oxid	oxid	k1gInSc1	oxid
tellurnatý	tellurnatý	k2eAgInSc1d1	tellurnatý
oxid	oxid	k1gInSc4	oxid
telluričitý	telluričitý	k2eAgInSc4d1	telluričitý
oxid	oxid	k1gInSc4	oxid
tellurový	tellurový	k2eAgInSc1d1	tellurový
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tellur	tellur	k1gInSc1	tellur
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tellur	tellur	k1gInSc1	tellur
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
