<p>
<s>
Prominence	prominence	k1gFnSc1	prominence
či	či	k8xC	či
význačnost	význačnost	k1gFnSc1	význačnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
topografická	topografický	k2eAgFnSc1d1	topografická
prominence	prominence	k1gFnSc1	prominence
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
označující	označující	k2eAgInSc4d1	označující
relativní	relativní	k2eAgFnSc4d1	relativní
výšku	výška	k1gFnSc4	výška
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
jako	jako	k9	jako
převýšení	převýšení	k1gNnSc1	převýšení
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
výšek	výška	k1gFnPc2	výška
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
<g/>
)	)	kIx)	)
sedlem	sedlo	k1gNnSc7	sedlo
s	s	k7c7	s
nějakou	nějaký	k3yIgFnSc7	nějaký
vyšší	vysoký	k2eAgFnSc7d2	vyšší
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
<g/>
)	)	kIx)	)
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
výškových	výškový	k2eAgInPc2d1	výškový
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgInPc4	který
musíme	muset	k5eAaImIp1nP	muset
minimálně	minimálně	k6eAd1	minimálně
sestoupit	sestoupit	k5eAaPmF	sestoupit
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
vyšší	vysoký	k2eAgNnSc4d2	vyšší
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
<g/>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
definice	definice	k1gFnSc1	definice
využívá	využívat	k5eAaPmIp3nS	využívat
stoupající	stoupající	k2eAgFnSc4d1	stoupající
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
–	–	k?	–
kdyby	kdyby	k9	kdyby
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
až	až	k6eAd1	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
dané	daný	k2eAgFnSc2d1	daná
hory	hora	k1gFnSc2	hora
udělala	udělat	k5eAaPmAgFnS	udělat
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
samostatného	samostatný	k2eAgInSc2d1	samostatný
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prominence	prominence	k1gFnSc1	prominence
dané	daný	k2eAgFnSc2d1	daná
hory	hora	k1gFnSc2	hora
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
teoretické	teoretický	k2eAgFnSc3d1	teoretická
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
vrcholu	vrchol	k1gInSc2	vrchol
při	při	k7c6	při
takto	takto	k6eAd1	takto
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
hladině	hladina	k1gFnSc3	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
stanovení	stanovení	k1gNnSc6	stanovení
prominence	prominence	k1gFnSc2	prominence
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
tvarem	tvar	k1gInSc7	tvar
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zásahů	zásah	k1gInPc2	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
sedlem	sedlo	k1gNnSc7	sedlo
pro	pro	k7c4	pro
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
je	být	k5eAaImIp3nS	být
Suezská	suezský	k2eAgFnSc1d1	Suezská
šíje	šíje	k1gFnSc1	šíje
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
hladina	hladina	k1gFnSc1	hladina
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
a	a	k8xC	a
ledová	ledový	k2eAgFnSc1d1	ledová
pokrývka	pokrývka	k1gFnSc1	pokrývka
-	-	kIx~	-
ani	ani	k8xC	ani
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřský	mateřský	k2eAgInSc4d1	mateřský
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Parent	Parent	k1gMnSc1	Parent
peak	peak	k1gMnSc1	peak
<g/>
)	)	kIx)	)
–	–	k?	–
vyšší	vysoký	k2eAgInSc4d2	vyšší
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
daným	daný	k2eAgInSc7d1	daný
vrcholem	vrchol	k1gInSc7	vrchol
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejbližší	blízký	k2eAgInSc1d3	Nejbližší
vyšší	vysoký	k2eAgInSc1d2	vyšší
vrchol	vrchol	k1gInSc1	vrchol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Velká	velká	k1gFnSc1	velká
Deštná	deštný	k2eAgFnSc1d1	Deštná
je	být	k5eAaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
Malé	Malá	k1gFnSc2	Malá
Deštné	deštný	k2eAgFnSc2d1	Deštná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
hor	hora	k1gFnPc2	hora
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
je	být	k5eAaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
Everest	Everest	k1gInSc1	Everest
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
5510	[number]	k4	5510
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
vrchol	vrchol	k1gInSc4	vrchol
nižší	nízký	k2eAgNnSc4d2	nižší
sedlo	sedlo	k1gNnSc4	sedlo
a	a	k8xC	a
mateřský	mateřský	k2eAgInSc4d1	mateřský
vrchol	vrchol	k1gInSc4	vrchol
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
krkonošský	krkonošský	k2eAgInSc4d1	krkonošský
Medvědín	Medvědín	k1gInSc4	Medvědín
jsou	být	k5eAaImIp3nP	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
Harrachovy	Harrachov	k1gInPc4	Harrachov
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nejbližším	blízký	k2eAgInSc7d3	Nejbližší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Železný	železný	k2eAgInSc1d1	železný
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
od	od	k7c2	od
Medvědína	Medvědín	k1gInSc2	Medvědín
oddělen	oddělit	k5eAaPmNgInS	oddělit
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
údolím	údolí	k1gNnSc7	údolí
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
sedlo	sedlo	k1gNnSc1	sedlo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Key	Key	k1gFnSc1	Key
col	cola	k1gFnPc2	cola
<g/>
)	)	kIx)	)
–	–	k?	–
sedlo	sednout	k5eAaPmAgNnS	sednout
oddělující	oddělující	k2eAgFnSc4d1	oddělující
danou	daný	k2eAgFnSc4d1	daná
horu	hora	k1gFnSc4	hora
od	od	k7c2	od
mateřského	mateřský	k2eAgInSc2d1	mateřský
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
vrcholy	vrchol	k1gInPc4	vrchol
kontinentů	kontinent	k1gInPc2	kontinent
nebo	nebo	k8xC	nebo
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
sedlem	sedlo	k1gNnSc7	sedlo
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
Ramzovské	Ramzovské	k2eAgNnSc6d1	Ramzovské
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
masiv	masiv	k1gInSc1	masiv
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
s	s	k7c7	s
Hrubým	hrubý	k2eAgInSc7d1	hrubý
Jeseníkem	Jeseník	k1gInSc7	Jeseník
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
vrchol	vrchol	k1gInSc4	vrchol
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
Pradědem	praděd	k1gMnSc7	praděd
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
alternativní	alternativní	k2eAgFnSc2d1	alternativní
definice	definice	k1gFnSc2	definice
je	být	k5eAaImIp3nS	být
klíčové	klíčový	k2eAgNnSc4d1	klíčové
sedlo	sedlo	k1gNnSc4	sedlo
posledním	poslední	k2eAgNnSc7d1	poslední
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
hora	hora	k1gFnSc1	hora
stoupající	stoupající	k2eAgFnSc2d1	stoupající
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
samostatného	samostatný	k2eAgInSc2d1	samostatný
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izolace	izolace	k1gFnSc1	izolace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Isolation	Isolation	k1gInSc1	Isolation
<g/>
)	)	kIx)	)
–	–	k?	–
osamocenost	osamocenost	k1gFnSc1	osamocenost
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejmenší	malý	k2eAgFnSc1d3	nejmenší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
vrcholu	vrchol	k1gInSc2	vrchol
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
vyššímu	vysoký	k2eAgInSc3d2	vyšší
bodu	bod	k1gInSc3	bod
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
bod	bod	k1gInSc1	bod
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
mateřském	mateřský	k2eAgInSc6d1	mateřský
vrcholu	vrchol	k1gInSc6	vrchol
ani	ani	k8xC	ani
na	na	k7c6	na
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vzdálenější	vzdálený	k2eAgInSc4d2	vzdálenější
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
bližší	blízký	k2eAgNnSc1d2	bližší
úbočí	úbočí	k1gNnSc1	úbočí
(	(	kIx(	(
<g/>
svah	svah	k1gInSc1	svah
<g/>
)	)	kIx)	)
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
Stříbrné	stříbrný	k1gInPc4	stříbrný
návrší	návrš	k1gFnPc2	návrš
je	být	k5eAaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
Studniční	studniční	k2eAgFnSc1d1	studniční
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
nejbližším	blízký	k2eAgInSc7d3	Nejbližší
vrcholem	vrchol	k1gInSc7	vrchol
Luční	luční	k2eAgFnSc1d1	luční
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
izolace	izolace	k1gFnSc1	izolace
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
svahu	svah	k1gInSc2	svah
Stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dominance	dominance	k1gFnSc1	dominance
–	–	k?	–
procentní	procentní	k2eAgNnSc4d1	procentní
vyjádření	vyjádření	k1gNnSc4	vyjádření
prominence	prominence	k1gFnSc2	prominence
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc1	Everest
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
dominantní	dominantní	k2eAgFnSc1d1	dominantní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
K2	K2	k1gFnSc1	K2
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
47	[number]	k4	47
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Gerlachovský	Gerlachovský	k2eAgInSc1d1	Gerlachovský
štít	štít	k1gInSc1	štít
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
89	[number]	k4	89
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
jen	jen	k9	jen
17	[number]	k4	17
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Sněžka	Sněžka	k1gFnSc1	Sněžka
má	mít	k5eAaImIp3nS	mít
dominanci	dominance	k1gFnSc4	dominance
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Luční	luční	k2eAgFnSc1d1	luční
hora	hora	k1gFnSc1	hora
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
nejdominantnější	dominantní	k2eAgFnSc1d3	nejdominantnější
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
dominantní	dominantní	k2eAgFnSc1d1	dominantní
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
Smrk	smrk	k1gInSc4	smrk
(	(	kIx(	(
<g/>
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jizera	Jizera	k1gFnSc1	Jizera
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
–	–	k?	–
Smrk	smrk	k1gInSc1	smrk
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Jizerských	jizerský	k2eAgFnPc2d1	Jizerská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
mateřským	mateřský	k2eAgInSc7d1	mateřský
vrcholem	vrchol	k1gInSc7	vrchol
(	(	kIx(	(
<g/>
polskou	polský	k2eAgFnSc7d1	polská
Wysokou	Wysoký	k2eAgFnSc7d1	Wysoký
Kopou	kopa	k1gFnSc7	kopa
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
mělčí	mělký	k2eAgNnSc4d2	mělčí
klíčové	klíčový	k2eAgNnSc4d1	klíčové
sedlo	sedlo	k1gNnSc4	sedlo
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
Jizera	Jizera	k1gFnSc1	Jizera
se	s	k7c7	s
Smrkem	smrk	k1gInSc7	smrk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
<g/>
"	"	kIx"	"
prominence	prominence	k1gFnSc1	prominence
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Wet	Wet	k1gFnSc1	Wet
prominence	prominence	k1gFnSc1	prominence
<g/>
)	)	kIx)	)
–	–	k?	–
prominence	prominence	k1gFnSc1	prominence
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
kontinentů	kontinent	k1gInPc2	kontinent
nebo	nebo	k8xC	nebo
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
"	"	kIx"	"
prominence	prominence	k1gFnSc1	prominence
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Dry	Dry	k1gFnSc1	Dry
prominence	prominence	k1gFnSc1	prominence
<g/>
)	)	kIx)	)
–	–	k?	–
alternativní	alternativní	k2eAgInSc4d1	alternativní
druh	druh	k1gInSc4	druh
prominence	prominence	k1gFnSc2	prominence
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nezohledňuje	zohledňovat	k5eNaImIp3nS	zohledňovat
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
počítá	počítat	k5eAaImIp3nS	počítat
tak	tak	k6eAd1	tak
i	i	k9	i
s	s	k7c7	s
podmořskými	podmořský	k2eAgNnPc7d1	podmořské
sedly	sedlo	k1gNnPc7	sedlo
a	a	k8xC	a
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Suchá	suchý	k2eAgFnSc1d1	suchá
prominence	prominence	k1gFnSc1	prominence
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
kontinentu	kontinent	k1gInSc2	kontinent
nebo	nebo	k8xC	nebo
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
mokrá	mokrý	k2eAgFnSc1d1	mokrá
prominence	prominence	k1gFnSc1	prominence
plus	plus	k6eAd1	plus
převýšení	převýšení	k1gNnSc3	převýšení
od	od	k7c2	od
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
podmořského	podmořský	k2eAgNnSc2d1	podmořské
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvosloví	názvosloví	k1gNnSc2	názvosloví
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
prominence	prominence	k1gFnSc2	prominence
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
související	související	k2eAgInPc4d1	související
termíny	termín	k1gInPc4	termín
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
tolik	tolik	k6eAd1	tolik
jako	jako	k8xS	jako
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
slova	slovo	k1gNnSc2	slovo
prominence	prominence	k1gFnSc2	prominence
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
výrazy	výraz	k1gInPc7	výraz
jako	jako	k8xC	jako
význačnost	význačnost	k1gFnSc1	význačnost
<g/>
,	,	kIx,	,
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
prominentní	prominentní	k2eAgFnSc1d1	prominentní
výška	výška	k1gFnSc1	výška
nebo	nebo	k8xC	nebo
převýšení	převýšení	k1gNnSc1	převýšení
od	od	k7c2	od
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgInPc1d1	alternativní
názvy	název	k1gInPc1	název
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
prominence	prominence	k1gFnSc2	prominence
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
relative	relativ	k1gInSc5	relativ
height	height	k1gMnSc1	height
<g/>
,	,	kIx,	,
shoulder	shoulder	k1gMnSc1	shoulder
drop	drop	k1gMnSc1	drop
nebo	nebo	k8xC	nebo
prime	prim	k1gInSc5	prim
factor	factor	k1gMnSc1	factor
<g/>
;	;	kIx,	;
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Schartenhöhe	Schartenhöhe	k1gInSc1	Schartenhöhe
nebo	nebo	k8xC	nebo
Prominenz	Prominenz	k1gInSc1	Prominenz
a	a	k8xC	a
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
hauteur	hauteura	k1gFnPc2	hauteura
de	de	k?	de
culminance	culminanec	k1gMnSc2	culminanec
nebo	nebo	k8xC	nebo
proéminence	proéminenec	k1gMnSc2	proéminenec
topographique	topographiqu	k1gMnSc2	topographiqu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
prominence	prominence	k1gFnSc2	prominence
==	==	k?	==
</s>
</p>
<p>
<s>
Prominence	prominence	k1gFnSc1	prominence
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k9	především
k	k	k7c3	k
hodnocení	hodnocení	k1gNnSc3	hodnocení
významnosti	významnost	k1gFnSc2	významnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
k	k	k7c3	k
posouzení	posouzení	k1gNnSc3	posouzení
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
horu	hora	k1gFnSc4	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
co	co	k3yQnSc1	co
už	už	k6eAd1	už
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
jednotné	jednotný	k2eAgNnSc1d1	jednotné
kritérium	kritérium	k1gNnSc1	kritérium
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
světová	světový	k2eAgNnPc4d1	světové
pohoří	pohoří	k1gNnPc4	pohoří
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
volí	volit	k5eAaImIp3nS	volit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
charakteru	charakter	k1gInSc3	charakter
pohoří	pohoří	k1gNnSc2	pohoří
nebo	nebo	k8xC	nebo
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
vrchol	vrchol	k1gInSc4	vrchol
obvykle	obvykle	k6eAd1	obvykle
považují	považovat	k5eAaImIp3nP	považovat
hory	hora	k1gFnPc1	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
500	[number]	k4	500
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
152	[number]	k4	152
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc4d1	stejná
hranici	hranice	k1gFnSc4	hranice
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
skotské	skotský	k1gInPc1	skotský
munros	munrosa	k1gFnPc2	munrosa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
alpské	alpský	k2eAgFnPc4d1	alpská
nebo	nebo	k8xC	nebo
tatranské	tatranský	k2eAgFnPc4d1	Tatranská
hory	hora	k1gFnPc4	hora
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
používá	používat	k5eAaImIp3nS	používat
prominence	prominence	k1gFnSc1	prominence
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Hory	hora	k1gFnSc2	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
ultraprominentní	ultraprominentní	k2eAgInSc4d1	ultraprominentní
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ultra	ultra	k2eAgFnSc1d1	ultra
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
asi	asi	k9	asi
1515	[number]	k4	1515
takových	takový	k3xDgInPc2	takový
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
:	:	kIx,	:
637	[number]	k4	637
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
355	[number]	k4	355
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
209	[number]	k4	209
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
119	[number]	k4	119
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
84	[number]	k4	84
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
69	[number]	k4	69
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
a	a	k8xC	a
39	[number]	k4	39
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
hory	hora	k1gFnPc4	hora
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Tisícovky	tisícovka	k1gFnSc2	tisícovka
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
zvolena	zvolen	k2eAgFnSc1d1	zvolena
prominence	prominence	k1gFnSc1	prominence
minimálně	minimálně	k6eAd1	minimálně
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
a	a	k8xC	a
izolace	izolace	k1gFnSc1	izolace
400	[number]	k4	400
m	m	kA	m
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
hlavní	hlavní	k2eAgInPc4d1	hlavní
vrcholy	vrchol	k1gInPc4	vrchol
a	a	k8xC	a
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
a	a	k8xC	a
izolace	izolace	k1gFnSc1	izolace
200	[number]	k4	200
m	m	kA	m
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnPc1d3	nejvýraznější
tisícovky	tisícovka	k1gFnPc1	tisícovka
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
a	a	k8xC	a
izolací	izolace	k1gFnSc7	izolace
1	[number]	k4	1
km	km	kA	km
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
ultratisícovky	ultratisícovka	k1gFnPc1	ultratisícovka
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vše	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
menší	malý	k2eAgFnSc7d2	menší
než	než	k8xS	než
5	[number]	k4	5
m	m	kA	m
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
spočinek	spočinek	k1gInSc1	spočinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejprominentnější	prominentní	k2eAgFnPc4d3	nejprominentnější
hory	hora	k1gFnPc4	hora
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
15	[number]	k4	15
nejprominentnějších	prominentní	k2eAgFnPc2d3	nejprominentnější
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
žebříček	žebříček	k1gInSc1	žebříček
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prvního	první	k4xOgInSc2	první
Everestu	Everest	k1gInSc2	Everest
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
jen	jen	k9	jen
1	[number]	k4	1
osmitisícovka	osmitisícovka	k1gFnSc1	osmitisícovka
(	(	kIx(	(
<g/>
Nanga	Nanga	k1gFnSc1	Nanga
Parbat	Parbat	k1gMnSc1	Parbat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
K2	K2	k1gFnSc2	K2
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pětitisícovka	pětitisícovka	k1gFnSc1	pětitisícovka
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
všech	všecek	k3xTgInPc2	všecek
7	[number]	k4	7
vrcholů	vrchol	k1gInPc2	vrchol
Seven	Seven	k2eAgInSc1d1	Seven
Summits	Summits	k1gInSc1	Summits
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
havajská	havajský	k2eAgFnSc1d1	Havajská
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
jen	jen	k9	jen
4205	[number]	k4	4205
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
necelá	celý	k2eNgFnSc1d1	necelá
polovina	polovina	k1gFnSc1	polovina
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
Everestu	Everest	k1gInSc2	Everest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
nejprominentnější	prominentní	k2eAgFnSc7d3	nejprominentnější
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
suché	suchý	k2eAgFnSc2d1	suchá
prominence	prominence	k1gFnSc2	prominence
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejprominentnější	prominentní	k2eAgFnSc7d3	nejprominentnější
horou	hora	k1gFnSc7	hora
(	(	kIx(	(
<g/>
4205	[number]	k4	4205
+	+	kIx~	+
5125	[number]	k4	5125
=	=	kIx~	=
9330	[number]	k4	9330
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Everestu	Everest	k1gInSc6	Everest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejprominentnější	prominentní	k2eAgFnPc1d3	nejprominentnější
hory	hora	k1gFnPc1	hora
Česka	Česko	k1gNnSc2	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
světových	světový	k2eAgFnPc2d1	světová
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
i	i	k9	i
tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Studniční	studniční	k2eAgFnSc1d1	studniční
hora	hora	k1gFnSc1	hora
má	mít	k5eAaImIp3nS	mít
prominenci	prominence	k1gFnSc4	prominence
jen	jen	k9	jen
60	[number]	k4	60
m	m	kA	m
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
českých	český	k2eAgFnPc2d1	Česká
tisícovek	tisícovka	k1gFnPc2	tisícovka
Čerchov	Čerchov	k1gInSc1	Čerchov
(	(	kIx(	(
<g/>
1024	[number]	k4	1024
m	m	kA	m
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
desátá	desátá	k1gFnSc1	desátá
nejprominentnější	prominentní	k2eAgFnSc1d3	nejprominentnější
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Plechý	plechý	k2eAgInSc4d1	plechý
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
také	také	k9	také
zastoupení	zastoupení	k1gNnSc1	zastoupení
hned	hned	k6eAd1	hned
tří	tři	k4xCgFnPc2	tři
beskydských	beskydský	k2eAgFnPc2d1	Beskydská
hor	hora	k1gFnPc2	hora
(	(	kIx(	(
<g/>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
Kněhyně	kněhyně	k1gFnSc1	kněhyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
odlišný	odlišný	k2eAgInSc1d1	odlišný
charakter	charakter	k1gInSc1	charakter
tohoto	tento	k3xDgNnSc2	tento
karpatského	karpatský	k2eAgNnSc2d1	Karpatské
pohoří	pohoří	k1gNnSc2	pohoří
s	s	k7c7	s
hlubokými	hluboký	k2eAgNnPc7d1	hluboké
údolími	údolí	k1gNnPc7	údolí
mezi	mezi	k7c7	mezi
strmými	strmý	k2eAgMnPc7d1	strmý
horskými	horský	k2eAgMnPc7d1	horský
hřbety	hřbet	k1gMnPc7	hřbet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
všechny	všechen	k3xTgFnPc4	všechen
české	český	k2eAgFnPc4d1	Česká
hory	hora	k1gFnPc4	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
alespoň	alespoň	k9	alespoň
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
české	český	k2eAgFnPc1d1	Česká
hory	hora	k1gFnPc1	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Seznam	seznam	k1gInSc4	seznam
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Topographic	Topographice	k1gFnPc2	Topographice
prominence	prominence	k1gFnSc2	prominence
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Schartenhöhe	Schartenhöhe	k1gFnSc6	Schartenhöhe
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
</s>
</p>
<p>
<s>
Převýšení	převýšení	k1gNnSc1	převýšení
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
-	-	kIx~	-
české	český	k2eAgFnSc2d1	Česká
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
kopce	kopec	k1gInSc2	kopec
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
100	[number]	k4	100
m	m	kA	m
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejprominentnějších	prominentní	k2eAgFnPc2d3	nejprominentnější
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
-	-	kIx~	-
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hory	hora	k1gFnSc2	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
500	[number]	k4	500
m	m	kA	m
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
podle	podle	k7c2	podle
prominence	prominence	k1gFnSc2	prominence
-	-	kIx~	-
alpské	alpský	k2eAgNnSc1d1	alpské
ultras	ultras	k1gInSc1	ultras
-	-	kIx~	-
hory	hora	k1gFnPc1	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
1500	[number]	k4	1500
m	m	kA	m
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ultras	ultras	k1gInSc1	ultras
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
-	-	kIx~	-
evropské	evropský	k2eAgFnSc2d1	Evropská
ultras	ultras	k1gInSc1	ultras
-	-	kIx~	-
hory	hora	k1gFnPc1	hora
s	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
nad	nad	k7c4	nad
1500	[number]	k4	1500
m	m	kA	m
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
-	-	kIx~	-
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
světa	svět	k1gInSc2	svět
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
prominence	prominence	k1gFnSc2	prominence
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Prominence	prominence	k1gFnSc2	prominence
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznamy	seznam	k1gInPc1	seznam
nejprominentnějších	prominentní	k2eAgFnPc2d3	nejprominentnější
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
všech	všecek	k3xTgFnPc2	všecek
ultras	ultrasa	k1gFnPc2	ultrasa
<g/>
)	)	kIx)	)
na	na	k7c4	na
Peaklist	Peaklist	k1gInSc4	Peaklist
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
