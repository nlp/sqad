<s>
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
komiksové	komiksový	k2eAgNnSc1d1	komiksové
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgNnSc1d1	spadající
pod	pod	k7c4	pod
společnost	společnost	k1gFnSc4	společnost
DC	DC	kA	DC
Entertainment	Entertainment	k1gInSc4	Entertainment
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastní	vlastní	k2eAgFnSc4d1	vlastní
společnost	společnost	k1gFnSc4	společnost
Warner	Warner	k1gMnSc1	Warner
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
jako	jako	k8xS	jako
National	National	k1gMnSc1	National
Allied	Allied	k1gMnSc1	Allied
Publications	Publications	k1gInSc4	Publications
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
DC	DC	kA	DC
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
populární	populární	k2eAgFnSc2d1	populární
komiksové	komiksový	k2eAgFnSc2d1	komiksová
série	série	k1gFnSc2	série
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vydávána	vydávat	k5eAaImNgFnS	vydávat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
postavami	postava	k1gFnPc7	postava
jejich	jejich	k3xOp3gMnPc2	jejich
komiksových	komiksový	k2eAgMnPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
jsou	být	k5eAaImIp3nP	být
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
Wonder	Wonder	k1gMnSc1	Wonder
Woman	Woman	k1gMnSc1	Woman
<g/>
,	,	kIx,	,
Green	Green	k2eAgMnSc1d1	Green
Lantern	Lantern	k1gMnSc1	Lantern
<g/>
,	,	kIx,	,
Flash	Flash	k1gMnSc1	Flash
<g/>
,	,	kIx,	,
Aquaman	Aquaman	k1gMnSc1	Aquaman
<g/>
,	,	kIx,	,
Hawkman	Hawkman	k1gMnSc1	Hawkman
<g/>
,	,	kIx,	,
Hawkgirl	Hawkgirl	k1gMnSc1	Hawkgirl
<g/>
,	,	kIx,	,
Green	Green	k2eAgMnSc1d1	Green
Arrow	Arrow	k1gMnSc1	Arrow
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Canary	Canara	k1gFnSc2	Canara
a	a	k8xC	a
Martian	Martian	k1gMnSc1	Martian
Manhunter	Manhunter	k1gMnSc1	Manhunter
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
týmy	tým	k1gInPc1	tým
Justice	justice	k1gFnSc1	justice
League	League	k1gInSc1	League
a	a	k8xC	a
Teen	Teen	k1gInSc1	Teen
Titans	Titansa	k1gFnPc2	Titansa
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Malcolm	Malcolm	k1gMnSc1	Malcolm
Wheeler-Nicholson	Wheeler-Nicholson	k1gMnSc1	Wheeler-Nicholson
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
New	New	k1gMnSc1	New
Fun	Fun	k1gMnSc1	Fun
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Big	Big	k1gMnSc1	Big
Comic	Comic	k1gMnSc1	Comic
Magazine	Magazin	k1gInSc5	Magazin
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
pravým	pravý	k2eAgInSc7d1	pravý
komiksovým	komiksový	k2eAgInSc7d1	komiksový
sešitem	sešit	k1gInSc7	sešit
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
byl	být	k5eAaImAgInS	být
New	New	k1gFnSc4	New
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
New	New	k1gFnSc4	New
Adventure	Adventur	k1gMnSc5	Adventur
Comics	comics	k1gInSc1	comics
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
na	na	k7c6	na
Adventure	Adventur	k1gMnSc5	Adventur
Comics	comics	k1gInSc4	comics
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1937	[number]	k4	1937
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
série	série	k1gFnSc1	série
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1939	[number]	k4	1939
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
na	na	k7c4	na
detektivní	detektivní	k2eAgFnSc4d1	detektivní
a	a	k8xC	a
krimi	krimi	k1gFnSc4	krimi
tematiku	tematika	k1gFnSc4	tematika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
objevily	objevit	k5eAaPmAgFnP	objevit
postavy	postava	k1gFnPc1	postava
Slam	slam	k1gInSc1	slam
Bradley	Bradlea	k1gFnSc2	Bradlea
nebo	nebo	k8xC	nebo
Speed	Speed	k1gInSc4	Speed
Saunders	Saundersa	k1gFnPc2	Saundersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nově	nově	k6eAd1	nově
vydávána	vydáván	k2eAgFnSc1d1	vydávána
série	série	k1gFnSc1	série
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
představen	představen	k2eAgMnSc1d1	představen
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Jerry	Jerr	k1gInPc7	Jerr
Siegel	Siegel	k1gMnSc1	Siegel
a	a	k8xC	a
Joe	Joe	k1gMnSc1	Joe
Shuster	Shuster	k1gMnSc1	Shuster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
na	na	k7c4	na
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc4	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
National	National	k1gFnSc2	National
Periodical	Periodical	k1gFnSc2	Periodical
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rané	raný	k2eAgFnSc6d1	raná
éře	éra	k1gFnSc6	éra
často	často	k6eAd1	často
soudně	soudně	k6eAd1	soudně
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
plagiátorství	plagiátorství	k1gNnSc3	plagiátorství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
Supermana	superman	k1gMnSc4	superman
(	(	kIx(	(
<g/>
např.	např.	kA	např.
postava	postava	k1gFnSc1	postava
Wonder	Wonder	k1gMnSc1	Wonder
Man	Man	k1gMnSc1	Man
od	od	k7c2	od
Fox	fox	k1gInSc4	fox
Comics	comics	k1gInSc1	comics
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
pře	pře	k1gFnSc1	pře
se	se	k3xPyFc4	se
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
o	o	k7c4	o
postavu	postava	k1gFnSc4	postava
Captain	Captain	k2eAgInSc4d1	Captain
Marvel	Marvel	k1gInSc4	Marvel
od	od	k7c2	od
Fawcett	Fawcetta	k1gFnPc2	Fawcetta
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velkého	velký	k2eAgInSc2d1	velký
útlumu	útlum	k1gInSc2	útlum
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
superhrdinské	superhrdinský	k2eAgInPc4d1	superhrdinský
komiksy	komiks	k1gInPc4	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Fawcett	Fawcett	k1gInSc1	Fawcett
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
a	a	k8xC	a
přestal	přestat	k5eAaPmAgInS	přestat
vydávat	vydávat	k5eAaImF	vydávat
tento	tento	k3xDgInSc4	tento
komiks	komiks	k1gInSc4	komiks
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
DC	DC	kA	DC
později	pozdě	k6eAd2	pozdě
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
postavu	postava	k1gFnSc4	postava
Captain	Captain	k2eAgInSc4d1	Captain
Marvel	Marvel	k1gInSc4	Marvel
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vydávat	vydávat	k5eAaPmF	vydávat
v	v	k7c6	v
sešitu	sešit	k1gInSc6	sešit
Shazam	Shazam	k1gInSc1	Shazam
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
poválečného	poválečný	k2eAgInSc2d1	poválečný
útlumu	útlum	k1gInSc2	útlum
se	s	k7c7	s
DC	DC	kA	DC
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
komiksová	komiksový	k2eAgNnPc1d1	komiksové
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
sci-fi	scii	k1gNnPc2	sci-fi
<g/>
,	,	kIx,	,
westernových	westernový	k2eAgInPc2d1	westernový
<g/>
,	,	kIx,	,
romantických	romantický	k2eAgInPc2d1	romantický
a	a	k8xC	a
zábavných	zábavný	k2eAgInPc2d1	zábavný
komiksů	komiks	k1gInPc2	komiks
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
i	i	k8xC	i
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vydávány	vydávat	k5eAaImNgInP	vydávat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
Jack	Jack	k1gMnSc1	Jack
Liebowitz	Liebowitz	k1gMnSc1	Liebowitz
a	a	k8xC	a
Irwin	Irwin	k1gMnSc1	Irwin
Donenfeld	Donenfeld	k1gMnSc1	Donenfeld
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
Robert	Robert	k1gMnSc1	Robert
Kanigher	Kanighra	k1gFnPc2	Kanighra
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Broome	Broom	k1gInSc5	Broom
a	a	k8xC	a
Carmine	Carmin	k1gInSc5	Carmin
Infantino	Infantin	k2eAgNnSc1d1	Infantino
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
dostali	dostat	k5eAaPmAgMnP	dostat
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
některých	některý	k3yIgFnPc2	některý
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
Flashe	Flash	k1gMnSc2	Flash
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jinak	jinak	k6eAd1	jinak
vzniknul	vzniknout	k5eAaPmAgMnS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
vydáván	vydáván	k2eAgInSc1d1	vydáván
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Showcase	Showcasa	k1gFnSc6	Showcasa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
až	až	k9	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
se	se	k3xPyFc4	se
také	také	k9	také
dočkal	dočkat	k5eAaPmAgInS	dočkat
Green	Green	k1gInSc1	Green
Lantern	Lanterno	k1gNnPc2	Lanterno
a	a	k8xC	a
supertým	supertá	k1gFnPc3	supertá
Justice	justice	k1gFnSc2	justice
League	Leagu	k1gFnSc2	Leagu
of	of	k?	of
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
JLA	JLA	kA	JLA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Editor	editor	k1gMnSc1	editor
Julius	Julius	k1gMnSc1	Julius
Schwartz	Schwartz	k1gMnSc1	Schwartz
tehdy	tehdy	k6eAd1	tehdy
dal	dát	k5eAaPmAgMnS	dát
zelenou	zelená	k1gFnSc4	zelená
i	i	k8xC	i
počátkum	počátkum	k1gNnSc4	počátkum
vesmíru	vesmír	k1gInSc2	vesmír
později	pozdě	k6eAd2	pozdě
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
DC	DC	kA	DC
Universe	Universe	k1gFnPc1	Universe
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechal	nechat	k5eAaPmAgInS	nechat
popsat	popsat	k5eAaPmF	popsat
svět	svět	k1gInSc1	svět
superhrdinů	superhrdin	k1gMnPc2	superhrdin
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
alternativní	alternativní	k2eAgInSc4d1	alternativní
vesmír	vesmír	k1gInSc4	vesmír
Earth	Eartha	k1gFnPc2	Eartha
2	[number]	k4	2
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
moderní	moderní	k2eAgMnPc1d1	moderní
hrdinové	hrdina	k1gMnPc1	hrdina
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c4	na
Earth	Earth	k1gInSc4	Earth
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
natáčen	natáčen	k2eAgInSc1d1	natáčen
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
s	s	k7c7	s
Batmanem	Batman	k1gInSc7	Batman
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
více	hodně	k6eAd2	hodně
fanoušků	fanoušek	k1gMnPc2	fanoušek
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
přibyli	přibýt	k5eAaPmAgMnP	přibýt
autoři	autor	k1gMnPc1	autor
Steve	Steve	k1gMnSc1	Steve
Ditko	Ditko	k1gNnSc1	Ditko
<g/>
,	,	kIx,	,
Neal	Neal	k1gInSc1	Neal
Adams	Adams	k1gInSc1	Adams
a	a	k8xC	a
Dennis	Dennis	k1gFnSc1	Dennis
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nahradili	nahradit	k5eAaPmAgMnP	nahradit
Joe	Joe	k1gFnSc7	Joe
Kuberta	Kubert	k1gMnSc2	Kubert
a	a	k8xC	a
Dicka	Dicek	k1gMnSc2	Dicek
Giordana	Giordan	k1gMnSc2	Giordan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
bylo	být	k5eAaImAgNnS	být
National	National	k1gFnSc3	National
Periodical	Periodical	k1gFnSc3	Periodical
Publications	Publicationsa	k1gFnPc2	Publicationsa
odkoupeno	odkoupen	k2eAgNnSc1d1	odkoupeno
společností	společnost	k1gFnSc7	společnost
Kinney	Kinnea	k1gFnSc2	Kinnea
National	National	k1gFnSc2	National
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
odkoupena	odkoupit	k5eAaPmNgFnS	odkoupit
společností	společnost	k1gFnSc7	společnost
Warner	Warnra	k1gFnPc2	Warnra
Communications	Communicationsa	k1gFnPc2	Communicationsa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
k	k	k7c3	k
DC	DC	kA	DC
přidal	přidat	k5eAaPmAgMnS	přidat
Jack	Jack	k1gMnSc1	Jack
Kirby	Kirba	k1gMnSc2	Kirba
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
autor	autor	k1gMnSc1	autor
z	z	k7c2	z
Marvel	Marvela	k1gFnPc2	Marvela
Comics	comics	k1gInSc1	comics
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
vlastní	vlastní	k2eAgInPc4d1	vlastní
příběhy	příběh	k1gInPc4	příběh
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c6	na
spojitosti	spojitost	k1gFnSc6	spojitost
předchozích	předchozí	k2eAgMnPc2d1	předchozí
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
série	série	k1gFnPc1	série
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xC	jako
The	The	k1gFnPc1	The
Fourth	Fourtha	k1gFnPc2	Fourtha
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Napočátku	Napočátko	k1gNnSc3	Napočátko
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Marvel	Marvel	k1gMnSc1	Marvel
<g/>
,	,	kIx,	,
DC	DC	kA	DC
dostalo	dostat	k5eAaPmAgNnS	dostat
od	od	k7c2	od
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
žádost	žádost	k1gFnSc1	žádost
o	o	k7c6	o
začlenění	začlenění	k1gNnSc6	začlenění
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
braní	braní	k1gNnSc3	braní
drog	droga	k1gFnPc2	droga
do	do	k7c2	do
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
Dennis	Dennis	k1gFnPc2	Dennis
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
a	a	k8xC	a
Neal	Neal	k1gMnSc1	Neal
Adams	Adamsa	k1gFnPc2	Adamsa
ji	on	k3xPp3gFnSc4	on
začlenili	začlenit	k5eAaPmAgMnP	začlenit
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
"	"	kIx"	"
<g/>
Snowbirds	Snowbirds	k1gInSc1	Snowbirds
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Fly	Fly	k1gMnSc1	Fly
<g/>
"	"	kIx"	"
v	v	k7c6	v
sešitu	sešit	k1gInSc6	sešit
Green	Green	k2eAgInSc1d1	Green
Lantern	Lantern	k1gInSc1	Lantern
/	/	kIx~	/
Green	Green	k1gInSc1	Green
Arrow	Arrow	k1gFnSc2	Arrow
#	#	kIx~	#
<g/>
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
Superman	superman	k1gMnSc1	superman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
restartu	restart	k1gInSc3	restart
charakterů	charakter	k1gInPc2	charakter
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Crisis	Crisis	k1gFnSc2	Crisis
on	on	k3xPp3gMnSc1	on
Infinite	Infinit	k1gInSc5	Infinit
Earths	Earthsa	k1gFnPc2	Earthsa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedli	vést	k5eAaImAgMnP	vést
autoři	autor	k1gMnPc1	autor
Marv	Marv	k1gMnSc1	Marv
Wolfman	Wolfman	k1gMnSc1	Wolfman
a	a	k8xC	a
George	Georg	k1gMnSc4	Georg
Pérez	Pérez	k1gInSc4	Pérez
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
série	série	k1gFnSc1	série
New	New	k1gMnPc2	New
Teen	Teen	k1gInSc4	Teen
Titans	Titansa	k1gFnPc2	Titansa
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
úspěchu	úspěch	k1gInSc6	úspěch
i	i	k9	i
spin-off	spinff	k1gMnSc1	spin-off
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
the	the	k?	the
New	New	k1gMnSc1	New
Teen	Teen	k1gMnSc1	Teen
Titans	Titans	k1gInSc4	Titans
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
DC	DC	kA	DC
Comics	comics	k1gInSc4	comics
přivalila	přivalit	k5eAaPmAgFnS	přivalit
britská	britský	k2eAgFnSc1d1	britská
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
oživil	oživit	k5eAaPmAgMnS	oživit
sérii	série	k1gFnSc3	série
Swamp	Swamp	k1gMnSc1	Swamp
Thing	Thing	k1gMnSc1	Thing
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
Watchmen	Watchmen	k2eAgMnSc1d1	Watchmen
<g/>
,	,	kIx,	,
a	a	k8xC	a
první	první	k4xOgFnSc2	první
zakázky	zakázka	k1gFnSc2	zakázka
dostávali	dostávat	k5eAaImAgMnP	dostávat
i	i	k9	i
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
a	a	k8xC	a
Grant	grant	k1gInSc1	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
vydány	vydán	k2eAgInPc1d1	vydán
temnější	temný	k2eAgInPc1d2	temnější
příběhy	příběh	k1gInPc1	příběh
i	i	k9	i
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
byl	být	k5eAaImAgMnS	být
přepracován	přepracován	k2eAgInSc4d1	přepracován
Frankem	Frank	k1gMnSc7	Frank
Millerem	Miller	k1gMnSc7	Miller
v	v	k7c6	v
komiksech	komiks	k1gInPc6	komiks
Návrat	návrat	k1gInSc1	návrat
temného	temný	k2eAgMnSc2d1	temný
rytíře	rytíř	k1gMnSc2	rytíř
a	a	k8xC	a
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Temnější	temný	k2eAgMnSc1d2	temnější
duch	duch	k1gMnSc1	duch
DC	DC	kA	DC
Comcis	Comcis	k1gInSc1	Comcis
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
do	do	k7c2	do
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
DC	DC	kA	DC
založilo	založit	k5eAaPmAgNnS	založit
imprint	imprint	k1gInSc4	imprint
Vertigo	Vertigo	k6eAd1	Vertigo
comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydává	vydávat	k5eAaImIp3nS	vydávat
temnější	temný	k2eAgInSc1d2	temnější
<g/>
,	,	kIx,	,
horrorové	horrorová	k1gFnPc4	horrorová
či	či	k8xC	či
fantasy	fantas	k1gInPc4	fantas
komiksy	komiks	k1gInPc7	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
superhrdinské	superhrdinský	k2eAgFnPc1d1	superhrdinská
postavy	postava	k1gFnPc1	postava
začaly	začít	k5eAaPmAgFnP	začít
zažívat	zažívat	k5eAaImF	zažívat
těžké	těžký	k2eAgInPc4d1	těžký
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
The	The	k1gMnSc1	The
Death	Death	k1gMnSc1	Death
of	of	k?	of
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
byl	být	k5eAaImAgMnS	být
zmrzačen	zmrzačit	k5eAaPmNgMnS	zmrzačit
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Knightfall	Knightfall	k1gMnSc1	Knightfall
a	a	k8xC	a
Green	Green	k2eAgMnSc1d1	Green
Lantern	Lantern	k1gMnSc1	Lantern
Hal	hala	k1gFnPc2	hala
Jordan	Jordan	k1gMnSc1	Jordan
se	se	k3xPyFc4	se
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c6	v
superzločince	superzločinka	k1gFnSc6	superzločinka
Parallexe	Parallex	k1gInSc5	Parallex
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Emerald	Emerald	k1gMnSc1	Emerald
Twilight	Twilight	k1gMnSc1	Twilight
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dějové	dějový	k2eAgFnPc1d1	dějová
linie	linie	k1gFnPc1	linie
velmi	velmi	k6eAd1	velmi
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
tržby	tržba	k1gFnPc1	tržba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožnily	umožnit	k5eAaPmAgInP	umožnit
vznik	vznik	k1gInSc4	vznik
crossoverů	crossover	k1gInPc2	crossover
jako	jako	k8xS	jako
Zero	Zero	k1gMnSc1	Zero
Hour	Hour	k1gMnSc1	Hour
<g/>
:	:	kIx,	:
Crisis	Crisis	k1gInSc1	Crisis
in	in	k?	in
Time	Time	k1gInSc1	Time
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
autorem	autor	k1gMnSc7	autor
Batmana	Batman	k1gMnSc2	Batman
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgMnS	být
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
nové	nový	k2eAgInPc1d1	nový
imprinty	imprint	k1gInPc1	imprint
jako	jako	k8xS	jako
Impact	Impact	k2eAgInSc1d1	Impact
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
Paradox	paradox	k1gInSc1	paradox
Press	Press	k1gInSc1	Press
a	a	k8xC	a
Wildstorm	Wildstorm	k1gInSc1	Wildstorm
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaPmNgFnP	vydávat
nové	nový	k2eAgFnPc1d1	nová
série	série	k1gFnSc1	série
jako	jako	k8xC	jako
Road	Road	k1gInSc1	Road
to	ten	k3xDgNnSc4	ten
Perdition	Perdition	k1gInSc4	Perdition
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
výjimečných	výjimečný	k2eAgNnPc2d1	výjimečné
(	(	kIx(	(
<g/>
také	také	k9	také
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Strong	Strong	k1gMnSc1	Strong
či	či	k8xC	či
Promethea	Prometheus	k1gMnSc4	Prometheus
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
autorem	autor	k1gMnSc7	autor
Batmana	Batman	k1gMnSc2	Batman
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
Grant	grant	k1gInSc1	grant
Morrison	Morrison	k1gInSc1	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
filmu	film	k1gInSc2	film
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
restart	restart	k1gInSc4	restart
DC	DC	kA	DC
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Infinite	Infinit	k1gInSc5	Infinit
Crisis	Crisis	k1gFnSc5	Crisis
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
restartu	restart	k1gMnSc3	restart
byla	být	k5eAaImAgFnS	být
série	série	k1gFnSc1	série
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vydávána	vydávat	k5eAaImNgFnS	vydávat
i	i	k9	i
série	série	k1gFnSc1	série
All-Star	All-Stara	k1gFnPc2	All-Stara
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
také	také	k6eAd1	také
byla	být	k5eAaImAgFnS	být
érou	éra	k1gFnSc7	éra
nových	nový	k2eAgInPc2d1	nový
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
jako	jako	k8xC	jako
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
a	a	k8xC	a
Superman	superman	k1gMnSc1	superman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
spustili	spustit	k5eAaPmAgMnP	spustit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Animation	Animation	k1gInSc1	Animation
projekt	projekt	k1gInSc1	projekt
vydávání	vydávání	k1gNnSc2	vydávání
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
DC	DC	kA	DC
Universe	Universe	k1gFnPc1	Universe
Animated	Animated	k1gMnSc1	Animated
Original	Original	k1gMnSc1	Original
Movies	Movies	k1gMnSc1	Movies
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dlouho	dlouho	k6eAd1	dlouho
očekávanému	očekávaný	k2eAgInSc3d1	očekávaný
a	a	k8xC	a
připravovanému	připravovaný	k2eAgMnSc3d1	připravovaný
restartu	restart	k1gMnSc3	restart
DC	DC	kA	DC
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
přivést	přivést	k5eAaPmF	přivést
nové	nový	k2eAgMnPc4d1	nový
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nedisponovali	disponovat	k5eNaBmAgMnP	disponovat
znalostmi	znalost	k1gFnPc7	znalost
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
DC	DC	kA	DC
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
komiksy	komiks	k1gInPc1	komiks
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vydávány	vydáván	k2eAgInPc1d1	vydáván
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
New	New	k1gFnSc2	New
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Význačnými	význačný	k2eAgMnPc7d1	význačný
autory	autor	k1gMnPc7	autor
New	New	k1gFnSc2	New
52	[number]	k4	52
byli	být	k5eAaImAgMnP	být
Geoff	Geoff	k1gMnSc1	Geoff
Johns	Johns	k1gInSc4	Johns
<g/>
,	,	kIx,	,
Grant	grant	k1gInSc4	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
<g/>
,	,	kIx,	,
Scott	Scott	k1gMnSc1	Scott
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
Conner	Conner	kA	Conner
<g/>
,	,	kIx,	,
Brian	Brian	k1gInSc1	Brian
Azzarello	Azzarello	k1gNnSc1	Azzarello
nebo	nebo	k8xC	nebo
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc6	Lea
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
crossoverem	crossover	k1gInSc7	crossover
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
Future	Futur	k1gMnSc5	Futur
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějšími	úspěšný	k2eAgFnPc7d3	nejúspěšnější
sériemi	série	k1gFnPc7	série
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Wonder	Wonder	k1gInSc1	Wonder
Woman	Womana	k1gFnPc2	Womana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
Justice	justice	k1gFnSc1	justice
League	League	k1gFnPc2	League
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
a	a	k8xC	a
Harley	harley	k1gInSc1	harley
Quinn	Quinna	k1gFnPc2	Quinna
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
nová	nový	k2eAgFnSc1d1	nová
velká	velký	k2eAgFnSc1d1	velká
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
DC	DC	kA	DC
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
minisérie	minisérie	k1gFnSc1	minisérie
Convergence	Convergence	k1gFnSc1	Convergence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovedla	dovést	k5eAaPmAgFnS	dovést
DC	DC	kA	DC
vesmír	vesmír	k1gInSc4	vesmír
ke	k	k7c3	k
zmíněným	zmíněný	k2eAgFnPc3d1	zmíněná
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
bylo	být	k5eAaImAgNnS	být
rozšířit	rozšířit	k5eAaPmF	rozšířit
DC	DC	kA	DC
portfolio	portfolio	k1gNnSc4	portfolio
a	a	k8xC	a
přivést	přivést	k5eAaPmF	přivést
nové	nový	k2eAgMnPc4d1	nový
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
tedy	tedy	k8xC	tedy
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
různorodost	různorodost	k1gFnSc4	různorodost
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
kresbách	kresba	k1gFnPc6	kresba
i	i	k8xC	i
způsobech	způsob	k1gInPc6	způsob
uchopení	uchopení	k1gNnSc2	uchopení
dané	daný	k2eAgFnSc2d1	daná
komiksové	komiksový	k2eAgFnSc2d1	komiksová
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
New	New	k1gFnPc2	New
52	[number]	k4	52
sérií	série	k1gFnPc2	série
bylo	být	k5eAaImAgNnS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
29	[number]	k4	29
nejlépe	dobře	k6eAd3	dobře
se	s	k7c7	s
prodávajících	prodávající	k2eAgMnPc2d1	prodávající
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
byly	být	k5eAaImAgFnP	být
zastaveny	zastaven	k2eAgFnPc1d1	zastavena
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
24	[number]	k4	24
novými	nový	k2eAgFnPc7d1	nová
sériemi	série	k1gFnPc7	série
<g/>
.	.	kIx.	.
</s>
<s>
Relaunch	Relaunch	k1gMnSc1	Relaunch
DC	DC	kA	DC
Rebirth	Rebirth	k1gMnSc1	Rebirth
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
DC	DC	kA	DC
vesmír	vesmír	k1gInSc1	vesmír
vrátil	vrátit	k5eAaPmAgInS	vrátit
před	před	k7c4	před
události	událost	k1gFnPc4	událost
eventu	event	k1gInSc2	event
Flashpoint	Flashpointa	k1gFnPc2	Flashpointa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
kontinuity	kontinuita	k1gFnSc2	kontinuita
New	New	k1gFnSc3	New
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnPc1	série
však	však	k9	však
byly	být	k5eAaImAgFnP	být
restartovány	restartovat	k5eAaBmNgFnP	restartovat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
od	od	k7c2	od
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnými	úspěšný	k2eAgFnPc7d1	úspěšná
sériemi	série	k1gFnPc7	série
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
klasické	klasický	k2eAgInPc1d1	klasický
taháky	tahák	k1gInPc1	tahák
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Justice	justice	k1gFnSc1	justice
League	League	k1gFnSc1	League
(	(	kIx(	(
<g/>
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superman	superman	k1gMnSc1	superman
(	(	kIx(	(
<g/>
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
Harley	harley	k1gInSc1	harley
Quinn	Quinna	k1gFnPc2	Quinna
(	(	kIx(	(
<g/>
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	s	k7c7	s
DC	DC	kA	DC
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
provázané	provázaný	k2eAgNnSc4d1	provázané
filmové	filmový	k2eAgNnSc4d1	filmové
univerzum	univerzum	k1gNnSc4	univerzum
(	(	kIx(	(
<g/>
DC	DC	kA	DC
Extended	Extended	k1gInSc1	Extended
Universe	Universe	k1gFnSc1	Universe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
filmem	film	k1gInSc7	film
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Batman	Batman	k1gMnSc1	Batman
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Úsvit	úsvit	k1gInSc1	úsvit
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
oddíl	oddíl	k1gInSc1	oddíl
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připravovány	připravován	k2eAgInPc1d1	připravován
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
snímky	snímek	k1gInPc4	snímek
Wonder	Wondra	k1gFnPc2	Wondra
Woman	Womana	k1gFnPc2	Womana
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
a	a	k8xC	a
Aquaman	Aquaman	k1gMnSc1	Aquaman
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
televizní	televizní	k2eAgFnPc4d1	televizní
stanice	stanice	k1gFnPc4	stanice
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
také	také	k9	také
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
seriály	seriál	k1gInPc4	seriál
Arrow	Arrow	k1gFnSc2	Arrow
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Flash	Flash	k1gMnSc1	Flash
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Supergirl	Supergirl	k1gMnSc1	Supergirl
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Legends	Legends	k1gInSc1	Legends
of	of	k?	of
Tomorrow	Tomorrow	k1gFnSc2	Tomorrow
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
lze	lze	k6eAd1	lze
sem	sem	k6eAd1	sem
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
zrušený	zrušený	k2eAgInSc4d1	zrušený
seriál	seriál	k1gInSc4	seriál
Constantine	Constantin	k1gInSc5	Constantin
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
The	The	k1gFnSc2	The
CW	CW	kA	CW
své	svůj	k3xOyFgNnSc4	svůj
DC	DC	kA	DC
seriály	seriál	k1gInPc1	seriál
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
vesmíru	vesmír	k1gInSc2	vesmír
s	s	k7c7	s
názvem	název	k1gInSc7	název
Arrowverse	Arrowverse	k1gFnSc2	Arrowverse
<g/>
.	.	kIx.	.
</s>
