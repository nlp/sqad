<s>
Fergie	Fergie	k1gFnSc1	Fergie
Duhamel	Duhamela	k1gFnPc2	Duhamela
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
jako	jako	k8xC	jako
Stacy	Stac	k2eAgFnPc1d1	Stac
Ann	Ann	k1gFnPc1	Ann
Ferguson	Fergusona	k1gFnPc2	Fergusona
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g/>
března	březen	k1gInSc2	březen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Hacienda	hacienda	k1gFnSc1	hacienda
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
působící	působící	k2eAgFnSc1d1	působící
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
Terra	Terr	k1gInSc2	Terr
a	a	k8xC	a
Pat	pata	k1gFnPc2	pata
<g/>
,	,	kIx,	,
podporovali	podporovat	k5eAaImAgMnP	podporovat
Stacy	Stacy	k1gInPc4	Stacy
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Danu	Dana	k1gFnSc4	Dana
Karrieren	Karrierna	k1gFnPc2	Karrierna
v	v	k7c6	v
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
působit	působit	k5eAaImF	působit
v	v	k7c6	v
showbusinessu	showbusiness	k1gInSc6	showbusiness
<g/>
.	.	kIx.	.
</s>
<s>
Stacy	Staca	k1gFnPc4	Staca
dostala	dostat	k5eAaPmAgFnS	dostat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hereckou	herecký	k2eAgFnSc4d1	herecká
příležitost	příležitost	k1gFnSc4	příležitost
jako	jako	k8xS	jako
moderátorka	moderátorka	k1gFnSc1	moderátorka
v	v	k7c6	v
zábavném	zábavný	k2eAgInSc6d1	zábavný
pořadu	pořad	k1gInSc6	pořad
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Charlie	Charlie	k1gMnSc1	Charlie
Brown	Brown	k1gMnSc1	Brown
and	and	k?	and
Snoopy	Snoop	k1gInPc1	Snoop
show	show	k1gFnSc1	show
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
dvou	dva	k4xCgNnPc2	dva
speciálních	speciální	k2eAgNnPc2d1	speciální
televizních	televizní	k2eAgNnPc2d1	televizní
vysílání	vysílání	k1gNnPc2	vysílání
-	-	kIx~	-
It	It	k1gFnSc1	It
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Flashbeagle	Flashbeagle	k1gNnSc7	Flashbeagle
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Brown	Brown	k1gMnSc1	Brown
and	and	k?	and
Snoop	Snoop	k1gInSc1	Snoop
show	show	k1gFnSc1	show
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInPc1d1	populární
moderátorské	moderátorský	k2eAgInPc1d1	moderátorský
pořady	pořad	k1gInPc1	pořad
a	a	k8xC	a
talk	talk	k1gInSc1	talk
show	show	k1gFnPc2	show
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
pro	pro	k7c4	pro
Rice	Rice	k1gFnSc4	Rice
Krispies	Krispiesa	k1gFnPc2	Krispiesa
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
Fergie	Fergie	k1gFnSc1	Fergie
v	v	k7c6	v
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
televizní	televizní	k2eAgFnSc6d1	televizní
show	show	k1gFnSc6	show
-	-	kIx~	-
Kinds	Kinds	k1gInSc1	Kinds
Incroporated	Incroporated	k1gInSc1	Incroporated
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
něco	něco	k3yInSc4	něco
jako	jako	k8xS	jako
dětská	dětský	k2eAgFnSc1d1	dětská
společnost	společnost	k1gFnSc1	společnost
<g/>
...	...	k?	...
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
hrála	hrát	k5eAaImAgFnS	hrát
Stacy	Stacy	k1gInPc4	Stacy
v	v	k7c6	v
hororové	hororový	k2eAgFnSc6d1	hororová
komedii	komedie	k1gFnSc6	komedie
-	-	kIx~	-
Monster	monstrum	k1gNnPc2	monstrum
in	in	k?	in
the	the	k?	the
closet	closet	k1gInSc1	closet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působila	působit	k5eAaImAgFnS	působit
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
-	-	kIx~	-
Kids	Kids	k1gInSc1	Kids
Incorporated	Incorporated	k1gInSc1	Incorporated
<g/>
:	:	kIx,	:
Charbusters	Charbusters	k1gInSc1	Charbusters
a	a	k8xC	a
Kids	Kids	k1gInSc1	Kids
Incorporated	Incorporated	k1gInSc1	Incorporated
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
in	in	k?	in
the	the	k?	the
New	New	k1gMnSc1	New
Year	Year	k1gMnSc1	Year
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
opustila	opustit	k5eAaPmAgFnS	opustit
Stacy	Stacy	k1gInPc4	Stacy
světla	světlo	k1gNnSc2	světlo
reflektorů	reflektor	k1gInPc2	reflektor
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
života	život	k1gInSc2	život
a	a	k8xC	a
normálního	normální	k2eAgNnSc2d1	normální
dění	dění	k1gNnSc2	dění
teenagerů	teenager	k1gMnPc2	teenager
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
pořád	pořád	k6eAd1	pořád
její	její	k3xOp3gFnSc7	její
největší	veliký	k2eAgFnSc7d3	veliký
vášní	vášeň	k1gFnSc7	vášeň
a	a	k8xC	a
tak	tak	k6eAd1	tak
založila	založit	k5eAaPmAgFnS	založit
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Stefanie	Stefanie	k1gFnSc1	Stefanie
Ridel	Ridela	k1gFnPc2	Ridela
a	a	k8xC	a
s	s	k7c7	s
kolegyní	kolegyně	k1gFnSc7	kolegyně
Rance	ranec	k1gInSc2	ranec
Sandstorm	Sandstorm	k1gInSc1	Sandstorm
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
pořadu	pořad	k1gInSc2	pořad
-	-	kIx~	-
Kids	Kids	k1gInSc1	Kids
Incorporaded	Incorporaded	k1gInSc1	Incorporaded
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
Wild	Wilda	k1gFnPc2	Wilda
Orchid	Orchida	k1gFnPc2	Orchida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
měla	mít	k5eAaImAgFnS	mít
dokonce	dokonce	k9	dokonce
vlastní	vlastní	k2eAgInSc4d1	vlastní
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
programu	program	k1gInSc6	program
FOX	fox	k1gInSc1	fox
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
veškerý	veškerý	k3xTgInSc4	veškerý
čas	čas	k1gInSc4	čas
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
věnovala	věnovat	k5eAaImAgFnS	věnovat
jen	jen	k6eAd1	jen
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Wild	Wilda	k1gFnPc2	Wilda
Orchid	Orchida	k1gFnPc2	Orchida
a	a	k8xC	a
hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
následovalo	následovat	k5eAaImAgNnS	následovat
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
OXYGEN	oxygen	k1gInSc1	oxygen
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
připravila	připravit	k5eAaPmAgFnS	připravit
skupina	skupina	k1gFnSc1	skupina
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
ho	on	k3xPp3gNnSc4	on
nestačila	stačit	k5eNaBmAgFnS	stačit
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
totiž	totiž	k9	totiž
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
duševním	duševní	k2eAgFnPc3d1	duševní
krizím	krize	k1gFnPc3	krize
a	a	k8xC	a
depresím	deprese	k1gFnPc3	deprese
které	který	k3yIgNnSc1	který
u	u	k7c2	u
Fergie	Fergie	k1gFnSc2	Fergie
působily	působit	k5eAaImAgInP	působit
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
podstoupit	podstoupit	k5eAaPmF	podstoupit
terapii	terapie	k1gFnSc4	terapie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
profesionální	profesionální	k2eAgFnSc3d1	profesionální
pěvecké	pěvecký	k2eAgFnSc3d1	pěvecká
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Toužila	toužit	k5eAaImAgFnS	toužit
však	však	k9	však
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
a	a	k8xC	a
tak	tak	k6eAd1	tak
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
potkala	potkat	k5eAaPmAgFnS	potkat
will	will	k1gInSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
z	z	k7c2	z
The	The	k1gFnSc2	The
Black	Black	k1gInSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc1	Peas
<g/>
.	.	kIx.	.
</s>
<s>
Potkávali	potkávat	k5eAaImAgMnP	potkávat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
televizních	televizní	k2eAgFnPc6d1	televizní
show	show	k1gFnPc6	show
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přijala	přijmout	k5eAaPmAgFnS	přijmout
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
nabídku	nabídka	k1gFnSc4	nabídka
a	a	k8xC	a
pěvecky	pěvecky	k6eAd1	pěvecky
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
skupinu	skupina	k1gFnSc4	skupina
Trio	trio	k1gNnSc1	trio
<g/>
.	.	kIx.	.
</s>
<s>
Nahrála	nahrát	k5eAaPmAgFnS	nahrát
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
3	[number]	k4	3
songy	song	k1gInPc7	song
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peasa	k1gFnPc2	Peasa
začali	začít	k5eAaPmAgMnP	začít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lban	Lban	k1gMnSc1	Lban
Klann	Klann	k1gMnSc1	Klann
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
,	,	kIx,	,
apl	apl	k?	apl
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
<g/>
.	.	kIx.	.
<g/>
ap	ap	kA	ap
a	a	k8xC	a
Taboo	Taboo	k6eAd1	Taboo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
uveřejnili	uveřejnit	k5eAaPmAgMnP	uveřejnit
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
názvem	název	k1gInSc7	název
Behind	Behind	k1gInSc4	Behind
the	the	k?	the
Front	front	k1gInSc1	front
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
album	album	k1gNnSc1	album
Bridging	Bridging	k1gInSc1	Bridging
the	the	k?	the
Gap	Gap	k1gFnSc2	Gap
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgInS	být
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
a	a	k8xC	a
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
už	už	k9	už
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
hvězdy	hvězda	k1gFnPc4	hvězda
jako	jako	k8xS	jako
např.	např.	kA	např.
Macy	Mac	k2eAgInPc1d1	Mac
Gray	Gray	k1gInPc1	Gray
nebo	nebo	k8xC	nebo
Wyclef	Wyclef	k1gMnSc1	Wyclef
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
Elephunk	Elephunka	k1gFnPc2	Elephunka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
už	už	k9	už
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
i	i	k9	i
Stacy	Stac	k1gMnPc4	Stac
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
přijala	přijmout	k5eAaPmAgFnS	přijmout
své	svůj	k3xOyFgNnSc4	svůj
umělecké	umělecký	k2eAgNnSc4d1	umělecké
jméno	jméno	k1gNnSc4	jméno
Fergie	Fergie	k1gFnSc1	Fergie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k8xC	jako
členka	členka	k1gFnSc1	členka
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
album	album	k1gNnSc1	album
Monkey	Monkea	k1gFnSc2	Monkea
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
páté	pátý	k4xOgNnSc4	pátý
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
E.	E.	kA	E.
<g/>
N.	N.	kA	N.
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Eyed	Eyed	k1gInSc1	Eyed
Peas	Peasa	k1gFnPc2	Peasa
se	se	k3xPyFc4	se
odklonili	odklonit	k5eAaPmAgMnP	odklonit
od	od	k7c2	od
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
stylu	styl	k1gInSc2	styl
spíše	spíše	k9	spíše
k	k	k7c3	k
elektru	elektrum	k1gNnSc3	elektrum
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
skladbami	skladba	k1gFnPc7	skladba
jsou	být	k5eAaImIp3nP	být
Boom	boom	k1gInSc4	boom
Boom	boom	k1gInSc1	boom
Pow	Pow	k1gFnSc1	Pow
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Meet	Meet	k2eAgMnSc1d1	Meet
Me	Me	k1gMnSc1	Me
Halfway	Halfwaa	k1gFnSc2	Halfwaa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
válcuje	válcovat	k5eAaImIp3nS	válcovat
všechny	všechen	k3xTgFnPc4	všechen
hudební	hudební	k2eAgFnPc4d1	hudební
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
máme	mít	k5eAaImIp1nP	mít
jejich	jejich	k3xOp3gNnSc4	jejich
zatím	zatím	k6eAd1	zatím
nejnovější	nový	k2eAgNnSc4d3	nejnovější
album	album	k1gNnSc4	album
The	The	k1gMnSc2	The
Beginning	Beginning	k1gInSc4	Beginning
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
30.11	[number]	k4	30.11
<g/>
.2010	.2010	k4	.2010
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
The	The	k1gFnSc2	The
Time	Tim	k1gFnSc2	Tim
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
remakem	remak	k1gMnSc7	remak
titulní	titulní	k2eAgFnSc2d1	titulní
písně	píseň	k1gFnSc2	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
Hříšný	hříšný	k2eAgInSc1d1	hříšný
Tanec	tanec	k1gInSc1	tanec
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
snad	snad	k9	snad
projela	projet	k5eAaPmAgFnS	projet
všemi	všecek	k3xTgInPc7	všecek
moderními	moderní	k2eAgInPc7d1	moderní
rádii	rádius	k1gInPc7	rádius
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
!	!	kIx.	!
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
ale	ale	k9	ale
nezanechalo	zanechat	k5eNaPmAgNnS	zanechat
ve	v	k7c6	v
fanoušcích	fanoušek	k1gMnPc6	fanoušek
The	The	k1gMnPc1	The
Black	Blacko	k1gNnPc2	Blacko
Eyed	Eyeda	k1gFnPc2	Eyeda
Peas	Peasa	k1gFnPc2	Peasa
moc	moc	k6eAd1	moc
velké	velký	k2eAgNnSc4d1	velké
nadšení	nadšení	k1gNnSc4	nadšení
jelikož	jelikož	k8xS	jelikož
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
will	will	k1gInSc4	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
amovo	amovo	k6eAd1	amovo
sólo	sólo	k1gNnSc1	sólo
než	než	k8xS	než
o	o	k7c4	o
skupinovou	skupinový	k2eAgFnSc4d1	skupinová
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
nechat	nechat	k5eAaPmF	nechat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
chtěli	chtít	k5eAaImAgMnP	chtít
pro	pro	k7c4	pro
novější	nový	k2eAgFnSc4d2	novější
generaci	generace	k1gFnSc4	generace
zrenovovat	zrenovovat	k5eAaPmF	zrenovovat
staré	starý	k2eAgInPc4d1	starý
hity	hit	k1gInPc4	hit
do	do	k7c2	do
novějšího	nový	k2eAgInSc2d2	novější
kabátu	kabát	k1gInSc2	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
nepotkáte	potkat	k5eNaPmIp2nP	potkat
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
pouštěl	pouštět	k5eAaImAgMnS	pouštět
klasickou	klasický	k2eAgFnSc4d1	klasická
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
Hříšného	hříšný	k2eAgInSc2d1	hříšný
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
musíme	muset	k5eAaImIp1nP	muset
ovšem	ovšem	k9	ovšem
The	The	k1gMnPc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
pochválit	pochválit	k5eAaPmF	pochválit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velkým	velký	k2eAgInPc3d1	velký
úspěchům	úspěch	k1gInPc3	úspěch
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
Fergie	Fergie	k1gFnSc1	Fergie
prorazit	prorazit	k5eAaPmF	prorazit
i	i	k9	i
v	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
will	willa	k1gFnPc2	willa
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
amem	amem	k6eAd1	amem
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
50	[number]	k4	50
<g/>
x	x	k?	x
a	a	k8xC	a
stále	stále	k6eAd1	stále
poprvé	poprvé	k6eAd1	poprvé
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnPc2	The
Dutchess	Dutchessa	k1gFnPc2	Dutchessa
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
písně	píseň	k1gFnSc2	píseň
London	London	k1gMnSc1	London
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
,	,	kIx,	,
Fergalicious	Fergalicious	k1gMnSc1	Fergalicious
<g/>
,	,	kIx,	,
Glamorous	Glamorous	k1gMnSc1	Glamorous
nebo	nebo	k8xC	nebo
Big	Big	k1gMnSc1	Big
Girls	girl	k1gFnPc2	girl
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Cry	Cry	k1gFnSc1	Cry
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
singly	singl	k1gInPc1	singl
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
5	[number]	k4	5
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
žádné	žádný	k3yNgFnSc3	žádný
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
singly	singl	k1gInPc1	singl
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k9	až
na	na	k7c4	na
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
za	za	k7c4	za
song	song	k1gInSc4	song
Big	Big	k1gFnSc2	Big
Girls	girl	k1gFnPc2	girl
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Cry	Cry	k1gFnSc1	Cry
obdržela	obdržet	k5eAaPmAgFnS	obdržet
i	i	k9	i
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
Rockovým	rockový	k2eAgMnPc3d1	rockový
fanouškům	fanoušek	k1gMnPc3	fanoušek
bude	být	k5eAaImBp3nS	být
jistě	jistě	k9	jistě
známá	známý	k2eAgFnSc1d1	známá
její	její	k3xOp3gFnSc1	její
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
kytaristou	kytarista	k1gMnSc7	kytarista
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
a	a	k8xC	a
nynějším	nynější	k2eAgMnSc7d1	nynější
lídrem	lídr	k1gMnSc7	lídr
Velvet	Velveta	k1gFnPc2	Velveta
Revolver	revolver	k1gInSc1	revolver
Slashem	Slash	k1gInSc7	Slash
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgInSc3	ten
zazpívala	zazpívat	k5eAaPmAgNnP	zazpívat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
koncertu	koncert	k1gInSc6	koncert
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
a	a	k8xC	a
hostovala	hostovat	k5eAaImAgFnS	hostovat
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
sólové	sólový	k2eAgFnSc6d1	sólová
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zpěv	zpěv	k1gInSc1	zpěv
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Beatiful	Beatiful	k1gInSc1	Beatiful
Dangerous	Dangerous	k1gInSc1	Dangerous
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
remix	remix	k1gInSc1	remix
rockové	rockový	k2eAgFnSc2d1	rocková
klasiky	klasika	k1gFnSc2	klasika
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
nahráli	nahrát	k5eAaPmAgMnP	nahrát
od	od	k7c2	od
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
několika	několik	k4yIc6	několik
společných	společný	k2eAgNnPc6d1	společné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
si	se	k3xPyFc3	se
Fergie	Fergie	k1gFnSc1	Fergie
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
také	také	k9	také
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
největší	veliký	k2eAgInSc4d3	veliký
hit	hit	k1gInSc4	hit
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
Sweet	Sweet	k1gMnSc1	Sweet
Child	Child	k1gMnSc1	Child
O	O	kA	O
<g/>
'	'	kIx"	'
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
song	song	k1gInSc1	song
"	"	kIx"	"
<g/>
A	a	k8xC	a
Little	Little	k1gFnSc1	Little
Party	parta	k1gFnSc2	parta
Never	Nevra	k1gFnPc2	Nevra
Killed	Killed	k1gMnSc1	Killed
Nobody	Noboda	k1gFnSc2	Noboda
(	(	kIx(	(
<g/>
All	All	k1gMnSc1	All
We	We	k1gMnSc1	We
Got	Got	k1gMnSc1	Got
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
písni	píseň	k1gFnSc6	píseň
hostuje	hostovat	k5eAaImIp3nS	hostovat
"	"	kIx"	"
<g/>
Q-Tip	Q-Tip	k1gInSc1	Q-Tip
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
GoonRock	GoonRock	k1gInSc1	GoonRock
<g/>
"	"	kIx"	"
.	.	kIx.	.
<g/>
A	a	k8xC	a
tento	tento	k3xDgInSc1	tento
song	song	k1gInSc1	song
je	být	k5eAaImIp3nS	být
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Velký	velký	k2eAgInSc4d1	velký
Gatsby	Gatsb	k1gInPc4	Gatsb
<g/>
.	.	kIx.	.
</s>
<s>
Nine	Nine	k1gFnSc1	Nine
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Planet	planeta	k1gFnPc2	planeta
Terror	Terror	k1gInSc1	Terror
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
...	...	k?	...
Tammy	Tamma	k1gFnSc2	Tamma
Visan	Visan	k1gMnSc1	Visan
Poseidon	Poseidon	k1gMnSc1	Poseidon
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
...	...	k?	...
Gloria	Gloria	k1gFnSc1	Gloria
Be	Be	k1gMnSc1	Be
Cool	Cool	k1gMnSc1	Cool
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
...	...	k?	...
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc1	Peas
"	"	kIx"	"
<g/>
Great	Great	k2eAgInSc1d1	Great
Pretenders	Pretenders	k1gInSc1	Pretenders
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
TV	TV	kA	TV
Série	série	k1gFnSc1	série
...	...	k?	...
Host	host	k1gMnSc1	host
Outside	Outsid	k1gInSc5	Outsid
Ozona	Ozono	k1gNnPc1	Ozono
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
...	...	k?	...
Girl	girl	k1gFnSc1	girl
Monster	monstrum	k1gNnPc2	monstrum
in	in	k?	in
the	the	k?	the
Closet	Closet	k1gInSc1	Closet
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
...	...	k?	...
Lucy	Lucy	k1gInPc1	Lucy
Chartbusters	Chartbustersa	k1gFnPc2	Chartbustersa
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
...	...	k?	...
Stacy	Staca	k1gFnSc2	Staca
Kids	Kids	k1gInSc1	Kids
Incorporated	Incorporated	k1gInSc1	Incorporated
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
In	In	k1gFnSc2	In
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
New	New	k1gMnSc1	New
Year	Year	k1gMnSc1	Year
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
...	...	k?	...
Stacy	Stac	k2eAgFnPc1d1	Stac
Snoopy	Snoopa	k1gFnPc1	Snoopa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Getting	Getting	k1gInSc1	Getting
Married	Married	k1gInSc1	Married
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
...	...	k?	...
Sally	Sall	k1gInPc1	Sall
Brown	Brown	k1gNnSc1	Brown
"	"	kIx"	"
<g/>
Kids	Kids	k1gInSc1	Kids
Incorporated	Incorporated	k1gInSc1	Incorporated
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
Série	série	k1gFnSc1	série
...	...	k?	...
Stacy	Stacy	k1gInPc1	Stacy
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
...	...	k?	...
aka	aka	k?	aka
Kids	Kids	k1gInSc1	Kids
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
:	:	kIx,	:
short	short	k1gInSc1	short
title	titla	k1gFnSc3	titla
<g/>
)	)	kIx)	)
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flashbeagle	Flashbeagle	k1gInSc1	Flashbeagle
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
...	...	k?	...
Sally	Salla	k1gMnSc2	Salla
Brown	Brown	k1gMnSc1	Brown
Be	Be	k1gMnSc2	Be
Somebody	Someboda	k1gFnSc2	Someboda
or	or	k?	or
Be	Be	k1gFnSc2	Be
Somebody	Someboda	k1gFnSc2	Someboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fool	Foola	k1gFnPc2	Foola
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
V	v	k7c6	v
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Dutchess	Dutchessa	k1gFnPc2	Dutchessa
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
London	London	k1gMnSc1	London
Bridge	Bridg	k1gMnSc2	Bridg
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Fergalicious	Fergalicious	k1gInSc1	Fergalicious
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Glamorous	Glamorous	k1gInSc1	Glamorous
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Big	Big	k1gFnSc6	Big
Girls	girl	k1gFnPc2	girl
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Cry	Cry	k1gFnSc1	Cry
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Clumsy	Clumsa	k1gFnSc2	Clumsa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Finally	Finalla	k1gFnSc2	Finalla
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
A	a	k8xC	a
Little	Little	k1gFnSc1	Little
Party	parta	k1gFnSc2	parta
Never	Nevra	k1gFnPc2	Nevra
Killed	Killed	k1gMnSc1	Killed
Nobody	Noboda	k1gFnSc2	Noboda
(	(	kIx(	(
<g/>
All	All	k1gMnSc1	All
We	We	k1gMnSc1	We
Got	Got	k1gMnSc1	Got
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
L.A	L.A	k1gFnSc1	L.A
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
M.	M.	kA	M.
<g/>
I.L.	I.L.	k1gFnPc2	I.L.
<g/>
F.	F.	kA	F.
$	$	kIx~	$
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
