<p>
<s>
Nosetín	Nosetín	k1gInSc1	Nosetín
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Chyšky	chyška	k1gFnSc2	chyška
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
Chyšek	chyška	k1gFnPc2	chyška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
24	[number]	k4	24
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
47	[number]	k4	47
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
121	[number]	k4	121
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
123	[number]	k4	123
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nosetín	Nosetín	k1gInSc1	Nosetín
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,73	[number]	k4	3,73
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Nosetín	Nosetína	k1gFnPc2	Nosetína
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
Branišov	Branišov	k1gInSc1	Branišov
<g/>
,	,	kIx,	,
Růžená	růžený	k2eAgFnSc1d1	Růžená
a	a	k8xC	a
Vilín	Vilín	k1gInSc1	Vilín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ves	ves	k1gFnSc1	ves
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1290	[number]	k4	1290
majetkem	majetek	k1gInSc7	majetek
hradu	hrad	k1gInSc2	hrad
Skalice	Skalice	k1gFnSc2	Skalice
u	u	k7c2	u
Sepekova	Sepekov	k1gInSc2	Sepekov
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
patřila	patřit	k5eAaImAgFnS	patřit
pražskému	pražský	k2eAgNnSc3d1	Pražské
biskupství	biskupství	k1gNnSc3	biskupství
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
majetkem	majetek	k1gInSc7	majetek
nadějkovského	nadějkovský	k2eAgNnSc2d1	nadějkovský
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Návesní	návesní	k2eAgFnSc1d1	návesní
kaple	kaple	k1gFnSc1	kaple
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
kříž	kříž	k1gInSc1	kříž
naproti	naproti	k7c3	naproti
návesní	návesní	k2eAgFnSc3d1	návesní
kapli	kaple	k1gFnSc3	kaple
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
u	u	k7c2	u
komunikace	komunikace	k1gFnSc2	komunikace
vedoucí	vedoucí	k1gFnSc2	vedoucí
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venkovská	venkovský	k2eAgFnSc1d1	venkovská
usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
10	[number]	k4	10
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
vesnice	vesnice	k1gFnSc2	vesnice
u	u	k7c2	u
komunikace	komunikace	k1gFnSc2	komunikace
je	být	k5eAaImIp3nS	být
vedená	vedený	k2eAgFnSc1d1	vedená
v	v	k7c6	v
Seznamu	seznam	k1gInSc6	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nosetín	Nosetína	k1gFnPc2	Nosetína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Nosetín	Nosetína	k1gFnPc2	Nosetína
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
