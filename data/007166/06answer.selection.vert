<s>
Kokosový	kokosový	k2eAgInSc1d1	kokosový
ořech	ořech	k1gInSc1	ořech
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
dřevnaté	dřevnatý	k2eAgFnSc2d1	dřevnatá
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
odlupujícími	odlupující	k2eAgInPc7d1	odlupující
se	s	k7c7	s
svazky	svazek	k1gInPc7	svazek
lýka	lýko	k1gNnSc2	lýko
<g/>
.	.	kIx.	.
</s>
