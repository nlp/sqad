<s>
Naopak	naopak	k6eAd1	naopak
nejdéle	dlouho	k6eAd3	dlouho
sloužil	sloužit	k5eAaImAgInS	sloužit
Franklin	Franklin	k1gInSc1	Franklin
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
po	po	k7c4	po
čtyři	čtyři	k4xCgNnPc4	čtyři
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
a	a	k8xC	a
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
strávil	strávit	k5eAaPmAgMnS	strávit
dvanáct	dvanáct	k4xCc1	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>

