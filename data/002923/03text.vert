<s>
Prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
President	president	k1gMnSc1	president
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
šéf	šéf	k1gMnSc1	šéf
exekutivy	exekutiva	k1gFnSc2	exekutiva
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
úřad	úřad	k1gInSc1	úřad
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
politickou	politický	k2eAgFnSc7d1	politická
funkcí	funkce	k1gFnSc7	funkce
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
amerických	americký	k2eAgFnPc2d1	americká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
nepřímo	přímo	k6eNd1	přímo
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
sborem	sborem	k6eAd1	sborem
volitelů	volitel	k1gMnPc2	volitel
(	(	kIx(	(
<g/>
Electoral	Electoral	k1gMnSc2	Electoral
College	Colleg	k1gMnSc2	Colleg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ratifikován	ratifikovat	k5eAaBmNgInS	ratifikovat
22	[number]	k4	22
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nikdo	nikdo	k3yNnSc1	nikdo
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
složily	složit	k5eAaPmAgFnP	složit
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
přísahu	přísaha	k1gFnSc4	přísaha
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
americké	americký	k2eAgFnSc2d1	americká
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
-	-	kIx~	-
tedy	tedy	k8xC	tedy
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
prezidenty	prezident	k1gMnPc7	prezident
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
(	(	kIx(	(
<g/>
Presidents	Presidents	k1gInSc1	Presidents
of	of	k?	of
the	the	k?	the
Continental	Continental	k1gMnSc1	Continental
Congress	Congress	k1gInSc1	Congress
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
rovněž	rovněž	k9	rovněž
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
osoby	osoba	k1gFnPc4	osoba
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
Acting	Acting	k1gInSc1	Acting
President	president	k1gMnSc1	president
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
44	[number]	k4	44
prezidentů	prezident	k1gMnPc2	prezident
čtyři	čtyři	k4xCgInPc4	čtyři
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
byli	být	k5eAaImAgMnP	být
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
George	George	k1gNnSc4	George
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
inaugurován	inaugurovat	k5eAaBmNgMnS	inaugurovat
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgInS	být
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolit	k5eAaPmNgInS	zvolit
kongresovým	kongresový	k2eAgInSc7d1	kongresový
volebním	volební	k2eAgInSc7d1	volební
výborem	výbor	k1gInSc7	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnSc4d3	nejkratší
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
úřadu	úřad	k1gInSc6	úřad
-	-	kIx~	-
31	[number]	k4	31
dnů	den	k1gInPc2	den
-	-	kIx~	-
strávil	strávit	k5eAaPmAgInS	strávit
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejdéle	dlouho	k6eAd3	dlouho
sloužil	sloužit	k5eAaImAgInS	sloužit
Franklin	Franklin	k1gInSc1	Franklin
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
po	po	k7c4	po
čtyři	čtyři	k4xCgNnPc4	čtyři
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
a	a	k8xC	a
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
strávil	strávit	k5eAaPmAgMnS	strávit
dvanáct	dvanáct	k4xCc1	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
mandát	mandát	k1gInSc1	mandát
vyprší	vypršet	k5eAaPmIp3nS	vypršet
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Grover	Grover	k1gInSc1	Grover
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
dvakrát	dvakrát	k6eAd1	dvakrát
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
nesouvisejících	související	k2eNgNnPc6d1	nesouvisející
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
jako	jako	k9	jako
22	[number]	k4	22
<g/>
.	.	kIx.	.
i	i	k9	i
24	[number]	k4	24
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
44	[number]	k4	44
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
43	[number]	k4	43
<g/>
.	.	kIx.	.
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zastává	zastávat	k5eAaImIp3nS	zastávat
tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
