<p>
<s>
Jean-François	Jean-François	k1gInSc1	Jean-François
Millet	Millet	k1gInSc1	Millet
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1814	[number]	k4	1814
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Camillem	Camill	k1gMnSc7	Camill
Corotem	Corot	k1gMnSc7	Corot
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
představitel	představitel	k1gMnSc1	představitel
barbizonské	barbizonský	k2eAgFnSc2d1	barbizonský
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Millet	Millet	k1gInSc1	Millet
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
zachycování	zachycování	k1gNnSc4	zachycování
života	život	k1gInSc2	život
rolníků	rolník	k1gMnPc2	rolník
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
realističností	realističnost	k1gFnSc7	realističnost
až	až	k8xS	až
naturalismem	naturalismus	k1gInSc7	naturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
rolníků	rolník	k1gMnPc2	rolník
v	v	k7c6	v
Milletově	Milletův	k2eAgNnSc6d1	Milletův
podání	podání	k1gNnSc6	podání
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
radosti	radost	k1gFnSc2	radost
–	–	k?	–
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
obrazy	obraz	k1gInPc1	obraz
jako	jako	k8xC	jako
Sběračky	sběračka	k1gFnPc1	sběračka
klasů	klas	k1gInPc2	klas
a	a	k8xC	a
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgFnSc2d1	páně
byly	být	k5eAaImAgFnP	být
doceněny	docenit	k5eAaPmNgFnP	docenit
až	až	k9	až
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Millet	Millet	k1gInSc1	Millet
byl	být	k5eAaImAgInS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dítě	dítě	k1gNnSc4	dítě
chudého	chudý	k2eAgInSc2d1	chudý
rolnického	rolnický	k2eAgInSc2d1	rolnický
páru	pár	k1gInSc2	pár
Jeana-Louise-Nicolase	Jeana-Louise-Nicolasa	k1gFnSc3	Jeana-Louise-Nicolasa
a	a	k8xC	a
Aimée-Henriette-Adélaï	Aimée-Henriette-Adélaï	k1gFnSc1	Aimée-Henriette-Adélaï
Henry	henry	k1gInSc2	henry
Milletových	Milletový	k2eAgMnPc2d1	Milletový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
normanské	normanský	k2eAgFnSc6d1	normanská
vesnici	vesnice	k1gFnSc6	vesnice
Gruchy	Grucha	k1gFnSc2	Grucha
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dvou	dva	k4xCgInPc2	dva
místních	místní	k2eAgMnPc2d1	místní
kněží	kněz	k1gMnPc2	kněz
získal	získat	k5eAaPmAgInS	získat
základy	základ	k1gInPc4	základ
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
moderními	moderní	k2eAgMnPc7d1	moderní
mistry	mistr	k1gMnPc7	mistr
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
do	do	k7c2	do
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
k	k	k7c3	k
portrétistovi	portrétista	k1gMnSc3	portrétista
Paulu	Paul	k1gMnSc3	Paul
Dumouchelovi	Dumouchel	k1gMnSc3	Dumouchel
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
u	u	k7c2	u
Luciena-Théophila	Luciena-Théophil	k1gMnSc2	Luciena-Théophil
Langloise	Langloise	k1gFnSc2	Langloise
<g/>
,	,	kIx,	,
žáka	žák	k1gMnSc2	žák
barona	baron	k1gMnSc2	baron
Grose	gros	k1gInSc5	gros
<g/>
.	.	kIx.	.
</s>
<s>
Stipendium	stipendium	k1gNnSc1	stipendium
<g/>
,	,	kIx,	,
poskytnuté	poskytnutý	k2eAgFnPc1d1	poskytnutá
Langloisem	Langlois	k1gInSc7	Langlois
i	i	k8xC	i
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Milletovi	Milletův	k2eAgMnPc1d1	Milletův
jet	jet	k5eAaImNgMnS	jet
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c6	na
École	Écola	k1gFnSc6	Écola
des	des	k1gNnSc2	des
Beaux-Arts	Beaux-Artsa	k1gFnPc2	Beaux-Artsa
u	u	k7c2	u
vyučujícího	vyučující	k2eAgMnSc2d1	vyučující
Paula	Paul	k1gMnSc2	Paul
Delaroche	Delaroch	k1gMnSc2	Delaroch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
jeho	jeho	k3xOp3gNnSc1	jeho
vyučování	vyučování	k1gNnSc1	vyučování
skončilo	skončit	k5eAaPmAgNnS	skončit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
objednávka	objednávka	k1gFnSc1	objednávka
z	z	k7c2	z
pařížského	pařížský	k2eAgInSc2d1	pařížský
Salonu	salon	k1gInSc2	salon
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paříž	Paříž	k1gFnSc1	Paříž
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
Salonem	salon	k1gInSc7	salon
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
jeden	jeden	k4xCgInSc1	jeden
jeho	jeho	k3xOp3gInSc6	jeho
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc4	portrét
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
Millet	Millet	k1gInSc1	Millet
do	do	k7c2	do
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
započal	započnout	k5eAaPmAgInS	započnout
novou	nový	k2eAgFnSc4d1	nová
kariéru	kariéra	k1gFnSc4	kariéra
portrétisty	portrétista	k1gMnSc2	portrétista
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Pauline-Virginie	Pauline-Virginie	k1gFnPc1	Pauline-Virginie
Ono	onen	k3xDgNnSc1	onen
a	a	k8xC	a
pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
vykázán	vykázat	k5eAaPmNgMnS	vykázat
ze	z	k7c2	z
Salonu	salon	k1gInSc2	salon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
zemřela	zemřít	k5eAaPmAgFnS	zemřít
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
opětovnému	opětovný	k2eAgInSc3d1	opětovný
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
cestoval	cestovat	k5eAaImAgMnS	cestovat
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
do	do	k7c2	do
Le	Le	k1gFnSc2	Le
Havru	Havr	k1gInSc2	Havr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
ženou	žena	k1gFnSc7	žena
Catherine	Catherin	k1gInSc5	Catherin
Lemaireovou	Lemaireův	k2eAgFnSc7d1	Lemaireův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
civilní	civilní	k2eAgInSc1d1	civilní
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
devět	devět	k4xCc1	devět
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
Milletovy	Milletův	k2eAgFnSc2d1	Milletův
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Le	Le	k1gFnSc6	Le
Havru	Havr	k1gInSc2	Havr
se	se	k3xPyFc4	se
Millet	Millet	k1gInSc1	Millet
živil	živit	k5eAaImAgInS	živit
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
malováním	malování	k1gNnSc7	malování
portrétů	portrét	k1gInPc2	portrét
a	a	k8xC	a
menších	malý	k2eAgNnPc2d2	menší
žánrových	žánrový	k2eAgNnPc2d1	žánrové
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
Constantem	Constant	k1gMnSc7	Constant
Troyonem	Troyon	k1gMnSc7	Troyon
<g/>
,	,	kIx,	,
Narcissem	Narciss	k1gMnSc7	Narciss
Diazem	Diaz	k1gMnSc7	Diaz
<g/>
,	,	kIx,	,
Charlesem	Charles	k1gMnSc7	Charles
Jacquesem	Jacques	k1gMnSc7	Jacques
<g/>
,	,	kIx,	,
Théodorem	Théodor	k1gMnSc7	Théodor
Rousseauem	Rousseau	k1gMnSc7	Rousseau
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
přátelství	přátelství	k1gNnSc4	přátelství
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
důležité	důležitý	k2eAgNnSc1d1	důležité
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
barbizonské	barbizonský	k2eAgFnSc2d1	barbizonský
školy	škola	k1gFnSc2	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Honorém	Honorý	k2eAgInSc6d1	Honorý
Daumierem	Daumier	k1gMnSc7	Daumier
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
figurální	figurální	k2eAgFnSc2d1	figurální
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
Alfredem	Alfred	k1gMnSc7	Alfred
Sensierem	Sensier	k1gMnSc7	Sensier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
Milletovým	Milletový	k2eAgMnSc7d1	Milletový
mecenášem	mecenáš	k1gMnSc7	mecenáš
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
životopisec	životopisec	k1gMnSc1	životopisec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barbizon	Barbizon	k1gInSc4	Barbizon
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
maloval	malovat	k5eAaImAgInS	malovat
Millet	Millet	k1gInSc1	Millet
státní	státní	k2eAgFnSc4d1	státní
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
Ženci	ženc	k1gMnSc3	ženc
<g/>
,	,	kIx,	,
na	na	k7c6	na
Salonu	salon	k1gInSc6	salon
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
Pastýřku	pastýřka	k1gFnSc4	pastýřka
sedící	sedící	k2eAgFnSc4d1	sedící
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
malá	malá	k1gFnSc1	malá
olejomalba	olejomalba	k1gFnSc1	olejomalba
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
odklon	odklon	k1gInSc1	odklon
umělce	umělec	k1gMnSc2	umělec
od	od	k7c2	od
idealizované	idealizovaný	k2eAgFnSc2d1	idealizovaná
skutečnosti	skutečnost	k1gFnSc2	skutečnost
k	k	k7c3	k
realističtějšímu	realistický	k2eAgNnSc3d2	realističtější
a	a	k8xC	a
osobitějšímu	osobitý	k2eAgNnSc3d2	osobitější
podání	podání	k1gNnSc3	podání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Barbizonu	Barbizon	k1gInSc2	Barbizon
před	před	k7c7	před
epidemií	epidemie	k1gFnSc7	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
strávit	strávit	k5eAaPmF	strávit
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
přítele	přítel	k1gMnSc4	přítel
Rousseaua	Rousseau	k1gMnSc4	Rousseau
zalíbilo	zalíbit	k5eAaPmAgNnS	zalíbit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
zde	zde	k6eAd1	zde
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
a	a	k8xC	a
zde	zde	k6eAd1	zde
také	také	k9	také
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
existenčními	existenční	k2eAgInPc7d1	existenční
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
dvě	dva	k4xCgFnPc4	dva
svoje	svůj	k3xOyFgNnPc4	svůj
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
každodenní	každodenní	k2eAgFnSc2d1	každodenní
barbizonské	barbizonský	k2eAgFnSc2d1	barbizonský
reality	realita	k1gFnSc2	realita
<g/>
:	:	kIx,	:
Sběračky	sběračka	k1gFnPc4	sběračka
klasů	klas	k1gInPc2	klas
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
zachycující	zachycující	k2eAgFnPc1d1	zachycující
tři	tři	k4xCgFnPc1	tři
chudé	chudý	k2eAgFnPc1d1	chudá
ženy	žena	k1gFnPc1	žena
paběrkující	paběrkující	k2eAgInPc4d1	paběrkující
zbylé	zbylý	k2eAgInPc4d1	zbylý
obilné	obilný	k2eAgInPc4d1	obilný
klasy	klas	k1gInPc4	klas
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
působivým	působivý	k2eAgNnSc7d1	působivé
teplým	teplý	k2eAgNnSc7d1	teplé
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
na	na	k7c6	na
výročním	výroční	k2eAgInSc6d1	výroční
Salonu	salon	k1gInSc6	salon
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
dokončil	dokončit	k5eAaPmAgMnS	dokončit
i	i	k8xC	i
Anděla	Anděla	k1gFnSc1	Anděla
Páně	páně	k2eAgFnSc1d1	páně
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
také	také	k6eAd1	také
známi	znám	k2eAgMnPc1d1	znám
jako	jako	k8xC	jako
Klekání	klekání	k1gNnPc1	klekání
nebo	nebo	k8xC	nebo
Angelus	Angelus	k1gInSc1	Angelus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
ženu	žena	k1gFnSc4	žena
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
modlící	modlící	k2eAgInSc4d1	modlící
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
při	při	k7c6	při
západu	západ	k1gInSc6	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Milletovi	Milletův	k2eAgMnPc1d1	Milletův
dostával	dostávat	k5eAaImAgMnS	dostávat
smíšených	smíšený	k2eAgFnPc2d1	smíšená
kritických	kritický	k2eAgFnPc2d1	kritická
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c6	na
Salonech	salon	k1gInPc6	salon
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jeho	jeho	k3xOp3gMnPc7	jeho
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
reputace	reputace	k1gFnSc1	reputace
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
desetiletí	desetiletí	k1gNnSc2	desetiletí
dostal	dostat	k5eAaPmAgInS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
malbu	malba	k1gFnSc4	malba
dvaceti	dvacet	k4xCc2	dvacet
pěti	pět	k4xCc2	pět
prací	práce	k1gFnPc2	práce
za	za	k7c4	za
měsíční	měsíční	k2eAgInSc4d1	měsíční
poplatek	poplatek	k1gInSc4	poplatek
na	na	k7c4	na
následující	následující	k2eAgInPc4d1	následující
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
mecenášem	mecenáš	k1gMnSc7	mecenáš
Emilem	Emil	k1gMnSc7	Emil
Gavetem	Gavet	k1gMnSc7	Gavet
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
kolekci	kolekce	k1gFnSc4	kolekce
pastelů	pastel	k1gInPc2	pastel
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
nakonec	nakonec	k6eAd1	nakonec
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byly	být	k5eAaImAgFnP	být
vystavovány	vystavovat	k5eAaImNgInP	vystavovat
nejznámější	známý	k2eAgInPc1d3	nejznámější
Milletovy	Milletův	k2eAgInPc1d1	Milletův
obrazy	obraz	k1gInPc1	obraz
včetně	včetně	k7c2	včetně
Sběraček	sběračka	k1gFnPc2	sběračka
klasů	klas	k1gInPc2	klas
<g/>
,	,	kIx,	,
Anděla	Anděla	k1gFnSc1	Anděla
Páně	páně	k2eAgNnSc2d1	páně
a	a	k8xC	a
Sazečů	sazeč	k1gMnPc2	sazeč
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
objednal	objednat	k5eAaPmAgMnS	objednat
Frédéric	Frédéric	k1gMnSc1	Frédéric
Hartmann	Hartmann	k1gMnSc1	Hartmann
Čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
za	za	k7c4	za
25	[number]	k4	25
000	[number]	k4	000
franků	frank	k1gInPc2	frank
a	a	k8xC	a
Millet	Millet	k1gInSc1	Millet
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
rytířem	rytíř	k1gMnSc7	rytíř
Řádu	řád	k1gInSc2	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
byl	být	k5eAaImAgInS	být
Millet	Millet	k1gInSc1	Millet
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
poroty	porota	k1gFnSc2	porota
Salonu	salon	k1gInSc2	salon
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
musel	muset	k5eAaImAgMnS	muset
utéct	utéct	k5eAaPmF	utéct
před	před	k7c7	před
prusko-francouzskou	pruskorancouzský	k2eAgFnSc7d1	prusko-francouzská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Cherbourgu	Cherbourg	k1gInSc6	Cherbourg
a	a	k8xC	a
Gréville	Grévill	k1gInSc6	Grévill
<g/>
,	,	kIx,	,
do	do	k7c2	do
Barbizonu	Barbizon	k1gInSc2	Barbizon
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k9	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
umělcova	umělcův	k2eAgNnPc1d1	umělcovo
léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
konjunkturou	konjunktura	k1gFnSc7	konjunktura
a	a	k8xC	a
společenským	společenský	k2eAgNnSc7d1	společenské
uznáním	uznání	k1gNnSc7	uznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	s	k7c7	s
zhoršujícím	zhoršující	k2eAgMnSc7d1	zhoršující
se	se	k3xPyFc4	se
zdravím	zdraví	k1gNnSc7	zdraví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
nedovolilo	dovolit	k5eNaPmAgNnS	dovolit
plnit	plnit	k5eAaImF	plnit
státní	státní	k2eAgFnPc4d1	státní
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
církevní	církevní	k2eAgInSc1d1	církevní
sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
svoji	svůj	k3xOyFgFnSc4	svůj
ženou	žena	k1gFnSc7	žena
Catherinou	Catherin	k2eAgFnSc7d1	Catherina
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
toho	ten	k3xDgNnSc2	ten
měsíce	měsíc	k1gInPc4	měsíc
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Milletovo	Milletův	k2eAgNnSc1d1	Milletův
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
inspiračním	inspirační	k2eAgInSc7d1	inspirační
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
impresionisty	impresionista	k1gMnPc4	impresionista
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
Vincenta	Vincent	k1gMnSc2	Vincent
van	vana	k1gFnPc2	vana
Gogha	Gogh	k1gMnSc2	Gogh
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
rané	raný	k2eAgNnSc4d1	rané
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
často	často	k6eAd1	často
citoval	citovat	k5eAaBmAgMnS	citovat
Milletovo	Milletův	k2eAgNnSc4d1	Milletův
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
bratru	bratr	k1gMnSc3	bratr
Theovi	Thea	k1gMnSc3	Thea
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgFnPc1d1	pozdní
Milletovy	Milletův	k2eAgFnPc1d1	Milletův
krajiny	krajina	k1gFnPc1	krajina
sloužily	sloužit	k5eAaImAgFnP	sloužit
za	za	k7c4	za
inspiraci	inspirace	k1gFnSc4	inspirace
Claudu	Claud	k1gMnSc3	Claud
Monetovi	Monet	k1gMnSc3	Monet
při	při	k7c6	při
malbě	malba	k1gFnSc6	malba
normandských	normandský	k2eAgNnPc2d1	normandské
pobřeží	pobřeží	k1gNnPc2	pobřeží
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc4	jeho
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
symbolismus	symbolismus	k1gInSc4	symbolismus
díla	dílo	k1gNnSc2	dílo
využíval	využívat	k5eAaImAgMnS	využívat
Georges	Georges	k1gMnSc1	Georges
Seurat	Seurat	k1gMnSc1	Seurat
<g/>
.	.	kIx.	.
<g/>
Hra	hra	k1gFnSc1	hra
Marka	Marek	k1gMnSc2	Marek
Twaina	Twain	k1gMnSc2	Twain
Is	Is	k1gMnSc2	Is
He	he	k0	he
Dead	Dead	k1gInSc4	Dead
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
Milleta	Milleta	k1gFnSc1	Milleta
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
umělci	umělec	k1gMnSc6	umělec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bojuje	bojovat	k5eAaImIp3nS	bojovat
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předstírá	předstírat	k5eAaImIp3nS	předstírat
smrt	smrt	k1gFnSc4	smrt
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
vydal	vydat	k5eAaPmAgMnS	vydat
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
umělcovu	umělcův	k2eAgFnSc4d1	umělcova
monografii	monografie	k1gFnSc4	monografie
François-Millet	François-Millet	k1gInSc1	François-Millet
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jean	Jean	k1gMnSc1	Jean
François	François	k1gFnSc2	François
Millet	Millet	k1gInSc1	Millet
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgNnSc2d1	páně
byl	být	k5eAaImAgInS	být
necelých	celý	k2eNgInPc2d1	necelý
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Milleta	Milleta	k1gFnSc1	Milleta
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
za	za	k7c4	za
80	[number]	k4	80
000	[number]	k4	000
zlatých	zlatý	k2eAgInPc2d1	zlatý
franků	frank	k1gInPc2	frank
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
diskuzi	diskuze	k1gFnSc4	diskuze
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
malířovým	malířův	k2eAgMnPc3d1	malířův
živořícím	živořící	k2eAgMnPc3d1	živořící
pozůstalým	pozůstalý	k1gMnPc3	pozůstalý
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
práva	právo	k1gNnSc2	právo
droit	droita	k1gFnPc2	droita
de	de	k?	de
suite	suit	k1gMnSc5	suit
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
umělci	umělec	k1gMnSc3	umělec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
dědicům	dědic	k1gMnPc3	dědic
dostávat	dostávat	k5eAaImF	dostávat
podíly	podíl	k1gInPc4	podíl
z	z	k7c2	z
prodejů	prodej	k1gInPc2	prodej
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
obrazem	obraz	k1gInSc7	obraz
také	také	k9	také
intenzivně	intenzivně	k6eAd1	intenzivně
zabýval	zabývat	k5eAaImAgMnS	zabývat
surrealista	surrealista	k1gMnSc1	surrealista
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
analýzu	analýza	k1gFnSc4	analýza
obrazu	obraz	k1gInSc2	obraz
The	The	k1gMnSc1	The
Tragic	Tragic	k1gMnSc1	Tragic
Myth	Myth	k1gMnSc1	Myth
of	of	k?	of
The	The	k1gMnSc1	The
Angelus	Angelus	k1gMnSc1	Angelus
of	of	k?	of
Millet	Millet	k1gInSc1	Millet
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
Tragický	tragický	k2eAgInSc1d1	tragický
mýtus	mýtus	k1gInSc1	mýtus
Milletova	Milletův	k2eAgMnSc2d1	Milletův
Anděla	Anděl	k1gMnSc2	Anděl
Páně	páně	k2eAgFnSc2d1	páně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
duchovní	duchovní	k2eAgInSc1d1	duchovní
mír	mír	k1gInSc1	mír
potlačenou	potlačený	k2eAgFnSc4d1	potlačená
sexuální	sexuální	k2eAgFnSc4d1	sexuální
agresivitu	agresivita	k1gFnSc4	agresivita
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pár	pár	k1gInSc1	pár
spíše	spíše	k9	spíše
modlí	modlit	k5eAaImIp3nS	modlit
nad	nad	k7c7	nad
svým	svůj	k3xOyFgNnSc7	svůj
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
pohřbeným	pohřbený	k2eAgNnSc7d1	pohřbené
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
než	než	k8xS	než
zmiňovaného	zmiňovaný	k2eAgMnSc4d1	zmiňovaný
Anděla	Anděl	k1gMnSc4	Anděl
Páně	páně	k2eAgMnSc4d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
domněnek	domněnka	k1gFnPc2	domněnka
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
vyšetření	vyšetření	k1gNnSc1	vyšetření
plátna	plátno	k1gNnSc2	plátno
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
současná	současný	k2eAgFnSc1d1	současná
barva	barva	k1gFnSc1	barva
překryla	překrýt	k5eAaPmAgFnS	překrýt
geometrický	geometrický	k2eAgInSc4d1	geometrický
objekt	objekt	k1gInSc4	objekt
připomínající	připomínající	k2eAgFnSc1d1	připomínající
rakev	rakev	k1gFnSc1	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Millet	Millet	k1gInSc1	Millet
změnil	změnit	k5eAaPmAgInS	změnit
koncepci	koncepce	k1gFnSc4	koncepce
obrazu	obraz	k1gInSc2	obraz
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
tento	tento	k3xDgInSc1	tento
předmět	předmět	k1gInSc1	předmět
skutečně	skutečně	k6eAd1	skutečně
dokládá	dokládat	k5eAaImIp3nS	dokládat
Dalího	Dalí	k1gMnSc4	Dalí
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jean-François	Jean-François	k1gInSc1	Jean-François
Millet	Millet	k1gInSc1	Millet
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CHAMPA	CHAMPA	kA	CHAMPA
<g/>
,	,	kIx,	,
Kermit	Kermit	k1gInSc1	Kermit
S.	S.	kA	S.
The	The	k1gMnSc2	The
Rise	Ris	k1gMnSc2	Ris
of	of	k?	of
Landscape	Landscap	k1gInSc5	Landscap
Painting	Painting	k1gInSc4	Painting
in	in	k?	in
France	Franc	k1gMnSc2	Franc
:	:	kIx,	:
Corot	Corot	k1gMnSc1	Corot
to	ten	k3xDgNnSc4	ten
Monet	moneta	k1gFnPc2	moneta
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Harry	Harra	k1gFnSc2	Harra
N.	N.	kA	N.
Abrams	Abrams	k1gInSc1	Abrams
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8109	[number]	k4	8109
<g/>
-	-	kIx~	-
<g/>
3757	[number]	k4	3757
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MURPHY	MURPHY	kA	MURPHY
<g/>
,	,	kIx,	,
Alexandra	Alexandr	k1gMnSc2	Alexandr
R.	R.	kA	R.
Jean-François	Jean-François	k1gInSc1	Jean-François
Millet	Millet	k1gInSc1	Millet
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Artsa	k1gFnPc2	Artsa
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
87846	[number]	k4	87846
<g/>
-	-	kIx~	-
<g/>
237	[number]	k4	237
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
malíři	malíř	k1gMnPc1	malíř
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
inspirace	inspirace	k1gFnSc1	inspirace
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Č.	Č.	kA	Č.
25	[number]	k4	25
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Francois	Francois	k1gMnSc1	Francois
Millet	Millet	k1gInSc1	Millet
a	a	k8xC	a
barbizonská	barbizonský	k2eAgFnSc1d1	barbizonský
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eaglemoss	Eaglemoss	k1gInSc1	Eaglemoss
International	International	k1gFnSc2	International
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
30	[number]	k4	30
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ROLAND	ROLAND	kA	ROLAND
<g/>
,	,	kIx,	,
Romain	Romain	k1gMnSc1	Romain
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
François	François	k1gFnSc2	François
Millet	Millet	k1gInSc1	Millet
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
89	[number]	k4	89
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jean-François	Jean-François	k1gInSc1	Jean-François
Millet	Millet	k1gInSc1	Millet
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jean-François	Jean-François	k1gFnSc1	Jean-François
Millet	Millet	k1gInSc1	Millet
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jean-François	Jean-François	k1gFnSc2	Jean-François
Millet	Millet	k1gInSc1	Millet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
