<s>
U	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Běstvina	Běstvina	k1gFnSc1	Běstvina
roste	růst	k5eAaImIp3nS	růst
památný	památný	k2eAgInSc4d1	památný
platan	platan	k1gInSc4	platan
javorolistý	javorolistý	k2eAgInSc4d1	javorolistý
(	(	kIx(	(
<g/>
Platanus	Platanus	k1gInSc4	Platanus
hispanica	hispanic	k1gInSc2	hispanic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
cca	cca	kA	cca
40	[number]	k4	40
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
strom	strom	k1gInSc4	strom
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
kmene	kmen	k1gInSc2	kmen
cca	cca	kA	cca
550	[number]	k4	550
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
se	se	k3xPyFc4	se
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
2,5	[number]	k4	2,5
m	m	kA	m
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mimořádně	mimořádně	k6eAd1	mimořádně
vzrostlý	vzrostlý	k2eAgInSc4d1	vzrostlý
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
památková	památkový	k2eAgFnSc1d1	památková
ochrana	ochrana	k1gFnSc1	ochrana
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
