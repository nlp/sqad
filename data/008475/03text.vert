<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Revište	Revište	k1gFnSc2	Revište
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
ležící	ležící	k2eAgFnPc4d1	ležící
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Žarnovica	Žarnovicum	k1gNnSc2	Žarnovicum
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Revištské	Revištský	k2eAgFnSc2d1	Revištský
Podzámčie	Podzámčie	k1gFnSc2	Podzámčie
<g/>
)	)	kIx)	)
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žarnovica	Žarnovic	k1gInSc2	Žarnovic
v	v	k7c6	v
Banskobystrickém	banskobystrický	k2eAgInSc6d1	banskobystrický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zalesněného	zalesněný	k2eAgInSc2d1	zalesněný
srázu	sráz	k1gInSc2	sráz
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Hron	Hron	k1gMnSc1	Hron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Šášovským	Šášovský	k2eAgInSc7d1	Šášovský
hradem	hrad	k1gInSc7	hrad
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
Hronu	Hron	k1gInSc2	Hron
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
úzkého	úzký	k2eAgInSc2d1	úzký
průchodu	průchod	k1gInSc2	průchod
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
vedla	vést	k5eAaImAgFnS	vést
obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
středoslovenským	středoslovenský	k2eAgNnPc3d1	Středoslovenské
hornickým	hornický	k2eAgNnPc3d1	Hornické
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listině	listina	k1gFnSc6	listina
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1265	[number]	k4	1265
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1331	[number]	k4	1331
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
těžiště	těžiště	k1gNnSc4	těžiště
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
hrad	hrad	k1gInSc1	hrad
královský	královský	k2eAgInSc1d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Procházel	procházet	k5eAaImAgMnS	procházet
rukama	ruka	k1gFnPc7	ruka
několika	několik	k4yIc2	několik
majitelů	majitel	k1gMnPc2	majitel
(	(	kIx(	(
<g/>
jágerský	jágerský	k2eAgMnSc1d1	jágerský
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Jiskra	jiskra	k1gFnSc1	jiskra
z	z	k7c2	z
Brandýsa	Brandýs	k1gInSc2	Brandýs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
majitelem	majitel	k1gMnSc7	majitel
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1447	[number]	k4	1447
<g/>
;	;	kIx,	;
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ho	on	k3xPp3gMnSc4	on
manželka	manželka	k1gFnSc1	manželka
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvína	k1gFnSc1	Korvína
Beatrix	Beatrix	k1gInSc1	Beatrix
darovala	darovat	k5eAaPmAgFnS	darovat
Dóczyům	Dóczy	k1gMnPc3	Dóczy
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
postrachem	postrach	k1gInSc7	postrach
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
svou	svůj	k3xOyFgFnSc4	svůj
rozpínavost	rozpínavost	k1gFnSc4	rozpínavost
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
hornickým	hornický	k2eAgNnPc3d1	Hornické
městům	město	k1gNnPc3	město
(	(	kIx(	(
<g/>
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
k	k	k7c3	k
obyčejným	obyčejný	k2eAgInPc3d1	obyčejný
přepadům	přepad	k1gInPc3	přepad
a	a	k8xC	a
rabování	rabování	k1gNnSc3	rabování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
majetku	majetek	k1gInSc6	majetek
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
vymření	vymření	k1gNnSc2	vymření
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1647	[number]	k4	1647
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
vesnicemi	vesnice	k1gFnPc7	vesnice
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
královského	královský	k2eAgInSc2d1	královský
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
z	z	k7c2	z
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
rodiny	rodina	k1gFnSc2	rodina
Grueberů	Grueber	k1gMnPc2	Grueber
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
Imricha	Imrich	k1gMnSc2	Imrich
Thökölyho	Thököly	k1gMnSc2	Thököly
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgInS	být
opraven	opraven	k2eAgInSc1d1	opraven
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
využití	využití	k1gNnSc1	využití
hradu	hrad	k1gInSc2	hrad
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházely	nacházet	k5eAaImAgFnP	nacházet
ubytovny	ubytovna	k1gFnPc1	ubytovna
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
odchodu	odchod	k1gInSc6	odchod
hrad	hrad	k1gInSc1	hrad
pustl	pustnout	k5eAaImAgInS	pustnout
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
ruinou	ruina	k1gFnSc7	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
malebné	malebný	k2eAgFnPc1d1	malebná
zříceniny	zřícenina	k1gFnPc1	zřícenina
zasazené	zasazený	k2eAgFnPc1d1	zasazená
do	do	k7c2	do
hustého	hustý	k2eAgInSc2d1	hustý
lesního	lesní	k2eAgInSc2d1	lesní
porostu	porost	k1gInSc2	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Skalní	skalní	k2eAgNnSc1d1	skalní
bradlo	bradlo	k1gNnSc1	bradlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
hrad	hrad	k1gInSc4	hrad
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
,	,	kIx,	,
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jeho	on	k3xPp3gInSc4	on
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
opevněné	opevněný	k2eAgFnSc2d1	opevněná
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
nádvoří	nádvoří	k1gNnSc2	nádvoří
s	s	k7c7	s
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
z	z	k7c2	z
dolního	dolní	k2eAgMnSc2d1	dolní
do	do	k7c2	do
horního	horní	k2eAgNnSc2d1	horní
nádvoří	nádvoří	k1gNnSc2	nádvoří
chránila	chránit	k5eAaImAgFnS	chránit
kruhová	kruhový	k2eAgFnSc1d1	kruhová
bašta	bašta	k1gFnSc1	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
dostal	dostat	k5eAaPmAgInS	dostat
později	pozdě	k6eAd2	pozdě
renesanční	renesanční	k2eAgNnSc4d1	renesanční
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uzavíralo	uzavírat	k5eAaImAgNnS	uzavírat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
předhradí	předhradí	k1gNnSc2	předhradí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
ze	z	k7c2	z
staveb	stavba	k1gFnPc2	stavba
hradu	hrad	k1gInSc2	hrad
zachovala	zachovat	k5eAaPmAgFnS	zachovat
část	část	k1gFnSc1	část
obvodového	obvodový	k2eAgNnSc2d1	obvodové
zdiva	zdivo	k1gNnSc2	zdivo
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
destrukcemi	destrukce	k1gFnPc7	destrukce
a	a	k8xC	a
zbytky	zbytek	k1gInPc7	zbytek
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Revište	Revište	k1gFnSc2	Revište
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Štefan	Štefan	k1gMnSc1	Štefan
Pisoň	Pisoň	k1gMnSc1	Pisoň
<g/>
,	,	kIx,	,
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
zámečky	zámeček	k1gInPc1	zámeček
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Osveta	Osveta	k1gFnSc1	Osveta
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hradů	hrad	k1gInPc2	hrad
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Revište	Revište	k1gFnSc2	Revište
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Revište	Revišit	k5eAaImRp2nP	Revišit
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
ruina	ruina	k1gFnSc1	ruina
<g/>
)	)	kIx)	)
</s>
</p>
