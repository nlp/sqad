<s>
Hrad	hrad	k1gInSc1	hrad
Revište	Revište	k1gFnSc2	Revište
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
ležící	ležící	k2eAgFnPc4d1	ležící
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Žarnovica	Žarnovicum	k1gNnSc2	Žarnovicum
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Revištské	Revištský	k2eAgFnSc2d1	Revištský
Podzámčie	Podzámčie	k1gFnSc2	Podzámčie
<g/>
)	)	kIx)	)
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žarnovica	Žarnovic	k1gInSc2	Žarnovic
v	v	k7c6	v
Banskobystrickém	banskobystrický	k2eAgInSc6d1	banskobystrický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
