<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
kolotoče	kolotoč	k1gInPc1	kolotoč
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
otáčivé	otáčivý	k2eAgInPc4d1	otáčivý
sloupy	sloup	k1gInPc4	sloup
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
řetízcích	řetízek	k1gInPc6	řetízek
zavěšeny	zavěšen	k2eAgInPc1d1	zavěšen
koše	koš	k1gInPc1	koš
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
koníci	koník	k1gMnPc1	koník
a	a	k8xC	a
loďky	loďka	k1gFnPc1	loďka
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
