<s>
Kolotoč	kolotoč	k1gInSc1	kolotoč
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
karusel	karusel	k1gInSc1	karusel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zábavní	zábavní	k2eAgFnSc1d1	zábavní
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
návštěvníci	návštěvník	k1gMnPc1	návštěvník
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
otáčivému	otáčivý	k2eAgInSc3d1	otáčivý
pohybu	pohyb	k1gInSc3	pohyb
a	a	k8xC	a
působení	působení	k1gNnSc3	působení
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kolotoče	kolotoč	k1gInPc1	kolotoč
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nP	stavit
jako	jako	k9	jako
pevné	pevný	k2eAgInPc4d1	pevný
(	(	kIx(	(
<g/>
stacionární	stacionární	k2eAgInPc4d1	stacionární
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
častěji	často	k6eAd2	často
jako	jako	k9	jako
mobilní	mobilní	k2eAgInPc1d1	mobilní
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
převážejí	převážet	k5eAaImIp3nP	převážet
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
kolotoče	kolotoč	k1gInPc1	kolotoč
řetízkové	řetízkový	k2eAgInPc1d1	řetízkový
<g/>
,	,	kIx,	,
plošinové	plošinový	k2eAgInPc1d1	plošinový
a	a	k8xC	a
naklápěcí	naklápěcí	k2eAgInPc1d1	naklápěcí
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
kolotoče	kolotoč	k1gInPc1	kolotoč
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
otáčivé	otáčivý	k2eAgInPc4d1	otáčivý
sloupy	sloup	k1gInPc4	sloup
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
řetízcích	řetízek	k1gInPc6	řetízek
zavěšeny	zavěšen	k2eAgInPc1d1	zavěšen
koše	koš	k1gInPc1	koš
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
koníci	koník	k1gMnPc1	koník
a	a	k8xC	a
loďky	loďka	k1gFnPc1	loďka
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
po	po	k7c6	po
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
i	i	k9	i
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
plošinový	plošinový	k2eAgInSc1d1	plošinový
kolotoč	kolotoč	k1gInSc1	kolotoč
v	v	k7c6	v
Hanau	Hanaus	k1gInSc6	Hanaus
u	u	k7c2	u
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
<g/>
;	;	kIx,	;
plošinové	plošinový	k2eAgInPc1d1	plošinový
kolotoče	kolotoč	k1gInPc1	kolotoč
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
v	v	k7c6	v
zábavních	zábavní	k2eAgInPc6d1	zábavní
parcích	park	k1gInPc6	park
a	a	k8xC	a
poháněly	pohánět	k5eAaImAgFnP	pohánět
ručně	ručně	k6eAd1	ručně
nebo	nebo	k8xC	nebo
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
postaven	postavit	k5eAaPmNgInS	postavit
první	první	k4xOgInSc1	první
kolotoč	kolotoč	k1gInSc1	kolotoč
na	na	k7c4	na
parní	parní	k2eAgInSc4d1	parní
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
elektrický	elektrický	k2eAgInSc4d1	elektrický
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
kolotoče	kolotoč	k1gInPc1	kolotoč
velice	velice	k6eAd1	velice
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
také	také	k9	také
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
technických	technický	k2eAgNnPc2d1	technické
zdokonalení	zdokonalení	k1gNnPc2	zdokonalení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mechanických	mechanický	k2eAgInPc2d1	mechanický
hudebních	hudební	k2eAgInPc2d1	hudební
automatů	automat	k1gInPc2	automat
(	(	kIx(	(
<g/>
orchestrion	orchestrion	k1gInSc1	orchestrion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pohyb	pohyb	k1gInSc1	pohyb
kolotoče	kolotoč	k1gInSc2	kolotoč
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
cirkusovou	cirkusový	k2eAgFnSc7d1	cirkusová
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
složitější	složitý	k2eAgInPc1d2	složitější
kolotoče	kolotoč	k1gInPc1	kolotoč
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgNnSc7d1	hydraulické
naklápěním	naklápění	k1gNnSc7	naklápění
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
pohyby	pohyb	k1gInPc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
jen	jen	k9	jen
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sedadly	sedadlo	k1gNnPc7	sedadlo
na	na	k7c6	na
řetízcích	řetízek	k1gInPc6	řetízek
nebo	nebo	k8xC	nebo
provazech	provaz	k1gInPc6	provaz
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
doplněn	doplnit	k5eAaPmNgInS	doplnit
horní	horní	k2eAgFnSc7d1	horní
plošinou	plošina	k1gFnSc7	plošina
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
zavěsit	zavěsit	k5eAaPmF	zavěsit
víc	hodně	k6eAd2	hodně
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
pevná	pevný	k2eAgFnSc1d1	pevná
plošina	plošina	k1gFnSc1	plošina
není	být	k5eNaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
k	k	k7c3	k
nasedání	nasedání	k1gNnSc3	nasedání
a	a	k8xC	a
vystupování	vystupování	k1gNnSc3	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
otáčení	otáčení	k1gNnSc6	otáčení
se	se	k3xPyFc4	se
sedátka	sedátko	k1gNnSc2	sedátko
s	s	k7c7	s
návštěvníky	návštěvník	k1gMnPc7	návštěvník
vlivem	vlivem	k7c2	vlivem
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
vyklánějí	vyklánět	k5eAaImIp3nP	vyklánět
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
návštěvníci	návštěvník	k1gMnPc1	návštěvník
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
mohou	moct	k5eAaImIp3nP	moct
navíc	navíc	k6eAd1	navíc
různě	různě	k6eAd1	různě
houpat	houpat	k5eAaImF	houpat
<g/>
,	,	kIx,	,
chytat	chytat	k5eAaImF	chytat
za	za	k7c4	za
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mají	mít	k5eAaImIp3nP	mít
pocit	pocit	k1gInSc4	pocit
volného	volný	k2eAgInSc2d1	volný
pohybu	pohyb	k1gInSc2	pohyb
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
větší	veliký	k2eAgFnSc3d2	veliký
mládeži	mládež	k1gFnSc3	mládež
a	a	k8xC	a
dospělým	dospělí	k1gMnPc3	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Otáčivá	otáčivý	k2eAgFnSc1d1	otáčivá
kruhová	kruhový	k2eAgFnSc1d1	kruhová
plošina	plošina	k1gFnSc1	plošina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
pevním	pevnit	k5eAaImIp1nS	pevnit
stavení	stavení	k1gNnSc2	stavení
nebo	nebo	k8xC	nebo
stanu	stan	k1gInSc2	stan
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
i	i	k9	i
střecha	střecha	k1gFnSc1	střecha
kolotoče	kolotoč	k1gInSc2	kolotoč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
jsou	být	k5eAaImIp3nP	být
upevněna	upevněn	k2eAgNnPc4d1	upevněno
sedadla	sedadlo	k1gNnPc4	sedadlo
<g/>
,	,	kIx,	,
lavičky	lavička	k1gFnPc4	lavička
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
koníčci	koníček	k1gMnPc1	koníček
<g/>
,	,	kIx,	,
autíčka	autíčko	k1gNnPc1	autíčko
nebo	nebo	k8xC	nebo
vagónky	vagónek	k1gInPc1	vagónek
<g/>
.	.	kIx.	.
</s>
<s>
Plošinové	plošinový	k2eAgInPc1d1	plošinový
kolotoče	kolotoč	k1gInPc1	kolotoč
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
především	především	k9	především
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
doprovázet	doprovázet	k5eAaImF	doprovázet
i	i	k8xC	i
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
stojí	stát	k5eAaImIp3nS	stát
historický	historický	k2eAgInSc1d1	historický
letenský	letenský	k2eAgInSc1d1	letenský
kolotoč	kolotoč	k1gInSc1	kolotoč
s	s	k7c7	s
koníčky	koníček	k1gInPc7	koníček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
zavřen	zavřen	k2eAgInSc1d1	zavřen
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
podobných	podobný	k2eAgInPc2d1	podobný
pouličních	pouliční	k2eAgInPc2d1	pouliční
kolotočů	kolotoč	k1gInPc2	kolotoč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
složitější	složitý	k2eAgInPc1d2	složitější
kolotoče	kolotoč	k1gInPc1	kolotoč
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgNnSc7d1	hydraulické
naklápěním	naklápění	k1gNnSc7	naklápění
plošiny	plošina	k1gFnSc2	plošina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
návštěvníci	návštěvník	k1gMnPc1	návštěvník
musí	muset	k5eAaImIp3nP	muset
připoutávat	připoutávat	k5eAaImF	připoutávat
<g/>
.	.	kIx.	.
</s>
<s>
Plošina	plošina	k1gFnSc1	plošina
se	se	k3xPyFc4	se
roztočí	roztočit	k5eAaPmIp3nS	roztočit
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
rovině	rovina	k1gFnSc6	rovina
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
sklopí	sklopit	k5eAaPmIp3nS	sklopit
až	až	k9	až
do	do	k7c2	do
svislé	svislý	k2eAgFnSc2d1	svislá
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
zábavních	zábavní	k2eAgInPc6d1	zábavní
parcích	park	k1gInPc6	park
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ještě	ještě	k9	ještě
složitější	složitý	k2eAgNnSc4d2	složitější
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
pohyb	pohyb	k1gInSc1	pohyb
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
pohyby	pohyb	k1gInPc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
řetízkový	řetízkový	k2eAgInSc1d1	řetízkový
kolotoč	kolotoč	k1gInSc1	kolotoč
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
zábavním	zábavní	k2eAgInSc6d1	zábavní
parku	park	k1gInSc6	park
Flags	Flags	k1gInSc1	Flags
over	over	k1gInSc1	over
Georgia	Georgius	k1gMnSc2	Georgius
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Georgia	Georgium	k1gNnSc2	Georgium
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
kolotoči	kolotoč	k1gInSc6	kolotoč
baví	bavit	k5eAaImIp3nS	bavit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
