<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálková	k1gFnSc1	Hálková
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1907	[number]	k4	1907
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1985	[number]	k4	1985
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
vnučka	vnučka	k1gFnSc1	vnučka
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Hálka	hálka	k1gFnSc1	hálka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
politika	politik	k1gMnSc2	politik
Ivana	Ivan	k1gMnSc2	Ivan
Hálka	hálka	k1gFnSc1	hálka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
herce	herec	k1gMnSc2	herec
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
herečky	herečka	k1gFnSc2	herečka
Jany	Jana	k1gFnSc2	Jana
Štěpánkové	Štěpánková	k1gFnSc2	Štěpánková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc4	divadlo
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
ochotnicky	ochotnicky	k6eAd1	ochotnicky
již	již	k6eAd1	již
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
bratislavského	bratislavský	k2eAgNnSc2d1	Bratislavské
gymnázia	gymnázium	k1gNnSc2	gymnázium
studovala	studovat	k5eAaImAgFnS	studovat
herectví	herectví	k1gNnSc4	herectví
na	na	k7c6	na
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
oddělení	oddělení	k1gNnSc6	oddělení
Státní	státní	k2eAgFnSc2d1	státní
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
krátké	krátký	k2eAgNnSc4d1	krátké
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
činohře	činohra	k1gFnSc6	činohra
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odjela	odjet	k5eAaPmAgFnS	odjet
už	už	k6eAd1	už
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
–	–	k?	–
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
satiry	satira	k1gFnSc2	satira
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
<g />
.	.	kIx.	.
</s>
<s>
ABC	ABC	kA	ABC
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Městských	městský	k2eAgNnPc6d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgInPc6d1	pražský
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
začleněno	začlenit	k5eAaPmNgNnS	začlenit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
ponejvíce	ponejvíce	k6eAd1	ponejvíce
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
jen	jen	k6eAd1	jen
řadu	řada	k1gFnSc4	řada
drobnějších	drobný	k2eAgFnPc2d2	drobnější
a	a	k8xC	a
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
==	==	k?	==
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
Eugene	Eugen	k1gInSc5	Eugen
O	o	k7c4	o
<g/>
́	́	k?	́
<g/>
Neill	Neill	k1gInSc4	Neill
<g/>
:	:	kIx,	:
Milionový	milionový	k2eAgMnSc1d1	milionový
Marco	Marco	k1gMnSc1	Marco
<g/>
,	,	kIx,	,
Kukašin	Kukašin	k2eAgMnSc1d1	Kukašin
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Bor	bor	k1gInSc1	bor
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulos	k1gInSc1	Makropulos
<g/>
,	,	kIx,	,
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bohuš	Bohuš	k1gMnSc1	Bohuš
Stejskal	Stejskal	k1gMnSc1	Stejskal
</s>
</p>
<p>
<s>
1933	[number]	k4	1933
Otto	Otto	k1gMnSc1	Otto
Indig	indigo	k1gNnPc2	indigo
<g/>
:	:	kIx,	:
Nevěsta	nevěsta	k1gFnSc1	nevěsta
z	z	k7c2	z
Torocka	Torocko	k1gNnSc2	Torocko
<g/>
,	,	kIx,	,
Klary	Klara	k1gFnSc2	Klara
Patkos-Nagyová	Patkos-Nagyový	k2eAgFnSc1d1	Patkos-Nagyový
<g/>
,	,	kIx,	,
Komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Salzer	Salzer	k1gMnSc1	Salzer
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
N.	N.	kA	N.
V.	V.	kA	V.
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
:	:	kIx,	:
Revizor	revizor	k1gMnSc1	revizor
<g/>
,	,	kIx,	,
Marja	Marj	k2eAgFnSc1d1	Marja
Antonovna	Antonovna	k1gFnSc1	Antonovna
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc2	Jiří
Frejka	Frejka	k1gFnSc1	Frejka
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
J.	J.	kA	J.
W.	W.	kA	W.
Goethe	Goethe	k1gFnSc1	Goethe
<g/>
:	:	kIx,	:
Faust	Faust	k1gFnSc1	Faust
<g/>
,	,	kIx,	,
Líza	Líza	k1gFnSc1	Líza
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Dostal	dostat	k5eAaPmAgMnS	dostat
</s>
</p>
<p>
<s>
1943	[number]	k4	1943
bratří	bratřit	k5eAaImIp3nS	bratřit
Mrštíkové	Mrštíkové	k2eAgFnSc1d1	Mrštíkové
<g/>
:	:	kIx,	:
Maryša	Maryša	k1gFnSc1	Maryša
<g/>
,	,	kIx,	,
Rozára	Rozára	k1gFnSc1	Rozára
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Aleš	Aleš	k1gMnSc1	Aleš
Podhorský	Podhorský	k1gMnSc1	Podhorský
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
I.	I.	kA	I.
S.	S.	kA	S.
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
<g/>
:	:	kIx,	:
Měsíc	měsíc	k1gInSc1	měsíc
na	na	k7c6	na
vsi	ves	k1gFnSc6	ves
<g/>
,	,	kIx,	,
Líza	Líza	k1gFnSc1	Líza
<g/>
,	,	kIx,	,
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Aleš	Aleš	k1gMnSc1	Aleš
Podhorský	Podhorský	k1gMnSc1	Podhorský
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
Carlo	Carlo	k1gNnSc1	Carlo
Goldoni	Goldon	k1gMnPc1	Goldon
<g/>
:	:	kIx,	:
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
Smeraldina	Smeraldina	k1gFnSc1	Smeraldina
<g/>
,	,	kIx,	,
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Dostal	dostat	k5eAaPmAgMnS	dostat
</s>
</p>
<p>
<s>
1957	[number]	k4	1957
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
<g/>
:	:	kIx,	:
<g/>
Balada	balada	k1gFnSc1	balada
z	z	k7c2	z
hadrů	hadr	k1gInPc2	hadr
<g/>
,	,	kIx,	,
Žebračka	žebračka	k1gFnSc1	žebračka
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ján	Ján	k1gMnSc1	Ján
Roháč	roháč	k1gMnSc1	roháč
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
Max	Max	k1gMnSc1	Max
Frisch	Frisch	k1gMnSc1	Frisch
<g/>
:	:	kIx,	:
Horká	horký	k2eAgFnSc1d1	horká
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
vdova	vdova	k1gFnSc1	vdova
Knechtlingová	Knechtlingový	k2eAgFnSc1d1	Knechtlingový
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Eva	Eva	k1gFnSc1	Eva
Sadková	Sadková	k1gFnSc1	Sadková
j.	j.	k?	j.
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brož	Brož	k1gMnSc1	Brož
<g/>
,	,	kIx,	,
Myrtil	Myrtil	k1gMnSc1	Myrtil
Frída	Frída	k1gMnSc1	Frída
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
československého	československý	k2eAgInSc2d1	československý
filmu	film	k1gInSc2	film
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
121	[number]	k4	121
<g/>
,	,	kIx,	,
155	[number]	k4	155
<g/>
,	,	kIx,	,
foto	foto	k1gNnSc1	foto
305	[number]	k4	305
<g/>
,	,	kIx,	,
385	[number]	k4	385
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Hraje	hrát	k5eAaImIp3nS	hrát
František	František	k1gMnSc1	František
Smolík	Smolík	k1gMnSc1	Smolík
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
144	[number]	k4	144
<g/>
,	,	kIx,	,
155	[number]	k4	155
<g/>
,	,	kIx,	,
312	[number]	k4	312
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Měnivá	měnivý	k2eAgFnSc1d1	měnivá
tvář	tvář	k1gFnSc1	tvář
divadla	divadlo	k1gNnSc2	divadlo
aneb	aneb	k?	aneb
Dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc2	století
s	s	k7c7	s
pražskými	pražský	k2eAgMnPc7d1	pražský
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
240	[number]	k4	240
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Pozdravy	pozdrav	k1gInPc1	pozdrav
za	za	k7c4	za
divadelní	divadelní	k2eAgFnSc4d1	divadelní
rampu	rampa	k1gFnSc4	rampa
<g/>
,	,	kIx,	,
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
152	[number]	k4	152
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Osudy	osud	k1gInPc1	osud
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
–	–	k?	–
Divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
156	[number]	k4	156
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-1502-0	[number]	k4	978-80-200-1502-0
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
divadla	divadlo	k1gNnPc1	divadlo
:	:	kIx,	:
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
divadelních	divadelní	k2eAgMnPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
615	[number]	k4	615
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7008	[number]	k4	7008
<g/>
-	-	kIx~	-
<g/>
107	[number]	k4	107
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
113	[number]	k4	113
<g/>
,	,	kIx,	,
265	[number]	k4	265
<g/>
,	,	kIx,	,
443	[number]	k4	443
<g/>
,	,	kIx,	,
505	[number]	k4	505
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Fikejz	Fikejz	k1gMnSc1	Fikejz
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
:	:	kIx,	:
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
K.	K.	kA	K.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
dotisk	dotisk	k1gInSc1	dotisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
750	[number]	k4	750
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
332	[number]	k4	332
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
347	[number]	k4	347
<g/>
–	–	k?	–
<g/>
348	[number]	k4	348
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
:	:	kIx,	:
DIVADLO	divadlo	k1gNnSc1	divadlo
aneb	aneb	k?	aneb
SNÁŘ	snář	k1gMnSc1	snář
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
113	[number]	k4	113
<g/>
,	,	kIx,	,
117	[number]	k4	117
<g/>
,	,	kIx,	,
158	[number]	k4	158
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
<g/>
/	/	kIx~	/
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
352	[number]	k4	352
<g/>
,	,	kIx,	,
463	[number]	k4	463
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
132	[number]	k4	132
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Konečná	konečný	k2eAgFnSc1d1	konečná
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Čtení	čtení	k1gNnSc1	čtení
o	o	k7c6	o
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc4	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
386	[number]	k4	386
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
:	:	kIx,	:
Kudy	kudy	k6eAd1	kudy
všudy	všudy	k6eAd1	všudy
za	za	k7c7	za
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
244	[number]	k4	244
<g/>
,	,	kIx,	,
248	[number]	k4	248
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
349	[number]	k4	349
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Müller	Müller	k1gMnSc1	Müller
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Padesát	padesát	k4xCc1	padesát
let	léto	k1gNnPc2	léto
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgInPc2d1	pražský
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
179	[number]	k4	179
</s>
</p>
<p>
<s>
Redakce	redakce	k1gFnSc1	redakce
umělecké	umělecký	k2eAgFnSc2d1	umělecká
správy	správa	k1gFnSc2	správa
divadla	divadlo	k1gNnSc2	divadlo
<g/>
:	:	kIx,	:
Čtvrtstoletí	čtvrtstoletí	k1gNnSc1	čtvrtstoletí
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
sborník	sborník	k1gInSc1	sborník
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c4	na
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
117	[number]	k4	117
</s>
</p>
<p>
<s>
Z.	Z.	kA	Z.
Sílová	sílový	k2eAgFnSc1d1	Sílová
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Kožíková	Kožíková	k1gFnSc1	Kožíková
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Mohylová	mohylový	k2eAgFnSc1d1	Mohylová
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
–	–	k?	–
Vinohradský	vinohradský	k2eAgInSc4d1	vinohradský
ansámbl	ansámbl	k1gInSc4	ansámbl
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
42	[number]	k4	42
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
,	,	kIx,	,
192	[number]	k4	192
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-239-9604-3	[number]	k4	978-80-239-9604-3
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
407	[number]	k4	407
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Tunys	Tunys	k1gInSc1	Tunys
<g/>
:	:	kIx,	:
Otomar	Otomar	k1gMnSc1	Otomar
Korbelář	korbelář	k1gMnSc1	korbelář
<g/>
,	,	kIx,	,
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
86	[number]	k4	86
<g/>
,	,	kIx,	,
95	[number]	k4	95
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7388-552-6	[number]	k4	978-80-7388-552-6
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Valtrová	Valtrová	k1gFnSc1	Valtrová
<g/>
:	:	kIx,	:
Kronika	kronika	k1gFnSc1	kronika
rodu	rod	k1gInSc2	rod
Hrušínských	Hrušínský	k2eAgMnPc2d1	Hrušínský
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
289	[number]	k4	289
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-0485-X	[number]	k4	80-207-0485-X
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Valtrová	Valtrová	k1gFnSc1	Valtrová
<g/>
:	:	kIx,	:
ORNESTINUM	ORNESTINUM	kA	ORNESTINUM
<g/>
,	,	kIx,	,
Slavná	slavný	k2eAgFnSc1d1	slavná
éra	éra	k1gFnSc1	éra
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgNnPc2d1	Pražské
<g/>
,	,	kIx,	,
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
,	,	kIx,	,
191	[number]	k4	191
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7243-121-8	[number]	k4	80-7243-121-8
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Valtrová	Valtrová	k1gFnSc1	Valtrová
–	–	k?	–
Ota	Ota	k1gMnSc1	Ota
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
:	:	kIx,	:
Hraje	hrát	k5eAaImIp3nS	hrát
váš	váš	k3xOp2gMnSc1	váš
tatínek	tatínek	k1gMnSc1	tatínek
ještě	ještě	k9	ještě
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
,	,	kIx,	,
238	[number]	k4	238
<g/>
,	,	kIx,	,
260	[number]	k4	260
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85625-19-9	[number]	k4	80-85625-19-9
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálkový	k2eAgFnSc1d1	Hálkový
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálkový	k2eAgFnSc1d1	Hálkový
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálkový	k2eAgFnSc1d1	Hálkový
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálkový	k2eAgFnSc1d1	Hálkový
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Hálková	Hálkový	k2eAgFnSc1d1	Hálkový
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
