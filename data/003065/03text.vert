<s>
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgInPc2d1	používaný
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
)	)	kIx)	)
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energii	energie	k1gFnSc4	energie
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
ani	ani	k8xC	ani
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
jí	jíst	k5eAaImIp3nS	jíst
podobné	podobný	k2eAgNnSc1d1	podobné
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
elementární	elementární	k2eAgFnPc4d1	elementární
"	"	kIx"	"
<g/>
definice	definice	k1gFnPc4	definice
<g/>
"	"	kIx"	"
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
intuitivní	intuitivní	k2eAgMnPc1d1	intuitivní
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
nedostatků	nedostatek	k1gInPc2	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
"	"	kIx"	"
<g/>
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
<g/>
"	"	kIx"	"
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
pomocí	pomocí	k7c2	pomocí
značně	značně	k6eAd1	značně
nefyzikálních	fyzikální	k2eNgInPc2d1	nefyzikální
<g/>
,	,	kIx,	,
nedefinovaných	definovaný	k2eNgInPc2d1	nedefinovaný
a	a	k8xC	a
vágních	vágní	k2eAgInPc2d1	vágní
pojmů	pojem	k1gInPc2	pojem
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zničit	zničit	k5eAaPmF	zničit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
přeměnit	přeměnit	k5eAaPmF	přeměnit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
antropomorfní	antropomorfní	k2eAgInSc4d1	antropomorfní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
<g/>
.	.	kIx.	.
</s>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
obecnější	obecní	k2eAgFnSc1d2	obecní
a	a	k8xC	a
přesnější	přesný	k2eAgFnSc1d2	přesnější
definice	definice	k1gFnSc1	definice
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
pojem	pojem	k1gInSc4	pojem
symetrie	symetrie	k1gFnSc2	symetrie
vůči	vůči	k7c3	vůči
časovému	časový	k2eAgNnSc3d1	časové
posunutí	posunutí	k1gNnSc3	posunutí
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc1d1	obecný
systém	systém	k1gInSc1	systém
symetrický	symetrický	k2eAgInSc1d1	symetrický
vůči	vůči	k7c3	vůči
operaci	operace	k1gFnSc3	operace
časového	časový	k2eAgNnSc2d1	časové
posunutí	posunutí	k1gNnSc2	posunutí
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
aditivní	aditivní	k2eAgFnSc1d1	aditivní
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
symetriemi	symetrie	k1gFnPc7	symetrie
a	a	k8xC	a
zákony	zákon	k1gInPc7	zákon
zachování	zachování	k1gNnSc2	zachování
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
obecněji	obecně	k6eAd2	obecně
popisuje	popisovat	k5eAaImIp3nS	popisovat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
důležitý	důležitý	k2eAgInSc1d1	důležitý
teorém	teorém	k1gInSc1	teorém
Emmy	Emma	k1gFnSc2	Emma
Noetherové	Noetherová	k1gFnSc2	Noetherová
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
lze	lze	k6eAd1	lze
formulovat	formulovat	k5eAaImF	formulovat
též	též	k9	též
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
obecností	obecnost	k1gFnSc7	obecnost
i	i	k8xC	i
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
soustavy	soustava	k1gFnSc2	soustava
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
při	při	k7c6	při
všech	všecek	k3xTgInPc6	všecek
dějích	děj	k1gInPc6	děj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc2	on
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Problémem	problém	k1gInSc7	problém
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
soustava	soustava	k1gFnSc1	soustava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
nesnadné	snadný	k2eNgNnSc1d1	nesnadné
zavést	zavést	k5eAaPmF	zavést
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
zákonu	zákon	k1gInSc6	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
vyhnout	vyhnout	k5eAaPmF	vyhnout
logické	logický	k2eAgFnSc3d1	logická
tautologii	tautologie	k1gFnSc3	tautologie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jiná	jiný	k2eAgFnSc1d1	jiná
formulace	formulace	k1gFnSc1	formulace
zákona	zákon	k1gInSc2	zákon
<g/>
:	:	kIx,	:
Není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sestrojit	sestrojit	k5eAaPmF	sestrojit
perpetuum	perpetuum	k1gNnSc4	perpetuum
mobile	mobile	k1gNnSc7	mobile
prvního	první	k4xOgInSc2	první
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
stroj	stroj	k1gInSc1	stroj
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
konal	konat	k5eAaImAgInS	konat
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nedostatkem	nedostatek	k1gInSc7	nedostatek
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
perpetuum	perpetuum	k1gNnSc1	perpetuum
mobile	mobile	k1gNnSc2	mobile
prvního	první	k4xOgMnSc2	první
druhu	druh	k1gInSc2	druh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
skutečnosti	skutečnost	k1gFnPc1	skutečnost
nelze	lze	k6eNd1	lze
dobře	dobře	k6eAd1	dobře
definovat	definovat	k5eAaBmF	definovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
zákonu	zákon	k1gInSc6	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
logické	logický	k2eAgFnSc3d1	logická
tautologii	tautologie	k1gFnSc3	tautologie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
elementární	elementární	k2eAgFnSc6d1	elementární
mechanice	mechanika	k1gFnSc6	mechanika
bývá	bývat	k5eAaImIp3nS	bývat
užívaná	užívaný	k2eAgFnSc1d1	užívaná
definice	definice	k1gFnSc1	definice
následujícího	následující	k2eAgInSc2d1	následující
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
mechanický	mechanický	k2eAgInSc1d1	mechanický
systém	systém	k1gInSc1	systém
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
účinkům	účinek	k1gInPc3	účinek
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
součet	součet	k1gInSc1	součet
kinetické	kinetický	k2eAgFnSc2d1	kinetická
a	a	k8xC	a
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
soustava	soustava	k1gFnSc1	soustava
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
mění	měnit	k5eAaImIp3nS	měnit
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
energie	energie	k1gFnSc2	energie
v	v	k7c4	v
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Také	také	k9	také
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
definice	definice	k1gFnSc1	definice
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
slabá	slabý	k2eAgNnPc4d1	slabé
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
nelze	lze	k6eNd1	lze
"	"	kIx"	"
<g/>
odstínit	odstínit	k5eAaPmF	odstínit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
požadavek	požadavek	k1gInSc4	požadavek
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
účinkům	účinek	k1gInPc3	účinek
okolí	okolí	k1gNnSc2	okolí
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
nikdy	nikdy	k6eAd1	nikdy
splnit	splnit	k5eAaPmF	splnit
exaktně	exaktně	k6eAd1	exaktně
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
užívána	užívat	k5eAaImNgFnS	užívat
určitá	určitý	k2eAgFnSc1d1	určitá
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
abstrakce	abstrakce	k1gFnSc1	abstrakce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přesto	přesto	k8xC	přesto
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
přiblížení	přiblížení	k1gNnSc2	přiblížení
či	či	k8xC	či
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
abstrakce	abstrakce	k1gFnSc2	abstrakce
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
Ek	Ek	k1gFnSc1	Ek
+	+	kIx~	+
Ep	Ep	k1gFnSc1	Ep
=	=	kIx~	=
konstanta	konstanta	k1gFnSc1	konstanta
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
Ek	Ek	k1gFnSc1	Ek
je	být	k5eAaImIp3nS	být
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
Ep	Ep	k1gFnSc1	Ep
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc2	energie
potenciální	potenciální	k2eAgNnSc1d1	potenciální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
mechanické	mechanický	k2eAgFnSc6d1	mechanická
soustavě	soustava	k1gFnSc6	soustava
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
naznačené	naznačený	k2eAgFnSc2d1	naznačená
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
abstrakce	abstrakce	k1gFnSc2	abstrakce
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
mechanické	mechanický	k2eAgFnSc2d1	mechanická
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
Celková	celkový	k2eAgFnSc1d1	celková
mechanická	mechanický	k2eAgFnSc1d1	mechanická
energie	energie	k1gFnSc1	energie
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
soustavy	soustava	k1gFnSc2	soustava
těles	těleso	k1gNnPc2	těleso
při	při	k7c6	při
mechanickém	mechanický	k2eAgInSc6d1	mechanický
ději	děj	k1gInSc6	děj
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
obecného	obecný	k2eAgNnSc2d1	obecné
zákonu	zákon	k1gInSc3	zákon
zachování	zachování	k1gNnSc1	zachování
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
především	především	k9	především
přeměna	přeměna	k1gFnSc1	přeměna
mechanické	mechanický	k2eAgFnSc2d1	mechanická
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
vznikající	vznikající	k2eAgFnSc4d1	vznikající
třením	tření	k1gNnSc7	tření
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
těles	těleso	k1gNnPc2	těleso
po	po	k7c6	po
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
těles	těleso	k1gNnPc2	těleso
látkovým	látkový	k2eAgNnSc7d1	látkové
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
Zcela	zcela	k6eAd1	zcela
obecnou	obecný	k2eAgFnSc7d1	obecná
a	a	k8xC	a
exaktní	exaktní	k2eAgFnSc7d1	exaktní
formulací	formulace	k1gFnSc7	formulace
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
představuje	představovat	k5eAaImIp3nS	představovat
první	první	k4xOgFnSc1	první
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
druh	druh	k1gInSc4	druh
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
a	a	k8xC	a
obecných	obecný	k2eAgInPc2d1	obecný
druhů	druh	k1gInPc2	druh
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Symetrie	symetrie	k1gFnSc2	symetrie
První	první	k4xOgFnSc1	první
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
věta	věta	k1gFnSc1	věta
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc6	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
Zákon	zákon	k1gInSc4	zákon
zachování	zachování	k1gNnSc4	zachování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
Kirchhoffovy	Kirchhoffův	k2eAgFnSc2d1	Kirchhoffova
zákony	zákon	k1gInPc1	zákon
</s>
