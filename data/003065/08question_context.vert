<s>
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgInPc2d1	používaný
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
)	)	kIx)	)
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energii	energie	k1gFnSc4	energie
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
ani	ani	k8xC	ani
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>

