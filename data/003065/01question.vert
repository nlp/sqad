<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
energii	energie	k1gFnSc4	energie
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
ani	ani	k8xC	ani
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
energie	energie	k1gFnSc2	energie
<g/>
?	?	kIx.	?
</s>
