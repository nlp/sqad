<s>
Osifikace	osifikace	k1gFnPc1	osifikace
<g/>
,	,	kIx,	,
též	též	k9	též
kostnatění	kostnatění	k1gNnSc4	kostnatění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přeměna	přeměna	k1gFnSc1	přeměna
chrupavky	chrupavka	k1gFnSc2	chrupavka
či	či	k8xC	či
vaziva	vazivo	k1gNnSc2	vazivo
na	na	k7c4	na
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
kostí	kost	k1gFnPc2	kost
i	i	k8xC	i
při	při	k7c6	při
hojení	hojení	k1gNnSc6	hojení
zlomeniny	zlomenina	k1gFnSc2	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidských	lidský	k2eAgFnPc2d1	lidská
kostí	kost	k1gFnPc2	kost
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
následně	následně	k6eAd1	následně
osifikuje	osifikovat	k5eAaBmIp3nS	osifikovat
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
chrupavky	chrupavka	k1gFnSc2	chrupavka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
enchondrálně	enchondrálně	k6eAd1	enchondrálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
např.	např.	kA	např.
klíční	klíční	k2eAgFnSc4d1	klíční
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
část	část	k1gFnSc4	část
lebky	lebka	k1gFnSc2	lebka
<g/>
)	)	kIx)	)
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
vaziva	vazivo	k1gNnSc2	vazivo
(	(	kIx(	(
<g/>
intramembranózně	intramembranózně	k6eAd1	intramembranózně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
vzniká	vznikat	k5eAaImIp3nS	vznikat
osifikační	osifikační	k2eAgNnSc1d1	osifikační
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
šíří	šíř	k1gFnSc7	šíř
k	k	k7c3	k
oběma	dva	k4xCgInPc3	dva
koncům	konec	k1gInPc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
jich	on	k3xPp3gFnPc2	on
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nP	objevit
se	se	k3xPyFc4	se
osifikační	osifikační	k2eAgNnPc1d1	osifikační
jádra	jádro	k1gNnPc1	jádro
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
středovému	středový	k2eAgInSc3d1	středový
naproti	naproti	k7c3	naproti
<g/>
.	.	kIx.	.
</s>
<s>
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
však	však	k9	však
zachován	zachován	k2eAgInSc1d1	zachován
proužek	proužek	k1gInSc1	proužek
chrupavky	chrupavka	k1gFnSc2	chrupavka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
růstová	růstový	k2eAgFnSc1d1	růstová
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
mizí	mizet	k5eAaImIp3nS	mizet
kolem	kolem	k7c2	kolem
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
obratlovců	obratlovec	k1gMnPc2	obratlovec
nejprve	nejprve	k6eAd1	nejprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
nezralá	zralý	k2eNgFnSc1d1	nezralá
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
kost	kost	k1gFnSc1	kost
bez	bez	k7c2	bez
stop	stopa	k1gFnPc2	stopa
lamelární	lamelární	k2eAgFnSc2d1	lamelární
struktury	struktura	k1gFnSc2	struktura
<g/>
;	;	kIx,	;
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
kostních	kostní	k2eAgFnPc2d1	kostní
buněk	buňka	k1gFnPc2	buňka
obklopených	obklopený	k2eAgFnPc2d1	obklopená
neuspořádanými	uspořádaný	k2eNgNnPc7d1	neuspořádané
vlákny	vlákno	k1gNnPc7	vlákno
kolagenu	kolagen	k1gInSc2	kolagen
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
postupně	postupně	k6eAd1	postupně
vzniká	vznikat	k5eAaImIp3nS	vznikat
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
stavbu	stavba	k1gFnSc4	stavba
–	–	k?	–
tzv.	tzv.	kA	tzv.
lamelární	lamelární	k2eAgFnSc1d1	lamelární
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lamelární	lamelární	k2eAgFnSc1d1	lamelární
kost	kost	k1gFnSc1	kost
následně	následně	k6eAd1	následně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
procesu	proces	k1gInSc2	proces
kostnatění	kostnatění	k1gNnSc1	kostnatění
(	(	kIx(	(
<g/>
osifikace	osifikace	k1gFnSc1	osifikace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
dva	dva	k4xCgInPc1	dva
odlišné	odlišný	k2eAgInPc1d1	odlišný
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
může	moct	k5eAaImIp3nS	moct
kostnatění	kostnatění	k1gNnSc3	kostnatění
tkáně	tkáň	k1gFnSc2	tkáň
probíhat	probíhat	k5eAaImF	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
mechanismy	mechanismus	k1gInPc7	mechanismus
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
chondrogenní	chondrogenní	k2eAgFnSc4d1	chondrogenní
(	(	kIx(	(
<g/>
enchondrální	enchondrální	k2eAgFnSc4d1	enchondrální
<g/>
)	)	kIx)	)
–	–	k?	–
kost	kost	k1gFnSc1	kost
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
chrupavky	chrupavka	k1gFnSc2	chrupavka
desmogenní	desmogenní	k2eAgFnSc2d1	desmogenní
(	(	kIx(	(
<g/>
též	též	k9	též
intramembranózní	intramembranózní	k2eAgFnSc1d1	intramembranózní
<g/>
)	)	kIx)	)
–	–	k?	–
kost	kost	k1gFnSc1	kost
vzniká	vznikat	k5eAaImIp3nS	vznikat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vaziva	vazivo	k1gNnSc2	vazivo
Při	při	k7c6	při
tzv.	tzv.	kA	tzv.
enchondrální	enchondrální	k2eAgFnSc6d1	enchondrální
osifikaci	osifikace	k1gFnSc6	osifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
lidských	lidský	k2eAgFnPc2d1	lidská
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
(	(	kIx(	(
<g/>
hyalinního	hyalinní	k2eAgInSc2d1	hyalinní
<g/>
)	)	kIx)	)
chrupavčitého	chrupavčitý	k2eAgInSc2d1	chrupavčitý
modelu	model	k1gInSc2	model
budoucí	budoucí	k2eAgFnSc2d1	budoucí
kosti	kost	k1gFnSc2	kost
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
mezenchymální	mezenchymální	k2eAgFnSc2d1	mezenchymální
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
lamelární	lamelární	k2eAgFnSc6d1	lamelární
kosti	kost	k1gFnSc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
vzniká	vznikat	k5eAaImIp3nS	vznikat
uprostřed	uprostřed	k7c2	uprostřed
tohoto	tento	k3xDgInSc2	tento
chrupavčitého	chrupavčitý	k2eAgInSc2d1	chrupavčitý
modelu	model	k1gInSc2	model
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
známé	známý	k2eAgFnSc6d1	známá
jako	jako	k8xS	jako
diafýza	diafýza	k1gFnSc1	diafýza
<g/>
)	)	kIx)	)
zárodek	zárodek	k1gInSc1	zárodek
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
–	–	k?	–
první	první	k4xOgFnSc2	první
kostní	kostní	k2eAgFnSc2d1	kostní
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
osteoblasty	osteoblast	k1gInPc1	osteoblast
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
buněk	buňka	k1gFnPc2	buňka
perichondriálního	perichondriální	k2eAgInSc2d1	perichondriální
obalu	obal	k1gInSc2	obal
chrupavky	chrupavka	k1gFnSc2	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
chrupavky	chrupavka	k1gFnSc2	chrupavka
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
vápenaté	vápenatý	k2eAgFnPc1d1	vápenatá
soli	sůl	k1gFnPc1	sůl
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
chrupavka	chrupavka	k1gFnSc1	chrupavka
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
naprosto	naprosto	k6eAd1	naprosto
zacementována	zacementovat	k5eAaPmNgFnS	zacementovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
buňky	buňka	k1gFnPc1	buňka
chrupavky	chrupavka	k1gFnSc2	chrupavka
odříznuty	odříznout	k5eAaPmNgFnP	odříznout
od	od	k7c2	od
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
umírají	umírat	k5eAaImIp3nP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
cementu	cement	k1gInSc2	cement
<g/>
"	"	kIx"	"
razí	razit	k5eAaImIp3nS	razit
cestu	cesta	k1gFnSc4	cesta
krevní	krevní	k2eAgFnSc2d1	krevní
vlásečnice	vlásečnice	k1gFnSc2	vlásečnice
<g/>
,	,	kIx,	,
rozrušujíc	rozrušovat	k5eAaImSgFnS	rozrušovat
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
chrupavčitou	chrupavčitý	k2eAgFnSc4d1	chrupavčitá
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
ustanoví	ustanovit	k5eAaPmIp3nS	ustanovit
tzv.	tzv.	kA	tzv.
primární	primární	k2eAgFnSc1d1	primární
osifikační	osifikační	k2eAgNnSc4d1	osifikační
jádro	jádro	k1gNnSc4	jádro
<g/>
;	;	kIx,	;
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
kostní	kostní	k2eAgInPc1d1	kostní
osteoblasty	osteoblast	k1gInPc1	osteoblast
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
osteoklasty	osteoklast	k1gInPc1	osteoklast
s	s	k7c7	s
neméně	málo	k6eNd2	málo
důležitou	důležitý	k2eAgFnSc7d1	důležitá
rolí	role	k1gFnSc7	role
<g/>
)	)	kIx)	)
šíří	šířit	k5eAaImIp3nP	šířit
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
budoucí	budoucí	k2eAgFnSc2d1	budoucí
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
růstových	růstový	k2eAgFnPc2d1	růstová
plotének	ploténka	k1gFnPc2	ploténka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neustále	neustále	k6eAd1	neustále
vzniká	vznikat	k5eAaImIp3nS	vznikat
nová	nový	k2eAgFnSc1d1	nová
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
,	,	kIx,	,
odstupuje	odstupovat	k5eAaImIp3nS	odstupovat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
mineralizuje	mineralizovat	k5eAaBmIp3nS	mineralizovat
a	a	k8xC	a
po	po	k7c6	po
osídlení	osídlení	k1gNnSc6	osídlení
kostními	kostní	k2eAgFnPc7d1	kostní
buňkami	buňka	k1gFnPc7	buňka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
regulérní	regulérní	k2eAgFnSc4d1	regulérní
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
kost	kost	k1gFnSc1	kost
roste	růst	k5eAaImIp3nS	růst
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
kostní	kostní	k2eAgFnPc1d1	kostní
buňky	buňka	k1gFnPc1	buňka
ukládají	ukládat	k5eAaImIp3nP	ukládat
i	i	k9	i
pod	pod	k7c7	pod
okosticí	okostice	k1gFnSc7	okostice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k5eAaImF	růst
kosti	kost	k1gFnPc4	kost
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
některých	některý	k3yIgMnPc2	některý
ještěrů	ještěr	k1gMnPc2	ještěr
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnSc4d1	další
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgFnSc4d1	sekundární
<g/>
)	)	kIx)	)
osifikační	osifikační	k2eAgNnPc1d1	osifikační
jádra	jádro	k1gNnPc1	jádro
(	(	kIx(	(
<g/>
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
kostní	kostní	k2eAgFnSc2d1	kostní
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
oblastech	oblast	k1gFnPc6	oblast
kosti	kost	k1gFnSc2	kost
–	–	k?	–
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
epifýzách	epifýza	k1gFnPc6	epifýza
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
však	však	k9	však
tato	tento	k3xDgNnPc1	tento
jádra	jádro	k1gNnPc1	jádro
vznikají	vznikat	k5eAaImIp3nP	vznikat
až	až	k9	až
velmi	velmi	k6eAd1	velmi
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
až	až	k6eAd1	až
třetího	třetí	k4xOgInSc2	třetí
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jedinec	jedinec	k1gMnSc1	jedinec
dospěje	dochvít	k5eAaPmIp3nS	dochvít
<g/>
,	,	kIx,	,
růstové	růstový	k2eAgFnSc2d1	růstová
destičky	destička	k1gFnSc2	destička
zanikají	zanikat	k5eAaImIp3nP	zanikat
a	a	k8xC	a
kosti	kost	k1gFnPc1	kost
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
prakticky	prakticky	k6eAd1	prakticky
přestanou	přestat	k5eAaPmIp3nP	přestat
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
např.	např.	kA	např.
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
obojživelníků	obojživelník	k1gMnPc2	obojživelník
a	a	k8xC	a
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
kosti	kost	k1gFnPc1	kost
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k1gInSc4	růst
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgNnPc4	tento
tzv.	tzv.	kA	tzv.
intramembranózní	intramembranózní	k2eAgFnSc2d1	intramembranózní
osifikace	osifikace	k1gFnSc2	osifikace
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
poměrně	poměrně	k6eAd1	poměrně
významná	významný	k2eAgFnSc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
:	:	kIx,	:
nevzniká	vznikat	k5eNaImIp3nS	vznikat
žádná	žádný	k3yNgFnSc1	žádný
přechodná	přechodný	k2eAgFnSc1d1	přechodná
chrupavčitá	chrupavčitý	k2eAgFnSc1d1	chrupavčitá
tkáň	tkáň	k1gFnSc1	tkáň
–	–	k?	–
veškerá	veškerý	k3xTgFnSc1	veškerý
kostní	kostní	k2eAgFnSc1d1	kostní
tkáň	tkáň	k1gFnSc1	tkáň
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
tvoří	tvořit	k5eAaImIp3nP	tvořit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
mezenchymálních	mezenchymální	k2eAgFnPc2d1	mezenchymální
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
zárodečné	zárodečný	k2eAgFnSc6d1	zárodečná
lamelární	lamelární	k2eAgFnSc6d1	lamelární
kosti	kost	k1gFnSc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
Mezenchym	mezenchym	k1gInSc1	mezenchym
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
uspořádává	uspořádávat	k5eAaImIp3nS	uspořádávat
do	do	k7c2	do
jistých	jistý	k2eAgFnPc2d1	jistá
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
membrán	membrána	k1gFnPc2	membrána
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgMnS	být
také	také	k9	také
odvozen	odvozen	k2eAgInSc4d1	odvozen
název	název	k1gInSc4	název
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
kostnatění	kostnatění	k1gNnSc2	kostnatění
<g/>
.	.	kIx.	.
</s>
<s>
Mezenchymální	Mezenchymální	k2eAgFnPc1d1	Mezenchymální
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
gelovitá	gelovitý	k2eAgFnSc1d1	gelovitá
mezibuněčná	mezibuněčný	k2eAgFnSc1d1	mezibuněčná
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc1	první
osteoblasty	osteoblast	k1gInPc1	osteoblast
a	a	k8xC	a
krevní	krevní	k2eAgNnPc1d1	krevní
zásobení	zásobení	k1gNnPc1	zásobení
<g/>
,	,	kIx,	,
gelovitá	gelovitý	k2eAgFnSc1d1	gelovitá
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
kompaktnějšími	kompaktní	k2eAgFnPc7d2	kompaktnější
tyčinkami	tyčinka	k1gFnPc7	tyčinka
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
mineralizuje	mineralizovat	k5eAaBmIp3nS	mineralizovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Kostní	kostní	k2eAgFnPc1d1	kostní
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vznikající	vznikající	k2eAgFnSc2d1	vznikající
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
další	další	k2eAgFnSc4d1	další
kostní	kostní	k2eAgFnSc4d1	kostní
hmotu	hmota	k1gFnSc4	hmota
–	–	k?	–
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
kost	kost	k1gFnSc4	kost
přirůstá	přirůstat	k5eAaImIp3nS	přirůstat
<g/>
.	.	kIx.	.
</s>
<s>
Intramembranózní	Intramembranózní	k2eAgFnSc1d1	Intramembranózní
osifikace	osifikace	k1gFnSc1	osifikace
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
především	především	k9	především
tzv.	tzv.	kA	tzv.
dermálních	dermální	k2eAgFnPc2d1	dermální
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kostí	kost	k1gFnPc2	kost
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
v	v	k7c6	v
mezenchymální	mezenchymální	k2eAgFnSc6d1	mezenchymální
tkáni	tkáň	k1gFnSc6	tkáň
kožní	kožní	k2eAgFnSc2d1	kožní
škáry	škára	k1gFnSc2	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gInSc1	dermis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
např.	např.	kA	např.
mnoha	mnoho	k4c2	mnoho
kostí	kost	k1gFnPc2	kost
lebky	lebka	k1gFnSc2	lebka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
např.	např.	kA	např.
moderní	moderní	k2eAgFnSc2d1	moderní
kosti	kost	k1gFnSc2	kost
dolní	dolní	k2eAgFnSc2d1	dolní
čelisti	čelist	k1gFnSc2	čelist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kostí	kost	k1gFnSc7	kost
pletence	pletenec	k1gInSc2	pletenec
horní	horní	k2eAgFnSc2d1	horní
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc7d1	jistá
specialitou	specialita	k1gFnSc7	specialita
jsou	být	k5eAaImIp3nP	být
sezamoidní	sezamoidní	k2eAgFnPc1d1	sezamoidní
kosti	kost	k1gFnPc1	kost
vznikající	vznikající	k2eAgInSc1d1	vznikající
osifikací	osifikace	k1gFnSc7	osifikace
ve	v	k7c6	v
šlachách	šlacha	k1gFnPc6	šlacha
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
zejména	zejména	k9	zejména
čéška	čéška	k1gFnSc1	čéška
a	a	k8xC	a
hrášková	hráškový	k2eAgFnSc1d1	hrášková
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
perichondrální	perichondrální	k2eAgInSc1d1	perichondrální
a	a	k8xC	a
periostální	periostální	k2eAgInSc1d1	periostální
růst	růst	k1gInSc1	růst
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
apozičním	apoziční	k2eAgInSc7d1	apoziční
růstem	růst	k1gInSc7	růst
<g/>
.	.	kIx.	.
</s>
