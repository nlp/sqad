<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
"	"	kIx"	"
<g/>
Tom	Tom	k1gMnSc1	Tom
<g/>
"	"	kIx"	"
Hanks	Hanks	k1gInSc1	Hanks
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1956	[number]	k4	1956
Concord	Concord	k1gInSc1	Concord
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
dvou	dva	k4xCgMnPc2	dva
Oscarů	Oscar	k1gMnPc2	Oscar
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgInPc2d1	placený
herců	herc	k1gInPc2	herc
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
filmu	film	k1gInSc2	film
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
herců	herec	k1gMnPc2	herec
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
Spencera	Spencer	k1gMnSc2	Spencer
Tracyho	Tracy	k1gMnSc2	Tracy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
cenu	cena	k1gFnSc4	cena
akademie	akademie	k1gFnSc2	akademie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgNnPc6d1	jdoucí
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
jižanského	jižanský	k2eAgMnSc2d1	jižanský
kuchaře	kuchař	k1gMnSc2	kuchař
Amose	Amos	k1gMnSc2	Amos
Mefforda	Mefford	k1gMnSc2	Mefford
Hankse	Hanks	k1gMnSc2	Hanks
a	a	k8xC	a
Angličanky	Angličanka	k1gFnSc2	Angličanka
Janet	Janeta	k1gFnPc2	Janeta
Merilyn	Merilyn	k1gMnSc1	Merilyn
Fragerové	Fragerová	k1gFnSc2	Fragerová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgMnPc4	tři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
sestru	sestra	k1gFnSc4	sestra
Sandru	Sandra	k1gFnSc4	Sandra
<g/>
,	,	kIx,	,
bratry	bratr	k1gMnPc4	bratr
Lawrence	Lawrenec	k1gMnSc4	Lawrenec
a	a	k8xC	a
Jimiho	Jimi	k1gMnSc4	Jimi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
amatérsky	amatérsky	k6eAd1	amatérsky
věnoval	věnovat	k5eAaPmAgMnS	věnovat
herectví	herectví	k1gNnSc4	herectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
provázelo	provázet	k5eAaImAgNnS	provázet
i	i	k8xC	i
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
hereckou	herecký	k2eAgFnSc4d1	herecká
zkušenost	zkušenost	k1gFnSc4	zkušenost
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
Shakespearových	Shakespearových	k2eAgFnPc6d1	Shakespearových
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
společnosti	společnost	k1gFnSc6	společnost
v	v	k7c6	v
Sacramentu	Sacrament	k1gInSc6	Sacrament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Samanthou	Samantha	k1gFnSc7	Samantha
Lewesovou	Lewesový	k2eAgFnSc7d1	Lewesový
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
práci	práce	k1gFnSc6	práce
ve	v	k7c6	v
festivalu	festival	k1gInSc6	festival
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
snaze	snaha	k1gFnSc6	snaha
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
malou	malý	k2eAgFnSc4d1	malá
<g/>
)	)	kIx)	)
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
He	he	k0	he
Knows	Knows	k1gInSc4	Knows
You	You	k1gFnPc4	You
<g/>
\	\	kIx~	\
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Alone	Alon	k1gInSc5	Alon
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
nezaznamenala	zaznamenat	k5eNaPmAgFnS	zaznamenat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
sitcomu	sitcom	k1gInSc6	sitcom
Bosom	Bosom	k1gInSc1	Bosom
Buddies	Buddies	k1gInSc1	Buddies
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
seriálech	seriál	k1gInPc6	seriál
jako	jako	k8xS	jako
například	například	k6eAd1	například
Mazes	Mazes	k1gInSc1	Mazes
and	and	k?	and
Monsters	Monsters	k1gInSc1	Monsters
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
úspěch	úspěch	k1gInSc1	úspěch
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgInS	dostavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
po	po	k7c6	po
filmu	film	k1gInSc6	film
Žbluňk	žbluňk	k0	žbluňk
(	(	kIx(	(
<g/>
Splash	Splasha	k1gFnPc2	Splasha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
získal	získat	k5eAaPmAgMnS	získat
další	další	k2eAgFnSc4d1	další
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Big	Big	k1gMnSc1	Big
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
VOLUNTEERS	VOLUNTEERS	kA	VOLUNTEERS
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Ritou	Rita	k1gFnSc7	Rita
Wilsonovou	Wilsonová	k1gFnSc7	Wilsonová
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
Ohňostroj	ohňostroj	k1gInSc4	ohňostroj
marnosti	marnost	k1gFnSc2	marnost
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Bruce	Bruce	k1gFnSc2	Bruce
Willise	Willise	k1gFnSc2	Willise
a	a	k8xC	a
Melanie	Melanie	k1gFnSc2	Melanie
Griffithové	Griffithová	k1gFnSc2	Griffithová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
zvětšovaly	zvětšovat	k5eAaImAgInP	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Meg	Meg	k1gFnSc2	Meg
Ryanové	Ryanová	k1gFnSc2	Ryanová
<g/>
,	,	kIx,	,
Garyho	Gary	k1gMnSc2	Gary
Sinise	Sinise	k1gFnSc2	Sinise
<g/>
,	,	kIx,	,
Robin	robin	k2eAgMnSc1d1	robin
Wright	Wright	k1gMnSc1	Wright
Pennové	Pennová	k1gFnSc2	Pennová
<g/>
,	,	kIx,	,
Denzela	Denzela	k1gMnSc2	Denzela
Washingtona	Washington	k1gMnSc2	Washington
<g/>
,	,	kIx,	,
Edworda	Edword	k1gMnSc2	Edword
Burnse	Burns	k1gMnSc2	Burns
<g/>
,	,	kIx,	,
Matta	Matt	k1gMnSc2	Matt
Damona	Damon	k1gMnSc2	Damon
<g/>
,	,	kIx,	,
Helen	Helena	k1gFnPc2	Helena
Huntové	Huntová	k1gFnSc2	Huntová
<g/>
,	,	kIx,	,
Leonardem	Leonardo	k1gMnSc7	Leonardo
DiCapriem	DiCaprium	k1gNnSc7	DiCaprium
<g/>
,	,	kIx,	,
Catherine	Catherin	k1gInSc5	Catherin
Zeta-Jonesové	Zeta-Jonesový	k2eAgFnPc1d1	Zeta-Jonesová
<g/>
,	,	kIx,	,
Jeana	Jean	k1gMnSc2	Jean
Rena	Renus	k1gMnSc4	Renus
<g/>
,	,	kIx,	,
Audrey	Audre	k1gMnPc4	Audre
Tautou	Tautá	k1gFnSc4	Tautá
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
Robertsové	Robertsová	k1gFnPc1	Robertsová
<g/>
,	,	kIx,	,
Ayelety	Ayelet	k1gInPc1	Ayelet
Zurerové	Zurerové	k2eAgInPc2d1	Zurerové
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
filmografie	filmografie	k1gFnSc1	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Bosom	Bosom	k1gInSc1	Bosom
Buddies	Buddies	k1gInSc1	Buddies
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
On	on	k3xPp3gMnSc1	on
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
sama	sám	k3xTgFnSc1	sám
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Pánská	pánský	k2eAgFnSc1d1	pánská
jízda	jízda	k1gFnSc1	jízda
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Žbluňk	žbluňk	k0	žbluňk
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Blázni	blázen	k1gMnPc1	blázen
a	a	k8xC	a
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
botou	bota	k1gFnSc7	bota
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Dům	dům	k1gInSc1	dům
za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
peníze	peníz	k1gInPc4	peníz
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Pokaždé	pokaždé	k6eAd1	pokaždé
se	se	k3xPyFc4	se
loučíme	loučit	k5eAaImIp1nP	loučit
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Zátah	zátah	k1gInSc1	zátah
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Pointa	pointa	k1gFnSc1	pointa
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Velký	velký	k2eAgMnSc1d1	velký
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Turner	turner	k1gMnSc1	turner
a	a	k8xC	a
Hooch	Hooch	k1gMnSc1	Hooch
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Joe	Joe	k1gMnSc1	Joe
proti	proti	k7c3	proti
sopce	sopka	k1gFnSc3	sopka
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Ohňostroj	ohňostroj	k1gInSc1	ohňostroj
marnosti	marnost	k1gFnSc2	marnost
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Radio	radio	k1gNnSc1	radio
Flyer	Flyer	k1gMnSc1	Flyer
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Velké	velká	k1gFnSc2	velká
vítězství	vítězství	k1gNnSc2	vítězství
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Padlí	padlý	k2eAgMnPc1d1	padlý
andělé	anděl	k1gMnPc1	anděl
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Samotář	samotář	k1gMnSc1	samotář
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Apollo	Apollo	k1gMnSc1	Apollo
13	[number]	k4	13
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
To	to	k9	to
je	být	k5eAaImIp3nS	být
náš	náš	k3xOp1gInSc4	náš
hit	hit	k1gInSc4	hit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Láska	láska	k1gFnSc1	láska
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
vojína	vojín	k1gMnSc2	vojín
Ryana	Ryan	k1gMnSc4	Ryan
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
2	[number]	k4	2
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Zelená	zelený	k2eAgFnSc1d1	zelená
míle	míle	k1gFnSc1	míle
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Trosečník	trosečník	k1gMnSc1	trosečník
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
neohrožených	ohrožený	k2eNgFnPc2d1	neohrožená
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Chyť	chytit	k5eAaPmRp2nS	chytit
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
dokážeš	dokázat	k5eAaPmIp2nS	dokázat
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Road	Road	k1gMnSc1	Road
to	ten	k3xDgNnSc1	ten
Perdition	Perdition	k1gInSc4	Perdition
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Lupiči	lupič	k1gMnPc1	lupič
paní	paní	k1gFnSc2	paní
domácí	domácí	k2eAgMnPc1d1	domácí
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Polární	polární	k2eAgInSc1d1	polární
expres	expres	k1gInSc1	expres
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Terminál	terminála	k1gFnPc2	terminála
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
válka	válka	k1gFnSc1	válka
pana	pan	k1gMnSc2	pan
Wilsona	Wilson	k1gMnSc2	Wilson
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
The	The	k1gMnSc1	The
Great	Great	k2eAgMnSc1d1	Great
Buck	Buck	k1gMnSc1	Buck
Howard	Howard	k1gMnSc1	Howard
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
The	The	k1gMnSc1	The
Pacific	Pacific	k1gMnSc1	Pacific
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
3	[number]	k4	3
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Electric	Electric	k1gMnSc1	Electric
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Havajské	havajský	k2eAgFnPc4d1	Havajská
prázdniny	prázdniny	k1gFnPc4	prázdniny
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Moje	můj	k3xOp1gFnSc1	můj
krásná	krásný	k2eAgFnSc1d1	krásná
učitelka	učitelka	k1gFnSc1	učitelka
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Movie	Movie	k1gFnSc1	Movie
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc2	Movie
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
hlasitě	hlasitě	k6eAd1	hlasitě
&	&	k?	&
nesmírně	smírně	k6eNd1	smírně
blízko	blízko	k6eAd1	blízko
(	(	kIx(	(
<g/>
Extremely	Extremel	k1gInPc1	Extremel
Loud	louda	k1gFnPc2	louda
&	&	k?	&
Incredibly	Incredibly	k1gMnSc2	Incredibly
Close	Clos	k1gMnSc2	Clos
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Small	Small	k1gMnSc1	Small
Fry	Fry	k1gMnSc1	Fry
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Atlas	Atlas	k1gInSc1	Atlas
mraků	mrak	k1gInPc2	mrak
(	(	kIx(	(
<g/>
Cloud	Cloud	k1gInSc1	Cloud
Atlas	Atlas	k1gInSc1	Atlas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
pana	pan	k1gMnSc4	pan
Bankse	Bankse	k1gFnSc1	Bankse
(	(	kIx(	(
<g/>
Saving	Saving	k1gInSc1	Saving
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Banks	Banks	k1gInSc1	Banks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Kapitán	kapitán	k1gMnSc1	kapitán
Phillips	Phillips	k1gInSc1	Phillips
(	(	kIx(	(
<g/>
Captain	Captain	k2eAgInSc1d1	Captain
Phillips	Phillips	k1gInSc1	Phillips
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Most	most	k1gInSc1	most
špionů	špion	k1gMnPc2	špion
(	(	kIx(	(
<g/>
Bridge	Bridge	k1gInSc1	Bridge
of	of	k?	of
Spies	Spies	k1gInSc1	Spies
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Inferno	inferno	k1gNnSc1	inferno
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Sully	Sulla	k1gFnSc2	Sulla
<g/>
:	:	kIx,	:
Zázrak	zázrak	k1gInSc1	zázrak
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Hudson	Hudson	k1gInSc1	Hudson
(	(	kIx(	(
<g/>
Sully	Sulla	k1gFnPc1	Sulla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Hologram	hologram	k1gInSc1	hologram
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
(	(	kIx(	(
<g/>
A	a	k9	a
Hologram	hologram	k1gInSc1	hologram
for	forum	k1gNnPc2	forum
the	the	k?	the
King	King	k1gMnSc1	King
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Akta	akta	k1gNnPc1	akta
Pentagon	Pentagon	k1gInSc1	Pentagon
<g/>
:	:	kIx,	:
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Post	posta	k1gFnPc2	posta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
The	The	k1gMnSc4	The
Circle	Circl	k1gMnSc4	Circl
</s>
</p>
<p>
<s>
==	==	k?	==
Dabing	dabing	k1gInSc4	dabing
==	==	k?	==
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Radio	radio	k1gNnSc1	radio
Flajer	Flajer	k1gMnSc1	Flajer
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
2	[number]	k4	2
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Auta	auto	k1gNnSc2	auto
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Simpsonovi	Simpson	k1gMnSc3	Simpson
ve	v	k7c6	v
filmu	film	k1gInSc6	film
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
3	[number]	k4	3
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gInSc1	Oscar
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Forrest	Forrest	k1gInSc1	Forrest
GumpZlatý	GumpZlatý	k2eAgInSc1d1	GumpZlatý
Glóbus	glóbus	k1gInSc1	glóbus
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Velký	velký	k2eAgMnSc1d1	velký
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Trosečník	trosečník	k1gMnSc1	trosečník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanks	k1gInSc1	Hanks
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
