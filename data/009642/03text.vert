<p>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
křída	křída	k1gFnSc1	křída
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgInSc1d2	starší
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
oddělení	oddělení	k1gNnPc2	oddělení
křídového	křídový	k2eAgInSc2d1	křídový
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
145	[number]	k4	145
až	až	k9	až
100,5	[number]	k4	100,5
Ma	Ma	k1gFnPc2	Ma
(	(	kIx(	(
<g/>
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
označení	označení	k1gNnSc3	označení
celé	celá	k1gFnSc2	celá
této	tento	k3xDgFnSc2	tento
geologické	geologický	k2eAgFnSc2d1	geologická
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
psittacosaurus	psittacosaurus	k1gInSc1	psittacosaurus
či	či	k8xC	či
spinosaurus	spinosaurus	k1gInSc1	spinosaurus
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
skuiny	skuin	k1gInPc1	skuin
přeživší	přeživší	k2eAgInPc1d1	přeživší
ze	z	k7c2	z
svrchní	svrchní	k2eAgFnSc2d1	svrchní
jury	jura	k1gFnSc2	jura
nabývají	nabývat	k5eAaImIp3nP	nabývat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Carcharodontosauridae	Carcharodontosaurida	k1gFnSc2	Carcharodontosaurida
či	či	k8xC	či
Coelurosauria	Coelurosaurium	k1gNnSc2	Coelurosaurium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořích	moře	k1gNnPc6	moře
ubývají	ubývat	k5eAaImIp3nP	ubývat
ichtyosauři	ichtyosaurus	k1gMnPc1	ichtyosaurus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
počátkem	počátkem	k7c2	počátkem
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
krytosemenné	krytosemenný	k2eAgFnPc1d1	krytosemenná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Early	earl	k1gMnPc4	earl
Cretaceous	Cretaceous	k1gInSc1	Cretaceous
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Křída	křída	k1gFnSc1	křída
</s>
</p>
<p>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
křída	křída	k1gFnSc1	křída
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spodní	spodní	k2eAgFnSc1d1	spodní
křída	křída	k1gFnSc1	křída
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stratigrafická	Stratigrafický	k2eAgFnSc1d1	Stratigrafická
tabulka	tabulka	k1gFnSc1	tabulka
</s>
</p>
