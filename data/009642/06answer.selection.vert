<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
křída	křída	k1gFnSc1	křída
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgInSc1d2	starší
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
oddělení	oddělení	k1gNnPc2	oddělení
křídového	křídový	k2eAgInSc2d1	křídový
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
145	[number]	k4	145
až	až	k9	až
100,5	[number]	k4	100,5
Ma	Ma	k1gFnPc2	Ma
(	(	kIx(	(
<g/>
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
označení	označení	k1gNnSc3	označení
celé	celá	k1gFnSc2	celá
této	tento	k3xDgFnSc2	tento
geologické	geologický	k2eAgFnSc2d1	geologická
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
