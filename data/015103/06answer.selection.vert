<s>
Národní	národní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
(	(	kIx(
<g/>
NÚKIB	NÚKIB	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ústředním	ústřední	k2eAgInSc7d1
správním	správní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
včetně	včetně	k7c2
ochrany	ochrana	k1gFnSc2
utajovaných	utajovaný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
informačních	informační	k2eAgInPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgInPc2d1
systémů	systém	k1gInPc2
a	a	k8xC
kryptografické	kryptografický	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sídlem	sídlo	k1gNnSc7
tohoto	tento	k3xDgInSc2
úřadu	úřad	k1gInSc2
je	být	k5eAaImIp3nS
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>