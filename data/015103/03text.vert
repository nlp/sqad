<s>
Národní	národní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
</s>
<s>
Národní	národní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Mučednická	mučednický	k2eAgFnSc1d1
1125	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
616	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ředitel	ředitel	k1gMnSc1
</s>
<s>
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Řehka	Řehka	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.nukib.cz	www.nukib.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
zzfnkp	zzfnkp	k1gInSc1
<g/>
3	#num#	k4
IČO	IČO	kA
</s>
<s>
05800226	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
(	(	kIx(
<g/>
NÚKIB	NÚKIB	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ústředním	ústřední	k2eAgInSc7d1
správním	správní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
včetně	včetně	k7c2
ochrany	ochrana	k1gFnSc2
utajovaných	utajovaný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
informačních	informační	k2eAgInPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgInPc2d1
systémů	systém	k1gInPc2
a	a	k8xC
kryptografické	kryptografický	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sídlem	sídlo	k1gNnSc7
tohoto	tento	k3xDgInSc2
úřadu	úřad	k1gInSc2
je	být	k5eAaImIp3nS
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Zavedla	zavést	k5eAaPmAgFnS
jej	on	k3xPp3gInSc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
novela	novela	k1gFnSc1
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
č.	č.	k?
205	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
zákona	zákon	k1gInSc2
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
zákona	zákon	k1gInSc2
č.	č.	k?
181	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7
NÚKIB	NÚKIB	kA
byl	být	k5eAaImAgInS
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
Dušan	Dušan	k1gMnSc1
Navrátil	Navrátil	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
je	být	k5eAaImIp3nS
ředitelem	ředitel	k1gMnSc7
NÚKIB	NÚKIB	kA
Karel	Karel	k1gMnSc1
Řehka	Řehka	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
NÚKIB	NÚKIB	kA
vykonává	vykonávat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
činností	činnost	k1gFnPc2
dle	dle	k7c2
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
ukládá	ukládat	k5eAaImIp3nS
příslušné	příslušný	k2eAgInPc4d1
správní	správní	k2eAgInPc4d1
tresty	trest	k1gInPc4
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
jako	jako	k9
koordinační	koordinační	k2eAgInSc1d1
orgán	orgán	k1gInSc1
ve	v	k7c6
stavu	stav	k1gInSc6
kybernetického	kybernetický	k2eAgNnSc2d1
nebezpečí	nebezpečí	k1gNnSc2
<g/>
,	,	kIx,
zajišťuje	zajišťovat	k5eAaImIp3nS
prevenci	prevence	k1gFnSc4
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc4
a	a	k8xC
metodickou	metodický	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
kybernetické	kybernetický	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
ve	v	k7c6
vybraných	vybraný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
ochrany	ochrana	k1gFnSc2
utajovaných	utajovaný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
analýzu	analýza	k1gFnSc4
a	a	k8xC
monitoring	monitoring	k1gInSc4
kybernetických	kybernetický	k2eAgFnPc2d1
hrozeb	hrozba	k1gFnPc2
a	a	k8xC
rizik	riziko	k1gNnPc2
<g/>
,	,	kIx,
vykonává	vykonávat	k5eAaImIp3nS
působnost	působnost	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
veřejné	veřejný	k2eAgFnSc2d1
regulované	regulovaný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
PRS	prs	k1gInSc1
<g/>
)	)	kIx)
Evropského	evropský	k2eAgInSc2d1
programu	program	k1gInSc2
družicové	družicový	k2eAgFnSc2d1
navigace	navigace	k1gFnSc2
Galileo	Galilea	k1gFnSc5
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Úřad	úřad	k1gInSc1
dále	daleko	k6eAd2
též	též	k9
vykonává	vykonávat	k5eAaImIp3nS
příslušnou	příslušný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
zahájil	zahájit	k5eAaPmAgInS
se	s	k7c7
120	#num#	k4
zaměstnanci	zaměstnanec	k1gMnPc1
svoji	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
a	a	k8xC
převzal	převzít	k5eAaPmAgMnS
agendy	agenda	k1gFnPc4
Národního	národní	k2eAgInSc2d1
bezpečnostního	bezpečnostní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
působnosti	působnost	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
kybernetické	kybernetický	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
(	(	kIx(
<g/>
NCKB	NCKB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
fungovalo	fungovat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NÚKIB	NÚKIB	kA
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
budově	budova	k1gFnSc6
kybernetického	kybernetický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
v	v	k7c6
Brně-Žabovřeskách	Brně-Žabovřeska	k1gFnPc6
v	v	k7c6
Mučednické	mučednický	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
sloužilo	sloužit	k5eAaImAgNnS
NCKB	NCKB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
plánu	plán	k1gInSc6
je	být	k5eAaImIp3nS
vybudování	vybudování	k1gNnSc1
nové	nový	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
v	v	k7c6
areálu	areál	k1gInSc6
kasáren	kasárny	k1gFnPc2
v	v	k7c6
Černých	Černých	k2eAgFnPc6d1
Polích	pole	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
působit	působit	k5eAaImF
až	až	k9
400	#num#	k4
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
uvedeno	uvést	k5eAaPmNgNnS
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2023	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Správní	správní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
NÚKIB	NÚKIB	kA
a	a	k8xC
přestupky	přestupek	k1gInPc1
poskytovatelů	poskytovatel	k1gMnPc2
služeb	služba	k1gFnPc2
elektronických	elektronický	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
podrobně	podrobně	k6eAd1
doplnil	doplnit	k5eAaPmAgMnS
Zákon	zákon	k1gInSc4
o	o	k7c6
právu	právo	k1gNnSc6
na	na	k7c4
digitální	digitální	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
změna	změna	k1gFnSc1
zákona	zákon	k1gInSc2
o	o	k7c6
kyb	kyb	k?
<g/>
.	.	kIx.
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Zákon	zákon	k1gInSc1
č.	č.	k?
181	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
21	#num#	k4
<g/>
a.	a.	k?
<g/>
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Alternativní	alternativní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
205	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
181	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
souvisejících	související	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
zákona	zákon	k1gInSc2
č.	č.	k?
104	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
některé	některý	k3yIgInPc1
další	další	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Alternativní	alternativní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
Na	na	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
v	v	k7c6
ČR	ČR	kA
dohlíží	dohlížet	k5eAaImIp3nS
nový	nový	k2eAgInSc4d1
úřad	úřad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
–	–	k?
České	český	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-01	2017-08-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Na	na	k7c4
počítačové	počítačový	k2eAgMnPc4d1
piráty	pirát	k1gMnPc4
si	se	k3xPyFc3
posvítí	posvítit	k5eAaPmIp3nP
nový	nový	k2eAgInSc4d1
úřad	úřad	k1gInSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
bránit	bránit	k5eAaImF
kybernetickým	kybernetický	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-01	2017-08-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
šéfem	šéf	k1gMnSc7
úřadu	úřad	k1gInSc2
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
je	být	k5eAaImIp3nS
generál	generál	k1gMnSc1
Karel	Karel	k1gMnSc1
Řehka	Řehka	k1gMnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-03-20	2020-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
181	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
22	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
vDostupné	vDostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Alternativní	alternativní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
181	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
kybernetické	kybernetický	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
§	§	k?
23	#num#	k4
<g/>
.	.	kIx.
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Alternativní	alternativní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
právu	právo	k1gNnSc6
na	na	k7c4
digitální	digitální	k2eAgFnPc4d1
služby	služba	k1gFnPc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
bezpečnostní	bezpečnostní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
CSIRT	CSIRT	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
pro	pro	k7c4
kybernetickou	kybernetický	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
