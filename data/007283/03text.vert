<s>
Iluze	iluze	k1gFnSc1	iluze
je	být	k5eAaImIp3nS	být
mylný	mylný	k2eAgInSc4d1	mylný
smyslový	smyslový	k2eAgInSc4d1	smyslový
vjem	vjem	k1gInSc4	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Iluze	iluze	k1gFnSc1	iluze
vždy	vždy	k6eAd1	vždy
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečného	skutečný	k2eAgInSc2d1	skutečný
podnětu	podnět	k1gInSc2	podnět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
smyslovými	smyslový	k2eAgInPc7d1	smyslový
receptory	receptor	k1gInPc7	receptor
chybně	chybně	k6eAd1	chybně
interpretován	interpretován	k2eAgMnSc1d1	interpretován
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
klamné	klamný	k2eAgNnSc1d1	klamné
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
,	,	kIx,	,
mylná	mylný	k2eAgFnSc1d1	mylná
naděje	naděje	k1gFnSc1	naděje
<g/>
.	.	kIx.	.
objektivní	objektivní	k2eAgFnSc1d1	objektivní
-	-	kIx~	-
podléhá	podléhat	k5eAaImIp3nS	podléhat
jí	on	k3xPp3gFnSc3	on
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
zapadající	zapadající	k2eAgNnSc1d1	zapadající
slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
polední	polední	k2eAgNnSc1d1	polední
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
tyčka	tyčka	k1gFnSc1	tyčka
ponořená	ponořený	k2eAgFnSc1d1	ponořená
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
vypadá	vypadat	k5eAaImIp3nS	vypadat
zlomená	zlomený	k2eAgFnSc1d1	zlomená
apod.	apod.	kA	apod.
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
iluze	iluze	k1gFnSc1	iluze
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
kouzelnických	kouzelnický	k2eAgInPc2d1	kouzelnický
triků	trik	k1gInPc2	trik
<g/>
.	.	kIx.	.
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
nepatická	patický	k2eNgFnSc1d1	patický
(	(	kIx(	(
<g/>
nechorobná	chorobný	k2eNgFnSc1d1	chorobný
<g/>
)	)	kIx)	)
-	-	kIx~	-
porucha	porucha	k1gFnSc1	porucha
lidského	lidský	k2eAgNnSc2d1	lidské
vnímání	vnímání	k1gNnSc2	vnímání
charakterizované	charakterizovaný	k2eAgFnSc6d1	charakterizovaná
zkreslenými	zkreslený	k2eAgInPc7d1	zkreslený
vjemy	vjem	k1gInPc7	vjem
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
hub	houba	k1gFnPc2	houba
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
splete	splést	k5eAaPmIp3nS	splést
suchý	suchý	k2eAgInSc4d1	suchý
list	list	k1gInSc4	list
s	s	k7c7	s
houbou	houba	k1gFnSc7	houba
<g/>
.	.	kIx.	.
patická	patická	k1gFnSc1	patická
(	(	kIx(	(
<g/>
chorobná	chorobný	k2eAgFnSc1d1	chorobná
<g/>
)	)	kIx)	)
-	-	kIx~	-
klamné	klamný	k2eAgNnSc1d1	klamné
vnímání	vnímání	k1gNnSc1	vnímání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
duševní	duševní	k2eAgFnSc7d1	duševní
poruchou	porucha	k1gFnSc7	porucha
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
u	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
unavených	unavený	k2eAgFnPc2d1	unavená
(	(	kIx(	(
<g/>
stres	stres	k1gInSc1	stres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iluze	iluze	k1gFnSc1	iluze
lze	lze	k6eAd1	lze
také	také	k9	také
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
smyslový	smyslový	k2eAgInSc1d1	smyslový
receptor	receptor	k1gInSc1	receptor
je	být	k5eAaImIp3nS	být
klamán	klamán	k2eAgInSc1d1	klamán
<g/>
,	,	kIx,	,
na	na	k7c4	na
zrakové	zrakový	k2eAgInPc4d1	zrakový
(	(	kIx(	(
<g/>
optické	optický	k2eAgInPc4d1	optický
klamy	klam	k1gInPc4	klam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluchové	sluchový	k2eAgFnPc4d1	sluchová
atd.	atd.	kA	atd.
Subjektivní	subjektivní	k2eAgFnPc4d1	subjektivní
iluze	iluze	k1gFnPc4	iluze
vznikají	vznikat	k5eAaImIp3nP	vznikat
především	především	k9	především
z	z	k7c2	z
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
pozornosti	pozornost	k1gFnSc2	pozornost
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
zabrán	zabrat	k5eAaPmNgMnS	zabrat
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyšel	slyšet	k5eAaImAgMnS	slyšet
klepání	klepání	k1gNnSc4	klepání
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
pozornosti	pozornost	k1gFnSc2	pozornost
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
očekává	očekávat	k5eAaImIp3nS	očekávat
návštěvu	návštěva	k1gFnSc4	návštěva
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyšel	slyšet	k5eAaImAgMnS	slyšet
klepání	klepání	k1gNnSc4	klepání
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
šatech	šat	k1gInPc6	šat
nebo	nebo	k8xC	nebo
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
snadno	snadno	k6eAd1	snadno
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
dojmu	dojem	k1gInSc3	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
všichni	všechen	k3xTgMnPc1	všechen
kolemjdoucí	kolemjdoucí	k1gMnPc1	kolemjdoucí
dívají	dívat	k5eAaImIp3nP	dívat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
vystrašený	vystrašený	k2eAgMnSc1d1	vystrašený
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
vidí	vidět	k5eAaImIp3nS	vidět
číhající	číhající	k2eAgNnSc1d1	číhající
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
(	(	kIx(	(
<g/>
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
strach	strach	k1gInSc1	strach
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgFnPc4d1	velká
oči	oko	k1gNnPc4	oko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
iluze	iluze	k1gFnPc1	iluze
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
afektivní	afektivní	k2eAgFnPc4d1	afektivní
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
silného	silný	k2eAgNnSc2d1	silné
emočního	emoční	k2eAgNnSc2d1	emoční
rozpoložení	rozpoložení	k1gNnSc2	rozpoložení
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
negativní	negativní	k2eAgFnPc4d1	negativní
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
člověk	člověk	k1gMnSc1	člověk
vůbec	vůbec	k9	vůbec
nevnímá	vnímat	k5eNaImIp3nS	vnímat
podnět	podnět	k1gInSc1	podnět
(	(	kIx(	(
<g/>
nemůže	moct	k5eNaImIp3nS	moct
něco	něco	k3yInSc4	něco
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
neslyší	slyšet	k5eNaImIp3nS	slyšet
zvonění	zvonění	k1gNnSc1	zvonění
telefonu	telefon	k1gInSc2	telefon
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
paobraz	paobraz	k1gInSc1	paobraz
-	-	kIx~	-
pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
hledí	hledět	k5eAaImIp3nS	hledět
na	na	k7c4	na
barevnou	barevný	k2eAgFnSc4d1	barevná
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
pak	pak	k6eAd1	pak
pohlédne	pohlédnout	k5eAaPmIp3nS	pohlédnout
jinam	jinam	k6eAd1	jinam
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
<g />
.	.	kIx.	.
</s>
<s>
dobu	doba	k1gFnSc4	doba
vidí	vidět	k5eAaImIp3nS	vidět
původní	původní	k2eAgInSc1d1	původní
obraz	obraz	k1gInSc1	obraz
v	v	k7c6	v
negativních	negativní	k2eAgFnPc6d1	negativní
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
pareidolie	pareidolie	k1gFnSc1	pareidolie
-	-	kIx~	-
fantazijní	fantazijní	k2eAgNnSc1d1	fantazijní
dotváření	dotváření	k1gNnSc1	dotváření
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
určitých	určitý	k2eAgInPc2d1	určitý
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mraky	mrak	k1gInPc1	mrak
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
připomínají	připomínat	k5eAaImIp3nP	připomínat
lidskou	lidský	k2eAgFnSc4d1	lidská
tvář	tvář	k1gFnSc4	tvář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
synestezie	synestezie	k1gFnSc1	synestezie
-	-	kIx~	-
asociace	asociace	k1gFnSc1	asociace
vjemů	vjem	k1gInPc2	vjem
různými	různý	k2eAgInPc7d1	různý
smyslovými	smyslový	k2eAgInPc7d1	smyslový
orgány	orgán	k1gInPc7	orgán
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
uslyšíme	uslyšet	k5eAaPmIp1nP	uslyšet
určitý	určitý	k2eAgInSc4d1	určitý
tón	tón	k1gInSc4	tón
a	a	k8xC	a
vybaví	vybavit	k5eAaPmIp3nP	vybavit
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
nějaká	nějaký	k3yIgFnSc1	nějaký
<g />
.	.	kIx.	.
</s>
<s>
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
fantom	fantom	k1gInSc1	fantom
údu	úd	k1gInSc2	úd
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
amputovanou	amputovaný	k2eAgFnSc7d1	amputovaná
končetinou	končetina	k1gFnSc7	končetina
tuto	tento	k3xDgFnSc4	tento
končetinu	končetina	k1gFnSc4	končetina
stále	stále	k6eAd1	stále
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
svědění	svědění	k1gNnSc4	svědění
apod.	apod.	kA	apod.
akuse	akus	k1gMnSc2	akus
hudby	hudba	k1gFnSc2	hudba
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
slyší	slyšet	k5eAaImIp3nS	slyšet
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
reálné	reálný	k2eAgInPc1d1	reálný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc4	řeč
nebo	nebo	k8xC	nebo
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
jiný	jiný	k2eAgInSc4d1	jiný
opakující	opakující	k2eAgInSc4d1	opakující
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
akustický	akustický	k2eAgInSc4d1	akustický
vjem	vjem	k1gInSc4	vjem
<g/>
.	.	kIx.	.
zraková	zrakový	k2eAgFnSc1d1	zraková
-	-	kIx~	-
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc1	postava
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
mohou	moct	k5eAaImIp3nP	moct
zdát	zdát	k5eAaPmF	zdát
menší	malý	k2eAgFnPc1d2	menší
či	či	k8xC	či
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
nám	my	k3xPp1nPc3	my
něco	něco	k6eAd1	něco
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
nehýbou	hýbat	k5eNaImIp3nP	hýbat
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
sluchová	sluchový	k2eAgFnSc1d1	sluchová
-	-	kIx~	-
slyšíme	slyšet	k5eAaImIp1nP	slyšet
lidi	člověk	k1gMnPc4	člověk
štěkat	štěkat	k5eAaImF	štěkat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
psy	pes	k1gMnPc4	pes
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
čichová	čichový	k2eAgFnSc1d1	čichová
a	a	k8xC	a
chuťová	chuťový	k2eAgFnSc1d1	chuťová
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
jídlo	jídlo	k1gNnSc1	jídlo
chutná	chutnat	k5eAaImIp3nS	chutnat
a	a	k8xC	a
nějak	nějak	k6eAd1	nějak
divně	divně	k6eAd1	divně
<g/>
,	,	kIx,	,
zapáchá	zapáchat	k5eAaImIp3nS	zapáchat
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
někdo	někdo	k3yInSc1	někdo
otrávit	otrávit	k5eAaPmF	otrávit
hmatová	hmatový	k2eAgNnPc1d1	hmatové
-	-	kIx~	-
prádlo	prádlo	k1gNnSc1	prádlo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
vzrušivé	vzrušivý	k2eAgInPc4d1	vzrušivý
doteky	dotek	k1gInPc4	dotek
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
pohybová	pohybový	k2eAgFnSc1d1	pohybová
-	-	kIx~	-
chůze	chůze	k1gFnSc1	chůze
je	být	k5eAaImIp3nS	být
těžká	těžký	k2eAgFnSc1d1	těžká
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc1	pocit
vznášení	vznášení	k1gNnSc2	vznášení
se	se	k3xPyFc4	se
útrobní	útrobní	k2eAgMnPc1d1	útrobní
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
pocity	pocit	k1gInPc1	pocit
změněných	změněný	k2eAgInPc2d1	změněný
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
blud	blud	k1gInSc1	blud
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
člověk	člověk	k1gMnSc1	člověk
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
objevuje	objevovat	k5eAaImIp3nS	objevovat
nezvykle	zvykle	k6eNd1	zvykle
mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyvodí	vyvodit	k5eAaPmIp3nS	vyvodit
blud	blud	k1gInSc1	blud
<g/>
,	,	kIx,	,
že	že	k8xS	že
naše	náš	k3xOp1gFnSc1	náš
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
déjà	déjà	k?	déjà
vu	vu	k?	vu
depersonalizace	depersonalizace	k1gFnSc1	depersonalizace
fikce	fikce	k1gFnSc1	fikce
halucinace	halucinace	k1gFnSc1	halucinace
-	-	kIx~	-
iluze	iluze	k1gFnSc1	iluze
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
<g />
.	.	kIx.	.
</s>
<s>
vyvolána	vyvolán	k2eAgFnSc1d1	vyvolána
podnětem	podnět	k1gInSc7	podnět
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
oklamáním	oklamání	k1gNnSc7	oklamání
smyslům	smysl	k1gInPc3	smysl
<g/>
,	,	kIx,	,
halucinace	halucinace	k1gFnSc1	halucinace
vzniká	vznikat	k5eAaImIp3nS	vznikat
bez	bez	k7c2	bez
podnětu	podnět	k1gInSc2	podnět
sugesce	sugesce	k1gFnSc2	sugesce
-	-	kIx~	-
sugestivní	sugestivní	k2eAgNnSc1d1	sugestivní
chování	chování	k1gNnSc1	chování
podporuje	podporovat	k5eAaImIp3nS	podporovat
vznik	vznik	k1gInSc4	vznik
iluzí	iluze	k1gFnPc2	iluze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
důvěřivému	důvěřivý	k2eAgMnSc3d1	důvěřivý
člověku	člověk	k1gMnSc3	člověk
budeme	být	k5eAaImBp1nP	být
vyprávět	vyprávět	k5eAaImF	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
hradě	hrad	k1gInSc6	hrad
straší	strašit	k5eAaImIp3nS	strašit
<g/>
,	,	kIx,	,
snadněji	snadno	k6eAd2	snadno
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
uviděl	uvidět	k5eAaPmAgMnS	uvidět
přízrak	přízrak	k1gInSc4	přízrak
HARTL	Hartl	k1gMnSc1	Hartl
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
HARTLOVÁ	Hartlová	k1gFnSc1	Hartlová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Psychologický	psychologický	k2eAgInSc1d1	psychologický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7178	[number]	k4	7178
<g/>
-	-	kIx~	-
<g/>
303	[number]	k4	303
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
224	[number]	k4	224
<g/>
.	.	kIx.	.
</s>
<s>
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc1d1	fantastický
a	a	k8xC	a
magické	magický	k2eAgInPc1d1	magický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7136	[number]	k4	7136
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
