<s>
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Bc.	Bc.	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
ministr	ministr	k1gMnSc1
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
ČR	ČR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1998	#num#	k4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Schneider	Schneider	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Černý	Černý	k1gMnSc1
</s>
<s>
místopředseda	místopředseda	k1gMnSc1
KDU-ČSL	KDU-ČSL	k1gMnSc1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2003	#num#	k4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
PČR	PČR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1998	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
</s>
<s>
zastupitel	zastupitel	k1gMnSc1
města	město	k1gNnSc2
Olomouce	Olomouc	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1998	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
od	od	k7c2
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1955	#num#	k4
(	(	kIx(
<g/>
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Olomouc	Olomouc	k1gFnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
VUT	VUT	kA
Brno	Brno	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1955	#num#	k4
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1997	#num#	k4
<g/>
–	–	kIx~
<g/>
1998	#num#	k4
ministr	ministr	k1gMnSc1
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
koncem	koncem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
v	v	k7c6
první	první	k4xOgFnSc6
dekádě	dekáda	k1gFnSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
za	za	k7c4
KDU-ČSL	KDU-ČSL	kA
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
též	též	k6eAd1
dlouholetým	dlouholetý	k2eAgMnSc7d1
aktivním	aktivní	k2eAgMnSc7d1
skautem	skaut	k1gMnSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc3
skautská	skautský	k2eAgFnSc1d1
přezdívka	přezdívka	k1gFnSc1
je	být	k5eAaImIp3nS
Starý	Starý	k1gMnSc1
pes	pes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
vystudoval	vystudovat	k5eAaPmAgMnS
elektrotechnickou	elektrotechnický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
absolvoval	absolvovat	k5eAaPmAgMnS
bakalářské	bakalářský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
na	na	k7c6
právnické	právnický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
KDU-ČSL	KDU-ČSL	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
náměstkem	náměstek	k1gMnSc7
primátora	primátor	k1gMnSc2
Olomouce	Olomouc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1994	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
za	za	k7c2
KDU-ČSL	KDU-ČSL	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opětovně	opětovně	k6eAd1
sem	sem	k6eAd1
o	o	k7c6
zvolení	zvolení	k1gNnSc6
neúspěšně	úspěšně	k6eNd1
pokoušel	pokoušet	k5eAaImAgInS
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
od	od	k7c2
listopadu	listopad	k1gInSc2
1996	#num#	k4
byl	být	k5eAaImAgInS
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1998	#num#	k4
byl	být	k5eAaImAgInS
ministrem	ministr	k1gMnSc7
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
v	v	k7c6
druhé	druhý	k4xOgFnSc6
vládě	vláda	k1gFnSc6
Václava	Václav	k1gMnSc4
Klause	Klaus	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sjezd	sjezd	k1gInSc1
KDU-ČSL	KDU-ČSL	k1gFnSc2
v	v	k7c6
květnu	květen	k1gInSc6
1999	#num#	k4
ho	on	k3xPp3gMnSc4
zvolil	zvolit	k5eAaPmAgMnS
místopředsedou	místopředseda	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Post	post	k1gInSc1
obhájil	obhájit	k5eAaPmAgInS
i	i	k9
po	po	k7c6
sjezdu	sjezd	k1gInSc6
KDU-ČSL	KDU-ČSL	k1gFnSc2
v	v	k7c6
květnu	květen	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1998	#num#	k4
neúspěšně	úspěšně	k6eNd1
kandidoval	kandidovat	k5eAaImAgMnS
do	do	k7c2
horní	horní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
parlamentu	parlament	k1gInSc2
za	za	k7c4
senátní	senátní	k2eAgInSc4d1
obvod	obvod	k1gInSc4
č.	č.	k?
61	#num#	k4
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
26	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
nepostoupil	postoupit	k5eNaPmAgMnS
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
předtím	předtím	k6eAd1
ale	ale	k8xC
byl	být	k5eAaImAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
zvolen	zvolit	k5eAaPmNgInS
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
za	za	k7c4
KDU-ČSL	KDU-ČSL	k1gFnSc4
(	(	kIx(
<g/>
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Severomoravský	severomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
místopředsedou	místopředseda	k1gMnSc7
sněmovního	sněmovní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandát	mandát	k1gInSc1
poslance	poslanec	k1gMnSc4
obhájil	obhájit	k5eAaPmAgInS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
funkčním	funkční	k2eAgNnSc6d1
období	období	k1gNnSc6
působil	působit	k5eAaImAgInS
opět	opět	k6eAd1
jako	jako	k9
místopředseda	místopředseda	k1gMnSc1
výboru	výbor	k1gInSc2
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovu	znovu	k6eAd1
se	se	k3xPyFc4
poslancem	poslanec	k1gMnSc7
stal	stát	k5eAaPmAgMnS
i	i	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opětovně	opětovně	k6eAd1
usedl	usednout	k5eAaPmAgMnS
do	do	k7c2
výboru	výbor	k1gInSc2
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
a	a	k8xC
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
jako	jako	k8xC,k8xS
místopředseda	místopředseda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2007-2010	2007-2010	k4
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
členem	člen	k1gMnSc7
výboru	výbor	k1gInSc2
pro	pro	k7c4
sociální	sociální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
krátce	krátce	k6eAd1
i	i	k9
členem	člen	k1gMnSc7
ústavněprávního	ústavněprávní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
leden	leden	k1gInSc1
2007	#num#	k4
–	–	k?
únor	únor	k1gInSc1
2008	#num#	k4
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
místopředsedou	místopředseda	k1gMnSc7
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
KDU-ČSL	KDU-ČSL	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentu	parlament	k1gInSc6
setrval	setrvat	k5eAaPmAgInS
do	do	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
sobotu	sobota	k1gFnSc4
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
utrpěl	utrpět	k5eAaPmAgMnS
mozkovou	mozkový	k2eAgFnSc4d1
mrtvici	mrtvice	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgInS
ve	v	k7c6
vážném	vážný	k2eAgInSc6d1
stavu	stav	k1gInSc6
hospitalizován	hospitalizován	k2eAgInSc4d1
v	v	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
nerezignoval	rezignovat	k5eNaBmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
poslanecké	poslanecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
se	se	k3xPyFc4
nezúčastňoval	zúčastňovat	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
vážnému	vážný	k2eAgInSc3d1
stavu	stav	k1gInSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
(	(	kIx(
<g/>
zpočátku	zpočátku	k6eAd1
kóma	kóma	k1gNnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ochrnutí	ochrnutí	k1gNnSc1
a	a	k8xC
ztráta	ztráta	k1gFnSc1
paměti	paměť	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
ani	ani	k8xC
rezignace	rezignace	k1gFnSc1
na	na	k7c4
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
schopen	schopen	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
zlepšoval	zlepšovat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.ct24.cz/domaci/13012-ve-skautskych-krojich-prisli-do-snemovny-stary-pes-madam-i-braca/1	http://www.ct24.cz/domaci/13012-ve-skautskych-krojich-prisli-do-snemovny-stary-pes-madam-i-braca/1	k4
2	#num#	k4
Stav	stav	k1gInSc1
poslance	poslanec	k1gMnSc2
Tomáše	Tomáš	k1gMnSc2
Kvapila	Kvapil	k1gMnSc2
se	se	k3xPyFc4
lepší	lepšit	k5eAaImIp3nS
po	po	k7c6
krůčcích	krůček	k1gInPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
olomoucky	olomoucky	k6eAd1
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
<g/>
:	:	kIx,
100	#num#	k4
381	#num#	k4
Osoba	osoba	k1gFnSc1
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
komunalnipolitika	komunalnipolitika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1450	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
179	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1441	#num#	k4
<g/>
-	-	kIx~
<g/>
1442	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://volby.cz/pls/senat/se2111?xjazyk=CZ&	http://volby.cz/pls/senat/se2111?xjazyk=CZ&	k?
Senátní	senátní	k2eAgFnSc2d1
volby	volba	k1gFnSc2
1998	#num#	k4
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
19	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
20.6	20.6	k4
<g/>
.1998	.1998	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://aktualne.centrum.cz/domaci/spolecnost/clanek.phtml?id=627944	http://aktualne.centrum.cz/domaci/spolecnost/clanek.phtml?id=627944	k4
<g/>
↑	↑	k?
Poslanec	poslanec	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
není	být	k5eNaImIp3nS
kvůli	kvůli	k7c3
nemoci	nemoc	k1gFnSc3
schopen	schopen	k2eAgInSc1d1
rezignovat	rezignovat	k5eAaBmF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tyden	tyden	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Václava	Václav	k1gMnSc4
Klause	Klaus	k1gMnSc4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
(	(	kIx(
<g/>
jmenován	jmenován	k2eAgMnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
v	v	k7c4
den	den	k1gInSc4
jmenování	jmenování	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1996	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Kalvoda	Kalvoda	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
spravedlnost	spravedlnost	k1gFnSc4
–	–	k?
demise	demise	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Kočárník	kočárník	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
finance	finance	k1gFnSc2
–	–	k?
odvolán	odvolán	k2eAgInSc1d1
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
Lux	Lux	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
Zieleniec	Zieleniec	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgFnSc2d1
věci	věc	k1gFnSc2
–	–	k?
demise	demise	k1gFnSc1
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Skalický	Skalický	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
–	–	k?
pověřen	pověřen	k2eAgInSc1d1
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
(	(	kIx(
<g/>
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
–	–	k?
demise	demise	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Pilip	Pilip	k1gMnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
–	–	k?
odvolán	odvolán	k2eAgInSc1d1
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
finance	finance	k1gFnPc1
–	–	k?
pověřen	pověřen	k2eAgInSc1d1
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
1997	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jan	Jan	k1gMnSc1
Ruml	Ruml	k1gMnSc1
(	(	kIx(
<g/>
vnitro	vnitro	k1gNnSc1
–	–	k?
demise	demise	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Vodička	Vodička	k1gMnSc1
(	(	kIx(
<g/>
práce	práce	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
věci	věc	k1gFnSc2
–	–	k?
odvolán	odvolán	k2eAgInSc1d1
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
vnitro	vnitro	k1gNnSc1
–	–	k?
pověřen	pověřen	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Říman	Říman	k1gMnSc1
(	(	kIx(
<g/>
doprava	doprava	k1gFnSc1
a	a	k8xC
spoje	spoj	k1gInPc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Schneider	Schneider	k1gMnSc1
(	(	kIx(
<g/>
hospodářství	hospodářství	k1gNnSc1
(	(	kIx(
<g/>
místní	místní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
demise	demise	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jan	Jan	k1gMnSc1
Stráský	Stráský	k1gMnSc1
(	(	kIx(
<g/>
zdravotnictví	zdravotnictví	k1gNnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Talíř	Talíř	k1gMnSc1
(	(	kIx(
<g/>
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Miloslav	Miloslav	k1gMnSc1
Výborný	Výborný	k1gMnSc1
(	(	kIx(
<g/>
obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pavel	Pavel	k1gMnSc1
Bratinka	Bratinka	k1gFnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
,	,	kIx,
ODA	ODA	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
jmenovaní	jmenovaný	k2eAgMnPc1d1
později	pozdě	k6eAd2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
Parkanová	Parkanová	k1gFnSc1
(	(	kIx(
<g/>
spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
ODA	ODA	kA
<g/>
)	)	kIx)
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
(	(	kIx(
<g/>
místní	místní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Kühnl	Kühnl	k1gMnSc1
(	(	kIx(
<g/>
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Gruša	Gruša	k1gMnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šedivý	Šedivý	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgFnSc2d1
věci	věc	k1gFnSc2
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Volák	Volák	k1gMnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
a	a	k8xC
tělovýchova	tělovýchova	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
</s>
<s>
Ministři	ministr	k1gMnPc1
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
20022002	#num#	k4
<g/>
–	–	k?
<g/>
20072008	#num#	k4
<g/>
–	–	k?
<g/>
20142014	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
1997	#num#	k4
<g/>
:	:	kIx,
Jaromír	Jaromír	k1gMnSc1
Schneider	Schneider	k1gMnSc1
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Černý	Černý	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
:	:	kIx,
Jaromír	Jaromír	k1gMnSc1
Císař	Císař	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Lachnit	Lachnita	k1gFnPc2
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Němec	Němec	k1gMnSc1
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Paroubek	Paroubka	k1gFnPc2
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Radko	Radka	k1gFnSc5
Martínek	Martínek	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Gandalovič	Gandalovič	k1gMnSc1
</s>
<s>
2007	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Čunek	Čunek	k1gMnSc1
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Čunek	Čunek	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Cyril	Cyril	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Rostislav	Rostislav	k1gMnSc1
Vondruška	Vondruška	k1gMnSc1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
:	:	kIx,
Kamil	Kamil	k1gMnSc1
Jankovský	Jankovský	k2eAgMnSc1d1
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Lukl	Lukl	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
:	:	kIx,
Věra	Věra	k1gFnSc1
Jourová	Jourová	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
Karla	Karla	k1gFnSc1
Šlechtová	Šlechtová	k1gFnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
2017	#num#	k4
<g/>
:	:	kIx,
Klára	Klára	k1gFnSc1
Dostálová	Dostálová	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20050404010	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5764	#num#	k4
4051	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84233229	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
