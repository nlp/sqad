<p>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Namibijská	Namibijský	k2eAgFnSc1d1	Namibijská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Angolou	Angola	k1gFnSc7	Angola
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
se	s	k7c7	s
Zambií	Zambie	k1gFnSc7	Zambie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Botswanou	Botswana	k1gFnSc7	Botswana
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
ji	on	k3xPp3gFnSc4	on
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
824	[number]	k4	824
292	[number]	k4	292
km2	km2	k4	km2
a	a	k8xC	a
přes	přes	k7c4	přes
2,2	[number]	k4	2,2
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
ob.	ob.	k?	ob.
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Windhoek	Windhoky	k1gFnPc2	Windhoky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
kolem	kolem	k7c2	kolem
320	[number]	k4	320
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Německá	německý	k2eAgFnSc1d1	německá
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Afrika	Afrika	k1gFnSc1	Afrika
přechodně	přechodně	k6eAd1	přechodně
spravovala	spravovat	k5eAaImAgFnS	spravovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
jít	jít	k5eAaImF	jít
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
čtyřmezí	čtyřmeze	k1gFnSc7	čtyřmeze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
území	území	k1gNnSc2	území
byli	být	k5eAaImAgMnP	být
Damarové	Damar	k1gMnPc1	Damar
a	a	k8xC	a
Sanové	Sanus	k1gMnPc1	Sanus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
území	území	k1gNnSc6	území
Namibie	Namibie	k1gFnSc2	Namibie
vylodili	vylodit	k5eAaPmAgMnP	vylodit
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
přišly	přijít	k5eAaPmAgInP	přijít
kmeny	kmen	k1gInPc1	kmen
Ovambů	Ovamb	k1gMnPc2	Ovamb
<g/>
,	,	kIx,	,
Hererů	Herer	k1gMnPc2	Herer
a	a	k8xC	a
Namů	Nam	k1gMnPc2	Nam
<g/>
.	.	kIx.	.
</s>
<s>
Hererové	Hererová	k1gFnPc1	Hererová
s	s	k7c7	s
Namy	Nam	k1gMnPc7	Nam
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
kmeny	kmen	k1gInPc7	kmen
začali	začít	k5eAaPmAgMnP	začít
vést	vést	k5eAaImF	vést
boje	boj	k1gInPc4	boj
o	o	k7c4	o
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
sporů	spor	k1gInPc2	spor
využili	využít	k5eAaPmAgMnP	využít
Němci	Němec	k1gMnPc1	Němec
k	k	k7c3	k
obsazování	obsazování	k1gNnSc3	obsazování
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
získal	získat	k5eAaPmAgMnS	získat
německý	německý	k2eAgMnSc1d1	německý
obchodník	obchodník	k1gMnSc1	obchodník
A.	A.	kA	A.
Lüderitz	Lüderitz	k1gMnSc1	Lüderitz
od	od	k7c2	od
Namů	Nam	k1gMnPc2	Nam
zátoku	zátok	k1gInSc2	zátok
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
<g/>
)	)	kIx)	)
Lüderitz	Lüderitz	k1gMnSc1	Lüderitz
Bay	Bay	k1gMnSc1	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Velrybí	velrybí	k2eAgFnSc4d1	velrybí
zátoku	zátoka	k1gFnSc4	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zakoupeném	zakoupený	k2eAgNnSc6d1	zakoupené
území	území	k1gNnSc6	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Lüderitz	Lüderitza	k1gFnPc2	Lüderitza
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
40	[number]	k4	40
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
protektorát	protektorát	k1gInSc4	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
odboj	odboj	k1gInSc1	odboj
Hotentotských	hotentotský	k2eAgInPc2d1	hotentotský
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
intenzívním	intenzívní	k2eAgNnSc7d1	intenzívní
osidlováním	osidlování	k1gNnSc7	osidlování
<g/>
.	.	kIx.	.
<g/>
Němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
statkáři	statkář	k1gMnPc1	statkář
zabírali	zabírat	k5eAaImAgMnP	zabírat
domorodým	domorodý	k2eAgInPc3d1	domorodý
kmenům	kmen	k1gInPc3	kmen
pastviny	pastvina	k1gFnSc2	pastvina
a	a	k8xC	a
zabavovali	zabavovat	k5eAaImAgMnP	zabavovat
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
proti	proti	k7c3	proti
útlaku	útlak	k1gInSc3	útlak
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
povstaly	povstat	k5eAaPmAgInP	povstat
kmeny	kmen	k1gInPc4	kmen
Hererů	Herer	k1gMnPc2	Herer
a	a	k8xC	a
Namů	Nam	k1gMnPc2	Nam
<g/>
;	;	kIx,	;
povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc1	následek
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
trvající	trvající	k2eAgFnSc4d1	trvající
genocidu	genocida	k1gFnSc4	genocida
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
padlo	padnout	k5eAaPmAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
asi	asi	k9	asi
65	[number]	k4	65
z	z	k7c2	z
80	[number]	k4	80
tisíc	tisíc	k4xCgInPc2	tisíc
Hererů	Herer	k1gInPc2	Herer
a	a	k8xC	a
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
Namů	Nam	k1gMnPc2	Nam
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
museli	muset	k5eAaImAgMnP	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
otrocké	otrocký	k2eAgFnPc4d1	otrocká
práce	práce	k1gFnPc4	práce
pro	pro	k7c4	pro
Německou	německý	k2eAgFnSc4d1	německá
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jich	on	k3xPp3gFnPc2	on
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
veškerá	veškerý	k3xTgFnSc1	veškerý
půda	půda	k1gFnSc1	půda
byla	být	k5eAaImAgFnS	být
císařským	císařský	k2eAgInSc7d1	císařský
dekretem	dekret	k1gInSc7	dekret
zkonfiskována	zkonfiskovat	k5eAaPmNgFnS	zkonfiskovat
a	a	k8xC	a
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
bílé	bílý	k2eAgMnPc4d1	bílý
farmáře	farmář	k1gMnPc4	farmář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
unie	unie	k1gFnSc1	unie
obsadila	obsadit	k5eAaPmAgFnS	obsadit
tuto	tento	k3xDgFnSc4	tento
původně	původně	k6eAd1	původně
německou	německý	k2eAgFnSc4d1	německá
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
správa	správa	k1gFnSc1	správa
území	území	k1gNnSc2	území
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
unie	unie	k1gFnSc1	unie
ani	ani	k8xC	ani
pozdější	pozdní	k2eAgFnSc1d2	pozdější
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
nikdy	nikdy	k6eAd1	nikdy
území	území	k1gNnSc4	území
de	de	k?	de
iure	iure	k6eAd1	iure
nepřipojila	připojit	k5eNaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
byli	být	k5eAaImAgMnP	být
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
v	v	k7c6	v
Jihoafrickém	jihoafrický	k2eAgInSc6d1	jihoafrický
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
existovalo	existovat	k5eAaImAgNnS	existovat
Zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
Národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Německý	německý	k2eAgInSc1d1	německý
svaz	svaz	k1gInSc1	svaz
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
;	;	kIx,	;
domorodému	domorodý	k2eAgNnSc3d1	domorodé
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
aktivní	aktivní	k2eAgFnSc2d1	aktivní
ani	ani	k8xC	ani
pasivní	pasivní	k2eAgFnSc2d1	pasivní
volební	volební	k2eAgFnSc2d1	volební
právo	právo	k1gNnSc1	právo
uděleno	udělit	k5eAaPmNgNnS	udělit
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
úrovni	úroveň	k1gFnSc6	úroveň
hráli	hrát	k5eAaImAgMnP	hrát
jistou	jistý	k2eAgFnSc4d1	jistá
roli	role	k1gFnSc4	role
většinou	většinou	k6eAd1	většinou
dosti	dosti	k6eAd1	dosti
servilní	servilní	k2eAgMnPc1d1	servilní
kmenoví	kmenový	k2eAgMnPc1d1	kmenový
náčelníci	náčelník	k1gMnPc1	náčelník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgMnPc7	který
udržovalo	udržovat	k5eAaImAgNnS	udržovat
velice	velice	k6eAd1	velice
omezené	omezený	k2eAgInPc4d1	omezený
kontakty	kontakt	k1gInPc4	kontakt
jihoafrické	jihoafrický	k2eAgNnSc4d1	jihoafrické
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
pro	pro	k7c4	pro
bantuské	bantuský	k2eAgFnPc4d1	bantuská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
projevy	projev	k1gInPc1	projev
namibijské	namibijský	k2eAgFnSc2d1	namibijská
lidové	lidový	k2eAgFnSc2d1	lidová
iniciativy	iniciativa	k1gFnSc2	iniciativa
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
až	až	k9	až
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
odborových	odborový	k2eAgNnPc2d1	odborové
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
o	o	k7c4	o
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
vznik	vznik	k1gInSc4	vznik
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
hlavně	hlavně	k9	hlavně
jihoafričtí	jihoafrický	k2eAgMnPc1d1	jihoafrický
odboráři	odborář	k1gMnPc1	odborář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
unii	unie	k1gFnSc6	unie
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
formovala	formovat	k5eAaImAgFnS	formovat
Organizace	organizace	k1gFnSc1	organizace
studentů	student	k1gMnPc2	student
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
řad	řada	k1gFnPc2	řada
vyšli	vyjít	k5eAaPmAgMnP	vyjít
pozdější	pozdní	k2eAgMnPc1d2	pozdější
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
organizací	organizace	k1gFnPc2	organizace
Národní	národní	k2eAgFnSc2d1	národní
unie	unie	k1gFnSc2	unie
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
Fanuel	Fanuel	k1gMnSc1	Fanuel
J.	J.	kA	J.
Kozonguizi	Kozonguize	k1gFnSc4	Kozonguize
a	a	k8xC	a
Zedekia	Zedekia	k1gFnSc1	Zedekia
Ngavirue	Ngavirue	k1gFnSc1	Ngavirue
a	a	k8xC	a
Lidová	lidový	k2eAgFnSc1d1	lidová
organizace	organizace	k1gFnSc1	organizace
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
SWAPO	SWAPO	kA	SWAPO
<g/>
)	)	kIx)	)
Mburumba	Mburumba	k1gMnSc1	Mburumba
Kerina	Kerina	k1gMnSc1	Kerina
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zasadili	zasadit	k5eAaPmAgMnP	zasadit
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
politické	politický	k2eAgFnPc1d1	politická
<g/>
,	,	kIx,	,
nacionalistické	nacionalistický	k2eAgFnPc1d1	nacionalistická
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k6eAd1	až
radikální	radikální	k2eAgFnPc4d1	radikální
alternativy	alternativa	k1gFnPc4	alternativa
k	k	k7c3	k
hierarchické	hierarchický	k2eAgFnSc3d1	hierarchická
kmenové	kmenový	k2eAgNnSc1d1	kmenové
<g/>
,	,	kIx,	,
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
nenárodní	národní	k2eNgInSc1d1	nenárodní
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
několikaletém	několikaletý	k2eAgNnSc6d1	několikaleté
úsilí	úsilí	k1gNnSc6	úsilí
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
Západní	západní	k2eAgFnPc1d1	západní
styčné	styčný	k2eAgFnPc1d1	styčná
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
tvořené	tvořený	k2eAgNnSc1d1	tvořené
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
<g/>
,	,	kIx,	,
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Velkou	velká	k1gFnSc7	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
diplomatické	diplomatický	k2eAgFnSc3d1	diplomatická
aktivitě	aktivita	k1gFnSc3	aktivita
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zároveň	zároveň	k6eAd1	zároveň
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
vlivu	vliv	k1gInSc2	vliv
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Kuby	Kuba	k1gFnSc2	Kuba
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
třináctiletém	třináctiletý	k2eAgInSc6d1	třináctiletý
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
boji	boj	k1gInSc6	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
byla	být	k5eAaImAgFnS	být
organizace	organizace	k1gFnSc1	organizace
SWAPO	SWAPO	kA	SWAPO
(	(	kIx(	(
<g/>
Lidová	lidový	k2eAgFnSc1d1	lidová
organizace	organizace	k1gFnSc1	organizace
Jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Samuelem	Samuel	k1gMnSc7	Samuel
Nujomou	Nujomý	k2eAgFnSc4d1	Nujomý
<g/>
.	.	kIx.	.
</s>
<s>
Pří	přít	k5eAaImIp3nS	přít
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
byl	být	k5eAaImAgMnS	být
Sam	Sam	k1gMnSc1	Sam
Nujoma	Nujoma	k1gNnSc4	Nujoma
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
vzešlým	vzešlý	k2eAgInSc7d1	vzešlý
ze	z	k7c2	z
svobodných	svobodný	k2eAgFnPc2d1	svobodná
a	a	k8xC	a
spravedlivých	spravedlivý	k2eAgFnPc2d1	spravedlivá
voleb	volba	k1gFnPc2	volba
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
Namibie	Namibie	k1gFnSc2	Namibie
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
jasné	jasný	k2eAgFnSc2d1	jasná
převahy	převaha	k1gFnSc2	převaha
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
opakovaně	opakovaně	k6eAd1	opakovaně
potvrzované	potvrzovaný	k2eAgFnPc1d1	potvrzovaná
volbami	volba	k1gFnPc7	volba
<g/>
)	)	kIx)	)
SWAPO	SWAPO	kA	SWAPO
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
opozicí	opozice	k1gFnSc7	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Nujoma	Nujoma	k1gFnSc1	Nujoma
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
patrně	patrně	k6eAd1	patrně
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
namibijským	namibijský	k2eAgMnSc7d1	namibijský
politikem	politik	k1gMnSc7	politik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Namibie	Namibie	k1gFnSc2	Namibie
na	na	k7c6	na
území	území	k1gNnSc6	území
tzv.	tzv.	kA	tzv.
Capriviho	Caprivi	k1gMnSc2	Caprivi
pruhu	pruh	k1gInSc6	pruh
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
separatistické	separatistický	k2eAgNnSc4d1	separatistické
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Afriky	Afrika	k1gFnSc2	Afrika
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
376	[number]	k4	376
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Angolou	Angola	k1gFnSc7	Angola
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
233	[number]	k4	233
km	km	kA	km
se	s	k7c7	s
Zambií	Zambie	k1gFnSc7	Zambie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
1	[number]	k4	1
360	[number]	k4	360
km	km	kA	km
s	s	k7c7	s
Botswanou	Botswana	k1gFnSc7	Botswana
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
855	[number]	k4	855
km	km	kA	km
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
1	[number]	k4	1
572	[number]	k4	572
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
825	[number]	k4	825
418	[number]	k4	418
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
10,5	[number]	k4	10,5
<g/>
×	×	k?	×
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
zabírá	zabírat	k5eAaImIp3nS	zabírat
asi	asi	k9	asi
2,7	[number]	k4	2,7
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
pouštěmi	poušť	k1gFnPc7	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
procento	procento	k1gNnSc1	procento
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
zabírá	zabírat	k5eAaImIp3nS	zabírat
Namibská	Namibský	k2eAgFnSc1d1	Namibská
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
řeky	řeka	k1gFnSc2	řeka
Okavango	Okavango	k6eAd1	Okavango
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Capriviho	Caprivi	k1gMnSc4	Caprivi
výběžku	výběžek	k1gInSc2	výběžek
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
část	část	k1gFnSc1	část
řeky	řeka	k1gFnSc2	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Namibská	Namibský	k2eAgFnSc1d1	Namibská
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
pak	pak	k6eAd1	pak
poušť	poušť	k1gFnSc4	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
.	.	kIx.	.
</s>
<s>
Savany	savana	k1gFnPc1	savana
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
45	[number]	k4	45
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Königstein	Königsteina	k1gFnPc2	Königsteina
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
2	[number]	k4	2
573	[number]	k4	573
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
administrativního	administrativní	k2eAgNnSc2d1	administrativní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Namibie	Namibie	k1gFnSc1	Namibie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
14	[number]	k4	14
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kunene	Kunen	k1gMnSc5	Kunen
</s>
</p>
<p>
<s>
Omusati	Omusat	k5eAaPmF	Omusat
</s>
</p>
<p>
<s>
Ošana	Ošana	k6eAd1	Ošana
</s>
</p>
<p>
<s>
Ohangwena	Ohangwena	k1gFnSc1	Ohangwena
</s>
</p>
<p>
<s>
Oikoto	Oikota	k1gFnSc5	Oikota
</s>
</p>
<p>
<s>
Západní	západní	k2eAgMnSc1d1	západní
Kawango	Kawango	k1gMnSc1	Kawango
</s>
</p>
<p>
<s>
Východní	východní	k2eAgMnSc1d1	východní
Kawango	Kawango	k1gMnSc1	Kawango
</s>
</p>
<p>
<s>
Zambezi	Zambezi	k1gNnSc1	Zambezi
</s>
</p>
<p>
<s>
Erongo	Erongo	k6eAd1	Erongo
</s>
</p>
<p>
<s>
Otjozondjupa	Otjozondjupa	k1gFnSc1	Otjozondjupa
</s>
</p>
<p>
<s>
Omaheke	Omaheke	k6eAd1	Omaheke
</s>
</p>
<p>
<s>
Khomas	Khomas	k1gMnSc1	Khomas
</s>
</p>
<p>
<s>
Hardap	Hardap	k1gMnSc1	Hardap
</s>
</p>
<p>
<s>
ǁ	ǁ	k?	ǁ
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Windhoek	Windhoek	k1gInSc1	Windhoek
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
450	[number]	k4	450
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Walvis	Walvis	k1gFnSc1	Walvis
Bay	Bay	k1gFnSc1	Bay
(	(	kIx(	(
<g/>
62	[number]	k4	62
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rundu	runda	k1gFnSc4	runda
(	(	kIx(	(
<g/>
62	[number]	k4	62
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Swakopmund	Swakopmund	k1gInSc1	Swakopmund
(	(	kIx(	(
<g/>
44	[number]	k4	44
725	[number]	k4	725
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oskahati	Oskahat	k5eAaImF	Oskahat
(	(	kIx(	(
<g/>
34	[number]	k4	34
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
africké	africký	k2eAgInPc4d1	africký
poměry	poměr	k1gInPc4	poměr
celkem	celkem	k6eAd1	celkem
slušnou	slušný	k2eAgFnSc4d1	slušná
síť	síť	k1gFnSc4	síť
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
německými	německý	k2eAgMnPc7d1	německý
kolonisty	kolonista	k1gMnPc7	kolonista
a	a	k8xC	a
modernizovanou	modernizovaný	k2eAgFnSc7d1	modernizovaná
jihoafrickými	jihoafrický	k2eAgMnPc7d1	jihoafrický
dělníky	dělník	k1gMnPc7	dělník
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
přístavem	přístav	k1gInSc7	přístav
je	být	k5eAaImIp3nS	být
Walvis	Walvis	k1gFnSc1	Walvis
Bay	Bay	k1gFnSc1	Bay
(	(	kIx(	(
<g/>
Velrybí	velrybí	k2eAgFnSc1d1	velrybí
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
dováží	dovázat	k5eAaPmIp3nP	dovázat
a	a	k8xC	a
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
většina	většina	k1gFnSc1	většina
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
producentem	producent	k1gMnSc7	producent
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
rudy	ruda	k1gFnPc1	ruda
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
6500	[number]	k4	6500
USD	USD	kA	USD
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
africké	africký	k2eAgFnPc4d1	africká
podmínky	podmínka	k1gFnPc4	podmínka
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
provázán	provázán	k2eAgInSc1d1	provázán
s	s	k7c7	s
JAR	jaro	k1gNnPc2	jaro
a	a	k8xC	a
zeměmi	zem	k1gFnPc7	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
namibijský	namibijský	k2eAgInSc1d1	namibijský
dolar	dolar	k1gInSc1	dolar
<g/>
.	.	kIx.	.
7	[number]	k4	7
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgNnSc3d1	původní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kupní	kupní	k2eAgFnSc4d1	kupní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Namibijci	Namibijce	k1gMnPc1	Namibijce
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
příslušníky	příslušník	k1gMnPc7	příslušník
Ovambského	Ovambský	k2eAgInSc2d1	Ovambský
kmene	kmen	k1gInSc2	kmen
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kavangové	Kavangový	k2eAgFnSc2d1	Kavangový
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
Hererové	Hererové	k2eAgFnSc1d1	Hererové
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Damarové	Damarové	k2eAgFnSc1d1	Damarové
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Namové	Namové	k2eAgFnSc1d1	Namové
7	[number]	k4	7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Sanové	Sanové	k2eAgFnSc1d1	Sanové
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
ostatních	ostatní	k2eAgInPc2d1	ostatní
kmenů	kmen	k1gInPc2	kmen
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
smíšeného	smíšený	k2eAgInSc2d1	smíšený
bělošského	bělošský	k2eAgInSc2d1	bělošský
a	a	k8xC	a
černošského	černošský	k2eAgInSc2d1	černošský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
barevní	barevný	k2eAgMnPc1d1	barevný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
6,5	[number]	k4	6,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
40	[number]	k4	40
000	[number]	k4	000
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Běloši	běloch	k1gMnPc1	běloch
tvoří	tvořit	k5eAaImIp3nP	tvořit
7	[number]	k4	7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
Portugalců	Portugalec	k1gMnPc2	Portugalec
<g/>
,	,	kIx,	,
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Britů	Brit	k1gMnPc2	Brit
a	a	k8xC	a
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
tvořili	tvořit	k5eAaImAgMnP	tvořit
běloši	běloch	k1gMnPc1	běloch
14	[number]	k4	14
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.75	.75	k4	.75
%	%	kIx~	%
–	–	k?	–
80	[number]	k4	80
%	%	kIx~	%
Namibijců	Namibijce	k1gMnPc2	Namibijce
je	být	k5eAaImIp3nS	být
gramotných	gramotný	k2eAgFnPc2d1	gramotná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
na	na	k7c4	na
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgFnSc2d1	čtvereční
připadá	připadat	k5eAaPmIp3nS	připadat
jen	jen	k9	jen
2,5	[number]	k4	2,5
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
je	být	k5eAaImIp3nS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
obyvatel	obyvatel	k1gMnPc2	obyvatel
20	[number]	k4	20
let	léto	k1gNnPc2	léto
a	a	k8xC	a
Nambijci	Nambijce	k1gMnPc1	Nambijce
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
průměrně	průměrně	k6eAd1	průměrně
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
pětina	pětina	k1gFnSc1	pětina
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
dospělých	dospělý	k2eAgMnPc2d1	dospělý
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
nositeli	nositel	k1gMnSc3	nositel
onemocnění	onemocnění	k1gNnSc4	onemocnění
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
chybí	chybit	k5eAaPmIp3nS	chybit
dostatek	dostatek	k1gInSc1	dostatek
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
lékaře	lékař	k1gMnSc4	lékař
připadá	připadat	k5eAaPmIp3nS	připadat
asi	asi	k9	asi
9	[number]	k4	9
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
také	také	k6eAd1	také
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
afrikánština	afrikánština	k1gFnSc1	afrikánština
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Namibie	Namibie	k1gFnSc1	Namibie
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
od	od	k7c2	od
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
Namibie	Namibie	k1gFnSc1	Namibie
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
upřednostňování	upřednostňování	k1gNnSc2	upřednostňování
afrikánštiny	afrikánština	k1gFnSc2	afrikánština
či	či	k8xC	či
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
angličtina	angličtina	k1gFnSc1	angličtina
stala	stát	k5eAaPmAgFnS	stát
jediným	jediný	k2eAgInSc7d1	jediný
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Afrikánština	afrikánština	k1gFnSc1	afrikánština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
Oshiwambo	Oshiwamba	k1gFnSc5	Oshiwamba
byly	být	k5eAaImAgFnP	být
uznány	uznán	k2eAgInPc1d1	uznán
jako	jako	k8xS	jako
regionální	regionální	k2eAgInPc1d1	regionální
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
Namibie	Namibie	k1gFnSc1	Namibie
má	mít	k5eAaImIp3nS	mít
jazyk	jazyk	k1gInSc4	jazyk
Oshiwambo	Oshiwamba	k1gFnSc5	Oshiwamba
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
afrikánština	afrikánština	k1gFnSc1	afrikánština
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
komunikačním	komunikační	k2eAgInSc7d1	komunikační
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
u	u	k7c2	u
mladší	mladý	k2eAgFnSc2d2	mladší
generace	generace	k1gFnSc2	generace
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Afrikánština	afrikánština	k1gFnSc1	afrikánština
i	i	k8xC	i
angličtina	angličtina	k1gFnSc1	angličtina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
především	především	k6eAd1	především
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
sféru	sféra	k1gFnSc4	sféra
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgFnPc1d2	menší
komunity	komunita	k1gFnPc1	komunita
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
existují	existovat	k5eAaImIp3nP	existovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
bílých	bílý	k2eAgMnPc2d1	bílý
obyvatel	obyvatel	k1gMnPc2	obyvatel
mluví	mluvit	k5eAaImIp3nS	mluvit
buď	buď	k8xC	buď
německy	německy	k6eAd1	německy
nebo	nebo	k8xC	nebo
afrikánsky	afrikánsky	k6eAd1	afrikánsky
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
100	[number]	k4	100
let	léto	k1gNnPc2	léto
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
německé	německý	k2eAgFnSc2d1	německá
koloniální	koloniální	k2eAgFnSc2d1	koloniální
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
hlavním	hlavní	k2eAgMnSc7d1	hlavní
obchodním	obchodní	k2eAgMnSc7d1	obchodní
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Afrikánsky	afrikánsky	k6eAd1	afrikánsky
hovoří	hovořit	k5eAaImIp3nS	hovořit
60	[number]	k4	60
%	%	kIx~	%
bílé	bílý	k2eAgFnSc2d1	bílá
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
32	[number]	k4	32
%	%	kIx~	%
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
mluví	mluvit	k5eAaImIp3nS	mluvit
7	[number]	k4	7
%	%	kIx~	%
a	a	k8xC	a
portugalsky	portugalsky	k6eAd1	portugalsky
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsky	portugalsky	k6eAd1	portugalsky
mluví	mluvit	k5eAaImIp3nP	mluvit
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
běloši	běloch	k1gMnPc1	běloch
z	z	k7c2	z
Angoly	Angola	k1gFnSc2	Angola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
luteránské	luteránský	k2eAgInPc1d1	luteránský
<g/>
,	,	kIx,	,
19	[number]	k4	19
%	%	kIx~	%
římskokatolické	římskokatolický	k2eAgInPc1d1	římskokatolický
<g/>
,	,	kIx,	,
5	[number]	k4	5
%	%	kIx~	%
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
holandští	holandský	k2eAgMnPc1d1	holandský
<g/>
/	/	kIx~	/
<g/>
afrikánští	afrikánský	k2eAgMnPc1d1	afrikánský
reformovaní	reformovaný	k2eAgMnPc1d1	reformovaný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kalvinisté	kalvinista	k1gMnPc1	kalvinista
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
desetiprocentní	desetiprocentní	k2eAgFnSc6d1	desetiprocentní
míře	míra	k1gFnSc6	míra
zastoupena	zastoupen	k2eAgNnPc1d1	zastoupeno
animistická	animistický	k2eAgNnPc1d1	animistické
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KLÍMA	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
439	[number]	k4	439
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Namibie	Namibie	k1gFnSc2	Namibie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Namibie	Namibie	k1gFnSc2	Namibie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Namibie	Namibie	k1gFnSc2	Namibie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
http://www.embnamibia.at	[url]	k1gInSc1	http://www.embnamibia.at
-	-	kIx~	-
EMBASSY	EMBASSY	kA	EMBASSY
/	/	kIx~	/
PERMANENT	permanent	k1gInSc1	permanent
MISSION	MISSION	kA	MISSION
OF	OF	kA	OF
THE	THE	kA	THE
REPUBLIC	REPUBLIC	kA	REPUBLIC
OF	OF	kA	OF
NAMIBIA	NAMIBIA	kA	NAMIBIA
in	in	k?	in
Austria	Austrium	k1gNnSc2	Austrium
</s>
</p>
<p>
<s>
Namibia	Namibia	k1gFnSc1	Namibia
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Namibia	Namibia	k1gFnSc1	Namibia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Namibia	Namibius	k1gMnSc2	Namibius
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Namibia	Namibia	k1gFnSc1	Namibia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-04	[number]	k4	2011-04-04
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Namibia	Namibia	k1gFnSc1	Namibia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Namibie	Namibie	k1gFnSc1	Namibie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-24	[number]	k4	2011-01-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GREEN	GREEN	kA	GREEN
<g/>
,	,	kIx,	,
Reginald	Reginald	k1gMnSc1	Reginald
Herbold	Herbold	k1gMnSc1	Herbold
<g/>
.	.	kIx.	.
</s>
<s>
Namibia	Namibia	k1gFnSc1	Namibia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geological	Geologicat	k5eAaPmAgInS	Geologicat
expedition	expedition	k1gInSc1	expedition
to	ten	k3xDgNnSc4	ten
Namibia	Namibia	k1gFnSc1	Namibia
in	in	k?	in
March	March	k1gInSc1	March
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
more	mor	k1gInSc5	mor
than	thana	k1gFnPc2	thana
300	[number]	k4	300
photographs	photographsa	k1gFnPc2	photographsa
<g/>
.	.	kIx.	.
</s>
<s>
Climate	Climat	k1gInSc5	Climat
<g/>
,	,	kIx,	,
ice	ice	k?	ice
<g/>
,	,	kIx,	,
water	water	k1gMnSc1	water
and	and	k?	and
landscapes	landscapes	k1gMnSc1	landscapes
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
search	search	k1gMnSc1	search
of	of	k?	of
traces	traces	k1gMnSc1	traces
of	of	k?	of
megatsunami	megatsuna	k1gFnPc7	megatsuna
<g/>
.	.	kIx.	.
</s>
</p>
