<p>
<s>
Tibor	Tibor	k1gMnSc1	Tibor
Kincses	Kincses	k1gMnSc1	Kincses
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
únoraa	února	k1gInSc2	února
1960	[number]	k4	1960
Kecskemét	Kecskemét	k1gInSc1	Kecskemét
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
maďarský	maďarský	k2eAgMnSc1d1	maďarský
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
–	–	k?	–
<g/>
judista	judista	k1gMnSc1	judista
romského	romský	k2eAgInSc2d1	romský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bronzový	bronzový	k2eAgMnSc1d1	bronzový
olympijský	olympijský	k2eAgMnSc1d1	olympijský
medailista	medailista	k1gMnSc1	medailista
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Připravoval	připravovat	k5eAaImAgInS	připravovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Kecskemétu	Kecskeméto	k1gNnSc6	Kecskeméto
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Andráse	András	k1gMnSc2	András
Vargy	Varga	k1gMnSc2	Varga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
ještě	ještě	k9	ještě
junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
turnajovými	turnajový	k2eAgFnPc7d1	turnajová
výsledky	výsledek	k1gInPc1	výsledek
reprezentačního	reprezentační	k2eAgMnSc2d1	reprezentační
trenéra	trenér	k1gMnSc2	trenér
Ference	Ferenc	k1gMnSc2	Ferenc
Moravtze	Moravtze	k1gFnSc2	Moravtze
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
superlehké	superlehký	k2eAgFnSc6d1	superlehká
váze	váha	k1gFnSc6	váha
do	do	k7c2	do
60	[number]	k4	60
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
ve	v	k7c6	v
výborné	výborný	k2eAgFnSc6d1	výborná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
nestačil	stačit	k5eNaBmAgInS	stačit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c6	na
Kubánce	Kubánka	k1gFnSc6	Kubánka
Rafaela	Rafael	k1gMnSc2	Rafael
Rodrígueze	Rodrígueze	k1gFnSc2	Rodrígueze
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
formu	forma	k1gFnSc4	forma
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
pololehké	pololehký	k2eAgFnSc2d1	pololehký
váhy	váha	k1gFnSc2	váha
do	do	k7c2	do
65	[number]	k4	65
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
maďarské	maďarský	k2eAgFnSc6d1	maďarská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
neprosazoval	prosazovat	k5eNaImAgMnS	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
trenérské	trenérský	k2eAgFnSc3d1	trenérská
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Tibora	Tibor	k1gMnSc2	Tibor
Kincsese	Kincsese	k1gFnSc2	Kincsese
na	na	k7c4	na
Judoinside	Judoinsid	k1gInSc5	Judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
