<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Karwina	Karwin	k2eAgFnSc1d1	Karwin
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Karwinr	Karwinr	k1gInSc1	Karwinr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
historického	historický	k2eAgNnSc2d1	historické
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Olši	olše	k1gFnSc6	olše
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Karviná	Karviná	k1gFnSc1	Karviná
projektována	projektovat	k5eAaBmNgFnS	projektovat
jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
česká	český	k2eAgFnSc1d1	Česká
metropole	metropole	k1gFnSc1	metropole
pro	pro	k7c4	pro
120	[number]	k4	120
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
53	[number]	k4	53
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
a	a	k8xC	a
polskou	polský	k2eAgFnSc7d1	polská
menšinou	menšina	k1gFnSc7	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
universitním	universitní	k2eAgMnSc7d1	universitní
<g/>
,	,	kIx,	,
turistickým	turistický	k2eAgMnSc7d1	turistický
<g/>
,	,	kIx,	,
lázeňským	lázeňský	k2eAgMnSc7d1	lázeňský
a	a	k8xC	a
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
centrem	centrum	k1gNnSc7	centrum
česko-polské	českoolský	k2eAgFnSc2d1	česko-polská
aglomerace	aglomerace	k1gFnSc2	aglomerace
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
400	[number]	k4	400
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Olši	olše	k1gFnSc6	olše
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Karviná	Karviná	k1gFnSc1	Karviná
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc4d1	původní
knížecí	knížecí	k2eAgNnSc4d1	knížecí
město	město	k1gNnSc4	město
Fryštát	Fryštát	k1gInSc4	Fryštát
<g/>
.	.	kIx.	.
</s>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
přemístěním	přemístění	k1gNnSc7	přemístění
obyvatel	obyvatel	k1gMnPc2	obyvatel
původní	původní	k2eAgFnSc2d1	původní
hornické	hornický	k2eAgFnSc2d1	hornická
Karvinné	Karvinný	k2eAgNnSc1d1	Karvinné
na	na	k7c4	na
fryštátské	fryštátský	k2eAgNnSc4d1	fryštátské
Horní	horní	k2eAgNnSc4d1	horní
Předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgNnSc7d1	úřední
spojením	spojení	k1gNnSc7	spojení
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
a	a	k8xC	a
připojením	připojení	k1gNnSc7	připojení
Ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
Darkova	Darkův	k2eAgNnSc2d1	Darkovo
a	a	k8xC	a
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
s	s	k7c7	s
názvem	název	k1gInSc7	název
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historickým	historický	k2eAgInSc7d1	historický
základem	základ	k1gInSc7	základ
Fryštátu	Fryštát	k1gInSc2	Fryštát
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
katastru	katastr	k1gInSc6	katastr
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Karviná	Karviná	k1gFnSc1	Karviná
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
Jan	Jan	k1gMnSc1	Jan
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
první	první	k4xOgMnSc1	první
náměstek	náměstek	k1gMnSc1	náměstek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
nahradil	nahradit	k5eAaPmAgInS	nahradit
svého	svůj	k3xOyFgMnSc4	svůj
stranického	stranický	k2eAgMnSc4d1	stranický
kolegu	kolega	k1gMnSc4	kolega
Tomáše	Tomáš	k1gMnSc4	Tomáš
Hanzela	Hanzela	k1gMnSc4	Hanzela
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Význam	význam	k1gInSc1	význam
Karviné	Karviná	k1gFnSc2	Karviná
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Fryštátu	Fryštát	k1gInSc3	Fryštát
<g/>
)	)	kIx)	)
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
předurčovala	předurčovat	k5eAaImAgFnS	předurčovat
její	její	k3xOp3gFnSc1	její
strategicky	strategicky	k6eAd1	strategicky
výhodná	výhodný	k2eAgFnSc1d1	výhodná
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
do	do	k7c2	do
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
učinila	učinit	k5eAaPmAgFnS	učinit
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
celé	celý	k2eAgFnSc2d1	celá
zdejší	zdejší	k2eAgFnSc2d1	zdejší
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
byl	být	k5eAaImAgInS	být
zdejší	zdejší	k2eAgInSc1d1	zdejší
hrad	hrad	k1gInSc1	hrad
sídelním	sídelní	k2eAgNnSc7d1	sídelní
místem	místo	k1gNnSc7	místo
slezských	slezský	k2eAgMnPc2d1	slezský
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
<s>
Prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nález	nález	k1gInSc4	nález
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
železárenského	železárenský	k2eAgInSc2d1	železárenský
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
byl	být	k5eAaImAgInS	být
význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
dán	dát	k5eAaPmNgInS	dát
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
těžbou	těžba	k1gFnSc7	těžba
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
rozvojem	rozvoj	k1gInSc7	rozvoj
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
které	který	k3yIgNnSc1	který
předurčily	předurčit	k5eAaPmAgFnP	předurčit
jak	jak	k6eAd1	jak
stavební	stavební	k2eAgFnPc1d1	stavební
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
demografický	demografický	k2eAgInSc1d1	demografický
vývoj	vývoj	k1gInSc1	vývoj
města	město	k1gNnSc2	město
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
Karviná	Karviná	k1gFnSc1	Karviná
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
sloučením	sloučení	k1gNnSc7	sloučení
města	město	k1gNnSc2	město
Fryštátu	Fryštát	k1gInSc6	Fryštát
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Freistadt	Freistadt	k2eAgInSc1d1	Freistadt
<g/>
;	;	kIx,	;
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Karviná	Karviná	k1gFnSc1	Karviná
1	[number]	k4	1
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
starého	starý	k2eAgInSc2d1	starý
Fryštátu	Fryštát	k1gInSc2	Fryštát
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnSc4d1	původní
Karvinné	Karvinný	k2eAgNnSc4d1	Karvinné
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
intenzivní	intenzivní	k2eAgFnSc3d1	intenzivní
těžbě	těžba	k1gFnSc3	těžba
uhlí	uhlí	k1gNnSc2	uhlí
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
předměstská	předměstský	k2eAgFnSc1d1	předměstská
část	část	k1gFnSc1	část
Karviná-Doly	Karviná-Dola	k1gFnSc2	Karviná-Dola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Darkova	Darkův	k2eAgFnSc1d1	Darkova
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ráje	ráje	k1gFnSc1	ráje
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ještě	ještě	k6eAd1	ještě
byly	být	k5eAaImAgFnP	být
připojeny	připojen	k2eAgFnPc1d1	připojena
Louky	louka	k1gFnPc1	louka
nad	nad	k7c7	nad
Olší	olše	k1gFnSc7	olše
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
fryštátského	fryštátský	k2eAgNnSc2d1	fryštátské
Horního	horní	k2eAgNnSc2d1	horní
Předměstí	předměstí	k1gNnSc2	předměstí
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Severní	severní	k2eAgFnSc1d1	severní
dráha	dráha	k1gFnSc1	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
přivedla	přivést	k5eAaPmAgFnS	přivést
do	do	k7c2	do
Fryštátu	Fryštát	k1gInSc2	Fryštát
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
hraniční	hraniční	k2eAgInPc1d1	hraniční
přechody	přechod	k1gInPc1	přechod
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgMnSc1	jeden
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
osobní	osobní	k2eAgNnPc4d1	osobní
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Karviná-Ráj	Karviná-Ráj	k1gInSc4	Karviná-Ráj
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
předhůří	předhůří	k1gNnSc1	předhůří
Beskyd	Beskyd	k1gInSc1	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Olše	olše	k1gFnSc2	olše
a	a	k8xC	a
od	od	k7c2	od
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Ostravy	Ostrava	k1gFnSc2	Ostrava
je	být	k5eAaImIp3nS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
18	[number]	k4	18
km	km	kA	km
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
vždy	vždy	k6eAd1	vždy
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
oblastem	oblast	k1gFnPc3	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těžebního	těžební	k2eAgMnSc4d1	těžební
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
navazují	navazovat	k5eAaImIp3nP	navazovat
další	další	k2eAgNnPc4d1	další
výrobní	výrobní	k2eAgNnPc4d1	výrobní
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
město	město	k1gNnSc1	město
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
díky	díky	k7c3	díky
lázeňství	lázeňství	k1gNnSc3	lázeňství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
lázně	lázeň	k1gFnPc1	lázeň
Darkov	Darkov	k1gInSc4	Darkov
díky	díky	k7c3	díky
nálezům	nález	k1gInPc3	nález
léčivé	léčivý	k2eAgFnSc2d1	léčivá
jodobromové	jodobromový	k2eAgFnSc2d1	jodobromová
solanky	solanka	k1gFnSc2	solanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
posílen	posílen	k2eAgInSc1d1	posílen
význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
obchodního	obchodní	k2eAgNnSc2d1	obchodní
a	a	k8xC	a
turistického	turistický	k2eAgNnSc2d1	turistické
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Vznikem	vznik	k1gInSc7	vznik
Obchodně	obchodně	k6eAd1	obchodně
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
fakulty	fakulta	k1gFnSc2	fakulta
Slezské	slezský	k2eAgFnSc2d1	Slezská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
se	se	k3xPyFc4	se
Karviná	Karviná	k1gFnSc1	Karviná
stala	stát	k5eAaPmAgFnS	stát
univerzitním	univerzitní	k2eAgNnSc7d1	univerzitní
městem	město	k1gNnSc7	město
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Opavou	Opava	k1gFnSc7	Opava
prvním	první	k4xOgMnSc6	první
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Karviné	Karviná	k1gFnSc2	Karviná
patří	patřit	k5eAaImIp3nS	patřit
ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
oblasti	oblast	k1gFnSc6	oblast
k	k	k7c3	k
místům	místo	k1gNnPc3	místo
s	s	k7c7	s
nejstarším	starý	k2eAgNnSc7d3	nejstarší
osídlením	osídlení	k1gNnSc7	osídlení
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gFnSc6	jehož
existenci	existence	k1gFnSc6	existence
svědčí	svědčit	k5eAaImIp3nP	svědčit
nálezy	nález	k1gInPc1	nález
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
z	z	k7c2	z
období	období	k1gNnSc2	období
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
písemnou	písemný	k2eAgFnSc7d1	písemná
zmínkou	zmínka	k1gFnSc7	zmínka
o	o	k7c6	o
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
osadě	osada	k1gFnSc6	osada
Solca	Solc	k1gInSc2	Solc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1268	[number]	k4	1268
<g/>
.	.	kIx.	.
</s>
<s>
Fryštát	Fryštát	k1gInSc1	Fryštát
však	však	k9	však
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
906	[number]	k4	906
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
a	a	k8xC	a
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1024	[number]	k4	1024
-	-	kIx~	-
1032	[number]	k4	1032
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Měška	Měško	k1gNnSc2	Měško
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Lamberta	Lamberta	k1gFnSc1	Lamberta
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
městských	městský	k2eAgFnPc2d1	městská
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
několikrát	několikrát	k6eAd1	několikrát
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgInS	být
Fryštát	Fryštát	k1gInSc1	Fryštát
přemístěn	přemístit	k5eAaPmNgInS	přemístit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1290	[number]	k4	1290
knížetem	kníže	k1gMnSc7	kníže
Měškem	Měšek	k1gMnSc7	Měšek
I.	I.	kA	I.
Těšínským	Těšínský	k1gMnPc3	Těšínský
<g/>
.	.	kIx.	.
</s>
<s>
Vratislavský	vratislavský	k2eAgInSc1d1	vratislavský
desátek	desátek	k1gInSc1	desátek
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1302	[number]	k4	1302
<g/>
–	–	k?	–
<g/>
1315	[number]	k4	1315
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
původní	původní	k2eAgInPc4d1	původní
názvy	název	k1gInPc4	název
některých	některý	k3yIgFnPc2	některý
zdejších	zdejší	k2eAgFnPc2d1	zdejší
lokalit	lokalita	k1gFnPc2	lokalita
jako	jako	k8xC	jako
Fryenstat	Fryenstat	k1gInSc1	Fryenstat
(	(	kIx(	(
<g/>
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
(	(	kIx(	(
<g/>
Ráj	ráj	k1gInSc1	ráj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Carwina	Carwina	k1gFnSc1	Carwina
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bertholdi	Berthold	k1gMnPc1	Berthold
Villa	Villo	k1gNnSc2	Villo
(	(	kIx(	(
<g/>
Darkov	Darkov	k1gInSc1	Darkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
strategicky	strategicky	k6eAd1	strategicky
výhodné	výhodný	k2eAgFnSc3d1	výhodná
poloze	poloha	k1gFnSc3	poloha
Fryštátu	Fryštát	k1gInSc2	Fryštát
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
do	do	k7c2	do
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
významné	významný	k2eAgNnSc1d1	významné
středisko	středisko	k1gNnSc1	středisko
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1290	[number]	k4	1290
až	až	k8xS	až
1945	[number]	k4	1945
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
slezské	slezský	k2eAgFnSc2d1	Slezská
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Fryštát	Fryštát	k1gInSc1	Fryštát
dal	dát	k5eAaPmAgInS	dát
také	také	k9	také
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
Karviné	Karviná	k1gFnSc2	Karviná
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
začalo	začít	k5eAaPmAgNnS	začít
budovat	budovat	k5eAaImF	budovat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
celé	celý	k2eAgFnSc2d1	celá
zdejší	zdejší	k2eAgFnSc2d1	zdejší
oblasti	oblast	k1gFnSc2	oblast
znamenal	znamenat	k5eAaImAgInS	znamenat
nález	nález	k1gInSc4	nález
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
svým	svůj	k3xOyFgInSc7	svůj
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
potenciálem	potenciál	k1gInSc7	potenciál
významnou	významný	k2eAgFnSc7d1	významná
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgInSc2d1	celý
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
přispěla	přispět	k5eAaPmAgFnS	přispět
výstavba	výstavba	k1gFnSc1	výstavba
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
:	:	kIx,	:
Severní	severní	k2eAgFnSc1d1	severní
dráha	dráha	k1gFnSc1	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
v	v	k7c6	v
Petrovicích	Petrovice	k1gFnPc6	Petrovice
<g/>
,	,	kIx,	,
Košicko-bohumínská	košickoohumínský	k2eAgFnSc1d1	košicko-bohumínská
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
části	část	k1gFnSc6	část
Doly	dol	k1gInPc1	dol
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
i	i	k8xC	i
Karvinská	karvinský	k2eAgFnSc1d1	karvinská
Místní	místní	k2eAgFnSc1d1	místní
dráha	dráha	k1gFnSc1	dráha
KFNB	KFNB	kA	KFNB
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Karviná	Karviná	k1gFnSc1	Karviná
-	-	kIx~	-
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
probíhal	probíhat	k5eAaImAgInS	probíhat
mezi	mezi	k7c7	mezi
nově	nově	k6eAd1	nově
vzniklými	vzniklý	k2eAgInPc7d1	vzniklý
státy	stát	k1gInPc7	stát
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
spor	spora	k1gFnPc2	spora
o	o	k7c4	o
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
padlo	padnout	k5eAaPmAgNnS	padnout
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
československých	československý	k2eAgMnPc2d1	československý
legionářů	legionář	k1gMnPc2	legionář
a	a	k8xC	a
boje	boj	k1gInPc1	boj
byly	být	k5eAaImAgInP	být
zastaveny	zastavit	k5eAaPmNgInP	zastavit
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
jednáním	jednání	k1gNnSc7	jednání
ministra	ministr	k1gMnSc2	ministr
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
s	s	k7c7	s
Poláky	Polák	k1gMnPc7	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
nařčen	nařčen	k2eAgInSc1d1	nařčen
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
nerozumí	rozumět	k5eNaImIp3nS	rozumět
politice	politika	k1gFnSc3	politika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
naše	náš	k3xOp1gFnPc4	náš
legie	legie	k1gFnPc4	legie
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
vojenském	vojenský	k2eAgNnSc6d1	vojenské
střetnutí	střetnutí	k1gNnSc6	střetnutí
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
mezi	mezi	k7c4	mezi
oba	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
arbitráží	arbitráž	k1gFnSc7	arbitráž
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Polsko	Polsko	k1gNnSc1	Polsko
začalo	začít	k5eAaPmAgNnS	začít
vůči	vůči	k7c3	vůči
Československu	Československo	k1gNnSc3	Československo
opět	opět	k6eAd1	opět
vznášet	vznášet	k5eAaImF	vznášet
územní	územní	k2eAgInPc4d1	územní
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
formulovány	formulovat	k5eAaImNgFnP	formulovat
polskou	polský	k2eAgFnSc7d1	polská
vládou	vláda	k1gFnSc7	vláda
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
a	a	k8xC	a
vzneseny	vznesen	k2eAgFnPc1d1	vznesena
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
godesberského	godesberský	k2eAgNnSc2d1	godesberský
memoranda	memorandum	k1gNnSc2	memorandum
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
zaslala	zaslat	k5eAaPmAgFnS	zaslat
československé	československý	k2eAgFnSc3d1	Československá
vládě	vláda	k1gFnSc3	vláda
ultimátum	ultimátum	k1gNnSc1	ultimátum
o	o	k7c6	o
okamžitém	okamžitý	k2eAgNnSc6d1	okamžité
vydání	vydání	k1gNnSc6	vydání
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
polská	polský	k2eAgFnSc1d1	polská
vojska	vojsko	k1gNnSc2	vojsko
obsadila	obsadit	k5eAaPmAgNnP	obsadit
požadovaná	požadovaný	k2eAgNnPc1d1	požadované
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Karviné	Karviná	k1gFnSc2	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
polského	polský	k2eAgInSc2d1	polský
záboru	zábor	k1gInSc2	zábor
však	však	k9	však
netrvalo	trvat	k5eNaImAgNnS	trvat
ani	ani	k8xC	ani
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Polska	Polsko	k1gNnSc2	Polsko
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
oblast	oblast	k1gFnSc1	oblast
obsadil	obsadit	k5eAaPmAgInS	obsadit
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
a	a	k8xC	a
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
s	s	k7c7	s
germánským	germánský	k2eAgInSc7d1	germánský
původem	původ	k1gInSc7	původ
bylo	být	k5eAaImAgNnS	být
přičleněno	přičlenit	k5eAaPmNgNnS	přičlenit
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
měst	město	k1gNnPc2	město
Karvinné	Karvinný	k2eAgFnPc1d1	Karvinná
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
písmena	písmeno	k1gNnPc4	písmeno
N	N	kA	N
v	v	k7c6	v
názvu	název	k1gInSc6	název
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fryštátu	Fryštát	k1gInSc2	Fryštát
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Ráje	rája	k1gFnSc2	rája
a	a	k8xC	a
Darkova	Darkov	k1gInSc2	Darkov
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
územní	územní	k2eAgInSc4d1	územní
celek	celek	k1gInSc4	celek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Karwin-Freistadt	Karwin-Freistadta	k1gFnPc2	Karwin-Freistadta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
města	město	k1gNnSc2	město
3.5	[number]	k4	3.5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
se	se	k3xPyFc4	se
dvojměstí	dvojměstí	k1gNnSc2	dvojměstí
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
výstavbu	výstavba	k1gFnSc4	výstavba
budoucího	budoucí	k2eAgNnSc2d1	budoucí
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ulici	ulice	k1gFnSc4	ulice
Zakladatelská	zakladatelský	k2eAgFnSc1d1	zakladatelská
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
předseda	předseda	k1gMnSc1	předseda
Benešovy	Benešův	k2eAgFnSc2d1	Benešova
vlády	vláda	k1gFnSc2	vláda
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
výkopem	výkop	k1gInSc7	výkop
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
následovalo	následovat	k5eAaImAgNnS	následovat
opětovné	opětovný	k2eAgNnSc1d1	opětovné
sloučení	sloučení	k1gNnSc4	sloučení
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
jeden	jeden	k4xCgInSc1	jeden
územně	územně	k6eAd1	územně
správní	správní	k2eAgInSc1d1	správní
celek	celek	k1gInSc1	celek
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
sloučeného	sloučený	k2eAgNnSc2d1	sloučené
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
velký	velký	k2eAgInSc1d1	velký
stavební	stavební	k2eAgInSc1d1	stavební
rozvoj	rozvoj	k1gInSc1	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Karviná	Karviná	k1gFnSc1	Karviná
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
vývoj	vývoj	k1gInSc1	vývoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
pátou	pátá	k1gFnSc4	pátá
českou	český	k2eAgFnSc7d1	Česká
metropolí	metropol	k1gFnSc7	metropol
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
120.000	[number]	k4	120.000
zastavily	zastavit	k5eAaPmAgFnP	zastavit
až	až	k9	až
listopadové	listopadový	k2eAgFnPc4d1	listopadová
události	událost	k1gFnPc4	událost
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
a	a	k8xC	a
zejména	zejména	k9	zejména
rozpad	rozpad	k1gInSc1	rozpad
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Karviná	Karviná	k1gFnSc1	Karviná
i	i	k9	i
z	z	k7c2	z
východu	východ	k1gInSc2	východ
stala	stát	k5eAaPmAgFnS	stát
téměř	téměř	k6eAd1	téměř
pohraničním	pohraniční	k2eAgNnSc7d1	pohraniční
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
městská	městský	k2eAgFnSc1d1	městská
zástavba	zástavba	k1gFnSc1	zástavba
leží	ležet	k5eAaImIp3nS	ležet
především	především	k9	především
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
Fryštátu	Fryštát	k1gInSc2	Fryštát
(	(	kIx(	(
<g/>
Karviná-město	Karvináěsta	k1gMnSc5	Karviná-města
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obce	obec	k1gFnSc2	obec
Ráje	rája	k1gFnSc2	rája
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metropole	metropole	k1gFnSc1	metropole
Karviná	Karviná	k1gFnSc1	Karviná
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
realizoval	realizovat	k5eAaBmAgInS	realizovat
projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
moderního	moderní	k2eAgNnSc2d1	moderní
centra	centrum	k1gNnSc2	centrum
Karviné	Karviná	k1gFnSc2	Karviná
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
bruselského	bruselský	k2eAgInSc2d1	bruselský
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
pátou	pátá	k1gFnSc4	pátá
českou	český	k2eAgFnSc7d1	Česká
metropolí	metropol	k1gFnSc7	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
silniční	silniční	k2eAgFnSc7d1	silniční
tepnou	tepna	k1gFnSc7	tepna
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
centrální	centrální	k2eAgFnSc1d1	centrální
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tř	tř	kA	tř
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
plnit	plnit	k5eAaImF	plnit
roli	role	k1gFnSc4	role
městského	městský	k2eAgInSc2d1	městský
bulváru	bulvár	k1gInSc2	bulvár
a	a	k8xC	a
díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
lázní	lázeň	k1gFnPc2	lázeň
ve	v	k7c6	v
městě	město	k1gNnSc6	město
navodit	navodit	k5eAaBmF	navodit
atmosféru	atmosféra	k1gFnSc4	atmosféra
pohodového	pohodový	k2eAgNnSc2d1	pohodové
a	a	k8xC	a
příjemného	příjemný	k2eAgNnSc2d1	příjemné
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
centrální	centrální	k2eAgFnSc6d1	centrální
třídě	třída	k1gFnSc6	třída
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
volné	volný	k2eAgFnPc4d1	volná
prostory	prostora	k1gFnPc4	prostora
plné	plný	k2eAgFnSc2d1	plná
městské	městský	k2eAgFnSc2d1	městská
zeleně	zeleň	k1gFnSc2	zeleň
s	s	k7c7	s
dominantními	dominantní	k2eAgFnPc7d1	dominantní
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
třídě	třída	k1gFnSc6	třída
hustý	hustý	k2eAgInSc4d1	hustý
silniční	silniční	k2eAgInSc4d1	silniční
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
láká	lákat	k5eAaImIp3nS	lákat
k	k	k7c3	k
procházce	procházka	k1gFnSc3	procházka
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
její	její	k3xOp3gFnSc6	její
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
sakurové	sakurový	k2eAgFnSc2d1	sakurová
aleje	alej	k1gFnSc2	alej
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vysázena	vysázen	k2eAgFnSc1d1	vysázena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
vzorně	vzorně	k6eAd1	vzorně
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
třída	třída	k1gFnSc1	třída
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
let	léto	k1gNnPc2	léto
dočkala	dočkat	k5eAaPmAgFnS	dočkat
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
necitlivých	citlivý	k2eNgInPc2d1	necitlivý
zásahů	zásah	k1gInPc2	zásah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
městská	městský	k2eAgFnSc1d1	městská
zeleň	zeleň	k1gFnSc1	zeleň
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
do	do	k7c2	do
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
rozměrů	rozměr	k1gInPc2	rozměr
</s>
</p>
<p>
<s>
módní	módní	k2eAgFnSc1d1	módní
dlažba	dlažba	k1gFnSc1	dlažba
byla	být	k5eAaImAgFnS	být
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
nevkusným	vkusný	k2eNgInSc7d1	nevkusný
asfaltem	asfalt	k1gInSc7	asfalt
</s>
</p>
<p>
<s>
dominantní	dominantní	k2eAgFnPc1d1	dominantní
budovy	budova	k1gFnPc1	budova
dostaly	dostat	k5eAaPmAgFnP	dostat
nevhodnou	vhodný	k2eNgFnSc4d1	nevhodná
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
přerostla	přerůst	k5eAaPmAgFnS	přerůst
zeleň	zeleň	k1gFnSc4	zeleň
</s>
</p>
<p>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
centrální	centrální	k2eAgFnSc2d1	centrální
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
křižovatkami	křižovatka	k1gFnPc7	křižovatka
u	u	k7c2	u
VZP	VZP	kA	VZP
a	a	k8xC	a
OD	od	k7c2	od
Prioru	prior	k1gMnSc3	prior
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
dominuje	dominovat	k5eAaImIp3nS	dominovat
vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
skupina	skupina	k1gFnSc1	skupina
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
působí	působit	k5eAaImIp3nS	působit
zanedbaným	zanedbaný	k2eAgInSc7d1	zanedbaný
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
nevkusně	vkusně	k6eNd1	vkusně
situovaných	situovaný	k2eAgFnPc2d1	situovaná
autobusových	autobusový	k2eAgFnPc2d1	autobusová
zastávek	zastávka	k1gFnPc2	zastávka
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dojem	dojem	k1gInSc1	dojem
periferního	periferní	k2eAgNnSc2d1	periferní
místa	místo	k1gNnSc2	místo
než	než	k8xS	než
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
přítomný	přítomný	k2eAgInSc1d1	přítomný
asfalt	asfalt	k1gInSc1	asfalt
a	a	k8xC	a
zeleň	zeleň	k1gFnSc1	zeleň
zastiňující	zastiňující	k2eAgFnSc2d1	zastiňující
dominantní	dominantní	k2eAgFnSc2d1	dominantní
budovy	budova	k1gFnSc2	budova
tento	tento	k3xDgInSc4	tento
dojem	dojem	k1gInSc4	dojem
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
umocňuje	umocňovat	k5eAaImIp3nS	umocňovat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
tento	tento	k3xDgInSc1	tento
centrální	centrální	k2eAgInSc1d1	centrální
prostor	prostor	k1gInSc1	prostor
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
nadále	nadále	k6eAd1	nadále
být	být	k5eAaImF	být
centrálním	centrální	k2eAgInSc7d1	centrální
parkem	park	k1gInSc7	park
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
kazil	kazit	k5eAaImAgInS	kazit
dojem	dojem	k1gInSc1	dojem
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
centrálním	centrální	k2eAgInSc6d1	centrální
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
za	za	k7c7	za
bujnou	bujný	k2eAgFnSc7d1	bujná
zelení	zeleň	k1gFnSc7	zeleň
schována	schován	k2eAgFnSc1d1	schována
řada	řada	k1gFnSc1	řada
architektonických	architektonický	k2eAgInPc2d1	architektonický
slohů	sloh	k1gInPc2	sloh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
vykácení	vykácení	k1gNnSc6	vykácení
části	část	k1gFnSc2	část
zeleně	zeleň	k1gFnSc2	zeleň
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
dominantní	dominantní	k2eAgInPc4d1	dominantní
prvky	prvek	k1gInPc4	prvek
celého	celý	k2eAgNnSc2d1	celé
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
nádhernou	nádherný	k2eAgFnSc4d1	nádherná
prvorepublikovou	prvorepublikový	k2eAgFnSc4d1	prvorepubliková
budovu	budova	k1gFnSc4	budova
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
roh	roh	k1gInSc4	roh
rovněž	rovněž	k9	rovněž
prvorepublikové	prvorepublikový	k2eAgFnPc4d1	prvorepubliková
budovy	budova	k1gFnPc4	budova
Magistrátu	magistrát	k1gInSc2	magistrát
<g/>
,	,	kIx,	,
modernu	moderna	k1gFnSc4	moderna
zastupující	zastupující	k2eAgMnSc1d1	zastupující
OD	od	k7c2	od
Prior	priora	k1gNnPc2	priora
a	a	k8xC	a
restauraci	restaurace	k1gFnSc6	restaurace
s	s	k7c7	s
obytným	obytný	k2eAgInSc7d1	obytný
blokem	blok	k1gInSc7	blok
nebo	nebo	k8xC	nebo
funkcionalistické	funkcionalistický	k2eAgInPc1d1	funkcionalistický
třípodlažní	třípodlažní	k2eAgInPc1d1	třípodlažní
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dominantou	dominanta	k1gFnSc7	dominanta
celého	celý	k2eAgInSc2d1	celý
tohoto	tento	k3xDgInSc2	tento
centrálního	centrální	k2eAgInSc2d1	centrální
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
skupina	skupina	k1gFnSc1	skupina
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Centrálnímu	centrální	k2eAgInSc3d1	centrální
prostoru	prostor	k1gInSc3	prostor
rovněž	rovněž	k9	rovněž
chybí	chybět	k5eAaImIp3nS	chybět
výšková	výškový	k2eAgFnSc1d1	výšková
solitéra	solitéra	k1gFnSc1	solitéra
např.	např.	kA	např.
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hodinové	hodinový	k2eAgFnSc2d1	hodinová
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
pomníku	pomník	k1gInSc2	pomník
či	či	k8xC	či
sochy	socha	k1gFnSc2	socha
nebo	nebo	k8xC	nebo
fontány	fontána	k1gFnSc2	fontána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
řada	řada	k1gFnSc1	řada
urbanistických	urbanistický	k2eAgFnPc2d1	urbanistická
studií	studie	k1gFnPc2	studie
na	na	k7c4	na
dotvoření	dotvoření	k1gNnSc4	dotvoření
městského	městský	k2eAgNnSc2d1	Městské
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
finanční	finanční	k2eAgFnSc4d1	finanční
náročnost	náročnost	k1gFnSc4	náročnost
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
realizována	realizován	k2eAgFnSc1d1	realizována
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
stavět	stavět	k5eAaImF	stavět
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
celé	celý	k2eAgFnSc3d1	celá
třídě	třída	k1gFnSc3	třída
a	a	k8xC	a
především	především	k6eAd1	především
centrálnímu	centrální	k2eAgInSc3d1	centrální
prostoru	prostor	k1gInSc3	prostor
vrátit	vrátit	k5eAaPmF	vrátit
zašlý	zašlý	k2eAgInSc4d1	zašlý
lesk	lesk	k1gInSc4	lesk
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vykácet	vykácet	k5eAaPmF	vykácet
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zastiňuje	zastiňovat	k5eAaImIp3nS	zastiňovat
dominantní	dominantní	k2eAgInPc4d1	dominantní
prvky	prvek	k1gInPc4	prvek
</s>
</p>
<p>
<s>
Dominantním	dominantní	k2eAgFnPc3d1	dominantní
budovám	budova	k1gFnPc3	budova
dát	dát	k5eAaPmF	dát
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
fasádu	fasáda	k1gFnSc4	fasáda
</s>
</p>
<p>
<s>
Veřejný	veřejný	k2eAgInSc1d1	veřejný
prostor	prostor	k1gInSc1	prostor
zbavit	zbavit	k5eAaPmF	zbavit
asfaltu	asfalt	k1gInSc3	asfalt
</s>
</p>
<p>
<s>
Instalovat	instalovat	k5eAaBmF	instalovat
dominantní	dominantní	k2eAgFnSc4d1	dominantní
výškovou	výškový	k2eAgFnSc4d1	výšková
solitéru	solitéra	k1gFnSc4	solitéra
</s>
</p>
<p>
<s>
Rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
autobusové	autobusový	k2eAgFnSc2d1	autobusová
zastávky	zastávka	k1gFnSc2	zastávka
do	do	k7c2	do
vzhledu	vzhled	k1gInSc2	vzhled
hodného	hodný	k2eAgNnSc2d1	hodné
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Instalovat	instalovat	k5eAaBmF	instalovat
informační	informační	k2eAgFnPc4d1	informační
tabule	tabule	k1gFnPc4	tabule
o	o	k7c6	o
zajímavostech	zajímavost	k1gFnPc6	zajímavost
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
mobiliář	mobiliář	k1gInSc1	mobiliář
a	a	k8xC	a
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
veřejné	veřejný	k2eAgNnSc1d1	veřejné
osvětlení	osvětlení	k1gNnSc1	osvětlení
</s>
</p>
<p>
<s>
===	===	k?	===
Pověsti	pověst	k1gFnSc6	pověst
===	===	k?	===
</s>
</p>
<p>
<s>
Zvon	zvon	k1gInSc1	zvon
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
kostele	kostel	k1gInSc6	kostel
měl	mít	k5eAaImAgInS	mít
nejsilnější	silný	k2eAgInSc1d3	nejsilnější
a	a	k8xC	a
nejlahodnější	lahodný	k2eAgInSc1d3	nejlahodnější
hlas	hlas	k1gInSc1	hlas
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prý	prý	k9	prý
do	do	k7c2	do
zvonoviny	zvonovina	k1gFnSc2	zvonovina
při	při	k7c6	při
odlévání	odlévání	k1gNnSc6	odlévání
vhodili	vhodit	k5eAaPmAgMnP	vhodit
díl	díl	k1gInSc4	díl
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
při	při	k7c6	při
zániku	zánik	k1gInSc6	zánik
původní	původní	k2eAgFnSc2d1	původní
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
až	až	k8xS	až
po	po	k7c6	po
létech	léto	k1gNnPc6	léto
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
vyhrabala	vyhrabat	k5eAaPmAgFnS	vyhrabat
svině	svině	k1gFnSc1	svině
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc4	zvon
bylo	být	k5eAaImAgNnS	být
slyšet	slyšet	k5eAaImF	slyšet
až	až	k9	až
do	do	k7c2	do
Těšína	Těšín	k1gInSc2	Těšín
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
hlasu	hlas	k1gInSc6	hlas
naslouchala	naslouchat	k5eAaImAgFnS	naslouchat
těšínská	těšínský	k2eAgFnSc1d1	těšínská
kněžna	kněžna	k1gFnSc1	kněžna
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
oblibou	obliba	k1gFnSc7	obliba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zvon	zvon	k1gInSc4	zvon
koupit	koupit	k5eAaPmF	koupit
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
fryštačtí	fryštacký	k1gMnPc1	fryštacký
jej	on	k3xPp3gMnSc4	on
nechtěli	chtít	k5eNaImAgMnP	chtít
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jí	on	k3xPp3gFnSc3	on
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
musela	muset	k5eAaImAgFnS	muset
vysázet	vysázet	k5eAaPmF	vysázet
tolik	tolik	k4xDc4	tolik
dukátů	dukát	k1gInPc2	dukát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Těšína	Těšín	k1gInSc2	Těšín
až	až	k6eAd1	až
do	do	k7c2	do
Fryštátu	Fryštát	k1gInSc2	Fryštát
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
kněžna	kněžna	k1gFnSc1	kněžna
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jí	on	k3xPp3gFnSc7	on
radní	radní	k1gFnSc7	radní
museli	muset	k5eAaImAgMnP	muset
zvon	zvon	k1gInSc4	zvon
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
si	se	k3xPyFc3	se
zvon	zvon	k1gInSc4	zvon
zavěsila	zavěsit	k5eAaPmAgFnS	zavěsit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvon	zvon	k1gInSc1	zvon
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
ulétl	ulétnout	k5eAaPmAgInS	ulétnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Fryštátu	Fryštát	k1gInSc2	Fryštát
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
vidouc	vidět	k5eAaImSgFnS	vidět
ten	ten	k3xDgMnSc1	ten
zázrak	zázrak	k1gInSc4	zázrak
to	ten	k3xDgNnSc1	ten
vzdala	vzdát	k5eAaPmAgFnS	vzdát
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
Fryštátu	Fryštát	k1gInSc2	Fryštát
jak	jak	k8xC	jak
zvon	zvon	k1gInSc4	zvon
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
okna	okno	k1gNnPc4	okno
kostelní	kostelní	k2eAgFnSc2d1	kostelní
věže	věž	k1gFnSc2	věž
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Těšínu	Těšín	k1gInSc3	Těšín
nechala	nechat	k5eAaPmAgFnS	nechat
zazdít	zazdít	k5eAaPmF	zazdít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
hlas	hlas	k1gInSc1	hlas
zvonu	zvon	k1gInSc2	zvon
nelákal	lákat	k5eNaImAgInS	lákat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
pověstí	pověst	k1gFnPc2	pověst
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Městské	městský	k2eAgFnPc1d1	městská
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historická	historický	k2eAgFnSc1d1	historická
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
založené	založený	k2eAgNnSc1d1	založené
jako	jako	k8xC	jako
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
fryštátské	fryštátský	k2eAgNnSc1d1	fryštátské
jeho	jeho	k3xOp3gFnPc3	jeho
prvním	první	k4xOgMnSc7	první
knížetem	kníže	k1gMnSc7	kníže
Měškem	Měšek	k1gInSc7	Měšek
I.	I.	kA	I.
Piastovským	Piastovský	k2eAgFnPc3d1	Piastovská
roku	rok	k1gInSc2	rok
1290	[number]	k4	1290
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
stavbou	stavba	k1gFnSc7	stavba
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
povýšeného	povýšený	k2eAgNnSc2d1	povýšené
na	na	k7c6	na
Slezské	Slezská	k1gFnSc6	Slezská
knížecí	knížecí	k2eAgNnSc1d1	knížecí
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc4	areál
Fryštátského	Fryštátský	k2eAgInSc2d1	Fryštátský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1288	[number]	k4	1288
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
slezské	slezský	k2eAgFnSc2d1	Slezská
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1327	[number]	k4	1327
-	-	kIx~	-
1573	[number]	k4	1573
byl	být	k5eAaImAgInS	být
lénem	léno	k1gNnSc7	léno
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Přilehlé	přilehlý	k2eAgNnSc1d1	přilehlé
město	město	k1gNnSc1	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
fryštátském	fryštátský	k2eAgNnSc6d1	fryštátské
předměstí	předměstí	k1gNnSc6	předměstí
objeveny	objeven	k2eAgInPc4d1	objeven
jodobromové	jodobromový	k2eAgInPc4d1	jodobromový
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
věhlasné	věhlasný	k2eAgFnPc1d1	věhlasná
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
léčila	léčit	k5eAaImAgFnS	léčit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
seznam	seznam	k1gInSc1	seznam
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
,	,	kIx,	,
Lázně	lázeň	k1gFnPc1	lázeň
Darkov	Darkov	k1gInSc1	Darkov
<g/>
,	,	kIx,	,
Doly	dol	k1gInPc1	dol
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Slezské	slezský	k2eAgNnSc1d1	Slezské
knížecí	knížecí	k2eAgNnSc1d1	knížecí
město	město	k1gNnSc1	město
====	====	k?	====
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
knížecí	knížecí	k2eAgInSc1d1	knížecí
hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
parkem	park	k1gInSc7	park
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
bývalým	bývalý	k2eAgInSc7d1	bývalý
knížecím	knížecí	k2eAgInSc7d1	knížecí
pivovarem	pivovar	k1gInSc7	pivovar
</s>
</p>
<p>
<s>
Knížecí	knížecí	k2eAgInSc1d1	knížecí
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
kašna	kašna	k1gFnSc1	kašna
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
</s>
</p>
<p>
<s>
====	====	k?	====
Staré	Staré	k2eAgFnPc1d1	Staré
lázně	lázeň	k1gFnPc1	lázeň
====	====	k?	====
</s>
</p>
<p>
<s>
Lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Sanatorium	sanatorium	k1gNnSc1	sanatorium
</s>
</p>
<p>
<s>
Společenský	společenský	k2eAgInSc1d1	společenský
dům	dům	k1gInSc1	dům
Kurhaus	Kurhaus	k1gInSc1	Kurhaus
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
historické	historický	k2eAgFnPc1d1	historická
památky	památka	k1gFnPc1	památka
====	====	k?	====
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
z	z	k7c2	z
Alkantary	Alkantara	k1gFnSc2	Alkantara
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
dvůr	dvůr	k1gInSc1	dvůr
hraběte	hrabě	k1gMnSc2	hrabě
Taafa	Taaf	k1gMnSc2	Taaf
</s>
</p>
<p>
<s>
===	===	k?	===
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
===	===	k?	===
</s>
</p>
<p>
<s>
Především	především	k9	především
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vybudován	vybudován	k2eAgInSc4d1	vybudován
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
páté	pátá	k1gFnSc2	pátá
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
největší	veliký	k2eAgFnSc2d3	veliký
slezské	slezský	k2eAgFnSc2d1	Slezská
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
počtu	počet	k1gInSc3	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
120.000	[number]	k4	120.000
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
centrum	centrum	k1gNnSc1	centrum
tvořené	tvořený	k2eAgFnSc2d1	tvořená
třídou	třída	k1gFnSc7	třída
Osvobození	osvobození	k1gNnSc2	osvobození
a	a	k8xC	a
třídou	třída	k1gFnSc7	třída
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
řadou	řada	k1gFnSc7	řada
významných	významný	k2eAgFnPc2d1	významná
stavebních	stavební	k2eAgFnPc2d1	stavební
dominant	dominanta	k1gFnPc2	dominanta
postavených	postavený	k2eAgFnPc2d1	postavená
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
"	"	kIx"	"
<g/>
Bruselský	bruselský	k2eAgInSc1d1	bruselský
styl	styl	k1gInSc1	styl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
budovy	budova	k1gFnPc1	budova
dodnes	dodnes	k6eAd1	dodnes
připomínají	připomínat	k5eAaImIp3nP	připomínat
původní	původní	k2eAgInSc4d1	původní
záměr	záměr	k1gInSc4	záměr
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
Karviné	Karviná	k1gFnSc2	Karviná
novou	nový	k2eAgFnSc4d1	nová
metropoli	metropole	k1gFnSc4	metropole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Oblast	oblast	k1gFnSc1	oblast
třídy	třída	k1gFnSc2	třída
Osvobození	osvobození	k1gNnSc2	osvobození
====	====	k?	====
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Pionýrů	pionýr	k1gInPc2	pionýr
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
tř	tř	kA	tř
Osvobození	osvobození	k1gNnSc1	osvobození
</s>
</p>
<p>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Budovatelů	budovatel	k1gMnPc2	budovatel
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
dům	dům	k1gInSc1	dům
kultury	kultura	k1gFnSc2	kultura
</s>
</p>
<p>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
Kovona	Kovona	k1gFnSc1	Kovona
</s>
</p>
<p>
<s>
Společenské	společenský	k2eAgNnSc1d1	společenské
centrum	centrum	k1gNnSc1	centrum
Kovona	Kovon	k1gMnSc2	Kovon
</s>
</p>
<p>
<s>
====	====	k?	====
Oblast	oblast	k1gFnSc1	oblast
třídy	třída	k1gFnSc2	třída
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
====	====	k?	====
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
PRIOR	priora	k1gNnPc2	priora
</s>
</p>
<p>
<s>
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Karviné	Karviná	k1gFnSc2	Karviná
</s>
</p>
<p>
<s>
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
</s>
</p>
<p>
<s>
Slezská	slezský	k2eAgFnSc1d1	Slezská
universita	universita	k1gFnSc1	universita
</s>
</p>
<p>
<s>
Bytový	bytový	k2eAgInSc1d1	bytový
výškový	výškový	k2eAgInSc1d1	výškový
blok	blok	k1gInSc1	blok
s	s	k7c7	s
tiskárnou	tiskárna	k1gFnSc7	tiskárna
Hutník	hutník	k1gMnSc1	hutník
</s>
</p>
<p>
<s>
Výškové	výškový	k2eAgFnPc1d1	výšková
budovy	budova	k1gFnPc1	budova
Brusel	Brusel	k1gInSc4	Brusel
</s>
</p>
<p>
<s>
Sakurová	Sakurový	k2eAgFnSc1d1	Sakurový
alej	alej	k1gFnSc1	alej
</s>
</p>
<p>
<s>
Nemocnice	nemocnice	k1gFnSc1	nemocnice
V	v	k7c6	v
Ráji	rája	k1gFnSc6	rája
</s>
</p>
<p>
<s>
====	====	k?	====
Oblast	oblast	k1gFnSc1	oblast
Horního	horní	k2eAgInSc2d1	horní
okruhu	okruh	k1gInSc2	okruh
====	====	k?	====
</s>
</p>
<p>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Jurije	Jurije	k1gMnSc2	Jurije
Gagarina	Gagarin	k1gMnSc2	Gagarin
</s>
</p>
<p>
<s>
Bytový	bytový	k2eAgInSc1d1	bytový
výškový	výškový	k2eAgInSc1d1	výškový
blok	blok	k1gInSc1	blok
s	s	k7c7	s
kavárnou	kavárna	k1gFnSc7	kavárna
U	u	k7c2	u
Slunka	slunko	k1gNnSc2	slunko
</s>
</p>
<p>
<s>
Kino	kino	k1gNnSc1	kino
Centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
JAS	Jasy	k1gInPc2	Jasy
</s>
</p>
<p>
<s>
Výškové	výškový	k2eAgInPc1d1	výškový
bloky	blok	k1gInPc1	blok
Sanatoria	sanatorium	k1gNnSc2	sanatorium
Lázní	lázeň	k1gFnPc2	lázeň
Darkov	Darkov	k1gInSc4	Darkov
</s>
</p>
<p>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
na	na	k7c4	na
tř	tř	kA	tř
<g/>
.	.	kIx.	.
Havířská	havířský	k2eAgFnSc1d1	Havířská
</s>
</p>
<p>
<s>
Hala	hala	k1gFnSc1	hala
házené	házená	k1gFnSc2	házená
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
</s>
</p>
<p>
<s>
===	===	k?	===
Industriální	industriální	k2eAgFnPc1d1	industriální
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Kovona	Kovona	k1gFnSc1	Kovona
a	a	k8xC	a
Jäkl	Jäkl	k1gInSc1	Jäkl
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
Karviná	Karviná	k1gFnSc1	Karviná
-	-	kIx~	-
město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
Vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
</s>
</p>
<p>
<s>
Důl	důl	k1gInSc1	důl
Gabriela	Gabriela	k1gFnSc1	Gabriela
</s>
</p>
<p>
<s>
Důl	důl	k1gInSc1	důl
Barbora	Barbora	k1gFnSc1	Barbora
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
Karviná	Karviná	k1gFnSc1	Karviná
-	-	kIx~	-
Louky	louka	k1gFnPc1	louka
nad	nad	k7c7	nad
Olší	olše	k1gFnSc7	olše
</s>
</p>
<p>
<s>
Úzkorozchodné	úzkorozchodný	k2eAgFnPc1d1	úzkorozchodná
tramvaje	tramvaj	k1gFnPc1	tramvaj
</s>
</p>
<p>
<s>
Automobil	automobil	k1gInSc1	automobil
Škoda	škoda	k1gFnSc1	škoda
100	[number]	k4	100
MB	MB	kA	MB
</s>
</p>
<p>
<s>
===	===	k?	===
Památná	památný	k2eAgNnPc4d1	památné
místa	místo	k1gNnPc4	místo
města	město	k1gNnSc2	město
Karviné	Karviná	k1gFnSc2	Karviná
===	===	k?	===
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
z	z	k7c2	z
Alkantary	Alkantara	k1gFnSc2	Alkantara
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Andělů	Anděl	k1gMnPc2	Anděl
strážných	strážný	k2eAgInPc2d1	strážný
</s>
</p>
<p>
<s>
Fryštátský	Fryštátský	k2eAgInSc1d1	Fryštátský
hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	s	k7c7	s
8,5	[number]	k4	8,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
hlásilo	hlásit	k5eAaImAgNnS	hlásit
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
a	a	k8xC	a
8	[number]	k4	8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
polské	polský	k2eAgFnSc3d1	polská
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
bydlících	bydlící	k2eAgMnPc2d1	bydlící
v	v	k7c6	v
panelových	panelový	k2eAgInPc6d1	panelový
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
92	[number]	k4	92
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
6	[number]	k4	6
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
leží	ležet	k5eAaImIp3nS	ležet
9	[number]	k4	9
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Darkov	Darkov	k1gInSc1	Darkov
–	–	k?	–
část	část	k1gFnSc1	část
Lázně	lázeň	k1gFnSc2	lázeň
Darkov	Darkov	k1gInSc1	Darkov
</s>
</p>
<p>
<s>
Karviná-Doly	Karviná-Dola	k1gFnPc1	Karviná-Dola
–	–	k?	–
část	část	k1gFnSc1	část
Doly	dol	k1gInPc4	dol
</s>
</p>
<p>
<s>
Karviná-město	Karvináěsto	k6eAd1	Karviná-město
–	–	k?	–
části	část	k1gFnSc3	část
Fryštát	Fryštát	k1gInSc4	Fryštát
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
Mizerov	Mizerov	k1gInSc1	Mizerov
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
Louky	louka	k1gFnPc1	louka
nad	nad	k7c7	nad
Olší	olše	k1gFnSc7	olše
–	–	k?	–
část	část	k1gFnSc1	část
Louky	louka	k1gFnSc2	louka
</s>
</p>
<p>
<s>
Ráj	ráj	k1gInSc1	ráj
–	–	k?	–
část	část	k1gFnSc1	část
Ráj	ráj	k1gInSc1	ráj
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
u	u	k7c2	u
Karviné	Karviná	k1gFnSc2	Karviná
–	–	k?	–
část	část	k1gFnSc4	část
Staré	Staré	k2eAgFnSc2d1	Staré
MěstoMístní	MěstoMístní	k2eAgFnSc2d1	MěstoMístní
části	část	k1gFnSc2	část
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
číslovány	číslován	k2eAgFnPc1d1	číslována
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
1	[number]	k4	1
–	–	k?	–
Fryštát	Fryštát	k1gInSc1	Fryštát
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Fryštát	Fryštát	k1gInSc1	Fryštát
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
2	[number]	k4	2
–	–	k?	–
Doly	dol	k1gInPc1	dol
–	–	k?	–
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Doly	dol	k1gInPc4	dol
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
3	[number]	k4	3
–	–	k?	–
Darkov	Darkov	k1gInSc1	Darkov
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Lázně	lázeň	k1gFnSc2	lázeň
Darkov	Darkov	k1gInSc1	Darkov
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
4	[number]	k4	4
–	–	k?	–
Ráj	ráj	k1gInSc1	ráj
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Ráj	ráj	k1gInSc1	ráj
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
5	[number]	k4	5
–	–	k?	–
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
6	[number]	k4	6
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
7	[number]	k4	7
–	–	k?	–
Mizerov	Mizerov	k1gInSc1	Mizerov
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Mizerov	Mizerovo	k1gNnPc2	Mizerovo
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
8	[number]	k4	8
–	–	k?	–
Hranice	hranice	k1gFnSc1	hranice
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Hranice	hranice	k1gFnSc2	hranice
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
9	[number]	k4	9
–	–	k?	–
Louky	louka	k1gFnSc2	louka
–	–	k?	–
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Louky	louka	k1gFnSc2	louka
nad	nad	k7c7	nad
Olší	olše	k1gFnSc7	olše
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Karviná	Karviná	k1gFnSc1	Karviná
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
17	[number]	k4	17
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
ze	z	k7c2	z
4	[number]	k4	4
obcí	obec	k1gFnPc2	obec
–	–	k?	–
Dětmarovic	Dětmarovice	k1gFnPc2	Dětmarovice
<g/>
,	,	kIx,	,
Karviné	Karviná	k1gFnSc2	Karviná
<g/>
,	,	kIx,	,
Petrovic	Petrovice	k1gFnPc2	Petrovice
u	u	k7c2	u
Karviné	Karviná	k1gFnSc2	Karviná
a	a	k8xC	a
Stonavy	Stonavy	k?	Stonavy
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
působí	působit	k5eAaImIp3nS	působit
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgNnSc1d1	okresní
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
sportovním	sportovní	k2eAgInSc7d1	sportovní
klubem	klub	k1gInSc7	klub
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
HCB	HCB	kA	HCB
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
házenkářský	házenkářský	k2eAgInSc1d1	házenkářský
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
pravidelně	pravidelně	k6eAd1	pravidelně
sbíral	sbírat	k5eAaImAgMnS	sbírat
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
desetinásobným	desetinásobný	k2eAgMnSc7d1	desetinásobný
mistrem	mistr	k1gMnSc7	mistr
samostatné	samostatný	k2eAgFnSc2d1	samostatná
ČR	ČR	kA	ČR
–	–	k?	–
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
každoročním	každoroční	k2eAgMnSc7d1	každoroční
účastníkem	účastník	k1gMnSc7	účastník
nejprestižnější	prestižní	k2eAgFnSc2d3	nejprestižnější
evropské	evropský	k2eAgFnSc2d1	Evropská
klubové	klubový	k2eAgFnSc2d1	klubová
soutěže	soutěž	k1gFnSc2	soutěž
–	–	k?	–
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
MFK	MFK	kA	MFK
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
hraje	hrát	k5eAaImIp3nS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
českou	český	k2eAgFnSc4d1	Česká
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
již	již	k6eAd1	již
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současný	současný	k2eAgInSc1d1	současný
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Karviná	Karviná	k1gFnSc1	Karviná
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc1	Sokol
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnPc1	jeho
junioři	junior	k1gMnPc1	junior
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
špičku	špička	k1gFnSc4	špička
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
;	;	kIx,	;
muži	muž	k1gMnPc1	muž
však	však	k9	však
hrají	hrát	k5eAaImIp3nP	hrát
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
extralize	extraliga	k1gFnSc6	extraliga
hokejbalu	hokejbal	k1gInSc2	hokejbal
(	(	kIx(	(
<g/>
HbK	HbK	k1gFnSc1	HbK
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
juda	judo	k1gNnSc2	judo
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc2d1	sportovní
střelby	střelba	k1gFnSc2	střelba
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
hraje	hrát	k5eAaImIp3nS	hrát
florbal	florbal	k1gInSc1	florbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
nová	nový	k2eAgFnSc1d1	nová
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplína	disciplína	k1gFnSc1	disciplína
workout	workout	k5eAaPmF	workout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
tak	tak	k6eAd1	tak
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
nová	nový	k2eAgNnPc4d1	nové
workoutová	workoutový	k2eAgNnPc4d1	workoutový
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
trénuje	trénovat	k5eAaImIp3nS	trénovat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
světových	světový	k2eAgMnPc2d1	světový
závodníků	závodník	k1gMnPc2	závodník
Marek	Marek	k1gMnSc1	Marek
Matis	Matis	k1gFnSc2	Matis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc3	ovzduší
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
oblastí	oblast	k1gFnPc2	oblast
patří	patřit	k5eAaImIp3nS	patřit
ovzduší	ovzduší	k1gNnSc1	ovzduší
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
mezi	mezi	k7c4	mezi
nejvíce	hodně	k6eAd3	hodně
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
znečištění	znečištění	k1gNnSc6	znečištění
rakovinotvorným	rakovinotvorný	k2eAgInSc7d1	rakovinotvorný
benzopyrenem	benzopyren	k1gInSc7	benzopyren
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
k	k	k7c3	k
nejzamořenějším	zamořený	k2eAgFnPc3d3	zamořený
oblastem	oblast	k1gFnPc3	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
průměrná	průměrný	k2eAgFnSc1d1	průměrná
celoroční	celoroční	k2eAgFnSc1d1	celoroční
koncentrace	koncentrace	k1gFnSc1	koncentrace
benzo-a-pyrenu	benzoyren	k1gInSc2	benzo-a-pyren
činila	činit	k5eAaImAgFnS	činit
5,1	[number]	k4	5,1
ng	ng	k?	ng
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
prováděného	prováděný	k2eAgInSc2d1	prováděný
společnou	společný	k2eAgFnSc7d1	společná
laboratoří	laboratoř	k1gFnSc7	laboratoř
Ústavu	ústav	k1gInSc2	ústav
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
na	na	k7c6	na
vzorku	vzorek	k1gInSc6	vzorek
23	[number]	k4	23
městských	městský	k2eAgMnPc2d1	městský
strážníků	strážník	k1gMnPc2	strážník
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
jimi	on	k3xPp3gMnPc7	on
vdechovaném	vdechovaný	k2eAgInSc6d1	vdechovaný
vzduchu	vzduch	k1gInSc2	vzduch
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
6,6	[number]	k4	6,6
ng	ng	k?	ng
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
benzo-a-pyrenu	benzoyreno	k1gNnSc6	benzo-a-pyreno
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
u	u	k7c2	u
strážníků	strážník	k1gMnPc2	strážník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
0,8	[number]	k4	0,8
ng	ng	k?	ng
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Následky	následek	k1gInPc1	následek
těžby	těžba	k1gFnSc2	těžba
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
také	také	k9	také
potýkají	potýkat	k5eAaImIp3nP	potýkat
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
těžby	těžba	k1gFnSc2	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
poddolováním	poddolování	k1gNnSc7	poddolování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
narušuje	narušovat	k5eAaImIp3nS	narušovat
statiku	statika	k1gFnSc4	statika
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
řada	řada	k1gFnSc1	řada
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
a	a	k8xC	a
stržena	strhnout	k5eAaPmNgFnS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k9	tak
těžbě	těžba	k1gFnSc3	těžba
padla	padnout	k5eAaImAgFnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
původní	původní	k2eAgFnSc1d1	původní
Karviná	Karviná	k1gFnSc1	Karviná
včetně	včetně	k7c2	včetně
řady	řada	k1gFnSc2	řada
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
likvidaci	likvidace	k1gFnSc4	likvidace
dalších	další	k2eAgFnPc2d1	další
příměstských	příměstský	k2eAgFnPc2d1	příměstská
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Zlikvidována	zlikvidován	k2eAgFnSc1d1	zlikvidována
a	a	k8xC	a
poddolována	poddolován	k2eAgFnSc1d1	poddolována
byla	být	k5eAaImAgFnS	být
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
Louky	louka	k1gFnSc2	louka
nebo	nebo	k8xC	nebo
Darkov	Darkov	k1gInSc1	Darkov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
těžbě	těžba	k1gFnSc6	těžba
pod	pod	k7c7	pod
čtvrtí	čtvrt	k1gFnSc7	čtvrt
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
nebo	nebo	k8xC	nebo
Starý	starý	k2eAgInSc1d1	starý
Ráj	ráj	k1gInSc1	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
však	však	k9	však
uhelné	uhelný	k2eAgFnSc2d1	uhelná
sloje	sloj	k1gFnSc2	sloj
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
převážnou	převážný	k2eAgFnSc7d1	převážná
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
budoucí	budoucí	k2eAgInSc1d1	budoucí
rozvoj	rozvoj	k1gInSc1	rozvoj
značně	značně	k6eAd1	značně
limitován	limitovat	k5eAaBmNgInS	limitovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ekonomika	ekonomika	k1gFnSc1	ekonomika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
patřila	patřit	k5eAaImAgFnS	patřit
Karviná	Karviná	k1gFnSc1	Karviná
k	k	k7c3	k
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
rozvíjejícím	rozvíjející	k2eAgFnPc3d1	rozvíjející
průmyslovým	průmyslový	k2eAgFnPc3d1	průmyslová
městům	město	k1gNnPc3	město
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
rostl	růst	k5eAaImAgInS	růst
současně	současně	k6eAd1	současně
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
bezmála	bezmála	k6eAd1	bezmála
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
prosperity	prosperita	k1gFnSc2	prosperita
byla	být	k5eAaImAgFnS	být
těžba	těžba	k1gFnSc1	těžba
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
kamenného	kamenný	k2eAgNnSc2d1	kamenné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
rozvoje	rozvoj	k1gInSc2	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
postupně	postupně	k6eAd1	postupně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
slučováním	slučování	k1gNnSc7	slučování
někdejších	někdejší	k2eAgFnPc2d1	někdejší
menších	malý	k2eAgFnPc2d2	menší
šachet	šachta	k1gFnPc2	šachta
několik	několik	k4yIc4	několik
velkodolů	velkodůl	k1gInPc2	velkodůl
–	–	k?	–
Důl	důl	k1gInSc1	důl
ČSA	ČSA	kA	ČSA
<g/>
,	,	kIx,	,
Důl	důl	k1gInSc1	důl
Darkov	Darkov	k1gInSc1	Darkov
<g/>
,	,	kIx,	,
Důl	důl	k1gInSc1	důl
ČSM	ČSM	kA	ČSM
<g/>
.	.	kIx.	.
</s>
<s>
Obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vývoji	vývoj	k1gInSc6	vývoj
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
útlumem	útlum	k1gInSc7	útlum
těžby	těžba	k1gFnSc2	těžba
(	(	kIx(	(
<g/>
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
části	část	k1gFnSc6	část
revíru	revír	k1gInSc2	revír
byla	být	k5eAaImAgFnS	být
těžba	těžba	k1gFnSc1	těžba
úplně	úplně	k6eAd1	úplně
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
o	o	k7c4	o
bezmála	bezmála	k6eAd1	bezmála
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
naopak	naopak	k6eAd1	naopak
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
uhelných	uhelný	k2eAgInPc2d1	uhelný
dolů	dol	k1gInPc2	dol
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Sídlila	sídlit	k5eAaImAgFnS	sídlit
zde	zde	k6eAd1	zde
Kovona	Kovona	k1gFnSc1	Kovona
Karviná	Karviná	k1gFnSc1	Karviná
–	–	k?	–
někdejší	někdejší	k2eAgMnSc1d1	někdejší
největší	veliký	k2eAgMnSc1d3	veliký
producent	producent	k1gMnSc1	producent
bytových	bytový	k2eAgNnPc2d1	bytové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
sklolaminátových	sklolaminátový	k2eAgInPc2d1	sklolaminátový
materiálů	materiál	k1gInPc2	materiál
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Nové	Nové	k2eAgFnSc2d1	Nové
Pole	pole	k1gFnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
výrobní	výrobní	k2eAgFnPc4d1	výrobní
haly	hala	k1gFnPc4	hala
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
firmy	firma	k1gFnPc1	firma
jako	jako	k8xS	jako
Robe	Robe	k?	Robe
lighting	lighting	k1gInSc1	lighting
<g/>
,	,	kIx,	,
Shimano	Shimana	k1gFnSc5	Shimana
<g/>
,	,	kIx,	,
Sejong	Sejong	k1gMnSc1	Sejong
<g/>
,	,	kIx,	,
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
Mölnlycke	Mölnlycke	k1gFnSc1	Mölnlycke
Health	Healtha	k1gFnPc2	Healtha
Care	car	k1gMnSc5	car
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plán	plán	k1gInSc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
spalovny	spalovna	k1gFnSc2	spalovna
odpadů	odpad	k1gInPc2	odpad
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Karviná-Doly	Karviná-Dola	k1gFnSc2	Karviná-Dola
naplánoval	naplánovat	k5eAaBmAgInS	naplánovat
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
výstavbu	výstavba	k1gFnSc4	výstavba
velkokapacitní	velkokapacitní	k2eAgFnSc2d1	velkokapacitní
spalovny	spalovna	k1gFnSc2	spalovna
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ze	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
mohlo	moct	k5eAaImAgNnS	moct
ročně	ročně	k6eAd1	ročně
svážet	svážet	k5eAaImF	svážet
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
obávají	obávat	k5eAaImIp3nP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
spalovna	spalovna	k1gFnSc1	spalovna
odpadů	odpad	k1gInPc2	odpad
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
kvalitu	kvalita	k1gFnSc4	kvalita
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
značně	značně	k6eAd1	značně
znečištěného	znečištěný	k2eAgNnSc2d1	znečištěné
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Karviná	Karviná	k1gFnSc1	Karviná
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
univerzitním	univerzitní	k2eAgNnSc7d1	univerzitní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Obchodně	obchodně	k6eAd1	obchodně
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
fakulta	fakulta	k1gFnSc1	fakulta
Slezské	slezský	k2eAgFnSc2d1	Slezská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
aj.	aj.	kA	aj.
Kromě	kromě	k7c2	kromě
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
také	také	k6eAd1	také
základní	základní	k2eAgFnPc4d1	základní
a	a	k8xC	a
mateřské	mateřský	k2eAgFnPc4d1	mateřská
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Ševčík	Ševčík	k1gMnSc1	Ševčík
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
-	-	kIx~	-
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wacław	Wacław	k?	Wacław
Olszak	Olszak	k1gInSc1	Olszak
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
polský	polský	k2eAgMnSc1d1	polský
starosta	starosta	k1gMnSc1	starosta
Karviné	Karviná	k1gFnSc2	Karviná
</s>
</p>
<p>
<s>
Gustaw	Gustaw	k?	Gustaw
Morcinek	Morcinek	k1gInSc1	Morcinek
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Franciszek	Franciszek	k1gInSc1	Franciszek
Świder	Świder	k1gInSc1	Świder
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Żebrok	Żebrok	k1gInSc1	Żebrok
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Józef	Józef	k1gMnSc1	Józef
Ondrusz	Ondrusz	k1gMnSc1	Ondrusz
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
polský	polský	k2eAgMnSc1d1	polský
etnograf	etnograf	k1gMnSc1	etnograf
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
</s>
</p>
<p>
<s>
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Biolek	Biolek	k1gMnSc1	Biolek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
televizní	televizní	k2eAgMnSc1d1	televizní
kuchař	kuchař	k1gMnSc1	kuchař
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
</s>
</p>
<p>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Przeczek	Przeczka	k1gFnPc2	Przeczka
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Ďuriš	Ďuriš	k1gMnSc1	Ďuriš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Voznica	Voznica	k1gMnSc1	Voznica
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Bokroš	Bokroš	k1gMnSc1	Bokroš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
Pavla	Pavla	k1gFnSc1	Pavla
Tomicová	Tomicová	k1gFnSc1	Tomicová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Fukala	Fukal	k1gMnSc2	Fukal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Draisaitl	Draisaitl	k1gMnSc1	Draisaitl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Vejmělková	Vejmělková	k1gFnSc1	Vejmělková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Šárka	Šárka	k1gFnSc1	Šárka
Kašpárková	kašpárkový	k2eAgFnSc1d1	Kašpárková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
trojskokanka	trojskokanka	k1gFnSc1	trojskokanka
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Kurfürstová	Kurfürstová	k1gFnSc1	Kurfürstová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžařka	lyžařka	k1gFnSc1	lyžařka
</s>
</p>
<p>
<s>
Adéla	Adéla	k1gFnSc1	Adéla
Kawka	Kawka	k1gFnSc1	Kawka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronomka	astronomka	k1gFnSc1	astronomka
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
</s>
</p>
<p>
<s>
Petra	Petra	k1gFnSc1	Petra
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
topmodelka	topmodelka	k1gFnSc1	topmodelka
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bába	bába	k1gFnSc1	bába
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
</s>
</p>
<p>
<s>
Denisa	Denisa	k1gFnSc1	Denisa
Rosolová	Rosolová	k1gFnSc1	Rosolová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
sprinterka	sprinterka	k1gFnSc1	sprinterka
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Doležalová	Doležalová	k1gFnSc1	Doležalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
blogerka	blogerka	k1gFnSc1	blogerka
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Jandová	Jandová	k1gFnSc1	Jandová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Henryk	Henryk	k6eAd1	Henryk
Flame	Flam	k1gInSc5	Flam
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
–	–	k?	–
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
protifašistických	protifašistický	k2eAgFnPc2d1	protifašistická
a	a	k8xC	a
protikomunistických	protikomunistický	k2eAgFnPc2d1	protikomunistická
bojových	bojový	k2eAgFnPc2d1	bojová
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
území	území	k1gNnSc6	území
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leon	Leona	k1gFnPc2	Leona
Derlich	Derlicha	k1gFnPc2	Derlicha
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
–	–	k?	–
etnograf	etnograf	k1gMnSc1	etnograf
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
lingvista	lingvista	k1gMnSc1	lingvista
a	a	k8xC	a
dialektolog	dialektolog	k1gMnSc1	dialektolog
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
hnutí	hnutí	k1gNnSc2	hnutí
odporu	odpor	k1gInSc2	odpor
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Jastrzębie-Zdrój	Jastrzębie-Zdrój	k1gFnSc1	Jastrzębie-Zdrój
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Jaworzno	Jaworzna	k1gFnSc5	Jaworzna
<g/>
,	,	kIx,	,
Polsko	Polska	k1gFnSc5	Polska
</s>
</p>
<p>
<s>
Rybnik	Rybnik	k1gInSc1	Rybnik
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Kaili	Kaile	k1gFnSc4	Kaile
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
</p>
<p>
<s>
Wodzisław	Wodzisław	k?	Wodzisław
Śląski	Śląski	k1gNnSc1	Śląski
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WENIG	WENIG	kA	WENIG
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
pověsti	pověst	k1gFnPc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LINDNER	LINDNER	kA	LINDNER
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
slezského	slezský	k2eAgInSc2d1	slezský
knížecího	knížecí	k2eAgInSc2d1	knížecí
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
zámku	zámek	k1gInSc2	zámek
Fryštát	Fryštát	k1gInSc4	Fryštát
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
</s>
</p>
<p>
<s>
Důl	důl	k1gInSc1	důl
Jan	Jan	k1gMnSc1	Jan
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
činný	činný	k2eAgInSc4d1	činný
důl	důl	k1gInSc4	důl
v	v	k7c6	v
Karviné-Dolech	Karviné-Dol	k1gInPc6	Karviné-Dol
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karviná	Karviná	k1gFnSc1	Karviná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Karviň	Karviň	k1gFnSc1	Karviň
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Karviná	Karviná	k1gFnSc1	Karviná
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Karviná	Karviná	k1gFnSc1	Karviná
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
karvinském	karvinský	k2eAgInSc6d1	karvinský
hradu	hrad	k1gInSc6	hrad
Fryštát	Fryštát	k1gInSc1	Fryštát
</s>
</p>
