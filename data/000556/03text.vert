<s>
Prosinec	prosinec	k1gInSc1	prosinec
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Prosinec	prosinec	k1gInSc1	prosinec
začíná	začínat	k5eAaImIp3nS	začínat
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xC	jako
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
nebo	nebo	k8xC	nebo
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgInSc1d1	zimní
slunovrat	slunovrat	k1gInSc1	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
obratníkem	obratník	k1gInSc7	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
nejkratší	krátký	k2eAgInSc1d3	nejkratší
den	den	k1gInSc1	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
byl	být	k5eAaImAgInS	být
prosinec	prosinec	k1gInSc1	prosinec
desátým	desátý	k4xOgInSc7	desátý
měsícem	měsíc	k1gInSc7	měsíc
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
december	decembra	k1gFnPc2	decembra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
decem	decem	k6eAd1	decem
=	=	kIx~	=
deset	deset	k4xCc1	deset
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
153	[number]	k4	153
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
den	den	k1gInSc4	den
připadá	připadat	k5eAaImIp3nS	připadat
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
poslední	poslední	k2eAgFnSc4d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
nebo	nebo	k8xC	nebo
první	první	k4xOgFnSc3	první
neděli	neděle	k1gFnSc3	neděle
prosince	prosinec	k1gInSc2	prosinec
advent	advent	k1gInSc1	advent
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
pondělí	pondělí	k1gNnSc2	pondělí
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
připočítány	připočítat	k5eAaPmNgInP	připočítat
tyto	tento	k3xDgInPc1	tento
dny	den	k1gInPc1	den
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
týdnu	týden	k1gInSc3	týden
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
končí	končit	k5eAaImIp3nS	končit
poslední	poslední	k2eAgInSc4d1	poslední
týden	týden	k1gInSc4	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
společně	společně	k6eAd1	společně
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
nedělí	neděle	k1gFnSc7	neděle
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
rok	rok	k1gInSc1	rok
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
52	[number]	k4	52
kalendářních	kalendářní	k2eAgInPc2d1	kalendářní
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
prosiněti	prosinět	k5eAaImF	prosinět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
problesknout	problesknout	k5eAaPmF	problesknout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slunce	slunce	k1gNnSc4	slunce
už	už	k6eAd1	už
jen	jen	k9	jen
občas	občas	k6eAd1	občas
prosvitne	prosvitnout	k5eAaPmIp3nS	prosvitnout
(	(	kIx(	(
<g/>
probleskne	problesknout	k5eAaPmIp3nS	problesknout
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
mraky	mrak	k1gInPc7	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ale	ale	k8xC	ale
být	být	k5eAaImF	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
též	též	k9	též
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
siný	siný	k2eAgInSc4d1	siný
(	(	kIx(	(
<g/>
modravý	modravý	k2eAgInSc4d1	modravý
<g/>
,	,	kIx,	,
šedivý	šedivý	k2eAgInSc4d1	šedivý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
časem	čas	k1gInSc7	čas
zabijaček	zabijačka	k1gFnPc2	zabijačka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
odvození	odvození	k1gNnSc1	odvození
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
prosit	prosit	k5eAaImF	prosit
<g/>
"	"	kIx"	"
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vánočním	vánoční	k2eAgNnSc7d1	vánoční
koledováním	koledování	k1gNnSc7	koledování
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
Silvestr	Silvestr	k1gMnSc1	Silvestr
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prosinec	prosinec	k1gInSc1	prosinec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
