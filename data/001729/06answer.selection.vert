<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Syda	Syda	k1gMnSc1	Syda
Barretta	Barretta	k1gMnSc1	Barretta
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
úspěchu	úspěch	k1gInSc2	úspěch
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
undergroundových	undergroundový	k2eAgFnPc2d1	undergroundová
kapel	kapela	k1gFnPc2	kapela
hrající	hrající	k2eAgInSc1d1	hrající
psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
