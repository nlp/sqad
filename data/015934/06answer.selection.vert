<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
plavidla	plavidlo	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
uváděna	uváděn	k2eAgFnSc1d1
2,2	2,2	k4
t	t	k?
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
9	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Obsaditelnost	obsaditelnost	k1gFnSc1
člunu	člun	k1gInSc2
byla	být	k5eAaImAgFnS
17	#num#	k4
osob	osoba	k1gFnPc2
plus	plus	k1gInSc4
vůdce	vůdce	k1gMnSc4
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>