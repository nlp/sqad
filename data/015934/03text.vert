<s>
Svratka	Svratka	k1gFnSc1
(	(	kIx(
<g/>
člun	člun	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SvratkaZákladní	SvratkaZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Typ	typ	k1gInSc1
</s>
<s>
osobní	osobní	k2eAgInSc1d1
motorový	motorový	k2eAgInSc1d1
člun	člun	k1gInSc1
Majitel	majitel	k1gMnSc1
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
Domovský	domovský	k2eAgInSc4d1
přístav	přístav	k1gInSc4
</s>
<s>
provoz	provoz	k1gInSc1
<g/>
:	:	kIx,
Brněnská	brněnský	k2eAgFnSc1d1
přehrada	přehrada	k1gFnSc1
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
Technická	technický	k2eAgFnSc1d1
data	datum	k1gNnPc4
Délka	délka	k1gFnSc1
</s>
<s>
6,65	6,65	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
1,84	1,84	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
benzínový	benzínový	k2eAgInSc1d1
motor	motor	k1gInSc1
<g/>
13	#num#	k4
kW	kW	kA
Rychlost	rychlost	k1gFnSc1
</s>
<s>
9	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
18	#num#	k4
osob	osoba	k1gFnPc2
</s>
<s>
Člun	člun	k1gInSc1
Svratka	Svratka	k1gFnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
plavidel	plavidlo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
využíval	využívat	k5eAaPmAgInS,k5eAaImAgInS
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
pro	pro	k7c4
provoz	provoz	k1gInSc4
lodní	lodní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
provozu	provoz	k1gInSc6
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
zřejmě	zřejmě	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
zahájení	zahájení	k1gNnSc4
lodní	lodní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
kromě	kromě	k7c2
dvou	dva	k4xCgFnPc2
německých	německý	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Brno	Brno	k1gNnSc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
také	také	k6eAd1
malý	malý	k2eAgInSc1d1
motorový	motorový	k2eAgInSc1d1
člun	člun	k1gInSc1
Svratka	Svratka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
fotografií	fotografia	k1gFnPc2
a	a	k8xC
vzpomínek	vzpomínka	k1gFnPc2
pamětníků	pamětník	k1gMnPc2
o	o	k7c6
něm	on	k3xPp3gNnSc6
však	však	k9
neexistují	existovat	k5eNaImIp3nP
téměř	téměř	k6eAd1
žádné	žádný	k3yNgInPc1
záznamy	záznam	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
kusých	kusý	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vyčíst	vyčíst	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Svratka	Svratka	k1gFnSc1
byla	být	k5eAaImAgFnS
možná	možná	k9
totožná	totožný	k2eAgFnSc1d1
s	s	k7c7
parníčkem	parníček	k1gInSc7
Carolus	Carolus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jezdil	jezdit	k5eAaImAgMnS
na	na	k7c6
Svratce	Svratka	k1gFnSc6
od	od	k7c2
Jundrova	Jundrov	k1gInSc2
do	do	k7c2
Kamenného	kamenný	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
kolem	kolem	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Člun	člun	k1gInSc4
Svratka	Svratka	k1gFnSc1
byl	být	k5eAaImAgInS
určen	určen	k2eAgMnSc1d1
k	k	k7c3
přepravě	přeprava	k1gFnSc3
malých	malý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
velké	velký	k2eAgFnSc3d1
poruchovosti	poruchovost	k1gFnSc3
byl	být	k5eAaImAgInS
ale	ale	k9
později	pozdě	k6eAd2
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
již	již	k6eAd1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
služební	služební	k2eAgFnSc1d1
k	k	k7c3
ošetřování	ošetřování	k1gNnSc3
a	a	k8xC
opravě	oprava	k1gFnSc3
přístavišť	přístaviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
byl	být	k5eAaImAgInS
zřejmě	zřejmě	k6eAd1
člun	člun	k1gInSc1
vyřazen	vyřazen	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
o	o	k7c6
něm	on	k3xPp3gMnSc6
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Trup	trup	k1gInSc1
člunu	člun	k1gInSc2
<g/>
,	,	kIx,
dlouhého	dlouhý	k2eAgNnSc2d1
6,65	6,65	k4
m	m	kA
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
ze	z	k7c2
želeného	želený	k2eAgInSc2d1
plechu	plech	k1gInSc2
o	o	k7c6
síle	síla	k1gFnSc6
2	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plavidlo	plavidlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vybaveno	vybavit	k5eAaPmNgNnS
benzínovým	benzínový	k2eAgInSc7d1
motorem	motor	k1gInSc7
o	o	k7c6
výkonu	výkon	k1gInSc6
13	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpozději	pozdě	k6eAd3
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1952	#num#	k4
byl	být	k5eAaImAgInS
člun	člun	k1gInSc1
přestavěn	přestavěn	k2eAgInSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
jeho	jeho	k3xOp3gInSc1
trup	trup	k1gInSc1
a	a	k8xC
také	také	k9
obdržel	obdržet	k5eAaPmAgMnS
elektrický	elektrický	k2eAgInSc4d1
pohon	pohon	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
ostatní	ostatní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
plavidla	plavidlo	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
uváděna	uváděn	k2eAgFnSc1d1
2,2	2,2	k4
t	t	k?
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
9	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Obsaditelnost	obsaditelnost	k1gFnSc1
člunu	člun	k1gInSc2
byla	být	k5eAaImAgFnS
17	#num#	k4
osob	osoba	k1gFnPc2
plus	plus	k1gInSc4
vůdce	vůdce	k1gMnSc4
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kolektiv	kolektiv	k1gInSc1
<g/>
:	:	kIx,
Lodní	lodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
1946	#num#	k4
–	–	k?
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903012	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
49	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lodě	loď	k1gFnSc2
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
jednopalubové	jednopalubový	k2eAgFnSc2d1
</s>
<s>
provozní	provozní	k2eAgInPc1d1
</s>
<s>
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
Úderník	úderník	k1gInSc1
<g/>
)	)	kIx)
vyřazené	vyřazený	k2eAgNnSc1d1
</s>
<s>
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svratka	Svratka	k1gFnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
člun	člun	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Veveří	veveří	k2eAgNnSc1d1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pionýr	pionýr	k1gInSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mír	mír	k1gInSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
dvoupalubové	dvoupalubový	k2eAgNnSc1d1
</s>
<s>
provozní	provozní	k2eAgInPc1d1
</s>
<s>
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Dallas	Dallas	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Lipsko	Lipsko	k1gNnSc4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Utrecht	Utrecht	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dallas	Dallas	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stuttgart	Stuttgart	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
vyřazené	vyřazený	k2eAgFnSc2d1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Veveří	veveří	k2eAgNnSc1d1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
postavené	postavený	k2eAgNnSc1d1
v	v	k7c4
DPMBpro	DPMBpro	k1gNnSc4
jiné	jiný	k2eAgNnSc4d1
provozovatele	provozovatel	k1gMnSc2
</s>
<s>
Gladius	Gladius	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
Družba	družba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mír	mír	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svoboda	Svoboda	k1gMnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Žilina	Žilina	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Malše	Malše	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
Solidarita	solidarita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Loďstvo	loďstvo	k1gNnSc1
</s>
