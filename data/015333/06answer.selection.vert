<s>
Cystein	cystein	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Cys	Cys	k1gFnSc1
nebo	nebo	k8xC
C	C	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgFnSc1d1
neesenciální	esenciální	k2eNgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
molekule	molekula	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
thiolovou	thiolový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
-SH	-SH	k?
<g/>
.	.	kIx.
</s>