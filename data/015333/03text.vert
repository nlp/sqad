<s>
Cystein	cystein	k1gInSc1
</s>
<s>
Cystein	cystein	k1gInSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Racionální	racionální	k2eAgInSc1d1
název	název	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
3	#num#	k4
<g/>
-sulfanylpropanová	-sulfanylpropanový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C3H7NO2S	C3H7NO2S	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
52-90-4	52-90-4	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
121,16	121,16	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
240	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Disociační	disociační	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
pKa	pKa	k?
</s>
<s>
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
8,14	8,14	k4
;	;	kIx,
<g/>
(	(	kIx(
<g/>
II	II	kA
<g/>
)	)	kIx)
10,34	10,34	k4
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cystein	cystein	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Cys	Cys	k1gFnSc1
nebo	nebo	k8xC
C	C	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgFnSc1d1
neesenciální	esenciální	k2eNgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
molekule	molekula	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
thiolovou	thiolový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
-SH	-SH	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
významně	významně	k6eAd1
na	na	k7c6
struktuře	struktura	k1gFnSc6
bílkovin	bílkovina	k1gFnPc2
(	(	kIx(
<g/>
tvoří	tvořit	k5eAaImIp3nP
disulfidové	disulfidový	k2eAgInPc1d1
můstky	můstek	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
udržení	udržení	k1gNnSc1
přiměřeného	přiměřený	k2eAgNnSc2d1
oxidačně-redukčního	oxidačně-redukční	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
v	v	k7c6
buňce	buňka	k1gFnSc6
a	a	k8xC
účastní	účastnit	k5eAaImIp3nS
se	se	k3xPyFc4
mnoha	mnoho	k4c2
metabolických	metabolický	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
v	v	k7c6
syntéze	syntéza	k1gFnSc6
glutathionu	glutathion	k1gInSc2
<g/>
,	,	kIx,
taurinu	taurin	k1gInSc2
a	a	k8xC
metabolismu	metabolismus	k1gInSc2
methioninu	methionin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Látky	látka	k1gFnSc2
odvozené	odvozený	k2eAgFnSc2d1
od	od	k7c2
cysteinu	cystein	k1gInSc2
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgFnPc1d1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
antioxidativní	antioxidativní	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
pomáhají	pomáhat	k5eAaImIp3nP
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
chorobám	choroba	k1gFnPc3
vyvolaným	vyvolaný	k2eAgMnSc7d1
oxidačním	oxidační	k2eAgInSc7d1
stresem	stres	k1gInSc7
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
s	s	k7c7
chronickými	chronický	k2eAgInPc7d1
záněty	zánět	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
většině	většina	k1gFnSc6
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
průměrné	průměrný	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
je	být	k5eAaImIp3nS
ale	ale	k8xC
asi	asi	k9
jen	jen	k9
2	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Thiolová	Thiolový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
má	mít	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
afinitu	afinita	k1gFnSc4
k	k	k7c3
těžkým	těžký	k2eAgInPc3d1
kovům	kov	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
proteinech	protein	k1gInPc6
k	k	k7c3
vazbě	vazba	k1gFnSc3
kovových	kovový	k2eAgInPc2d1
ligandů	ligand	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
slouží	sloužit	k5eAaImIp3nP
například	například	k6eAd1
v	v	k7c6
enzymech	enzym	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
odstranění	odstranění	k1gNnSc3
těžkých	těžký	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
rtuť	rtuť	k1gFnSc1
<g/>
,	,	kIx,
olovo	olovo	k1gNnSc1
nebo	nebo	k8xC
kadmium	kadmium	k1gNnSc1
z	z	k7c2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
při	při	k7c6
akutních	akutní	k2eAgFnPc6d1
otravách	otrava	k1gFnPc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
derivát	derivát	k1gInSc1
N-acetylcystein	N-acetylcystein	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Cystin	Cystin	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
aminokyselinu	aminokyselina	k1gFnSc4
přítomnou	přítomný	k2eAgFnSc4d1
v	v	k7c6
proteinech	protein	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
ovšem	ovšem	k9
až	až	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
zrání	zrání	k1gNnSc4
proteinu	protein	k1gInSc2
kovalentním	kovalentní	k2eAgNnPc3d1
propojení	propojení	k1gNnSc2
dvou	dva	k4xCgInPc2
cysteinů	cystein	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzkum	výzkum	k1gInSc1
cysteinu	cystein	k1gInSc2
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
cystinem	cystin	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
kovalentně	kovalentně	k6eAd1
propojené	propojený	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
cysteinu	cystein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cystin	Cystin	k1gInSc1
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
Wollastonem	Wollaston	k1gInSc7
už	už	k6eAd1
roku	rok	k1gInSc2
1810	#num#	k4
v	v	k7c6
močových	močový	k2eAgInPc6d1
kamenech	kámen	k1gInPc6
(	(	kIx(
<g/>
cystolitech	cystolit	k1gInPc6
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
název	název	k1gInSc1
<g/>
)	)	kIx)
ale	ale	k8xC
v	v	k7c6
proteinech	protein	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
kravím	kraví	k2eAgInSc6d1
rohu	roh	k1gInSc6
<g/>
)	)	kIx)
až	až	k8xS
Mörnerem	Mörner	k1gMnSc7
roku	rok	k1gInSc2
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
komplikoval	komplikovat	k5eAaBmAgInS
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dlouho	dlouho	k6eAd1
vědělo	vědět	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
proteiny	protein	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
síru	síra	k1gFnSc4
<g/>
,	,	kIx,
používané	používaný	k2eAgFnSc2d1
chemické	chemický	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
srážely	srážet	k5eAaImAgFnP
sloučeniny	sloučenina	k1gFnPc1
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
cystin	cystin	k1gInSc4
a	a	k8xC
cystein	cystein	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relativně	relativně	k6eAd1
dlouho	dlouho	k6eAd1
trvalo	trvat	k5eAaImAgNnS
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
cystein	cystein	k1gInSc1
z	z	k7c2
močových	močový	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
a	a	k8xC
proteinů	protein	k1gInPc2
jsou	být	k5eAaImIp3nP
identické	identický	k2eAgFnPc4d1
látky	látka	k1gFnPc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
z	z	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
zpomalovalo	zpomalovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
cystin	cystin	k1gInSc1
při	při	k7c6
izolacích	izolace	k1gFnPc6
rozpadal	rozpadat	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
látku	látka	k1gFnSc4
–	–	k?
cystein	cystein	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
proteinů	protein	k1gInPc2
se	se	k3xPyFc4
jako	jako	k9
první	první	k4xOgFnSc1
vnáší	vnášet	k5eAaImIp3nS
cystein	cystein	k1gInSc1
a	a	k8xC
cystin	cystin	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
až	až	k9
následně	následně	k6eAd1
z	z	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zjistil	zjistit	k5eAaPmAgMnS
Vincent	Vincent	k1gMnSc1
du	du	k?
Vigneaud	Vigneaud	k1gMnSc1
při	při	k7c6
studiu	studio	k1gNnSc6
inzulinu	inzulin	k1gInSc2
<g/>
,	,	kIx,
vazopresinu	vazopresina	k1gFnSc4
a	a	k8xC
oxytocinu	oxytocin	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
peptidické	peptidický	k2eAgInPc1d1
hormony	hormon	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
struktuře	struktura	k1gFnSc6
obsahují	obsahovat	k5eAaImIp3nP
kovalentně	kovalentně	k6eAd1
propojené	propojený	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
cysteinu	cystein	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
cystin	cystin	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vytváří	vytvářit	k5eAaPmIp3nP,k5eAaImIp3nP
tzv.	tzv.	kA
disulfidický	disulfidický	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
dostal	dostat	k5eAaPmAgInS
roku	rok	k1gInSc2
1955	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
studium	studium	k1gNnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
souvisí	souviset	k5eAaImIp3nS
vznikání	vznikání	k1gNnSc4
a	a	k8xC
zanikání	zanikání	k1gNnPc4
disulfidických	disulfidický	k2eAgInPc2d1
můstků	můstek	k1gInPc2
se	s	k7c7
skládáním	skládání	k1gNnSc7
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
také	také	k9
Christianu	Christian	k1gMnSc3
Anfinsenovi	Anfinsen	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Funkce	funkce	k1gFnPc4
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc4
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Cystein	cystein	k1gInSc1
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
methioninem	methionin	k1gInSc7
zástupcem	zástupce	k1gMnSc7
proteinogenní	proteinogenní	k2eAgFnSc2d1
aminokyseliny	aminokyselina	k1gFnSc2
obsahující	obsahující	k2eAgFnSc4d1
síru	síra	k1gFnSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
v	v	k7c6
cysteinu	cystein	k1gInSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgFnSc1d1
thiolová	thiolový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postranní	postranní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
volného	volný	k2eAgInSc2d1
cysteinu	cystein	k1gInSc2
má	mít	k5eAaImIp3nS
pK	pK	k?
kolem	kolem	k7c2
8,5	8,5	k4
–	–	k?
takže	takže	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
v	v	k7c6
neutrálním	neutrální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
buňky	buňka	k1gFnSc2
protonovaná	protonovaný	k2eAgNnPc1d1
a	a	k8xC
nereaktivní	reaktivní	k2eNgNnPc1d1
<g/>
;	;	kIx,
v	v	k7c6
proteinech	protein	k1gInPc6
se	se	k3xPyFc4
ale	ale	k9
běžně	běžně	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
5	#num#	k4
<g/>
—	—	k?
<g/>
10	#num#	k4
a	a	k8xC
může	moct	k5eAaImIp3nS
klesnout	klesnout	k5eAaPmF
až	až	k9
k	k	k7c3
3,5	3,5	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
alkalickém	alkalický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgInSc1d1
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
nukleofil	nukleofil	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
proto	proto	k8xC
často	často	k6eAd1
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
enzymy	enzym	k1gInPc7
v	v	k7c6
aktivních	aktivní	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
při	při	k7c6
oxidaci	oxidace	k1gFnSc6
dvou	dva	k4xCgInPc2
cysteinových	cysteinův	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
vzniká	vznikat	k5eAaImIp3nS
disulfidická	disulfidický	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
propojení	propojení	k1gNnSc3
mezi	mezi	k7c4
polypeptidy	polypeptid	k1gInPc4
s	s	k7c7
takto	takto	k6eAd1
propojenými	propojený	k2eAgInPc7d1
cysteiny	cystein	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cystein	cystein	k1gInSc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
rozpustný	rozpustný	k2eAgMnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
vodíkové	vodíkový	k2eAgInPc4d1
můstky	můstek	k1gInPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
tradičně	tradičně	k6eAd1
uváděn	uvádět	k5eAaImNgInS
jako	jako	k8xS,k8xC
nenabitá	nabitý	k2eNgFnSc1d1
<g/>
,	,	kIx,
polární	polární	k2eAgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
má	mít	k5eAaImIp3nS
vlastnosti	vlastnost	k1gFnPc4
hydrofóbních	hydrofóbní	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
například	například	k6eAd1
stabilizovat	stabilizovat	k5eAaBmF
tvorbu	tvorba	k1gFnSc4
micel	micela	k1gFnPc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
proteinech	protein	k1gInPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
ve	v	k7c6
shlucích	shluk	k1gInPc6
hydrofobních	hydrofobní	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Vysvětlení	vysvětlení	k1gNnSc4
tohoto	tento	k3xDgNnSc2
neobvyklého	obvyklý	k2eNgNnSc2d1
chování	chování	k1gNnSc2
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
cystein	cystein	k1gInSc4
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
podobný	podobný	k2eAgInSc1d1
serinu	serin	k2eAgInSc3d1
<g/>
,	,	kIx,
tak	tak	k9
-SH	-SH	k?
skupina	skupina	k1gFnSc1
cysteinu	cystein	k1gInSc2
nedokáže	dokázat	k5eNaPmIp3nS
vytvářet	vytvářet	k5eAaImF
vodíkové	vodíkový	k2eAgInPc4d1
můstky	můstek	k1gInPc4
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
-OH	-OH	k?
skupiny	skupina	k1gFnSc2
serinu	serinout	k5eAaPmIp1nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
v	v	k7c6
proteinech	protein	k1gInPc6
</s>
<s>
Role	role	k1gFnSc1
cysteinu	cystein	k1gInSc2
v	v	k7c6
proteinech	protein	k1gInPc6
u	u	k7c2
eukaryot	eukaryota	k1gFnPc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
protein	protein	k1gInSc1
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neredukujícím	redukující	k2eNgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
mimo	mimo	k7c4
buňku	buňka	k1gFnSc4
a	a	k8xC
některých	některý	k3yIgFnPc6
organelách	organela	k1gFnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
drsném	drsný	k2eAgNnSc6d1
endoplasmatickém	endoplasmatický	k2eAgNnSc6d1
retikulu	retikulum	k1gNnSc6
<g/>
,	,	kIx,
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
páry	pár	k1gInPc4
cysteinů	cystein	k1gInPc2
kovalentně	kovalentně	k6eAd1
propojené	propojený	k2eAgInPc1d1
disulfidické	disulfidický	k2eAgInPc1d1
můstky	můstek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
dramaticky	dramaticky	k6eAd1
určují	určovat	k5eAaImIp3nP
prostorové	prostorový	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
proteinů	protein	k1gInPc2
a	a	k8xC
velmi	velmi	k6eAd1
zvyšují	zvyšovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
odolnost	odolnost	k1gFnSc4
proti	proti	k7c3
proteázám	proteáza	k1gFnPc3
a	a	k8xC
celkovou	celkový	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
proteinu	protein	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
zastoupeny	zastoupit	k5eAaPmNgFnP
například	například	k6eAd1
v	v	k7c6
keratinu	keratin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
redukujícím	redukující	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
cytosolu	cytosol	k1gInSc2
se	se	k3xPyFc4
disulfidické	disulfidický	k2eAgInPc1d1
můstky	můstek	k1gInPc1
vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
jen	jen	k9
velmi	velmi	k6eAd1
vzácně	vzácně	k6eAd1
a	a	k8xC
role	role	k1gFnSc1
cysteinu	cystein	k1gInSc2
je	být	k5eAaImIp3nS
hlavně	hlavně	k9
v	v	k7c6
koordinační	koordinační	k2eAgFnSc6d1
vazbě	vazba	k1gFnSc6
kovových	kovový	k2eAgInPc2d1
ligandů	ligand	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
zinek	zinek	k1gInSc4
v	v	k7c6
případě	případ	k1gInSc6
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1
a	a	k8xC
strukturního	strukturní	k2eAgInSc2d1
motivu	motiv	k1gInSc2
nazývaného	nazývaný	k2eAgMnSc2d1
zinkový	zinkový	k2eAgInSc4d1
prst	prst	k1gInSc4
<g/>
,	,	kIx,
mědi	měď	k1gFnPc4
v	v	k7c4
plastocyaninu	plastocyanina	k1gFnSc4
,	,	kIx,
železa	železo	k1gNnPc1
v	v	k7c6
cytochromu	cytochrom	k1gInSc6
P450	P450	k1gFnSc2
a	a	k8xC
NiFe	nife	k1gNnSc2
hydrogenázách	hydrogenáza	k1gFnPc6
mnohých	mnohý	k2eAgInPc2d1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Cystein	cystein	k1gInSc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
cysteinové	cysteinový	k2eAgFnPc1d1
proteázy	proteáza	k1gFnPc1
(	(	kIx(
<g/>
papain	papain	k1gInSc1
<g/>
,	,	kIx,
kaspázy	kaspáza	k1gFnPc1
<g/>
)	)	kIx)
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
aktivních	aktivní	k2eAgNnPc6d1
místech	místo	k1gNnPc6
a	a	k8xC
metalloproteázy	metalloproteáza	k1gFnSc2
jím	jíst	k5eAaImIp1nS
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
aktivního	aktivní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
váží	vážit	k5eAaImIp3nP
kovy	kov	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Antioxidant	antioxidant	k1gInSc1
</s>
<s>
Protože	protože	k8xS
je	být	k5eAaImIp3nS
cystein	cystein	k1gInSc1
antioxidant	antioxidant	k1gInSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
spolu	spolu	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
sloučeninami	sloučenina	k1gFnPc7
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c4
udržování	udržování	k1gNnSc4
oxidačně-redukčního	oxidačně-redukční	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
redukujícím	redukující	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
buněčného	buněčný	k2eAgInSc2d1
cytosolu	cytosol	k1gInSc2
je	být	k5eAaImIp3nS
cystein	cystein	k1gInSc1
schopný	schopný	k2eAgInSc1d1
navázat	navázat	k5eAaPmF
nebezpečné	bezpečný	k2eNgFnPc4d1
oxidující	oxidující	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
kyslíkové	kyslíkový	k2eAgInPc4d1
radikály	radikál	k1gInPc4
<g/>
,	,	kIx,
peroxid	peroxid	k1gInSc4
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
organické	organický	k2eAgInPc4d1
hydroperoxidy	hydroperoxid	k1gInPc4
<g/>
,	,	kIx,
peroxynitrity	peroxynitrit	k1gInPc4
a	a	k8xC
mnohé	mnohý	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
redukuje	redukovat	k5eAaBmIp3nS
a	a	k8xC
naopak	naopak	k6eAd1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
stává	stávat	k5eAaImIp3nS
reaktivním	reaktivní	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
nestabilní	stabilní	k2eNgInSc4d1
meziprodukt	meziprodukt	k1gInSc4
následně	následně	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
nereaktivní	reaktivní	k2eNgFnSc1d1
<g/>
,	,	kIx,
oxidovaná	oxidovaný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
cysteinu	cystein	k1gInSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
disulfidový	disulfidový	k2eAgInSc1d1
můstek	můstek	k1gInSc1
vznikající	vznikající	k2eAgInSc1d1
s	s	k7c7
blízkým	blízký	k2eAgInSc7d1
cysteinem	cystein	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
oxidovaného	oxidovaný	k2eAgInSc2d1
cysteinu	cystein	k1gInSc2
nenachází	nacházet	k5eNaImIp3nS
jiný	jiný	k2eAgInSc4d1
cystein	cystein	k1gInSc4
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
využíváno	využíván	k2eAgNnSc4d1
navázání	navázání	k1gNnSc4
jiného	jiný	k2eAgInSc2d1
cysteinu	cystein	k1gInSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
cysteinu	cystein	k1gInSc2
v	v	k7c6
glutathionu	glutathion	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významným	významný	k2eAgInSc7d1
antioxidantem	antioxidant	k1gInSc7
založeným	založený	k2eAgInSc7d1
na	na	k7c6
cysteinu	cystein	k1gInSc6
je	být	k5eAaImIp3nS
glutathion	glutathion	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
neobvyklý	obvyklý	k2eNgInSc1d1
tripeptid	tripeptid	k1gInSc1
obsahující	obsahující	k2eAgInSc1d1
glutamin	glutamin	k1gInSc1
<g/>
,	,	kIx,
cystein	cystein	k1gInSc1
a	a	k8xC
glycin	glycin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
cysteinu	cystein	k1gInSc3
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
redukovat	redukovat	k5eAaBmF
nebezpečné	bezpečný	k2eNgInPc4d1
oxidanty	oxidant	k1gInPc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
oxiduje	oxidovat	k5eAaBmIp3nS
a	a	k8xC
následně	následně	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
glutathion	glutathion	k1gInSc1
disulfid	disulfida	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
dva	dva	k4xCgInPc1
glutathiony	glutathion	k1gInPc1
propojené	propojený	k2eAgInPc1d1
disulfidickou	disulfidický	k2eAgFnSc7d1
vazbou	vazba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
další	další	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
je	být	k5eAaImIp3nS
redukovat	redukovat	k5eAaBmF
disulfidické	disulfidický	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
ostatních	ostatní	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
disulfidických	disulfidický	k2eAgInPc2d1
můstků	můstek	k1gInPc2
mezi	mezi	k7c7
proteinem	protein	k1gInSc7
a	a	k8xC
jiným	jiný	k2eAgInSc7d1
glutathionem	glutathion	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
opět	opět	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
glutathion	glutathion	k1gInSc4
disulfid	disulfida	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glutathion	Glutathion	k1gInSc1
disulfid	disulfid	k1gInSc4
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
redukován	redukovat	k5eAaBmNgInS
na	na	k7c4
dvě	dva	k4xCgFnPc4
molekuly	molekula	k1gFnPc4
glutathionu	glutathion	k1gInSc2
glutathion	glutathion	k1gInSc4
reduktázou	reduktáza	k1gFnSc7
a	a	k8xC
může	moct	k5eAaImIp3nS
znovu	znovu	k6eAd1
působit	působit	k5eAaImF
jako	jako	k8xC,k8xS
antioxidant	antioxidant	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antioxidační	antioxidační	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
glutathionu	glutathion	k1gInSc2
je	být	k5eAaImIp3nS
umožněná	umožněný	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
glutathion	glutathion	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
buňkách	buňka	k1gFnPc6
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
koncentraci	koncentrace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
glutathion	glutathion	k1gInSc1
není	být	k5eNaImIp3nS
při	při	k7c6
trávení	trávení	k1gNnSc6
dobře	dobře	k6eAd1
vstřebáván	vstřebávat	k5eAaImNgInS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
jej	on	k3xPp3gMnSc4
v	v	k7c6
buňce	buňka	k1gFnSc6
vytvářet	vytvářet	k5eAaImF
z	z	k7c2
prekurzorů	prekurzor	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
limitujícím	limitující	k2eAgInSc7d1
krokem	krok	k1gInSc7
dostupnost	dostupnost	k1gFnSc1
cysteinu	cystein	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
proteinech	protein	k1gInPc6
relativně	relativně	k6eAd1
vzácný	vzácný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Posttranslační	Posttranslační	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
</s>
<s>
Proinzulin	Proinzulin	k2eAgInSc1d1
<g/>
:	:	kIx,
příklad	příklad	k1gInSc1
krátké	krátký	k2eAgFnSc2d1
bílkoviny	bílkovina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
disulfidické	disulfidický	k2eAgInPc4d1
můstky	můstek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nůžky	nůžky	k1gFnPc1
naznačují	naznačovat	k5eAaImIp3nP
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
pro	pro	k7c4
vznik	vznik	k1gInSc4
funkčního	funkční	k2eAgInSc2d1
inzulinu	inzulin	k1gInSc2
odštěpena	odštěpen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Disulfidický	Disulfidický	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Cystein	cystein	k1gInSc1
je	být	k5eAaImIp3nS
cílem	cíl	k1gInSc7
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
posttranslačních	posttranslační	k2eAgFnPc2d1
modifikací	modifikace	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
především	především	k9
kovalentní	kovalentní	k2eAgNnSc4d1
propojení	propojení	k1gNnSc4
dvou	dva	k4xCgInPc2
cysteinů	cystein	k1gInPc2
vytvářející	vytvářející	k2eAgInSc1d1
tzv.	tzv.	kA
disulfidický	disulfidický	k2eAgInSc1d1
můstek	můstek	k1gInSc1
<g/>
,	,	kIx,
schematicky	schematicky	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc4d1
disulfidický	disulfidický	k2eAgInSc4d1
můstek	můstek	k1gInSc4
znázornit	znázornit	k5eAaPmF
jako	jako	k9
R	R	kA
<g/>
–	–	k?
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
R	R	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
„	„	k?
<g/>
R	R	kA
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
bílkovinné	bílkovinný	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgFnSc1d1
thiolová	thiolový	k2eAgFnSc1d1
umožňuje	umožňovat	k5eAaImIp3nS
napojení	napojení	k1gNnSc4
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
palmitoylaci	palmitoylace	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
připojení	připojení	k1gNnSc1
zbytku	zbytek	k1gInSc2
palmitové	palmitový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
C-terminálních	C-terminální	k2eAgInPc2d1
cysteinů	cystein	k1gInPc2
připojení	připojení	k1gNnSc2
farnesylu	farnesyl	k1gInSc2
nebo	nebo	k8xC
geranyl-geranylu	geranyl-geranyl	k1gInSc2
(	(	kIx(
<g/>
isopenoidy	isopenoida	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cystein	cystein	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
cílem	cíl	k1gInSc7
připojení	připojení	k1gNnSc2
ubiquitinu	ubiquitin	k1gInSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
tato	tento	k3xDgFnSc1
modifikace	modifikace	k1gFnSc1
mnohem	mnohem	k6eAd1
častěji	často	k6eAd2
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c4
lyzinu	lyzina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzniku	vznik	k1gInSc6
železosirných	železosirný	k2eAgNnPc2d1
center	centrum	k1gNnPc2
v	v	k7c6
FeS	fes	k1gNnSc6
proteinech	protein	k1gInPc6
slouží	sloužit	k5eAaImIp3nS
cystein	cystein	k1gInSc1
jako	jako	k8xC,k8xS
dárce	dárce	k1gMnSc1
síry	síra	k1gFnSc2
<g/>
:	:	kIx,
pomocí	pomocí	k7c2
enzymu	enzym	k1gInSc2
jménem	jméno	k1gNnSc7
cystein	cystein	k1gInSc1
desulfuráza	desulfuráza	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
cysteinu	cystein	k1gInSc2
odštěpena	odštěpen	k2eAgFnSc1d1
síra	síra	k1gFnSc1
a	a	k8xC
přesunuta	přesunout	k5eAaPmNgFnS
do	do	k7c2
vznikajícího	vznikající	k2eAgNnSc2d1
železosirného	železosirný	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
,	,	kIx,
namísto	namísto	k7c2
cysteinu	cystein	k1gInSc2
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
proteinu	protein	k1gInSc6
alanin	alanina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
proteinech	protein	k1gInPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
nestandardní	standardní	k2eNgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
selenocystein	selenocysteina	k1gFnPc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
ale	ale	k9
nevzniká	vznikat	k5eNaImIp3nS
modifikací	modifikace	k1gFnSc7
cysteinu	cystein	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
od	od	k7c2
serinu	serin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Metabolismus	metabolismus	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
v	v	k7c6
potravě	potrava	k1gFnSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Cystein	cystein	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
člověka	člověk	k1gMnSc4
podmínečně	podmínečně	k6eAd1
esenciální	esenciální	k2eAgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
vyžaduje	vyžadovat	k5eAaImIp3nS
zdroj	zdroj	k1gInSc4
methioninu	methionin	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dodává	dodávat	k5eAaImIp3nS
atom	atom	k1gInSc4
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
bez	bez	k7c2
něj	on	k3xPp3gInSc2
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
syntetizován	syntetizován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
syntéza	syntéza	k1gFnSc1
cysteinu	cystein	k1gInSc2
neprobíhá	probíhat	k5eNaImIp3nS
při	při	k7c6
některých	některý	k3yIgFnPc6
jaderných	jaderný	k2eAgFnPc6d1
poruchách	porucha	k1gFnPc6
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
u	u	k7c2
novorozenců	novorozenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nedostatek	nedostatek	k1gInSc1
aminokyseliny	aminokyselina	k1gFnSc2
cysteinu	cystein	k1gInSc2
navíc	navíc	k6eAd1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
zvýšené	zvýšený	k2eAgFnSc3d1
spotřebě	spotřeba	k1gFnSc3
esenciálního	esenciální	k2eAgInSc2d1
methioninu	methionin	k1gInSc2
(	(	kIx(
<g/>
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
metabolismu	metabolismus	k1gInSc6
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nezbytný	zbytný	k2eNgInSc4d1,k2eAgInSc4d1
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc4
methylových	methylový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
následně	následně	k6eAd1
může	moct	k5eAaImIp3nS
tělu	tělo	k1gNnSc3
chybět	chybět	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
potravinách	potravina	k1gFnPc6
bohatých	bohatý	k2eAgFnPc6d1
na	na	k7c4
proteiny	protein	k1gInPc4
<g/>
,	,	kIx,
významně	významně	k6eAd1
je	být	k5eAaImIp3nS
zastoupen	zastoupen	k2eAgInSc1d1
např.	např.	kA
v	v	k7c6
syrovátce	syrovátka	k1gFnSc6
<g/>
,	,	kIx,
cibuli	cibule	k1gFnSc6
<g/>
,	,	kIx,
česneku	česnek	k1gInSc6
<g/>
,	,	kIx,
brokolici	brokolice	k1gFnSc6
a	a	k8xC
ovsu	oves	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Biosyntéza	biosyntéza	k1gFnSc1
</s>
<s>
Syntéza	syntéza	k1gFnSc1
cysteinu	cystein	k1gInSc2
u	u	k7c2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
syntéza	syntéza	k1gFnSc1
může	moct	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
tělo	tělo	k1gNnSc4
dostatečný	dostatečný	k2eAgInSc1d1
přísun	přísun	k1gInSc1
aminokyseliny	aminokyselina	k1gFnSc2
methioninu	methionin	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
přeměně	přeměna	k1gFnSc6
na	na	k7c4
homocystein	homocystein	k1gInSc4
dodává	dodávat	k5eAaImIp3nS
atom	atom	k1gInSc4
síry	síra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
lidi	člověk	k1gMnPc4
je	být	k5eAaImIp3nS
cystein	cystein	k1gInSc1
semiesenciální	semiesenciální	k2eAgFnSc1d1
aminokyselina	aminokyselina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidské	lidský	k2eAgInPc1d1
tělo	tělo	k1gNnSc1
si	se	k3xPyFc3
jej	on	k3xPp3gInSc4
dokáže	dokázat	k5eAaPmIp3nS
vytvářet	vytvářet	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
dostatečný	dostatečný	k2eAgInSc4d1
přísun	přísun	k1gInSc4
aminokyseliny	aminokyselina	k1gFnSc2
methioninu	methionin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
biosyntézu	biosyntéza	k1gFnSc4
cysteinu	cystein	k1gInSc2
jsou	být	k5eAaImIp3nP
nezbytné	nezbytný	k2eAgFnPc1d1,k2eNgFnPc1d1
dvě	dva	k4xCgFnPc1
aminokyseliny	aminokyselina	k1gFnPc1
<g/>
:	:	kIx,
serin	serin	k1gInSc1
dodávající	dodávající	k2eAgFnSc4d1
uhlíkovou	uhlíkový	k2eAgFnSc4d1
kostru	kostra	k1gFnSc4
a	a	k8xC
methionin	methionin	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zdrojem	zdroj	k1gInSc7
síry	síra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
procesu	proces	k1gInSc6
je	být	k5eAaImIp3nS
methionin	methionin	k1gInSc1
nejdříve	dříve	k6eAd3
zpracován	zpracovat	k5eAaPmNgInS
na	na	k7c4
S-adenosylmethionin	S-adenosylmethionin	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
přeměněn	přeměněn	k2eAgInSc1d1
na	na	k7c4
homocystein	homocystein	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enzym	enzym	k1gInSc1
cystathionin	cystathionin	k2eAgInSc1d1
beta-syntáza	beta-syntáza	k1gFnSc1
poté	poté	k6eAd1
propojí	propojit	k5eAaPmIp3nS
homocystein	homocystein	k1gInSc4
a	a	k8xC
serin	serin	k1gInSc4
za	za	k7c2
vzniku	vznik	k1gInSc2
cystathioninu	cystathionin	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
enzym	enzym	k1gInSc4
cystathionin	cystathionina	k1gFnPc2
gama-lyáza	gama-lyáza	k1gFnSc1
odštěpí	odštěpit	k5eAaPmIp3nS
amoniak	amoniak	k1gInSc4
<g/>
,	,	kIx,
alfa-ketoglutarát	alfa-ketoglutarát	k1gInSc4
a	a	k8xC
cystein	cystein	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bakterie	bakterie	k1gFnPc1
a	a	k8xC
rostliny	rostlina	k1gFnPc1
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgInPc1d1
pro	pro	k7c4
syntézu	syntéza	k1gFnSc4
cysteinu	cystein	k1gInSc2
využívat	využívat	k5eAaImF,k5eAaPmF
anorganickou	anorganický	k2eAgFnSc4d1
síru	síra	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
sulfátu	sulfát	k1gInSc2
(	(	kIx(
<g/>
SO	So	kA
<g/>
42	#num#	k4
<g/>
−	−	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
nejdříve	dříve	k6eAd3
ve	v	k7c6
dvou	dva	k4xCgInPc6
krocích	krok	k1gInPc6
"	"	kIx"
<g/>
aktivován	aktivován	k2eAgInSc1d1
<g/>
"	"	kIx"
za	za	k7c2
vzniku	vznik	k1gInSc2
3	#num#	k4
<g/>
'	'	kIx"
<g/>
-fosfoadenosin	-fosfoadenosin	k1gInSc1
5	#num#	k4
<g/>
'	'	kIx"
<g/>
-fosfosulfátu	-fosfosulfát	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
redukován	redukovat	k5eAaBmNgInS
do	do	k7c2
podoby	podoba	k1gFnSc2
sulfitu	sulfit	k1gInSc2
(	(	kIx(
<g/>
SO	So	kA
<g/>
32	#num#	k4
<g/>
−	−	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sulfit	sulfit	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
ve	v	k7c6
dvou	dva	k4xCgInPc6
krocích	krok	k1gInPc6
připojen	připojen	k2eAgInSc1d1
na	na	k7c4
serin	serin	k1gInSc4
za	za	k7c2
vzniku	vznik	k1gInSc2
cysteinu	cystein	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odbourávání	odbourávání	k1gNnSc1
</s>
<s>
Zpracování	zpracování	k1gNnSc1
cysteinu	cystein	k1gInSc2
je	být	k5eAaImIp3nS
klíčový	klíčový	k2eAgInSc4d1
krok	krok	k1gInSc4
pro	pro	k7c4
celkový	celkový	k2eAgInSc4d1
metabolismus	metabolismus	k1gInSc4
dalších	další	k2eAgInPc2d1
pro	pro	k7c4
organismus	organismus	k1gInSc4
významných	významný	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
síry	síra	k1gFnSc2
<g/>
:	:	kIx,
methioninu	methionin	k1gInSc2
a	a	k8xC
homocysteinu	homocystein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgFnPc1
molekuly	molekula	k1gFnPc1
síry	síra	k1gFnSc2
z	z	k7c2
těchto	tento	k3xDgInPc2
proteinů	protein	k1gInPc2
určené	určený	k2eAgFnPc1d1
pro	pro	k7c4
vyloučení	vyloučení	k1gNnSc4
jsou	být	k5eAaImIp3nP
přeneseny	přenést	k5eAaPmNgInP
na	na	k7c4
serin	serin	k1gInSc4
procesem	proces	k1gInSc7
transsulfurace	transsulfurace	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
cystein	cystein	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
degradace	degradace	k1gFnSc1
cysteinu	cystein	k1gInSc2
může	moct	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
několika	několik	k4yIc2
cestami	cesta	k1gFnPc7
s	s	k7c7
použitím	použití	k1gNnSc7
různých	různý	k2eAgInPc2d1
enzymů	enzym	k1gInPc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
buněčném	buněčný	k2eAgInSc6d1
typu	typ	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
degradace	degradace	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
některým	některý	k3yIgMnPc3
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
dostupnosti	dostupnost	k1gFnPc1
aminokyselin	aminokyselina	k1gFnPc2
a	a	k8xC
některých	některý	k3yIgFnPc2
sloučenin	sloučenina	k1gFnPc2
obsahujících	obsahující	k2eAgMnPc2d1
síru	síra	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
S-adenosylmethionin	S-adenosylmethionin	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
degradační	degradační	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
dva	dva	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
degradace	degradace	k1gFnSc2
–	–	k?
oxidační	oxidační	k2eAgFnSc4d1
nebo	nebo	k8xC
neoxidační	oxidační	k2eNgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cystein	cystein	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
oxidován	oxidovat	k5eAaBmNgInS
a	a	k8xC
následně	následně	k6eAd1
přeměněn	přeměněn	k2eAgMnSc1d1
buď	buď	k8xC
na	na	k7c4
taurin	taurin	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c4
síranový	síranový	k2eAgInSc4d1
anion	anion	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vyloučeny	vyloučit	k5eAaPmNgInP
močí	moč	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případně	případně	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
může	moct	k5eAaImIp3nS
v	v	k7c6
několika	několik	k4yIc6
krocích	krok	k1gInPc6
nezahrnujících	zahrnující	k2eNgInPc2d1
oxidaci	oxidace	k1gFnSc6
oddělit	oddělit	k5eAaPmF
toxický	toxický	k2eAgInSc1d1
sulfan	sulfan	k1gInSc1
nebo	nebo	k8xC
sulfid	sulfid	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
detoxifikovány	detoxifikovat	k5eAaPmNgInP,k5eAaImNgInP,k5eAaBmNgInP
oxidací	oxidace	k1gFnSc7
v	v	k7c6
mitochondriích	mitochondrie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nedostatku	nedostatek	k1gInSc6
cysteinu	cystein	k1gInSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
síry	síra	k1gFnSc2
jsou	být	k5eAaImIp3nP
preferovány	preferován	k2eAgFnPc1d1
neoxidační	oxidační	k2eNgFnPc1d1
cesty	cesta	k1gFnPc1
<g/>
,	,	kIx,
při	při	k7c6
nadbytku	nadbytek	k1gInSc6
cesty	cesta	k1gFnSc2
oxidační	oxidační	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
aminokyseliny	aminokyselina	k1gFnPc1
</s>
<s>
proteiny	protein	k1gInPc1
</s>
<s>
methionin	methionin	k1gInSc1
</s>
<s>
selenocystein	selenocystein	k1gMnSc1
</s>
<s>
taurin	taurin	k1gInSc1
</s>
<s>
cysteinové	cysteinový	k2eAgFnPc1d1
proteázy	proteáza	k1gFnPc1
</s>
<s>
N-acetylcystein	N-acetylcystein	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
MCPHERSON	MCPHERSON	kA
<g/>
,	,	kIx,
RA	ra	k0
<g/>
.	.	kIx.
<g/>
;	;	kIx,
HARDY	HARDY	kA
<g/>
,	,	kIx,
G.	G.	kA
Clinical	Clinical	k1gMnSc1
and	and	k?
nutritional	nutritionat	k5eAaPmAgMnS,k5eAaImAgMnS
benefits	benefits	k6eAd1
of	of	k?
cysteine-enriched	cysteine-enriched	k1gInSc1
protein	protein	k1gInSc1
supplements	supplements	k1gInSc1
<g/>
..	..	k?
Curr	Curr	k1gMnSc1
Opin	Opin	k1gMnSc1
Clin	Clin	k1gMnSc1
Nutr	Nutr	k1gMnSc1
Metab	Metab	k1gMnSc1
Care	car	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nov	nov	k1gInSc1
2011	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
562	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
MCO	MCO	kA
<g/>
.0	.0	k4
<g/>
b	b	k?
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
e	e	k0
<g/>
32834	#num#	k4
<g/>
c	c	k0
<g/>
1780	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
21986479	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
WHITFORD	WHITFORD	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proteins	Proteins	k1gInSc4
<g/>
:	:	kIx,
Structure	Structur	k1gMnSc5
and	and	k?
Function	Function	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley	Wiley	k1gInPc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
542	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
471498940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amino	Amino	k1gNnSc1
acids	acidsa	k1gFnPc2
<g/>
:	:	kIx,
the	the	k?
building	building	k1gInSc1
blocks	blocks	k1gInSc1
of	of	k?
proteins	proteins	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FLORA	FLORA	kA
<g/>
,	,	kIx,
SJ	SJ	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
PACHAURI	PACHAURI	kA
<g/>
,	,	kIx,
V.	V.	kA
Chelation	Chelation	k1gInSc1
in	in	k?
metal	metal	k1gInSc1
intoxication	intoxication	k1gInSc1
<g/>
..	..	k?
Int	Int	k1gMnSc1
J	J	kA
Environ	Environ	k1gMnSc1
Res	Res	k1gFnSc2
Public	publicum	k1gNnPc2
Health	Health	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jul	Jula	k1gFnPc2
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2745	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.339	10.339	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
ijerph	ijerph	k1gInSc1
<g/>
7072745	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20717537	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VICKERY	VICKERY	kA
<g/>
,	,	kIx,
Hubert	Hubert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Discovery	Discovera	k1gFnSc2
of	of	k?
the	the	k?
Amino	Amino	k6eAd1
Acids	Acidsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
reviews	reviewsa	k1gFnPc2
<g/>
.	.	kIx.
1931	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
-318	-318	k4
<g/>
.	.	kIx.
↑	↑	k?
Award	Award	k1gMnSc1
Ceremony	Ceremona	k1gFnSc2
Speech	speech	k1gInSc1
-	-	kIx~
du	du	k?
Vigneaud	Vigneaud	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
nobelprize	nobelprize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Award	Award	k1gMnSc1
Ceremony	Ceremona	k1gFnSc2
Speech	speech	k1gInSc1
-	-	kIx~
Anfinsen	Anfinsen	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
nobelprice	nobelprika	k1gFnSc3
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
KLOMSIRI	KLOMSIRI	kA
<g/>
,	,	kIx,
C.	C.	kA
<g/>
;	;	kIx,
KARPLUS	KARPLUS	kA
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
POOLE	POOLE	kA
<g/>
,	,	kIx,
LB	LB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cysteine-based	Cysteine-based	k1gInSc1
redox	redox	k2eAgInSc4d1
switches	switches	k1gInSc4
in	in	k?
enzymes	enzymes	k1gInSc1
<g/>
..	..	k?
Antioxid	Antioxid	k1gInSc1
Redox	redox	k2eAgFnPc2d1
Signal	Signal	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mar	Mar	k1gFnSc1
2011	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1065	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
ars	ars	k?
<g/>
.2010	.2010	k4
<g/>
.3376	.3376	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20799881	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HEITMANN	HEITMANN	kA
<g/>
,	,	kIx,
P.	P.	kA
A	a	k9
model	model	k1gInSc4
for	forum	k1gNnPc2
sulfhydryl	sulfhydryl	k1gInSc1
groups	groups	k1gInSc1
in	in	k?
proteins	proteins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydrophobic	Hydrophobice	k1gInPc2
interactions	interactionsa	k1gFnPc2
of	of	k?
the	the	k?
cystein	cystein	k1gInSc1
side	sidat	k5eAaPmIp3nS
chain	chain	k2eAgInSc1d1
in	in	k?
micelles	micelles	k1gInSc1
<g/>
..	..	k?
Eur	euro	k1gNnPc2
J	J	kA
Biochem	Bioch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
1968	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
346	#num#	k4
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
5650851	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NAGANO	Nagano	k1gNnSc4
<g/>
,	,	kIx,
N.	N.	kA
<g/>
;	;	kIx,
OTA	Ota	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
NISHIKAWA	NISHIKAWA	kA
<g/>
,	,	kIx,
K.	K.	kA
Strong	Strong	k1gMnSc1
hydrophobic	hydrophobic	k1gMnSc1
nature	natur	k1gMnSc5
of	of	k?
cysteine	cystein	k1gInSc5
residues	residues	k1gInSc4
in	in	k?
proteins	proteins	k1gInSc1
<g/>
..	..	k?
FEBS	FEBS	kA
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sep	Sep	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
458	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10518936	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BARNES	BARNES	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
R.	R.	kA
Bioinformatics	Bioinformatics	k1gInSc1
for	forum	k1gNnPc2
Geneticists	Geneticists	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Bioinformatics	Bioinformatics	k1gInSc1
Primer	primera	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Analysis	Analysis	k1gFnSc2
of	of	k?
Genetic	Genetice	k1gFnPc2
Data	datum	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley	Wiley	k1gInPc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
576	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
470026205	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
14	#num#	k4
<g/>
:	:	kIx,
Amino	Amino	k6eAd1
Acid	Acid	k1gMnSc1
Properties	Properties	k1gMnSc1
and	and	k?
Consequences	Consequences	k1gMnSc1
of	of	k?
Substitutions	Substitutions	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
en	en	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LILL	LILL	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
;	;	kIx,
MÜHLENHOFF	MÜHLENHOFF	kA
<g/>
,	,	kIx,
U.	U.	kA
Iron-sulfur	Iron-sulfur	k1gMnSc1
protein	protein	k1gInSc4
biogenesis	biogenesis	k1gFnSc2
in	in	k?
eukaryotes	eukaryotes	k1gInSc1
<g/>
:	:	kIx,
components	components	k1gInSc1
and	and	k?
mechanisms	mechanisms	k1gInSc4
<g/>
..	..	k?
Annu	Anna	k1gFnSc4
Rev	Rev	k1gFnSc2
Cell	cello	k1gNnPc2
Dev	Dev	k1gMnSc7
Biol.	Biol.	k1gMnSc7
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
457	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
annurev	annurev	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cellbio	cellbio	k6eAd1
<g/>
.22	.22	k4
<g/>
.010305	.010305	k4
<g/>
.104538	.104538	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16824008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FUKAGAWA	FUKAGAWA	kA
<g/>
,	,	kIx,
NK	NK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparing	sparing	k1gInSc1
of	of	k?
methionine	methionin	k1gInSc5
requirements	requirements	k1gInSc1
<g/>
:	:	kIx,
evaluation	evaluation	k1gInSc1
of	of	k?
human	human	k1gMnSc1
data	datum	k1gNnSc2
takes	takes	k1gMnSc1
sulfur	sulfur	k1gMnSc1
amino	amino	k6eAd1
acids	acids	k6eAd1
beyond	beyond	k1gInSc1
protein	protein	k1gInSc1
<g/>
..	..	k?
J	J	kA
Nutr	Nutr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jun	jun	k1gMnSc1
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
136	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
Suppl	Suppl	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
1676	#num#	k4
<g/>
S-	S-	k1gFnSc1
<g/>
1681	#num#	k4
<g/>
S.	S.	kA
PMID	PMID	kA
16702339	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
COX	COX	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
M.	M.	kA
Lehninger	Lehninger	k1gMnSc1
Principles	Principles	k1gMnSc1
of	of	k?
Biochemistry	Biochemistr	k1gMnPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
W.H.	W.H.	k1gMnSc1
Freeman	Freeman	k1gMnSc1
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1429234146	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
22	#num#	k4
<g/>
:	:	kIx,
Biosynthesis	Biosynthesis	k1gFnSc1
of	of	k?
amino	amino	k6eAd1
acids	acids	k6eAd1
<g/>
,	,	kIx,
nucleotides	nucleotides	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
related	related	k1gMnSc1
molecules	molecules	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
STIPANUK	STIPANUK	kA
<g/>
,	,	kIx,
MH	MH	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
UEKI	UEKI	kA
<g/>
,	,	kIx,
I.	I.	kA
Dealing	Dealing	k1gInSc1
with	with	k1gInSc1
methionine	methionin	k1gInSc5
<g/>
/	/	kIx~
<g/>
homocysteine	homocysteinout	k5eAaPmIp3nS
sulfur	sulfur	k1gMnSc1
<g/>
:	:	kIx,
cysteine	cystein	k1gInSc5
metabolism	metabolism	k1gInSc1
to	ten	k3xDgNnSc1
taurine	taurin	k1gInSc5
and	and	k?
inorganic	inorganice	k1gFnPc2
sulfur	sulfur	k1gMnSc1
<g/>
..	..	k?
J	J	kA
Inherit	Inherit	k1gInSc1
Metab	Metab	k1gInSc1
Dis.	Dis.	k1gMnSc2
Feb	Feb	k1gMnSc2
2011	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
34	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
-	-	kIx~
<g/>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
10545	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
9006	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20162368	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
L-cystein	L-cysteina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Proteinogenní	Proteinogenní	k2eAgFnSc2d1
aminokyseliny	aminokyselina	k1gFnSc2
Obecná	obecný	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Aminokyselina	aminokyselina	k1gFnSc1
</s>
<s>
Peptid	peptid	k1gInSc1
</s>
<s>
Protein	protein	k1gInSc1
</s>
<s>
Genetický	genetický	k2eAgInSc4d1
kód	kód	k1gInSc4
Rozdělení	rozdělení	k1gNnSc1
</s>
<s>
Alifatické	alifatický	k2eAgNnSc1d1
</s>
<s>
S	s	k7c7
rozvětveným	rozvětvený	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
(	(	kIx(
<g/>
Valin	valin	k1gInSc1
</s>
<s>
Isoleucin	Isoleucin	k1gMnSc1
</s>
<s>
Leucin	leucin	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Methionin	methionin	k1gInSc1
</s>
<s>
Alanin	Alanin	k1gInSc1
</s>
<s>
Prolin	prolina	k1gFnPc2
</s>
<s>
Glycin	glycin	k1gInSc1
Aromatické	aromatický	k2eAgFnSc2d1
</s>
<s>
Fenylalanin	Fenylalanin	k2eAgInSc1d1
</s>
<s>
Tyrosin	Tyrosin	k1gMnSc1
</s>
<s>
Tryptofan	Tryptofan	k1gMnSc1
</s>
<s>
Histidin	histidin	k1gInSc1
Polární	polární	k2eAgInSc1d1
<g/>
,	,	kIx,
nenabité	nabitý	k2eNgFnSc2d1
</s>
<s>
Asparagin	Asparagin	k1gMnSc1
</s>
<s>
Glutamin	Glutamin	k1gInSc1
</s>
<s>
Serin	Serin	k1gMnSc1
</s>
<s>
Threonin	Threonin	k2eAgInSc1d1
Nesoucí	nesoucí	k2eAgInSc1d1
pozitivní	pozitivní	k2eAgInSc1d1
náboj	náboj	k1gInSc1
(	(	kIx(
<g/>
pKa	pKa	k?
<g/>
)	)	kIx)
</s>
<s>
Lysin	lysin	k1gInSc1
(	(	kIx(
<g/>
≈	≈	k?
<g/>
10,8	10,8	k4
<g/>
)	)	kIx)
</s>
<s>
Arginin	Arginin	k2eAgInSc1d1
(	(	kIx(
<g/>
≈	≈	k?
<g/>
12,5	12,5	k4
<g/>
)	)	kIx)
</s>
<s>
Histidin	histidin	k1gInSc1
(	(	kIx(
<g/>
≈	≈	k?
<g/>
6,1	6,1	k4
<g/>
)	)	kIx)
Nesoucí	nesoucí	k2eAgInSc1d1
negativní	negativní	k2eAgInSc1d1
náboj	náboj	k1gInSc1
(	(	kIx(
<g/>
pKa	pKa	k?
<g/>
)	)	kIx)
</s>
<s>
Kyselina	kyselina	k1gFnSc1
asparagová	asparagový	k2eAgFnSc1d1
(	(	kIx(
<g/>
≈	≈	k?
<g/>
3,9	3,9	k4
<g/>
)	)	kIx)
</s>
<s>
Kyselina	kyselina	k1gFnSc1
glutamová	glutamový	k2eAgFnSc1d1
(	(	kIx(
<g/>
≈	≈	k?
<g/>
4,1	4,1	k4
<g/>
)	)	kIx)
Obsahující	obsahující	k2eAgFnSc2d1
síru	síra	k1gFnSc4
</s>
<s>
Cystein	cystein	k1gInSc1
</s>
<s>
Methionin	methionin	k1gInSc1
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1
případy	případ	k1gInPc1
</s>
<s>
Selenocystein	Selenocystein	k1gMnSc1
</s>
<s>
Pyrolysin	Pyrolysin	k1gMnSc1
</s>
<s>
N-formylmethionin	N-formylmethionin	k1gInSc1
Další	další	k2eAgInSc1d1
rozdělení	rozdělení	k1gNnSc4
</s>
<s>
Esenciální	esenciální	k2eAgFnSc1d1
</s>
<s>
Ketogenní	Ketogenní	k2eAgFnSc1d1
</s>
<s>
Glukogenní	Glukogenní	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
