<p>
<s>
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
je	být	k5eAaImIp3nS	být
pop-rocková	popockový	k2eAgFnSc1d1	pop-rocková
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Colorada	Colorado	k1gNnSc2	Colorado
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Timbalandem	Timbalando	k1gNnSc7	Timbalando
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
společné	společný	k2eAgFnSc3d1	společná
písni	píseň	k1gFnSc3	píseň
Apologize	Apologize	k1gFnSc2	Apologize
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Leaderem	leader	k1gMnSc7	leader
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Ryan	Ryan	k1gMnSc1	Ryan
Tedder	Tedder	k1gMnSc1	Tedder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Ryanem	Ryan	k1gMnSc7	Ryan
Tedderem	Tedder	k1gMnSc7	Tedder
a	a	k8xC	a
Zachem	Zach	k1gMnSc7	Zach
Filkinsem	Filkins	k1gMnSc7	Filkins
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
studiu	studio	k1gNnSc6	studio
na	na	k7c4	na
Colorado	Colorado	k1gNnSc4	Colorado
Springs	Springsa	k1gFnPc2	Springsa
Christian	Christian	k1gMnSc1	Christian
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
společné	společný	k2eAgFnSc6d1	společná
cestě	cesta	k1gFnSc6	cesta
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
Tedder	Tedder	k1gInSc1	Tedder
s	s	k7c7	s
Filkinsem	Filkins	k1gInSc7	Filkins
bavili	bavit	k5eAaImAgMnP	bavit
o	o	k7c6	o
svých	svůj	k3xOyFgNnPc6	svůj
oblíbených	oblíbený	k2eAgNnPc6d1	oblíbené
interpretech	interpret	k1gMnPc6	interpret
(	(	kIx(	(
<g/>
Fiona	Fion	k1gMnSc4	Fion
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Gabriel	Gabriel	k1gMnSc1	Gabriel
a	a	k8xC	a
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
založit	založit	k5eAaPmF	založit
společně	společně	k6eAd1	společně
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgInSc1	první
společný	společný	k2eAgInSc1d1	společný
počin	počin	k1gInSc1	počin
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
This	This	k1gInSc1	This
Beautiful	Beautifula	k1gFnPc2	Beautifula
Mess	Messa	k1gFnPc2	Messa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc1	počátek
kariéry	kariéra	k1gFnSc2	kariéra
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jejich	jejich	k3xOp3gFnSc1	jejich
druhá	druhý	k4xOgFnSc1	druhý
kapela	kapela	k1gFnSc1	kapela
Republic	Republice	k1gFnPc2	Republice
<g/>
.	.	kIx.	.
</s>
<s>
Tedder	Tedder	k1gMnSc1	Tedder
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
již	již	k9	již
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
Filkinse	Filkins	k1gMnSc4	Filkins
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Columbia	Columbia	k1gFnSc1	Columbia
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
ustálili	ustálit	k5eAaPmAgMnP	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
Ryan	Ryan	k1gMnSc1	Ryan
Tedder	Tedder	k1gMnSc1	Tedder
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
místo	místo	k1gNnSc4	místo
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
Zach	Zach	k1gMnSc1	Zach
Filkins	Filkinsa	k1gFnPc2	Filkinsa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kytaristou	kytarista	k1gMnSc7	kytarista
a	a	k8xC	a
doprovodným	doprovodný	k2eAgMnSc7d1	doprovodný
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc1	Eddie
Fischer	Fischer	k1gMnSc1	Fischer
bubeníkem	bubeník	k1gMnSc7	bubeník
<g/>
,	,	kIx,	,
Brent	Brent	k?	Brent
Kutzle	Kutzle	k1gMnSc1	Kutzle
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
basu	basa	k1gFnSc4	basa
a	a	k8xC	a
Drew	Drew	k1gFnSc4	Drew
Brown	Browna	k1gFnPc2	Browna
o	o	k7c4	o
hlavní	hlavní	k2eAgFnSc4d1	hlavní
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jméno	jméno	k1gNnSc1	jméno
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c6	na
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
název	název	k1gInSc1	název
Republic	Republice	k1gFnPc2	Republice
mohl	moct	k5eAaImAgInS	moct
působit	působit	k5eAaImF	působit
kontroverzně	kontroverzně	k6eAd1	kontroverzně
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
poté	poté	k6eAd1	poté
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
na	na	k7c4	na
MySpace	MySpace	k1gFnPc4	MySpace
<g/>
.	.	kIx.	.
</s>
<s>
Debutová	debutový	k2eAgFnSc1d1	debutová
deska	deska	k1gFnSc1	deska
Dreaming	Dreaming	k1gInSc1	Dreaming
Out	Out	k1gFnSc2	Out
Loud	louda	k1gFnPc2	louda
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
Stop	stop	k1gInSc1	stop
and	and	k?	and
Stare	star	k1gInSc5	star
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
pestrý	pestrý	k2eAgInSc1d1	pestrý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
směsicí	směsice	k1gFnSc7	směsice
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
popu	pop	k1gInSc2	pop
<g/>
,	,	kIx,	,
indie	indie	k1gFnSc2	indie
a	a	k8xC	a
hip-hopu	hipop	k1gInSc2	hip-hop
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
například	například	k6eAd1	například
U2	U2	k1gFnSc7	U2
a	a	k8xC	a
The	The	k1gFnSc7	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
například	například	k6eAd1	například
Leona	Leona	k1gFnSc1	Leona
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
,	,	kIx,	,
B.o.B	B.o.B	k1gFnSc1	B.o.B
<g/>
,	,	kIx,	,
Timbaland	Timbaland	k1gInSc1	Timbaland
<g/>
,	,	kIx,	,
Sara	Sara	k1gMnSc1	Sara
Bareilles	Bareilles	k1gMnSc1	Bareilles
<g/>
,	,	kIx,	,
Maroon	Maroon	k1gMnSc1	Maroon
5	[number]	k4	5
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
2007	[number]	k4	2007
-	-	kIx~	-
Dreaming	Dreaming	k1gInSc1	Dreaming
Out	Out	k1gFnSc2	Out
Loud	louda	k1gFnPc2	louda
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Say	Say	k?	Say
(	(	kIx(	(
<g/>
All	All	k1gMnSc1	All
I	i	k8xC	i
Need	Need	k1gMnSc1	Need
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mercy	Merca	k1gFnPc1	Merca
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stop	stop	k1gInSc1	stop
and	and	k?	and
Stare	star	k1gInSc5	star
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Apologize	Apologize	k6eAd1	Apologize
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Goodbye	Goodby	k1gInPc1	Goodby
<g/>
,	,	kIx,	,
Apathy	Apath	k1gInPc1	Apath
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
All	All	k?	All
Fall	Fall	k1gMnSc1	Fall
Down	Down	k1gMnSc1	Down
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tyrant	Tyrant	k1gMnSc1	Tyrant
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Prodigal	Prodigat	k5eAaPmAgMnS	Prodigat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
All	All	k?	All
We	We	k1gFnSc1	We
Are	ar	k1gInSc5	ar
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Someone	Someon	k1gMnSc5	Someon
to	ten	k3xDgNnSc1	ten
Save	Sav	k1gMnSc4	Sav
You	You	k1gMnSc4	You
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Come	Come	k1gFnSc1	Come
Home	Home	k1gFnSc1	Home
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
Apologize	Apologize	k1gFnSc2	Apologize
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
zazněla	zaznět	k5eAaImAgFnS	zaznět
v	v	k7c4	v
Cold	Cold	k1gInSc4	Cold
Case	Cas	k1gFnSc2	Cas
<g/>
,	,	kIx,	,
Gossip	Gossip	k1gInSc4	Gossip
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
Smallville	Smallville	k1gNnSc2	Smallville
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gMnSc1	Ghost
Whisperer	Whisperer	k1gMnSc1	Whisperer
a	a	k8xC	a
The	The	k1gMnSc1	The
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2009	[number]	k4	2009
-	-	kIx~	-
Waking	Waking	k1gInSc1	Waking
Up	Up	k1gFnSc2	Up
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Made	Made	k1gFnPc2	Made
for	forum	k1gNnPc2	forum
You	You	k1gMnPc2	You
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
All	All	k?	All
the	the	k?	the
Right	Right	k1gMnSc1	Right
Moves	Moves	k1gMnSc1	Moves
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Secrets	Secrets	k6eAd1	Secrets
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Everybody	Everybod	k1gInPc1	Everybod
Loves	Lovesa	k1gFnPc2	Lovesa
Me	Me	k1gFnPc2	Me
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Missing	Missing	k1gInSc1	Missing
Persons	Persons	k1gInSc1	Persons
1	[number]	k4	1
&	&	k?	&
2	[number]	k4	2
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Good	Good	k1gInSc1	Good
Life	Lif	k1gFnSc2	Lif
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
All	All	k?	All
This	This	k1gInSc1	This
Time	Time	k1gFnSc1	Time
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Fear	Fear	k1gMnSc1	Fear
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Waking	Waking	k1gInSc1	Waking
Up	Up	k1gFnSc2	Up
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Marchin	Marchin	k1gMnSc1	Marchin
On	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lullaby	Lullaba	k1gFnPc1	Lullaba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2013	[number]	k4	2013
-	-	kIx~	-
Native	Natiev	k1gFnSc2	Natiev
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Kids	Kids	k1gInSc1	Kids
<g/>
,	,	kIx,	,
<g/>
Counting	Counting	k1gInSc1	Counting
stars	starsa	k1gFnPc2	starsa
<g/>
,	,	kIx,	,
If	If	k1gFnSc6	If
I	i	k8xC	i
lose	los	k1gInSc6	los
myself	myself	k1gMnSc1	myself
,	,	kIx,	,
Feel	Feel	k1gMnSc1	Feel
again	again	k1gMnSc1	again
,	,	kIx,	,
What	What	k1gMnSc1	What
you	you	k?	you
wanted	wanted	k1gMnSc1	wanted
,	,	kIx,	,
I	i	k9	i
Lived	Lived	k1gMnSc1	Lived
,	,	kIx,	,
Light	Light	k1gMnSc1	Light
it	it	k?	it
up	up	k?	up
,	,	kIx,	,
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
Au	au	k0	au
revoir	revoir	k1gInSc1	revoir
,	,	kIx,	,
Burning	Burning	k1gInSc1	Burning
bridges	bridges	k1gInSc1	bridges
,	,	kIx,	,
Something	Something	k1gInSc1	Something
I	i	k8xC	i
need	need	k1gInSc1	need
<g/>
,	,	kIx,	,
Preacher	Preachra	k1gFnPc2	Preachra
,	,	kIx,	,
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
look	look	k1gMnSc1	look
down	down	k1gMnSc1	down
</s>
</p>
<p>
<s>
Bonus	bonus	k1gInSc1	bonus
<g/>
:	:	kIx,	:
Something	Something	k1gInSc1	Something
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
gotta	gott	k1gInSc2	gott
give	giv	k1gFnSc2	giv
,	,	kIx,	,
Life	Lif	k1gInSc2	Lif
in	in	k?	in
color	color	k1gMnSc1	color
</s>
</p>
<p>
<s>
Re-edice	Redice	k1gFnSc1	Re-edice
<g/>
:	:	kIx,	:
Love	lov	k1gInSc5	lov
Runs	Runs	k1gInSc1	Runs
Out	Out	k1gFnPc7	Out
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
-	-	kIx~	-
Oh	oh	k0	oh
My	my	k3xPp1nPc1	my
my	my	k3xPp1nPc1	my
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hurt	Hurt	k1gMnSc1	Hurt
Tonight	Tonight	k1gMnSc1	Tonight
<g/>
,	,	kIx,	,
Future	Futur	k1gMnSc5	Futur
Looks	Looksa	k1gFnPc2	Looksa
Good	Good	k1gInSc1	Good
<g/>
,	,	kIx,	,
Oh	oh	k0	oh
My	my	k3xPp1nPc1	my
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Kids	Kids	k1gInSc1	Kids
<g/>
,	,	kIx,	,
Dream	Dream	k1gInSc1	Dream
<g/>
,	,	kIx,	,
Choke	Choke	k1gFnSc1	Choke
<g/>
,	,	kIx,	,
A.I.	A.I.	k1gFnSc1	A.I.
<g/>
,	,	kIx,	,
Better	Better	k1gInSc1	Better
<g/>
,	,	kIx,	,
Born	Born	k1gInSc1	Born
<g/>
,	,	kIx,	,
Fingertips	Fingertips	k1gInSc1	Fingertips
<g/>
,	,	kIx,	,
Human	Human	k1gInSc1	Human
<g/>
,	,	kIx,	,
Lift	lift	k1gInSc1	lift
Me	Me	k1gMnSc1	Me
Up	Up	k1gMnSc1	Up
<g/>
,	,	kIx,	,
NbHD	NbHD	k1gMnSc1	NbHD
<g/>
,	,	kIx,	,
Wherever	Wherever	k1gMnSc1	Wherever
I	i	k8xC	i
Go	Go	k1gMnSc1	Go
<g/>
,	,	kIx,	,
All	All	k1gMnSc1	All
These	these	k1gFnSc2	these
Things	Things	k1gInSc1	Things
<g/>
,	,	kIx,	,
Heaven	Heaven	k2eAgInSc1d1	Heaven
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
Apologize	Apologize	k1gFnSc1	Apologize
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Timbaland	Timbaland	k1gInSc1	Timbaland
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
All	All	k?	All
Fall	Fall	k1gMnSc1	Fall
Down	Down	k1gMnSc1	Down
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
If	If	k?	If
I	i	k8xC	i
lose	los	k1gInSc6	los
Myself	Myself	k1gInSc1	Myself
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Counting	Counting	k1gInSc1	Counting
Stars	Stars	k1gInSc1	Stars
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Love	lov	k1gInSc5	lov
Runs	Runs	k1gInSc4	Runs
Out	Out	k1gMnSc4	Out
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
Lived	Lived	k1gInSc1	Lived
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Love	lov	k1gInSc5	lov
runs	runs	k1gInSc1	runs
out	out	k?	out
</s>
</p>
<p>
<s>
Something	Something	k1gInSc1	Something
I	i	k9	i
need	ed	k6eNd1	ed
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Good	Good	k6eAd1	Good
life	lifat	k5eAaPmIp3nS	lifat
</s>
</p>
<p>
<s>
Wherever	Wherever	k1gMnSc1	Wherever
I	i	k8xC	i
Go	Go	k1gMnSc1	Go
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kids	Kids	k1gInSc1	Kids
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
No	no	k9	no
Vacancy	Vacanca	k1gFnPc1	Vacanca
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vše	všechen	k3xTgNnSc4	všechen
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
OneRepublic	OneRepublice	k1gFnPc2	OneRepublice
</s>
</p>
