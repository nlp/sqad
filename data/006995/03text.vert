<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
rallye	rallye	k1gFnSc1	rallye
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc7	třetí
soutěží	soutěž	k1gFnSc7	soutěž
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gNnSc6	rallye
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
Markku	Markka	k1gFnSc4	Markka
Alen	Alena	k1gFnPc2	Alena
s	s	k7c7	s
lancií	lancie	k1gFnSc7	lancie
Deltou	delta	k1gFnSc7	delta
HF	HF	kA	HF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
testu	test	k1gInSc6	test
se	se	k3xPyFc4	se
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
pořadí	pořadí	k1gNnSc1	pořadí
Juha	Juhum	k1gNnSc2	Juhum
Kankkunen	Kankkunna	k1gFnPc2	Kankkunna
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
Lancia	Lancia	k1gFnSc1	Lancia
Delta	delta	k1gNnSc2	delta
HF	HF	kA	HF
<g/>
,	,	kIx,	,
Timo	Timo	k6eAd1	Timo
Salonen	Salonen	k1gInSc1	Salonen
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Mazda	Mazda	k1gFnSc1	Mazda
323	[number]	k4	323
4	[number]	k4	4
<g/>
WD	WD	kA	WD
<g/>
,	,	kIx,	,
Markku	Markko	k1gNnSc6	Markko
Alen	Alena	k1gFnPc2	Alena
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
Lancií	Lancia	k1gFnSc7	Lancia
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Ragnotti	Ragnott	k1gMnPc1	Ragnott
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Renault	renault	k1gInSc4	renault
11	[number]	k4	11
Turbo	turba	k1gFnSc5	turba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
Kenneth	Kenneth	k1gMnSc1	Kenneth
Eriksson	Eriksson	k1gMnSc1	Eriksson
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
Volkswagen	volkswagen	k1gInSc1	volkswagen
Golf	golf	k1gInSc1	golf
II	II	kA	II
GTI	GTI	kA	GTI
16	[number]	k4	16
<g/>
V.	V.	kA	V.
Miki	Mike	k1gFnSc4	Mike
Biasion	Biasion	k1gInSc1	Biasion
měl	mít	k5eAaImAgInS	mít
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
Lancie	Lancia	k1gFnSc2	Lancia
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
palivovým	palivový	k2eAgNnSc7d1	palivové
čerpadlem	čerpadlo	k1gNnSc7	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
časem	čas	k1gInSc7	čas
zahájil	zahájit	k5eAaPmAgInS	zahájit
soutěž	soutěž	k1gFnSc4	soutěž
druhý	druhý	k4xOgInSc4	druhý
jezdec	jezdec	k1gInSc4	jezdec
Renaultu	renault	k1gInSc2	renault
Francois	Francois	k1gMnSc1	Francois
Chatriot	Chatriot	k1gMnSc1	Chatriot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
soutěže	soutěž	k1gFnSc2	soutěž
jeho	jeho	k3xOp3gInSc1	jeho
vůz	vůz	k1gInSc1	vůz
nestačil	stačit	k5eNaBmAgInS	stačit
speciálům	speciál	k1gInPc3	speciál
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
všech	všecek	k3xTgFnPc2	všecek
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
Golfu	golf	k1gInSc6	golf
jel	jet	k5eAaImAgMnS	jet
Ervin	Ervin	k1gMnSc1	Ervin
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
první	první	k4xOgFnSc6	první
etapě	etapa	k1gFnSc6	etapa
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
i	i	k9	i
Ingvar	Ingvar	k1gInSc1	Ingvar
Carlsson	Carlssona	k1gFnPc2	Carlssona
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
Mazdě	Mazda	k1gFnSc6	Mazda
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
testu	test	k1gInSc6	test
získal	získat	k5eAaPmAgMnS	získat
mladý	mladý	k2eAgMnSc1d1	mladý
jezdec	jezdec	k1gMnSc1	jezdec
Carlos	Carlos	k1gMnSc1	Carlos
Sainz	Sainz	k1gMnSc1	Sainz
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Ford	ford	k1gInSc4	ford
Sierra	Sierra	k1gFnSc1	Sierra
Cosworth	Cosworth	k1gInSc1	Cosworth
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ten	ten	k3xDgMnSc1	ten
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
odstoupit	odstoupit	k5eAaPmF	odstoupit
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
turbodmychadlem	turbodmychadlo	k1gNnSc7	turbodmychadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
udržoval	udržovat	k5eAaImAgInS	udržovat
vedení	vedení	k1gNnSc4	vedení
Alen	Alena	k1gFnPc2	Alena
<g/>
.	.	kIx.	.
</s>
<s>
Kankkunena	Kankkunena	k1gFnSc1	Kankkunena
a	a	k8xC	a
Biasiona	Biasiona	k1gFnSc1	Biasiona
trápily	trápit	k5eAaImAgInP	trápit
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
tlumiči	tlumič	k1gInPc7	tlumič
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
byl	být	k5eAaImAgMnS	být
Ragnotti	Ragnotti	k1gNnSc4	Ragnotti
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
po	po	k7c6	po
defektu	defekt	k1gInSc6	defekt
Kankkunen	Kankkunen	k1gInSc1	Kankkunen
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
pozici	pozice	k1gFnSc4	pozice
držel	držet	k5eAaImAgMnS	držet
Eriksson	Eriksson	k1gMnSc1	Eriksson
<g/>
.	.	kIx.	.
</s>
<s>
Porucha	porucha	k1gFnSc1	porucha
převodovky	převodovka	k1gFnSc2	převodovka
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
také	také	k9	také
Salonena	Salonen	k2eAgFnSc1d1	Salonen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
dešti	dešť	k1gInSc6	dešť
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
držel	držet	k5eAaImAgMnS	držet
Alen	Alena	k1gFnPc2	Alena
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
byl	být	k5eAaImAgMnS	být
Ragnotti	Ragnotti	k1gNnSc4	Ragnotti
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
Eriksson	Eriksson	k1gNnSc4	Eriksson
<g/>
.	.	kIx.	.
</s>
<s>
Biasion	Biasion	k1gInSc1	Biasion
měl	mít	k5eAaImAgInS	mít
poruchu	porucha	k1gFnSc4	porucha
palivového	palivový	k2eAgNnSc2d1	palivové
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
etapě	etapa	k1gFnSc6	etapa
hlídal	hlídat	k5eAaImAgMnS	hlídat
Alen	Alena	k1gFnPc2	Alena
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
náskok	náskok	k1gInSc4	náskok
přes	přes	k7c4	přes
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Alen	Alena	k1gFnPc2	Alena
měl	mít	k5eAaImAgInS	mít
ale	ale	k8xC	ale
prasklý	prasklý	k2eAgInSc4d1	prasklý
tlumič	tlumič	k1gInSc4	tlumič
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
náskoku	náskok	k1gInSc2	náskok
ztratil	ztratit	k5eAaPmAgInS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
ragnotti	ragnott	k5eAaImF	ragnott
a	a	k8xC	a
třetí	třetí	k4xOgNnSc1	třetí
Eriksson	Eriksson	k1gNnSc1	Eriksson
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
pozici	pozice	k1gFnSc4	pozice
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgInS	probojovat
Biasion	Biasion	k1gInSc1	Biasion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
utočil	utočit	k5eAaPmAgInS	utočit
na	na	k7c4	na
Erikssona	Eriksson	k1gMnSc4	Eriksson
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
byl	být	k5eAaImAgInS	být
Kankkunen	Kankkunen	k2eAgMnSc1d1	Kankkunen
a	a	k8xC	a
šestý	šestý	k4xOgMnSc1	šestý
Chatriot	Chatriot	k1gMnSc1	Chatriot
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
etapě	etapa	k1gFnSc6	etapa
rostly	růst	k5eAaImAgFnP	růst
Alenovi	Alenův	k2eAgMnPc1d1	Alenův
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
tlumiči	tlumič	k1gInPc7	tlumič
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
náskok	náskok	k1gInSc4	náskok
na	na	k7c4	na
Ragnottiho	Ragnotti	k1gMnSc2	Ragnotti
se	se	k3xPyFc4	se
zmenšoval	zmenšovat	k5eAaImAgMnS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Eriksson	Eriksson	k1gMnSc1	Eriksson
stále	stále	k6eAd1	stále
držel	držet	k5eAaImAgMnS	držet
třetí	třetí	k4xOgFnSc4	třetí
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
Kankkunen	Kankkunen	k1gInSc4	Kankkunen
a	a	k8xC	a
Chatriot	Chatriot	k1gInSc4	Chatriot
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
držely	držet	k5eAaImAgInP	držet
dva	dva	k4xCgInPc1	dva
vozy	vůz	k1gInPc1	vůz
Audi	Audi	k1gNnSc2	Audi
Quattro	Quattro	k1gNnSc4	Quattro
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
Rudi	ruď	k1gFnPc1	ruď
Stohl	Stohl	k1gFnSc2	Stohl
Markku	Markk	k1gInSc2	Markk
Alen	Alena	k1gFnPc2	Alena
<g/>
,	,	kIx,	,
Kivimaki	Kivimaki	k1gNnSc1	Kivimaki
-	-	kIx~	-
Lancia	Lancia	k1gFnSc1	Lancia
Delta	delta	k1gNnSc2	delta
HF	HF	kA	HF
Jean	Jean	k1gMnSc1	Jean
Ragnotti	Ragnotť	k1gFnSc2	Ragnotť
<g/>
,	,	kIx,	,
Thimonier	Thimonier	k1gMnSc1	Thimonier
-	-	kIx~	-
Renault	renault	k1gInSc1	renault
11	[number]	k4	11
Turbo	turba	k1gFnSc5	turba
Kenneth	Kenneth	k1gMnSc1	Kenneth
Eriksson	Eriksson	k1gMnSc1	Eriksson
<g/>
,	,	kIx,	,
Dietman	Dietman	k1gMnSc1	Dietman
-	-	kIx~	-
Volkswagen	volkswagen	k1gInSc1	volkswagen
Golf	golf	k1gInSc1	golf
II	II	kA	II
GTI	GTI	kA	GTI
16V	[number]	k4	16V
Juha	Juh	k1gInSc2	Juh
Kankkunen	Kankkunen	k1gInSc1	Kankkunen
<g/>
,	,	kIx,	,
Piironen	Piironen	k1gInSc1	Piironen
-	-	kIx~	-
Lancia	Lancia	k1gFnSc1	Lancia
<g />
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
HF	HF	kA	HF
Francois	Francois	k1gMnSc1	Francois
Chatriot	Chatriot	k1gMnSc1	Chatriot
<g/>
,	,	kIx,	,
Perin	Perin	k1gMnSc1	Perin
-	-	kIx~	-
Renault	renault	k1gInSc1	renault
11	[number]	k4	11
Turbo	turba	k1gFnSc5	turba
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Zeltner	Zeltner	k1gMnSc1	Zeltner
-	-	kIx~	-
Audi	Audi	k1gNnSc1	Audi
Quattro	Quattro	k1gNnSc1	Quattro
Rudi	ruď	k1gFnSc2	ruď
Stohl	Stohl	k1gFnSc2	Stohl
<g/>
,	,	kIx,	,
Kaufmann	Kaufmann	k1gNnSc1	Kaufmann
-	-	kIx~	-
Audi	Audi	k1gNnSc1	Audi
Quattro	Quattro	k1gNnSc1	Quattro
Miki	Mik	k1gFnSc2	Mik
Biasion	Biasion	k1gInSc1	Biasion
<g/>
,	,	kIx,	,
Siviero	Siviero	k1gNnSc1	Siviero
-	-	kIx~	-
Lancia	Lancia	k1gFnSc1	Lancia
Delta	delta	k1gNnSc2	delta
HF	HF	kA	HF
Santos	Santos	k1gMnSc1	Santos
<g/>
,	,	kIx,	,
Oliviera	Oliviera	k1gFnSc1	Oliviera
-	-	kIx~	-
Ford	ford	k1gInSc1	ford
Sierra	Sierra	k1gFnSc1	Sierra
Cosworth	Cosworth	k1gMnSc1	Cosworth
Jorge	Jorgus	k1gMnSc5	Jorgus
Recalde	Recald	k1gMnSc5	Recald
<g/>
,	,	kIx,	,
Del	Del	k1gMnPc4	Del
Buono	Buono	k6eAd1	Buono
-	-	kIx~	-
Fiat	fiat	k1gInSc1	fiat
Uno	Uno	k1gFnSc2	Uno
Turbo	turba	k1gFnSc5	turba
</s>
