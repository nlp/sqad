<p>
<s>
Grø	Grø	k?	Grø
Seminarius	Seminarius	k1gInSc1	Seminarius
SK	Sk	kA	Sk
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Timersoqatigiiffik	Timersoqatigiiffika	k1gFnPc2	Timersoqatigiiffika
Sportsforeningen	Sportsforeningen	k1gInSc1	Sportsforeningen
Grø	Grø	k1gMnSc1	Grø
Seminarius	Seminarius	k1gMnSc1	Seminarius
Sportklub	Sportklub	k1gMnSc1	Sportklub
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
grónský	grónský	k2eAgInSc1d1	grónský
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nuuk	Nuuka	k1gFnPc2	Nuuka
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Godthå	Godthå	k1gFnSc1	Godthå
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
letopočet	letopočet	k1gInSc1	letopočet
vzniku	vznik	k1gInSc3	vznik
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
klubovém	klubový	k2eAgInSc6d1	klubový
emblému	emblém	k1gInSc6	emblém
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1	klubová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
<g/>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
oddíl	oddíl	k1gInSc1	oddíl
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
účastní	účastnit	k5eAaImIp3nS	účastnit
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
mužský	mužský	k2eAgInSc1d1	mužský
oddíl	oddíl	k1gInSc1	oddíl
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pětinásobným	pětinásobný	k2eAgMnSc7d1	pětinásobný
mistrem	mistr	k1gMnSc7	mistr
Grónska	Grónsko	k1gNnSc2	Grónsko
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
oddíl	oddíl	k1gInSc1	oddíl
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Nuuk	Nuuk	k1gInSc1	Nuuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
2	[number]	k4	2
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
házené	házená	k1gFnSc2	házená
je	být	k5eAaImIp3nS	být
čtyřnásobným	čtyřnásobný	k2eAgMnSc7d1	čtyřnásobný
mistrem	mistr	k1gMnSc7	mistr
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
oddílem	oddíl	k1gInSc7	oddíl
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
volejbalový	volejbalový	k2eAgInSc1d1	volejbalový
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
národních	národní	k2eAgInPc2d1	národní
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
jedenáctkrát	jedenáctkrát	k6eAd1	jedenáctkrát
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
třináctkrát	třináctkrát	k6eAd1	třináctkrát
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k6eAd1	mimo
mužský	mužský	k2eAgInSc1d1	mužský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
oddíl	oddíl	k1gInSc1	oddíl
má	mít	k5eAaImIp3nS	mít
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
oddíl	oddíl	k1gInSc1	oddíl
ženského	ženský	k2eAgInSc2d1	ženský
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
házené	házená	k1gFnSc2	házená
a	a	k8xC	a
volejbalu	volejbal	k1gInSc2	volejbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Získané	získaný	k2eAgFnPc4d1	získaná
trofeje	trofej	k1gFnPc4	trofej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fotbal	fotbal	k1gInSc1	fotbal
===	===	k?	===
</s>
</p>
<p>
<s>
Angutit	Angutit	k5eAaPmF	Angutit
Inersimasut	Inersimasut	k1gInSc4	Inersimasut
GM	GM	kA	GM
(	(	kIx(	(
5	[number]	k4	5
<g/>
x	x	k?	x
)	)	kIx)	)
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
===	===	k?	===
Házená	házený	k2eAgNnPc4d1	házené
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
4	[number]	k4	4
<g/>
x	x	k?	x
)	)	kIx)	)
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
===	===	k?	===
Volejbal	volejbal	k1gInSc4	volejbal
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
muži	muž	k1gMnSc6	muž
(	(	kIx(	(
11	[number]	k4	11
<g/>
x	x	k?	x
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
13	[number]	k4	13
<g/>
x	x	k?	x
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc4	bod
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
podbarvení	podbarvení	k1gNnSc4	podbarvení
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
bronzové	bronzový	k2eAgNnSc1d1	bronzové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
fázím	fáze	k1gFnPc3	fáze
grónského	grónský	k2eAgInSc2d1	grónský
mistrovství	mistrovství	k1gNnSc6	mistrovství
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
<g/>
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
národního	národní	k2eAgNnSc2d1	národní
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
mužstvem	mužstvo	k1gNnSc7	mužstvo
Kissaviarsuk-	Kissaviarsuk-	k1gFnSc2	Kissaviarsuk-
<g/>
33	[number]	k4	33
neznámým	známý	k2eNgInSc7d1	neznámý
poměrem	poměr	k1gInSc7	poměr
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
národního	národní	k2eAgNnSc2d1	národní
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mužstvu	mužstvo	k1gNnSc3	mužstvo
Kissaviarsuk-	Kissaviarsuk-	k1gFnSc7	Kissaviarsuk-
<g/>
33	[number]	k4	33
neznámým	známý	k2eNgInSc7d1	neznámý
poměrem	poměr	k1gInSc7	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
národního	národní	k2eAgNnSc2d1	národní
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mužstvu	mužstvo	k1gNnSc3	mužstvo
Kissaviarsuk-	Kissaviarsuk-	k1gFnSc7	Kissaviarsuk-
<g/>
33	[number]	k4	33
poměrem	poměr	k1gInSc7	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mužstvu	mužstvo	k1gNnSc3	mužstvo
Siumut	Siumut	k2eAgInSc4d1	Siumut
Amerdlok	Amerdlok	k1gInSc4	Amerdlok
Kunuk	Kunuka	k1gFnPc2	Kunuka
poměrem	poměr	k1gInSc7	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
:	:	kIx,	:
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
klub	klub	k1gInSc1	klub
skončil	skončit	k5eAaPmAgInS	skončit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
skupiny	skupina	k1gFnSc2	skupina
Midtgrø	Midtgrø	k1gFnSc2	Midtgrø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mužstvu	mužstvo	k1gNnSc3	mužstvo
Siumut	Siumut	k2eAgInSc4d1	Siumut
Amerdlok	Amerdlok	k1gInSc4	Amerdlok
Kunuk	Kunuka	k1gFnPc2	Kunuka
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
klub	klub	k1gInSc1	klub
skončil	skončit	k5eAaPmAgInS	skončit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
skupiny	skupina	k1gFnSc2	skupina
Nuuk	Nuuka	k1gFnPc2	Nuuka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mužstvu	mužstvo	k1gNnSc6	mužstvo
B-67	B-67	k1gFnSc2	B-67
neznámým	známý	k2eNgInSc7d1	neznámý
poměrem	poměr	k1gInSc7	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
konečné	konečný	k2eAgNnSc1d1	konečné
umístění	umístění	k1gNnSc1	umístění
finálové	finálový	k2eAgFnSc2d1	finálová
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgNnSc6d1	celkové
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
konečné	konečný	k2eAgNnSc1d1	konečné
umístění	umístění	k1gNnSc1	umístění
finálové	finálový	k2eAgFnSc2d1	finálová
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
konečné	konečný	k2eAgNnSc1d1	konečné
umístění	umístění	k1gNnSc1	umístění
finálové	finálový	k2eAgFnSc2d1	finálová
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
třetí	třetí	k4xOgInSc4	třetí
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
konečné	konečný	k2eAgNnSc1d1	konečné
umístění	umístění	k1gNnSc1	umístění
finálové	finálový	k2eAgFnSc2d1	finálová
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
konečné	konečný	k2eAgNnSc1d1	konečné
umístění	umístění	k1gNnSc1	umístění
finálové	finálový	k2eAgFnSc2d1	finálová
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
pátý	pátý	k4xOgInSc4	pátý
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
zaručil	zaručit	k5eAaPmAgMnS	zaručit
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
postup	postup	k1gInSc4	postup
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
nepostupovém	postupový	k2eNgNnSc6d1	nepostupové
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Midtgrø	Midtgrø	k1gFnSc2	Midtgrø
B.	B.	kA	B.
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
zaručil	zaručit	k5eAaPmAgMnS	zaručit
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
postup	postup	k1gInSc4	postup
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
nepostupovém	postupový	k2eNgNnSc6d1	nepostupové
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Midtgrø	Midtgrø	k1gFnSc2	Midtgrø
A.	A.	kA	A.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
weltfussballarchiv	weltfussballarchiv	k6eAd1	weltfussballarchiv
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
