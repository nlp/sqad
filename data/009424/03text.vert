<p>
<s>
Scott	Scott	k1gMnSc1	Scott
Ramon	Ramona	k1gFnPc2	Ramona
Seguro	Segura	k1gFnSc5	Segura
Mescudi	Mescud	k1gMnPc1	Mescud
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Kid	Kid	k1gFnSc2	Kid
Cudi	Cud	k1gFnSc2	Cud
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
KiD	KiD	k1gMnPc7	KiD
CuDi	CuD	k1gFnSc2	CuD
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
WZRD	WZRD	kA	WZRD
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
2	[number]	k4	2
Be	Be	k1gFnPc2	Be
Continuum	Continuum	k1gInSc4	Continuum
a	a	k8xC	a
Wizard	Wizard	k1gInSc4	Wizard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
hudební	hudební	k2eAgFnSc2d1	hudební
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Wicked	Wicked	k1gMnSc1	Wicked
Awesome	Awesom	k1gInSc5	Awesom
Records	Records	k1gInSc4	Records
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
byl	být	k5eAaImAgInS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
labelu	label	k1gInSc2	label
Dream	Dream	k1gInSc1	Dream
On	on	k3xPp3gInSc1	on
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
Ohio	Ohio	k1gNnSc4	Ohio
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c4	v
clevelandských	clevelandský	k2eAgInPc2d1	clevelandský
předměstí	předměstí	k1gNnSc4	předměstí
Shaker	Shakra	k1gFnPc2	Shakra
Heights	Heightsa	k1gFnPc2	Heightsa
a	a	k8xC	a
Solon	Solona	k1gFnPc2	Solona
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Američan	Američan	k1gMnSc1	Američan
mexického	mexický	k2eAgInSc2d1	mexický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Afroameričanka	Afroameričanka	k1gFnSc1	Afroameričanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
Scottovu	Scottův	k2eAgFnSc4d1	Scottova
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Shaker	Shakra	k1gFnPc2	Shakra
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Solon	Solona	k1gFnPc2	Solona
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
zakončil	zakončit	k5eAaPmAgInS	zakončit
všeobecným	všeobecný	k2eAgInSc7d1	všeobecný
srovnávacím	srovnávací	k2eAgInSc7d1	srovnávací
testem	test	k1gInSc7	test
GED	GED	kA	GED
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
studoval	studovat	k5eAaImAgInS	studovat
filmovou	filmový	k2eAgFnSc4d1	filmová
školu	škola	k1gFnSc4	škola
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Toledo	Toledo	k1gNnSc1	Toledo
<g/>
,	,	kIx,	,
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
rapováním	rapování	k1gNnSc7	rapování
započal	započnout	k5eAaPmAgInS	započnout
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
alternativní	alternativní	k2eAgFnPc1d1	alternativní
hip-hopové	hipopový	k2eAgFnPc1d1	hip-hopová
skupiny	skupina	k1gFnPc1	skupina
The	The	k1gFnSc2	The
Pharcyde	Pharcyd	k1gMnSc5	Pharcyd
a	a	k8xC	a
A	a	k9	a
Tribe	Trib	k1gInSc5	Trib
Called	Called	k1gMnSc1	Called
Quest	Quest	k1gFnSc1	Quest
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
newyorské	newyorský	k2eAgFnSc2d1	newyorská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
branži	branže	k1gFnSc6	branže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgInS	vydat
nezávislý	závislý	k2eNgInSc1d1	nezávislý
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
A	a	k8xC	a
Kid	Kid	k1gMnSc3	Kid
Named	Named	k1gMnSc1	Named
Cudi	Cud	k1gMnSc3	Cud
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
zájem	zájem	k1gInSc4	zájem
u	u	k7c2	u
rappera	rapper	k1gMnSc2	rapper
a	a	k8xC	a
hudebního	hudební	k2eAgMnSc2d1	hudební
producenta	producent	k1gMnSc2	producent
Kanye	Kany	k1gMnSc2	Kany
Westa	West	k1gMnSc2	West
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gMnSc1	West
ho	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
upsal	upsat	k5eAaPmAgMnS	upsat
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
GOOD	GOOD	kA	GOOD
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
Westovu	Westův	k2eAgFnSc4d1	Westova
albu	alba	k1gFnSc4	alba
808	[number]	k4	808
<g/>
s	s	k7c7	s
&	&	k?	&
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc4	ten
Heartbreak	Heartbreak	k1gMnSc1	Heartbreak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ihned	ihned	k6eAd1	ihned
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
debutovém	debutový	k2eAgNnSc6d1	debutové
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Day	Day	k1gFnSc1	Day
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Nite	nit	k1gInSc5	nit
<g/>
"	"	kIx"	"
vydána	vydán	k2eAgFnSc1d1	vydána
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
již	již	k6eAd1	již
na	na	k7c6	na
zmiňovaném	zmiňovaný	k2eAgInSc6d1	zmiňovaný
mixtapu	mixtap	k1gInSc6	mixtap
A	a	k9	a
Kid	Kid	k1gMnSc1	Kid
Named	Named	k1gMnSc1	Named
Cudi	Cudi	k1gNnPc2	Cudi
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc4	píseň
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
v	v	k7c4	v
USA	USA	kA	USA
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zabodovala	zabodovat	k5eAaPmAgFnS	zabodovat
i	i	k9	i
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
UK	UK	kA	UK
Singles	Singlesa	k1gFnPc2	Singlesa
Chart	charta	k1gFnPc2	charta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Man	mana	k1gFnPc2	mana
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
End	End	k1gFnPc2	End
of	of	k?	of
Day	Day	k1gFnPc2	Day
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
týden	týden	k1gInSc1	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
104	[number]	k4	104
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
lehce	lehko	k6eAd1	lehko
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
certifikaci	certifikace	k1gFnSc4	certifikace
zlatá	zlatá	k1gFnSc1	zlatá
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgInSc2d1	další
úspěchu	úspěch	k1gInSc2	úspěch
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
jen	jen	k9	jen
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
singly	singl	k1gInPc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Make	Make	k1gNnSc1	Make
Her	hra	k1gFnPc2	hra
Say	Say	k1gFnPc2	Say
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kanye	Kanye	k1gFnSc1	Kanye
West	Westa	k1gFnPc2	Westa
a	a	k8xC	a
Common	Commona	k1gFnPc2	Commona
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pursuit	Pursuit	k2eAgInSc4d1	Pursuit
of	of	k?	of
Hapiness	Hapiness	k1gInSc4	Hapiness
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
MGMT	MGMT	kA	MGMT
a	a	k8xC	a
Ratatat	Ratatat	k1gFnSc2	Ratatat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
album	album	k1gNnSc4	album
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
Blueprint	Blueprint	k1gInSc4	Blueprint
3	[number]	k4	3
rapového	rapový	k2eAgMnSc2d1	rapový
velikána	velikán	k1gMnSc2	velikán
Jay-Z	Jay-Z	k1gMnSc2	Jay-Z
<g/>
,	,	kIx,	,
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Already	Alreada	k1gFnSc2	Alreada
Home	Hom	k1gFnSc2	Hom
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
mezinárodně	mezinárodně	k6eAd1	mezinárodně
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
singlu	singl	k1gInSc6	singl
DJe	DJe	k1gMnSc2	DJe
Davida	David	k1gMnSc2	David
Guetty	Guetta	k1gMnSc2	Guetta
"	"	kIx"	"
<g/>
Memories	Memories	k1gInSc1	Memories
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
II	II	kA	II
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
reedici	reedice	k1gFnSc6	reedice
alba	album	k1gNnSc2	album
Snoop	Snoop	k1gInSc1	Snoop
Dogga	Dogg	k1gMnSc2	Dogg
More	mor	k1gInSc5	mor
Malice	Malika	k1gFnSc6	Malika
<g/>
,	,	kIx,	,
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
That	That	k2eAgInSc1d1	That
Tree	Tree	k1gInSc1	Tree
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
během	během	k7c2	během
roku	rok	k1gInSc2	rok
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
albu	album	k1gNnSc6	album
s	s	k7c7	s
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Cudder	Cudder	k1gInSc1	Cudder
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Revolution	Revolution	k1gInSc1	Revolution
of	of	k?	of
Evolution	Evolution	k1gInSc1	Evolution
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jediný	jediný	k2eAgInSc1d1	jediný
oficiální	oficiální	k2eAgInSc1d1	oficiální
single	singl	k1gInSc5	singl
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
"	"	kIx"	"
<g/>
Erase	Erase	k1gFnSc1	Erase
Me	Me	k1gFnSc1	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kanye	Kanye	k1gFnSc1	Kanye
West	Westa	k1gFnPc2	Westa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Finálním	finální	k2eAgInSc7d1	finální
názvem	název	k1gInSc7	název
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Man	mana	k1gFnPc2	mana
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
II	II	kA	II
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Rager	Rager	k1gInSc1	Rager
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
týden	týden	k1gInSc1	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
169	[number]	k4	169
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
pozicích	pozice	k1gFnPc6	pozice
žebříčků	žebříček	k1gInPc2	žebříček
Top	topit	k5eAaImRp2nS	topit
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
Hip-Hop	Hip-Hop	k1gInSc1	Hip-Hop
Albums	Albumsa	k1gFnPc2	Albumsa
a	a	k8xC	a
Top	topit	k5eAaImRp2nS	topit
Rap	rap	k1gMnSc1	rap
Albums	Albums	k1gInSc1	Albums
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
alba	alba	k1gFnSc1	alba
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
430	[number]	k4	430
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
i	i	k8xC	i
promo	promo	k6eAd1	promo
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Marijuana	Marijuana	k1gFnSc1	Marijuana
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Rager	Rager	k1gInSc1	Rager
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
také	také	k9	také
hostoval	hostovat	k5eAaImAgInS	hostovat
s	s	k7c7	s
Kanye	Kanye	k1gInSc1	Kanye
Westem	West	k1gInSc7	West
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
rappera	rapper	k1gMnSc2	rapper
T.I.	T.I.	k1gMnSc2	T.I.
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
No	no	k9	no
Mercy	Merca	k1gFnPc1	Merca
a	a	k8xC	a
na	na	k7c6	na
albu	album	k1gNnSc6	album
Kanyeho	Kanye	k1gMnSc2	Kanye
Westa	West	k1gMnSc2	West
My	my	k3xPp1nPc1	my
Beautiful	Beautifula	k1gFnPc2	Beautifula
Dark	Dark	k1gMnSc1	Dark
Twisted	Twisted	k1gMnSc1	Twisted
Fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
na	na	k7c6	na
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
singlu	singl	k1gInSc6	singl
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
of	of	k?	of
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Gorgeous	Gorgeous	k1gInSc1	Gorgeous
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
WZRD	WZRD	kA	WZRD
a	a	k8xC	a
indicud	indicud	k1gMnSc1	indicud
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
chtěl	chtít	k5eAaImAgMnS	chtít
vydat	vydat	k5eAaPmF	vydat
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
A	a	k9	a
Man	mana	k1gFnPc2	mana
Named	Named	k1gMnSc1	Named
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
úspěchu	úspěch	k1gInSc6	úspěch
jeho	jeho	k3xOp3gInSc2	jeho
prvního	první	k4xOgInSc2	první
mixtapu	mixtap	k1gInSc2	mixtap
A	a	k9	a
Kid	Kid	k1gMnSc1	Kid
Named	Named	k1gMnSc1	Named
Cudi	Cude	k1gFnSc4	Cude
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
zrušil	zrušit	k5eAaPmAgInS	zrušit
vydání	vydání	k1gNnSc4	vydání
mixtapu	mixtap	k1gInSc2	mixtap
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
debutové	debutový	k2eAgNnSc1d1	debutové
pop	pop	k1gInSc4	pop
rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
dua	duo	k1gNnSc2	duo
WZRD	WZRD	kA	WZRD
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
producent	producent	k1gMnSc1	producent
Dot	Dot	k1gMnSc1	Dot
da	da	k?	da
Genius	genius	k1gMnSc1	genius
<g/>
.	.	kIx.	.
</s>
<s>
Stejnojmenné	stejnojmenný	k2eAgNnSc1d1	stejnojmenné
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
s	s	k7c7	s
66	[number]	k4	66
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
84	[number]	k4	84
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
původně	původně	k6eAd1	původně
chystal	chystat	k5eAaImAgMnS	chystat
pokračování	pokračování	k1gNnSc4	pokračování
své	svůj	k3xOyFgFnSc2	svůj
série	série	k1gFnSc2	série
"	"	kIx"	"
<g/>
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
tři	tři	k4xCgNnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
ponese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
indicud	indicud	k6eAd1	indicud
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
společném	společný	k2eAgNnSc6d1	společné
kompilačním	kompilační	k2eAgNnSc6d1	kompilační
albu	album	k1gNnSc6	album
hudebního	hudební	k2eAgNnSc2d1	hudební
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
GOOD	GOOD	kA	GOOD
Music	Music	k1gMnSc1	Music
Cruel	Cruel	k1gMnSc1	Cruel
Summer	Summer	k1gMnSc1	Summer
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
prostor	prostor	k1gInSc4	prostor
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Morning	Morning	k1gInSc1	Morning
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Creepers	Creepers	k1gInSc1	Creepers
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
indicud	indicud	k6eAd1	indicud
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
jako	jako	k8xC	jako
standardní	standardní	k2eAgNnSc4d1	standardní
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
solo	solo	k1gNnSc4	solo
album	album	k1gNnSc4	album
u	u	k7c2	u
labelu	label	k1gInSc2	label
Republic	Republice	k1gFnPc2	Republice
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
What	What	k2eAgInSc1d1	What
I	i	k9	i
Am	Am	k1gMnPc2	Am
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
74	[number]	k4	74
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Immortal	Immortal	k1gFnSc1	Immortal
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Girls	girl	k1gFnPc4	girl
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
nezabodovaly	zabodovat	k5eNaPmAgFnP	zabodovat
<g/>
.	.	kIx.	.
</s>
<s>
Promo	Promo	k6eAd1	Promo
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
King	King	k1gMnSc1	King
Wizard	Wizard	k1gMnSc1	Wizard
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
91	[number]	k4	91
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
136	[number]	k4	136
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
o	o	k7c4	o
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Kid	Kid	k1gFnSc2	Kid
Cudi	Cud	k1gFnSc2	Cud
sám	sám	k3xTgInSc4	sám
produkoval	produkovat	k5eAaImAgMnS	produkovat
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
219	[number]	k4	219
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Satellite	Satellit	k1gInSc5	Satellit
Flight	Flight	k2eAgInSc1d1	Flight
a	a	k8xC	a
Speedin	Speedin	k2eAgInSc1d1	Speedin
<g/>
'	'	kIx"	'
Bullet	Bullet	k1gInSc1	Bullet
To	ten	k3xDgNnSc1	ten
Heaven	Heaven	k2eAgMnSc1d1	Heaven
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
plán	plán	k1gInSc1	plán
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
album	album	k1gNnSc1	album
indicud	indicuda	k1gFnPc2	indicuda
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
3	[number]	k4	3
bude	být	k5eAaImBp3nS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
3	[number]	k4	3
hodlal	hodlat	k5eAaImAgMnS	hodlat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vydat	vydat	k5eAaPmF	vydat
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Satellite	Satellit	k1gInSc5	Satellit
Flight	Flight	k1gInSc4	Flight
<g/>
:	:	kIx,	:
Journey	Journea	k1gMnSc2	Journea
to	ten	k3xDgNnSc1	ten
Mother	Mothra	k1gFnPc2	Mothra
Moon	Moona	k1gFnPc2	Moona
EP	EP	kA	EP
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
učiní	učinit	k5eAaImIp3nS	učinit
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
deset	deset	k4xCc1	deset
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Kid	Kid	k1gFnPc1	Kid
Cudi	Cud	k1gFnSc2	Cud
také	také	k9	také
produkoval	produkovat	k5eAaImAgMnS	produkovat
<g/>
;	;	kIx,	;
čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
s	s	k7c7	s
87	[number]	k4	87
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
95	[number]	k4	95
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vydá	vydat	k5eAaPmIp3nS	vydat
další	další	k2eAgNnSc4d1	další
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
vydá	vydat	k5eAaPmIp3nS	vydat
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
vydal	vydat	k5eAaPmAgMnS	vydat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
nahrána	nahrát	k5eAaPmNgFnS	nahrát
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
Satellite	Satellit	k1gInSc5	Satellit
Flight	Flight	k1gInSc1	Flight
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oznámil	oznámit	k5eAaPmAgInS	oznámit
název	název	k1gInSc1	název
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Speedin	Speedin	k2eAgInSc1d1	Speedin
<g/>
'	'	kIx"	'
Bullet	Bullet	k1gInSc1	Bullet
to	ten	k3xDgNnSc1	ten
Heaven	Heaven	k2eAgMnSc1d1	Heaven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
stanovil	stanovit	k5eAaPmAgInS	stanovit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vydal	vydat	k5eAaPmAgMnS	vydat
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Wedding	Wedding	k1gInSc1	Wedding
Tux	Tux	k1gFnSc2	Tux
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Judgemental	Judgemental	k1gMnSc1	Judgemental
Cunt	Cunt	k1gMnSc1	Cunt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Experimentálního	experimentální	k2eAgNnSc2d1	experimentální
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
19	[number]	k4	19
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Passion	Passion	k1gInSc1	Passion
<g/>
,	,	kIx,	,
Pain	Pain	k1gMnSc1	Pain
&	&	k?	&
Demon	Demon	k1gMnSc1	Demon
Slayin	Slayin	k1gMnSc1	Slayin
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kolovaly	kolovat	k5eAaImAgFnP	kolovat
fámy	fáma	k1gFnPc1	fáma
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
Man	mana	k1gFnPc2	mana
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
však	však	k9	však
Cudi	Cud	k1gInSc3	Cud
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
ponese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Passion	Passion	k1gInSc1	Passion
<g/>
,	,	kIx,	,
Pain	Pain	k1gMnSc1	Pain
&	&	k?	&
Demon	Demon	k1gMnSc1	Demon
Slayin	Slayin	k1gMnSc1	Slayin
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
zapsat	zapsat	k5eAaPmF	zapsat
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
depresím	deprese	k1gFnPc3	deprese
a	a	k8xC	a
sebevražedným	sebevražedný	k2eAgFnPc3d1	sebevražedná
myšlenkám	myšlenka	k1gFnPc3	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
i	i	k9	i
na	na	k7c6	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
špatně	špatně	k6eAd1	špatně
přijatému	přijatý	k2eAgNnSc3d1	přijaté
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
experimentálnímu	experimentální	k2eAgNnSc3d1	experimentální
albu	album	k1gNnSc3	album
Speedin	Speedina	k1gFnPc2	Speedina
<g/>
'	'	kIx"	'
Bullet	Bullet	k1gInSc1	Bullet
to	ten	k3xDgNnSc1	ten
Heaven	Heaven	k2eAgMnSc1d1	Heaven
(	(	kIx(	(
<g/>
44	[number]	k4	44
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
100	[number]	k4	100
na	na	k7c6	na
Metacritic	Metacritice	k1gFnPc2	Metacritice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
Passion	Passion	k1gInSc1	Passion
<g/>
,	,	kIx,	,
Pain	Pain	k1gMnSc1	Pain
&	&	k?	&
Demon	Demon	k1gMnSc1	Demon
Slayin	Slayin	k1gMnSc1	Slayin
<g/>
'	'	kIx"	'
vrací	vracet	k5eAaImIp3nS	vracet
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
typickému	typický	k2eAgInSc3d1	typický
stylu	styl	k1gInSc3	styl
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Mixtapy	Mixtap	k1gInPc4	Mixtap
===	===	k?	===
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
A	a	k8xC	a
Kid	Kid	k1gMnSc1	Kid
Named	Named	k1gMnSc1	Named
Cudi	Cude	k1gFnSc4	Cude
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
===	===	k?	===
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Day	Day	k1gFnSc2	Day
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Nite	nit	k1gInSc5	nit
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Make	Make	k1gNnSc1	Make
Her	hra	k1gFnPc2	hra
Say	Say	k1gFnPc2	Say
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kanye	Kanye	k1gFnSc1	Kanye
West	Westa	k1gFnPc2	Westa
a	a	k8xC	a
Common	Commona	k1gFnPc2	Commona
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Pursuit	Pursuit	k1gInSc1	Pursuit
of	of	k?	of
Happiness	Happiness	k1gInSc1	Happiness
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
MGMT	MGMT	kA	MGMT
a	a	k8xC	a
Ratatat	Ratatat	k1gFnSc1	Ratatat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Erase	Erase	k1gFnSc1	Erase
Me	Me	k1gFnSc1	Me
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kanye	Kanye	k1gFnSc1	Kanye
West	Westa	k1gFnPc2	Westa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Just	just	k6eAd1	just
What	What	k2eAgInSc1d1	What
I	i	k9	i
Am	Am	k1gMnPc2	Am
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gMnSc1	King
Chip	Chip	k1gMnSc1	Chip
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
King	King	k1gMnSc1	King
Wizard	Wizard	k1gMnSc1	Wizard
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
One	One	k1gMnSc1	One
Tree	Tre	k1gFnSc2	Tre
Hill	Hill	k1gMnSc1	Hill
-	-	kIx~	-
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
</s>
</p>
<p>
<s>
2010-11	[number]	k4	2010-11
-	-	kIx~	-
How	How	k1gFnSc4	How
to	ten	k3xDgNnSc1	ten
Make	Make	k1gNnSc1	Make
It	It	k1gMnSc2	It
in	in	k?	in
America	Americus	k1gMnSc2	Americus
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jak	jak	k8xS	jak
dobýt	dobýt	k5eAaPmF	dobýt
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
)	)	kIx)	)
-	-	kIx~	-
16	[number]	k4	16
epizod	epizoda	k1gFnPc2	epizoda
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Cleveland	Cleveland	k1gInSc1	Cleveland
show	show	k1gNnSc1	show
-	-	kIx~	-
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Nine-Nine	Nine-Nin	k1gInSc5	Nine-Nin
-	-	kIx~	-
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Scorpion	Scorpion	k1gInSc1	Scorpion
-	-	kIx~	-
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
Comedy	Comed	k1gMnPc4	Comed
Bang	Bang	k1gInSc4	Bang
<g/>
!	!	kIx.	!
</s>
<s>
Bang	Bang	k1gMnSc1	Bang
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
21	[number]	k4	21
epizod	epizoda	k1gFnPc2	epizoda
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Talking	Talking	k1gInSc1	Talking
Dead	Dead	k1gInSc1	Dead
-	-	kIx~	-
1	[number]	k4	1
epizoda	epizoda	k1gFnSc1	epizoda
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
Maniac	Maniac	k1gFnSc1	Maniac
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgFnSc1d1	krátkometrážní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Cruel	Cruel	k1gMnSc1	Cruel
Summer	Summer	k1gMnSc1	Summer
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgMnSc1d1	krátkometrážní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Goodbye	Goodby	k1gInSc2	Goodby
World	World	k1gInSc1	World
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Need	Needa	k1gFnPc2	Needa
for	forum	k1gNnPc2	forum
Speed	Speed	k1gMnSc1	Speed
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
The	The	k1gMnSc1	The
Ever	Ever	k1gMnSc1	Ever
After	After	k1gMnSc1	After
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Two	Two	k1gFnSc6	Two
Night	Nighta	k1gFnPc2	Nighta
Stand	Standa	k1gFnPc2	Standa
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
Entourage	Entourag	k1gInSc2	Entourag
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
Meadowland	Meadowland	k1gInSc1	Meadowland
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
James	James	k1gMnSc1	James
White	Whit	k1gInSc5	Whit
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Vincent-N-Roxxy	Vincent-N-Roxx	k1gInPc1	Vincent-N-Roxx
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Tacoma	Tacoma	k1gFnSc1	Tacoma
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kid	Kid	k1gFnSc2	Kid
Cudi	Cudi	k1gNnSc2	Cudi
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
<g/>
http://www.facebook.com/pages/Kid-Cudi-CzSk/171637049605380?fref=ts	[url]	k6eAd1	http://www.facebook.com/pages/Kid-Cudi-CzSk/171637049605380?fref=ts
CZ	CZ	kA	CZ
<g/>
/	/	kIx~	/
<g/>
SK	Sk	kA	Sk
Fan	Fana	k1gFnPc2	Fana
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
