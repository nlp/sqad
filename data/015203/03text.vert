<s>
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
</s>
<s>
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
Žánr	žánr	k1gInSc4
</s>
<s>
komedie	komedie	k1gFnSc1
Formát	formát	k1gInSc1
</s>
<s>
seriál	seriál	k1gInSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Štefan	Štefan	k1gMnSc1
Titka	Titka	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Končinsky	Končinsky	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Hodan	Hodan	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Trojánek	Trojánek	k1gMnSc1
Tomislav	Tomislav	k1gMnSc1
Čečka	Čečka	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Zahrádka	Zahrádka	k1gMnSc1
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Robert	Robert	k1gMnSc1
MiklušMarek	MiklušMarka	k1gFnPc2
GeišbergJenovéfa	GeišbergJenovéf	k1gMnSc2
BokováJiří	BokováJiří	k1gNnSc2
BartoškaVeronika	BartoškaVeronik	k1gMnSc2
FreimanováMiroslav	FreimanováMiroslava	k1gFnPc2
DonutilGabriela	DonutilGabriel	k1gMnSc2
MarcinkováJosefína	MarcinkováJosefín	k1gMnSc2
Krycnerová	Krycnerový	k2eAgFnSc1d1
Země	země	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
Česko	Česko	k1gNnSc1
ČeskoSlovensko	Československo	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
Jazyky	jazyk	k1gInPc1
</s>
<s>
češtinaslovenština	češtinaslovenština	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
13	#num#	k4
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
54	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Vedoucíprodukce	Vedoucíprodukce	k1gFnSc2
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
Šlajer	Šlajer	k?
Producent	producent	k1gMnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Ondřejková	Ondřejková	k1gFnSc1
Lokace	lokace	k1gFnSc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
KarloviceOstrava	KarloviceOstrava	k1gFnSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Holman	Holman	k1gMnSc1
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc4
</s>
<s>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Bionaut	Bionaut	k1gMnSc1
RTVS	RTVS	kA
Premiérové	premiérový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Stanice	stanice	k1gFnSc1
</s>
<s>
ČT1	ČT1	k4
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2019	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
Posloupnost	posloupnost	k1gFnSc4
Předchozí	předchozí	k2eAgFnSc4d1
</s>
<s>
Záhada	záhada	k1gFnSc1
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
SZNěkterá	SZNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
je	být	k5eAaImIp3nS
komediální	komediální	k2eAgMnSc1d1
a	a	k8xC
kriminální	kriminální	k2eAgInSc1d1
seriál	seriál	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
RTVS	RTVS	kA
<g/>
,	,	kIx,
volné	volný	k2eAgNnSc1d1
pokračování	pokračování	k1gNnSc1
seriálu	seriál	k1gInSc2
Doktor	doktor	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vysílán	vysílán	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
od	od	k7c2
ledna	leden	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
spin-off	spin-off	k1gInSc4
<g/>
,	,	kIx,
věnující	věnující	k2eAgFnSc6d1
se	se	k3xPyFc4
postavě	postava	k1gFnSc6
strážmistra	strážmistr	k1gMnSc4
Tomáše	Tomáš	k1gMnSc2
Topinky	Topinka	k1gMnSc2
(	(	kIx(
<g/>
Robert	Robert	k1gMnSc1
Mikluš	Mikluš	k1gMnSc1
<g/>
)	)	kIx)
z	z	k7c2
česko-slovenského	česko-slovenský	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Doktor	doktor	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
postav	postav	k1gInSc1
seriálů	seriál	k1gInPc2
Doktor	doktor	k1gMnSc1
Martin	Martin	k1gMnSc1
a	a	k8xC
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
Mikluš	Mikluš	k1gMnSc1
jako	jako	k8xC,k8xS
stržm.	stržm.	k?
Tomáš	Tomáš	k1gMnSc1
Topinka	Topinka	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bartoška	Bartoška	k1gMnSc1
jako	jako	k8xS,k8xC
mjr.	mjr.	kA
Jan	Jan	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
Veronika	Veronika	k1gFnSc1
Freimanová	Freimanová	k1gFnSc1
jako	jako	k8xC,k8xS
Marie	Marie	k1gFnSc1
Topinková	Topinková	k1gFnSc1
</s>
<s>
Jenovéfa	Jenovéfa	k1gFnSc1
Boková	bokový	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Lucie	Lucie	k1gFnSc1
Mazalová	Mazalová	k1gFnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Geišberg	Geišberg	k1gMnSc1
jako	jako	k8xS,k8xC
stržm.	stržm.	k?
Ľubomír	Ľubomír	k1gMnSc1
Ostrý	ostrý	k2eAgMnSc1d1
</s>
<s>
Gabriela	Gabriela	k1gFnSc1
Marcinková	Marcinkový	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
sestřička	sestřička	k1gFnSc1
Irena	Irena	k1gFnSc1
Bezáková	Bezáková	k1gFnSc1
<g/>
,	,	kIx,
DiS.	DiS.	k1gFnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Donutil	donutit	k5eAaPmAgMnS
jako	jako	k9
doc.	doc.	kA
MUDr.	MUDr.	kA
Martin	Martin	k1gMnSc1
Elinger	Elinger	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
jako	jako	k8xS,k8xC
Mgr.	Mgr.	kA
Robert	Robert	k1gMnSc1
Fiala	Fiala	k1gMnSc1
</s>
<s>
Norbert	Norbert	k1gMnSc1
Lichý	lichý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Antonín	Antonín	k1gMnSc1
Brázda	Brázda	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Měcháček	Měcháček	k1gMnSc1
jako	jako	k8xS,k8xC
Miloš	Miloš	k1gMnSc1
Brázda	Brázda	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
jako	jako	k8xC,k8xS
Radek	Radek	k1gMnSc1
Horák	Horák	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Hanák	Hanák	k1gMnSc1
jako	jako	k8xS,k8xC
komisař	komisař	k1gMnSc1
Majer	Majer	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Nebřenský	Nebřenský	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
strážmistr	strážmistr	k1gMnSc1
Štros	Štros	k1gMnSc1
</s>
<s>
Elizaveta	Elizavet	k2eAgFnSc1d1
Maximová	Maximová	k1gFnSc1
jako	jako	k8xC,k8xS
por.	por.	k?
Klára	Klára	k1gFnSc1
Volná	volný	k2eAgFnSc1d1
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
Marek	Marek	k1gMnSc1
Taclík	Taclík	k1gMnSc1
jako	jako	k8xC,k8xS
kpt.	kpt.	k?
František	František	k1gMnSc1
Weber	Weber	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Nový	Nový	k1gMnSc1
jako	jako	k8xS,k8xC
Rudolf	Rudolf	k1gMnSc1
Kytka	kytka	k1gFnSc1
</s>
<s>
Josefína	Josefína	k1gFnSc1
Krycnerová	Krycnerová	k1gFnSc1
jako	jako	k8xS,k8xC
Rozárka	Rozárka	k1gFnSc1
Mazalová	Mazalová	k1gFnSc1
</s>
<s>
Miloš	Miloš	k1gMnSc1
Vávra	Vávra	k1gMnSc1
jako	jako	k8xC,k8xS
starosta	starosta	k1gMnSc1
Protějova	Protějův	k2eAgInSc2d1
Prušek	Prušek	k1gInSc4
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Bauer	Bauer	k1gMnSc1
jako	jako	k8xS,k8xC
Vašek	Vašek	k1gMnSc1
Tobiáš	Tobiáš	k1gMnSc1
</s>
<s>
Milena	Milena	k1gFnSc1
Steinmasslová	Steinmasslový	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Sandra	Sandra	k1gFnSc1
Kunešová	Kunešová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Leinweberová	Leinweberová	k1gFnSc1
jako	jako	k8xC,k8xS
Karolína	Karolína	k1gFnSc1
Sovová	Sovová	k1gFnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Plesl	Plesl	k1gInSc1
jako	jako	k8xC,k8xS
Norbert	Norbert	k1gMnSc1
Skruz	Skruz	k1gMnSc1
</s>
<s>
Píďa	Píďa	k1gMnSc1
jako	jako	k8xS,k8xC
pes	pes	k1gMnSc1
Píďa	Píďa	k1gMnSc1
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
Scénář	scénář	k1gInSc4
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
Tomáš	Tomáš	k1gMnSc1
Končinský	Končinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Štefan	Štefan	k1gMnSc1
Titka	Titka	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Hodan	Hodan	k1gMnSc1
<g/>
,	,	kIx,
Hynek	Hynek	k1gMnSc1
Trojánek	Trojánek	k1gMnSc1
<g/>
,	,	kIx,
Tomislav	Tomislav	k1gMnSc1
Čečka	Čečka	k1gMnSc1
a	a	k8xC
režie	režie	k1gFnSc1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Zahrádka	Zahrádka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
tak	tak	k9
podíleli	podílet	k5eAaImAgMnP
tvůrci	tvůrce	k1gMnPc7
seriálů	seriál	k1gInPc2
Terapie	terapie	k1gFnSc2
či	či	k8xC
Nevinné	vinný	k2eNgFnSc2d1
lži	lež	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
filmů	film	k1gInPc2
Kdopak	kdopak	k3yQnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
vlka	vlk	k1gMnSc2
bál	bál	k1gInSc4
<g/>
,	,	kIx,
Smradi	Smradi	k?
<g/>
,	,	kIx,
Pouta	pouto	k1gNnSc2
a	a	k8xC
na	na	k7c6
původním	původní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Doktor	doktor	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natáčení	natáčení	k1gNnPc4
probíhá	probíhat	k5eAaImIp3nS
od	od	k7c2
podzimu	podzim	k1gInSc2
2017	#num#	k4
do	do	k7c2
podzimu	podzim	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Strážmistr	strážmistr	k1gMnSc1
Topinka	Topinka	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
