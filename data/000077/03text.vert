<s>
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
cena	cena	k1gFnSc1	cena
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
soška	soška	k1gFnSc1	soška
lva	lev	k1gInSc2	lev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
udělováno	udělován	k2eAgNnSc1d1	udělováno
v	v	k7c6	v
několika	několik	k4yIc2	několik
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
prošel	projít	k5eAaPmAgInS	projít
reorganizací	reorganizace	k1gFnSc7	reorganizace
a	a	k8xC	a
soutěž	soutěž	k1gFnSc1	soutěž
nyní	nyní	k6eAd1	nyní
pořádá	pořádat	k5eAaImIp3nS	pořádat
Česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
ČFTA	ČFTA	kA	ČFTA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ocenění	ocenění	k1gNnSc4	ocenění
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
uděluje	udělovat	k5eAaImIp3nS	udělovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
poroty	porota	k1gFnSc2	porota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
ČFTA	ČFTA	kA	ČFTA
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
filmových	filmový	k2eAgMnPc2d1	filmový
profesionálů	profesionál	k1gMnPc2	profesionál
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
kritici	kritik	k1gMnPc1	kritik
<g/>
,	,	kIx,	,
producenti	producent	k1gMnPc1	producent
<g/>
,	,	kIx,	,
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
odborníci	odborník	k1gMnPc1	odborník
napříč	napříč	k7c7	napříč
celým	celý	k2eAgInSc7d1	celý
filmovým	filmový	k2eAgInSc7d1	filmový
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Akademici	akademik	k1gMnPc1	akademik
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
české	český	k2eAgInPc4d1	český
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
distribuční	distribuční	k2eAgInPc4d1	distribuční
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
koprodukce	koprodukce	k1gFnPc4	koprodukce
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
audiovizuální	audiovizuální	k2eAgNnPc1d1	audiovizuální
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
uplynulého	uplynulý	k2eAgInSc2d1	uplynulý
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
akademici	akademik	k1gMnPc1	akademik
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
nominují	nominovat	k5eAaBmIp3nP	nominovat
tvůrce	tvůrce	k1gMnSc1	tvůrce
a	a	k8xC	a
filmy	film	k1gInPc4	film
v	v	k7c6	v
několika	několik	k4yIc6	několik
statutárních	statutární	k2eAgFnPc6d1	statutární
i	i	k8xC	i
nestatutárních	statutární	k2eNgFnPc6d1	statutární
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnPc1	nominace
jsou	být	k5eAaImIp3nP	být
vyhlášeny	vyhlásit	k5eAaPmNgFnP	vyhlásit
s	s	k7c7	s
předstihem	předstih	k1gInSc7	předstih
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
udílení	udílení	k1gNnSc1	udílení
cen	cena	k1gFnPc2	cena
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
galavečeru	galavečer	k1gInSc6	galavečer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
událostí	událost	k1gFnPc2	událost
českého	český	k2eAgNnSc2d1	české
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
cen	cena	k1gFnPc2	cena
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
získávají	získávat	k5eAaImIp3nP	získávat
křišťálové	křišťálový	k2eAgFnPc1d1	Křišťálová
trofeje	trofej	k1gFnPc1	trofej
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
český	český	k2eAgMnSc1d1	český
designér	designér	k1gMnSc1	designér
Jakub	Jakub	k1gMnSc1	Jakub
Berdych	Berdych	k1gMnSc1	Berdych
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
světelný	světelný	k2eAgInSc4d1	světelný
kužel	kužel	k1gInSc4	kužel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
plastický	plastický	k2eAgInSc1d1	plastický
zlacený	zlacený	k2eAgInSc1d1	zlacený
lev	lev	k1gInSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
soška	soška	k1gFnSc1	soška
prošla	projít	k5eAaPmAgFnS	projít
drobnou	drobný	k2eAgFnSc7d1	drobná
úpravou	úprava	k1gFnSc7	úprava
–	–	k?	–
trofej	trofej	k1gFnSc1	trofej
se	se	k3xPyFc4	se
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
a	a	k8xC	a
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
soutěže	soutěž	k1gFnSc2	soutěž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
2D	[number]	k4	2D
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nestatutárních	statutární	k2eNgFnPc2d1	statutární
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
filmových	filmový	k2eAgMnPc2d1	filmový
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
plakát	plakát	k1gInSc4	plakát
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
studentský	studentský	k2eAgInSc4d1	studentský
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
diplomu	diplom	k1gInSc2	diplom
rovněž	rovněž	k9	rovněž
udělována	udělován	k2eAgFnSc1d1	udělována
menší	malý	k2eAgFnSc1d2	menší
trofej	trofej	k1gFnSc1	trofej
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
elektronky	elektronka	k1gFnSc2	elektronka
se	s	k7c7	s
zlaceným	zlacený	k2eAgInSc7d1	zlacený
proužkem	proužek	k1gInSc7	proužek
a	a	k8xC	a
logem	log	k1gInSc7	log
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
rovněž	rovněž	k9	rovněž
Berdych	Berdych	k1gInSc1	Berdych
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
typografického	typografický	k2eAgInSc2d1	typografický
konceptu	koncept	k1gInSc2	koncept
obou	dva	k4xCgFnPc2	dva
trofejí	trofej	k1gFnPc2	trofej
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Brousil	brousit	k5eAaImAgInS	brousit
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
kategorie	kategorie	k1gFnSc1	kategorie
cen	cena	k1gFnPc2	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
založení	založení	k1gNnSc2	založení
průběžně	průběžně	k6eAd1	průběžně
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nejsledovanějších	sledovaný	k2eAgFnPc2d3	nejsledovanější
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
herecké	herecký	k2eAgInPc4d1	herecký
výkony	výkon	k1gInPc4	výkon
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc4	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
počin	počin	k1gInSc4	počin
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
podkategorií	podkategorie	k1gFnPc2	podkategorie
–	–	k?	–
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
filmová	filmový	k2eAgFnSc1d1	filmová
scénografie	scénografie	k1gFnSc1	scénografie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
masky	maska	k1gFnPc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
teoretiků	teoretik	k1gMnPc2	teoretik
nahradily	nahradit	k5eAaPmAgFnP	nahradit
samostatné	samostatný	k2eAgFnPc1d1	samostatná
Ceny	cena	k1gFnPc1	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
udělují	udělovat	k5eAaImIp3nP	udělovat
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
televizní	televizní	k2eAgFnSc4d1	televizní
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
doporučí	doporučit	k5eAaPmIp3nP	doporučit
televizní	televizní	k2eAgMnPc1d1	televizní
provozovatelé	provozovatel	k1gMnPc1	provozovatel
do	do	k7c2	do
hlasování	hlasování	k1gNnSc2	hlasování
několik	několik	k4yIc1	několik
svých	svůj	k3xOyFgMnPc2	svůj
televizních	televizní	k2eAgMnPc2d1	televizní
počinů	počin	k1gInPc2	počin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
o	o	k7c6	o
vítězích	vítěz	k1gMnPc6	vítěz
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
akademici	akademik	k1gMnPc1	akademik
ČFTA	ČFTA	kA	ČFTA
<g/>
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
udělovány	udělovat	k5eAaImNgFnP	udělovat
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
nebo	nebo	k8xC	nebo
minisérie	minisérie	k1gFnSc1	minisérie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
dramatický	dramatický	k2eAgInSc4d1	dramatický
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Statutární	statutární	k2eAgFnPc1d1	statutární
ceny	cena	k1gFnPc1	cena
<g/>
:	:	kIx,	:
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
režie	režie	k1gFnSc2	režie
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
střih	střih	k1gInSc4	střih
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zvuk	zvuk	k1gInSc4	zvuk
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
počin	počin	k1gInSc4	počin
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
dlouholetý	dlouholetý	k2eAgInSc1d1	dlouholetý
umělecký	umělecký	k2eAgInSc1d1	umělecký
přínos	přínos	k1gInSc1	přínos
českému	český	k2eAgInSc3d1	český
filmu	film	k1gInSc3	film
<g />
.	.	kIx.	.
</s>
<s>
cena	cena	k1gFnSc1	cena
filmových	filmový	k2eAgMnPc2d1	filmový
fanoušků	fanoušek	k1gMnPc2	fanoušek
–	–	k?	–
cena	cena	k1gFnSc1	cena
poprvé	poprvé	k6eAd1	poprvé
předána	předán	k2eAgFnSc1d1	předána
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
divácky	divácky	k6eAd1	divácky
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
film	film	k1gInSc1	film
–	–	k?	–
kategorie	kategorie	k1gFnSc1	kategorie
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
dokument	dokument	k1gInSc1	dokument
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
)	)	kIx)	)
Další	další	k2eAgFnPc1d1	další
ceny	cena	k1gFnPc1	cena
<g/>
:	:	kIx,	:
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
filmový	filmový	k2eAgInSc1d1	filmový
plakát	plakát	k1gInSc1	plakát
Cena	cena	k1gFnSc1	cena
Magnesie	magnesie	k1gFnSc1	magnesie
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
film	film	k1gInSc1	film
–	–	k?	–
kategorie	kategorie	k1gFnSc1	kategorie
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Cena	cena	k1gFnSc1	cena
Magnesia	magnesium	k1gNnSc2	magnesium
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
studentský	studentský	k2eAgInSc4d1	studentský
film	film	k1gInSc4	film
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
uděluje	udělovat	k5eAaImIp3nS	udělovat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Cena	cena	k1gFnSc1	cena
Sazky	Sazka	k1gFnSc2	Sazka
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
nerealizovaný	realizovaný	k2eNgInSc1d1	nerealizovaný
scénář	scénář	k1gInSc1	scénář
Cena	cena	k1gFnSc1	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
–	–	k?	–
kategorie	kategorie	k1gFnSc1	kategorie
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Cena	cena	k1gFnSc1	cena
čtenářů	čtenář	k1gMnPc2	čtenář
časopisu	časopis	k1gInSc2	časopis
Premiere	Premier	k1gInSc5	Premier
Plyšový	plyšový	k2eAgInSc1d1	plyšový
lev	lev	k1gInSc1	lev
–	–	k?	–
nejhorší	zlý	k2eAgInSc1d3	Nejhorší
film	film	k1gInSc1	film
–	–	k?	–
kategorie	kategorie	k1gFnSc1	kategorie
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
zřízena	zřízen	k2eAgFnSc1d1	zřízena
kategorie	kategorie	k1gFnSc2	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokument	dokument	k1gInSc4	dokument
Cena	cena	k1gFnSc1	cena
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
audiovizuální	audiovizuální	k2eAgInSc4d1	audiovizuální
počin	počin	k1gInSc4	počin
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
získává	získávat	k5eAaImIp3nS	získávat
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
režisér	režisér	k1gMnSc1	režisér
vítězného	vítězný	k2eAgInSc2d1	vítězný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvořily	tvořit	k5eAaImAgInP	tvořit
ročníky	ročník	k1gInPc1	ročník
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
vítězným	vítězný	k2eAgInSc7d1	vítězný
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Otesánek	otesánek	k1gInSc1	otesánek
Jana	Jan	k1gMnSc2	Jan
Švankmajera	Švankmajer	k1gMnSc2	Švankmajer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c4	za
Tmavomodrý	tmavomodrý	k2eAgInSc4d1	tmavomodrý
svět	svět	k1gInSc4	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
vítězným	vítězný	k2eAgInSc7d1	vítězný
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
Tajnosti	tajnost	k1gFnPc1	tajnost
Alice	Alice	k1gFnSc1	Alice
Nellis	Nellis	k1gFnSc1	Nellis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c2	za
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
herců	herc	k1gInPc2	herc
a	a	k8xC	a
hereček	herečka	k1gFnPc2	herečka
získalo	získat	k5eAaPmAgNnS	získat
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
víckrát	víckrát	k6eAd1	víckrát
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
HR	hr	k6eAd1	hr
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
VR	vr	k0	vr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plyšový	plyšový	k2eAgMnSc1d1	plyšový
lev	lev	k1gMnSc1	lev
byla	být	k5eAaImAgFnS	být
anticena	anticet	k5eAaImNgFnS	anticet
kritiků	kritik	k1gMnPc2	kritik
za	za	k7c4	za
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
např.	např.	kA	např.
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
malina	malina	k1gFnSc1	malina
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
cenu	cena	k1gFnSc4	cena
osobně	osobně	k6eAd1	osobně
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Tyc	Tyc	k1gMnSc1	Tyc
za	za	k7c4	za
film	film	k1gInSc4	film
Už	už	k6eAd1	už
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
ročníku	ročník	k1gInSc6	ročník
1997	[number]	k4	1997
nebyla	být	k5eNaImAgFnS	být
anticena	antitit	k5eAaPmNgFnS	antitit
udělena	udělen	k2eAgFnSc1d1	udělena
a	a	k8xC	a
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
vyhlašování	vyhlašování	k1gNnSc1	vyhlašování
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
udílecího	udílecí	k2eAgInSc2d1	udílecí
večera	večer	k1gInSc2	večer
na	na	k7c4	na
večer	večer	k1gInSc4	večer
nominační	nominační	k2eAgInSc4d1	nominační
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
jen	jen	k9	jen
na	na	k7c4	na
tiskovou	tiskový	k2eAgFnSc4d1	tisková
konferenci	konference	k1gFnSc4	konference
<g/>
.	.	kIx.	.
</s>
<s>
Anticena	Anticen	k2eAgFnSc1d1	Anticena
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
zrušena	zrušit	k5eAaPmNgFnS	zrušit
při	při	k7c6	při
revizi	revize	k1gFnSc6	revize
kategorií	kategorie	k1gFnPc2	kategorie
k	k	k7c3	k
Českému	český	k2eAgInSc3d1	český
lvu	lev	k1gInSc3	lev
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
–	–	k?	–
Kanárská	kanárský	k2eAgFnSc1d1	Kanárská
spojka	spojka	k1gFnSc1	spojka
1994	[number]	k4	1994
–	–	k?	–
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgMnSc1d2	veliký
blbec	blbec	k1gMnSc1	blbec
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
doufali	doufat	k5eAaImAgMnP	doufat
1995	[number]	k4	1995
–	–	k?	–
Divoké	divoký	k2eAgNnSc4d1	divoké
pivo	pivo	k1gNnSc4	pivo
1996	[number]	k4	1996
–	–	k?	–
UŽ	už	k9	už
1997	[number]	k4	1997
–	–	k?	–
cena	cena	k1gFnSc1	cena
neudělena	udělen	k2eNgFnSc1d1	neudělena
1998	[number]	k4	1998
–	–	k?	–
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
pohyby	pohyb	k1gInPc1	pohyb
očí	oko	k1gNnPc2	oko
1999	[number]	k4	1999
–	–	k?	–
Nebát	bát	k5eNaImF	bát
se	se	k3xPyFc4	se
a	a	k8xC	a
nakrást	nakrást	k5eAaBmF	nakrást
2000	[number]	k4	2000
–	–	k?	–
Začátek	začátek	k1gInSc1	začátek
světa	svět	k1gInSc2	svět
2001	[number]	k4	2001
–	–	k?	–
Jak	jak	k6eAd1	jak
ukrást	ukrást	k5eAaPmF	ukrást
Dagmaru	Dagmar	k1gFnSc4	Dagmar
2002	[number]	k4	2002
–	–	k?	–
Waterloo	Waterloo	k1gNnSc2	Waterloo
po	po	k7c6	po
česku	česk	k1gInSc6	česk
2003	[number]	k4	2003
–	–	k?	–
Kameňák	Kameňák	k?	Kameňák
2004	[number]	k4	2004
–	–	k?	–
Kameňák	Kameňák	k?	Kameňák
2	[number]	k4	2
2005	[number]	k4	2005
–	–	k?	–
Kameňák	Kameňák	k?	Kameňák
3	[number]	k4	3
2006	[number]	k4	2006
–	–	k?	–
Po	po	k7c6	po
hlavě	hlava	k1gFnSc6	hlava
<g/>
...	...	k?	...
do	do	k7c2	do
prdele	prdel	k1gFnSc2	prdel
2007	[number]	k4	2007
–	–	k?	–
Poslední	poslední	k2eAgFnSc1d1	poslední
plavky	plavka	k1gFnSc2	plavka
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
</s>
