<s>
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
Baltimore	Baltimore	k1gInSc1	Baltimore
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
romantický	romantický	k2eAgMnSc1d1	romantický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
zpravidla	zpravidla	k6eAd1	zpravidla
fantastických	fantastický	k2eAgInPc2d1	fantastický
a	a	k8xC	a
mystických	mystický	k2eAgInPc2d1	mystický
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
zakladatelem	zakladatel	k1gMnSc7	zakladatel
detektivního	detektivní	k2eAgInSc2d1	detektivní
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
mistrovsky	mistrovsky	k6eAd1	mistrovsky
zachytit	zachytit	k5eAaPmF	zachytit
stav	stav	k1gInSc4	stav
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
příběh	příběh	k1gInSc4	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c7	za
ranou	rána	k1gFnSc7	rána
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
pera	pero	k1gNnSc2	pero
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
několik	několik	k4yIc4	několik
humoristických	humoristický	k2eAgInPc2d1	humoristický
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1809	[number]	k4	1809
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
páru	pár	k1gInSc2	pár
kočovných	kočovný	k2eAgInPc2d1	kočovný
herců	herc	k1gInPc2	herc
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Poeových	Poeových	k2eAgMnSc2d1	Poeových
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
trpěl	trpět	k5eAaImAgMnS	trpět
alkoholismem	alkoholismus	k1gInSc7	alkoholismus
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
rodinu	rodina	k1gFnSc4	rodina
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
24	[number]	k4	24
let	léto	k1gNnPc2	léto
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
tak	tak	k6eAd1	tak
po	po	k7c6	po
sobě	se	k3xPyFc3	se
tři	tři	k4xCgMnPc4	tři
sirotky	sirotek	k1gMnPc4	sirotek
<g/>
:	:	kIx,	:
malého	malý	k2eAgMnSc4d1	malý
Edgara	Edgar	k1gMnSc4	Edgar
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
mentálně	mentálně	k6eAd1	mentálně
postiženou	postižený	k2eAgFnSc4d1	postižená
sestru	sestra	k1gFnSc4	sestra
Rosalii	Rosalie	k1gFnSc4	Rosalie
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
Williama	William	k1gMnSc4	William
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
propadl	propadnout	k5eAaPmAgMnS	propadnout
alkoholismu	alkoholismus	k1gInSc3	alkoholismus
a	a	k8xC	a
předčasně	předčasně	k6eAd1	předčasně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
tříletý	tříletý	k2eAgMnSc1d1	tříletý
sirotek	sirotek	k1gMnSc1	sirotek
byl	být	k5eAaImAgMnS	být
Edgar	Edgar	k1gMnSc1	Edgar
svěřen	svěřit	k5eAaPmNgMnS	svěřit
do	do	k7c2	do
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c6	v
Richmondu	Richmond	k1gInSc6	Richmond
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
po	po	k7c6	po
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
ujala	ujmout	k5eAaPmAgFnS	ujmout
rodina	rodina	k1gFnSc1	rodina
Allanů	Allan	k1gMnPc2	Allan
<g/>
,	,	kIx,	,
velkoobchodníků	velkoobchodník	k1gMnPc2	velkoobchodník
s	s	k7c7	s
tabákem	tabák	k1gInSc7	tabák
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
Poeovo	Poeův	k2eAgNnSc4d1	Poeův
prostřední	prostřední	k2eAgNnSc4d1	prostřední
jméno	jméno	k1gNnSc4	jméno
Allan	Allan	k1gMnSc1	Allan
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
Anglie	Anglie	k1gFnSc1	Anglie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
mladému	mladý	k2eAgMnSc3d1	mladý
Edgarovi	Edgar	k1gMnSc3	Edgar
zdrojem	zdroj	k1gInSc7	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Dodala	dodat	k5eAaPmAgFnS	dodat
jeho	jeho	k3xOp3gNnSc7	jeho
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
dílům	dílo	k1gNnPc3	dílo
onu	onen	k3xDgFnSc4	onen
fantaskní	fantaskní	k2eAgMnSc1d1	fantaskní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
typicky	typicky	k6eAd1	typicky
poeovskou	poeovský	k2eAgFnSc4d1	poeovský
hrůzostrašnost	hrůzostrašnost	k1gFnSc4	hrůzostrašnost
<g/>
.	.	kIx.	.
</s>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
studoval	studovat	k5eAaImAgMnS	studovat
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Virginia	Virginium	k1gNnSc2	Virginium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
mít	mít	k5eAaImF	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
s	s	k7c7	s
hráčskými	hráčský	k2eAgInPc7d1	hráčský
dluhy	dluh	k1gInPc7	dluh
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
Edgara	Edgar	k1gMnSc4	Edgar
odmítal	odmítat	k5eAaImAgMnS	odmítat
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Poe	Poe	k?	Poe
se	se	k3xPyFc4	se
ve	v	k7c6	v
finanční	finanční	k2eAgFnSc6d1	finanční
krizi	krize	k1gFnSc6	krize
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
ve	v	k7c4	v
West	West	k1gInSc4	West
Pointu	pointa	k1gFnSc4	pointa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jako	jako	k8xC	jako
nedisciplinovaný	disciplinovaný	k2eNgMnSc1d1	nedisciplinovaný
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozporům	rozpor	k1gInPc3	rozpor
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
s	s	k7c7	s
Edgarovým	Edgarův	k2eAgMnSc7d1	Edgarův
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgInS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
organizované	organizovaný	k2eAgNnSc1d1	organizované
baltimorským	baltimorský	k2eAgInSc7d1	baltimorský
týdeníkem	týdeník	k1gInSc7	týdeník
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Visitor	Visitor	k1gInSc4	Visitor
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
příběhem	příběh	k1gInSc7	příběh
Rukopis	rukopis	k1gInSc1	rukopis
nalezený	nalezený	k2eAgMnSc1d1	nalezený
v	v	k7c6	v
láhvi	láhev	k1gFnSc6	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
richmondského	richmondský	k2eAgMnSc2d1	richmondský
Southern	Southern	k1gInSc1	Southern
Literary	Literara	k1gFnSc2	Literara
Messenger	Messengra	k1gFnPc2	Messengra
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
třináctiletou	třináctiletý	k2eAgFnSc7d1	třináctiletá
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Virginií	Virginie	k1gFnPc2	Virginie
Clemm	Clemma	k1gFnPc2	Clemma
(	(	kIx(	(
<g/>
zesnulou	zesnulá	k1gFnSc4	zesnulá
o	o	k7c4	o
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
a	a	k8xC	a
drogy	droga	k1gFnPc1	droga
způsobovaly	způsobovat	k5eAaImAgInP	způsobovat
Poeovi	Poea	k1gMnSc3	Poea
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
stavy	stav	k1gInPc4	stav
deprese	deprese	k1gFnSc2	deprese
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ztrátu	ztráta	k1gFnSc4	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
Havrana	Havran	k1gMnSc4	Havran
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
doslova	doslova	k6eAd1	doslova
ohromil	ohromit	k5eAaPmAgMnS	ohromit
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Poe	Poe	k?	Poe
tedy	tedy	k9	tedy
zažil	zažít	k5eAaPmAgMnS	zažít
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
mondénních	mondénní	k2eAgInPc2d1	mondénní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
alkoholové	alkoholový	k2eAgInPc1d1	alkoholový
a	a	k8xC	a
drogové	drogový	k2eAgInPc1d1	drogový
dluhy	dluh	k1gInPc1	dluh
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
z	z	k7c2	z
onoho	onen	k3xDgNnSc2	onen
postavení	postavení	k1gNnSc2	postavení
brzy	brzy	k6eAd1	brzy
stačily	stačit	k5eAaBmAgFnP	stačit
uvrhnout	uvrhnout	k5eAaPmF	uvrhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
zoufalé	zoufalý	k2eAgFnSc2d1	zoufalá
bídy	bída	k1gFnSc2	bída
<g/>
.	.	kIx.	.
</s>
<s>
Poeova	Poeův	k2eAgFnSc1d1	Poeova
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
hodna	hoden	k2eAgFnSc1d1	hodna
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
opilý	opilý	k2eAgMnSc1d1	opilý
(	(	kIx(	(
<g/>
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
omamných	omamný	k2eAgFnPc2d1	omamná
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
na	na	k7c6	na
baltimorském	baltimorský	k2eAgInSc6d1	baltimorský
chodníku	chodník	k1gInSc6	chodník
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Light	Light	k1gMnSc1	Light
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
urychleně	urychleně	k6eAd1	urychleně
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
kómatu	kóma	k1gNnSc2	kóma
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgMnSc2	který
následně	následně	k6eAd1	následně
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
neprobral	probrat	k5eNaPmAgMnS	probrat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
překrvení	překrvení	k1gNnSc4	překrvení
mozku	mozek	k1gInSc2	mozek
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
byl	být	k5eAaImAgMnS	být
obdarován	obdarovat	k5eAaPmNgMnS	obdarovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
zdvořilý	zdvořilý	k2eAgMnSc1d1	zdvořilý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
až	až	k9	až
přehnaně	přehnaně	k6eAd1	přehnaně
prudký	prudký	k2eAgInSc1d1	prudký
a	a	k8xC	a
divoký	divoký	k2eAgInSc1d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
četl	číst	k5eAaImAgMnS	číst
díla	dílo	k1gNnPc4	dílo
Lorda	lord	k1gMnSc2	lord
Byrona	Byron	k1gMnSc2	Byron
<g/>
,	,	kIx,	,
Samuela	Samuel	k1gMnSc2	Samuel
Taylora	Taylor	k1gMnSc2	Taylor
Coleridge	Coleridg	k1gMnSc2	Coleridg
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
romantických	romantický	k2eAgMnPc2d1	romantický
autorů	autor	k1gMnPc2	autor
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Hlouběji	hluboko	k6eAd2	hluboko
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zabýval	zabývat	k5eAaImAgInS	zabývat
kosmogonií	kosmogonie	k1gFnSc7	kosmogonie
<g/>
,	,	kIx,	,
přírodními	přírodní	k2eAgFnPc7d1	přírodní
vědami	věda	k1gFnPc7	věda
a	a	k8xC	a
mysticismem	mysticismus	k1gInSc7	mysticismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
své	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
následně	následně	k6eAd1	následně
používal	používat	k5eAaImAgInS	používat
při	při	k7c6	při
psaní	psaní	k1gNnSc3	psaní
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pád	Pád	k1gInSc1	Pád
do	do	k7c2	do
Maelströmu	Maelström	k1gInSc2	Maelström
<g/>
,	,	kIx,	,
Tisící	tisící	k4xOgInPc4	tisící
druhý	druhý	k4xOgInSc1	druhý
Šeherezádin	Šeherezádin	k2eAgInSc1d1	Šeherezádin
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc4	tři
neděle	neděle	k1gFnPc4	neděle
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dokázal	dokázat	k5eAaPmAgMnS	dokázat
s	s	k7c7	s
matematickou	matematický	k2eAgFnSc7d1	matematická
přesností	přesnost	k1gFnSc7	přesnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
geniálního	geniální	k2eAgInSc2d1	geniální
efektu	efekt	k1gInSc2	efekt
a	a	k8xC	a
zanechat	zanechat	k5eAaPmF	zanechat
tak	tak	k6eAd1	tak
ve	v	k7c6	v
čtenáři	čtenář	k1gMnSc6	čtenář
předem	předem	k6eAd1	předem
psychologicky	psychologicky	k6eAd1	psychologicky
promyšlenou	promyšlený	k2eAgFnSc4d1	promyšlená
impresi	imprese	k1gFnSc4	imprese
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
strachu	strach	k1gInSc2	strach
či	či	k8xC	či
hnusu	hnus	k1gInSc2	hnus
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ale	ale	k8xC	ale
i	i	k8xC	i
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
ztratil	ztratit	k5eAaPmAgMnS	ztratit
všechny	všechen	k3xTgFnPc4	všechen
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
znesvářil	znesvářit	k5eAaPmAgMnS	znesvářit
se	se	k3xPyFc4	se
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
těžkých	těžký	k2eAgInPc6d1	těžký
okamžicích	okamžik	k1gInPc6	okamžik
schopni	schopen	k2eAgMnPc1d1	schopen
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
takřka	takřka	k6eAd1	takřka
nepřetržité	přetržitý	k2eNgFnSc6d1	nepřetržitá
bídě	bída	k1gFnSc6	bída
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
i	i	k9	i
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
dočkal	dočkat	k5eAaPmAgMnS	dočkat
jisté	jistý	k2eAgFnPc4d1	jistá
slávy	sláva	k1gFnPc4	sláva
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgFnPc7	svůj
žurnalistickými	žurnalistický	k2eAgFnPc7d1	žurnalistická
pracemi	práce	k1gFnPc7	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
mnohých	mnohý	k2eAgFnPc2d1	mnohá
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
za	za	k7c2	za
Poeova	Poeův	k2eAgInSc2d1	Poeův
života	život	k1gInSc2	život
takřka	takřka	k6eAd1	takřka
neznámé	známý	k2eNgNnSc1d1	neznámé
dílo	dílo	k1gNnSc1	dílo
dostalo	dostat	k5eAaPmAgNnS	dostat
především	především	k9	především
díky	díky	k7c3	díky
překladům	překlad	k1gInPc3	překlad
Charlese	Charles	k1gMnSc2	Charles
Baudelaira	Baudelaira	k1gMnSc1	Baudelaira
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Stéphana	Stéphan	k1gMnSc4	Stéphan
Mallarmého	Mallarmý	k2eAgMnSc4d1	Mallarmý
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jsem	být	k5eAaImIp1nS	být
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
nabývám	nabývat	k5eAaImIp1nS	nabývat
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
Poea	Poeum	k1gNnPc4	Poeum
jediné	jediný	k2eAgFnSc2d1	jediná
velké	velká	k1gFnSc2	velká
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgInSc1d1	duchovní
svět	svět	k1gInSc1	svět
básníka	básník	k1gMnSc2	básník
nebo	nebo	k8xC	nebo
i	i	k9	i
pijáka	piják	k1gMnSc2	piják
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgNnSc7d1	jediné
vytrvalým	vytrvalý	k2eAgNnSc7d1	vytrvalé
úsilím	úsilí	k1gNnSc7	úsilí
uniknout	uniknout	k5eAaPmF	uniknout
vlivu	vliv	k1gInSc2	vliv
této	tento	k3xDgFnSc2	tento
odporné	odporný	k2eAgFnSc2d1	odporná
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
inspirací	inspirace	k1gFnSc7	inspirace
mnoha	mnoho	k4c2	mnoho
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
asi	asi	k9	asi
nejznámějšího	známý	k2eAgMnSc4d3	nejznámější
sira	sir	k1gMnSc4	sir
Arthura	Arthur	k1gMnSc4	Arthur
Conana	Conan	k1gMnSc4	Conan
Doyla	Doyl	k1gMnSc4	Doyl
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
kultovního	kultovní	k2eAgMnSc4d1	kultovní
hrdinu	hrdina	k1gMnSc4	hrdina
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Poeovou	Poeový	k2eAgFnSc7d1	Poeový
postavou	postava	k1gFnSc7	postava
výjimečně	výjimečně	k6eAd1	výjimečně
inteligentního	inteligentní	k2eAgMnSc4d1	inteligentní
detektiva	detektiv	k1gMnSc4	detektiv
Dupina	Dupin	k1gMnSc4	Dupin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
stopu	stopa	k1gFnSc4	stopa
zanechal	zanechat	k5eAaPmAgMnS	zanechat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
francouzských	francouzský	k2eAgInPc2d1	francouzský
básnících	básnící	k2eAgInPc2d1	básnící
Charlesi	Charles	k1gMnSc6	Charles
Baudelairovi	Baudelair	k1gMnSc6	Baudelair
a	a	k8xC	a
Stéphanu	Stéphan	k1gMnSc6	Stéphan
Mallarmém	Mallarmý	k2eAgMnSc6d1	Mallarmý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
na	na	k7c4	na
Paulu	Paula	k1gFnSc4	Paula
Valérym	Valérymum	k1gNnPc2	Valérymum
<g/>
,	,	kIx,	,
Juliu	Julius	k1gMnSc6	Julius
Vernovi	Vern	k1gMnSc6	Vern
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
onou	onen	k3xDgFnSc7	onen
až	až	k6eAd1	až
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
přesností	přesnost	k1gFnSc7	přesnost
popisu	popis	k1gInSc2	popis
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k8xC	i
na	na	k7c6	na
Fjodoru	Fjodor	k1gInSc6	Fjodor
Dostojevském	Dostojevský	k2eAgInSc6d1	Dostojevský
a	a	k8xC	a
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
Arthuru	Arthur	k1gInSc6	Arthur
Conanu	Conan	k1gMnSc3	Conan
Doylovi	Doyl	k1gMnSc3	Doyl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pravým	pravý	k2eAgMnSc7d1	pravý
dědicem	dědic	k1gMnSc7	dědic
a	a	k8xC	a
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
Howard	Howard	k1gMnSc1	Howard
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
Poem	poema	k1gFnPc2	poema
ražené	ražený	k2eAgFnSc3d1	ražená
cestě	cesta	k1gFnSc3	cesta
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
extrémnějších	extrémní	k2eAgNnPc2d2	extrémnější
zákoutí	zákoutí	k1gNnPc2	zákoutí
hrůzy	hrůza	k1gFnSc2	hrůza
a	a	k8xC	a
hnusu	hnus	k1gInSc2	hnus
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
literatuře	literatura	k1gFnSc6	literatura
není	být	k5eNaImIp3nS	být
Poe	Poe	k1gFnSc4	Poe
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
především	především	k9	především
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
do	do	k7c2	do
široce	široko	k6eAd1	široko
populárního	populární	k2eAgNnSc2d1	populární
díla	dílo	k1gNnSc2	dílo
Joanne	Joann	k1gInSc5	Joann
Kathleen	Kathleen	k1gInSc4	Kathleen
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
z	z	k7c2	z
Poeových	Poeův	k2eAgNnPc2d1	Poeův
děl	dělo	k1gNnPc2	dělo
byla	být	k5eAaImAgFnS	být
nejen	nejen	k6eAd1	nejen
zdramatizována	zdramatizován	k2eAgFnSc1d1	zdramatizována
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zfilmována	zfilmovat	k5eAaPmNgFnS	zfilmovat
a	a	k8xC	a
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
komiksové	komiksový	k2eAgFnSc2d1	komiksová
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Široce	široko	k6eAd1	široko
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
Poeovou	Poeový	k2eAgFnSc7d1	Poeový
literaturou	literatura	k1gFnSc7	literatura
inspirovat	inspirovat	k5eAaBmF	inspirovat
např.	např.	kA	např.
Ray	Ray	k1gFnSc1	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neskrývaje	skrývat	k5eNaImSgInS	skrývat
svou	svůj	k3xOyFgFnSc4	svůj
adoraci	adorace	k1gFnSc4	adorace
k	k	k7c3	k
Poeovu	Poeův	k2eAgNnSc3d1	Poeův
dílu	dílo	k1gNnSc3	dílo
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
jeho	jeho	k3xOp3gFnSc3	jeho
osobnosti	osobnost	k1gFnSc3	osobnost
celou	celý	k2eAgFnSc4d1	celá
kapitolu	kapitola	k1gFnSc4	kapitola
z	z	k7c2	z
Marťanské	marťanský	k2eAgFnSc2d1	Marťanská
kroniky	kronika	k1gFnSc2	kronika
(	(	kIx(	(
<g/>
syntetizoval	syntetizovat	k5eAaImAgMnS	syntetizovat
zde	zde	k6eAd1	zde
dva	dva	k4xCgInPc1	dva
zdánlivě	zdánlivě	k6eAd1	zdánlivě
odlišné	odlišný	k2eAgInPc1d1	odlišný
prvky	prvek	k1gInPc1	prvek
z	z	k7c2	z
Masky	maska	k1gFnSc2	maska
červené	červený	k2eAgFnSc2d1	červená
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
z	z	k7c2	z
Jámy	jáma	k1gFnSc2	jáma
a	a	k8xC	a
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
Tamerlane	Tamerlan	k1gMnSc5	Tamerlan
And	Anda	k1gFnPc2	Anda
Other	Othra	k1gFnPc2	Othra
Poems	Poems	k1gInSc1	Poems
-	-	kIx~	-
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
Al	ala	k1gFnPc2	ala
Aaraaf	Aaraaf	k1gInSc1	Aaraaf
<g/>
,	,	kIx,	,
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
Aaraaf	Aaraaf	k1gInSc1	Aaraaf
<g/>
,	,	kIx,	,
Tamerlane	Tamerlan	k1gMnSc5	Tamerlan
and	and	k?	and
Minor	minor	k2eAgFnPc2d1	minor
Poems	Poemsa	k1gFnPc2	Poemsa
-	-	kIx~	-
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
Básně	báseň	k1gFnSc2	báseň
(	(	kIx(	(
<g/>
Poems	Poems	k1gInSc1	Poems
-	-	kIx~	-
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
Havran	Havran	k1gMnSc1	Havran
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Raven	Raven	k1gInSc1	Raven
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Originální	originální	k2eAgInSc1d1	originální
anglický	anglický	k2eAgInSc1d1	anglický
text	text	k1gInSc1	text
a	a	k8xC	a
české	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
Havrana	Havran	k1gMnSc2	Havran
na	na	k7c6	na
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
Wikisource	Wikisourka	k1gFnSc3	Wikisourka
<g/>
)	)	kIx)	)
Zvukový	zvukový	k2eAgInSc4d1	zvukový
záznam	záznam	k1gInSc4	záznam
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
na	na	k7c4	na
LibriVox	LibriVox	k1gInSc4	LibriVox
Ulalume	Ulalum	k1gInSc5	Ulalum
-	-	kIx~	-
1847	[number]	k4	1847
Euréka	Euréko	k1gNnSc2	Euréko
-	-	kIx~	-
prosaická	prosaický	k2eAgFnSc1d1	prosaická
báseň	báseň	k1gFnSc1	báseň
(	(	kIx(	(
<g/>
Eureka	Eureka	k1gFnSc1	Eureka
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
Zvony	zvon	k1gInPc1	zvon
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Bells	Bellsa	k1gFnPc2	Bellsa
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Eldorado	Eldorada	k1gFnSc5	Eldorada
-	-	kIx~	-
1849	[number]	k4	1849
Grotesky	groteska	k1gFnSc2	groteska
a	a	k8xC	a
arabesky	arabeska	k1gFnSc2	arabeska
(	(	kIx(	(
<g/>
Tales	Tales	k1gMnSc1	Tales
Of	Of	k1gMnSc2	Of
The	The	k1gFnSc2	The
Grotesque	Grotesqu	k1gMnSc2	Grotesqu
And	Anda	k1gFnPc2	Anda
Arabesque	Arabesqu	k1gMnSc2	Arabesqu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
Černý	černý	k2eAgInSc1d1	černý
kocour	kocour	k1gInSc1	kocour
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Cat	Cat	k1gMnSc1	Cat
And	Anda	k1gFnPc2	Anda
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1843	[number]	k4	1843
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Tales	Tales	k1gInSc1	Tales
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
postava	postava	k1gFnSc1	postava
C.	C.	kA	C.
Auguste	August	k1gMnSc5	August
Dupin	Dupin	k2eAgInSc1d1	Dupin
(	(	kIx(	(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1	seřazeno
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vraždy	vražda	k1gFnPc1	vražda
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Morgue	Morgue	k1gFnSc1	Morgue
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Murders	Murdersa	k1gFnPc2	Murdersa
In	In	k1gMnSc2	In
The	The	k1gMnSc2	The
Rue	Rue	k1gMnSc2	Rue
Morgue	Morgu	k1gMnSc2	Morgu
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Záhada	záhada	k1gFnSc1	záhada
Marie	Marie	k1gFnSc1	Marie
Rogê	Rogê	k1gFnSc1	Rogê
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
The	The	k1gFnSc1	The
Mystery	Myster	k1gInPc4	Myster
Of	Of	k1gFnSc2	Of
Marie	Maria	k1gFnSc2	Maria
Rogê	Rogê	k1gFnSc2	Rogê
-	-	kIx~	-
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Odcizený	odcizený	k2eAgInSc1d1	odcizený
dopis	dopis	k1gInSc1	dopis
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Purloined	Purloined	k1gMnSc1	Purloined
Letter	Letter	k1gMnSc1	Letter
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
česky	česky	k6eAd1	česky
nebo	nebo	k8xC	nebo
slovensky	slovensky	k6eAd1	slovensky
vydané	vydaný	k2eAgFnPc1d1	vydaná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1	seřazeno
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
U	u	k7c2	u
konce	konec	k1gInSc2	konec
s	s	k7c7	s
dechem	dech	k1gInSc7	dech
(	(	kIx(	(
<g/>
Loss	Loss	k1gInSc1	Loss
of	of	k?	of
Breath	Breath	k1gInSc1	Breath
-	-	kIx~	-
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Vévoda	vévoda	k1gMnSc1	vévoda
De	De	k?	De
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Omelette	Omelett	k1gMnSc5	Omelett
(	(	kIx(	(
<g/>
The	The	k1gMnSc6	The
Duc	duc	k0	duc
De	De	k?	De
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Omelette	Omelett	k1gInSc5	Omelett
-	-	kIx~	-
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
A	a	k9	a
<g />
.	.	kIx.	.
</s>
<s>
Tale	Tale	k1gFnSc1	Tale
of	of	k?	of
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
-	-	kIx~	-
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Ohnivý	ohnivý	k2eAgMnSc1d1	ohnivý
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
Metzengerstein	Metzengerstein	k1gMnSc1	Metzengerstein
-	-	kIx~	-
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Bon-Bon	Bon-Bon	k1gInSc1	Bon-Bon
(	(	kIx(	(
<g/>
Bon-Bon	Bon-Bon	k1gInSc1	Bon-Bon
-	-	kIx~	-
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Rukopis	rukopis	k1gInSc1	rukopis
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
láhvi	láhev	k1gFnSc6	láhev
(	(	kIx(	(
<g/>
MS.	MS.	k1gFnSc1	MS.
Found	Founda	k1gFnPc2	Founda
in	in	k?	in
a	a	k8xC	a
Bottle	Bottle	k1gFnSc1	Bottle
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Dostaveníčko	dostaveníčko	k1gNnSc1	dostaveníčko
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Assignation	Assignation	k1gInSc1	Assignation
-	-	kIx~	-
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Berenice	Berenice	k1gFnSc1	Berenice
(	(	kIx(	(
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Král	Král	k1gMnSc1	Král
Mor	mora	k1gFnPc2	mora
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
Pest	Pest	k1gMnSc1	Pest
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Dělat	dělat	k5eAaImF	dělat
lva	lev	k1gMnSc4	lev
(	(	kIx(	(
<g/>
Lionizing	Lionizing	k1gInSc1	Lionizing
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Morella	Morella	k1gFnSc1	Morella
(	(	kIx(	(
<g/>
Morella	Morella	k1gFnSc1	Morella
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Bezpříkladná	bezpříkladný	k2eAgFnSc1d1	bezpříkladná
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
jistého	jistý	k2eAgMnSc2d1	jistý
Hanse	Hans	k1gMnSc2	Hans
Pfalla	Pfall	k1gMnSc2	Pfall
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Unparalleled	Unparalleled	k1gMnSc1	Unparalleled
Adventure	Adventur	k1gMnSc5	Adventur
of	of	k?	of
One	One	k1gMnSc1	One
Hans	Hans	k1gMnSc1	Hans
Pfaall	Pfaall	k1gMnSc1	Pfaall
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Stín	stín	k1gInSc1	stín
-	-	kIx~	-
Podobenství	podobenství	k1gNnSc1	podobenství
(	(	kIx(	(
<g/>
Shadow	Shadow	k1gFnSc1	Shadow
-	-	kIx~	-
A	a	k9	a
Parable	Parable	k1gFnSc1	Parable
-	-	kIx~	-
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Čtyři	čtyři	k4xCgNnPc4	čtyři
zvířátka	zvířátko	k1gNnPc4	zvířátko
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
<g/>
:	:	kIx,	:
Homo-Camelopardalis	Homo-Camelopardalis	k1gFnSc2	Homo-Camelopardalis
(	(	kIx(	(
<g/>
Four	Four	k1gMnSc1	Four
Beasts	Beastsa	k1gFnPc2	Beastsa
in	in	k?	in
One	One	k1gMnSc1	One
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Homo-Cameleopard	Homo-Cameleopard	k1gMnSc1	Homo-Cameleopard
-	-	kIx~	-
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
<g />
.	.	kIx.	.
</s>
<s>
aneb	aneb	k?	aneb
Petr	Petr	k1gMnSc1	Petr
Tůdle	Tůdl	k1gMnSc2	Tůdl
(	(	kIx(	(
<g/>
Magazine-Writing	Magazine-Writing	k1gInSc1	Magazine-Writing
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
Snook	Snook	k1gInSc1	Snook
-	-	kIx~	-
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
Mystifikace	mystifikace	k1gFnSc1	mystifikace
(	(	kIx(	(
<g/>
Mystification	Mystification	k1gInSc1	Mystification
-	-	kIx~	-
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Ligeia	Ligeia	k1gFnSc1	Ligeia
(	(	kIx(	(
<g/>
Ligeia	Ligeia	k1gFnSc1	Ligeia
-	-	kIx~	-
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Mlčení	mlčení	k1gNnSc1	mlčení
-	-	kIx~	-
Bajka	bajka	k1gFnSc1	bajka
(	(	kIx(	(
<g/>
Silence	silenka	k1gFnSc6	silenka
-	-	kIx~	-
A	a	k9	a
Fable	Fable	k1gNnSc2	Fable
-	-	kIx~	-
1838	[number]	k4	1838
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
V	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
(	(	kIx(	(
<g/>
A	a	k9	a
Predicament	Predicament	k1gInSc1	Predicament
-	-	kIx~	-
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc1	house
of	of	k?	of
Usher	Usher	k1gMnSc1	Usher
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Rozhovor	rozhovor	k1gInSc1	rozhovor
Eirose	Eirosa	k1gFnSc6	Eirosa
s	s	k7c7	s
Charmionem	Charmion	k1gInSc7	Charmion
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Conversation	Conversation	k1gInSc1	Conversation
Of	Of	k1gFnSc2	Of
Eiros	Eirosa	k1gFnPc2	Eirosa
And	Anda	k1gFnPc2	Anda
Charmion	Charmion	k1gInSc1	Charmion
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Ďábel	ďábel	k1gMnSc1	ďábel
ve	v	k7c6	v
zvonici	zvonice	k1gFnSc6	zvonice
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Devil	Devil	k1gInSc1	Devil
in	in	k?	in
the	the	k?	the
Belfry	Belfra	k1gFnSc2	Belfra
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
William	William	k1gInSc1	William
Wilson	Wilson	k1gInSc1	Wilson
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Wilson	Wilson	k1gNnSc1	Wilson
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Muž	muž	k1gMnSc1	muž
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Man	Man	k1gMnSc1	Man
that	that	k1gMnSc1	that
Was	Was	k1gMnSc1	Was
Used	Used	k1gMnSc1	Used
Up	Up	k1gMnSc1	Up
-	-	kIx~	-
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Muž	muž	k1gMnSc1	muž
davu	dav	k1gInSc2	dav
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Crowd	Crowd	k1gMnSc1	Crowd
-	-	kIx~	-
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Proč	proč	k6eAd1	proč
ten	ten	k3xDgMnSc1	ten
malý	malý	k2eAgMnSc1d1	malý
Francous	Francous	k1gMnSc1	Francous
nosí	nosit	k5eAaImIp3nS	nosit
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
pásce	páska	k1gFnSc6	páska
(	(	kIx(	(
<g/>
Why	Why	k1gFnSc1	Why
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Little	Little	k1gFnSc2	Little
Frenchman	Frenchman	k1gMnSc1	Frenchman
Wears	Wearsa	k1gFnPc2	Wearsa
His	his	k1gNnSc1	his
Hand	Hand	k1gMnSc1	Hand
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Sling	Slinga	k1gFnPc2	Slinga
-	-	kIx~	-
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Obchodník	obchodník	k1gMnSc1	obchodník
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Business	business	k1gInSc1	business
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Ostrov	ostrov	k1gInSc1	ostrov
víl	víla	k1gFnPc2	víla
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Island	Island	k1gInSc1	Island
of	of	k?	of
the	the	k?	the
Fay	Fay	k1gFnSc2	Fay
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Tři	tři	k4xCgFnPc1	tři
neděle	neděle	k1gFnPc1	neděle
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
(	(	kIx(	(
<g/>
Three	Three	k1gNnSc1	Three
Sundays	Sundaysa	k1gFnPc2	Sundaysa
in	in	k?	in
a	a	k8xC	a
Week	Week	k1gMnSc1	Week
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Pád	Pád	k1gInSc1	Pád
do	do	k7c2	do
Maelströmu	Maelström	k1gInSc2	Maelström
(	(	kIx(	(
<g/>
A	a	k9	a
Descent	Descent	k1gMnSc1	Descent
into	into	k1gMnSc1	into
the	the	k?	the
Maelström	Maelström	k1gMnSc1	Maelström
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Eleonora	Eleonora	k1gFnSc1	Eleonora
(	(	kIx(	(
<g/>
Eleonora	Eleonora	k1gFnSc1	Eleonora
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
nesázej	sázet	k5eNaImRp2nS	sázet
o	o	k7c6	o
hlavu	hlava	k1gFnSc4	hlava
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Never	Never	k1gMnSc1	Never
Bet	Bet	k1gMnSc1	Bet
the	the	k?	the
Devil	Devil	k1gMnSc1	Devil
Your	Your	k1gMnSc1	Your
Head	Head	k1gMnSc1	Head
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Medailon	medailon	k1gInSc1	medailon
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Oval	ovalit	k5eAaPmRp2nS	ovalit
Portrait	Portrait	k1gMnSc1	Portrait
-	-	kIx~	-
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c6	na
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
<g/>
)	)	kIx)	)
Maska	maska	k1gFnSc1	maska
červené	červený	k2eAgFnSc2d1	červená
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Masque	Masqu	k1gFnSc2	Masqu
of	of	k?	of
the	the	k?	the
Red	Red	k1gMnSc1	Red
Death	Death	k1gMnSc1	Death
-	-	kIx~	-
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
originál	originál	k1gInSc1	originál
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Pit	pit	k2eAgMnSc1d1	pit
and	and	k?	and
the	the	k?	the
Pendulum	Pendulum	k1gInSc1	Pendulum
-	-	kIx~	-
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c6	na
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
brouk	brouk	k1gMnSc1	brouk
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Gold	Gold	k1gMnSc1	Gold
Bug	Bug	k1gMnSc1	Bug
-	-	kIx~	-
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Černý	černý	k2eAgMnSc1d1	černý
kocour	kocour	k1gMnSc1	kocour
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Cat	Cat	k1gMnSc1	Cat
-	-	kIx~	-
1843	[number]	k4	1843
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Zrádné	zrádný	k2eAgNnSc1d1	zrádné
srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Tell-Tale	Tell-Tala	k1gFnSc3	Tell-Tala
Heart	Heart	k1gInSc1	Heart
-	-	kIx~	-
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c6	na
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
<g/>
)	)	kIx)	)
O	o	k7c6	o
šizení	šizení	k1gNnSc6	šizení
jakožto	jakožto	k8xS	jakožto
exaktní	exaktní	k2eAgFnSc3d1	exaktní
vědě	věda	k1gFnSc3	věda
(	(	kIx(	(
<g/>
Diddling	Diddling	k1gInSc1	Diddling
<g/>
,	,	kIx,	,
Considered	Considered	k1gMnSc1	Considered
as	as	k1gNnSc2	as
one	one	k?	one
of	of	k?	of
the	the	k?	the
Exact	Exact	k1gMnSc1	Exact
Sciences	Sciences	k1gMnSc1	Sciences
-	-	kIx~	-
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Mesmerické	mesmerický	k2eAgNnSc1d1	mesmerický
odhalení	odhalení	k1gNnSc1	odhalení
(	(	kIx(	(
<g/>
Mesmeric	Mesmerice	k1gFnPc2	Mesmerice
Revelation	Revelation	k1gInSc1	Revelation
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Literární	literární	k2eAgInSc1d1	literární
život	život	k1gInSc1	život
váženého	vážený	k2eAgMnSc2d1	vážený
pana	pan	k1gMnSc2	pan
Tenta	Tent	k1gMnSc2	Tent
Nonce	Nonce	k1gMnSc2	Nonce
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Literary	Literara	k1gFnSc2	Literara
Life	Lif	k1gMnSc2	Lif
of	of	k?	of
Thingum	Thingum	k1gNnSc1	Thingum
Bob	Bob	k1gMnSc1	Bob
<g/>
,	,	kIx,	,
Esq	Esq	k1gMnSc1	Esq
<g/>
.	.	kIx.	.
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Rozeklaných	rozeklaný	k2eAgFnPc2d1	rozeklaná
hor	hora	k1gFnPc2	hora
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Tale	Tale	k1gFnSc1	Tale
Of	Of	k1gFnSc2	Of
The	The	k1gFnSc1	The
Ragged	Ragged	k1gMnSc1	Ragged
Mountains	Mountains	k1gInSc1	Mountains
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Předčasný	předčasný	k2eAgInSc4d1	předčasný
pohřeb	pohřeb	k1gInSc4	pohřeb
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Premature	Prematur	k1gMnSc5	Prematur
Burial	Burial	k1gMnSc1	Burial
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Senzace	senzace	k1gFnSc1	senzace
s	s	k7c7	s
balónem	balón	k1gInSc7	balón
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Balloon-Hoax	Balloon-Hoax	k1gInSc1	Balloon-Hoax
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
bedna	bedna	k1gFnSc1	bedna
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Oblong	Oblong	k1gMnSc1	Oblong
Box	box	k1gInSc1	box
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Vrah	vrah	k1gMnSc1	vrah
jsi	být	k5eAaImIp2nS	být
ty	ty	k3xPp2nSc1	ty
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Thou	Tho	k1gMnSc3	Tho
Art	Art	k1gMnSc3	Art
the	the	k?	the
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Anděl	Anděla	k1gFnPc2	Anděla
pitvornosti	pitvornost	k1gFnSc2	pitvornost
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Angel	angel	k1gMnSc1	angel
of	of	k?	of
the	the	k?	the
Odd	odd	kA	odd
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Metoda	metoda	k1gFnSc1	metoda
doktora	doktor	k1gMnSc2	doktor
Téra	Térus	k1gMnSc2	Térus
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
Péra	péro	k1gNnSc2	péro
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
System	Syst	k1gInSc7	Syst
of	of	k?	of
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Tarr	Tarr	k1gMnSc1	Tarr
and	and	k?	and
Prof.	prof.	kA	prof.
<g />
.	.	kIx.	.
</s>
<s>
Fether	Fethra	k1gFnPc2	Fethra
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Brýle	brýle	k1gFnPc1	brýle
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Spectacles	Spectacles	k1gMnSc1	Spectacles
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Na	na	k7c4	na
slovíčko	slovíčko	k1gNnSc4	slovíčko
s	s	k7c7	s
mumií	mumie	k1gFnSc7	mumie
(	(	kIx(	(
<g/>
Some	Some	k1gFnSc1	Some
Words	Wordsa	k1gFnPc2	Wordsa
with	witha	k1gFnPc2	witha
a	a	k8xC	a
Mummy	Mumma	k1gFnSc2	Mumma
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Démon	démon	k1gMnSc1	démon
zvrácenosti	zvrácenost	k1gFnSc2	zvrácenost
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Imp	Imp	k1gMnSc1	Imp
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
the	the	k?	the
Perverse	perverse	k1gFnSc2	perverse
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Tisící	tisící	k4xOgFnSc1	tisící
druhý	druhý	k4xOgInSc1	druhý
Šeherezádin	Šeherezádin	k2eAgInSc1d1	Šeherezádin
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Thousand-And-Second	Thousand-And-Second	k1gInSc1	Thousand-And-Second
Tale	Tale	k1gFnSc1	Tale
of	of	k?	of
Scheherazade	Scheherazad	k1gInSc5	Scheherazad
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Fakta	faktum	k1gNnSc2	faktum
případu	případ	k1gInSc2	případ
monsieura	monsieura	k1gFnSc1	monsieura
Valdemara	Valdemara	k1gFnSc1	Valdemara
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Facts	Facts	k1gInSc1	Facts
in	in	k?	in
the	the	k?	the
Case	Cas	k1gFnSc2	Cas
of	of	k?	of
M.	M.	kA	M.
Valdemar	Valdemara	k1gFnPc2	Valdemara
-	-	kIx~	-
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
originál	originál	k1gInSc1	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Sud	sud	k1gInSc1	sud
vína	víno	k1gNnSc2	víno
amontilladského	amontilladský	k2eAgNnSc2d1	amontilladský
(	(	kIx(	(
<g/>
The	The	k1gFnSc4	The
Cask	Caska	k1gFnPc2	Caska
of	of	k?	of
Amontillado	Amontillada	k1gFnSc5	Amontillada
-	-	kIx~	-
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Sphinx	Sphinx	k1gInSc1	Sphinx
-	-	kIx~	-
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Arnheimské	Arnheimský	k2eAgNnSc1d1	Arnheimský
panství	panství	k1gNnSc1	panství
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Domain	Domain	k1gMnSc1	Domain
of	of	k?	of
Arnheim	Arnheim	k1gMnSc1	Arnheim
-	-	kIx~	-
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
originál	originál	k1gInSc1	originál
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Skokan	Skokan	k1gMnSc1	Skokan
(	(	kIx(	(
<g/>
Hop-Frog	Hop-Frog	k1gMnSc1	Hop-Frog
-	-	kIx~	-
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c6	na
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
<g/>
)	)	kIx)	)
Von	von	k1gInSc1	von
Kempelen	Kempelen	k2eAgInSc1d1	Kempelen
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
(	(	kIx(	(
<g/>
Von	von	k1gInSc1	von
Kempelen	Kempelna	k1gFnPc2	Kempelna
and	and	k?	and
his	his	k1gNnSc4	his
Discovery	Discovera	k1gFnSc2	Discovera
-	-	kIx~	-
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Landorova	Landorův	k2eAgFnSc1d1	Landorův
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
Landor	Landor	k1gInSc1	Landor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cottage	Cottage	k1gFnSc7	Cottage
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
(	(	kIx(	(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1	seřazeno
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Colloquy	Colloqua	k1gFnSc2	Colloqua
of	of	k?	of
Monos	Monos	k1gMnSc1	Monos
and	and	k?	and
Una	Una	k1gMnSc1	Una
-	-	kIx~	-
1841	[number]	k4	1841
The	The	k1gMnSc1	The
Power	Power	k1gMnSc1	Power
of	of	k?	of
Words	Words	k1gInSc1	Words
-	-	kIx~	-
1841	[number]	k4	1841
Mellonta	Mellonta	k1gMnSc1	Mellonta
Tauta	Tauta	k1gMnSc1	Tauta
-	-	kIx~	-
1849	[number]	k4	1849
X-ing	Xnga	k1gFnPc2	X-inga
a	a	k8xC	a
Paragrab	Paragraba	k1gFnPc2	Paragraba
-	-	kIx~	-
1849	[number]	k4	1849
Příběhy	příběh	k1gInPc7	příběh
Arthura	Arthur	k1gMnSc2	Arthur
Gordona	Gordon	k1gMnSc2	Gordon
Pyma	Pymus	k1gMnSc2	Pymus
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Narrative	Narrativ	k1gInSc5	Narrativ
Of	Of	k1gMnSc1	Of
Arthur	Arthura	k1gFnPc2	Arthura
Gordon	Gordon	k1gMnSc1	Gordon
Pym	Pym	k1gMnSc1	Pym
of	of	k?	of
Nantucket	Nantucket	k1gMnSc1	Nantucket
-	-	kIx~	-
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Julius	Julius	k1gMnSc1	Julius
Rodman	Rodman	k1gMnSc1	Rodman
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
-	-	kIx~	-
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Maelzelův	Maelzelův	k2eAgMnSc1d1	Maelzelův
Šachista	šachista	k1gMnSc1	šachista
(	(	kIx(	(
<g/>
Maelzel	Maelzel	k1gMnSc1	Maelzel
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chess-Player	Chess-Playra	k1gFnPc2	Chess-Playra
-	-	kIx~	-
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Kterak	kterak	k8xS	kterak
psáti	psát	k5eAaImF	psát
článek	článek	k1gInSc4	článek
pro	pro	k7c4	pro
Blackwood	Blackwood	k1gInSc4	Blackwood
(	(	kIx(	(
<g/>
How	How	k1gFnSc4	How
to	ten	k3xDgNnSc1	ten
Write	Writ	k1gInSc5	Writ
a	a	k8xC	a
Blackwood	Blackwood	k1gInSc4	Blackwood
Article	Article	k1gFnSc2	Article
-	-	kIx~	-
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Filosofie	filosofie	k1gFnSc1	filosofie
nábytku	nábytek	k1gInSc2	nábytek
(	(	kIx(	(
<g/>
Philosophy	Philosopha	k1gFnPc1	Philosopha
of	of	k?	of
Furniture	Furnitur	k1gMnSc5	Furnitur
-	-	kIx~	-
1840	[number]	k4	1840
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
A	a	k9	a
Few	Few	k1gFnSc4	Few
Words	Words	k1gInSc4	Words
on	on	k3xPp3gMnSc1	on
Secret	Secret	k1gMnSc1	Secret
Writing	Writing	k1gInSc1	Writing
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Morning	Morning	k1gInSc1	Morning
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Wissahiccon	Wissahiccon	k1gNnSc1	Wissahiccon
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Filosofie	filosofie	k1gFnSc1	filosofie
básnické	básnický	k2eAgFnSc2d1	básnická
skladby	skladba	k1gFnSc2	skladba
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Philosophy	Philosopha	k1gFnSc2	Philosopha
of	of	k?	of
Composition	Composition	k1gInSc1	Composition
-	-	kIx~	-
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
originál	originál	k1gInSc1	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Eureka	Eureka	k1gFnSc1	Eureka
<g/>
:	:	kIx,	:
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
hmotném	hmotný	k2eAgInSc6d1	hmotný
a	a	k8xC	a
duchovním	duchovní	k2eAgInSc6d1	duchovní
vesmíru	vesmír	k1gInSc6	vesmír
(	(	kIx(	(
<g/>
Eureka	Eureko	k1gNnSc2	Eureko
<g/>
:	:	kIx,	:
A	a	k8xC	a
Prose	prosa	k1gFnSc3	prosa
Poem	poema	k1gFnPc2	poema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Rationale	Rationale	k1gFnSc2	Rationale
of	of	k?	of
Verse	verse	k1gFnSc2	verse
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Poetic	Poetice	k1gFnPc2	Poetice
<g />
.	.	kIx.	.
</s>
<s>
Principle	Principle	k6eAd1	Principle
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
originál	originál	k1gInSc4	originál
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
<g/>
)	)	kIx)	)
Maják	maják	k1gInSc1	maják
-	-	kIx~	-
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
nebo	nebo	k8xC	nebo
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
fragmentu	fragment	k1gInSc2	fragment
textu	text	k1gInSc2	text
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Robert	Robert	k1gMnSc1	Robert
Bloch	Bloch	k1gMnSc1	Bloch
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
text	text	k1gInSc1	text
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
na	na	k7c6	na
Wikisource	Wikisourka	k1gFnSc6	Wikisourka
The	The	k1gMnSc1	The
Conchologist	Conchologist	k1gMnSc1	Conchologist
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
First	First	k1gMnSc1	First
Book	Book	k1gMnSc1	Book
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
-	-	kIx~	-
učebnice	učebnice	k1gFnPc1	učebnice
konchologie	konchologie	k1gFnSc2	konchologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sice	sice	k8xC	sice
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pod	pod	k7c7	pod
Poeovým	Poeův	k2eAgNnSc7d1	Poeův
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napsal	napsat	k5eAaPmAgMnS	napsat
ji	on	k3xPp3gFnSc4	on
Thomas	Thomas	k1gMnSc1	Thomas
Wyatt	Wyatt	k1gMnSc1	Wyatt
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
A.	A.	kA	A.
Poe	Poe	k1gMnSc1	Poe
jeho	jeho	k3xOp3gInSc4	jeho
text	text	k1gInSc4	text
upravil	upravit	k5eAaPmAgMnS	upravit
a	a	k8xC	a
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česká	český	k2eAgNnPc1d1	české
a	a	k8xC	a
slovenská	slovenský	k2eAgNnPc1d1	slovenské
vydání	vydání	k1gNnPc1	vydání
obsahující	obsahující	k2eAgFnSc2d1	obsahující
povídky	povídka	k1gFnSc2	povídka
či	či	k8xC	či
básně	báseň	k1gFnSc2	báseň
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gFnSc1	Poea
<g/>
)	)	kIx)	)
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
A.G.	A.G.	k1gFnSc2	A.G.
<g/>
Pyma	Pymum	k1gNnSc2	Pymum
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
vydal	vydat	k5eAaPmAgMnS	vydat
Melantrich	Melantrich	k1gMnSc1	Melantrich
Praha	Praha	k1gFnSc1	Praha
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Anděl	Anděla	k1gFnPc2	Anděla
pitvornosti	pitvornost	k1gFnSc2	pitvornost
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Černý	černý	k2eAgInSc1d1	černý
kocour	kocour	k1gInSc1	kocour
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Četba	četba	k1gFnSc1	četba
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
brožovaná	brožovaný	k2eAgFnSc1d1	brožovaná
<g/>
,	,	kIx,	,
152	[number]	k4	152
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
45	[number]	k4	45
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Černý	černý	k2eAgInSc1d1	černý
kocour	kocour	k1gInSc1	kocour
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
hororové	hororový	k2eAgFnPc1d1	hororová
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
U	U	kA	U
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Dana	Dana	k1gFnSc1	Dana
Krejčová	Krejčová	k1gFnSc1	Krejčová
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnSc1	ilustrace
Luis	Luisa	k1gFnPc2	Luisa
Scafati	Scafati	k1gFnPc2	Scafati
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
upr	upr	k?	upr
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
96	[number]	k4	96
s.	s.	k?	s.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87222	[number]	k4	87222
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
Démon	démon	k1gMnSc1	démon
zvrácenosti	zvrácenost	k1gFnSc2	zvrácenost
<g/>
:	:	kIx,	:
Detektivní	detektivní	k2eAgInPc1d1	detektivní
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
senzační	senzační	k2eAgInPc1d1	senzační
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
86202	[number]	k4	86202
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
184	[number]	k4	184
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Sacher	Sachra	k1gFnPc2	Sachra
Edgar	Edgar	k1gMnSc1	Edgar
(	(	kIx(	(
<g/>
Dryada	Dryada	k1gFnSc1	Dryada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Fantastic	Fantastice	k1gFnPc2	Fantastice
Tales	Talesa	k1gFnPc2	Talesa
/	/	kIx~	/
Fantastické	fantastický	k2eAgInPc1d1	fantastický
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Havran	Havran	k1gMnSc1	Havran
-	-	kIx~	-
šestnáct	šestnáct	k4xCc4	šestnáct
českých	český	k2eAgInPc2d1	český
překladů	překlad	k1gInPc2	překlad
Hrůzný	hrůzný	k2eAgMnSc1d1	hrůzný
stařec	stařec	k1gMnSc1	stařec
<g/>
:	:	kIx,	:
Deset	deset	k4xCc4	deset
světových	světový	k2eAgInPc2d1	světový
horrorů	horror	k1gInPc2	horror
(	(	kIx(	(
<g/>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
/	/	kIx~	/
The	The	k1gMnSc1	The
Pit	pit	k2eAgMnSc1d1	pit
and	and	k?	and
the	the	k?	the
Pendulum	Pendulum	k1gInSc1	Pendulum
and	and	k?	and
other	other	k1gInSc1	other
stories	stories	k1gInSc1	stories
(	(	kIx(	(
<g/>
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
&	&	k?	&
kyvadlo	kyvadlo	k1gNnSc4	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
fantastické	fantastický	k2eAgFnPc4d1	fantastická
<g />
.	.	kIx.	.
</s>
<s>
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Odeon	odeon	k1gInSc1	odeon
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc1	KMa
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Krajina	Krajina	k1gFnSc1	Krajina
stínů	stín	k1gInPc2	stín
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Lupiči	lupič	k1gMnPc1	lupič
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Světové	světový	k2eAgInPc1d1	světový
horory	horor	k1gInPc1	horor
(	(	kIx(	(
<g/>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Na	na	k7c4	na
slovíčko	slovíčko	k1gNnSc4	slovíčko
s	s	k7c7	s
mumií	mumie	k1gFnSc7	mumie
<g/>
:	:	kIx,	:
Grotesky	groteska	k1gFnPc1	groteska
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
směšné	směšný	k2eAgInPc1d1	směšný
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Hynek	Hynek	k1gMnSc1	Hynek
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Pád	Pád	k1gInSc1	Pád
do	do	k7c2	do
Maelströmu	Maelström	k1gInSc2	Maelström
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
939	[number]	k4	939
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šenkyřík	Šenkyřík	k1gMnSc1	Šenkyřík
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
248	[number]	k4	248
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Alén	Alén	k1gMnSc1	Alén
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
)	)	kIx)	)
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
:	:	kIx,	:
Horrory	horror	k1gInPc1	horror
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g />
.	.	kIx.	.
</s>
<s>
děsivé	děsivý	k2eAgInPc1d1	děsivý
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Hynek	Hynek	k1gMnSc1	Hynek
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Půlnoční	půlnoční	k2eAgFnSc2d1	půlnoční
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
sestavily	sestavit	k5eAaPmAgFnP	sestavit
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ceplová	Ceplová	k1gFnSc1	Ceplová
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
Rosíková	Rosíková	k1gFnSc1	Rosíková
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Stráž	stráž	k1gFnSc4	stráž
u	u	k7c2	u
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
hrůzostrašné	hrůzostrašný	k2eAgFnPc1d1	hrůzostrašná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
sestavila	sestavit	k5eAaPmAgFnS	sestavit
Marie	Marie	k1gFnSc1	Marie
Švestková	Švestková	k1gFnSc1	Švestková
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
V	v	k7c6	v
mistrově	mistrův	k2eAgInSc6d1	mistrův
stínu	stín	k1gInSc6	stín
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnSc2	povídka
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
301	[number]	k4	301
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šenkyřík	Šenkyřík	k1gMnSc1	Šenkyřík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
384	[number]	k4	384
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
obálka	obálka	k1gFnSc1	obálka
<g/>
:	:	kIx,	:
Isifa	Isif	k1gMnSc2	Isif
Image	image	k1gFnSc1	image
Service	Service	k1gFnSc1	Service
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Karman	karman	k1gInSc1	karman
<g/>
)	)	kIx)	)
Vraždy	vražda	k1gFnPc1	vražda
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Morgue	Morgu	k1gFnSc2	Morgu
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vladimír	Vladimír	k1gMnSc1	Vladimír
Henzl	Henzl	k1gMnSc1	Henzl
<g/>
,	,	kIx,	,
brožovaná	brožovaný	k2eAgFnSc1d1	brožovaná
<g/>
,	,	kIx,	,
82	[number]	k4	82
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Marcel	Marcel	k1gMnSc1	Marcel
Stecker	Stecker	k1gMnSc1	Stecker
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
(	(	kIx(	(
<g/>
Tatran	Tatran	k1gInSc1	Tatran
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
295	[number]	k4	295
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
<g/>
:	:	kIx,	:
Devatero	devatero	k1gNnSc1	devatero
podivuhodných	podivuhodný	k2eAgInPc2d1	podivuhodný
příběhů	příběh	k1gInPc2	příběh
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
(	(	kIx(	(
<g/>
Albatros	albatros	k1gMnSc1	albatros
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Klub	klub	k1gInSc1	klub
mladých	mladý	k2eAgMnPc2d1	mladý
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
228	[number]	k4	228
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Havran	Havran	k1gMnSc1	Havran
<g/>
;	;	kIx,	;
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
<g/>
;	;	kIx,	;
Príhody	Príhoda	k1gFnSc2	Príhoda
Arthura	Arthur	k1gMnSc2	Arthur
Gordona	Gordon	k1gMnSc2	Gordon
Pyma	Pymus	k1gMnSc2	Pymus
(	(	kIx(	(
<g/>
Tatran	Tatran	k1gInSc1	Tatran
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jana	Jana	k1gFnSc1	Jana
Kantorová-Báliková	Kantorová-Bálikový	k2eAgFnSc1d1	Kantorová-Bálikový
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
622	[number]	k4	622
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
Zrádné	zrádný	k2eAgNnSc1d1	zrádné
srdce	srdce	k1gNnSc1	srdce
<g/>
:	:	kIx,	:
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
vázaná	vázané	k1gNnPc4	vázané
s	s	k7c7	s
papírovým	papírový	k2eAgInSc7d1	papírový
přebalem	přebal	k1gInSc7	přebal
<g/>
,	,	kIx,	,
676	[number]	k4	676
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
