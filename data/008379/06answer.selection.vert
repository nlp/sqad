<s>
Louis	Louis	k1gMnSc1	Louis
Braille	Braille	k1gFnSc2	Braille
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
lui	lui	k?	lui
braj	braj	k1gInSc1	braj
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1809	[number]	k4	1809
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vynálezce	vynálezce	k1gMnSc1	vynálezce
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
systému	systém	k1gInSc2	systém
psaní	psaní	k1gNnSc2	psaní
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
<g/>
.	.	kIx.	.
</s>
