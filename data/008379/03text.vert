<p>
<s>
Louis	Louis	k1gMnSc1	Louis
Braille	Braille	k1gFnSc2	Braille
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
lui	lui	k?	lui
braj	braj	k1gInSc1	braj
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1809	[number]	k4	1809
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vynálezce	vynálezce	k1gMnSc1	vynálezce
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
systému	systém	k1gInSc2	systém
psaní	psaní	k1gNnSc2	psaní
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Braille	Braille	k6eAd1	Braille
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Coupvray	Coupvraa	k1gFnSc2	Coupvraa
blízko	blízko	k7c2	blízko
Paříže	Paříž	k1gFnSc2	Paříž
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Simon-René	Simon-Rený	k2eAgFnSc2d1	Simon-Rený
Braille	Braille	k1gFnSc2	Braille
byl	být	k5eAaImAgMnS	být
sedlář	sedlář	k1gMnSc1	sedlář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
Louis	Louis	k1gMnSc1	Louis
poranil	poranit	k5eAaPmAgMnS	poranit
levé	levý	k2eAgNnSc1d1	levé
oko	oko	k1gNnSc1	oko
šídlem	šídlo	k1gNnSc7	šídlo
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
dílny	dílna	k1gFnSc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přenesla	přenést	k5eAaPmAgFnS	přenést
i	i	k9	i
na	na	k7c4	na
pravé	pravý	k2eAgNnSc4d1	pravé
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
dostal	dostat	k5eAaPmAgMnS	dostat
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
Institutu	institut	k1gInSc6	institut
des	des	k1gNnSc1	des
Jeunes	Jeunes	k1gInSc1	Jeunes
Aveugles	Aveugles	k1gMnSc1	Aveugles
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
slepou	slepý	k2eAgFnSc4d1	slepá
mládež	mládež	k1gFnSc4	mládež
<g/>
)	)	kIx)	)
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
tomtéž	týž	k3xTgInSc6	týž
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
učily	učít	k5eAaPmAgFnP	učít
číst	číst	k5eAaImF	číst
pouze	pouze	k6eAd1	pouze
rozeznáváním	rozeznávání	k1gNnSc7	rozeznávání
vystouplých	vystouplý	k2eAgNnPc2d1	vystouplé
písmen	písmeno	k1gNnPc2	písmeno
hmatem	hmat	k1gInSc7	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Nemohly	moct	k5eNaImAgInP	moct
však	však	k9	však
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tisk	tisk	k1gInSc1	tisk
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
vlisováním	vlisování	k1gNnSc7	vlisování
drátěných	drátěný	k2eAgNnPc2d1	drátěné
písmen	písmeno	k1gNnPc2	písmeno
do	do	k7c2	do
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
patnáct	patnáct	k4xCc1	patnáct
<g/>
,	,	kIx,	,
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
systém	systém	k1gInSc4	systém
vyražených	vyražený	k2eAgNnPc2d1	vyražené
(	(	kIx(	(
<g/>
vystouplých	vystouplý	k2eAgNnPc2d1	vystouplé
<g/>
)	)	kIx)	)
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
systém	systém	k1gInSc1	systém
kapitána	kapitán	k1gMnSc2	kapitán
Charlese	Charles	k1gMnSc2	Charles
Barbiera	Barbier	k1gMnSc2	Barbier
de	de	k?	de
la	la	k1gNnSc1	la
Serre	Serr	k1gMnSc5	Serr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
navštívil	navštívit	k5eAaPmAgMnS	navštívit
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgInS	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
již	již	k6eAd1	již
nepoužívaný	používaný	k2eNgInSc1d1	nepoužívaný
vojenský	vojenský	k2eAgInSc1d1	vojenský
systém	systém	k1gInSc1	systém
nočního	noční	k2eAgNnSc2d1	noční
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
přijímat	přijímat	k5eAaImF	přijímat
rozkazy	rozkaz	k1gInPc4	rozkaz
i	i	k9	i
za	za	k7c4	za
tmy	tma	k1gFnPc4	tma
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
dvanácti	dvanáct	k4xCc6	dvanáct
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Braille	Braille	k6eAd1	Braille
jej	on	k3xPp3gMnSc4	on
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
použil	použít	k5eAaPmAgMnS	použít
bodů	bod	k1gInPc2	bod
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Louis	Louis	k1gMnSc1	Louis
Braille	Braill	k1gMnSc2	Braill
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
slepeckém	slepecký	k2eAgInSc6d1	slepecký
ústavu	ústav	k1gInSc6	ústav
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
příručku	příručka	k1gFnSc4	příručka
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
Braille	Braille	k1gFnSc2	Braille
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
psaní	psaní	k1gNnPc4	psaní
přidáním	přidání	k1gNnSc7	přidání
znamének	znaménko	k1gNnPc2	znaménko
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
matematických	matematický	k2eAgInPc2d1	matematický
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
Nemeth	Nemeth	k1gInSc1	Nemeth
Braille	Braille	k1gFnSc2	Braille
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Braille	Braille	k1gFnSc1	Braille
music	music	k1gMnSc1	music
code	code	k1gInSc1	code
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
však	však	k9	však
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
systém	systém	k1gInSc1	systém
prosadil	prosadit	k5eAaPmAgInS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1852	[number]	k4	1852
se	se	k3xPyFc4	se
v	v	k7c6	v
Braillově	Braillův	k2eAgNnSc6d1	Braillovo
písmu	písmo	k1gNnSc6	písmo
začaly	začít	k5eAaPmAgFnP	začít
tisknout	tisknout	k5eAaImF	tisknout
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
byl	být	k5eAaImAgInS	být
slabikář	slabikář	k1gInSc1	slabikář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Braille	Braille	k1gInSc1	Braille
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
chatrné	chatrný	k2eAgFnSc2d1	chatrná
zdraví	zdraví	k1gNnSc3	zdraví
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Omezoval	omezovat	k5eAaImAgMnS	omezovat
hodiny	hodina	k1gFnPc4	hodina
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1844	[number]	k4	1844
se	se	k3xPyFc4	se
učení	učení	k1gNnSc2	učení
definitivně	definitivně	k6eAd1	definitivně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
zdokonalování	zdokonalování	k1gNnSc2	zdokonalování
svého	svůj	k3xOyFgNnSc2	svůj
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1847	[number]	k4	1847
sestavil	sestavit	k5eAaPmAgInS	sestavit
první	první	k4xOgInSc4	první
psací	psací	k2eAgInSc4d1	psací
stroj	stroj	k1gInSc4	stroj
pro	pro	k7c4	pro
Braillovou	Braillová	k1gFnSc4	Braillová
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1851	[number]	k4	1851
musel	muset	k5eAaImAgMnS	muset
všechnu	všechen	k3xTgFnSc4	všechen
činnost	činnost	k1gFnSc4	činnost
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
rapidně	rapidně	k6eAd1	rapidně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vykašlávat	vykašlávat	k5eAaImF	vykašlávat
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Braille	Braille	k1gInSc1	Braille
zemřel	zemřít	k5eAaPmAgInS	zemřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1852	[number]	k4	1852
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
43	[number]	k4	43
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
slavnostně	slavnostně	k6eAd1	slavnostně
převezeny	převézt	k5eAaPmNgInP	převézt
do	do	k7c2	do
pařížského	pařížský	k2eAgInSc2d1	pařížský
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
ruce	ruka	k1gFnPc1	ruka
byly	být	k5eAaImAgFnP	být
ponechány	ponechat	k5eAaPmNgFnP	ponechat
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
hrobě	hrob	k1gInSc6	hrob
v	v	k7c4	v
Coupvray	Coupvraa	k1gFnPc4	Coupvraa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
bylo	být	k5eAaImAgNnS	být
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
oficiálně	oficiálně	k6eAd1	oficiálně
uznáno	uznat	k5eAaPmNgNnS	uznat
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
mezi	mezi	k7c7	mezi
nevidomými	vidomý	k2eNgFnPc7d1	nevidomá
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1878	[number]	k4	1878
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
ve	v	k7c6	v
slepeckých	slepecký	k2eAgFnPc6d1	slepecká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Braillova	Braillův	k2eAgFnSc1d1	Braillova
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
posmrtné	posmrtný	k2eAgFnSc2d1	posmrtná
masky	maska	k1gFnSc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
byl	být	k5eAaImAgInS	být
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
portrétech	portrét	k1gInPc6	portrét
<g/>
,	,	kIx,	,
pomnících	pomník	k1gInPc6	pomník
<g/>
,	,	kIx,	,
poštovních	poštovní	k2eAgFnPc6d1	poštovní
známkách	známka	k1gFnPc6	známka
a	a	k8xC	a
mincích	mince	k1gFnPc6	mince
vydaných	vydaný	k2eAgInPc2d1	vydaný
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
v	v	k7c4	v
Coupvray	Coupvray	k1gInPc4	Coupvray
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
předměty	předmět	k1gInPc1	předmět
spjaté	spjatý	k2eAgInPc1d1	spjatý
s	s	k7c7	s
Braillovým	Braillův	k2eAgInSc7d1	Braillův
životem	život	k1gInSc7	život
vč.	vč.	k?	vč.
pomůcek	pomůcka	k1gFnPc2	pomůcka
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
<g/>
.	.	kIx.	.
<g/>
Rok	rok	k1gInSc1	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
nevidomých	vidomý	k2eNgFnPc2d1	nevidomá
Rokem	rok	k1gInSc7	rok
Louise	Louis	k1gMnSc2	Louis
Brailla	Braill	k1gMnSc2	Braill
na	na	k7c4	na
připomínku	připomínka	k1gFnSc4	připomínka
dvoustého	dvoustý	k2eAgNnSc2d1	dvoustý
výročí	výročí	k1gNnSc2	výročí
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
200	[number]	k4	200
let	léto	k1gNnPc2	léto
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
Louise	Louis	k1gMnSc4	Louis
Brailleho	Braille	k1gMnSc4	Braille
vydala	vydat	k5eAaPmAgFnS	vydat
srbská	srbský	k2eAgFnSc1d1	Srbská
pošta	pošta	k1gFnSc1	pošta
známku	známka	k1gFnSc4	známka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
46	[number]	k4	46
dinárů	dinár	k1gInPc2	dinár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
STREIT	STREIT	kA	STREIT
<g/>
,	,	kIx,	,
Jakob	Jakob	k1gMnSc1	Jakob
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Louise	Louis	k1gMnSc2	Louis
Braille	Braill	k1gMnSc2	Braill
<g/>
:	:	kIx,	:
nevidomý	nevidomý	k1gMnSc1	nevidomý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
slepecké	slepecký	k2eAgNnSc4d1	slepecké
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Dostalová	Dostalová	k1gFnSc1	Dostalová
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnPc1	ilustrace
Christiane	Christian	k1gMnSc5	Christian
Leschová	Leschová	k1gFnSc1	Leschová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
<g/>
:	:	kIx,	:
Fabula	Fabula	k1gFnSc1	Fabula
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86600	[number]	k4	86600
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Louis	louis	k1gInSc2	louis
Braille	Braille	k1gFnSc2	Braille
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vlastních	vlastní	k2eAgNnPc6d1	vlastní
křídlech	křídlo	k1gNnPc6	křídlo
–	–	k?	–
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
Beaty	beat	k1gInPc1	beat
Panákové	Panákové	k?	Panákové
natočená	natočený	k2eAgNnPc4d1	natočené
Českým	český	k2eAgInSc7d1	český
rozhlasem	rozhlas	k1gInSc7	rozhlas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
</s>
</p>
