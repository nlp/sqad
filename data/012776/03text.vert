<p>
<s>
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
situační	situační	k2eAgFnSc1d1	situační
komedie	komedie	k1gFnSc1	komedie
Carla	Carl	k1gMnSc2	Carl
Goldoniho	Goldoni	k1gMnSc2	Goldoni
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgInSc1d1	spadající
do	do	k7c2	do
specifického	specifický	k2eAgInSc2d1	specifický
žánru	žánr	k1gInSc2	žánr
commedia	commedium	k1gNnSc2	commedium
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gFnPc2	arte
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
Beatrice	Beatrice	k1gFnSc1	Beatrice
převlečená	převlečený	k2eAgFnSc1d1	převlečená
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
tam	tam	k6eAd1	tam
najít	najít	k5eAaPmF	najít
svého	svůj	k3xOyFgMnSc4	svůj
milence	milenec	k1gMnSc4	milenec
Florinda	Florind	k1gMnSc4	Florind
<g/>
.	.	kIx.	.
</s>
<s>
Ubytuje	ubytovat	k5eAaPmIp3nS	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
Brighelle	Brighelle	k1gInSc4	Brighelle
a	a	k8xC	a
najme	najmout	k5eAaPmIp3nS	najmout
si	se	k3xPyFc3	se
sluhu	sluha	k1gMnSc4	sluha
Truffaldina	Truffaldin	k2eAgMnSc4d1	Truffaldin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
i	i	k9	i
u	u	k7c2	u
Florinda	Florind	k1gMnSc2	Florind
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
tedy	tedy	k9	tedy
dvěma	dva	k4xCgMnPc3	dva
pánům	pan	k1gMnPc3	pan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
hodně	hodně	k6eAd1	hodně
komických	komický	k2eAgFnPc2d1	komická
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
když	když	k8xS	když
je	on	k3xPp3gFnPc4	on
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
při	při	k7c6	při
obědě	oběd	k1gInSc6	oběd
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pěkný	pěkný	k2eAgInSc4d1	pěkný
zmatek	zmatek	k1gInSc4	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
má	mít	k5eAaImIp3nS	mít
vyvětrat	vyvětrat	k5eAaPmF	vyvětrat
šatník	šatník	k1gInSc4	šatník
svých	svůj	k3xOyFgMnPc2	svůj
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
poplete	poplést	k5eAaPmIp3nS	poplést
jaké	jaký	k3yQgInPc4	jaký
šaty	šat	k1gInPc4	šat
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
jakého	jaký	k3yQgInSc2	jaký
kufru	kufr	k1gInSc2	kufr
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
řekne	říct	k5eAaPmIp3nS	říct
svým	svůj	k3xOyFgMnPc3	svůj
pánům	pan	k1gMnPc3	pan
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
toho	ten	k3xDgMnSc2	ten
druhého	druhý	k4xOgMnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Sluha	sluha	k1gMnSc1	sluha
Truffaldino	Truffaldin	k2eAgNnSc1d1	Truffaldino
sehraje	sehrát	k5eAaPmIp3nS	sehrát
mnoho	mnoho	k4c1	mnoho
ztřeštěných	ztřeštěný	k2eAgInPc2d1	ztřeštěný
kousků	kousek	k1gInPc2	kousek
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
i	i	k9	i
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Klaričiny	Klaričina	k1gFnSc2	Klaričina
služky	služka	k1gFnSc2	služka
Smeraldiny	Smeraldina	k1gFnSc2	Smeraldina
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Beatrice	Beatrice	k1gFnSc1	Beatrice
odhalí	odhalit	k5eAaPmIp3nS	odhalit
svou	svůj	k3xOyFgFnSc4	svůj
totožnost	totožnost	k1gFnSc4	totožnost
<g/>
,	,	kIx,	,
shledá	shledat	k5eAaPmIp3nS	shledat
se	se	k3xPyFc4	se
s	s	k7c7	s
Florindem	Florind	k1gInSc7	Florind
<g/>
.	.	kIx.	.
</s>
<s>
Sluha	sluha	k1gMnSc1	sluha
se	se	k3xPyFc4	se
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloužil	sloužit	k5eAaImAgMnS	sloužit
dvěma	dva	k4xCgFnPc7	dva
pánům	pan	k1gMnPc3	pan
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
konečně	konečně	k6eAd1	konečně
vzít	vzít	k5eAaPmF	vzít
svoji	svůj	k3xOyFgFnSc4	svůj
milovanou	milovaný	k2eAgFnSc4d1	milovaná
Smeraldinu	Smeraldina	k1gFnSc4	Smeraldina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
současné	současný	k2eAgFnPc4d1	současná
inscenace	inscenace	k1gFnPc4	inscenace
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
patří	patřit	k5eAaImIp3nS	patřit
provedení	provedení	k1gNnSc1	provedení
činohry	činohra	k1gFnSc2	činohra
pražského	pražský	k2eAgNnSc2d1	Pražské
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Ivana	Ivan	k1gMnSc2	Ivan
Rajmonta	Rajmont	k1gMnSc2	Rajmont
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Donutilem	Donutil	k1gMnSc7	Donutil
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Inscenace	inscenace	k1gFnSc1	inscenace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
poprvé	poprvé	k6eAd1	poprvé
uvedena	uveden	k2eAgFnSc1d1	uvedena
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc4d1	oficiální
premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
<g/>
,	,	kIx,	,
šestisté	šestista	k1gMnPc1	šestista
představení	představení	k1gNnSc2	představení
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
27	[number]	k4	27
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obsazení	obsazení	k1gNnSc1	obsazení
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
inscenace	inscenace	k1gFnSc1	inscenace
===	===	k?	===
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
komedii	komedie	k1gFnSc4	komedie
mělo	mít	k5eAaImAgNnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
repertoáru	repertoár	k1gInSc6	repertoár
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
také	také	k6eAd1	také
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
exceloval	excelovat	k5eAaImAgMnS	excelovat
herec	herec	k1gMnSc1	herec
Martin	Martin	k1gMnSc1	Martin
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
měla	mít	k5eAaImAgFnS	mít
derniéru	derniéra	k1gFnSc4	derniéra
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obsazení	obsazení	k1gNnSc1	obsazení
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
díla	dílo	k1gNnSc2	dílo
na	na	k7c4	na
český-jazyk	českýazyk	k1gInSc4	český-jazyk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
na	na	k7c6	na
kdykde	kdykde	k6eAd1	kdykde
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánu	pán	k1gMnSc3	pán
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
