<s>
Sluha	sluha	k1gMnSc1	sluha
dvou	dva	k4xCgMnPc2	dva
pánů	pan	k1gMnPc2	pan
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
situační	situační	k2eAgFnSc1d1	situační
komedie	komedie	k1gFnSc1	komedie
Carla	Carl	k1gMnSc2	Carl
Goldoniho	Goldoni	k1gMnSc2	Goldoni
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgInSc1d1	spadající
do	do	k7c2	do
specifického	specifický	k2eAgInSc2d1	specifický
žánru	žánr	k1gInSc2	žánr
commedia	commedium	k1gNnSc2	commedium
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gFnPc2	arte
<g/>
.	.	kIx.	.
</s>
