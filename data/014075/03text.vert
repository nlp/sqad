<s>
Promethium	Promethium	k1gNnSc1
</s>
<s>
Promethium	Promethium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
5	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Pm	Pm	kA
</s>
<s>
61	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Promethium	Promethium	k1gNnSc1
<g/>
,	,	kIx,
Pm	Pm	k1gFnSc1
<g/>
,	,	kIx,
61	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Promethium	Promethium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-12-2	7440-12-2	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
145	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
1,85	1,85	k4
Å	Å	k?
(	(	kIx(
<g/>
185	#num#	k4
pm	pm	k?
<g/>
)	)	kIx)
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
5	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,16	1,16	k4
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
540	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1050	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2150	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
7,26	7,26	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1042	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
315,15	315,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
3000	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
273,15	273,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
7,13	7,13	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
289	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Neodym	neodym	k1gInSc1
≺	≺	k?
<g/>
Pm	Pm	k1gFnSc2
<g/>
≻	≻	k?
Samarium	samarium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Np	Np	k1gFnSc1
</s>
<s>
Promethium	Promethium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Pm	Pm	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Promethium	Promethium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
lanthanoidem	lanthanoid	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nemá	mít	k5eNaImIp3nS
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
a	a	k8xC
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Promethium	Promethium	k1gNnSc1
je	být	k5eAaImIp3nS
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc1d1
radioaktivní	radioaktivní	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
ultrastopových	ultrastopový	k2eAgNnPc6d1
množstvích	množství	k1gNnPc6
jako	jako	k8xC,k8xS
člen	člen	k1gInSc1
uranových	uranový	k2eAgFnPc2d1
rozpadových	rozpadový	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
nejsou	být	k5eNaImIp3nP
detailně	detailně	k6eAd1
známy	znám	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dostatečné	dostatečný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
čistého	čistý	k2eAgInSc2d1
kovu	kov	k1gInSc2
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
exaktní	exaktní	k2eAgNnSc4d1
měření	měření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
38	#num#	k4
izotopů	izotop	k1gInPc2
promethia	promethius	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
pouze	pouze	k6eAd1
tři	tři	k4xCgMnPc1
mají	mít	k5eAaImIp3nP
dostatečně	dostatečně	k6eAd1
dlouhý	dlouhý	k2eAgInSc4d1
poločas	poločas	k1gInSc4
přeměny	přeměna	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
prakticky	prakticky	k6eAd1
zkoumat	zkoumat	k5eAaImF
<g/>
:	:	kIx,
145	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
17,7	17,7	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
146	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
5,53	5,53	k4
let	léto	k1gNnPc2
a	a	k8xC
147	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
2,62	2,62	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poločas	poločas	k1gInSc1
delší	dlouhý	k2eAgInSc1d2
než	než	k8xS
10	#num#	k4
dnů	den	k1gInPc2
mají	mít	k5eAaImIp3nP
ještě	ještě	k9
144	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
(	(	kIx(
<g/>
363	#num#	k4
dnů	den	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
143	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
(	(	kIx(
<g/>
265	#num#	k4
dnů	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
uvedené	uvedený	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
jsou	být	k5eAaImIp3nP
beta	beta	k1gNnPc2
zářiči	zářič	k1gInPc7
<g/>
,	,	kIx,
147	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
vyzařuje	vyzařovat	k5eAaImIp3nS
záření	záření	k1gNnSc1
beta	beta	k1gNnPc2
minus	minus	k6eAd1
<g/>
,	,	kIx,
146	#num#	k4
<g/>
Pm	Pm	k1gFnSc2
beta	beta	k1gNnSc2
plus	plus	k1gInSc1
(	(	kIx(
<g/>
65,7	65,7	k4
%	%	kIx~
<g/>
)	)	kIx)
i	i	k9
beta	beta	k1gNnSc1
minus	minus	k1gNnSc1
(	(	kIx(
<g/>
34,3	34,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
uvedené	uvedený	k2eAgFnPc1d1
pouze	pouze	k6eAd1
záření	záření	k1gNnSc2
beta	beta	k1gNnSc1
plus	plus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Existence	existence	k1gFnSc1
promethia	promethia	k1gFnSc1
byla	být	k5eAaImAgFnS
teoreticky	teoreticky	k6eAd1
poprvé	poprvé	k6eAd1
předpovězena	předpovězen	k2eAgFnSc1d1
českým	český	k2eAgMnSc7d1
chemikem	chemik	k1gMnSc7
Bohuslavem	Bohuslav	k1gMnSc7
Braunerem	Brauner	k1gMnSc7
roku	rok	k1gInSc2
1902	#num#	k4
a	a	k8xC
opětně	opětně	k6eAd1
potvrzena	potvrzen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1914	#num#	k4
Henrym	Henry	k1gMnSc7
Mosleyem	Mosley	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Důkaz	důkaz	k1gInSc1
o	o	k7c4
skutečné	skutečný	k2eAgFnPc4d1
existenci	existence	k1gFnSc4
promethia	promethius	k1gMnSc2
podali	podat	k5eAaPmAgMnP
teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
Jacob	Jacoba	k1gFnPc2
A.	A.	kA
Marinsky	Marinsky	k1gFnPc1
<g/>
,	,	kIx,
Lawrence	Lawrence	k1gFnPc1
E.	E.	kA
Glendenin	Glendenin	k2eAgMnSc1d1
a	a	k8xC
Charles	Charles	k1gMnSc1
D.	D.	kA
Coryell	Coryell	k1gMnSc1
na	na	k7c6
základě	základ	k1gInSc6
analýzy	analýza	k1gFnSc2
produktů	produkt	k1gInPc2
jaderného	jaderný	k2eAgInSc2d1
rozpadu	rozpad	k1gInSc2
uranu	uran	k1gInSc2
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoje	svůj	k3xOyFgInPc4
poznatky	poznatek	k1gInPc4
však	však	k9
publikovali	publikovat	k5eAaBmAgMnP
až	až	k6eAd1
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
prvku	prvek	k1gInSc2
odvozeno	odvozen	k2eAgNnSc1d1
od	od	k7c2
hrdiny	hrdina	k1gMnSc2
řeckých	řecký	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
Prométhea	Prométheus	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ukradl	ukradnout	k5eAaPmAgMnS
olympským	olympský	k2eAgMnSc7d1
bohům	bůh	k1gMnPc3
tajemství	tajemství	k1gNnSc4
ohně	oheň	k1gInPc4
a	a	k8xC
daroval	darovat	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
a	a	k8xC
využití	využití	k1gNnSc1
</s>
<s>
Obsah	obsah	k1gInSc1
promethia	promethium	k1gNnSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
je	být	k5eAaImIp3nS
neměřitelně	měřitelně	k6eNd1
nízký	nízký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gInSc6
výskytu	výskyt	k1gInSc6
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
přinášejí	přinášet	k5eAaImIp3nP
důkaz	důkaz	k1gInSc1
spektra	spektrum	k1gNnSc2
některých	některý	k3yIgFnPc2
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgFnP
emisní	emisní	k2eAgFnPc1d1
linie	linie	k1gFnPc1
připisované	připisovaný	k2eAgFnPc1d1
atomům	atom	k1gInPc3
promethia	promethium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
umělé	umělý	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
promethia	promethium	k1gNnSc2
se	se	k3xPyFc4
vychází	vycházet	k5eAaImIp3nS
buď	buď	k8xC
z	z	k7c2
produktů	produkt	k1gInPc2
radioaktivního	radioaktivní	k2eAgNnSc2d1
štěpení	štěpení	k1gNnSc2
uranu	uran	k1gInSc2
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
nebo	nebo	k8xC
se	se	k3xPyFc4
připravují	připravovat	k5eAaImIp3nP
bombardováním	bombardování	k1gNnSc7
izotopu	izotop	k1gInSc2
146	#num#	k4
<g/>
Nd	Nd	k1gFnSc1
neutrony	neutron	k1gInPc1
za	za	k7c2
vzniku	vznik	k1gInSc2
147	#num#	k4
<g/>
Nd	Nd	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
beta	beta	k1gNnSc1
minus	minus	k6eAd1
přeměnou	přeměna	k1gFnSc7
mění	měnit	k5eAaImIp3nS
na	na	k7c4
147	#num#	k4
<g/>
Pm	Pm	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
promethia	promethium	k1gNnSc2
jako	jako	k8xS,k8xC
zářiče	zářič	k1gInSc2
beta	beta	k1gNnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
například	například	k6eAd1
ke	k	k7c3
kontinuálnímu	kontinuální	k2eAgNnSc3d1
měření	měření	k1gNnSc3
velmi	velmi	k6eAd1
malých	malý	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
při	při	k7c6
výrobě	výroba	k1gFnSc6
papíru	papír	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
jako	jako	k9
energetický	energetický	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
článcích	článek	k1gInPc6
<g/>
,	,	kIx,
užívaných	užívaný	k2eAgInPc2d1
obvykle	obvykle	k6eAd1
v	v	k7c6
kosmickém	kosmický	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
promethium	promethium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
promethium	promethium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4175917-5	4175917-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85107404	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85107404	#num#	k4
</s>
