<p>
<s>
Alecia	Alecia	k1gFnSc1	Alecia
Beth	Betha	k1gFnPc2	Betha
Moore	Moor	k1gInSc5	Moor
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Doylestown	Doylestown	k1gInSc1	Doylestown
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
světě	svět	k1gInSc6	svět
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Pink	pink	k2eAgFnSc1d1	pink
<g/>
,	,	kIx,	,
stylizovaně	stylizovaně	k6eAd1	stylizovaně
psáno	psát	k5eAaImNgNnS	psát
P!nk	P!nko	k1gNnPc2	P!nko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
popová	popový	k2eAgFnSc1d1	popová
a	a	k8xC	a
R&	R&	k1gFnSc1	R&
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
zatím	zatím	k6eAd1	zatím
čtyři	čtyři	k4xCgInPc1	čtyři
koncertní	koncertní	k2eAgInPc1d1	koncertní
DVD	DVD	kA	DVD
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
držitelka	držitelka	k1gFnSc1	držitelka
Grammy	Gramma	k1gFnSc2	Gramma
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
produkování	produkování	k1gNnSc1	produkování
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
herectví	herectví	k1gNnSc2	herectví
(	(	kIx(	(
<g/>
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
např.	např.	kA	např.
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Charlieho	Charlie	k1gMnSc2	Charlie
Andílci	andílek	k1gMnPc1	andílek
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c2	za
motokrosového	motokrosový	k2eAgMnSc2d1	motokrosový
závodníka	závodník	k1gMnSc2	závodník
Careyho	Carey	k1gMnSc2	Carey
Harta	Hart	k1gMnSc2	Hart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
debutovala	debutovat	k5eAaBmAgFnS	debutovat
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
počinem	počin	k1gInSc7	počin
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Take	Take	k1gInSc1	Take
Me	Me	k1gMnSc2	Me
Home	Hom	k1gMnSc2	Hom
<g/>
,	,	kIx,	,
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
You	You	k1gMnSc5	You
Go	Go	k1gMnSc5	Go
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
dostal	dostat	k5eAaPmAgInS	dostat
mezi	mezi	k7c4	mezi
40	[number]	k4	40
nejhranějších	hraný	k2eAgFnPc2d3	nejhranější
skladeb	skladba	k1gFnPc2	skladba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgInSc4d1	masivní
průlom	průlom	k1gInSc4	průlom
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
pop	pop	k1gInSc4	pop
rocková	rockový	k2eAgFnSc1d1	rocková
deska	deska	k1gFnSc1	deska
M	M	kA	M
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
ssundaztood	ssundaztood	k1gInSc1	ssundaztood
s	s	k7c7	s
hity	hit	k1gInPc7	hit
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
like	like	k1gInSc1	like
a	a	k8xC	a
Pill	Pill	k1gInSc1	Pill
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Get	Get	k1gFnSc1	Get
the	the	k?	the
Party	part	k1gInPc1	part
Started	Started	k1gInSc1	Started
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Get	Get	k1gFnSc4	Get
the	the	k?	the
Party	party	k1gFnSc1	party
Started	Started	k1gMnSc1	Started
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
Grammy	Gramma	k1gFnPc4	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
deska	deska	k1gFnSc1	deska
Try	Try	k1gFnSc1	Try
This	This	k1gInSc1	This
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
menší	malý	k2eAgInSc4d2	menší
propad	propad	k1gInSc4	propad
prodejnosti	prodejnost	k1gFnSc2	prodejnost
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Dead	Deada	k1gFnPc2	Deada
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
opět	opět	k6eAd1	opět
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
rádiích	rádio	k1gNnPc6	rádio
úspěšně	úspěšně	k6eAd1	úspěšně
rotovaly	rotovat	k5eAaImAgFnP	rotovat
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Stupid	Stupid	k1gInSc1	Stupid
Girls	girl	k1gFnPc2	girl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
Ur	Ur	k1gInSc1	Ur
Hand	Hand	k1gInSc1	Hand
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dear	Dear	k1gMnSc1	Dear
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
President	president	k1gMnSc1	president
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
Funhouse	Funhouse	k1gFnSc1	Funhouse
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Pink	pink	k6eAd1	pink
propagovala	propagovat	k5eAaImAgFnS	propagovat
celosvětovým	celosvětový	k2eAgNnSc7d1	celosvětové
turné	turné	k1gNnSc7	turné
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
takové	takový	k3xDgFnSc6	takový
hity	hit	k1gInPc4	hit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Funhouse	Funhouse	k1gFnSc1	Funhouse
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
So	So	kA	So
What	What	k1gInSc1	What
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sober	Sober	k1gInSc1	Sober
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Please	Pleasa	k1gFnSc6	Pleasa
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Leave	Leaev	k1gFnSc2	Leaev
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zrekapitulovat	zrekapitulovat	k5eAaPmF	zrekapitulovat
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
připravila	připravit	k5eAaPmAgFnS	připravit
kompilační	kompilační	k2eAgFnSc4d1	kompilační
desku	deska	k1gFnSc4	deska
The	The	k1gFnSc1	The
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
<g/>
...	...	k?	...
So	So	kA	So
Far	fara	k1gFnPc2	fara
<g/>
!	!	kIx.	!
</s>
<s>
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Raise	Raisa	k1gFnSc6	Raisa
Your	Youra	k1gFnPc2	Youra
Glass	Glassa	k1gFnPc2	Glassa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
známé	známý	k2eAgInPc1d1	známý
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
Blow	Blow	k1gFnPc1	Blow
Me	Me	k1gFnSc7	Me
(	(	kIx(	(
<g/>
One	One	k1gFnSc7	One
Last	Last	k2eAgInSc4d1	Last
Kiss	Kiss	k1gInSc4	Kiss
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Just	just	k6eAd1	just
Give	Give	k1gNnSc1	Give
Me	Me	k1gFnPc2	Me
a	a	k8xC	a
Reason	Reasona	k1gFnPc2	Reasona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
Beautiful	Beautifula	k1gFnPc2	Beautifula
Trauma	trauma	k1gNnSc4	trauma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnPc2	dětství
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
objevení	objevení	k1gNnSc1	objevení
==	==	k?	==
</s>
</p>
<p>
<s>
Pink	pink	k6eAd1	pink
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c4	v
Doylestown	Doylestown	k1gInSc4	Doylestown
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
Pensylvanie	Pensylvanie	k1gFnSc1	Pensylvanie
<g/>
)	)	kIx)	)
veteránovi	veterán	k1gMnSc3	veterán
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
Jimu	Jim	k1gMnSc3	Jim
Moorovi	Moor	k1gMnSc3	Moor
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
sestře	sestra	k1gFnSc3	sestra
Judy	judo	k1gNnPc7	judo
Kugelové	Kugelové	k2eAgInSc4d1	Kugelové
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
katolík	katolík	k1gMnSc1	katolík
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
též	též	k9	též
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c4	v
Doylestown	Doylestown	k1gInSc4	Doylestown
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Central	Central	k1gMnSc1	Central
Bucks	Bucksa	k1gFnPc2	Bucksa
West	West	k1gMnSc1	West
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
hrával	hrávat	k5eAaImAgMnS	hrávat
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
jí	on	k3xPp3gFnSc2	on
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
rockovou	rockový	k2eAgFnSc7d1	rocková
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	s	k7c7	s
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
hudební	hudební	k2eAgFnSc3d1	hudební
skupině	skupina	k1gFnSc3	skupina
Middleground	Middlegrounda	k1gFnPc2	Middlegrounda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nestala	stát	k5eNaPmAgFnS	stát
známou	známý	k2eAgFnSc7d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
měli	mít	k5eAaImAgMnP	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
Mary	Mary	k1gFnSc1	Mary
J.	J.	kA	J.
Blige	Blig	k1gFnPc1	Blig
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
Pac	pac	k1gFnPc1	pac
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
objevila	objevit	k5eAaPmAgFnS	objevit
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
již	již	k6eAd1	již
jako	jako	k9	jako
malá	malá	k1gFnSc1	malá
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
astmatem	astma	k1gNnSc7	astma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
trápilo	trápit	k5eAaImAgNnS	trápit
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teenagerském	teenagerský	k2eAgInSc6d1	teenagerský
věku	věk	k1gInSc6	věk
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
po	po	k7c6	po
filadelfských	filadelfský	k2eAgInPc6d1	filadelfský
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
B	B	kA	B
triu	trio	k1gNnSc6	trio
Choice	Choice	k1gInPc1	Choice
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
pod	pod	k7c4	pod
LaFace	LaFace	k1gFnPc4	LaFace
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
přispěly	přispět	k5eAaPmAgFnP	přispět
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
Key	Key	k1gFnSc4	Key
to	ten	k3xDgNnSc1	ten
my	my	k3xPp1nPc1	my
heart	hearta	k1gFnPc2	hearta
<g/>
"	"	kIx"	"
k	k	k7c3	k
soundtracku	soundtrack	k1gInSc3	soundtrack
filmu	film	k1gInSc2	film
Kazaam	Kazaam	k1gInSc4	Kazaam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Choice	Choice	k1gFnSc1	Choice
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
rozpadly	rozpadnout	k5eAaPmAgFnP	rozpadnout
po	po	k7c4	po
nahrávání	nahrávání	k1gNnSc4	nahrávání
nevydaného	vydaný	k2eNgNnSc2d1	nevydané
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k6eAd1	pink
setrvala	setrvat	k5eAaPmAgFnS	setrvat
u	u	k7c2	u
LaFace	LaFace	k1gFnSc2	LaFace
records	records	k1gInSc1	records
jako	jako	k8xC	jako
sólová	sólový	k2eAgFnSc1d1	sólová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
<g/>
.	.	kIx.	.
</s>
<s>
Daryl	Daryl	k1gInSc1	Daryl
Simmons	Simmonsa	k1gFnPc2	Simmonsa
ji	on	k3xPp3gFnSc4	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívala	zpívat	k5eAaImAgFnS	zpívat
vokály	vokál	k1gInPc4	vokál
pro	pro	k7c4	pro
Dianu	Diana	k1gFnSc4	Diana
Ross	Rossa	k1gFnPc2	Rossa
<g/>
,	,	kIx,	,
93	[number]	k4	93
Degrees	Degreesa	k1gFnPc2	Degreesa
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
návštěva	návštěva	k1gFnSc1	návštěva
Pink	pink	k6eAd1	pink
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc4d1	hudební
web	web	k1gInSc4	web
iReport	iReport	k1gInSc4	iReport
tehdy	tehdy	k6eAd1	tehdy
vystoupení	vystoupení	k1gNnPc4	vystoupení
ohodnotil	ohodnotit	k5eAaPmAgMnS	ohodnotit
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
a	a	k8xC	a
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
absolutní	absolutní	k2eAgFnSc4d1	absolutní
špičku	špička	k1gFnSc4	špička
co	co	k9	co
se	s	k7c7	s
živého	živý	k2eAgNnSc2d1	živé
vystupování	vystupování	k1gNnSc2	vystupování
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Ski	ski	k1gFnSc2	ski
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Max	max	kA	max
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rollerball	Rollerball	k1gMnSc1	Rollerball
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přišla	přijít	k5eAaPmAgFnS	přijít
role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Charlieho	Charlie	k1gMnSc2	Charlie
andílci	andílek	k1gMnPc1	andílek
<g/>
:	:	kIx,	:
Na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
pecky	pecek	k1gInPc4	pecek
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
hororu	horor	k1gInSc6	horor
Katakomby	katakomby	k1gFnPc1	katakomby
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
hrát	hrát	k5eAaImF	hrát
Janis	Janis	k1gFnPc1	Janis
Joplin	Joplina	k1gFnPc2	Joplina
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
nové	nový	k2eAgFnSc6d1	nová
biografii	biografie	k1gFnSc6	biografie
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Gospel	gospel	k1gInSc1	gospel
According	According	k1gInSc1	According
to	ten	k3xDgNnSc1	ten
Janis	Janis	k1gInSc4	Janis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účast	účast	k1gFnSc1	účast
odřekla	odřeknout	k5eAaPmAgFnS	odřeknout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
neslušný	slušný	k2eNgMnSc1d1	neslušný
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
osobnosti	osobnost	k1gFnSc3	osobnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
filmoví	filmový	k2eAgMnPc1d1	filmový
tvůrci	tvůrce	k1gMnPc1	tvůrce
nechtěli	chtít	k5eNaImAgMnP	chtít
zahrnout	zahrnout	k5eAaPmF	zahrnout
její	její	k3xOp3gFnSc1	její
smrt	smrt	k1gFnSc1	smrt
předávkováním	předávkování	k1gNnSc7	předávkování
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
Pictures	Pictures	k1gMnSc1	Pictures
Entertainment	Entertainment	k1gMnSc1	Entertainment
projevilo	projevit	k5eAaPmAgNnS	projevit
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
dílu	díl	k1gInSc6	díl
Charlieho	Charlie	k1gMnSc2	Charlie
andílků	andílek	k1gMnPc2	andílek
<g/>
.	.	kIx.	.
</s>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
moci	moct	k5eAaImF	moct
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hrát	hrát	k5eAaImF	hrát
jako	jako	k8xS	jako
zlá	zlý	k2eAgFnSc1d1	zlá
holka	holka	k1gFnSc1	holka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Manželství	manželství	k1gNnSc1	manželství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
motokrosového	motokrosový	k2eAgMnSc2d1	motokrosový
závodníka	závodník	k1gMnSc2	závodník
Careyho	Carey	k1gMnSc2	Carey
Harta	Hart	k1gMnSc2	Hart
<g/>
,	,	kIx,	,
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
držela	držet	k5eAaImAgFnS	držet
tabuli	tabule	k1gFnSc4	tabule
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vezmeš	vzít	k5eAaPmIp2nS	vzít
si	se	k3xPyFc3	se
mě	já	k3xPp1nSc2	já
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
to	ten	k3xDgNnSc1	ten
vážně	vážně	k6eAd1	vážně
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
si	se	k3xPyFc3	se
Carey	Carea	k1gFnPc4	Carea
tabuli	tabule	k1gFnSc3	tabule
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
,	,	kIx,	,
málem	málem	k6eAd1	málem
způsobil	způsobit	k5eAaPmAgMnS	způsobit
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
na	na	k7c6	na
Kostarice	Kostarika	k1gFnSc6	Kostarika
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
při	při	k7c6	při
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měsících	měsíc	k1gInPc6	měsíc
spekulací	spekulace	k1gFnPc2	spekulace
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
PEOPLE	PEOPLE	kA	PEOPLE
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
Hart	Hart	k1gInSc4	Hart
odloučili	odloučit	k5eAaPmAgMnP	odloučit
<g/>
.	.	kIx.	.
</s>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
vyplnila	vyplnit	k5eAaPmAgFnS	vyplnit
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
opět	opět	k6eAd1	opět
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
videoklipu	videoklip	k1gInSc2	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
So	So	kA	So
What	Whata	k1gFnPc2	Whata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hart	Hart	k1gInSc1	Hart
také	také	k9	také
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
2.6	[number]	k4	2.6
<g/>
.2011	.2011	k4	.2011
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
Willow	Willow	k1gMnSc4	Willow
Sage	Sage	k1gNnSc7	Sage
Hart	Hart	k1gMnSc1	Hart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
PETA	PETA	kA	PETA
===	===	k?	===
</s>
</p>
<p>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc7d1	přední
bojovnicí	bojovnice	k1gFnSc7	bojovnice
organizace	organizace	k1gFnSc2	organizace
PETA	PETA	kA	PETA
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
kauzám	kauza	k1gFnPc3	kauza
jako	jako	k8xC	jako
například	například	k6eAd1	například
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
KFC	KFC	kA	KFC
<g/>
.	.	kIx.	.
</s>
<s>
Poslala	poslat	k5eAaPmAgFnS	poslat
dopis	dopis	k1gInSc4	dopis
Princi	princ	k1gMnSc3	princ
Williamovi	William	k1gMnSc3	William
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ho	on	k3xPp3gNnSc4	on
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
kvůli	kvůli	k7c3	kvůli
honu	hon	k1gInSc3	hon
na	na	k7c4	na
lišku	liška	k1gFnSc4	liška
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
poslala	poslat	k5eAaPmAgFnS	poslat
také	také	k6eAd1	také
Královně	královna	k1gFnSc6	královna
Alžbětě	Alžběta	k1gFnSc6	Alžběta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
použití	použití	k1gNnSc3	použití
pravé	pravý	k2eAgFnSc2d1	pravá
medvědí	medvědí	k2eAgFnSc2d1	medvědí
kožešiny	kožešina	k1gFnSc2	kožešina
na	na	k7c6	na
čepicích	čepice	k1gFnPc6	čepice
londýnské	londýnský	k2eAgFnSc2d1	londýnská
hradní	hradní	k2eAgFnSc2d1	hradní
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zmínila	zmínit	k5eAaPmAgFnS	zmínit
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
News	Newsa	k1gFnPc2	Newsa
of	of	k?	of
the	the	k?	the
world	world	k1gInSc1	world
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
znechucena	znechutit	k5eAaPmNgFnS	znechutit
kolegyní	kolegyně	k1gFnSc7	kolegyně
Beyoncé	Beyoncá	k1gFnSc2	Beyoncá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nosila	nosit	k5eAaImAgFnS	nosit
pravou	pravý	k2eAgFnSc4d1	pravá
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
PETA	PETA	kA	PETA
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
Australský	australský	k2eAgInSc4d1	australský
vlnařský	vlnařský	k2eAgInSc4d1	vlnařský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
charitu	charita	k1gFnSc4	charita
===	===	k?	===
</s>
</p>
<p>
<s>
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
mnoho	mnoho	k6eAd1	mnoho
charitativních	charitativní	k2eAgFnPc2d1	charitativní
organizací	organizace	k1gFnPc2	organizace
jako	jako	k9	jako
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
Save	Save	k1gFnPc2	Save
the	the	k?	the
children	childrna	k1gFnPc2	childrna
<g/>
,	,	kIx,	,
ONE	ONE	kA	ONE
Campaign	Campaigno	k1gNnPc2	Campaigno
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Turné	turné	k1gNnSc2	turné
==	==	k?	==
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Try	Try	k1gFnPc2	Try
This	This	k1gInSc1	This
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Dead	Dead	k1gMnSc1	Dead
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Funhouse	Funhouse	k1gFnSc1	Funhouse
Tour	Toura	k1gFnPc2	Toura
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Funhouse	Funhouse	k1gFnSc2	Funhouse
Summer	Summer	k1gMnSc1	Summer
Carnival	Carnival	k1gMnSc1	Carnival
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Love	lov	k1gInSc5	lov
Tour	Tour	k1gInSc4	Tour
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
Beautiful	Beautiful	k1gInSc1	Beautiful
Trauma	trauma	k1gNnSc1	trauma
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Can	Can	k1gFnPc2	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Take	Take	k1gInSc1	Take
Me	Me	k1gMnSc2	Me
Home	Hom	k1gMnSc2	Hom
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
M	M	kA	M
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
ssundaztood	ssundaztood	k1gInSc1	ssundaztood
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Try	Try	k1gMnSc4	Try
This	This	k1gInSc1	This
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Dead	Deada	k1gFnPc2	Deada
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Funhouse	Funhouse	k1gFnSc2	Funhouse
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
The	The	k1gMnSc1	The
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Love	lov	k1gInSc5	lov
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Beautiful	Beautiful	k1gInSc1	Beautiful
Trauma	trauma	k1gNnSc1	trauma
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
–	–	k?	–
Hurts	Hurtsa	k1gFnPc2	Hurtsa
2B	[number]	k4	2B
Human	Humana	k1gFnPc2	Humana
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
The	The	k1gMnSc1	The
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
<g/>
...	...	k?	...
So	So	kA	So
Far	fara	k1gFnPc2	fara
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Pink	pink	k2eAgInPc1d1	pink
<g/>
:	:	kIx,	:
Live	Live	k1gInSc1	Live
in	in	k?	in
Europe	Europ	k1gMnSc5	Europ
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Pink	pink	k2eAgInSc2d1	pink
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
from	from	k1gInSc1	from
Wembley	Wemblea	k1gMnSc2	Wemblea
Arena	Aren	k1gMnSc2	Aren
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
<g/>
:	:	kIx,	:
Live	Liv	k1gMnSc2	Liv
in	in	k?	in
Australia	Australius	k1gMnSc2	Australius
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
P	P	kA	P
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
nk	nk	k?	nk
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Live	Live	k1gNnSc1	Live
From	Froma	k1gFnPc2	Froma
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pink	pink	k6eAd1	pink
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pinkspage	Pinkspage	k1gFnSc1	Pinkspage
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
pink	pink	k6eAd1	pink
<g/>
.	.	kIx.	.
<g/>
er	er	k?	er
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
fanstránky	fanstránka	k1gFnSc2	fanstránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
pink	pink	k2eAgFnSc2d1	pink
<g/>
.	.	kIx.	.
<g/>
nazory	nazora	k1gFnSc2	nazora
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
fanstránky	fanstránka	k1gFnSc2	fanstránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
IMDb	IMDb	k1gInSc1	IMDb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Pink	pink	k6eAd1	pink
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
MySpace	MySpace	k1gFnSc1	MySpace
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Pinkspage	Pinkspag	k1gMnSc2	Pinkspag
</s>
</p>
