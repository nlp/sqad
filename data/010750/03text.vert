<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
metalový	metalový	k2eAgMnSc1d1	metalový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
kdysi	kdysi	k6eAd1	kdysi
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
legendární	legendární	k2eAgFnSc2d1	legendární
české	český	k2eAgFnSc2d1	Česká
thrash	thrash	k1gMnSc1	thrash
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Arakain	Arakaina	k1gFnPc2	Arakaina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kapelou	kapela	k1gFnSc7	kapela
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
Project	Project	k1gMnSc1	Project
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Urbanem	Urban	k1gMnSc7	Urban
a	a	k8xC	a
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Nedvědem	Nedvěd	k1gMnSc7	Nedvěd
založili	založit	k5eAaPmAgMnP	založit
tehdy	tehdy	k6eAd1	tehdy
heavy	heava	k1gFnPc4	heava
metalovou	metalový	k2eAgFnSc4d1	metalová
kapelu	kapela	k1gFnSc4	kapela
Arakain	Arakaina	k1gFnPc2	Arakaina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
opustil	opustit	k5eAaPmAgMnS	opustit
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
koncertu	koncert	k1gInSc2	koncert
ke	k	k7c3	k
dvacátému	dvacátý	k4xOgNnSc3	dvacátý
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
Petr	Petr	k1gMnSc1	Petr
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Jan	Jan	k1gMnSc1	Jan
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
veřejně	veřejně	k6eAd1	veřejně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
osobním	osobní	k2eAgFnPc3d1	osobní
neshodám	neshoda	k1gFnPc3	neshoda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Arakainu	Arakain	k1gInSc2	Arakain
zpíval	zpívat	k5eAaImAgMnS	zpívat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
s	s	k7c7	s
Arakainem	Arakain	k1gInSc7	Arakain
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pětadvacátému	pětadvacátý	k4xOgInSc3	pětadvacátý
výročí	výročí	k1gNnSc3	výročí
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
XXV	XXV	kA	XXV
Eden	Eden	k1gInSc1	Eden
sice	sice	k8xC	sice
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
neobjasněných	objasněný	k2eNgInPc2d1	neobjasněný
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
další	další	k2eAgFnPc4d1	další
spolupráce	spolupráce	k1gFnPc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skupinu	skupina	k1gFnSc4	skupina
ABBAND	ABBAND	kA	ABBAND
(	(	kIx(	(
<g/>
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
sdružil	sdružit	k5eAaPmAgMnS	sdružit
jeho	jeho	k3xOp3gMnPc4	jeho
dřívější	dřívější	k2eAgMnPc4d1	dřívější
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
jako	jako	k8xC	jako
Mirka	Mirek	k1gMnSc4	Mirek
Macha	Mach	k1gMnSc4	Mach
nebo	nebo	k8xC	nebo
Karla	Karel	k1gMnSc4	Karel
Adama	Adam	k1gMnSc4	Adam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
posluchači	posluchač	k1gMnSc3	posluchač
českého	český	k2eAgNnSc2d1	české
Radia	radio	k1gNnSc2	radio
Beat	beat	k1gInSc1	beat
zvolen	zvolen	k2eAgMnSc1d1	zvolen
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Osobnost	osobnost	k1gFnSc1	osobnost
<g/>
"	"	kIx"	"
do	do	k7c2	do
Beatové	beatový	k2eAgFnSc2d1	beatová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
padesátinám	padesátina	k1gFnPc3	padesátina
Aleše	Aleš	k1gMnSc2	Aleš
Brichty	Bricht	k1gMnPc7	Bricht
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nahrávka	nahrávka	k1gFnSc1	nahrávka
AB	AB	kA	AB
50	[number]	k4	50
-	-	kIx~	-
A	a	k8xC	a
Tribute	tribut	k1gInSc5	tribut
to	ten	k3xDgNnSc4	ten
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
ABBANDu	ABBANDus	k1gInSc6	ABBANDus
k	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
personální	personální	k2eAgFnSc3d1	personální
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
rovnala	rovnat	k5eAaImAgFnS	rovnat
rozpadu	rozpad	k1gInSc3	rozpad
nebýt	být	k5eNaImF	být
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
sestava	sestava	k1gFnSc1	sestava
nová	nový	k2eAgFnSc1d1	nová
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
názvem	název	k1gInSc7	název
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
Arakainem	Arakain	k1gInSc7	Arakain
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
výroční	výroční	k2eAgNnSc4d1	výroční
turné	turné	k1gNnSc4	turné
Arakain	Arakain	k2eAgInSc1d1	Arakain
memoriál	memoriál	k1gInSc1	memoriál
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
ve	v	k7c6	v
Slovanském	slovanský	k2eAgInSc6d1	slovanský
domě	dům	k1gInSc6	dům
koncert	koncert	k1gInSc1	koncert
30	[number]	k4	30
let	léto	k1gNnPc2	léto
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Údolí	údolí	k1gNnSc2	údolí
sviní	svinit	k5eAaImIp3nS	svinit
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
už	už	k9	už
pod	pod	k7c4	pod
seskupení	seskupení	k1gNnSc4	seskupení
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
Project	Project	k1gMnSc1	Project
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
hudebně	hudebně	k6eAd1	hudebně
zní	znět	k5eAaImIp3nS	znět
jako	jako	k8xC	jako
předešlé	předešlý	k2eAgNnSc1d1	předešlé
album	album	k1gNnSc1	album
Deratizer	Deratizra	k1gFnPc2	Deratizra
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
textově	textově	k6eAd1	textově
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Paláci	palác	k1gInSc6	palác
Akropolis	Akropolis	k1gFnSc1	Akropolis
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
křest	křest	k1gInSc1	křest
alba	album	k1gNnSc2	album
Údolí	údolí	k1gNnSc2	údolí
sviní	svinit	k5eAaImIp3nS	svinit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
šlo	jít	k5eAaImAgNnS	jít
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
prvních	první	k4xOgNnPc2	první
500	[number]	k4	500
očíslovaných	očíslovaný	k2eAgInPc2d1	očíslovaný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Aleše	Aleš	k1gMnSc2	Aleš
ml.	ml.	kA	ml.
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Michaelu	Michaela	k1gFnSc4	Michaela
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
o	o	k7c4	o
23	[number]	k4	23
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc4d2	mladší
polskou	polský	k2eAgFnSc4d1	polská
partnerku	partnerka	k1gFnSc4	partnerka
Joannu	Joann	k1gInSc2	Joann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Solová	Solový	k2eAgFnSc1d1	Solový
dráha	dráha	k1gFnSc1	dráha
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Růže	růž	k1gInSc2	růž
pro	pro	k7c4	pro
Algernon	Algernon	k1gNnSc4	Algernon
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Ráno	ráno	k6eAd1	ráno
ve	v	k7c6	v
dveřích	dveře	k1gFnPc6	dveře
Armády	armáda	k1gFnSc2	armáda
spásy	spása	k1gFnSc2	spása
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Hledač	hledač	k1gInSc1	hledač
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
American	American	k1gInSc1	American
Bull	bulla	k1gFnPc2	bulla
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Anděl	Anděl	k1gMnSc1	Anděl
posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Legendy	legenda	k1gFnSc2	legenda
2	[number]	k4	2
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Kompilace	kompilace	k1gFnSc2	kompilace
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
perlami	perla	k1gFnPc7	perla
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
-	-	kIx~	-
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
American	American	k1gInSc1	American
Bull	bulla	k1gFnPc2	bulla
-	-	kIx~	-
New	New	k1gFnSc1	New
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
<g/>
:	:	kIx,	:
Beatová	beatový	k2eAgFnSc1d1	beatová
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
DVD	DVD	kA	DVD
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
12	[number]	k4	12
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Arakain	Arakain	k2eAgMnSc1d1	Arakain
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Demonahrávky	Demonahrávka	k1gFnSc2	Demonahrávka
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Nesmíš	smět	k5eNaImIp2nS	smět
to	ten	k3xDgNnSc1	ten
vzdát	vzdát	k5eAaPmF	vzdát
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Singly	singl	k1gInPc4	singl
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Excalibur	Excalibura	k1gFnPc2	Excalibura
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Ku-Klux-Klan	Kuluxlan	k1gInSc1	Ku-klux-klan
<g/>
/	/	kIx~	/
<g/>
Orion	orion	k1gInSc1	orion
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Zase	zase	k9	zase
spíš	spíš	k9	spíš
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
sama	sám	k3xTgFnSc1	sám
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Páteční	páteční	k2eAgInSc1d1	páteční
flám	flám	k1gInSc1	flám
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Ztráty	ztráta	k1gFnPc1	ztráta
a	a	k8xC	a
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Thrash	Thrash	k1gMnSc1	Thrash
The	The	k1gMnSc1	The
Trash	Trash	k1gMnSc1	Trash
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Black	Black	k1gMnSc1	Black
Jack	Jack	k1gMnSc1	Jack
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Salto	salto	k1gNnSc1	salto
Mortale	Mortal	k1gMnSc5	Mortal
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Thrash	Thrasha	k1gFnPc2	Thrasha
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Legendy	legenda	k1gFnSc2	legenda
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
S.	S.	kA	S.
<g/>
O.S.	O.S.	k1gFnSc6	O.S.
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
Apage	Apag	k1gInSc2	Apag
Satanas	Satanas	k1gInSc1	Satanas
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
Farao	farao	k1gNnSc4	farao
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
Archeology	archeolog	k1gMnPc7	archeolog
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Kompilace	kompilace	k1gFnSc2	kompilace
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
15	[number]	k4	15
Vol	vol	k6eAd1	vol
<g/>
.1	.1	k4	.1
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
15	[number]	k4	15
Vol	vol	k6eAd1	vol
<g/>
.2	.2	k4	.2
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
15	[number]	k4	15
Vol	vol	k6eAd1	vol
<g/>
.1	.1	k4	.1
&	&	k?	&
2	[number]	k4	2
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Balady	balada	k1gFnSc2	balada
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
History	Histor	k1gInPc1	Histor
Live	Liv	k1gFnSc2	Liv
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Gambrinus	gambrinus	k1gInSc1	gambrinus
Live	Liv	k1gInSc2	Liv
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
XXV	XXV	kA	XXV
Eden	Eden	k1gInSc1	Eden
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
DVD	DVD	kA	DVD
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
15	[number]	k4	15
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Gambrinus	gambrinus	k1gInSc1	gambrinus
Live	Liv	k1gInSc2	Liv
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
20	[number]	k4	20
let	léto	k1gNnPc2	léto
na	na	k7c4	na
tvrdo	tvrdo	k1gNnSc4	tvrdo
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Grizzly	grizzly	k1gMnPc5	grizzly
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
!	!	kIx.	!
</s>
<s>
<g/>
New	New	k1gFnSc1	New
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
Band	banda	k1gFnPc2	banda
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Nech	nechat	k5eAaPmRp2nS	nechat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
projít	projít	k5eAaPmF	projít
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Deratizer	Deratizra	k1gFnPc2	Deratizra
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Grizzly	grizzly	k1gMnSc1	grizzly
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
Podle	podle	k7c2	podle
nepodepsaného	podepsaný	k2eNgInSc2d1	nepodepsaný
článku	článek	k1gInSc2	článek
v	v	k7c6	v
Lidovkách	Lidovky	k1gFnPc6	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
text	text	k1gInSc4	text
úvodní	úvodní	k2eAgFnSc2d1	úvodní
písně	píseň	k1gFnSc2	píseň
alba	album	k1gNnSc2	album
Deratizer	Deratizer	k1gMnSc1	Deratizer
recenzent	recenzent	k1gMnSc1	recenzent
serveru	server	k1gInSc2	server
www.musicserver.cz	www.musicserver.cz	k1gMnSc1	www.musicserver.cz
Petr	Petr	k1gMnSc1	Petr
Bláha	Bláha	k1gMnSc1	Bláha
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
volání	volání	k1gNnSc4	volání
po	po	k7c6	po
deratizaci	deratizace	k1gFnSc6	deratizace
určitého	určitý	k2eAgNnSc2d1	určité
etnika	etnikum	k1gNnSc2	etnikum
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Brichta	Brichta	k1gMnSc1	Brichta
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nesmysl	nesmysl	k1gInSc4	nesmysl
<g/>
.	.	kIx.	.
</s>
<s>
Bláha	Bláha	k1gMnSc1	Bláha
rovněž	rovněž	k9	rovněž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
obrázek	obrázek	k1gInSc4	obrázek
na	na	k7c6	na
předsádce	předsádka	k1gFnSc6	předsádka
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vykreslena	vykreslen	k2eAgFnSc1d1	vykreslena
postava	postava	k1gFnSc1	postava
očividně	očividně	k6eAd1	očividně
romského	romský	k2eAgInSc2d1	romský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
nese	nést	k5eAaImIp3nS	nést
autorádio	autorádio	k1gNnSc1	autorádio
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dámskou	dámský	k2eAgFnSc7d1	dámská
kabelku	kabelka	k1gFnSc4	kabelka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
Brichty	Bricht	k1gInPc7	Bricht
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hudebního	hudební	k2eAgMnSc2d1	hudební
publicisty	publicista	k1gMnSc2	publicista
Petra	Petr	k1gMnSc2	Petr
Korála	korál	k1gMnSc2	korál
verše	verš	k1gInSc2	verš
Deratizera	Deratizer	k1gMnSc2	Deratizer
nejsou	být	k5eNaImIp3nP	být
myšleny	myšlen	k2eAgFnPc1d1	myšlena
jako	jako	k8xS	jako
primitivní	primitivní	k2eAgFnSc1d1	primitivní
propagace	propagace	k1gFnSc1	propagace
paušálního	paušální	k2eAgNnSc2d1	paušální
silového	silový	k2eAgNnSc2d1	silové
řešení	řešení	k1gNnSc2	řešení
problému	problém	k1gInSc2	problém
s	s	k7c7	s
́	́	k?	́
<g/>
nepřizpůsobivými	přizpůsobivý	k2eNgMnPc7d1	nepřizpůsobivý
občany	občan	k1gMnPc7	občan
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zneužitelné	zneužitelný	k2eAgFnPc1d1	zneužitelná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hudebního	hudební	k2eAgMnSc2d1	hudební
publicisty	publicista	k1gMnSc2	publicista
Františka	František	k1gMnSc2	František
Kovače	Kovač	k1gMnSc2	Kovač
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc1d1	úvodní
skladba	skladba	k1gFnSc1	skladba
hudebně	hudebně	k6eAd1	hudebně
velmi	velmi	k6eAd1	velmi
zahuštěná	zahuštěný	k2eAgFnSc1d1	zahuštěná
<g/>
,	,	kIx,	,
s	s	k7c7	s
nekompromisním	kompromisní	k2eNgInSc7d1	nekompromisní
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
určitá	určitý	k2eAgFnSc1d1	určitá
záměrná	záměrný	k2eAgFnSc1d1	záměrná
naivita	naivita	k1gFnSc1	naivita
přesně	přesně	k6eAd1	přesně
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
aktuální	aktuální	k2eAgFnSc7d1	aktuální
politicko-sociální	politickoociální	k2eAgFnSc7d1	politicko-sociální
bezmocí	bezmoc	k1gFnSc7	bezmoc
většiny	většina	k1gFnSc2	většina
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
skladba	skladba	k1gFnSc1	skladba
Píseň	píseň	k1gFnSc1	píseň
pro	pro	k7c4	pro
bílou	bílý	k2eAgFnSc4d1	bílá
hůl	hůl	k1gFnSc4	hůl
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgFnSc6d1	pojednávající
o	o	k7c6	o
slepém	slepý	k2eAgMnSc6d1	slepý
romském	romský	k2eAgMnSc6d1	romský
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
,	,	kIx,	,
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brichtovi	Brichtův	k2eAgMnPc1d1	Brichtův
nechybí	chybět	k5eNaImIp3nP	chybět
ani	ani	k8xC	ani
sociální	sociální	k2eAgNnSc4d1	sociální
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
50	[number]	k4	50
TESLA	Tesla	k1gMnSc1	Tesla
ARENA-TRUE	ARENA-TRUE	k1gMnSc1	ARENA-TRUE
LIVE	LIVE	kA	LIVE
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
Project	Project	k1gMnSc1	Project
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Údolí	údolí	k1gNnSc1	údolí
sviní	svině	k1gFnPc2	svině
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Anebo	anebo	k8xC	anebo
taky	taky	k9	taky
datel	datel	k1gMnSc1	datel
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Bricht	k1gInSc2	Bricht
Trio	trio	k1gNnSc1	trio
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Papírovej	Papírovej	k?	Papírovej
drak	drak	k1gInSc1	drak
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
DVD	DVD	kA	DVD
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hattrick	hattrick	k1gInSc4	hattrick
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Hattrick	hattrick	k1gInSc1	hattrick
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Hattrick	hattrick	k1gInSc1	hattrick
I.	I.	kA	I.
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
víc	hodně	k6eAd2	hodně
<g/>
...	...	k?	...
<g/>
Olympic	Olympic	k1gMnSc1	Olympic
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Gott	Gotta	k1gFnPc2	Gotta
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
70	[number]	k4	70
[	[	kIx(	[
<g/>
TV	TV	kA	TV
koncert	koncert	k1gInSc1	koncert
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Muzikály	muzikál	k1gInPc4	muzikál
===	===	k?	===
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Jesus	Jesus	k1gInSc1	Jesus
Christ	Christ	k1gInSc4	Christ
Superstar	superstar	k1gFnSc2	superstar
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
je	být	k5eAaImIp3nS	být
příbuzný	příbuzný	k1gMnSc1	příbuzný
ruského	ruský	k2eAgMnSc2d1	ruský
válečného	válečný	k2eAgMnSc2d1	válečný
malíře	malíř	k1gMnSc2	malíř
Vasilije	Vasilije	k1gMnSc4	Vasilije
Vasiljeviče	Vasiljevič	k1gMnSc4	Vasiljevič
Vereščagina	Vereščagin	k1gMnSc4	Vereščagin
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
obraz	obraz	k1gInSc4	obraz
Apoteóza	apoteóza	k1gFnSc1	apoteóza
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
obale	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
Farao	farao	k1gNnSc4	farao
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Akce	akce	k1gFnSc2	akce
proti	proti	k7c3	proti
imigraci	imigrace	k1gFnSc3	imigrace
<g/>
,	,	kIx,	,
kvótám	kvóta	k1gFnPc3	kvóta
a	a	k8xC	a
za	za	k7c4	za
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Bricht	k1gInSc2	Bricht
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
a	a	k8xC	a
AB	AB	kA	AB
Band	band	k1gInSc1	band
</s>
</p>
