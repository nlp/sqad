<s>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
fotbalistu	fotbalista	k1gMnSc4
dle	dle	k7c2
mezinárodního	mezinárodní	k2eAgInSc2d1
panelu	panel	k1gInSc2
sportovních	sportovní	k2eAgMnPc2d1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
získal	získat	k5eAaPmAgMnS
liberijský	liberijský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
George	George	k1gFnSc1
Weah	Weah	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
přestoupil	přestoupit	k5eAaPmAgMnS
z	z	k7c2
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gInSc1
do	do	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>