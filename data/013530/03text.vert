<s>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
1995	#num#	k4
</s>
<s>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
1995	#num#	k4
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
SportFotbal	SportFotbal	k1gInSc1
</s>
<s>
OrganizaceFrance	OrganizaceFrance	k1gFnSc1
Football	Footballa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
<g/>
1995	#num#	k4
</s>
<s>
Ročník	ročník	k1gInSc1
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
George	Georg	k1gMnSc2
Weah	Weah	k1gMnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
</s>
<s>
Matthias	Matthias	k1gMnSc1
Sammer	Sammer	k1gMnSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
fotbalistu	fotbalista	k1gMnSc4
dle	dle	k7c2
mezinárodního	mezinárodní	k2eAgInSc2d1
panelu	panel	k1gInSc2
sportovních	sportovní	k2eAgMnPc2d1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
získal	získat	k5eAaPmAgMnS
liberijský	liberijský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
George	George	k1gFnSc1
Weah	Weah	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
přestoupil	přestoupit	k5eAaPmAgMnS
z	z	k7c2
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gInSc1
do	do	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
40	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
ankety	anketa	k1gFnSc2
<g/>
,	,	kIx,
organizoval	organizovat	k5eAaBmAgMnS
ho	on	k3xPp3gInSc4
časopis	časopis	k1gInSc4
France	Franc	k1gMnSc2
Football	Footballa	k1gFnPc2
a	a	k8xC
výsledky	výsledek	k1gInPc4
určili	určit	k5eAaPmAgMnP
sportovní	sportovní	k2eAgMnPc1d1
publicisté	publicista	k1gMnPc1
ze	z	k7c2
49	#num#	k4
zemí	zem	k1gFnPc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
hlasovat	hlasovat	k5eAaImF
i	i	k9
pro	pro	k7c4
hráče	hráč	k1gMnPc4
z	z	k7c2
neevropských	evropský	k2eNgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
hráli	hrát	k5eAaImAgMnP
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
klubech	klub	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
už	už	k6eAd1
prakticky	prakticky	k6eAd1
všichni	všechen	k3xTgMnPc1
nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
přeměnila	přeměnit	k5eAaPmAgFnS
se	se	k3xPyFc4
fakticky	fakticky	k6eAd1
anketa	anketa	k1gFnSc1
z	z	k7c2
nejlepšího	dobrý	k2eAgMnSc2d3
hráče	hráč	k1gMnSc2
Evropy	Evropa	k1gFnSc2
na	na	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
od	od	k7c2
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
časopis	časopis	k1gInSc1
France	Franc	k1gMnSc2
Football	Football	k1gInSc1
nejdříve	dříve	k6eAd3
nominuje	nominovat	k5eAaBmIp3nS
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yRgMnPc2,k3yIgMnPc2,k3yQgMnPc2
mohou	moct	k5eAaImIp3nP
hlasující	hlasující	k1gMnPc1
vybírat	vybírat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Weah	Weah	k1gMnSc1
v	v	k7c6
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
<g/>
George	Georg	k1gInSc2
Weah	Weah	k1gMnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
144	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
Jürgen	Jürgen	k1gInSc1
Klinsmann	Klinsmann	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
108	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
Jari	jar	k1gFnSc3
Litmanen	Litmanen	k2eAgInSc4d1
Ajax	Ajax	k1gInSc4
Amsterdam	Amsterdam	k1gInSc1
<g/>
67	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
Alessandro	Alessandra	k1gFnSc5
Del	Del	k1gFnSc4
Piero	Piero	k1gNnSc4
Juventus	Juventus	k1gInSc1
<g/>
57	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
Patrick	Patrick	k1gMnSc1
Kluivert	Kluivert	k1gMnSc1
Ajax	Ajax	k1gInSc4
<g/>
47	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
Gianfranco	Gianfranco	k6eAd1
Zola	Zol	k2eAgFnSc1d1
Parma	Parma	k1gFnSc1
<g/>
41	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
Paolo	Paolo	k1gNnSc1
Maldini	Maldin	k2eAgMnPc1d1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
36	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
Marc	Marc	k1gFnSc1
Overmars	Overmars	k1gInSc4
Ajax	Ajax	k1gInSc1
<g/>
33	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
Matthias	Matthias	k1gMnSc1
Sammer	Sammer	k1gMnSc1
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
18	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
Michael	Michael	k1gMnSc1
Laudrup	Laudrup	k1gMnSc1
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
17	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
Marcel	Marcela	k1gFnPc2
Desailly	Desailla	k1gFnSc2
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
16	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
Ajax	Ajax	k1gInSc4
<g/>
15	#num#	k4
</s>
<s>
Fabrizio	Fabrizio	k6eAd1
Ravanelli	Ravanelle	k1gFnSc4
Juventus	Juventus	k1gInSc1
<g/>
15	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
Paulo	Paula	k1gFnSc5
Sousa	Sous	k1gMnSc4
Juventus	Juventus	k1gInSc4
<g/>
14	#num#	k4
</s>
<s>
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
Barcelona	Barcelona	k1gFnSc1
Parma	Parma	k1gFnSc1
<g/>
14	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
Dejan	Dejan	k1gInSc1
Savićević	Savićević	k1gFnSc2
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
12	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
Davor	Davor	k1gMnSc1
Šuker	Šuker	k1gMnSc1
Sevilla	Sevilla	k1gFnSc1
FC10	FC10	k1gFnSc2
</s>
<s>
18	#num#	k4
<g/>
Fernando	Fernanda	k1gFnSc5
Hierro	Hierra	k1gFnSc5
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
9	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
Gianluca	Gianluc	k1gInSc2
Vialli	Vialle	k1gFnSc4
Juventus	Juventus	k1gInSc1
<g/>
8	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
Gabriel	Gabriel	k1gMnSc1
Batistuta	Batistut	k1gMnSc4
Fiorentina	Fiorentin	k1gMnSc4
<g/>
7	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
Franco	Franco	k1gMnSc1
Baresi	Barese	k1gFnSc4
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
6	#num#	k4
</s>
<s>
Finidi	Finid	k1gMnPc1
George	Georg	k1gFnSc2
Ajax	Ajax	k1gInSc1
<g/>
6	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gMnSc1
Juventus	Juventus	k1gInSc4
<g/>
5	#num#	k4
</s>
<s>
Anthony	Anthona	k1gFnPc1
Yeboah	Yeboaha	k1gFnPc2
Eintracht	Eintracht	k2eAgInSc1d1
Frankfurt	Frankfurt	k1gInSc1
Leeds	Leeds	k1gInSc1
United	United	k1gInSc1
<g/>
5	#num#	k4
</s>
<s>
Zvonimir	Zvonimir	k1gInSc1
Boban	Boban	k1gInSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
5	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
Ronaldo	Ronaldo	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhovna	k1gFnPc2
<g/>
4	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
Juan	Juan	k1gMnSc1
Esnáider	Esnáider	k1gMnSc1
Real	Real	k1gInSc4
Zaragoza	Zaragoza	k1gFnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
3	#num#	k4
</s>
<s>
Iván	Iván	k1gMnSc1
Zamorano	Zamorana	k1gFnSc5
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
3	#num#	k4
</s>
<s>
Andreas	Andreas	k1gMnSc1
Möller	Möller	k1gMnSc1
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
3	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
Vítor	Vítor	k1gMnSc1
Baía	Baía	k1gMnSc1
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
</s>
<s>
Bebeto	Bebeta	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
La	la	k1gNnPc2
Coruñ	Coruñ	k1gFnSc1
<g/>
2	#num#	k4
</s>
<s>
32	#num#	k4
<g/>
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
Blackburn	Blackburn	k1gMnSc1
Rovers	Rovers	k1gInSc4
<g/>
1	#num#	k4
</s>
<s>
Luís	Luís	k6eAd1
Figo	Figo	k6eAd1
Sporting	Sporting	k1gInSc1
CP	CP	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
Ian	Ian	k?
Wright	Wright	k1gInSc1
Arsenal	Arsenal	k1gFnSc1
<g/>
1	#num#	k4
</s>
<s>
Tito	tento	k3xDgMnPc1
hráči	hráč	k1gMnPc1
byli	být	k5eAaImAgMnP
rovněž	rovněž	k9
nominováni	nominován	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nedostali	dostat	k5eNaPmAgMnP
žádný	žádný	k3yNgInSc4
hlas	hlas	k1gInSc4
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
Amokachi	Amokach	k1gFnSc2
<g/>
,	,	kIx,
Dino	Dino	k1gMnSc1
Baggio	Baggio	k1gMnSc1
<g/>
,	,	kIx,
Abel	Abel	k1gMnSc1
Balbo	Balba	k1gFnSc5
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Basler	Basler	k1gMnSc1
<g/>
,	,	kIx,
Júlio	Júlio	k1gMnSc1
César	César	k1gMnSc1
<g/>
,	,	kIx,
Didier	Didier	k1gMnSc1
Deschamps	Deschamps	k1gInSc1
<g/>
,	,	kIx,
Donato	Donat	k2eAgNnSc1d1
Gama	gama	k1gNnSc1
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
Effenberg	Effenberg	k1gMnSc1
<g/>
,	,	kIx,
Vincent	Vincent	k1gMnSc1
Guérin	Guérin	k1gInSc1
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
Karembeu	Karembeus	k1gInSc2
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
Lama	lama	k1gMnSc1
<g/>
,	,	kIx,
Japhet	Japhet	k1gMnSc1
N	N	kA
<g/>
'	'	kIx"
<g/>
Doram	Doram	k1gInSc1
<g/>
,	,	kIx,
Jay-Jay	Jay-Ja	k1gMnPc4
Okocha	Okoch	k1gMnSc4
<g/>
,	,	kIx,
Fernando	Fernanda	k1gFnSc5
Redondo	Redonda	k1gFnSc5
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Schmeichel	Schmeichel	k1gMnSc1
a	a	k8xC
Clarence	Clarence	k1gFnSc1
Seedorf	Seedorf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
or	or	k?
1995	#num#	k4
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
France	Franc	k1gMnSc2
Football	Football	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Držitelé	držitel	k1gMnPc1
ocenění	ocenění	k1gNnSc2
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
1956	#num#	k4
Stanley	Stanlea	k1gFnSc2
Matthews	Matthewsa	k1gFnPc2
•	•	k?
1957	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1958	#num#	k4
Raymond	Raymonda	k1gFnPc2
Kopa	kopa	k1gFnSc1
•	•	k?
1959	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1960	#num#	k4
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
•	•	k?
1961	#num#	k4
Omar	Omar	k1gInSc1
Sívori	Sívor	k1gFnSc2
•	•	k?
1962	#num#	k4
Josef	Josef	k1gMnSc1
Masopust	Masopust	k1gMnSc1
•	•	k?
1963	#num#	k4
Lev	lev	k1gInSc1
Jašin	Jašin	k1gInSc4
•	•	k?
1964	#num#	k4
Denis	Denisa	k1gFnPc2
Law	Law	k1gFnPc2
•	•	k?
1965	#num#	k4
Eusébio	Eusébio	k1gNnSc4
•	•	k?
1966	#num#	k4
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
•	•	k?
1967	#num#	k4
Flórián	Flórián	k1gMnSc1
Albert	Albert	k1gMnSc1
•	•	k?
1968	#num#	k4
George	Georg	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Best	Best	k1gInSc1
•	•	k?
1969	#num#	k4
Gianni	Gianeň	k1gFnSc6
Rivera	Rivera	k1gFnSc1
•	•	k?
1970	#num#	k4
Gerd	Gerd	k1gInSc1
Müller	Müller	k1gInSc4
•	•	k?
1971	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1972	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1973	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1974	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1975	#num#	k4
Oleg	Oleg	k1gMnSc1
Blochin	Blochin	k1gMnSc1
•	•	k?
1976	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1977	#num#	k4
Allan	Allan	k1gMnSc1
Simonsen	Simonsen	k1gInSc1
•	•	k?
1978	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1979	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1980	#num#	k4
Karl-Heinz	Karl-Heinz	k1gInSc1
Rummenigge	Rummenigg	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1981	#num#	k4
Karl-Heinz	Karl-Heinza	k1gFnPc2
Rummenigge	Rummenigge	k1gFnPc2
•	•	k?
1982	#num#	k4
Paolo	Paolo	k1gNnSc4
Rossi	Rosse	k1gFnSc3
•	•	k?
1983	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1984	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1985	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1986	#num#	k4
Ihor	Ihor	k1gInSc1
Belanov	Belanov	k1gInSc4
•	•	k?
1987	#num#	k4
Ruud	Ruud	k1gInSc1
Gullit	Gullit	k1gInSc4
•	•	k?
1988	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1989	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1990	#num#	k4
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
1991	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Papin	Papin	k1gMnSc1
•	•	k?
1992	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1993	#num#	k4
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc4
•	•	k?
1994	#num#	k4
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
•	•	k?
1995	#num#	k4
George	George	k1gInSc1
Weah	Weah	k1gInSc4
•	•	k?
1996	#num#	k4
Matthias	Matthias	k1gInSc1
Sammer	Sammer	k1gInSc4
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
1999	#num#	k4
Rivaldo	Rivaldo	k1gNnSc4
•	•	k?
2000	#num#	k4
Luís	Luísa	k1gFnPc2
Figo	Figo	k6eAd1
•	•	k?
2001	#num#	k4
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
•	•	k?
2002	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2003	#num#	k4
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
•	•	k?
2004	#num#	k4
Andrij	Andrij	k1gFnSc1
Ševčenko	Ševčenka	k1gFnSc5
•	•	k?
2005	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2006	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Fabio	Fabio	k1gNnSc1
Cannavaro	Cannavara	k1gFnSc5
•	•	k?
2007	#num#	k4
Kaká	kakat	k5eAaImIp3nS
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gFnSc2
•	•	k?
2019	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
neuděleno	udělen	k2eNgNnSc1d1
V	v	k7c6
letech	let	k1gInPc6
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
sloučeno	sloučit	k5eAaPmNgNnS
do	do	k7c2
ankety	anketa	k1gFnSc2
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFAUmístění	FIFAUmístění	k1gNnSc2
českých	český	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
