<s>
Jagodina	Jagodina	k1gFnSc1
</s>
<s>
Jagodina	Jagodina	k1gFnSc1
С	С	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
43	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
111	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Jagodina	Jagodina	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
470	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.jagodina.org.rs	www.jagodina.org.rs	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
035	#num#	k4
PSČ	PSČ	kA
</s>
<s>
35000	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
JA	JA	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jagodina	Jagodina	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
srbské	srbský	k2eAgFnSc6d1
cyrilici	cyrilice	k1gFnSc6
Ј	Ј	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
centrálním	centrální	k2eAgNnSc6d1
Srbsku	Srbsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
administrativním	administrativní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
Pomoravského	pomoravský	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
28	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Kragujevace	Kragujevace	k1gFnSc2
a	a	k8xC
115	#num#	k4
km	km	kA
jihovýchodě	jihovýchod	k1gInSc6
od	od	k7c2
Bělehradu	Bělehrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
slovanského	slovanský	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
jahody	jahoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
neslo	nést	k5eAaImAgNnS
město	město	k1gNnSc1
název	název	k1gInSc1
Svetozarevo	Svetozarevo	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
srbské	srbský	k2eAgFnSc6d1
cyrilici	cyrilice	k1gFnSc6
С	С	k?
<g/>
,	,	kIx,
podle	podle	k7c2
srbského	srbský	k2eAgMnSc2d1
socialisty	socialista	k1gMnSc2
Svetozara	Svetozar	k1gMnSc2
Markoviće	Marković	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Jagodina	Jagodina	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
významném	významný	k2eAgInSc6d1
dopravním	dopravní	k2eAgInSc6d1
tahu	tah	k1gInSc6
mezi	mezi	k7c7
městy	město	k1gNnPc7
Bělehrad	Bělehrad	k1gInSc1
a	a	k8xC
Niš	Niš	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
pohoří	pohoří	k1gNnSc2
Juhor	Juhora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
vede	vést	k5eAaImIp3nS
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Bělehrad	Bělehrad	k1gInSc1
<g/>
–	–	k?
<g/>
Niš	Niš	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
A	a	k9
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městem	město	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Belica	Belica	k1gFnSc1
<g/>
,	,	kIx,
přítok	přítok	k1gInSc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
osídlena	osídlen	k2eAgFnSc1d1
již	již	k9
od	od	k7c2
dob	doba	k1gFnPc2
existence	existence	k1gFnSc2
Keltů	Kelt	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
osadě	osada	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1399	#num#	k4
<g/>
;	;	kIx,
město	město	k1gNnSc1
samotné	samotný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
připomínáno	připomínán	k2eAgNnSc1d1
také	také	k6eAd1
i	i	k9
kvůli	kvůli	k7c3
pádu	pád	k1gInSc3
do	do	k7c2
rukou	ruka	k1gFnPc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1458	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
cestovatele	cestovatel	k1gMnSc2
Evlije	Evlije	k1gMnSc2
Čelebiho	Čelebi	k1gMnSc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Jagodina	Jagodin	k2eAgNnSc2d1
okolo	okolo	k7c2
1500	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
převážně	převážně	k6eAd1
muslimské	muslimský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhém	druhý	k4xOgInSc6
srbském	srbský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
nezávislosti	nezávislost	k1gFnSc3
Srbského	srbský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
,	,	kIx,
však	však	k9
byli	být	k5eAaImAgMnP
tito	tento	k3xDgMnPc1
muslimové	muslim	k1gMnPc1
vystěhováni	vystěhovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
Jagodina	Jagodina	k1gFnSc1
rozvíjela	rozvíjet	k5eAaImAgFnS
především	především	k9
díky	díky	k7c3
svému	svůj	k3xOyFgNnSc3
dobrému	dobrý	k2eAgNnSc3d1
dopravnímu	dopravní	k2eAgNnSc3d1
spojení	spojení	k1gNnSc3
se	s	k7c7
zbytkem	zbytek	k1gInSc7
Srbska	Srbsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
zde	zde	k6eAd1
např.	např.	kA
byla	být	k5eAaImAgFnS
zprovozněna	zprovozněn	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
jízdní	jízdní	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byla	být	k5eAaImAgFnS
Jagodina	Jagodina	k1gFnSc1
bombardována	bombardovat	k5eAaImNgFnS
letouny	letoun	k1gInPc7
NATO	nato	k6eAd1
během	během	k7c2
Operace	operace	k1gFnSc2
Spojenecká	spojenecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jagodina	Jagodin	k2eAgInSc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Srbsko	Srbsko	k1gNnSc1
–	–	k?
С	С	k?
<g/>
,	,	kIx,
Srbijа	Srbijа	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
SRB	Srb	k1gMnSc1
<g/>
)	)	kIx)
Okruhy	okruh	k1gInPc1
v	v	k7c6
Srbsku	Srbsko	k1gNnSc6
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
Centrální	centrální	k2eAgNnPc1d1
Srbsko	Srbsko	k1gNnSc4
</s>
<s>
Město	město	k1gNnSc1
Bělehrad	Bělehrad	k1gInSc1
•	•	k?
Borský	Borský	k1gMnSc1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
Bor	bor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Braničevský	Braničevský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Požarevac	Požarevac	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Jablanický	Jablanický	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Leskovac	Leskovac	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Kolubarský	Kolubarský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Valjevo	Valjevo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Mačvanský	Mačvanský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Šabac	Šabac	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Moravický	Moravický	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Čačak	Čačak	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Nišavský	Nišavský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
Niš	Niš	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pčinjský	Pčinjský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Vranje	Vranje	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pirotský	Pirotský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Pirot	Pirot	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Podunajský	podunajský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Smederevo	Smederevo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Pomoravský	pomoravský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Jagodina	Jagodina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rasinský	Rasinský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Kruševac	Kruševac	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Rašský	Rašský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Kraljevo	Kraljevo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Šumadijský	Šumadijský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Kragujevac	Kragujevac	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Toplický	Toplický	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Prokuplje	Prokuplje	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zaječarský	Zaječarský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Zaječar	Zaječar	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Zlatiborský	Zlatiborský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Užice	Užice	k1gFnSc1
<g/>
)	)	kIx)
Vojvodina	Vojvodina	k1gFnSc1
</s>
<s>
Jihobačský	Jihobačský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
Novi	Novi	k1gNnSc1
Sad	sada	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Jihobanátský	Jihobanátský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Pančevo	Pančevo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Severobačský	Severobačský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Subotica	Subotica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Severobanátský	Severobanátský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Kikinda	Kikinda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Středobanátský	Středobanátský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Zrenjanin	Zrenjanin	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Sremský	Sremský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Sremska	Sremsek	k1gMnSc2
Mitrovica	Mitrovicus	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
Západobačský	Západobačský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Sombor	Sombor	k1gInSc1
<g/>
)	)	kIx)
Kosovo	Kosův	k2eAgNnSc1d1
a	a	k8xC
Metochie	Metochie	k1gFnSc1
*	*	kIx~
</s>
<s>
okruhy	okruh	k1gInPc1
(	(	kIx(
<g/>
neexistující	existující	k2eNgFnPc1d1
od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
srbskou	srbský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
nadále	nadále	k6eAd1
uznávané	uznávaný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Prištinský	Prištinský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
Priština	Priština	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kosovskomitrovický	Kosovskomitrovický	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Kosovska	Kosovsko	k1gNnSc2
Mitrovica	Mitrovic	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Kosovsko-Pomoravský	kosovsko-pomoravský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Gnjilane	Gnjilan	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
Pećský	Pećský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Peć	Peć	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Prizrenský	Prizrenský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
(	(	kIx(
<g/>
Prizren	Prizrna	k1gFnPc2
<g/>
)	)	kIx)
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
*	*	kIx~
Kosovo	Kosův	k2eAgNnSc1d1
jednostranně	jednostranně	k6eAd1
vyhlásilo	vyhlásit	k5eAaPmAgNnS
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
většina	většina	k1gFnSc1
světového	světový	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
jej	on	k3xPp3gMnSc4
nadále	nadále	k6eAd1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
součást	součást	k1gFnSc4
srbského	srbský	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
nezávislou	závislý	k2eNgFnSc4d1
Kosovskou	kosovský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
neuznávají	uznávat	k5eNaImIp3nP
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
článek	článek	k1gInSc1
Seznam	seznam	k1gInSc4
států	stát	k1gInPc2
a	a	k8xC
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
podle	podle	k7c2
postoje	postoj	k1gInSc2
k	k	k7c3
nezávislosti	nezávislost	k1gFnSc3
Kosova	Kosův	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
1088971	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4525456-4	4525456-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149082745	#num#	k4
</s>
