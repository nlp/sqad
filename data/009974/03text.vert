<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Caius	Caius	k1gMnSc1	Caius
(	(	kIx(	(
<g/>
Gaius	Gaius	k1gMnSc1	Gaius
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
také	také	k9	také
Kájus	Kájus	k1gInSc1	Kájus
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
283	[number]	k4	283
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
296	[number]	k4	296
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Caius	Caius	k1gMnSc1	Caius
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Saloně	salon	k1gInSc6	salon
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
jen	jen	k9	jen
zříceniny	zřícenina	k1gFnSc2	zřícenina
poblíž	poblíž	k7c2	poblíž
Splitu	Split	k1gInSc2	Split
<g/>
)	)	kIx)	)
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
císaře	císař	k1gMnSc2	císař
Diocletiana	Diocletian	k1gMnSc2	Diocletian
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
pouze	pouze	k6eAd1	pouze
zmínky	zmínka	k1gFnSc2	zmínka
v	v	k7c6	v
životopisech	životopis	k1gInPc6	životopis
svatého	svatý	k1gMnSc2	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
a	a	k8xC	a
svaté	svatý	k2eAgFnSc2d1	svatá
Zuzany	Zuzana	k1gFnSc2	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
jejich	jejich	k3xOp3gMnSc7	jejich
strýcem	strýc	k1gMnSc7	strýc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zda	zda	k8xS	zda
byl	být	k5eAaImAgMnS	být
mučedníkem	mučedník	k1gMnSc7	mučedník
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
krvavé	krvavý	k2eAgNnSc1d1	krvavé
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Diocletiana	Diocletian	k1gMnSc2	Diocletian
(	(	kIx(	(
<g/>
císařem	císař	k1gMnSc7	císař
284	[number]	k4	284
–	–	k?	–
305	[number]	k4	305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgInPc1	první
edikty	edikt	k1gInPc1	edikt
proti	proti	k7c3	proti
křesťanům	křesťan	k1gMnPc3	křesťan
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
303	[number]	k4	303
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
Caius	Caius	k1gInSc1	Caius
dávno	dávno	k6eAd1	dávno
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrobka	hrobka	k1gFnSc1	hrobka
papeže	papež	k1gMnSc2	papež
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
Kalixtových	Kalixtův	k2eAgFnPc6d1	Kalixtův
katakombách	katakomby	k1gFnPc6	katakomby
včetně	včetně	k7c2	včetně
originálního	originální	k2eAgInSc2d1	originální
nápisu	nápis	k1gInSc2	nápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
bývá	bývat	k5eAaImIp3nS	bývat
Caius	Caius	k1gInSc1	Caius
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
s	s	k7c7	s
papežskou	papežský	k2eAgFnSc7d1	Papežská
tiárou	tiára	k1gFnSc7	tiára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
připomíná	připomínat	k5eAaImIp3nS	připomínat
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
společně	společně	k6eAd1	společně
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Soterem	Soter	k1gMnSc7	Soter
<g/>
.	.	kIx.	.
</s>
<s>
Caius	Caius	k1gMnSc1	Caius
je	být	k5eAaImIp3nS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
zejména	zejména	k9	zejména
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
a	a	k8xC	a
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Caius	Caius	k1gInSc1	Caius
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
