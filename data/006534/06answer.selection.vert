<s>
Nejsoustavnější	soustavní	k2eAgInSc1d3	soustavní
výklad	výklad	k1gInSc1	výklad
Descartovy	Descartův	k2eAgFnSc2d1	Descartova
fyziky	fyzika	k1gFnSc2	fyzika
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
O	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sice	sice	k8xC	sice
napsal	napsat	k5eAaBmAgInS	napsat
už	už	k6eAd1	už
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepublikoval	publikovat	k5eNaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
