<s>
Pisárky	Pisárka	k1gFnPc1	Pisárka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schreibwald	Schreibwald	k1gInSc1	Schreibwald
<g/>
,	,	kIx,	,
v	v	k7c6	v
hantecu	hantecum	k1gNnSc6	hantecum
Šrajbec	Šrajbec	k1gInSc4	Šrajbec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
4,67	[number]	k4	4,67
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
Pisárek	Pisárka	k1gFnPc2	Pisárka
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc6	Brno
připojeno	připojit	k5eAaPmNgNnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Pisárky	Pisárka	k1gFnPc1	Pisárka
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
mezi	mezi	k7c4	mezi
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1	Brno-Kohoutovice
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
Brno-Jundrov	Brno-Jundrov	k1gInSc1	Brno-Jundrov
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
2600	[number]	k4	2600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
katastru	katastr	k1gInSc2	katastr
Pisárek	Pisárka	k1gFnPc2	Pisárka
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
označovaný	označovaný	k2eAgInSc1d1	označovaný
"	"	kIx"	"
<g/>
Pisárky	Pisárka	k1gFnPc1	Pisárka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
čtvrť	čtvrť	k1gFnSc1	čtvrť
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
Pisárek	Pisárka	k1gFnPc2	Pisárka
náleží	náležet	k5eAaImIp3nS	náležet
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
k	k	k7c3	k
areálu	areál	k1gInSc3	areál
zdejšího	zdejší	k2eAgMnSc2d1	zdejší
mezinárodně	mezinárodně	k6eAd1	mezinárodně
proslulého	proslulý	k2eAgNnSc2d1	proslulé
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
(	(	kIx(	(
<g/>
BVV	BVV	kA	BVV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
čtvrtě	čtvrt	k1gFnSc2	čtvrt
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nedaleko	nedaleko	k7c2	nedaleko
areálu	areál	k1gInSc2	areál
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
výstaviště	výstaviště	k1gNnSc2	výstaviště
nachází	nacházet	k5eAaImIp3nS	nacházet
zalesněný	zalesněný	k2eAgInSc1d1	zalesněný
svah	svah	k1gInSc1	svah
s	s	k7c7	s
cyklistickou	cyklistický	k2eAgFnSc7d1	cyklistická
stezkou	stezka	k1gFnSc7	stezka
procházející	procházející	k2eAgFnSc7d1	procházející
kolem	kolem	k7c2	kolem
zdejšího	zdejší	k2eAgInSc2d1	zdejší
proslulého	proslulý	k2eAgInSc2d1	proslulý
pavilónu	pavilón	k1gInSc2	pavilón
Anthropos	Anthroposa	k1gFnPc2	Anthroposa
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
čtvrtě	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
lázně	lázeň	k1gFnPc1	lázeň
Riviéra	Riviéra	k1gFnSc1	Riviéra
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Holiday	Holidaa	k1gFnSc2	Holidaa
Inn	Inn	k1gFnSc2	Inn
<g/>
,	,	kIx,	,
generální	generální	k2eAgInSc4d1	generální
konzulát	konzulát	k1gInSc4	konzulát
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
generální	generální	k2eAgInSc4d1	generální
konzulát	konzulát	k1gInSc4	konzulát
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
vozoven	vozovna	k1gFnPc2	vozovna
<g/>
,	,	kIx,	,
Vozovna	vozovna	k1gFnSc1	vozovna
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
a	a	k8xC	a
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Veslařská	veslařský	k2eAgFnSc1d1	Veslařská
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
řada	řada	k1gFnSc1	řada
vil	vila	k1gFnPc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Libušino	Libušin	k2eAgNnSc1d1	Libušino
údolí	údolí	k1gNnSc1	údolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Pisárek	Pisárka	k1gFnPc2	Pisárka
původně	původně	k6eAd1	původně
patřila	patřit	k5eAaImAgFnS	patřit
ke	k	k7c3	k
Starému	starý	k2eAgNnSc3d1	staré
Brnu	Brno	k1gNnSc3	Brno
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
jejich	jejich	k3xOp3gInSc2	jejich
katastru	katastr	k1gInSc2	katastr
náležely	náležet	k5eAaImAgInP	náležet
i	i	k9	i
ke	k	k7c3	k
Křížové	Křížová	k1gFnSc3	Křížová
(	(	kIx(	(
<g/>
sever	sever	k1gInSc1	sever
Pisárek	Pisárek	k1gInSc1	Pisárek
počínaje	počínaje	k7c7	počínaje
středem	střed	k1gInSc7	střed
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jundrovu	Jundrov	k1gInSc3	Jundrov
(	(	kIx(	(
<g/>
severozápad	severozápad	k1gInSc1	severozápad
Pisárek	Pisárek	k1gInSc1	Pisárek
s	s	k7c7	s
ulicemi	ulice	k1gFnPc7	ulice
Antonína	Antonín	k1gMnSc2	Antonín
Procházky	Procházka	k1gMnSc2	Procházka
<g/>
,	,	kIx,	,
Libušino	Libušin	k2eAgNnSc1d1	Libušino
Údolí	údolí	k1gNnSc1	údolí
a	a	k8xC	a
Veslařská	veslařský	k2eAgFnSc1d1	Veslařská
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Žabovřeskám	Žabovřesky	k1gFnPc3	Žabovřesky
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
Pisárek	Pisárka	k1gFnPc2	Pisárka
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kohoutovicím	Kohoutovice	k1gFnPc3	Kohoutovice
(	(	kIx(	(
<g/>
malé	malý	k2eAgNnSc1d1	malé
zalesněné	zalesněný	k2eAgNnSc1d1	zalesněné
území	území	k1gNnSc1	území
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
středem	střed	k1gInSc7	střed
toku	tok	k1gInSc2	tok
Kohoutovického	kohoutovický	k2eAgInSc2d1	kohoutovický
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
Libušina	Libušin	k2eAgFnSc1d1	Libušina
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohunicím	Bohunice	k1gFnPc3	Bohunice
(	(	kIx(	(
<g/>
okrajová	okrajový	k2eAgNnPc1d1	okrajové
území	území	k1gNnPc1	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
současného	současný	k2eAgInSc2d1	současný
katastru	katastr	k1gInSc2	katastr
Pisárek	Pisárka	k1gFnPc2	Pisárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Brno	Brno	k1gNnSc4	Brno
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Pisárek	Pisárka	k1gFnPc2	Pisárka
důležité	důležitý	k2eAgNnSc4d1	důležité
už	už	k9	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tudy	tudy	k6eAd1	tudy
od	od	k7c2	od
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Kamenného	kamenný	k2eAgInSc2d1	kamenný
mlýna	mlýn	k1gInSc2	mlýn
vedlo	vést	k5eAaImAgNnS	vést
kamenné	kamenný	k2eAgNnSc1d1	kamenné
potrubí	potrubí	k1gNnSc1	potrubí
prvního	první	k4xOgInSc2	první
městského	městský	k2eAgInSc2d1	městský
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stavěl	stavět	k5eAaImAgMnS	stavět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1416	[number]	k4	1416
Prokop	Prokop	k1gMnSc1	Prokop
z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
brněnským	brněnský	k2eAgMnSc7d1	brněnský
měšťanem	měšťan	k1gMnSc7	měšťan
Václavem	Václav	k1gMnSc7	Václav
Hazem	Haz	k1gMnSc7	Haz
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Pisárek	Pisárek	k1gInSc1	Pisárek
<g/>
,	,	kIx,	,
prvně	prvně	k?	prvně
doložený	doložený	k2eAgInSc1d1	doložený
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
v	v	k7c6	v
soupisu	soupis	k1gInSc6	soupis
jmění	jmění	k1gNnSc2	jmění
starobrněnského	starobrněnský	k2eAgInSc2d1	starobrněnský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
starých	starý	k2eAgNnPc2d1	staré
místních	místní	k2eAgNnPc2d1	místní
pojmenování	pojmenování	k1gNnPc2	pojmenování
"	"	kIx"	"
<g/>
písařův	písařův	k2eAgInSc1d1	písařův
les	les	k1gInSc1	les
<g/>
"	"	kIx"	"
a	a	k8xC	a
louka	louka	k1gFnSc1	louka
"	"	kIx"	"
<g/>
písařska	písařska	k1gFnSc1	písařska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Pisárek	Pisárka	k1gFnPc2	Pisárka
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
zde	zde	k6eAd1	zde
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
domů	dům	k1gInPc2	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
tvořil	tvořit	k5eAaImAgInS	tvořit
původně	původně	k6eAd1	původně
jen	jen	k9	jen
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
Kamenný	kamenný	k2eAgInSc1d1	kamenný
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1366	[number]	k4	1366
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
něhož	jenž	k3xRgInSc2	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
osada	osada	k1gFnSc1	osada
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
i	i	k8xC	i
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
stávající	stávající	k2eAgInSc1d1	stávající
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
Žabovřeskám	Žabovřesky	k1gFnPc3	Žabovřesky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Žlutého	žlutý	k2eAgInSc2d1	žlutý
kopce	kopec	k1gInSc2	kopec
se	se	k3xPyFc4	se
rozkládaly	rozkládat	k5eAaImAgFnP	rozkládat
zahrady	zahrada	k1gFnPc1	zahrada
a	a	k8xC	a
vinice	vinice	k1gFnPc1	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1815	[number]	k4	1815
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
areálu	areál	k1gInSc2	areál
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
postaven	postaven	k2eAgInSc4d1	postaven
empírový	empírový	k2eAgInSc4d1	empírový
Bauerův	Bauerův	k2eAgInSc4d1	Bauerův
zámeček	zámeček	k1gInSc4	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
postaveny	postaven	k2eAgFnPc4d1	postavena
budovy	budova	k1gFnPc4	budova
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
,	,	kIx,	,
zbořené	zbořený	k2eAgNnSc1d1	zbořené
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
započala	započnout	k5eAaPmAgFnS	započnout
výstavba	výstavba	k1gFnSc1	výstavba
vil	vila	k1gFnPc2	vila
ve	v	k7c6	v
Veslařské	veslařský	k2eAgFnSc6d1	Veslařská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
tehdy	tehdy	k6eAd1	tehdy
k	k	k7c3	k
Jundrovu	Jundrov	k1gInSc3	Jundrov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rovněž	rovněž	k9	rovněž
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
ulice	ulice	k1gFnSc2	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
vilové	vilový	k2eAgFnSc2d1	vilová
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
takřka	takřka	k6eAd1	takřka
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
letech	let	k1gInPc6	let
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
ulici	ulice	k1gFnSc6	ulice
vystavně	vystavně	k6eAd1	vystavně
také	také	k9	také
Hechtova	Hechtův	k2eAgFnSc1d1	Hechtova
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
sídlí	sídlet	k5eAaImIp3nS	sídlet
Generální	generální	k2eAgInSc1d1	generální
konzulát	konzulát	k1gInSc1	konzulát
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926-1928	[number]	k4	1926-1928
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Pisárecké	pisárecký	k2eAgFnSc6d1	Pisárecká
nivě	niva	k1gFnSc6	niva
západně	západně	k6eAd1	západně
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Bauerova	Bauerův	k2eAgInSc2d1	Bauerův
zámečku	zámeček	k1gInSc2	zámeček
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
světoznámé	světoznámý	k2eAgNnSc1d1	světoznámé
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
těžce	těžce	k6eAd1	těžce
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
a	a	k8xC	a
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955-1959	[number]	k4	1955-1959
byla	být	k5eAaImAgFnS	být
plocha	plocha	k1gFnSc1	plocha
výstaviště	výstaviště	k1gNnSc2	výstaviště
zvětšena	zvětšit	k5eAaPmNgFnS	zvětšit
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
26	[number]	k4	26
ha	ha	kA	ha
na	na	k7c4	na
54	[number]	k4	54
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
Pisárky	Pisárek	k1gInPc1	Pisárek
až	až	k9	až
při	při	k7c6	při
radikální	radikální	k2eAgFnSc6d1	radikální
druhé	druhý	k4xOgFnSc6	druhý
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
Brna	Brno	k1gNnSc2	Brno
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
moderní	moderní	k2eAgFnPc4d1	moderní
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1	Brno-Kohoutovice
a	a	k8xC	a
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1995	[number]	k4	1995
pak	pak	k8xC	pak
část	část	k1gFnSc1	část
Pisárek	Pisárka	k1gFnPc2	Pisárka
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc2	Brno-Kohoutovice
podepsané	podepsaný	k2eAgFnSc2d1	podepsaná
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-Jundrov	Brno-Jundrov	k1gInSc4	Brno-Jundrov
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Jundrov	Jundrov	k1gInSc1	Jundrov
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnPc4	účinnost
menší	malý	k2eAgFnSc1d2	menší
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
k.	k.	k?	k.
ú.	ú.	k?	ú.
Pisárky	Pisárka	k1gFnPc4	Pisárka
a	a	k8xC	a
Bohunice	Bohunice	k1gFnPc4	Bohunice
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
a	a	k8xC	a
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
Bohunic	Bohunice	k1gFnPc2	Bohunice
přešly	přejít	k5eAaPmAgFnP	přejít
k	k	k7c3	k
Pisárkám	Pisárka	k1gFnPc3	Pisárka
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
)	)	kIx)	)
parcely	parcela	k1gFnPc1	parcela
s	s	k7c7	s
novými	nový	k2eAgNnPc7d1	nové
čísly	číslo	k1gNnPc7	číslo
2334	[number]	k4	2334
<g/>
,	,	kIx,	,
2335	[number]	k4	2335
<g/>
,	,	kIx,	,
2336	[number]	k4	2336
a	a	k8xC	a
2337	[number]	k4	2337
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
dohromady	dohromady	k6eAd1	dohromady
rozlohu	rozloha	k1gFnSc4	rozloha
42	[number]	k4	42
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
k	k	k7c3	k
Pisárkám	Pisárka	k1gFnPc3	Pisárka
nově	nově	k6eAd1	nově
připojené	připojený	k2eAgInPc1d1	připojený
pozemky	pozemek	k1gInPc1	pozemek
náležely	náležet	k5eAaImAgInP	náležet
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ke	k	k7c3	k
k.	k.	k?	k.
ú.	ú.	k?	ú.
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
realizaci	realizace	k1gFnSc6	realizace
projektu	projekt	k1gInSc2	projekt
sportovně-rekreační	sportovněekreační	k2eAgFnSc2d1	sportovně-rekreační
oblasti	oblast	k1gFnSc2	oblast
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
nacházet	nacházet	k5eAaImF	nacházet
za	za	k7c7	za
budovou	budova	k1gFnSc7	budova
Anthropos	Anthropos	k1gInSc1	Anthropos
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
architekti	architekt	k1gMnPc1	architekt
z	z	k7c2	z
brněnské	brněnský	k2eAgFnSc2d1	brněnská
architektonické	architektonický	k2eAgFnSc2d1	architektonická
kanceláře	kancelář	k1gFnSc2	kancelář
A	a	k8xC	a
PLUS	plus	k1gNnSc2	plus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
návrhem	návrh	k1gInSc7	návrh
Univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Anthropos	Anthropos	k1gInSc1	Anthropos
Brněnské	brněnský	k2eAgInPc1d1	brněnský
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
výstaviště	výstaviště	k1gNnSc2	výstaviště
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Generální	generální	k2eAgInSc1d1	generální
konzulát	konzulát	k1gInSc1	konzulát
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
Hechtova	Hechtův	k2eAgFnSc1d1	Hechtova
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
Generálního	generální	k2eAgInSc2d1	generální
konzulátu	konzulát	k1gInSc2	konzulát
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
)	)	kIx)	)
Reissigova	Reissigův	k2eAgFnSc1d1	Reissigova
vila	vila	k1gFnSc1	vila
Vozovna	vozovna	k1gFnSc1	vozovna
Pisárky	Pisárka	k1gFnSc2	Pisárka
Hotel	hotel	k1gInSc1	hotel
Holiday	Holidaa	k1gFnSc2	Holidaa
Inn	Inn	k1gFnSc2	Inn
Brno	Brno	k1gNnSc1	Brno
Seznam	seznam	k1gInSc4	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
(	(	kIx(	(
<g/>
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Brně-Kohoutovicích	Brně-Kohoutovice	k1gFnPc6	Brně-Kohoutovice
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc2	Brno-Kohoutovice
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Brně-Jundrově	Brně-Jundrův	k2eAgFnSc6d1	Brně-Jundrův
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-Jundrov	Brno-Jundrov	k1gInSc1	Brno-Jundrov
<g/>
)	)	kIx)	)
</s>
