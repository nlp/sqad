<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
slané	slaný	k2eAgNnSc1d1	slané
(	(	kIx(	(
<g/>
brakické	brakický	k2eAgNnSc4d1	brakické
<g/>
)	)	kIx)	)
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sladkovodních	sladkovodní	k2eAgMnPc2d1	sladkovodní
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
