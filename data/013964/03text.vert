<s>
Evropský	evropský	k2eAgInSc1d1
systém	systém	k1gInSc1
centrálních	centrální	k2eAgFnPc2d1
bank	banka	k1gFnPc2
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
systém	systém	k1gInSc1
centrálních	centrální	k2eAgFnPc2d1
bank	banka	k1gFnPc2
(	(	kIx(
<g/>
ESCB	ESCB	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
ustanoven	ustanovit	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
Maastrichtské	maastrichtský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
oficiálně	oficiálně	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
činnost	činnost	k1gFnSc4
1.1	1.1	k4
<g/>
.	.	kIx.
1999	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
praktické	praktický	k2eAgFnSc2d1
realizace	realizace	k1gFnSc2
hospodářské	hospodářský	k2eAgFnSc2d1
a	a	k8xC
měnové	měnový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
Evropské	evropský	k2eAgFnSc2d1
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
a	a	k8xC
centrálních	centrální	k2eAgFnPc2d1
bank	banka	k1gFnPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
i	i	k9
těch	ten	k3xDgInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
nejsou	být	k5eNaImIp3nP
členem	člen	k1gInSc7
Eurozóny	Eurozóna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Byly	být	k5eAaImAgInP
založeny	založit	k5eAaPmNgInP
tyto	tento	k3xDgInPc1
zásadní	zásadní	k2eAgInPc1d1
principy	princip	k1gInPc1
činnosti	činnost	k1gFnSc2
ESCB	ESCB	kA
<g/>
:	:	kIx,
</s>
<s>
udržování	udržování	k1gNnSc1
cenové	cenový	k2eAgFnSc2d1
stability	stabilita	k1gFnSc2
</s>
<s>
vytváření	vytváření	k1gNnSc1
a	a	k8xC
realizace	realizace	k1gFnSc1
jednotné	jednotný	k2eAgFnSc2d1
měnové	měnový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
</s>
<s>
podpora	podpora	k1gFnSc1
stability	stabilita	k1gFnSc2
měnových	měnový	k2eAgInPc2d1
kurzů	kurz	k1gInPc2
</s>
<s>
správa	správa	k1gFnSc1
devizových	devizový	k2eAgFnPc2d1
rezerv	rezerva	k1gFnPc2
</s>
<s>
péče	péče	k1gFnSc1
o	o	k7c4
řádný	řádný	k2eAgInSc4d1
chod	chod	k1gInSc4
systému	systém	k1gInSc2
centrálních	centrální	k2eAgFnPc2d1
bank	banka	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
3046795-0	3046795-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
95055496	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149840603	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
95055496	#num#	k4
</s>
