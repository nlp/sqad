<s>
Evropský	evropský	k2eAgInSc1d1
systém	systém	k1gInSc1
centrálních	centrální	k2eAgFnPc2d1
bank	banka	k1gFnPc2
(	(	kIx(
<g/>
ESCB	ESCB	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
ustanoven	ustanovit	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
Maastrichtské	maastrichtský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
oficiálně	oficiálně	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
činnost	činnost	k1gFnSc4
1.1	1.1	k4
<g/>
.	.	kIx.
1999	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
praktické	praktický	k2eAgFnSc2d1
realizace	realizace	k1gFnSc2
hospodářské	hospodářský	k2eAgFnSc2d1
a	a	k8xC
měnové	měnový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>