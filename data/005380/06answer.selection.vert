<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
města	město	k1gNnSc2	město
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
Královské	královský	k2eAgNnSc4d1	královské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
