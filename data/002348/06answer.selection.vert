<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
erysipel	erysipel	k1gInSc4	erysipel
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
beta-hemolytickým	betaemolytický	k2eAgInSc7d1	beta-hemolytický
streptokokem	streptokok	k1gInSc7	streptokok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
2-3	[number]	k4	2-3
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
onemocnění	onemocnění	k1gNnSc6	onemocnění
vyloučit	vyloučit	k5eAaPmF	vyloučit
revmatickou	revmatický	k2eAgFnSc4d1	revmatická
horečku	horečka	k1gFnSc4	horečka
nebo	nebo	k8xC	nebo
postreptokokovou	postreptokokový	k2eAgFnSc4d1	postreptokokový
glomerulonefritidu	glomerulonefritida	k1gFnSc4	glomerulonefritida
(	(	kIx(	(
<g/>
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
moč	moč	k1gFnSc4	moč
a	a	k8xC	a
sedimentaci	sedimentace	k1gFnSc4	sedimentace
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
poslech	poslech	k1gInSc1	poslech
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
