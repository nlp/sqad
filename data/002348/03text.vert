<s>
Erysipel	erysipel	k1gInSc1	erysipel
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
erysipelas	erysipelas	k1gInSc1	erysipelas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
růže	růž	k1gFnPc4	růž
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
akutní	akutní	k2eAgFnPc4d1	akutní
erytémové	erytémová	k1gFnPc4	erytémová
onemocnění	onemocnění	k1gNnSc2	onemocnění
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
vrchní	vrchní	k2eAgFnSc2d1	vrchní
vrstvy	vrstva	k1gFnSc2	vrstva
podkoží	podkoží	k1gNnSc1	podkoží
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
většinou	většina	k1gFnSc7	většina
beta-hemolytickými	betaemolytický	k2eAgInPc7d1	beta-hemolytický
streptokoky	streptokok	k1gInPc7	streptokok
skupiny	skupina	k1gFnSc2	skupina
A.	A.	kA	A.
Onemocnění	onemocnění	k1gNnSc1	onemocnění
nezanechává	zanechávat	k5eNaImIp3nS	zanechávat
imunitu	imunita	k1gFnSc4	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
erysipel	erysipel	k1gInSc4	erysipel
i	i	k9	i
beta-hemolytické	betaemolytický	k2eAgInPc4d1	beta-hemolytický
streptokoky	streptokok	k1gInPc4	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
G	G	kA	G
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
ev.	ev.	k?	ev.
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
s	s	k7c7	s
poruchou	porucha	k1gFnSc7	porucha
imunity	imunita	k1gFnSc2	imunita
mohou	moct	k5eAaImIp3nP	moct
erysipel	erysipel	k1gInSc4	erysipel
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
baktérie	baktérie	k1gFnPc4	baktérie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
postihuje	postihovat	k5eAaImIp3nS	postihovat
převážně	převážně	k6eAd1	převážně
mladé	mladý	k2eAgMnPc4d1	mladý
nebo	nebo	k8xC	nebo
starší	starý	k2eAgMnPc4d2	starší
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Erysipel	erysipel	k1gInSc1	erysipel
vzniká	vznikat	k5eAaImIp3nS	vznikat
zejména	zejména	k9	zejména
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
oslabených	oslabený	k2eAgMnPc2d1	oslabený
<g/>
,	,	kIx,	,
po	po	k7c6	po
traumatu	trauma	k1gNnSc6	trauma
<g/>
,	,	kIx,	,
podchlazení	podchlazení	k1gNnSc6	podchlazení
a	a	k8xC	a
s	s	k7c7	s
oslabenou	oslabený	k2eAgFnSc7d1	oslabená
imunitou	imunita	k1gFnSc7	imunita
(	(	kIx(	(
<g/>
diabetici	diabetik	k1gMnPc1	diabetik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
čerstvá	čerstvý	k2eAgNnPc4d1	čerstvé
nebo	nebo	k8xC	nebo
stará	starý	k2eAgNnPc4d1	staré
poranění	poranění	k1gNnPc4	poranění
(	(	kIx(	(
<g/>
po	po	k7c6	po
otevřených	otevřený	k2eAgFnPc6d1	otevřená
frakturách	fraktura	k1gFnPc6	fraktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vředy	vřed	k1gInPc1	vřed
na	na	k7c6	na
dolních	dolní	k2eAgFnPc6d1	dolní
končetinách	končetina	k1gFnPc6	končetina
<g/>
,	,	kIx,	,
ragády	ragáda	k1gFnPc1	ragáda
při	při	k7c6	při
dermatomykózách	dermatomykóza	k1gFnPc6	dermatomykóza
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
<g/>
,	,	kIx,	,
lymfostáza	lymfostáza	k1gFnSc1	lymfostáza
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
(	(	kIx(	(
<g/>
při	při	k7c6	při
městnání	městnání	k1gNnSc6	městnání
srdečním	srdeční	k2eAgNnSc6d1	srdeční
<g/>
,	,	kIx,	,
po	po	k7c6	po
exenteraci	exenterace	k1gFnSc6	exenterace
axilárních	axilární	k2eAgFnPc2d1	axilární
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
erose	erose	k1gFnSc1	erose
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
nosního	nosní	k2eAgInSc2d1	nosní
při	při	k7c6	při
rýmě	rýma	k1gFnSc6	rýma
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dolních	dolní	k2eAgFnPc2d1	dolní
a	a	k8xC	a
horních	horní	k2eAgFnPc2d1	horní
končetin	končetina	k1gFnPc2	končetina
postihuje	postihovat	k5eAaImIp3nS	postihovat
obličej	obličej	k1gInSc4	obličej
(	(	kIx(	(
<g/>
víčka	víčko	k1gNnPc1	víčko
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
<g/>
)	)	kIx)	)
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
i	i	k9	i
vulvu	vulva	k1gFnSc4	vulva
nebo	nebo	k8xC	nebo
penis	penis	k1gInSc4	penis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
komplikován	komplikovat	k5eAaBmNgInS	komplikovat
nekrózou	nekróza	k1gFnSc7	nekróza
a	a	k8xC	a
gangrénou	gangréna	k1gFnSc7	gangréna
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
začíná	začínat	k5eAaImIp3nS	začínat
prudkým	prudký	k2eAgInSc7d1	prudký
vzestupem	vzestup	k1gInSc7	vzestup
teplot	teplota	k1gFnPc2	teplota
ke	k	k7c3	k
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
spojeným	spojený	k2eAgNnSc7d1	spojené
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
zchvácenností	zchvácennost	k1gFnSc7	zchvácennost
<g/>
,	,	kIx,	,
zimnicí	zimnice	k1gFnSc7	zimnice
a	a	k8xC	a
třesavkou	třesavka	k1gFnSc7	třesavka
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
nauzeou	nauzea	k1gFnSc7	nauzea
až	až	k8xS	až
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgFnPc2d1	následující
hodin	hodina	k1gFnPc2	hodina
až	až	k9	až
1	[number]	k4	1
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
2	[number]	k4	2
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
kožní	kožní	k2eAgInPc1d1	kožní
projevy	projev	k1gInPc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zarudnutí	zarudnutí	k1gNnSc4	zarudnutí
<g/>
,	,	kIx,	,
palpační	palpační	k2eAgFnSc4d1	palpační
citlivost	citlivost	k1gFnSc4	citlivost
a	a	k8xC	a
mírný	mírný	k2eAgInSc4d1	mírný
edém	edém	k1gInSc4	edém
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Ložisko	ložisko	k1gNnSc1	ložisko
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
růžové	růžový	k2eAgNnSc1d1	růžové
až	až	k8xS	až
světle	světle	k6eAd1	světle
červené	červený	k2eAgFnPc4d1	červená
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
a	a	k8xC	a
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
jazykovitými	jazykovitý	k2eAgInPc7d1	jazykovitý
výběžky	výběžek	k1gInPc7	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
postižení	postižení	k1gNnSc2	postižení
je	být	k5eAaImIp3nS	být
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
hmatné	hmatný	k2eAgFnPc1d1	hmatná
a	a	k8xC	a
bolestivé	bolestivý	k2eAgFnPc1d1	bolestivá
spádové	spádový	k2eAgFnPc1d1	spádová
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
erysipel	erysipel	k1gInSc1	erysipel
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
dolních	dolní	k2eAgFnPc6d1	dolní
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
na	na	k7c6	na
tvářích	tvář	k1gFnPc6	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
typického	typický	k2eAgInSc2d1	typický
obrazu	obraz	k1gInSc2	obraz
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
erysipel	erysipel	k1gInSc4	erysipel
charakter	charakter	k1gInSc1	charakter
vesikulózní	vesikulózní	k2eAgInSc1d1	vesikulózní
(	(	kIx(	(
<g/>
e.	e.	k?	e.
vesicullosum	vesicullosum	k1gInSc1	vesicullosum
<g/>
)	)	kIx)	)
až	až	k9	až
bulózní	bulózní	k2eAgFnSc1d1	bulózní
(	(	kIx(	(
<g/>
e.	e.	k?	e.
bullosum	bullosum	k1gInSc1	bullosum
<g/>
)	)	kIx)	)
s	s	k7c7	s
hemoragiemi	hemoragie	k1gFnPc7	hemoragie
do	do	k7c2	do
puchýřků	puchýřek	k1gInPc2	puchýřek
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
ložiska	ložisko	k1gNnPc4	ložisko
přecházet	přecházet	k5eAaImF	přecházet
v	v	k7c4	v
nekrózu	nekróza	k1gFnSc4	nekróza
(	(	kIx(	(
<g/>
e.	e.	k?	e.
gangrenosum	gangrenosum	k1gInSc1	gangrenosum
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
flegmónu	flegmóna	k1gFnSc4	flegmóna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
dolních	dolní	k2eAgFnPc6d1	dolní
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
jevem	jev	k1gInSc7	jev
u	u	k7c2	u
erysipelu	erysipel	k1gInSc2	erysipel
jsou	být	k5eAaImIp3nP	být
recidivy	recidiva	k1gFnPc1	recidiva
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zhojení	zhojení	k1gNnSc6	zhojení
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
streptokok	streptokok	k1gInSc1	streptokok
opouzdřen	opouzdřen	k2eAgInSc1d1	opouzdřen
v	v	k7c6	v
tkáni	tkáň	k1gFnSc6	tkáň
a	a	k8xC	a
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
oslabení	oslabení	k1gNnSc2	oslabení
organismu	organismus	k1gInSc2	organismus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinickém	klinický	k2eAgInSc6d1	klinický
obrazu	obraz	k1gInSc6	obraz
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
chybět	chybět	k5eAaImF	chybět
některé	některý	k3yIgInPc1	některý
celkové	celkový	k2eAgInPc1d1	celkový
příznaky	příznak	k1gInPc1	příznak
jako	jako	k8xS	jako
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
-	-	kIx~	-
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
nauzea	nauzea	k1gFnSc1	nauzea
nebo	nebo	k8xC	nebo
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Erytém	erytém	k1gInSc1	erytém
rovněž	rovněž	k9	rovněž
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
výrazný	výrazný	k2eAgInSc1d1	výrazný
a	a	k8xC	a
typický	typický	k2eAgInSc1d1	typický
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
recidivách	recidiva	k1gFnPc6	recidiva
erysipelu	erysipel	k1gInSc2	erysipel
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sklerotizaci	sklerotizace	k1gFnSc3	sklerotizace
podkožních	podkožní	k2eAgFnPc2d1	podkožní
lymfatických	lymfatický	k2eAgFnPc2d1	lymfatická
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
edému	edém	k1gInSc3	edém
končetiny	končetina	k1gFnSc2	končetina
a	a	k8xC	a
následně	následně	k6eAd1	následně
vývoji	vývoj	k1gInSc3	vývoj
elefantiázy	elefantiáza	k1gFnSc2	elefantiáza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratorních	laboratorní	k2eAgNnPc6d1	laboratorní
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
zjišťujeme	zjišťovat	k5eAaImIp1nP	zjišťovat
zvýšení	zvýšení	k1gNnSc3	zvýšení
sedimentace	sedimentace	k1gFnSc2	sedimentace
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
leukocytózu	leukocytóza	k1gFnSc4	leukocytóza
a	a	k8xC	a
vzestup	vzestup	k1gInSc4	vzestup
ASLO	ASLO	kA	ASLO
<g/>
.	.	kIx.	.
</s>
<s>
Výtěry	výtěr	k1gInPc1	výtěr
provedené	provedený	k2eAgInPc1d1	provedený
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
bran	brána	k1gFnPc2	brána
vstupu	vstup	k1gInSc2	vstup
nebývají	bývat	k5eNaImIp3nP	bývat
příliš	příliš	k6eAd1	příliš
přínosná	přínosný	k2eAgNnPc1d1	přínosné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozitivita	pozitivita	k1gFnSc1	pozitivita
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
brána	brána	k1gFnSc1	brána
vstupu	vstup	k1gInSc2	vstup
není	být	k5eNaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
kožní	kožní	k2eAgInPc1d1	kožní
stěry	stěr	k1gInPc1	stěr
jsou	být	k5eAaImIp3nP	být
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
terapie	terapie	k1gFnSc2	terapie
tyto	tento	k3xDgFnPc4	tento
metody	metoda	k1gFnPc4	metoda
nemohou	moct	k5eNaImIp3nP	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
klinickém	klinický	k2eAgInSc6d1	klinický
obrazu	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
následovaných	následovaný	k2eAgFnPc6d1	následovaná
vznikem	vznik	k1gInSc7	vznik
ostře	ostro	k6eAd1	ostro
ohraničeného	ohraničený	k2eAgInSc2d1	ohraničený
erytému	erytém	k1gInSc2	erytém
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k9	i
anamnéza	anamnéza	k1gFnSc1	anamnéza
(	(	kIx(	(
<g/>
časté	častý	k2eAgFnPc1d1	častá
recidivy	recidiva	k1gFnPc1	recidiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnóza	diagnóza	k1gFnSc1	diagnóza
<g/>
:	:	kIx,	:
při	při	k7c6	při
diagnostice	diagnostika	k1gFnSc6	diagnostika
erysipelu	erysipel	k1gInSc2	erysipel
musíme	muset	k5eAaImIp1nP	muset
především	především	k6eAd1	především
vyloučit	vyloučit	k5eAaPmF	vyloučit
cellulitidu	cellulitida	k1gFnSc4	cellulitida
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
flegmónou	flegmóna	k1gFnSc7	flegmóna
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
MKN-10	MKN-10	k1gFnSc2	MKN-10
dg	dg	kA	dg
<g/>
.	.	kIx.	.
<g/>
L	L	kA	L
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
postihuje	postihovat	k5eAaImIp3nS	postihovat
hlubší	hluboký	k2eAgFnPc4d2	hlubší
vrstvy	vrstva	k1gFnPc4	vrstva
podkoží	podkoží	k1gNnSc2	podkoží
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
přesně	přesně	k6eAd1	přesně
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
fascie	fascie	k1gFnPc4	fascie
a	a	k8xC	a
na	na	k7c4	na
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
větší	veliký	k2eAgInSc4d2	veliký
edém	edém	k1gInSc4	edém
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
akutní	akutní	k2eAgMnSc1d1	akutní
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
subakutní	subakutní	k2eAgFnSc1d1	subakutní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chronický	chronický	k2eAgInSc1d1	chronický
<g/>
.	.	kIx.	.
</s>
<s>
Odlišení	odlišení	k1gNnSc4	odlišení
těchto	tento	k3xDgInPc2	tento
2	[number]	k4	2
onemocnění	onemocnění	k1gNnPc2	onemocnění
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
poměrně	poměrně	k6eAd1	poměrně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyloučit	vyloučit	k5eAaPmF	vyloučit
tromboflebitidu	tromboflebitida	k1gFnSc4	tromboflebitida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
jak	jak	k8xC	jak
při	při	k7c6	při
dif	dif	k?	dif
<g/>
.	.	kIx.	.
dg	dg	kA	dg
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
při	při	k7c6	při
komplikacích	komplikace	k1gFnPc6	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Varikózní	varikózní	k2eAgInSc4d1	varikózní
<g/>
,	,	kIx,	,
hypostatický	hypostatický	k2eAgInSc4d1	hypostatický
či	či	k8xC	či
kontaktní	kontaktní	k2eAgInSc4d1	kontaktní
ekzém	ekzém	k1gInSc4	ekzém
nemají	mít	k5eNaImIp3nP	mít
celkové	celkový	k2eAgInPc1d1	celkový
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nP	šířit
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Erytéma	Erytéma	k1gFnSc1	Erytéma
migrans	migransa	k1gFnPc2	migransa
(	(	kIx(	(
<g/>
I.	I.	kA	I.
stadium	stadium	k1gNnSc1	stadium
lymské	lymské	k1gNnSc1	lymské
boréliózy	borélióza	k1gFnSc2	borélióza
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
bez	bez	k7c2	bez
celkových	celkový	k2eAgInPc2d1	celkový
a	a	k8xC	a
lokálních	lokální	k2eAgInPc2d1	lokální
subj.	subj.	k?	subj.
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Angioneurotický	Angioneurotický	k2eAgMnSc1d1	Angioneurotický
(	(	kIx(	(
<g/>
Quinckeho	Quincke	k1gMnSc4	Quincke
<g/>
)	)	kIx)	)
edém	edém	k1gInSc1	edém
probíhá	probíhat	k5eAaImIp3nS	probíhat
akutně	akutně	k6eAd1	akutně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemívá	mívat	k5eNaImIp3nS	mívat
zarudnutí	zarudnutí	k1gNnSc1	zarudnutí
<g/>
.	.	kIx.	.
</s>
<s>
Erysipeloid	Erysipeloid	k1gInSc1	Erysipeloid
má	mít	k5eAaImIp3nS	mít
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
lokalisaci	lokalisace	k1gFnSc4	lokalisace
na	na	k7c6	na
prstech	prst	k1gInPc6	prst
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
snadno	snadno	k6eAd1	snadno
prokazatelný	prokazatelný	k2eAgInSc1d1	prokazatelný
kontakt	kontakt	k1gInSc1	kontakt
postiženého	postižený	k1gMnSc2	postižený
se	s	k7c7	s
skotem	skot	k1gInSc7	skot
nebo	nebo	k8xC	nebo
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Incipientní	Incipientní	k2eAgNnSc1d1	Incipientní
stadium	stadium	k1gNnSc1	stadium
herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gInSc1	simplex
a	a	k8xC	a
herpes	herpes	k1gInSc1	herpes
zoster	zostrum	k1gNnPc2	zostrum
nemívá	mívat	k5eNaImIp3nS	mívat
celkové	celkový	k2eAgInPc1d1	celkový
příznaky	příznak	k1gInPc1	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Erytéma	Erytéma	k1gFnSc1	Erytéma
nodózum	nodózum	k1gInSc1	nodózum
má	mít	k5eAaImIp3nS	mít
tuhé	tuhý	k2eAgInPc4d1	tuhý
a	a	k8xC	a
bolestivé	bolestivý	k2eAgInPc4d1	bolestivý
podkožní	podkožní	k2eAgInPc4d1	podkožní
infiltráty	infiltrát	k1gInPc4	infiltrát
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
bércích	bérec	k1gInPc6	bérec
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
není	být	k5eNaImIp3nS	být
akutní	akutní	k2eAgMnSc1d1	akutní
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
celkových	celkový	k2eAgInPc2d1	celkový
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
projev	projev	k1gInSc4	projev
jiného	jiný	k2eAgNnSc2d1	jiné
závažného	závažný	k2eAgNnSc2d1	závažné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénu	terén	k1gInSc6	terén
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
léčit	léčit	k5eAaImF	léčit
pacienty	pacient	k1gMnPc4	pacient
s	s	k7c7	s
jasnou	jasný	k2eAgFnSc7d1	jasná
diagnózou	diagnóza	k1gFnSc7	diagnóza
<g/>
,	,	kIx,	,
nekomplikované	komplikovaný	k2eNgFnSc2d1	nekomplikovaná
formy	forma	k1gFnSc2	forma
(	(	kIx(	(
<g/>
ne	ne	k9	ne
erysipel	erysipel	k1gInSc1	erysipel
vesikulózní	vesikulózní	k2eAgInSc1d1	vesikulózní
<g/>
,	,	kIx,	,
bulózní	bulózní	k2eAgInSc1d1	bulózní
či	či	k8xC	či
gangrenózní	gangrenózní	k2eAgInSc1d1	gangrenózní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pacienty	pacient	k1gMnPc4	pacient
bez	bez	k7c2	bez
celkových	celkový	k2eAgFnPc2d1	celková
poruch	porucha	k1gFnPc2	porucha
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
erysipelu	erysipel	k1gInSc2	erysipel
mimo	mimo	k7c4	mimo
obličej	obličej	k1gInSc4	obličej
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pacienty	pacient	k1gMnPc4	pacient
s	s	k7c7	s
častýni	častýň	k1gMnPc7	častýň
recidivami	recidiva	k1gFnPc7	recidiva
dobře	dobře	k6eAd1	dobře
reagujícími	reagující	k2eAgFnPc7d1	reagující
na	na	k7c6	na
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vyvolavatel	vyvolavatel	k1gMnSc1	vyvolavatel
erysipelu	erysipel	k1gInSc2	erysipel
-	-	kIx~	-
beta-hemolytické	betaemolytický	k2eAgInPc4d1	beta-hemolytický
streptokoky	streptokok	k1gInPc4	streptokok
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
dobře	dobře	k6eAd1	dobře
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
penicilin	penicilin	k1gInSc4	penicilin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
penicilin	penicilin	k1gInSc1	penicilin
lékem	lék	k1gInSc7	lék
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
klinický	klinický	k2eAgInSc1d1	klinický
obraz	obraz	k1gInSc1	obraz
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ponechat	ponechat	k5eAaPmF	ponechat
pacienta	pacient	k1gMnSc4	pacient
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
volíme	volit	k5eAaImIp1nP	volit
phenoxymethylpenicillinum	phenoxymethylpenicillinum	k1gNnSc4	phenoxymethylpenicillinum
(	(	kIx(	(
<g/>
V-Penicilin	V-Penicilin	k1gInSc1	V-Penicilin
<g/>
,	,	kIx,	,
Pencid	Pencid	k1gInSc1	Pencid
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
v	v	k7c6	v
dávce	dávka	k1gFnSc6	dávka
0,5	[number]	k4	0,5
g	g	kA	g
p.	p.	k?	p.
<g/>
o.	o.	k?	o.
po	po	k7c6	po
6	[number]	k4	6
hodinách	hodina	k1gFnPc6	hodina
u	u	k7c2	u
dospělého	dospělý	k2eAgNnSc2d1	dospělé
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
doba	doba	k1gFnSc1	doba
podávání	podávání	k1gNnSc2	podávání
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
dříve	dříve	k6eAd2	dříve
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
příznaků	příznak	k1gInPc2	příznak
je	být	k5eAaImIp3nS	být
penicilin	penicilin	k1gInSc1	penicilin
nasazen	nasadit	k5eAaPmNgInS	nasadit
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
má	mít	k5eAaImIp3nS	mít
onemocnění	onemocnění	k1gNnSc4	onemocnění
kratší	krátký	k2eAgInSc1d2	kratší
průběh	průběh	k1gInSc1	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
alergii	alergie	k1gFnSc6	alergie
na	na	k7c4	na
penicilin	penicilin	k1gInSc4	penicilin
je	být	k5eAaImIp3nS	být
lékem	lék	k1gInSc7	lék
druhé	druhý	k4xOgFnSc2	druhý
volby	volba	k1gFnSc2	volba
erythromycin	erythromycina	k1gFnPc2	erythromycina
(	(	kIx(	(
<g/>
250	[number]	k4	250
mg	mg	kA	mg
á	á	k0	á
6	[number]	k4	6
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
event.	event.	k?	event.
jiné	jiný	k2eAgFnSc2d1	jiná
makrolidové	makrolidový	k2eAgFnSc2d1	makrolidová
ATB	ATB	kA	ATB
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
nesmíme	smět	k5eNaImIp1nP	smět
opominout	opominout	k5eAaPmF	opominout
léčit	léčit	k5eAaImF	léčit
možnou	možný	k2eAgFnSc4d1	možná
bránu	brána	k1gFnSc4	brána
vstupu	vstup	k1gInSc2	vstup
-	-	kIx~	-
vředy	vřed	k1gInPc1	vřed
<g/>
,	,	kIx,	,
ragády	ragáda	k1gFnPc1	ragáda
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
erysipelem	erysipel	k1gInSc7	erysipel
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
etiol	etiol	k1gInSc1	etiol
<g/>
.	.	kIx.	.
agens	agens	k1gInSc1	agens
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenzae	k1gInSc4	influenzae
<g/>
,	,	kIx,	,
odesíláme	odesílat	k5eAaImIp1nP	odesílat
ihned	ihned	k6eAd1	ihned
k	k	k7c3	k
hospitalisaci	hospitalisace	k1gFnSc3	hospitalisace
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
léčba	léčba	k1gFnSc1	léčba
má	mít	k5eAaImIp3nS	mít
podpůrný	podpůrný	k2eAgInSc4d1	podpůrný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
mírnící	mírnící	k2eAgFnPc4d1	mírnící
subjektivní	subjektivní	k2eAgFnPc4d1	subjektivní
obtíže	obtíž	k1gFnPc4	obtíž
pálení	pálení	k1gNnSc2	pálení
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
aplikovat	aplikovat	k5eAaBmF	aplikovat
chladné	chladný	k2eAgInPc4d1	chladný
obklady	obklad	k1gInPc4	obklad
(	(	kIx(	(
<g/>
chladná	chladný	k2eAgFnSc1d1	chladná
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
<g/>
lépe	dobře	k6eAd2	dobře
ledové	ledový	k2eAgInPc1d1	ledový
obklady	obklad	k1gInPc1	obklad
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgInPc1d1	moderní
ochlazovací	ochlazovací	k2eAgInPc1d1	ochlazovací
gely	gel	k1gInPc1	gel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
adstringentní	adstringentní	k2eAgInPc1d1	adstringentní
octanové	octanový	k2eAgInPc1d1	octanový
obklady	obklad	k1gInPc1	obklad
=	=	kIx~	=
liq	liq	k?	liq
<g/>
.	.	kIx.	.
aluminii	aluminium	k1gNnPc7	aluminium
aceticotartarici	aceticotartarik	k1gMnPc5	aceticotartarik
=	=	kIx~	=
sol	sol	k1gNnSc4	sol
<g/>
.	.	kIx.	.
</s>
<s>
Burowi	Burowi	k6eAd1	Burowi
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
protizánětlivé	protizánětlivý	k2eAgFnPc4d1	protizánětlivá
masti	mast	k1gFnPc4	mast
např.	např.	kA	např.
Ung	Ung	k1gFnSc2	Ung
<g/>
.	.	kIx.	.
</s>
<s>
Ichtamoli	Ichtamoli	k6eAd1	Ichtamoli
nebo	nebo	k8xC	nebo
Ichtoxyl	ichtoxyl	k1gInSc4	ichtoxyl
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
erysipel	erysipel	k1gInSc4	erysipel
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
beta-hemolytickým	betaemolytický	k2eAgInSc7d1	beta-hemolytický
streptokokem	streptokok	k1gInSc7	streptokok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
2-3	[number]	k4	2-3
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
onemocnění	onemocnění	k1gNnSc6	onemocnění
vyloučit	vyloučit	k5eAaPmF	vyloučit
revmatickou	revmatický	k2eAgFnSc4d1	revmatická
horečku	horečka	k1gFnSc4	horečka
nebo	nebo	k8xC	nebo
postreptokokovou	postreptokokový	k2eAgFnSc4d1	postreptokokový
glomerulonefritidu	glomerulonefritida	k1gFnSc4	glomerulonefritida
(	(	kIx(	(
<g/>
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
moč	moč	k1gFnSc4	moč
a	a	k8xC	a
sedimentaci	sedimentace	k1gFnSc4	sedimentace
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
poslech	poslech	k1gInSc1	poslech
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
revmatická	revmatický	k2eAgFnSc1d1	revmatická
vada	vada	k1gFnSc1	vada
srdeční	srdeční	k2eAgNnSc1d1	srdeční
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
předchozích	předchozí	k2eAgInPc2d1	předchozí
příznaků	příznak	k1gInPc2	příznak
revmatické	revmatický	k2eAgFnSc2d1	revmatická
horečky	horečka	k1gFnSc2	horečka
<g/>
!	!	kIx.	!
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
lymfangoitida	lymfangoitida	k1gFnSc1	lymfangoitida
a	a	k8xC	a
lymfadenopatie	lymfadenopatie	k1gFnSc1	lymfadenopatie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
uzliny	uzlina	k1gFnPc4	uzlina
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
a	a	k8xC	a
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
terapie	terapie	k1gFnSc2	terapie
abychom	aby	kYmCp1nP	aby
předešli	předejít	k5eAaPmAgMnP	předejít
komplikaci	komplikace	k1gFnSc4	komplikace
při	při	k7c6	při
event.	event.	k?	event.
kolikvaci	kolikvace	k1gFnSc6	kolikvace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
erysipel	erysipel	k1gInSc1	erysipel
přejít	přejít	k5eAaPmF	přejít
v	v	k7c4	v
cellulitidu	cellulitida	k1gFnSc4	cellulitida
(	(	kIx(	(
<g/>
flegmónu	flegmóna	k1gFnSc4	flegmóna
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dif	dif	k?	dif
<g/>
.	.	kIx.	.
<g/>
dg	dg	kA	dg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšířit	rozšířit	k5eAaPmF	rozšířit
se	se	k3xPyFc4	se
na	na	k7c4	na
fascie	fascie	k1gFnPc4	fascie
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
fasciitidu	fasciitida	k1gFnSc4	fasciitida
<g/>
,	,	kIx,	,
myositidu	myositida	k1gFnSc4	myositida
nebo	nebo	k8xC	nebo
subkutánní	subkutánní	k2eAgInPc4d1	subkutánní
abscesy	absces	k1gInPc4	absces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
rozvíjející	rozvíjející	k2eAgFnSc4d1	rozvíjející
se	se	k3xPyFc4	se
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
tromboflebitidu	tromboflebitida	k1gFnSc4	tromboflebitida
podáme	podat	k5eAaPmIp1nP	podat
antikoagulancia	antikoagulancius	k1gMnSc2	antikoagulancius
a	a	k8xC	a
odešleme	odeslat	k5eAaPmIp1nP	odeslat
pac	pac	k1gFnSc4	pac
<g/>
.	.	kIx.	.
k	k	k7c3	k
hospitalisaci	hospitalisace	k1gFnSc3	hospitalisace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
elefantiáza	elefantiáza	k1gFnSc1	elefantiáza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
terapii	terapie	k1gFnSc4	terapie
konzultovat	konzultovat	k5eAaImF	konzultovat
s	s	k7c7	s
cévním	cévní	k2eAgMnSc7d1	cévní
a	a	k8xC	a
kožním	kožní	k2eAgMnSc7d1	kožní
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
profylaxí	profylaxe	k1gFnSc7	profylaxe
je	být	k5eAaImIp3nS	být
podávání	podávání	k1gNnSc1	podávání
benzatinbenzylpenicilinu	benzatinbenzylpenicilin	k1gInSc2	benzatinbenzylpenicilin
(	(	kIx(	(
<g/>
Pendepon	Pendepon	k1gMnSc1	Pendepon
<g/>
,	,	kIx,	,
Retarpen	Retarpen	k2eAgMnSc1d1	Retarpen
<g/>
)	)	kIx)	)
1,2	[number]	k4	1,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2,4	[number]	k4	2,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
I.U.	I.U.	k?	I.U.
i.	i.	k?	i.
<g/>
m.	m.	k?	m.
každé	každý	k3xTgNnSc4	každý
3-4	[number]	k4	3-4
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vysodávkovaných	vysodávkovaný	k2eAgFnPc2d1	vysodávkovaný
tablet	tableta	k1gFnPc2	tableta
penicilinu	penicilin	k1gInSc2	penicilin
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přechodně	přechodně	k6eAd1	přechodně
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dovolených	dovolená	k1gFnPc2	dovolená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
trvale	trvale	k6eAd1	trvale
podávat	podávat	k5eAaImF	podávat
phenoxymethylpenicilin	phenoxymethylpenicilin	k2eAgInSc4d1	phenoxymethylpenicilin
(	(	kIx(	(
<g/>
Pencid	Pencid	k1gInSc4	Pencid
<g/>
,	,	kIx,	,
Penbene	Penben	k1gMnSc5	Penben
<g/>
)	)	kIx)	)
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
I.U.	I.U.	k1gFnSc1	I.U.
2	[number]	k4	2
<g/>
×	×	k?	×
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
