<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
synovitida	synovitida	k1gFnSc1	synovitida
je	být	k5eAaImIp3nS	být
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
vyvolávané	vyvolávaný	k2eAgFnSc2d1	vyvolávaná
bakterií	bakterie	k1gFnSc7	bakterie
Mycoplasma	Mycoplasmum	k1gNnSc2	Mycoplasmum
synoviae	synovia	k1gInSc2	synovia
a	a	k8xC	a
postihující	postihující	k2eAgFnSc4d1	postihující
synoviální	synoviální	k2eAgFnSc4d1	synoviální
blánu	blána	k1gFnSc4	blána
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
subklinická	subklinický	k2eAgFnSc1d1	subklinická
infekce	infekce	k1gFnSc1	infekce
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
anebo	anebo	k8xC	anebo
v	v	k7c4	v
kombinaci	kombinace	k1gFnSc4	kombinace
s	s	k7c7	s
konkurentními	konkurentní	k2eAgFnPc7d1	konkurentní
infekcemi	infekce	k1gFnPc7	infekce
(	(	kIx(	(
<g/>
Newcastleská	Newcastleský	k2eAgFnSc1d1	Newcastleská
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
bronchitida	bronchitida	k1gFnSc1	bronchitida
<g/>
,	,	kIx,	,
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zánět	zánět	k1gInSc4	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Synovitida	Synovitida	k1gFnSc1	Synovitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
synoviální	synoviální	k2eAgFnSc2d1	synoviální
blanky	blanka	k1gFnSc2	blanka
kloubu	kloub	k1gInSc2	kloub
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
Olsonem	Olson	k1gInSc7	Olson
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4-16	[number]	k4	4-16
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
také	také	k9	také
jako	jako	k9	jako
subklinická	subklinický	k2eAgFnSc1d1	subklinická
chronická	chronický	k2eAgFnSc1d1	chronická
infekce	infekce	k1gFnSc1	infekce
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
u	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
Newcastleskou	Newcastleský	k2eAgFnSc7d1	Newcastleská
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
infekční	infekční	k2eAgFnSc7d1	infekční
bronchitidou	bronchitida	k1gFnSc7	bronchitida
jako	jako	k9	jako
zánět	zánět	k1gInSc1	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Krůty	krůta	k1gFnSc2	krůta
postihuje	postihovat	k5eAaImIp3nS	postihovat
onemocnění	onemocnění	k1gNnPc1	onemocnění
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
10-24	[number]	k4	10-24
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
je	být	k5eAaImIp3nS	být
celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
již	již	k6eAd1	již
existují	existovat	k5eAaImIp3nP	existovat
hejna	hejno	k1gNnPc4	hejno
prostá	prostý	k2eAgFnSc1d1	prostá
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
rozmnožovací	rozmnožovací	k2eAgInPc1d1	rozmnožovací
chovy	chov	k1gInPc1	chov
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
synoviae	synoviae	k1gFnSc1	synoviae
(	(	kIx(	(
<g/>
MS	MS	kA	MS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
charakterizaci	charakterizace	k1gFnSc6	charakterizace
typických	typický	k2eAgFnPc6d1	typická
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
buněčné	buněčný	k2eAgFnSc6d1	buněčná
morfologii	morfologie	k1gFnSc6	morfologie
<g/>
,	,	kIx,	,
biochemických	biochemický	k2eAgFnPc6d1	biochemická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
speciálních	speciální	k2eAgInPc6d1	speciální
požadavcích	požadavek	k1gInPc6	požadavek
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
a	a	k8xC	a
sérologii	sérologie	k1gFnSc4	sérologie
<g/>
.	.	kIx.	.
</s>
<s>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
identifikace	identifikace	k1gFnSc1	identifikace
kolonií	kolonie	k1gFnPc2	kolonie
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
imunofluorescenčními	imunofluorescenční	k2eAgFnPc7d1	imunofluorescenční
technikami	technika	k1gFnPc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obarvení	obarvení	k1gNnSc6	obarvení
Giemsou	Giemsa	k1gFnSc7	Giemsa
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
pleomorfní	pleomorfní	k2eAgNnPc4d1	pleomorfní
kokoidní	kokoidní	k2eAgNnPc4d1	kokoidní
tělíska	tělísko	k1gNnPc4	tělísko
velikosti	velikost	k1gFnSc2	velikost
0,2	[number]	k4	0,2
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
kulaté	kulatý	k2eAgInPc1d1	kulatý
s	s	k7c7	s
granulárními	granulární	k2eAgInPc7d1	granulární
ribosomy	ribosom	k1gInPc7	ribosom
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc2	velikost
300-500	[number]	k4	300-500
nm	nm	k?	nm
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obaleny	obalen	k2eAgFnPc1d1	obalena
3	[number]	k4	3
<g/>
vrstevnou	vrstevný	k2eAgFnSc7d1	vrstevná
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
růst	růst	k1gInSc4	růst
přítomnost	přítomnost	k1gFnSc1	přítomnost
nikotinamid	nikotinamida	k1gFnPc2	nikotinamida
adenin	adenin	k1gInSc4	adenin
dinukleotidu	dinukleotida	k1gFnSc4	dinukleotida
(	(	kIx(	(
<g/>
NAD	NAD	kA	NAD
<g/>
)	)	kIx)	)
a	a	k8xC	a
prasečí	prasečí	k2eAgNnSc4d1	prasečí
sérum	sérum	k1gNnSc4	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
°	°	k?	°
<g/>
C.	C.	kA	C.
Primoizolace	Primoizolace	k1gFnSc2	Primoizolace
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
do	do	k7c2	do
bujónu	bujón	k1gInSc2	bujón
nebo	nebo	k8xC	nebo
na	na	k7c4	na
agar	agar	k1gInSc4	agar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikají	vznikat	k5eAaImIp3nP	vznikat
kolonie	kolonie	k1gFnPc1	kolonie
velké	velký	k2eAgFnPc1d1	velká
1-3	[number]	k4	1-3
mm	mm	kA	mm
za	za	k7c4	za
4-5	[number]	k4	4-5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
je	být	k5eAaImIp3nS	být
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
pH	ph	kA	ph
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc4d1	nutná
časté	častý	k2eAgFnPc4d1	častá
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Pomnožuje	pomnožovat	k5eAaImIp3nS	pomnožovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
denních	denní	k2eAgNnPc6d1	denní
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
glukózu	glukóza	k1gFnSc4	glukóza
i	i	k8xC	i
maltózu	maltóza	k1gFnSc4	maltóza
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
laktózu	laktóza	k1gFnSc4	laktóza
<g/>
,	,	kIx,	,
dulcitol	dulcitol	k1gInSc4	dulcitol
<g/>
,	,	kIx,	,
salicin	salicin	k1gInSc4	salicin
ani	ani	k8xC	ani
trehalózu	trehalóza	k1gFnSc4	trehalóza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
fosfatáza	fosfatáza	k1gFnSc1	fosfatáza
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
M.	M.	kA	M.
synoviae	synoviae	k6eAd1	synoviae
hemaglutinují	hemaglutinovat	k5eAaImIp3nP	hemaglutinovat
erytrocyty	erytrocyt	k1gInPc1	erytrocyt
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
redukovat	redukovat	k5eAaBmF	redukovat
sole	sol	k1gInSc5	sol
tetrazolia	tetrazolia	k1gFnSc1	tetrazolia
je	být	k5eAaImIp3nS	být
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
MS	MS	kA	MS
k	k	k7c3	k
dezinfekčním	dezinfekční	k2eAgInPc3d1	dezinfekční
prostředkům	prostředek	k1gInPc3	prostředek
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
<g/>
.	.	kIx.	.
</s>
<s>
Typizace	typizace	k1gFnSc1	typizace
MS	MS	kA	MS
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
sklíčkové	sklíčkový	k2eAgFnSc2d1	sklíčkový
aglutinace	aglutinace	k1gFnSc2	aglutinace
se	s	k7c7	s
sérem	sérum	k1gNnSc7	sérum
<g/>
,	,	kIx,	,
zkumavkovou	zkumavkův	k2eAgFnSc7d1	zkumavkův
aglutinací	aglutinace	k1gFnSc7	aglutinace
<g/>
,	,	kIx,	,
hemaglutinací	hemaglutinace	k1gFnSc7	hemaglutinace
<g/>
,	,	kIx,	,
imunodifuzí	imunodifuze	k1gFnSc7	imunodifuze
a	a	k8xC	a
ELISA	ELISA	kA	ELISA
testem	test	k1gInSc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
informací	informace	k1gFnPc2	informace
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
sérotyp	sérotyp	k1gInSc4	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kmeny	kmen	k1gInPc1	kmen
MS	MS	kA	MS
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
virulencí	virulence	k1gFnSc7	virulence
<g/>
;	;	kIx,	;
pasážováním	pasážování	k1gNnSc7	pasážování
in	in	k?	in
vitro	vitro	k6eAd1	vitro
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
patogenita	patogenita	k1gFnSc1	patogenita
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Izoláty	izolát	k1gInPc1	izolát
ze	z	k7c2	z
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
častěji	často	k6eAd2	často
aerosakulitidu	aerosakulitida	k1gFnSc4	aerosakulitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc4	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
izoláty	izolát	k1gInPc1	izolát
ze	z	k7c2	z
synovie	synovie	k1gFnSc2	synovie
zase	zase	k9	zase
častěji	často	k6eAd2	často
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
synovitidu	synovitida	k1gFnSc4	synovitida
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
synovitida	synovitida	k1gFnSc1	synovitida
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
krůt	krůta	k1gFnPc2	krůta
a	a	k8xC	a
perliček	perlička	k1gFnPc2	perlička
<g/>
;	;	kIx,	;
přirozeně	přirozeně	k6eAd1	přirozeně
infikovány	infikován	k2eAgInPc1d1	infikován
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
kachny	kachna	k1gFnPc4	kachna
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc4	husa
<g/>
,	,	kIx,	,
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgFnPc1d1	japonská
křepelky	křepelka	k1gFnPc1	křepelka
a	a	k8xC	a
orebice	orebice	k1gFnSc1	orebice
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgMnPc1d1	domácí
vrabci	vrabec	k1gMnPc1	vrabec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
experimentálně	experimentálně	k6eAd1	experimentálně
infikováni	infikován	k2eAgMnPc1d1	infikován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
rezistentní	rezistentní	k2eAgFnPc1d1	rezistentní
<g/>
.	.	kIx.	.
</s>
<s>
Králíci	Králík	k1gMnPc1	Králík
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
morčata	morče	k1gNnPc1	morče
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc1	myš
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
jehňata	jehně	k1gNnPc1	jehně
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
refrakterní	refrakterní	k2eAgFnSc3d1	refrakterní
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgInSc1d1	primární
význam	význam	k1gInSc1	význam
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
má	mít	k5eAaImIp3nS	mít
vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
M.	M.	kA	M.
synoviae	synovia	k1gInPc1	synovia
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
chovů	chov	k1gInPc2	chov
(	(	kIx(	(
<g/>
RCH	RCH	kA	RCH
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
snížení	snížení	k1gNnSc4	snížení
oplozenosti	oplozenost	k1gFnSc2	oplozenost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
i	i	k8xC	i
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
embryonální	embryonální	k2eAgInSc1d1	embryonální
úhyn	úhyn	k1gInSc1	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Kuřata	kuře	k1gNnPc1	kuře
pocházející	pocházející	k2eAgNnPc1d1	pocházející
z	z	k7c2	z
takových	takový	k3xDgNnPc2	takový
hejn	hejno	k1gNnPc2	hejno
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
MS	MS	kA	MS
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
RCH	RCH	kA	RCH
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
snášky	snáška	k1gFnSc2	snáška
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
její	její	k3xOp3gInSc1	její
pokles	pokles	k1gInSc1	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
transovariálního	transovariální	k2eAgInSc2d1	transovariální
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
4-6	[number]	k4	4-6
týdnech	týden	k1gInPc6	týden
po	po	k7c4	po
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vylučování	vylučování	k1gNnSc1	vylučování
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Horizontálně	horizontálně	k6eAd1	horizontálně
se	se	k3xPyFc4	se
infekce	infekce	k1gFnPc1	infekce
rychle	rychle	k6eAd1	rychle
šíří	šířit	k5eAaImIp3nP	šířit
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
(	(	kIx(	(
<g/>
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
přes	přes	k7c4	přes
prostředí	prostředí	k1gNnSc4	prostředí
kontaminované	kontaminovaný	k2eAgInPc4d1	kontaminovaný
odměšky	odměšek	k1gInPc4	odměšek
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
latentní	latentní	k2eAgFnPc1d1	latentní
infekce	infekce	k1gFnPc1	infekce
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
s	s	k7c7	s
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
perzistencí	perzistence	k1gFnSc7	perzistence
a	a	k8xC	a
vylučováním	vylučování	k1gNnSc7	vylučování
původce	původce	k1gMnSc2	původce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výskyt	výskyt	k1gInSc1	výskyt
onemocnění	onemocnění	k1gNnSc2	onemocnění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
propuknutí	propuknutí	k1gNnSc3	propuknutí
(	(	kIx(	(
<g/>
exacerbaci	exacerbace	k1gFnSc4	exacerbace
<g/>
)	)	kIx)	)
a	a	k8xC	a
klinické	klinický	k2eAgFnSc3d1	klinická
manifestaci	manifestace	k1gFnSc3	manifestace
synovitidy	synovitis	k1gFnSc2	synovitis
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
interkurentních	interkurentní	k2eAgFnPc2d1	interkurentní
infekcí	infekce	k1gFnPc2	infekce
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
synoviae	synoviae	k1gNnSc1	synoviae
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
(	(	kIx(	(
<g/>
ID	ido	k1gNnPc2	ido
<g/>
)	)	kIx)	)
po	po	k7c6	po
vertikální	vertikální	k2eAgFnSc6d1	vertikální
infekci	infekce	k1gFnSc6	infekce
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
synovitida	synovitida	k1gFnSc1	synovitida
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
již	již	k6eAd1	již
u	u	k7c2	u
6	[number]	k4	6
<g/>
denních	denní	k2eAgNnPc2d1	denní
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kontaktní	kontaktní	k2eAgFnSc6d1	kontaktní
infekci	infekce	k1gFnSc6	infekce
je	být	k5eAaImIp3nS	být
ID	Ida	k1gFnPc2	Ida
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
5-24	[number]	k4	5-24
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
infekční	infekční	k2eAgFnSc2d1	infekční
dávky	dávka	k1gFnSc2	dávka
<g/>
,	,	kIx,	,
virulenci	virulence	k1gFnSc4	virulence
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
při	při	k7c6	při
experimentální	experimentální	k2eAgFnSc6d1	experimentální
infekci	infekce	k1gFnSc6	infekce
také	také	k9	také
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
inokulace	inokulace	k1gFnSc2	inokulace
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
jsou	být	k5eAaImIp3nP	být
detekovatelné	detekovatelný	k2eAgFnPc1d1	detekovatelná
již	již	k6eAd1	již
před	před	k7c7	před
výskytem	výskyt	k1gInSc7	výskyt
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
u	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Promořenost	Promořenost	k1gFnSc1	Promořenost
hejn	hejno	k1gNnPc2	hejno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
100	[number]	k4	100
%	%	kIx~	%
<g/>
;	;	kIx,	;
nemocnost	nemocnost	k1gFnSc1	nemocnost
obvykle	obvykle	k6eAd1	obvykle
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
5-15	[number]	k4	5-15
%	%	kIx~	%
a	a	k8xC	a
mortalita	mortalita	k1gFnSc1	mortalita
mezi	mezi	k7c7	mezi
1-10	[number]	k4	1-10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
bývá	bývat	k5eAaImIp3nS	bývat
morbidita	morbidita	k1gFnSc1	morbidita
kolem	kolem	k7c2	kolem
1-20	[number]	k4	1-20
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mortalita	mortalita	k1gFnSc1	mortalita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
někdy	někdy	k6eAd1	někdy
značná	značný	k2eAgFnSc1d1	značná
(	(	kIx(	(
<g/>
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
<g/>
,	,	kIx,	,
ušlapání	ušlapání	k1gNnSc1	ušlapání
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnostní	hmotnostní	k2eAgInPc1d1	hmotnostní
přírůstky	přírůstek	k1gInPc1	přírůstek
a	a	k8xC	a
konverze	konverze	k1gFnSc1	konverze
krmiva	krmivo	k1gNnSc2	krmivo
u	u	k7c2	u
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgInPc1d2	nižší
<g/>
,	,	kIx,	,
procento	procento	k1gNnSc1	procento
konfiskátů	konfiskát	k1gInPc2	konfiskát
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
vyšší	vysoký	k2eAgFnSc6d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Respirační	respirační	k2eAgInPc1d1	respirační
příznaky	příznak	k1gInPc1	příznak
většinou	většinou	k6eAd1	většinou
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
experimentálně	experimentálně	k6eAd1	experimentálně
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
až	až	k9	až
u	u	k7c2	u
90-100	[number]	k4	90-100
%	%	kIx~	%
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
inokulace	inokulace	k1gFnSc2	inokulace
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
dávky	dávka	k1gFnSc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgInSc1d1	akutní
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
zpravidla	zpravidla	k6eAd1	zpravidla
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
subklinickou	subklinický	k2eAgFnSc4d1	subklinická
<g/>
,	,	kIx,	,
chronickou	chronický	k2eAgFnSc4d1	chronická
infekci	infekce	k1gFnSc4	infekce
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Kuřata	kuře	k1gNnPc1	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
příznakem	příznak	k1gInSc7	příznak
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
postiženém	postižený	k2eAgNnSc6d1	postižené
synovitidou	synovitida	k1gFnSc7	synovitida
je	být	k5eAaImIp3nS	být
bledý	bledý	k2eAgInSc1d1	bledý
hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
,	,	kIx,	,
slabost	slabost	k1gFnSc1	slabost
a	a	k8xC	a
zpomalení	zpomalení	k1gNnSc1	zpomalení
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupující	postupující	k2eAgFnSc7d1	postupující
nemocí	nemoc	k1gFnSc7	nemoc
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
načepýřené	načepýřený	k2eAgNnSc1d1	načepýřené
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
svraštělý	svraštělý	k2eAgInSc1d1	svraštělý
anebo	anebo	k8xC	anebo
namodralý	namodralý	k2eAgInSc1d1	namodralý
hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
,	,	kIx,	,
zduření	zduření	k1gNnSc1	zduření
kloubů	kloub	k1gInPc2	kloub
(	(	kIx(	(
<g/>
primárně	primárně	k6eAd1	primárně
hleznový	hleznový	k2eAgInSc1d1	hleznový
kloub	kloub	k1gInSc1	kloub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prstních	prstní	k2eAgInPc2d1	prstní
polštářků	polštářek	k1gInPc2	polštářek
a	a	k8xC	a
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
prsní	prsní	k2eAgFnSc2d1	prsní
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
otlaky	otlak	k1gInPc1	otlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
kloubů	kloub	k1gInPc2	kloub
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pohybové	pohybový	k2eAgFnSc2d1	pohybová
potíže	potíž	k1gFnSc2	potíž
<g/>
,	,	kIx,	,
kulhání	kulhání	k1gNnSc2	kulhání
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
generalizovaná	generalizovaný	k2eAgFnSc1d1	generalizovaná
infekce	infekce	k1gFnSc1	infekce
bez	bez	k7c2	bez
postižení	postižení	k1gNnPc2	postižení
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
je	být	k5eAaImIp3nS	být
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
dehydratovaná	dehydratovaný	k2eAgFnSc1d1	dehydratovaná
a	a	k8xC	a
vyhublá	vyhublý	k2eAgFnSc1d1	vyhublá
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
i	i	k8xC	i
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
nazelenalý	nazelenalý	k2eAgInSc4d1	nazelenalý
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgNnSc4d1	obsahující
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
urátů	urát	k1gInPc2	urát
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
až	až	k9	až
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Krůťata	krůtě	k1gNnPc1	krůtě
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
obdobné	obdobný	k2eAgInPc1d1	obdobný
příznaky	příznak	k1gInPc1	příznak
jako	jako	k8xS	jako
kuřata	kuře	k1gNnPc1	kuře
<g/>
;	;	kIx,	;
dominují	dominovat	k5eAaImIp3nP	dominovat
tělesná	tělesný	k2eAgFnSc1d1	tělesná
slabost	slabost	k1gFnSc1	slabost
<g/>
,	,	kIx,	,
zduření	zduření	k1gNnSc1	zduření
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
zvětšení	zvětšení	k1gNnSc1	zvětšení
sternální	sternální	k2eAgFnSc2d1	sternální
burzy	burza	k1gFnSc2	burza
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
růstu	růst	k1gInSc2	růst
i	i	k8xC	i
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Respirační	respirační	k2eAgInPc1d1	respirační
příznaky	příznak	k1gInPc1	příznak
většinou	většinou	k6eAd1	většinou
chybí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Simultánní	simultánní	k2eAgFnSc1d1	simultánní
infekce	infekce	k1gFnSc1	infekce
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gNnSc1	gallisepticum
a	a	k8xC	a
M.	M.	kA	M.
synoviae	synoviae	k6eAd1	synoviae
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
průběh	průběh	k1gInSc4	průběh
sinusitidy	sinusitida	k1gFnSc2	sinusitida
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
akutní	akutní	k2eAgFnSc6d1	akutní
fázi	fáze	k1gFnSc6	fáze
nemoci	moct	k5eNaImF	moct
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
šedozelenými	šedozelený	k2eAgFnPc7d1	šedozelená
okrskovými	okrskový	k2eAgFnPc7d1	okrsková
nekrózami	nekróza	k1gFnPc7	nekróza
ve	v	k7c6	v
zduřelé	zduřelý	k2eAgFnSc6d1	zduřelá
slezině	slezina	k1gFnSc6	slezina
<g/>
,	,	kIx,	,
ledvinách	ledvina	k1gFnPc6	ledvina
a	a	k8xC	a
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
podkožním	podkožní	k2eAgInSc7d1	podkožní
edémem	edém	k1gInSc7	edém
<g/>
,	,	kIx,	,
šedožlutým	šedožlutý	k2eAgInSc7d1	šedožlutý
hlenovitým	hlenovitý	k2eAgInSc7d1	hlenovitý
až	až	k8xS	až
zapáchajícím	zapáchající	k2eAgInSc7d1	zapáchající
kaseózním	kaseózní	k2eAgInSc7d1	kaseózní
(	(	kIx(	(
<g/>
sýrovitým	sýrovitý	k2eAgInSc7d1	sýrovitý
<g/>
)	)	kIx)	)
exsudátem	exsudát	k1gInSc7	exsudát
ve	v	k7c6	v
sternální	sternální	k2eAgFnSc6d1	sternální
burze	burza	k1gFnSc6	burza
<g/>
,	,	kIx,	,
kloubech	kloub	k1gInPc6	kloub
hlezna	hlezno	k1gNnSc2	hlezno
<g/>
,	,	kIx,	,
pochvách	pochva	k1gFnPc6	pochva
šlachových	šlachový	k2eAgInPc6d1	šlachový
a	a	k8xC	a
prstních	prstní	k2eAgInPc6d1	prstní
polštářcích	polštářek	k1gInPc6	polštářek
anebo	anebo	k8xC	anebo
fibrinózním	fibrinózní	k2eAgInSc7d1	fibrinózní
zánětem	zánět	k1gInSc7	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Imunita	imunita	k1gFnSc1	imunita
vzniká	vznikat	k5eAaImIp3nS	vznikat
po	po	k7c6	po
přirozené	přirozený	k2eAgFnSc6d1	přirozená
i	i	k8xC	i
umělé	umělý	k2eAgFnSc3d1	umělá
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Aglutinační	aglutinační	k2eAgFnPc1d1	aglutinační
protilátky	protilátka	k1gFnPc1	protilátka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
za	za	k7c4	za
2-4	[number]	k4	2-4
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Krůty	krůta	k1gFnPc1	krůta
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
respirační	respirační	k2eAgFnSc7d1	respirační
cestou	cesta	k1gFnSc7	cesta
reagují	reagovat	k5eAaBmIp3nP	reagovat
nižší	nízký	k2eAgFnSc7d2	nižší
produkcí	produkce	k1gFnSc7	produkce
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výsledky	výsledek	k1gInPc1	výsledek
aglutinace	aglutinace	k1gFnSc2	aglutinace
nemusí	muset	k5eNaImIp3nP	muset
vždy	vždy	k6eAd1	vždy
odpovídat	odpovídat	k5eAaImF	odpovídat
skutečnému	skutečný	k2eAgInSc3d1	skutečný
stavu	stav	k1gInSc3	stav
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
klinického	klinický	k2eAgNnSc2d1	klinické
<g/>
,	,	kIx,	,
pitevního	pitevní	k2eAgNnSc2d1	pitevní
a	a	k8xC	a
histologického	histologický	k2eAgNnSc2d1	histologické
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
,	,	kIx,	,
potvrzeného	potvrzený	k2eAgInSc2d1	potvrzený
izolací	izolace	k1gFnSc7	izolace
a	a	k8xC	a
identifikací	identifikace	k1gFnSc7	identifikace
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Sérologické	sérologický	k2eAgNnSc1d1	sérologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
průkazu	průkaz	k1gInSc6	průkaz
aglutinačních	aglutinační	k2eAgInPc2d1	aglutinační
nebo	nebo	k8xC	nebo
hemaglutinačně	hemaglutinačně	k6eAd1	hemaglutinačně
inhibičních	inhibiční	k2eAgFnPc2d1	inhibiční
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
odlišit	odlišit	k5eAaPmF	odlišit
reovirovou	reovirový	k2eAgFnSc4d1	reovirový
artritidu	artritida	k1gFnSc4	artritida
<g/>
,	,	kIx,	,
koliseptikémii	koliseptikémie	k1gFnSc4	koliseptikémie
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnPc4	infekce
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc4	gallisepticum
a	a	k8xC	a
M.	M.	kA	M.
meleagridis	meleagridis	k1gInSc4	meleagridis
<g/>
,	,	kIx,	,
choleru	cholera	k1gFnSc4	cholera
<g/>
,	,	kIx,	,
salmonelózy	salmonelóza	k1gFnPc4	salmonelóza
<g/>
,	,	kIx,	,
stafylokokózu	stafylokokóza	k1gFnSc4	stafylokokóza
a	a	k8xC	a
streptokokózu	streptokokóza	k1gFnSc4	streptokokóza
<g/>
.	.	kIx.	.
</s>
<s>
Léčebná	léčebný	k2eAgNnPc1d1	léčebné
i	i	k8xC	i
preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
jako	jako	k9	jako
při	při	k7c6	při
infekci	infekce	k1gFnSc6	infekce
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
MG	mg	kA	mg
je	být	k5eAaImIp3nS	být
M.	M.	kA	M.
synoviae	synoviae	k1gFnSc1	synoviae
rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
na	na	k7c4	na
erytromycin	erytromycin	k1gInSc4	erytromycin
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
snižování	snižování	k1gNnSc1	snižování
vnímavosti	vnímavost	k1gFnSc2	vnímavost
k	k	k7c3	k
chlortetracyklinu	chlortetracyklin	k1gInSc3	chlortetracyklin
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
staršími	starý	k2eAgInPc7d2	starší
izoláty	izolát	k1gInPc7	izolát
MS.	MS.	k1gFnSc2	MS.
Medikamentózní	medikamentózní	k2eAgFnSc1d1	medikamentózní
léčba	léčba	k1gFnSc1	léčba
snižuje	snižovat	k5eAaImIp3nS	snižovat
výskyt	výskyt	k1gInSc1	výskyt
zánětu	zánět	k1gInSc2	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
i	i	k8xC	i
synovitidy	synovitis	k1gFnSc2	synovitis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
již	již	k6eAd1	již
existující	existující	k2eAgFnPc4d1	existující
změny	změna	k1gFnPc4	změna
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
Antibiotika	antibiotikum	k1gNnPc1	antibiotikum
také	také	k9	také
neeliminují	eliminovat	k5eNaBmIp3nP	eliminovat
M.	M.	kA	M.
synoviae	synoviae	k6eAd1	synoviae
z	z	k7c2	z
hejna	hejno	k1gNnSc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
používání	používání	k1gNnSc4	používání
komerčně	komerčně	k6eAd1	komerčně
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
inaktivované	inaktivovaný	k2eAgFnPc1d1	inaktivovaná
vakcíny	vakcína	k1gFnPc1	vakcína
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
