<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
synovitida	synovitida	k1gFnSc1	synovitida
je	být	k5eAaImIp3nS	být
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
vyvolávané	vyvolávaný	k2eAgFnSc2d1	vyvolávaná
bakterií	bakterie	k1gFnSc7	bakterie
Mycoplasma	Mycoplasmum	k1gNnSc2	Mycoplasmum
synoviae	synovia	k1gInSc2	synovia
a	a	k8xC	a
postihující	postihující	k2eAgFnSc4d1	postihující
synoviální	synoviální	k2eAgFnSc4d1	synoviální
blánu	blána	k1gFnSc4	blána
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
<g/>
.	.	kIx.	.
</s>
