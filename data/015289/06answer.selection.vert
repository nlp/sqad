<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
let	léto	k1gNnPc2
Gemini	Gemin	k2eAgMnPc1d1
9	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
V	V	kA
katalogu	katalog	k1gInSc2
COSPAR	COSPAR	kA
získal	získat	k5eAaPmAgMnS
označení	označení	k1gNnSc4
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
47	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
11	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
do	do	k7c2
vesmíru	vesmír	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
USA	USA	kA
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
planety	planeta	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>