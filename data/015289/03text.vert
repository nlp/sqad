<s>
Gemini	Gemin	k1gMnPc1
9	#num#	k4
</s>
<s>
Gemini	Gemin	k1gMnPc1
9A	9A	k4
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
lodi	loď	k1gFnSc6
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
pilotovaná	pilotovaný	k2eAgFnSc1d1
kosmická	kosmický	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
COSPAR	COSPAR	kA
</s>
<s>
1966-047A	1966-047A	k4
</s>
<s>
Katalogové	katalogový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
2191	#num#	k4
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
McDonnell	McDonnell	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
NASA	NASA	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
3750	#num#	k4
kg	kg	kA
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
letu	let	k1gInSc6
</s>
<s>
Členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
</s>
<s>
2	#num#	k4
</s>
<s>
Datum	datum	k1gNnSc1
startu	start	k1gInSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
UTC	UTC	kA
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gMnSc7
Air	Air	k1gFnSc2
Force	force	k1gFnPc2
Station	station	k1gInSc1
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
rampa	rampa	k1gFnSc1
</s>
<s>
LC-19	LC-19	k4
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Titan	titan	k1gInSc1
2	#num#	k4
GLV	GLV	kA
</s>
<s>
Délka	délka	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
3	#num#	k4
dny	den	k1gInPc4
<g/>
,	,	kIx,
0	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
20	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
50	#num#	k4
sekund	sekunda	k1gFnPc2
</s>
<s>
Počet	počet	k1gInSc1
výstupů	výstup	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
1	#num#	k4
</s>
<s>
Délka	délka	k1gFnSc1
výstupů	výstup	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
2	#num#	k4
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
7	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
přistání	přistání	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
UTC	UTC	kA
</s>
<s>
Místo	místo	k7c2
přistání	přistání	k1gNnSc2
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
(	(	kIx(
<g/>
27,87	27,87	k4
<g/>
°	°	k?
s.	s.	k?
<g/>
š.	š.	k?
<g/>
,	,	kIx,
75,00	75,00	k4
<g/>
°	°	k?
z.d.	z.d.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USS	USS	kA
Wasp	Wasp	k1gInSc1
</s>
<s>
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
</s>
<s>
typ	typ	k1gInSc1
dráhyLEO	dráhyLEO	k?
</s>
<s>
Epocha	epocha	k1gFnSc1
<g/>
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
11	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Apogeum	apogeum	k1gNnSc1
<g/>
302,6	302,6	k4
km	km	kA
</s>
<s>
Perigeum	perigeum	k1gNnSc1
<g/>
290,2	290,2	k4
km	km	kA
</s>
<s>
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
<g/>
28,89	28,89	k4
<g/>
°	°	k?
</s>
<s>
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
<g/>
90,51	90,51	k4
minut	minuta	k1gFnPc2
</s>
<s>
Počet	počet	k1gInSc1
oběhů	oběh	k1gInPc2
Země	zem	k1gFnSc2
<g/>
47	#num#	k4
</s>
<s>
Fotografie	fotografia	k1gFnPc1
posádky	posádka	k1gFnSc2
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Stafford	Stafford	k1gMnSc1
<g/>
,	,	kIx,
Cernan	Cernan	k1gMnSc1
</s>
<s>
Navigace	navigace	k1gFnSc1
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1
</s>
<s>
Následující	následující	k2eAgInSc1d1
</s>
<s>
Gemini	Gemin	k1gMnPc1
8	#num#	k4
</s>
<s>
Gemini	Gemin	k1gMnPc1
10	#num#	k4
</s>
<s>
Gemini	Gemin	k1gMnPc1
9	#num#	k4
byl	být	k5eAaImAgInS
pilotovaný	pilotovaný	k2eAgInSc1d1
kosmický	kosmický	k2eAgInSc1d1
let	let	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
amerického	americký	k2eAgInSc2d1
programu	program	k1gInSc2
Gemini	Gemin	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
ve	v	k7c6
dnech	den	k1gInPc6
3	#num#	k4
<g/>
.	.	kIx.
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
let	léto	k1gNnPc2
Gemini	Gemin	k2eAgMnPc1d1
9	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
V	V	kA
katalogu	katalog	k1gInSc2
COSPAR	COSPAR	kA
získal	získat	k5eAaPmAgMnS
označení	označení	k1gNnSc4
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
47	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
11	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
do	do	k7c2
vesmíru	vesmír	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
USA	USA	kA
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
planety	planeta	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Stafford	Stafford	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velící	velící	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
</s>
<s>
Eugene	Eugen	k1gMnSc5
Cernan	Cernan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
</s>
<s>
V	v	k7c6
závorkách	závorka	k1gFnPc6
je	být	k5eAaImIp3nS
uvedený	uvedený	k2eAgInSc1d1
dosavadní	dosavadní	k2eAgInSc1d1
počet	počet	k1gInSc1
letů	let	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
včetně	včetně	k7c2
této	tento	k3xDgFnSc2
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
vybraná	vybraný	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
Elliott	Elliott	k1gMnSc1
See	See	k1gMnSc1
a	a	k8xC
Charles	Charles	k1gMnSc1
Bassett	Bassett	k1gMnSc1
zahynula	zahynout	k5eAaPmAgFnS
při	při	k7c6
leteckém	letecký	k2eAgNnSc6d1
neštěstí	neštěstí	k1gNnSc6
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
před	před	k7c7
startem	start	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
letoun	letoun	k1gMnSc1
T-38	T-38	k1gMnSc1
narazil	narazit	k5eAaPmAgMnS
při	při	k7c6
přistávacím	přistávací	k2eAgInSc6d1
manévru	manévr	k1gInSc6
do	do	k7c2
budovy	budova	k1gFnSc2
společnosti	společnost	k1gFnSc2
McDonnell	McDonnella	k1gFnPc2
v	v	k7c6
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
měli	mít	k5eAaImAgMnP
převzít	převzít	k5eAaPmF
dokončovanou	dokončovaný	k2eAgFnSc4d1
kabinu	kabina	k1gFnSc4
Gemini	Gemin	k1gMnPc1
9	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záložní	záložní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
</s>
<s>
Jim	on	k3xPp3gMnPc3
Lovell	Lovell	k1gMnSc1
<g/>
,	,	kIx,
velící	velící	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
Cernan	Cernan	k1gMnSc1
</s>
<s>
Začátek	začátek	k1gInSc1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
UTC	UTC	kA
</s>
<s>
Konec	konec	k1gInSc1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
UTC	UTC	kA
</s>
<s>
Trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
2	#num#	k4
hodiny	hodina	k1gFnSc2
7	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Průběh	průběh	k1gInSc1
letu	let	k1gInSc2
</s>
<s>
Přistání	přistání	k1gNnSc4
lodě	loď	k1gFnSc2
Gemini	Gemin	k2eAgMnPc5d1
9	#num#	k4
</s>
<s>
Start	start	k1gInSc1
lodě	loď	k1gFnSc2
byl	být	k5eAaImAgInS
po	po	k7c6
poledni	poledne	k1gNnSc6
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
na	na	k7c6
Floridě	Florida	k1gFnSc6
z	z	k7c2
mysu	mys	k1gInSc2
Canaveral	Canaveral	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posádce	posádka	k1gFnSc6
Gemini	Gemin	k1gMnPc1
9	#num#	k4
byli	být	k5eAaImAgMnP
velitel	velitel	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Stafford	Stafford	k1gMnSc1
a	a	k8xC
pilot	pilot	k1gMnSc1
Eugene	Eugen	k1gInSc5
Cernan	Cernany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
poruchu	porucha	k1gFnSc4
na	na	k7c6
pasivním	pasivní	k2eAgNnSc6d1
cílovém	cílový	k2eAgNnSc6d1
tělesu	těleso	k1gNnSc6
Agena	Agena	k1gFnSc1
(	(	kIx(
<g/>
nedostala	dostat	k5eNaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
orbitu	orbita	k1gFnSc4
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
na	na	k7c6
náhradním	náhradní	k2eAgInSc6d1
cílovém	cílový	k2eAgInSc6d1
objektu	objekt	k1gInSc6
ATDA	ATDA	kA
(	(	kIx(
<g/>
proto	proto	k8xC
9	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
provedeny	provést	k5eAaPmNgFnP
celkem	celkem	k6eAd1
3	#num#	k4
různé	různý	k2eAgInPc4d1
přibližovací	přibližovací	k2eAgInPc4d1
manévry	manévr	k1gInPc4
bez	bez	k7c2
konečného	konečný	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byl	být	k5eAaImAgInS
neuvolněný	uvolněný	k2eNgInSc1d1
kryt	kryt	k1gInSc1
spojovacího	spojovací	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
letu	let	k1gInSc2
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
Cernanem	Cernan	k1gInSc7
dvouhodinový	dvouhodinový	k2eAgInSc4d1
namáhavý	namáhavý	k2eAgInSc4d1
výstup	výstup	k1gInSc4
do	do	k7c2
kosmického	kosmický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
na	na	k7c6
sedmimetrovém	sedmimetrový	k2eAgNnSc6d1
laně	lano	k1gNnSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
EVA	Eva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gemini	Gemin	k2eAgMnPc5d1
přistála	přistát	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c6
padácích	padák	k1gInPc6
po	po	k7c6
třídenním	třídenní	k2eAgInSc6d1
letu	let	k1gInSc6
na	na	k7c6
vlnách	vlna	k1gFnPc6
oceánu	oceán	k1gInSc2
u	u	k7c2
Bahamských	bahamský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
3	#num#	k4
km	km	kA
od	od	k7c2
čekající	čekající	k2eAgFnSc2d1
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
USS	USS	kA
Wasp	Wasp	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
kosmonauty	kosmonaut	k1gMnPc4
nalodila	nalodit	k5eAaPmAgFnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
palubu	paluba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Launch	Launch	k1gInSc1
<g/>
/	/	kIx~
<g/>
Orbital	orbital	k1gInSc1
information	information	k1gInSc1
for	forum	k1gNnPc2
Gemini	Gemin	k2eAgMnPc1d1
9A	9A	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2011-4-26	2011-4-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
LÁLA	Lála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Americké	americký	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
s.	s.	k?
335	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HALOUSEK	HALOUSEK	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
PŘIBYL	Přibyl	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TRAGICKÝ	tragický	k2eAgInSc1d1
LET	let	k1gInSc1
POSÁDKY	posádka	k1gFnSc2
GEMINI	GEMINI	kA
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
KOSMOS	kosmos	k1gInSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Eugene	Eugen	k1gInSc5
Andrew	Andrew	k1gMnSc1
Cernan	Cernan	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
284	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gemini	Gemin	k1gMnPc1
9	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Gemini	Gemin	k2eAgMnPc1d1
9A	9A	k4
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
encyklopedii	encyklopedie	k1gFnSc6
kosmonautiky	kosmonautika	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Popis	popis	k1gInSc1
letu	let	k1gInSc2
na	na	k7c6
kosmo	kosmo	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Program	program	k1gInSc1
Gemini	Gemin	k1gMnPc5
</s>
<s>
Gemini	Gemin	k1gMnPc1
1	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
2	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
3	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
4	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
5	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
6A	6A	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
7	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
8	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
9	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
10	#num#	k4
•	•	k?
Gemini	Gemin	k2eAgMnPc1d1
11	#num#	k4
•	•	k?
Gemini	Gemin	k1gMnPc5
12	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
