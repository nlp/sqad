<s>
Peking	Peking	k1gInSc1	Peking
(	(	kIx(	(
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
北	北	k?	北
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Běijī	Běijī	k1gMnSc1	Běijī
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Pej-ťing	Pej-ťing	k1gInSc1	Pej-ťing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
centrálně	centrálně	k6eAd1	centrálně
spravované	spravovaný	k2eAgNnSc1d1	spravované
město	město	k1gNnSc1	město
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincie	provincie	k1gFnSc2	provincie
podléhá	podléhat	k5eAaImIp3nS	podléhat
přímo	přímo	k6eAd1	přímo
ústřední	ústřední	k2eAgInSc1d1	ústřední
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
staletí	staletí	k1gNnSc4	staletí
správním	správní	k2eAgNnSc7d1	správní
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
proslulé	proslulý	k2eAgNnSc4d1	proslulé
náměstí	náměstí	k1gNnSc4	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
či	či	k8xC	či
Zakázané	zakázaný	k2eAgNnSc4d1	zakázané
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
dříve	dříve	k6eAd2	dříve
vládli	vládnout	k5eAaImAgMnP	vládnout
čínští	čínský	k2eAgMnPc1d1	čínský
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pekingu	Peking	k1gInSc2	Peking
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
17	[number]	k4	17
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
už	už	k9	už
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Peking	Peking	k1gInSc1	Peking
hostil	hostit	k5eAaImAgInS	hostit
XXIX	XXIX	kA	XXIX
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
pojmenování	pojmenování	k1gNnSc1	pojmenování
města	město	k1gNnSc2	město
Peking	Peking	k1gInSc1	Peking
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
v	v	k7c6	v
jihočínských	jihočínský	k2eAgInPc6d1	jihočínský
dialektech	dialekt	k1gInPc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
jazyku	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
výslovnosti	výslovnost	k1gFnSc6	výslovnost
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Pekingu	Peking	k1gInSc6	Peking
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
vyslovován	vyslovován	k2eAgInSc1d1	vyslovován
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k8xC	jako
Pej-ťing	Pej-ťing	k1gInSc1	Pej-ťing
(	(	kIx(	(
<g/>
北	北	k?	北
pej	pej	k?	pej
=	=	kIx~	=
sever	sever	k1gInSc1	sever
a	a	k8xC	a
京	京	k?	京
ťing	ťing	k1gInSc1	ťing
=	=	kIx~	=
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Severní	severní	k2eAgNnSc4d1	severní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
přepis	přepis	k1gInSc1	přepis
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
je	být	k5eAaImIp3nS	být
Beijing	Beijing	k1gInSc1	Beijing
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
opevněné	opevněný	k2eAgNnSc1d1	opevněné
město	město	k1gNnSc1	město
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
byl	být	k5eAaImAgInS	být
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
Ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnPc1d1	existující
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Ji	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
současného	současný	k2eAgNnSc2d1	současné
Pekingského	pekingský	k2eAgNnSc2d1	pekingské
západního	západní	k2eAgNnSc2d1	západní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Číny	Čína	k1gFnSc2	Čína
prošlo	projít	k5eAaPmAgNnS	projít
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
historií	historie	k1gFnSc7	historie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
počátky	počátek	k1gInPc4	počátek
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
již	již	k9	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vydrancováno	vydrancovat	k5eAaPmNgNnS	vydrancovat
vojsky	vojsky	k6eAd1	vojsky
Čingischána	Čingischán	k1gMnSc2	Čingischán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
dvou	dva	k4xCgNnPc2	dva
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vzpamatovávalo	vzpamatovávat	k5eAaImAgNnS	vzpamatovávat
z	z	k7c2	z
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
městem	město	k1gNnSc7	město
celé	celý	k2eAgFnSc2d1	celá
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
zejména	zejména	k9	zejména
za	za	k7c2	za
posledního	poslední	k2eAgInSc2d1	poslední
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
významně	významně	k6eAd1	významně
přetvořili	přetvořit	k5eAaPmAgMnP	přetvořit
město	město	k1gNnSc4	město
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
svému	svůj	k3xOyFgInSc3	svůj
<g/>
,	,	kIx,	,
strhli	strhnout	k5eAaPmAgMnP	strhnout
mnoho	mnoho	k4c4	mnoho
oslavných	oslavný	k2eAgInPc2d1	oslavný
oblouků	oblouk	k1gInPc2	oblouk
<g/>
,	,	kIx,	,
zbourali	zbourat	k5eAaPmAgMnP	zbourat
vnější	vnější	k2eAgFnPc4d1	vnější
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
široké	široký	k2eAgInPc4d1	široký
bulváry	bulvár	k1gInPc4	bulvár
a	a	k8xC	a
moderní	moderní	k2eAgNnSc4d1	moderní
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
tradiční	tradiční	k2eAgFnSc1d1	tradiční
zástavba	zástavba	k1gFnSc1	zástavba
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zlikvidována	zlikvidovat	k5eAaPmNgFnS	zlikvidovat
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
trendu	trend	k1gInSc6	trend
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Přelomové	přelomový	k2eAgNnSc1d1	přelomové
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
Peking	Peking	k1gInSc1	Peking
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
přebudován	přebudován	k2eAgMnSc1d1	přebudován
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc4d1	nová
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
trasy	trasa	k1gFnPc4	trasa
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
sportoviště	sportoviště	k1gNnSc2	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
obklopen	obklopit	k5eAaPmNgMnS	obklopit
provincií	provincie	k1gFnSc7	provincie
Che-pej	Cheej	k1gMnPc2	Che-pej
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
straně	strana	k1gFnSc6	strana
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Tchien-ťin	Tchien-ťina	k1gFnPc2	Tchien-ťina
<g/>
,	,	kIx,	,
dalším	další	k2eAgFnPc3d1	další
ze	z	k7c2	z
čtveřice	čtveřice	k1gFnSc2	čtveřice
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgMnPc2d1	postavený
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
provinciím	provincie	k1gFnPc3	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
krajině	krajina	k1gFnSc6	krajina
150	[number]	k4	150
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Pochajské	Pochajský	k2eAgFnSc2d1	Pochajský
zátoky	zátoka	k1gFnSc2	zátoka
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
63	[number]	k4	63
m.	m.	k?	m.
Jestliže	jestliže	k8xS	jestliže
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
městské	městský	k2eAgFnSc2d1	městská
provincie	provincie	k1gFnSc2	provincie
je	být	k5eAaImIp3nS	být
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
místem	místo	k1gNnSc7	místo
44	[number]	k4	44
m	m	kA	m
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
a	a	k8xC	a
severním	severní	k2eAgNnSc6d1	severní
pomezí	pomezí	k1gNnSc6	pomezí
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
hory	hora	k1gFnPc1	hora
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
v	v	k7c4	v
Si-šan	Si-šan	k1gInSc4	Si-šan
-	-	kIx~	-
"	"	kIx"	"
<g/>
Západních	západní	k2eAgFnPc6d1	západní
horách	hora	k1gFnPc6	hora
<g/>
"	"	kIx"	"
-	-	kIx~	-
vrcholem	vrchol	k1gInSc7	vrchol
Min-šang	Min-šang	k1gInSc4	Min-šang
2	[number]	k4	2
303	[number]	k4	303
m.	m.	k?	m.
Vysoké	vysoký	k2eAgInPc1d1	vysoký
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgFnPc1d1	další
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Chaj-tchuo-šan	Chajchuo-šan	k1gMnSc1	Chaj-tchuo-šan
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
má	mít	k5eAaImIp3nS	mít
2241	[number]	k4	2241
m	m	kA	m
a	a	k8xC	a
Wu-ling-šan	Wuing-šan	k1gMnSc1	Wu-ling-šan
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
2116	[number]	k4	2116
m.	m.	k?	m.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
významných	významný	k2eAgNnPc2d1	významné
čínských	čínský	k2eAgNnPc2d1	čínské
měst	město	k1gNnPc2	město
Peking	Peking	k1gInSc1	Peking
neleží	ležet	k5eNaImIp3nS	ležet
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
velkém	velký	k2eAgInSc6d1	velký
vodním	vodní	k2eAgInSc6d1	vodní
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
území	území	k1gNnSc6	území
provincie	provincie	k1gFnSc2	provincie
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
Jung-ting	Junging	k1gInSc4	Jung-ting
a	a	k8xC	a
zejména	zejména	k9	zejména
Čchao-paj	Čchaoaj	k1gFnSc1	Čchao-paj
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
90	[number]	k4	90
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
vybudována	vybudován	k2eAgFnSc1d1	vybudována
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Min-čün	Min-čün	k1gInSc4	Min-čün
<g/>
,	,	kIx,	,
zásobující	zásobující	k2eAgInSc4d1	zásobující
Peking	Peking	k1gInSc4	Peking
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
14	[number]	k4	14
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
městské	městský	k2eAgInPc1d1	městský
okresy	okres	k1gInPc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
město	město	k1gNnSc4	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
obvody	obvod	k1gInPc4	obvod
Tung-čcheng	Tung-čchenga	k1gFnPc2	Tung-čchenga
a	a	k8xC	a
Si-čcheng	Si-čchenga	k1gFnPc2	Si-čchenga
<g/>
,	,	kIx,	,
k	k	k7c3	k
širšímu	široký	k2eAgNnSc3d2	širší
centru	centrum	k1gNnSc3	centrum
patří	patřit	k5eAaImIp3nS	patřit
obvody	obvod	k1gInPc4	obvod
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-ťing-šan	-ťing-šan	k1gMnSc1	-ťing-šan
<g/>
,	,	kIx,	,
Chaj-tien	Chajien	k1gInSc1	Chaj-tien
<g/>
,	,	kIx,	,
Čchao-jang	Čchaoang	k1gInSc1	Čchao-jang
a	a	k8xC	a
Feng-tchaj	Fengchaj	k1gInSc1	Feng-tchaj
<g/>
.	.	kIx.	.
</s>
<s>
Periferii	periferie	k1gFnSc4	periferie
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
obvody	obvod	k1gInPc1	obvod
Men-tchou-kou	Menchoua	k1gFnSc7	Men-tchou-ka
<g/>
,	,	kIx,	,
Fang-šan	Fang-šan	k1gInSc1	Fang-šan
<g/>
,	,	kIx,	,
Tchung-čou	Tchung-čá	k1gFnSc4	Tchung-čá
<g/>
,	,	kIx,	,
Šun-i	Šun	k1gFnSc4	Šun-e
<g/>
,	,	kIx,	,
Čchang-pching	Čchangching	k1gInSc4	Čchang-pching
a	a	k8xC	a
Ta-sing	Taing	k1gInSc4	Ta-sing
a	a	k8xC	a
k	k	k7c3	k
Pekingu	Peking	k1gInSc3	Peking
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nP	patřit
venkovské	venkovský	k2eAgInPc1d1	venkovský
obvody	obvod	k1gInPc1	obvod
Chuaj-žou	Chuaj-žou	k1gFnSc2	Chuaj-žou
a	a	k8xC	a
Pching-ku	Pching	k1gInSc2	Pching-k
a	a	k8xC	a
okresy	okres	k1gInPc1	okres
Jen-čching	Jen-čching	k1gInSc1	Jen-čching
a	a	k8xC	a
Mi-jün	Miün	k1gInSc1	Mi-jün
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nevelkou	velký	k2eNgFnSc4d1	nevelká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
s	s	k7c7	s
mrazivou	mrazivý	k2eAgFnSc7d1	mrazivá
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
anticyklóna	anticyklóna	k1gFnSc1	anticyklóna
<g/>
,	,	kIx,	,
suchým	suchý	k2eAgInSc7d1	suchý
a	a	k8xC	a
prašným	prašný	k2eAgInSc7d1	prašný
jarem	jar	k1gInSc7	jar
<g/>
,	,	kIx,	,
horkým	horký	k2eAgNnSc7d1	horké
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
létem	léto	k1gNnSc7	léto
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
jižního	jižní	k2eAgInSc2d1	jižní
monzunu	monzun	k1gInSc2	monzun
a	a	k8xC	a
suchým	suchý	k2eAgInSc7d1	suchý
mírným	mírný	k2eAgInSc7d1	mírný
podzimem	podzim	k1gInSc7	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
průměr	průměr	k1gInSc1	průměr
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
26	[number]	k4	26
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
průměrného	průměrný	k2eAgInSc2d1	průměrný
úhrnu	úhrn	k1gInSc2	úhrn
srážek	srážka	k1gFnPc2	srážka
620	[number]	k4	620
mm	mm	kA	mm
spadne	spadnout	k5eAaPmIp3nS	spadnout
většina	většina	k1gFnSc1	většina
během	během	k7c2	během
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc7d3	nejvhodnější
dobou	doba	k1gFnSc7	doba
k	k	k7c3	k
návštěvě	návštěva	k1gFnSc3	návštěva
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
září	září	k1gNnSc4	září
a	a	k8xC	a
říjen	říjen	k1gInSc4	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Vážným	vážný	k2eAgInSc7d1	vážný
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
plošnou	plošný	k2eAgFnSc7d1	plošná
asanací	asanace	k1gFnSc7	asanace
starých	starý	k2eAgInPc2d1	starý
přízemních	přízemní	k2eAgInPc2d1	přízemní
domků	domek	k1gInPc2	domek
<g/>
,	,	kIx,	,
vytápěných	vytápěný	k2eAgNnPc2d1	vytápěné
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
množství	množství	k1gNnSc1	množství
emisí	emise	k1gFnPc2	emise
způsobených	způsobený	k2eAgFnPc2d1	způsobená
topením	topení	k1gNnSc7	topení
<g/>
,	,	kIx,	,
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc4	rozvoj
individuálního	individuální	k2eAgInSc2d1	individuální
motorismu	motorismus	k1gInSc2	motorismus
nedává	dávat	k5eNaImIp3nS	dávat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
mnoho	mnoho	k4c4	mnoho
nadějí	naděje	k1gFnPc2	naděje
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
městské	městský	k2eAgInPc4d1	městský
stromy	strom	k1gInPc4	strom
patří	patřit	k5eAaImIp3nS	patřit
zeravec	zeravec	k1gInSc1	zeravec
východní	východní	k2eAgInSc1d1	východní
(	(	kIx(	(
<g/>
Platycladus	Platycladus	k1gInSc1	Platycladus
orientalis	orientalis	k1gFnSc2	orientalis
<g/>
)	)	kIx)	)
a	a	k8xC	a
jerlín	jerlín	k1gInSc1	jerlín
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Sophora	Sophora	k1gFnSc1	Sophora
japonica	japonica	k1gMnSc1	japonica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městskými	městský	k2eAgFnPc7d1	městská
květinami	květina	k1gFnPc7	květina
jsou	být	k5eAaImIp3nP	být
chryzantéma	chryzantéma	k1gFnSc1	chryzantéma
(	(	kIx(	(
<g/>
Chrysanthemum	Chrysanthemum	k1gInSc1	Chrysanthemum
morifolium	morifolium	k1gNnSc1	morifolium
<g/>
)	)	kIx)	)
a	a	k8xC	a
čínská	čínský	k2eAgFnSc1d1	čínská
růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
chinensis	chinensis	k1gFnSc2	chinensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
podél	podél	k7c2	podél
neviditelné	viditelný	k2eNgFnSc2d1	neviditelná
severojižní	severojižní	k2eAgFnSc2d1	severojižní
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
zároveň	zároveň	k6eAd1	zároveň
páteř	páteř	k1gFnSc4	páteř
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
Chrám	chrám	k1gInSc1	chrám
nebes	nebesa	k1gNnPc2	nebesa
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
náměstí	náměstí	k1gNnSc1	náměstí
Tian	Tian	k1gNnSc1	Tian
<g/>
'	'	kIx"	'
<g/>
anmen	anmen	k1gInSc1	anmen
<g/>
,	,	kIx,	,
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
město	město	k1gNnSc1	město
a	a	k8xC	a
park	park	k1gInSc1	park
Jingshan	Jingshana	k1gFnPc2	Jingshana
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bubnová	bubnový	k2eAgFnSc1d1	bubnová
a	a	k8xC	a
Zvonová	zvonový	k2eAgFnSc1d1	zvonová
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
sídlem	sídlo	k1gNnSc7	sídlo
císařů	císař	k1gMnPc2	císař
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
císař	císař	k1gMnSc1	císař
Jung-le	Junge	k1gNnSc2	Jung-le
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
budov	budova	k1gFnPc2	budova
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k6eAd1	až
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
a	a	k8xC	a
nedotknutelného	nedotknutelného	k?	nedotknutelného
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
mohutnými	mohutný	k2eAgInPc7d1	mohutný
příkopy	příkop	k1gInPc7	příkop
a	a	k8xC	a
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Směl	smět	k5eAaImAgMnS	smět
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
zdržovat	zdržovat	k5eAaImF	zdržovat
jen	jen	k9	jen
císař	císař	k1gMnSc1	císař
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
smrtelníkům	smrtelník	k1gMnPc3	smrtelník
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgMnS	být
vstup	vstup	k1gInSc4	vstup
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
smrti	smrt	k1gFnSc2	smrt
přísně	přísně	k6eAd1	přísně
zakázán	zakázat	k5eAaPmNgInS	zakázat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
domovem	domov	k1gInSc7	domov
24	[number]	k4	24
císařů	císař	k1gMnPc2	císař
dvou	dva	k4xCgFnPc2	dva
dynastií	dynastie	k1gFnPc2	dynastie
-	-	kIx~	-
Ming	Ming	k1gInSc1	Ming
a	a	k8xC	a
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
bylo	být	k5eAaImAgNnS	být
povoláno	povolán	k2eAgNnSc1d1	povoláno
roku	rok	k1gInSc2	rok
1406	[number]	k4	1406
milión	milión	k4xCgInSc1	milión
dělníků	dělník	k1gMnPc2	dělník
císařem	císař	k1gMnSc7	císař
Jung-le	Junge	k1gFnSc2	Jung-le
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
byly	být	k5eAaImAgInP	být
nesmírné	smírný	k2eNgInPc1d1	nesmírný
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
půdorys	půdorys	k1gInSc4	půdorys
a	a	k8xC	a
za	za	k7c7	za
jeho	jeho	k3xOp3gInPc7	jeho
12,5	[number]	k4	12,5
m	m	kA	m
vysokými	vysoký	k2eAgFnPc7d1	vysoká
zdmi	zeď	k1gFnPc7	zeď
není	být	k5eNaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
budova	budova	k1gFnSc1	budova
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soubor	soubor	k1gInSc1	soubor
vícero	vícero	k1gNnSc1	vícero
síní	síň	k1gFnPc2	síň
<g/>
,	,	kIx,	,
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
chrámů	chrám	k1gInPc2	chrám
propojených	propojený	k2eAgInPc2d1	propojený
průchody	průchod	k1gInPc7	průchod
a	a	k8xC	a
chodbami	chodba	k1gFnPc7	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
9,999	[number]	k4	9,999
místností	místnost	k1gFnPc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
mu	on	k3xPp3gMnSc3	on
hrozil	hrozit	k5eAaImAgInS	hrozit
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jej	on	k3xPp3gMnSc4	on
zapálili	zapálit	k5eAaPmAgMnP	zapálit
Mandžuové	Mandžu	k1gMnPc1	Mandžu
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pak	pak	k6eAd1	pak
založili	založit	k5eAaPmAgMnP	založit
dynastii	dynastie	k1gFnSc4	dynastie
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
i	i	k9	i
Kuomintang	Kuomintang	k1gInSc1	Kuomintang
-	-	kIx~	-
čínští	čínský	k2eAgMnPc1d1	čínský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
na	na	k7c4	na
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
opojení	opojení	k1gNnSc2	opojení
Kulturní	kulturní	k2eAgFnSc7d1	kulturní
revolucí	revoluce	k1gFnSc7	revoluce
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
málem	málem	k6eAd1	málem
rozmetán	rozmetat	k5eAaImNgInS	rozmetat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tradiční	tradiční	k2eAgFnSc1d1	tradiční
čínská	čínský	k2eAgFnSc1d1	čínská
architektura	architektura	k1gFnSc1	architektura
pracuje	pracovat	k5eAaImIp3nS	pracovat
spíše	spíše	k9	spíše
s	s	k7c7	s
horizontálními	horizontální	k2eAgFnPc7d1	horizontální
než	než	k8xS	než
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
liniemi	linie	k1gFnPc7	linie
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc1	budova
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
nevysoké	vysoký	k2eNgInPc1d1	nevysoký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostor	prostor	k1gInSc1	prostor
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gInPc2	on
doslova	doslova	k6eAd1	doslova
bere	brát	k5eAaImIp3nS	brát
dech	dech	k1gInSc4	dech
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náměstí	náměstí	k1gNnSc2	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
Náměstí	náměstí	k1gNnSc1	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Tian	Tian	k1gInSc1	Tian
<g/>
'	'	kIx"	'
<g/>
anmen	anmen	k2eAgInSc1d1	anmen
Guangchang	Guangchang	k1gInSc1	Guangchang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
budoucí	budoucí	k2eAgMnPc1d1	budoucí
čínský	čínský	k2eAgMnSc1d1	čínský
vládce	vládce	k1gMnSc1	vládce
a	a	k8xC	a
diktátor	diktátor	k1gMnSc1	diktátor
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Čínskou	čínský	k2eAgFnSc4d1	čínská
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
jej	on	k3xPp3gNnSc4	on
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Brána	brána	k1gFnSc1	brána
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Tian	Tian	k1gInSc1	Tian
<g/>
'	'	kIx"	'
<g/>
anmen	anmen	k1gInSc1	anmen
<g/>
)	)	kIx)	)
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
portrét	portrét	k1gInSc1	portrét
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
<g/>
.	.	kIx.	.
</s>
<s>
U	U	kA	U
prostřed	prostřed	k7c2	prostřed
náměstí	náměstí	k1gNnSc2	náměstí
stojí	stát	k5eAaImIp3nS	stát
památník	památník	k1gInSc1	památník
Lidových	lidový	k2eAgFnPc2d1	lidová
hrdinu	hrdina	k1gMnSc4	hrdina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
polovině	polovina	k1gFnSc6	polovina
pak	pak	k6eAd1	pak
Mao	Mao	k1gMnPc3	Mao
Ce-tungovo	Ceungův	k2eAgNnSc1d1	Ce-tungovo
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
dokončené	dokončený	k2eAgNnSc1d1	dokončené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
stojí	stát	k5eAaImIp3nS	stát
Brána	brána	k1gFnSc1	brána
Správného	správný	k2eAgNnSc2d1	správné
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
Zhengyangmen	Zhengyangmen	k1gInSc1	Zhengyangmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Přední	přední	k2eAgFnSc1d1	přední
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Qianmen	Qianmen	k1gInSc1	Qianmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Šípová	šípový	k2eAgFnSc1d1	šípová
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
Jian	Jian	k1gMnSc1	Jian
Lou	Lou	k1gMnSc1	Lou
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
kdysi	kdysi	k6eAd1	kdysi
propojeny	propojit	k5eAaPmNgFnP	propojit
okrouhlou	okrouhlý	k2eAgFnSc7d1	okrouhlá
hradbou	hradba	k1gFnSc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc3d1	západní
straně	strana	k1gFnSc3	strana
vévodí	vévodit	k5eAaImIp3nS	vévodit
Velká	velký	k2eAgFnSc1d1	velká
síň	síň	k1gFnSc1	síň
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
Renmin	Renmin	k1gInSc1	Renmin
Dahui	Dahue	k1gFnSc4	Dahue
Tang	tango	k1gNnPc2	tango
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
čínský	čínský	k2eAgInSc1d1	čínský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
stojí	stát	k5eAaImIp3nS	stát
Čínské	čínský	k2eAgNnSc1d1	čínské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
čínská	čínský	k2eAgFnSc1d1	čínská
armáda	armáda	k1gFnSc1	armáda
brutálně	brutálně	k6eAd1	brutálně
zakročila	zakročit	k5eAaPmAgFnS	zakročit
proti	proti	k7c3	proti
demonstrujícím	demonstrující	k2eAgMnPc3d1	demonstrující
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
si	se	k3xPyFc3	se
proráželi	prorážet	k5eAaImAgMnP	prorážet
cestu	cesta	k1gFnSc4	cesta
pomocí	pomocí	k7c2	pomocí
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
střelbou	střelba	k1gFnSc7	střelba
z	z	k7c2	z
automatických	automatický	k2eAgFnPc2d1	automatická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
zákrok	zákrok	k1gInSc1	zákrok
na	na	k7c6	na
pekingském	pekingský	k2eAgNnSc6d1	pekingské
náměstí	náměstí	k1gNnSc6	náměstí
Tchien-an-men	Tchiennen	k1gInSc4	Tchien-an-men
proti	proti	k7c3	proti
čínským	čínský	k2eAgMnPc3d1	čínský
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
demonstracemi	demonstrace	k1gFnPc7	demonstrace
a	a	k8xC	a
hladovkami	hladovka	k1gFnPc7	hladovka
volali	volat	k5eAaImAgMnP	volat
po	po	k7c6	po
širší	široký	k2eAgFnSc6d2	širší
demokratizaci	demokratizace	k1gFnSc6	demokratizace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Čínu	Čína	k1gFnSc4	Čína
znamenal	znamenat	k5eAaImAgInS	znamenat
několikaletou	několikaletý	k2eAgFnSc4d1	několikaletá
ztrátu	ztráta	k1gFnSc4	ztráta
kreditu	kredit	k1gInSc2	kredit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vnitropolitický	vnitropolitický	k2eAgInSc1d1	vnitropolitický
vývoj	vývoj	k1gInSc1	vývoj
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
300	[number]	k4	300
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
skončilo	skončit	k5eAaPmAgNnS	skončit
šest	šest	k4xCc1	šest
neklidných	klidný	k2eNgFnPc2d1	neklidná
neděl	neděle	k1gFnPc2	neděle
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
statisíce	statisíce	k1gInPc1	statisíce
lidí	člověk	k1gMnPc2	člověk
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
demonstrací	demonstrace	k1gFnPc2	demonstrace
a	a	k8xC	a
hladovek	hladovka	k1gFnPc2	hladovka
volaly	volat	k5eAaImAgFnP	volat
po	po	k7c4	po
větší	veliký	k2eAgFnSc4d2	veliký
demokratizaci	demokratizace	k1gFnSc4	demokratizace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
Tian	Tian	k1gInSc1	Tian
<g/>
'	'	kIx"	'
<g/>
anmen	anmen	k1gInSc1	anmen
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Chrám	chrám	k1gInSc1	chrám
nebes	nebesa	k1gNnPc2	nebesa
(	(	kIx(	(
<g/>
Tian	Tian	k1gMnSc1	Tian
Tan	Tan	k1gMnSc1	Tan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
uprostřed	uprostřed	k7c2	uprostřed
rozlehlého	rozlehlý	k2eAgInSc2d1	rozlehlý
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
polokruhovitá	polokruhovitý	k2eAgFnSc1d1	polokruhovitá
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ta	ten	k3xDgFnSc1	ten
jižní	jižní	k2eAgFnSc1d1	jižní
je	být	k5eAaImIp3nS	být
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
roku	rok	k1gInSc2	rok
1406	[number]	k4	1406
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
staveb	stavba	k1gFnPc2	stavba
-	-	kIx~	-
Kruhový	kruhový	k2eAgInSc4d1	kruhový
oltář	oltář	k1gInSc4	oltář
(	(	kIx(	(
<g/>
Yuanqiu	Yuanqius	k1gMnSc3	Yuanqius
Tan	Tan	k1gMnSc3	Tan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Síň	síň	k1gFnSc1	síň
Císařské	císařský	k2eAgFnSc2d1	císařská
krypty	krypta	k1gFnSc2	krypta
nebes	nebesa	k1gNnPc2	nebesa
(	(	kIx(	(
<g/>
Huangqiong	Huangqiong	k1gMnSc1	Huangqiong
Yu	Yu	k1gMnSc1	Yu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Síň	síň	k1gFnSc1	síň
Zdrženlivosti	zdrženlivost	k1gFnSc2	zdrženlivost
(	(	kIx(	(
<g/>
Zhai	Zha	k1gFnSc2	Zha
Gong	gong	k1gInSc1	gong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kancelář	kancelář	k1gFnSc1	kancelář
Božské	božský	k2eAgFnSc2d1	božská
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Shenyue	Shenyue	k1gFnSc1	Shenyue
Shu	Shu	k1gMnSc1	Shu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tou	ten	k3xDgFnSc7	ten
nejznámější	známý	k2eAgFnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
Síň	síň	k1gFnSc1	síň
Modliteb	modlitba	k1gFnPc2	modlitba
za	za	k7c4	za
dobrou	dobrý	k2eAgFnSc4d1	dobrá
úrodu	úroda	k1gFnSc4	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
38	[number]	k4	38
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
třípatrová	třípatrový	k2eAgFnSc1d1	třípatrová
kruhová	kruhový	k2eAgFnSc1d1	kruhová
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
střechou	střecha	k1gFnSc7	střecha
,	,	kIx,	,
<g/>
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
zlacenou	zlacený	k2eAgFnSc7d1	zlacená
kulovou	kulový	k2eAgFnSc7d1	kulová
makovicí	makovice	k1gFnSc7	makovice
<g/>
;	;	kIx,	;
skvost	skvost	k1gInSc1	skvost
tradiční	tradiční	k2eAgFnSc2d1	tradiční
staré	starý	k2eAgFnSc2d1	stará
čínské	čínský	k2eAgFnSc2d1	čínská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
hlavní	hlavní	k2eAgInPc1d1	hlavní
pilíře	pilíř	k1gInPc1	pilíř
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
po	po	k7c6	po
12	[number]	k4	12
sloupech	sloup	k1gInPc6	sloup
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
měsíce	měsíc	k1gInPc1	měsíc
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc1	rozdělení
dne	den	k1gInSc2	den
do	do	k7c2	do
dvouhodinových	dvouhodinový	k2eAgFnPc2d1	dvouhodinová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
náboženských	náboženský	k2eAgFnPc2d1	náboženská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
už	už	k6eAd1	už
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc2	veřejnost
Palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
plochy	plocha	k1gFnPc4	plocha
letního	letní	k2eAgInSc2d1	letní
paláce	palác	k1gInSc2	palác
tvoří	tvořit	k5eAaImIp3nS	tvořit
ručně	ručně	k6eAd1	ručně
vyhloubené	vyhloubený	k2eAgNnSc1d1	vyhloubené
jezero	jezero	k1gNnSc1	jezero
Kunming	Kunming	k1gInSc1	Kunming
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
jeho	on	k3xPp3gInSc2	on
břehu	břeh	k1gInSc2	břeh
je	být	k5eAaImIp3nS	být
krásná	krásný	k2eAgFnSc1d1	krásná
kolonáda	kolonáda	k1gFnSc1	kolonáda
<g/>
.	.	kIx.	.
</s>
