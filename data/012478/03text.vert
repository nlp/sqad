<p>
<s>
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
IP	IP	kA	IP
address	addressa	k1gFnPc2	addressa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jednoznačně	jednoznačně	k6eAd1	jednoznačně
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
síťové	síťový	k2eAgNnSc1d1	síťové
rozhraní	rozhraní	k1gNnSc1	rozhraní
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
IP	IP	kA	IP
protokol	protokol	k1gInSc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zapisovány	zapisovat	k5eAaImNgFnP	zapisovat
dekadicky	dekadicky	k6eAd1	dekadicky
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
oktetech	oktet	k1gInPc6	oktet
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
po	po	k7c6	po
osmicích	osmik	k1gInPc6	osmik
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
192.168.0.2	[number]	k4	192.168.0.2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
adres	adresa	k1gFnPc2	adresa
je	být	k5eAaImIp3nS	být
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
postupně	postupně	k6eAd1	postupně
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
protokolem	protokol	k1gInSc7	protokol
IPv	IPv	k1gFnSc2	IPv
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
používá	používat	k5eAaImIp3nS	používat
128	[number]	k4	128
<g/>
bitové	bitový	k2eAgFnPc4d1	bitová
IP	IP	kA	IP
adresy	adresa	k1gFnPc4	adresa
zapsané	zapsaný	k2eAgFnPc4d1	zapsaná
hexadecimálně	hexadecimálně	k6eAd1	hexadecimálně
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
<g/>
db	db	kA	db
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1234	[number]	k4	1234
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
567	[number]	k4	567
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
síťových	síťový	k2eAgNnPc2d1	síťové
rozhraní	rozhraní	k1gNnPc2	rozhraní
připojených	připojený	k2eAgNnPc2d1	připojené
k	k	k7c3	k
počítačové	počítačový	k2eAgFnSc3d1	počítačová
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Síťovým	síťový	k2eAgNnSc7d1	síťové
rozhraním	rozhraní	k1gNnSc7	rozhraní
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
(	(	kIx(	(
<g/>
Ethernet	Ethernet	k1gInSc1	Ethernet
<g/>
,	,	kIx,	,
Wi-Fi	Wi-Fi	k1gNnSc1	Wi-Fi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
IA	ia	k0	ia
port	port	k1gInSc4	port
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
virtuální	virtuální	k2eAgNnSc4d1	virtuální
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
loopback	loopback	k1gInSc1	loopback
<g/>
,	,	kIx,	,
rozhraní	rozhraní	k1gNnSc1	rozhraní
pro	pro	k7c4	pro
virtuální	virtuální	k2eAgInSc4d1	virtuální
počítač	počítač	k1gInSc4	počítač
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
IP	IP	kA	IP
znamená	znamenat	k5eAaImIp3nS	znamenat
Internet	Internet	k1gInSc1	Internet
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yIgNnSc2	který
spolu	spolu	k6eAd1	spolu
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
všechna	všechen	k3xTgNnPc1	všechen
zařízení	zařízení	k1gNnPc1	zařízení
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
používaná	používaný	k2eAgFnSc1d1	používaná
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
však	však	k9	však
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c6	na
novější	nový	k2eAgFnSc6d2	novější
verzi	verze	k1gFnSc6	verze
6	[number]	k4	6
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
protokolech	protokol	k1gInPc6	protokol
se	se	k3xPyFc4	se
adresování	adresování	k1gNnSc2	adresování
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zařízení	zařízení	k1gNnPc2	zařízení
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
jinak	jinak	k6eAd1	jinak
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
MAC	Mac	kA	Mac
adresa	adresa	k1gFnSc1	adresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IP	IP	kA	IP
protokol	protokol	k1gInSc1	protokol
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
komunikace	komunikace	k1gFnSc2	komunikace
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
síti	síť	k1gFnSc6	síť
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
rozhraní	rozhraní	k1gNnPc2	rozhraní
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejná	stejný	k2eAgFnSc1d1	stejná
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
více	hodně	k6eAd2	hodně
rozhraních	rozhraní	k1gNnPc6	rozhraní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
NAT	NAT	kA	NAT
a	a	k8xC	a
privátní	privátní	k2eAgFnSc2d1	privátní
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veškerá	veškerý	k3xTgNnPc1	veškerý
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
síťovými	síťový	k2eAgNnPc7d1	síťové
rozhraními	rozhraní	k1gNnPc7	rozhraní
přenášena	přenášen	k2eAgFnSc1d1	přenášena
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
IP	IP	kA	IP
datagramů	datagram	k1gInPc2	datagram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
uživatele	uživatel	k1gMnSc2	uživatel
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
pamatovat	pamatovat	k5eAaImF	pamatovat
si	se	k3xPyFc3	se
číselné	číselný	k2eAgFnPc4d1	číselná
adresy	adresa	k1gFnPc4	adresa
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
služba	služba	k1gFnSc1	služba
DNS	DNS	kA	DNS
(	(	kIx(	(
<g/>
Domain	Domain	k1gInSc1	Domain
Name	Nam	k1gInSc2	Nam
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
používat	používat	k5eAaImF	používat
snadněji	snadno	k6eAd2	snadno
zapamatovatelná	zapamatovatelný	k2eAgNnPc1d1	zapamatovatelné
doménová	doménový	k2eAgNnPc1d1	doménové
jména	jméno	k1gNnPc1	jméno
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
automaticky	automaticky	k6eAd1	automaticky
převáděna	převádět	k5eAaImNgFnS	převádět
na	na	k7c4	na
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
www.seznam.cz	www.seznam.cz	k1gInSc1	www.seznam.cz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
IPv	IPv	k1gMnSc1	IPv
<g/>
4	[number]	k4	4
adresy	adresa	k1gFnSc2	adresa
==	==	k?	==
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
protokol	protokol	k1gInSc1	protokol
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
tzv.	tzv.	kA	tzv.
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
formát	formát	k1gInSc1	formát
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
IP	IP	kA	IP
adresou	adresa	k1gFnSc7	adresa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgNnSc1d1	bitové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xS	jako
čtyři	čtyři	k4xCgInPc1	čtyři
oktety	oktet	k1gInPc1	oktet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
čtyři	čtyři	k4xCgInPc4	čtyři
osmice	osmice	k1gInPc1	osmice
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
počítače	počítač	k1gInPc1	počítač
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používaly	používat	k5eAaImAgFnP	používat
osmibitové	osmibitový	k2eAgNnSc4d1	osmibitové
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bajt	bajt	k1gInSc1	bajt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
zapisována	zapisovat	k5eAaImNgFnS	zapisovat
čtyřmi	čtyři	k4xCgInPc7	čtyři
desítkovými	desítkový	k2eAgInPc7d1	desítkový
čísly	čísnout	k5eAaPmAgFnP	čísnout
oddělenými	oddělený	k2eAgFnPc7d1	oddělená
tečkami	tečka	k1gFnPc7	tečka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každé	každý	k3xTgNnSc4	každý
číslo	číslo	k1gNnSc4	číslo
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
jednu	jeden	k4xCgFnSc4	jeden
osmici	osmice	k1gFnSc4	osmice
z	z	k7c2	z
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jeden	jeden	k4xCgInSc4	jeden
bajt	bajt	k1gInSc4	bajt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
192.168.48.39	[number]	k4	192.168.48.39
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgNnSc4d1	bitové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
takových	takový	k3xDgMnPc2	takový
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
zapsat	zapsat	k5eAaPmF	zapsat
232	[number]	k4	232
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
miliardy	miliarda	k4xCgFnPc4	miliarda
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
4	[number]	k4	4
294	[number]	k4	294
967	[number]	k4	967
296	[number]	k4	296
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahu	rozsah	k1gInSc2	rozsah
použitelných	použitelný	k2eAgFnPc2d1	použitelná
adres	adresa	k1gFnPc2	adresa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
adresní	adresní	k2eAgInSc1d1	adresní
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Adresní	adresní	k2eAgInSc1d1	adresní
prostor	prostor	k1gInSc1	prostor
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
určitá	určitý	k2eAgFnSc1d1	určitá
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rezervována	rezervovat	k5eAaBmNgFnS	rezervovat
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
potřeby	potřeba	k1gFnPc4	potřeba
protokolu	protokol	k1gInSc2	protokol
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
směrování	směrování	k1gNnSc4	směrování
<g/>
)	)	kIx)	)
IP	IP	kA	IP
adresy	adresa	k1gFnPc4	adresa
shlukovány	shlukován	k2eAgFnPc4d1	shlukován
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
adresní	adresní	k2eAgInSc1d1	adresní
prostor	prostor	k1gInSc1	prostor
dále	daleko	k6eAd2	daleko
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prudkým	prudký	k2eAgInSc7d1	prudký
rozmachem	rozmach	k1gInSc7	rozmach
počítačů	počítač	k1gMnPc2	počítač
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
IPv	IPv	k1gFnPc2	IPv
<g/>
4	[number]	k4	4
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
adresy	adresa	k1gFnSc2	adresa
===	===	k?	===
</s>
</p>
<p>
<s>
Adresa	adresa	k1gFnSc1	adresa
se	se	k3xPyFc4	se
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
části	část	k1gFnPc1	část
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
snadné	snadný	k2eAgNnSc1d1	snadné
určit	určit	k5eAaPmF	určit
umístění	umístění	k1gNnSc2	umístění
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obdobu	obdoba	k1gFnSc4	obdoba
poštovní	poštovní	k2eAgFnSc2d1	poštovní
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
adresáta	adresát	k1gMnSc2	adresát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
adresou	adresa	k1gFnSc7	adresa
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
a	a	k8xC	a
počítače	počítač	k1gInSc2	počítač
určuje	určovat	k5eAaImIp3nS	určovat
maska	maska	k1gFnSc1	maska
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
tvaru	tvar	k1gInSc6	tvar
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedničky	jednička	k1gFnPc4	jednička
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
adrese	adresa	k1gFnSc6	adresa
nachází	nacházet	k5eAaImIp3nS	nacházet
číslo	číslo	k1gNnSc1	číslo
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
podsítě	podsíť	k1gFnSc2	podsíť
<g/>
,	,	kIx,	,
a	a	k8xC	a
nuly	nula	k1gFnPc1	nula
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třídy	třída	k1gFnSc2	třída
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
sloužila	sloužit	k5eAaImAgFnS	sloužit
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
podsítě	podsíť	k1gFnSc2	podsíť
první	první	k4xOgFnSc2	první
osmice	osmice	k1gFnSc2	osmice
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
v	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
tzv.	tzv.	kA	tzv.
třídy	třída	k1gFnSc2	třída
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
podsítě	podsíť	k1gFnPc4	podsíť
určeno	určit	k5eAaPmNgNnS	určit
tzv.	tzv.	kA	tzv.
maskou	maska	k1gFnSc7	maska
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
prvními	první	k4xOgFnPc7	první
několika	několik	k4yIc7	několik
bity	bit	k1gInPc7	bit
samotné	samotný	k2eAgFnSc2d1	samotná
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
ukázal	ukázat	k5eAaPmAgInS	ukázat
také	také	k9	také
jako	jako	k9	jako
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
relativně	relativně	k6eAd1	relativně
hodně	hodně	k6eAd1	hodně
velkých	velký	k2eAgFnPc2d1	velká
podsítí	podsíť	k1gFnPc2	podsíť
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
A	a	k9	a
<g/>
)	)	kIx)	)
a	a	k8xC	a
málo	málo	k4c4	málo
malých	malý	k2eAgFnPc2d1	malá
podsítí	podsíť	k1gFnPc2	podsíť
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
beztřídní	beztřídní	k2eAgNnSc1d1	beztřídní
dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
podsítě	podsíť	k1gFnPc4	podsíť
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Classless	Classless	k1gInSc1	Classless
delegace	delegace	k1gFnSc2	delegace
===	===	k?	===
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
podsítě	podsíť	k1gFnPc4	podsíť
podle	podle	k7c2	podle
tříd	třída	k1gFnPc2	třída
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nepružné	pružný	k2eNgNnSc1d1	nepružné
a	a	k8xC	a
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
nedostatkem	nedostatek	k1gInSc7	nedostatek
IPv	IPv	k1gFnSc7	IPv
<g/>
4	[number]	k4	4
adres	adresa	k1gFnPc2	adresa
byly	být	k5eAaImAgInP	být
hledány	hledán	k2eAgInPc1d1	hledán
způsoby	způsob	k1gInPc1	způsob
pro	pro	k7c4	pro
vylepšení	vylepšení	k1gNnSc4	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
tzv.	tzv.	kA	tzv.
CIDR	CIDR	kA	CIDR
(	(	kIx(	(
<g/>
Classless	Classless	k1gInSc1	Classless
Inter-Domain	Inter-Domain	k2eAgInSc1d1	Inter-Domain
Routing	Routing	k1gInSc1	Routing
<g/>
,	,	kIx,	,
beztřídní	beztřídní	k2eAgFnSc1d1	beztřídní
mezidoménové	mezidoménový	k2eAgNnSc4d1	mezidoménový
směrování	směrování	k1gNnSc4	směrování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
IP	IP	kA	IP
adrese	adresa	k1gFnSc6	adresa
možné	možný	k2eAgFnSc6d1	možná
hranici	hranice	k1gFnSc6	hranice
čísla	číslo	k1gNnSc2	číslo
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
umisťovat	umisťovat	k5eAaImF	umisťovat
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
libovolné	libovolný	k2eAgInPc4d1	libovolný
bity	bit	k1gInPc4	bit
dané	daný	k2eAgFnSc2d1	daná
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
zapsána	zapsat	k5eAaPmNgFnS	zapsat
bitově	bitově	k6eAd1	bitově
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hranice	hranice	k1gFnSc1	hranice
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c7	mezi
desítkovými	desítkový	k2eAgNnPc7d1	desítkové
čísly	číslo	k1gNnPc7	číslo
zápisu	zápis	k1gInSc2	zápis
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
určuje	určovat	k5eAaImIp3nS	určovat
tzv	tzv	kA	tzv
<g/>
,	,	kIx,	,
maska	maska	k1gFnSc1	maska
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
síťového	síťový	k2eAgNnSc2d1	síťové
rozhraní	rozhraní	k1gNnSc2	rozhraní
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zapisována	zapisován	k2eAgFnSc1d1	zapisována
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
prefixu	prefix	k1gInSc2	prefix
nebo	nebo	k8xC	nebo
desítkovým	desítkový	k2eAgInSc7d1	desítkový
zápisem	zápis	k1gInSc7	zápis
masky	maska	k1gFnSc2	maska
sítě	síť	k1gFnSc2	síť
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
příklad	příklad	k1gInSc4	příklad
níže	níže	k1gFnPc4	níže
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
zápisy	zápis	k1gInPc1	zápis
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
<g/>
,	,	kIx,	,
podsíť	podsíť	k1gFnSc1	podsíť
používá	používat	k5eAaImIp3nS	používat
rozsah	rozsah	k1gInSc4	rozsah
adres	adresa	k1gFnPc2	adresa
192.168.0.0	[number]	k4	192.168.0.0
<g/>
–	–	k?	–
<g/>
192.168.31.255	[number]	k4	192.168.31.255
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
192.168.24.0	[number]	k4	192.168.24.0
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
</s>
</p>
<p>
<s>
192.168.24.0	[number]	k4	192.168.24.0
<g/>
/	/	kIx~	/
<g/>
255.255.224.0	[number]	k4	255.255.224.0
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhrazené	vyhrazený	k2eAgFnPc1d1	vyhrazená
adresy	adresa	k1gFnPc1	adresa
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
využít	využít	k5eAaPmF	využít
všechny	všechen	k3xTgInPc4	všechen
IP	IP	kA	IP
adresy	adresa	k1gFnPc1	adresa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
speciální	speciální	k2eAgNnPc4d1	speciální
určení	určení	k1gNnPc4	určení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
adresa	adresa	k1gFnSc1	adresa
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
adresou	adresa	k1gFnSc7	adresa
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
jako	jako	k8xS	jako
označení	označení	k1gNnSc1	označení
celé	celá	k1gFnSc2	celá
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
<g/>
)	)	kIx)	)
<g/>
sítě	síť	k1gFnPc4	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
síť	síť	k1gFnSc1	síť
192.168.24.0	[number]	k4	192.168.24.0
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
adresa	adresa	k1gFnSc1	adresa
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
adresa	adresa	k1gFnSc1	adresa
stanice	stanice	k1gFnSc2	stanice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
samé	samý	k3xTgFnPc4	samý
binární	binární	k2eAgFnPc4d1	binární
jedničky	jednička	k1gFnPc4	jednička
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
adresa	adresa	k1gFnSc1	adresa
pro	pro	k7c4	pro
všesměrové	všesměrový	k2eAgNnSc4d1	všesměrové
vysílání	vysílání	k1gNnSc4	vysílání
(	(	kIx(	(
<g/>
broadcast	broadcast	k1gInSc1	broadcast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takové	takový	k3xDgFnPc1	takový
adresy	adresa	k1gFnPc1	adresa
tedy	tedy	k9	tedy
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
normální	normální	k2eAgInPc4d1	normální
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adresy	adresa	k1gFnPc1	adresa
127	[number]	k4	127
<g/>
.	.	kIx.	.
<g/>
x.x.x	x.x.x	k1gInSc1	x.x.x
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
localhost	localhost	k1gFnSc4	localhost
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
adresa	adresa	k1gFnSc1	adresa
127.0.0.1	[number]	k4	127.0.0.1
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rezervovány	rezervovat	k5eAaBmNgInP	rezervovat
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
loopback	loopback	k1gInSc4	loopback
<g/>
,	,	kIx,	,
logickou	logický	k2eAgFnSc4d1	logická
smyčku	smyčka	k1gFnSc4	smyčka
umožňující	umožňující	k2eAgFnSc1d1	umožňující
posílat	posílat	k5eAaImF	posílat
pakety	paket	k1gInPc4	paket
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
vyčleněny	vyčleněn	k2eAgInPc1d1	vyčleněn
rozsahy	rozsah	k1gInPc1	rozsah
tzv.	tzv.	kA	tzv.
interních	interní	k2eAgFnPc2d1	interní
(	(	kIx(	(
<g/>
neveřejných	veřejný	k2eNgFnPc2d1	neveřejná
<g/>
)	)	kIx)	)
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
privátní	privátní	k2eAgInSc4d1	privátní
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
adresování	adresování	k1gNnSc4	adresování
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lokálních	lokální	k2eAgNnPc6d1	lokální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nemohou	moct	k5eNaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
neveřejné	veřejný	k2eNgFnPc1d1	neveřejná
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgFnPc1d1	určena
adresy	adresa	k1gFnPc1	adresa
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
A	A	kA	A
<g/>
:	:	kIx,	:
10.0.0.0	[number]	k4	10.0.0.0
až	až	k9	až
10.255.255.255	[number]	k4	10.255.255.255
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
<g/>
×	×	k?	×
16	[number]	k4	16
777	[number]	k4	777
216	[number]	k4	216
adres	adresa	k1gFnPc2	adresa
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
16	[number]	k4	16
777	[number]	k4	777
216	[number]	k4	216
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
použitelných	použitelný	k2eAgInPc2d1	použitelný
jen	jen	k6eAd1	jen
16	[number]	k4	16
777	[number]	k4	777
214	[number]	k4	214
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
B	B	kA	B
<g/>
:	:	kIx,	:
172.16.0.0	[number]	k4	172.16.0.0
až	až	k9	až
172.31.255.255	[number]	k4	172.31.255.255
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
<g/>
×	×	k?	×
65	[number]	k4	65
536	[number]	k4	536
adres	adresa	k1gFnPc2	adresa
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
1	[number]	k4	1
048	[number]	k4	048
576	[number]	k4	576
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
použitelných	použitelný	k2eAgInPc2d1	použitelný
jen	jen	k6eAd1	jen
1	[number]	k4	1
048	[number]	k4	048
544	[number]	k4	544
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
C	C	kA	C
<g/>
:	:	kIx,	:
192.168.0.0	[number]	k4	192.168.0.0
až	až	k9	až
192.168.255.255	[number]	k4	192.168.255.255
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
256	[number]	k4	256
<g/>
×	×	k?	×
256	[number]	k4	256
adres	adresa	k1gFnPc2	adresa
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
65	[number]	k4	65
536	[number]	k4	536
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
použitelných	použitelný	k2eAgInPc2d1	použitelný
jen	jen	k6eAd1	jen
65	[number]	k4	65
0	[number]	k4	0
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
100.64.0.0	[number]	k4	100.64.0.0
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
-	-	kIx~	-
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
Prefix	prefix	k1gInSc1	prefix
for	forum	k1gNnPc2	forum
Shared	Shared	k1gInSc1	Shared
Address	Addressa	k1gFnPc2	Addressa
Space	Space	k1gFnSc2	Space
podle	podle	k7c2	podle
specifikace	specifikace	k1gFnSc2	specifikace
RFC	RFC	kA	RFC
6598	[number]	k4	6598
</s>
</p>
<p>
<s>
==	==	k?	==
Adresy	adresa	k1gFnSc2	adresa
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
6	[number]	k4	6
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
IPv	IPv	k1gFnPc2	IPv
<g/>
4	[number]	k4	4
adres	adresa	k1gFnPc2	adresa
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
protokol	protokol	k1gInSc1	protokol
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
128	[number]	k4	128
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
2128	[number]	k4	2128
≈	≈	k?	≈
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
1038	[number]	k4	1038
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IPv	IPv	k?	IPv
<g/>
6	[number]	k4	6
adresa	adresa	k1gFnSc1	adresa
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
osm	osm	k4xCc1	osm
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
čtyřech	čtyři	k4xCgFnPc6	čtyři
hexadecimálních	hexadecimální	k2eAgFnPc6d1	hexadecimální
číslicích	číslice	k1gFnPc6	číslice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
718	[number]	k4	718
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
214	[number]	k4	214
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
ff	ff	kA	ff
<g/>
:	:	kIx,	:
<g/>
fec	fec	k?	fec
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
ca	ca	kA	ca
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
Úvodní	úvodní	k2eAgFnPc1d1	úvodní
nuly	nula	k1gFnPc1	nula
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
skupině	skupina	k1gFnSc6	skupina
lze	lze	k6eAd1	lze
ze	z	k7c2	z
zápisu	zápis	k1gInSc2	zápis
vynechat	vynechat	k5eAaPmF	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
adresu	adresa	k1gFnSc4	adresa
tedy	tedy	k9	tedy
lze	lze	k6eAd1	lze
psát	psát	k5eAaImF	psát
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
<g/>
718	[number]	k4	718
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
214	[number]	k4	214
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
ff	ff	kA	ff
<g/>
:	:	kIx,	:
<g/>
fec	fec	k?	fec
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
ca	ca	kA	ca
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
adresa	adresa	k1gFnSc1	adresa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
nulových	nulový	k2eAgFnPc2d1	nulová
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
zapsat	zapsat	k5eAaPmF	zapsat
jen	jen	k6eAd1	jen
"	"	kIx"	"
<g/>
::	::	k?	::
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zkratka	zkratka	k1gFnSc1	zkratka
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
adrese	adresa	k1gFnSc6	adresa
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
u	u	k7c2	u
prefixů	prefix	k1gInPc2	prefix
pro	pro	k7c4	pro
nulový	nulový	k2eAgInSc4d1	nulový
konec	konec	k1gInSc4	konec
adresy	adresa	k1gFnSc2	adresa
či	či	k8xC	či
u	u	k7c2	u
speciálních	speciální	k2eAgFnPc2d1	speciální
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
loopback	loopback	k1gInSc1	loopback
(	(	kIx(	(
<g/>
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
tvar	tvar	k1gInSc1	tvar
::	::	k?	::
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
příjemnější	příjemný	k2eAgMnSc1d2	příjemnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
plný	plný	k2eAgInSc1d1	plný
zápis	zápis	k1gInSc1	zápis
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skupinové	skupinový	k2eAgFnPc1d1	skupinová
adresy	adresa	k1gFnPc1	adresa
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
všesměrové	všesměrový	k2eAgNnSc1d1	všesměrové
(	(	kIx(	(
<g/>
broadcast	broadcast	k1gFnSc1	broadcast
<g/>
)	)	kIx)	)
adresy	adresa	k1gFnPc1	adresa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
doručení	doručení	k1gNnSc4	doručení
jedné	jeden	k4xCgFnSc2	jeden
zprávy	zpráva	k1gFnSc2	zpráva
více	hodně	k6eAd2	hodně
příjemcům	příjemce	k1gMnPc3	příjemce
zavedeny	zaveden	k2eAgInPc4d1	zaveden
tzv.	tzv.	kA	tzv.
skupinové	skupinový	k2eAgFnPc4d1	skupinová
adresy	adresa	k1gFnPc4	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
adresa	adresa	k1gFnSc1	adresa
ff	ff	kA	ff
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
::	::	k?	::
<g/>
1	[number]	k4	1
označuje	označovat	k5eAaImIp3nS	označovat
všechny	všechen	k3xTgInPc4	všechen
uzly	uzel	k1gInPc4	uzel
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
