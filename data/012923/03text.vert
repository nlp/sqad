<p>
<s>
Amathus	Amathus	k1gMnSc1	Amathus
nebo	nebo	k8xC	nebo
Amafunt	Amafunt	k1gMnSc1	Amafunt
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
asi	asi	k9	asi
24	[number]	k4	24
mil	míle	k1gFnPc2	míle
západně	západně	k6eAd1	západně
od	od	k7c2	od
Larnaky	Larnak	k1gMnPc4	Larnak
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Amathus	Amathus	k1gInSc1	Amathus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Pafos	Pafos	k1gInSc1	Pafos
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
míst	místo	k1gNnPc2	místo
uctívání	uctívání	k1gNnSc2	uctívání
bohyně	bohyně	k1gFnSc2	bohyně
Afrodity	Afrodita	k1gFnSc2	Afrodita
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Starověk	starověk	k1gInSc4	starověk
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgInPc4d1	bronzový
žádné	žádný	k3yNgNnSc1	žádný
město	město	k1gNnSc1	město
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
neexistovalo	existovat	k5eNaImAgNnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
stopy	stopa	k1gFnPc4	stopa
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnPc1d1	železná
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
město	město	k1gNnSc1	město
založil	založit	k5eAaPmAgInS	založit
Kinyras	Kinyras	k1gInSc4	Kinyras
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
otec	otec	k1gMnSc1	otec
Adónise	Adónis	k1gMnSc2	Adónis
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
kultu	kult	k1gInSc2	kult
Afrodity	Afrodita	k1gFnSc2	Afrodita
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
město	město	k1gNnSc4	město
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
své	svůj	k3xOyFgFnSc2	svůj
zesnulé	zesnulý	k2eAgFnSc2d1	zesnulá
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
Theseus	Theseus	k1gMnSc1	Theseus
opustil	opustit	k5eAaPmAgMnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
milovanou	milovaný	k2eAgFnSc4d1	milovaná
Ariadne	Ariadne	k1gFnSc4	Ariadne
v	v	k7c6	v
posvátném	posvátný	k2eAgInSc6d1	posvátný
háji	háj	k1gInSc6	háj
s	s	k7c7	s
názvem	název	k1gInSc7	název
Amalthus	Amalthus	k1gMnSc1	Amalthus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřela	zemřít	k5eAaPmAgFnS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tam	tam	k6eAd1	tam
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
háji	háj	k1gInSc6	háj
byla	být	k5eAaImAgFnS	být
svatyně	svatyně	k1gFnSc1	svatyně
Afrodity	Afrodita	k1gFnSc2	Afrodita
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
posvátného	posvátný	k2eAgInSc2d1	posvátný
háje	háj	k1gInSc2	háj
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
háje	háj	k1gInSc2	háj
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amathus	Amathus	k1gInSc1	Amathus
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
pobřežním	pobřežní	k2eAgInSc6d1	pobřežní
útesu	útes	k1gInSc6	útes
s	s	k7c7	s
nedalekým	daleký	k2eNgInSc7d1	nedaleký
přírodním	přírodní	k2eAgInSc7d1	přírodní
přístavem	přístav	k1gInSc7	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
fázích	fáze	k1gFnPc6	fáze
historie	historie	k1gFnSc2	historie
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
prosperující	prosperující	k2eAgNnSc1d1	prosperující
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
městský	městský	k2eAgInSc1d1	městský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Levantou	Levanta	k1gFnSc7	Levanta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
především	především	k9	především
díky	díky	k7c3	díky
obilí	obilí	k1gNnSc3	obilí
<g/>
,	,	kIx,	,
měděným	měděný	k2eAgInPc3d1	měděný
dolům	dol	k1gInPc3	dol
a	a	k8xC	a
chovu	chov	k1gInSc3	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
skále	skála	k1gFnSc6	skála
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
chrám	chrám	k1gInSc1	chrám
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
Afroditě	Afrodita	k1gFnSc3	Afrodita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
patronku	patronka	k1gFnSc4	patronka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
stavba	stavba	k1gFnSc1	stavba
dalšího	další	k2eAgInSc2d1	další
chrámu	chrám	k1gInSc2	chrám
Afrodity	Afrodita	k1gFnSc2	Afrodita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
slavnosti	slavnost	k1gFnPc1	slavnost
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Adónise	Adónis	k1gMnSc2	Adónis
<g/>
.	.	kIx.	.
</s>
<s>
Sportovci	sportovec	k1gMnPc1	sportovec
zde	zde	k6eAd1	zde
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
divočáků	divočák	k1gMnPc2	divočák
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
tanci	tanec	k1gInSc6	tanec
a	a	k8xC	a
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
stavba	stavba	k1gFnSc1	stavba
chrámu	chrám	k1gInSc2	chrám
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
archeologické	archeologický	k2eAgInPc1d1	archeologický
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
hroby	hrob	k1gInPc4	hrob
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
(	(	kIx(	(
<g/>
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1000	[number]	k4	1000
až	až	k6eAd1	až
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
z	z	k7c2	z
období	období	k1gNnSc2	období
řecko-fénického	řeckoénický	k2eAgInSc2d1	řecko-fénický
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Amathus	Amathus	k1gMnSc1	Amathus
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
v	v	k7c6	v
análech	anály	k1gInPc6	anály
asyrského	asyrský	k2eAgMnSc4d1	asyrský
krále	král	k1gMnSc4	král
Asarhaddona	Asarhaddon	k1gMnSc4	Asarhaddon
(	(	kIx(	(
<g/>
668	[number]	k4	668
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mj.	mj.	kA	mj.
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
město	město	k1gNnSc4	město
Kar-Aššur-aha-iddina	Kar-Aššurhaddin	k2eAgNnSc2d1	Kar-Aššur-aha-iddin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amathus	Amathus	k1gInSc1	Amathus
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
pradávné	pradávný	k2eAgFnSc6d1	pradávná
historii	historie	k1gFnSc6	historie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
samostatné	samostatný	k2eAgNnSc1d1	samostatné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
jeho	jeho	k3xOp3gNnSc4	jeho
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
vzpouře	vzpoura	k1gFnSc3	vzpoura
proti	proti	k7c3	proti
Achaimenovcům	Achaimenovec	k1gMnPc3	Achaimenovec
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
probíhala	probíhat	k5eAaImAgNnP	probíhat
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
500	[number]	k4	500
až	až	k9	až
494	[number]	k4	494
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Město	město	k1gNnSc1	město
Amalthus	Amalthus	k1gInSc1	Amalthus
bylo	být	k5eAaImAgNnS	být
neúspěšně	úspěšně	k6eNd1	úspěšně
obléháno	obléhán	k2eAgNnSc1d1	obléháno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
jeho	jeho	k3xOp3gNnSc1	jeho
politický	politický	k2eAgInSc1d1	politický
význam	význam	k1gInSc1	význam
postupně	postupně	k6eAd1	postupně
ustupoval	ustupovat	k5eAaImAgInS	ustupovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
vzkvétajícímu	vzkvétající	k2eAgInSc3d1	vzkvétající
kultu	kult	k1gInSc3	kult
Afrodity	Afrodita	k1gFnSc2	Afrodita
a	a	k8xC	a
Adónise	Adónis	k1gMnSc4	Adónis
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
ještě	ještě	k9	ještě
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
"	"	kIx"	"
<g/>
amatusky	amatusky	k6eAd1	amatusky
<g/>
"	"	kIx"	"
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
poezii	poezie	k1gFnSc6	poezie
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
běžnější	běžný	k2eAgMnSc1d2	běžnější
než	než	k8xS	než
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
slávu	sláva	k1gFnSc4	sláva
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
začlenění	začlenění	k1gNnSc2	začlenění
do	do	k7c2	do
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
okresů	okres	k1gInPc2	okres
Kypru	Kypr	k1gInSc2	Kypr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Středověk	středověk	k1gInSc1	středověk
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
náboženský	náboženský	k2eAgInSc4d1	náboženský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
usadil	usadit	k5eAaPmAgMnS	usadit
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
biskup	biskup	k1gMnSc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
stále	stále	k6eAd1	stále
plné	plný	k2eAgFnSc2d1	plná
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
616	[number]	k4	616
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
budoucí	budoucí	k2eAgMnSc1d1	budoucí
alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
patriarcha	patriarcha	k1gMnSc1	patriarcha
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Milosrdný	milosrdný	k2eAgMnSc1d1	milosrdný
a	a	k8xC	a
roku	rok	k1gInSc2	rok
640	[number]	k4	640
řecký	řecký	k2eAgMnSc1d1	řecký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
mnich	mnich	k1gMnSc1	mnich
a	a	k8xC	a
opat	opat	k1gMnSc1	opat
Kláštera	klášter	k1gInSc2	klášter
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Anastasius	Anastasius	k1gMnSc1	Anastasius
Sinaita	Sinaita	k1gMnSc1	Sinaita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1191	[number]	k4	1191
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Richard	Richard	k1gMnSc1	Richard
I.	I.	kA	I.
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnPc4	srdce
obsadil	obsadit	k5eAaPmAgInS	obsadit
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
prázdné	prázdný	k2eAgNnSc1d1	prázdné
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
starověkých	starověký	k2eAgFnPc2d1	starověká
hrobek	hrobka	k1gFnPc2	hrobka
bylo	být	k5eAaImAgNnS	být
vypleněno	vyplenit	k5eAaPmNgNnS	vyplenit
a	a	k8xC	a
kameny	kámen	k1gInPc1	kámen
kdysi	kdysi	k6eAd1	kdysi
krásných	krásný	k2eAgInPc2d1	krásný
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
budov	budova	k1gFnPc2	budova
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
Limassolu	Limassol	k1gInSc2	Limassol
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nových	nový	k2eAgFnPc2d1	nová
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
staletích	staletí	k1gNnPc6	staletí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zbývající	zbývající	k2eAgInPc1d1	zbývající
kameny	kámen	k1gInPc1	kámen
města	město	k1gNnSc2	město
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
trosek	troska	k1gFnPc2	troska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgFnSc1d1	moderní
doba	doba	k1gFnSc1	doba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
Luigi	Luig	k1gFnSc2	Luig
di	di	k?	di
Palma	palma	k1gFnSc1	palma
Chesnola	Chesnola	k1gFnSc1	Chesnola
provedl	provést	k5eAaPmAgInS	provést
vykopávky	vykopávka	k1gFnSc2	vykopávka
v	v	k7c6	v
nekropoli	nekropole	k1gFnSc6	nekropole
města	město	k1gNnSc2	město
Amathus	Amathus	k1gInSc1	Amathus
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
nálezy	nález	k1gInPc1	nález
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
Britském	britský	k2eAgNnSc6d1	Britské
muzeu	muzeum	k1gNnSc6	muzeum
a	a	k8xC	a
v	v	k7c6	v
Metropolitním	metropolitní	k2eAgNnSc6d1	metropolitní
muzeu	muzeum	k1gNnSc6	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
výzkumy	výzkum	k1gInPc1	výzkum
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
objevili	objevit	k5eAaPmAgMnP	objevit
nekropoli	nekropole	k1gFnSc4	nekropole
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc4	chrám
Afrodity	Afrodita	k1gFnSc2	Afrodita
<g/>
,	,	kIx,	,
městský	městský	k2eAgInSc1d1	městský
trh	trh	k1gInSc1	trh
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
baziliku	bazilika	k1gFnSc4	bazilika
a	a	k8xC	a
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
nálezů	nález	k1gInPc2	nález
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
umístěny	umístit	k5eAaPmNgInP	umístit
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Nikósii	Nikósie	k1gFnSc6	Nikósie
a	a	k8xC	a
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Limassolu	Limassol	k1gInSc6	Limassol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
А	А	k?	А
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
