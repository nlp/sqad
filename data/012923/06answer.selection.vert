<s>
Amathus	Amathus	k1gMnSc1	Amathus
nebo	nebo	k8xC	nebo
Amafunt	Amafunt	k1gMnSc1	Amafunt
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
asi	asi	k9	asi
24	[number]	k4	24
mil	míle	k1gFnPc2	míle
západně	západně	k6eAd1	západně
od	od	k7c2	od
Larnaky	Larnak	k1gInPc5	Larnak
<g/>
.	.	kIx.	.
</s>
