<s>
Caipirinha	Caipirinha	k1gFnSc1	Caipirinha
je	být	k5eAaImIp3nS	být
brazilský	brazilský	k2eAgInSc4d1	brazilský
národní	národní	k2eAgInSc4d1	národní
koktejl	koktejl	k1gInSc4	koktejl
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
Cachaçy	Cachaça	k1gFnSc2	Cachaça
<g/>
,	,	kIx,	,
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
limety	limeta	k1gFnSc2	limeta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
historii	historie	k1gFnSc6	historie
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulu	Paula	k1gFnSc4	Paula
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
mocný	mocný	k2eAgInSc4d1	mocný
lék	lék	k1gInSc4	lék
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Caipirinha	Caipirinh	k1gMnSc2	Caipirinh
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
Caipira	Caipir	k1gInSc2	Caipir
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
venkovan	venkovan	k1gMnSc1	venkovan
nebo	nebo	k8xC	nebo
křupan	křupan	k?	křupan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
domorodém	domorodý	k2eAgNnSc6d1	domorodé
slově	slovo	k1gNnSc6	slovo
kaapira	kaapiro	k1gNnSc2	kaapiro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Tupi	Tup	k1gFnSc2	Tup
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
seká	sekat	k5eAaImIp3nS	sekat
křoví	křoví	k1gNnSc1	křoví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakrájené	nakrájený	k2eAgFnPc1d1	nakrájená
limetky	limetka	k1gFnPc1	limetka
rozmačkáme	rozmačkat	k5eAaPmIp1nP	rozmačkat
ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
<g/>
,	,	kIx,	,
přidáme	přidat	k5eAaPmIp1nP	přidat
třtinový	třtinový	k2eAgInSc4d1	třtinový
cukr	cukr	k1gInSc4	cukr
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1	[number]	k4	1
lžička	lžička	k1gFnSc1	lžička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zalijeme	zalít	k5eAaPmIp1nP	zalít
5	[number]	k4	5
cl	cl	k?	cl
cachaçy	cachaça	k1gFnSc2	cachaça
a	a	k8xC	a
sklenici	sklenice	k1gFnSc4	sklenice
naplníme	naplnit	k5eAaPmIp1nP	naplnit
rozdrceným	rozdrcený	k2eAgInSc7d1	rozdrcený
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
mojito	mojit	k2eAgNnSc1d1	mojito
daiquiri	daiquiri	k1gNnSc1	daiquiri
julep	julep	k1gMnSc1	julep
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Caipirinha	Caipirinh	k1gMnSc2	Caipirinh
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Příprava	příprava	k1gFnSc1	příprava
nápoje	nápoj	k1gInPc4	nápoj
</s>
