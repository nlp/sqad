<s>
Nakadžima	Nakadžima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
中	中	k?
二	二	k?
<g/>
,	,	kIx,
Nakadžima	Nakadžima	k1gFnSc1
Nišiki	Nišik	k1gFnSc2
Suidžó	Suidžó	k1gFnSc2
Sentóki	Sentók	k1gFnSc2
-	-	kIx~
Plovákový	plovákový	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Typ	typ	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
spojeneckém	spojenecký	k2eAgInSc6d1
kódu	kód	k1gInSc6
Rufe	Rufe	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
jednomístný	jednomístný	k2eAgInSc1d1
jednomotorový	jednomotorový	k2eAgInSc1d1
plovákový	plovákový	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
používalo	používat	k5eAaImAgNnS
japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>