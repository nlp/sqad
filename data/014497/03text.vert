<s>
Nakadžima	Nakadžima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc2
</s>
<s>
A6M2-N	A6M2-N	k4
Určení	určení	k1gNnSc1
</s>
<s>
plovákový	plovákový	k2eAgMnSc1d1
stíhací	stíhací	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Nakadžima	Nakadžima	k1gNnSc1
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Šinobu	Šinoba	k1gFnSc4
Micutake	Micutake	k1gFnPc2
První	první	k4xOgFnSc7
let	léto	k1gNnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1941	#num#	k4
Zařazeno	zařazen	k2eAgNnSc4d1
</s>
<s>
1942	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
327	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nakadžima	Nakadžima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
中	中	k?
二	二	k?
<g/>
,	,	kIx,
Nakadžima	Nakadžima	k1gFnSc1
Nišiki	Nišik	k1gFnSc2
Suidžó	Suidžó	k1gFnSc2
Sentóki	Sentók	k1gFnSc2
-	-	kIx~
Plovákový	plovákový	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Typ	typ	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
spojeneckém	spojenecký	k2eAgInSc6d1
kódu	kód	k1gInSc6
Rufe	Rufe	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
jednomístný	jednomístný	k2eAgInSc1d1
jednomotorový	jednomotorový	k2eAgInSc1d1
plovákový	plovákový	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
používalo	používat	k5eAaImAgNnS
japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
konstrukce	konstrukce	k1gFnSc2
vycházela	vycházet	k5eAaImAgNnP
z	z	k7c2
námořního	námořní	k2eAgInSc2d1
stíhacího	stíhací	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Micubiši	Micubiše	k1gFnSc4
A	a	k8xC
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Rufe	Rufe	k1gInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
ze	z	k7c2
stíhacího	stíhací	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Micubiši	Micubiš	k1gMnSc3
A6M2	A6M2	k1gMnSc1
Zero	Zero	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
účelem	účel	k1gInSc7
byla	být	k5eAaImAgFnS
podpora	podpora	k1gFnSc1
obojživelných	obojživelný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
a	a	k8xC
ochrana	ochrana	k1gFnSc1
vzdálených	vzdálený	k2eAgMnPc2d1
japonských	japonský	k2eAgMnPc2d1
základem	základ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
vzešel	vzejít	k5eAaPmAgInS
i	i	k8xC
typ	typ	k1gInSc1
Kawaniši	Kawaniš	k1gInSc6
N	N	kA
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
upraven	upravit	k5eAaPmNgInS
na	na	k7c4
úspěšný	úspěšný	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
základem	základ	k1gInSc7
byl	být	k5eAaImAgMnS
trup	trup	k1gMnSc1
A6M2	A6M2	k1gMnSc1
Model	model	k1gInSc4
11	#num#	k4
s	s	k7c7
modifikovaným	modifikovaný	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
a	a	k8xC
plováky	plovák	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvoření	vytvoření	k1gNnSc1
tohoto	tento	k3xDgInSc2
letounu	letoun	k1gInSc2
bylo	být	k5eAaImAgNnS
nápadem	nápad	k1gInSc7
Šinobua	Šinobu	k1gInSc2
Micutakeho	Micutake	k1gMnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
Nakadžimy	Nakadžima	k1gFnSc2
a	a	k8xC
konstruktéra	konstruktér	k1gMnSc2
Acušiho	Acuši	k1gMnSc2
Tadžimy	Tadžima	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
vzlétl	vzlétnout	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
1943	#num#	k4
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
327	#num#	k4
kusů	kus	k1gInPc2
včetně	včetně	k7c2
prototypu	prototyp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
velkého	velký	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
a	a	k8xC
dvou	dva	k4xCgInPc2
menších	malý	k2eAgInPc2d2
plováků	plovák	k1gInPc2
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
snížení	snížení	k1gNnSc3
výkonů	výkon	k1gInPc2
letounu	letoun	k1gInSc2
oproti	oproti	k7c3
Zeru	Zerus	k1gInSc3
o	o	k7c4
20	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgInS
Rufe	Rufe	k1gInSc4
v	v	k7c6
nevýhodě	nevýhoda	k1gFnSc6
při	při	k7c6
leteckém	letecký	k2eAgNnSc6d1
souboji	souboj	k1gInSc6
s	s	k7c7
jakýmkoliv	jakýkoliv	k3yIgMnSc7
spojeneckým	spojenecký	k2eAgMnSc7d1
stíhačem	stíhač	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Nasazení	nasazení	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
letounů	letoun	k1gInPc2
Nakadžima	Nakadžim	k1gMnSc2
A6M2-N	A6M2-N	k1gMnSc2
v	v	k7c6
Jižním	jižní	k2eAgInSc6d1
Pacifiku	Pacifik	k1gInSc6
</s>
<s>
Do	do	k7c2
služby	služba	k1gFnSc2
typ	typ	k1gInSc1
vstoupil	vstoupit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgInS
využit	využít	k5eAaPmNgInS
při	při	k7c6
obraně	obrana	k1gFnSc6
Aleut	Aleuty	k1gFnPc2
a	a	k8xC
Šalomounových	Šalomounův	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rufe	Ruf	k1gInSc2
se	se	k3xPyFc4
osvědčily	osvědčit	k5eAaPmAgInP
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
vyrušování	vyrušování	k1gNnSc6
amerických	americký	k2eAgInPc2d1
torpédových	torpédový	k2eAgInPc2d1
člunů	člun	k1gInPc2
PT	PT	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgNnSc1d1
odhalit	odhalit	k5eAaPmF
dokonce	dokonce	k9
i	i	k9
pro	pro	k7c4
prozatím	prozatím	k6eAd1
nepříliš	příliš	k6eNd1
dokonalé	dokonalý	k2eAgInPc1d1
japonské	japonský	k2eAgInPc1d1
radary	radar	k1gInPc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piloti	pilot	k1gMnPc1
A6M2-N	A6M2-N	k1gFnPc2
také	také	k9
používali	používat	k5eAaImAgMnP
světlice	světlice	k1gFnPc4
pro	pro	k7c4
osvětlení	osvětlení	k1gNnSc4
torpédových	torpédový	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
velice	velice	k6eAd1
zranitelné	zranitelný	k2eAgInPc1d1
při	při	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
torpédoborci	torpédoborec	k1gMnPc7
a	a	k8xC
tma	tma	k6eAd1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
byla	být	k5eAaImAgFnS
nutnou	nutný	k2eAgFnSc7d1
obranou	obrana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Letoun	letoun	k1gInSc1
sloužil	sloužit	k5eAaImAgInS
i	i	k9
jako	jako	k9
stíhací	stíhací	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
skladišť	skladiště	k1gNnPc2
paliva	palivo	k1gNnSc2
a	a	k8xC
letišť	letiště	k1gNnPc2
na	na	k7c6
Balikpapanu	Balikpapan	k1gInSc6
v	v	k7c6
Nizozemské	nizozemský	k2eAgFnSc6d1
Východní	východní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
a	a	k8xC
také	také	k9
v	v	k7c6
podobné	podobný	k2eAgFnSc6d1
roli	role	k1gFnSc6
na	na	k7c6
Kurilských	kurilský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Stíhačky	stíhačka	k1gFnPc4
Rufe	Ruf	k1gInSc2
nesl	nést	k5eAaImAgInS
nosič	nosič	k1gInSc1
hydroplánů	hydroplán	k1gInPc2
Kamikawa	Kamikawa	k1gFnSc1
Maru	Maru	k1gFnSc1
při	při	k7c6
operacích	operace	k1gFnPc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
Šalomounových	Šalomounův	k2eAgInPc2d1
a	a	k8xC
Kurilských	kurilský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
japonské	japonský	k2eAgInPc1d1
pomocné	pomocný	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Hokoku	Hokok	k1gInSc2
Maru	Maru	k1gFnSc2
a	a	k8xC
Aikoku	Aikok	k1gInSc2
Maru	Maru	k1gFnSc2
při	při	k7c6
výpadech	výpad	k1gInPc6
do	do	k7c2
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
během	během	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
používala	používat	k5eAaImAgFnS
letouny	letoun	k1gInPc4
Rufe	Rufe	k1gNnSc2
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
plovákovými	plovákový	k2eAgMnPc7d1
stíhači	stíhač	k1gMnPc7
Kawaniši	Kawaniš	k1gInSc6
N	N	kA
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
,	,	kIx,
letecká	letecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Ócu	Ócu	k1gFnSc2
při	při	k7c6
operacích	operace	k1gFnPc6
z	z	k7c2
hladiny	hladina	k1gFnSc2
jezera	jezero	k1gNnSc2
Biwa	Biw	k2eAgFnSc1d1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Honšú	Honšú	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Nakadžima	Nakadžima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc2
</s>
<s>
Údaje	údaj	k1gInPc1
dle	dle	k7c2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
12,00	12,00	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
9,05	9,05	k4
<g/>
m	m	kA
(	(	kIx(
<g/>
s	s	k7c7
plovákem	plovák	k1gInSc7
10,131	10,131	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
4,305	4,305	k4
m	m	kA
</s>
<s>
Plocha	plocha	k1gFnSc1
křídla	křídlo	k1gNnSc2
<g/>
:	:	kIx,
22,438	22,438	k4
m²	m²	k?
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
<g/>
:	:	kIx,
109,70	109,70	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
:	:	kIx,
1923	#num#	k4
kg	kg	kA
</s>
<s>
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
2883	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
×	×	k?
hvězdicový	hvězdicový	k2eAgInSc1d1
čtrnáctiválec	čtrnáctiválec	k1gInSc1
Nakadžima	Nakadžimum	k1gNnSc2
Sakae	Saka	k1gFnSc2
12	#num#	k4
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
940	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
701	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
436	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
v	v	k7c6
5000	#num#	k4
m	m	kA
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
296	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
12,3	12,3	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
9760	#num#	k4
m	m	kA
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
6	#num#	k4
min	min	kA
72	#num#	k4
s	s	k7c7
do	do	k7c2
výšky	výška	k1gFnSc2
5000	#num#	k4
m	m	kA
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
1778	#num#	k4
km	km	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
2	#num#	k4
×	×	k?
synchronizovaný	synchronizovaný	k2eAgInSc1d1
7,7	7,7	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
Typ	typ	k1gInSc1
97	#num#	k4
</s>
<s>
2	#num#	k4
×	×	k?
20	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
Typ	typ	k1gInSc1
99	#num#	k4
</s>
<s>
2	#num#	k4
×	×	k?
60	#num#	k4
<g/>
kg	kg	kA
puma	puma	k1gFnSc1
</s>
<s>
2	#num#	k4
×	×	k?
kanystrová	kanystrový	k2eAgFnSc1d1
fosforová	fosforový	k2eAgFnSc1d1
bomba	bomba	k1gFnSc1
Typ	typ	k1gInSc1
N3	N3	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Nakajima	Nakajim	k1gMnSc2
A6M2-N	A6M2-N	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
VALCHAŘ	valchař	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakadžima	Nakadžima	k1gNnSc1
A6M2-N	A6M2-N	k1gFnPc2
Suisen	Suisno	k1gNnPc2
2	#num#	k4
(	(	kIx(
<g/>
Rufe	Ruf	k1gInSc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FRANCILLON	FRANCILLON	kA
<g/>
,	,	kIx,
René	René	k1gMnSc1
J.	J.	kA
Japanese	Japanese	k1gFnPc1
Aircraft	Aircraft	k1gMnSc1
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Nakajima	Nakajima	k1gFnSc1
A	a	k9
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
-N	-N	k?
<g/>
,	,	kIx,
s.	s.	k?
426	#num#	k4
až	až	k9
428	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NĚMEČEK	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
(	(	kIx(
<g/>
Rufe	Rufe	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
LVIII	LVIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
271	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nakadžima	Nakadžim	k1gMnSc2
A6M2-N	A6M2-N	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Fotogalerie	Fotogalerie	k1gFnSc1
typu	typ	k1gInSc2
A6M2-N	A6M2-N	k1gFnSc2
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-10-22	2017-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
2	#num#	k4
Yokosuka	Yokosuk	k1gMnSc2
Kokutai	Kokutae	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-10-27	2017-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
3	#num#	k4
Yokohama	Yokohamum	k1gNnSc2
Kokutai	Kokutai	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-11-07	2017-11-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
4	#num#	k4
Aleutians	Aleutiansa	k1gFnPc2
Toko	toka	k1gFnSc5
Kokutai	Kokuta	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-11-19	2017-11-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
5	#num#	k4
Aleutians	Aleutians	k1gInSc1
5	#num#	k4
<g/>
th	th	k?
Kokutai	Kokutai	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-11-28	2017-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
6	#num#	k4
Aleutians	Aleutians	k1gInSc1
452	#num#	k4
<g/>
th	th	k?
Kokutai	Kokutai	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-12-03	2017-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
7	#num#	k4
Kuril	Kurily	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Chishima	Chishima	k1gFnSc1
Islands	Islands	k1gInSc1
452	#num#	k4
<g/>
th	th	k?
Kokutai	Kokutai	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-12-08	2017-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
8	#num#	k4
-	-	kIx~
Kimikawa	Kimikaw	k2eAgFnSc1d1
Maru	Maru	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-12-20	2017-12-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakajima	Nakajima	k1gFnSc1
A6M2-N	A6M2-N	k1gFnSc1
"	"	kIx"
<g/>
Rufe	Rufe	k1gFnSc1
<g/>
"	"	kIx"
pt	pt	k?
<g/>
.	.	kIx.
9	#num#	k4
-	-	kIx~
Kamikawa	Kamikaw	k2eAgFnSc1d1
Maru	Maru	k1gFnSc1
/	/	kIx~
Shortland	Shortland	k1gInSc1
Islands	Islands	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-01-08	2018-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Letadla	letadlo	k1gNnSc2
Nakadžima	Nakadžimum	k1gNnSc2
Letadla	letadlo	k1gNnSc2
Japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
</s>
<s>
Palubní	palubní	k2eAgInPc1d1
stíhací	stíhací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
A1N	A1N	k4
</s>
<s>
A2N	A2N	k4
</s>
<s>
A3N	A3N	k4
</s>
<s>
A4N	A4N	k4
Plovákové	plovákový	k2eAgInPc4d1
stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
A6M2-N	A6M2-N	k4
Palubní	palubní	k2eAgInPc4d1
torpédové	torpédový	k2eAgInPc4d1
bombardéry	bombardér	k1gInPc4
</s>
<s>
B3N	B3N	k4
</s>
<s>
B4N	B4N	k4
</s>
<s>
B5N	B5N	k4
</s>
<s>
B6N	B6N	k4
Průzkumné	průzkumný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
C2N	C2N	k4
</s>
<s>
C3N	C3N	k4
</s>
<s>
C6N	C6N	k4
Střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
</s>
<s>
D3N	D3N	k4
Plovákové	plovákový	k2eAgInPc4d1
průzkumné	průzkumný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
E2N	E2N	k4
</s>
<s>
E4N	E4N	k4
</s>
<s>
E8N	E8N	k4
Pozemní	pozemní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
</s>
<s>
G5N	G5N	k4
</s>
<s>
G8N	G8N	k4
</s>
<s>
G10N	G10N	k4
Záchytné	záchytný	k2eAgInPc4d1
stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
J1N	J1N	k4
</s>
<s>
J5N	J5N	k4
</s>
<s>
J9N	J9N	k4
Transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
L1N	L1N	k4
</s>
<s>
L2D	L2D	k4
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
LB-2	LB-2	k4
</s>
<s>
Letadla	letadlo	k1gNnPc1
Japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
armádního	armádní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1
typová	typový	k2eAgFnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
91	#num#	k4
</s>
<s>
průzkumný	průzkumný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
94	#num#	k4
</s>
<s>
cvičný	cvičný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
95	#num#	k4
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
97	#num#	k4
</s>
<s>
transportní	transportní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
97	#num#	k4
</s>
<s>
těžký	těžký	k2eAgInSc1d1
bombardér	bombardér	k1gInSc1
typ	typ	k1gInSc1
100	#num#	k4
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
1	#num#	k4
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
2	#num#	k4
</s>
<s>
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
4	#num#	k4
Čísla	číslo	k1gNnSc2
draků	drak	k1gMnPc2
(	(	kIx(
<g/>
Kitai	Kitai	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Ki-	Ki-	k?
<g/>
4	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
6	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
8	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
11	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
12	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
19	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
27	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
34	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
43	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
44	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
49	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
58	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
62	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
63	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
80	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
84	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
87	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
106	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
115	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
116	#num#	k4
</s>
<s>
Ki-	Ki-	k?
<g/>
201	#num#	k4
</s>
<s>
Podle	podle	k7c2
jmen	jméno	k1gNnPc2
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
</s>
<s>
Curudži	Curudzat	k5eAaPmIp1nS
</s>
<s>
Donrjú	Donrjú	k?
</s>
<s>
Fugaku	Fugak	k1gMnSc3
</s>
<s>
Gekkó	Gekkó	k?
</s>
<s>
Hajabusa	Hajabus	k1gMnSc4
</s>
<s>
Hajaté	Hajat	k1gMnPc1
</s>
<s>
Karjú	Karjú	k?
</s>
<s>
Kikka	Kikka	k6eAd1
</s>
<s>
Renzan	Renzan	k1gMnSc1
</s>
<s>
Saiun	Saiun	k1gMnSc1
</s>
<s>
Šinzan	Šinzan	k1gMnSc1
</s>
<s>
Šóki	Šóki	k6eAd1
</s>
<s>
Tenrai	Tenrai	k6eAd1
</s>
<s>
Tenzan	Tenzany	k1gInPc2
Spojenecká	spojenecký	k2eAgNnPc4d1
kódová	kódový	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
</s>
<s>
Abdul	Abdul	k1gMnSc1
</s>
<s>
Dave	Dav	k1gInSc5
</s>
<s>
Frank	Frank	k1gMnSc1
</s>
<s>
Helen	Helena	k1gFnPc2
</s>
<s>
Irving	Irving	k1gInSc1
</s>
<s>
Jill	Jill	k1gMnSc1
</s>
<s>
Jim	on	k3xPp3gMnPc3
</s>
<s>
John	John	k1gMnSc1
</s>
<s>
Kate	kat	k1gMnSc5
</s>
<s>
Liz	liz	k1gInSc1
</s>
<s>
Mabel	Mabel	k1gMnSc1
</s>
<s>
Myrt	myrta	k1gFnPc2
</s>
<s>
Nate	Nate	k6eAd1
</s>
<s>
Oscar	Oscar	k1gInSc1
</s>
<s>
Rita	Rita	k1gFnSc1
</s>
<s>
Rufe	Rufe	k6eAd1
</s>
<s>
Tabby	Tabba	k1gFnPc1
</s>
<s>
Thora	Thora	k6eAd1
</s>
<s>
Tojo	Tojo	k6eAd1
</s>
<s>
Hydroplány	hydroplán	k1gInPc1
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
Před	před	k7c7
zavedením	zavedení	k1gNnSc7
krátkého	krátký	k2eAgNnSc2d1
značení	značení	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
MF	MF	kA
<g/>
.7	.7	k4
•	•	k?
MF	MF	kA
<g/>
.11	.11	k4
•	•	k?
F.	F.	kA
<g/>
5	#num#	k4
•	•	k?
Jokosuka	Jokosuk	k1gMnSc4
Ro-gó	Ro-gó	k1gFnSc2
Kó-gata	Kó-gata	k1gFnSc1
•	•	k?
W	W	kA
<g/>
.29	.29	k4
•	•	k?
HD	HD	kA
25	#num#	k4
Průzkumné	průzkumný	k2eAgInPc1d1
hydroplány	hydroplán	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
E1Y	E1Y	k4
•	•	k?
E2N	E2N	k1gMnSc1
•	•	k?
E3A	E3A	k1gMnSc1
•	•	k?
E4N	E4N	k1gMnSc1
•	•	k?
E	E	kA
<g/>
5	#num#	k4
<g/>
Y	Y	kA
<g/>
/	/	kIx~
<g/>
E	E	kA
<g/>
5	#num#	k4
<g/>
K	k	k7c3
•	•	k?
E7K	E7K	k1gMnSc3
•	•	k?
E8N	E8N	k1gMnSc1
•	•	k?
E9W	E9W	k1gMnSc1
•	•	k?
E10A	E10A	k1gMnSc1
•	•	k?
E11A	E11A	k1gMnSc1
•	•	k?
E13A	E13A	k1gMnSc1
•	•	k?
E14Y	E14Y	k1gMnSc1
•	•	k?
E15K	E15K	k1gMnSc1
(	(	kIx(
<g/>
Šiun	Šiun	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
E16A	E16A	k1gFnSc1
(	(	kIx(
<g/>
Zuiun	Zuiun	k1gNnSc1
<g/>
)	)	kIx)
Pozorovací	pozorovací	k2eAgInPc1d1
hydroplány	hydroplán	k1gInPc1
</s>
<s>
F1M	F1M	k4
Létající	létající	k2eAgInPc1d1
čluny	člun	k1gInPc1
</s>
<s>
H1H	H1H	k4
•	•	k?
H2H	H2H	k1gMnSc1
•	•	k?
H3K	H3K	k1gMnSc1
•	•	k?
H4H	H4H	k1gMnSc1
•	•	k?
H5Y	H5Y	k1gMnSc1
•	•	k?
H6K	H6K	k1gMnSc1
•	•	k?
H8K	H8K	k1gMnSc1
•	•	k?
H9A	H9A	k1gMnSc1
Speciální	speciální	k2eAgInPc4d1
hydroplány	hydroplán	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
M6A	M6A	k4
(	(	kIx(
<g/>
Seiran	Seirana	k1gFnPc2
<g/>
)	)	kIx)
Stíhací	stíhací	k2eAgInPc1d1
hydroplány	hydroplán	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
A6M2-N	A6M2-N	k4
•	•	k?
N1K	N1K	k1gMnSc1
(	(	kIx(
<g/>
Kjófú	Kjófú	k1gFnSc1
<g/>
)	)	kIx)
Cvičné	cvičný	k2eAgInPc1d1
hydroplány	hydroplán	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
K1Y	K1Y	k4
•	•	k?
K4Y	K4Y	k1gMnSc1
•	•	k?
K5Y	K5Y	k1gMnSc1
</s>
<s>
Letouny	letoun	k1gInPc1
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
A4N	A4N	k4
•	•	k?
A5M	A5M	k1gMnSc1
•	•	k?
A6M	A6M	k1gMnSc1
•	•	k?
A6M2-N	A6M2-N	k1gMnSc1
•	•	k?
A7M	A7M	k1gMnSc1
•	•	k?
J1N	J1N	k1gMnSc1
•	•	k?
J2M	J2M	k1gMnSc1
•	•	k?
J5N	J5N	k1gMnSc1
•	•	k?
N1K	N1K	k1gMnSc1
Bombardovací	bombardovací	k2eAgInPc4d1
a	a	k8xC
torpédové	torpédový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
B5M	B5M	k4
•	•	k?
B5N	B5N	k1gMnSc1
•	•	k?
B6N	B6N	k1gMnSc1
•	•	k?
B7A	B7A	k1gMnSc1
•	•	k?
D1A	D1A	k1gMnSc1
•	•	k?
D3A	D3A	k1gMnSc1
•	•	k?
D4Y	D4Y	k1gMnSc1
•	•	k?
G3M	G3M	k1gMnSc1
•	•	k?
G4M	G4M	k1gMnSc1
•	•	k?
G5N	G5N	k1gMnSc1
•	•	k?
G8N	G8N	k1gMnSc1
•	•	k?
M6A	M6A	k1gMnSc1
•	•	k?
P1Y	P1Y	k1gMnSc1
Průzkumné	průzkumný	k2eAgInPc4d1
a	a	k8xC
spojovací	spojovací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
C3N	C3N	k4
•	•	k?
C5M	C5M	k1gMnSc1
•	•	k?
C6N	C6N	k1gMnSc1
•	•	k?
E7K	E7K	k1gMnSc1
•	•	k?
E8N	E8N	k1gMnSc1
•	•	k?
E11A	E11A	k1gMnSc1
•	•	k?
E13A	E13A	k1gMnSc1
•	•	k?
E14Y	E14Y	k1gMnSc1
•	•	k?
E15K	E15K	k1gMnSc1
•	•	k?
E16A	E16A	k1gMnSc1
•	•	k?
F1M	F1M	k1gMnSc1
•	•	k?
H5Y	H5Y	k1gMnSc1
•	•	k?
H6K	H6K	k1gMnSc1
•	•	k?
H8K	H8K	k1gMnSc1
•	•	k?
H9A	H9A	k1gMnSc1
•	•	k?
Q1W	Q1W	k1gMnSc1
Zbývající	zbývající	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Baika	Baika	k1gFnSc1
•	•	k?
MXY	MXY	kA
5	#num#	k4
•	•	k?
MXY7	MXY7	k1gMnSc1
Transportní	transportní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
L2D	L2D	k4
•	•	k?
L3Y	L3Y	k1gMnSc1
•	•	k?
L4M	L4M	k1gMnSc1
Experimentální	experimentální	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
</s>
<s>
J7W	J7W	k4
•	•	k?
J8M	J8M	k1gMnSc1
•	•	k?
J9N	J9N	k1gMnSc1
•	•	k?
R2Y	R2Y	k1gMnSc1
•	•	k?
S1A	S1A	k1gFnSc2
Značení	značení	k1gNnSc2
japonských	japonský	k2eAgNnPc2d1
vojenských	vojenský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
