<s>
Wellington	Wellington	k1gInSc1	Wellington
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
maorsky	maorsky	k6eAd1	maorsky
<g/>
:	:	kIx,	:
Te	Te	k1gFnSc1	Te
Whanganui-á-Tara	Whanganui-á-Tara	k1gFnSc1	Whanganui-á-Tara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
významný	významný	k2eAgInSc1d1	významný
přístav	přístav	k1gInSc1	přístav
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
Severního	severní	k2eAgInSc2d1	severní
ostrova	ostrov	k1gInSc2	ostrov
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Cookova	Cookův	k2eAgInSc2d1	Cookův
průlivu	průliv	k1gInSc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejjižněji	jižně	k6eAd3	jižně
položené	položený	k2eAgNnSc4d1	položené
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
okolo	okolo	k7c2	okolo
180	[number]	k4	180
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
městy	město	k1gNnPc7	město
Lower	Lower	k1gMnSc1	Lower
Hutt	Hutt	k1gMnSc1	Hutt
<g/>
,	,	kIx,	,
Upper	Upper	k1gMnSc1	Upper
Hutt	Hutt	k1gMnSc1	Hutt
a	a	k8xC	a
Porirua	Porirua	k1gFnSc1	Porirua
tvoří	tvořit	k5eAaImIp3nS	tvořit
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
aglomeraci	aglomerace	k1gFnSc4	aglomerace
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
kolem	kolem	k7c2	kolem
386	[number]	k4	386
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
anglickými	anglický	k2eAgMnPc7d1	anglický
osadníky	osadník	k1gMnPc7	osadník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
status	status	k1gInSc4	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
převzalo	převzít	k5eAaPmAgNnS	převzít
po	po	k7c6	po
Waitangi	Waitang	k1gInSc6	Waitang
a	a	k8xC	a
Aucklandu	Auckland	k1gInSc6	Auckland
o	o	k7c4	o
25	[number]	k4	25
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
sídlí	sídlet	k5eAaImIp3nS	sídlet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
státní	státní	k2eAgFnSc1d1	státní
instituce	instituce	k1gFnSc1	instituce
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jako	jako	k8xS	jako
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
přístav	přístav	k1gInSc1	přístav
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
tektonické	tektonický	k2eAgFnSc3d1	tektonická
aktivitě	aktivita	k1gFnSc3	aktivita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
byl	být	k5eAaImAgInS	být
Wellington	Wellington	k1gInSc1	Wellington
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
několikrát	několikrát	k6eAd1	několikrát
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
silnými	silný	k2eAgNnPc7d1	silné
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
desetimetrovou	desetimetrový	k2eAgFnSc4d1	desetimetrová
tsunami	tsunami	k1gNnSc3	tsunami
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
srovnala	srovnat	k5eAaPmAgFnS	srovnat
wellingtonský	wellingtonský	k2eAgInSc4d1	wellingtonský
přístav	přístav	k1gInSc4	přístav
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vyrovnané	vyrovnaný	k2eAgInPc4d1	vyrovnaný
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
budují	budovat	k5eAaImIp3nP	budovat
větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Wellingtonu	Wellington	k1gInSc2	Wellington
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
původních	původní	k2eAgMnPc2d1	původní
novozélandských	novozélandský	k2eAgMnPc2d1	novozélandský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
Maorů	Maor	k1gMnPc2	Maor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
jejich	jejich	k3xOp3gNnSc3	jejich
průměrnému	průměrný	k2eAgNnSc3d1	průměrné
zastoupení	zastoupení	k1gNnSc3	zastoupení
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
celého	celý	k2eAgInSc2d1	celý
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
poměrně	poměrně	k6eAd1	poměrně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgNnSc1d1	aktivní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
populace	populace	k1gFnSc2	populace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
je	být	k5eAaImIp3nS	být
i	i	k9	i
úroveň	úroveň	k1gFnSc4	úroveň
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
a	a	k8xC	a
roční	roční	k2eAgInPc1d1	roční
příjmy	příjem	k1gInPc1	příjem
wellingtonských	wellingtonský	k2eAgFnPc2d1	wellingtonský
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
Wellingtonu	Wellington	k1gInSc2	Wellington
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dvě	dva	k4xCgFnPc1	dva
státní	státní	k2eAgFnPc1d1	státní
dálnice	dálnice	k1gFnPc1	dálnice
<g/>
,	,	kIx,	,
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ročně	ročně	k6eAd1	ročně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
region	region	k1gInSc1	region
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
bohatou	bohatý	k2eAgFnSc4d1	bohatá
síť	síť	k1gFnSc4	síť
veřejné	veřejný	k2eAgFnSc2d1	veřejná
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
zejména	zejména	k9	zejména
autobusové	autobusový	k2eAgInPc4d1	autobusový
spoje	spoj	k1gInPc4	spoj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
trajekty	trajekt	k1gInPc1	trajekt
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stoletou	stoletý	k2eAgFnSc7d1	stoletá
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Služeb	služba	k1gFnPc2	služba
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
využívá	využívat	k5eAaPmIp3nS	využívat
každoročně	každoročně	k6eAd1	každoročně
přes	přes	k7c4	přes
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Wellingtonu	Wellington	k1gInSc2	Wellington
připluli	připlout	k5eAaPmAgMnP	připlout
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1839	[number]	k4	1839
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
jménem	jméno	k1gNnSc7	jméno
Tory	torus	k1gInPc4	torus
patřící	patřící	k2eAgFnSc3d1	patřící
Novozélandské	novozélandský	k2eAgFnSc3d1	novozélandská
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgFnSc6d1	anglická
organizaci	organizace	k1gFnSc6	organizace
věnující	věnující	k2eAgFnSc6d1	věnující
se	se	k3xPyFc4	se
systematické	systematický	k2eAgFnSc3d1	systematická
kolonizaci	kolonizace	k1gFnSc3	kolonizace
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Aurora	Aurora	k1gFnSc1	Aurora
dalších	další	k2eAgInPc2d1	další
150	[number]	k4	150
osadníků	osadník	k1gMnPc2	osadník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
stavěny	stavit	k5eAaImNgInP	stavit
v	v	k7c6	v
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Hutt	Hutta	k1gFnPc2	Hutta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nachází	nacházet	k5eAaImIp3nS	nacházet
Petone	Peton	k1gInSc5	Peton
<g/>
,	,	kIx,	,
šestitisícové	šestitisícový	k2eAgNnSc4d1	šestitisícové
předměstí	předměstí	k1gNnSc4	předměstí
Lower	Lower	k1gMnSc1	Lower
Hutt	Hutt	k1gMnSc1	Hutt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesunutí	přesunutí	k1gNnSc3	přesunutí
nově	nově	k6eAd1	nově
budované	budovaný	k2eAgFnSc2d1	budovaná
osady	osada	k1gFnSc2	osada
více	hodně	k6eAd2	hodně
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
přiměl	přimět	k5eAaPmAgInS	přimět
osadníky	osadník	k1gMnPc4	osadník
bažinatý	bažinatý	k2eAgInSc4d1	bažinatý
terén	terén	k1gInSc4	terén
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
obydlí	obydlí	k1gNnPc2	obydlí
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
v	v	k7c6	v
záplavové	záplavový	k2eAgFnSc6d1	záplavová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
osadníky	osadník	k1gMnPc4	osadník
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
podle	podle	k7c2	podle
Arthura	Arthur	k1gMnSc2	Arthur
Wellesleyho	Wellesley	k1gMnSc2	Wellesley
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
a	a	k8xC	a
vítěze	vítěz	k1gMnSc4	vítěz
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
titul	titul	k1gInSc1	titul
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
městečka	městečko	k1gNnSc2	městečko
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Somersetu	Somerset	k1gInSc6	Somerset
<g/>
.	.	kIx.	.
</s>
<s>
Maorský	maorský	k2eAgInSc1d1	maorský
název	název	k1gInSc1	název
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Wellington	Wellington	k1gInSc4	Wellington
zní	znět	k5eAaImIp3nS	znět
Te	Te	k1gFnSc1	Te
Whanga-nui-a-Tara	Whangaui-Tara	k1gFnSc1	Whanga-nui-a-Tara
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
přístav	přístav	k1gInSc1	přístav
Tary	Tara	k1gFnSc2	Tara
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pō	Pō	k1gMnSc1	Pō
nebo	nebo	k8xC	nebo
Te	Te	k1gMnSc1	Te
Upoko-o-te-Ika-a-Mā	Upokoe-Ika-Mā	k1gMnSc1	Upoko-o-te-Ika-a-Mā
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hlava	hlava	k1gFnSc1	hlava
Máuiho	Máui	k1gMnSc2	Máui
ryby	ryba	k1gFnSc2	ryba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tradičním	tradiční	k2eAgNnSc7d1	tradiční
označením	označení	k1gNnSc7	označení
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
Severního	severní	k2eAgInSc2d1	severní
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
legendě	legenda	k1gFnSc3	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
Severní	severní	k2eAgInSc1d1	severní
ostrov	ostrov	k1gInSc1	ostrov
vyloven	vylovit	k5eAaPmNgInS	vylovit
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
polobohem	polobůh	k1gMnSc7	polobůh
Máuim	Máuima	k1gFnPc2	Máuima
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
Severního	severní	k2eAgInSc2d1	severní
ostrova	ostrov	k1gInSc2	ostrov
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Cookova	Cookův	k2eAgInSc2d1	Cookův
průlivu	průliv	k1gInSc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
město	město	k1gNnSc4	město
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ideální	ideální	k2eAgFnSc4d1	ideální
polohu	poloha	k1gFnSc4	poloha
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
kopce	kopec	k1gInPc1	kopec
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
již	již	k9	již
také	také	k9	také
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
městská	městský	k2eAgFnSc1d1	městská
zástavba	zástavba	k1gFnSc1	zástavba
a	a	k8xC	a
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
jižní	jižní	k2eAgFnSc7d1	jižní
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
41	[number]	k4	41
<g/>
°	°	k?	°
<g/>
17	[number]	k4	17
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Guinnessovy	Guinnessův	k2eAgFnSc2d1	Guinnessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
nejjižněji	jižně	k6eAd3	jižně
položeným	položený	k2eAgNnSc7d1	položené
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
prostoru	prostor	k1gInSc3	prostor
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
mezi	mezi	k7c7	mezi
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
okolními	okolní	k2eAgInPc7d1	okolní
kopci	kopec	k1gInPc7	kopec
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
porovnání	porovnání	k1gNnSc4	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
městy	město	k1gNnPc7	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
městy	město	k1gNnPc7	město
Lower	Lower	k1gMnSc1	Lower
Hutt	Hutt	k1gMnSc1	Hutt
<g/>
,	,	kIx,	,
Upper	Upper	k1gMnSc1	Upper
Hutt	Hutt	k1gMnSc1	Hutt
a	a	k8xC	a
Porirua	Porirua	k1gMnSc1	Porirua
tvoří	tvořit	k5eAaImIp3nS	tvořit
Wellington	Wellington	k1gInSc1	Wellington
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
aglomeraci	aglomerace	k1gFnSc4	aglomerace
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
386	[number]	k4	386
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
wellingtonského	wellingtonský	k2eAgInSc2d1	wellingtonský
přístavu	přístav	k1gInSc2	přístav
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Someský	Someský	k2eAgInSc1d1	Someský
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
domorodci	domorodec	k1gMnPc1	domorodec
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
Matiu	Matius	k1gMnSc6	Matius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
využíván	využíván	k2eAgInSc1d1	využíván
jako	jako	k8xC	jako
karanténní	karanténní	k2eAgFnSc1d1	karanténní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
za	za	k7c2	za
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jako	jako	k8xS	jako
internační	internační	k2eAgInSc1d1	internační
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
německé	německý	k2eAgNnSc4d1	německé
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
japonské	japonský	k2eAgNnSc4d1	Japonské
a	a	k8xC	a
italské	italský	k2eAgNnSc4d1	italské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
pro	pro	k7c4	pro
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
živočišné	živočišný	k2eAgInPc4d1	živočišný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Makaro	Makara	k1gFnSc5	Makara
a	a	k8xC	a
Mokopuna	Mokopuna	k1gFnSc1	Mokopuna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
neobydlené	obydlený	k2eNgFnPc1d1	neobydlená
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
Novým	nový	k2eAgInSc7d1	nový
Zélandem	Zéland	k1gInSc7	Zéland
má	mít	k5eAaImIp3nS	mít
Wellington	Wellington	k1gInSc4	Wellington
typické	typický	k2eAgNnSc1d1	typické
oceánické	oceánický	k2eAgNnSc1d1	oceánické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
jsou	být	k5eAaImIp3nP	být
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
poměrně	poměrně	k6eAd1	poměrně
vyrovnané	vyrovnaný	k2eAgFnPc1d1	vyrovnaná
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
až	až	k8xS	až
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
dvaceti	dvacet	k4xCc2	dvacet
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
překročí	překročit	k5eAaPmIp3nS	překročit
hranici	hranice	k1gFnSc4	hranice
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
obvykle	obvykle	k6eAd1	obvykle
teploty	teplota	k1gFnSc2	teplota
stoupají	stoupat	k5eAaImIp3nP	stoupat
maximálně	maximálně	k6eAd1	maximálně
ke	k	k7c3	k
14	[number]	k4	14
stupňům	stupeň	k1gInPc3	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
nepropadají	propadat	k5eNaImIp3nP	propadat
se	se	k3xPyFc4	se
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
stupně	stupeň	k1gInPc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
byla	být	k5eAaImAgFnS	být
31,1	[number]	k4	31,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
1,9	[number]	k4	1,9
stupně	stupeň	k1gInSc2	stupeň
pod	pod	k7c7	pod
nulou	nula	k1gFnSc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Úhrn	úhrn	k1gInSc1	úhrn
ročních	roční	k2eAgFnPc2d1	roční
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
poměrně	poměrně	k6eAd1	poměrně
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
nejvíce	nejvíce	k6eAd1	nejvíce
prší	pršet	k5eAaImIp3nP	pršet
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ke	k	k7c3	k
147	[number]	k4	147
milimetrům	milimetr	k1gInPc3	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
společně	společně	k6eAd1	společně
s	s	k7c7	s
červencem	červenec	k1gInSc7	červenec
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvlhčím	vlhký	k2eAgInPc3d3	nejvlhčí
měsícům	měsíc	k1gInPc3	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
naprší	napršet	k5eAaPmIp3nS	napršet
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1249	[number]	k4	1249
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
objevuje	objevovat	k5eAaImIp3nS	objevovat
jinovatka	jinovatka	k1gFnSc1	jinovatka
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
větrné	větrný	k2eAgNnSc1d1	větrné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
celoroční	celoroční	k2eAgFnSc2d1	celoroční
tlakové	tlakový	k2eAgFnSc2d1	tlaková
níže	níže	k1gFnSc2	níže
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k6eAd1	kolem
čtyřicátého	čtyřicátý	k4xOgInSc2	čtyřicátý
stupně	stupeň	k1gInSc2	stupeň
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vystaven	vystavit	k5eAaPmNgInS	vystavit
západnímu	západní	k2eAgInSc3d1	západní
proudění	proudění	k1gNnSc3	proudění
přicházejícímu	přicházející	k2eAgInSc3d1	přicházející
k	k	k7c3	k
městu	město	k1gNnSc3	město
z	z	k7c2	z
Tasmanova	Tasmanův	k2eAgNnSc2d1	Tasmanovo
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Větrné	větrný	k2eAgNnSc1d1	větrné
podnebí	podnebí	k1gNnSc1	podnebí
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
projekt	projekt	k1gInSc1	projekt
West	Westa	k1gFnPc2	Westa
Wind	Wind	k1gInSc1	Wind
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
62	[number]	k4	62
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
kapacitou	kapacita	k1gFnSc7	kapacita
143	[number]	k4	143
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
podobným	podobný	k2eAgInSc7d1	podobný
projektem	projekt	k1gInSc7	projekt
je	být	k5eAaImIp3nS	být
Mill	Mill	k1gInSc1	Mill
Creek	Creek	k1gInSc1	Creek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vybudovat	vybudovat	k5eAaPmF	vybudovat
dalších	další	k2eAgFnPc2d1	další
31	[number]	k4	31
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
tektonicky	tektonicky	k6eAd1	tektonicky
aktivní	aktivní	k2eAgFnSc6d1	aktivní
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
a	a	k8xC	a
Australské	australský	k2eAgFnSc2d1	australská
litosférické	litosférický	k2eAgFnSc2d1	litosférická
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
oblasti	oblast	k1gFnSc2	oblast
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
hojným	hojný	k2eAgInSc7d1	hojný
výskytem	výskyt	k1gInSc7	výskyt
sopek	sopka	k1gFnPc2	sopka
a	a	k8xC	a
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Nejinak	nejinak	k6eAd1	nejinak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
Wellington	Wellington	k1gInSc1	Wellington
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
prochází	procházet	k5eAaImIp3nS	procházet
velký	velký	k2eAgInSc1d1	velký
tektonický	tektonický	k2eAgInSc1d1	tektonický
zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
výraznějších	výrazný	k2eAgInPc2d2	výraznější
zlomů	zlom	k1gInPc2	zlom
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
menších	malý	k2eAgFnPc2d2	menší
tektonických	tektonický	k2eAgFnPc2d1	tektonická
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Wellingtonu	Wellington	k1gInSc2	Wellington
každoročně	každoročně	k6eAd1	každoročně
několik	několik	k4yIc1	několik
otřesů	otřes	k1gInPc2	otřes
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
nejpatrnější	patrný	k2eAgInPc1d3	Nejpatrnější
ve	v	k7c6	v
výškových	výškový	k2eAgFnPc6d1	výšková
budovách	budova	k1gFnPc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
škody	škoda	k1gFnPc4	škoda
způsobila	způsobit	k5eAaPmAgFnS	způsobit
Wellingtonu	Wellington	k1gInSc2	Wellington
série	série	k1gFnSc1	série
otřesů	otřes	k1gInPc2	otřes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
velké	velký	k2eAgNnSc4d1	velké
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zrovna	zrovna	k9	zrovna
probíhaly	probíhat	k5eAaImAgFnP	probíhat
oslavy	oslava	k1gFnPc1	oslava
15	[number]	k4	15
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
jeho	on	k3xPp3gNnSc2	on
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
síly	síla	k1gFnPc4	síla
8,2	[number]	k4	8,2
stupně	stupeň	k1gInSc2	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
vertikální	vertikální	k2eAgInSc4d1	vertikální
pohyb	pohyb	k1gInSc4	pohyb
zeminy	zemina	k1gFnSc2	zemina
až	až	k9	až
o	o	k7c4	o
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Wellingtonský	Wellingtonský	k2eAgInSc4d1	Wellingtonský
přístav	přístav	k1gInSc4	přístav
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
desetimetrová	desetimetrový	k2eAgFnSc1d1	desetimetrová
vlna	vlna	k1gFnSc1	vlna
tsunami	tsunami	k1gNnSc1	tsunami
přicházející	přicházející	k2eAgFnSc1d1	přicházející
z	z	k7c2	z
Cookova	Cookův	k2eAgInSc2d1	Cookův
průlivu	průliv	k1gInSc2	průliv
a	a	k8xC	a
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ho	on	k3xPp3gInSc4	on
v	v	k7c4	v
bahnitý	bahnitý	k2eAgInSc4d1	bahnitý
močál	močál	k1gInSc4	močál
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
kamenné	kamenný	k2eAgFnPc1d1	kamenná
budovy	budova	k1gFnPc1	budova
věznice	věznice	k1gFnSc2	věznice
a	a	k8xC	a
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
otřesy	otřes	k1gInPc1	otřes
neustála	ustát	k5eNaPmAgFnS	ustát
ani	ani	k8xC	ani
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
budova	budova	k1gFnSc1	budova
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
kanceláře	kancelář	k1gFnSc2	kancelář
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
domů	dům	k1gInPc2	dům
však	však	k9	však
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
či	či	k8xC	či
menším	malý	k2eAgNnSc7d2	menší
poškozením	poškození	k1gNnSc7	poškození
ustála	ustát	k5eAaPmAgFnS	ustát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
stavěly	stavět	k5eAaImAgFnP	stavět
zejména	zejména	k9	zejména
jednopatrové	jednopatrový	k2eAgFnPc1d1	jednopatrová
stavby	stavba	k1gFnPc1	stavba
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
o	o	k7c4	o
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
zvedl	zvednout	k5eAaPmAgInS	zvednout
terén	terén	k1gInSc1	terén
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
zatopena	zatopen	k2eAgFnSc1d1	zatopena
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
wellingtonskému	wellingtonský	k2eAgInSc3d1	wellingtonský
přístavu	přístav	k1gInSc3	přístav
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
významně	významně	k6eAd1	významně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
CBD	CBD	kA	CBD
-	-	kIx~	-
Central	Central	k1gMnSc2	Central
Business	business	k1gInSc1	business
District	District	k2eAgInSc1d1	District
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
například	například	k6eAd1	například
ulice	ulice	k1gFnSc1	ulice
Lambton	Lambton	k1gInSc4	Lambton
Quay	Quaa	k1gFnSc2	Quaa
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vedoucí	vedoucí	k1gMnPc1	vedoucí
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
často	často	k6eAd1	často
zaplavovaná	zaplavovaný	k2eAgFnSc1d1	zaplavovaná
přílivem	příliv	k1gInSc7	příliv
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
srdcem	srdce	k1gNnSc7	srdce
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Wellingtonu	Wellington	k1gInSc2	Wellington
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
asi	asi	k9	asi
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandský	novozélandský	k2eAgInSc1d1	novozélandský
parlament	parlament	k1gInSc1	parlament
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
poprvé	poprvé	k6eAd1	poprvé
zasedal	zasedat	k5eAaImAgInS	zasedat
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
Wellington	Wellington	k1gInSc1	Wellington
oficiálně	oficiálně	k6eAd1	oficiálně
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1863	[number]	k4	1863
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
při	při	k7c6	při
zasedání	zasedání	k1gNnSc6	zasedání
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Aucklandu	Auckland	k1gInSc6	Auckland
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Alfred	Alfred	k1gMnSc1	Alfred
Domett	Domett	k1gInSc4	Domett
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nevyhnutelným	vyhnutelný	k2eNgMnSc7d1	nevyhnutelný
<g/>
,	,	kIx,	,
že	že	k8xS	že
sídlo	sídlo	k1gNnSc1	sídlo
vlády	vláda	k1gFnSc2	vláda
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
vhodnější	vhodný	k2eAgFnSc2d2	vhodnější
lokality	lokalita	k1gFnSc2	lokalita
u	u	k7c2	u
Cookova	Cookův	k2eAgInSc2d1	Cookův
průlivu	průliv	k1gInSc2	průliv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Panovaly	panovat	k5eAaImAgFnP	panovat
totiž	totiž	k9	totiž
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
jižní	jižní	k2eAgFnSc6d1	jižní
oblasti	oblast	k1gFnSc6	oblast
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
naleziště	naleziště	k1gNnSc2	naleziště
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
nejvhodnější	vhodný	k2eAgFnSc2d3	nejvhodnější
oblasti	oblast	k1gFnSc2	oblast
byli	být	k5eAaImAgMnP	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
nezávislosti	nezávislost	k1gFnSc3	nezávislost
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
komisaři	komisar	k1gMnPc1	komisar
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ty	k3xPp2nSc3	ty
za	za	k7c4	za
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
lokalitu	lokalita	k1gFnSc4	lokalita
zvolili	zvolit	k5eAaPmAgMnP	zvolit
Wellington	Wellington	k1gInSc4	Wellington
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
strategickému	strategický	k2eAgInSc3d1	strategický
přístavu	přístav	k1gInSc3	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
když	když	k8xS	když
převzal	převzít	k5eAaPmAgMnS	převzít
tento	tento	k3xDgInSc4	tento
status	status	k1gInSc4	status
od	od	k7c2	od
Aucklandu	Auckland	k1gInSc2	Auckland
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
fungoval	fungovat	k5eAaImAgInS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
v	v	k7c6	v
září	září	k1gNnSc6	září
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
oficiální	oficiální	k2eAgNnSc1d1	oficiální
zasedání	zasedání	k1gNnSc1	zasedání
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
tehdy	tehdy	k6eAd1	tehdy
žilo	žít	k5eAaImAgNnS	žít
okolo	okolo	k7c2	okolo
4900	[number]	k4	4900
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
i	i	k9	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
,	,	kIx,	,
sir	sir	k1gMnSc1	sir
Anand	Ananda	k1gFnPc2	Ananda
Satyanand	Satyanand	k1gInSc1	Satyanand
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
spravovaná	spravovaný	k2eAgFnSc1d1	spravovaná
wellingtonskou	wellingtonský	k2eAgFnSc7d1	wellingtonský
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
(	(	kIx(	(
<g/>
Wellington	Wellington	k1gInSc1	Wellington
City	city	k1gNnSc1	city
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc4	několik
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
například	například	k6eAd1	například
jako	jako	k8xC	jako
volební	volební	k2eAgInPc1d1	volební
okrsky	okrsek	k1gInPc1	okrsek
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čtvrtí	čtvrt	k1gFnSc7	čtvrt
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obývají	obývat	k5eAaImIp3nP	obývat
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
komunity	komunita	k1gFnPc1	komunita
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
čtrnáct	čtrnáct	k4xCc4	čtrnáct
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Starostkou	starostka	k1gFnSc7	starostka
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
Celia	Celia	k1gFnSc1	Celia
Wade-Brownová	Wade-Brownová	k1gFnSc1	Wade-Brownová
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
seznam	seznam	k1gInSc4	seznam
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
:	:	kIx,	:
Northern	Northern	k1gInSc1	Northern
Ward	Ward	k1gInSc1	Ward
Okrsky	okrsek	k1gInPc1	okrsek
<g/>
:	:	kIx,	:
Broadmeadows	Broadmeadows	k1gInSc1	Broadmeadows
<g/>
,	,	kIx,	,
Churton	Churton	k1gInSc1	Churton
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Glenside	Glensid	k1gMnSc5	Glensid
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
North	North	k1gInSc1	North
<g/>
,	,	kIx,	,
Horokiwi	Horokiwi	k1gNnSc1	Horokiwi
<g/>
,	,	kIx,	,
Johnsonville	Johnsonville	k1gNnSc1	Johnsonville
<g/>
,	,	kIx,	,
Khandallah	Khandallah	k1gInSc1	Khandallah
<g/>
,	,	kIx,	,
Newlands	Newlands	k1gInSc1	Newlands
<g/>
,	,	kIx,	,
Ohariu	Oharium	k1gNnSc6	Oharium
<g/>
,	,	kIx,	,
Paparangi	Paparangi	k1gNnSc6	Paparangi
<g/>
,	,	kIx,	,
Tawa	Tawa	k1gMnSc1	Tawa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Takapu	Takap	k1gInSc3	Takap
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
Woodridge	Woodridg	k1gFnSc2	Woodridg
Neoficiální	oficiální	k2eNgFnSc2d1	neoficiální
oblasti	oblast	k1gFnSc2	oblast
<g/>
:	:	kIx,	:
Greenacres	Greenacresa	k1gFnPc2	Greenacresa
<g/>
,	,	kIx,	,
Redwood	Redwooda	k1gFnPc2	Redwooda
<g/>
,	,	kIx,	,
Linden	Lindna	k1gFnPc2	Lindna
Onslow-Western	Onslow-Westerna	k1gFnPc2	Onslow-Westerna
Ward	Ward	k1gInSc1	Ward
Okrsky	okrsek	k1gInPc1	okrsek
<g/>
:	:	kIx,	:
Karori	Karor	k1gFnPc1	Karor
<g/>
,	,	kIx,	,
Northland	Northland	k1gInSc1	Northland
<g/>
,	,	kIx,	,
Crofton	Crofton	k1gInSc1	Crofton
Downs	Downs	k1gInSc1	Downs
<g/>
,	,	kIx,	,
Kaiwharawhara	Kaiwharawhara	k1gFnSc1	Kaiwharawhara
<g/>
,	,	kIx,	,
Ngaio	Ngaio	k1gNnSc1	Ngaio
<g/>
,	,	kIx,	,
Ngauranga	Ngauranga	k1gFnSc1	Ngauranga
<g/>
,	,	kIx,	,
Makara	Makara	k1gFnSc1	Makara
<g/>
,	,	kIx,	,
Makara	Makara	k1gFnSc1	Makara
Beach	Beach	k1gInSc1	Beach
<g/>
,	,	kIx,	,
Wilton	Wilton	k1gInSc1	Wilton
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
Cashmere	Cashmer	k1gMnSc5	Cashmer
<g/>
,	,	kIx,	,
Chartwell	Chartwell	k1gMnSc1	Chartwell
<g/>
,	,	kIx,	,
Rangoon	Rangoon	k1gMnSc1	Rangoon
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc1	Te
Kainga	Kainga	k1gFnSc1	Kainga
Lambton	Lambton	k1gInSc4	Lambton
Ward	Ward	k1gInSc1	Ward
Okrsky	okrsek	k1gInPc1	okrsek
<g/>
:	:	kIx,	:
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
Aro	ara	k1gMnSc5	ara
Valley	Valley	k1gInPc7	Valley
<g/>
,	,	kIx,	,
Kelburn	Kelburn	k1gMnSc1	Kelburn
<g/>
,	,	kIx,	,
Mount	Mount	k1gMnSc1	Mount
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
Oriental	Oriental	k1gMnSc1	Oriental
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
Te	Te	k1gMnSc5	Te
Aro	ara	k1gMnSc5	ara
<g/>
,	,	kIx,	,
Thorndon	Thorndona	k1gFnPc2	Thorndona
<g/>
,	,	kIx,	,
Wadestown	Wadestowna	k1gFnPc2	Wadestowna
<g/>
,	,	kIx,	,
Highbury	Highbura	k1gFnSc2	Highbura
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pipitea	Pipitea	k1gMnSc1	Pipitea
<g/>
,	,	kIx,	,
Roseneath	Roseneath	k1gMnSc1	Roseneath
Southern	Southerna	k1gFnPc2	Southerna
Ward	Ward	k1gInSc1	Ward
Okrsky	okrsek	k1gInPc1	okrsek
<g/>
:	:	kIx,	:
Berhampore	Berhampor	k1gMnSc5	Berhampor
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
Bay	Bay	k1gFnSc1	Bay
<g/>
,	,	kIx,	,
Newtown	Newtown	k1gInSc1	Newtown
<g/>
,	,	kIx,	,
Vogeltown	Vogeltown	k1gInSc1	Vogeltown
<g/>
,	,	kIx,	,
Houghton	Houghton	k1gInSc1	Houghton
Bay	Bay	k1gFnSc1	Bay
<g/>
,	,	kIx,	,
Kingston	Kingston	k1gInSc1	Kingston
<g/>
,	,	kIx,	,
Mornington	Mornington	k1gInSc1	Mornington
<g/>
,	,	kIx,	,
Mount	Mount	k1gMnSc1	Mount
Cook	Cook	k1gMnSc1	Cook
<g/>
,	,	kIx,	,
Owhiro	Owhira	k1gMnSc5	Owhira
Bay	Bay	k1gMnSc5	Bay
<g/>
,	,	kIx,	,
Southgate	Southgat	k1gMnSc5	Southgat
Neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
oblasti	oblast	k1gFnSc2	oblast
<g/>
:	:	kIx,	:
Kowhai	Kowha	k1gFnSc2	Kowha
Park	park	k1gInSc1	park
Eastern	Eastern	k1gMnSc1	Eastern
<g />
.	.	kIx.	.
</s>
<s>
Ward	Ward	k1gInSc1	Ward
Okrsky	okrsek	k1gInPc1	okrsek
<g/>
:	:	kIx,	:
Hataitai	Hataita	k1gFnPc1	Hataita
<g/>
,	,	kIx,	,
Lyall	Lyall	k1gMnSc1	Lyall
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
Kilbirnie	Kilbirnie	k1gFnSc1	Kilbirnie
<g/>
,	,	kIx,	,
Miramar	Miramar	k1gMnSc1	Miramar
<g/>
,	,	kIx,	,
Seatoun	Seatoun	k1gMnSc1	Seatoun
<g/>
,	,	kIx,	,
Breaker	Breaker	k1gMnSc1	Breaker
Bay	Bay	k1gMnSc2	Bay
<g/>
,	,	kIx,	,
Karaka	Karak	k1gMnSc2	Karak
Bays	Baysa	k1gFnPc2	Baysa
<g/>
,	,	kIx,	,
Maupuia	Maupuium	k1gNnSc2	Maupuium
<g/>
,	,	kIx,	,
Melrose	Melrosa	k1gFnSc3	Melrosa
<g/>
,	,	kIx,	,
Moa	Moa	k1gFnSc3	Moa
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
Rongotai	Rongota	k1gMnSc5	Rongota
<g/>
,	,	kIx,	,
Strathmore	Strathmor	k1gMnSc5	Strathmor
Neoficiální	oficiální	k2eNgMnSc1d1	oficiální
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
Crawford	Crawford	k1gMnSc1	Crawford
<g/>
,	,	kIx,	,
Seatoun	Seatoun	k1gMnSc1	Seatoun
Bays	Bays	k1gInSc1	Bays
<g/>
,	,	kIx,	,
Seatoun	Seatoun	k1gInSc1	Seatoun
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Miramar	Miramar	k1gInSc1	Miramar
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
Strathmore	Strathmor	k1gInSc5	Strathmor
Heights	Heightsa	k1gFnPc2	Heightsa
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
stojí	stát	k5eAaImIp3nS	stát
novogotická	novogotický	k2eAgFnSc1d1	novogotická
budova	budova	k1gFnSc1	budova
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
komplexu	komplex	k1gInSc2	komplex
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
budov	budova	k1gFnPc2	budova
nebyla	být	k5eNaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
požárem	požár	k1gInSc7	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgFnS	být
architektem	architekt	k1gMnSc7	architekt
Thomasem	Thomas	k1gMnSc7	Thomas
Turnbullem	Turnbull	k1gMnSc7	Turnbull
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
etapách	etapa	k1gFnPc6	etapa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
a	a	k8xC	a
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
,	,	kIx,	,
neobešla	obešnout	k5eNaPmAgFnS	obešnout
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
a	a	k8xC	a
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
prošla	projít	k5eAaPmAgFnS	projít
nákladnou	nákladný	k2eAgFnSc7d1	nákladná
tříletou	tříletý	k2eAgFnSc7d1	tříletá
restaurací	restaurace	k1gFnSc7	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zajímavosti	zajímavost	k1gFnPc4	zajímavost
knihovny	knihovna	k1gFnSc2	knihovna
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
původní	původní	k2eAgFnPc4d1	původní
železné	železný	k2eAgFnPc4d1	železná
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
úspěšně	úspěšně	k6eAd1	úspěšně
chránily	chránit	k5eAaImAgInP	chránit
uložené	uložený	k2eAgInPc1d1	uložený
svazky	svazek	k1gInPc1	svazek
před	před	k7c7	před
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
originální	originální	k2eAgNnPc1d1	originální
rozetová	rozetový	k2eAgNnPc1d1	rozetové
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
prosvětlována	prosvětlován	k2eAgFnSc1d1	prosvětlován
<g/>
.	.	kIx.	.
</s>
<s>
Požáru	požár	k1gInSc3	požár
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
komplexu	komplex	k1gInSc2	komplex
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
zasedal	zasedat	k5eAaImAgInS	zasedat
vlastní	vlastní	k2eAgInSc1d1	vlastní
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1911	[number]	k4	1911
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
premiér	premiér	k1gMnSc1	premiér
Joseph	Josepha	k1gFnPc2	Josepha
Ward	Ward	k1gInSc4	Ward
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c4	na
podobu	podoba	k1gFnSc4	podoba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
33	[number]	k4	33
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
architekt	architekt	k1gMnSc1	architekt
John	John	k1gMnSc1	John
Campbell	Campbell	k1gMnSc1	Campbell
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
neoklasicistní	neoklasicistní	k2eAgFnSc2d1	neoklasicistní
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
onu	onen	k3xDgFnSc4	onen
původní	původní	k2eAgFnSc4d1	původní
novogotickou	novogotický	k2eAgFnSc4d1	novogotická
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
přemístil	přemístit	k5eAaPmAgInS	přemístit
již	již	k6eAd1	již
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
ukončit	ukončit	k5eAaPmF	ukončit
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
budově	budova	k1gFnSc6	budova
novozélandské	novozélandský	k2eAgFnSc2d1	novozélandská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
instituce	instituce	k1gFnPc4	instituce
kapacitně	kapacitně	k6eAd1	kapacitně
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc7d3	nejnovější
budovou	budova	k1gFnSc7	budova
tohoto	tento	k3xDgInSc2	tento
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
stavba	stavba	k1gFnSc1	stavba
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Beehive	Beehiev	k1gFnSc2	Beehiev
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
včelí	včelí	k2eAgInSc4d1	včelí
úl	úl	k1gInSc4	úl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
britský	britský	k2eAgMnSc1d1	britský
architekt	architekt	k1gMnSc1	architekt
Sir	sir	k1gMnSc1	sir
Basil	Basil	k1gInSc4	Basil
Spence	Spenec	k1gInSc2	Spenec
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáctipodlažní	čtrnáctipodlažní	k2eAgFnSc1d1	čtrnáctipodlažní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
čtyři	čtyři	k4xCgNnPc4	čtyři
spodní	spodní	k2eAgNnPc4d1	spodní
patra	patro	k1gNnPc4	patro
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
72	[number]	k4	72
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
novozélandské	novozélandský	k2eAgFnSc2d1	novozélandská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podzemním	podzemní	k2eAgInSc7d1	podzemní
tunelem	tunel	k1gInSc7	tunel
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
objektem	objekt	k1gInSc7	objekt
Bowen	Bowna	k1gFnPc2	Bowna
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
kanceláře	kancelář	k1gFnPc1	kancelář
řady	řada	k1gFnSc2	řada
ministrů	ministr	k1gMnPc2	ministr
i	i	k8xC	i
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
"	"	kIx"	"
<g/>
Včelího	včelí	k2eAgInSc2d1	včelí
úlu	úl	k1gInSc2	úl
<g/>
"	"	kIx"	"
funguje	fungovat	k5eAaImIp3nS	fungovat
návštěvnické	návštěvnický	k2eAgNnSc1d1	návštěvnické
centrum	centrum	k1gNnSc1	centrum
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
budova	budova	k1gFnSc1	budova
společně	společně	k6eAd1	společně
s	s	k7c7	s
knihovnou	knihovna	k1gFnSc7	knihovna
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nekoná	konat	k5eNaImIp3nS	konat
zasedání	zasedání	k1gNnSc1	zasedání
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
turistických	turistický	k2eAgInPc2d1	turistický
symbolů	symbol	k1gInPc2	symbol
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
spojující	spojující	k2eAgNnSc1d1	spojující
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
příměstskou	příměstský	k2eAgFnSc7d1	příměstská
částí	část	k1gFnSc7	část
Kelburn	Kelburna	k1gFnPc2	Kelburna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
městská	městský	k2eAgFnSc1d1	městská
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
je	být	k5eAaImIp3nS	být
612	[number]	k4	612
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
stoupá	stoupat	k5eAaImIp3nS	stoupat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
120	[number]	k4	120
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
konečnými	konečný	k2eAgFnPc7d1	konečná
stanicemi	stanice	k1gFnPc7	stanice
leží	ležet	k5eAaImIp3nS	ležet
ještě	ještě	k6eAd1	ještě
tři	tři	k4xCgInPc1	tři
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
míjení	míjení	k1gNnSc4	míjení
protijedoucích	protijedoucí	k2eAgInPc2d1	protijedoucí
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vozový	vozový	k2eAgInSc4d1	vozový
park	park	k1gInSc4	park
lanovky	lanovka	k1gFnSc2	lanovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
propojeny	propojit	k5eAaPmNgInP	propojit
lanem	lano	k1gNnSc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
lanovky	lanovka	k1gFnSc2	lanovka
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
30	[number]	k4	30
sedících	sedící	k2eAgFnPc2d1	sedící
a	a	k8xC	a
70	[number]	k4	70
stojících	stojící	k2eAgMnPc2d1	stojící
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
18	[number]	k4	18
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
původní	původní	k2eAgFnSc2d1	původní
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
domů	dům	k1gInPc2	dům
v	v	k7c6	v
kopcích	kopec	k1gInPc6	kopec
oblasti	oblast	k1gFnSc2	oblast
Kelburnu	Kelburn	k1gInSc2	Kelburn
třeba	třeba	k6eAd1	třeba
zajistit	zajistit	k5eAaPmF	zajistit
spolehlivé	spolehlivý	k2eAgNnSc4d1	spolehlivé
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc7d1	vznikající
čtvrtí	čtvrt	k1gFnSc7	čtvrt
a	a	k8xC	a
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
James	James	k1gInSc1	James
Fulton	Fulton	k1gInSc1	Fulton
projekt	projekt	k1gInSc1	projekt
budoucí	budoucí	k2eAgFnSc2d1	budoucí
lanovky	lanovka	k1gFnSc2	lanovka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
výstavbě	výstavba	k1gFnSc6	výstavba
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
pracovaly	pracovat	k5eAaImAgFnP	pracovat
i	i	k9	i
skupiny	skupina	k1gFnPc1	skupina
odsouzených	odsouzená	k1gFnPc2	odsouzená
z	z	k7c2	z
věznice	věznice	k1gFnSc2	věznice
Terrace	Terrace	k1gFnSc1	Terrace
Gaol	Gaol	k1gMnSc1	Gaol
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1902	[number]	k4	1902
vyjel	vyjet	k5eAaPmAgInS	vyjet
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
první	první	k4xOgInSc4	první
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
lanovka	lanovka	k1gFnSc1	lanovka
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
vlnu	vlna	k1gFnSc4	vlna
nadšení	nadšení	k1gNnSc2	nadšení
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
obrovský	obrovský	k2eAgInSc4d1	obrovský
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
víkendu	víkend	k1gInSc2	víkend
provozu	provoz	k1gInSc2	provoz
přepravily	přepravit	k5eAaPmAgInP	přepravit
její	její	k3xOp3gInPc1	její
vozy	vůz	k1gInPc1	vůz
čtyři	čtyři	k4xCgNnPc4	čtyři
tisíce	tisíc	k4xCgInPc4	tisíc
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
za	za	k7c4	za
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
více	hodně	k6eAd2	hodně
než	než	k8xS	než
425	[number]	k4	425
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
dráha	dráha	k1gFnSc1	dráha
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
785	[number]	k4	785
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
tunely	tunel	k1gInPc1	tunel
a	a	k8xC	a
viadukty	viadukt	k1gInPc1	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
elektřina	elektřina	k1gFnSc1	elektřina
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
větrný	větrný	k2eAgInSc4d1	větrný
pohon	pohon	k1gInSc4	pohon
lanovky	lanovka	k1gFnSc2	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
přineslo	přinést	k5eAaPmAgNnS	přinést
řidičům	řidič	k1gMnPc3	řidič
lepší	dobrý	k2eAgFnSc4d2	lepší
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
jízdou	jízda	k1gFnSc7	jízda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mohli	moct	k5eAaImAgMnP	moct
nyní	nyní	k6eAd1	nyní
stroj	stroj	k1gInSc4	stroj
řídit	řídit	k5eAaImF	řídit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k6eAd1	jen
signalizační	signalizační	k2eAgInSc4d1	signalizační
zvon	zvon	k1gInSc4	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
vozy	vůz	k1gInPc1	vůz
se	se	k3xPyFc4	se
však	však	k9	však
postupně	postupně	k6eAd1	postupně
stávaly	stávat	k5eAaImAgFnP	stávat
nevyhovujícími	vyhovující	k2eNgInPc7d1	nevyhovující
a	a	k8xC	a
nesplňovaly	splňovat	k5eNaImAgFnP	splňovat
moderní	moderní	k2eAgFnPc1d1	moderní
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
normy	norma	k1gFnPc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dělníků	dělník	k1gMnPc2	dělník
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
silnice	silnice	k1gFnSc2	silnice
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
klesajícímu	klesající	k2eAgInSc3d1	klesající
vozu	vůz	k1gInSc3	vůz
lanovky	lanovka	k1gFnSc2	lanovka
a	a	k8xC	a
přivodil	přivodit	k5eAaBmAgMnS	přivodit
si	se	k3xPyFc3	se
vážná	vážná	k1gFnSc1	vážná
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
kontrole	kontrola	k1gFnSc6	kontrola
vyšly	vyjít	k5eAaPmAgInP	vyjít
najevo	najevo	k6eAd1	najevo
technické	technický	k2eAgInPc4d1	technický
i	i	k8xC	i
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
nedostatky	nedostatek	k1gInPc4	nedostatek
historické	historický	k2eAgFnSc2d1	historická
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1978	[number]	k4	1978
staré	starý	k2eAgInPc4d1	starý
vozy	vůz	k1gInPc4	vůz
za	za	k7c2	za
velkého	velký	k2eAgInSc2d1	velký
zájmu	zájem	k1gInSc2	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
vyjely	vyjet	k5eAaPmAgFnP	vyjet
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
trasa	trasa	k1gFnSc1	trasa
lanovky	lanovka	k1gFnSc2	lanovka
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
jevila	jevit	k5eAaImAgFnS	jevit
jako	jako	k9	jako
nejpraktičtější	praktický	k2eAgFnSc1d3	nejpraktičtější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
modernější	moderní	k2eAgFnSc1d2	modernější
trať	trať	k1gFnSc1	trať
následně	následně	k6eAd1	následně
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dráhy	dráha	k1gFnSc2	dráha
původní	původní	k2eAgFnSc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Otevřena	otevřít	k5eAaPmNgFnS	otevřít
byla	být	k5eAaImAgFnS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1979	[number]	k4	1979
a	a	k8xC	a
stará	starý	k2eAgNnPc4d1	staré
dřevěná	dřevěný	k2eAgNnPc4d1	dřevěné
vozidla	vozidlo	k1gNnPc4	vozidlo
nahradily	nahradit	k5eAaPmAgFnP	nahradit
modernější	moderní	k2eAgInPc4d2	modernější
vozy	vůz	k1gInPc4	vůz
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
vozidla	vozidlo	k1gNnSc2	vozidlo
lanovky	lanovka	k1gFnSc2	lanovka
dříve	dříve	k6eAd2	dříve
otáčela	otáčet	k5eAaImAgFnS	otáčet
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
již	již	k6eAd1	již
dvakrát	dvakrát	k6eAd1	dvakrát
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
demolici	demolice	k1gFnSc4	demolice
ustála	ustát	k5eAaPmAgFnS	ustát
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
museum	museum	k1gNnSc1	museum
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
na	na	k7c4	na
území	území	k1gNnSc4	území
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
přes	přes	k7c4	přes
stovku	stovka	k1gFnSc4	stovka
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
oficiálně	oficiálně	k6eAd1	oficiálně
chovaným	chovaný	k2eAgNnSc7d1	chované
zvířetem	zvíře	k1gNnSc7	zvíře
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
zoo	zoo	k1gNnSc6	zoo
byl	být	k5eAaImAgMnS	být
lev	lev	k1gMnSc1	lev
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
Dick	Dick	k1gMnSc1	Dick
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
přidaly	přidat	k5eAaPmAgFnP	přidat
lamy	lama	k1gFnPc1	lama
<g/>
,	,	kIx,	,
emu	emu	k1gMnPc1	emu
a	a	k8xC	a
klokani	klokan	k1gMnPc1	klokan
<g/>
.	.	kIx.	.
</s>
<s>
Wellingtonská	Wellingtonský	k2eAgFnSc1d1	Wellingtonský
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
i	i	k9	i
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
pelikán	pelikán	k1gMnSc1	pelikán
Percy	Perca	k1gFnSc2	Perca
dožil	dožít	k5eAaPmAgMnS	dožít
věku	věk	k1gInSc2	věk
62	[number]	k4	62
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
žijícím	žijící	k2eAgNnSc7d1	žijící
zvířetem	zvíře	k1gNnSc7	zvíře
wellingtonském	wellingtonský	k2eAgNnSc6d1	wellingtonský
zoo	zoo	k1gNnSc6	zoo
byl	být	k5eAaImAgMnS	být
však	však	k9	však
gibon	gibon	k1gMnSc1	gibon
Nippy	Nippa	k1gFnSc2	Nippa
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zahrada	zahrada	k1gFnSc1	zahrada
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uhynul	uhynout	k5eAaPmAgMnS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
zejména	zejména	k9	zejména
svým	svůj	k3xOyFgInSc7	svůj
chovem	chov	k1gInSc7	chov
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
dokonce	dokonce	k9	dokonce
zoo	zoo	k1gFnSc1	zoo
pořádalo	pořádat	k5eAaImAgNnS	pořádat
čajové	čajový	k2eAgInPc4d1	čajový
dýchánky	dýchánek	k1gInPc4	dýchánek
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
šimpanzi	šimpanz	k1gMnPc7	šimpanz
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
obývají	obývat	k5eAaImIp3nP	obývat
wellingtonští	wellingtonský	k2eAgMnPc1d1	wellingtonský
šimpanzi	šimpanz	k1gMnPc1	šimpanz
nový	nový	k2eAgInSc4d1	nový
prostorný	prostorný	k2eAgInSc4d1	prostorný
výběh	výběh	k1gInSc4	výběh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
26	[number]	k4	26
hektarů	hektar	k1gInPc2	hektar
v	v	k7c6	v
Kelburnu	Kelburn	k1gInSc6	Kelburn
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
konečné	konečný	k2eAgFnSc2d1	konečná
stanice	stanice	k1gFnSc2	stanice
wellingtonské	wellingtonský	k2eAgFnSc2d1	wellingtonský
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
růžová	růžový	k2eAgFnSc1d1	růžová
zahrada	zahrada	k1gFnSc1	zahrada
lady	lady	k1gFnSc2	lady
Norwoodové	Norwoodový	k2eAgFnSc2d1	Norwoodová
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
tisíci	tisíc	k4xCgInPc7	tisíc
druhy	druh	k1gInPc7	druh
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
hřbitov	hřbitov	k1gInSc1	hřbitov
"	"	kIx"	"
<g/>
Bolton	Bolton	k1gInSc1	Bolton
Street	Street	k1gInSc1	Street
Memorial	Memorial	k1gInSc1	Memorial
Park	park	k1gInSc1	park
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pochovány	pochován	k2eAgFnPc4d1	pochována
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
žilo	žít	k5eAaImAgNnS	žít
trvale	trvale	k6eAd1	trvale
na	na	k7c6	na
území	území	k1gNnSc6	území
Wellingtonu	Wellington	k1gInSc2	Wellington
179	[number]	k4	179
466	[number]	k4	466
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
šesté	šestý	k4xOgNnSc1	šestý
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
tak	tak	k9	tak
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
15	[number]	k4	15
639	[number]	k4	639
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
9,5	[number]	k4	9,5
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Wellingtonu	Wellington	k1gInSc2	Wellington
tvoří	tvořit	k5eAaImIp3nP	tvořit
4,5	[number]	k4	4,5
procenta	procento	k1gNnSc2	procento
populace	populace	k1gFnSc2	populace
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mediánový	mediánový	k2eAgInSc1d1	mediánový
věk	věk	k1gInSc1	věk
obyvatel	obyvatel	k1gMnPc2	obyvatel
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
populací	populace	k1gFnSc7	populace
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
mediánový	mediánový	k2eAgInSc1d1	mediánový
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
40	[number]	k4	40
letům	léto	k1gNnPc3	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Wellingtonu	Wellington	k1gInSc2	Wellington
výrazně	výrazně	k6eAd1	výrazně
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgNnSc1d1	aktivní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
téměř	téměř	k6eAd1	téměř
74	[number]	k4	74
procent	procento	k1gNnPc2	procento
wellingtonské	wellingtonský	k2eAgFnSc2d1	wellingtonský
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
20	[number]	k4	20
a	a	k8xC	a
34	[number]	k4	34
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
státem	stát	k1gInSc7	stát
má	mít	k5eAaImIp3nS	mít
Wellington	Wellington	k1gInSc1	Wellington
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
důchodců	důchodce	k1gMnPc2	důchodce
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Wellingtonu	Wellington	k1gInSc2	Wellington
tvoří	tvořit	k5eAaImIp3nP	tvořit
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelnou	zanedbatelný	k2eNgFnSc7d1	nezanedbatelná
národnostní	národnostní	k2eAgFnSc7d1	národnostní
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zastoupení	zastoupení	k1gNnSc1	zastoupení
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
procenta	procento	k1gNnPc4	procento
vyšší	vysoký	k2eAgNnPc4d2	vyšší
než	než	k8xS	než
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Wellingtonu	Wellington	k1gInSc2	Wellington
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
celkovým	celkový	k2eAgNnSc7d1	celkové
složením	složení	k1gNnSc7	složení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nP	tvořit
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
Maorové	Maorová	k1gFnSc2	Maorová
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
populace	populace	k1gFnSc1	populace
tvoří	tvořit	k5eAaImIp3nS	tvořit
7,7	[number]	k4	7,7
procent	procento	k1gNnPc2	procento
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
28	[number]	k4	28
procent	procento	k1gNnPc2	procento
současných	současný	k2eAgMnPc2d1	současný
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mimo	mimo	k7c4	mimo
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
nejčastěji	často	k6eAd3	často
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
úřední	úřední	k2eAgFnSc2d1	úřední
angličtiny	angličtina	k1gFnSc2	angličtina
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
maorštinu	maorština	k1gFnSc4	maorština
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
70,5	[number]	k4	70,5
procenta	procento	k1gNnPc4	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
Wellingtonu	Wellington	k1gInSc3	Wellington
však	však	k9	však
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Nadstandardní	nadstandardní	k2eAgNnSc1d1	nadstandardní
vzdělání	vzdělání	k1gNnSc1	vzdělání
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
55,5	[number]	k4	55,5
%	%	kIx~	%
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
15,5	[number]	k4	15,5
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
například	například	k6eAd1	například
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
6	[number]	k4	6
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
novozélandském	novozélandský	k2eAgNnSc6d1	novozélandské
městě	město	k1gNnSc6	město
Aucklandu	Auckland	k1gInSc2	Auckland
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
doloženého	doložený	k2eAgNnSc2d1	doložené
standardního	standardní	k2eAgNnSc2d1	standardní
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
státem	stát	k1gInSc7	stát
nižší	nízký	k2eAgFnSc1d2	nižší
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Wellingtonu	Wellington	k1gInSc2	Wellington
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
pozicích	pozice	k1gFnPc6	pozice
odborníků	odborník	k1gMnPc2	odborník
nebo	nebo	k8xC	nebo
manažerů	manažer	k1gMnPc2	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
obyvatel	obyvatel	k1gMnPc2	obyvatel
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
32	[number]	k4	32
500	[number]	k4	500
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
8100	[number]	k4	8100
dolarů	dolar	k1gInPc2	dolar
více	hodně	k6eAd2	hodně
než	než	k8xS	než
průměrný	průměrný	k2eAgInSc1d1	průměrný
plat	plat	k1gInSc1	plat
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
roční	roční	k2eAgInPc4d1	roční
příjmy	příjem	k1gInPc4	příjem
vyšší	vysoký	k2eAgInPc4d2	vyšší
než	než	k8xS	než
50	[number]	k4	50
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
asi	asi	k9	asi
9	[number]	k4	9
procent	procento	k1gNnPc2	procento
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
fungovalo	fungovat	k5eAaImAgNnS	fungovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
celkem	celkem	k6eAd1	celkem
82	[number]	k4	82
základních	základní	k2eAgFnPc2d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
státní	státní	k2eAgFnPc4d1	státní
smíšené	smíšený	k2eAgFnPc4d1	smíšená
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
4	[number]	k4	4
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
soukromé	soukromý	k2eAgFnPc1d1	soukromá
<g/>
,	,	kIx,	,
6	[number]	k4	6
zařízení	zařízení	k1gNnSc2	zařízení
je	být	k5eAaImIp3nS	být
dívčích	dívčí	k2eAgFnPc2d1	dívčí
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
studentů	student	k1gMnPc2	student
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
chlapeckou	chlapecký	k2eAgFnSc4d1	chlapecká
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Wellington	Wellington	k1gInSc1	Wellington
College	Colleg	k1gFnPc1	Colleg
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
evidovala	evidovat	k5eAaImAgFnS	evidovat
1532	[number]	k4	1532
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
školou	škola	k1gFnSc7	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Tawa	Taw	k2eAgFnSc1d1	Taw
College	College	k1gFnSc1	College
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
studovalo	studovat	k5eAaImAgNnS	studovat
1382	[number]	k4	1382
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Viktoriina	Viktoriin	k2eAgFnSc1d1	Viktoriina
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Victoria	Victorium	k1gNnSc2	Victorium
University	universita	k1gFnSc2	universita
of	of	k?	of
Wellington	Wellington	k1gInSc1	Wellington
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
a	a	k8xC	a
původně	původně	k6eAd1	původně
fungovala	fungovat	k5eAaImAgFnS	fungovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Novozélandské	novozélandský	k2eAgFnSc2d1	novozélandská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
především	především	k9	především
její	její	k3xOp3gFnSc1	její
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
budově	budova	k1gFnSc6	budova
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
fakulta	fakulta	k1gFnSc1	fakulta
humanitních	humanitní	k2eAgInPc2d1	humanitní
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
však	však	k9	však
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
studium	studium	k1gNnSc4	studium
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
ji	on	k3xPp3gFnSc4	on
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
22	[number]	k4	22
270	[number]	k4	270
studentů	student	k1gMnPc2	student
a	a	k8xC	a
1	[number]	k4	1
884	[number]	k4	884
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kelburnu	Kelburn	k1gInSc6	Kelburn
<g/>
,	,	kIx,	,
fakulty	fakulta	k1gFnSc2	fakulta
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
biologického	biologický	k2eAgInSc2d1	biologický
výzkumu	výzkum	k1gInSc2	výzkum
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
odloučených	odloučený	k2eAgNnPc6d1	odloučené
pracovištích	pracoviště	k1gNnPc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Masseyskou	Masseyský	k2eAgFnSc7d1	Masseyský
univerzitou	univerzita	k1gFnSc7	univerzita
provozuje	provozovat	k5eAaImIp3nS	provozovat
Viktoriina	Viktoriin	k2eAgFnSc1d1	Viktoriina
univerzita	univerzita	k1gFnSc1	univerzita
Novozélandskou	novozélandský	k2eAgFnSc4d1	novozélandská
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Masseyova	Masseyův	k2eAgFnSc1d1	Masseyův
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Massey	Masse	k2eAgFnPc1d1	Masse
University	universita	k1gFnPc1	universita
<g/>
)	)	kIx)	)
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k8xC	jako
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
škola	škola	k1gFnSc1	škola
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Novozélandské	novozélandský	k2eAgFnSc2d1	novozélandská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
rozpadu	rozpad	k1gInSc6	rozpad
existovala	existovat	k5eAaImAgFnS	existovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Viktoriiny	Viktoriin	k2eAgFnSc2d1	Viktoriina
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
univerzitou	univerzita	k1gFnSc7	univerzita
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
studovalo	studovat	k5eAaImAgNnS	studovat
na	na	k7c6	na
Masseyově	Masseyův	k2eAgFnSc6d1	Masseyův
univerzitě	univerzita	k1gFnSc6	univerzita
35	[number]	k4	35
509	[number]	k4	509
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
nabízí	nabízet	k5eAaImIp3nS	nabízet
kromě	kromě	k7c2	kromě
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
estetických	estetický	k2eAgInPc2d1	estetický
a	a	k8xC	a
humanitních	humanitní	k2eAgInPc2d1	humanitní
oborů	obor	k1gInPc2	obor
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
letectví	letectví	k1gNnSc1	letectví
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc1	řešení
sporů	spor	k1gInPc2	spor
nebo	nebo	k8xC	nebo
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
akreditována	akreditovat	k5eAaBmNgFnS	akreditovat
Americkou	americký	k2eAgFnSc7d1	americká
asociací	asociace	k1gFnSc7	asociace
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
jsou	být	k5eAaImIp3nP	být
absolventům	absolvent	k1gMnPc3	absolvent
Masseyovy	Masseyův	k2eAgFnSc2d1	Masseyův
univerzity	univerzita	k1gFnSc2	univerzita
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
uznávány	uznáván	k2eAgInPc4d1	uznáván
tituly	titul	k1gInPc4	titul
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
veteriny	veterin	k1gInPc1	veterin
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitu	univerzita	k1gFnSc4	univerzita
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgInPc1	tři
kampusy	kampus	k1gInPc1	kampus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
sídlí	sídlet	k5eAaImIp3nS	sídlet
fakulka	fakulka	k1gFnSc1	fakulka
designu	design	k1gInSc2	design
a	a	k8xC	a
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
hudební	hudební	k2eAgFnSc1d1	hudební
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Masseyova	Masseyův	k2eAgFnSc1d1	Masseyův
univerzita	univerzita	k1gFnSc1	univerzita
společně	společně	k6eAd1	společně
s	s	k7c7	s
univerzitou	univerzita	k1gFnSc7	univerzita
Viktoriinou	Viktoriin	k2eAgFnSc7d1	Viktoriina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Otažské	Otažský	k2eAgFnSc2d1	Otažský
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
část	část	k1gFnSc1	část
wellingtonského	wellingtonský	k2eAgInSc2d1	wellingtonský
technického	technický	k2eAgInSc2d1	technický
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Wellingtonu	Wellington	k1gInSc2	Wellington
je	být	k5eAaImIp3nS	být
obsluhováno	obsluhován	k2eAgNnSc1d1	obsluhováno
dvěma	dva	k4xCgFnPc7	dva
státními	státní	k2eAgFnPc7d1	státní
dálnicemi	dálnice	k1gFnPc7	dálnice
SH1	SH1	k1gFnSc2	SH1
a	a	k8xC	a
SH	SH	kA	SH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
mimoúrovňové	mimoúrovňový	k2eAgFnSc6d1	mimoúrovňová
křižovatce	křižovatka	k1gFnSc6	křižovatka
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
vede	vést	k5eAaImIp3nS	vést
samotným	samotný	k2eAgInSc7d1	samotný
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
areálu	areál	k1gInSc2	areál
Wellingtonského	Wellingtonský	k2eAgNnSc2d1	Wellingtonský
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
fungujícího	fungující	k2eAgInSc2d1	fungující
ve	v	k7c6	v
městě	město	k1gNnSc6	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
leží	ležet	k5eAaImIp3nS	ležet
6	[number]	k4	6
kilometrů	kilometr	k1gInPc2	kilometr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
lety	let	k1gInPc4	let
napříč	napříč	k7c7	napříč
novozélandskými	novozélandský	k2eAgInPc7d1	novozélandský
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
letadel	letadlo	k1gNnPc2	letadlo
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
na	na	k7c6	na
wellingtonském	wellingtonský	k2eAgNnSc6d1	wellingtonský
letišti	letiště	k1gNnSc6	letiště
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
přistávají	přistávat	k5eAaImIp3nP	přistávat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
737-800	[number]	k4	737-800
a	a	k8xC	a
Airbus	airbus	k1gInSc1	airbus
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
letadla	letadlo	k1gNnPc1	letadlo
musí	muset	k5eAaImIp3nP	muset
využívat	využívat	k5eAaImF	využívat
jiná	jiný	k2eAgNnPc4d1	jiné
letiště	letiště	k1gNnPc4	letiště
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jediná	jediný	k2eAgFnSc1d1	jediná
wellingtonská	wellingtonský	k2eAgFnSc1d1	wellingtonský
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pouze	pouze	k6eAd1	pouze
2051	[number]	k4	2051
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
Wellingtonské	Wellingtonský	k2eAgNnSc1d1	Wellingtonský
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
přepraví	přepravit	k5eAaPmIp3nS	přepravit
ročně	ročně	k6eAd1	ročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
nejrušnějšími	rušný	k2eAgFnPc7d3	nejrušnější
letišti	letiště	k1gNnSc6	letiště
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Aucklandu	Auckland	k1gInSc6	Auckland
a	a	k8xC	a
Christchurchu	Christchurch	k1gInSc6	Christchurch
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
wellingtonský	wellingtonský	k2eAgInSc4d1	wellingtonský
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ji	on	k3xPp3gFnSc4	on
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
společností	společnost	k1gFnPc2	společnost
pod	pod	k7c7	pod
společnou	společný	k2eAgFnSc7d1	společná
značkou	značka	k1gFnSc7	značka
Metlink	Metlink	k1gInSc1	Metlink
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
přepraví	přepravit	k5eAaPmIp3nS	přepravit
až	až	k9	až
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
stále	stále	k6eAd1	stále
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Metlinku	Metlinka	k1gFnSc4	Metlinka
nejvytíženější	vytížený	k2eAgFnSc1d3	nejvytíženější
dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
městské	městský	k2eAgInPc4d1	městský
autobusy	autobus	k1gInPc4	autobus
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
na	na	k7c6	na
103	[number]	k4	103
linkách	linka	k1gFnPc6	linka
regionu	region	k1gInSc2	region
jezdí	jezdit	k5eAaImIp3nP	jezdit
470	[number]	k4	470
a	a	k8xC	a
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
2800	[number]	k4	2800
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
linek	linka	k1gFnPc2	linka
je	být	k5eAaImIp3nS	být
obsluhováno	obsluhován	k2eAgNnSc1d1	obsluhováno
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
stále	stále	k6eAd1	stále
používají	používat	k5eAaImIp3nP	používat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
<g/>
.	.	kIx.	.
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
přepraví	přepravit	k5eAaPmIp3nP	přepravit
každoročně	každoročně	k6eAd1	každoročně
místní	místní	k2eAgInPc1d1	místní
železniční	železniční	k2eAgInPc1d1	železniční
spoje	spoj	k1gInPc1	spoj
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
milion	milion	k4xCgInSc1	milion
cestujících	cestující	k1gFnPc2	cestující
jezdí	jezdit	k5eAaImIp3nS	jezdit
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
wellingtonskou	wellingtonský	k2eAgFnSc7d1	wellingtonský
lanovkou	lanovka	k1gFnSc7	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
Zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
osobní	osobní	k2eAgInPc4d1	osobní
trajekty	trajekt	k1gInPc4	trajekt
využije	využít	k5eAaPmIp3nS	využít
každoročně	každoročně	k6eAd1	každoročně
asi	asi	k9	asi
150	[number]	k4	150
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
jejich	jejich	k3xOp3gNnSc4	jejich
vytížení	vytížení	k1gNnSc4	vytížení
roste	růst	k5eAaImIp3nS	růst
nejrychleji	rychle	k6eAd3	rychle
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
wellingtonské	wellingtonský	k2eAgFnSc2d1	wellingtonský
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Wellingtonu	Wellington	k1gInSc6	Wellington
provozována	provozován	k2eAgFnSc1d1	provozována
i	i	k8xC	i
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Sydney	Sydney	k1gNnSc1	Sydney
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
Peking	Peking	k1gInSc1	Peking
(	(	kIx(	(
<g/>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Sia-men	Siaen	k1gInSc1	Sia-men
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnSc1	provincie
Fu-ťien	Fu-ťien	k1gInSc1	Fu-ťien
(	(	kIx(	(
<g/>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Sakai	Sakai	k1gNnSc1	Sakai
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
Harrogate	Harrogat	k1gMnSc5	Harrogat
(	(	kIx(	(
<g/>
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
Chania	Chanium	k1gNnSc2	Chanium
<g/>
,	,	kIx,	,
Kréta	Kréta	k1gFnSc1	Kréta
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
</s>
