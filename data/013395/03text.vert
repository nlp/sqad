<p>
<s>
V	v	k7c6	v
lingvistice	lingvistika	k1gFnSc6	lingvistika
a	a	k8xC	a
informatice	informatika	k1gFnSc6	informatika
označuje	označovat	k5eAaImIp3nS	označovat
pojem	pojem	k1gInSc1	pojem
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Context-free	Contextree	k1gNnSc1	Context-free
Grammar	Grammara	k1gFnPc2	Grammara
<g/>
,	,	kIx,	,
CFG	CFG	kA	CFG
<g/>
)	)	kIx)	)
formální	formální	k2eAgFnSc4d1	formální
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
mají	mít	k5eAaImIp3nP	mít
všechna	všechen	k3xTgNnPc1	všechen
přepisovací	přepisovací	k2eAgNnPc1d1	přepisovací
pravidla	pravidlo	k1gNnPc1	pravidlo
tvar	tvar	k1gInSc1	tvar
</s>
</p>
<p>
<s>
A	a	k9	a
→	→	k?	→
β	β	k?	β
A	a	k9	a
je	být	k5eAaImIp3nS	být
neterminál	neterminál	k1gInSc1	neterminál
a	a	k8xC	a
β	β	k?	β
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
"	"	kIx"	"
<g/>
nekontextová	kontextový	k2eNgFnSc1d1	kontextový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
neterminál	neterminál	k1gInSc1	neterminál
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přepsat	přepsat	k5eAaPmF	přepsat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
okolní	okolní	k2eAgInSc4d1	okolní
kontext	kontext	k1gInSc4	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
gramatiky	gramatika	k1gFnSc2	gramatika
kontextové	kontextový	k2eAgInPc1d1	kontextový
(	(	kIx(	(
<g/>
kontext	kontext	k1gInSc1	kontext
je	být	k5eAaImIp3nS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
generované	generovaný	k2eAgInPc1d1	generovaný
bezkontextovými	bezkontextový	k2eAgFnPc7d1	bezkontextová
gramatikami	gramatika	k1gFnPc7	gramatika
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bezkontextové	bezkontextový	k2eAgFnPc1d1	bezkontextová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Termíny	termín	k1gInPc1	termín
==	==	k?	==
</s>
</p>
<p>
<s>
Bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
konečnou	konečný	k2eAgFnSc7d1	konečná
množinou	množina	k1gFnSc7	množina
neterminálů	neterminál	k1gInPc2	neterminál
(	(	kIx(	(
<g/>
neterminálních	terminální	k2eNgInPc2d1	neterminální
symbolů	symbol	k1gInPc2	symbol
-	-	kIx~	-
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konečnou	konečný	k2eAgFnSc7d1	konečná
množinou	množina	k1gFnSc7	množina
terminálů	terminál	k1gInPc2	terminál
(	(	kIx(	(
<g/>
terminálních	terminální	k2eAgInPc2d1	terminální
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
společné	společný	k2eAgInPc4d1	společný
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
množinou	množina	k1gFnSc7	množina
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
,	,	kIx,	,
počátečním	počáteční	k2eAgInSc7d1	počáteční
neterminálem	neterminál	k1gInSc7	neterminál
S	s	k7c7	s
a	a	k8xC	a
konečnou	konečný	k2eAgFnSc7d1	konečná
množinou	množina	k1gFnSc7	množina
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
tvaru	tvar	k1gInSc2	tvar
A	a	k8xC	a
→	→	k?	→
β	β	k?	β
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
A	a	k8xC	a
přepiš	přepsat	k5eAaPmRp2nS	přepsat
na	na	k7c4	na
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
A	a	k9	a
je	být	k5eAaImIp3nS	být
neterminál	neterminál	k1gInSc1	neterminál
a	a	k8xC	a
β	β	k?	β
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
z	z	k7c2	z
neterminálů	neterminál	k1gInPc2	neterminál
a	a	k8xC	a
terminálů	terminál	k1gInPc2	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přepsání	přepsání	k1gNnSc1	přepsání
je	být	k5eAaImIp3nS	být
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
složeném	složený	k2eAgInSc6d1	složený
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
neterminálů	neterminál	k1gInPc2	neterminál
nahradí	nahradit	k5eAaPmIp3nS	nahradit
podřetězec	podřetězec	k1gInSc1	podřetězec
tvořící	tvořící	k2eAgFnSc4d1	tvořící
levou	levý	k2eAgFnSc4d1	levá
stranu	strana	k1gFnSc4	strana
nějakého	nějaký	k3yIgNnSc2	nějaký
pravidla	pravidlo	k1gNnSc2	pravidlo
gramatiky	gramatika	k1gFnSc2	gramatika
pravou	pravý	k2eAgFnSc7d1	pravá
stranou	strana	k1gFnSc7	strana
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
přepsání	přepsání	k1gNnSc4	přepsání
pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
přepisovacího	přepisovací	k2eAgNnSc2d1	přepisovací
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odvození	odvození	k1gNnSc1	odvození
neboli	neboli	k8xC	neboli
derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
na	na	k7c4	na
řetězec	řetězec	k1gInSc4	řetězec
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
konečný	konečný	k2eAgInSc1d1	konečný
počet	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
žádné	žádný	k3yNgNnSc1	žádný
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
přepsání	přepsání	k1gNnSc1	přepsání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větná	větný	k2eAgFnSc1d1	větná
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
sentential	sentential	k1gMnSc1	sentential
form	form	k1gMnSc1	form
<g/>
)	)	kIx)	)
určité	určitý	k2eAgFnSc2d1	určitá
gramatiky	gramatika	k1gFnSc2	gramatika
je	být	k5eAaImIp3nS	být
libovolný	libovolný	k2eAgInSc4d1	libovolný
řetězec	řetězec	k1gInSc4	řetězec
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
počátečního	počáteční	k2eAgInSc2d1	počáteční
symbolu	symbol	k1gInSc2	symbol
konečným	konečný	k2eAgInSc7d1	konečný
počtem	počet	k1gInSc7	počet
přepsání	přepsání	k1gNnSc2	přepsání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řetězec	řetězec	k1gInSc1	řetězec
generovaný	generovaný	k2eAgInSc1d1	generovaný
danou	daný	k2eAgFnSc7d1	daná
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
formálně	formálně	k6eAd1	formálně
věta	věta	k1gFnSc1	věta
gramatiky	gramatika	k1gFnSc2	gramatika
je	být	k5eAaImIp3nS	být
větná	větný	k2eAgFnSc1d1	větná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pouze	pouze	k6eAd1	pouze
terminálními	terminální	k2eAgInPc7d1	terminální
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
generovaný	generovaný	k2eAgInSc1d1	generovaný
gramatikou	gramatika	k1gFnSc7	gramatika
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
generovaných	generovaný	k2eAgFnPc2d1	generovaná
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levá	levý	k2eAgFnSc1d1	levá
derivace	derivace	k1gFnSc1	derivace
neboli	neboli	k8xC	neboli
levé	levý	k2eAgNnSc1d1	levé
odvození	odvození	k1gNnSc1	odvození
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
leftmost	leftmost	k1gFnSc1	leftmost
derivation	derivation	k1gInSc1	derivation
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
postup	postup	k1gInSc4	postup
při	při	k7c6	při
generování	generování	k1gNnSc6	generování
určitého	určitý	k2eAgNnSc2d1	určité
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
bezkontextové	bezkontextový	k2eAgFnSc6d1	bezkontextová
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
první	první	k4xOgInSc1	první
neterminální	terminální	k2eNgInSc1d1	neterminální
symbol	symbol	k1gInSc1	symbol
zleva	zleva	k6eAd1	zleva
<g/>
.	.	kIx.	.
</s>
<s>
Bezkontextovost	Bezkontextovost	k1gFnSc1	Bezkontextovost
gramatiky	gramatika	k1gFnSc2	gramatika
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přepisovat	přepisovat	k5eAaImF	přepisovat
neterminály	neterminála	k1gFnPc4	neterminála
v	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
derivace	derivace	k1gFnSc1	derivace
neboli	neboli	k8xC	neboli
pravé	pravý	k2eAgNnSc1d1	pravé
odvození	odvození	k1gNnSc1	odvození
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
rightmost	rightmost	k1gFnSc1	rightmost
derivation	derivation	k1gInSc1	derivation
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
postup	postup	k1gInSc4	postup
při	při	k7c6	při
generování	generování	k1gNnSc6	generování
určitého	určitý	k2eAgNnSc2d1	určité
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
bezkontextové	bezkontextový	k2eAgFnSc6d1	bezkontextová
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
poslední	poslední	k2eAgInSc1d1	poslední
neterminální	terminální	k2eNgInSc1d1	neterminální
symbol	symbol	k1gInSc1	symbol
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
první	první	k4xOgFnSc7	první
zprava	zprava	k6eAd1	zprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formální	formální	k2eAgFnPc1d1	formální
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
čtveřice	čtveřice	k1gFnSc1	čtveřice
G	G	kA	G
=	=	kIx~	=
(	(	kIx(	(
<g/>
VN	VN	kA	VN
<g/>
,	,	kIx,	,
VT	VT	kA	VT
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
<s>
VN	VN	kA	VN
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
neterminálních	terminální	k2eNgInPc2d1	neterminální
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
VT	VT	kA	VT
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
terminálních	terminální	k2eAgInPc2d1	terminální
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
terminálů	terminál	k1gInPc2	terminál
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
množiny	množina	k1gFnPc4	množina
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgInPc1	žádný
společné	společný	k2eAgInPc1d1	společný
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
VT	VT	kA	VT
∩	∩	k?	∩
VN	VN	kA	VN
=	=	kIx~	=
ø	ø	k?	ø
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
P	P	kA	P
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
tvaru	tvar	k1gInSc2	tvar
A	a	k8xC	a
→	→	k?	→
β	β	k?	β
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
neterminál	neterminál	k1gInSc1	neterminál
<g/>
,	,	kIx,	,
A	a	k8xC	a
∈	∈	k?	∈
VN	VN	kA	VN
</s>
</p>
<p>
<s>
β	β	k?	β
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
,	,	kIx,	,
β	β	k?	β
∈	∈	k?	∈
(	(	kIx(	(
<g/>
VN	VN	kA	VN
∪	∪	k?	∪
VT	VT	kA	VT
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgInSc1d1	počáteční
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
VN	VN	kA	VN
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
.	.	kIx.	.
<g/>
Přepsání	přepsání	k1gNnSc1	přepsání
α	α	k?	α
⊨	⊨	k?	⊨
α	α	k?	α
je	být	k5eAaImIp3nS	být
nahrazení	nahrazení	k1gNnSc4	nahrazení
řetězce	řetězec	k1gInPc1	řetězec
α	α	k?	α
řetězcem	řetězec	k1gInSc7	řetězec
α	α	k?	α
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
A	a	k9	a
→	→	k?	→
β	β	k?	β
je	být	k5eAaImIp3nS	být
pravidlo	pravidlo	k1gNnSc1	pravidlo
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
α	α	k?	α
<g/>
,	,	kIx,	,
<g/>
γ	γ	k?	γ
∈	∈	k?	∈
(	(	kIx(	(
<g/>
VN	VN	kA	VN
∪	∪	k?	∪
VT	VT	kA	VT
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Derivace	derivace	k1gFnSc1	derivace
⊨	⊨	k?	⊨
<g/>
*	*	kIx~	*
je	být	k5eAaImIp3nS	být
tranzitivní	tranzitivní	k2eAgInSc4d1	tranzitivní
a	a	k8xC	a
reflexívní	reflexívní	k2eAgInSc4d1	reflexívní
uzávěr	uzávěr	k1gInSc4	uzávěr
přepsání	přepsání	k1gNnSc2	přepsání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc4	jazyk
generovaný	generovaný	k2eAgInSc4d1	generovaný
gramatikou	gramatika	k1gFnSc7	gramatika
G	G	kA	G
je	být	k5eAaImIp3nS	být
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
=	=	kIx~	=
{	{	kIx(	{
w	w	k?	w
|	|	kIx~	|
w	w	k?	w
∈	∈	k?	∈
VT	VT	kA	VT
<g/>
*	*	kIx~	*
&	&	k?	&
S	s	k7c7	s
⊨	⊨	k?	⊨
<g/>
*	*	kIx~	*
w	w	k?	w
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
přepisovacím	přepisovací	k2eAgNnSc7d1	přepisovací
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
aSb	aSb	k?	aSb
|	|	kIx~	|
ab	ab	k?	ab
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
A	a	k9	a
→	→	k?	→
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
kde	kde	k6eAd1	kde
znak	znak	k1gInSc1	znak
|	|	kIx~	|
separuje	separovat	k5eAaBmIp3nS	separovat
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
řetězců	řetězec	k1gInPc2	řetězec
(	(	kIx(	(
<g/>
w	w	k?	w
<g/>
)	)	kIx)	)
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znamenají	znamenat	k5eAaImIp3nP	znamenat
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
jako	jako	k9	jako
zápis	zápis	k1gInSc1	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
aSb	aSb	k?	aSb
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
abkde	abkde	k6eAd1	abkde
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
označuje	označovat	k5eAaImIp3nS	označovat
terminály	terminál	k1gInPc4	terminál
<g/>
,	,	kIx,	,
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
startovní	startovní	k2eAgInSc1d1	startovní
symbol	symbol	k1gInSc1	symbol
(	(	kIx(	(
<g/>
neterminál	neterminál	k1gInSc1	neterminál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
gramatika	gramatika	k1gFnSc1	gramatika
generuje	generovat	k5eAaImIp3nS	generovat
jazyk	jazyk	k1gInSc4	jazyk
anbn	anbna	k1gFnPc2	anbna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
regulární	regulární	k2eAgInSc1d1	regulární
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
symbol	symbol	k1gInSc1	symbol
ε	ε	k?	ε
označuje	označovat	k5eAaImIp3nS	označovat
prázdný	prázdný	k2eAgInSc1d1	prázdný
řetězec	řetězec	k1gInSc1	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
změníme	změnit	k5eAaPmIp1nP	změnit
pravidlo	pravidlo	k1gNnSc4	pravidlo
na	na	k7c4	na
S	s	k7c7	s
→	→	k?	→
aSb	aSb	k?	aSb
|	|	kIx~	|
ε	ε	k?	ε
získáme	získat	k5eAaPmIp1nP	získat
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
jazyk	jazyk	k1gInSc4	jazyk
anbn	anbna	k1gFnPc2	anbna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tato	tento	k3xDgNnPc1	tento
přepisovací	přepisovací	k2eAgNnPc1d1	přepisovací
pravidlo	pravidlo	k1gNnSc1	pravidlo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
přepis	přepis	k1gInSc4	přepis
na	na	k7c4	na
prázdný	prázdný	k2eAgInSc4d1	prázdný
řetězec	řetězec	k1gInSc4	řetězec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
původní	původní	k2eAgFnSc1d1	původní
gramatika	gramatika	k1gFnSc1	gramatika
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
generuje	generovat	k5eAaImIp3nS	generovat
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
výrazy	výraz	k1gInPc4	výraz
s	s	k7c7	s
proměnnými	proměnná	k1gFnPc7	proměnná
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
x	x	k?	x
|	|	kIx~	|
y	y	k?	y
|	|	kIx~	|
z	z	k7c2	z
|	|	kIx~	|
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
|	|	kIx~	|
S	s	k7c7	s
-	-	kIx~	-
S	s	k7c7	s
|	|	kIx~	|
S	s	k7c7	s
*	*	kIx~	*
S	s	k7c7	s
|	|	kIx~	|
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
S	s	k7c7	s
|	|	kIx~	|
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
gramatikou	gramatika	k1gFnSc7	gramatika
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
například	například	k6eAd1	například
generovat	generovat	k5eAaImF	generovat
řetězec	řetězec	k1gInSc4	řetězec
"	"	kIx"	"
<g/>
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
)	)	kIx)	)
*	*	kIx~	*
x	x	k?	x
-	-	kIx~	-
z	z	k7c2	z
*	*	kIx~	*
y	y	k?	y
/	/	kIx~	/
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
x	x	k?	x
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
řetězec	řetězec	k1gInSc4	řetězec
získáme	získat	k5eAaPmIp1nP	získat
následujícím	následující	k2eAgInSc7d1	následující
postupem	postup	k1gInSc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Startovací	startovací	k2eAgInSc4d1	startovací
symbol	symbol	k1gInSc4	symbol
S	s	k7c7	s
přepíšeme	přepsat	k5eAaPmIp1nP	přepsat
podle	podle	k7c2	podle
páté	pátý	k4xOgFnSc2	pátý
transformace	transformace	k1gFnSc2	transformace
[	[	kIx(	[
<g/>
S	s	k7c7	s
→	→	k?	→
S	s	k7c7	s
-	-	kIx~	-
S	s	k7c7	s
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
S	s	k7c7	s
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
přepíší	přepsat	k5eAaPmIp3nP	přepsat
podle	podle	k7c2	podle
šesté	šestý	k4xOgFnSc2	šestý
a	a	k8xC	a
sedmé	sedmý	k4xOgFnSc2	sedmý
transformace	transformace	k1gFnSc2	transformace
na	na	k7c4	na
řetězec	řetězec	k1gInSc4	řetězec
"	"	kIx"	"
<g/>
S	s	k7c7	s
*	*	kIx~	*
S	s	k7c7	s
-	-	kIx~	-
S	s	k7c7	s
/	/	kIx~	/
S	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
použijeme	použít	k5eAaPmIp1nP	použít
poslední	poslední	k2eAgFnSc4d1	poslední
transformaci	transformace	k1gFnSc4	transformace
s	s	k7c7	s
uzávorkováním	uzávorkování	k1gNnSc7	uzávorkování
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
získáme	získat	k5eAaPmIp1nP	získat
"	"	kIx"	"
<g/>
(	(	kIx(	(
S	s	k7c7	s
)	)	kIx)	)
*	*	kIx~	*
S	s	k7c7	s
-	-	kIx~	-
S	s	k7c7	s
/	/	kIx~	/
(	(	kIx(	(
S	s	k7c7	s
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
uzávorkovaná	uzávorkovaný	k2eAgNnPc4d1	uzávorkovaný
S	s	k7c7	s
přepíšeme	přepsat	k5eAaPmIp1nP	přepsat
podle	podle	k7c2	podle
transformace	transformace	k1gFnSc2	transformace
[	[	kIx(	[
<g/>
S	s	k7c7	s
→	→	k?	→
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
dostanem	dostanem	k?	dostanem
řetězec	řetězec	k1gInSc4	řetězec
neterminálů	neterminál	k1gInPc2	neterminál
"	"	kIx"	"
<g/>
(	(	kIx(	(
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
)	)	kIx)	)
*	*	kIx~	*
S	s	k7c7	s
-	-	kIx~	-
S	s	k7c7	s
*	*	kIx~	*
S	s	k7c7	s
/	/	kIx~	/
(	(	kIx(	(
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgNnSc2	který
výsledný	výsledný	k2eAgInSc4d1	výsledný
řetězec	řetězec	k1gInSc4	řetězec
"	"	kIx"	"
<g/>
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
)	)	kIx)	)
*	*	kIx~	*
x	x	k?	x
-	-	kIx~	-
z	z	k7c2	z
*	*	kIx~	*
y	y	k?	y
/	/	kIx~	/
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
x	x	k?	x
)	)	kIx)	)
<g/>
"	"	kIx"	"
získáme	získat	k5eAaPmIp1nP	získat
převedením	převedení	k1gNnSc7	převedení
neterminálů	neterminál	k1gInPc2	neterminál
S	s	k7c7	s
na	na	k7c4	na
terminály	terminál	k1gInPc4	terminál
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z.	z.	k?	z.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
3	[number]	k4	3
===	===	k?	===
</s>
</p>
<p>
<s>
Bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
obsahující	obsahující	k2eAgInPc4d1	obsahující
znaky	znak	k1gInPc1	znak
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
počtu	počet	k1gInSc6	počet
a	a	k8xC	a
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
U	U	kA	U
|	|	kIx~	|
V	v	k7c6	v
</s>
</p>
<p>
<s>
U	u	k7c2	u
→	→	k?	→
TaU	tau	k1gNnSc2	tau
|	|	kIx~	|
TaT	TaT	k1gFnSc1	TaT
</s>
</p>
<p>
<s>
V	v	k7c6	v
→	→	k?	→
TbV	TbV	k1gMnSc1	TbV
|	|	kIx~	|
TbT	TbT	k1gMnSc1	TbT
</s>
</p>
<p>
<s>
T	T	kA	T
→	→	k?	→
aTbT	aTbT	k?	aTbT
|	|	kIx~	|
bTaT	bTaT	k?	bTaT
|	|	kIx~	|
ε	ε	k?	ε
T	T	kA	T
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
generovat	generovat	k5eAaImF	generovat
všechny	všechen	k3xTgInPc4	všechen
řetězce	řetězec	k1gInPc4	řetězec
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
áček	áčko	k1gNnPc2	áčko
a	a	k8xC	a
béček	béčko	k1gNnPc2	béčko
<g/>
.	.	kIx.	.
</s>
<s>
Neterminál	Neterminál	k1gInSc1	Neterminál
U	U	kA	U
generuje	generovat	k5eAaImIp3nS	generovat
řetězec	řetězec	k1gInSc1	řetězec
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
áček	áčko	k1gNnPc2	áčko
než	než	k8xS	než
béček	béčko	k1gNnPc2	béčko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
neterminál	neterminál	k1gInSc1	neterminál
V	V	kA	V
generuje	generovat	k5eAaImIp3nS	generovat
řetězec	řetězec	k1gInSc1	řetězec
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
béček	béčko	k1gNnPc2	béčko
než	než	k8xS	než
áček	áčko	k1gNnPc2	áčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
4	[number]	k4	4
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
jazyk	jazyk	k1gInSc4	jazyk
{	{	kIx(	{
<g/>
bnam	bnam	k6eAd1	bnam
<g/>
2	[number]	k4	2
<g/>
*	*	kIx~	*
<g/>
bn	bn	k?	bn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
není	být	k5eNaImIp3nS	být
regulární	regulární	k2eAgInSc1d1	regulární
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
bezkontextový	bezkontextový	k2eAgMnSc1d1	bezkontextový
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
generován	generovat	k5eAaImNgMnS	generovat
bezkontextovou	bezkontextový	k2eAgFnSc7d1	bezkontextová
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
bSbb	bSbb	k1gInSc1	bSbb
|	|	kIx~	|
A	a	k9	a
</s>
</p>
<p>
<s>
A	a	k9	a
→	→	k?	→
aA	aA	k?	aA
|	|	kIx~	|
ε	ε	k?	ε
</s>
</p>
<p>
<s>
===	===	k?	===
Derivace	derivace	k1gFnPc4	derivace
a	a	k8xC	a
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
způsobů	způsob	k1gInPc2	způsob
jak	jak	k6eAd1	jak
získat	získat	k5eAaPmF	získat
potřebný	potřebný	k2eAgInSc4d1	potřebný
řetězec	řetězec	k1gInSc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
jak	jak	k8xS	jak
jej	on	k3xPp3gInSc4	on
získat	získat	k5eAaPmF	získat
je	on	k3xPp3gNnSc4	on
derivování	derivování	k1gNnSc4	derivování
od	od	k7c2	od
startovního	startovní	k2eAgInSc2d1	startovní
symbolu	symbol	k1gInSc2	symbol
pomocí	pomocí	k7c2	pomocí
dané	daný	k2eAgFnSc2d1	daná
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
cestou	cesta	k1gFnSc7	cesta
je	být	k5eAaImIp3nS	být
výpis	výpis	k1gInSc1	výpis
mezikroků	mezikrok	k1gInPc2	mezikrok
od	od	k7c2	od
startovního	startovní	k2eAgInSc2d1	startovní
symbolu	symbol	k1gInSc2	symbol
až	až	k9	až
po	po	k7c4	po
výsledný	výsledný	k2eAgInSc4d1	výsledný
řetězec	řetězec	k1gInSc4	řetězec
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
výpis	výpis	k1gInSc4	výpis
použitých	použitý	k2eAgNnPc2d1	Použité
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
použijeme	použít	k5eAaPmIp1nP	použít
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
vždy	vždy	k6eAd1	vždy
nejprve	nejprve	k6eAd1	nejprve
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
nejlevější	levý	k2eAgInSc4d3	nejlevější
neterminál	neterminál	k1gInSc4	neterminál
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
kontextovou	kontextový	k2eAgFnSc4d1	kontextová
gramatiku	gramatika	k1gFnSc4	gramatika
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
pravidel	pravidlo	k1gNnPc2	pravidlo
použité	použitý	k2eAgFnSc2d1	použitá
gramatiky	gramatika	k1gFnSc2	gramatika
dostačující	dostačující	k2eAgFnSc2d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nazýváme	nazývat	k5eAaImIp1nP	nazývat
"	"	kIx"	"
<g/>
levou	levý	k2eAgFnSc7d1	levá
derivací	derivace	k1gFnSc7	derivace
<g/>
"	"	kIx"	"
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
vezmeme	vzít	k5eAaPmIp1nP	vzít
tuto	tento	k3xDgFnSc4	tento
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
S	s	k7c7	s
→	→	k?	→
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
S	s	k7c7	s
→	→	k?	→
1	[number]	k4	1
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
S	s	k7c7	s
→	→	k?	→
aa	aa	k?	aa
řetězec	řetězec	k1gInSc1	řetězec
"	"	kIx"	"
<g/>
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
levá	levý	k2eAgFnSc1d1	levá
derivace	derivace	k1gFnSc1	derivace
tohoto	tento	k3xDgInSc2	tento
řetězce	řetězec	k1gInSc2	řetězec
bude	být	k5eAaImBp3nS	být
posloupnost	posloupnost	k1gFnSc1	posloupnost
použitých	použitý	k2eAgNnPc2d1	Použité
pravidel	pravidlo	k1gNnPc2	pravidlo
[	[	kIx(	[
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
u	u	k7c2	u
pravé	pravý	k2eAgFnSc2d1	pravá
derivace	derivace	k1gFnSc2	derivace
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nejprve	nejprve	k6eAd1	nejprve
nahradí	nahradit	k5eAaPmIp3nS	nahradit
nejpravější	pravý	k2eAgInSc4d3	nejpravější
neterminál	neterminál	k1gInSc4	neterminál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
bude	být	k5eAaImBp3nS	být
seznam	seznam	k1gInSc1	seznam
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
[	[	kIx(	[
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
levou	levý	k2eAgFnSc7d1	levá
a	a	k8xC	a
pravou	pravý	k2eAgFnSc7d1	pravá
derivací	derivace	k1gFnSc7	derivace
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
parsovacích	parsovací	k2eAgFnPc2d1	parsovací
transformací	transformace	k1gFnPc2	transformace
vstupu	vstup	k1gInSc2	vstup
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
kus	kus	k1gInSc1	kus
kódu	kód	k1gInSc2	kód
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
pravidlo	pravidlo	k1gNnSc4	pravidlo
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
při	při	k7c6	při
parsování	parsování	k1gNnSc6	parsování
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
zvolit	zvolit	k5eAaPmF	zvolit
levou	levý	k2eAgFnSc4d1	levá
nebo	nebo	k8xC	nebo
pravou	pravý	k2eAgFnSc4d1	pravá
derivaci	derivace	k1gFnSc4	derivace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
provádět	provádět	k5eAaImF	provádět
části	část	k1gFnPc1	část
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
LL	LL	kA	LL
syntaktický	syntaktický	k2eAgMnSc1d1	syntaktický
analyzátor	analyzátor	k1gMnSc1	analyzátor
a	a	k8xC	a
LR	LR	kA	LR
syntaktický	syntaktický	k2eAgMnSc1d1	syntaktický
analyzátor	analyzátor	k1gMnSc1	analyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
derivace	derivace	k1gFnSc2	derivace
uložíme	uložit	k5eAaPmIp1nP	uložit
hierarchickou	hierarchický	k2eAgFnSc4d1	hierarchická
strukturu	struktura	k1gFnSc4	struktura
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
derivován	derivován	k2eAgInSc1d1	derivován
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
poslouží	posloužit	k5eAaPmIp3nS	posloužit
řetězec	řetězec	k1gInSc1	řetězec
"	"	kIx"	"
<g/>
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
derivován	derivovat	k5eAaBmNgInS	derivovat
podle	podle	k7c2	podle
levé	levý	k2eAgFnSc2d1	levá
derivace	derivace	k1gFnSc2	derivace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
a	a	k8xC	a
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
řetězcová	řetězcový	k2eAgFnSc1d1	řetězcová
struktura	struktura	k1gFnSc1	struktura
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaImF	vypadat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
{	{	kIx(	{
{	{	kIx(	{
{	{	kIx(	{
1	[number]	k4	1
}	}	kIx)	}
<g/>
S	s	k7c7	s
+	+	kIx~	+
{	{	kIx(	{
1	[number]	k4	1
}	}	kIx)	}
<g/>
S	s	k7c7	s
}	}	kIx)	}
<g/>
S	s	k7c7	s
+	+	kIx~	+
{	{	kIx(	{
a	a	k8xC	a
}	}	kIx)	}
<g/>
S	s	k7c7	s
}	}	kIx)	}
<g/>
Skde	Skde	k1gNnSc7	Skde
{	{	kIx(	{
...	...	k?	...
}	}	kIx)	}
<g/>
S	s	k7c7	s
rozpoznaný	rozpoznaný	k2eAgInSc4d1	rozpoznaný
jako	jako	k9	jako
podřetězec	podřetězec	k1gInSc4	podřetězec
S.	S.	kA	S.
Tato	tento	k3xDgFnSc1	tento
hierarchie	hierarchie	k1gFnSc1	hierarchie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
jako	jako	k9	jako
strom	strom	k1gInSc1	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
</s>
</p>
<p>
<s>
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
</s>
</p>
<p>
<s>
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
</s>
</p>
<p>
<s>
S	s	k7c7	s
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
S	s	k7c7	s
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
|	|	kIx~	|
</s>
</p>
<p>
<s>
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
|	|	kIx~	|
</s>
</p>
<p>
<s>
S	s	k7c7	s
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
S	s	k7c7	s
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
|	|	kIx~	|
|	|	kIx~	|
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
1	[number]	k4	1
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
1	[number]	k4	1
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
strom	strom	k1gInSc1	strom
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
abstraktní	abstraktní	k2eAgInSc4d1	abstraktní
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
<g/>
)	)	kIx)	)
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
levá	levý	k2eAgFnSc1d1	levá
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
derivace	derivace	k1gFnSc1	derivace
definuje	definovat	k5eAaBmIp3nS	definovat
stejný	stejný	k2eAgInSc4d1	stejný
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
řetězce	řetězec	k1gInSc2	řetězec
můžeme	moct	k5eAaImIp1nP	moct
pomocí	pomocí	k7c2	pomocí
levé	levý	k2eAgFnSc2d1	levá
derivace	derivace	k1gFnSc2	derivace
získat	získat	k5eAaPmF	získat
jinou	jiný	k2eAgFnSc4d1	jiná
stromovou	stromový	k2eAgFnSc4d1	stromová
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
záměnou	záměna	k1gFnSc7	záměna
prováděných	prováděný	k2eAgNnPc2d1	prováděné
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
→	→	k?	→
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
S	s	k7c7	s
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
S	s	k7c7	s
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
→	→	k?	→
1	[number]	k4	1
+	+	kIx~	+
1	[number]	k4	1
+	+	kIx~	+
a	a	k8xC	a
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
ta	ten	k3xDgFnSc1	ten
definuje	definovat	k5eAaBmIp3nS	definovat
následný	následný	k2eAgInSc4d1	následný
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
strom	strom	k1gInSc4	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
</s>
</p>
<p>
<s>
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
</s>
</p>
<p>
<s>
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
</s>
</p>
<p>
<s>
S	s	k7c7	s
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
S	s	k7c7	s
</s>
</p>
<p>
<s>
|	|	kIx~	|
/	/	kIx~	/
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
</s>
</p>
<p>
<s>
|	|	kIx~	|
/	/	kIx~	/
|	|	kIx~	|
\	\	kIx~	\
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
1	[number]	k4	1
<g/>
'	'	kIx"	'
S	s	k7c7	s
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
S	s	k7c7	s
</s>
</p>
<p>
<s>
|	|	kIx~	|
|	|	kIx~	|
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
1	[number]	k4	1
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
řetězec	řetězec	k1gInSc4	řetězec
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
gramatiky	gramatika	k1gFnSc2	gramatika
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
jeden	jeden	k4xCgInSc1	jeden
parsovací	parsovací	k2eAgInSc1d1	parsovací
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
gramatika	gramatika	k1gFnSc1	gramatika
nazývá	nazývat	k5eAaImIp3nS	nazývat
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
gramatiky	gramatika	k1gFnPc1	gramatika
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
těžko	těžko	k6eAd1	těžko
rozebírat	rozebírat	k5eAaImF	rozebírat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
syntaktický	syntaktický	k2eAgInSc1d1	syntaktický
analyzátor	analyzátor	k1gInSc1	analyzátor
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
z	z	k7c2	z
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
má	mít	k5eAaImIp3nS	mít
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
mnohoznačnost	mnohoznačnost	k1gFnSc1	mnohoznačnost
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
gramatika	gramatika	k1gFnSc1	gramatika
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
generovat	generovat	k5eAaImF	generovat
stejný	stejný	k2eAgInSc4d1	stejný
bezkontextový	bezkontextový	k2eAgInSc4d1	bezkontextový
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nelze	lze	k6eNd1	lze
vygenerovat	vygenerovat	k5eAaPmF	vygenerovat
žádnou	žádný	k3yNgFnSc7	žádný
jednoznačnou	jednoznačný	k2eAgFnSc7d1	jednoznačná
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
podstatně	podstatně	k6eAd1	podstatně
nejednoznačné	jednoznačný	k2eNgFnPc1d1	nejednoznačná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Normální	normální	k2eAgFnPc4d1	normální
formy	forma	k1gFnPc4	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
bezkontextové	bezkontextový	k2eAgFnSc3d1	bezkontextová
gramatice	gramatika	k1gFnSc3	gramatika
lze	lze	k6eAd1	lze
sestrojit	sestrojit	k5eAaPmF	sestrojit
gramatiku	gramatika	k1gFnSc4	gramatika
v	v	k7c4	v
Chomského	Chomský	k2eAgMnSc4d1	Chomský
normální	normální	k2eAgFnSc6d1	normální
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
v	v	k7c6	v
Greibachové	Greibachový	k2eAgFnSc6d1	Greibachový
normální	normální	k2eAgFnSc6d1	normální
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
gramatikou	gramatika	k1gFnSc7	gramatika
ekvivalentní	ekvivalentní	k2eAgFnSc7d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
gramatiky	gramatika	k1gFnPc1	gramatika
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
generují	generovat	k5eAaImIp3nP	generovat
stejný	stejný	k2eAgInSc4d1	stejný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
vytváření	vytváření	k1gNnSc2	vytváření
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
Chomského	Chomský	k2eAgMnSc4d1	Chomský
normální	normální	k2eAgFnSc1d1	normální
forma	forma	k1gFnSc1	forma
vhodná	vhodný	k2eAgFnSc1d1	vhodná
jak	jak	k8xS	jak
pro	pro	k7c4	pro
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
praktickou	praktický	k2eAgFnSc4d1	praktická
realizaci	realizace	k1gFnSc4	realizace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
bezkontextové	bezkontextový	k2eAgFnSc2d1	bezkontextová
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
Chomského	Chomský	k2eAgMnSc4d1	Chomský
normální	normální	k2eAgNnSc1d1	normální
formu	forma	k1gFnSc4	forma
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
časově	časově	k6eAd1	časově
polynomicky	polynomicky	k6eAd1	polynomicky
náročného	náročný	k2eAgInSc2d1	náročný
algoritmu	algoritmus	k1gInSc2	algoritmus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
daný	daný	k2eAgInSc1d1	daný
řetězec	řetězec	k1gInSc1	řetězec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
reprezentovaným	reprezentovaný	k2eAgMnSc7d1	reprezentovaný
gramatikou	gramatika	k1gFnSc7	gramatika
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
CYK	CYK	kA	CYK
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nerozhodnutelné	rozhodnutelný	k2eNgInPc4d1	nerozhodnutelný
problémy	problém	k1gInPc4	problém
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
některé	některý	k3yIgFnPc4	některý
operace	operace	k1gFnPc4	operace
s	s	k7c7	s
bezkontextovými	bezkontextový	k2eAgFnPc7d1	bezkontextová
gramatikami	gramatika	k1gFnPc7	gramatika
jsou	být	k5eAaImIp3nP	být
rozhodnutelné	rozhodnutelný	k2eAgInPc1d1	rozhodnutelný
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
omezené	omezený	k2eAgFnSc6d1	omezená
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
bezkontextové	bezkontextový	k2eAgFnPc1d1	bezkontextová
gramatiky	gramatika	k1gFnPc1	gramatika
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
i	i	k9	i
o	o	k7c4	o
nerozhodnutelné	rozhodnutelný	k2eNgInPc4d1	nerozhodnutelný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejjednodušších	jednoduchý	k2eAgMnPc2d3	nejjednodušší
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
řešených	řešený	k2eAgInPc2d1	řešený
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
zda	zda	k8xS	zda
gramatika	gramatika	k1gFnSc1	gramatika
příjme	příjem	k1gInSc5	příjem
všechny	všechen	k3xTgInPc1	všechen
řetězce	řetězec	k1gInPc1	řetězec
(	(	kIx(	(
<g/>
slova	slovo	k1gNnPc1	slovo
<g/>
)	)	kIx)	)
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
dobře	dobře	k6eAd1	dobře
známý	známý	k2eAgInSc1d1	známý
Turingův	Turingův	k2eAgInSc1d1	Turingův
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Redukce	redukce	k1gFnSc1	redukce
využívá	využívat	k5eAaImIp3nS	využívat
koncept	koncept	k1gInSc4	koncept
historie	historie	k1gFnSc2	historie
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
řetězce	řetězec	k1gInSc2	řetězec
popisujeme	popisovat	k5eAaImIp1nP	popisovat
celkový	celkový	k2eAgInSc4d1	celkový
výpočet	výpočet	k1gInSc4	výpočet
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dokážeme	dokázat	k5eAaPmIp1nP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
bezkontextovou	bezkontextový	k2eAgFnSc4d1	bezkontextová
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
neakceptují	akceptovat	k5eNaBmIp3nP	akceptovat
historii	historie	k1gFnSc4	historie
výpočtu	výpočet	k1gInSc2	výpočet
pro	pro	k7c4	pro
specifický	specifický	k2eAgInSc4d1	specifický
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
specifický	specifický	k2eAgInSc4d1	specifický
vstup	vstup	k1gInSc4	vstup
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
přijímá	přijímat	k5eAaImIp3nS	přijímat
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
stroj	stroj	k1gInSc1	stroj
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
tento	tento	k3xDgInSc4	tento
vstup	vstup	k1gInSc4	vstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Regulární	regulární	k2eAgInPc1d1	regulární
a	a	k8xC	a
bezkontextové	bezkontextový	k2eAgInPc1d1	bezkontextový
jazyky	jazyk	k1gInPc1	jazyk
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
