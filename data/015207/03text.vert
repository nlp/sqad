<s>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
</s>
<s>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rodné	rodný	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Antoni	Anton	k1gMnPc1
Smykiewicz	Smykiewicza	k1gFnPc2
Jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc1d1
</s>
<s>
Antek	Antek	k6eAd1
Smykiewicz	Smykiewicz	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1987	#num#	k4
(	(	kIx(
<g/>
33	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
Žánry	žánr	k1gInPc4
</s>
<s>
Pop	pop	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
2010	#num#	k4
<g/>
—	—	k?
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Universal	Universat	k5eAaImAgMnS,k5eAaPmAgMnS
Music	Music	k1gMnSc1
Polska	Polsko	k1gNnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Antoni	Antoň	k1gFnSc3
Smykiewicz	Smykiewicz	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc4
1987	#num#	k4
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Mazovské	Mazovský	k2eAgNnSc1d1
vojvodství	vojvodství	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polský	polský	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
polský	polský	k2eAgInSc4d1
hudební	hudební	k2eAgInSc4d1
trh	trh	k1gInSc4
vstoupil	vstoupit	k5eAaPmAgMnS
hitem	hit	k1gInSc7
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
kariéry	kariéra	k1gFnSc2
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
započal	započnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
jako	jako	k8xS,k8xC
zpěvák	zpěvák	k1gMnSc1
v	v	k7c6
kapele	kapela	k1gFnSc6
Ściana	Ścian	k1gMnSc2
Wschodnia	Wschodnium	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
také	také	k9
spoluzaložil	spoluzaložit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
vyhrál	vyhrát	k5eAaPmAgInS
celostátní	celostátní	k2eAgInSc1d1
festival	festival	k1gInSc1
III	III	kA
Karaoke	Karaok	k1gFnSc2
MAXXX	MAXXX	kA
Festiwal	Festiwal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
Opolích	opolí	k1gNnPc6
organizovaný	organizovaný	k2eAgInSc4d1
rozhlasovou	rozhlasový	k2eAgFnSc4d1
stanice	stanice	k1gFnPc4
RMF	RMF	kA
Maxxx	Maxxx	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
první	první	k4xOgFnPc4
řady	řada	k1gFnPc4
soutěže	soutěž	k1gFnSc2
The	The	k1gFnPc2
Voice	Voice	k1gMnSc1
of	of	k?
Poland	Poland	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
zařadil	zařadit	k5eAaPmAgMnS
do	do	k7c2
týmu	tým	k1gInSc2
Andrzeje	Andrzeje	k1gMnSc2
Piaseczného	Piaseczný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpěvák	Zpěvák	k1gMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
obsadil	obsadit	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
talentové	talentový	k2eAgFnSc2d1
show	show	k1gFnSc2
Must	Must	k1gMnSc1
Be	Be	k1gMnSc1
the	the	k?
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tylko	Tylko	k1gNnSc1
muzyka	muzyek	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
2015	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
a	a	k8xC
album	album	k1gNnSc1
Nasz	Nasza	k1gFnPc2
film	film	k1gInSc1
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
2014	#num#	k4
zpěvák	zpěvák	k1gMnSc1
podepsal	podepsat	k5eAaPmAgMnS
nahrávací	nahrávací	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Universal	Universal	k1gFnSc7
Music	Musice	k1gInPc2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
vydal	vydat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
debutový	debutový	k2eAgInSc4d1
singl	singl	k1gInSc4
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
a	a	k8xC
složil	složit	k5eAaPmAgMnS
Marek	Marek	k1gMnSc1
Kościkiewicz	Kościkiewicz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Píseň	píseň	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hitem	hit	k1gInSc7
a	a	k8xC
vyšplhala	vyšplhat	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
žebříčku	žebříček	k1gInSc6
AirPlay	AirPlaa	k1gFnSc2
nejhranějších	hraný	k2eAgFnPc2d3
písní	píseň	k1gFnPc2
polských	polský	k2eAgNnPc2d1
rádií	rádio	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
píseň	píseň	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
Polsku	Polsko	k1gNnSc6
čtyřnásobné	čtyřnásobný	k2eAgFnSc2d1
platinové	platinový	k2eAgFnSc2d1
certifikace	certifikace	k1gFnSc2
za	za	k7c4
prodej	prodej	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
80	#num#	k4
000	#num#	k4
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
vystoupili	vystoupit	k5eAaPmAgMnP
během	během	k7c2
silvestrovského	silvestrovský	k2eAgInSc2d1
koncertu	koncert	k1gInSc2
v	v	k7c6
Krakově	Krakov	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
organizovala	organizovat	k5eAaBmAgFnS
stanice	stanice	k1gFnSc1
TVN	TVN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
jeho	jeho	k3xOp3gInSc1
druhý	druhý	k4xOgInSc1
singl	singl	k1gInSc1
„	„	k?
<g/>
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
vytvořen	vytvořit	k5eAaPmNgMnS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
samotným	samotný	k2eAgMnSc7d1
zpěvákem	zpěvák	k1gMnSc7
(	(	kIx(
<g/>
text	text	k1gInSc4
<g/>
)	)	kIx)
s	s	k7c7
Przemysławem	Przemysław	k1gMnSc7
Pukiem	Pukius	k1gMnSc7
(	(	kIx(
<g/>
hudba	hudba	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Sarsou	Sarsa	k1gFnSc7
(	(	kIx(
<g/>
text	text	k1gInSc1
a	a	k8xC
hudba	hudba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Kompozice	kompozice	k1gFnSc1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
nejvýše	vysoce	k6eAd3,k6eAd1
na	na	k7c4
21	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
žebříčku	žebříček	k1gInSc6
AirPlay	AirPlaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
vyšlo	vyjít	k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc4
debutové	debutový	k2eAgNnSc4d1
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
Nasz	Nasza	k1gFnPc2
film	film	k1gInSc1
(	(	kIx(
<g/>
Náš	náš	k3xOp1gInSc1
film	film	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
11	#num#	k4
skladeb	skladba	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
se	se	k3xPyFc4
autorsky	autorsky	k6eAd1
podílel	podílet	k5eAaImAgMnS
samotný	samotný	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
zpěvák	zpěvák	k1gMnSc1
Sarsa	Sarsa	k1gFnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Kościkiewicz	Kościkiewicz	k1gMnSc1
<g/>
,	,	kIx,
Ewa	Ewa	k1gMnSc2
Farna	Farn	k1gMnSc2
a	a	k8xC
Przemysław	Przemysław	k1gMnSc2
Puka	Pukus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c6
koncertě	koncert	k1gInSc6
Rádiový	rádiový	k2eAgInSc4d1
hit	hit	k1gInSc4
roku	rok	k1gInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
festivalu	festival	k1gInSc2
Polsat	Polsat	k1gFnSc2
SuperHit	superhit	k1gInSc1
Festiwal	Festiwal	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zazpíval	zazpívat	k5eAaPmAgInS
singl	singl	k1gInSc1
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
zařadil	zařadit	k5eAaPmAgInS
mezi	mezi	k7c4
jedny	jeden	k4xCgFnPc4
z	z	k7c2
nejčastěji	často	k6eAd3
vysílaných	vysílaný	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
v	v	k7c6
polských	polský	k2eAgFnPc6d1
rozhlasových	rozhlasový	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
v	v	k7c6
předchozím	předchozí	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
písní	píseň	k1gFnSc7
se	se	k3xPyFc4
na	na	k7c6
začátku	začátek	k1gInSc6
června	červen	k1gInSc2
zúčastnil	zúčastnit	k5eAaPmAgMnS
soutěže	soutěž	k1gFnPc4
Debiuty	Debiut	k2eAgFnPc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
rámci	rámec	k1gInSc6
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národního	národní	k2eAgInSc2d1
festivalu	festival	k1gInSc2
polské	polský	k2eAgFnSc2d1
písně	píseň	k1gFnSc2
v	v	k7c6
Opole	Opola	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1987	#num#	k4
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
devět	devět	k4xCc1
sourozenců	sourozenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Vystudoval	vystudovat	k5eAaPmAgMnS
hudební	hudební	k2eAgFnSc4d1
školu	škola	k1gFnSc4
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
akordeon	akordeon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Detaily	detail	k1gInPc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Nasz	Nasz	k1gInSc1
film	film	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
<g/>
:	:	kIx,
6	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2016	#num#	k4
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1
<g/>
:	:	kIx,
Universal	Universal	k1gFnSc1
Music	Musice	k1gInPc2
Polska	Polsko	k1gNnSc2
</s>
<s>
Formát	formát	k1gInSc1
<g/>
:	:	kIx,
CD	CD	kA
<g/>
,	,	kIx,
digital	digitat	k5eAaPmAgMnS,k5eAaImAgMnS
download	download	k6eAd1
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
žebříčku	žebříček	k1gInSc6
</s>
<s>
Certifikace	certifikace	k1gFnSc1
</s>
<s>
Prodeje	prodej	k1gInPc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
POL	pola	k1gFnPc2
<g/>
(	(	kIx(
<g/>
airplay	airpla	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
POL	pola	k1gFnPc2
<g/>
(	(	kIx(
<g/>
airplaynowości	airplaynowośce	k1gFnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RMF	RMF	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eska	eska	k1gFnSc1
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zet	zet	k1gNnSc1
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maxxx	Maxxx	k1gInSc1
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
SLiP	slip	k1gInSc1
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
</s>
<s>
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
</s>
<s>
1411213	#num#	k4
</s>
<s>
POL	pola	k1gFnPc2
<g/>
:	:	kIx,
4	#num#	k4
<g/>
×	×	k?
platina	platina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
POL	pola	k1gFnPc2
<g/>
:	:	kIx,
80	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nasz	Nasz	k1gInSc1
film	film	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
„	„	k?
<g/>
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
<g/>
”	”	k?
</s>
<s>
213	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
37	#num#	k4
</s>
<s>
„	„	k?
<g/>
Jak	jak	k8xC,k8xS
sen	sen	k1gInSc1
<g/>
”	”	k?
</s>
<s>
471	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
–	–	k?
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1
kompozice	kompozice	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Album	album	k1gNnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
„	„	k?
<g/>
Raj	Raj	k1gMnSc1
będzie	będzie	k1gFnSc2
nasz	nasz	k1gMnSc1
<g/>
”	”	k?
(	(	kIx(
<g/>
Tomasz	Tomasz	k1gMnSc1
Lubert	Lubert	k1gMnSc1
feat	feat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nika	nika	k1gFnSc1
<g/>
,	,	kIx,
Marcin	Marcin	k1gMnSc1
Spenner	Spenner	k1gMnSc1
<g/>
,	,	kIx,
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
miłości	miłośce	k1gFnSc3
do	do	k7c2
muzyki	muzyk	k1gFnSc2
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
</s>
<s>
„	„	k?
<g/>
Ten	ten	k3xDgMnSc1
zimowy	zimowa	k1gFnPc1
czas	czas	k1gInSc1
<g/>
”	”	k?
(	(	kIx(
<g/>
&	&	k?
Siemacha	Siemacha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Gwiazdy	Gwiazd	k1gInPc1
po	po	k7c4
kolędzie	kolędzie	k1gFnPc4
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Gdy	Gdy	k1gMnSc1
się	się	k?
Chrystus	Chrystus	k1gMnSc1
rodzi	rodh	k1gMnPc1
<g/>
”	”	k?
(	(	kIx(
<g/>
&	&	k?
Margaret	Margareta	k1gFnPc2
<g/>
,	,	kIx,
Rafał	Rafał	k1gFnSc2
Brzozowski	Brzozowsk	k1gFnSc2
<g/>
,	,	kIx,
Sarsa	Sarsa	k1gFnSc1
<g/>
,	,	kIx,
Kasia	Kasia	k1gFnSc1
Popowska	Popowska	k1gFnSc1
<g/>
,	,	kIx,
Pamela	Pamela	k1gFnSc1
Stone	ston	k1gInSc5
<g/>
,	,	kIx,
Siemacha	Siemacha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
nominace	nominace	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Polsat	Polsat	k5eAaPmF,k5eAaImF
SuperHit	superhit	k1gInSc4
Festiwal	Festiwal	k1gInSc1
2016	#num#	k4
</s>
<s>
Rádiový	rádiový	k2eAgInSc1d1
hit	hit	k1gInSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
festival	festival	k1gInSc1
polské	polský	k2eAgFnSc2d1
písně	píseň	k1gFnSc2
v	v	k7c6
Opole	Opola	k1gFnSc6
</s>
<s>
Debiuty	Debiut	k1gInPc1
</s>
<s>
nominace	nominace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eska	eska	k1gFnSc1
Music	Musice	k1gInPc2
Awards	Awards	k1gInSc1
2016	#num#	k4
</s>
<s>
Najlepší	Najlepší	k2eAgMnSc1d1
interpret	interpret	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Najlepší	Najlepší	k2eAgInSc1d1
hit	hit	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Najlepší	Najlepší	k2eAgInSc1d1
rádový	rádový	k2eAgInSc1d1
debut	debut	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Co	co	k8xS
zmienia	zmienium	k1gNnSc2
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
.	.	kIx.
muzyka	muzyek	k1gInSc2
<g/>
.	.	kIx.
<g/>
interia	interium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
III	III	kA
Karaoke	Karaok	k1gFnSc2
MAXXX	MAXXX	kA
Festiwal	Festiwal	k1gInSc4
2010	#num#	k4
.	.	kIx.
fm	fm	k?
<g/>
.	.	kIx.
<g/>
strefa	strefa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
rmf	rmf	k?
<g/>
.	.	kIx.
<g/>
fm	fm	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
<g />
.	.	kIx.
</s>
<s hack="1">
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
w	w	k?
końcu	końcu	k6eAd1
debiutuje	debiutovat	k5eAaPmIp3nS
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
.	.	kIx.
muzyka	muzyko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
interia	interium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Limit	limit	k1gInSc1
szans	szansa	k1gFnPc2
nowy	nowa	k1gFnSc2
singiel	singiel	k1gInSc1
.	.	kIx.
radio	radio	k1gNnSc1
<g/>
.	.	kIx.
<g/>
rzeszow	rzeszow	k?
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Antek	Antko	k1gNnPc2
Smykiewicz	Smykiewicz	k1gInSc1
–	–	k?
Biografia	Biografius	k1gMnSc2
.	.	kIx.
muzyka	muzyek	k1gMnSc2
<g/>
.	.	kIx.
<g/>
interia	interium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gInSc1
Smykiewicz	Smykiewicz	k1gInSc1
w	w	k?
kontynuacji	kontynuact	k5eAaPmIp1nS
„	„	k?
<g/>
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobacz	Zobacz	k1gInSc1
teledysk	teledysk	k1gInSc4
.	.	kIx.
popheart	popheart	k1gInSc4
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Debiutancki	Debiutancki	k1gNnSc1
singel	singel	k1gMnSc1
Antka	Antek	k1gMnSc2
Smykiewicza	Smykiewicz	k1gMnSc2
.	.	kIx.
universalmusic	universalmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Pomimo	Pomimo	k?
Burz	burza	k1gFnPc2
–	–	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
empik	empik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Antek	Antky	k1gFnPc2
Smykiewicz	Smykiewicza	k1gFnPc2
–	–	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
(	(	kIx(
<g/>
AirPlay	AirPlaa	k1gFnPc1
Top	topit	k5eAaImRp2nS
<g/>
:	:	kIx,
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.	.	kIx.
–	–	k?
13.11	13.11	k4
<g/>
.2015	.2015	k4
<g/>
)	)	kIx)
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
AirPlay	AirPlaum	k1gNnPc7
Top	topit	k5eAaImRp2nS
<g/>
:	:	kIx,
19.03	19.03	k4
<g/>
.	.	kIx.
–	–	k?
25.03	25.03	k4
<g/>
.2016	.2016	k4
<g/>
)	)	kIx)
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Jak	jak	k8xS,k8xC
sen	sen	k1gInSc1
(	(	kIx(
<g/>
AirPlay	AirPlaa	k1gFnPc1
Top	topit	k5eAaImRp2nS
<g/>
:	:	kIx,
0	#num#	k4
<g/>
9.07	9.07	k4
<g/>
.	.	kIx.
–	–	k?
15.07	15.07	k4
<g/>
.2016	.2016	k4
<g/>
)	)	kIx)
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Platynowe	Platynow	k1gMnSc2
płyty	płyta	k1gMnSc2
CD	CD	kA
(	(	kIx(
<g/>
przyznane	przyznanout	k5eAaPmIp3nS
w	w	k?
2016	#num#	k4
roku	rok	k1gInSc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.1	.1	k4
2	#num#	k4
Regulamin	Regulamin	k1gInSc1
przyznawania	przyznawanium	k1gNnSc2
wyróżnień	wyróżnień	k?
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Poznaj	Poznaj	k1gMnSc1
wszystkich	wszystkich	k1gMnSc1
wykonawców	wykonawców	k?
Sylwestra	Sylwestra	k1gFnSc1
TVN	TVN	kA
2015	#num#	k4
w	w	k?
Krakowie	Krakowie	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
.	.	kIx.
tvn	tvn	k?
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
"	"	kIx"
<g/>
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
<g/>
"	"	kIx"
<g/>
:	:	kIx,
Już	Już	k1gFnSc1
dziś	dziś	k?
premiera	premiera	k1gFnSc1
piosenki	piosenk	k1gFnSc2
w	w	k?
RMF	RMF	kA
FM	FM	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
.	.	kIx.
rmf	rmf	k?
<g/>
.	.	kIx.
<g/>
fm	fm	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Limit	limit	k1gInSc1
Szans	Szans	k1gInSc1
–	–	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
empik	empik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgMnSc3d1
<g />
.	.	kIx.
</s>
<s hack="1">
použití	použití	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nasz	Nasz	k1gInSc1
film	film	k1gInSc1
–	–	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
empik	empik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Posłuchaj	Posłuchaj	k1gInSc1
w	w	k?
całości	całośce	k1gFnSc4
<g/>
:	:	kIx,
Antek	Antek	k6eAd1
Smykiewicz	Smykiewicz	k1gInSc1
–	–	k?
Nasz	Nasz	k1gInSc1
film	film	k1gInSc1
.	.	kIx.
allaboutmusic	allaboutmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Koncert	koncert	k1gInSc1
„	„	k?
<g/>
Radiowy	Radiowa	k1gFnSc2
Przebój	Przebój	k1gMnSc1
Roku	rok	k1gInSc2
<g/>
”	”	k?
.	.	kIx.
polsatsuperhitfestiwal	polsatsuperhitfestiwal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
Festiwal	Festiwal	k1gMnSc1
w	w	k?
Opolu	Opola	k1gFnSc4
2016	#num#	k4
<g/>
:	:	kIx,
Debiuty	Debiut	k1gInPc1
–	–	k?
Kortez	Kortez	k1gInSc1
i	i	k8xC
Daria	Daria	k1gFnSc1
Zawiałow	Zawiałow	k1gFnSc2
triumfują	triumfują	k?
.	.	kIx.
muzyka	muzyek	k1gMnSc2
<g/>
.	.	kIx.
<g/>
interia	interium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
rateyourmusic	rateyourmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kto	Kto	k1gMnSc1
wygra	wygra	k1gMnSc1
"	"	kIx"
<g/>
The	The	k1gMnSc1
Voice	Voice	k1gMnSc1
of	of	k?
Poland	Poland	k1gInSc1
<g/>
"	"	kIx"
<g/>
?	?	kIx.
</s>
<s desamb="1">
.	.	kIx.
muzyka	muzyek	k1gInSc2
<g/>
.	.	kIx.
<g/>
interia	interium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
zaśpiewa	zaśpiewa	k1gMnSc1
na	na	k7c4
Wielkim	Wielkim	k1gInSc4
Balu	bal	k1gInSc2
Sportowca	Sportowc	k1gInSc2
.	.	kIx.
gloswielkopolski	gloswielkopolskit	k5eAaPmRp2nS,k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
(	(	kIx(
<g/>
AirPlay	AirPla	k2eAgFnPc1d1
Nowości	Nowośce	k1gFnSc3
<g/>
:	:	kIx,
0	#num#	k4
<g/>
5.09	5.09	k4
<g/>
.	.	kIx.
–	–	k?
11.09	11.09	k4
<g/>
.2015	.2015	k4
<g/>
)	)	kIx)
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Limit	limit	k1gInSc1
szans	szansa	k1gFnPc2
(	(	kIx(
<g/>
AirPlay	AirPla	k2eAgFnPc1d1
Nowości	Nowośce	k1gFnSc3
<g/>
:	:	kIx,
20.02	20.02	k4
<g/>
.	.	kIx.
–	–	k?
26.02	26.02	k4
<g/>
.2016	.2016	k4
<g/>
)	)	kIx)
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Jak	jak	k8xS,k8xC
sen	sen	k1gInSc1
(	(	kIx(
<g/>
AirPlay	AirPla	k2eAgFnPc1d1
Nowości	Nowośce	k1gFnSc3
<g/>
:	:	kIx,
0	#num#	k4
<g/>
9.07	9.07	k4
<g/>
.	.	kIx.
–	–	k?
15.07	15.07	k4
<g/>
.2016	.2016	k4
<g/>
)	)	kIx)
.	.	kIx.
bestsellery	bestseller	k1gInPc7
<g/>
.	.	kIx.
<g/>
zpav	zpavit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
–	–	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
rmf	rmf	k?
<g/>
.	.	kIx.
<g/>
fm	fm	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
2579	#num#	k4
<g/>
)	)	kIx)
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
2649	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Jak	jak	k8xS,k8xC
sen	sen	k1gInSc1
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
2733	#num#	k4
<g/>
)	)	kIx)
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
z	z	k7c2
10.10	10.10	k4
<g/>
.2015	.2015	k4
<g/>
)	)	kIx)
.	.	kIx.
radiozet	radiozet	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Pomimo	Pomimo	k?
burz	burza	k1gFnPc2
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
7231	#num#	k4
<g/>
)	)	kIx)
.	.	kIx.
rmfmaxxx	rmfmaxxx	k1gInSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SLiP	slip	k1gInSc1
–	–	k?
Statystyki	Statystyk	k1gFnSc2
dla	dla	k?
wykonawcy	wykonawca	k1gFnSc2
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
.	.	kIx.
radioszczecin	radioszczecin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
Antek	Antek	k1gMnSc1
Smykiewicz	Smykiewicz	k1gMnSc1
–	–	k?
Limit	limit	k1gInSc1
szans	szans	k1gInSc1
(	(	kIx(
<g/>
Notowanie	Notowanie	k1gFnSc1
1294	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
.	.	kIx.
radioszczecin	radioszczecin	k1gInSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lubert	Lubert	k1gInSc1
z	z	k7c2
miłości	miłośce	k1gMnPc1
do	do	k7c2
muzyki	muzyk	k1gFnSc2
–	–	k?
Lubert	Lubert	k1gMnSc1
.	.	kIx.
universalmusic	universalmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
8	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Gwiazdy	Gwiazda	k1gFnPc1
po	po	k7c4
kolędzie	kolędzie	k1gFnPc4
–	–	k?
Various	Various	k1gInSc1
Artists	Artists	k1gInSc1
.	.	kIx.
empik	empik	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Polsat	Polsat	k1gFnSc1
SuperHit	superhit	k1gInSc1
Festiwal	Festiwal	k1gInSc1
2016	#num#	k4
odbędzie	odbędzie	k1gFnSc2
się	się	k?
pod	pod	k7c7
koniec	koniec	k1gMnSc1
maja	maja	k1gMnSc1
.	.	kIx.
mediafm	mediafm	k1gMnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Eska	eska	k1gFnSc1
Music	Musice	k1gInPc2
Awards	Awards	k1gInSc1
2016	#num#	k4
–	–	k?
nominacje	nominacje	k1gFnSc1
<g/>
:	:	kIx,
Najlepszy	Najlepsz	k1gInPc1
artysta	artysta	k1gFnSc1
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Eska	eska	k1gFnSc1
Music	Musice	k1gInPc2
Awards	Awards	k1gInSc1
2016	#num#	k4
–	–	k?
nominacje	nominacj	k1gInSc2
<g/>
:	:	kIx,
Najlepszy	Najlepsza	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
hit	hit	k1gInSc1
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Eska	eska	k1gFnSc1
Music	Musice	k1gInPc2
Awards	Awards	k1gInSc1
2016	#num#	k4
–	–	k?
nominacje	nominacj	k1gInSc2
<g/>
:	:	kIx,
Najlepszy	Najlepsza	k1gFnSc2
radiowy	radiowa	k1gFnSc2
debiut	debiut	k1gInSc1
.	.	kIx.
eska	eska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl.	pl.	k?
[	[	kIx(
<g/>
dostęp	dostęp	k1gInSc1
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cytuj	Cytuj	k1gFnSc1
stronę	stronę	k?
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
53146936609613780188	#num#	k4
</s>
