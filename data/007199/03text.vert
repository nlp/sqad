<s>
Mravencovití	Mravencovitý	k2eAgMnPc1d1	Mravencovitý
(	(	kIx(	(
<g/>
Formicidae	Formicidae	k1gNnSc7	Formicidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
skupin	skupina	k1gFnPc2	skupina
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
v	v	k7c6	v
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
zájmu	zájem	k1gInSc6	zájem
vědců	vědec	k1gMnPc2	vědec
–	–	k?	–
myrmekologů	myrmekolog	k1gMnPc2	myrmekolog
<g/>
,	,	kIx,	,
ekologů	ekolog	k1gMnPc2	ekolog
<g/>
,	,	kIx,	,
biosociologů	biosociolog	k1gMnPc2	biosociolog
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
je	být	k5eAaImIp3nS	být
přikládána	přikládán	k2eAgFnSc1d1	přikládána
jejich	jejich	k3xOp3gInSc3	jejich
sociálnímu	sociální	k2eAgInSc3d1	sociální
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
specializaci	specializace	k1gFnSc4	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
jsou	být	k5eAaImIp3nP	být
sociální	sociální	k2eAgInSc4d1	sociální
hmyz	hmyz	k1gInSc4	hmyz
žijící	žijící	k2eAgInSc4d1	žijící
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
ve	v	k7c6	v
zbudovaných	zbudovaný	k2eAgNnPc6d1	zbudované
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obvykle	obvykle	k6eAd1	obvykle
říká	říkat	k5eAaImIp3nS	říkat
mraveniště	mraveniště	k1gNnSc4	mraveniště
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
mravenců	mravenec	k1gMnPc2	mravenec
může	moct	k5eAaImIp3nS	moct
zpočátku	zpočátku	k6eAd1	zpočátku
čítat	čítat	k5eAaImF	čítat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
také	také	k9	také
několika	několik	k4yIc2	několik
miliónů	milión	k4xCgInPc2	milión
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
kolonie	kolonie	k1gFnPc1	kolonie
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
superkolonie	superkolonie	k1gFnPc4	superkolonie
dosahující	dosahující	k2eAgFnSc2d1	dosahující
délky	délka	k1gFnSc2	délka
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
Linepithema	Linepithema	k1gFnSc1	Linepithema
humile	humile	k6eAd1	humile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
kolonie	kolonie	k1gFnPc4	kolonie
mravenců	mravenec	k1gMnPc2	mravenec
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
organizace	organizace	k1gFnSc2	organizace
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
jedince	jedinec	k1gMnSc2	jedinec
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
superorganismus	superorganismus	k1gInSc1	superorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
jsou	být	k5eAaImIp3nP	být
drobný	drobný	k2eAgInSc4d1	drobný
blanokřídlý	blanokřídlý	k2eAgInSc4d1	blanokřídlý
hmyz	hmyz	k1gInSc4	hmyz
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
od	od	k7c2	od
několika	několik	k4yIc2	několik
milimetrů	milimetr	k1gInPc2	milimetr
po	po	k7c4	po
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
tvoří	tvořit	k5eAaImIp3nS	tvořit
jasně	jasně	k6eAd1	jasně
oddělené	oddělený	k2eAgFnPc4d1	oddělená
části	část	k1gFnPc4	část
-	-	kIx~	-
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc4	hruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kryto	krýt	k5eAaImNgNnS	krýt
chloupky	chloupek	k1gInPc1	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
typicky	typicky	k6eAd1	typicky
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k1gInSc4	pár
článkovaných	článkovaný	k2eAgNnPc2d1	článkované
lomených	lomený	k2eAgNnPc2d1	lomené
paličkovitých	paličkovitý	k2eAgNnPc2d1	paličkovitý
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
facetované	facetovaný	k2eAgInPc1d1	facetovaný
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jen	jen	k6eAd1	jen
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
temenní	temenní	k2eAgNnPc1d1	temenní
očka	očko	k1gNnPc1	očko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
uzpůsobená	uzpůsobený	k2eAgNnPc4d1	uzpůsobené
a	a	k8xC	a
vyvinutá	vyvinutý	k2eAgNnPc4d1	vyvinuté
kusadla	kusadla	k1gNnPc4	kusadla
–	–	k?	–
kousací	kousací	k2eAgNnSc4d1	kousací
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
hruď	hruď	k1gFnSc1	hruď
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
ostny	osten	k1gInPc4	osten
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
pronotum	pronotum	k1gNnSc4	pronotum
<g/>
,	,	kIx,	,
mezonotum	mezonotum	k1gNnSc4	mezonotum
a	a	k8xC	a
epinotum	epinotum	k1gNnSc4	epinotum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
části	část	k1gFnSc2	část
vychází	vycházet	k5eAaImIp3nS	vycházet
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
páru	pár	k1gInSc6	pár
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
kyčle	kyčel	k1gFnSc2	kyčel
<g/>
,	,	kIx,	,
příkyčlí	příkyčlí	k1gNnSc2	příkyčlí
<g/>
,	,	kIx,	,
stehna	stehno	k1gNnSc2	stehno
<g/>
,	,	kIx,	,
holeně	holeň	k1gFnSc2	holeň
a	a	k8xC	a
chodidla	chodidlo	k1gNnSc2	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Zadeček	zadeček	k1gInSc1	zadeček
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
oddělen	oddělit	k5eAaPmNgInS	oddělit
tělní	tělní	k2eAgFnSc7d1	tělní
stopkou	stopka	k1gFnSc7	stopka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jednočlenná	jednočlenný	k2eAgFnSc1d1	jednočlenná
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
podčeledi	podčeleď	k1gFnSc2	podčeleď
Formicinae	Formicina	k1gFnSc2	Formicina
<g/>
,	,	kIx,	,
Ponerinae	Ponerina	k1gFnSc2	Ponerina
a	a	k8xC	a
Dolichoderinae	Dolichoderina	k1gFnSc2	Dolichoderina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dvoučlenná	dvoučlenný	k2eAgFnSc1d1	dvoučlenná
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Myrmicinae	Myrmicina	k1gFnSc2	Myrmicina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zadeček	zadeček	k1gInSc1	zadeček
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
blankou	blanka	k1gFnSc7	blanka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mravencům	mravenec	k1gMnPc3	mravenec
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
měnit	měnit	k5eAaImF	měnit
velikost	velikost	k1gFnSc4	velikost
zadečku	zadeček	k1gInSc2	zadeček
–	–	k?	–
Myrmecocystus	Myrmecocystus	k1gMnSc1	Myrmecocystus
minimus	minimus	k1gMnSc1	minimus
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgNnPc4d1	zadní
křídla	křídlo	k1gNnPc4	křídlo
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
menší	malý	k2eAgInPc1d2	menší
délky	délka	k1gFnPc4	délka
než	než	k8xS	než
přední	přední	k2eAgFnPc4d1	přední
<g/>
,	,	kIx,	,
po	po	k7c4	po
rojení	rojení	k1gNnSc4	rojení
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
královny	královna	k1gFnSc2	královna
ulomí	ulomit	k5eAaPmIp3nP	ulomit
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
odpadnou	odpadnout	k5eAaPmIp3nP	odpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
několik	několik	k4yIc4	několik
kast	kasta	k1gFnPc2	kasta
<g/>
,	,	kIx,	,
základními	základní	k2eAgFnPc7d1	základní
kastami	kasta	k1gFnPc7	kasta
jsou	být	k5eAaImIp3nP	být
královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
gyne	gyne	k1gInSc1	gyne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
(	(	kIx(	(
<g/>
aner	aner	k1gMnSc1	aner
<g/>
)	)	kIx)	)
a	a	k8xC	a
dělnice	dělnice	k1gFnPc1	dělnice
(	(	kIx(	(
<g/>
ergate	ergat	k1gInSc5	ergat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dělnice	dělnice	k1gFnPc1	dělnice
s	s	k7c7	s
neobvykle	obvykle	k6eNd1	obvykle
vyvinutými	vyvinutý	k2eAgFnPc7d1	vyvinutá
kusadly	kusadla	k1gNnPc7	kusadla
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgMnPc4d1	nazýván
vojáky	voják	k1gMnPc4	voják
(	(	kIx(	(
<g/>
dinergate	dinergat	k1gInSc5	dinergat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kasty	kasta	k1gFnPc1	kasta
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
tvarem	tvar	k1gInSc7	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dělnice	dělnice	k1gFnPc1	dělnice
rodu	rod	k1gInSc2	rod
Atta	Attum	k1gNnSc2	Attum
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
300	[number]	k4	300
<g/>
x	x	k?	x
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
jiná	jiný	k2eAgFnSc1d1	jiná
dělnice	dělnice	k1gFnSc1	dělnice
z	z	k7c2	z
téhož	týž	k3xTgNnSc2	týž
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kolonie	kolonie	k1gFnSc1	kolonie
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
jednu	jeden	k4xCgFnSc4	jeden
královnu	královna	k1gFnSc4	královna
a	a	k8xC	a
několik	několik	k4yIc4	několik
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
jsou	být	k5eAaImIp3nP	být
hmyz	hmyz	k1gInSc4	hmyz
s	s	k7c7	s
proměnou	proměna	k1gFnSc7	proměna
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
a	a	k8xC	a
z	z	k7c2	z
kukly	kukla	k1gFnSc2	kukla
se	se	k3xPyFc4	se
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
dospělý	dospělý	k1gMnSc1	dospělý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
živočišné	živočišný	k2eAgFnSc2d1	živočišná
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jim	on	k3xPp3gMnPc3	on
totiž	totiž	k9	totiž
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
teplé	teplý	k2eAgNnSc4d1	teplé
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc4d1	vlhké
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vlhkost	vlhkost	k1gFnSc4	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nežijí	žít	k5eNaImIp3nP	žít
ve	v	k7c6	v
vysloveně	vysloveně	k6eAd1	vysloveně
zamokřených	zamokřený	k2eAgNnPc6d1	zamokřené
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
nebo	nebo	k8xC	nebo
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Obtížně	obtížně	k6eAd1	obtížně
snášejí	snášet	k5eAaImIp3nP	snášet
sucho	sucho	k1gNnSc4	sucho
a	a	k8xC	a
chlad	chlad	k1gInSc4	chlad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
musejí	muset	k5eAaImIp3nP	muset
některé	některý	k3yIgFnPc1	některý
dělnice	dělnice	k1gFnPc1	dělnice
neustále	neustále	k6eAd1	neustále
donášet	donášet	k5eAaImF	donášet
do	do	k7c2	do
mraveniště	mraveniště	k1gNnSc2	mraveniště
kapičky	kapička	k1gFnSc2	kapička
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chladu	chlad	k1gInSc3	chlad
jsou	být	k5eAaImIp3nP	být
hnízda	hnízdo	k1gNnPc4	hnízdo
často	často	k6eAd1	často
zakládána	zakládat	k5eAaImNgFnS	zakládat
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
rychle	rychle	k6eAd1	rychle
ohřívají	ohřívat	k5eAaImIp3nP	ohřívat
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgNnPc1d1	klasické
kupovitá	kupovitý	k2eAgNnPc1d1	kupovité
mraveniště	mraveniště	k1gNnPc1	mraveniště
jsou	být	k5eAaImIp3nP	být
dokonalými	dokonalý	k2eAgFnPc7d1	dokonalá
stavbami	stavba	k1gFnPc7	stavba
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc4	jejich
jižní	jižní	k2eAgInSc1d1	jižní
svah	svah	k1gInSc1	svah
bývá	bývat	k5eAaImIp3nS	bývat
mnohem	mnohem	k6eAd1	mnohem
mírnější	mírný	k2eAgFnSc1d2	mírnější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hnízdo	hnízdo	k1gNnSc1	hnízdo
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
tepla	teplo	k1gNnSc2	teplo
pohlcovalo	pohlcovat	k5eAaImAgNnS	pohlcovat
a	a	k8xC	a
co	co	k9	co
nejméně	málo	k6eAd3	málo
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
mravenci	mravenec	k1gMnPc1	mravenec
budují	budovat	k5eAaImIp3nP	budovat
hnízda	hnízdo	k1gNnPc4	hnízdo
také	také	k6eAd1	také
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
listy	list	k1gInPc7	list
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
Oecophylla	Oecophylla	k1gFnSc1	Oecophylla
sp	sp	k?	sp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc4d1	známo
12	[number]	k4	12
520	[number]	k4	520
mravenčích	mravenčí	k2eAgInPc2d1	mravenčí
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
hmotnost	hmotnost	k1gFnSc1	hmotnost
všech	všecek	k3xTgMnPc2	všecek
mravenců	mravenec	k1gMnPc2	mravenec
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
přibližně	přibližně	k6eAd1	přibližně
rovná	rovnat	k5eAaImIp3nS	rovnat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgMnSc1d1	běžný
mravenec	mravenec	k1gMnSc1	mravenec
lesní	lesní	k2eAgMnSc1d1	lesní
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
věku	věk	k1gInSc2	věk
7	[number]	k4	7
až	až	k9	až
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
královna	královna	k1gFnSc1	královna
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
může	moct	k5eAaImIp3nS	moct
zplodit	zplodit	k5eAaPmF	zplodit
až	až	k9	až
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
kolonii	kolonie	k1gFnSc4	kolonie
zakládá	zakládat	k5eAaImIp3nS	zakládat
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hnízda	hnízdo	k1gNnSc2	hnízdo
provádějí	provádět	k5eAaImIp3nP	provádět
mravenci	mravenec	k1gMnPc1	mravenec
svatební	svatební	k2eAgInSc4d1	svatební
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
královny	královna	k1gFnPc1	královna
oplodněny	oplodněn	k2eAgMnPc4d1	oplodněn
samečky	sameček	k1gMnPc4	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
si	se	k3xPyFc3	se
uloží	uložit	k5eAaPmIp3nS	uložit
ejakulát	ejakulát	k1gInSc4	ejakulát
samečka	sameček	k1gMnSc2	sameček
do	do	k7c2	do
orgánu	orgán	k1gInSc2	orgán
na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
zadečku	zadeček	k1gInSc2	zadeček
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
spermatéky	spermatéky	k6eAd1	spermatéky
<g/>
,	,	kIx,	,
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
pak	pak	k6eAd1	pak
samečkovy	samečkův	k2eAgFnPc4d1	samečkův
spermie	spermie	k1gFnPc4	spermie
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mravenčí	mravenčí	k2eAgMnPc1d1	mravenčí
samečci	sameček	k1gMnPc1	sameček
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
postaví	postavit	k5eAaPmIp3nS	postavit
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
mraveniště	mraveniště	k1gNnSc1	mraveniště
a	a	k8xC	a
vychová	vychovat	k5eAaPmIp3nS	vychovat
první	první	k4xOgFnSc4	první
generaci	generace	k1gFnSc4	generace
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
o	o	k7c6	o
získávání	získávání	k1gNnSc6	získávání
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
kolonie	kolonie	k1gFnSc2	kolonie
dbají	dbát	k5eAaImIp3nP	dbát
dělnice	dělnice	k1gFnPc1	dělnice
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
jen	jen	k9	jen
o	o	k7c6	o
kladení	kladení	k1gNnSc6	kladení
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zakládání	zakládání	k1gNnSc1	zakládání
kolonie	kolonie	k1gFnSc2	kolonie
je	být	k5eAaImIp3nS	být
nejriskantnější	riskantní	k2eAgNnSc4d3	nejriskantnější
období	období	k1gNnSc4	období
v	v	k7c6	v
životě	život	k1gInSc6	život
mraveniště	mraveniště	k1gNnSc2	mraveniště
a	a	k8xC	a
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
královen	královna	k1gFnPc2	královna
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
stane	stanout	k5eAaPmIp3nS	stanout
obětí	oběť	k1gFnSc7	oběť
predátorů	predátor	k1gMnPc2	predátor
nebo	nebo	k8xC	nebo
uhyne	uhynout	k5eAaPmIp3nS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
pohlaví	pohlaví	k1gNnSc4	pohlaví
potomstva	potomstvo	k1gNnSc2	potomstvo
-	-	kIx~	-
z	z	k7c2	z
oplodněných	oplodněný	k2eAgNnPc2d1	oplodněné
vajíček	vajíčko	k1gNnPc2	vajíčko
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
dělnice	dělnice	k1gFnPc1	dělnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
neoplodněných	oplodněný	k2eNgInPc2d1	oplodněný
samečci	sameček	k1gMnPc1	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
geny	gen	k1gInPc1	gen
na	na	k7c4	na
potomstvo	potomstvo	k1gNnSc4	potomstvo
přenášejí	přenášet	k5eAaImIp3nP	přenášet
haplodiploidně	haplodiploidně	k6eAd1	haplodiploidně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dělnice	dělnice	k1gFnPc1	dělnice
nemají	mít	k5eNaImIp3nP	mít
navzájem	navzájem	k6eAd1	navzájem
společných	společný	k2eAgNnPc2d1	společné
pouze	pouze	k6eAd1	pouze
50	[number]	k4	50
%	%	kIx~	%
genů	gen	k1gInPc2	gen
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgFnSc3d1	běžná
např.	např.	kA	např.
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
75	[number]	k4	75
%	%	kIx~	%
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
zároveň	zároveň	k6eAd1	zároveň
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
sekret	sekret	k1gInSc1	sekret
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
larev	larva	k1gFnPc2	larva
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
nové	nový	k2eAgFnPc1d1	nová
královny	královna	k1gFnPc1	královna
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
zplodit	zplodit	k5eAaPmF	zplodit
nové	nový	k2eAgFnSc2d1	nová
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
přestane	přestat	k5eAaPmIp3nS	přestat
sekret	sekret	k1gInSc4	sekret
vylučovat	vylučovat	k5eAaImF	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dělnice	dělnice	k1gFnPc1	dělnice
schopny	schopen	k2eAgFnPc1d1	schopna
snášet	snášet	k5eAaImF	snášet
neoplodněná	oplodněný	k2eNgNnPc4d1	neoplodněné
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
<g/>
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
královna	královna	k1gFnSc1	královna
zemře	zemřít	k5eAaPmIp3nS	zemřít
a	a	k8xC	a
kolonie	kolonie	k1gFnSc1	kolonie
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
komunikačním	komunikační	k2eAgInSc7d1	komunikační
prvkem	prvek	k1gInSc7	prvek
jsou	být	k5eAaImIp3nP	být
mravenčí	mravenčí	k2eAgInPc1d1	mravenčí
pachy	pach	k1gInPc1	pach
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
feromony	feromon	k1gInPc4	feromon
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
někteří	některý	k3yIgMnPc1	některý
mravenci	mravenec	k1gMnPc1	mravenec
užívají	užívat	k5eAaImIp3nP	užívat
také	také	k9	také
chuť	chuť	k1gFnSc4	chuť
(	(	kIx(	(
<g/>
dávají	dávat	k5eAaImIp3nP	dávat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
jedincům	jedinec	k1gMnPc3	jedinec
ochutnat	ochutnat	k5eAaPmF	ochutnat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
našli	najít	k5eAaPmAgMnP	najít
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
cvrkot	cvrkot	k1gInSc1	cvrkot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stridulace	stridulace	k1gFnSc1	stridulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
vnímán	vnímat	k5eAaImNgInS	vnímat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
otřesů	otřes	k1gInPc2	otřes
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Zrak	zrak	k1gInSc1	zrak
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
mravenců	mravenec	k1gMnPc2	mravenec
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc4d1	malý
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejpropracovanějších	propracovaný	k2eAgFnPc6d3	nejpropracovanější
strukturách	struktura	k1gFnPc6	struktura
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
mravenci	mravenec	k1gMnPc1	mravenec
schopni	schopen	k2eAgMnPc1d1	schopen
sdělit	sdělit	k5eAaPmF	sdělit
přibližně	přibližně	k6eAd1	přibližně
10-20	[number]	k4	10-20
"	"	kIx"	"
<g/>
příkazů	příkaz	k1gInPc2	příkaz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
orientace	orientace	k1gFnSc2	orientace
mravenců	mravenec	k1gMnPc2	mravenec
podle	podle	k7c2	podle
pachů	pach	k1gInPc2	pach
je	být	k5eAaImIp3nS	být
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
systému	systém	k1gInSc2	systém
–	–	k?	–
mravencům	mravenec	k1gMnPc3	mravenec
stačí	stačit	k5eAaBmIp3nS	stačit
i	i	k9	i
malý	malý	k2eAgInSc1d1	malý
mozeček	mozeček	k1gInSc1	mozeček
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
obdivuhodné	obdivuhodný	k2eAgFnSc2d1	obdivuhodná
společenské	společenský	k2eAgFnSc2d1	společenská
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
slabinou	slabina	k1gFnSc7	slabina
této	tento	k3xDgFnSc2	tento
dovednosti	dovednost	k1gFnSc2	dovednost
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadná	snadný	k2eAgFnSc1d1	snadná
napadnutelnost	napadnutelnost	k1gFnSc1	napadnutelnost
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
mraveništi	mraveniště	k1gNnSc6	mraveniště
žije	žít	k5eAaImIp3nS	žít
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
napodobit	napodobit	k5eAaPmF	napodobit
mravenčí	mravenčí	k2eAgInSc4d1	mravenčí
pach	pach	k1gInSc4	pach
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jim	on	k3xPp3gMnPc3	on
jinak	jinak	k6eAd1	jinak
nejsou	být	k5eNaImIp3nP	být
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
vůbec	vůbec	k9	vůbec
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
častými	častý	k2eAgMnPc7d1	častý
parazity	parazit	k1gMnPc7	parazit
mravenců	mravenec	k1gMnPc2	mravenec
jsou	být	k5eAaImIp3nP	být
mravenci	mravenec	k1gMnPc1	mravenec
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Symbiotické	symbiotický	k2eAgInPc4d1	symbiotický
vztahy	vztah	k1gInPc4	vztah
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
parazitismu	parazitismus	k1gInSc2	parazitismus
žijí	žít	k5eAaImIp3nP	žít
mravenci	mravenec	k1gMnPc1	mravenec
i	i	k8xC	i
v	v	k7c6	v
pozitivní	pozitivní	k2eAgFnSc6d1	pozitivní
symbióze	symbióza	k1gFnSc6	symbióza
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
živočišných	živočišný	k2eAgInPc2d1	živočišný
i	i	k8xC	i
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obligátní	obligátní	k2eAgFnSc4d1	obligátní
symbiózu	symbióza	k1gFnSc4	symbióza
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
druhý	druhý	k4xOgMnSc1	druhý
účastník	účastník	k1gMnSc1	účastník
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
existenčně	existenčně	k6eAd1	existenčně
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
mšicemi	mšice	k1gFnPc7	mšice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mravenci	mravenec	k1gMnPc1	mravenec
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
nepřáteli	nepřítel	k1gMnPc7	nepřítel
nebo	nebo	k8xC	nebo
je	on	k3xPp3gNnPc4	on
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
vyhlédnutý	vyhlédnutý	k2eAgInSc4d1	vyhlédnutý
strom	strom	k1gInSc4	strom
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
jejich	jejich	k3xOp3gInPc1	jejich
výměšky	výměšek	k1gInPc1	výměšek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
i	i	k9	i
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
červci	červec	k1gMnPc7	červec
či	či	k8xC	či
larvami	larva	k1gFnPc7	larva
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
druh	druh	k1gInSc4	druh
symbiózy	symbióza	k1gFnSc2	symbióza
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mravenci	mravenec	k1gMnPc1	mravenec
rodu	rod	k1gInSc2	rod
Atta	Attum	k1gNnSc2	Attum
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
mraveništi	mraveniště	k1gNnSc6	mraveniště
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
"	"	kIx"	"
<g/>
krmí	krmě	k1gFnSc7	krmě
<g/>
"	"	kIx"	"
drobnými	drobný	k2eAgInPc7d1	drobný
lístečky	lísteček	k1gInPc7	lísteček
a	a	k8xC	a
houba	houba	k1gFnSc1	houba
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
královna	královna	k1gFnSc1	královna
si	se	k3xPyFc3	se
při	při	k7c6	při
svatebním	svatební	k2eAgInSc6d1	svatební
letu	let	k1gInSc6	let
odnáší	odnášet	k5eAaImIp3nS	odnášet
úlomek	úlomek	k1gInSc1	úlomek
této	tento	k3xDgFnSc2	tento
houby	houba	k1gFnSc2	houba
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
mohla	moct	k5eAaImAgFnS	moct
založit	založit	k5eAaPmF	založit
novou	nový	k2eAgFnSc4d1	nová
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
někteří	některý	k3yIgMnPc1	některý
mravenci	mravenec	k1gMnPc1	mravenec
chrání	chránit	k5eAaImIp3nP	chránit
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
dutině	dutina	k1gFnSc6	dutina
žijí	žít	k5eAaImIp3nP	žít
nebo	nebo	k8xC	nebo
jehož	jenž	k3xRgMnSc4	jenž
míza	míza	k1gFnSc1	míza
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
potrava	potrava	k1gFnSc1	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
proto	proto	k8xC	proto
vyhubí	vyhubit	k5eAaPmIp3nP	vyhubit
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
typem	typ	k1gInSc7	typ
symbiózy	symbióza	k1gFnSc2	symbióza
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
mravenci	mravenec	k1gMnPc1	mravenec
tkalci	tkadlec	k1gMnPc1	tkadlec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
larev	larva	k1gFnPc2	larva
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
jinak	jinak	k6eAd1	jinak
z	z	k7c2	z
vlákna	vlákno	k1gNnSc2	vlákno
spřádají	spřádat	k5eAaImIp3nP	spřádat
kokon	kokon	k1gInSc4	kokon
<g/>
)	)	kIx)	)
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
spletené	spletený	k2eAgNnSc1d1	spletené
hnízdo	hnízdo	k1gNnSc1	hnízdo
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
najde	najít	k5eAaPmIp3nS	najít
mravenec	mravenec	k1gMnSc1	mravenec
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
mraveniště	mraveniště	k1gNnSc2	mraveniště
<g/>
:	:	kIx,	:
První	první	k4xOgMnSc1	první
mravenec	mravenec	k1gMnSc1	mravenec
najde	najít	k5eAaPmIp3nS	najít
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
najde	najít	k5eAaPmIp3nS	najít
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
mraveniště	mraveniště	k1gNnSc2	mraveniště
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
a	a	k8xC	a
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
feromonovou	feromonový	k2eAgFnSc4d1	feromonová
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Mravenec	mravenec	k1gMnSc1	mravenec
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
vrací	vracet	k5eAaImIp3nS	vracet
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
orientuje	orientovat	k5eAaBmIp3nS	orientovat
podle	podle	k7c2	podle
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
následují	následovat	k5eAaImIp3nP	následovat
prvního	první	k4xOgMnSc4	první
a	a	k8xC	a
použijí	použít	k5eAaPmIp3nP	použít
4	[number]	k4	4
možné	možný	k2eAgFnPc1d1	možná
trasy	trasa	k1gFnPc1	trasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zesílená	zesílený	k2eAgFnSc1d1	zesílená
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
stopa	stopa	k1gFnSc1	stopa
dělá	dělat	k5eAaImIp3nS	dělat
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cestu	cesta	k1gFnSc4	cesta
nejvíce	nejvíce	k6eAd1	nejvíce
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
putují	putovat	k5eAaImIp3nP	putovat
po	po	k7c6	po
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
feromony	feromon	k1gInPc4	feromon
z	z	k7c2	z
delší	dlouhý	k2eAgFnSc2d2	delší
trasy	trasa	k1gFnSc2	trasa
se	se	k3xPyFc4	se
vypařují	vypařovat	k5eAaImIp3nP	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
jsou	být	k5eAaImIp3nP	být
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
nejagresivnějších	agresivní	k2eAgMnPc2d3	nejagresivnější
tvorů	tvor	k1gMnPc2	tvor
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
život	život	k1gInSc4	život
kolonie	kolonie	k1gFnSc2	kolonie
provázejí	provázet	k5eAaImIp3nP	provázet
neustálé	neustálý	k2eAgInPc4d1	neustálý
výpady	výpad	k1gInPc4	výpad
na	na	k7c4	na
cizí	cizí	k2eAgNnPc4d1	cizí
území	území	k1gNnPc4	území
a	a	k8xC	a
souboje	souboj	k1gInPc4	souboj
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
koloniemi	kolonie	k1gFnPc7	kolonie
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mravenců	mravenec	k1gMnPc2	mravenec
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
kastu	kasta	k1gFnSc4	kasta
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
zvláště	zvláště	k6eAd1	zvláště
velkými	velký	k2eAgFnPc7d1	velká
a	a	k8xC	a
silnými	silný	k2eAgFnPc7d1	silná
dělnicemi	dělnice	k1gFnPc7	dělnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
nevykonávají	vykonávat	k5eNaImIp3nP	vykonávat
žádnou	žádný	k3yNgFnSc4	žádný
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
jen	jen	k9	jen
čekají	čekat	k5eAaImIp3nP	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
jich	on	k3xPp3gInPc2	on
bude	být	k5eAaImBp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
boje	boj	k1gInSc2	boj
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
vytlačení	vytlačení	k1gNnSc4	vytlačení
poražené	poražený	k2eAgFnSc2d1	poražená
kolonie	kolonie	k1gFnSc2	kolonie
z	z	k7c2	z
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
ke	k	k7c3	k
skutečnému	skutečný	k2eAgNnSc3d1	skutečné
vyhlazení	vyhlazení	k1gNnSc3	vyhlazení
kolonie	kolonie	k1gFnSc2	kolonie
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
oddanosti	oddanost	k1gFnSc6	oddanost
jedinců	jedinec	k1gMnPc2	jedinec
celé	celý	k2eAgFnSc6d1	celá
kolonii	kolonie	k1gFnSc6	kolonie
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgFnPc1d1	stará
dělnice	dělnice	k1gFnPc1	dělnice
vždy	vždy	k6eAd1	vždy
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
jako	jako	k9	jako
první	první	k4xOgFnPc4	první
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
obětovaly	obětovat	k5eAaBmAgFnP	obětovat
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
zbraně	zbraň	k1gFnPc4	zbraň
mravenců	mravenec	k1gMnPc2	mravenec
patří	patřit	k5eAaImIp3nS	patřit
kusadla	kusadlo	k1gNnSc2	kusadlo
a	a	k8xC	a
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
nevítězí	vítězit	k5eNaImIp3nS	vítězit
vždy	vždy	k6eAd1	vždy
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
nebo	nebo	k8xC	nebo
nejjedovatější	jedovatý	k2eAgFnSc1d3	nejjedovatější
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolonie	kolonie	k1gFnSc1	kolonie
s	s	k7c7	s
nejdokonalejším	dokonalý	k2eAgInSc7d3	nejdokonalejší
systémem	systém	k1gInSc7	systém
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
strategické	strategický	k2eAgInPc4d1	strategický
triky	trik	k1gInPc4	trik
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
odříznutí	odříznutí	k1gNnSc1	odříznutí
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
napadání	napadání	k1gNnSc1	napadání
dělnic	dělnice	k1gFnPc2	dělnice
nosících	nosící	k2eAgFnPc2d1	nosící
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
znemožnění	znemožnění	k1gNnSc4	znemožnění
návratu	návrat	k1gInSc2	návrat
jedinců	jedinec	k1gMnPc2	jedinec
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
mraveniště	mraveniště	k1gNnSc2	mraveniště
<g/>
,	,	kIx,	,
prokopání	prokopání	k1gNnSc2	prokopání
chodby	chodba	k1gFnSc2	chodba
do	do	k7c2	do
cizí	cizí	k2eAgFnSc2d1	cizí
líhně	líheň	k1gFnSc2	líheň
<g/>
,	,	kIx,	,
ucpání	ucpání	k1gNnSc1	ucpání
východů	východ	k1gInPc2	východ
z	z	k7c2	z
mraveniště	mraveniště	k1gNnSc2	mraveniště
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
obývají	obývat	k5eAaImIp3nP	obývat
různá	různý	k2eAgNnPc4d1	různé
stanoviště	stanoviště	k1gNnPc4	stanoviště
a	a	k8xC	a
nevyhýbají	vyhýbat	k5eNaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
lidským	lidský	k2eAgNnPc3d1	lidské
obydlím	obydlí	k1gNnPc3	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
a	a	k8xC	a
nejobtížnějším	obtížný	k2eAgMnSc7d3	nejobtížnější
mravencem	mravenec	k1gMnSc7	mravenec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mravenec	mravenec	k1gMnSc1	mravenec
faraón	faraón	k1gMnSc1	faraón
(	(	kIx(	(
<g/>
Monomorium	Monomorium	k1gNnSc1	Monomorium
pharaonis	pharaonis	k1gFnPc2	pharaonis
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
obtížnost	obtížnost	k1gFnSc1	obtížnost
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
přenášení	přenášení	k1gNnSc6	přenášení
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
všudypřítomnosti	všudypřítomnost	k1gFnSc6	všudypřítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zahrádkáři	zahrádkář	k1gMnPc1	zahrádkář
nevidí	vidět	k5eNaImIp3nP	vidět
mravence	mravenec	k1gMnPc4	mravenec
příliš	příliš	k6eAd1	příliš
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mravenci	mravenec	k1gMnPc1	mravenec
jsou	být	k5eAaImIp3nP	být
ochránci	ochránce	k1gMnPc1	ochránce
mšic	mšice	k1gFnPc2	mšice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
schraňují	schraňovat	k5eAaImIp3nP	schraňovat
jak	jak	k6eAd1	jak
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
tak	tak	k9	tak
na	na	k7c6	na
ovocných	ovocný	k2eAgInPc6d1	ovocný
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
spatřit	spatřit	k5eAaPmF	spatřit
některé	některý	k3yIgFnPc4	některý
rostliny	rostlina	k1gFnPc4	rostlina
obestavěny	obestavět	k5eAaPmNgInP	obestavět
hliněnými	hliněný	k2eAgFnPc7d1	hliněná
stavbičkami	stavbička	k1gFnPc7	stavbička
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
mravenci	mravenec	k1gMnPc1	mravenec
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
své	svůj	k3xOyFgFnPc4	svůj
mšice	mšice	k1gFnPc4	mšice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
mravenci	mravenec	k1gMnPc7	mravenec
potírají	potírat	k5eAaImIp3nP	potírat
zahrádkáři	zahrádkář	k1gMnPc1	zahrádkář
kmeny	kmen	k1gInPc1	kmen
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
lepem	lep	k1gInSc7	lep
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
ochranné	ochranný	k2eAgInPc1d1	ochranný
lepové	lepový	k2eAgInPc1d1	lepový
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
mravenci	mravenec	k1gMnSc3	mravenec
člověku	člověk	k1gMnSc3	člověk
někdy	někdy	k6eAd1	někdy
nepříjemným	příjemný	k2eNgMnSc7d1	nepříjemný
společníkem	společník	k1gMnSc7	společník
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mravence	mravenec	k1gMnSc4	mravenec
chovají	chovat	k5eAaImIp3nP	chovat
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
nádobách	nádoba	k1gFnPc6	nádoba
–	–	k?	–
formikáriích	formikárium	k1gNnPc6	formikárium
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
někteří	některý	k3yIgMnPc1	některý
myrmekologové	myrmekolog	k1gMnPc1	myrmekolog
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
mravenců	mravenec	k1gMnPc2	mravenec
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
myrmekologie	myrmekologie	k1gFnSc1	myrmekologie
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
blanokřídlý	blanokřídlý	k2eAgInSc4d1	blanokřídlý
hmyz	hmyz	k1gInSc4	hmyz
jsou	být	k5eAaImIp3nP	být
myrmekologové	myrmekolog	k1gMnPc1	myrmekolog
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
popularizaci	popularizace	k1gFnSc6	popularizace
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
obzvlášť	obzvlášť	k6eAd1	obzvlášť
mravenců	mravenec	k1gMnPc2	mravenec
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekora	k1gFnSc1	Sekora
sérií	série	k1gFnPc2	série
dětských	dětský	k2eAgFnPc2d1	dětská
knížek	knížka	k1gFnPc2	knížka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
HÖLLDOBLER	HÖLLDOBLER	kA	HÖLLDOBLER
<g/>
,	,	kIx,	,
Bert	Berta	k1gFnPc2	Berta
<g/>
;	;	kIx,	;
WILSON	WILSON	kA	WILSON
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Osborne	Osborn	k1gInSc5	Osborn
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
mravencům	mravenec	k1gMnPc3	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
612	[number]	k4	612
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
SADIL	sadit	k5eAaPmAgMnS	sadit
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Naši	náš	k3xOp1gMnPc1	náš
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
224	[number]	k4	224
<g/>
.	.	kIx.	.
</s>
<s>
OBENBERGER	OBENBERGER	kA	OBENBERGER
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
života	život	k1gInSc2	život
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
221	[number]	k4	221
<g/>
.	.	kIx.	.
</s>
<s>
ZAHRADNÍK	Zahradník	k1gMnSc1	Zahradník
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Artia	Artia	k1gFnSc1	Artia
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
182	[number]	k4	182
<g/>
.	.	kIx.	.
</s>
<s>
SEIFERT	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Ameisen	Ameisen	k2eAgMnSc1d1	Ameisen
Mittel-	Mittel-	k1gMnSc1	Mittel-
und	und	k?	und
Nordeuropas	Nordeuropas	k1gMnSc1	Nordeuropas
<g/>
.	.	kIx.	.
</s>
<s>
Görlitz	Görlitz	k1gMnSc1	Görlitz
<g/>
/	/	kIx~	/
<g/>
Tauer	Tauer	k1gMnSc1	Tauer
:	:	kIx,	:
Lutra	Lutra	k1gMnSc1	Lutra
Verlags-	Verlags-	k1gMnSc1	Verlags-
und	und	k?	und
Vertriebsgesellschaft	Vertriebsgesellschaft	k1gMnSc1	Vertriebsgesellschaft
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
936412	[number]	k4	936412
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
I.	I.	kA	I.
<g/>
:	:	kIx,	:
Žahadloví	Žahadlový	k2eAgMnPc1d1	Žahadlový
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
524	[number]	k4	524
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Atlas	Atlas	k1gInSc1	Atlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1772	[number]	k4	1772
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Eusocialita	Eusocialita	k1gFnSc1	Eusocialita
Termiti	termit	k1gMnPc1	termit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mravencovití	mravencovitý	k2eAgMnPc1d1	mravencovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gNnSc1	téma
mravenec	mravenec	k1gMnSc1	mravenec
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
mravenec	mravenec	k1gMnSc1	mravenec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Formicidae	Formicidae	k1gInSc1	Formicidae
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Mravenci	mravenec	k1gMnPc1	mravenec
ON-LINE	ON-LINE	k1gFnSc2	ON-LINE
Myrmekologie	myrmekologie	k1gFnSc2	myrmekologie
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svépomocí	svépomoc	k1gFnSc7	svépomoc
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
mravence	mravenec	k1gMnPc4	mravenec
<g/>
.	.	kIx.	.
</s>
