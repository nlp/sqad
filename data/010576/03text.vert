<p>
<s>
Eldrick	Eldrick	k1gMnSc1	Eldrick
Tont	Tont	k1gMnSc1	Tont
"	"	kIx"	"
<g/>
Tiger	Tiger	k1gMnSc1	Tiger
<g/>
"	"	kIx"	"
Woods	Woods	k1gInSc1	Woods
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Cypress	Cypress	k1gInSc1	Cypress
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
profesionální	profesionální	k2eAgMnSc1d1	profesionální
golfista	golfista	k1gMnSc1	golfista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgMnPc4d3	nejúspěšnější
golfisty	golfista	k1gMnPc4	golfista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgMnPc2d1	placený
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
nejlépe	dobře	k6eAd3	dobře
placeným	placený	k2eAgMnSc7d1	placený
sportovcem	sportovec	k1gMnSc7	sportovec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
vydělal	vydělat	k5eAaPmAgInS	vydělat
122	[number]	k4	122
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Golf	golf	k1gInSc4	golf
Digest	Digest	k1gFnSc4	Digest
jeho	jeho	k3xOp3gInPc2	jeho
finanční	finanční	k2eAgInPc4d1	finanční
příjmy	příjem	k1gInPc7	příjem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
až	až	k9	až
2007	[number]	k4	2007
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
výše	vysoce	k6eAd2	vysoce
769	[number]	k4	769
440	[number]	k4	440
709	[number]	k4	709
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
časopis	časopis	k1gInSc1	časopis
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
prvním	první	k4xOgMnSc7	první
sportovcem	sportovec	k1gMnSc7	sportovec
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
překročit	překročit	k5eAaPmF	překročit
výdělek	výdělek	k1gInSc1	výdělek
jedné	jeden	k4xCgFnSc2	jeden
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Woods	Woods	k6eAd1	Woods
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesionálem	profesionál	k1gMnSc7	profesionál
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
svůj	svůj	k3xOyFgMnSc1	svůj
první	první	k4xOgMnSc1	první
major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
turnaj	turnaj	k1gInSc1	turnaj
Masters	Masters	k1gInSc1	Masters
s	s	k7c7	s
rekordním	rekordní	k2eAgInSc7d1	rekordní
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
stal	stát	k5eAaPmAgMnS	stát
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
golfu	golf	k1gInSc6	golf
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1997	[number]	k4	1997
až	až	k9	až
2010	[number]	k4	2010
strávil	strávit	k5eAaPmAgInS	strávit
Woods	Woods	k1gInSc1	Woods
na	na	k7c6	na
postu	post	k1gInSc6	post
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
623	[number]	k4	623
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Woods	Woods	k1gInSc1	Woods
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
patnáct	patnáct	k4xCc4	patnáct
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
majorů	major	k1gMnPc2	major
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc4	druhý
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
golfu	golf	k1gInSc6	golf
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
víc	hodně	k6eAd2	hodně
vyhraných	vyhraný	k2eAgMnPc2d1	vyhraný
majorů	major	k1gMnPc2	major
a	a	k8xC	a
turnajů	turnaj	k1gInPc2	turnaj
na	na	k7c4	na
PGA	PGA	kA	PGA
Tour	Tour	k1gInSc4	Tour
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
jiný	jiný	k2eAgMnSc1d1	jiný
aktivní	aktivní	k2eAgMnSc1d1	aktivní
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
golfista	golfista	k1gMnSc1	golfista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
kariérového	kariérový	k2eAgInSc2d1	kariérový
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
(	(	kIx(	(
<g/>
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
čtyři	čtyři	k4xCgInPc4	čtyři
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
turnaje	turnaj	k1gInPc4	turnaj
(	(	kIx(	(
<g/>
majory	major	k1gMnPc7	major
<g/>
)	)	kIx)	)
-	-	kIx~	-
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
British	British	k1gMnSc1	British
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
USPGA	USPGA	kA	USPGA
Championship	Championship	k1gInSc1	Championship
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
golfista	golfista	k1gMnSc1	golfista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nejrychleji	rychle	k6eAd3	rychle
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
50	[number]	k4	50
turnajů	turnaj	k1gInPc2	turnaj
na	na	k7c4	na
tour	tour	k1gInSc4	tour
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
roku	rok	k1gInSc2	rok
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
golfistů	golfista	k1gMnPc2	golfista
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
osmkrát	osmkrát	k6eAd1	osmkrát
získal	získat	k5eAaPmAgInS	získat
Cenu	cena	k1gFnSc4	cena
Byrona	Byron	k1gMnSc2	Byron
Nelsona	Nelson	k1gMnSc2	Nelson
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
nerozhodně	rozhodně	k6eNd1	rozhodně
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Nicklausem	Nicklaus	k1gMnSc7	Nicklaus
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgMnPc2d1	placený
golfistů	golfista	k1gMnPc2	golfista
během	během	k7c2	během
osmi	osm	k4xCc2	osm
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
Associated	Associated	k1gInSc4	Associated
Press	Press	k1gInSc1	Press
a	a	k8xC	a
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
byl	být	k5eAaImAgMnS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jednou	jednou	k6eAd1	jednou
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
časopisem	časopis	k1gInSc7	časopis
Sports	Sportsa	k1gFnPc2	Sportsa
Illustrated	Illustrated	k1gMnSc1	Illustrated
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
se	se	k3xPyFc4	se
golf	golf	k1gInSc1	golf
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
sportů	sport	k1gInPc2	sport
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dramaticky	dramaticky	k6eAd1	dramaticky
se	se	k3xPyFc4	se
zvedla	zvednout	k5eAaPmAgFnS	zvednout
sledovanost	sledovanost	k1gFnSc1	sledovanost
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
považována	považován	k2eAgFnSc1d1	považována
jen	jen	k9	jen
za	za	k7c4	za
hru	hra	k1gFnSc4	hra
"	"	kIx"	"
<g/>
vyvolené	vyvolený	k2eAgFnPc4d1	vyvolená
<g/>
"	"	kIx"	"
elity	elita	k1gFnPc4	elita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
si	se	k3xPyFc3	se
odpočinul	odpočinout	k5eAaPmAgMnS	odpočinout
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
levého	levý	k2eAgNnSc2d1	levé
kolena	koleno	k1gNnSc2	koleno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
kvůli	kvůli	k7c3	kvůli
nevěře	nevěra	k1gFnSc3	nevěra
oznámil	oznámit	k5eAaPmAgMnS	oznámit
přerušení	přerušení	k1gNnPc4	přerušení
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
více	hodně	k6eAd2	hodně
věnovat	věnovat	k5eAaPmF	věnovat
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
zachránit	zachránit	k5eAaPmF	zachránit
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
ani	ani	k8xC	ani
jediného	jediný	k2eAgInSc2d1	jediný
turnaje	turnaj	k1gInSc2	turnaj
PGA	PGA	kA	PGA
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
odehrál	odehrát	k5eAaPmAgInS	odehrát
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
oficiální	oficiální	k2eAgInSc1d1	oficiální
turnaj	turnaj	k1gInSc1	turnaj
PGA	PGA	kA	PGA
Tour	Tour	k1gInSc1	Tour
<g/>
,	,	kIx,	,
Farmers	Farmers	k1gInSc1	Farmers
Insurance	Insurance	k1gFnSc2	Insurance
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neprošel	projít	k5eNaPmAgInS	projít
cutem	cutem	k1gInSc1	cutem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2017	[number]	k4	2017
se	se	k3xPyFc4	se
po	po	k7c6	po
10	[number]	k4	10
měsících	měsíc	k1gInPc6	měsíc
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
soutěžnímu	soutěžní	k2eAgInSc3d1	soutěžní
golfu	golf	k1gInSc3	golf
<g/>
,	,	kIx,	,
když	když	k8xS	když
odehrál	odehrát	k5eAaPmAgInS	odehrát
turnaj	turnaj	k1gInSc1	turnaj
Hero	Hero	k6eAd1	Hero
World	World	k1gInSc4	World
Challenge	Challeng	k1gFnSc2	Challeng
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
oznámil	oznámit	k5eAaPmAgMnS	oznámit
plán	plán	k1gInSc4	plán
odehrát	odehrát	k5eAaPmF	odehrát
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
umístění	umístění	k1gNnSc4	umístění
na	na	k7c4	na
major	major	k1gMnSc1	major
turnaji	turnaj	k1gInSc6	turnaj
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
posledního	poslední	k2eAgNnSc2d1	poslední
vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
když	když	k8xS	když
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgInSc1	druhý
na	na	k7c6	na
PGA	PGA	kA	PGA
Championship	Championship	k1gInSc1	Championship
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
dokázal	dokázat	k5eAaPmAgInS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
PGA	PGA	kA	PGA
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
Tour	Tour	k1gMnSc1	Tour
Championship	Championship	k1gMnSc1	Championship
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
vítězství	vítězství	k1gNnSc4	vítězství
ale	ale	k8xC	ale
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
na	na	k7c4	na
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
FedexCup	FedexCup	k1gInSc4	FedexCup
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
díky	díky	k7c3	díky
dělenému	dělený	k2eAgNnSc3d1	dělené
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
místu	místo	k1gNnSc3	místo
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
Championship	Championship	k1gMnSc1	Championship
udržel	udržet	k5eAaPmAgMnS	udržet
Justin	Justin	k1gMnSc1	Justin
Rose	Rose	k1gMnSc1	Rose
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
duelu	duel	k1gInSc2	duel
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Match	Matcha	k1gFnPc2	Matcha
<g/>
"	"	kIx"	"
o	o	k7c4	o
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
proti	proti	k7c3	proti
Philu	Phil	k1gMnSc3	Phil
Mickelsonovi	Mickelson	k1gMnSc3	Mickelson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
prohrál	prohrát	k5eAaPmAgMnS	prohrát
až	až	k9	až
v	v	k7c6	v
play	play	k0	play
off	off	k?	off
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
jamce	jamka	k1gFnSc6	jamka
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
dokázal	dokázat	k5eAaPmAgMnS	dokázat
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
zvítězit	zvítězit	k5eAaPmF	zvítězit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
na	na	k7c4	na
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
a	a	k8xC	a
patnáctý	patnáctý	k4xOgMnSc1	patnáctý
major	major	k1gMnSc1	major
titul	titul	k1gInSc4	titul
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
baseball	baseball	k1gInSc4	baseball
<g/>
.	.	kIx.	.
</s>
<s>
Zvolil	zvolit	k5eAaPmAgMnS	zvolit
však	však	k9	však
golf	golf	k1gInSc4	golf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
učí	učit	k5eAaImIp3nP	učit
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
pouze	pouze	k6eAd1	pouze
základům	základ	k1gInPc3	základ
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgMnS	zahrát
první	první	k4xOgFnSc4	první
devítku	devítka	k1gFnSc4	devítka
na	na	k7c4	na
pouhých	pouhý	k2eAgFnPc2d1	pouhá
42	[number]	k4	42
ran	rána	k1gFnPc2	rána
na	na	k7c4	na
paru	para	k1gFnSc4	para
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Vyniká	vynikat	k5eAaImIp3nS	vynikat
zejména	zejména	k9	zejména
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
švédskou	švédský	k2eAgFnSc7d1	švédská
modelkou	modelka	k1gFnSc7	modelka
Elin	Elina	k1gFnPc2	Elina
Nordegren	Nordegrna	k1gFnPc2	Nordegrna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
vzešly	vzejít	k5eAaPmAgFnP	vzejít
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zveřejní	zveřejnit	k5eAaPmIp3nP	zveřejnit
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
udržuje	udržovat	k5eAaImIp3nS	udržovat
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
lyžařkou	lyžařka	k1gFnSc7	lyžařka
Lindsey	Lindsea	k1gFnSc2	Lindsea
Vonnovou	Vonnová	k1gFnSc7	Vonnová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tiger	Tigra	k1gFnPc2	Tigra
Woods	Woodsa	k1gFnPc2	Woodsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Tiger	Tigra	k1gFnPc2	Tigra
Woods	Woodsa	k1gFnPc2	Woodsa
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
