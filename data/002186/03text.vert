<s>
Cheyenne	Cheyennout	k5eAaImIp3nS	Cheyennout
je	on	k3xPp3gNnSc4	on
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Wyoming	Wyoming	k1gInSc4	Wyoming
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
okresu	okres	k1gInSc2	okres
Laramie	Laramie	k1gFnSc2	Laramie
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Založeno	založen	k2eAgNnSc1d1	založeno
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
57,9	[number]	k4	57,9
km2	km2	k4	km2
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
59	[number]	k4	59
456	[number]	k4	456
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
969,6	[number]	k4	969,6
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Rensis	Rensis	k1gInSc1	Rensis
Likert	Likert	k1gInSc1	Likert
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
-	-	kIx~	-
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Lompoc	Lompoc	k1gInSc1	Lompoc
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Lurdy	Lurda	k1gFnPc1	Lurda
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Tchaj-čung	Tchaj-čung	k1gInSc1	Tchaj-čung
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
Waimea	Waimea	k1gFnSc1	Waimea
<g/>
,	,	kIx,	,
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
