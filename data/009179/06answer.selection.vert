<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
Fortuna	Fortuna	k1gFnSc1	Fortuna
bohyní	bohyně	k1gFnPc2	bohyně
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
personifikací	personifikace	k1gFnSc7	personifikace
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnSc1	naděje
v	v	k7c4	v
dobré	dobrý	k2eAgNnSc4d1	dobré
pořízení	pořízení	k1gNnSc4	pořízení
a	a	k8xC	a
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
nevypočitatelnost	nevypočitatelnost	k1gFnSc4	nevypočitatelnost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
