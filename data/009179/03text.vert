<p>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
Fortuna	Fortuna	k1gFnSc1	Fortuna
bohyní	bohyně	k1gFnPc2	bohyně
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
personifikací	personifikace	k1gFnSc7	personifikace
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnSc1	naděje
v	v	k7c4	v
dobré	dobrý	k2eAgNnSc4d1	dobré
pořízení	pořízení	k1gNnSc4	pořízení
a	a	k8xC	a
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
nevypočitatelnost	nevypočitatelnost	k1gFnSc4	nevypočitatelnost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
zahalená	zahalený	k2eAgFnSc1d1	zahalená
a	a	k8xC	a
slepá	slepý	k2eAgFnSc1d1	slepá
jako	jako	k8xS	jako
moderní	moderní	k2eAgNnSc1d1	moderní
pojetí	pojetí	k1gNnSc1	pojetí
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
bohyně	bohyně	k1gFnSc1	bohyně
Tyché	Tychý	k2eAgFnSc2d1	Tychý
<g/>
,	,	kIx,	,
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Spása	spása	k1gFnSc1	spása
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
řeckému	řecký	k2eAgNnSc3d1	řecké
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
žádného	žádný	k3yNgMnSc4	žádný
milence	milenec	k1gMnSc4	milenec
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
blahoslavené	blahoslavený	k2eAgFnSc6d1	blahoslavená
družině	družina	k1gFnSc6	družina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Copia	Copia	k1gFnSc1	Copia
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
hojnost	hojnost	k1gFnSc1	hojnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Annonaria	Annonarium	k1gNnSc2	Annonarium
chránila	chránit	k5eAaImAgFnS	chránit
dodávky	dodávka	k1gFnPc4	dodávka
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fortunu	Fortuna	k1gFnSc4	Fortuna
si	se	k3xPyFc3	se
například	například	k6eAd1	například
nakláněly	naklánět	k5eAaImAgFnP	naklánět
a	a	k8xC	a
uctívaly	uctívat	k5eAaImAgFnP	uctívat
matky	matka	k1gFnPc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
kult	kult	k1gInSc1	kult
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xC	jako
Fortuna	Fortuna	k1gFnSc1	Fortuna
Populi	Popule	k1gFnSc4	Popule
Romani	Romaň	k1gFnSc3	Romaň
<g/>
,	,	kIx,	,
Fortuna	Fortuna	k1gFnSc1	Fortuna
lidu	lid	k1gInSc2	lid
římského	římský	k2eAgMnSc2d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vládl	vládnout	k5eAaImAgMnS	vládnout
král	král	k1gMnSc1	král
Servius	Servius	k1gMnSc1	Servius
Tullius	Tullius	k1gMnSc1	Tullius
<g/>
.	.	kIx.	.
</s>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
má	mít	k5eAaImIp3nS	mít
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
svůj	svůj	k3xOyFgInSc4	svůj
chrám	chrám	k1gInSc4	chrám
na	na	k7c6	na
Foru	forum	k1gNnSc6	forum
Boariu	Boarium	k1gNnSc6	Boarium
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
svatyni	svatyně	k1gFnSc4	svatyně
na	na	k7c6	na
prastarém	prastarý	k2eAgInSc6d1	prastarý
Quirinalu	Quirinal	k1gInSc6	Quirinal
<g/>
,	,	kIx,	,
uctívaném	uctívaný	k2eAgInSc6d1	uctívaný
jako	jako	k8xC	jako
opatrovnický	opatrovnický	k2eAgMnSc1d1	opatrovnický
duch	duch	k1gMnSc1	duch
strážný	strážný	k1gMnSc1	strážný
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
chrám	chrám	k1gInSc1	chrám
Fortuna	Fortuna	k1gFnSc1	Fortuna
Muliebris	Muliebris	k1gFnSc1	Muliebris
<g/>
,	,	kIx,	,
Fortuna	Fortuna	k1gFnSc1	Fortuna
ženská	ženská	k1gFnSc1	ženská
ve	v	k7c6	v
věštírně	věštírna	k1gFnSc6	věštírna
v	v	k7c6	v
Praeneste	Praenest	k1gInSc5	Praenest
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
určoval	určovat	k5eAaImAgMnS	určovat
budoucnost	budoucnost	k1gFnSc4	budoucnost
z	z	k7c2	z
dubových	dubový	k2eAgInPc2d1	dubový
výhonků	výhonek	k1gInPc2	výhonek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
římský	římský	k2eAgInSc1d1	římský
svět	svět	k1gInSc1	svět
uctíval	uctívat	k5eAaImAgInS	uctívat
Fortunu	Fortuna	k1gFnSc4	Fortuna
v	v	k7c6	v
obrovském	obrovský	k2eAgNnSc6d1	obrovské
množství	množství	k1gNnSc6	množství
svatyní	svatyně	k1gFnPc2	svatyně
a	a	k8xC	a
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
záležely	záležet	k5eAaImAgFnP	záležet
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
životních	životní	k2eAgFnPc6d1	životní
okolnostech	okolnost	k1gFnPc6	okolnost
a	a	k8xC	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c4	v
co	co	k3yRnSc4	co
lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ten	ten	k3xDgInSc4	ten
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
nebyla	být	k5eNaImAgFnS	být
jen	jen	k6eAd1	jen
kladnou	kladný	k2eAgFnSc7d1	kladná
a	a	k8xC	a
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
Fortuna	Fortuna	k1gFnSc1	Fortuna
Dubia	Dubium	k1gNnSc2	Dubium
byla	být	k5eAaImAgFnS	být
nerozhodná	rozhodný	k2eNgFnSc1d1	nerozhodná
<g/>
,	,	kIx,	,
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
a	a	k8xC	a
nespolehlivá	spolehlivý	k2eNgFnSc1d1	nespolehlivá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Fortuna	Fortuna	k1gFnSc1	Fortuna
Brevis	Brevis	k1gFnSc1	Brevis
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
nevypočitatelná	vypočitatelný	k2eNgFnSc1d1	nevypočitatelná
<g/>
,	,	kIx,	,
přelétavá	přelétavý	k2eAgFnSc1d1	přelétavá
<g/>
,	,	kIx,	,
vrtošivá	vrtošivý	k2eAgFnSc1d1	vrtošivá
a	a	k8xC	a
nestálá	stálý	k2eNgFnSc1d1	nestálá
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
Fortuna	Fortuna	k1gFnSc1	Fortuna
Mala	Mala	k1gFnSc1	Mala
mohla	moct	k5eAaImAgFnS	moct
dokonce	dokonce	k9	dokonce
škodit	škodit	k5eAaImF	škodit
a	a	k8xC	a
přivádět	přivádět	k5eAaImF	přivádět
lidi	člověk	k1gMnPc4	člověk
do	do	k7c2	do
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
zřejmě	zřejmě	k6eAd1	zřejmě
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
Vertumna	Vertumn	k1gInSc2	Vertumn
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
otáčí	otáčet	k5eAaImIp3nS	otáčet
rokem	rok	k1gInSc7	rok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
ještě	ještě	k9	ještě
dřívější	dřívější	k2eAgInPc4d1	dřívější
odkazy	odkaz	k1gInPc4	odkaz
v	v	k7c6	v
Ciceronově	Ciceronův	k2eAgInSc6d1	Ciceronův
díle	díl	k1gInSc6	díl
In	In	k1gMnPc2	In
Pisonem	Pison	k1gInSc7	Pison
<g/>
,	,	kIx,	,
cca	cca	kA	cca
55	[number]	k4	55
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c4	na
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
kolo	kolo	k1gNnSc4	kolo
osudu	osud	k1gInSc2	osud
(	(	kIx(	(
<g/>
Rota	rota	k1gFnSc1	rota
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
)	)	kIx)	)
symbolizující	symbolizující	k2eAgFnPc1d1	symbolizující
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
životě	život	k1gInSc6	život
od	od	k7c2	od
prosperity	prosperita	k1gFnSc2	prosperita
až	až	k9	až
po	po	k7c4	po
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fortuna	Fortuna	k1gFnSc1	Fortuna
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
==	==	k?	==
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
nezmizela	zmizet	k5eNaPmAgFnS	zmizet
z	z	k7c2	z
povědomí	povědomí	k1gNnPc2	povědomí
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
oslavou	oslava	k1gFnSc7	oslava
křesťanství	křesťanství	k1gNnSc2	křesťanství
ani	ani	k8xC	ani
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kresba	kresba	k1gFnSc1	kresba
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Boëthius	Boëthius	k1gMnSc1	Boëthius
sepsal	sepsat	k5eAaPmAgMnS	sepsat
spis	spis	k1gInSc4	spis
Útěcha	útěcha	k1gFnSc1	útěcha
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
mj.	mj.	kA	mj.
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
osud	osud	k1gInSc1	osud
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Štěstěny	Štěstěna	k1gFnSc2	Štěstěna
či	či	k8xC	či
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
náhodné	náhodný	k2eAgFnPc1d1	náhodná
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
božího	boží	k2eAgInSc2d1	boží
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
člověk	člověk	k1gMnSc1	člověk
nevidí	vidět	k5eNaImIp3nS	vidět
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
kterému	který	k3yQgNnSc3	který
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
okolnosti	okolnost	k1gFnPc4	okolnost
člověka	člověk	k1gMnSc2	člověk
mohou	moct	k5eAaImIp3nP	moct
přivést	přivést	k5eAaPmF	přivést
k	k	k7c3	k
nejvyššímu	vysoký	k2eAgNnSc3d3	nejvyšší
dobru	dobro	k1gNnSc3	dobro
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Boëthiovo	Boëthiův	k2eAgNnSc1d1	Boëthiův
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všudepřítomné	Všudepřítomný	k2eAgNnSc1d1	Všudepřítomný
zobrazení	zobrazení	k1gNnSc1	zobrazení
kola	kolo	k1gNnSc2	kolo
osudu	osud	k1gInSc2	osud
(	(	kIx(	(
<g/>
Rota	rota	k1gFnSc1	rota
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
přímým	přímý	k2eAgInSc7d1	přímý
odkazem	odkaz	k1gInSc7	odkaz
druhé	druhý	k4xOgFnSc2	druhý
Boethiovy	Boethiův	k2eAgFnSc2d1	Boethiův
knihy	kniha	k1gFnSc2	kniha
Consolation	Consolation	k1gInSc1	Consolation
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
se	se	k3xPyFc4	se
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
od	od	k7c2	od
malinkých	malinký	k2eAgFnPc2d1	malinká
miniatur	miniatura	k1gFnPc2	miniatura
v	v	k7c6	v
iluminacích	iluminace	k1gFnPc6	iluminace
a	a	k8xC	a
rukopisech	rukopis	k1gInPc6	rukopis
až	až	k8xS	až
po	po	k7c6	po
obrovská	obrovský	k2eAgNnPc1d1	obrovské
barevná	barevný	k2eAgNnPc1d1	barevné
skleněná	skleněný	k2eAgNnPc1d1	skleněné
okna	okno	k1gNnPc1	okno
v	v	k7c6	v
katedrálách	katedrála	k1gFnPc6	katedrála
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
například	například	k6eAd1	například
v	v	k7c4	v
Amiens	Amiens	k1gInSc4	Amiens
<g/>
.	.	kIx.	.
</s>
<s>
Dáma	dáma	k1gFnSc1	dáma
Fortuna	Fortuna	k1gFnSc1	Fortuna
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
představována	představovat	k5eAaImNgFnS	představovat
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
její	její	k3xOp3gFnSc1	její
důležitost	důležitost	k1gFnSc1	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
osudu	osud	k1gInSc2	osud
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
čtyři	čtyři	k4xCgInPc4	čtyři
stupně	stupeň	k1gInPc4	stupeň
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
znázorněnými	znázorněný	k2eAgFnPc7d1	znázorněná
lidskými	lidský	k2eAgFnPc7d1	lidská
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
napsáno	napsat	k5eAaBmNgNnS	napsat
regnabo	regnaba	k1gFnSc5	regnaba
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgInS	mít
bych	by	kYmCp1nS	by
vládnout	vládnout	k5eAaImF	vládnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
regno	regno	k6eAd1	regno
(	(	kIx(	(
<g/>
vládnu	vládnout	k5eAaImIp1nS	vládnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
postava	postava	k1gFnSc1	postava
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
sestupně	sestupně	k6eAd1	sestupně
vpravo	vpravo	k6eAd1	vpravo
regnavi	regnaev	k1gFnSc3	regnaev
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgInS	vládnout
jsem	být	k5eAaImIp1nS	být
<g/>
)	)	kIx)	)
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
nejníže	nízce	k6eAd3	nízce
vespod	vespod	k6eAd1	vespod
umístěný	umístěný	k2eAgMnSc1d1	umístěný
jedinec	jedinec	k1gMnSc1	jedinec
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
sum	suma	k1gFnPc2	suma
sine	sinus	k1gInSc5	sinus
regno	regen	k2eAgNnSc4d1	regen
(	(	kIx(	(
<g/>
nemám	mít	k5eNaImIp1nS	mít
žádné	žádný	k3yNgNnSc1	žádný
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
jako	jako	k8xC	jako
žena	žena	k1gFnSc1	žena
kolem	kolem	k6eAd1	kolem
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fortuna	Fortuna	k1gFnSc1	Fortuna
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
kulturních	kulturní	k2eAgInPc6d1	kulturní
dílech	díl	k1gInPc6	díl
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
básní	báseň	k1gFnPc2	báseň
Guillauma	Guillauma	k1gFnSc1	Guillauma
de	de	k?	de
Lorrise	Lorrise	k1gFnSc2	Lorrise
a	a	k8xC	a
Jeana	Jean	k1gMnSc2	Jean
de	de	k?	de
Meuna	Meun	k1gInSc2	Meun
Le	Le	k1gMnSc1	Le
Roman	Roman	k1gMnSc1	Roman
de	de	k?	de
la	la	k1gNnSc2	la
Rose	Ros	k1gMnSc2	Ros
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Román	román	k1gInSc1	román
o	o	k7c4	o
růži	růže	k1gFnSc4	růže
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Fortuna	Fortuna	k1gFnSc1	Fortuna
zklamala	zklamat	k5eAaPmAgFnS	zklamat
naděje	naděje	k1gFnPc4	naděje
milence	milenec	k1gMnSc2	milenec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
zachráněn	zachránit	k5eAaPmNgMnS	zachránit
personifikovanou	personifikovaný	k2eAgFnSc7d1	personifikovaná
postavou	postava	k1gFnSc7	postava
"	"	kIx"	"
<g/>
Příčina	příčina	k1gFnSc1	příčina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Danteho	Dante	k1gMnSc2	Dante
díle	díl	k1gInSc6	díl
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Božské	božský	k2eAgFnSc2d1	božská
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
zpěvu	zpěv	k1gInSc6	zpěv
Vergilius	Vergilius	k1gMnSc1	Vergilius
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
povahu	povaha	k1gFnSc4	povaha
Fortuny	Fortuna	k1gFnSc2	Fortuna
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lydgate	Lydgat	k1gInSc5	Lydgat
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
Boccacciova	Boccacciův	k2eAgNnSc2d1	Boccacciův
díla	dílo	k1gNnSc2	dílo
De	De	k?	De
Casibus	Casibus	k1gMnSc1	Casibus
Virorum	Virorum	k1gNnSc1	Virorum
Illustrium	Illustrium	k1gNnSc1	Illustrium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Osudy	osud	k1gInPc1	osud
slavných	slavný	k2eAgMnPc2d1	slavný
mužů	muž	k1gMnPc2	muž
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dílo	dílo	k1gNnSc4	dílo
Falls	Falls	k1gInSc1	Falls
of	of	k?	of
Princes	princes	k1gInSc1	princes
(	(	kIx(	(
<g/>
Pády	Pád	k1gInPc1	Pád
princů	princ	k1gMnPc2	princ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
při	při	k7c6	při
otáčení	otáčení	k1gNnSc6	otáčení
kola	kolo	k1gNnSc2	kolo
osudu	osud	k1gInSc2	osud
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
neštěstí	neštěstí	k1gNnSc3	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
středověkých	středověký	k2eAgFnPc2d1	středověká
náboženských	náboženský	k2eAgFnPc2d1	náboženská
<g/>
,	,	kIx,	,
milostných	milostný	k2eAgFnPc2d1	milostná
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
sepsaném	sepsaný	k2eAgNnSc6d1	sepsané
kolem	kolem	k6eAd1	kolem
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
a	a	k8xC	a
názvem	název	k1gInSc7	název
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
25	[number]	k4	25
<g/>
.	.	kIx.	.
kapitole	kapitola	k1gFnSc6	kapitola
Machiavelliho	Machiavelli	k1gMnSc2	Machiavelli
Prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
říká	říkat	k5eAaImIp3nS	říkat
Fortuně	Fortuna	k1gFnSc3	Fortuna
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
polovina	polovina	k1gFnSc1	polovina
mužského	mužský	k2eAgInSc2d1	mužský
života	život	k1gInSc2	život
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
osudu	osud	k1gInSc6	osud
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Machiavelli	Machiavelli	k1gMnSc1	Machiavelli
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fortuna	Fortuna	k1gFnSc1	Fortuna
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
projevuje	projevovat	k5eAaImIp3nS	projevovat
přízeň	přízeň	k1gFnSc4	přízeň
mužům	muž	k1gMnPc3	muž
silným	silný	k2eAgInSc7d1	silný
nebo	nebo	k8xC	nebo
vládnoucím	vládnoucí	k2eAgInSc7d1	vládnoucí
pevnou	pevný	k2eAgFnSc7d1	pevná
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
protekci	protekce	k1gFnSc4	protekce
více	hodně	k6eAd2	hodně
průbojným	průbojný	k2eAgMnPc3d1	průbojný
a	a	k8xC	a
statečným	statečný	k2eAgMnPc3d1	statečný
mladým	mladý	k2eAgMnPc3d1	mladý
mužům	muž	k1gMnPc3	muž
než	než	k8xS	než
těm	ten	k3xDgFnPc3	ten
bojácným	bojácný	k2eAgFnPc3d1	bojácná
a	a	k8xC	a
starým	starý	k2eAgFnPc3d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
také	také	k9	také
není	být	k5eNaImIp3nS	být
cizí	cizí	k2eAgMnSc1d1	cizí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
nemilosti	nemilost	k1gFnSc6	nemilost
Fortuny	Fortuna	k1gFnSc2	Fortuna
i	i	k8xC	i
očích	oko	k1gNnPc6	oko
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
ocitnu	ocitnout	k5eAaPmIp1nS	ocitnout
</s>
</p>
<p>
<s>
zapláču	zaplakat	k5eAaPmIp1nS	zaplakat
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
nad	nad	k7c7	nad
údělem	úděl	k1gInSc7	úděl
vyděděnce	vyděděnec	k1gMnSc2	vyděděnec
<g/>
...	...	k?	...
–	–	k?	–
Sonet	sonet	k1gInSc4	sonet
29	[number]	k4	29
<g/>
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
Fortuně	Fortuna	k1gFnSc6	Fortuna
věnoval	věnovat	k5eAaImAgMnS	věnovat
první	první	k4xOgNnSc4	první
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Imperatrix	Imperatrix	k1gInSc4	Imperatrix
Mundi	Mund	k1gMnPc1	Mund
<g/>
)	)	kIx)	)
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
<g/>
:	:	kIx,	:
Text	text	k1gInSc1	text
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začíná	začínat	k5eAaImIp3nS	začínat
</s>
</p>
<p>
<s>
O	O	kA	O
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
velut	velut	k1gInSc1	velut
luna	luna	k1gFnSc1	luna
</s>
</p>
<p>
<s>
statu	status	k1gInSc2	status
variabilis	variabilis	k1gFnPc1	variabilis
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Ó	ó	k0	ó
Fortuno	Fortuna	k1gFnSc5	Fortuna
<g/>
,	,	kIx,	,
jsi	být	k5eAaImIp2nS	být
jak	jak	k8xS	jak
Měsíc	měsíc	k1gInSc4	měsíc
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g/>
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
píseň	píseň	k1gFnSc1	píseň
začíná	začínat	k5eAaImIp3nS	začínat
</s>
</p>
<p>
<s>
Fortune	Fortun	k1gMnSc5	Fortun
plango	plango	k1gMnSc1	plango
vulnera	vulner	k1gMnSc4	vulner
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Rány	Rán	k1gMnPc7	Rán
Fortuny	Fortuna	k1gFnSc2	Fortuna
zraňují	zraňovat	k5eAaImIp3nP	zraňovat
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Atributy	atribut	k1gInPc4	atribut
==	==	k?	==
</s>
</p>
<p>
<s>
Středověké	středověký	k2eAgNnSc1d1	středověké
líčení	líčení	k1gNnSc1	líčení
Fortuny	Fortuna	k1gFnSc2	Fortuna
zdůrazňovalo	zdůrazňovat	k5eAaImAgNnS	zdůrazňovat
její	její	k3xOp3gNnSc1	její
dualitu	dualita	k1gFnSc4	dualita
a	a	k8xC	a
nestabilitu	nestabilita	k1gFnSc4	nestabilita
jako	jako	k9	jako
dvě	dva	k4xCgFnPc4	dva
tváře	tvář	k1gFnPc4	tvář
bok	boka	k1gFnPc2	boka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
byl	být	k5eAaImAgMnS	být
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
římský	římský	k2eAgMnSc1d1	římský
bůh	bůh	k1gMnSc1	bůh
začátků	začátek	k1gInPc2	začátek
a	a	k8xC	a
konců	konec	k1gInPc2	konec
Janus	Janus	k1gMnSc1	Janus
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
tvář	tvář	k1gFnSc1	tvář
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
mračí	mračit	k5eAaImIp3nS	mračit
<g/>
;	;	kIx,	;
jedna	jeden	k4xCgFnSc1	jeden
polovina	polovina	k1gFnSc1	polovina
tváře	tvář	k1gFnSc2	tvář
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slepá	slepý	k2eAgFnSc1d1	slepá
se	s	k7c7	s
zahalenýma	zahalený	k2eAgNnPc7d1	zahalené
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
vah	váha	k1gFnPc2	váha
<g/>
,	,	kIx,	,
slepá	slepý	k2eAgFnSc1d1	slepá
ke	k	k7c3	k
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
její	její	k3xOp3gFnSc4	její
temperamentní	temperamentní	k2eAgNnSc1d1	temperamentní
oblečení	oblečení	k1gNnSc1	oblečení
a	a	k8xC	a
nebojácná	bojácný	k2eNgFnSc1d1	nebojácná
nemravnost	nemravnost	k1gFnSc1	nemravnost
připomínat	připomínat	k5eAaImF	připomínat
prostitutku	prostitutka	k1gFnSc4	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
rohem	roh	k1gInSc7	roh
hojnosti	hojnost	k1gFnSc2	hojnost
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cornu	cornout	k5eAaImIp1nS	cornout
copiae	copiae	k1gInSc1	copiae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kormidlem	kormidlo	k1gNnSc7	kormidlo
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
koulí	koule	k1gFnSc7	koule
nebo	nebo	k8xC	nebo
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Cornucopia	Cornucopia	k1gFnSc1	Cornucopia
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hojnost	hojnost	k1gFnSc1	hojnost
přetéká	přetékat	k5eAaImIp3nS	přetékat
<g/>
,	,	kIx,	,
kolo	kolo	k1gNnSc1	kolo
kormidelníka	kormidelník	k1gMnSc2	kormidelník
manévruje	manévrovat	k5eAaImIp3nS	manévrovat
osudem	osud	k1gInSc7	osud
<g/>
,	,	kIx,	,
koule	koule	k1gFnSc1	koule
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
šance	šance	k1gFnSc2	šance
(	(	kIx(	(
<g/>
komu	kdo	k3yInSc3	kdo
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
štěstí	štěstí	k1gNnSc1	štěstí
nebo	nebo	k8xC	nebo
smůla	smůla	k1gFnSc1	smůla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolo	kolo	k1gNnSc1	kolo
osudu	osud	k1gInSc2	osud
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
že	že	k8xS	že
štěstí	štěstí	k1gNnSc1	štěstí
nebo	nebo	k8xC	nebo
smůla	smůla	k1gFnSc1	smůla
nikdy	nikdy	k6eAd1	nikdy
netrvá	trvat	k5eNaImIp3nS	trvat
věčně	věčně	k6eAd1	věčně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svátky	svátek	k1gInPc4	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
Fortuně	Fortuna	k1gFnSc6	Fortuna
11	[number]	k4	11
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
slavil	slavit	k5eAaImAgInS	slavit
velký	velký	k2eAgInSc1d1	velký
svátek	svátek	k1gInSc1	svátek
Fors	Fors	k1gInSc1	Fors
Fortuna	Fortuna	k1gFnSc1	Fortuna
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Božstvo	božstvo	k1gNnSc4	božstvo
šťastné	šťastný	k2eAgFnSc2d1	šťastná
náhody	náhoda	k1gFnSc2	náhoda
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přídomky	přídomek	k1gInPc1	přídomek
Fortuny	Fortuna	k1gFnSc2	Fortuna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
===	===	k?	===
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Annonaria	Annonarium	k1gNnSc2	Annonarium
Fortuna	Fortuna	k1gFnSc1	Fortuna
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Belli	Belle	k1gFnSc4	Belle
Fortuna	Fortuna	k1gFnSc1	Fortuna
válečná	válečná	k1gFnSc1	válečná
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Victrix	Victrix	k1gInSc1	Victrix
Fortuna	Fortuna	k1gFnSc1	Fortuna
vítězná	vítězný	k2eAgFnSc1d1	vítězná
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Conservatrix	Conservatrix	k1gInSc1	Conservatrix
Fortuna	Fortuna	k1gFnSc1	Fortuna
zachránkyně	zachránkyně	k1gFnSc1	zachránkyně
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Equestris	Equestris	k1gFnSc1	Equestris
Fortuna	Fortuna	k1gFnSc1	Fortuna
rytířská	rytířský	k2eAgFnSc1d1	rytířská
(	(	kIx(	(
<g/>
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Huiusque	Huiusqu	k1gFnSc2	Huiusqu
Fortuna	Fortuna	k1gFnSc1	Fortuna
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Obsequens	Obsequensa	k1gFnPc2	Obsequensa
Fortuna	Fortuna	k1gFnSc1	Fortuna
poddaná	poddaná	k1gFnSc1	poddaná
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Privata	Privat	k2eAgFnSc1d1	Privat
Fortuna	Fortuna	k1gFnSc1	Fortuna
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Publica	publicum	k1gNnSc2	publicum
Fortuna	Fortuna	k1gFnSc1	Fortuna
státní	státní	k2eAgFnSc2d1	státní
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Romana	Romana	k1gFnSc1	Romana
Fortuna	Fortuna	k1gFnSc1	Fortuna
Římská	římský	k2eAgFnSc1d1	římská
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Virgo	Virgo	k6eAd1	Virgo
Fortuna	Fortuna	k1gFnSc1	Fortuna
panenská	panenský	k2eAgFnSc1d1	panenská
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Respiciens	Respiciensa	k1gFnPc2	Respiciensa
Fortuna	Fortuna	k1gFnSc1	Fortuna
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
ohlížejí	ohlížet	k5eAaImIp3nP	ohlížet
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Augusta	August	k1gMnSc2	August
Fortuna	Fortuna	k1gFnSc1	Fortuna
vznešená	vznešený	k2eAgNnPc4d1	vznešené
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Balnearis	Balnearis	k1gFnSc1	Balnearis
Fortuna	Fortuna	k1gFnSc1	Fortuna
lazebnická	lazebnický	k2eAgFnSc1d1	Lazebnická
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Primigenia	Primigenium	k1gNnSc2	Primigenium
Fortuna	Fortuna	k1gFnSc1	Fortuna
prvorozená	prvorozený	k2eAgFnSc1d1	prvorozená
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Muliebris	Muliebris	k1gFnSc1	Muliebris
Fortuna	Fortuna	k1gFnSc1	Fortuna
ženská	ženská	k1gFnSc1	ženská
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Virilis	Virilis	k1gFnSc1	Virilis
Fortuna	Fortuna	k1gFnSc1	Fortuna
mužská	mužský	k2eAgFnSc1d1	mužská
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Redux	Redux	k1gInSc1	Redux
Fortuna	Fortuna	k1gFnSc1	Fortuna
přinášející	přinášející	k2eAgInSc4d1	přinášející
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
domov	domov	k1gInSc4	domov
(	(	kIx(	(
<g/>
přivádějící	přivádějící	k2eAgFnPc1d1	přivádějící
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Negativní	negativní	k2eAgNnSc4d1	negativní
===	===	k?	===
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Dubia	Dubia	k1gFnSc1	Dubia
Fortuna	Fortuna	k1gFnSc1	Fortuna
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
<g/>
,	,	kIx,	,
nespolehlivá	spolehlivý	k2eNgFnSc1d1	nespolehlivá
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Brevis	Brevis	k1gFnSc1	Brevis
Fortuna	Fortuna	k1gFnSc1	Fortuna
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgFnSc1d1	krátká
</s>
</p>
<p>
<s>
Fortuna	Fortuna	k1gFnSc1	Fortuna
Mala	Mala	k1gFnSc1	Mala
Fortuna	Fortuna	k1gFnSc1	Fortuna
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
<g/>
,	,	kIx,	,
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Howard	Howard	k1gMnSc1	Howard
Rollin	Rollina	k1gFnPc2	Rollina
Patch	Patch	k1gMnSc1	Patch
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Tradition	Tradition	k1gInSc1	Tradition
of	of	k?	of
the	the	k?	the
Goddess	Goddess	k1gInSc1	Goddess
Fortuna	Fortuna	k1gFnSc1	Fortuna
in	in	k?	in
Medieval	medieval	k1gInSc1	medieval
Philosophy	Philosopha	k1gMnSc2	Philosopha
and	and	k?	and
Literature	Literatur	k1gMnSc5	Literatur
</s>
</p>
<p>
<s>
Howard	Howard	k1gMnSc1	Howard
Rollin	Rollina	k1gFnPc2	Rollina
Patch	Patch	k1gMnSc1	Patch
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fortuna	Fortuna	k1gFnSc1	Fortuna
in	in	k?	in
Old	Olda	k1gFnPc2	Olda
French	French	k1gMnSc1	French
Literature	Literatur	k1gInSc5	Literatur
</s>
</p>
<p>
<s>
Howard	Howard	k1gMnSc1	Howard
Rollin	Rollina	k1gFnPc2	Rollina
Patch	Patch	k1gMnSc1	Patch
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
repr	repr	k1gInSc1	repr
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Goddess	Goddess	k1gInSc1	Goddess
Fortuna	Fortuna	k1gFnSc1	Fortuna
in	in	k?	in
Medieval	medieval	k1gInSc1	medieval
Literature	Literatur	k1gMnSc5	Literatur
</s>
</p>
<p>
<s>
Lesley	Leslea	k1gFnPc1	Leslea
Adkins	Adkinsa	k1gFnPc2	Adkinsa
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
A.	A.	kA	A.
Adkins	Adkins	k1gInSc1	Adkins
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Roman	Roman	k1gMnSc1	Roman
Religion	religion	k1gInSc1	religion
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
</s>
</p>
<p>
<s>
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
</s>
</p>
<p>
<s>
Vertumnus	Vertumnus	k1gMnSc1	Vertumnus
</s>
</p>
<p>
<s>
Janus	Janus	k1gMnSc1	Janus
</s>
</p>
<p>
<s>
Sudička	sudička	k1gFnSc1	sudička
</s>
</p>
<p>
<s>
Norny	norna	k1gFnPc1	norna
</s>
</p>
<p>
<s>
Moiry	Moira	k1gFnPc1	Moira
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fortuna	Fortuna	k1gFnSc1	Fortuna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Best	Best	k1gMnSc1	Best
<g/>
:	:	kIx,	:
Středověká	středověký	k2eAgFnSc1d1	středověká
tragédie	tragédie	k1gFnSc1	tragédie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.novaroma.org/nr/Cultus_Fortunae	[url]	k6eAd1	http://www.novaroma.org/nr/Cultus_Fortunae
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Fortuna	Fortuna	k1gFnSc1	Fortuna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
