<s>
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gFnSc1	Valle
je	být	k5eAaImIp3nS	být
masivní	masivní	k2eAgFnSc1d1	masivní
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
neaktivní	aktivní	k2eNgInSc4d1	neaktivní
stratovulkán	stratovulkán	k1gInSc4	stratovulkán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Panamá	Panamý	k2eAgFnSc1d1	Panamá
<g/>
.	.	kIx.	.
</s>
