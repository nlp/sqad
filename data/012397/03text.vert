<p>
<s>
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gFnSc1	Valle
je	být	k5eAaImIp3nS	být
masivní	masivní	k2eAgFnSc1d1	masivní
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
neaktivní	aktivní	k2eNgInSc4d1	neaktivní
stratovulkán	stratovulkán	k1gInSc4	stratovulkán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Panamá	Panamý	k2eAgFnSc1d1	Panamá
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
starší	starší	k1gMnPc4	starší
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
široké	široký	k2eAgFnSc6d1	široká
pleistocénní	pleistocénní	k2eAgFnSc6d1	pleistocénní
kaldeře	kaldera	k1gFnSc6	kaldera
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gFnSc2	Valle
de	de	k?	de
Antón	Antón	k1gInSc1	Antón
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gNnPc2	Valle
má	mít	k5eAaImIp3nS	mít
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
sopečný	sopečný	k2eAgInSc4d1	sopečný
kráter	kráter	k1gInSc4	kráter
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
5	[number]	k4	5
x	x	k?	x
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postkalderové	Postkalderový	k2eAgNnSc1d1	Postkalderový
stadium	stadium	k1gNnSc1	stadium
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přineslo	přinést	k5eAaPmAgNnS	přinést
vznik	vznik	k1gInSc4	vznik
komplexu	komplex	k1gInSc2	komplex
tří	tři	k4xCgInPc2	tři
dacitových	dacitový	k2eAgInPc2d1	dacitový
lávových	lávový	k2eAgInPc2d1	lávový
dómů	dóm	k1gInPc2	dóm
nazvaných	nazvaný	k2eAgInPc2d1	nazvaný
Cerro	Cerro	k1gNnSc4	Cerro
Pajita	Pajita	k1gFnSc1	Pajita
<g/>
,	,	kIx,	,
Cerro	Cerro	k1gNnSc1	Cerro
Gaital	Gaital	k1gMnSc1	Gaital
<g/>
,	,	kIx,	,
a	a	k8xC	a
Cerro	Cerro	k1gNnSc1	Cerro
Caracoral	Caracoral	k1gFnSc2	Caracoral
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
sérii	série	k1gFnSc4	série
erupcí	erupce	k1gFnPc2	erupce
pliniovského	pliniovský	k2eAgInSc2d1	pliniovský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vyprodukovaly	vyprodukovat	k5eAaPmAgFnP	vyprodukovat
značné	značný	k2eAgInPc4d1	značný
objemy	objem	k1gInPc4	objem
pyroklastických	pyroklastický	k2eAgInPc2d1	pyroklastický
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
depozity	depozit	k1gInPc1	depozit
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
až	až	k9	až
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
25	[number]	k4	25
km	km	kA	km
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gFnSc1	Valle
skončila	skončit	k5eAaPmAgFnS	skončit
asi	asi	k9	asi
před	před	k7c7	před
13	[number]	k4	13
000	[number]	k4	000
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
oblast	oblast	k1gFnSc1	oblast
předmětem	předmět	k1gInSc7	předmět
geofyzikálního	geofyzikální	k2eAgInSc2d1	geofyzikální
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
sopky	sopka	k1gFnSc2	sopka
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
údolí	údolí	k1gNnSc1	údolí
s	s	k7c7	s
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
lávovou	lávový	k2eAgFnSc7d1	lávová
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vynikající	vynikající	k2eAgFnPc4d1	vynikající
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
faunu	fauna	k1gFnSc4	fauna
i	i	k8xC	i
flóru	flóra	k1gFnSc4	flóra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zástupcům	zástupce	k1gMnPc3	zástupce
zdejší	zdejší	k2eAgFnSc2d1	zdejší
zvířeny	zvířena	k1gFnSc2	zvířena
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
"	"	kIx"	"
<g/>
zlaté	zlatý	k2eAgFnPc4d1	zlatá
<g/>
"	"	kIx"	"
žáby	žába	k1gFnPc4	žába
(	(	kIx(	(
<g/>
atelopus	atelopus	k1gMnSc1	atelopus
panamský	panamský	k2eAgMnSc1d1	panamský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modří	modrý	k2eAgMnPc1d1	modrý
motýli	motýl	k1gMnPc1	motýl
(	(	kIx(	(
<g/>
Morpho	Morp	k1gMnSc2	Morp
menelaus	menelaus	k1gMnSc1	menelaus
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kolibříků	kolibřík	k1gMnPc2	kolibřík
<g/>
,	,	kIx,	,
tangar	tangara	k1gFnPc2	tangara
<g/>
,	,	kIx,	,
lejsků	lejsek	k1gMnPc2	lejsek
<g/>
,	,	kIx,	,
holubů	holub	k1gMnPc2	holub
či	či	k8xC	či
datlů	datel	k1gMnPc2	datel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gInSc1	Valle
(	(	kIx(	(
<g/>
sopka	sopka	k1gFnSc1	sopka
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
www.volcano.si.edu	www.volcano.si.edu	k6eAd1	www.volcano.si.edu
stratovulkán	stratovulkán	k1gInSc1	stratovulkán
El	Ela	k1gFnPc2	Ela
Valle	Valle	k1gFnSc2	Valle
na	na	k7c4	na
Global	globat	k5eAaImAgInS	globat
Volcanism	Volcanism	k1gInSc1	Volcanism
Program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
