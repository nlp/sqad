<p>
<s>
Finština	finština	k1gFnSc1	finština
(	(	kIx(	(
<g/>
zast.	zast.	k?	zast.
čuchonština	čuchonština	k1gFnSc1	čuchonština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
jazyk	jazyk	k1gInSc4	jazyk
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hovoří	hovořit	k5eAaImIp3nS	hovořit
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
ruské	ruský	k2eAgFnSc6d1	ruská
části	část	k1gFnSc6	část
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
finsky	finsky	k6eAd1	finsky
mluví	mluvit	k5eAaImIp3nS	mluvit
další	další	k2eAgInSc1d1	další
milion	milion	k4xCgInSc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ugrofinský	ugrofinský	k2eAgInSc1d1	ugrofinský
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
baltofinský	baltofinský	k2eAgInSc1d1	baltofinský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
i	i	k9	i
přes	přes	k7c4	přes
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
soužití	soužití	k1gNnSc4	soužití
s	s	k7c7	s
indoevropskými	indoevropský	k2eAgMnPc7d1	indoevropský
jazyky	jazyk	k1gMnPc7	jazyk
zachoval	zachovat	k5eAaPmAgMnS	zachovat
mnohé	mnohý	k2eAgNnSc4d1	mnohé
své	své	k1gNnSc4	své
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
germánských	germánský	k2eAgMnPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
například	například	k6eAd1	například
na	na	k7c6	na
systému	systém	k1gInSc6	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
obohacení	obohacení	k1gNnSc1	obohacení
o	o	k7c6	o
perifrasticky	perifrasticky	k6eAd1	perifrasticky
tvořené	tvořený	k2eAgInPc4d1	tvořený
tvary	tvar	k1gInPc4	tvar
časů	čas	k1gInPc2	čas
předpřítomného	předpřítomný	k2eAgInSc2d1	předpřítomný
<g/>
,	,	kIx,	,
předminulého	předminulý	k2eAgInSc2d1	předminulý
a	a	k8xC	a
málo	málo	k6eAd1	málo
užívaného	užívaný	k2eAgNnSc2d1	užívané
budoucího	budoucí	k2eAgNnSc2d1	budoucí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
preferování	preferování	k1gNnSc1	preferování
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
vět	věta	k1gFnPc2	věta
před	před	k7c7	před
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
s	s	k7c7	s
nominálními	nominální	k2eAgInPc7d1	nominální
tvary	tvar	k1gInPc7	tvar
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
psané	psaný	k2eAgInPc1d1	psaný
finské	finský	k2eAgInPc1d1	finský
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
vydal	vydat	k5eAaPmAgMnS	vydat
Mikael	Mikalo	k1gNnPc2	Mikalo
Agricola	Agricola	k1gFnSc1	Agricola
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
překlad	překlad	k1gInSc4	překlad
Nového	Nový	k1gMnSc2	Nový
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
byla	být	k5eAaImAgFnS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
finském	finský	k2eAgNnSc6d1	finské
území	území	k1gNnSc6	území
švédština	švédština	k1gFnSc1	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
Finsko	Finsko	k1gNnSc1	Finsko
přešlo	přejít	k5eAaPmAgNnS	přejít
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ruští	ruský	k2eAgMnPc1d1	ruský
vládci	vládce	k1gMnPc1	vládce
snažili	snažit	k5eAaImAgMnP	snažit
oslabit	oslabit	k5eAaPmF	oslabit
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
Švédsko	Švédsko	k1gNnSc4	Švédsko
i	i	k8xC	i
podporou	podpora	k1gFnSc7	podpora
finštiny	finština	k1gFnSc2	finština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
jazyk	jazyk	k1gInSc1	jazyk
používat	používat	k5eAaImF	používat
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
finština	finština	k1gFnSc1	finština
doporučena	doporučen	k2eAgFnSc1d1	doporučena
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
a	a	k8xC	a
při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
je	být	k5eAaImIp3nS	být
finština	finština	k1gFnSc1	finština
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
zcela	zcela	k6eAd1	zcela
rovnoprávná	rovnoprávný	k2eAgFnSc1d1	rovnoprávná
se	s	k7c7	s
švédštinou	švédština	k1gFnSc7	švédština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnPc4d1	jazyková
varianty	varianta	k1gFnPc4	varianta
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
jazykům	jazyk	k1gInPc3	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
mluveným	mluvený	k2eAgInSc7d1	mluvený
a	a	k8xC	a
psaným	psaný	k2eAgInSc7d1	psaný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vykání	vykání	k1gNnSc1	vykání
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
jen	jen	k9	jen
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
zdvořilé	zdvořilý	k2eAgFnSc3d1	zdvořilá
konverzaci	konverzace	k1gFnSc3	konverzace
nebo	nebo	k8xC	nebo
projevu	projev	k1gInSc3	projev
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
finštině	finština	k1gFnSc6	finština
odlišných	odlišný	k2eAgInPc2d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
–	–	k?	–
minä	minä	k?	minä
olen	olen	k1gInSc1	olen
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
mä	mä	k?	mä
oon	oon	k?	oon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
máš	mít	k5eAaImIp2nS	mít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
onko	onko	k1gMnSc1	onko
sinulla	sinulla	k1gMnSc1	sinulla
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c4	na
onks	onks	k1gInSc4	onks
sulla	sullo	k1gNnSc2	sullo
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgNnSc1d1	aktivní
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc1d1	hlavní
dvě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
východní	východní	k2eAgInPc4d1	východní
a	a	k8xC	a
západní	západní	k2eAgInPc4d1	západní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
severogermánských	severogermánský	k2eAgInPc2d1	severogermánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
švédštiny	švédština	k1gFnPc1	švédština
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
finskou	finský	k2eAgFnSc4d1	finská
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
jazyky	jazyk	k1gInPc1	jazyk
baltské	baltský	k2eAgInPc1d1	baltský
<g/>
,	,	kIx,	,
hansovní	hansovní	k2eAgFnSc1d1	hansovní
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
především	především	k9	především
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
slov	slovo	k1gNnPc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skládání	skládání	k1gNnSc1	skládání
slov	slovo	k1gNnPc2	slovo
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
aglutinačního	aglutinační	k2eAgInSc2d1	aglutinační
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvaroslovné	tvaroslovný	k2eAgFnPc1d1	tvaroslovná
přípony	přípona	k1gFnPc1	přípona
se	se	k3xPyFc4	se
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
nalepují	nalepovat	k5eAaImIp3nP	nalepovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
tvarově	tvarově	k6eAd1	tvarově
různorodá	různorodý	k2eAgNnPc1d1	různorodé
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
morfy	morf	k1gInPc1	morf
nesou	nést	k5eAaImIp3nP	nést
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
jeden	jeden	k4xCgInSc4	jeden
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vo	vo	k?	vo
<g/>
/	/	kIx~	/
<g/>
isi	isi	k?	isi
<g/>
/	/	kIx~	/
<g/>
mme	mme	k?	mme
<g/>
/	/	kIx~	/
<g/>
ko	ko	k?	ko
<g/>
/	/	kIx~	/
<g/>
han	hana	k1gFnPc2	hana
–	–	k?	–
"	"	kIx"	"
<g/>
mohli	moct	k5eAaImAgMnP	moct
bychom	by	kYmCp1nP	by
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Kmen	kmen	k1gInSc1	kmen
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
změněný	změněný	k2eAgMnSc1d1	změněný
z	z	k7c2	z
voi	voi	k?	voi
na	na	k7c6	na
vo	vo	k?	vo
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
-isi	-isi	k6eAd1	-isi
značí	značit	k5eAaImIp3nS	značit
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
-mme	-mme	k1gInSc1	-mme
1	[number]	k4	1
<g/>
.	.	kIx.	.
osobu	osoba	k1gFnSc4	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
-ko	-ko	k?	-ko
ano-ne	anoout	k5eAaImIp3nS	ano-nout
otázku	otázka	k1gFnSc4	otázka
a	a	k8xC	a
přípona	přípona	k1gFnSc1	přípona
-han	-hana	k1gFnPc2	-hana
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
významů	význam	k1gInPc2	význam
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
potvrzení	potvrzení	k1gNnSc4	potvrzení
<g/>
,	,	kIx,	,
asi	asi	k9	asi
"	"	kIx"	"
<g/>
přece	přece	k9	přece
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
že	že	k9	že
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
talo	talo	k1gNnSc1	talo
<g/>
/	/	kIx~	/
<g/>
ssa	ssa	k?	ssa
<g/>
/	/	kIx~	/
<g/>
ni	on	k3xPp3gFnSc4	on
<g/>
/	/	kIx~	/
<g/>
kin	kino	k1gNnPc2	kino
<g/>
/	/	kIx~	/
<g/>
ko	ko	k?	ko
–	–	k?	–
"	"	kIx"	"
<g/>
také	také	k9	také
v	v	k7c6	v
mém	můj	k3xOp1gInSc6	můj
domě	dům	k1gInSc6	dům
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Kmen	kmen	k1gInSc1	kmen
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
talo	talo	k6eAd1	talo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
-ssa	-ssa	k6eAd1	-ssa
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
v	v	k7c4	v
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
-ni	-ni	k?	-ni
je	být	k5eAaImIp3nS	být
posesivní	posesivní	k2eAgFnSc1d1	posesivní
přípona	přípona	k1gFnSc1	přípona
"	"	kIx"	"
<g/>
můj	můj	k1gMnSc1	můj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
-kin	-kin	k1gInSc1	-kin
značí	značit	k5eAaImIp3nS	značit
"	"	kIx"	"
<g/>
také	také	k9	také
<g/>
"	"	kIx"	"
a	a	k8xC	a
-ko	-ko	k?	-ko
ano-ne	anoout	k5eAaImIp3nS	ano-nout
otázku	otázka	k1gFnSc4	otázka
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
běžné	běžný	k2eAgFnSc2d1	běžná
složeniny	složenina	k1gFnSc2	složenina
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgNnPc1d1	výsledné
slova	slovo	k1gNnPc1	slovo
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opravdu	opravdu	k6eAd1	opravdu
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kolmivaihekilowattituntimittari	kolmivaihekilowattituntimittari	k6eAd1	kolmivaihekilowattituntimittari
–	–	k?	–
"	"	kIx"	"
<g/>
třífázový	třífázový	k2eAgInSc1d1	třífázový
kilowatthodinametr	kilowatthodinametr	k1gInSc1	kilowatthodinametr
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
vrstvu	vrstva	k1gFnSc4	vrstva
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
tvoří	tvořit	k5eAaImIp3nS	tvořit
původem	původ	k1gInSc7	původ
staroíránská	staroíránský	k2eAgNnPc1d1	staroíránský
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
z	z	k7c2	z
baltských	baltský	k2eAgInPc2d1	baltský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
vypůjčeno	vypůjčen	k2eAgNnSc1d1	vypůjčeno
ze	z	k7c2	z
švédštiny	švédština	k1gFnSc2	švédština
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
z	z	k7c2	z
historické	historický	k2eAgFnSc2d1	historická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
poměrně	poměrně	k6eAd1	poměrně
nedávné	dávný	k2eNgFnPc1d1	nedávná
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k6eAd1	zejména
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Finsko	Finsko	k1gNnSc1	Finsko
patřilo	patřit	k5eAaImAgNnS	patřit
carskému	carský	k2eAgNnSc3d1	carské
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
východě	východ	k1gInSc6	východ
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
přijato	přijmout	k5eAaPmNgNnS	přijmout
také	také	k6eAd1	také
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
zejména	zejména	k9	zejména
americké	americký	k2eAgFnSc2d1	americká
kultury	kultura	k1gFnSc2	kultura
přejímají	přejímat	k5eAaImIp3nP	přejímat
slova	slovo	k1gNnPc1	slovo
i	i	k8xC	i
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
slovo	slovo	k1gNnSc4	slovo
tsekkaa	tsekka	k1gInSc2	tsekka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
anglické	anglický	k2eAgNnSc1d1	anglické
to	ten	k3xDgNnSc4	ten
check	check	k6eAd1	check
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
i	i	k8xC	i
finština	finština	k1gFnSc1	finština
přijímá	přijímat	k5eAaImIp3nS	přijímat
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
<g/>
,	,	kIx,	,
i	i	k9	i
exotických	exotický	k2eAgInPc2d1	exotický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
mnoho	mnoho	k4c4	mnoho
japonských	japonský	k2eAgNnPc2d1	Japonské
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
několik	několik	k4yIc1	několik
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
robotti	robott	k5eAaImF	robott
nebo	nebo	k8xC	nebo
pistooli	pistooli	k6eAd1	pistooli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
patrné	patrný	k2eAgInPc1d1	patrný
vlivy	vliv	k1gInPc1	vliv
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
i	i	k9	i
v	v	k7c6	v
intonaci	intonace	k1gFnSc6	intonace
<g/>
.	.	kIx.	.
</s>
<s>
Finskou	finský	k2eAgFnSc4d1	finská
ano-ne	anoout	k5eAaImIp3nS	ano-nout
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
intonací	intonace	k1gFnSc7	intonace
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
velmi	velmi	k6eAd1	velmi
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
angličtinou	angličtina	k1gFnSc7	angličtina
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
Finska	Finsko	k1gNnSc2	Finsko
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	s	k7c7	s
zřetelným	zřetelný	k2eAgMnSc7d1	zřetelný
zdvihajícím	zdvihající	k2eAgMnSc7d1	zdvihající
se	se	k3xPyFc4	se
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecné	obecný	k2eAgInPc1d1	obecný
rysy	rys	k1gInPc1	rys
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Abeceda	abeceda	k1gFnSc1	abeceda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
původních	původní	k2eAgNnPc6d1	původní
finských	finský	k2eAgNnPc6d1	finské
slovech	slovo	k1gNnPc6	slovo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
souhlásky	souhláska	k1gFnPc1	souhláska
D	D	kA	D
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
T	T	kA	T
a	a	k8xC	a
V	V	kA	V
(	(	kIx(	(
<g/>
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jen	jen	k9	jen
S	s	k7c7	s
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
zasyčí	zasyčet	k5eAaPmIp3nS	zasyčet
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
podobá	podobat	k5eAaImIp3nS	podobat
českému	český	k2eAgMnSc3d1	český
Š	Š	kA	Š
a	a	k8xC	a
R	R	kA	R
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
kmitů	kmit	k1gInPc2	kmit
<g/>
)	)	kIx)	)
a	a	k8xC	a
samohlásky	samohláska	k1gFnSc2	samohláska
A	A	kA	A
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
a	a	k8xC	a
Ö	Ö	kA	Ö
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
a	a	k8xC	a
U	U	kA	U
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
Y	Y	kA	Y
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
německé	německý	k2eAgInPc1d1	německý
Ü	Ü	kA	Ü
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
A	A	kA	A
a	a	k8xC	a
E	E	kA	E
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gInSc3	jeho
německému	německý	k2eAgInSc3d1	německý
protějšku	protějšek	k1gInSc3	protějšek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgNnPc1d1	finské
slova	slovo	k1gNnPc1	slovo
oplývají	oplývat	k5eAaImIp3nP	oplývat
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
švédštiny	švédština	k1gFnSc2	švédština
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
přidalo	přidat	k5eAaPmAgNnS	přidat
Å	Å	k?	Å
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
v	v	k7c6	v
původních	původní	k2eAgNnPc6d1	původní
švédských	švédský	k2eAgNnPc6d1	švédské
jménech	jméno	k1gNnPc6	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásek	souhláska	k1gFnPc2	souhláska
moderní	moderní	k2eAgFnSc1d1	moderní
finština	finština	k1gFnSc1	finština
přijala	přijmout	k5eAaPmAgFnS	přijmout
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
B	B	kA	B
a	a	k8xC	a
F.	F.	kA	F.
Finská	finský	k2eAgFnSc1d1	finská
abeceda	abeceda	k1gFnSc1	abeceda
tak	tak	k9	tak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechna	všechen	k3xTgNnPc4	všechen
písmena	písmeno	k1gNnPc4	písmeno
anglické	anglický	k2eAgFnSc2d1	anglická
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
občasné	občasný	k2eAgNnSc1d1	občasné
používání	používání	k1gNnSc1	používání
hlásky	hláska	k1gFnSc2	hláska
Š	Š	kA	Š
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
standardní	standardní	k2eAgFnSc2d1	standardní
finštiny	finština	k1gFnSc2	finština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
již	již	k6eAd1	již
asi	asi	k9	asi
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k8xC	i
se	s	k7c7	s
Ž.	Ž.	kA	Ž.
Podle	podle	k7c2	podle
přijatých	přijatý	k2eAgFnPc2d1	přijatá
zvyklostí	zvyklost	k1gFnPc2	zvyklost
se	se	k3xPyFc4	se
názvy	název	k1gInPc1	název
cizích	cizí	k2eAgNnPc2d1	cizí
míst	místo	k1gNnPc2	místo
mají	mít	k5eAaImIp3nP	mít
psát	psát	k5eAaImF	psát
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
písmeny	písmeno	k1gNnPc7	písmeno
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
anglické	anglický	k2eAgFnSc2d1	anglická
ortografie	ortografie	k1gFnSc2	ortografie
jako	jako	k8xC	jako
sh	sh	k?	sh
a	a	k8xC	a
zh	zh	k?	zh
<g/>
.	.	kIx.	.
</s>
<s>
Diakritický	diakritický	k2eAgInSc1d1	diakritický
zápis	zápis	k1gInSc1	zápis
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
souhlásek	souhláska	k1gFnPc2	souhláska
byl	být	k5eAaImAgInS	být
vypůjčen	vypůjčit	k5eAaPmNgInS	vypůjčit
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tšekki	Tšekki	k1gNnSc1	Tšekki
nebo	nebo	k8xC	nebo
Tshekki	Tshekki	k1gNnSc1	Tshekki
–	–	k?	–
"	"	kIx"	"
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Tšaikovski	Tšaikovski	k1gNnSc1	Tšaikovski
nebo	nebo	k8xC	nebo
Tshaikovski	Tshaikovski	k1gNnSc1	Tshaikovski
–	–	k?	–
"	"	kIx"	"
<g/>
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
šakki	šakki	k1gNnSc1	šakki
nebo	nebo	k8xC	nebo
shakki	shakki	k1gNnSc1	shakki
–	–	k?	–
"	"	kIx"	"
<g/>
šachy	šach	k1gInPc4	šach
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Židlicky	Židlicky	k6eAd1	Židlicky
nebo	nebo	k8xC	nebo
Zhidlicky	Zhidlicky	k1gFnSc1	Zhidlicky
–	–	k?	–
"	"	kIx"	"
<g/>
Židlický	Židlický	k1gMnSc1	Židlický
<g/>
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Finů	Fin	k1gMnPc2	Fin
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nepřekonatelným	překonatelný	k2eNgInSc7d1	nepřekonatelný
problémem	problém	k1gInSc7	problém
rozlišit	rozlišit	k5eAaPmF	rozlišit
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Č	Č	kA	Č
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Š	Š	kA	Š
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Z	Z	kA	Z
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ž	Ž	kA	Ž
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
P	P	kA	P
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
F	F	kA	F
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
V	V	kA	V
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
problém	problém	k1gInSc4	problém
při	při	k7c6	při
rozlišování	rozlišování	k1gNnSc6	rozlišování
finských	finský	k2eAgFnPc2d1	finská
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mezi	mezi	k7c4	mezi
A	A	kA	A
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
a	a	k8xC	a
E	E	kA	E
nebo	nebo	k8xC	nebo
i	i	k8xC	i
mezi	mezi	k7c7	mezi
U	U	kA	U
a	a	k8xC	a
Y.	Y.	kA	Y.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vůbec	vůbec	k9	vůbec
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
D	D	kA	D
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
nahrazení	nahrazení	k1gNnSc1	nahrazení
souhláskou	souhláska	k1gFnSc7	souhláska
R.	R.	kA	R.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
objevuje	objevovat	k5eAaImIp3nS	objevovat
nosovka	nosovka	k1gFnSc1	nosovka
ŋ	ŋ	k?	ŋ
<g/>
,	,	kIx,	,
ve	v	k7c4	v
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
mění	měnit	k5eAaImIp3nS	měnit
N	N	kA	N
a	a	k8xC	a
G.	G.	kA	G.
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
M	M	kA	M
občas	občas	k6eAd1	občas
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
ɱ	ɱ	k?	ɱ
a	a	k8xC	a
H	H	kA	H
na	na	k7c6	na
ɦ	ɦ	k?	ɦ
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lankoni	Lankoň	k1gFnSc3	Lankoň
tanssi	tansse	k1gFnSc4	tansse
vahingossa	vahingoss	k1gMnSc2	vahingoss
tangoa	tangous	k1gMnSc2	tangous
flamingon	flamingon	k1gMnSc1	flamingon
kanssa	kanss	k1gMnSc2	kanss
Hangossa	Hangoss	k1gMnSc2	Hangoss
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
výslovnost	výslovnost	k1gFnSc1	výslovnost
"	"	kIx"	"
<g/>
Laŋ	Laŋ	k1gMnSc3	Laŋ
tanssi	tansse	k1gFnSc4	tansse
vahiŋ	vahiŋ	k?	vahiŋ
taŋ	taŋ	k?	taŋ
flamiŋ	flamiŋ	k?	flamiŋ
kanssa	kanssa	k1gFnSc1	kanssa
Haŋ	Haŋ	k1gFnSc1	Haŋ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gMnSc1	můj
švagr	švagr	k1gMnSc1	švagr
náhodou	náhodou	k6eAd1	náhodou
tancoval	tancovat	k5eAaImAgMnS	tancovat
tango	tango	k1gNnSc4	tango
s	s	k7c7	s
plameňákem	plameňák	k1gMnSc7	plameňák
v	v	k7c4	v
Hanku	Hanka	k1gFnSc4	Hanka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
magneetin	magneetin	k2eAgInSc1d1	magneetin
kanssa	kanssa	k1gFnSc1	kanssa
má	můj	k3xOp1gFnSc1	můj
výslovnost	výslovnost	k1gFnSc1	výslovnost
"	"	kIx"	"
<g/>
maŋ	maŋ	k?	maŋ
kanssa	kanssa	k1gFnSc1	kanssa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
s	s	k7c7	s
magnetem	magnet	k1gInSc7	magnet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zdvojené	zdvojený	k2eAgInPc1d1	zdvojený
hlásky	hlásek	k1gInPc1	hlásek
===	===	k?	===
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
české	český	k2eAgNnSc1d1	české
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zdvojené	zdvojený	k2eAgInPc1d1	zdvojený
hlásky	hlásek	k1gInPc1	hlásek
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
samohláska	samohláska	k1gFnSc1	samohláska
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jednoduše	jednoduše	k6eAd1	jednoduše
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
finská	finský	k2eAgFnSc1d1	finská
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
hláska	hláska	k1gFnSc1	hláska
aa	aa	k?	aa
se	se	k3xPyFc4	se
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
jako	jako	k9	jako
v	v	k7c4	v
české	český	k2eAgNnSc4d1	české
"	"	kIx"	"
<g/>
á	á	k0	á
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
však	však	k9	však
zná	znát	k5eAaImIp3nS	znát
i	i	k9	i
dvojhlásky	dvojhláska	k1gFnPc4	dvojhláska
ze	z	k7c2	z
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
gemináty	gemináta	k1gFnSc2	gemináta
-	-	kIx~	-
např.	např.	kA	např.
jako	jako	k8xC	jako
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
-	-	kIx~	-
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
také	také	k9	také
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
dlouze	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
nebo	nebo	k8xC	nebo
zdvojené	zdvojený	k2eAgFnSc2d1	zdvojená
souhlásky	souhláska	k1gFnSc2	souhláska
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
krátké	krátký	k2eAgNnSc4d1	krátké
zadržení	zadržení	k1gNnSc4	zadržení
při	při	k7c6	při
mluvení	mluvení	k1gNnSc6	mluvení
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
zdvojených	zdvojený	k2eAgFnPc2d1	zdvojená
souhlásek	souhláska	k1gFnPc2	souhláska
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
švy	šev	k1gInPc1	šev
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jejich	jejich	k3xOp3gFnSc4	jejich
výslovnost	výslovnost	k1gFnSc4	výslovnost
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xC	jako
náznak	náznak	k1gInSc1	náznak
slabikování	slabikování	k1gNnSc2	slabikování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nerodilého	rodilý	k2eNgMnSc4d1	nerodilý
mluvčího	mluvčí	k1gMnSc4	mluvčí
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
těžké	těžký	k2eAgNnSc1d1	těžké
takovou	takový	k3xDgFnSc4	takový
souhlásku	souhláska	k1gFnSc4	souhláska
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zcela	zcela	k6eAd1	zcela
mění	měnit	k5eAaImIp3nS	měnit
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgFnPc1d1	možná
zdvojené	zdvojený	k2eAgFnPc1d1	zdvojená
souhlásky	souhláska	k1gFnPc1	souhláska
jsou	být	k5eAaImIp3nP	být
kk	kk	k?	kk
<g/>
,	,	kIx,	,
ll	ll	k?	ll
<g/>
,	,	kIx,	,
mm	mm	kA	mm
<g/>
,	,	kIx,	,
nn	nn	k?	nn
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
,	,	kIx,	,
rr	rr	k?	rr
<g/>
,	,	kIx,	,
ss	ss	k?	ss
a	a	k8xC	a
tt	tt	k?	tt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
palo	palo	k1gMnSc1	palo
–	–	k?	–
"	"	kIx"	"
<g/>
požár	požár	k1gInSc1	požár
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
pallo	palla	k1gFnSc5	palla
–	–	k?	–
"	"	kIx"	"
<g/>
míč	míč	k1gInSc4	míč
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
tapaa	tapaa	k1gFnSc1	tapaa
–	–	k?	–
"	"	kIx"	"
<g/>
potkává	potkávat	k5eAaImIp3nS	potkávat
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
tappaa	tappaa	k1gFnSc1	tappaa
–	–	k?	–
"	"	kIx"	"
<g/>
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
trojitými	trojitý	k2eAgFnPc7d1	trojitá
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
první	první	k4xOgFnSc1	první
dvojice	dvojice	k1gFnSc1	dvojice
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vaa	vaa	k?	vaa
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
–	–	k?	–
genitiv	genitiv	k1gInSc1	genitiv
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
vaaka	vaaek	k1gInSc2	vaaek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
váha	váha	k1gFnSc1	váha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
mnoho	mnoho	k4c4	mnoho
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
:	:	kIx,	:
ai	ai	k?	ai
<g/>
,	,	kIx,	,
ei	ei	k?	ei
<g/>
,	,	kIx,	,
oi	oi	k?	oi
<g/>
,	,	kIx,	,
ui	ui	k?	ui
<g/>
,	,	kIx,	,
yi	yi	k?	yi
<g/>
,	,	kIx,	,
äi	äi	k?	äi
<g/>
,	,	kIx,	,
öi	öi	k?	öi
<g/>
,	,	kIx,	,
au	au	k0	au
<g/>
,	,	kIx,	,
ou	ou	k0	ou
<g/>
,	,	kIx,	,
eu	eu	k?	eu
<g/>
,	,	kIx,	,
iu	iu	k?	iu
<g/>
,	,	kIx,	,
äy	äy	k?	äy
<g/>
,	,	kIx,	,
öy	öy	k?	öy
<g/>
,	,	kIx,	,
ey	ey	k?	ey
<g/>
,	,	kIx,	,
iy	iy	k?	iy
<g/>
,	,	kIx,	,
ie	ie	k?	ie
<g/>
,	,	kIx,	,
yö	yö	k?	yö
<g/>
,	,	kIx,	,
uo	uo	k?	uo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sauna	sauna	k1gFnSc1	sauna
–	–	k?	–
"	"	kIx"	"
<g/>
sauna	sauna	k1gFnSc1	sauna
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
äiti	äiti	k6eAd1	äiti
–	–	k?	–
"	"	kIx"	"
<g/>
máma	máma	k1gFnSc1	máma
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásková	samohláskový	k2eAgFnSc1d1	samohlásková
harmonie	harmonie	k1gFnSc1	harmonie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
slovu	slovo	k1gNnSc6	slovo
smí	smět	k5eAaImIp3nS	smět
současně	současně	k6eAd1	současně
objevit	objevit	k5eAaPmF	objevit
jenom	jenom	k9	jenom
samohlásky	samohláska	k1gFnPc4	samohláska
zadní	zadní	k2eAgFnPc4d1	zadní
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
nebo	nebo	k8xC	nebo
přední	přední	k2eAgMnPc1d1	přední
ä	ä	k?	ä
<g/>
,	,	kIx,	,
ö	ö	k?	ö
<g/>
,	,	kIx,	,
y.	y.	k?	y.
Samohlásky	samohláska	k1gFnSc2	samohláska
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
jsou	být	k5eAaImIp3nP	být
neutrální	neutrální	k2eAgInPc1d1	neutrální
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
slově	slovo	k1gNnSc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
neutrální	neutrální	k2eAgFnPc4d1	neutrální
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
přípona	přípona	k1gFnSc1	přípona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
existuje	existovat	k5eAaImIp3nS	existovat
jak	jak	k6eAd1	jak
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
variantě	varianta	k1gFnSc6	varianta
<g/>
,	,	kIx,	,
přednost	přednost	k1gFnSc4	přednost
má	mít	k5eAaImIp3nS	mít
přední	přední	k2eAgMnSc1d1	přední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
kratších	krátký	k2eAgNnPc2d2	kratší
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
samohlásková	samohláskový	k2eAgFnSc1d1	samohlásková
harmonie	harmonie	k1gFnSc1	harmonie
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
obsahovat	obsahovat	k5eAaImF	obsahovat
přední	přední	k2eAgFnPc4d1	přední
i	i	k8xC	i
zadní	zadní	k2eAgFnPc4d1	zadní
samohlásky	samohláska	k1gFnPc4	samohláska
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samohláskovou	samohláskový	k2eAgFnSc4d1	samohlásková
harmonii	harmonie	k1gFnSc4	harmonie
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
turečtině	turečtina	k1gFnSc3	turečtina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
odlišné	odlišný	k2eAgFnSc6d1	odlišná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc1	změna
kmenových	kmenový	k2eAgFnPc2d1	kmenová
souhlásek	souhláska	k1gFnPc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
Třebaže	třebaže	k8xS	třebaže
pádové	pádový	k2eAgFnPc1d1	pádová
přípony	přípona	k1gFnPc1	přípona
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgFnPc1d1	stejná
pro	pro	k7c4	pro
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
před	před	k7c7	před
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
kmenové	kmenový	k2eAgFnSc2d1	kmenová
souhlásky	souhláska	k1gFnSc2	souhláska
probíhá	probíhat	k5eAaImIp3nS	probíhat
mezi	mezi	k7c7	mezi
souhláskami	souhláska	k1gFnPc7	souhláska
silného	silný	k2eAgMnSc2d1	silný
a	a	k8xC	a
slabého	slabý	k2eAgInSc2d1	slabý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gNnSc4	jejich
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stupeň	stupeň	k1gInSc1	stupeň
mění	měnit	k5eAaImIp3nS	měnit
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
lehce	lehko	k6eAd1	lehko
zvládnutelné	zvládnutelný	k2eAgFnPc1d1	zvládnutelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
silta	silta	k1gFnSc1	silta
→	→	k?	→
sillalla	sillalla	k1gFnSc1	sillalla
(	(	kIx(	(
<g/>
lt	lt	k?	lt
-	-	kIx~	-
ll	ll	k?	ll
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
most	most	k1gInSc1	most
<g/>
"	"	kIx"	"
→	→	k?	→
"	"	kIx"	"
<g/>
na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
ikä	ikä	k?	ikä
→	→	k?	→
iässä	iässä	k?	iässä
(	(	kIx(	(
<g/>
k	k	k7c3	k
-	-	kIx~	-
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
věk	věk	k1gInSc1	věk
<g/>
"	"	kIx"	"
→	→	k?	→
"	"	kIx"	"
<g/>
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
tahtoa	tahtoa	k1gMnSc1	tahtoa
→	→	k?	→
tahdon	tahdon	k1gMnSc1	tahdon
(	(	kIx(	(
<g/>
t	t	k?	t
-	-	kIx~	-
d	d	k?	d
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
chtít	chtít	k5eAaImF	chtít
<g/>
"	"	kIx"	"
→	→	k?	→
"	"	kIx"	"
<g/>
chci	chtít	k5eAaImIp1nS	chtít
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Intonace	intonace	k1gFnSc1	intonace
a	a	k8xC	a
přízvuk	přízvuk	k1gInSc1	přízvuk
===	===	k?	===
</s>
</p>
<p>
<s>
Intonace	intonace	k1gFnSc1	intonace
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
klesající	klesající	k2eAgFnSc7d1	klesající
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
ano-ne	anoout	k5eAaPmIp3nS	ano-nout
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	slabý	k2eAgInSc1d1	slabý
přízvuk	přízvuk	k1gInSc1	přízvuk
lze	lze	k6eAd1	lze
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
také	také	k9	také
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ano-ne	Ano	k1gInSc5	Ano-n
otázka	otázka	k1gFnSc1	otázka
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
interogativní	interogativní	k2eAgFnSc4d1	interogativní
příponu	přípona	k1gFnSc4	přípona
-ko	-ko	k?	-ko
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
přední	přední	k2eAgFnSc4d1	přední
variantu	varianta	k1gFnSc4	varianta
-kö	-kö	k?	-kö
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přikládá	přikládat	k5eAaImIp3nS	přikládat
ke	k	k7c3	k
slovům	slovo	k1gNnPc3	slovo
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
odpovědět	odpovědět	k5eAaPmF	odpovědět
"	"	kIx"	"
<g/>
ano	ano	k9	ano
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
se	se	k3xPyFc4	se
nepřikládá	přikládat	k5eNaImIp3nS	přikládat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Ano-ne	Anoout	k5eAaPmIp3nS	Ano-nout
otázka	otázka	k1gFnSc1	otázka
má	mít	k5eAaImIp3nS	mít
klesající	klesající	k2eAgFnSc4d1	klesající
intonaci	intonace	k1gFnSc4	intonace
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
oznamovací	oznamovací	k2eAgFnSc1d1	oznamovací
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
tázací	tázací	k2eAgFnPc1d1	tázací
věty	věta	k1gFnPc1	věta
odlišeny	odlišit	k5eAaPmNgFnP	odlišit
intonačně	intonačně	k6eAd1	intonačně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Onko	Onko	k6eAd1	Onko
hän	hän	k?	hän
kotona	kotona	k1gFnSc1	kotona
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Missä	Missä	k?	Missä
on	on	k3xPp3gMnSc1	on
kukka	kukka	k6eAd1	kukka
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kytka	kytka	k1gFnSc1	kytka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kytka	kytka	k1gFnSc1	kytka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
odpovědět	odpovědět	k5eAaPmF	odpovědět
ano	ano	k9	ano
<g/>
/	/	kIx~	/
<g/>
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
přípony	přípona	k1gFnPc1	přípona
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
přípony	přípona	k1gFnSc2	přípona
-ko	-ko	k?	-ko
má	mít	k5eAaImIp3nS	mít
finština	finština	k1gFnSc1	finština
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgFnPc2d1	další
částicových	částicový	k2eAgFnPc2d1	částicová
přípon	přípona	k1gFnPc2	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
speciálně	speciálně	k6eAd1	speciálně
zmíněny	zmíněn	k2eAgFnPc1d1	zmíněna
přivlastňovací	přivlastňovací	k2eAgFnPc1d1	přivlastňovací
přípony	přípona	k1gFnPc1	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-kin	-kino	k1gNnPc2	-kino
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
české	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
také	také	k9	také
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
-kaan	-kaan	k1gInSc1	-kaan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
překládána	překládat	k5eAaImNgFnS	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ani	ani	k9	ani
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přípona	přípona	k1gFnSc1	přípona
-han	-han	k1gMnSc1	-han
/	/	kIx~	/
-hän	-hän	k1gMnSc1	-hän
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
významů	význam	k1gInPc2	význam
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
české	český	k2eAgFnSc3d1	Česká
příponě	přípona	k1gFnSc3	přípona
"	"	kIx"	"
<g/>
-pak	-pak	k6eAd1	-pak
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
této	tento	k3xDgFnSc6	tento
české	český	k2eAgFnSc6d1	Česká
částicové	částicový	k2eAgFnSc6d1	částicová
příponě	přípona	k1gFnSc6	přípona
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
finská	finský	k2eAgFnSc1d1	finská
-pa	-pa	k?	-pa
/	/	kIx~	/
-pä	-pä	k?	-pä
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
případě	případ	k1gInSc6	případ
přípona	přípona	k1gFnSc1	přípona
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
řečnickou	řečnický	k2eAgFnSc4d1	řečnická
otázku	otázka	k1gFnSc4	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
autokin	autokin	k1gInSc1	autokin
–	–	k?	–
"	"	kIx"	"
<g/>
i	i	k9	i
auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
minäkään	minäkään	k1gInSc1	minäkään
–	–	k?	–
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Missähän	Missähän	k1gMnSc1	Missähän
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Kdepak	kdepak	k6eAd1	kdepak
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Voinkohan	Voinkohan	k1gMnSc1	Voinkohan
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Mohu	moct	k5eAaImIp1nS	moct
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řečnická	řečnický	k2eAgFnSc1d1	řečnická
otázka	otázka	k1gFnSc1	otázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Nomina	Nomin	k2eAgNnPc4d1	Nomin
(	(	kIx(	(
<g/>
jména	jméno	k1gNnPc4	jméno
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Nomina	Nomino	k1gNnPc1	Nomino
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
a	a	k8xC	a
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členy	člen	k1gInPc7	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ugrických	ugrický	k2eAgInPc2d1	ugrický
jazyků	jazyk	k1gInPc2	jazyk
nepoužívají	používat	k5eNaImIp3nP	používat
u	u	k7c2	u
žádných	žádný	k3yNgFnPc2	žádný
nomen	nomna	k1gFnPc2	nomna
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rody	rod	k1gInPc4	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
se	se	k3xPyFc4	se
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
rody	rod	k1gInPc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
z	z	k7c2	z
korespondence	korespondence	k1gFnSc2	korespondence
tedy	tedy	k9	tedy
nejde	jít	k5eNaImIp3nS	jít
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pisatelem	pisatel	k1gMnSc7	pisatel
muž	muž	k1gMnSc1	muž
či	či	k8xC	či
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
osobní	osobní	k2eAgNnSc4d1	osobní
zájmeno	zájmeno	k1gNnSc4	zájmeno
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
hän	hän	k?	hän
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ona	onen	k3xDgFnSc1	onen
<g/>
"	"	kIx"	"
i	i	k8xC	i
"	"	kIx"	"
<g/>
ono	onen	k3xDgNnSc4	onen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skloňování	skloňování	k1gNnSc2	skloňování
===	===	k?	===
</s>
</p>
<p>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc1d1	společné
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
nomina	nomino	k1gNnPc4	nomino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
publikacích	publikace	k1gFnPc6	publikace
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
společný	společný	k2eAgInSc4d1	společný
vzor	vzor	k1gInSc4	vzor
<g/>
,	,	kIx,	,
jiné	jiné	k1gNnSc4	jiné
používají	používat	k5eAaImIp3nP	používat
systém	systém	k1gInSc4	systém
vzorů	vzor	k1gInPc2	vzor
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
vzorů	vzor	k1gInPc2	vzor
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
maa	maa	k?	maa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
talo	talo	k1gNnSc1	talo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bussi	busse	k1gFnSc4	busse
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
autobus	autobus	k1gInSc1	autobus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ovi	ovi	k?	ovi
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dveře	dveře	k1gFnPc4	dveře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kieli	kiel	k1gMnPc1	kiel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
käsi	käsi	k1gNnSc1	käsi
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ruka	ruka	k1gFnSc1	ruka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
huone	huonout	k5eAaPmIp3nS	huonout
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pokoj	pokoj	k1gInSc1	pokoj
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nainen	nainen	k1gInSc1	nainen
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
žena	žena	k1gFnSc1	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sairas	sairas	k1gInSc1	sairas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
salaisuus	salaisuus	k1gInSc1	salaisuus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vastaus	vastaus	k1gInSc1	vastaus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
avain	avain	k1gInSc1	avain
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
klíč	klíč	k1gInSc4	klíč
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
vzorů	vzor	k1gInPc2	vzor
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
méně	málo	k6eAd2	málo
užívaných	užívaný	k2eAgInPc2d1	užívaný
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kaunis	kaunis	k1gFnSc1	kaunis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
lapsi	lapse	k1gFnSc4	lapse
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
veli	veli	k6eAd1	veli
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bratr	bratr	k1gMnSc1	bratr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pády	Pád	k1gInPc4	Pád
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
užívá	užívat	k5eAaImIp3nS	užívat
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
pádů	pád	k1gInPc2	pád
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
užití	užití	k1gNnSc1	užití
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
překládá	překládat	k5eAaImIp3nS	překládat
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
pád	pád	k1gInSc1	pád
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
obvykle	obvykle	k6eAd1	obvykle
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgInPc6d1	různý
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
15	[number]	k4	15
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tvarově	tvarově	k6eAd1	tvarově
téměř	téměř	k6eAd1	téměř
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
)	)	kIx)	)
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
genitivem	genitiv	k1gInSc7	genitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
jak	jak	k6eAd1	jak
zadní	zadní	k2eAgInSc1d1	zadní
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
přední	přední	k2eAgFnSc1d1	přední
varianta	varianta	k1gFnSc1	varianta
pádové	pádový	k2eAgFnSc2d1	pádová
přípony	přípona	k1gFnSc2	přípona
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
-ssa	-ssa	k6eAd1	-ssa
a	a	k8xC	a
-ssä	-ssä	k?	-ssä
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
objekt	objekt	k1gInSc4	objekt
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
situace	situace	k1gFnSc2	situace
používá	používat	k5eAaImIp3nS	používat
nominativu	nominativ	k1gInSc2	nominativ
<g/>
,	,	kIx,	,
genitivu	genitiv	k1gInSc2	genitiv
<g/>
,	,	kIx,	,
akuzativu	akuzativ	k1gInSc2	akuzativ
a	a	k8xC	a
partitivu	partitiv	k1gInSc2	partitiv
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
lokálních	lokální	k2eAgInPc2d1	lokální
pádů	pád	k1gInPc2	pád
tvoří	tvořit	k5eAaImIp3nS	tvořit
šestice	šestice	k1gFnSc1	šestice
inessiv	inessit	k5eAaPmDgInS	inessit
<g/>
,	,	kIx,	,
elativ	elativ	k1gInSc1	elativ
<g/>
,	,	kIx,	,
illativ	illativ	k1gInSc1	illativ
<g/>
,	,	kIx,	,
adessiv	adessit	k5eAaPmDgInS	adessit
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc4	ablativ
a	a	k8xC	a
allativ	allativ	k1gInSc4	allativ
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
(	(	kIx(	(
<g/>
adessiv	adessit	k5eAaPmDgInS	adessit
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc4	ablativ
a	a	k8xC	a
allativ	allativ	k1gInSc4	allativ
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vnější	vnější	k2eAgFnPc1d1	vnější
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
otevřené	otevřený	k2eAgFnPc4d1	otevřená
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
vnitřní	vnitřní	k2eAgMnSc1d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
uzavřená	uzavřený	k2eAgNnPc4d1	uzavřené
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komitativ	Komitativ	k1gInSc1	Komitativ
a	a	k8xC	a
instruktiv	instruktiv	k1gInSc1	instruktiv
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
pouze	pouze	k6eAd1	pouze
tvary	tvar	k1gInPc1	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Komitativ	Komitativ	k1gInSc1	Komitativ
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
s	s	k7c7	s
posesivní	posesivní	k2eAgFnSc7d1	posesivní
příponou	přípona	k1gFnSc7	přípona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inessiv	Inessit	k5eAaPmDgInS	Inessit
<g/>
,	,	kIx,	,
elativ	elativ	k1gInSc1	elativ
<g/>
,	,	kIx,	,
illativ	illativ	k1gInSc1	illativ
<g/>
,	,	kIx,	,
adessiv	adessit	k5eAaPmDgInS	adessit
a	a	k8xC	a
abessiv	abessit	k5eAaPmDgInS	abessit
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
spojovat	spojovat	k5eAaImF	spojovat
i	i	k9	i
se	s	k7c7	s
slovesy	sloveso	k1gNnPc7	sloveso
například	například	k6eAd1	například
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
třetího	třetí	k4xOgInSc2	třetí
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Pádové	pádový	k2eAgFnPc4d1	pádová
přípony	přípona	k1gFnPc4	přípona
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vysledovat	vysledovat	k5eAaImF	vysledovat
i	i	k9	i
v	v	k7c6	v
postpozicích	postpozice	k1gFnPc6	postpozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
menen	menen	k1gInSc1	menen
syömästä	syömästä	k?	syömästä
–	–	k?	–
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
z	z	k7c2	z
jídla	jídlo	k1gNnSc2	jídlo
(	(	kIx(	(
<g/>
jezení	jezení	k1gNnSc2	jezení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k8xC	i
zbytky	zbytek	k1gInPc4	zbytek
prauralského	prauralský	k2eAgInSc2d1	prauralský
lokativu	lokativ	k1gInSc2	lokativ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
ulkona	ulkona	k1gFnSc1	ulkona
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
venku	venku	k6eAd1	venku
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
podobný	podobný	k2eAgInSc4d1	podobný
zbytek	zbytek	k1gInSc4	zbytek
dalšího	další	k2eAgInSc2d1	další
pádu	pád	k1gInSc2	pád
lativní	lativnět	k5eAaPmIp3nS	lativnět
koncovka	koncovka	k1gFnSc1	koncovka
slova	slovo	k1gNnSc2	slovo
ulos	ulosa	k1gFnPc2	ulosa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ven	ven	k6eAd1	ven
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
příslovce	příslovka	k1gFnSc3	příslovka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
pádem	pád	k1gInSc7	pád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
jen	jen	k9	jen
v	v	k7c6	v
náznacích	náznak	k1gInPc6	náznak
je	být	k5eAaImIp3nS	být
prolativ	prolatit	k5eAaPmDgInS	prolatit
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
české	český	k2eAgFnPc1d1	Česká
předložky	předložka	k1gFnPc1	předložka
"	"	kIx"	"
<g/>
skrze	skrze	k?	skrze
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
přes	přes	k7c4	přes
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
český	český	k2eAgInSc4d1	český
instrumentál	instrumentál	k1gInSc4	instrumentál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
-tse	-tse	k1gInSc2	-ts
a	a	k8xC	a
najdeme	najít	k5eAaPmIp1nP	najít
ho	on	k3xPp3gInSc4	on
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
ustálených	ustálený	k2eAgNnPc6d1	ustálené
spojeních	spojení	k1gNnPc6	spojení
jako	jako	k8xC	jako
postitse	postitse	k1gFnSc1	postitse
–	–	k?	–
"	"	kIx"	"
<g/>
poštou	pošta	k1gFnSc7	pošta
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
puhelimitse	puhelimitse	k1gFnSc1	puhelimitse
–	–	k?	–
"	"	kIx"	"
<g/>
telefonem	telefon	k1gInSc7	telefon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
komitativ	komitativ	k1gInSc4	komitativ
a	a	k8xC	a
instruktiv	instruktiv	k1gInSc4	instruktiv
i	i	k8xC	i
zbytky	zbytek	k1gInPc4	zbytek
prolativu	prolativ	k1gInSc2	prolativ
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
v	v	k7c6	v
množném	množný	k2eAgInSc6d1	množný
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnSc2	slovo
končící	končící	k2eAgFnPc1d1	končící
na	na	k7c6	na
-	-	kIx~	-
<g/>
i.	i.	k?	i.
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
pádů	pád	k1gInPc2	pád
není	být	k5eNaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
příklady	příklad	k1gInPc4	příklad
uvedené	uvedený	k2eAgInPc4d1	uvedený
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nP	pojit
například	například	k6eAd1	například
s	s	k7c7	s
illativem	illativ	k1gInSc7	illativ
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
s	s	k7c7	s
inessivem	inessiv	k1gInSc7	inessiv
nebo	nebo	k8xC	nebo
elativem	elativ	k1gInSc7	elativ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
prakticky	prakticky	k6eAd1	prakticky
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
pády	pád	k1gInPc1	pád
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
ustálených	ustálený	k2eAgNnPc6d1	ustálené
spojeních	spojení	k1gNnPc6	spojení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
adessiv	adessit	k5eAaPmDgInS	adessit
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
český	český	k2eAgInSc4d1	český
instrumentál	instrumentál	k1gInSc4	instrumentál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
rakastun	rakastuna	k1gFnPc2	rakastuna
saunaan	saunaany	k1gInPc2	saunaany
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Zamilovávám	zamilovávat	k5eAaImIp1nS	zamilovávat
se	se	k3xPyFc4	se
do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
käyn	käyn	k1gInSc4	käyn
saunassa	saunass	k1gMnSc4	saunass
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Navštěvuji	navštěvovat	k5eAaImIp1nS	navštěvovat
saunu	sauna	k1gFnSc4	sauna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
pidän	pidän	k1gInSc4	pidän
saunasta	saunast	k1gMnSc4	saunast
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
saunu	sauna	k1gFnSc4	sauna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
kirjoitan	kirjoitan	k1gInSc1	kirjoitan
kynällä	kynällä	k?	kynällä
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Píši	psát	k5eAaImIp1nS	psát
tužkou	tužka	k1gFnSc7	tužka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
starších	starý	k2eAgInPc6d2	starší
zdrojích	zdroj	k1gInPc6	zdroj
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
pády	pád	k1gInPc1	pád
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
upřesnění	upřesnění	k1gNnSc3	upřesnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
indefinitiv	indefinitiv	k1gInSc1	indefinitiv
=	=	kIx~	=
partitiv	partitiv	k1gInSc1	partitiv
</s>
</p>
<p>
<s>
objektiv	objektiv	k1gInSc1	objektiv
=	=	kIx~	=
akuzativ	akuzativ	k1gInSc1	akuzativ
</s>
</p>
<p>
<s>
mutativ	mutativ	k1gInSc1	mutativ
=	=	kIx~	=
translativ	translatit	k5eAaPmDgMnS	translatit
</s>
</p>
<p>
<s>
prosekutiv	prosekutit	k5eAaPmDgInS	prosekutit
=	=	kIx~	=
prolativ	prolatit	k5eAaPmDgMnS	prolatit
</s>
</p>
<p>
<s>
===	===	k?	===
Přivlastňovací	přivlastňovací	k2eAgFnSc2d1	přivlastňovací
přípony	přípona	k1gFnSc2	přípona
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
nemá	mít	k5eNaImIp3nS	mít
přivlastňovací	přivlastňovací	k2eAgNnPc4d1	přivlastňovací
zájmena	zájmeno	k1gNnPc4	zájmeno
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
využívá	využívat	k5eAaImIp3nS	využívat
genitivu	genitiv	k1gInSc6	genitiv
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
finštině	finština	k1gFnSc6	finština
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
přivlastňovaný	přivlastňovaný	k2eAgInSc1d1	přivlastňovaný
předmět	předmět	k1gInSc1	předmět
doplněn	doplnit	k5eAaPmNgInS	doplnit
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
příponou	přípona	k1gFnSc7	přípona
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
přípona	přípona	k1gFnSc1	přípona
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
samozřejmě	samozřejmě	k6eAd1	samozřejmě
přidávat	přidávat	k5eAaImF	přidávat
za	za	k7c4	za
pádové	pádový	k2eAgFnPc4d1	pádová
přípony	přípona	k1gFnPc4	přípona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zájmena	zájmeno	k1gNnPc1	zájmeno
i	i	k8xC	i
zcela	zcela	k6eAd1	zcela
vypouštět	vypouštět	k5eAaImF	vypouštět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
minun	minun	k1gNnSc1	minun
autoni	auton	k1gMnPc1	auton
–	–	k?	–
"	"	kIx"	"
<g/>
moje	můj	k3xOp1gNnSc1	můj
auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
vaimosi	vaimose	k1gFnSc3	vaimose
–	–	k?	–
"	"	kIx"	"
<g/>
tvá	tvůj	k3xOp2gFnSc1	tvůj
manželka	manželka	k1gFnSc1	manželka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
hänen	hänen	k1gInSc1	hänen
talostansa	talostansa	k1gFnSc1	talostansa
<g/>
/	/	kIx~	/
<g/>
hänen	hänen	k2eAgInSc1d1	hänen
talostaan	talostaan	k1gInSc1	talostaan
–	–	k?	–
"	"	kIx"	"
<g/>
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
(	(	kIx(	(
<g/>
jejího	její	k3xOp3gInSc2	její
<g/>
)	)	kIx)	)
domu	dům	k1gInSc2	dům
<g/>
"	"	kIx"	"
<g/>
Finština	finština	k1gFnSc1	finština
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k9	ani
přivlastňovací	přivlastňovací	k2eAgNnPc4d1	přivlastňovací
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
využívá	využívat	k5eAaImIp3nS	využívat
genetivu	genetiva	k1gFnSc4	genetiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
isän	isän	k1gMnSc1	isän
auto	auto	k1gNnSc1	auto
–	–	k?	–
"	"	kIx"	"
<g/>
otcovo	otcův	k2eAgNnSc1d1	otcovo
auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Slovesa	sloveso	k1gNnSc2	sloveso
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ohýbání	ohýbání	k1gNnSc1	ohýbání
sloves	sloveso	k1gNnPc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
vzory	vzor	k1gInPc4	vzor
u	u	k7c2	u
ohýbání	ohýbání	k1gNnSc2	ohýbání
substantiv	substantivum	k1gNnPc2	substantivum
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gInPc4	on
najít	najít	k5eAaPmF	najít
i	i	k9	i
u	u	k7c2	u
ohýbání	ohýbání	k1gNnPc2	ohýbání
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
v	v	k7c6	v
publikacích	publikace	k1gFnPc6	publikace
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
saa	saa	k?	saa
<g/>
/	/	kIx~	/
<g/>
da	da	k?	da
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dostat	dostat	k5eAaPmF	dostat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
puhu	puhu	k5eAaPmIp1nS	puhu
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mluvit	mluvit	k5eAaImF	mluvit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
podtřídy	podtřída	k1gFnPc4	podtřída
</s>
</p>
<p>
<s>
podtřída	podtřída	k1gFnSc1	podtřída
3	[number]	k4	3
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vzory	vzor	k1gInPc4	vzor
</s>
</p>
<p>
<s>
vzor	vzor	k1gInSc1	vzor
tul	tula	k1gFnPc2	tula
<g/>
/	/	kIx~	/
<g/>
la	la	k1gNnSc2	la
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
přijít	přijít	k5eAaPmF	přijít
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vzor	vzor	k1gInSc1	vzor
sur	sur	k?	sur
<g/>
/	/	kIx~	/
<g/>
ra	ra	k0	ra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
smutnit	smutnit	k5eAaImF	smutnit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vzor	vzor	k1gInSc1	vzor
pan	pan	k1gMnSc1	pan
<g/>
/	/	kIx~	/
<g/>
na	na	k7c4	na
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
položit	položit	k5eAaPmF	položit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podtřída	podtřída	k1gFnSc1	podtřída
3	[number]	k4	3
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
nous	nous	k1gInSc1	nous
<g/>
/	/	kIx~	/
<g/>
ta	ta	k0	ta
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
podtřídy	podtřída	k1gFnPc4	podtřída
</s>
</p>
<p>
<s>
podtřída	podtřída	k1gFnSc1	podtřída
4	[number]	k4	4
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
halu	hala	k1gFnSc4	hala
<g/>
/	/	kIx~	/
<g/>
ta	ten	k3xDgFnSc1	ten
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chtít	chtít	k5eAaImF	chtít
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podtřída	podtřída	k1gFnSc1	podtřída
4	[number]	k4	4
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
tarvi	tarev	k1gFnSc6	tarev
<g/>
/	/	kIx~	/
<g/>
ta	ten	k3xDgFnSc1	ten
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
potřebovat	potřebovat	k5eAaImF	potřebovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podtřída	podtřída	k1gFnSc1	podtřída
4	[number]	k4	4
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
vanhe	vanhe	k1gNnPc7	vanhe
<g/>
/	/	kIx~	/
<g/>
ta	ten	k3xDgFnSc1	ten
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stárnout	stárnout	k5eAaImF	stárnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
tříd	třída	k1gFnPc2	třída
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
několika	několik	k4yIc7	několik
nepravidelnými	pravidelný	k2eNgNnPc7d1	nepravidelné
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
nähdä	nähdä	k?	nähdä
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vidět	vidět	k5eAaImF	vidět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
olla	olla	k6eAd1	olla
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Časy	čas	k1gInPc4	čas
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
slovesné	slovesný	k2eAgInPc4d1	slovesný
časy	čas	k1gInPc4	čas
</s>
</p>
<p>
<s>
předminulý	předminulý	k2eAgInSc1d1	předminulý
(	(	kIx(	(
<g/>
plusquamperfektum	plusquamperfektum	k1gNnSc1	plusquamperfektum
<g/>
)	)	kIx)	)
–	–	k?	–
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
slovesné	slovesný	k2eAgFnSc2d1	slovesná
spony	spona	k1gFnSc2	spona
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
a	a	k8xC	a
činného	činný	k2eAgNnSc2d1	činné
příčestí	příčestí	k1gNnSc2	příčestí
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
minulý	minulý	k2eAgInSc1d1	minulý
(	(	kIx(	(
<g/>
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
vlastně	vlastně	k9	vlastně
préteritem	préteritum	k1gNnSc7	préteritum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
předpřítomný	předpřítomný	k2eAgMnSc1d1	předpřítomný
(	(	kIx(	(
<g/>
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
)	)	kIx)	)
–	–	k?	–
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
slovesné	slovesný	k2eAgFnSc2d1	slovesná
spony	spona	k1gFnSc2	spona
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
činného	činný	k2eAgNnSc2d1	činné
příčestí	příčestí	k1gNnSc2	příčestí
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
přítomný	přítomný	k2eAgInSc1d1	přítomný
(	(	kIx(	(
<g/>
prézens	prézens	k1gInSc1	prézens
<g/>
)	)	kIx)	)
<g/>
Finština	finština	k1gFnSc1	finština
nemá	mít	k5eNaImIp3nS	mít
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
buď	buď	k8xC	buď
opisnými	opisný	k2eAgInPc7d1	opisný
tvary	tvar	k1gInPc7	tvar
nebo	nebo	k8xC	nebo
přítomným	přítomný	k2eAgInSc7d1	přítomný
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
pádových	pádový	k2eAgFnPc2d1	pádová
koncovek	koncovka	k1gFnPc2	koncovka
přidávaných	přidávaný	k2eAgFnPc2d1	přidávaná
k	k	k7c3	k
odvozeninám	odvozenina	k1gFnPc3	odvozenina
ze	z	k7c2	z
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
určitá	určitý	k2eAgFnSc1d1	určitá
průběhovost	průběhovost	k1gFnSc1	průběhovost
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
olin	olin	k1gInSc1	olin
mennyt	mennyt	k1gInSc1	mennyt
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
stalo	stát	k5eAaPmAgNnS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
menin	menin	k1gInSc1	menin
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
olen	olen	k1gInSc1	olen
mennyt	mennyt	k1gInSc1	mennyt
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
chvílí	chvíle	k1gFnSc7	chvíle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
menen	menen	k1gInSc1	menen
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
aion	aion	k1gInSc1	aion
mennä	mennä	k?	mennä
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Půjdu	jít	k5eAaImIp1nS	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Plánuju	plánovat	k5eAaImIp1nS	plánovat
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
olen	olen	k1gInSc1	olen
menossa	menossa	k1gFnSc1	menossa
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
(	(	kIx(	(
<g/>
právě	právě	k9	právě
teď	teď	k6eAd1	teď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
chození	chození	k1gNnSc6	chození
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Záporné	záporný	k2eAgFnPc1d1	záporná
věty	věta	k1gFnPc1	věta
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pomocí	pomocí	k7c2	pomocí
záporného	záporný	k2eAgNnSc2d1	záporné
slovesa	sloveso	k1gNnSc2	sloveso
</s>
</p>
<p>
<s>
===	===	k?	===
Záporné	záporný	k2eAgNnSc1d1	záporné
sloveso	sloveso	k1gNnSc1	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
záporka	záporka	k1gFnSc1	záporka
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
"	"	kIx"	"
<g/>
ne-	ne-	k?	ne-
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
záporného	záporný	k2eAgNnSc2d1	záporné
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
v	v	k7c6	v
první	první	k4xOgFnSc6	první
nebo	nebo	k8xC	nebo
druhé	druhý	k4xOgFnSc3	druhý
osobě	osoba	k1gFnSc3	osoba
bez	bez	k7c2	bez
přípony	přípona	k1gFnSc2	přípona
osobu	osoba	k1gFnSc4	osoba
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
puhu	puhus	k1gInSc2	puhus
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nemluvím	mluvit	k5eNaImIp1nS	mluvit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Hän	Hän	k?	Hän
ei	ei	k?	ei
puhu	puhus	k1gInSc2	puhus
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Etkö	Etkö	k?	Etkö
sinä	sinä	k?	sinä
puhu	puhus	k1gInSc2	puhus
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Nemluvíš	mluvit	k5eNaImIp2nS	mluvit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
puhun	puhun	k1gInSc1	puhun
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvím	mluvit	k5eAaImIp1nS	mluvit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Záporné	záporný	k2eAgFnPc1d1	záporná
věty	věta	k1gFnPc1	věta
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
časech	čas	k1gInPc6	čas
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pomocí	pomocí	k7c2	pomocí
záporného	záporný	k2eAgNnSc2d1	záporné
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
slovesné	slovesný	k2eAgFnSc2d1	slovesná
spony	spona	k1gFnSc2	spona
a	a	k8xC	a
činného	činný	k2eAgNnSc2d1	činné
minulého	minulý	k2eAgNnSc2d1	Minulé
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
.	.	kIx.	.
</s>
<s>
Slovesná	slovesný	k2eAgFnSc1d1	slovesná
spona	spona	k1gFnSc1	spona
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
slovesa	sloveso	k1gNnSc2	sloveso
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
činným	činný	k2eAgNnSc7d1	činné
minulým	minulý	k2eAgNnSc7d1	Minulé
příčestím	příčestí	k1gNnSc7	příčestí
tohoto	tento	k3xDgNnSc2	tento
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
mene	mene	k1gInSc1	mene
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nejdu	jít	k5eNaImIp1nS	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
mennyt	mennyt	k1gInSc1	mennyt
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nešel	jít	k5eNaImAgInS	jít
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
ole	ole	k?	ole
mennyt	mennyt	k1gInSc1	mennyt
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nešel	jít	k5eNaImAgInS	jít
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
chvílí	chvíle	k1gFnSc7	chvíle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
ollut	ollut	k2eAgInSc4d1	ollut
mennyt	mennyt	k1gInSc4	mennyt
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nešel	jít	k5eNaImAgInS	jít
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
stalo	stát	k5eAaPmAgNnS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Minä	Minä	k?	Minä
en	en	k?	en
ollut	ollut	k1gMnSc1	ollut
ollut	ollut	k1gMnSc1	ollut
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Nebyl	být	k5eNaImAgInS	být
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
stalo	stát	k5eAaPmAgNnS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesné	slovesný	k2eAgInPc4d1	slovesný
způsoby	způsob	k1gInPc4	způsob
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
používá	používat	k5eAaImIp3nS	používat
slovesa	sloveso	k1gNnPc4	sloveso
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
slovesných	slovesný	k2eAgInPc6d1	slovesný
způsobech	způsob	k1gInPc6	způsob
</s>
</p>
<p>
<s>
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc1	indikativ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
(	(	kIx(	(
<g/>
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
potenciálPrvní	potenciálPrvnit	k5eAaPmIp3nP	potenciálPrvnit
tři	tři	k4xCgMnPc1	tři
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálem	potenciál	k1gInSc7	potenciál
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
možnost	možnost	k1gFnSc1	možnost
nebo	nebo	k8xC	nebo
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
za	za	k7c4	za
pátý	pátý	k4xOgInSc4	pátý
způsob	způsob	k1gInSc4	způsob
považuje	považovat	k5eAaImIp3nS	považovat
eventiv	eventiv	k1gInSc1	eventiv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
eposu	epos	k1gInSc6	epos
Kalevala	Kalevala	k1gFnSc2	Kalevala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sinä	Sinä	k?	Sinä
puhut	puhut	k1gInSc1	puhut
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvíš	mluvit	k5eAaImIp2nS	mluvit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Puhu	Puhu	k6eAd1	Puhu
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Mluv	mluv	k1gInSc1	mluv
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Älä	Älä	k?	Älä
puhu	puhu	k5eAaPmIp1nS	puhu
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Nemluv	nemluva	k1gFnPc2	nemluva
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Puhukaa	Puhukaa	k6eAd1	Puhukaa
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvte	mluvit	k5eAaImRp2nP	mluvit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Älkää	Älkää	k?	Älkää
puhuko	puhuko	k1gNnSc1	puhuko
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Nemluvte	mluvit	k5eNaImRp2nP	mluvit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Puhuisit	Puhuisit	k1gInSc1	Puhuisit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvil	mluvit	k5eAaImAgInS	mluvit
bys	by	kYmCp2nS	by
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Olisit	Olisit	k1gInSc1	Olisit
puhunut	puhunut	k2eAgInSc1d1	puhunut
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvil	mluvit	k5eAaImAgInS	mluvit
bys	by	kYmCp2nS	by
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Puhuttaisiin	Puhuttaisiin	k2eAgMnSc1d1	Puhuttaisiin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Mluvilo	mluvit	k5eAaImAgNnS	mluvit
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesný	slovesný	k2eAgInSc1d1	slovesný
rod	rod	k1gInSc1	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
činný	činný	k2eAgMnSc1d1	činný
(	(	kIx(	(
<g/>
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
trpný	trpný	k2eAgInSc4d1	trpný
(	(	kIx(	(
<g/>
pasivní	pasivní	k2eAgInSc4d1	pasivní
<g/>
)	)	kIx)	)
slovesný	slovesný	k2eAgInSc4d1	slovesný
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
finštině	finština	k1gFnSc6	finština
se	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
někdy	někdy	k6eAd1	někdy
přenášejí	přenášet	k5eAaImIp3nP	přenášet
i	i	k9	i
do	do	k7c2	do
činného	činný	k2eAgInSc2d1	činný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
infinitivu	infinitiv	k1gInSc2	infinitiv
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
-Kaan	-Kaan	k1gMnSc1	-Kaan
(	(	kIx(	(
<g/>
K	K	kA	K
představuje	představovat	k5eAaImIp3nS	představovat
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
a	a	k8xC	a
koncovek	koncovka	k1gFnPc2	koncovka
-tiin	-tiin	k2eAgMnSc1d1	-tiin
nebo	nebo	k8xC	nebo
-ttiin	-ttiin	k2eAgMnSc1d1	-ttiin
v	v	k7c6	v
případě	případ	k1gInSc6	případ
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
existence	existence	k1gFnSc1	existence
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
tvaru	tvar	k1gInSc2	tvar
pro	pro	k7c4	pro
podmiňovací	podmiňovací	k2eAgInSc4d1	podmiňovací
způsob	způsob	k1gInSc4	způsob
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Přítomný	přítomný	k2eAgInSc4d1	přítomný
čas	čas	k1gInSc4	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
slabému	slabý	k2eAgNnSc3d1	slabé
samohláskovému	samohláskový	k2eAgNnSc3d1	samohláskové
nebo	nebo	k8xC	nebo
souhláskovému	souhláskový	k2eAgMnSc3d1	souhláskový
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
<g/>
)	)	kIx)	)
kmenu	kmen	k1gInSc2	kmen
připojí	připojit	k5eAaPmIp3nS	připojit
přípona	přípona	k1gFnSc1	přípona
-taan	-taan	k1gMnSc1	-taan
<g/>
/	/	kIx~	/
<g/>
-tään	-tään	k1gMnSc1	-tään
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kmen	kmen	k1gInSc1	kmen
slovesa	sloveso	k1gNnSc2	sloveso
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
-a	-a	k?	-a
<g/>
/	/	kIx~	/
<g/>
-ä	-ä	k?	-ä
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
změně	změna	k1gFnSc3	změna
na	na	k7c4	na
-	-	kIx~	-
<g/>
e.	e.	k?	e.
Slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
infinitiv	infinitiv	k1gInSc1	infinitiv
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
-da	-da	k?	-da
<g/>
/	/	kIx~	/
<g/>
-dä	-dä	k?	-dä
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
příponu	přípona	k1gFnSc4	přípona
daan	daan	k1gNnSc1	daan
<g/>
/	/	kIx~	/
<g/>
dään	dään	k1gNnSc1	dään
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
infinitivní	infinitivní	k2eAgFnSc4d1	infinitivní
koncovku	koncovka	k1gFnSc4	koncovka
-la	-la	k?	-la
<g/>
/	/	kIx~	/
<g/>
-lä	lä	k?	-lä
<g/>
,	,	kIx,	,
-na	-na	k?	-na
<g/>
/	/	kIx~	/
<g/>
-nä	-nä	k?	-nä
nebo	nebo	k8xC	nebo
-ra	-ra	k?	-ra
<g/>
/	/	kIx~	/
<g/>
-rä	-rä	k?	-rä
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
příponu	přípona	k1gFnSc4	přípona
pasivu	pasivum	k1gNnSc3	pasivum
-laan	-laany	k1gInPc2	-laany
<g/>
/	/	kIx~	/
<g/>
-lään	-lään	k1gMnSc1	-lään
<g/>
,	,	kIx,	,
-naan	-naan	k1gMnSc1	-naan
<g/>
/	/	kIx~	/
<g/>
-nään	-nään	k1gMnSc1	-nään
nebo	nebo	k8xC	nebo
-raan	-raan	k1gMnSc1	-raan
<g/>
/	/	kIx~	/
<g/>
-rään	-rään	k1gMnSc1	-rään
<g/>
.	.	kIx.	.
</s>
<s>
Verba	verbum	k1gNnPc1	verbum
kontrakta	kontrakto	k1gNnPc1	kontrakto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
infinitivu	infinitiv	k1gInSc6	infinitiv
koncovku	koncovka	k1gFnSc4	koncovka
-ta	-ta	k?	-ta
<g/>
,	,	kIx,	,
připojují	připojovat	k5eAaImIp3nP	připojovat
v	v	k7c6	v
pasivu	pasivum	k1gNnSc6	pasivum
pouze	pouze	k6eAd1	pouze
příponu	přípona	k1gFnSc4	přípona
-an	-an	k?	-an
<g/>
/	/	kIx~	/
<g/>
-än	-än	k?	-än
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
infinitivu	infinitiv	k1gInSc3	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Záporný	záporný	k2eAgInSc1d1	záporný
tvar	tvar	k1gInSc1	tvar
pasivu	pasivum	k1gNnSc3	pasivum
se	se	k3xPyFc4	se
utvoří	utvořit	k5eAaPmIp3nS	utvořit
s	s	k7c7	s
kladného	kladný	k2eAgInSc2d1	kladný
pasivu	pasivum	k1gNnSc3	pasivum
odtržením	odtržení	k1gNnSc7	odtržení
koncového	koncový	k2eAgMnSc2d1	koncový
-an	-an	k?	-an
<g/>
/	/	kIx~	/
<g/>
-än	-än	k?	-än
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
záporného	záporný	k2eAgNnSc2d1	záporné
slovesa	sloveso	k1gNnSc2	sloveso
ei	ei	k?	ei
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
jsou	být	k5eAaImIp3nP	být
tvary	tvar	k1gInPc4	tvar
některých	některý	k3yIgNnPc2	některý
sloves	sloveso	k1gNnPc2	sloveso
shodné	shodný	k2eAgInPc4d1	shodný
s	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Minulé	minulý	k2eAgNnSc1d1	Minulé
pasivum	pasivum	k1gNnSc1	pasivum
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
pasivum	pasivum	k1gNnSc4	pasivum
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
samohláskovému	samohláskový	k2eAgMnSc3d1	samohláskový
nebo	nebo	k8xC	nebo
souhláskovému	souhláskový	k2eAgMnSc3d1	souhláskový
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
<g/>
)	)	kIx)	)
kmenu	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
samohláskovému	samohláskový	k2eAgInSc3d1	samohláskový
kmenu	kmen	k1gInSc3	kmen
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
samohláskou	samohláska	k1gFnSc7	samohláska
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přípona	přípona	k1gFnSc1	přípona
-ttiin	-ttiin	k2eAgInSc4d1	-ttiin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
přípona	přípona	k1gFnSc1	přípona
-tiin	-tiina	k1gFnPc2	-tiina
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přítomného	přítomný	k2eAgNnSc2d1	přítomné
pasiva	pasivum	k1gNnSc2	pasivum
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
samohlásky	samohláska	k1gFnSc2	samohláska
-a	-a	k?	-a
<g/>
/	/	kIx~	/
<g/>
-ä	-ä	k?	-ä
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kmenu	kmen	k1gInSc2	kmen
na	na	k7c4	na
-	-	kIx~	-
<g/>
e.	e.	k?	e.
Záporný	záporný	k2eAgInSc4d1	záporný
tvar	tvar	k1gInSc4	tvar
minulého	minulý	k2eAgInSc2d1	minulý
pasivu	pasivum	k1gNnSc3	pasivum
dostaneme	dostat	k5eAaPmIp1nP	dostat
odtržením	odtržení	k1gNnSc7	odtržení
-iin	-iin	k1gNnSc4	-iin
od	od	k7c2	od
kladného	kladný	k2eAgNnSc2d1	kladné
minulého	minulý	k2eAgNnSc2d1	Minulé
pasiva	pasivum	k1gNnSc2	pasivum
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
-u	-u	k?	-u
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
y.	y.	k?	y.
Přípony	přípona	k1gFnSc2	přípona
záporného	záporný	k2eAgNnSc2d1	záporné
minulého	minulý	k2eAgNnSc2d1	Minulé
pasiva	pasivum	k1gNnSc2	pasivum
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
-ttu	-ttu	k6eAd1	-ttu
<g/>
/	/	kIx~	/
<g/>
-tty	-tty	k1gInPc1	-tty
a	a	k8xC	a
-tu	-tu	k?	-tu
<g/>
/	/	kIx~	/
<g/>
-ty	-ty	k?	-ty
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
minulého	minulý	k2eAgNnSc2d1	Minulé
pasivního	pasivní	k2eAgNnSc2d1	pasivní
participia	participium	k1gNnSc2	participium
<g/>
.	.	kIx.	.
</s>
<s>
Verba	verbum	k1gNnPc1	verbum
kontrakta	kontrakto	k1gNnSc2	kontrakto
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
přítomného	přítomný	k2eAgNnSc2d1	přítomné
pasiva	pasivum	k1gNnSc2	pasivum
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
souhláskovému	souhláskový	k2eAgInSc3d1	souhláskový
kmeni	kmen	k1gInSc3	kmen
připojíme	připojit	k5eAaPmIp1nP	připojit
koncovku	koncovka	k1gFnSc4	koncovka
tiin	tiin	k1gNnSc1	tiin
<g/>
/	/	kIx~	/
<g/>
tiin	tiin	k1gNnSc1	tiin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Předpřítomný	Předpřítomný	k2eAgInSc1d1	Předpřítomný
a	a	k8xC	a
předminulý	předminulý	k2eAgInSc1d1	předminulý
čas	čas	k1gInSc1	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
pomocným	pomocný	k2eAgNnSc7d1	pomocné
slovesem	sloveso	k1gNnSc7	sloveso
olla	oll	k1gInSc2	oll
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
minulého	minulý	k2eAgNnSc2d1	Minulé
pasivního	pasivní	k2eAgNnSc2d1	pasivní
participia	participium	k1gNnSc2	participium
(	(	kIx(	(
<g/>
přípony	přípona	k1gFnPc4	přípona
-ttu	-ttu	k6eAd1	-ttu
<g/>
/	/	kIx~	/
<g/>
-tty	-tty	k1gInPc1	-tty
a	a	k8xC	a
-tu	-tu	k?	-tu
<g/>
/	/	kIx~	/
<g/>
-ty	-ty	k?	-ty
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
olla	oll	k1gInSc2	oll
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
předpřítomném	předpřítomný	k2eAgNnSc6d1	předpřítomný
pasivu	pasivum	k1gNnSc6	pasivum
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
on	on	k3xPp3gMnSc1	on
<g/>
/	/	kIx~	/
<g/>
ei	ei	k?	ei
ole	ole	k?	ole
a	a	k8xC	a
v	v	k7c6	v
předminulém	předminulý	k2eAgNnSc6d1	předminulé
pasivu	pasivum	k1gNnSc6	pasivum
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
oli	oli	k?	oli
<g/>
/	/	kIx~	/
<g/>
ei	ei	k?	ei
ollut	ollut	k1gInSc1	ollut
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
olla	oll	k1gInSc2	oll
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
tvarech	tvar	k1gInPc6	tvar
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpřítomný	Předpřítomný	k2eAgInSc1d1	Předpřítomný
čas	čas	k1gInSc1	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
on	on	k3xPp3gMnSc1	on
annettu	annetto	k1gNnSc3	annetto
-	-	kIx~	-
ei	ei	k?	ei
ole	ole	k?	ole
annettu	annett	k1gInSc2	annett
</s>
</p>
<p>
<s>
on	on	k3xPp3gMnSc1	on
tehty	teht	k1gInPc1	teht
-	-	kIx~	-
ei	ei	k?	ei
ole	ole	k?	ole
tehty	tehta	k1gFnSc2	tehta
</s>
</p>
<p>
<s>
on	on	k3xPp3gMnSc1	on
menty	menta	k1gFnPc1	menta
-	-	kIx~	-
ei	ei	k?	ei
ole	ole	k?	ole
mentyPředminulý	mentyPředminulý	k2eAgInSc4d1	mentyPředminulý
čas	čas	k1gInSc4	čas
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
oli	oli	k?	oli
annettu	annett	k1gInSc2	annett
-	-	kIx~	-
ei	ei	k?	ei
ollut	ollut	k1gMnSc1	ollut
annettu	annett	k1gInSc2	annett
</s>
</p>
<p>
<s>
oli	oli	k?	oli
tehty	tehta	k1gFnPc1	tehta
-	-	kIx~	-
ei	ei	k?	ei
ollut	ollut	k1gInSc1	ollut
tehty	tehta	k1gFnSc2	tehta
</s>
</p>
<p>
<s>
oli	oli	k?	oli
menty	menta	k1gFnPc1	menta
-	-	kIx~	-
ei	ei	k?	ei
ollut	ollut	k1gInSc1	ollut
menty	menta	k1gFnSc2	menta
</s>
</p>
<p>
<s>
===	===	k?	===
Posesivní	posesivní	k2eAgFnSc1d1	posesivní
konstrukce	konstrukce	k1gFnSc1	konstrukce
===	===	k?	===
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
nemá	mít	k5eNaImIp3nS	mít
sloveso	sloveso	k1gNnSc4	sloveso
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
opisem	opis	k1gInSc7	opis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
asi	asi	k9	asi
nejlépe	dobře	k6eAd3	dobře
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
u	u	k7c2	u
někoho	někdo	k3yInSc2	někdo
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
na	na	k7c6	na
někom	někdo	k3yInSc6	někdo
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minulla	Minulla	k6eAd1	Minulla
on	on	k3xPp3gMnSc1	on
auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
–	–	k?	–
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
U	u	k7c2	u
mě	já	k3xPp1nSc2	já
je	být	k5eAaImIp3nS	být
auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Neohebné	ohebný	k2eNgInPc1d1	neohebný
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Příslovce	příslovce	k1gNnSc2	příslovce
===	===	k?	===
</s>
</p>
<p>
<s>
Příslovce	příslovce	k1gNnPc1	příslovce
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocí	pomocí	k7c2	pomocí
přípon	přípona	k1gFnPc2	přípona
z	z	k7c2	z
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postpozice	postpozice	k1gFnSc2	postpozice
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
finština	finština	k1gFnSc1	finština
hojně	hojně	k6eAd1	hojně
využívá	využívat	k5eAaImIp3nS	využívat
pádových	pádový	k2eAgFnPc2d1	pádová
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
mnoho	mnoho	k4c1	mnoho
předložek	předložka	k1gFnPc2	předložka
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
fenoménem	fenomén	k1gInSc7	fenomén
jsou	být	k5eAaImIp3nP	být
postpozice	postpozice	k1gFnPc4	postpozice
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
záložky	záložka	k1gFnPc4	záložka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
vždy	vždy	k6eAd1	vždy
za	za	k7c7	za
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Pojí	pojíst	k5eAaPmIp3nS	pojíst
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
s	s	k7c7	s
genetivem	genetivum	k1gNnSc7	genetivum
nebo	nebo	k8xC	nebo
partitivem	partitivum	k1gNnSc7	partitivum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
talon	talon	k1gInSc1	talon
edessä	edessä	k?	edessä
–	–	k?	–
"	"	kIx"	"
<g/>
před	před	k7c7	před
domem	dům	k1gInSc7	dům
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
loppuun	loppuun	k1gMnSc1	loppuun
asti	ast	k1gFnSc2	ast
–	–	k?	–
"	"	kIx"	"
<g/>
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
finština	finština	k1gFnSc1	finština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
finština	finština	k1gFnSc1	finština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Finsko-český	finsko-český	k2eAgInSc1d1	finsko-český
slovník	slovník	k1gInSc1	slovník
</s>
</p>
<p>
<s>
Finsko-český	finsko-český	k2eAgInSc1d1	finsko-český
slovník	slovník	k1gInSc1	slovník
Lingea	Lingea	k1gFnSc1	Lingea
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
)	)	kIx)	)
Příklad	příklad	k1gInSc1	příklad
vyčasování	vyčasování	k1gNnSc2	vyčasování
slovesa	sloveso	k1gNnSc2	sloveso
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Finština	finština	k1gFnSc1	finština
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Finnish	Finnish	k1gInSc1	Finnish
101	[number]	k4	101
Learn	Learno	k1gNnPc2	Learno
Finnish	Finnisha	k1gFnPc2	Finnisha
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
)	)	kIx)	)
Pikantnosti	pikantnost	k1gFnSc3	pikantnost
finštiny	finština	k1gFnSc2	finština
při	při	k7c6	při
přejímání	přejímání	k1gNnSc6	přejímání
slov	slovo	k1gNnPc2	slovo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
O	o	k7c6	o
Š	Š	kA	Š
a	a	k8xC	a
Ž	Ž	kA	Ž
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
)	)	kIx)	)
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
přehled	přehled	k1gInSc1	přehled
vzorů	vzor	k1gInPc2	vzor
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
na	na	k7c6	na
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
)	)	kIx)	)
<g/>
Kaannos	Kaannos	k1gInSc1	Kaannos
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Free	Free	k1gFnSc1	Free
dictionary	dictionara	k1gFnSc2	dictionara
</s>
</p>
