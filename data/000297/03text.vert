<s>
Victor-Marie	Victor-Marie	k1gFnSc1	Victor-Marie
Hugo	Hugo	k1gMnSc1	Hugo
[	[	kIx(	[
<g/>
ygo	ygo	k?	ygo
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
Besançon	Besançon	k1gInSc1	Besançon
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgMnSc1d1	vrcholný
představitel	představitel	k1gMnSc1	představitel
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
francouzských	francouzský	k2eAgMnPc2d1	francouzský
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
francouzský	francouzský	k2eAgMnSc1d1	francouzský
zastánce	zastánce	k1gMnSc1	zastánce
komunismu	komunismus	k1gInSc2	komunismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Besançonu	Besançon	k1gInSc6	Besançon
<g/>
,	,	kIx,	,
když	když	k8xS	když
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
praví	pravit	k5eAaBmIp3nS	pravit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
své	svůj	k3xOyFgFnSc6	svůj
autobiografické	autobiografický	k2eAgFnSc6d1	autobiografická
básni	báseň	k1gFnSc6	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
pobožnou	pobožný	k2eAgFnSc7d1	pobožná
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přívrženkyní	přívrženkyně	k1gFnSc7	přívrženkyně
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
a	a	k8xC	a
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
royalistka	royalistka	k1gFnSc1	royalistka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
stoupencem	stoupenec	k1gMnSc7	stoupenec
Napoleona	Napoleon	k1gMnSc2	Napoleon
(	(	kIx(	(
<g/>
bonapartista	bonapartista	k1gMnSc1	bonapartista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třetím	třetí	k4xOgMnSc7	třetí
synem	syn	k1gMnSc7	syn
Josepha	Joseph	k1gMnSc2	Joseph
Léopolda	Léopold	k1gMnSc2	Léopold
Sigisberta	Sigisbert	k1gMnSc2	Sigisbert
Huga	Hugo	k1gMnSc2	Hugo
a	a	k8xC	a
Sophie	Sophie	k1gFnSc2	Sophie
Trébuchet	Trébucheta	k1gFnPc2	Trébucheta
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
v	v	k7c6	v
Besançonu	Besançon	k1gInSc6	Besançon
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
byli	být	k5eAaImAgMnP	být
Abel	Abel	k1gMnSc1	Abel
Joseph	Joseph	k1gMnSc1	Joseph
Hugo	Hugo	k1gMnSc1	Hugo
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eugéne	Eugén	k1gInSc5	Eugén
Hugo	Hugo	k1gMnSc1	Hugo
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc1	dětství
Viktora	Viktor	k1gMnSc2	Viktor
Huga	Hugo	k1gMnSc2	Hugo
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
významnými	významný	k2eAgFnPc7d1	významná
událostmi	událost	k1gFnPc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Victoru	Victor	k1gMnSc3	Victor
Hugovi	Hugo	k1gMnSc3	Hugo
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
Bourbounská	Bourbounský	k2eAgFnSc1d1	Bourbounský
Monarchie	monarchie	k1gFnSc1	monarchie
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
před	před	k7c7	před
jeho	jeho	k3xOp3gFnPc7	jeho
třináctinami	třináctina	k1gFnPc7	třináctina
<g/>
.	.	kIx.	.
</s>
<s>
Opačné	opačný	k2eAgInPc4d1	opačný
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měli	mít	k5eAaImAgMnP	mít
Hugovi	Hugův	k2eAgMnPc1d1	Hugův
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
zobrazovaly	zobrazovat	k5eAaImAgFnP	zobrazovat
ideologické	ideologický	k2eAgFnPc4d1	ideologická
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přely	přít	k5eAaImAgFnP	přít
o	o	k7c4	o
nadvládu	nadvláda	k1gFnSc4	nadvláda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
celý	celý	k2eAgInSc4d1	celý
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
:	:	kIx,	:
Hugův	Hugův	k2eAgMnSc1d1	Hugův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
vysoce	vysoce	k6eAd1	vysoce
postavený	postavený	k2eAgMnSc1d1	postavený
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
atheistický	atheistický	k2eAgMnSc1d1	atheistický
republikán	republikán	k1gMnSc1	republikán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
považoval	považovat	k5eAaImAgMnS	považovat
Napoleona	Napoleon	k1gMnSc4	Napoleon
za	za	k7c4	za
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
extrémní	extrémní	k2eAgFnSc1d1	extrémní
katolická	katolický	k2eAgFnSc1d1	katolická
royalistka	royalistka	k1gFnSc1	royalistka
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
domněnek	domněnka	k1gFnPc2	domněnka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
milenkou	milenka	k1gFnSc7	milenka
Generála	generál	k1gMnSc2	generál
Victora	Victor	k1gMnSc2	Victor
F.	F.	kA	F.
de	de	k?	de
Lahorie	Lahorie	k1gFnSc2	Lahorie
<g/>
,	,	kIx,	,
popraveného	popravený	k2eAgInSc2d1	popravený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
za	za	k7c4	za
spiknutí	spiknutí	k1gNnSc4	spiknutí
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
Hugův	Hugův	k2eAgMnSc1d1	Hugův
otec	otec	k1gMnSc1	otec
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
se	se	k3xPyFc4	se
při	při	k7c6	při
cestování	cestování	k1gNnSc6	cestování
hodně	hodně	k6eAd1	hodně
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
viděl	vidět	k5eAaImAgMnS	vidět
rozlehlé	rozlehlý	k2eAgInPc4d1	rozlehlý
Alpské	alpský	k2eAgInPc4d1	alpský
vrcholky	vrcholek	k1gInPc4	vrcholek
<g/>
,	,	kIx,	,
velkolepě	velkolepě	k6eAd1	velkolepě
modré	modrý	k2eAgNnSc1d1	modré
středozemní	středozemní	k2eAgNnSc1d1	středozemní
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
v	v	k7c6	v
době	doba	k1gFnSc6	doba
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
uchoval	uchovat	k5eAaPmAgMnS	uchovat
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
půlroční	půlroční	k2eAgInSc4d1	půlroční
výlet	výlet	k1gInSc4	výlet
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Sophie	Sophie	k1gFnSc1	Sophie
následovala	následovat	k5eAaImAgFnS	následovat
svého	svůj	k3xOyFgMnSc4	svůj
muže	muž	k1gMnSc4	muž
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
Léopold	Léopold	k1gMnSc1	Léopold
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
guvernér	guvernér	k1gMnSc1	guvernér
provincie	provincie	k1gFnSc2	provincie
blízko	blízko	k7c2	blízko
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
)	)	kIx)	)
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
tří	tři	k4xCgFnPc2	tři
španělských	španělský	k2eAgFnPc2d1	španělská
provincií	provincie	k1gFnPc2	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojená	spokojený	k2eNgFnSc1d1	nespokojená
s	s	k7c7	s
neustálým	neustálý	k2eAgNnSc7d1	neustálé
stěhováním	stěhování	k1gNnSc7	stěhování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
vojenský	vojenský	k2eAgInSc1d1	vojenský
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
faktem	fakt	k1gInSc7	fakt
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
nesdílel	sdílet	k5eNaImAgMnS	sdílet
její	její	k3xOp3gFnSc4	její
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
Sophie	Sophie	k1gFnSc1	Sophie
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nakrátko	nakrátko	k6eAd1	nakrátko
opustit	opustit	k5eAaPmF	opustit
Léopolda	Léopolda	k1gFnSc1	Léopolda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
a	a	k8xC	a
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dominantním	dominantní	k2eAgInSc7d1	dominantní
způsobem	způsob	k1gInSc7	způsob
řídila	řídit	k5eAaImAgFnS	řídit
Hugovu	Hugův	k2eAgFnSc4d1	Hugova
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
Hugova	Hugův	k2eAgNnSc2d1	Hugovo
první	první	k4xOgNnPc1	první
díla	dílo	k1gNnPc1	dílo
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
a	a	k8xC	a
próze	próza	k1gFnSc3	próza
odrážela	odrážet	k5eAaImAgFnS	odrážet
ještě	ještě	k6eAd1	ještě
její	její	k3xOp3gNnSc4	její
vášnivé	vášnivý	k2eAgNnSc4d1	vášnivé
oddání	oddání	k1gNnSc4	oddání
víře	víra	k1gFnSc3	víra
a	a	k8xC	a
Králi	Král	k1gMnSc3	Král
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
během	během	k7c2	během
událostí	událost	k1gFnPc2	událost
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
k	k	k7c3	k
Francouzské	francouzský	k2eAgFnSc3d1	francouzská
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Hugo	Hugo	k1gMnSc1	Hugo
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
svému	svůj	k3xOyFgNnSc3	svůj
katolickému	katolický	k2eAgNnSc3d1	katolické
royalistickému	royalistický	k2eAgNnSc3d1	royalistické
vzdělání	vzdělání	k1gNnSc3	vzdělání
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
stoupencem	stoupenec	k1gMnSc7	stoupenec
volno-myšlenkářství	volnoyšlenkářství	k1gNnSc2	volno-myšlenkářství
a	a	k8xC	a
přívržencem	přívrženec	k1gMnSc7	přívrženec
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1819	[number]	k4	1819
založil	založit	k5eAaPmAgMnS	založit
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Abelem	Abel	k1gMnSc7	Abel
čtrnáctideník	čtrnáctideník	k1gInSc4	čtrnáctideník
Le	Le	k1gFnSc2	Le
Conservateur	Conservateura	k1gFnPc2	Conservateura
littéraire	littérair	k1gInSc5	littérair
(	(	kIx(	(
<g/>
vycházel	vycházet	k5eAaImAgInS	vycházet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
otiskoval	otiskovat	k5eAaImAgMnS	otiskovat
literárněkritické	literárněkritický	k2eAgFnSc2d1	literárněkritická
stati	stať	k1gFnSc2	stať
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
recenze	recenze	k1gFnSc2	recenze
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
monarchistické	monarchistický	k2eAgNnSc4d1	monarchistické
zaměření	zaměření	k1gNnSc4	zaměření
časopisu	časopis	k1gInSc2	časopis
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
již	již	k6eAd1	již
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
otázkami	otázka	k1gFnPc7	otázka
svobody	svoboda	k1gFnSc2	svoboda
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojeného	spojený	k2eAgNnSc2d1	spojené
svobodného	svobodný	k2eAgNnSc2d1	svobodné
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
druhý	druhý	k4xOgMnSc1	druhý
bratr	bratr	k1gMnSc1	bratr
Victora	Victor	k1gMnSc4	Victor
Huga	Hugo	k1gMnSc4	Hugo
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gMnSc4	Eugè
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
literární	literární	k2eAgFnSc4d1	literární
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
sužován	sužován	k2eAgInSc1d1	sužován
duševní	duševní	k2eAgFnSc7d1	duševní
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
37	[number]	k4	37
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Viktor	Viktor	k1gMnSc1	Viktor
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Adè	Adè	k1gFnSc2	Adè
Foucher	Fouchra	k1gFnPc2	Fouchra
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
tajně	tajně	k6eAd1	tajně
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Adè	Adè	k1gMnSc7	Adè
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
až	až	k9	až
poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
Léopold	Léopold	k1gMnSc1	Léopold
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
nemluvně	nemluvně	k1gNnSc4	nemluvně
<g/>
.	.	kIx.	.
</s>
<s>
Hugovy	Hugův	k2eAgFnPc1d1	Hugova
další	další	k2eAgFnPc1d1	další
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
Léopoldine	Léopoldin	k1gInSc5	Léopoldin
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
François-Victor	François-Victor	k1gInSc1	François-Victor
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
a	a	k8xC	a
Adè	Adè	k1gMnSc1	Adè
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
vydal	vydat	k5eAaPmAgMnS	vydat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
novelu	novela	k1gFnSc4	novela
Han	Hana	k1gFnPc2	Hana
z	z	k7c2	z
Islandu	Island	k1gInSc2	Island
(	(	kIx(	(
<g/>
Han	Hana	k1gFnPc2	Hana
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Islande	Island	k1gInSc5	Island
<g/>
)	)	kIx)	)
r.	r.	kA	r.
1823	[number]	k4	1823
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
Veliký	veliký	k2eAgMnSc1d1	veliký
Jargal	Jargal	k1gMnSc1	Jargal
(	(	kIx(	(
<g/>
Bug-Jargal	Bug-Jargal	k1gMnSc1	Bug-Jargal
<g/>
)	)	kIx)	)
r.	r.	kA	r.
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1829	[number]	k4	1829
a	a	k8xC	a
1840	[number]	k4	1840
vydal	vydat	k5eAaPmAgInS	vydat
dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc4	pět
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
(	(	kIx(	(
<g/>
Les	les	k1gInSc4	les
Orientales	Orientalesa	k1gFnPc2	Orientalesa
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
<g/>
;	;	kIx,	;
Les	les	k1gInSc1	les
Feuilles	Feuilles	k1gInSc1	Feuilles
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
automne	automnout	k5eAaPmIp3nS	automnout
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
;	;	kIx,	;
Les	les	k1gInSc1	les
Chants	Chants	k1gInSc1	Chants
du	du	k?	du
crépuscule	crépuscule	k1gFnSc2	crépuscule
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
<g/>
;	;	kIx,	;
Les	les	k1gInSc1	les
Voix	Voix	k1gInSc1	Voix
intérieures	intérieuresa	k1gFnPc2	intérieuresa
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
<g/>
;	;	kIx,	;
a	a	k8xC	a
Les	les	k1gInSc4	les
Rayons	Rayons	k1gInSc1	Rayons
et	et	k?	et
les	les	k1gInSc1	les
ombres	ombresa	k1gFnPc2	ombresa
<g/>
,	,	kIx,	,
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
upevnili	upevnit	k5eAaPmAgMnP	upevnit
jeho	jeho	k3xOp3gFnSc4	jeho
reputaci	reputace	k1gFnSc4	reputace
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
poetů	poet	k1gMnPc2	poet
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
spisovatele	spisovatel	k1gMnSc2	spisovatel
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Adè	Adè	k1gFnSc4	Adè
navázala	navázat	k5eAaPmAgFnS	navázat
milostný	milostný	k2eAgInSc4d1	milostný
poměr	poměr	k1gInSc4	poměr
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
dobrým	dobrý	k2eAgMnSc7d1	dobrý
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
kritikem	kritik	k1gMnSc7	kritik
Sainte-Beuvem	Sainte-Beuv	k1gMnSc7	Sainte-Beuv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Juliette	Juliett	k1gInSc5	Juliett
Drouet	Drouet	k1gInSc4	Drouet
(	(	kIx(	(
<g/>
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Julienne	Julienn	k1gInSc5	Julienn
Gauvain	Gauvain	k2eAgMnSc1d1	Gauvain
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
milenkou	milenka	k1gFnSc7	milenka
<g/>
,	,	kIx,	,
dobrovolnou	dobrovolný	k2eAgFnSc7d1	dobrovolná
sekretářkou	sekretářka	k1gFnSc7	sekretářka
a	a	k8xC	a
společnicí	společnice	k1gFnSc7	společnice
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
další	další	k2eAgFnPc1d1	další
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
trval	trvat	k5eAaImAgInS	trvat
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Provázela	provázet	k5eAaImAgFnS	provázet
ho	on	k3xPp3gInSc4	on
i	i	k9	i
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gInSc2	on
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
však	však	k9	však
bydleli	bydlet	k5eAaImAgMnP	bydlet
spolu	spolu	k6eAd1	spolu
pod	pod	k7c7	pod
jednou	jeden	k4xCgFnSc7	jeden
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
jí	on	k3xPp3gFnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mnoho	mnoho	k4c4	mnoho
svých	svůj	k3xOyFgFnPc2	svůj
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
napsala	napsat	k5eAaPmAgFnS	napsat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
milostná	milostný	k2eAgFnSc1d1	milostná
korespondence	korespondence	k1gFnSc1	korespondence
vyšla	vyjít	k5eAaPmAgFnS	vyjít
knižně	knižně	k6eAd1	knižně
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lettres	Lettres	k1gInSc4	Lettres
à	à	k?	à
Juliette	Juliett	k1gInSc5	Juliett
Drouet	Drouet	k1gInSc1	Drouet
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
:	:	kIx,	:
le	le	k?	le
livre	livr	k1gInSc5	livr
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
anniversaire	anniversair	k1gInSc5	anniversair
par	para	k1gFnPc2	para
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
et	et	k?	et
Juliette	Juliett	k1gInSc5	Juliett
Drouet	Drouet	k1gInSc4	Drouet
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
byl	být	k5eAaImAgMnS	být
zdrcen	zdrtit	k5eAaPmNgMnS	zdrtit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Léopoldine	Léopoldin	k1gInSc5	Léopoldin
tragicky	tragicky	k6eAd1	tragicky
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Utopila	utopit	k5eAaPmAgFnS	utopit
se	se	k3xPyFc4	se
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
i	i	k8xC	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
při	při	k7c6	při
projížďce	projížďka	k1gFnSc6	projížďka
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
Seine	Sein	k1gInSc5	Sein
à	à	k?	à
Villequier	Villequira	k1gFnPc2	Villequira
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Hugo	Hugo	k1gMnSc1	Hugo
byl	být	k5eAaImAgMnS	být
zrovna	zrovna	k6eAd1	zrovna
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
se	se	k3xPyFc4	se
o	o	k7c6	o
Léopoldině	Léopoldin	k2eAgFnSc6d1	Léopoldin
smrti	smrt	k1gFnSc6	smrt
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
šok	šok	k1gInSc4	šok
<g/>
,	,	kIx,	,
zděšení	zděšení	k1gNnSc4	zděšení
a	a	k8xC	a
žal	žal	k1gInSc4	žal
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
básni	báseň	k1gFnSc6	báseň
À	À	k?	À
Villequier	Villequira	k1gFnPc2	Villequira
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgMnS	napsat
mnoho	mnoho	k4c4	mnoho
básní	báseň	k1gFnPc2	báseň
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Demain	Demain	k1gMnSc1	Demain
<g/>
,	,	kIx,	,
dè	dè	k?	dè
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
aube	aub	k1gMnSc2	aub
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
návštěvu	návštěva	k1gFnSc4	návštěva
jejího	její	k3xOp3gInSc2	její
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
snažil	snažit	k5eAaImAgMnS	snažit
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
milované	milovaný	k2eAgFnSc2d1	milovaná
dcery	dcera	k1gFnSc2	dcera
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
nejen	nejen	k6eAd1	nejen
zármutek	zármutek	k1gInSc1	zármutek
truchlícího	truchlící	k2eAgMnSc2d1	truchlící
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
soucit	soucit	k1gInSc4	soucit
s	s	k7c7	s
bolestí	bolest	k1gFnSc7	bolest
všech	všecek	k3xTgFnPc2	všecek
trpících	trpící	k2eAgFnPc2d1	trpící
např.	např.	kA	např.
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
básní	básnit	k5eAaImIp3nS	básnit
La	la	k1gNnSc1	la
Pitié	Pitiá	k1gFnSc2	Pitiá
suprê	suprê	k?	suprê
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Hugo	Hugo	k1gMnSc1	Hugo
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
pairem	pair	k1gMnSc7	pair
<g/>
,	,	kIx,	,
v	v	k7c6	v
pairovské	pairovský	k2eAgFnSc6d1	pairovský
komoře	komora	k1gFnSc6	komora
se	se	k3xPyFc4	se
vyslovoval	vyslovovat	k5eAaImAgMnS	vyslovovat
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bonaparta	Bonapart	k1gMnSc2	Bonapart
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
pairie	pairie	k1gFnSc2	pairie
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
úspěšně	úspěšně	k6eAd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
zprvu	zprvu	k6eAd1	zprvu
politickým	politický	k2eAgMnSc7d1	politický
stoupencem	stoupenec	k1gMnSc7	stoupenec
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc4	jeho
odpůrce	odpůrce	k1gMnPc4	odpůrce
a	a	k8xC	a
jako	jako	k9	jako
zastánce	zastánce	k1gMnSc4	zastánce
liberálního	liberální	k2eAgInSc2d1	liberální
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
převratu	převrat	k1gInSc2	převrat
provedeného	provedený	k2eAgInSc2d1	provedený
Napoleonem	napoleon	k1gInSc7	napoleon
III	III	kA	III
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
žil	žít	k5eAaImAgMnS	žít
téměř	téměř	k6eAd1	téměř
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
v	v	k7c4	v
Jersey	Jersea	k1gFnPc4	Jersea
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zamířil	zamířit	k5eAaPmAgMnS	zamířit
na	na	k7c4	na
menší	malý	k2eAgInSc4d2	menší
ostrov	ostrov	k1gInSc4	ostrov
Guernsey	Guernsea	k1gFnSc2	Guernsea
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
prožil	prožít	k5eAaPmAgInS	prožít
další	další	k2eAgFnSc4d1	další
osobní	osobní	k2eAgFnSc4d1	osobní
tragédii	tragédie	k1gFnSc4	tragédie
se	se	k3xPyFc4	se
svoji	svůj	k3xOyFgFnSc4	svůj
krásnou	krásný	k2eAgFnSc4d1	krásná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psychicky	psychicky	k6eAd1	psychicky
labilní	labilní	k2eAgFnSc7d1	labilní
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
dcerou	dcera	k1gFnSc7	dcera
Adè	Adè	k1gFnSc2	Adè
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útrapách	útrapa	k1gFnPc6	útrapa
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgFnPc6d1	spojená
s	s	k7c7	s
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
láskou	láska	k1gFnSc7	láska
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
milovala	milovat	k5eAaImAgFnS	milovat
<g/>
,	,	kIx,	,
věřila	věřit	k5eAaImAgFnS	věřit
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
za	za	k7c7	za
nímž	jenž	k3xRgNnSc7	jenž
odjela	odjet	k5eAaPmAgFnS	odjet
až	až	k6eAd1	až
do	do	k7c2	do
daleké	daleký	k2eAgFnSc2d1	daleká
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
veřejně	veřejně	k6eAd1	veřejně
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
naplno	naplno	k6eAd1	naplno
propukla	propuknout	k5eAaPmAgFnS	propuknout
vážná	vážný	k2eAgFnSc1d1	vážná
duševní	duševní	k2eAgFnSc1d1	duševní
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
žila	žít	k5eAaImAgFnS	žít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
nemocné	mocný	k2eNgMnPc4d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Napoleon	napoleon	k1gInSc1	napoleon
III	III	kA	III
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
amnestii	amnestie	k1gFnSc4	amnestie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
donucen	donutit	k5eAaPmNgMnS	donutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
trůnu	trůn	k1gInSc3	trůn
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
Prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
za	za	k7c2	za
bouřlivých	bouřlivý	k2eAgFnPc2d1	bouřlivá
oslav	oslava	k1gFnPc2	oslava
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
pruskými	pruský	k2eAgNnPc7d1	pruské
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
k	k	k7c3	k
ideálům	ideál	k1gInPc3	ideál
Komuny	komuna	k1gFnSc2	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
Hugo	Hugo	k1gMnSc1	Hugo
mohl	moct	k5eAaImAgMnS	moct
natrvalo	natrvalo	k6eAd1	natrvalo
usadit	usadit	k5eAaPmF	usadit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
naposledy	naposledy	k6eAd1	naposledy
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Guernsey	Guernsea	k1gFnSc2	Guernsea
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zažil	zažít	k5eAaPmAgMnS	zažít
obléhání	obléhání	k1gNnSc4	obléhání
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
senátorským	senátorský	k2eAgMnSc7d1	senátorský
delegátem	delegát	k1gMnSc7	delegát
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
senátorem	senátor	k1gMnSc7	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
senátorským	senátorský	k2eAgMnSc7d1	senátorský
delegátem	delegát	k1gMnSc7	delegát
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stal	stát	k5eAaPmAgInS	stát
senátorem	senátor	k1gMnSc7	senátor
<g/>
;	;	kIx,	;
psal	psát	k5eAaImAgInS	psát
další	další	k2eAgNnPc4d1	další
různorodá	různorodý	k2eAgNnPc4d1	různorodé
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
řada	řada	k1gFnSc1	řada
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jako	jako	k9	jako
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
národní	národní	k2eAgMnSc1d1	národní
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	on	k3xPp3gInSc2	on
pohřbu	pohřeb	k1gInSc2	pohřeb
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
státní	státní	k2eAgInSc1d1	státní
smutek	smutek	k1gInSc1	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Panthéonu	Panthéon	k1gInSc6	Panthéon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
dvě	dva	k4xCgNnPc1	dva
muzea	muzeum	k1gNnPc1	muzeum
<g/>
:	:	kIx,	:
jedno	jeden	k4xCgNnSc1	jeden
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guernsey	Guernsea	k1gFnSc2	Guernsea
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Rohan	Rohan	k1gMnSc1	Rohan
na	na	k7c6	na
Place	plac	k1gInSc6	plac
des	des	k1gNnSc2	des
Vosges	Vosgesa	k1gFnPc2	Vosgesa
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
klasicistních	klasicistní	k2eAgFnPc2d1	klasicistní
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
romantických	romantický	k2eAgNnPc2d1	romantické
dramat	drama	k1gNnPc2	drama
a	a	k8xC	a
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
mísil	mísit	k5eAaImAgInS	mísit
postupy	postup	k1gInPc4	postup
černého	černý	k2eAgInSc2d1	černý
románu	román	k1gInSc2	román
se	s	k7c7	s
zásadami	zásada	k1gFnPc7	zásada
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
spojoval	spojovat	k5eAaImAgMnS	spojovat
snové	snový	k2eAgFnSc3d1	snová
<g/>
,	,	kIx,	,
filosofické	filosofický	k2eAgFnSc3d1	filosofická
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnSc3d1	politická
a	a	k8xC	a
lyrické	lyrický	k2eAgFnSc3d1	lyrická
oblasti	oblast	k1gFnSc3	oblast
<g/>
,	,	kIx,	,
mísil	mísit	k5eAaImAgInS	mísit
tragično	tragično	k1gNnSc4	tragično
a	a	k8xC	a
komično	komično	k1gNnSc4	komično
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
ošklivost	ošklivost	k1gFnSc4	ošklivost
<g/>
,	,	kIx,	,
vznešenost	vznešenost	k1gFnSc4	vznešenost
a	a	k8xC	a
grotesknost	grotesknost	k1gFnSc4	grotesknost
a	a	k8xC	a
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
humanisty	humanista	k1gMnSc2	humanista
se	se	k3xPyFc4	se
nadčasově	nadčasově	k6eAd1	nadčasově
dotkl	dotknout	k5eAaPmAgMnS	dotknout
řady	řada	k1gFnPc4	řada
sociálních	sociální	k2eAgMnPc2d1	sociální
problémů	problém	k1gInPc2	problém
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
romány	román	k1gInPc1	román
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
byly	být	k5eAaImAgFnP	být
katolickou	katolický	k2eAgFnSc4d1	katolická
církví	církev	k1gFnPc2	církev
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
Chrám	chrám	k1gInSc1	chrám
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1834	[number]	k4	1834
a	a	k8xC	a
Bídníci	bídník	k1gMnPc1	bídník
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
misérables	misérables	k1gInSc1	misérables
<g/>
)	)	kIx)	)
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Cromwell	Cromwell	k1gInSc1	Cromwell
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předmluva	předmluva	k1gFnSc1	předmluva
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
dílu	díl	k1gInSc3	díl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
manifestem	manifest	k1gInSc7	manifest
romantického	romantický	k2eAgNnSc2d1	romantické
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
diskuzi	diskuze	k1gFnSc4	diskuze
mezi	mezi	k7c7	mezi
francouzským	francouzský	k2eAgInSc7d1	francouzský
klasicismem	klasicismus	k1gInSc7	klasicismus
a	a	k8xC	a
romantismem	romantismus	k1gInSc7	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Marion	Marion	k1gInSc1	Marion
de	de	k?	de
Lorme	Lorm	k1gInSc5	Lorm
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hernani	Hernan	k1gMnPc1	Hernan
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
napsal	napsat	k5eAaPmAgMnS	napsat
italský	italský	k2eAgMnSc1d1	italský
skladatel	skladatel	k1gMnSc1	skladatel
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdi	Verd	k1gMnPc1	Verd
svou	svůj	k3xOyFgFnSc4	svůj
operu	opera	k1gFnSc4	opera
Ernani	Ernaň	k1gFnSc3	Ernaň
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
baví	bavit	k5eAaImIp3nS	bavit
(	(	kIx(	(
<g/>
Le	Le	k1gMnPc1	Le
Roi	Roi	k1gFnSc7	Roi
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
amuse	amus	k1gMnSc2	amus
<g/>
,	,	kIx,	,
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
napsal	napsat	k5eAaPmAgMnS	napsat
italský	italský	k2eAgMnSc1d1	italský
skladatel	skladatel	k1gMnSc1	skladatel
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdi	Verd	k1gMnPc1	Verd
svou	svůj	k3xOyFgFnSc4	svůj
operu	opera	k1gFnSc4	opera
Rigoletto	Rigolett	k2eAgNnSc1d1	Rigoletto
<g/>
.	.	kIx.	.
</s>
<s>
Lukrécie	Lukrécie	k1gFnSc1	Lukrécie
Borgia	Borgia	k1gFnSc1	Borgia
(	(	kIx(	(
<g/>
Lucrè	Lucrè	k1gFnSc1	Lucrè
Borgia	Borgia	k1gFnSc1	Borgia
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Tudorovna	Tudorovna	k1gFnSc1	Tudorovna
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Tudor	tudor	k1gInSc1	tudor
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
Ruy	Ruy	k1gFnSc1	Ruy
Blas	Blasa	k1gFnPc2	Blasa
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angelo	Angela	k1gFnSc5	Angela
<g/>
,	,	kIx,	,
tyran	tyran	k1gMnSc1	tyran
padovský	padovský	k2eAgMnSc1d1	padovský
(	(	kIx(	(
<g/>
Angelo	Angela	k1gFnSc5	Angela
<g/>
,	,	kIx,	,
tyran	tyran	k1gMnSc1	tyran
de	de	k?	de
Padoue	Padoue	k1gInSc1	Padoue
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Purkrabí	purkrabí	k1gMnSc1	purkrabí
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Burgraves	Burgravesa	k1gFnPc2	Burgravesa
<g/>
,	,	kIx,	,
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Torquemada	Torquemada	k1gFnSc1	Torquemada
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
psal	psát	k5eAaImAgMnS	psát
poezii	poezie	k1gFnSc4	poezie
lyrickou	lyrický	k2eAgFnSc4d1	lyrická
<g/>
,	,	kIx,	,
epickou	epický	k2eAgFnSc4d1	epická
<g/>
,	,	kIx,	,
milostnou	milostný	k2eAgFnSc4d1	milostná
i	i	k8xC	i
satirickou	satirický	k2eAgFnSc4d1	satirická
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
dokonalostí	dokonalost	k1gFnSc7	dokonalost
veršované	veršovaný	k2eAgFnSc2d1	veršovaná
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
barvitostí	barvitost	k1gFnPc2	barvitost
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
líčení	líčení	k1gNnSc4	líčení
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
autorovým	autorův	k2eAgFnPc3d1	autorova
básnickým	básnický	k2eAgFnPc3d1	básnická
sbírkám	sbírka	k1gFnPc3	sbírka
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Ódy	ód	k1gInPc1	ód
a	a	k8xC	a
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
Odes	Odes	k1gInSc1	Odes
et	et	k?	et
poésies	poésies	k1gMnSc1	poésies
diverses	diverses	k1gMnSc1	diverses
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
ódy	ód	k1gInPc1	ód
(	(	kIx(	(
<g/>
Nouvelles	Nouvelles	k1gInSc1	Nouvelles
Odes	Odesa	k1gFnPc2	Odesa
<g/>
,	,	kIx,	,
1824	[number]	k4	1824
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ódy	óda	k1gFnSc2	óda
a	a	k8xC	a
balady	balada	k1gFnSc2	balada
(	(	kIx(	(
<g/>
Odes	Odes	k1gInSc1	Odes
et	et	k?	et
Ballades	Ballades	k1gInSc1	Ballades
<g/>
,	,	kIx,	,
1826	[number]	k4	1826
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInPc1d1	východní
zpěvy	zpěv	k1gInPc1	zpěv
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Orientales	Orientalesa	k1gFnPc2	Orientalesa
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Podzimní	podzimní	k2eAgNnSc1d1	podzimní
listí	listí	k1gNnSc1	listí
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Feuilles	Feuilles	k1gInSc1	Feuilles
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
automne	automnout	k5eAaPmIp3nS	automnout
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zpěvy	zpěv	k1gInPc1	zpěv
soumraku	soumrak	k1gInSc2	soumrak
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Chantes	Chantes	k1gInSc1	Chantes
du	du	k?	du
crépescule	crépescule	k1gFnSc2	crépescule
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
hlasy	hlas	k1gInPc1	hlas
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voix	Voix	k1gInSc1	Voix
intérieurs	intérieursa	k1gFnPc2	intérieursa
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paprsky	paprsek	k1gInPc1	paprsek
a	a	k8xC	a
stíny	stín	k1gInPc1	stín
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Rayons	Rayons	k1gInSc1	Rayons
et	et	k?	et
les	les	k1gInSc1	les
ombres	ombresa	k1gFnPc2	ombresa
<g/>
,	,	kIx,	,
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tresty	trest	k1gInPc1	trest
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Châtiments	Châtimentsa	k1gFnPc2	Châtimentsa
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satirická	satirický	k2eAgFnSc1d1	satirická
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
III	III	kA	III
<g/>
..	..	k?	..
Kontemplace	kontemplace	k1gFnSc1	kontemplace
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Contemplations	Contemplationsa	k1gFnPc2	Contemplationsa
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
věků	věk	k1gInPc2	věk
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
La	la	k0	la
Légende	Légend	k1gMnSc5	Légend
des	des	k1gNnPc1	des
Siè	Siè	k1gMnSc1	Siè
<g/>
,	,	kIx,	,
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básnická	básnický	k2eAgFnSc1d1	básnická
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
třídílný	třídílný	k2eAgInSc1d1	třídílný
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
básník	básník	k1gMnSc1	básník
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
nad	nad	k7c7	nad
vývojem	vývoj	k1gInSc7	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
pokrokem	pokrok	k1gInSc7	pokrok
a	a	k8xC	a
humanitou	humanita	k1gFnSc7	humanita
a	a	k8xC	a
optimisticky	optimisticky	k6eAd1	optimisticky
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
lepší	dobrý	k2eAgFnSc4d2	lepší
budoucnost	budoucnost	k1gFnSc4	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
lesů	les	k1gInPc2	les
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Chansons	Chansonsa	k1gFnPc2	Chansonsa
des	des	k1gNnSc2	des
rues	ruesa	k1gFnPc2	ruesa
et	et	k?	et
des	des	k1gNnSc4	des
bois	bois	k6eAd1	bois
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hrozný	hrozný	k2eAgInSc1d1	hrozný
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Année	Année	k1gFnPc2	Année
terrible	terrible	k6eAd1	terrible
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc4	umění
být	být	k5eAaImF	být
dědečkem	dědeček	k1gMnSc7	dědeček
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
d	d	k?	d
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ê	ê	k?	ê
grand-pè	grandè	k?	grand-pè
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Papež	Papež	k1gMnSc1	Papež
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Pape	Pape	k1gFnSc1	Pape
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soucit	soucit	k1gInSc1	soucit
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Pitié	Pitiá	k1gFnSc2	Pitiá
suprê	suprê	k?	suprê
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Osel	osel	k1gMnSc1	osel
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
âne	âne	k?	âne
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
mn	mn	k?	mn
<g/>
.	.	kIx.	.
<g/>
č.	č.	k?	č.
<g/>
)	)	kIx)	)
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
Religions	Religions	k1gInSc1	Religions
et	et	k?	et
religion	religion	k1gInSc1	religion
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čtvero	čtvero	k4xRgNnSc1	čtvero
proudů	proud	k1gInPc2	proud
ducha	duch	k1gMnSc2	duch
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Quatre	Quatr	k1gInSc5	Quatr
Vents	Vents	k1gInSc4	Vents
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Esprit	esprit	k1gInSc1	esprit
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc2	autor
<g />
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
Satana	Satan	k1gMnSc2	Satan
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Fin	Fin	k1gMnSc1	Fin
de	de	k?	de
Satan	Satan	k1gMnSc1	Satan
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Celá	celý	k2eAgFnSc1d1	celá
lyra	lyra	k1gFnSc1	lyra
(	(	kIx(	(
<g/>
Toute	Tout	k1gInSc5	Tout
la	la	k1gNnPc7	la
lyre	lyr	k1gInPc1	lyr
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
(	(	kIx(	(
<g/>
Dieu	Dieus	k1gInSc2	Dieus
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Neblahé	blahý	k2eNgInPc1d1	neblahý
roky	rok	k1gInPc1	rok
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Anées	Anées	k1gMnSc1	Anées
funestes	funestes	k1gMnSc1	funestes
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
kytice	kytice	k1gFnSc1	kytice
(	(	kIx(	(
<g/>
Derniè	Derniè	k1gFnSc1	Derniè
gerbe	gerbat	k5eAaPmIp3nS	gerbat
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Han	Hana	k1gFnPc2	Hana
z	z	k7c2	z
Islandu	Island	k1gInSc2	Island
(	(	kIx(	(
<g/>
Han	Hana	k1gFnPc2	Hana
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Islande	Island	k1gInSc5	Island
<g/>
,	,	kIx,	,
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorova	autorův	k2eAgFnSc1d1	autorova
románová	románový	k2eAgFnSc1d1	románová
prvotina	prvotina	k1gFnSc1	prvotina
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
černým	černý	k2eAgInSc7d1	černý
románem	román	k1gInSc7	román
a	a	k8xC	a
frenetickou	frenetický	k2eAgFnSc7d1	frenetická
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
Jargal	Jargal	k1gMnSc1	Jargal
(	(	kIx(	(
<g/>
Bug-Jargal	Bug-Jargal	k1gMnSc1	Bug-Jargal
<g/>
,	,	kIx,	,
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
románů	román	k1gInPc2	román
Waltera	Walter	k1gMnSc2	Walter
Scotta	Scott	k1gMnSc2	Scott
<g/>
,	,	kIx,	,
líčící	líčící	k2eAgNnSc1d1	líčící
vzpouru	vzpoura	k1gFnSc4	vzpoura
černošských	černošský	k2eAgMnPc2d1	černošský
otroků	otrok	k1gMnPc2	otrok
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
den	den	k1gInSc1	den
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Dernier	Dernier	k1gMnSc1	Dernier
Jour	Jour	k?	Jour
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
condamné	condamný	k2eAgInPc1d1	condamný
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
zachycující	zachycující	k2eAgFnSc4d1	zachycující
dobu	doba	k1gFnSc4	doba
od	od	k7c2	od
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
odsouzeným	odsouzený	k1gMnSc7	odsouzený
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
až	až	k6eAd1	až
po	po	k7c4	po
poslední	poslední	k2eAgInPc4d1	poslední
okamžiky	okamžik	k1gInPc4	okamžik
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Čtenář	čtenář	k1gMnSc1	čtenář
se	se	k3xPyFc4	se
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
jméno	jméno	k1gNnSc1	jméno
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
prožije	prožít	k5eAaPmIp3nS	prožít
utrpení	utrpení	k1gNnSc4	utrpení
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
pobytem	pobyt	k1gInSc7	pobyt
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
z	z	k7c2	z
očekávání	očekávání	k1gNnSc2	očekávání
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c4	za
zrušení	zrušení	k1gNnSc4	zrušení
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
hrbatý	hrbatý	k2eAgMnSc1d1	hrbatý
zvoník	zvoník	k1gMnSc1	zvoník
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
ujal	ujmout	k5eAaPmAgMnS	ujmout
kněz	kněz	k1gMnSc1	kněz
Frollo	Frollo	k1gNnSc4	Frollo
a	a	k8xC	a
vychoval	vychovat	k5eAaPmAgMnS	vychovat
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
se	se	k3xPyFc4	se
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
pokusí	pokusit	k5eAaPmIp3nS	pokusit
unést	unést	k5eAaPmF	unést
cikánskou	cikánský	k2eAgFnSc4d1	cikánská
tanečnici	tanečnice	k1gFnSc4	tanečnice
Esmeraldu	Esmeralda	k1gFnSc4	Esmeralda
<g/>
,	,	kIx,	,
do	do	k7c2	do
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
Frollo	Frollo	k1gNnSc1	Frollo
zamilován	zamilován	k2eAgMnSc1d1	zamilován
<g/>
.	.	kIx.	.
</s>
<s>
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
na	na	k7c6	na
pranýři	pranýř	k1gInSc6	pranýř
<g/>
.	.	kIx.	.
</s>
<s>
Esmeralda	Esmeralda	k1gFnSc1	Esmeralda
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
slituje	slitovat	k5eAaPmIp3nS	slitovat
<g/>
.	.	kIx.	.
</s>
<s>
Frollovými	Frollův	k2eAgFnPc7d1	Frollův
intrikami	intrika	k1gFnPc7	intrika
je	být	k5eAaImIp3nS	být
Esmeralda	Esmeralda	k1gFnSc1	Esmeralda
popravena	popraven	k2eAgFnSc1d1	popravena
a	a	k8xC	a
Quasimodo	Quasimodo	k1gMnSc1	Quasimodo
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mstí	mstít	k5eAaImIp3nS	mstít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
zvoníkovo	zvoníkův	k2eAgNnSc1d1	zvoníkův
tělo	tělo	k1gNnSc1	tělo
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
v	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Esmeralda	Esmeralda	k1gFnSc1	Esmeralda
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Bídníci	bídník	k1gMnPc1	bídník
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Misérables	Misérablesa	k1gFnPc2	Misérablesa
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Ubožáci	ubožák	k1gMnPc1	ubožák
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Hugo	Hugo	k1gMnSc1	Hugo
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
život	život	k1gInSc4	život
francouzského	francouzský	k2eAgInSc2d1	francouzský
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Jean	Jean	k1gMnSc1	Jean
Valjean	Valjean	k1gMnSc1	Valjean
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bývalý	bývalý	k2eAgMnSc1d1	bývalý
galejník	galejník	k1gMnSc1	galejník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
galeje	galej	k1gFnPc4	galej
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
krádež	krádež	k1gFnSc4	krádež
chleba	chléb	k1gInSc2	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
okradl	okrást	k5eAaPmAgMnS	okrást
starého	starý	k1gMnSc4	starý
kněze	kněz	k1gMnSc4	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
nocleh	nocleh	k1gInSc4	nocleh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
chytli	chytnout	k5eAaPmAgMnP	chytnout
četníci	četník	k1gMnPc1	četník
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
jim	on	k3xPp3gMnPc3	on
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
příbory	příbor	k1gInPc1	příbor
Jeanu	Jean	k1gMnSc3	Jean
Valjeanovi	Valjeanův	k2eAgMnPc1d1	Valjeanův
dal	dát	k5eAaPmAgInS	dát
darem	dar	k1gInSc7	dar
<g/>
,	,	kIx,	,
přidal	přidat	k5eAaPmAgMnS	přidat
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k6eAd1	ještě
stříbrné	stříbrný	k2eAgMnPc4d1	stříbrný
svícny	svícen	k1gInPc4	svícen
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ho	on	k3xPp3gInSc4	on
zachránil	zachránit	k5eAaPmAgInS	zachránit
od	od	k7c2	od
galejí	galej	k1gFnPc2	galej
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Jeana	Jean	k1gMnSc4	Jean
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgInS	stát
dobrodincem	dobrodinec	k1gMnSc7	dobrodinec
a	a	k8xC	a
ochráncem	ochránce	k1gMnSc7	ochránce
těch	ten	k3xDgMnPc2	ten
nejbídnějších	bídný	k2eAgMnPc2d3	nejbídnější
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
protihráčem	protihráč	k1gMnSc7	protihráč
je	být	k5eAaImIp3nS	být
policejní	policejní	k2eAgMnSc1d1	policejní
prefekt	prefekt	k1gMnSc1	prefekt
Javert	Javert	k1gMnSc1	Javert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
galejník	galejník	k1gMnSc1	galejník
zůstane	zůstat	k5eAaPmIp3nS	zůstat
galejníkem	galejník	k1gMnSc7	galejník
<g/>
,	,	kIx,	,
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
dopadení	dopadení	k1gNnSc4	dopadení
Valjeana	Valjean	k1gMnSc2	Valjean
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
životní	životní	k2eAgNnSc4d1	životní
poslání	poslání	k1gNnSc4	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Ženskými	ženský	k2eAgFnPc7d1	ženská
hrdinkami	hrdinka	k1gFnPc7	hrdinka
románu	román	k1gInSc2	román
jsou	být	k5eAaImIp3nP	být
Fantine	Fantin	k1gInSc5	Fantin
<g/>
,	,	kIx,	,
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bída	bída	k1gFnSc1	bída
dohnala	dohnat	k5eAaPmAgFnS	dohnat
k	k	k7c3	k
prostituci	prostituce	k1gFnSc3	prostituce
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
dcera	dcera	k1gFnSc1	dcera
Cosette	Cosett	k1gMnSc5	Cosett
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Jean	Jean	k1gMnSc1	Jean
Valjean	Valjean	k1gMnSc1	Valjean
a	a	k8xC	a
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
jako	jako	k9	jako
obžaloba	obžaloba	k1gFnSc1	obžaloba
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
mravů	mrav	k1gInPc2	mrav
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Travailleurs	Travailleurs	k1gInSc1	Travailleurs
de	de	k?	de
la	la	k1gNnSc2	la
Mer	Mer	k1gFnPc2	Mer
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
drsné	drsný	k2eAgFnSc2d1	drsná
přírody	příroda	k1gFnSc2	příroda
Normanských	normanský	k2eAgInPc2d1	normanský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
době	doba	k1gFnSc6	doba
plachetnic	plachetnice	k1gFnPc2	plachetnice
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
parníků	parník	k1gInPc2	parník
<g/>
,	,	kIx,	,
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
vítězný	vítězný	k2eAgInSc4d1	vítězný
lidský	lidský	k2eAgInSc4d1	lidský
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
mořským	mořský	k2eAgInSc7d1	mořský
živlem	živel	k1gInSc7	živel
a	a	k8xC	a
lidskou	lidský	k2eAgFnSc4d1	lidská
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
qui	qui	k?	qui
rit	rito	k1gNnPc2	rito
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
prostředí	prostředí	k1gNnSc2	prostředí
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nás	my	k3xPp1nPc4	my
zavádí	zavádět	k5eAaImIp3nS	zavádět
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
kočovných	kočovný	k2eAgFnPc2d1	kočovná
tlup	tlupa	k1gFnPc2	tlupa
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
námořníky	námořník	k1gMnPc7	námořník
<g/>
,	,	kIx,	,
do	do	k7c2	do
žalářů	žalář	k1gInPc2	žalář
i	i	k9	i
do	do	k7c2	do
paláců	palác	k1gInPc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
podobný	podobný	k2eAgInSc4d1	podobný
námět	námět	k1gInSc4	námět
jako	jako	k8xS	jako
v	v	k7c6	v
románu	román	k1gInSc6	román
Chrám	chrám	k1gInSc1	chrám
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
znetvořena	znetvořen	k2eAgFnSc1d1	znetvořena
tvář	tvář	k1gFnSc1	tvář
do	do	k7c2	do
"	"	kIx"	"
<g/>
věčného	věčný	k2eAgInSc2d1	věčný
úsměvu	úsměv	k1gInSc2	úsměv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krutým	krutý	k2eAgInSc7d1	krutý
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
poznamenávaly	poznamenávat	k5eAaImAgFnP	poznamenávat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nechtěné	chtěný	k2eNgFnPc1d1	nechtěná
a	a	k8xC	a
odložené	odložený	k2eAgFnPc1d1	odložená
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nepokoušely	pokoušet	k5eNaImAgFnP	pokoušet
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
najít	najít	k5eAaPmF	najít
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ze	z	k7c2	z
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Mladík	mladík	k1gInSc1	mladík
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
potulných	potulný	k2eAgMnPc2d1	potulný
komediantů	komediant	k1gMnPc2	komediant
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
klaun	klaun	k1gMnSc1	klaun
<g/>
.	.	kIx.	.
</s>
<s>
Útěchou	útěcha	k1gFnSc7	útěcha
v	v	k7c6	v
nelehkém	lehký	k2eNgInSc6d1	nelehký
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stává	stávat	k5eAaImIp3nS	stávat
láska	láska	k1gFnSc1	láska
krásné	krásný	k2eAgFnSc2d1	krásná
slepé	slepý	k2eAgFnSc2d1	slepá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nevidí	vidět	k5eNaImIp3nS	vidět
jeho	jeho	k3xOp3gFnSc4	jeho
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
vnímá	vnímat	k5eAaImIp3nS	vnímat
jeho	jeho	k3xOp3gFnSc4	jeho
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc4d1	dobré
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
šlechetnou	šlechetný	k2eAgFnSc4d1	šlechetná
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tragickém	tragický	k2eAgInSc6d1	tragický
závěru	závěr	k1gInSc6	závěr
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pro	pro	k7c4	pro
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
odchod	odchod	k1gInSc4	odchod
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uhájil	uhájit	k5eAaPmAgMnS	uhájit
čest	čest	k1gFnSc4	čest
svého	svůj	k3xOyFgNnSc2	svůj
pravého	pravý	k2eAgNnSc2d1	pravé
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
<g/>
,	,	kIx,	,
romantický	romantický	k2eAgInSc1d1	romantický
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
autorovy	autorův	k2eAgInPc1d1	autorův
romány	román	k1gInPc1	román
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
uznávaná	uznávaný	k2eAgNnPc4d1	uznávané
díla	dílo	k1gNnPc4	dílo
nejen	nejen	k6eAd1	nejen
francouzské	francouzský	k2eAgNnSc4d1	francouzské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Devadesát	devadesát	k4xCc1	devadesát
tři	tři	k4xCgNnPc1	tři
(	(	kIx(	(
<g/>
Quatrevingt-treize	Quatrevingtreize	k1gFnSc1	Quatrevingt-treize
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Hugo	Hugo	k1gMnSc1	Hugo
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
nejdramatičtější	dramatický	k2eAgInSc4d3	nejdramatičtější
rok	rok	k1gInSc4	rok
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
<s>
Přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
osudech	osud	k1gInPc6	osud
tří	tři	k4xCgFnPc2	tři
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
vůdce	vůdce	k1gMnSc2	vůdce
protirevoluční	protirevoluční	k2eAgFnSc2d1	protirevoluční
selské	selský	k2eAgFnSc2d1	selská
vzpoury	vzpoura	k1gFnSc2	vzpoura
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vendée	Vendé	k1gFnSc2	Vendé
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
na	na	k7c6	na
postavách	postava	k1gFnPc6	postava
dvou	dva	k4xCgInPc2	dva
stoupenců	stoupenec	k1gMnPc2	stoupenec
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tragicky	tragicky	k6eAd1	tragicky
hynou	hynout	k5eAaImIp3nP	hynout
pro	pro	k7c4	pro
rozpory	rozpor	k1gInPc4	rozpor
mezi	mezi	k7c7	mezi
věrností	věrnost	k1gFnSc7	věrnost
revoluci	revoluce	k1gFnSc3	revoluce
a	a	k8xC	a
osobními	osobní	k2eAgInPc7d1	osobní
city	cit	k1gInPc7	cit
<g/>
.	.	kIx.	.
</s>
<s>
Claude	Claude	k6eAd1	Claude
Gueux	Gueux	k1gInSc1	Gueux
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
zařadit	zařadit	k5eAaPmF	zařadit
spíše	spíše	k9	spíše
mezi	mezi	k7c4	mezi
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
líčí	líčit	k5eAaImIp3nP	líčit
osudy	osud	k1gInPc1	osud
Clauda	Clauda	k1gFnSc1	Clauda
Gueux	Gueux	k1gInSc1	Gueux
<g/>
,	,	kIx,	,
zloděje	zloděj	k1gMnPc4	zloděj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
provinil	provinit	k5eAaPmAgMnS	provinit
jen	jen	k9	jen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
nasytit	nasytit	k5eAaPmF	nasytit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Rýn	Rýn	k1gInSc1	Rýn
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Rhin	Rhin	k1gMnSc1	Rhin
<g/>
,	,	kIx,	,
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc2	autor
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
(	(	kIx(	(
<g/>
Alpes	Alpes	k1gMnSc1	Alpes
et	et	k?	et
Pyrénées	Pyrénées	k1gMnSc1	Pyrénées
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Belgie	Belgie	k1gFnSc2	Belgie
(	(	kIx(	(
<g/>
France	Franc	k1gMnSc2	Franc
et	et	k?	et
Belgique	Belgiqu	k1gMnSc2	Belgiqu
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
literárních	literární	k2eAgFnPc2d1	literární
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
statí	stať	k1gFnPc2	stať
(	(	kIx(	(
<g/>
Littérature	Littératur	k1gMnSc5	Littératur
et	et	k?	et
philosophie	philosophie	k1gFnSc2	philosophie
mê	mê	k?	mê
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
Napoleon	Napoleon	k1gMnSc1	Napoleon
Malý	Malý	k1gMnSc1	Malý
(	(	kIx(	(
<g/>
Napoléon-le-Petit	Napoléone-Petit	k1gMnSc1	Napoléon-le-Petit
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
pamflet	pamflet	k1gInSc1	pamflet
<g/>
,	,	kIx,	,
mistrný	mistrný	k2eAgInSc1d1	mistrný
portrét	portrét	k1gInSc1	portrét
dobrodruha	dobrodruh	k1gMnSc2	dobrodruh
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
životopisná	životopisný	k2eAgFnSc1d1	životopisná
esej	esej	k1gFnSc1	esej
Činy	čina	k1gFnSc2	čina
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
Actes	Actes	k1gMnSc1	Actes
et	et	k?	et
paroles	paroles	k1gMnSc1	paroles
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politické	politický	k2eAgInPc1d1	politický
projevy	projev	k1gInPc1	projev
Historie	historie	k1gFnSc1	historie
jednoho	jeden	k4xCgInSc2	jeden
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gInSc5	Histoir
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
un	un	k?	un
crime	crimat	k5eAaPmIp3nS	crimat
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protest	protest	k1gInSc1	protest
proti	proti	k7c3	proti
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejotřesnějších	otřesný	k2eAgFnPc2d3	nejotřesnější
událostí	událost	k1gFnPc2	událost
francouzské	francouzský	k2eAgFnSc2d1	francouzská
historie	historie	k1gFnSc2	historie
-	-	kIx~	-
proti	proti	k7c3	proti
státnímu	státní	k2eAgInSc3d1	státní
převratu	převrat	k1gInSc3	převrat
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bonaparta	Bonapart	k1gMnSc2	Bonapart
<g/>
.	.	kIx.	.
vyšly	vyjít	k5eAaPmAgInP	vyjít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc4	autor
Co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
viděl	vidět	k5eAaImAgMnS	vidět
(	(	kIx(	(
<g/>
Choses	Choses	k1gInSc1	Choses
vues	vuesa	k1gFnPc2	vuesa
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Post	post	k1gInSc1	post
scriptum	scriptum	k1gNnSc1	scriptum
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Post-scriptum	Postcriptum	k1gNnSc1	Post-scriptum
de	de	k?	de
ma	ma	k?	ma
vie	vie	k?	vie
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
1830-1848	[number]	k4	1830-1848
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souborná	souborný	k2eAgNnPc1d1	souborné
vydání	vydání	k1gNnPc1	vydání
děl	dělo	k1gNnPc2	dělo
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
vycházela	vycházet	k5eAaImAgFnS	vycházet
především	především	k6eAd1	především
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
nakladatelstvích	nakladatelství	k1gNnPc6	nakladatelství
Hetzel-Quantin	Hetzel-Quantin	k1gInSc4	Hetzel-Quantin
(	(	kIx(	(
<g/>
48	[number]	k4	48
sv.	sv.	kA	sv.
od	od	k7c2	od
r.	r.	kA	r.
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ollendorf-Michel	Ollendorf-Michel	k1gMnSc1	Ollendorf-Michel
(	(	kIx(	(
<g/>
45	[number]	k4	45
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gallimard	Gallimard	k1gMnSc1	Gallimard
(	(	kIx(	(
<g/>
6	[number]	k4	6
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
poezie	poezie	k1gFnSc1	poezie
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
překlady	překlad	k1gInPc4	překlad
poezie	poezie	k1gFnSc2	poezie
Victora	Victor	k1gMnSc4	Victor
Huga	Hugo	k1gMnSc4	Hugo
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
Básně	báseň	k1gFnSc2	báseň
V.	V.	kA	V.
<g/>
Huga	Hugo	k1gMnSc2	Hugo
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jar	jar	k1gFnSc1	jar
<g/>
.	.	kIx.	.
<g/>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakl	Nakl	k1gMnSc1	Nakl
<g/>
.	.	kIx.	.
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
Grérga	Grérga	k1gFnSc1	Grérga
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
Poesie	poesie	k1gFnSc1	poesie
francouzská	francouzský	k2eAgFnSc1d1	francouzská
nové	nový	k2eAgFnPc4d1	nová
doby	doba	k1gFnPc4	doba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
překlad	překlad	k1gInSc1	překlad
Jar	jar	k1gFnSc1	jar
<g/>
.	.	kIx.	.
<g/>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakl	Nakl	k1gMnSc1	Nakl
<g/>
.	.	kIx.	.
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
Grégra	Grégra	k1gMnSc1	Grégra
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
Nové	Nové	k2eAgFnPc1d1	Nové
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jar	jar	k1gFnSc1	jar
<g/>
.	.	kIx.	.
<g/>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
<g/>
Otta	Otta	k1gMnSc1	Otta
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc4d1	Nové
překlady	překlad	k1gInPc4	překlad
z	z	k7c2	z
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jar	jar	k1gFnSc1	jar
<g/>
.	.	kIx.	.
<g/>
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
<g/>
Otta	Otta	k1gMnSc1	Otta
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
Spící	spící	k2eAgInSc1d1	spící
Booz	Booz	k1gInSc1	Booz
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Boh.	Boh.	k1gMnSc1	Boh.
<g/>
Reynek	Reynek	k1gMnSc1	Reynek
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
:	:	kIx,	:
Marta	Marta	k1gFnSc1	Marta
Florianová	Florianová	k1gFnSc1	Florianová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Satyr	satyr	k1gMnSc1	satyr
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Sv.	sv.	kA	sv.
<g/>
Kadlec	Kadlec	k1gMnSc1	Kadlec
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
T.	T.	kA	T.
<g/>
Vondrovic	Vondrovice	k1gFnPc2	Vondrovice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lyra	lyra	k1gFnSc1	lyra
Pragensis	Pragensis	k1gFnSc1	Pragensis
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc4	výbor
a	a	k8xC	a
překlad	překlad	k1gInSc4	překlad
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
K.	K.	kA	K.
<g/>
Bednář	Bednář	k1gMnSc1	Bednář
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
Fikar	Fikar	k1gInSc1	Fikar
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
<g/>
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Pilař	Pilař	k1gMnSc1	Pilař
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
Slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Valja	Valj	k1gInSc2	Valj
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Jen	jen	k9	jen
ty	ty	k3xPp2nSc1	ty
nám	my	k3xPp1nPc3	my
zůstáváš	zůstávat	k5eAaImIp2nS	zůstávat
<g/>
,	,	kIx,	,
ó	ó	k0	ó
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
překlad	překlad	k1gInSc1	překlad
P.	P.	kA	P.
<g/>
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Pět	pět	k4xCc1	pět
romantických	romantický	k2eAgFnPc2d1	romantická
siluet	silueta	k1gFnPc2	silueta
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnPc4	poezie
francouzského	francouzský	k2eAgInSc2d1	francouzský
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Vl	Vl	k1gFnSc2	Vl
<g/>
.	.	kIx.	.
<g/>
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lyra	lyra	k1gFnSc1	lyra
Pragensis	Pragensis	k1gFnSc2	Pragensis
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Beru	brát	k5eAaImIp1nS	brát
si	se	k3xPyFc3	se
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc4	výbor
a	a	k8xC	a
překlad	překlad	k1gInSc4	překlad
kolektiv	kolektiv	k1gInSc4	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
K.	K.	kA	K.
<g/>
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
Fikar	Fikar	k1gInSc4	Fikar
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
<g/>
Francl	Francl	k1gMnSc1	Francl
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
<g/>
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
,	,	kIx,	,
Vl	Vl	k1gMnPc1	Vl
<g/>
.	.	kIx.	.
<g/>
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Pechar	Pechar	k1gMnSc1	Pechar
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Pelán	Pelán	k1gInSc1	Pelán
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
Slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Taufer	Taufer	k1gInSc1	Taufer
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Valja	Valj	k1gInSc2	Valj
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Širé	širý	k2eAgNnSc1d1	širé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
širé	širý	k2eAgNnSc1d1	širé
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
N.	N.	kA	N.
<g/>
Macurová	Macurová	k1gFnSc1	Macurová
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
Hrbata	Hrbata	k1gFnSc1	Hrbata
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Han	Hana	k1gFnPc2	Hana
z	z	k7c2	z
Islandu	Island	k1gInSc2	Island
(	(	kIx(	(
<g/>
Han	Hana	k1gFnPc2	Hana
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Islande	Island	k1gInSc5	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Em	Ema	k1gFnPc2	Ema
<g/>
.	.	kIx.	.
<g/>
Čenkov	Čenkov	k1gInSc1	Čenkov
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Veliký	veliký	k2eAgInSc1d1	veliký
Jargal	Jargal	k1gInSc1	Jargal
(	(	kIx(	(
<g/>
Bug-Jargal	Bug-Jargal	k1gInSc1	Bug-Jargal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1924	[number]	k4	1924
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgInSc1d1	poslední
den	den	k1gInSc1	den
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Dernier	Dernier	k1gMnSc1	Dernier
Jour	Jour	k?	Jour
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
condamné	condamný	k2eAgFnPc1d1	condamný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Janů	Janů	k1gFnSc2	Janů
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Chrám	chrám	k1gInSc1	chrám
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1864	[number]	k4	1864
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Chrám	chrám	k1gInSc1	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
překlad	překlad	k1gInSc1	překlad
1923	[number]	k4	1923
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Zvoník	zvoník	k1gMnSc1	zvoník
u	u	k7c2	u
Matky	matka	k1gFnSc2	matka
boží	boží	k2eAgInSc4d1	boží
překlad	překlad	k1gInSc4	překlad
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Bídníci	bídník	k1gMnPc5	bídník
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Ubožáci	ubožák	k1gMnPc1	ubožák
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
četné	četný	k2eAgFnSc2d1	četná
adaptace	adaptace	k1gFnSc2	adaptace
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
M.	M.	kA	M.
<g/>
Majerové	Majerové	k2eAgInPc1d1	Majerové
a	a	k8xC	a
výbory	výbor	k1gInPc1	výbor
<g/>
,	,	kIx,	,
Dělníci	dělník	k1gMnPc1	dělník
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Travailleurs	Travailleurs	k1gInSc1	Travailleurs
de	de	k?	de
la	la	k1gNnSc7	la
Mer	Mer	k1gFnSc2	Mer
<g/>
)	)	kIx)	)
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
qui	qui	k?	qui
rit	rit	k1gInSc1	rit
<g/>
)	)	kIx)	)
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
upraveno	upravit	k5eAaPmNgNnS	upravit
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Devadesát	devadesát	k4xCc1	devadesát
tři	tři	k4xCgNnPc1	tři
(	(	kIx(	(
<g/>
Quatrevingt-treize	Quatrevingtreize	k1gFnSc1	Quatrevingt-treize
<g/>
)	)	kIx)	)
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Claudius	Claudius	k1gInSc1	Claudius
Gueux	Gueux	k1gInSc4	Gueux
(	(	kIx(	(
<g/>
Claude	Claud	k1gMnSc5	Claud
Gueux	Gueux	k1gInSc1	Gueux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlady	překlad	k1gInPc1	překlad
<g/>
:	:	kIx,	:
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
