<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
pět	pět	k4xCc4	pět
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
)	)	kIx)	)
a	a	k8xC	a
střih	střih	k1gInSc1	střih
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
