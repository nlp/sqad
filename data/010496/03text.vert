<p>
<s>
Socialistický	socialistický	k2eAgInSc1d1	socialistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
organizace	organizace	k1gFnSc1	organizace
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
sdružující	sdružující	k2eAgMnPc4d1	sdružující
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc4d1	fungující
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1970	[number]	k4	1970
až	až	k9	až
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
byla	být	k5eAaImAgFnS	být
řízena	řídit	k5eAaImNgFnS	řídit
a	a	k8xC	a
spravována	spravovat	k5eAaImNgFnS	spravovat
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
SSM	SSM	kA	SSM
byla	být	k5eAaImAgFnS	být
nástupnickou	nástupnický	k2eAgFnSc7d1	nástupnická
organizací	organizace	k1gFnSc7	organizace
po	po	k7c6	po
zaniklém	zaniklý	k2eAgInSc6d1	zaniklý
Československém	československý	k2eAgInSc6d1	československý
svazu	svaz	k1gInSc6	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
ČSM	ČSM	kA	ČSM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
Juraj	Juraj	k1gMnSc1	Juraj
Varholík	Varholík	k1gMnSc1	Varholík
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnSc7d1	poslední
předsedou	předseda	k1gMnSc7	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
před	před	k7c7	před
zánikem	zánik	k1gInSc7	zánik
organizace	organizace	k1gFnSc2	organizace
byl	být	k5eAaImAgMnS	být
Martin	Martin	k1gMnSc1	Martin
Ulčák	Ulčák	k1gMnSc1	Ulčák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslání	poslání	k1gNnPc4	poslání
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
SSM	SSM	kA	SSM
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Československa	Československo	k1gNnSc2	Československo
spojeneckými	spojenecký	k2eAgInPc7d1	spojenecký
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
pokynů	pokyn	k1gInPc2	pokyn
nově	nově	k6eAd1	nově
<g/>
,	,	kIx,	,
nesvobodně	svobodně	k6eNd1	svobodně
ustanoveného	ustanovený	k2eAgNnSc2d1	ustanovené
vedení	vedení	k1gNnSc2	vedení
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ovládnout	ovládnout	k5eAaPmF	ovládnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
znormalizovat	znormalizovat	k5eAaPmF	znormalizovat
stav	stav	k1gInSc4	stav
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
cenzurovat	cenzurovat	k5eAaImF	cenzurovat
názory	názor	k1gInPc4	názor
mladých	mladý	k2eAgMnPc2d1	mladý
studujících	studující	k2eAgMnPc2d1	studující
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
potlačovat	potlačovat	k5eAaImF	potlačovat
včas	včas	k6eAd1	včas
svobodu	svoboda	k1gFnSc4	svoboda
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
posilovat	posilovat	k5eAaImF	posilovat
v	v	k7c6	v
mladých	mladý	k2eAgNnPc6d1	mladé
lidech	lido	k1gNnPc6	lido
vědomí	vědomí	k1gNnSc2	vědomí
mocenského	mocenský	k2eAgNnSc2d1	mocenské
<g/>
,	,	kIx,	,
neměnného	neměnný	k2eAgNnSc2d1	neměnné
postavení	postavení	k1gNnSc2	postavení
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
SSM	SSM	kA	SSM
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předstupněm	předstupeň	k1gInSc7	předstupeň
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
výchova	výchova	k1gFnSc1	výchova
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
jednotného	jednotný	k2eAgInSc2d1	jednotný
systému	systém	k1gInSc2	systém
ČSM	ČSM	kA	ČSM
a	a	k8xC	a
PO	Po	kA	Po
ČSM	ČSM	kA	ČSM
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
následovalo	následovat	k5eAaImAgNnS	následovat
obsazení	obsazení	k1gNnSc1	obsazení
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
a	a	k8xC	a
navazující	navazující	k2eAgNnSc4d1	navazující
období	období	k1gNnSc4	období
postupné	postupný	k2eAgFnSc2d1	postupná
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
Federální	federální	k2eAgFnSc1d1	federální
rada	rada	k1gFnSc1	rada
dětských	dětský	k2eAgFnPc2d1	dětská
a	a	k8xC	a
mládežnických	mládežnický	k2eAgFnPc2d1	mládežnická
organizací	organizace	k1gFnPc2	organizace
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připravila	připravit	k5eAaPmAgFnS	připravit
jejich	jejich	k3xOp3gNnSc4	jejich
opětné	opětný	k2eAgNnSc4d1	opětné
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
zamezena	zamezen	k2eAgFnSc1d1	zamezena
činnost	činnost	k1gFnSc1	činnost
samostatných	samostatný	k2eAgFnPc2d1	samostatná
skautských	skautský	k2eAgFnPc2d1	skautská
organizací	organizace	k1gFnPc2	organizace
-	-	kIx~	-
Junáků	junák	k1gMnPc2	junák
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
17	[number]	k4	17
mládežnických	mládežnický	k2eAgFnPc2d1	mládežnická
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
založených	založený	k2eAgInPc2d1	založený
či	či	k8xC	či
obnovených	obnovený	k2eAgInPc2d1	obnovený
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1968	[number]	k4	1968
a	a	k8xC	a
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
bývalým	bývalý	k2eAgMnSc7d1	bývalý
vedoucím	vedoucí	k1gMnSc7	vedoucí
junáku	junák	k1gMnSc3	junák
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
SSM	SSM	kA	SSM
odpírána	odpírat	k5eAaImNgFnS	odpírat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
nahradily	nahradit	k5eAaPmAgFnP	nahradit
zakázaný	zakázaný	k2eAgInSc4d1	zakázaný
skauting	skauting	k1gInSc4	skauting
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
normalizované	normalizovaný	k2eAgFnSc2d1	normalizovaná
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
PO	po	k7c6	po
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
SSM	SSM	kA	SSM
byl	být	k5eAaImAgInS	být
organizací	organizace	k1gFnSc7	organizace
navazující	navazující	k2eAgFnSc7d1	navazující
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
na	na	k7c4	na
dětský	dětský	k2eAgInSc4d1	dětský
Pionýr	pionýr	k1gInSc4	pionýr
<g/>
.	.	kIx.	.
</s>
<s>
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
organizačně	organizačně	k6eAd1	organizačně
<g/>
,	,	kIx,	,
byrokraticky	byrokraticky	k6eAd1	byrokraticky
od	od	k7c2	od
KSČ	KSČ	kA	KSČ
řízenou	řízený	k2eAgFnSc7d1	řízená
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhovující	vyhovující	k2eAgMnPc1d1	vyhovující
funkcionáři	funkcionář	k1gMnPc1	funkcionář
SSM	SSM	kA	SSM
byli	být	k5eAaImAgMnP	být
připravováni	připravován	k2eAgMnPc1d1	připravován
na	na	k7c6	na
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
na	na	k7c4	na
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
kádrovým	kádrový	k2eAgMnPc3d1	kádrový
útvarům	útvar	k1gInPc3	útvar
v	v	k7c6	v
podnicích	podnik	k1gInPc6	podnik
při	při	k7c6	při
normalizaci	normalizace	k1gFnSc6	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
funkcionáři	funkcionář	k1gMnPc7	funkcionář
SSM	SSM	kA	SSM
byli	být	k5eAaImAgMnP	být
vyčleněni	vyčleněn	k2eAgMnPc1d1	vyčleněn
speciálně	speciálně	k6eAd1	speciálně
vyhovující	vyhovující	k2eAgMnPc1d1	vyhovující
a	a	k8xC	a
prověření	prověřený	k2eAgMnPc1d1	prověřený
členové	člen	k1gMnPc1	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
styční	styčný	k2eAgMnPc1d1	styčný
pracovníci	pracovník	k1gMnPc1	pracovník
se	s	k7c7	s
SSM	SSM	kA	SSM
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
například	například	k6eAd1	například
působil	působit	k5eAaImAgMnS	působit
takto	takto	k6eAd1	takto
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šlouf	Šlouf	k1gMnSc1	Šlouf
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionáři	funkcionář	k1gMnPc1	funkcionář
SSM	SSM	kA	SSM
sdělovali	sdělovat	k5eAaImAgMnP	sdělovat
své	svůj	k3xOyFgNnSc4	svůj
hodnocení	hodnocení	k1gNnSc4	hodnocení
spolužáků	spolužák	k1gMnPc2	spolužák
či	či	k8xC	či
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
styčným	styčný	k2eAgFnPc3d1	styčná
osobám	osoba	k1gFnPc3	osoba
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Spoludodávali	Spoludodávat	k5eAaImAgMnP	Spoludodávat
svá	svůj	k3xOyFgNnPc4	svůj
doporučení	doporučení	k1gNnPc4	doporučení
pro	pro	k7c4	pro
možnosti	možnost	k1gFnPc4	možnost
pracovního	pracovní	k2eAgNnSc2d1	pracovní
uplatnění	uplatnění	k1gNnSc2	uplatnění
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
kádrové	kádrový	k2eAgNnSc4d1	kádrové
hodnocení	hodnocení	k1gNnSc4	hodnocení
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
i	i	k9	i
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
členy	člen	k1gInPc4	člen
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
tak	tak	k6eAd1	tak
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
znemožněnou	znemožněný	k2eAgFnSc4d1	znemožněná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
nedostal	dostat	k5eNaPmAgInS	dostat
doporučení	doporučení	k1gNnSc4	doporučení
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
nedostal	dostat	k5eNaPmAgMnS	dostat
také	také	k9	také
doporučení	doporučení	k1gNnSc4	doporučení
k	k	k7c3	k
individuálnímu	individuální	k2eAgInSc3d1	individuální
výjezdu	výjezd	k1gInSc3	výjezd
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
způsob	způsob	k1gInSc4	způsob
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
organizace	organizace	k1gFnSc2	organizace
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
taktéž	taktéž	k?	taktéž
lidově	lidově	k6eAd1	lidově
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Svaz	svaz	k1gInSc1	svaz
souložící	souložící	k2eAgFnSc2d1	souložící
mládeže	mládež	k1gFnSc2	mládež
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ústředí	ústředí	k1gNnSc2	ústředí
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
svazáckým	svazácký	k2eAgInSc7d1	svazácký
orgánem	orgán	k1gInSc7	orgán
byl	být	k5eAaImAgInS	být
celostátní	celostátní	k2eAgInSc1d1	celostátní
sjezd	sjezd	k1gInSc1	sjezd
<g/>
,	,	kIx,	,
svolávaný	svolávaný	k2eAgInSc1d1	svolávaný
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
sjezdy	sjezd	k1gInPc7	sjezd
organizaci	organizace	k1gFnSc4	organizace
řídil	řídit	k5eAaImAgMnS	řídit
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
organizace	organizace	k1gFnSc1	organizace
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
územními	územní	k2eAgInPc7d1	územní
republikovými	republikový	k2eAgInPc7d1	republikový
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
SSR	SSR	kA	SSR
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
organizací	organizace	k1gFnSc7	organizace
SSM	SSM	kA	SSM
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
republikové	republikový	k2eAgFnPc1d1	republiková
organizace	organizace	k1gFnPc1	organizace
svolávaly	svolávat	k5eAaImAgFnP	svolávat
republikové	republikový	k2eAgInPc4d1	republikový
sjezdy	sjezd	k1gInPc4	sjezd
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
své	své	k1gNnSc4	své
sekretariáty	sekretariát	k1gInPc4	sekretariát
a	a	k8xC	a
předsednictva	předsednictvo	k1gNnPc4	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
zároveň	zároveň	k6eAd1	zároveň
řídily	řídit	k5eAaImAgInP	řídit
krajské	krajský	k2eAgInPc1d1	krajský
výbory	výbor	k1gInPc1	výbor
SSM	SSM	kA	SSM
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
PO	po	k7c6	po
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
v	v	k7c6	v
krajích	kraj	k1gInPc6	kraj
===	===	k?	===
</s>
</p>
<p>
<s>
Krajskou	krajský	k2eAgFnSc4d1	krajská
organizaci	organizace	k1gFnSc4	organizace
tvořily	tvořit	k5eAaImAgFnP	tvořit
všechny	všechen	k3xTgFnPc4	všechen
okresní	okresní	k2eAgFnPc4d1	okresní
svazácké	svazácký	k2eAgFnPc4d1	svazácká
organizace	organizace	k1gFnPc4	organizace
příslušného	příslušný	k2eAgInSc2d1	příslušný
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
byl	být	k5eAaImAgInS	být
krajský	krajský	k2eAgInSc1d1	krajský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nepřímo	přímo	k6eNd1	přímo
řídil	řídit	k5eAaImAgInS	řídit
i	i	k9	i
krajskou	krajský	k2eAgFnSc4d1	krajská
organizaci	organizace	k1gFnSc4	organizace
PO	po	k7c6	po
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
krajskými	krajský	k2eAgInPc7d1	krajský
orgány	orgán	k1gInPc7	orgán
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
sekretariát	sekretariát	k1gInSc4	sekretariát
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
organizace	organizace	k1gFnPc1	organizace
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Bratislavě	Bratislava	k1gFnSc6	Bratislava
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
organizací	organizace	k1gFnPc2	organizace
krajských	krajský	k2eAgFnPc2d1	krajská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okresní	okresní	k2eAgFnSc2d1	okresní
organizace	organizace	k1gFnSc2	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Tu	tu	k6eAd1	tu
tvořily	tvořit	k5eAaImAgFnP	tvořit
všechny	všechen	k3xTgFnPc1	všechen
základní	základní	k2eAgFnPc1d1	základní
organizace	organizace	k1gFnPc1	organizace
SSM	SSM	kA	SSM
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
vyjma	vyjma	k7c2	vyjma
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
řídily	řídit	k5eAaImAgFnP	řídit
okresní	okresní	k2eAgFnSc4d1	okresní
organizaci	organizace	k1gFnSc4	organizace
PO	po	k7c6	po
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
byla	být	k5eAaImAgFnS	být
formálně	formálně	k6eAd1	formálně
okresní	okresní	k2eAgFnSc1d1	okresní
konference	konference	k1gFnSc1	konference
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
pak	pak	k9	pak
sekretariát	sekretariát	k1gInSc1	sekretariát
OV	OV	kA	OV
SSM	SSM	kA	SSM
a	a	k8xC	a
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
OV	OV	kA	OV
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
OV	OV	kA	OV
SSM	SSM	kA	SSM
vznikaly	vznikat	k5eAaImAgInP	vznikat
ZO	ZO	kA	ZO
SSM	SSM	kA	SSM
na	na	k7c6	na
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
školách	škola	k1gFnPc6	škola
všeho	všecek	k3xTgInSc2	všecek
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
i	i	k8xC	i
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
školách	škola	k1gFnPc6	škola
pak	pak	k6eAd1	pak
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
celozávodní	celozávodní	k2eAgInPc1d1	celozávodní
<g/>
,	,	kIx,	,
celoškolské	celoškolský	k2eAgInPc1d1	celoškolský
<g/>
,	,	kIx,	,
celoučilištní	celoučilištní	k2eAgInPc1d1	celoučilištní
výbory	výbor	k1gInPc1	výbor
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
městské	městský	k2eAgInPc1d1	městský
výbory	výbor	k1gInPc1	výbor
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pak	pak	k6eAd1	pak
svolávaly	svolávat	k5eAaImAgInP	svolávat
své	svůj	k3xOyFgFnPc4	svůj
konference	konference	k1gFnPc4	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sjezdy	sjezd	k1gInPc4	sjezd
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
I.	I.	kA	I.
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
ČSSR	ČSSR	kA	ČSSR
1972	[number]	k4	1972
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1972	[number]	k4	1972
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
český	český	k2eAgInSc1d1	český
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
666	[number]	k4	666
delegátů	delegát	k1gMnPc2	delegát
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
410	[number]	k4	410
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
českého	český	k2eAgInSc2d1	český
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
ČÚV	ČÚV	kA	ČÚV
<g/>
)	)	kIx)	)
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jindřich	Jindřich	k1gMnSc1	Jindřich
Poledník	poledník	k1gMnSc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1972	[number]	k4	1972
následoval	následovat	k5eAaImAgInS	následovat
slovenský	slovenský	k2eAgInSc1d1	slovenský
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
slovenského	slovenský	k2eAgNnSc2d1	slovenské
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Michal	Michal	k1gMnSc1	Michal
Zozulák	Zozulák	k1gMnSc1	Zozulák
<g/>
.	.	kIx.	.
</s>
<s>
Celostátní	celostátní	k2eAgInSc1d1	celostátní
I.	I.	kA	I.
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
ČSSR	ČSSR	kA	ČSSR
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
ve	v	k7c6	v
dnech	den	k1gInPc6	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1972	[number]	k4	1972
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Sjezdovém	sjezdový	k2eAgInSc6d1	sjezdový
paláci	palác	k1gInSc6	palác
Parku	park	k1gInSc6	park
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
oddechu	oddech	k1gInSc2	oddech
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
<g/>
.	.	kIx.	.
</s>
<s>
Sjezdu	sjezd	k1gInSc2	sjezd
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
1	[number]	k4	1
285	[number]	k4	285
delegátů	delegát	k1gMnPc2	delegát
a	a	k8xC	a
520	[number]	k4	520
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
státní	státní	k2eAgMnPc1d1	státní
a	a	k8xC	a
straničtí	stranický	k2eAgMnPc1d1	stranický
činitelé	činitel	k1gMnPc1	činitel
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
mládežnických	mládežnický	k2eAgFnPc2d1	mládežnická
organizací	organizace	k1gFnPc2	organizace
ze	z	k7c2	z
32	[number]	k4	32
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
delegace	delegace	k1gFnSc2	delegace
Světové	světový	k2eAgFnSc2d1	světová
federace	federace	k1gFnSc2	federace
demokratické	demokratický	k2eAgFnSc2d1	demokratická
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
SFDM	SFDM	kA	SFDM
<g/>
)	)	kIx)	)
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svazu	svaz	k1gInSc2	svaz
studentstva	studentstvo	k1gNnSc2	studentstvo
(	(	kIx(	(
<g/>
MSS	MSS	kA	MSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
přijal	přijmout	k5eAaPmAgInS	přijmout
některé	některý	k3yIgFnPc4	některý
úpravy	úprava	k1gFnPc4	úprava
ve	v	k7c6	v
Stanovách	stanova	k1gFnPc6	stanova
<g/>
,	,	kIx,	,
např.	např.	kA	např.
upravil	upravit	k5eAaPmAgMnS	upravit
formulaci	formulace	k1gFnSc4	formulace
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
SSM	SSM	kA	SSM
ke	k	k7c3	k
KSČ	KSČ	kA	KSČ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
SSM	SSM	kA	SSM
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
z	z	k7c2	z
programu	program	k1gInSc2	program
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
aktivním	aktivní	k2eAgMnSc7d1	aktivní
pomocníkem	pomocník	k1gMnSc7	pomocník
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
upravil	upravit	k5eAaPmAgMnS	upravit
formulaci	formulace	k1gFnSc4	formulace
činnosti	činnost	k1gFnSc2	činnost
SSM	SSM	kA	SSM
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
SSM	SSM	kA	SSM
neustále	neustále	k6eAd1	neustále
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
jednotu	jednota	k1gFnSc4	jednota
všech	všecek	k3xTgFnPc2	všecek
sociálních	sociální	k2eAgFnPc2d1	sociální
a	a	k8xC	a
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
čs	čs	kA	čs
<g/>
.	.	kIx.	.
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Předsedou	předseda	k1gMnSc7	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Juraj	Juraj	k1gMnSc1	Juraj
Varholík	Varholík	k1gMnSc1	Varholík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
ČSSR	ČSSR	kA	ČSSR
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1977	[number]	k4	1977
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
SSM	SSM	kA	SSM
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
v	v	k7c6	v
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1974-1977	[number]	k4	1974-1977
předseda	předseda	k1gMnSc1	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
Jindřich	Jindřich	k1gMnSc1	Jindřich
Poledník	poledník	k1gMnSc1	poledník
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
předseda	předseda	k1gMnSc1	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
Miroslav	Miroslav	k1gMnSc1	Miroslav
Dočkal	Dočkal	k1gMnSc1	Dočkal
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
ČSSR	ČSSR	kA	ČSSR
1	[number]	k4	1
<g/>
.	.	kIx.	.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1982	[number]	k4	1982
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jenerál	Jenerál	k?	Jenerál
</s>
</p>
<p>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
ČSSR	ČSSR	kA	ČSSR
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc4	říjen
1987	[number]	k4	1987
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
Vasil	Vasila	k1gFnPc2	Vasila
Mohorita	Mohorita	k1gFnSc1	Mohorita
</s>
</p>
<p>
<s>
===	===	k?	===
Mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
sjezd	sjezd	k1gInSc1	sjezd
SSM	SSM	kA	SSM
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
SSM	SSM	kA	SSM
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právním	právní	k2eAgMnSc7d1	právní
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Svaz	svaz	k1gInSc1	svaz
mladých	mladý	k1gMnPc2	mladý
(	(	kIx(	(
<g/>
SM	SM	kA	SM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
zaregistrován	zaregistrovat	k5eAaPmNgMnS	zaregistrovat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
ustanovení	ustanovení	k1gNnSc2	ustanovení
§	§	k?	§
2	[number]	k4	2
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1951	[number]	k4	1951
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
dobrovolných	dobrovolný	k2eAgFnPc6d1	dobrovolná
organizacích	organizace	k1gFnPc6	organizace
a	a	k8xC	a
shromážděních	shromáždění	k1gNnPc6	shromáždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Transformace	transformace	k1gFnSc1	transformace
SSM	SSM	kA	SSM
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
zastával	zastávat	k5eAaImAgInS	zastávat
prodemokratický	prodemokratický	k2eAgInSc1d1	prodemokratický
blok	blok	k1gInSc1	blok
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
mládežnický	mládežnický	k2eAgMnSc1d1	mládežnický
moloch	moloch	k1gMnSc1	moloch
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
<g/>
/	/	kIx~	/
<g/>
90	[number]	k4	90
někde	někde	k6eAd1	někde
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
úrovni	úroveň	k1gFnSc6	úroveň
i	i	k9	i
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
centrální	centrální	k2eAgFnSc6d1	centrální
úrovni	úroveň	k1gFnSc6	úroveň
panoval	panovat	k5eAaImAgInS	panovat
jiný	jiný	k2eAgInSc1d1	jiný
názor	názor	k1gInSc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Předsedovi	předseda	k1gMnSc3	předseda
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
SSM	SSM	kA	SSM
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
sjezdu	sjezd	k1gInSc6	sjezd
SSM	SSM	kA	SSM
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
podařilo	podařit	k5eAaPmAgNnS	podařit
zastánce	zastánce	k1gMnPc4	zastánce
likvidace	likvidace	k1gFnSc2	likvidace
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
přijmout	přijmout	k5eAaPmF	přijmout
vnějškově	vnějškově	k6eAd1	vnějškově
demokratizační	demokratizační	k2eAgFnPc4d1	demokratizační
stanovy	stanova	k1gFnPc4	stanova
a	a	k8xC	a
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
socialistický	socialistický	k2eAgInSc4d1	socialistický
svaz	svaz	k1gInSc4	svaz
na	na	k7c4	na
Svaz	svaz	k1gInSc4	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
i	i	k9	i
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
bývalý	bývalý	k2eAgMnSc1d1	bývalý
disident	disident	k1gMnSc1	disident
John	John	k1gMnSc1	John
Bok	bok	k1gInSc4	bok
ze	z	k7c2	z
znovuobnovených	znovuobnovený	k2eAgFnPc2d1	znovuobnovená
organizací	organizace	k1gFnPc2	organizace
"	"	kIx"	"
<g/>
Majetkoprávní	majetkoprávní	k2eAgFnSc3d1	majetkoprávní
unii	unie	k1gFnSc3	unie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zabránit	zabránit	k5eAaPmF	zabránit
zpronevěření	zpronevěření	k1gNnSc4	zpronevěření
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
svazáckého	svazácký	k2eAgInSc2d1	svazácký
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
i	i	k9	i
objekty	objekt	k1gInPc1	objekt
zrušených	zrušený	k2eAgFnPc2d1	zrušená
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jozefa	Jozef	k1gMnSc2	Jozef
Mikloška	Miklošek	k1gMnSc2	Miklošek
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zmapovat	zmapovat	k5eAaPmF	zmapovat
rozsah	rozsah	k1gInSc4	rozsah
majetku	majetek	k1gInSc2	majetek
zabaveného	zabavený	k2eAgInSc2d1	zabavený
mládežnickým	mládežnický	k2eAgFnPc3d1	mládežnická
organizacím	organizace	k1gFnPc3	organizace
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Majetek	majetek	k1gInSc1	majetek
bývalého	bývalý	k2eAgMnSc2d1	bývalý
SSM	SSM	kA	SSM
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
majetek	majetek	k1gInSc1	majetek
základních	základní	k2eAgFnPc2d1	základní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
působících	působící	k2eAgMnPc2d1	působící
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
fakultách	fakulta	k1gFnPc6	fakulta
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
v	v	k7c6	v
podnicích	podnik	k1gInPc6	podnik
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
Pionýrské	pionýrský	k2eAgFnSc2d1	Pionýrská
organizace	organizace	k1gFnSc2	organizace
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
vyšších	vysoký	k2eAgFnPc2d2	vyšší
struktur	struktura	k1gFnPc2	struktura
SSM	SSM	kA	SSM
(	(	kIx(	(
<g/>
obvodních	obvodní	k2eAgFnPc2d1	obvodní
<g/>
,	,	kIx,	,
městských	městský	k2eAgFnPc2d1	městská
<g/>
,	,	kIx,	,
okresních	okresní	k2eAgFnPc2d1	okresní
<g/>
,	,	kIx,	,
krajských	krajský	k2eAgMnPc2d1	krajský
a	a	k8xC	a
republikových	republikový	k2eAgMnPc2d1	republikový
výborů	výbor	k1gInPc2	výbor
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
tzv.	tzv.	kA	tzv.
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
Smena	Smena	k1gFnSc1	Smena
<g/>
,	,	kIx,	,
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Jenom	jenom	k8xS	jenom
nemovitostí	nemovitost	k1gFnPc2	nemovitost
bylo	být	k5eAaImAgNnS	být
132	[number]	k4	132
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
nemovitosti	nemovitost	k1gFnSc2	nemovitost
získal	získat	k5eAaPmAgInS	získat
svaz	svaz	k1gInSc1	svaz
budováním	budování	k1gNnSc7	budování
ze	z	k7c2	z
státních	státní	k2eAgFnPc2d1	státní
podpor	podpora	k1gFnPc2	podpora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
kolem	kolem	k7c2	kolem
200	[number]	k4	200
–	–	k?	–
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
jako	jako	k9	jako
nástupnická	nástupnický	k2eAgFnSc1d1	nástupnická
organizace	organizace	k1gFnSc1	organizace
ČSM	ČSM	kA	ČSM
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabírala	zabírat	k5eAaImAgFnS	zabírat
majetek	majetek	k1gInSc4	majetek
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
organizací	organizace	k1gFnPc2	organizace
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Předběžně	předběžně	k6eAd1	předběžně
byl	být	k5eAaImAgInS	být
vyčíslen	vyčíslit	k5eAaPmNgInS	vyčíslit
na	na	k7c4	na
2,921	[number]	k4	2,921
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Majetek	majetek	k1gInSc1	majetek
organizací	organizace	k1gFnPc2	organizace
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
od	od	k7c2	od
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
tzv.	tzv.	kA	tzv.
zmrazen	zmrazit	k5eAaPmNgInS	zmrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
podařilo	podařit	k5eAaPmAgNnS	podařit
založit	založit	k5eAaPmF	založit
kolem	kolem	k7c2	kolem
300	[number]	k4	300
postsvazáckých	postsvazácký	k2eAgFnPc2d1	postsvazácký
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
převést	převést	k5eAaPmF	převést
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
497	[number]	k4	497
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
navrácení	navrácení	k1gNnSc6	navrácení
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
lidu	lid	k1gInSc2	lid
ČSFR	ČSFR	kA	ČSFR
stát	stát	k1gInSc1	stát
nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgInS	získat
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
svazáckého	svazácký	k2eAgInSc2d1	svazácký
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
nutno	nutno	k6eAd1	nutno
ale	ale	k9	ale
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
období	období	k1gNnSc1	období
transformace	transformace	k1gFnSc2	transformace
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jara	jaro	k1gNnSc2	jaro
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
provázela	provázet	k5eAaImAgFnS	provázet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
pochybných	pochybný	k2eAgInPc2d1	pochybný
postsvazáckých	postsvazácký	k2eAgInPc2d1	postsvazácký
majetkových	majetkový	k2eAgInPc2d1	majetkový
přesunů	přesun	k1gInPc2	přesun
<g/>
,	,	kIx,	,
problematické	problematický	k2eAgInPc1d1	problematický
postupy	postup	k1gInPc1	postup
Federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kontroly	kontrola	k1gFnSc2	kontrola
(	(	kIx(	(
<g/>
FMK	FMK	kA	FMK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionáři	funkcionář	k1gMnPc1	funkcionář
SM	SM	kA	SM
odmítali	odmítat	k5eAaImAgMnP	odmítat
vydávání	vydávání	k1gNnSc4	vydávání
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
prováděli	provádět	k5eAaImAgMnP	provádět
účetní	účetní	k2eAgFnPc4d1	účetní
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
,	,	kIx,	,
podávali	podávat	k5eAaImAgMnP	podávat
na	na	k7c4	na
státní	státní	k2eAgFnPc4d1	státní
autority	autorita	k1gFnPc4	autorita
žaloby	žaloba	k1gFnSc2	žaloba
a	a	k8xC	a
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Státem	stát	k1gInSc7	stát
převzatý	převzatý	k2eAgInSc1d1	převzatý
majetek	majetek	k1gInSc1	majetek
byl	být	k5eAaImAgInS	být
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
113	[number]	k4	113
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
Fondu	fond	k1gInSc6	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
dán	dát	k5eAaPmNgMnS	dát
do	do	k7c2	do
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
spravoval	spravovat	k5eAaImAgInS	spravovat
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Navrácení	navrácení	k1gNnSc2	navrácení
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
bývalého	bývalý	k2eAgMnSc2d1	bývalý
SSM	SSM	kA	SSM
mohl	moct	k5eAaImAgInS	moct
jeho	jeho	k3xOp3gFnSc4	jeho
držitel	držitel	k1gMnSc1	držitel
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
volně	volně	k6eAd1	volně
nakládat	nakládat	k5eAaImF	nakládat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zákonným	zákonný	k2eAgNnSc7d1	zákonné
opatřením	opatření	k1gNnSc7	opatření
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
č.	č.	k?	č.
177	[number]	k4	177
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
některých	některý	k3yIgNnPc6	některý
opatřeních	opatření	k1gNnPc6	opatření
týkajících	týkající	k2eAgNnPc6d1	týkající
se	se	k3xPyFc4	se
majetku	majetek	k1gInSc2	majetek
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
politických	politický	k2eAgNnPc2d1	politické
hnutí	hnutí	k1gNnPc2	hnutí
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
převody	převod	k1gInPc1	převod
majetku	majetek	k1gInSc2	majetek
zakázány	zakázán	k2eAgInPc1d1	zakázán
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
jejich	jejich	k3xOp3gFnSc1	jejich
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zákonnému	zákonný	k2eAgNnSc3d1	zákonné
opatření	opatření	k1gNnSc3	opatření
vydalo	vydat	k5eAaPmAgNnS	vydat
Federální	federální	k2eAgNnSc1d1	federální
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
(	(	kIx(	(
<g/>
FMF	FMF	kA	FMF
<g/>
)	)	kIx)	)
prováděcí	prováděcí	k2eAgFnSc4d1	prováděcí
vyhlášku	vyhláška	k1gFnSc4	vyhláška
č.	č.	k?	č.
224	[number]	k4	224
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
evidenci	evidence	k1gFnSc4	evidence
majetku	majetek	k1gInSc2	majetek
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
nestanovila	stanovit	k5eNaPmAgFnS	stanovit
nejen	nejen	k6eAd1	nejen
soupis	soupis	k1gInSc4	soupis
věcných	věcný	k2eAgFnPc2d1	věcná
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
nestanovila	stanovit	k5eNaPmAgFnS	stanovit
vyčíslení	vyčíslení	k1gNnSc4	vyčíslení
hodnoty	hodnota	k1gFnSc2	hodnota
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
<g/>
Zestátnění	zestátnění	k1gNnSc2	zestátnění
a	a	k8xC	a
převzetí	převzetí	k1gNnSc2	převzetí
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
497	[number]	k4	497
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
navrácení	navrácení	k1gNnSc6	navrácení
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
lidu	lid	k1gInSc2	lid
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
dnem	den	k1gInSc7	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
nepřesně	přesně	k6eNd1	přesně
vymezil	vymezit	k5eAaPmAgInS	vymezit
rozsah	rozsah	k1gInSc1	rozsah
majetku	majetek	k1gInSc2	majetek
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSM	SSM	kA	SSM
a	a	k8xC	a
neurčil	určit	k5eNaPmAgInS	určit
přesně	přesně	k6eAd1	přesně
pojem	pojem	k1gInSc1	pojem
držení	držení	k1gNnSc2	držení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
přijala	přijmout	k5eAaPmAgFnS	přijmout
vláda	vláda	k1gFnSc1	vláda
ČSFR	ČSFR	kA	ČSFR
usnesení	usnesení	k1gNnSc1	usnesení
č.	č.	k?	č.
902	[number]	k4	902
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
určila	určit	k5eAaPmAgFnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
"	"	kIx"	"
převezmou	převzít	k5eAaPmIp3nP	převzít
za	za	k7c4	za
stát	stát	k1gInSc1	stát
zmocněnci	zmocněnec	k1gMnPc1	zmocněnec
<g/>
.	.	kIx.	.
</s>
<s>
Majetek	majetek	k1gInSc1	majetek
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSM	SSM	kA	SSM
měly	mít	k5eAaImAgInP	mít
převzít	převzít	k5eAaPmF	převzít
okresní	okresní	k2eAgInPc1d1	okresní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Nedořešená	dořešený	k2eNgFnSc1d1	nedořešená
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
otázka	otázka	k1gFnSc1	otázka
majetku	majetek	k1gInSc2	majetek
bývalých	bývalý	k2eAgMnPc2d1	bývalý
fakultních	fakultní	k2eAgMnPc2d1	fakultní
<g/>
,	,	kIx,	,
celoškolských	celoškolský	k2eAgInPc2d1	celoškolský
<g/>
,	,	kIx,	,
celoučilištních	celoučilištní	k2eAgInPc2d1	celoučilištní
a	a	k8xC	a
celozávodních	celozávodní	k2eAgInPc2d1	celozávodní
výborů	výbor	k1gInPc2	výbor
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
předběžně	předběžně	k6eAd1	předběžně
vyčíslena	vyčíslit	k5eAaPmNgFnS	vyčíslit
celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
majetku	majetek	k1gInSc2	majetek
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSM	SSM	kA	SSM
na	na	k7c4	na
necelé	celý	k2eNgNnSc4d1	necelé
2,9	[number]	k4	2,9
mld.	mld.	k?	mld.
Kčs	Kčs	kA	Kčs
(	(	kIx(	(
<g/>
v	v	k7c6	v
pořizovacích	pořizovací	k2eAgFnPc6d1	pořizovací
cenách	cena	k1gFnPc6	cena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
cca	cca	kA	cca
1	[number]	k4	1
500	[number]	k4	500
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
cca	cca	kA	cca
860	[number]	k4	860
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
a	a	k8xC	a
v	v	k7c6	v
SR	SR	kA	SR
cca	cca	kA	cca
510	[number]	k4	510
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zařízení	zařízení	k1gNnPc4	zařízení
Svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zařízení	zařízení	k1gNnPc4	zařízení
Svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
SM	SM	kA	SM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
akciové	akciový	k2eAgFnPc1d1	akciová
společnosti	společnost	k1gFnPc1	společnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rekreačně	rekreačně	k6eAd1	rekreačně
vzdělávací	vzdělávací	k2eAgNnSc1d1	vzdělávací
zařízení	zařízení	k1gNnSc1	zařízení
československých	československý	k2eAgFnPc2d1	Československá
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
Seč	seč	k1gFnSc4	seč
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
218	[number]	k4	218
941	[number]	k4	941
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1973	[number]	k4	1953-1973
Československý	československý	k2eAgInSc1d1	československý
pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
1973-1982	[number]	k4	1973-1982
Institut	institut	k1gInSc1	institut
PO	Po	kA	Po
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
1983-1992	[number]	k4	1983-1992
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
politická	politický	k2eAgFnSc1d1	politická
škola	škola	k1gFnSc1	škola
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
Seč	seč	k1gFnSc1	seč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
a.	a.	k?	a.
s.	s.	k?	s.
JUNIOR	junior	k1gMnSc1	junior
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
jediného	jediný	k2eAgMnSc2d1	jediný
akcionáře	akcionář	k1gMnSc2	akcionář
(	(	kIx(	(
<g/>
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vilimovský	Vilimovský	k1gMnSc1	Vilimovský
<g/>
)	)	kIx)	)
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
ke	k	k7c3	k
dni	den	k1gInSc3	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
ÚV	ÚV	kA	ÚV
ČSM	ČSM	kA	ČSM
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
182	[number]	k4	182
120	[number]	k4	120
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
a.	a.	k?	a.
s.	s.	k?	s.
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
</s>
</p>
<p>
<s>
Stavocentral	Stavocentrat	k5eAaImAgMnS	Stavocentrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
zřízený	zřízený	k2eAgInSc1d1	zřízený
Národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
Svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
českomoravského	českomoravský	k2eAgInSc2d1	českomoravský
venkova	venkov	k1gInSc2	venkov
(	(	kIx(	(
<g/>
JUVENA	JUVENA	kA	JUVENA
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
Stavocentral	Stavocentral	k1gFnPc2	Stavocentral
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInSc1d1	stavební
podnik	podnik	k1gInSc1	podnik
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
1975-1990	[number]	k4	1975-1990
Stavocentral	Stavocentral	k1gFnPc2	Stavocentral
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInSc1d1	stavební
podnik	podnik	k1gInSc1	podnik
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1990-1993	[number]	k4	1990-1993
STAVOCENTRAL	STAVOCENTRAL	kA	STAVOCENTRAL
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
a.	a.	k?	a.
s.	s.	k?	s.
Stavocentral	Stavocentral	k1gFnSc2	Stavocentral
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
a.	a.	k?	a.
s.	s.	k?	s.
Stavocentral	Stavocentral	k1gFnSc2	Stavocentral
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
AGM	AGM	kA	AGM
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Agentura	agentura	k1gFnSc1	agentura
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
podnik	podnik	k1gInSc4	podnik
služeb	služba	k1gFnPc2	služba
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
58	[number]	k4	58
803	[number]	k4	803
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
přešlo	přejít	k5eAaPmAgNnS	přejít
s	s	k7c7	s
převodem	převod	k1gInSc7	převod
CKM	CKM	kA	CKM
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
764	[number]	k4	764
633	[number]	k4	633
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
)	)	kIx)	)
i	i	k8xC	i
majetkové	majetkový	k2eAgNnSc4d1	majetkové
právo	právo	k1gNnSc4	právo
CKM	CKM	kA	CKM
v	v	k7c6	v
a.	a.	k?	a.
s.	s.	k?	s.
EKOS	EKOS	kA	EKOS
<g/>
,	,	kIx,	,
zřízené	zřízený	k2eAgFnPc1d1	zřízená
jako	jako	k8xC	jako
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
CKM	CKM	kA	CKM
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
mládeže	mládež	k1gFnSc2	mládež
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
různé	různý	k2eAgInPc4d1	různý
turistické	turistický	k2eAgInPc4d1	turistický
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hotel	hotel	k1gInSc1	hotel
Globus	globus	k1gInSc1	globus
v	v	k7c4	v
Praze-Roztyly	Praze-Roztyl	k1gInPc4	Praze-Roztyl
<g/>
,	,	kIx,	,
Hotely	hotel	k1gInPc4	hotel
"	"	kIx"	"
<g/>
Alice	Alice	k1gFnSc1	Alice
<g/>
"	"	kIx"	"
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
Hotel	hotel	k1gInSc1	hotel
"	"	kIx"	"
<g/>
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
<g/>
"	"	kIx"	"
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc1	hotel
"	"	kIx"	"
<g/>
Fit-Fun	Fit-Fun	k1gInSc1	Fit-Fun
<g/>
"	"	kIx"	"
Harachov	Harachov	k1gInSc1	Harachov
<g/>
,	,	kIx,	,
turistické	turistický	k2eAgNnSc1d1	turistické
středisko	středisko	k1gNnSc1	středisko
Kolovraty	kolovrat	k1gInPc7	kolovrat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Prknovka	Prknovka	k1gFnSc1	Prknovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgNnPc4d1	další
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Dům	dům	k1gInSc1	dům
československých	československý	k2eAgFnPc2d1	Československá
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
6	[number]	k4	6
676	[number]	k4	676
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
</s>
</p>
<p>
<s>
M-Art	M-Art	k1gInSc1	M-Art
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
7	[number]	k4	7
736	[number]	k4	736
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přílohy	příloha	k1gFnSc2	příloha
č.	č.	k?	č.
1	[number]	k4	1
k	k	k7c3	k
usnesení	usnesení	k1gNnSc3	usnesení
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
ze	z	k7c2	z
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
č.	č.	k?	č.
598	[number]	k4	598
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
zařízení	zařízení	k1gNnSc4	zařízení
bývalého	bývalý	k2eAgMnSc2d1	bývalý
SSM	SSM	kA	SSM
M	M	kA	M
–	–	k?	–
ART	ART	kA	ART
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
likvidací	likvidace	k1gFnSc7	likvidace
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
platnými	platný	k2eAgInPc7d1	platný
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnik	podnik	k1gInSc1	podnik
služeb	služba	k1gFnPc2	služba
mládeže	mládež	k1gFnSc2	mládež
Bratislava	Bratislava	k1gFnSc1	Bratislava
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
služeb	služba	k1gFnPc2	služba
mládeže	mládež	k1gFnSc2	mládež
Bratislava	Bratislava	k1gFnSc1	Bratislava
</s>
</p>
<p>
<s>
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Smena	Smen	k1gInSc2	Smen
Bratislava	Bratislava	k1gFnSc1	Bratislava
</s>
</p>
<p>
<s>
Federální	federální	k2eAgNnSc1d1	federální
koordinační	koordinační	k2eAgNnSc1d1	koordinační
centrum	centrum	k1gNnSc1	centrum
SM	SM	kA	SM
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
109	[number]	k4	109
093	[number]	k4	093
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ČÚV	ČÚV	kA	ČÚV
SSM	SSM	kA	SSM
a	a	k8xC	a
SÚV	SÚV	kA	SÚV
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zenitcentrum	Zenitcentrum	k1gNnSc1	Zenitcentrum
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
zřízené	zřízený	k2eAgFnSc2d1	zřízená
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
vědeckotechnické	vědeckotechnický	k2eAgFnSc2d1	vědeckotechnická
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
dovážel	dovážet	k5eAaImAgInS	dovážet
a	a	k8xC	a
distribuoval	distribuovat	k5eAaBmAgInS	distribuovat
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
techniku	technika	k1gFnSc4	technika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vědeckofantastická	vědeckofantastický	k2eAgFnSc1d1	vědeckofantastická
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
PO	Po	kA	Po
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Zenitcentrum	Zenitcentrum	k1gNnSc4	Zenitcentrum
činil	činit	k5eAaImAgInS	činit
předběžně	předběžně	k6eAd1	předběžně
vyčíslený	vyčíslený	k2eAgInSc1d1	vyčíslený
stav	stav	k1gInSc1	stav
majetku	majetek	k1gInSc2	majetek
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
61	[number]	k4	61
155	[number]	k4	155
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
organizace	organizace	k1gFnSc2	organizace
se	s	k7c7	s
státem	stát	k1gInSc7	stát
nespolupracovalo	spolupracovat	k5eNaImAgNnS	spolupracovat
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
se	se	k3xPyFc4	se
sepisoval	sepisovat	k5eAaImAgInS	sepisovat
stav	stav	k1gInSc1	stav
pohledávek	pohledávka	k1gFnPc2	pohledávka
a	a	k8xC	a
závazků	závazek	k1gInPc2	závazek
(	(	kIx(	(
<g/>
absence	absence	k1gFnSc1	absence
faktur	faktura	k1gFnPc2	faktura
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
náležitostí	náležitost	k1gFnPc2	náležitost
<g/>
,	,	kIx,	,
nevyúčtování	nevyúčtování	k1gNnSc1	nevyúčtování
faktur	faktura	k1gFnPc2	faktura
<g/>
,	,	kIx,	,
neprůkaznost	neprůkaznost	k1gFnSc1	neprůkaznost
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
obchodování	obchodování	k1gNnSc2	obchodování
<g/>
,	,	kIx,	,
atd	atd	kA	atd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skutečnosti	skutečnost	k1gFnPc1	skutečnost
nasvědčovaly	nasvědčovat	k5eAaImAgFnP	nasvědčovat
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Státu	stát	k1gInSc3	stát
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
došetřit	došetřit	k5eAaPmF	došetřit
přebírání	přebírání	k1gNnSc4	přebírání
majetku	majetek	k1gInSc2	majetek
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Zenitcentru	Zenitcentrum	k1gNnSc6	Zenitcentrum
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
střediska	středisko	k1gNnSc2	středisko
Ing.	ing.	kA	ing.
Střítecký	Střítecký	k2eAgInSc1d1	Střítecký
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
zástupce	zástupce	k1gMnSc1	zástupce
i	i	k8xC	i
ekonomický	ekonomický	k2eAgMnSc1d1	ekonomický
náměstek	náměstek	k1gMnSc1	náměstek
dali	dát	k5eAaPmAgMnP	dát
výpověď	výpověď	k1gFnSc4	výpověď
a	a	k8xC	a
podnik	podnik	k1gInSc4	podnik
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
bez	bez	k7c2	bez
statutárního	statutární	k2eAgMnSc2d1	statutární
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
inventarizace	inventarizace	k1gFnSc2	inventarizace
zahraničně	zahraničně	k6eAd1	zahraničně
obchodní	obchodní	k2eAgFnSc2d1	obchodní
činnosti	činnost	k1gFnSc2	činnost
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
neoprávněné	oprávněný	k2eNgNnSc1d1	neoprávněné
snížení	snížení	k1gNnSc1	snížení
majetku	majetek	k1gInSc2	majetek
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
zmocněnce	zmocněnec	k1gMnSc2	zmocněnec
JUDr.	JUDr.	kA	JUDr.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Absolóna	Absolón	k1gMnSc2	Absolón
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
plnil	plnit	k5eAaImAgInS	plnit
úkoly	úkol	k1gInPc4	úkol
jen	jen	k9	jen
formálně	formálně	k6eAd1	formálně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
funkce	funkce	k1gFnSc1	funkce
31	[number]	k4	31
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
zmocněnkyně	zmocněnkyně	k1gFnSc2	zmocněnkyně
Ing.	ing.	kA	ing.
L.	L.	kA	L.
Klofová	Klofový	k2eAgNnPc4d1	Klofový
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
z	z	k7c2	z
části	část	k1gFnSc2	část
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
pomalém	pomalý	k2eAgInSc6d1	pomalý
průběhu	průběh	k1gInSc6	průběh
inventarizace	inventarizace	k1gFnSc2	inventarizace
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
platební	platební	k2eAgFnSc6d1	platební
neschopnosti	neschopnost	k1gFnSc6	neschopnost
a	a	k8xC	a
FMF	FMF	kA	FMF
tak	tak	k9	tak
muselo	muset	k5eAaImAgNnS	muset
organizaci	organizace	k1gFnSc4	organizace
poskytnout	poskytnout	k5eAaPmF	poskytnout
půjčku	půjčka	k1gFnSc4	půjčka
na	na	k7c4	na
výplaty	výplata	k1gFnPc4	výplata
mezd	mzda	k1gFnPc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ČSFR	ČSFR	kA	ČSFR
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
schůzi	schůze	k1gFnSc6	schůze
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
nakonec	nakonec	k9	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
likvidaci	likvidace	k1gFnSc4	likvidace
Zenitcentra	Zenitcentrum	k1gNnSc2	Zenitcentrum
a	a	k8xC	a
o	o	k7c6	o
bezodkladném	bezodkladný	k2eAgNnSc6d1	bezodkladné
ustavení	ustavení	k1gNnSc6	ustavení
likvidátora	likvidátor	k1gMnSc2	likvidátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Molek	Molek	k1gMnSc1	Molek
<g/>
:	:	kIx,	:
Navrácení	navrácení	k1gNnSc1	navrácení
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Kontroly	kontrola	k1gFnPc1	kontrola
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994-1996	[number]	k4	1994-1996
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
hospodářství	hospodářství	k1gNnSc1	hospodářství
(	(	kIx(	(
<g/>
právní	právní	k2eAgMnSc1d1	právní
předchůdce	předchůdce	k1gMnSc1	předchůdce
MHPR	MHPR	kA	MHPR
<g/>
)	)	kIx)	)
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
pololetí	pololetí	k1gNnSc2	pololetí
1993	[number]	k4	1993
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
organizacím	organizace	k1gFnPc3	organizace
původní	původní	k2eAgFnSc7d1	původní
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neuvedlo	uvést	k5eNaPmAgNnS	uvést
náležitosti	náležitost	k1gFnPc4	náležitost
do	do	k7c2	do
souladu	soulad	k1gInSc2	soulad
s	s	k7c7	s
HZ	Hz	kA	Hz
a	a	k8xC	a
Obchodním	obchodní	k2eAgInSc7d1	obchodní
zákoníkem	zákoník	k1gInSc7	zákoník
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
porušilo	porušit	k5eAaPmAgNnS	porušit
povinnost	povinnost	k1gFnSc4	povinnost
využívat	využívat	k5eAaPmF	využívat
právní	právní	k2eAgInPc4d1	právní
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
že	že	k8xS	že
transformací	transformace	k1gFnSc7	transformace
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zařízení	zařízení	k1gNnPc2	zařízení
na	na	k7c6	na
akciové	akciový	k2eAgFnSc6d1	akciová
společnosti	společnost	k1gFnSc6	společnost
(	(	kIx(	(
<g/>
JUNIOR	junior	k1gMnSc1	junior
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
a	a	k8xC	a
Stavocentral	Stavocentral	k1gFnSc1	Stavocentral
<g/>
)	)	kIx)	)
postupovalo	postupovat	k5eAaImAgNnS	postupovat
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
právním	právní	k2eAgInSc6d1	právní
řádu	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Návazně	návazně	k6eAd1	návazně
pak	pak	k6eAd1	pak
postupovalo	postupovat	k5eAaImAgNnS	postupovat
opětovně	opětovně	k6eAd1	opětovně
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
právním	právní	k2eAgInSc6d1	právní
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
když	když	k8xS	když
převedlo	převést	k5eAaPmAgNnS	převést
místo	místo	k1gNnSc4	místo
akcií	akcie	k1gFnPc2	akcie
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
"	"	kIx"	"
<g/>
Fondu	fond	k1gInSc2	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
FDM	FDM	kA	FDM
<g/>
)	)	kIx)	)
vklady	vklad	k1gInPc1	vklad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nepřevedlo	převést	k5eNaPmAgNnS	převést
právo	právo	k1gNnSc1	právo
hospodaření	hospodaření	k1gNnSc2	hospodaření
s	s	k7c7	s
majetkovým	majetkový	k2eAgNnSc7d1	majetkové
právem	právo	k1gNnSc7	právo
plynoucím	plynoucí	k2eAgNnSc7d1	plynoucí
z	z	k7c2	z
majetkového	majetkový	k2eAgInSc2d1	majetkový
vkladu	vklad	k1gInSc2	vklad
Cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
mládeže	mládež	k1gFnSc2	mládež
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,9	[number]	k4	1,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
v	v	k7c6	v
EKOS	EKOS	kA	EKOS
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
CKM	CKM	kA	CKM
nehospodaří	hospodařit	k5eNaImIp3nS	hospodařit
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
řádného	řádný	k2eAgMnSc2d1	řádný
hospodáře	hospodář	k1gMnSc2	hospodář
<g/>
.	.	kIx.	.
<g/>
Akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
"	"	kIx"	"
<g/>
EKOS	EKOS	kA	EKOS
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
JUNIOR	junior	k1gMnSc1	junior
centrum	centrum	k1gNnSc4	centrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Stavocentral	Stavocentral	k1gFnSc1	Stavocentral
<g/>
"	"	kIx"	"
nevydaly	vydat	k5eNaPmAgFnP	vydat
akcie	akcie	k1gFnPc4	akcie
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
ani	ani	k8xC	ani
zatímní	zatímní	k2eAgInSc1d1	zatímní
list	list	k1gInSc1	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavocentral	Stavocentrat	k5eAaPmAgMnS	Stavocentrat
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
obchodních	obchodní	k2eAgInPc6d1	obchodní
vztazích	vztah	k1gInPc6	vztah
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
jako	jako	k8xC	jako
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
rejstříku	rejstřík	k1gInSc6	rejstřík
zapsán	zapsat	k5eAaPmNgMnS	zapsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EKOS	EKOS	kA	EKOS
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
nakládala	nakládat	k5eAaImAgFnS	nakládat
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
majetkem	majetek	k1gInSc7	majetek
(	(	kIx(	(
<g/>
Podnik	podnik	k1gInSc1	podnik
bytového	bytový	k2eAgNnSc2d1	bytové
hospodářství	hospodářství	k1gNnSc2	hospodářství
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
opěrná	opěrný	k2eAgFnSc1d1	opěrná
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
pozemky	pozemek	k1gInPc1	pozemek
p.	p.	k?	p.
č.	č.	k?	č.
103	[number]	k4	103
a	a	k8xC	a
p.	p.	k?	p.
č.	č.	k?	č.
98	[number]	k4	98
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
353	[number]	k4	353
m	m	kA	m
<g/>
2	[number]	k4	2
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
neoprávněně	oprávněně	k6eNd1	oprávněně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc4	svaz
mladých	mladý	k1gMnPc2	mladý
hospodařil	hospodařit	k5eAaImAgMnS	hospodařit
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
převod	převod	k1gInSc1	převod
práva	právo	k1gNnSc2	právo
hospodaření	hospodaření	k1gNnSc2	hospodaření
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
majetkem	majetek	k1gInSc7	majetek
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
práva	právo	k1gNnSc2	právo
možný	možný	k2eAgInSc1d1	možný
jen	jen	k6eAd1	jen
mezi	mezi	k7c7	mezi
státními	státní	k2eAgFnPc7d1	státní
organizacemi	organizace	k1gFnPc7	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
411	[number]	k4	411
v	v	k7c6	v
Praze-Holešovicích	Praze-Holešovice	k1gFnPc6	Praze-Holešovice
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
231	[number]	k4	231
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
a	a	k8xC	a
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
268	[number]	k4	268
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Kontrolní	kontrolní	k2eAgInPc4d1	kontrolní
závěry	závěr	k1gInPc4	závěr
z	z	k7c2	z
kontroly	kontrola	k1gFnSc2	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
Věstník	věstník	k1gMnSc1	věstník
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
===	===	k?	===
Správy	správa	k1gFnSc2	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
část	část	k1gFnSc1	část
majetku	majetek	k1gInSc2	majetek
SSM	SSM	kA	SSM
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
ve	v	k7c6	v
správách	správa	k1gFnPc6	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
SÚZ	SÚZ	kA	SÚZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
představoval	představovat	k5eAaImAgMnS	představovat
majetek	majetek	k1gInSc4	majetek
SÚZ	SÚZ	kA	SÚZ
cca	cca	kA	cca
723	[number]	k4	723
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SR	SR	kA	SR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
majetek	majetek	k1gInSc1	majetek
SÚZ	SÚZ	kA	SÚZ
včetně	včetně	k7c2	včetně
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
"	"	kIx"	"
<g/>
Smena	Smeno	k1gNnSc2	Smeno
<g/>
"	"	kIx"	"
činil	činit	k5eAaImAgMnS	činit
cca	cca	kA	cca
243	[number]	k4	243
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
majetku	majetek	k1gInSc6	majetek
cca	cca	kA	cca
48	[number]	k4	48
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Správy	správa	k1gFnPc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
Svazu	svaz	k1gInSc2	svaz
mladých	mladý	k1gMnPc2	mladý
(	(	kIx(	(
<g/>
bývalých	bývalý	k2eAgMnPc2d1	bývalý
SÚZ	SÚZ	kA	SÚZ
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
přešly	přejít	k5eAaPmAgFnP	přejít
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
na	na	k7c6	na
základě	základ	k1gInSc6	základ
§	§	k?	§
1	[number]	k4	1
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
497	[number]	k4	497
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
majetku	majetek	k1gInSc2	majetek
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
lidu	lid	k1gInSc2	lid
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
17	[number]	k4	17
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
JUNIA	JUNIA	kA	JUNIA
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
došlo	dojít	k5eAaPmAgNnS	dojít
zápisem	zápis	k1gInSc7	zápis
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
vložila	vložit	k5eAaPmAgFnS	vložit
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
MH	MH	kA	MH
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
vklad	vklad	k1gInSc4	vklad
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
Kč	Kč	kA	Kč
jako	jako	k8xC	jako
základní	základní	k2eAgNnSc1d1	základní
jmění	jmění	k1gNnSc1	jmění
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgFnPc1d1	organizační
složky	složka	k1gFnPc1	složka
(	(	kIx(	(
<g/>
7	[number]	k4	7
odštěpných	odštěpný	k2eAgInPc2d1	odštěpný
závodů	závod	k1gInPc2	závod
JUNIE	JUNIE	kA	JUNIE
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
:	:	kIx,	:
odštěpné	odštěpný	k2eAgInPc1d1	odštěpný
závody	závod	k1gInPc1	závod
JUNIA	JUNIA	kA	JUNIA
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
dne	den	k1gInSc2	den
29.7	[number]	k4	29.7
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
NKÚ	NKÚ	kA	NKÚ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
kontroly	kontrola	k1gFnSc2	kontrola
nebyl	být	k5eNaImAgMnS	být
mezi	mezi	k7c7	mezi
Fondem	fond	k1gInSc7	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
JUNIÍ	JUNIÍ	kA	JUNIÍ
učiněn	učinit	k5eAaPmNgInS	učinit
právní	právní	k2eAgInSc1d1	právní
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
obchodní	obchodní	k2eAgFnSc4d1	obchodní
společnost	společnost	k1gFnSc4	společnost
opravňoval	opravňovat	k5eAaImAgMnS	opravňovat
k	k	k7c3	k
nakládáni	nakládán	k2eAgMnPc1d1	nakládán
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
bývalého	bývalý	k2eAgNnSc2d1	bývalé
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
SM	SM	kA	SM
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
SM	SM	kA	SM
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
</s>
</p>
<p>
<s>
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Jihomoravská	jihomoravský	k2eAgFnSc1d1	Jihomoravská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Severomoravská	severomoravský	k2eAgFnSc1d1	Severomoravská
správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
Středoslovenského	středoslovenský	k2eAgInSc2d1	středoslovenský
kraje	kraj	k1gInSc2	kraj
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
Východoslovenského	východoslovenský	k2eAgInSc2d1	východoslovenský
kraje	kraj	k1gInSc2	kraj
Košice	Košice	k1gInPc1	Košice
</s>
</p>
<p>
<s>
===	===	k?	===
Fond	fond	k1gInSc1	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
113	[number]	k4	113
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
Fond	fond	k1gInSc1	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
FDM	FDM	kA	FDM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
byl	být	k5eAaImAgInS	být
svěřen	svěřen	k2eAgInSc1d1	svěřen
majetek	majetek	k1gInSc1	majetek
bývalého	bývalý	k2eAgNnSc2d1	bývalé
SSM	SSM	kA	SSM
<g/>
.	.	kIx.	.
</s>
<s>
Kontroly	kontrola	k1gFnPc1	kontrola
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994-1996	[number]	k4	1994-1996
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fond	fond	k1gInSc1	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
převzal	převzít	k5eAaPmAgInS	převzít
od	od	k7c2	od
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
místo	místo	k7c2	místo
akcií	akcie	k1gFnPc2	akcie
(	(	kIx(	(
<g/>
založených	založený	k2eAgFnPc2d1	založená
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
AGM	AGM	kA	AGM
<g/>
,	,	kIx,	,
CKM	CKM	kA	CKM
<g/>
,	,	kIx,	,
JUNIOR	junior	k1gMnSc1	junior
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
a	a	k8xC	a
Stavocentral	Stavocentral	k1gFnSc1	Stavocentral
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
hodnotu	hodnota	k1gFnSc4	hodnota
vkladu	vklad	k1gInSc2	vklad
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
doložena	doložit	k5eAaPmNgFnS	doložit
výpisy	výpis	k1gInPc4	výpis
z	z	k7c2	z
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
jako	jako	k8xC	jako
zakladateli	zakladatel	k1gMnPc7	zakladatel
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
nebyly	být	k5eNaImAgFnP	být
akcie	akcie	k1gFnPc1	akcie
odevzdány	odevzdat	k5eAaPmNgFnP	odevzdat
hned	hned	k6eAd1	hned
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
přímo	přímo	k6eAd1	přímo
Fondu	fond	k1gInSc3	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
FDM	FDM	kA	FDM
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
uvedených	uvedený	k2eAgFnPc2d1	uvedená
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
(	(	kIx(	(
<g/>
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
až	až	k9	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
pololetí	pololetí	k1gNnSc4	pololetí
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
právního	právní	k2eAgInSc2d1	právní
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
Fond	fond	k1gInSc1	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
zrušen	zrušit	k5eAaPmNgMnS	zrušit
a	a	k8xC	a
dán	dát	k5eAaPmNgMnS	dát
do	do	k7c2	do
likvidace	likvidace	k1gFnSc2	likvidace
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Ing.	ing.	kA	ing.
Petr	Petr	k1gMnSc1	Petr
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
přešla	přejít	k5eAaPmAgFnS	přejít
práva	práv	k2eAgFnSc1d1	práva
a	a	k8xC	a
povinnosti	povinnost	k1gFnSc3	povinnost
zrušeného	zrušený	k2eAgInSc2d1	zrušený
fondu	fond	k1gInSc2	fond
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
<g/>
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
posledními	poslední	k2eAgMnPc7d1	poslední
správci	správce	k1gMnPc7	správce
podivně	podivně	k6eAd1	podivně
rozprodaného	rozprodaný	k2eAgInSc2d1	rozprodaný
majetku	majetek	k1gInSc2	majetek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Petrem	Petr	k1gMnSc7	Petr
Kučerou	Kučera	k1gMnSc7	Kučera
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
společnosti	společnost	k1gFnSc2	společnost
fondu	fond	k1gInSc2	fond
AGM	AGM	kA	AGM
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pavlem	Pavel	k1gMnSc7	Pavel
Žákem	Žák	k1gMnSc7	Žák
a	a	k8xC	a
</s>
</p>
<p>
<s>
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Hanákem	Hanák	k1gMnSc7	Hanák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Nejkřiklavější	křiklavý	k2eAgInSc1d3	nejkřiklavější
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
hotelu	hotel	k1gInSc2	hotel
Belária	Belárium	k1gNnSc2	Belárium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
nad	nad	k7c7	nad
Moravicí	Moravice	k1gFnSc7	Moravice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
kontroly	kontrola	k1gFnSc2	kontrola
NKÚ	NKÚ	kA	NKÚ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
cenu	cena	k1gFnSc4	cena
nejméně	málo	k6eAd3	málo
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
prodán	prodat	k5eAaPmNgMnS	prodat
za	za	k7c4	za
24	[number]	k4	24
<g/>
,	,	kIx,	,
5	[number]	k4	5
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
při	při	k7c6	při
započtení	započtení	k1gNnSc6	započtení
údajného	údajný	k2eAgNnSc2d1	údajné
zhodnocení	zhodnocení	k1gNnSc2	zhodnocení
nájemcem	nájemce	k1gMnSc7	nájemce
<g/>
..	..	k?	..
Za	za	k7c4	za
nezákonný	zákonný	k2eNgInSc4d1	nezákonný
a	a	k8xC	a
nevýhodný	výhodný	k2eNgInSc4d1	nevýhodný
prodej	prodej	k1gInSc4	prodej
nemovitostí	nemovitost	k1gFnPc2	nemovitost
Fondu	fond	k1gInSc2	fond
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
FDM	FDM	kA	FDM
<g/>
)	)	kIx)	)
dostal	dostat	k5eAaPmAgMnS	dostat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
likvidátor	likvidátor	k1gMnSc1	likvidátor
Pavel	Pavel	k1gMnSc1	Pavel
Žák	Žák	k1gMnSc1	Žák
trest	trest	k1gInSc1	trest
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
nepodmíněně	podmíněně	k6eNd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgMnSc1d1	někdejší
likvidátor	likvidátor	k1gMnSc1	likvidátor
fondu	fond	k1gInSc2	fond
způsobil	způsobit	k5eAaPmAgMnS	způsobit
podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
soudu	soud	k1gInSc2	soud
převodem	převod	k1gInSc7	převod
36	[number]	k4	36
budov	budova	k1gFnPc2	budova
či	či	k8xC	či
pozemků	pozemek	k1gInPc2	pozemek
škodu	škoda	k1gFnSc4	škoda
přes	přes	k7c4	přes
215	[number]	k4	215
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Budova	budova	k1gFnSc1	budova
a	a	k8xC	a
2	[number]	k4	2
pozemky	pozemek	k1gInPc1	pozemek
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Teplice	teplice	k1gFnSc2	teplice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
ÚZSVM	ÚZSVM	kA	ÚZSVM
květen	květen	k1gInSc4	květen
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
==	==	k?	==
Následnické	následnický	k2eAgFnSc2d1	následnická
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nedistancovali	distancovat	k5eNaBmAgMnP	distancovat
od	od	k7c2	od
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
socialismus	socialismus	k1gInSc4	socialismus
nezažili	zažít	k5eNaPmAgMnP	zažít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
je	on	k3xPp3gFnPc4	on
socialistické	socialistický	k2eAgFnPc4d1	socialistická
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
několik	několik	k4yIc4	několik
svazů	svaz	k1gInPc2	svaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
mladých	mladý	k2eAgMnPc2d1	mladý
komunistů	komunista	k1gMnPc2	komunista
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
===	===	k?	===
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Socialistický	socialistický	k2eAgInSc1d1	socialistický
zväz	zväz	k1gInSc1	zväz
mladých	mladý	k2eAgMnPc2d1	mladý
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
serveru	server	k1gInSc6	server
Totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stanovy	stanova	k1gFnPc1	stanova
SSM	SSM	kA	SSM
</s>
</p>
<p>
<s>
Metodické	metodický	k2eAgInPc1d1	metodický
listy	list	k1gInPc1	list
pro	pro	k7c4	pro
tělovýchovnou	tělovýchovný	k2eAgFnSc4d1	Tělovýchovná
a	a	k8xC	a
brannou	branný	k2eAgFnSc4d1	Branná
činnost	činnost	k1gFnSc4	činnost
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
český	český	k2eAgInSc4d1	český
ÚV	ÚV	kA	ÚV
SSM	SSM	kA	SSM
1987	[number]	k4	1987
</s>
</p>
