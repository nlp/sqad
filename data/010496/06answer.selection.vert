<s>
Socialistický	socialistický	k2eAgInSc1d1	socialistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SSM	SSM	kA	SSM
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
organizace	organizace	k1gFnSc1	organizace
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
sdružující	sdružující	k2eAgMnPc4d1	sdružující
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc4d1	fungující
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1970	[number]	k4	1970
až	až	k9	až
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
