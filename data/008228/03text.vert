<p>
<s>
O	o	k7c6	o
vepříku	vepřík	k1gMnSc6	vepřík
a	a	k8xC	a
kůzleti	kůzle	k1gNnSc6	kůzle
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
loutkový	loutkový	k2eAgInSc1d1	loutkový
seriál	seriál	k1gInSc1	seriál
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
předlohy	předloha	k1gFnSc2	předloha
Václava	Václav	k1gMnSc2	Václav
Čtvrtka	čtvrtek	k1gInSc2	čtvrtek
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
třináct	třináct	k4xCc4	třináct
desetiminutových	desetiminutový	k2eAgInPc2d1	desetiminutový
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
černobílém	černobílý	k2eAgNnSc6d1	černobílé
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
režirovala	režirovat	k5eAaBmAgFnS	režirovat
Libuše	Libuše	k1gFnSc1	Libuše
Koutná	Koutná	k1gFnSc1	Koutná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
vepříkovi	vepřík	k1gMnSc6	vepřík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
lenivý	lenivý	k2eAgMnSc1d1	lenivý
popleta	popleta	k1gMnSc1	popleta
a	a	k8xC	a
kůzleti	kůzle	k1gNnSc6	kůzle
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
pravý	pravý	k2eAgInSc1d1	pravý
opak	opak	k1gInSc1	opak
<g/>
,	,	kIx,	,
pracovitý	pracovitý	k2eAgInSc1d1	pracovitý
a	a	k8xC	a
moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
protichůdným	protichůdný	k2eAgFnPc3d1	protichůdná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
obou	dva	k4xCgFnPc2	dva
hrdinů	hrdina	k1gMnPc2	hrdina
prožívají	prožívat	k5eAaImIp3nP	prožívat
mnoho	mnoho	k4c4	mnoho
veselých	veselý	k2eAgFnPc2d1	veselá
příhod	příhoda	k1gFnPc2	příhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
dílů	díl	k1gInPc2	díl
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
vepřík	vepřík	k1gMnSc1	vepřík
uschl	uschnout	k5eAaPmAgMnS	uschnout
docela	docela	k6eAd1	docela
na	na	k7c4	na
placičku	placička	k1gFnSc4	placička
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
vepřík	vepřík	k1gMnSc1	vepřík
létat	létat	k5eAaImF	létat
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
stonali	stonat	k5eAaImAgMnP	stonat
na	na	k7c4	na
sáně	sáně	k1gFnPc4	sáně
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
slavili	slavit	k5eAaImAgMnP	slavit
Vánoce	Vánoce	k1gFnPc1	Vánoce
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
pletli	plést	k5eAaImAgMnP	plést
svetr	svetr	k1gInSc4	svetr
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
koupili	koupit	k5eAaPmAgMnP	koupit
trumpetu	trumpeta	k1gFnSc4	trumpeta
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
louskali	louskat	k5eAaImAgMnP	louskat
ořechy	ořech	k1gInPc1	ořech
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
zvedali	zvedat	k5eAaImAgMnP	zvedat
činku	činka	k1gFnSc4	činka
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
hledali	hledat	k5eAaImAgMnP	hledat
houby	houby	k6eAd1	houby
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
pouštěli	pouštět	k5eAaImAgMnP	pouštět
prasátka	prasátko	k1gNnPc4	prasátko
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
zasadili	zasadit	k5eAaPmAgMnP	zasadit
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
vepřík	vepřík	k1gMnSc1	vepřík
a	a	k8xC	a
kůzle	kůzle	k1gNnSc4	kůzle
malovali	malovat	k5eAaImAgMnP	malovat
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
k	k	k7c3	k
vepříku	vepřík	k1gMnSc3	vepřík
a	a	k8xC	a
kůzleti	kůzle	k1gNnSc6	kůzle
přišlo	přijít	k5eAaPmAgNnS	přijít
padací	padací	k2eAgNnSc1d1	padací
house	house	k1gNnSc1	house
</s>
</p>
