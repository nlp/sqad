<s>
Neon	neon	k1gInSc1	neon
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Neonum	Neonum	k1gNnSc1	Neonum
<g/>
)	)	kIx)	)
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ne	ne	k9	ne
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
2	[number]	k4	2
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
nereaktivní	reaktivní	k2eNgFnSc1d1	nereaktivní
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
neonu	neon	k1gInSc2	neon
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
litru	litr	k1gInSc6	litr
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
10,4	[number]	k4	10,4
ml	ml	kA	ml
neonu	neon	k1gInSc2	neon
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
neon	neon	k1gInSc4	neon
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c6	na
aktivním	aktivní	k2eAgNnSc6d1	aktivní
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
ionizovaném	ionizovaný	k2eAgInSc6d1	ionizovaný
stavu	stav	k1gInSc6	stav
intenzivně	intenzivně	k6eAd1	intenzivně
září	zářit	k5eAaImIp3nS	zářit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
výbojkách	výbojka	k1gFnPc6	výbojka
šarlatovou	šarlatový	k2eAgFnSc4d1	šarlatová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
objevil	objevit	k5eAaPmAgInS	objevit
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Rayleightem	Rayleight	k1gInSc7	Rayleight
argon	argon	k1gInSc1	argon
a	a	k8xC	a
správně	správně	k6eAd1	správně
oba	dva	k4xCgInPc4	dva
plyny	plyn	k1gInPc4	plyn
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
mu	on	k3xPp3gMnSc3	on
volné	volný	k2eAgNnSc1d1	volné
místo	místo	k1gNnSc1	místo
před	před	k7c7	před
a	a	k8xC	a
za	za	k7c7	za
argonem	argon	k1gInSc7	argon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
volných	volný	k2eAgNnPc2d1	volné
míst	místo	k1gNnPc2	místo
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
neon	neon	k1gInSc4	neon
a	a	k8xC	a
krypton	krypton	k1gInSc4	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
Williamem	William	k1gInSc7	William
Ramsayem	Ramsay	k1gInSc7	Ramsay
a	a	k8xC	a
Morrisem	Morris	k1gInSc7	Morris
Traversem	travers	k1gInSc7	travers
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
využil	využít	k5eAaPmAgMnS	využít
nové	nový	k2eAgFnPc4d1	nová
metody	metoda	k1gFnPc4	metoda
frakční	frakční	k2eAgFnSc2d1	frakční
destilace	destilace	k1gFnSc2	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
neonem	neon	k1gInSc7	neon
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
krypton	krypton	k1gInSc1	krypton
a	a	k8xC	a
xenon	xenon	k1gInSc1	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
dvanáctiletým	dvanáctiletý	k2eAgMnSc7d1	dvanáctiletý
Ramseyovým	Ramseyův	k2eAgMnSc7d1	Ramseyův
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
díval	dívat	k5eAaImAgMnS	dívat
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
<g/>
,	,	kIx,	,
šarlatově	šarlatově	k6eAd1	šarlatově
červené	červený	k2eAgNnSc1d1	červené
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vycházelo	vycházet	k5eAaImAgNnS	vycházet
ze	z	k7c2	z
spektrální	spektrální	k2eAgFnSc2d1	spektrální
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
tento	tento	k3xDgInSc4	tento
prvek	prvek	k1gInSc4	prvek
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
jako	jako	k8xS	jako
nový	nový	k2eAgInSc4d1	nový
-	-	kIx~	-
neon	neon	k1gInSc4	neon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
0,001	[number]	k4	0,001
8	[number]	k4	8
%	%	kIx~	%
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
100	[number]	k4	100
litrech	litr	k1gInPc6	litr
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1,82	[number]	k4	1,82
ml	ml	kA	ml
neonu	neon	k1gInSc6	neon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
po	po	k7c6	po
argonu	argon	k1gInSc6	argon
druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
vzácným	vzácný	k2eAgInSc7d1	vzácný
plynem	plyn	k1gInSc7	plyn
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
pátým	pátý	k4xOgInSc7	pátý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
plynem	plyn	k1gInSc7	plyn
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
získáván	získáván	k2eAgInSc1d1	získáván
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
získávání	získávání	k1gNnSc2	získávání
neonu	neon	k1gInSc2	neon
je	být	k5eAaImIp3nS	být
frakční	frakční	k2eAgFnSc1d1	frakční
adsorpce	adsorpce	k1gFnSc1	adsorpce
na	na	k7c4	na
aktivní	aktivní	k2eAgNnSc4d1	aktivní
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
kapalného	kapalný	k2eAgInSc2d1	kapalný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
neonu	neon	k1gInSc2	neon
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
několik	několik	k4yIc1	několik
torrů	torr	k1gInPc2	torr
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
%	%	kIx~	%
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
světelné	světelný	k2eAgNnSc1d1	světelné
záření	záření	k1gNnSc1	záření
oranžově-červené	oranžově-červený	k2eAgFnSc2d1	oranžově-červený
(	(	kIx(	(
<g/>
šarlatové	šarlatový	k2eAgFnSc2d1	šarlatová
<g/>
)	)	kIx)	)
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pří	přít	k5eAaImIp3nS	přít
výrobě	výroba	k1gFnSc3	výroba
výbojek	výbojka	k1gFnPc2	výbojka
tzv.	tzv.	kA	tzv.
neonek	neonka	k1gFnPc2	neonka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
osvětlovací	osvětlovací	k2eAgNnPc1d1	osvětlovací
tělesa	těleso	k1gNnPc1	těleso
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc1d1	různý
světelné	světelný	k2eAgInPc1d1	světelný
indikátory	indikátor	k1gInPc1	indikátor
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
heliem	helium	k1gNnSc7	helium
lze	lze	k6eAd1	lze
neon	neon	k1gInSc4	neon
využít	využít	k5eAaPmF	využít
i	i	k9	i
v	v	k7c6	v
obloukových	obloukový	k2eAgFnPc6d1	oblouková
lampách	lampa	k1gFnPc6	lampa
a	a	k8xC	a
doutnavkách	doutnavka	k1gFnPc6	doutnavka
<g/>
.	.	kIx.	.
</s>
<s>
Neonové	neonový	k2eAgFnPc1d1	neonová
svítící	svítící	k2eAgFnPc1d1	svítící
reklamy	reklama	k1gFnPc1	reklama
si	se	k3xPyFc3	se
získaly	získat	k5eAaPmAgFnP	získat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
takovou	takový	k3xDgFnSc4	takový
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
že	že	k8xS	že
neon	neon	k1gInSc1	neon
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
všem	všecek	k3xTgFnPc3	všecek
svítícím	svítící	k2eAgFnPc3d1	svítící
trubicím	trubice	k1gFnPc3	trubice
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
kromě	kromě	k7c2	kromě
červených	červená	k1gFnPc2	červená
jsou	být	k5eAaImIp3nP	být
plněné	plněný	k2eAgInPc1d1	plněný
jinými	jiný	k2eAgInPc7d1	jiný
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
barev	barva	k1gFnPc2	barva
světlo	světlo	k1gNnSc4	světlo
nevydává	vydávat	k5eNaImIp3nS	vydávat
samotný	samotný	k2eAgInSc1d1	samotný
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
luminofor	luminofor	k1gInSc1	luminofor
nanesený	nanesený	k2eAgInSc1d1	nanesený
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stěně	stěna	k1gFnSc6	stěna
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Neonové	neonový	k2eAgFnPc1d1	neonová
trubice	trubice	k1gFnPc1	trubice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
(	(	kIx(	(
<g/>
usměrňovače	usměrňovač	k1gInPc1	usměrňovač
<g/>
,	,	kIx,	,
pojistky	pojistka	k1gFnPc1	pojistka
<g/>
,	,	kIx,	,
reduktory	reduktor	k1gInPc1	reduktor
napětí	napětí	k1gNnSc2	napětí
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
neon	neon	k1gInSc1	neon
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
kryogenní	kryogenní	k2eAgFnSc6d1	kryogenní
technice	technika	k1gFnSc6	technika
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
dražšího	drahý	k2eAgNnSc2d2	dražší
a	a	k8xC	a
obtížněji	obtížně	k6eAd2	obtížně
připravitelného	připravitelný	k2eAgNnSc2d1	připravitelné
kapalného	kapalný	k2eAgNnSc2d1	kapalné
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
laserů	laser	k1gInPc2	laser
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neon	neon	k1gInSc1	neon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
neon	neon	k1gInSc1	neon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Elemental	Elemental	k1gMnSc1	Elemental
-	-	kIx~	-
Neon	neon	k1gInSc1	neon
USGS	USGS	kA	USGS
Periodic	Periodice	k1gFnPc2	Periodice
Table	tablo	k1gNnSc6	tablo
-	-	kIx~	-
Neon	neon	k1gInSc1	neon
Atomic	Atomice	k1gFnPc2	Atomice
Spectrum	Spectrum	k1gNnSc1	Spectrum
of	of	k?	of
Neon	neon	k1gInSc4	neon
</s>
