<s>
Škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
místo	místo	k1gNnSc4	místo
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
institucí	instituce	k1gFnPc2	instituce
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
pojmu	pojem	k1gInSc6	pojem
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
budou	být	k5eAaImBp3nP	být
studovat	studovat	k5eAaImF	studovat
státní	státní	k2eAgFnSc4d1	státní
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
přestoupí	přestoupit	k5eAaPmIp3nS	přestoupit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
nebo	nebo	k8xC	nebo
využijí	využít	k5eAaPmIp3nP	využít
studia	studio	k1gNnPc1	studio
na	na	k7c6	na
soukromých	soukromý	k2eAgFnPc6d1	soukromá
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
základní	základní	k2eAgFnSc2d1	základní
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
je	být	k5eAaImIp3nS	být
dětem	dítě	k1gFnPc3	dítě
umožněno	umožnit	k5eAaPmNgNnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyučily	vyučit	k5eAaPmAgFnP	vyučit
nějakému	nějaký	k3yIgNnSc3	nějaký
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
zakončeno	zakončen	k2eAgNnSc4d1	zakončeno
maturitou	maturita	k1gFnSc7	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
složil	složit	k5eAaPmAgMnS	složit
úspěšně	úspěšně	k6eAd1	úspěšně
maturitní	maturitní	k2eAgFnPc4d1	maturitní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
speciální	speciální	k2eAgFnSc1d1	speciální
škola	škola	k1gFnSc1	škola
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
základní	základní	k2eAgFnSc1d1	základní
jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
střední	střední	k2eAgFnSc1d1	střední
odborné	odborný	k2eAgNnSc4d1	odborné
učiliště	učiliště	k1gNnSc4	učiliště
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
gymnázium	gymnázium	k1gNnSc1	gymnázium
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
V	v	k7c6	v
UK	UK	kA	UK
termín	termín	k1gInSc1	termín
škola	škola	k1gFnSc1	škola
prvotně	prvotně	k6eAd1	prvotně
označuje	označovat	k5eAaImIp3nS	označovat
předuniverzitní	předuniverzitní	k2eAgFnPc4d1	předuniverzitní
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
dále	daleko	k6eAd2	daleko
dělené	dělený	k2eAgNnSc1d1	dělené
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnost	výkonnost	k1gFnSc1	výkonnost
školy	škola	k1gFnSc2	škola
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
Inspektorát	inspektorát	k1gInSc1	inspektorát
vzdělání	vzdělání	k1gNnSc2	vzdělání
jejího	její	k3xOp3gNnSc2	její
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
termín	termín	k1gInSc1	termín
škola	škola	k1gFnSc1	škola
může	moct	k5eAaImIp3nS	moct
označovat	označovat	k5eAaImF	označovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
instituci	instituce	k1gFnSc4	instituce
na	na	k7c6	na
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
stupni	stupeň	k1gInSc6	stupeň
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vše	všechen	k3xTgNnSc1	všechen
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
dětské	dětský	k2eAgFnPc1d1	dětská
jesle	jesle	k1gFnPc1	jesle
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
batolata	batole	k1gNnPc4	batole
<g/>
)	)	kIx)	)
-	-	kIx~	-
nursery	nurser	k1gInPc1	nurser
<g/>
,	,	kIx,	,
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
-	-	kIx~	-
pre	pre	k?	pre
kindergarten	kindergartno	k1gNnPc2	kindergartno
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
grade	grad	k1gInSc5	grad
<g/>
)	)	kIx)	)
-	-	kIx~	-
elementary	elementar	k1gInPc4	elementar
school	school	k1gInSc1	school
-	-	kIx~	-
ISCED	ISCED	kA	ISCED
1	[number]	k4	1
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
integrovaným	integrovaný	k2eAgInSc7d1	integrovaný
předškolním	předškolní	k2eAgInSc7d1	předškolní
ročníkem	ročník	k1gInSc7	ročník
-	-	kIx~	-
kindergarten	kindergarten	k2eAgMnSc1d1	kindergarten
-	-	kIx~	-
ISCED	ISCED	kA	ISCED
0	[number]	k4	0
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
obvykle	obvykle	k6eAd1	obvykle
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
grade	grad	k1gInSc5	grad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
specifických	specifický	k2eAgFnPc2d1	specifická
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
geografické	geografický	k2eAgFnSc2d1	geografická
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
-	-	kIx~	-
middle	middle	k6eAd1	middle
school	school	k1gInSc1	school
-	-	kIx~	-
ISECD	ISECD	kA	ISECD
2	[number]	k4	2
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
(	(	kIx(	(
<g/>
grade	grad	k1gInSc5	grad
<g/>
)	)	kIx)	)
-	-	kIx~	-
high	high	k1gInSc1	high
school	school	k1gInSc1	school
-	-	kIx~	-
ISCED	ISCED	kA	ISCED
3	[number]	k4	3
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
-	-	kIx~	-
ISCED	ISCED	kA	ISCED
5	[number]	k4	5
-	-	kIx~	-
a	a	k8xC	a
doktorandské	doktorandský	k2eAgNnSc4d1	doktorandské
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
studium	studium	k1gNnSc4	studium
-	-	kIx~	-
ISCED	ISCED	kA	ISCED
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
výkonnost	výkonnost	k1gFnSc4	výkonnost
škol	škola	k1gFnPc2	škola
po	po	k7c4	po
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k9	jako
jednotřídné	jednotřídné	k1gNnSc1	jednotřídné
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jediný	jediný	k2eAgMnSc1d1	jediný
učitel	učitel	k1gMnSc1	učitel
učil	učít	k5eAaPmAgMnS	učít
sedm	sedm	k4xCc4	sedm
stupňů	stupeň	k1gInPc2	stupeň
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
jednotřídné	jednotřídný	k2eAgFnPc1d1	jednotřídný
školy	škola	k1gFnPc1	škola
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
do	do	k7c2	do
vícetřídních	vícetřídní	k2eAgInPc2d1	vícetřídní
objektů	objekt	k1gInPc2	objekt
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
nejčastěji	často	k6eAd3	často
poskytovanou	poskytovaný	k2eAgFnSc7d1	poskytovaná
dětskými	dětský	k2eAgFnPc7d1	dětská
drožkami	drožka	k1gFnPc7	drožka
a	a	k8xC	a
školními	školní	k2eAgInPc7d1	školní
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
velký	velký	k2eAgInSc1d1	velký
nedostatek	nedostatek	k1gInSc1	nedostatek
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
přetížené	přetížený	k2eAgFnPc1d1	přetížená
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
etiopské	etiopský	k2eAgFnSc6d1	etiopská
provincii	provincie	k1gFnSc6	provincie
Awassa	Awassa	k1gFnSc1	Awassa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
třídě	třída	k1gFnSc6	třída
85	[number]	k4	85
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
proto	proto	k8xC	proto
probíhá	probíhat	k5eAaImIp3nS	probíhat
často	často	k6eAd1	často
výuka	výuka	k1gFnSc1	výuka
na	na	k7c4	na
směny	směna	k1gFnPc4	směna
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
docházejí	docházet	k5eAaImIp3nP	docházet
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
i	i	k9	i
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostupnému	dostupný	k2eNgNnSc3d1	nedostupné
vzdělání	vzdělání	k1gNnSc3	vzdělání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
stále	stále	k6eAd1	stále
nízká	nízký	k2eAgFnSc1d1	nízká
gramotnost	gramotnost	k1gFnSc1	gramotnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dále	daleko	k6eAd2	daleko
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
chudobu	chudoba	k1gFnSc4	chudoba
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
či	či	k8xC	či
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělé	vyspělý	k2eAgInPc1d1	vyspělý
státy	stát	k1gInPc1	stát
proto	proto	k8xC	proto
podporují	podporovat	k5eAaImIp3nP	podporovat
výstavbu	výstavba	k1gFnSc4	výstavba
nových	nový	k2eAgFnPc2d1	nová
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
