<s>
Největším	veliký	k2eAgInSc7d3	veliký
přínosem	přínos	k1gInSc7	přínos
západnímu	západní	k2eAgInSc3d1	západní
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
Sókratova	Sókratův	k2eAgFnSc1d1	Sókratova
dialogická	dialogický	k2eAgFnSc1d1	dialogická
metoda	metoda	k1gFnSc1	metoda
tázání	tázání	k1gNnSc2	tázání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sókratovská	sókratovský	k2eAgFnSc1d1	sókratovská
či	či	k8xC	či
maieutická	maieutický	k2eAgFnSc1d1	maieutický
<g/>
.	.	kIx.	.
</s>
