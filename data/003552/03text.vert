<s>
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
skupina	skupina	k1gFnSc1	skupina
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pěti	pět	k4xCc2	pět
příbuzných	příbuzný	k1gMnPc2	příbuzný
západogermánských	západogermánský	k2eAgInPc2d1	západogermánský
jazyků	jazyk	k1gInPc2	jazyk
používaných	používaný	k2eAgInPc2d1	používaný
etnickou	etnický	k2eAgFnSc7d1	etnická
skupinou	skupina	k1gFnSc7	skupina
Frísů	Frís	k1gMnPc2	Frís
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
území	území	k1gNnSc6	území
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
dosud	dosud	k6eAd1	dosud
živé	živý	k2eAgMnPc4d1	živý
příbuzné	příbuzný	k1gMnPc4	příbuzný
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tak	tak	k9	tak
zkráceně	zkráceně	k6eAd1	zkráceně
říká	říkat	k5eAaImIp3nS	říkat
též	tenž	k3xDgFnSc3	tenž
západofríštině	západofríština	k1gFnSc3	západofríština
<g/>
.	.	kIx.	.
západofríština	západofríština	k1gFnSc1	západofríština
severofríština	severofríština	k1gFnSc1	severofríština
východofríština	východofríština	k1gFnSc1	východofríština
sater-fríština	saterríština	k1gFnSc1	sater-fríština
stadsfries	stadsfries	k1gInSc4	stadsfries
Navzájem	navzájem	k6eAd1	navzájem
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
jazyky	jazyk	k1gInPc1	jazyk
odlišné	odlišný	k2eAgInPc1d1	odlišný
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
<g/>
.	.	kIx.	.
</s>
<s>
Frísové	Frís	k1gMnPc1	Frís
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
významnými	významný	k2eAgMnPc7d1	významný
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
rejdaři	rejdař	k1gMnPc7	rejdař
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Frísko	Frísko	k1gNnSc1	Frísko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
existovaly	existovat	k5eAaImAgInP	existovat
po	po	k7c6	po
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
systematicky	systematicky	k6eAd1	systematicky
vyučovány	vyučován	k2eAgFnPc1d1	vyučována
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaPmIp3nP	vydávat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc2	on
tiskoviny	tiskovina	k1gFnSc2	tiskovina
<g/>
,	,	kIx,	,
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Theun	Theun	k1gMnSc1	Theun
de	de	k?	de
Vries	Vries	k1gMnSc1	Vries
napsal	napsat	k5eAaBmAgMnS	napsat
některá	některý	k3yIgNnPc4	některý
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
ve	v	k7c6	v
fríštině	fríština	k1gFnSc6	fríština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
fríština	fríština	k1gFnSc1	fríština
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
staré	starý	k2eAgFnSc3d1	stará
angličtině	angličtina	k1gFnSc3	angličtina
a	a	k8xC	a
i	i	k9	i
dnešní	dnešní	k2eAgInPc1d1	dnešní
fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jazyky	jazyk	k1gMnPc4	jazyk
angličtině	angličtina	k1gFnSc6	angličtina
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
anglickému	anglický	k2eAgMnSc3d1	anglický
green	green	k1gInSc1	green
cheese	cheesa	k1gFnSc3	cheesa
(	(	kIx(	(
<g/>
nezralý	zralý	k2eNgInSc1d1	nezralý
sýr	sýr	k1gInSc1	sýr
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
západofríské	západofríské	k2eAgFnSc1d1	západofríské
griene	grien	k1gInSc5	grien
tsiis	tsiis	k1gFnSc4	tsiis
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nizozemsky	nizozemsky	k6eAd1	nizozemsky
se	se	k3xPyFc4	se
totéž	týž	k3xTgNnSc1	týž
řekne	říct	k5eAaPmIp3nS	říct
groene	groen	k1gInSc5	groen
kaas	kaas	k1gInSc4	kaas
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dlouholeté	dlouholetý	k2eAgFnSc3d1	dlouholetá
nadvládě	nadvláda	k1gFnSc3	nadvláda
ostatních	ostatní	k2eAgInPc2d1	ostatní
národů	národ	k1gInPc2	národ
však	však	k9	však
jejich	jejich	k3xOp3gInPc1	jejich
jazyky	jazyk	k1gInPc1	jazyk
zanechaly	zanechat	k5eAaPmAgInP	zanechat
na	na	k7c6	na
fríských	fríský	k2eAgInPc6d1	fríský
jazycích	jazyk	k1gInPc6	jazyk
přece	přece	k9	přece
jen	jen	k6eAd1	jen
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
stopy	stopa	k1gFnPc1	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Fríská	fríský	k2eAgFnSc1d1	fríská
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc4d1	složitý
systém	systém	k1gInSc4	systém
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
popsán	popsat	k5eAaPmNgInS	popsat
jen	jen	k9	jen
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
a	a	k8xC	a
O	o	k7c4	o
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
otevřené	otevřený	k2eAgInPc4d1	otevřený
O.	O.	kA	O.
Â	Â	kA	Â
a	a	k8xC	a
Ô	Ô	kA	Ô
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Ó.	Ó.	kA	Ó.
AA	AA	kA	AA
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Á.	Á.	kA	Á.
E	E	kA	E
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
otevřené	otevřený	k2eAgNnSc1d1	otevřené
<g/>
.	.	kIx.	.
Ê	Ê	k?	Ê
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
otevřené	otevřený	k2eAgInPc4d1	otevřený
É.	É.	kA	É.
EE	EE	kA	EE
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Í.	Í.	kA	Í.
I	I	kA	I
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
I	i	k9	i
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Í.	Í.	kA	Í.
OO	OO	kA	OO
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
OU	ou	k0	ou
<g/>
.	.	kIx.	.
</s>
<s>
U	U	kA	U
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Ö.	Ö.	k1gFnSc1	Ö.
Û	Û	k?	Û
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
U.	U.	kA	U.
Ú	Ú	kA	Ú
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
Ü.	Ü.	k1gFnSc1	Ü.
Y	Y	kA	Y
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
P.	P.	kA	P.
D	D	kA	D
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
T.	T.	kA	T.
H	H	kA	H
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
před	před	k7c7	před
J.	J.	kA	J.
V	V	kA	V
i	i	k8xC	i
W	W	kA	W
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
V.	V.	kA	V.
Q	Q	kA	Q
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
KW	kw	kA	kw
<g/>
.	.	kIx.	.
</s>
<s>
X	X	kA	X
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
KS	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
analytické	analytický	k2eAgInPc1d1	analytický
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
opisné	opisný	k2eAgNnSc1d1	opisné
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
,	,	kIx,	,
pády	pád	k1gInPc1	pád
se	se	k3xPyFc4	se
vytratily	vytratit	k5eAaPmAgInP	vytratit
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
omezeně	omezeně	k6eAd1	omezeně
užívaného	užívaný	k2eAgInSc2d1	užívaný
genitivu	genitiv	k1gInSc2	genitiv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rody	rod	k1gInPc1	rod
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
určitý	určitý	k2eAgInSc1d1	určitý
a	a	k8xC	a
neurčitý	určitý	k2eNgInSc1d1	neurčitý
člen	člen	k1gInSc1	člen
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
v	v	k7c6	v
nizozemštině	nizozemština	k1gFnSc6	nizozemština
<g/>
.	.	kIx.	.
</s>
<s>
Us	Us	k?	Us
Heit	Heit	k1gInSc1	Heit
yn	yn	k?	yn
'	'	kIx"	'
<g/>
e	e	k0	e
Himel	Himel	k1gInSc1	Himel
<g/>
,	,	kIx,	,
lit	lit	k1gInSc1	lit
jo	jo	k9	jo
namme	nammat	k5eAaPmIp3nS	nammat
hillige	hillige	k6eAd1	hillige
wurde	wurde	k6eAd1	wurde
<g/>
,	,	kIx,	,
lit	lit	k1gInSc1	lit
jo	jo	k9	jo
keninkryk	keninkryk	k6eAd1	keninkryk
komme	kommat	k5eAaPmIp3nS	kommat
<g/>
,	,	kIx,	,
lit	lit	k2eAgMnSc1d1	lit
jo	jo	k9	jo
wil	wil	k?	wil
dien	dien	k1gInSc1	dien
wurde	wurde	k6eAd1	wurde
op	op	k1gMnSc1	op
ierde	ierde	k6eAd1	ierde
likegoed	likegoed	k1gMnSc1	likegoed
as	as	k1gNnSc2	as
yn	yn	k?	yn
'	'	kIx"	'
<g/>
e	e	k0	e
himel	himel	k1gInSc1	himel
<g/>
.	.	kIx.	.
</s>
<s>
Jou	Jou	k?	Jou
ús	ús	k?	ús
hjoed	hjoed	k1gMnSc1	hjoed
ús	ús	k?	ús
deistich	deistich	k1gMnSc1	deistich
brea	brea	k1gMnSc1	brea
en	en	k?	en
ferjou	ferjou	k6eAd1	ferjou
ús	ús	k?	ús
ús	ús	k?	ús
skulden	skuldna	k1gFnPc2	skuldna
<g/>
,	,	kIx,	,
sa	sa	k?	sa
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
wy	wy	k?	wy
ús	ús	k?	ús
skuldners	skuldners	k1gInSc1	skuldners
ek	ek	k?	ek
ferjû	ferjû	k?	ferjû
hawwe	hawwe	k1gInSc1	hawwe
<g/>
;	;	kIx,	;
en	en	k?	en
lit	lit	k1gInSc1	lit
ús	ús	k?	ús
net	net	k?	net
yn	yn	k?	yn
fersiking	fersiking	k1gInSc1	fersiking
komme	kommat	k5eAaPmIp3nS	kommat
<g/>
,	,	kIx,	,
mar	mar	k?	mar
ferlos	ferlos	k1gInSc1	ferlos
ús	ús	k?	ús
fan	fana	k1gFnPc2	fana
'	'	kIx"	'
<g/>
e	e	k0	e
kweade	kweást	k5eAaPmIp3nS	kweást
<g/>
.	.	kIx.	.
want	want	k2eAgInSc1d1	want
jowes	jowes	k1gInSc1	jowes
is	is	k?	is
it	it	k?	it
keninkryk	keninkryk	k1gInSc1	keninkryk
en	en	k?	en
de	de	k?	de
krê	krê	k?	krê
en	en	k?	en
de	de	k?	de
hearlikheid	hearlikheid	k1gInSc1	hearlikheid
oant	oant	k1gInSc1	oant
yn	yn	k?	yn
ivichheid	ivichheid	k1gInSc1	ivichheid
<g/>
.	.	kIx.	.
</s>
<s>
Amen	amen	k1gNnSc1	amen
<g/>
!	!	kIx.	!
</s>
<s>
Každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
fríských	fríský	k2eAgInPc2d1	fríský
jazyků	jazyk	k1gInPc2	jazyk
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
některými	některý	k3yIgNnPc7	některý
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
tak	tak	k6eAd1	tak
veliké	veliký	k2eAgInPc1d1	veliký
<g/>
,	,	kIx,	,
že	že	k8xS	že
překážejí	překážet	k5eAaImIp3nP	překážet
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
porozumění	porozumění	k1gNnSc3	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tak	tak	k9	tak
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
rozumět	rozumět	k5eAaImF	rozumět
ani	ani	k9	ani
sousedním	sousední	k2eAgInPc3d1	sousední
dialektům	dialekt	k1gInPc3	dialekt
<g/>
.	.	kIx.	.
západofríština	západofríština	k1gFnSc1	západofríština
používaná	používaný	k2eAgFnSc1d1	používaná
ve	v	k7c6	v
Frísku	Frísko	k1gNnSc6	Frísko
(	(	kIx(	(
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
Klaaifrysk	Klaaifrysk	k1gInSc1	Klaaifrysk
Wâldfrysk	Wâldfrysk	k1gInSc1	Wâldfrysk
Noardhoeks	Noardhoeks	k1gInSc1	Noardhoeks
jihofríština	jihofríština	k1gFnSc1	jihofríština
(	(	kIx(	(
<g/>
Súdhoeks	Súdhoeks	k1gInSc1	Súdhoeks
<g/>
)	)	kIx)	)
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
fríština	fríština	k1gFnSc1	fríština
(	(	kIx(	(
<g/>
Súdwesthoeksk	Súdwesthoeksk	k1gInSc1	Súdwesthoeksk
<g/>
)	)	kIx)	)
Schiermonnikoogs	Schiermonnikoogs	k1gInSc1	Schiermonnikoogs
Hindeloopers	Hindeloopers	k1gInSc1	Hindeloopers
Aasters	Aasters	k1gInSc1	Aasters
Westers	Westers	k1gInSc1	Westers
východofríština	východofríština	k1gFnSc1	východofríština
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Sasku	Sasko	k1gNnSc6	Sasko
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
sater-fríština	saterríština	k1gFnSc1	sater-fríština
<g />
.	.	kIx.	.
</s>
<s>
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
dialektů	dialekt	k1gInPc2	dialekt
severofríštína	severofríštín	k1gInSc2	severofríštín
používaná	používaný	k2eAgFnSc1d1	používaná
ve	v	k7c6	v
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc6	Šlesvicku-Holštýnsko
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
pevninské	pevninský	k2eAgInPc4d1	pevninský
dialekty	dialekt	k1gInPc4	dialekt
dialekt	dialekt	k1gInSc1	dialekt
Mooring	Mooring	k1gInSc1	Mooring
dialekt	dialekt	k1gInSc1	dialekt
Hoorning	Hoorning	k1gInSc1	Hoorning
dialekt	dialekt	k1gInSc1	dialekt
Wiedingharde	Wiedinghard	k1gMnSc5	Wiedinghard
dialekt	dialekt	k1gInSc4	dialekt
Tideland	Tideland	k1gInSc1	Tideland
Islands	Islands	k1gInSc1	Islands
ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
dialekty	dialekt	k1gInPc1	dialekt
dialekt	dialekt	k1gInSc4	dialekt
Sylt	Sylt	k2eAgInSc1d1	Sylt
dialekt	dialekt	k1gInSc1	dialekt
Föhr	Föhr	k1gInSc1	Föhr
dialekt	dialekt	k1gInSc1	dialekt
Amrum	Amrum	k1gInSc4	Amrum
Helgolandština	Helgolandština	k1gFnSc1	Helgolandština
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fríské	fríský	k2eAgInPc1d1	fríský
jazyky	jazyk	k1gInPc1	jazyk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
