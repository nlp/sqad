<s>
Annapolis	Annapolis	k1gInSc1	Annapolis
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Maryland	Marylando	k1gNnPc2	Marylando
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
okresu	okres	k1gInSc2	okres
Anne	Ann	k1gFnSc2	Ann
Arundel	Arundlo	k1gNnPc2	Arundlo
County	Count	k1gInPc7	Count
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInPc1d1	ležící
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
zátoky	zátoka	k1gFnSc2	zátoka
Chesapeake	Chesapeak	k1gFnSc2	Chesapeak
50	[number]	k4	50
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Washingtonu	Washington	k1gInSc2	Washington
<g/>
.	.	kIx.	.
</s>
