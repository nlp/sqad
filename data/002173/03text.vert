<s>
Annapolis	Annapolis	k1gInSc1	Annapolis
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Maryland	Marylando	k1gNnPc2	Marylando
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
okresu	okres	k1gInSc2	okres
Anne	Ann	k1gFnSc2	Ann
Arundel	Arundlo	k1gNnPc2	Arundlo
County	Count	k1gInPc7	Count
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInPc1d1	ležící
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
zátoky	zátoka	k1gFnSc2	zátoka
Chesapeake	Chesapeak	k1gFnSc2	Chesapeak
50	[number]	k4	50
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Washingtonu	Washington	k1gInSc2	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
19,7	[number]	k4	19,7
km2	km2	k4	km2
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
36	[number]	k4	36
217	[number]	k4	217
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Námořní	námořní	k2eAgFnSc1d1	námořní
akademie	akademie	k1gFnSc1	akademie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konala	konat	k5eAaImAgFnS	konat
Mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
Annapolis	Annapolis	k1gFnSc2	Annapolis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1783	[number]	k4	1783
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1784	[number]	k4	1784
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
Maryland	Maryland	k1gInSc1	Maryland
State	status	k1gInSc5	status
House	house	k1gNnSc1	house
zasedal	zasedat	k5eAaImAgInS	zasedat
Kongres	kongres	k1gInSc4	kongres
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Annapolis	Annapolis	k1gFnSc3	Annapolis
bylo	být	k5eAaImAgNnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
plachtařská	plachtařský	k2eAgNnPc4d1	plachtařské
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
rejdařská	rejdařský	k2eAgFnSc1d1	rejdařská
metropole	metropole	k1gFnSc1	metropole
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
38	[number]	k4	38
394	[number]	k4	394
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
60,1	[number]	k4	60,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
26,0	[number]	k4	26,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,1	[number]	k4	2,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
9,0	[number]	k4	9,0
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
16,8	[number]	k4	16,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
M.	M.	kA	M.
Cain	Cain	k1gMnSc1	Cain
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robert	Robert	k1gMnSc1	Robert
Duvall	Duvall	k1gMnSc1	Duvall
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Travis	Travis	k1gFnSc2	Travis
Pastrana	Pastrana	k1gFnSc1	Pastrana
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Leo	Leo	k1gMnSc1	Leo
Strauss	Strauss	k1gInSc1	Strauss
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
Stan	stan	k1gInSc1	stan
Stearns	Stearns	k1gInSc1	Stearns
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Tallinn	Tallinn	k1gMnSc1	Tallinn
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
Newport	Newport	k1gInSc1	Newport
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Dumfries	Dumfries	k1gInSc1	Dumfries
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Wexford	Wexford	k1gInSc1	Wexford
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
Annapolis	Annapolis	k1gFnSc2	Annapolis
Royal	Royal	k1gInSc1	Royal
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Karlskrona	Karlskrona	k1gFnSc1	Karlskrona
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
Redwood	Redwooda	k1gFnPc2	Redwooda
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
