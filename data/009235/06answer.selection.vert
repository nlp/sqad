<s>
Interstate	Interstat	k1gMnSc5	Interstat
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
mezistátní	mezistátní	k2eAgFnSc1d1	mezistátní
dálnice	dálnice	k1gFnSc1	dálnice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
Mexiko	Mexiko	k1gNnSc1	Mexiko
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
