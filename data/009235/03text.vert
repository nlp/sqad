<p>
<s>
Interstate	Interstat	k1gMnSc5	Interstat
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
mezistátní	mezistátní	k2eAgFnSc1d1	mezistátní
dálnice	dálnice	k1gFnSc1	dálnice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
Mexiko	Mexiko	k1gNnSc1	Mexiko
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležitá	důležitý	k2eAgNnPc4d1	důležité
města	město	k1gNnPc4	město
ležící	ležící	k2eAgNnPc4d1	ležící
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
trase	trasa	k1gFnSc6	trasa
patří	patřit	k5eAaImIp3nS	patřit
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Portland	Portland	k1gInSc1	Portland
<g/>
,	,	kIx,	,
Tacoma	Tacoma	k1gFnSc1	Tacoma
a	a	k8xC	a
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Délka	délka	k1gFnSc1	délka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Interstate	Interstat	k1gMnSc5	Interstat
5	[number]	k4	5
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Interstate	Interstat	k1gInSc5	Interstat
5	[number]	k4	5
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
