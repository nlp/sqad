<p>
<s>
Soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
Povídky	povídka	k1gFnSc2	povídka
malostranské	malostranský	k2eAgNnSc1d1	Malostranské
je	být	k5eAaImIp3nS	být
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
prozaické	prozaický	k2eAgNnSc1d1	prozaické
dílo	dílo	k1gNnSc1	dílo
Jana	Jan	k1gMnSc2	Jan
Nerudy	Neruda	k1gMnSc2	Neruda
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Třináct	třináct	k4xCc1	třináct
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
původně	původně	k6eAd1	původně
samostatně	samostatně	k6eAd1	samostatně
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
Květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInPc1d1	národní
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Podřipan	Podřipan	k1gMnSc1	Podřipan
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
sestavil	sestavit	k5eAaPmAgMnS	sestavit
a	a	k8xC	a
prvně	prvně	k?	prvně
společně	společně	k6eAd1	společně
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Grégr	Grégra	k1gFnPc2	Grégra
a	a	k8xC	a
Ferd	Ferda	k1gFnPc2	Ferda
<g/>
.	.	kIx.	.
</s>
<s>
Dattel	Dattel	k1gMnSc1	Dattel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náměty	námět	k1gInPc1	námět
k	k	k7c3	k
povídkám	povídka	k1gFnPc3	povídka
autor	autor	k1gMnSc1	autor
čerpal	čerpat	k5eAaImAgInS	čerpat
ze	z	k7c2	z
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
léta	léto	k1gNnPc4	léto
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
dospívání	dospívání	k1gNnSc2	dospívání
strávená	strávený	k2eAgFnSc1d1	strávená
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáři	čtenář	k1gMnPc1	čtenář
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
mohou	moct	k5eAaImIp3nP	moct
vžít	vžít	k5eAaPmF	vžít
do	do	k7c2	do
věrného	věrný	k2eAgInSc2d1	věrný
obrazu	obraz	k1gInSc2	obraz
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
měšťanů	měšťan	k1gMnPc2	měšťan
i	i	k8xC	i
prostých	prostý	k2eAgMnPc2d1	prostý
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
pečlivě	pečlivě	k6eAd1	pečlivě
a	a	k8xC	a
podrobně	podrobně	k6eAd1	podrobně
charakterizovány	charakterizován	k2eAgFnPc1d1	charakterizována
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
i	i	k9	i
přesný	přesný	k2eAgInSc4d1	přesný
místopis	místopis	k1gInSc4	místopis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
děje	děj	k1gInPc1	děj
povídek	povídka	k1gFnPc2	povídka
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
následujícími	následující	k2eAgFnPc7d1	následující
povídkami	povídka	k1gFnPc7	povídka
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
uvedený	uvedený	k2eAgInSc1d1	uvedený
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
rokem	rok	k1gInSc7	rok
prvního	první	k4xOgNnSc2	první
časopiseckého	časopisecký	k2eAgNnSc2d1	časopisecké
publikování	publikování	k1gNnSc2	publikování
příslušné	příslušný	k2eAgFnSc2d1	příslušná
povídky	povídka	k1gFnSc2	povídka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Týden	týden	k1gInSc1	týden
v	v	k7c6	v
tichém	tichý	k2eAgInSc6d1	tichý
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
krátké	krátký	k2eAgInPc4d1	krátký
obrázky	obrázek	k1gInPc4	obrázek
a	a	k8xC	a
úryvky	úryvek	k1gInPc4	úryvek
života	život	k1gInSc2	život
a	a	k8xC	a
prolínající	prolínající	k2eAgInPc1d1	prolínající
se	se	k3xPyFc4	se
osudy	osud	k1gInPc7	osud
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
život	život	k1gInSc1	život
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
malostranském	malostranský	k2eAgInSc6d1	malostranský
měšťanském	měšťanský	k2eAgInSc6d1	měšťanský
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejrozsáhlejší	rozsáhlý	k2eAgFnSc6d3	nejrozsáhlejší
povídce	povídka	k1gFnSc6	povídka
sbírky	sbírka	k1gFnSc2	sbírka
autor	autor	k1gMnSc1	autor
pečlivě	pečlivě	k6eAd1	pečlivě
popisuje	popisovat	k5eAaImIp3nS	popisovat
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
děj	děj	k1gInSc1	děj
samotný	samotný	k2eAgMnSc1d1	samotný
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
úryvkovitě	úryvkovitě	k6eAd1	úryvkovitě
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
řeči	řeč	k1gFnSc6	řeč
místy	místy	k6eAd1	místy
pouze	pouze	k6eAd1	pouze
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
směr	směr	k1gInSc1	směr
vývoje	vývoj	k1gInSc2	vývoj
děje	dít	k5eAaImIp3nS	dít
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
na	na	k7c4	na
čtenáři	čtenář	k1gMnPc5	čtenář
domyslet	domyslet	k5eAaPmF	domyslet
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
obrazotvorností	obrazotvornost	k1gFnPc2	obrazotvornost
důvody	důvod	k1gInPc4	důvod
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
jednání	jednání	k1gNnSc3	jednání
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc7d1	celá
povídkou	povídka	k1gFnSc7	povídka
prochází	procházet	k5eAaImIp3nS	procházet
kritika	kritika	k1gFnSc1	kritika
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
a	a	k8xC	a
přetvářky	přetvářka	k1gFnSc2	přetvářka
společnosti	společnost	k1gFnSc2	společnost
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
domu	dům	k1gInSc2	dům
bydlí	bydlet	k5eAaImIp3nS	bydlet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
hokynářském	hokynářský	k2eAgInSc6d1	hokynářský
krámě	krám	k1gInSc6	krám
manželé	manžel	k1gMnPc1	manžel
Bavorovi	Bavorův	k2eAgMnPc1d1	Bavorův
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Václavem	Václav	k1gMnSc7	Václav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
miluje	milovat	k5eAaImIp3nS	milovat
Márinku	Márinka	k1gFnSc4	Márinka
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
hospodské	hospodská	k1gFnSc2	hospodská
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Bavorová	Bavorový	k2eAgFnSc1d1	Bavorový
jako	jako	k8xC	jako
náruživá	náruživý	k2eAgFnSc1d1	náruživá
vykladačka	vykladačka	k1gFnSc1	vykladačka
snů	sen	k1gInPc2	sen
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
budí	budit	k5eAaImIp3nS	budit
a	a	k8xC	a
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
si	se	k3xPyFc3	se
sny	sen	k1gInPc4	sen
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
snáře	snář	k1gInSc2	snář
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
související	související	k2eAgFnSc4d1	související
číslice	číslice	k1gFnPc4	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
přivydělala	přivydělat	k5eAaPmAgFnS	přivydělat
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
paní	paní	k1gFnSc1	paní
Bavorová	Bavorová	k1gFnSc1	Bavorová
prát	prát	k5eAaImF	prát
domácím	domácí	k1gMnPc3	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
Matylda	Matylda	k1gFnSc1	Matylda
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
hledá	hledat	k5eAaImIp3nS	hledat
vhodného	vhodný	k2eAgMnSc4d1	vhodný
partnera	partner	k1gMnSc4	partner
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
Valentina	Valentina	k1gFnSc1	Valentina
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
být	být	k5eAaImF	být
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
jako	jako	k9	jako
velké	velký	k2eAgNnSc1d1	velké
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
paní	paní	k1gFnSc1	paní
domácí	domácí	k1gFnSc1	domácí
si	se	k3xPyFc3	se
přivydělává	přivydělávat	k5eAaImIp3nS	přivydělávat
špatně	špatně	k6eAd1	špatně
placenou	placený	k2eAgFnSc7d1	placená
prací	práce	k1gFnSc7	práce
spravování	spravování	k1gNnSc2	spravování
hadrů	hadr	k1gInPc2	hadr
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
úředníkem	úředník	k1gMnSc7	úředník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nadřízeným	nadřízený	k1gMnPc3	nadřízený
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
patře	patro	k1gNnSc6	patro
domu	dům	k1gInSc2	dům
bydlí	bydlet	k5eAaImIp3nS	bydlet
slečna	slečna	k1gFnSc1	slečna
Josefínka	Josefínka	k1gFnSc1	Josefínka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
starat	starat	k5eAaImF	starat
o	o	k7c4	o
nemocnou	mocný	k2eNgFnSc4d1	mocný
slečnu	slečna	k1gFnSc4	slečna
Žanýnku	Žanýnka	k1gFnSc4	Žanýnka
z	z	k7c2	z
přízemí	přízemí	k1gNnSc2	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
nese	nést	k5eAaImIp3nS	nést
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
ji	on	k3xPp3gFnSc4	on
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Slečnu	slečna	k1gFnSc4	slečna
Josefínku	Josefínek	k1gInSc2	Josefínek
má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
pan	pan	k1gMnSc1	pan
Loukota	Loukota	k1gMnSc1	Loukota
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
posílá	posílat	k5eAaImIp3nS	posílat
milostné	milostný	k2eAgFnPc4d1	milostná
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
jako	jako	k8xC	jako
podnájemník	podnájemník	k1gMnSc1	podnájemník
u	u	k7c2	u
hospodářského	hospodářský	k2eAgMnSc2d1	hospodářský
úředníka	úředník	k1gMnSc2	úředník
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
pana	pan	k1gMnSc2	pan
Lakmuse	lakmus	k1gInSc6	lakmus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnSc1	jejich
podnájemník	podnájemník	k1gMnSc1	podnájemník
miluje	milovat	k5eAaImIp3nS	milovat
jejich	jejich	k3xOp3gFnSc4	jejich
dceru	dcera	k1gFnSc4	dcera
Klárku	Klárka	k1gFnSc4	Klárka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
snaží	snažit	k5eAaImIp3nP	snažit
sjednat	sjednat	k5eAaPmF	sjednat
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
snaha	snaha	k1gFnSc1	snaha
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
šťastné	šťastný	k2eAgFnSc2d1	šťastná
náhody	náhoda	k1gFnSc2	náhoda
nemine	minout	k5eNaImIp3nS	minout
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Loukota	Loukota	k1gMnSc1	Loukota
píše	psát	k5eAaImIp3nS	psát
žádost	žádost	k1gFnSc4	žádost
úřadům	úřad	k1gInPc3	úřad
o	o	k7c4	o
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
Josefinkou	Josefinka	k1gFnSc7	Josefinka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c4	na
cokoli	cokoli	k3yInSc4	cokoli
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
vepsat	vepsat	k5eAaPmF	vepsat
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
přichází	přicházet	k5eAaImIp3nS	přicházet
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
slečna	slečna	k1gFnSc1	slečna
Josefinka	Josefinka	k1gFnSc1	Josefinka
miluje	milovat	k5eAaImIp3nS	milovat
strojmistra	strojmistr	k1gMnSc4	strojmistr
Bavoráka	bavorák	k1gMnSc4	bavorák
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nadcházející	nadcházející	k2eAgFnSc4d1	nadcházející
neděli	neděle	k1gFnSc4	neděle
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Nedopsanou	dopsaný	k2eNgFnSc4d1	nedopsaná
žádost	žádost	k1gFnSc4	žádost
najde	najít	k5eAaPmIp3nS	najít
paní	paní	k1gFnSc1	paní
Lakmusová	lakmusový	k2eAgFnSc1d1	Lakmusová
a	a	k8xC	a
přiměje	přimět	k5eAaPmIp3nS	přimět
nešťastného	šťastný	k2eNgMnSc4d1	nešťastný
pana	pan	k1gMnSc4	pan
Loukotu	Loukota	k1gMnSc4	Loukota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
doplnil	doplnit	k5eAaPmAgMnS	doplnit
jméno	jméno	k1gNnSc4	jméno
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
Klárky	Klárka	k1gFnSc2	Klárka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
pan	pan	k1gMnSc1	pan
domácí	domácí	k1gMnSc1	domácí
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
paní	paní	k1gFnSc3	paní
Bavorové	Bavorové	k?	Bavorové
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
vyhodit	vyhodit	k5eAaPmF	vyhodit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
psal	psát	k5eAaImAgMnS	psát
hanebné	hanebný	k2eAgFnPc4d1	hanebná
básně	báseň	k1gFnPc4	báseň
o	o	k7c6	o
panu	pan	k1gMnSc6	pan
prezidentovi	prezident	k1gMnSc6	prezident
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
nedůsledný	důsledný	k2eNgInSc4d1	nedůsledný
<g/>
,	,	kIx,	,
nepořádný	pořádný	k2eNgInSc4d1	nepořádný
a	a	k8xC	a
nechová	chovat	k5eNaImIp3nS	chovat
se	se	k3xPyFc4	se
dostatečně	dostatečně	k6eAd1	dostatečně
uctivě	uctivě	k6eAd1	uctivě
ke	k	k7c3	k
starším	starý	k2eAgMnPc3d2	starší
a	a	k8xC	a
výše	vysoce	k6eAd2	vysoce
postaveným	postavený	k2eAgMnPc3d1	postavený
úředníkům	úředník	k1gMnPc3	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Následného	následný	k2eAgInSc2d1	následný
pohřbu	pohřeb	k1gInSc2	pohřeb
slečny	slečna	k1gFnSc2	slečna
Žanýnky	Žanýnka	k1gFnSc2	Žanýnka
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jejího	její	k3xOp3gNnSc2	její
nenadále	nenadále	k6eAd1	nenadále
objeveného	objevený	k2eAgNnSc2d1	objevené
příbuzenstva	příbuzenstvo	k1gNnSc2	příbuzenstvo
účastní	účastnit	k5eAaImIp3nP	účastnit
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
ty	ten	k3xDgInPc4	ten
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgInPc4d2	lepší
<g/>
"	"	kIx"	"
–	–	k?	–
jedoucí	jedoucí	k2eAgFnSc3d1	jedoucí
v	v	k7c6	v
pohřebním	pohřební	k2eAgInSc6d1	pohřební
průvodu	průvod	k1gInSc6	průvod
v	v	k7c6	v
kočáře	kočár	k1gInSc6	kočár
–	–	k?	–
a	a	k8xC	a
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
"	"	kIx"	"
<g/>
horší	zlý	k2eAgFnPc4d2	horší
<g/>
"	"	kIx"	"
–	–	k?	–
jdoucí	jdoucí	k2eAgFnSc1d1	jdoucí
pěšky	pěšky	k6eAd1	pěšky
(	(	kIx(	(
<g/>
paní	paní	k1gFnSc1	paní
Bavorová	Bavorový	k2eAgFnSc1d1	Bavorový
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
paní	paní	k1gFnSc1	paní
Bavorová	Bavorový	k2eAgFnSc1d1	Bavorový
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
čísel	číslo	k1gNnPc2	číslo
ze	z	k7c2	z
snáře	snář	k1gInSc2	snář
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
v	v	k7c6	v
loterii	loterie	k1gFnSc6	loterie
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
použije	použít	k5eAaPmIp3nS	použít
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
musel	muset	k5eAaImAgMnS	muset
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
nečekaném	čekaný	k2eNgNnSc6d1	nečekané
zbohatnutí	zbohatnutí	k1gNnSc6	zbohatnutí
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
domácí	domácí	k2eAgNnSc1d1	domácí
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
velké	velký	k2eAgNnSc4d1	velké
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Bavorovými	Bavorův	k2eAgFnPc7d1	Bavorova
sblížit	sblížit	k5eAaPmF	sblížit
a	a	k8xC	a
následně	následně	k6eAd1	následně
provdat	provdat	k5eAaPmF	provdat
Matyldu	Matylda	k1gFnSc4	Matylda
za	za	k7c4	za
Václava	Václav	k1gMnSc4	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
hru	hra	k1gFnSc4	hra
<g/>
"	"	kIx"	"
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naoko	naoko	k6eAd1	naoko
blízkým	blízký	k2eAgInSc7d1	blízký
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
Matyldou	Matylda	k1gFnSc7	Matylda
pomstí	pomstit	k5eAaImIp3nS	pomstit
domácímu	domácí	k1gMnSc3	domácí
za	za	k7c2	za
vyhození	vyhození	k1gNnSc2	vyhození
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pan	Pan	k1gMnSc1	Pan
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Schlegl	Schlegl	k1gMnSc1	Schlegl
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
vztahu	vztah	k1gInSc2	vztah
pana	pan	k1gMnSc2	pan
Ryšánka	Ryšánek	k1gMnSc2	Ryšánek
a	a	k8xC	a
pana	pan	k1gMnSc2	pan
Schlegla	Schlegl	k1gMnSc2	Schlegl
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
oba	dva	k4xCgMnPc1	dva
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
malostranského	malostranský	k2eAgInSc2d1	malostranský
hostince	hostinec	k1gInSc2	hostinec
U	u	k7c2	u
Štajniců	Štajniec	k1gInPc2	Štajniec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
tam	tam	k6eAd1	tam
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
chodili	chodit	k5eAaImAgMnP	chodit
<g/>
,	,	kIx,	,
sedali	sedat	k5eAaImAgMnP	sedat
u	u	k7c2	u
stejného	stejný	k2eAgInSc2d1	stejný
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
spolu	spolu	k6eAd1	spolu
nikdy	nikdy	k6eAd1	nikdy
nepromluvili	promluvit	k5eNaPmAgMnP	promluvit
ani	ani	k8xC	ani
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Sedávali	sedávat	k5eAaImAgMnP	sedávat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
stejná	stejný	k2eAgNnPc4d1	stejné
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
opačných	opačný	k2eAgFnPc6d1	opačná
stranách	strana	k1gFnPc6	strana
malého	malý	k2eAgInSc2d1	malý
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
bokem	bokem	k6eAd1	bokem
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k9	jen
sledovali	sledovat	k5eAaImAgMnP	sledovat
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
sále	sál	k1gInSc6	sál
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nemluvnost	nemluvnost	k1gFnSc1	nemluvnost
a	a	k8xC	a
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nevraživost	nevraživost	k1gFnSc1	nevraživost
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
láskou	láska	k1gFnSc7	láska
ke	k	k7c3	k
stejné	stejný	k2eAgFnSc3d1	stejná
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
prvně	prvně	k?	prvně
stýkala	stýkat	k5eAaImAgFnS	stýkat
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Ryšánkem	Ryšánek	k1gMnSc7	Ryšánek
a	a	k8xC	a
hned	hned	k6eAd1	hned
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Schleglem	Schlegl	k1gMnSc7	Schlegl
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yQgMnSc4	který
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
a	a	k8xC	a
porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gMnSc3	on
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
přestal	přestat	k5eAaPmAgMnS	přestat
pan	pan	k1gMnSc1	pan
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
zotavil	zotavit	k5eAaPmAgMnS	zotavit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
samozřejmě	samozřejmě	k6eAd1	samozřejmě
do	do	k7c2	do
hostince	hostinec	k1gInSc2	hostinec
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
zvyku	zvyk	k1gInSc2	zvyk
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
zapálit	zapálit	k5eAaPmF	zapálit
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
tabák	tabák	k1gInSc4	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
moment	moment	k1gInSc4	moment
pan	pan	k1gMnSc1	pan
Schlegl	Schlegl	k1gMnSc1	Schlegl
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jejich	jejich	k3xOp3gNnSc4	jejich
letité	letitý	k2eAgNnSc4d1	letité
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
nabídkou	nabídka	k1gFnSc7	nabídka
vlastního	vlastní	k2eAgInSc2d1	vlastní
tabáku	tabák	k1gInSc2	tabák
ke	k	k7c3	k
kouření	kouření	k1gNnSc3	kouření
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
prvně	prvně	k?	prvně
promluvili	promluvit	k5eAaPmAgMnP	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
pánové	pán	k1gMnPc1	pán
bavili	bavit	k5eAaImAgMnP	bavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přivedla	přivést	k5eAaPmAgFnS	přivést
žebráka	žebrák	k1gMnSc4	žebrák
na	na	k7c4	na
mizinu	mizina	k1gFnSc4	mizina
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
povídce	povídka	k1gFnSc6	povídka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
autor	autor	k1gMnSc1	autor
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
všechno	všechen	k3xTgNnSc1	všechen
zmůžou	zmoct	k5eAaPmIp3nPwO	zmoct
nepravdivé	pravdivý	k2eNgFnPc1d1	nepravdivá
řeči	řeč	k1gFnPc1	řeč
a	a	k8xC	a
pomluvy	pomluva	k1gFnPc1	pomluva
o	o	k7c6	o
nějakém	nějaký	k3yIgInSc6	nějaký
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jak	jak	k6eAd1	jak
moc	moc	k6eAd1	moc
mu	on	k3xPp3gMnSc3	on
mohou	moct	k5eAaImIp3nP	moct
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
milým	milý	k2eAgInSc7d1	milý
<g/>
,	,	kIx,	,
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
a	a	k8xC	a
čistotným	čistotný	k2eAgMnSc7d1	čistotný
malostranským	malostranský	k2eAgMnSc7d1	malostranský
žebrákem	žebrák	k1gMnSc7	žebrák
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
od	od	k7c2	od
dveří	dveře	k1gFnPc2	dveře
ke	k	k7c3	k
dveřím	dveře	k1gFnPc3	dveře
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rádi	rád	k2eAgMnPc1d1	rád
několik	několik	k4yIc4	několik
slov	slovo	k1gNnPc2	slovo
promluvili	promluvit	k5eAaPmAgMnP	promluvit
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
tam	tam	k6eAd1	tam
nějaký	nějaký	k3yIgInSc4	nějaký
peníz	peníz	k1gInSc4	peníz
dali	dát	k5eAaPmAgMnP	dát
nebo	nebo	k8xC	nebo
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
pozvali	pozvat	k5eAaPmAgMnP	pozvat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
místní	místní	k2eAgMnPc1d1	místní
strážníci	strážník	k1gMnPc1	strážník
jej	on	k3xPp3gMnSc4	on
nepronásledovali	pronásledovat	k5eNaImAgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
o	o	k7c4	o
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
milá	milý	k2eAgNnPc4d1	milé
slova	slovo	k1gNnPc4	slovo
neměl	mít	k5eNaImAgInS	mít
nouzi	nouze	k1gFnSc4	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Osudným	osudný	k2eAgMnPc3d1	osudný
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
žebračkou	žebračka	k1gFnSc7	žebračka
žebrající	žebrající	k2eAgFnSc7d1	žebrající
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
u	u	k7c2	u
chrámu	chrám	k1gInSc2	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
upravená	upravený	k2eAgFnSc1d1	upravená
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
nezval	zvát	k5eNaImAgMnS	zvát
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
panu	pan	k1gMnSc3	pan
Vojtíškovi	Vojtíšek	k1gMnSc3	Vojtíšek
záviděla	závidět	k5eAaImAgFnS	závidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
polepšila	polepšit	k5eAaPmAgFnS	polepšit
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
dát	dát	k5eAaPmF	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
on	on	k3xPp3gMnSc1	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Zakrátko	zakrátko	k6eAd1	zakrátko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
po	po	k7c6	po
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pan	pan	k1gMnSc1	pan
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
chudý	chudý	k2eAgMnSc1d1	chudý
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
že	že	k8xS	že
snad	snad	k9	snad
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
podobné	podobný	k2eAgFnPc4d1	podobná
řeči	řeč	k1gFnPc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
přestali	přestat	k5eAaPmAgMnP	přestat
mít	mít	k5eAaImF	mít
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gMnSc3	on
nechtěl	chtít	k5eNaImAgMnS	chtít
dávat	dávat	k5eAaImF	dávat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jej	on	k3xPp3gMnSc4	on
nezval	zvát	k5eNaImAgMnS	zvát
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
musel	muset	k5eAaImAgMnS	muset
tedy	tedy	k9	tedy
opustit	opustit	k5eAaPmF	opustit
Malou	malý	k2eAgFnSc4d1	malá
Stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
jít	jít	k5eAaImF	jít
žebrat	žebrat	k5eAaImF	žebrat
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jej	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
domu	dům	k1gInSc2	dům
nepouštěl	pouštět	k5eNaImAgInS	pouštět
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
musel	muset	k5eAaImAgMnS	muset
sbírat	sbírat	k5eAaImF	sbírat
jen	jen	k9	jen
peníze	peníz	k1gInPc4	peníz
do	do	k7c2	do
čepice	čepice	k1gFnSc2	čepice
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zle	zle	k6eAd1	zle
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
vedlo	vést	k5eAaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
jednou	jednou	k6eAd1	jednou
jej	on	k3xPp3gMnSc4	on
našli	najít	k5eAaPmAgMnP	najít
na	na	k7c6	na
Újezdě	Újezd	k1gInSc6	Újezd
bez	bez	k7c2	bez
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
kabátu	kabát	k1gInSc6	kabát
a	a	k8xC	a
kalhotách	kalhoty	k1gFnPc6	kalhoty
umrzlého	umrzlý	k2eAgMnSc2d1	umrzlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
měkkém	měkký	k2eAgNnSc6d1	měkké
srdci	srdce	k1gNnSc6	srdce
paní	paní	k1gFnSc2	paní
Rusky	Ruska	k1gFnSc2	Ruska
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Paní	paní	k1gFnSc1	paní
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
nebožtíkovi	nebožtík	k1gMnSc6	nebožtík
Rusovi	Rus	k1gMnSc6	Rus
<g/>
,	,	kIx,	,
hostinském	hostinský	k2eAgInSc6d1	hostinský
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Gráfovské	Gráfovská	k1gFnSc2	Gráfovská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývaly	bývat	k5eAaImAgInP	bývat
nejkrásnější	krásný	k2eAgInPc1d3	nejkrásnější
kanonýrské	kanonýrský	k2eAgInPc1d1	kanonýrský
bály	bál	k1gInPc1	bál
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
jedinou	jediný	k2eAgFnSc7d1	jediná
zábavou	zábava	k1gFnSc7	zábava
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodila	chodit	k5eAaImAgFnS	chodit
na	na	k7c4	na
pohřby	pohřeb	k1gInPc4	pohřeb
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
úplně	úplně	k6eAd1	úplně
cizím	cizí	k2eAgMnPc3d1	cizí
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
životě	život	k1gInSc6	život
i	i	k9	i
jejich	jejich	k3xOp3gMnPc2	jejich
blízkých	blízký	k2eAgMnPc2d1	blízký
roznášela	roznášet	k5eAaImAgFnS	roznášet
ošklivé	ošklivý	k2eAgFnPc4d1	ošklivá
řeči	řeč	k1gFnPc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
lidem	lid	k1gInSc7	lid
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
měšťana	měšťan	k1gMnSc2	měšťan
a	a	k8xC	a
kupce	kupec	k1gMnSc2	kupec
Josefa	Josef	k1gMnSc2	Josef
Velše	Velš	k1gMnSc2	Velš
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zavolali	zavolat	k5eAaPmAgMnP	zavolat
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
pak	pak	k6eAd1	pak
zakázala	zakázat	k5eAaPmAgFnS	zakázat
na	na	k7c4	na
pohřby	pohřeb	k1gInPc4	pohřeb
chodit	chodit	k5eAaImF	chodit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
paní	paní	k1gFnSc1	paní
Ruska	Rusko	k1gNnSc2	Rusko
nepřišla	přijít	k5eNaPmAgFnS	přijít
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
k	k	k7c3	k
Oujezdské	Oujezdský	k2eAgFnSc3d1	Oujezdská
bráně	brána	k1gFnSc3	brána
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
musel	muset	k5eAaImAgInS	muset
každý	každý	k3xTgInSc4	každý
pohřeb	pohřeb	k1gInSc4	pohřeb
jít	jít	k5eAaImF	jít
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
vycházela	vycházet	k5eAaImAgFnS	vycházet
před	před	k7c4	před
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Večerní	večerní	k2eAgInPc1d1	večerní
šplechty	šplecht	k1gInPc1	šplecht
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
vysokoškolských	vysokoškolský	k2eAgNnPc2d1	vysokoškolské
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
postava	postava	k1gFnSc1	postava
Hovora	hovora	k1gMnSc1	hovora
<g/>
)	)	kIx)	)
za	za	k7c2	za
hezkých	hezký	k2eAgInPc2d1	hezký
večerů	večer	k1gInPc2	večer
schází	scházet	k5eAaImIp3nS	scházet
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
domu	dům	k1gInSc2	dům
za	za	k7c7	za
vikýřem	vikýř	k1gInSc7	vikýř
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
kamarády	kamarád	k1gMnPc7	kamarád
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kouřili	kouřit	k5eAaImAgMnP	kouřit
a	a	k8xC	a
probírali	probírat	k5eAaImAgMnP	probírat
různá	různý	k2eAgNnPc4d1	různé
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
den	den	k1gInSc4	den
vybírá	vybírat	k5eAaImIp3nS	vybírat
téma	téma	k1gNnSc4	téma
k	k	k7c3	k
diskusi	diskuse	k1gFnSc3	diskuse
Jäkl	Jäkla	k1gFnPc2	Jäkla
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
společně	společně	k6eAd1	společně
s	s	k7c7	s
Kupkou	kupka	k1gFnSc7	kupka
a	a	k8xC	a
Novomlýnským	novomlýnský	k2eAgInSc7d1	novomlýnský
vyprávět	vyprávět	k5eAaImF	vyprávět
nejstarší	starý	k2eAgFnSc4d3	nejstarší
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Jäkl	Jäkl	k1gInSc1	Jäkl
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Lízince	Lízinka	k1gFnSc3	Lízinka
Perálkové	Perálek	k1gMnPc1	Perálek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nečekaně	nečekaně	k6eAd1	nečekaně
porodila	porodit	k5eAaPmAgFnS	porodit
kluka	kluk	k1gMnSc4	kluk
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
střechou	střecha	k1gFnSc7	střecha
poslouchala	poslouchat	k5eAaImAgNnP	poslouchat
děvčata	děvče	k1gNnPc1	děvče
<g/>
,	,	kIx,	,
vyběhnou	vyběhnout	k5eAaPmIp3nP	vyběhnout
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Jäkla	Jäklo	k1gNnSc2	Jäklo
za	za	k7c7	za
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doktor	doktor	k1gMnSc1	doktor
Kazisvět	kazisvět	k1gMnSc1	kazisvět
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
podivínského	podivínský	k2eAgMnSc2d1	podivínský
doktora	doktor	k1gMnSc2	doktor
Heriberta	Heribert	k1gMnSc2	Heribert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nikdy	nikdy	k6eAd1	nikdy
neprovozoval	provozovat	k5eNaImAgMnS	provozovat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
procházce	procházka	k1gFnSc6	procházka
potkal	potkat	k5eAaPmAgInS	potkat
pohřební	pohřební	k2eAgInSc1d1	pohřební
průvod	průvod	k1gInSc1	průvod
pochovávající	pochovávající	k2eAgFnSc2d1	pochovávající
pana	pan	k1gMnSc4	pan
Schepelera	Schepeler	k1gMnSc4	Schepeler
<g/>
,	,	kIx,	,
váženého	vážený	k2eAgMnSc4d1	vážený
a	a	k8xC	a
bohatého	bohatý	k2eAgMnSc4d1	bohatý
radu	rada	k1gMnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
průvod	průvod	k1gInSc1	průvod
míjel	míjet	k5eAaImAgInS	míjet
Heriberta	Heribert	k1gMnSc4	Heribert
<g/>
,	,	kIx,	,
spadla	spadnout	k5eAaPmAgFnS	spadnout
rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
z	z	k7c2	z
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Heribert	Heribert	k1gMnSc1	Heribert
si	se	k3xPyFc3	se
všimnul	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pan	pan	k1gMnSc1	pan
rada	rada	k1gMnSc1	rada
není	být	k5eNaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gInSc4	on
zanést	zanést	k5eAaPmF	zanést
do	do	k7c2	do
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pana	pan	k1gMnSc4	pan
radu	rada	k1gMnSc4	rada
křísil	křísit	k5eAaImAgMnS	křísit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
vyléčený	vyléčený	k2eAgInSc1d1	vyléčený
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
zmínily	zmínit	k5eAaPmAgFnP	zmínit
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
dědit	dědit	k5eAaImF	dědit
jeho	jeho	k3xOp3gInSc4	jeho
velký	velký	k2eAgInSc4d1	velký
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
radost	radost	k1gFnSc4	radost
<g/>
;	;	kIx,	;
dali	dát	k5eAaPmAgMnP	dát
doktoru	doktor	k1gMnSc3	doktor
Heribertovi	Heribert	k1gMnSc3	Heribert
přezdívku	přezdívka	k1gFnSc4	přezdívka
Kazisvět	kazisvět	k1gMnSc1	kazisvět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hastrman	hastrman	k1gMnSc1	hastrman
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
pana	pan	k1gMnSc2	pan
Rybáře	Rybář	k1gMnSc4	Rybář
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
nikdo	nikdo	k3yNnSc1	nikdo
neříkal	říkat	k5eNaImAgInS	říkat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
"	"	kIx"	"
<g/>
hastrman	hastrman	k1gMnSc1	hastrman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
malé	malý	k2eAgFnSc6d1	malá
výšce	výška	k1gFnSc6	výška
nosil	nosit	k5eAaImAgInS	nosit
zelený	zelený	k2eAgInSc1d1	zelený
kabátek	kabátek	k1gInSc1	kabátek
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
šosy	šos	k1gInPc7	šos
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Bydlel	bydlet	k5eAaImAgMnS	bydlet
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
rozmlouval	rozmlouvat	k5eAaImAgMnS	rozmlouvat
s	s	k7c7	s
kanovníky	kanovník	k1gMnPc7	kanovník
a	a	k8xC	a
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
doma	doma	k6eAd1	doma
velkou	velký	k2eAgFnSc4d1	velká
sbírku	sbírka	k1gFnSc4	sbírka
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
drahé	drahý	k2eAgInPc1d1	drahý
ty	ten	k3xDgInPc1	ten
kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Zašel	zajít	k5eAaPmAgMnS	zajít
tedy	tedy	k9	tedy
s	s	k7c7	s
krabicí	krabice	k1gFnSc7	krabice
kamenů	kámen	k1gInPc2	kámen
za	za	k7c7	za
profesorem	profesor	k1gMnSc7	profesor
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
přírodopisu	přírodopis	k1gInSc2	přírodopis
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kameny	kámen	k1gInPc1	kámen
nemají	mít	k5eNaImIp3nP	mít
velké	velký	k2eAgFnPc4d1	velká
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
pan	pan	k1gMnSc1	pan
Rybář	Rybář	k1gMnSc1	Rybář
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnPc4	svůj
sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
ji	on	k3xPp3gFnSc4	on
vyhazovat	vyhazovat	k5eAaImF	vyhazovat
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
jeho	jeho	k3xOp3gFnSc6	jeho
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgMnSc4	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gInSc4	on
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
nedělal	dělat	k5eNaImAgMnS	dělat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgInPc1	ten
kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
bohatstvím	bohatství	k1gNnPc3	bohatství
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc2	on
samotného	samotný	k2eAgNnSc2d1	samotné
a	a	k8xC	a
cenné	cenný	k2eAgFnPc1d1	cenná
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pan	pan	k1gMnSc1	pan
Rybář	Rybář	k1gMnSc1	Rybář
je	být	k5eAaImIp3nS	být
bohatstvím	bohatství	k1gNnSc7	bohatství
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k8xC	jak
si	se	k3xPyFc3	se
pan	pan	k1gMnSc1	pan
Vorel	Vorel	k1gMnSc1	Vorel
nakouřil	nakouřit	k5eAaBmAgMnS	nakouřit
pěnovku	pěnovka	k1gFnSc4	pěnovka
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Vorel	Vorel	k1gMnSc1	Vorel
se	se	k3xPyFc4	se
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
na	na	k7c4	na
Malou	malý	k2eAgFnSc4d1	malá
Stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgInS	otevřít
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
krupařský	krupařský	k2eAgInSc4d1	krupařský
krám	krám	k1gInSc4	krám
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jeho	on	k3xPp3gInSc4	on
obchod	obchod	k1gInSc4	obchod
nenavštěvovali	navštěvovat	k5eNaImAgMnP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
pana	pan	k1gMnSc4	pan
Vorla	Vorel	k1gMnSc4	Vorel
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kouří	kouřit	k5eAaImIp3nS	kouřit
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
krámě	krám	k1gInSc6	krám
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pěnovky	pěnovka	k1gFnSc2	pěnovka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
tam	tam	k6eAd1	tam
zavítala	zavítat	k5eAaPmAgFnS	zavítat
slečna	slečna	k1gFnSc1	slečna
Poldýnka	Poldýnka	k1gFnSc1	Poldýnka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
obchodě	obchod	k1gInSc6	obchod
zakouřeno	zakouřen	k2eAgNnSc1d1	zakouřeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
mouka	mouka	k1gFnSc1	mouka
načichla	načichnout	k5eAaPmAgFnS	načichnout
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
ho	on	k3xPp3gNnSc4	on
začali	začít	k5eAaPmAgMnP	začít
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
uzený	uzený	k2eAgMnSc1d1	uzený
krupař	krupař	k1gMnSc1	krupař
<g/>
"	"	kIx"	"
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
tam	tam	k6eAd1	tam
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nepřišel	přijít	k5eNaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Vorel	Vorel	k1gMnSc1	Vorel
zkrachoval	zkrachovat	k5eAaPmAgMnS	zkrachovat
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Jenomže	jenomže	k8xC	jenomže
v	v	k7c4	v
den	den	k1gInSc4	den
stěhování	stěhování	k1gNnSc2	stěhování
nevycházel	vycházet	k5eNaImAgMnS	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gInSc4	jeho
krám	krám	k1gInSc4	krám
otevřela	otevřít	k5eAaPmAgFnS	otevřít
násilím	násilí	k1gNnSc7	násilí
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
otevření	otevření	k1gNnSc4	otevření
dveří	dveře	k1gFnPc2	dveře
uviděli	uvidět	k5eAaPmAgMnP	uvidět
pana	pan	k1gMnSc4	pan
Vorla	Vorel	k1gMnSc4	Vorel
oběšeného	oběšený	k2eAgInSc2d1	oběšený
hned	hned	k6eAd1	hned
za	za	k7c7	za
dveřmi	dveře	k1gFnPc7	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
komisař	komisař	k1gMnSc1	komisař
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
kapsy	kapsa	k1gFnSc2	kapsa
pěnovku	pěnovka	k1gFnSc4	pěnovka
a	a	k8xC	a
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Takhle	takhle	k6eAd1	takhle
krásně	krásně	k6eAd1	krásně
nakouřenou	nakouřený	k2eAgFnSc4d1	nakouřená
pěnovku	pěnovka	k1gFnSc4	pěnovka
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k9	ještě
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
U	u	k7c2	u
Tří	tři	k4xCgNnPc2	tři
lilií	lilium	k1gNnPc2	lilium
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
popisuje	popisovat	k5eAaImIp3nS	popisovat
překvapivý	překvapivý	k2eAgInSc1d1	překvapivý
moment	moment	k1gInSc1	moment
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
bouřce	bouřka	k1gFnSc6	bouřka
zdržel	zdržet	k5eAaPmAgInS	zdržet
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
tančilo	tančit	k5eAaImAgNnS	tančit
<g/>
.	.	kIx.	.
</s>
<s>
Upoutala	upoutat	k5eAaPmAgFnS	upoutat
jej	on	k3xPp3gInSc4	on
jedna	jeden	k4xCgFnSc1	jeden
dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
velkýma	velký	k2eAgNnPc7d1	velké
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
on	on	k3xPp3gMnSc1	on
upoutal	upoutat	k5eAaPmAgMnS	upoutat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
večer	večer	k1gInSc1	večer
ji	on	k3xPp3gFnSc4	on
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
přiběhla	přiběhnout	k5eAaPmAgFnS	přiběhnout
jiná	jiný	k2eAgFnSc1d1	jiná
dívka	dívka	k1gFnSc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
pak	pak	k6eAd1	pak
zmizela	zmizet	k5eAaPmAgFnS	zmizet
do	do	k7c2	do
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
lhostejným	lhostejný	k2eAgInSc7d1	lhostejný
hlasem	hlas	k1gInSc7	hlas
oznámila	oznámit	k5eAaPmAgFnS	oznámit
své	svůj	k3xOyFgFnSc3	svůj
kamarádce	kamarádka	k1gFnSc3	kamarádka
jako	jako	k8xC	jako
důvod	důvod	k1gInSc1	důvod
své	svůj	k3xOyFgFnSc2	svůj
krátké	krátký	k2eAgFnSc2d1	krátká
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
právě	právě	k6eAd1	právě
umřela	umřít	k5eAaPmAgFnS	umřít
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
za	za	k7c4	za
zuřící	zuřící	k2eAgFnPc4d1	zuřící
bouřky	bouřka	k1gFnPc4	bouřka
společně	společně	k6eAd1	společně
vytrácejí	vytrácet	k5eAaImIp3nP	vytrácet
z	z	k7c2	z
taneční	taneční	k2eAgFnSc2d1	taneční
zábavy	zábava	k1gFnSc2	zábava
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
tělesnému	tělesný	k2eAgNnSc3d1	tělesné
sblížení	sblížení	k1gNnSc3	sblížení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
mše	mše	k1gFnSc1	mše
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
je	být	k5eAaImIp3nS	být
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
nechal	nechat	k5eAaPmAgMnS	nechat
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
sám	sám	k3xTgInSc4	sám
zavřít	zavřít	k5eAaPmF	zavřít
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Slyšel	slyšet	k5eAaImAgMnS	slyšet
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
vždy	vždy	k6eAd1	vždy
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
ve	v	k7c6	v
svatováclavské	svatováclavský	k2eAgFnSc6d1	Svatováclavská
kapli	kaple	k1gFnSc6	kaple
mše	mše	k1gFnSc2	mše
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
všech	všecek	k3xTgMnPc2	všecek
králů	král	k1gMnPc2	král
a	a	k8xC	a
svatých	svatá	k1gFnPc2	svatá
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
s	s	k7c7	s
přibývající	přibývající	k2eAgFnSc7d1	přibývající
pozdní	pozdní	k2eAgFnSc7d1	pozdní
hodinou	hodina	k1gFnSc7	hodina
jej	on	k3xPp3gInSc2	on
začaly	začít	k5eAaPmAgInP	začít
sužovat	sužovat	k5eAaImF	sužovat
hlad	hlad	k1gInSc4	hlad
a	a	k8xC	a
chlad	chlad	k1gInSc4	chlad
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
mysli	mysl	k1gFnSc2	mysl
se	se	k3xPyFc4	se
vloudily	vloudit	k5eAaPmAgFnP	vloudit
hrůzostrašné	hrůzostrašný	k2eAgFnPc1d1	hrůzostrašná
představy	představa	k1gFnPc1	představa
o	o	k7c4	o
mši	mše	k1gFnSc4	mše
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
tak	tak	k9	tak
živě	živě	k6eAd1	živě
<g/>
,	,	kIx,	,
až	až	k9	až
se	s	k7c7	s
strachy	strach	k1gInPc7	strach
rozplakal	rozplakat	k5eAaPmAgMnS	rozplakat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
usnul	usnout	k5eAaPmAgInS	usnout
a	a	k8xC	a
probudil	probudit	k5eAaPmAgInS	probudit
se	se	k3xPyFc4	se
až	až	k9	až
ráno	ráno	k6eAd1	ráno
na	na	k7c4	na
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spatřil	spatřit	k5eAaPmAgMnS	spatřit
svou	svůj	k3xOyFgFnSc4	svůj
velmi	velmi	k6eAd1	velmi
smutnou	smutný	k2eAgFnSc4d1	smutná
matku	matka	k1gFnSc4	matka
s	s	k7c7	s
tetou	teta	k1gFnSc7	teta
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
měl	mít	k5eAaImAgMnS	mít
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mši	mše	k1gFnSc6	mše
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
počkal	počkat	k5eAaPmAgMnS	počkat
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
a	a	k8xC	a
s	s	k7c7	s
pláčem	pláč	k1gInSc7	pláč
políbil	políbit	k5eAaPmAgMnS	políbit
matce	matka	k1gFnSc3	matka
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc1	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
,	,	kIx,	,
že	že	k8xS	že
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
o	o	k7c4	o
půl	půl	k1xP	půl
jedné	jeden	k4xCgFnSc2	jeden
s	s	k7c7	s
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
nebylo	být	k5eNaImAgNnS	být
rozbořeno	rozbořit	k5eAaPmNgNnS	rozbořit
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
patnáctiletí	patnáctiletý	k2eAgMnPc1d1	patnáctiletý
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
nechyběl	chybět	k5eNaImAgMnS	chybět
ani	ani	k8xC	ani
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zahájí	zahájit	k5eAaPmIp3nS	zahájit
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
svrhnou	svrhnout	k5eAaPmIp3nP	svrhnout
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Označili	označit	k5eAaPmAgMnP	označit
se	s	k7c7	s
přezdívkami	přezdívka	k1gFnPc7	přezdívka
významných	významný	k2eAgMnPc2d1	významný
českých	český	k2eAgMnPc2d1	český
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
historie	historie	k1gFnSc2	historie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
Prokůpek	Prokůpek	k1gMnSc1	Prokůpek
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Husi	Hus	k1gFnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
dobytím	dobytí	k1gNnSc7	dobytí
citadely	citadela	k1gFnSc2	citadela
na	na	k7c6	na
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
hradbách	hradba	k1gFnPc6	hradba
nad	nad	k7c7	nad
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
boji	boj	k1gInPc7	boj
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
zakončena	zakončit	k5eAaPmNgFnS	zakončit
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dobytím	dobytí	k1gNnSc7	dobytí
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
si	se	k3xPyFc3	se
koupili	koupit	k5eAaPmAgMnP	koupit
z	z	k7c2	z
našetřených	našetřený	k2eAgInPc2d1	našetřený
peněz	peníze	k1gInPc2	peníze
bambitku	bambitka	k1gFnSc4	bambitka
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
zlatek	zlatka	k1gFnPc2	zlatka
na	na	k7c4	na
střelný	střelný	k2eAgInSc4d1	střelný
prach	prach	k1gInSc4	prach
dostal	dostat	k5eAaPmAgMnS	dostat
předem	předem	k6eAd1	předem
hokynář	hokynář	k1gMnSc1	hokynář
Pohorák	Pohorák	k1gMnSc1	Pohorák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přivážel	přivážet	k5eAaImAgMnS	přivážet
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
na	na	k7c4	na
trh	trh	k1gInSc4	trh
čerstvá	čerstvý	k2eAgNnPc4d1	čerstvé
kuřata	kuře	k1gNnPc4	kuře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
uvedený	uvedený	k2eAgInSc4d1	uvedený
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
chlapci	chlapec	k1gMnPc1	chlapec
dostavili	dostavit	k5eAaPmAgMnP	dostavit
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
místa	místo	k1gNnPc4	místo
kolem	kolem	k7c2	kolem
citadely	citadela	k1gFnSc2	citadela
a	a	k8xC	a
čekali	čekat	k5eAaImAgMnP	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Pohorák	Pohorák	k1gInSc1	Pohorák
s	s	k7c7	s
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
chlapci	chlapec	k1gMnPc1	chlapec
doslechli	doslechnout	k5eAaPmAgMnP	doslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
hokynáře	hokynář	k1gMnPc4	hokynář
sebrala	sebrat	k5eAaPmAgFnS	sebrat
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
a	a	k8xC	a
zalekli	zaleknout	k5eAaPmAgMnP	zaleknout
se	se	k3xPyFc4	se
a	a	k8xC	a
utekli	utéct	k5eAaPmAgMnP	utéct
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pohorák	Pohorák	k1gMnSc1	Pohorák
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
trh	trh	k1gInSc4	trh
opilý	opilý	k2eAgInSc4d1	opilý
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
nic	nic	k6eAd1	nic
neprodal	prodat	k5eNaPmAgMnS	prodat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ve	v	k7c6	v
špatné	špatný	k2eAgFnSc6d1	špatná
náladě	nálada	k1gFnSc6	nálada
a	a	k8xC	a
zmožen	zmožen	k2eAgInSc1d1	zmožen
pitím	pití	k1gNnSc7	pití
usnul	usnout	k5eAaPmAgInS	usnout
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
vozem	vůz	k1gInSc7	vůz
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
našla	najít	k5eAaPmAgFnS	najít
u	u	k7c2	u
kašny	kašna	k1gFnSc2	kašna
nová	nový	k2eAgFnSc1d1	nová
bambitka	bambitka	k1gFnSc1	bambitka
a	a	k8xC	a
Pohorák	Pohorák	k1gMnSc1	Pohorák
zapíral	zapírat	k5eAaImAgMnS	zapírat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
nevzpomínal	vzpomínat	k5eNaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
od	od	k7c2	od
kluků	kluk	k1gMnPc2	kluk
obdržel	obdržet	k5eAaPmAgInS	obdržet
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Psáno	psát	k5eAaImNgNnS	psát
o	o	k7c6	o
letošních	letošní	k2eAgFnPc6d1	letošní
Dušičkách	Dušičky	k1gFnPc6	Dušičky
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
osamělé	osamělý	k2eAgFnSc2d1	osamělá
slečny	slečna	k1gFnSc2	slečna
Máry	Máry	k1gFnSc2	Máry
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
Dušiček	Dušičky	k1gFnPc2	Dušičky
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
děvčátkem	děvčátko	k1gNnSc7	děvčátko
zapůjčeným	zapůjčený	k2eAgMnPc3d1	zapůjčený
ze	z	k7c2	z
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nP	chodit
mezi	mezi	k7c4	mezi
hroby	hrob	k1gInPc4	hrob
a	a	k8xC	a
na	na	k7c4	na
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
položí	položit	k5eAaPmIp3nS	položit
bílé	bílý	k2eAgInPc1d1	bílý
věnce	věnec	k1gInPc1	věnec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
její	její	k3xOp3gFnSc4	její
ruku	ruka	k1gFnSc4	ruka
se	se	k3xPyFc4	se
ucházeli	ucházet	k5eAaImAgMnP	ucházet
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
najednou	najednou	k6eAd1	najednou
–	–	k?	–
rytec	rytec	k1gMnSc1	rytec
Jan	Jan	k1gMnSc1	Jan
Rechner	Rechner	k1gMnSc1	Rechner
a	a	k8xC	a
kupec	kupec	k1gMnSc1	kupec
Vilém	Vilém	k1gMnSc1	Vilém
Cibulka	Cibulka	k1gMnSc1	Cibulka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
dobří	dobrý	k2eAgMnPc1d1	dobrý
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
o	o	k7c6	o
obou	dva	k4xCgFnPc6	dva
kolovaly	kolovat	k5eAaImAgFnP	kolovat
pověsti	pověst	k1gFnPc1	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
flamendři	flamendr	k1gMnPc1	flamendr
<g/>
"	"	kIx"	"
a	a	k8xC	a
návštěvám	návštěva	k1gFnPc3	návštěva
hospod	hospod	k?	hospod
věnují	věnovat	k5eAaPmIp3nP	věnovat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
než	než	k8xS	než
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
těchto	tento	k3xDgMnPc2	tento
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
Máry	Máry	k1gFnSc1	Máry
svěřila	svěřit	k5eAaPmAgFnS	svěřit
své	svůj	k3xOyFgFnPc4	svůj
dobré	dobrý	k2eAgFnPc4d1	dobrá
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnPc1d1	stará
vdově	vdova	k1gFnSc3	vdova
paní	paní	k1gFnSc7	paní
Nocarové	Nocarové	k2eAgFnSc7d1	Nocarové
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgMnPc1	dva
pánové	pán	k1gMnPc1	pán
zaslali	zaslat	k5eAaPmAgMnP	zaslat
slečně	slečna	k1gFnSc6	slečna
Máry	Máry	k1gFnSc1	Máry
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
přebírat	přebírat	k5eAaImF	přebírat
známost	známost	k1gFnSc4	známost
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
nemají	mít	k5eNaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Slečna	slečna	k1gFnSc1	slečna
Máry	Máry	k1gFnSc2	Máry
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
paní	paní	k1gFnSc2	paní
Nocarové	Nocarová	k1gFnSc2	Nocarová
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nerozmyslí	rozmyslet	k5eNaPmIp3nP	rozmyslet
<g/>
.	.	kIx.	.
</s>
<s>
Nevyjádřil	vyjádřit	k5eNaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
žádný	žádný	k1gMnSc1	žádný
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
potkávali	potkávat	k5eAaImAgMnP	potkávat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pan	pan	k1gMnSc1	pan
Rechner	Rechner	k1gMnSc1	Rechner
<g/>
.	.	kIx.	.
</s>
<s>
Slečna	slečna	k1gFnSc1	slečna
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
ji	on	k3xPp3gFnSc4	on
pan	pan	k1gMnSc1	pan
Cibulka	Cibulka	k1gMnSc1	Cibulka
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
on	on	k3xPp3gMnSc1	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Slečna	slečna	k1gFnSc1	slečna
Máry	Máry	k1gFnSc2	Máry
pak	pak	k6eAd1	pak
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c4	na
Dušičky	Dušičky	k1gFnPc4	Dušičky
nosila	nosit	k5eAaImAgNnP	nosit
k	k	k7c3	k
jejich	jejich	k3xOp3gInPc3	jejich
hrobům	hrob	k1gInPc3	hrob
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Figurky	figurka	k1gFnSc2	figurka
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
povídka	povídka	k1gFnSc1	povídka
je	být	k5eAaImIp3nS	být
poskládaná	poskládaný	k2eAgFnSc1d1	poskládaná
z	z	k7c2	z
drobných	drobný	k2eAgInPc2d1	drobný
útržků	útržek	k1gInPc2	útržek
života	život	k1gInSc2	život
mladého	mladý	k2eAgMnSc4d1	mladý
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
několikrát	několikrát	k6eAd1	několikrát
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
povídky	povídka	k1gFnSc2	povídka
je	být	k5eAaImIp3nS	být
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
Krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
advokátní	advokátní	k2eAgFnPc4d1	advokátní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
proto	proto	k8xC	proto
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
klidné	klidný	k2eAgNnSc1d1	klidné
bydlení	bydlení	k1gNnSc1	bydlení
v	v	k7c6	v
podnájmu	podnájem	k1gInSc6	podnájem
u	u	k7c2	u
konduktorky	konduktorka	k1gFnSc2	konduktorka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgMnS	připravit
na	na	k7c4	na
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
obyvateli	obyvatel	k1gMnPc7	obyvatel
domu	dům	k1gInSc2	dům
<g/>
:	:	kIx,	:
malířem	malíř	k1gMnSc7	malíř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
ženou	hnát	k5eAaImIp3nP	hnát
šišlající	šišlající	k2eAgInPc4d1	šišlající
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
zlobivým	zlobivý	k2eAgNnSc7d1	zlobivé
a	a	k8xC	a
často	často	k6eAd1	často
trestaným	trestaný	k2eAgMnSc7d1	trestaný
malířovým	malířův	k2eAgMnSc7d1	malířův
synem	syn	k1gMnSc7	syn
Pepíkem	Pepík	k1gMnSc7	Pepík
<g/>
,	,	kIx,	,
trochu	trochu	k6eAd1	trochu
hypochondrickým	hypochondrický	k2eAgMnSc7d1	hypochondrický
panem	pan	k1gMnSc7	pan
domácím	domácí	k1gMnSc7	domácí
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
Otýlií	Otýlie	k1gFnSc7	Otýlie
<g/>
,	,	kIx,	,
podivínským	podivínský	k2eAgMnSc7d1	podivínský
komentátorem	komentátor	k1gMnSc7	komentátor
všeho	všecek	k3xTgNnSc2	všecek
dění	dění	k1gNnSc2	dění
v	v	k7c6	v
domě	dům	k1gInSc6	dům
a	a	k8xC	a
provokatérem	provokatér	k1gMnSc7	provokatér
Provazníkem	Provazník	k1gMnSc7	Provazník
léčeným	léčený	k2eAgMnSc7d1	léčený
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
choromyslné	choromyslný	k1gMnPc4	choromyslný
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
Krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
učí	učit	k5eAaImIp3nS	učit
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
všechny	všechen	k3xTgFnPc4	všechen
snahy	snaha	k1gFnPc4	snaha
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obyvatel	obyvatel	k1gMnPc2	obyvatel
domu	dům	k1gInSc2	dům
o	o	k7c4	o
nějaké	nějaký	k3yIgNnSc4	nějaký
sblížení	sblížení	k1gNnSc4	sblížení
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
všímá	všímat	k5eAaImIp3nS	všímat
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
domě	dům	k1gInSc6	dům
a	a	k8xC	a
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
také	také	k9	také
více	hodně	k6eAd2	hodně
účastní	účastnit	k5eAaImIp3nS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
sousedy	soused	k1gMnPc4	soused
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
tráví	trávit	k5eAaImIp3nP	trávit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
odpolední	odpolední	k2eAgFnPc1d1	odpolední
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
besídkách	besídka	k1gFnPc6	besídka
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Počne	počnout	k5eAaPmIp3nS	počnout
během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
pociťovat	pociťovat	k5eAaImF	pociťovat
náklonnost	náklonnost	k1gFnSc4	náklonnost
k	k	k7c3	k
slečně	slečna	k1gFnSc3	slečna
Otýlii	Otýlie	k1gFnSc3	Otýlie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyjádří	vyjádřit	k5eNaPmIp3nS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
díky	díky	k7c3	díky
všem	všecek	k3xTgFnPc3	všecek
těmto	tento	k3xDgFnPc3	tento
aktivitám	aktivita	k1gFnPc3	aktivita
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
a	a	k8xC	a
méně	málo	k6eAd2	málo
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
advokátní	advokátní	k2eAgFnPc4d1	advokátní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
domě	dům	k1gInSc6	dům
něco	něco	k3yInSc4	něco
vyrušuje	vyrušovat	k5eAaImIp3nS	vyrušovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nemá	mít	k5eNaImIp3nS	mít
potřebný	potřebný	k2eAgInSc4d1	potřebný
klid	klid	k1gInSc4	klid
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
a	a	k8xC	a
potkává	potkávat	k5eAaImIp3nS	potkávat
své	svůj	k3xOyFgMnPc4	svůj
dřívější	dřívější	k2eAgMnPc4d1	dřívější
kamarády	kamarád	k1gMnPc4	kamarád
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
spěje	spět	k5eAaImIp3nS	spět
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
z	z	k7c2	z
malostranského	malostranský	k2eAgInSc2d1	malostranský
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
všeho	všecek	k3xTgNnSc2	všecek
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vystupování	vystupování	k1gNnSc4	vystupování
jeho	jeho	k3xOp3gFnSc2	jeho
bytné	bytný	k2eAgFnSc2d1	bytná
konduktorky	konduktorka	k1gFnSc2	konduktorka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
manželovi	manžel	k1gMnSc6	manžel
pouze	pouze	k6eAd1	pouze
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Krumlovský	krumlovský	k2eAgInSc1d1	krumlovský
jej	on	k3xPp3gMnSc4	on
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Konduktorka	konduktorka	k1gFnSc1	konduktorka
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
stále	stále	k6eAd1	stále
vtírá	vtírat	k5eAaImIp3nS	vtírat
do	do	k7c2	do
přízně	přízeň	k1gFnSc2	přízeň
Krumlovského	krumlovský	k2eAgNnSc2d1	krumlovské
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
později	pozdě	k6eAd2	pozdě
poněkud	poněkud	k6eAd1	poněkud
ochladla	ochladnout	k5eAaPmAgFnS	ochladnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
záletná	záletný	k2eAgFnSc1d1	záletná
konduktorka	konduktorka	k1gFnSc1	konduktorka
stýká	stýkat	k5eAaImIp3nS	stýkat
s	s	k7c7	s
nadporučíkem	nadporučík	k1gMnSc7	nadporučík
Rybáckým	rybácký	k2eAgMnSc7d1	rybácký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Krumlovského	krumlovský	k2eAgNnSc2d1	krumlovské
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
o	o	k7c4	o
čest	čest	k1gFnSc4	čest
šavlí	šavle	k1gFnPc2	šavle
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
neuctivé	uctivý	k2eNgFnSc3d1	neuctivá
poznámce	poznámka	k1gFnSc3	poznámka
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
a	a	k8xC	a
konduktorce	konduktorka	k1gFnSc6	konduktorka
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
souboj	souboj	k1gInSc4	souboj
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
odstěhuje	odstěhovat	k5eAaPmIp3nS	odstěhovat
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
skládá	skládat	k5eAaImIp3nS	skládat
poslední	poslední	k2eAgFnSc4d1	poslední
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgNnSc4d1	filmové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
zpracování	zpracování	k1gNnSc4	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Povídky	povídka	k1gFnSc2	povídka
malostranské	malostranský	k2eAgFnSc2d1	Malostranská
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
třídílný	třídílný	k2eAgInSc1d1	třídílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Pavel	Pavel	k1gMnSc1	Pavel
Háša	Háša	k1gMnSc1	Háša
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
O	o	k7c6	o
měkkém	měkký	k2eAgNnSc6d1	měkké
srdci	srdce	k1gNnSc6	srdce
paní	paní	k1gFnSc2	paní
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
;	;	kIx,	;
Doktor	doktor	k1gMnSc1	doktor
Kazisvět	kazisvět	k1gMnSc1	kazisvět
</s>
</p>
<p>
<s>
Lízinka	Lízinka	k1gFnSc1	Lízinka
<g/>
;	;	kIx,	;
Slečna	slečna	k1gFnSc1	slečna
Máry	Máry	k1gFnSc2	Máry
</s>
</p>
<p>
<s>
Přivedla	přivést	k5eAaPmAgFnS	přivést
žebráka	žebrák	k1gMnSc4	žebrák
na	na	k7c4	na
mizinu	mizina	k1gFnSc4	mizina
<g/>
;	;	kIx,	;
Hastrman	hastrman	k1gMnSc1	hastrman
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Povídka	povídka	k1gFnSc1	povídka
malostranská	malostranský	k2eAgFnSc1d1	Malostranská
československý	československý	k2eAgInSc4d1	československý
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Figurky	figurka	k1gFnSc2	figurka
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Krejčík	Krejčík	k1gMnSc1	Krejčík
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
Hastrman	hastrman	k1gMnSc1	hastrman
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Iva	Ivo	k1gMnSc2	Ivo
Paukerta	Paukert	k1gMnSc2	Paukert
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Eduard	Eduard	k1gMnSc1	Eduard
Kohout	Kohout	k1gMnSc1	Kohout
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Týden	týden	k1gInSc1	týden
v	v	k7c6	v
tichém	tichý	k2eAgInSc6d1	tichý
domě	dům	k1gInSc6	dům
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Krejčík	Krejčík	k1gMnSc1	Krejčík
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
Vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
–	–	k?	–
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Figurky	figurka	k1gFnSc2	figurka
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Slavíček	Slavíček	k1gMnSc1	Slavíček
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
malostranské	malostranský	k2eAgFnPc1d1	Malostranská
<g/>
,	,	kIx,	,
ARSCI	ARSCI	kA	ARSCI
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NERUDA	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
malostranské	malostranský	k2eAgFnSc2d1	Malostranská
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grégr	Grégr	k1gMnSc1	Grégr
a	a	k8xC	a
Ferd	Ferd	k1gMnSc1	Ferd
<g/>
.	.	kIx.	.
</s>
<s>
Dattel	Dattel	k1gMnSc1	Dattel
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Povídky	povídka	k1gFnSc2	povídka
malostranské	malostranský	k2eAgFnSc2d1	Malostranská
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
malostranské	malostranský	k2eAgFnPc1d1	Malostranská
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
Malostranské	malostranský	k2eAgFnPc1d1	Malostranská
v	v	k7c6	v
rozhlasové	rozhlasový	k2eAgFnSc6d1	rozhlasová
četbě	četba	k1gFnSc6	četba
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Hausenblas	Hausenblas	k1gMnSc1	Hausenblas
<g/>
:	:	kIx,	:
Řeč	řeč	k1gFnSc1	řeč
o	o	k7c6	o
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
Nerudově	Nerudův	k2eAgFnSc6d1	Nerudova
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
U	u	k7c2	u
tří	tři	k4xCgNnPc2	tři
lilií	lilium	k1gNnPc2	lilium
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
:	:	kIx,	:
Poznání	poznání	k1gNnSc1	poznání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
v	v	k7c6	v
Povídkách	povídka	k1gFnPc6	povídka
malostranských	malostranský	k2eAgMnPc2d1	malostranský
(	(	kIx(	(
<g/>
K	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
Nerudova	Nerudův	k2eAgInSc2d1	Nerudův
realismu	realismus	k1gInSc2	realismus
<g/>
)	)	kIx)	)
</s>
</p>
