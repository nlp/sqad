<s>
Soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
Povídky	povídka	k1gFnSc2	povídka
malostranské	malostranský	k2eAgNnSc1d1	Malostranské
je	být	k5eAaImIp3nS	být
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
prozaické	prozaický	k2eAgNnSc1d1	prozaické
dílo	dílo	k1gNnSc1	dílo
Jana	Jan	k1gMnSc2	Jan
Nerudy	Neruda	k1gMnSc2	Neruda
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
