<s>
Jussieu	Jussieu	k5eAaPmIp1nS
(	(	kIx(
<g/>
stanice	stanice	k1gFnSc1
metra	metro	k1gNnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Jussieu	Jussieu	k6eAd1
</s>
<s>
StátFrancie	StátFrancie	k1gFnSc1
</s>
<s>
MěstoPaříž	MěstoPaříž	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
Courneuve	Courneuev	k1gFnSc2
<g/>
,	,	kIx,
Pantin	Pantina	k1gFnPc2
<g/>
,	,	kIx,
Aubervilliers	Aubervilliersa	k1gFnPc2
<g/>
,	,	kIx,
Ivry-sur-Seine	Ivry-sur-Sein	k1gMnSc5
<g/>
,	,	kIx,
Le	Le	k1gMnSc5
Kremlin-Bicê	Kremlin-Bicê	k1gMnSc5
a	a	k8xC
Villejuif	Villejuif	k1gMnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
<g/>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1931	#num#	k4
</s>
<s>
Linky	linka	k1gFnPc1
<g/>
710	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
45,79	45,79	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
20,66	20,66	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1
linky	linka	k1gFnSc2
10	#num#	k4
</s>
<s>
Jussieu	Jussieu	k6eAd1
je	být	k5eAaImIp3nS
přestupní	přestupní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
pařížského	pařížský	k2eAgNnSc2d1
metra	metro	k1gNnSc2
mezi	mezi	k7c7
linkami	linka	k1gFnPc7
7	#num#	k4
a	a	k8xC
10	#num#	k4
v	v	k7c4
5	#num#	k4
<g/>
.	.	kIx.
obvodu	obvod	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
pod	pod	k7c7
náměstím	náměstí	k1gNnSc7
Place	plac	k1gInSc6
Jussieu	Jussieus	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
kříží	křížit	k5eAaImIp3nS
Rue	Rue	k1gFnSc1
Jussieu	Jussieus	k1gInSc2
a	a	k8xC
Rue	Rue	k1gFnSc2
Linné	Linná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1931	#num#	k4
pro	pro	k7c4
obě	dva	k4xCgFnPc4
linky	linka	k1gFnPc4
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
dostavěn	dostavěn	k2eAgInSc1d1
tunel	tunel	k1gInSc1
pod	pod	k7c7
Seinou	Seina	k1gFnSc7
a	a	k8xC
zprovozněn	zprovozněn	k2eAgInSc1d1
úsek	úsek	k1gInSc1
mezi	mezi	k7c7
Sully	Sullo	k1gNnPc7
–	–	k?
Morland	Morlanda	k1gFnPc2
a	a	k8xC
Place	plac	k1gInSc6
Monge	Monge	k1gFnPc2
na	na	k7c6
lince	linka	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
nástupiště	nástupiště	k1gNnSc1
linky	linka	k1gFnSc2
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
byla	být	k5eAaImAgFnS
nástupiště	nástupiště	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
renovována	renovován	k2eAgFnSc1d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zde	zde	k6eAd1
poprvé	poprvé	k6eAd1
k	k	k7c3
obkladu	obklad	k1gInSc3
stěn	stěna	k1gFnPc2
použity	použit	k2eAgFnPc1d1
obdélníkové	obdélníkový	k2eAgFnPc4d1
dlaždice	dlaždice	k1gFnPc4
namísto	namísto	k7c2
tradičních	tradiční	k2eAgMnPc2d1
zkosených	zkosený	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
se	se	k3xPyFc4
dříve	dříve	k6eAd2
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
Jussieu	Jussiea	k1gFnSc4
–	–	k?
Halle-aux-vins	Halle-aux-vinsa	k1gFnPc2
(	(	kIx(
<g/>
neboli	neboli	k8xC
tržnice	tržnice	k1gFnSc1
s	s	k7c7
vínem	víno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
pouze	pouze	k6eAd1
název	název	k1gInSc1
Jussieu	Jussieus	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
malý	malý	k2eAgInSc1d1
trh	trh	k1gInSc1
s	s	k7c7
vínem	víno	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
založil	založit	k5eAaPmAgMnS
Napoleon	napoleon	k1gInSc4
Bonaparte	bonapart	k1gInSc5
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zbořen	zbořit	k5eAaPmNgInS
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
kampus	kampus	k1gInSc4
Jussieu	Jussieus	k1gInSc2
pro	pro	k7c4
Univerzitu	univerzita	k1gFnSc4
Paříž	Paříž	k1gFnSc4
VI	VI	kA
a	a	k8xC
Institut	institut	k1gInSc1
de	de	k?
physique	physique	k1gInSc1
du	du	k?
globe	globus	k1gInSc5
de	de	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
Fyzikální	fyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
zeměkoule	zeměkoule	k1gFnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rue	Rue	k1gFnSc1
Jussieu	Jussieus	k1gInSc2
nese	nést	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
francouzské	francouzský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
Jussieu	Jussieus	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vzešlo	vzejít	k5eAaPmAgNnS
několik	několik	k4yIc1
botaniků	botanik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vstupy	vstup	k1gInPc1
</s>
<s>
Východy	východ	k1gInPc1
ze	z	k7c2
stanice	stanice	k1gFnSc2
vedou	vést	k5eAaImIp3nP
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Place	plac	k1gInSc6
Jussieu	Jussieus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
</s>
<s>
Arè	Arè	k1gInSc1
de	de	k?
Lutè	Lutè	k1gInSc2
–	–	k?
antická	antický	k2eAgFnSc1d1
aréna	aréna	k1gFnSc1
</s>
<s>
Jardin	Jardin	k2eAgInSc1d1
des	des	k1gNnSc7
plantes	plantes	k1gInSc1
–	–	k?
veřejný	veřejný	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Muséum	Muséum	k1gInSc1
national	nationat	k5eAaPmAgInS,k5eAaImAgInS
d	d	k?
<g/>
'	'	kIx"
<g/>
histoire	histoir	k1gMnSc5
naturelle	naturell	k1gMnSc5
–	–	k?
Národní	národní	k2eAgNnSc1d1
přírodovědecké	přírodovědecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Institut	institut	k1gInSc1
du	du	k?
monde	mond	k1gInSc5
arabe	arab	k1gMnSc5
–	–	k?
Institut	institut	k1gInSc4
arabského	arabský	k2eAgInSc2d1
světa	svět	k1gInSc2
</s>
<s>
Kampus	kampus	k1gInSc1
Jussieu	Jussieus	k1gInSc2
–	–	k?
univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Jussieu	Jussieus	k1gInSc2
(	(	kIx(
<g/>
métro	métro	k6eAd1
de	de	k?
Paris	Paris	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jussieu	Jussieus	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
←	←	k?
směr	směr	k1gInSc1
La	la	k1gNnSc1
Courneuve	Courneuev	k1gFnSc2
–	–	k?
8	#num#	k4
Mai	Mai	k1gFnSc1
1945	#num#	k4
</s>
<s>
Metro	metro	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
–	–	k?
Linka	linka	k1gFnSc1
7	#num#	k4
</s>
<s>
směr	směr	k1gInSc1
Villejuif	Villejuif	k1gMnSc1
–	–	k?
Louis	Louis	k1gMnSc1
Aragon	Aragon	k1gMnSc1
→	→	k?
směr	směr	k1gInSc1
Mairie	Mairie	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ivry	Ivra	k1gMnSc2
→	→	k?
</s>
<s>
Sully	Sull	k1gInPc1
–	–	k?
Morland	Morland	k1gInSc1
</s>
<s>
Jussieu	Jussieu	k6eAd1
</s>
<s>
Place	plac	k1gInSc5
Monge	Monge	k1gFnPc2
</s>
<s>
La	la	k1gNnSc1
Courneuve	Courneuev	k1gFnSc2
–	–	k?
8	#num#	k4
Mai	Mai	k1gFnSc1
1945	#num#	k4
–	–	k?
</s>
<s>
Fort	Fort	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Aubervilliers	Aubervilliers	k1gInSc1
–	–	k?
</s>
<s>
Aubervilliers	Aubervilliers	k1gInSc1
–	–	k?
Pantin	Pantin	k1gInSc1
–	–	k?
Quatre	Quatr	k1gInSc5
Chemins	Cheminsa	k1gFnPc2
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
de	de	k?
la	la	k0
Villette	Villett	k1gInSc5
–	–	k?
</s>
<s>
Corentin	Corentin	k2eAgMnSc1d1
Cariou	Caria	k1gFnSc7
–	–	k?
</s>
<s>
Crimée	Crimée	k1gFnSc1
–	–	k?
</s>
<s>
Riquet	Riquet	k1gInSc1
–	–	k?
</s>
<s>
Stalingrad	Stalingrad	k1gInSc1
–	–	k?
</s>
<s>
Louis	Louis	k1gMnSc1
Blanc	Blanc	k1gMnSc1
–	–	k?
</s>
<s>
Château-Landon	Château-Landon	k1gInSc1
–	–	k?
</s>
<s>
Gare	Gare	k1gFnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Est	Est	k1gMnSc1
–	–	k?
</s>
<s>
Poissonniè	Poissonniè	k1gMnSc5
–	–	k?
</s>
<s>
Cadet	Cadet	k1gInSc1
–	–	k?
</s>
<s>
Le	Le	k?
Peletier	Peletier	k1gInSc1
–	–	k?
</s>
<s>
Chaussée	chaussée	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Antin	Antin	k1gMnSc1
–	–	k?
La	la	k1gNnPc2
Fayette	Fayett	k1gInSc5
–	–	k?
</s>
<s>
Opéra	Opéra	k1gFnSc1
–	–	k?
</s>
<s>
Pyramides	Pyramides	k1gInSc1
–	–	k?
</s>
<s>
Palais	Palais	k1gInSc1
Royal	Royal	k1gInSc1
–	–	k?
Musée	Musée	k1gInSc1
du	du	k?
Louvre	Louvre	k1gInSc1
–	–	k?
</s>
<s>
Pont	Pont	k1gInSc1
Neuf	Neuf	k1gInSc1
–	–	k?
</s>
<s>
Châtelet	Châtelet	k1gInSc1
–	–	k?
</s>
<s>
Pont	Pont	k1gInSc1
Marie	Maria	k1gFnSc2
–	–	k?
</s>
<s>
Sully	Sull	k1gInPc1
–	–	k?
Morland	Morland	k1gInSc1
–	–	k?
</s>
<s>
Jussieu	Jussieu	k6eAd1
–	–	k?
</s>
<s>
Place	plac	k1gInSc6
Monge	Monge	k1gInSc4
–	–	k?
</s>
<s>
Censier	Censier	k1gInSc1
–	–	k?
Daubenton	Daubenton	k1gInSc1
–	–	k?
</s>
<s>
Les	les	k1gInSc1
Gobelins	Gobelins	k1gInSc1
–	–	k?
</s>
<s>
Place	plac	k1gInSc6
d	d	k?
<g/>
'	'	kIx"
<g/>
Italie	Italie	k1gFnSc1
–	–	k?
</s>
<s>
Tolbiac	Tolbiac	k1gFnSc1
–	–	k?
</s>
<s>
Maison	Maison	k1gMnSc1
Blanche	Blanch	k1gFnSc2
–	–	k?
</s>
<s>
Le	Le	k?
Kremlin-Bicê	Kremlin-Bicê	k1gMnSc5
–	–	k?
</s>
<s>
Villejuif	Villejuif	k1gMnSc1
–	–	k?
Léo	Léo	k1gMnSc1
Lagrange	Lagrang	k1gFnSc2
–	–	k?
</s>
<s>
Villejuif	Villejuif	k1gMnSc1
–	–	k?
Paul	Paul	k1gMnSc1
Vaillant-Couturier	Vaillant-Couturier	k1gMnSc1
–	–	k?
</s>
<s>
Villejuif	Villejuif	k1gMnSc1
–	–	k?
Louis	Louis	k1gMnSc1
Aragon	Aragon	k1gMnSc1
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italie	Italie	k1gFnSc1
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
de	de	k?
Choisy	Chois	k1gInPc1
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Ivry	Ivra	k1gMnSc2
–	–	k?
</s>
<s>
Pierre	Pierr	k1gMnSc5
et	et	k?
Marie	Maria	k1gFnSc2
Curie	Curie	k1gMnSc1
–	–	k?
</s>
<s>
Mairie	Mairie	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ivry	Ivra	k1gMnSc2
</s>
<s>
←	←	k?
směr	směr	k1gInSc1
Boulogne	Boulogn	k1gInSc5
–	–	k?
Pont	Pont	k1gInSc1
de	de	k?
Saint-Cloud	Saint-Cloud	k1gInSc1
</s>
<s>
Metro	metro	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
–	–	k?
Linka	linka	k1gFnSc1
10	#num#	k4
</s>
<s>
směr	směr	k1gInSc1
Gare	Gar	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
→	→	k?
</s>
<s>
Cardinal	Cardinat	k5eAaPmAgMnS,k5eAaImAgMnS
Lemoine	Lemoin	k1gMnSc5
</s>
<s>
Jussieu	Jussieu	k6eAd1
</s>
<s>
Gare	Gare	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
</s>
<s>
Boulogne	Boulognout	k5eAaPmIp3nS,k5eAaImIp3nS
–	–	k?
Pont	Pont	k1gInSc1
de	de	k?
Saint-Cloud	Saint-Cloud	k1gInSc1
–	–	k?
</s>
<s>
Boulogne	Boulognout	k5eAaPmIp3nS,k5eAaImIp3nS
–	–	k?
Jean	Jean	k1gMnSc1
Jaurè	Jaurè	k1gFnPc2
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Auteuil	Auteuil	k1gMnSc1
–	–	k?
</s>
<s>
Michel-Ange	Michel-Ange	k1gFnSc1
–	–	k?
Molitor	Molitor	k1gInSc1
–	–	k?
</s>
<s>
Michel-Ange	Michel-Ange	k1gFnSc1
–	–	k?
Auteuil	Auteuil	k1gInSc1
–	–	k?
</s>
<s>
Chardon-Lagache	Chardon-Lagache	k1gFnSc1
–	–	k?
</s>
<s>
Église	Église	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Auteuil	Auteuil	k1gMnSc1
–	–	k?
</s>
<s>
Mirabeau	Mirabeau	k6eAd1
–	–	k?
</s>
<s>
Javel	Javel	k1gMnSc1
–	–	k?
André	André	k1gMnSc1
Citroën	Citroën	k1gMnSc1
–	–	k?
</s>
<s>
Charles	Charles	k1gMnSc1
Michels	Michelsa	k1gFnPc2
–	–	k?
</s>
<s>
Avenue	avenue	k1gFnSc1
Émile	Émile	k1gFnSc1
Zola	Zola	k1gFnSc1
–	–	k?
</s>
<s>
La	la	k1gNnSc1
Motte-Picquet	Motte-Picquet	k1gMnSc1
–	–	k?
Grenelle	Grenelle	k1gInSc1
–	–	k?
</s>
<s>
Ségur	Ségur	k1gMnSc1
–	–	k?
</s>
<s>
Duroc	Duroc	k1gFnSc1
–	–	k?
</s>
<s>
Vaneau	Vaneau	k6eAd1
–	–	k?
</s>
<s>
Sè	Sè	k1gInSc1
–	–	k?
Babylone	Babylon	k1gInSc5
–	–	k?
</s>
<s>
Mabillon	Mabillon	k1gInSc1
–	–	k?
</s>
<s>
Odéon	Odéon	k1gInSc1
–	–	k?
</s>
<s>
Cluny	Cluna	k1gFnPc1
–	–	k?
La	la	k1gNnSc1
Sorbonne	Sorbonn	k1gInSc5
–	–	k?
</s>
<s>
Maubert	Maubert	k1gMnSc1
–	–	k?
Mutualité	Mutualitý	k2eAgNnSc1d1
–	–	k?
</s>
<s>
Cardinal	Cardinat	k5eAaImAgMnS,k5eAaPmAgMnS
Lemoine	Lemoin	k1gInSc5
–	–	k?
</s>
<s>
Jussieu	Jussieu	k6eAd1
–	–	k?
</s>
<s>
Gare	Gare	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
Metro	metro	k1gNnSc1
</s>
