<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
Dopravním	dopravní	k2eAgInSc7d1	dopravní
podnikem	podnik	k1gInSc7	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
síť	síť	k1gFnSc4	síť
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc1	její
historie	historie	k1gFnSc1	historie
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
po	po	k7c4	po
pražské	pražský	k2eAgNnSc4d1	Pražské
také	také	k9	také
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
provozní	provozní	k2eAgFnSc1d1	provozní
délka	délka	k1gFnSc1	délka
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
města	město	k1gNnSc2	město
Modřice	Modřice	k1gFnSc2	Modřice
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
70,2	[number]	k4	70,2
km	km	kA	km
<g/>
,	,	kIx,	,
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
činí	činit	k5eAaImIp3nS	činit
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
napájecí	napájecí	k2eAgNnSc1d1	napájecí
napětí	napětí	k1gNnSc1	napětí
600	[number]	k4	600
V.	V.	kA	V.
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
třináct	třináct	k4xCc4	třináct
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
denních	denní	k2eAgFnPc2d1	denní
linek	linka	k1gFnPc2	linka
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
s	s	k7c7	s
provozní	provozní	k2eAgFnSc7d1	provozní
délkou	délka	k1gFnSc7	délka
137,7	[number]	k4	137,7
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgFnPc2	tento
linek	linka	k1gFnPc2	linka
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
jedenáct	jedenáct	k4xCc4	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
provozoval	provozovat	k5eAaImAgInS	provozovat
DPMB	DPMB	kA	DPMB
celkem	celkem	k6eAd1	celkem
305	[number]	k4	305
tramvají	tramvaj	k1gFnPc2	tramvaj
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vozy	vůz	k1gInPc1	vůz
najely	najet	k5eAaPmAgInP	najet
celkem	celkem	k6eAd1	celkem
14,350	[number]	k4	14,350
milionů	milion	k4xCgInPc2	milion
vozokilometrů	vozokilometr	k1gInPc2	vozokilometr
a	a	k8xC	a
přepravily	přepravit	k5eAaPmAgInP	přepravit
195,350	[number]	k4	195,350
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
hantecu	hantecus	k1gInSc6	hantecus
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tramvaj	tramvaj	k1gFnSc4	tramvaj
používá	používat	k5eAaImIp3nS	používat
známé	známý	k2eAgNnSc1d1	známé
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
šalina	šalina	k1gFnSc1	šalina
<g/>
"	"	kIx"	"
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
německého	německý	k2eAgNnSc2d1	německé
označení	označení	k1gNnSc2	označení
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
"	"	kIx"	"
<g/>
Elektrische	Elektrische	k1gFnSc1	Elektrische
Linie	linie	k1gFnSc1	linie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
"	"	kIx"	"
<g/>
elektriŠE	elektriŠE	k?	elektriŠE
LÍNYe	LÍNYe	k1gInSc1	LÍNYe
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jako	jako	k8xS	jako
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
městě	město	k1gNnSc6	město
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
linka	linka	k1gFnSc1	linka
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
trať	trať	k1gFnSc1	trať
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
k	k	k7c3	k
hostinci	hostinec	k1gInSc3	hostinec
Semilasso	Semilassa	k1gFnSc5	Semilassa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
obci	obec	k1gFnSc6	obec
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc4	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway	Tramwaa	k1gFnSc2	Tramwaa
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Personen-	Personen-	k1gMnSc1	Personen-
und	und	k?	und
Frachtenverkehr	Frachtenverkehr	k1gMnSc1	Frachtenverkehr
(	(	kIx(	(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
provozu	provoz	k1gInSc2	provoz
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
6	[number]	k4	6
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
společnost	společnost	k1gFnSc1	společnost
nakoupila	nakoupit	k5eAaPmAgFnS	nakoupit
celkem	celkem	k6eAd1	celkem
53	[number]	k4	53
osobních	osobní	k2eAgMnPc2d1	osobní
a	a	k8xC	a
10	[number]	k4	10
nákladních	nákladní	k2eAgInPc2d1	nákladní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Kolejová	kolejový	k2eAgFnSc1d1	kolejová
síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
rozšiřována	rozšiřován	k2eAgFnSc1d1	rozšiřována
(	(	kIx(	(
<g/>
tratě	trať	k1gFnPc1	trať
po	po	k7c6	po
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
na	na	k7c4	na
Šilingrovo	Šilingrův	k2eAgNnSc4d1	Šilingrovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc4	prodloužení
z	z	k7c2	z
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
přes	přes	k7c4	přes
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
Mendlovo	Mendlův	k2eAgNnSc4d1	Mendlovo
náměstí	náměstí	k1gNnSc4	náměstí
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
<g/>
,	,	kIx,	,
trať	trať	k1gFnSc1	trať
po	po	k7c6	po
ulici	ulice	k1gFnSc6	ulice
Cejl	Cejla	k1gFnPc2	Cejla
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc1d1	nákladní
odbočka	odbočka	k1gFnSc1	odbočka
na	na	k7c4	na
Rosické	rosický	k2eAgNnSc4d1	rosické
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
délky	délka	k1gFnPc4	délka
14,15	[number]	k4	14,15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
tři	tři	k4xCgFnPc1	tři
vozovny	vozovna	k1gFnPc1	vozovna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
konečných	konečná	k1gFnPc6	konečná
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
a	a	k8xC	a
Králově	Králův	k2eAgFnSc3d1	Králova
Poli	pole	k1gFnSc3	pole
a	a	k8xC	a
poblíž	poblíž	k7c2	poblíž
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Marešově	Marešův	k2eAgFnSc6d1	Marešova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
provozu	provoz	k1gInSc2	provoz
měli	mít	k5eAaImAgMnP	mít
lidé	člověk	k1gMnPc1	člověk
o	o	k7c4	o
přepravu	přeprava	k1gFnSc4	přeprava
zájem	zájem	k1gInSc1	zájem
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
opadl	opadnout	k5eAaPmAgMnS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdný	k2eAgNnSc1d1	jízdné
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nerentabilitu	nerentabilita	k1gFnSc4	nerentabilita
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1874	[number]	k4	1874
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
pražské	pražský	k2eAgFnSc2d1	Pražská
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
společnosti	společnost	k1gFnSc2	společnost
Bernhard	Bernhard	k1gMnSc1	Bernhard
Kollmann	Kollmann	k1gMnSc1	Kollmann
projevil	projevit	k5eAaPmAgMnS	projevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
podnikání	podnikání	k1gNnSc4	podnikání
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
radními	radní	k1gMnPc7	radní
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
převzít	převzít	k5eAaPmF	převzít
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
zchátralost	zchátralost	k1gFnSc4	zchátralost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
společnosti	společnost	k1gFnSc2	společnost
Brünner	Brünner	k1gMnSc1	Brünner
Tramway-Unternehmung	Tramway-Unternehmung	k1gMnSc1	Tramway-Unternehmung
(	(	kIx(	(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
správa	správa	k1gFnSc1	správa
tramwayská	tramwayská	k1gFnSc1	tramwayská
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1876	[number]	k4	1876
opětovně	opětovně	k6eAd1	opětovně
zahájil	zahájit	k5eAaPmAgInS	zahájit
s	s	k7c7	s
nově	nově	k6eAd1	nově
dodanými	dodaný	k2eAgInPc7d1	dodaný
vozy	vůz	k1gInPc7	vůz
provoz	provoz	k1gInSc1	provoz
koňky	koňka	k1gFnSc2	koňka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
k	k	k7c3	k
Semilassu	Semilass	k1gInSc3	Semilass
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
trase	trasa	k1gFnSc6	trasa
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedla	vést	k5eAaImAgFnS	vést
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
tratí	trať	k1gFnPc2	trať
I.	I.	kA	I.
období	období	k1gNnSc4	období
koňky	koňka	k1gFnSc2	koňka
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
13	[number]	k4	13
otevřených	otevřený	k2eAgMnPc2d1	otevřený
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
letních	letní	k2eAgNnPc2d1	letní
<g/>
)	)	kIx)	)
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
zkušební	zkušební	k2eAgFnSc1d1	zkušební
jízda	jízda	k1gFnSc1	jízda
parní	parní	k2eAgFnSc2d1	parní
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolejový	kolejový	k2eAgInSc1d1	kolejový
svršek	svršek	k1gInSc1	svršek
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
těžký	těžký	k2eAgInSc4d1	těžký
stroj	stroj	k1gInSc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
koňky	koňka	k1gFnSc2	koňka
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
nerentabilitu	nerentabilita	k1gFnSc4	nerentabilita
ukončen	ukončit	k5eAaPmNgInS	ukončit
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parní	parní	k2eAgFnSc1d1	parní
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
město	město	k1gNnSc1	město
hledalo	hledat	k5eAaImAgNnS	hledat
dalšího	další	k2eAgMnSc4d1	další
provozovatele	provozovatel	k1gMnSc4	provozovatel
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvážnějším	vážní	k2eAgMnPc3d3	nejvážnější
uchazečům	uchazeč	k1gMnPc3	uchazeč
patřil	patřit	k5eAaImAgInS	patřit
Wilhelm	Wilhelm	k1gInSc1	Wilhelm
von	von	k1gInSc1	von
Lindheim	Lindheim	k1gInSc4	Lindheim
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc4	zastánce
parní	parní	k2eAgFnSc2d1	parní
trakce	trakce	k1gFnSc2	trakce
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
městských	městský	k2eAgFnPc2d1	městská
drah	draha	k1gFnPc2	draha
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
rakouské	rakouský	k2eAgFnSc6d1	rakouská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnPc1	jednání
pokračovala	pokračovat	k5eAaImAgNnP	pokračovat
ale	ale	k9	ale
zvolna	zvolna	k6eAd1	zvolna
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
města	město	k1gNnSc2	město
s	s	k7c7	s
Lindheimem	Lindheim	k1gInSc7	Lindheim
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
podnikatel	podnikatel	k1gMnSc1	podnikatel
začal	začít	k5eAaPmAgMnS	začít
ihned	ihned	k6eAd1	ihned
budovat	budovat	k5eAaImF	budovat
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
-	-	kIx~	-
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
-	-	kIx~	-
Pisárky	Pisárka	k1gFnPc1	Pisárka
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
depo	depo	k1gNnSc1	depo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
odbočce	odbočka	k1gFnSc6	odbočka
z	z	k7c2	z
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Václavské	václavský	k2eAgFnSc2d1	Václavská
ulice	ulice	k1gFnSc2	ulice
k	k	k7c3	k
Ústřednímu	ústřední	k2eAgInSc3d1	ústřední
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
,	,	kIx,	,
nacházejícímu	nacházející	k2eAgMnSc3d1	nacházející
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nesla	nést	k5eAaImAgFnS	nést
společnost	společnost	k1gFnSc1	společnost
provozující	provozující	k2eAgFnSc2d1	provozující
tramvaje	tramvaj	k1gFnSc2	tramvaj
název	název	k1gInSc1	název
Brünner	Brünner	k1gInSc1	Brünner
Dampf-Tramway	Dampf-Tramwaa	k1gFnSc2	Dampf-Tramwaa
(	(	kIx(	(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
parní	parní	k2eAgFnSc1d1	parní
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Brünner	Brünner	k1gInSc4	Brünner
Local	Local	k1gMnSc1	Local
Eisenbahn	Eisenbahn	k1gMnSc1	Eisenbahn
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
(	(	kIx(	(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
společnost	společnost	k1gFnSc1	společnost
místních	místní	k2eAgFnPc2d1	místní
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
BLEG	BLEG	kA	BLEG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parní	parní	k2eAgFnSc4d1	parní
tramvaj	tramvaj	k1gFnSc4	tramvaj
již	již	k6eAd1	již
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
rentabilitou	rentabilita	k1gFnSc7	rentabilita
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
množství	množství	k1gNnSc1	množství
nákladních	nákladní	k2eAgFnPc2d1	nákladní
vleček	vlečka	k1gFnPc2	vlečka
vedoucích	vedoucí	k1gMnPc2	vedoucí
z	z	k7c2	z
tratí	trať	k1gFnPc2	trať
do	do	k7c2	do
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sídlily	sídlit	k5eAaImAgFnP	sídlit
poblíž	poblíž	k6eAd1	poblíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevilo	objevit	k5eAaPmAgNnS	objevit
15	[number]	k4	15
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
a	a	k8xC	a
31	[number]	k4	31
vlečných	vlečný	k2eAgInPc2d1	vlečný
vozů	vůz	k1gInPc2	vůz
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osm	osm	k4xCc1	osm
převzatých	převzatý	k2eAgMnPc2d1	převzatý
z	z	k7c2	z
II	II	kA	II
<g/>
.	.	kIx.	.
období	období	k1gNnSc4	období
koňky	koňka	k1gFnSc2	koňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
prodala	prodat	k5eAaPmAgFnS	prodat
BLEG	BLEG	kA	BLEG
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
provoz	provoz	k1gInSc1	provoz
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
elektrárenské	elektrárenský	k2eAgFnSc2d1	elektrárenská
společnosti	společnost	k1gFnSc2	společnost
Union	union	k1gInSc1	union
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
Oesterreichische	Oesterreichische	k1gInSc1	Oesterreichische
Union	union	k1gInSc1	union
Elektrizitäts	Elektrizitäts	k1gInSc1	Elektrizitäts
-	-	kIx~	-
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
in	in	k?	in
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
OeUEG	OeUEG	k1gMnSc1	OeUEG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
městem	město	k1gNnSc7	město
začala	začít	k5eAaPmAgFnS	začít
elektrifikovat	elektrifikovat	k5eAaBmF	elektrifikovat
celou	celý	k2eAgFnSc4d1	celá
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
síť	síť	k1gFnSc4	síť
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
tratě	trať	k1gFnSc2	trať
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
-	-	kIx~	-
Pisárky	Pisárek	k1gInPc1	Pisárek
a	a	k8xC	a
Václavská	václavský	k2eAgFnSc1d1	Václavská
-	-	kIx~	-
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1	osobní
provoz	provoz	k1gInSc1	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
však	však	k9	však
parní	parní	k2eAgFnPc1d1	parní
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
nezmizely	zmizet	k5eNaPmAgFnP	zmizet
z	z	k7c2	z
brněnských	brněnský	k2eAgFnPc2d1	brněnská
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
nadále	nadále	k6eAd1	nadále
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
nákladní	nákladní	k2eAgFnSc6d1	nákladní
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vlečky	vlečka	k1gFnPc1	vlečka
zpočátku	zpočátku	k6eAd1	zpočátku
nebyly	být	k5eNaImAgFnP	být
elektrifikované	elektrifikovaný	k2eAgFnPc1d1	elektrifikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Caroline	Carolin	k1gInSc5	Carolin
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
už	už	k6eAd1	už
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Vojenského	vojenský	k2eAgInSc2d1	vojenský
stavebního	stavební	k2eAgInSc2d1	stavební
dozoru	dozor	k1gInSc2	dozor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
přešla	přejít	k5eAaPmAgNnP	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
převedla	převést	k5eAaPmAgFnS	převést
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
železniční	železniční	k2eAgFnSc4d1	železniční
vlečku	vlečka	k1gFnSc4	vlečka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
fungujícímu	fungující	k2eAgInSc3d1	fungující
systému	systém	k1gInSc3	systém
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
byla	být	k5eAaImAgFnS	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
trakce	trakce	k1gFnSc1	trakce
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
pokusná	pokusný	k2eAgFnSc1d1	pokusná
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
vyjela	vyjet	k5eAaPmAgFnS	vyjet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
klasické	klasický	k2eAgFnSc2d1	klasická
městské	městský	k2eAgFnSc2d1	městská
tramvaje	tramvaj	k1gFnSc2	tramvaj
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
dopravě	doprava	k1gFnSc6	doprava
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
elektrifikovaných	elektrifikovaný	k2eAgFnPc6d1	elektrifikovaná
tratích	trať	k1gFnPc6	trať
původní	původní	k2eAgFnSc2d1	původní
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Gesellschaft	Gesellschafta	k1gFnPc2	Gesellschafta
der	drát	k5eAaImRp2nS	drát
Brünner	Brünner	k1gInSc4	Brünner
Elektrischen	Elektrischen	k2eAgInSc1d1	Elektrischen
Strassenbahnen	Strassenbahnen	k1gInSc1	Strassenbahnen
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
brněnských	brněnský	k2eAgFnPc2d1	brněnská
elektrických	elektrický	k2eAgFnPc2d1	elektrická
pouličních	pouliční	k2eAgFnPc2d1	pouliční
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
GBES	GBES	kA	GBES
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
majiteli	majitel	k1gMnPc7	majitel
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
OeUEG	OeUEG	k1gFnSc1	OeUEG
a	a	k8xC	a
AEG	AEG	kA	AEG
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
provozovatelem	provozovatel	k1gMnSc7	provozovatel
brněnských	brněnský	k2eAgFnPc2d1	brněnská
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
tramvaj	tramvaj	k1gFnSc4	tramvaj
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
depo	depo	k1gNnSc1	depo
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
šesti	šest	k4xCc6	šest
letech	let	k1gInPc6	let
provozu	provoz	k1gInSc2	provoz
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
základní	základní	k2eAgFnSc2d1	základní
kostry	kostra	k1gFnSc2	kostra
dnešní	dnešní	k2eAgFnSc2d1	dnešní
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
ulicemi	ulice	k1gFnPc7	ulice
Kobližnou	kobližný	k2eAgFnSc4d1	Kobližná
a	a	k8xC	a
Cejl	Cejl	k1gInSc4	Cejl
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
k	k	k7c3	k
mostu	most	k1gInSc3	most
přes	přes	k7c4	přes
Svitavu	Svitava	k1gFnSc4	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
a	a	k8xC	a
1902	[number]	k4	1902
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
po	po	k7c6	po
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
do	do	k7c2	do
Komárova	komárův	k2eAgNnSc2d1	komárův
<g/>
,	,	kIx,	,
po	po	k7c6	po
ulici	ulice	k1gFnSc6	ulice
Veveří	veveří	k2eAgFnSc6d1	veveří
a	a	k8xC	a
po	po	k7c6	po
Údolní	údolní	k2eAgFnSc6d1	údolní
ulici	ulice	k1gFnSc6	ulice
do	do	k7c2	do
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
následujícím	následující	k2eAgInSc6d1	následující
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
trať	trať	k1gFnSc4	trať
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
Drobného	drobný	k2eAgNnSc2d1	drobné
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
konečná	konečný	k2eAgFnSc1d1	konečná
zastávka	zastávka	k1gFnSc1	zastávka
Sadová	sadový	k2eAgFnSc1d1	Sadová
<g/>
)	)	kIx)	)
a	a	k8xC	a
úsek	úsek	k1gInSc4	úsek
Mendlovo	Mendlův	k2eAgNnSc4d1	Mendlovo
náměstí	náměstí	k1gNnSc4	náměstí
-	-	kIx~	-
Šilingrovo	Šilingrův	k2eAgNnSc4d1	Šilingrovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
prodloužen	prodloužen	k2eAgMnSc1d1	prodloužen
až	až	k8xS	až
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jezdilo	jezdit	k5eAaImAgNnS	jezdit
pět	pět	k4xCc4	pět
linek	linka	k1gFnPc2	linka
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
barevnými	barevný	k2eAgFnPc7d1	barevná
svítilnami	svítilna	k1gFnPc7	svítilna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
označení	označení	k1gNnSc3	označení
čísly	číslo	k1gNnPc7	číslo
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
následujících	následující	k2eAgNnPc6d1	následující
došlo	dojít	k5eAaPmAgNnS	dojít
především	především	k9	především
k	k	k7c3	k
rekonstrukcím	rekonstrukce	k1gFnPc3	rekonstrukce
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zdvojkolejňování	zdvojkolejňování	k1gNnSc3	zdvojkolejňování
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
smyčka	smyčka	k1gFnSc1	smyčka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
konečné	konečná	k1gFnSc6	konečná
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
tramvaje	tramvaj	k1gFnPc1	tramvaj
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c4	na
transportu	transporta	k1gFnSc4	transporta
raněných	raněný	k1gMnPc2	raněný
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
do	do	k7c2	do
lazaretů	lazaret	k1gInPc2	lazaret
umístěných	umístěný	k2eAgInPc2d1	umístěný
různě	různě	k6eAd1	různě
po	po	k7c6	po
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
i	i	k8xC	i
k	k	k7c3	k
dílčímu	dílčí	k2eAgNnSc3d1	dílčí
prodloužení	prodloužení	k1gNnSc3	prodloužení
trati	trať	k1gFnSc2	trať
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Veveří	veveří	k2eAgFnSc4d1	veveří
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
ale	ale	k8xC	ale
během	během	k7c2	během
války	válka	k1gFnSc2	válka
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
,	,	kIx,	,
pracovníci	pracovník	k1gMnPc1	pracovník
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
odvedeni	odvést	k5eAaPmNgMnP	odvést
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
museli	muset	k5eAaImAgMnP	muset
zaujmout	zaujmout	k5eAaPmF	zaujmout
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
mladiství	mladistvý	k2eAgMnPc1d1	mladistvý
a	a	k8xC	a
vysloužilí	vysloužilý	k2eAgMnPc1d1	vysloužilý
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Panoval	panovat	k5eAaImAgInS	panovat
nedostatek	nedostatek	k1gInSc4	nedostatek
materiálu	materiál	k1gInSc2	materiál
i	i	k8xC	i
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
nárůst	nárůst	k1gInSc4	nárůst
počtu	počet	k1gInSc2	počet
cestujících	cestující	k1gMnPc2	cestující
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tramvají	tramvaj	k1gFnPc2	tramvaj
odstavena	odstaven	k2eAgFnSc1d1	odstavena
jako	jako	k8xS	jako
neprovozuschopná	provozuschopný	k2eNgFnSc1d1	neprovozuschopná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
zcela	zcela	k6eAd1	zcela
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
provozu	provoz	k1gInSc2	provoz
elektrických	elektrický	k2eAgFnPc2d1	elektrická
tramvají	tramvaj	k1gFnPc2	tramvaj
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
dodáno	dodat	k5eAaPmNgNnS	dodat
41	[number]	k4	41
motorových	motorový	k2eAgInPc2d1	motorový
a	a	k8xC	a
12	[number]	k4	12
vlečných	vlečný	k2eAgInPc2d1	vlečný
vozů	vůz	k1gInPc2	vůz
z	z	k7c2	z
vagonky	vagonek	k1gInPc1	vagonek
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
29	[number]	k4	29
vleků	vlek	k1gInPc2	vlek
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
GBES	GBES	kA	GBES
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
další	další	k2eAgInPc4d1	další
motorové	motorový	k2eAgInPc4d1	motorový
vozy	vůz	k1gInPc4	vůz
<g/>
:	:	kIx,	:
sedm	sedm	k4xCc1	sedm
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
a	a	k8xC	a
1904	[number]	k4	1904
od	od	k7c2	od
strojírny	strojírna	k1gFnSc2	strojírna
Lederer	Lederra	k1gFnPc2	Lederra
a	a	k8xC	a
Porges	Porgesa	k1gFnPc2	Porgesa
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
vyrobeny	vyroben	k2eAgInPc4d1	vyroben
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
výrobek	výrobek	k1gInSc1	výrobek
vagónky	vagónek	k1gInPc4	vagónek
ve	v	k7c6	v
Studénce	studénka	k1gFnSc6	studénka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
vlečných	vlečný	k2eAgInPc2d1	vlečný
vozů	vůz	k1gInPc2	vůz
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jedinému	jediný	k2eAgNnSc3d1	jediné
rozšíření	rozšíření	k1gNnSc3	rozšíření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
12	[number]	k4	12
ojetých	ojetý	k2eAgInPc2d1	ojetý
vleků	vlek	k1gInPc2	vlek
původně	původně	k6eAd1	původně
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
pro	pro	k7c4	pro
koněspřežnou	koněspřežný	k2eAgFnSc4d1	koněspřežná
tramvaj	tramvaj	k1gFnSc4	tramvaj
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nákladním	nákladní	k2eAgInSc6d1	nákladní
provozu	provoz	k1gInSc6	provoz
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
využívány	využíván	k2eAgFnPc1d1	využívána
čtyři	čtyři	k4xCgFnPc1	čtyři
parní	parní	k2eAgFnPc1d1	parní
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
elektrifikaci	elektrifikace	k1gFnSc3	elektrifikace
většiny	většina	k1gFnSc2	většina
vleček	vlečka	k1gFnPc2	vlečka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupit	k5eAaPmNgFnP	zakoupit
dvě	dva	k4xCgFnPc1	dva
elektrické	elektrický	k2eAgFnPc1d1	elektrická
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc3	třetí
byla	být	k5eAaImAgFnS	být
dodána	dodán	k2eAgFnSc1d1	dodána
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
a	a	k8xC	a
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
parní	parní	k2eAgFnSc1d1	parní
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
byla	být	k5eAaImAgFnS	být
odstavena	odstavit	k5eAaPmNgFnS	odstavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
po	po	k7c6	po
zatrolejování	zatrolejování	k1gNnSc6	zatrolejování
poslední	poslední	k2eAgFnSc2d1	poslední
vlečky	vlečka	k1gFnSc2	vlečka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
ČSR	ČSR	kA	ČSR
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
provozovatelem	provozovatel	k1gMnSc7	provozovatel
sítě	síť	k1gFnSc2	síť
Společnost	společnost	k1gFnSc1	společnost
brněnských	brněnský	k2eAgNnPc2d1	brněnské
pouličních	pouliční	k2eAgNnPc2d1	pouliční
drah	draha	k1gNnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zlepšit	zlepšit	k5eAaPmF	zlepšit
stav	stav	k1gInSc4	stav
sítě	síť	k1gFnSc2	síť
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
23	[number]	k4	23
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
chátrala	chátrat	k5eAaImAgFnS	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
poměrech	poměr	k1gInPc6	poměr
začal	začít	k5eAaPmAgInS	začít
její	její	k3xOp3gInSc4	její
opětovný	opětovný	k2eAgInSc4d1	opětovný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgFnS	stavět
jedna	jeden	k4xCgFnSc1	jeden
trať	trať	k1gFnSc1	trať
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
<g/>
;	;	kIx,	;
tramvaj	tramvaj	k1gFnSc4	tramvaj
začala	začít	k5eAaPmAgFnS	začít
jezdit	jezdit	k5eAaImF	jezdit
mj.	mj.	kA	mj.
do	do	k7c2	do
Žabovřesk	Žabovřesky	k1gFnPc2	Žabovřesky
<g/>
,	,	kIx,	,
Židenic	Židenice	k1gFnPc2	Židenice
<g/>
,	,	kIx,	,
Obřan	Obřana	k1gFnPc2	Obřana
<g/>
,	,	kIx,	,
Řečkovic	Řečkovice	k1gFnPc2	Řečkovice
a	a	k8xC	a
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
nově	nově	k6eAd1	nově
připojených	připojený	k2eAgFnPc2d1	připojená
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
nová	nový	k2eAgFnSc1d1	nová
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Husovicích	Husovice	k1gFnPc6	Husovice
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
tratě	trať	k1gFnPc1	trať
téměř	téměř	k6eAd1	téměř
nestavěly	stavět	k5eNaImAgFnP	stavět
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výjimky	výjimka	k1gFnPc1	výjimka
-	-	kIx~	-
např.	např.	kA	např.
prodloužení	prodloužení	k1gNnSc4	prodloužení
v	v	k7c6	v
Žabovřeskách	Žabovřesky	k1gFnPc6	Žabovřesky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdvojkolejňovaly	zdvojkolejňovat	k5eAaImAgFnP	zdvojkolejňovat
se	se	k3xPyFc4	se
ty	ten	k3xDgFnPc1	ten
stávající	stávající	k2eAgFnPc1d1	stávající
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
8	[number]	k4	8
linek	linka	k1gFnPc2	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
sítě	síť	k1gFnSc2	síť
nastal	nastat	k5eAaPmAgInS	nastat
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tratě	trať	k1gFnPc4	trať
jako	jako	k8xS	jako
např.	např.	kA	např.
pod	pod	k7c7	pod
Petrovem	Petrov	k1gInSc7	Petrov
po	po	k7c6	po
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Černých	Černých	k2eAgFnPc2d1	Černých
Polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
převzata	převzat	k2eAgFnSc1d1	převzata
místní	místní	k2eAgFnSc1d1	místní
dráha	dráha	k1gFnSc1	dráha
Černovice	Černovice	k1gFnSc1	Černovice
-	-	kIx~	-
Líšeň	Líšeň	k1gFnSc1	Líšeň
(	(	kIx(	(
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
železnice	železnice	k1gFnSc1	železnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
roku	rok	k1gInSc2	rok
a	a	k8xC	a
půl	půl	k1xP	půl
byla	být	k5eAaImAgFnS	být
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
a	a	k8xC	a
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
délky	délka	k1gFnSc2	délka
74,6	[number]	k4	74,6
km	km	kA	km
<g/>
)	)	kIx)	)
obnovoval	obnovovat	k5eAaImAgMnS	obnovovat
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
vyhořelá	vyhořelý	k2eAgFnSc1d1	vyhořelá
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
<g/>
,	,	kIx,	,
opraveny	opraven	k2eAgInPc1d1	opraven
vozy	vůz	k1gInPc1	vůz
i	i	k8xC	i
trolejové	trolejový	k2eAgNnSc1d1	trolejové
vedení	vedení	k1gNnSc1	vedení
(	(	kIx(	(
<g/>
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
%	%	kIx~	%
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
parní	parní	k2eAgFnSc1d1	parní
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Caroline	Carolin	k1gInSc5	Carolin
(	(	kIx(	(
<g/>
na	na	k7c4	na
odklízení	odklízení	k1gNnSc4	odklízení
trosek	troska	k1gFnPc2	troska
i	i	k9	i
na	na	k7c4	na
nouzový	nouzový	k2eAgInSc4d1	nouzový
osobní	osobní	k2eAgInSc4d1	osobní
provoz	provoz	k1gInSc4	provoz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
první	první	k4xOgFnSc1	první
poválečná	poválečný	k2eAgFnSc1d1	poválečná
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Komína	Komín	k1gInSc2	Komín
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
a	a	k8xC	a
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
vozovna	vozovna	k1gFnSc1	vozovna
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
byla	být	k5eAaImAgFnS	být
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
na	na	k7c4	na
měnírnu	měnírna	k1gFnSc4	měnírna
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
-	-	kIx~	-
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
stavět	stavět	k5eAaImF	stavět
nová	nový	k2eAgFnSc1d1	nová
moderní	moderní	k2eAgFnSc1d1	moderní
vozovna	vozovna	k1gFnSc1	vozovna
Medlánky	Medlánka	k1gFnSc2	Medlánka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
trať	trať	k1gFnSc1	trať
ze	z	k7c2	z
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
do	do	k7c2	do
Líšně	Líšeň	k1gFnSc2	Líšeň
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
místní	místní	k2eAgFnSc2d1	místní
železnice	železnice	k1gFnSc2	železnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
nový	nový	k2eAgInSc1d1	nový
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
byla	být	k5eAaImAgFnS	být
převedena	převeden	k2eAgFnSc1d1	převedena
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Mendlova	Mendlův	k2eAgNnSc2d1	Mendlovo
náměstí	náměstí	k1gNnSc2	náměstí
k	k	k7c3	k
výstavišti	výstaviště	k1gNnSc3	výstaviště
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
provoz	provoz	k1gInSc1	provoz
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
(	(	kIx(	(
<g/>
do	do	k7c2	do
1914	[number]	k4	1914
ještě	ještě	k9	ještě
střídavě	střídavě	k6eAd1	střídavě
s	s	k7c7	s
parními	parní	k2eAgMnPc7d1	parní
<g/>
)	)	kIx)	)
4	[number]	k4	4
postupně	postupně	k6eAd1	postupně
zakoupené	zakoupený	k2eAgFnSc2d1	zakoupená
elektrické	elektrický	k2eAgFnSc2d1	elektrická
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgInP	dodat
poslední	poslední	k2eAgInPc1d1	poslední
dvounápravové	dvounápravový	k2eAgInPc1d1	dvounápravový
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
elektrické	elektrický	k2eAgFnSc2d1	elektrická
výzbroje	výzbroj	k1gFnSc2	výzbroj
označovány	označován	k2eAgInPc1d1	označován
4	[number]	k4	4
<g/>
MT	MT	kA	MT
<g/>
)	)	kIx)	)
z	z	k7c2	z
Královopolské	královopolský	k2eAgFnSc2d1	Královopolská
strojírny	strojírna	k1gFnSc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
doplněn	doplnit	k5eAaPmNgInS	doplnit
prvními	první	k4xOgFnPc7	první
čtyřnápravovými	čtyřnápravový	k2eAgFnPc7d1	čtyřnápravová
tramvajemi	tramvaj	k1gFnPc7	tramvaj
řady	řada	k1gFnSc2	řada
T	T	kA	T
z	z	k7c2	z
ČKD	ČKD	kA	ČKD
(	(	kIx(	(
<g/>
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
tramvaje	tramvaj	k1gFnSc2	tramvaj
T1	T1	k1gFnSc2	T1
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nikdy	nikdy	k6eAd1	nikdy
nejezdily	jezdit	k5eNaImAgInP	jezdit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byly	být	k5eAaImAgInP	být
ještě	ještě	k6eAd1	ještě
doplňovány	doplňován	k2eAgInPc4d1	doplňován
článkovými	článkový	k2eAgInPc7d1	článkový
vozy	vůz	k1gInPc7	vůz
Tatra	Tatra	k1gFnSc1	Tatra
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
17	[number]	k4	17
stálých	stálý	k2eAgFnPc2d1	stálá
linek	linka	k1gFnPc2	linka
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
157,7	[number]	k4	157,7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
moderní	moderní	k2eAgFnPc1d1	moderní
tratě	trať	k1gFnPc1	trať
rychlodrážního	rychlodrážní	k2eAgInSc2d1	rychlodrážní
charakteru	charakter	k1gInSc2	charakter
na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
nová	nový	k2eAgNnPc4d1	nové
brněnská	brněnský	k2eAgNnPc4d1	brněnské
sídliště	sídliště	k1gNnPc4	sídliště
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
-	-	kIx~	-
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
úsek	úsek	k1gInSc1	úsek
Komín	Komín	k1gInSc1	Komín
-	-	kIx~	-
Bystrc	Bystrc	k1gFnSc1	Bystrc
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
ukončila	ukončit	k5eAaPmAgFnS	ukončit
tramvajový	tramvajový	k2eAgInSc4d1	tramvajový
provoz	provoz	k1gInSc4	provoz
vozovna	vozovna	k1gFnSc1	vozovna
Husovice	Husovice	k1gFnPc1	Husovice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
přenechána	přenechat	k5eAaPmNgFnS	přenechat
trolejbusům	trolejbus	k1gInPc3	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
tramvaje	tramvaj	k1gFnSc2	tramvaj
jezdí	jezdit	k5eAaImIp3nP	jezdit
i	i	k9	i
po	po	k7c6	po
meziměstské	meziměstský	k2eAgFnSc6d1	meziměstská
trati	trať	k1gFnSc6	trať
do	do	k7c2	do
Modřic	Modřice	k1gFnPc2	Modřice
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
trať	trať	k1gFnSc1	trať
skrz	skrz	k7c4	skrz
Židenice	Židenice	k1gFnPc4	Židenice
a	a	k8xC	a
zkrácena	zkrátit	k5eAaPmNgFnS	zkrátit
do	do	k7c2	do
smyčky	smyčka	k1gFnSc2	smyčka
Stará	starý	k2eAgFnSc1d1	stará
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
prodlužovala	prodlužovat	k5eAaImAgFnS	prodlužovat
nová	nový	k2eAgFnSc1d1	nová
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Líšně	Líšeň	k1gFnSc2	Líšeň
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
podzemní	podzemní	k2eAgFnSc7d1	podzemní
stanicí	stanice	k1gFnSc7	stanice
Jírova	Jírův	k2eAgMnSc2d1	Jírův
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1998	[number]	k4	1998
a	a	k8xC	a
2004	[number]	k4	2004
též	též	k9	též
úvraťová	úvraťový	k2eAgNnPc1d1	úvraťové
konečná	konečný	k2eAgNnPc1d1	konečné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
odlehčení	odlehčení	k1gNnSc6	odlehčení
přetížených	přetížený	k2eAgFnPc2d1	přetížená
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
(	(	kIx(	(
<g/>
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
a	a	k8xC	a
Husova	Husův	k2eAgFnSc1d1	Husova
-	-	kIx~	-
relace	relace	k1gFnSc1	relace
Mendlovo	Mendlův	k2eAgNnSc1d1	Mendlovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
-	-	kIx~	-
<g/>
Česká	český	k2eAgFnSc1d1	Česká
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zvažována	zvažován	k2eAgFnSc1d1	zvažována
možnost	možnost	k1gFnSc1	možnost
vybudovat	vybudovat	k5eAaPmF	vybudovat
pod	pod	k7c7	pod
vrchem	vrch	k1gInSc7	vrch
Špilberk	Špilberk	k1gInSc4	Špilberk
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
tunel	tunel	k1gInSc1	tunel
mezi	mezi	k7c7	mezi
dolní	dolní	k2eAgFnSc7d1	dolní
částí	část	k1gFnSc7	část
ulice	ulice	k1gFnSc1	ulice
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
a	a	k8xC	a
křižovatkou	křižovatka	k1gFnSc7	křižovatka
ulic	ulice	k1gFnPc2	ulice
Joštova	Joštův	k2eAgNnSc2d1	Joštův
a	a	k8xC	a
Marešova	Marešův	k2eAgNnSc2d1	Marešovo
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
650	[number]	k4	650
m	m	kA	m
a	a	k8xC	a
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
profilu	profil	k1gInSc6	profil
10	[number]	k4	10
×	×	k?	×
10	[number]	k4	10
m.	m.	k?	m.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
ražba	ražba	k1gFnSc1	ražba
průzkumné	průzkumný	k2eAgFnSc2d1	průzkumná
levé	levý	k2eAgFnSc2d1	levá
patní	patní	k2eAgFnSc2d1	patní
štoly	štola	k1gFnSc2	štola
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Pekařská	pekařský	k2eAgFnSc1d1	Pekařská
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyražení	vyražení	k1gNnSc6	vyražení
necelých	celý	k2eNgInPc2d1	necelý
510	[number]	k4	510
m	m	kA	m
však	však	k9	však
byly	být	k5eAaImAgInP	být
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
zastaveny	zastaven	k2eAgInPc1d1	zastaven
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
statických	statický	k2eAgFnPc2d1	statická
poruch	porucha	k1gFnPc2	porucha
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
Pellicově	Pellicův	k2eAgFnSc6d1	Pellicova
a	a	k8xC	a
Sladové	sladový	k2eAgFnSc6d1	sladová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Pekařské	pekařský	k2eAgFnSc6d1	Pekařská
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
90	[number]	k4	90
patrná	patrný	k2eAgFnSc1d1	patrná
proluka	proluka	k1gFnSc1	proluka
mezi	mezi	k7c7	mezi
domy	dům	k1gInPc7	dům
a	a	k8xC	a
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
štoly	štola	k1gFnSc2	štola
zarůstající	zarůstající	k2eAgFnSc7d1	zarůstající
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
provoz	provoz	k1gInSc1	provoz
starých	starý	k2eAgFnPc2d1	stará
dvounápravových	dvounápravový	k2eAgFnPc2d1	dvounápravová
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jezdí	jezdit	k5eAaImIp3nP	jezdit
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
vozy	vůz	k1gInPc1	vůz
koncepce	koncepce	k1gFnSc2	koncepce
PCC	PCC	kA	PCC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
spřahovat	spřahovat	k5eAaImF	spřahovat
do	do	k7c2	do
jízdních	jízdní	k2eAgFnPc2d1	jízdní
souprav	souprava	k1gFnPc2	souprava
tramvaje	tramvaj	k1gFnSc2	tramvaj
typu	typ	k1gInSc2	typ
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastal	nastat	k5eAaPmAgInS	nastat
s	s	k7c7	s
vozy	vůz	k1gInPc1	vůz
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
elektrická	elektrický	k2eAgFnSc1d1	elektrická
výzbroj	výzbroj	k1gFnSc1	výzbroj
nebyla	být	k5eNaImAgFnS	být
kompatibilní	kompatibilní	k2eAgFnSc4d1	kompatibilní
s	s	k7c7	s
vozy	vůz	k1gInPc7	vůz
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tramvaje	tramvaj	k1gFnPc1	tramvaj
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
do	do	k7c2	do
souprav	souprava	k1gFnPc2	souprava
nespojovaly	spojovat	k5eNaImAgInP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
však	však	k9	však
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
generálních	generální	k2eAgFnPc2d1	generální
oprav	oprava	k1gFnPc2	oprava
rekonstruovány	rekonstruován	k2eAgInPc1d1	rekonstruován
na	na	k7c4	na
typ	typ	k1gInSc4	typ
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dal	dát	k5eAaPmAgInS	dát
běžně	běžně	k6eAd1	běžně
spojovat	spojovat	k5eAaImF	spojovat
do	do	k7c2	do
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
dvou	dva	k4xCgInPc2	dva
vozů	vůz	k1gInPc2	vůz
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
T2R	T2R	k1gFnSc2	T2R
+	+	kIx~	+
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
specialitou	specialita	k1gFnSc7	specialita
byly	být	k5eAaImAgFnP	být
i	i	k8xC	i
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
havarovaná	havarovaný	k2eAgFnSc1d1	havarovaná
T2	T2	k1gFnSc1	T2
obdržela	obdržet	k5eAaPmAgFnS	obdržet
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
přední	přední	k2eAgMnSc1d1	přední
nebo	nebo	k8xC	nebo
zadní	zadní	k2eAgNnSc1d1	zadní
čelo	čelo	k1gNnSc1	čelo
používané	používaný	k2eAgNnSc1d1	používané
u	u	k7c2	u
typu	typ	k1gInSc2	typ
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vypadala	vypadat	k5eAaPmAgFnS	vypadat
jako	jako	k9	jako
novější	nový	k2eAgInSc4d2	novější
typ	typ	k1gInSc4	typ
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
takto	takto	k6eAd1	takto
opravovaná	opravovaný	k2eAgNnPc1d1	opravované
vozidla	vozidlo	k1gNnPc1	vozidlo
jezdila	jezdit	k5eAaImAgNnP	jezdit
v	v	k7c6	v
soupravách	souprava	k1gFnPc6	souprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
vozidla	vozidlo	k1gNnPc1	vozidlo
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
tříčlánkové	tříčlánkový	k2eAgFnSc2d1	tříčlánková
tramvaje	tramvaj	k1gFnSc2	tramvaj
Tatra	Tatra	k1gFnSc1	Tatra
KT	KT	kA	KT
<g/>
8	[number]	k4	8
<g/>
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dodána	dodat	k5eAaPmNgFnS	dodat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
tramvaje	tramvaj	k1gFnPc4	tramvaj
ověřovací	ověřovací	k2eAgFnSc2d1	ověřovací
série	série	k1gFnSc2	série
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
byly	být	k5eAaImAgFnP	být
zakoupeny	zakoupen	k2eAgFnPc1d1	zakoupena
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
trať	trať	k1gFnSc1	trať
po	po	k7c6	po
bývalé	bývalý	k2eAgFnSc6d1	bývalá
"	"	kIx"	"
<g/>
výpadovce	výpadovka	k1gFnSc6	výpadovka
<g/>
"	"	kIx"	"
-	-	kIx~	-
Renneské	Renneský	k2eAgFnSc3d1	Renneská
třídě	třída	k1gFnSc3	třída
-	-	kIx~	-
ke	k	k7c3	k
krematoriu	krematorium	k1gNnSc3	krematorium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
dopravu	doprava	k1gFnSc4	doprava
ze	z	k7c2	z
sídlišť	sídliště	k1gNnPc2	sídliště
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
a	a	k8xC	a
Bohunice	Bohunice	k1gFnPc1	Bohunice
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
líšeňská	líšeňský	k2eAgFnSc1d1	Líšeňská
trať	trať	k1gFnSc1	trať
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
zastávku	zastávka	k1gFnSc4	zastávka
do	do	k7c2	do
podzemní	podzemní	k2eAgFnSc2d1	podzemní
úvraťové	úvraťový	k2eAgFnSc2d1	úvraťová
konečné	konečná	k1gFnSc2	konečná
Jírova	Jírův	k2eAgFnSc1d1	Jírova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
přestaly	přestat	k5eAaPmAgInP	přestat
jezdit	jezdit	k5eAaImF	jezdit
tramvaje	tramvaj	k1gFnPc4	tramvaj
přes	přes	k7c4	přes
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
;	;	kIx,	;
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
historické	historický	k2eAgFnSc2d1	historická
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
však	však	k9	však
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
síti	síť	k1gFnSc6	síť
nastala	nastat	k5eAaPmAgFnS	nastat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tramvaje	tramvaj	k1gFnPc1	tramvaj
začaly	začít	k5eAaPmAgFnP	začít
jezdit	jezdit	k5eAaImF	jezdit
z	z	k7c2	z
líšeňské	líšeňský	k2eAgFnSc2d1	Líšeňská
konečné	konečný	k2eAgFnSc2d1	konečná
stanice	stanice	k1gFnSc2	stanice
Jírova	Jírův	k2eAgFnSc1d1	Jírova
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
zastávku	zastávka	k1gFnSc4	zastávka
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
taktéž	taktéž	k?	taktéž
úvraťovou	úvraťový	k2eAgFnSc4d1	úvraťová
konečnou	konečná	k1gFnSc4	konečná
Mifkova	Mifkov	k1gInSc2	Mifkov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
opětovně	opětovně	k6eAd1	opětovně
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgInSc7d1	poslední
novým	nový	k2eAgInSc7d1	nový
tramvajovým	tramvajový	k2eAgInSc7d1	tramvajový
úsekem	úsek	k1gInSc7	úsek
je	být	k5eAaImIp3nS	být
prodloužení	prodloužení	k1gNnSc1	prodloužení
tratě	trať	k1gFnSc2	trať
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
od	od	k7c2	od
Technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
smyčka	smyčka	k1gFnSc1	smyčka
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2007	[number]	k4	2007
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stavby	stavba	k1gFnSc2	stavba
mimoúrovňové	mimoúrovňový	k2eAgFnSc2d1	mimoúrovňová
křižovatky	křižovatka	k1gFnSc2	křižovatka
<g/>
)	)	kIx)	)
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
zastávku	zastávka	k1gFnSc4	zastávka
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
smyčky	smyčka	k1gFnSc2	smyčka
Technologický	technologický	k2eAgInSc1d1	technologický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
spojka	spojka	k1gFnSc1	spojka
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
smyčky	smyčka	k1gFnSc2	smyčka
do	do	k7c2	do
vozovny	vozovna	k1gFnSc2	vozovna
Medlánky	Medlánka	k1gFnSc2	Medlánka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
při	při	k7c6	při
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
odkázaná	odkázaný	k2eAgFnSc1d1	odkázaná
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
trať	trať	k1gFnSc4	trať
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
Lidické	lidický	k2eAgInPc4d1	lidický
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
přestupného	přestupný	k2eAgInSc2d1	přestupný
tarifu	tarif	k1gInSc2	tarif
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
celé	celý	k2eAgFnSc2d1	celá
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
z	z	k7c2	z
22	[number]	k4	22
na	na	k7c4	na
13	[number]	k4	13
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
byl	být	k5eAaImAgInS	být
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nově	nově	k6eAd1	nově
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
polookružní	polookružný	k2eAgMnPc1d1	polookružný
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
linky	linka	k1gFnPc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
brněnské	brněnský	k2eAgFnSc2d1	brněnská
tramvaje	tramvaj	k1gFnSc2	tramvaj
T3	T3	k1gFnSc2	T3
kvůli	kvůli	k7c3	kvůli
prodloužení	prodloužení	k1gNnSc3	prodloužení
životnosti	životnost	k1gFnSc2	životnost
postupně	postupně	k6eAd1	postupně
modernizují	modernizovat	k5eAaBmIp3nP	modernizovat
(	(	kIx(	(
<g/>
vozy	vůz	k1gInPc4	vůz
Tatra	Tatra	k1gFnSc1	Tatra
K2	K2	k1gFnSc1	K2
od	od	k7c2	od
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
nakupovány	nakupován	k2eAgInPc1d1	nakupován
nové	nový	k2eAgInPc1d1	nový
vozy	vůz	k1gInPc1	vůz
-	-	kIx~	-
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
6	[number]	k4	6
<g/>
A	a	k9	a
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
RT	RT	kA	RT
<g/>
6	[number]	k4	6
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
KT	KT	kA	KT
<g/>
8	[number]	k4	8
<g/>
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
RF	RF	kA	RF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
dojezdily	dojezdit	k5eAaPmAgFnP	dojezdit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
typu	typ	k1gInSc2	typ
T	T	kA	T
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
první	první	k4xOgInPc1	první
vůz	vůz	k1gInSc1	vůz
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R.	R.	kA	R.
<g/>
EV.	EV.	k1gFnSc2	EV.
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibyly	přibýt	k5eAaPmAgFnP	přibýt
tramvaje	tramvaj	k1gFnPc1	tramvaj
nejen	nejen	k6eAd1	nejen
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R.	R.	kA	R.
<g/>
PV	PV	kA	PV
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
první	první	k4xOgInPc4	první
vozy	vůz	k1gInPc4	vůz
Škoda	škoda	k6eAd1	škoda
03T	[number]	k4	03T
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Anitra	Anitrum	k1gNnSc2	Anitrum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
první	první	k4xOgFnSc1	první
tramvaj	tramvaj	k1gFnSc1	tramvaj
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
R-N	R-N	k1gFnSc2	R-N
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
přibyla	přibýt	k5eAaPmAgFnS	přibýt
další	další	k2eAgFnSc1d1	další
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vozy	vůz	k1gInPc1	vůz
vychází	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
typu	typ	k1gInSc2	typ
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
Pars	Parsa	k1gFnPc2	Parsa
nova	nova	k1gFnSc1	nova
například	například	k6eAd1	například
dosadila	dosadit	k5eAaPmAgFnS	dosadit
nový	nový	k2eAgInSc4d1	nový
střední	střední	k2eAgInSc4d1	střední
nízkopodlažní	nízkopodlažní	k2eAgInSc4d1	nízkopodlažní
článek	článek	k1gInSc4	článek
a	a	k8xC	a
čela	čelo	k1gNnSc2	čelo
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
designem	design	k1gInSc7	design
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
tramvaje	tramvaj	k1gFnPc1	tramvaj
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přímo	přímo	k6eAd1	přímo
modernizací	modernizace	k1gFnSc7	modernizace
starších	starý	k2eAgInPc2d2	starší
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
druhé	druhý	k4xOgInPc1	druhý
dva	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
novostavby	novostavba	k1gFnPc4	novostavba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlečný	vlečný	k2eAgInSc1d1	vlečný
vůz	vůz	k1gInSc1	vůz
VV60LF	VV60LF	k1gFnSc2	VV60LF
se	se	k3xPyFc4	se
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
do	do	k7c2	do
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
vlečný	vlečný	k2eAgInSc1d1	vlečný
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
spřahován	spřahovat	k5eAaImNgInS	spřahovat
do	do	k7c2	do
unikátní	unikátní	k2eAgFnSc2d1	unikátní
soupravy	souprava	k1gFnSc2	souprava
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R.	R.	kA	R.
<g/>
EV	Eva	k1gFnPc2	Eva
<g/>
+	+	kIx~	+
<g/>
VV	VV	kA	VV
<g/>
60	[number]	k4	60
<g/>
LF	LF	kA	LF
<g/>
+	+	kIx~	+
<g/>
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R.	R.	kA	R.
<g/>
EV	Eva	k1gFnPc2	Eva
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
trojče	trojče	k1gNnSc4	trojče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
délce	délka	k1gFnSc3	délka
přesahující	přesahující	k2eAgFnSc4d1	přesahující
40	[number]	k4	40
m	m	kA	m
získat	získat	k5eAaPmF	získat
speciální	speciální	k2eAgNnPc1d1	speciální
povolení	povolení	k1gNnPc1	povolení
od	od	k7c2	od
Drážního	drážní	k2eAgInSc2d1	drážní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
nejkapacitnější	kapacitný	k2eAgFnSc1d3	kapacitný
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
souprava	souprava	k1gFnSc1	souprava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vyjela	vyjet	k5eAaPmAgFnS	vyjet
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
nová	nový	k2eAgFnSc1d1	nová
tramvaj	tramvaj	k1gFnSc1	tramvaj
VarioLF	VarioLF	k1gFnSc2	VarioLF
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
v	v	k7c6	v
září	září	k1gNnSc6	září
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
dodána	dodat	k5eAaPmNgFnS	dodat
první	první	k4xOgInPc4	první
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
objednaných	objednaný	k2eAgFnPc2d1	objednaná
částečně	částečně	k6eAd1	částečně
nízkopodlažních	nízkopodlažní	k2eAgFnPc2d1	nízkopodlažní
tramvají	tramvaj	k1gFnPc2	tramvaj
Škoda	škoda	k6eAd1	škoda
13	[number]	k4	13
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
dodávky	dodávka	k1gFnPc1	dodávka
probíhaly	probíhat	k5eAaImAgFnP	probíhat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
devatenáct	devatenáct	k4xCc4	devatenáct
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
vlečné	vlečný	k2eAgInPc1d1	vlečný
vozy	vůz	k1gInPc1	vůz
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
"	"	kIx"	"
<g/>
trojčat	trojče	k1gNnPc2	trojče
<g/>
"	"	kIx"	"
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
VarioLF	VarioLF	k1gFnSc2	VarioLF
<g/>
+	+	kIx~	+
<g/>
VV	VV	kA	VV
<g/>
60	[number]	k4	60
<g/>
LF	LF	kA	LF
<g/>
+	+	kIx~	+
<g/>
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
R.	R.	kA	R.
<g/>
EV.	EV.	k1gFnSc2	EV.
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
dodána	dodat	k5eAaPmNgFnS	dodat
první	první	k4xOgFnSc1	první
tramvaj	tramvaj	k1gFnSc1	tramvaj
dalšího	další	k2eAgInSc2d1	další
typu	typ	k1gInSc2	typ
-	-	kIx~	-
kloubové	kloubový	k2eAgInPc1d1	kloubový
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
nízkopodlažní	nízkopodlažní	k2eAgFnSc1d1	nízkopodlažní
VarioLF	VarioLF	k1gFnSc1	VarioLF
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bývají	bývat	k5eAaImIp3nP	bývat
tyto	tento	k3xDgInPc4	tento
vozy	vůz	k1gInPc4	vůz
řazeny	řazen	k2eAgInPc4d1	řazen
do	do	k7c2	do
souprav	souprava	k1gFnPc2	souprava
2	[number]	k4	2
<g/>
×	×	k?	×
VarioLF	VarioLF	k1gFnSc2	VarioLF
<g/>
2	[number]	k4	2
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
soupravy	souprava	k1gFnPc1	souprava
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
soupravami	souprava	k1gFnPc7	souprava
VarioLF	VarioLF	k1gMnSc7	VarioLF
<g/>
2	[number]	k4	2
+	+	kIx~	+
VarioLF	VarioLF	k1gFnSc2	VarioLF
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
probíhaly	probíhat	k5eAaImAgFnP	probíhat
zkušební	zkušební	k2eAgFnPc1d1	zkušební
jízdy	jízda	k1gFnPc1	jízda
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
u	u	k7c2	u
jednoho	jeden	k4xCgNnSc2	jeden
z	z	k7c2	z
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
odstavených	odstavený	k2eAgInPc2d1	odstavený
nízkopodlažních	nízkopodlažní	k2eAgInPc2d1	nízkopodlažní
vozů	vůz	k1gInPc2	vůz
RT	RT	kA	RT
<g/>
6	[number]	k4	6
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
zkoušky	zkouška	k1gFnPc1	zkouška
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
o	o	k7c6	o
schválení	schválení	k1gNnSc6	schválení
typu	typ	k1gInSc2	typ
drážního	drážní	k2eAgNnSc2d1	drážní
vozidla	vozidlo	k1gNnSc2	vozidlo
Drážním	drážní	k2eAgInSc7d1	drážní
úřadem	úřad	k1gInSc7	úřad
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
RT6N1	RT6N1	k1gFnPc2	RT6N1
byly	být	k5eAaImAgFnP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
poruchové	poruchový	k2eAgNnSc1d1	poruchové
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
zprovozněny	zprovoznit	k5eAaPmNgInP	zprovoznit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
vozy	vůz	k1gInPc4	vůz
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
odstavený	odstavený	k2eAgInSc1d1	odstavený
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
Poznaně	Poznaň	k1gFnSc2	Poznaň
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
poruchovost	poruchovost	k1gFnSc4	poruchovost
brzd	brzda	k1gFnPc2	brzda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
postupně	postupně	k6eAd1	postupně
odstavovány	odstavovat	k5eAaImNgFnP	odstavovat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
dojezdila	dojezdit	k5eAaPmAgFnS	dojezdit
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
potkal	potkat	k5eAaPmAgInS	potkat
také	také	k9	také
vlečné	vlečný	k2eAgInPc4d1	vlečný
vozy	vůz	k1gInPc4	vůz
VV	VV	kA	VV
<g/>
60	[number]	k4	60
<g/>
LF	LF	kA	LF
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
poruchovost	poruchovost	k1gFnSc4	poruchovost
brzd	brzda	k1gFnPc2	brzda
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
odstavovány	odstavován	k2eAgInPc1d1	odstavován
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
dojezdily	dojezdit	k5eAaPmAgInP	dojezdit
poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
K3R-N	K3R-N	k1gFnPc1	K3R-N
taktéž	taktéž	k?	taktéž
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
bezproblémové	bezproblémový	k2eAgInPc1d1	bezproblémový
-	-	kIx~	-
pro	pro	k7c4	pro
závadu	závada	k1gFnSc4	závada
kloubních	kloubní	k2eAgInPc2d1	kloubní
čepů	čep	k1gInPc2	čep
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
odstaveny	odstaven	k2eAgMnPc4d1	odstaven
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc1d1	zbylý
dva	dva	k4xCgInPc1	dva
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
dalších	další	k2eAgInPc2d1	další
deset	deset	k4xCc4	deset
kusů	kus	k1gInPc2	kus
tramvají	tramvaj	k1gFnPc2	tramvaj
13T	[number]	k4	13T
již	již	k9	již
s	s	k7c7	s
částečnými	částečný	k2eAgFnPc7d1	částečná
modifikacemi	modifikace	k1gFnPc7	modifikace
(	(	kIx(	(
<g/>
především	především	k9	především
změna	změna	k1gFnSc1	změna
interiéru	interiér	k1gInSc2	interiér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
dojezdila	dojezdit	k5eAaPmAgFnS	dojezdit
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
poslední	poslední	k2eAgFnSc1d1	poslední
tramvaj	tramvaj	k1gFnSc1	tramvaj
typu	typ	k1gInSc2	typ
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
M.	M.	kA	M.
Tři	tři	k4xCgNnPc4	tři
vozy	vůz	k1gInPc1	vůz
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
však	však	k9	však
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
služebním	služební	k2eAgInSc6d1	služební
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
dodána	dodat	k5eAaPmNgFnS	dodat
první	první	k4xOgFnSc1	první
tramvaj	tramvaj	k1gFnSc1	tramvaj
13T	[number]	k4	13T
z	z	k7c2	z
dvacetikusové	dvacetikusový	k2eAgFnSc2d1	dvacetikusová
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
několika	několik	k4yIc3	několik
změnami	změna	k1gFnPc7	změna
oproti	oproti	k7c3	oproti
dříve	dříve	k6eAd2	dříve
dodaným	dodaný	k2eAgFnPc3d1	dodaná
tramvajím	tramvaj	k1gFnPc3	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Již	jenž	k3xRgFnSc4	jenž
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
brněnském	brněnský	k2eAgInSc6d1	brněnský
kolejovém	kolejový	k2eAgInSc6d1	kolejový
diametru	diametr	k1gInSc6	diametr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stavbě	stavba	k1gFnSc3	stavba
podzemní	podzemní	k2eAgFnPc1d1	podzemní
dráhy	dráha	k1gFnPc1	dráha
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
náročný	náročný	k2eAgInSc1d1	náročný
a	a	k8xC	a
proto	proto	k8xC	proto
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
realizace	realizace	k1gFnSc1	realizace
trvat	trvat	k5eAaImF	trvat
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
prodloužení	prodloužení	k1gNnSc3	prodloužení
stávajících	stávající	k2eAgFnPc2d1	stávající
tratí	trať	k1gFnPc2	trať
(	(	kIx(	(
<g/>
odbočka	odbočka	k1gFnSc1	odbočka
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Lískovci	Lískovec	k1gInSc6	Lískovec
k	k	k7c3	k
Univerzitnímu	univerzitní	k2eAgInSc3d1	univerzitní
kampusu	kampus	k1gInSc3	kampus
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc3	prodloužení
trati	trať	k1gFnSc2	trať
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
na	na	k7c4	na
sídliště	sídliště	k1gNnSc4	sídliště
Kamechy	Kamech	k1gInPc4	Kamech
<g/>
,	,	kIx,	,
či	či	k8xC	či
prodloužení	prodloužení	k1gNnSc4	prodloužení
ze	z	k7c2	z
Štefánikovy	Štefánikův	k2eAgFnSc2d1	Štefánikova
čtvrti	čtvrt	k1gFnSc2	čtvrt
na	na	k7c4	na
Lesnou	lesný	k2eAgFnSc4d1	Lesná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
je	být	k5eAaImIp3nS	být
zorganizování	zorganizování	k1gNnSc1	zorganizování
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
případném	případný	k2eAgInSc6d1	případný
přesunu	přesun	k1gInSc6	přesun
vlakového	vlakový	k2eAgNnSc2d1	vlakové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gFnSc6	jehož
poloze	poloha	k1gFnSc6	poloha
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
již	již	k6eAd1	již
druhé	druhý	k4xOgNnSc4	druhý
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
neplatné	platný	k2eNgNnSc1d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
vozovny	vozovna	k1gFnPc4	vozovna
<g/>
:	:	kIx,	:
vozovna	vozovna	k1gFnSc1	vozovna
Medlánky	Medlánka	k1gFnSc2	Medlánka
(	(	kIx(	(
<g/>
od	od	k7c2	od
1958	[number]	k4	1958
<g/>
;	;	kIx,	;
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ústřední	ústřední	k2eAgFnSc2d1	ústřední
dílny	dílna	k1gFnSc2	dílna
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
<g/>
)	)	kIx)	)
vozovna	vozovna	k1gFnSc1	vozovna
Pisárky	Pisárka	k1gFnSc2	Pisárka
(	(	kIx(	(
<g/>
od	od	k7c2	od
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
noční	noční	k2eAgNnSc4d1	noční
odstavování	odstavování	k1gNnSc4	odstavování
čtyř	čtyři	k4xCgFnPc2	čtyři
obousměrných	obousměrný	k2eAgFnPc2d1	obousměrná
tramvají	tramvaj	k1gFnPc2	tramvaj
KT8D5	KT8D5	k1gFnSc1	KT8D5
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
využívána	využívat	k5eAaImNgFnS	využívat
také	také	k9	také
podzemní	podzemní	k2eAgFnSc1d1	podzemní
zastávka	zastávka	k1gFnSc1	zastávka
Jírova	Jírův	k2eAgFnSc1d1	Jírova
v	v	k7c6	v
Líšni	Líšeň	k1gFnSc6	Líšeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vjezdovými	vjezdový	k2eAgInPc7d1	vjezdový
vraty	vrat	k1gInPc7	vrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
fungovaly	fungovat	k5eAaImAgFnP	fungovat
i	i	k8xC	i
tři	tři	k4xCgFnPc4	tři
jiné	jiný	k2eAgFnPc4d1	jiná
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
vozovny	vozovna	k1gFnPc4	vozovna
<g/>
:	:	kIx,	:
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Marešově	Marešův	k2eAgFnSc6d1	Marešova
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
vozovna	vozovna	k1gFnSc1	vozovna
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
a	a	k8xC	a
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
měnírna	měnírna	k1gFnSc1	měnírna
<g/>
)	)	kIx)	)
vozovna	vozovna	k1gFnSc1	vozovna
Husovice	Husovice	k1gFnPc1	Husovice
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
)	)	kIx)	)
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nachází	nacházet	k5eAaImIp3nS	nacházet
28	[number]	k4	28
měníren	měnírna	k1gFnPc2	měnírna
(	(	kIx(	(
<g/>
16	[number]	k4	16
stabilních	stabilní	k2eAgMnPc2d1	stabilní
<g/>
,	,	kIx,	,
10	[number]	k4	10
kontejnerových	kontejnerový	k2eAgFnPc2d1	kontejnerová
a	a	k8xC	a
2	[number]	k4	2
podzemní	podzemní	k2eAgFnSc4d1	podzemní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
napájí	napájet	k5eAaImIp3nP	napájet
proudem	proud	k1gInSc7	proud
i	i	k9	i
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
troleje	trolej	k1gFnSc2	trolej
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
pojízdná	pojízdný	k2eAgFnSc1d1	pojízdná
měnírna	měnírna	k1gFnSc1	měnírna
v	v	k7c6	v
Žabovřeskách	Žabovřesky	k1gFnPc6	Žabovřesky
na	na	k7c6	na
Přívratu	přívrat	k1gInSc6	přívrat
byla	být	k5eAaImAgNnP	být
zlikvidována	zlikvidovat	k5eAaPmNgNnP	zlikvidovat
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
a	a	k8xC	a
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
kontejnerovou	kontejnerový	k2eAgFnSc7d1	kontejnerová
měnírnou	měnírna	k1gFnSc7	měnírna
<g/>
.	.	kIx.	.
</s>
<s>
Napájení	napájení	k1gNnSc1	napájení
brněnské	brněnský	k2eAgFnSc2d1	brněnská
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
600	[number]	k4	600
V	v	k7c6	v
stejnosměrných	stejnosměrný	k2eAgFnPc6d1	stejnosměrná
<g/>
;	;	kIx,	;
záporný	záporný	k2eAgInSc1d1	záporný
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
troleji	trolej	k1gInSc6	trolej
(	(	kIx(	(
<g/>
sběrači	sběrač	k1gMnPc1	sběrač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kladný	kladný	k2eAgInSc1d1	kladný
v	v	k7c6	v
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
tramvajovým	tramvajový	k2eAgInPc3d1	tramvajový
provozům	provoz	k1gInPc3	provoz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Praha	Praha	k1gFnSc1	Praha
nebo	nebo	k8xC	nebo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
minimalizovaly	minimalizovat	k5eAaBmAgInP	minimalizovat
účinky	účinek	k1gInPc1	účinek
bludných	bludný	k2eAgInPc2d1	bludný
(	(	kIx(	(
<g/>
plazivých	plazivý	k2eAgInPc2d1	plazivý
<g/>
)	)	kIx)	)
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
měníren	měnírna	k1gFnPc2	měnírna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
ničily	ničit	k5eAaImAgFnP	ničit
kovové	kovový	k2eAgInPc4d1	kovový
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
potrubí	potrubí	k1gNnSc2	potrubí
i	i	k8xC	i
samotné	samotný	k2eAgFnSc2d1	samotná
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
)	)	kIx)	)
uložené	uložený	k2eAgFnSc2d1	uložená
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tramvajové	tramvajový	k2eAgInPc4d1	tramvajový
provozy	provoz	k1gInPc4	provoz
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
polaritou	polarita	k1gFnSc7	polarita
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
nejrozmanitější	rozmanitý	k2eAgInSc1d3	nejrozmanitější
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
tramvají	tramvaj	k1gFnPc2	tramvaj
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
305	[number]	k4	305
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
služební	služební	k2eAgInPc1d1	služební
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
tři	tři	k4xCgInPc1	tři
vozy	vůz	k1gInPc1	vůz
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
restaurační	restaurační	k2eAgInSc4d1	restaurační
jeden	jeden	k4xCgInSc4	jeden
vůz	vůz	k1gInSc4	vůz
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
R-RT	R-RT	k1gFnSc2	R-RT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
typy	typ	k1gInPc1	typ
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sbírka	sbírka	k1gFnSc1	sbírka
vozidel	vozidlo	k1gNnPc2	vozidlo
MHD	MHD	kA	MHD
Technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
brněnských	brněnský	k2eAgFnPc2d1	brněnská
i	i	k8xC	i
mimobrněnských	mimobrněnský	k2eAgFnPc2d1	mimobrněnský
historických	historický	k2eAgFnPc2d1	historická
tramvají	tramvaj	k1gFnPc2	tramvaj
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
kolejových	kolejový	k2eAgNnPc2d1	kolejové
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
Technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Umístěny	umístěn	k2eAgInPc1d1	umístěn
jsou	být	k5eAaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
přístupném	přístupný	k2eAgInSc6d1	přístupný
depozitáři	depozitář	k1gInSc6	depozitář
v	v	k7c6	v
Líšni	Líšeň	k1gFnSc6	Líšeň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
někdejší	někdejší	k2eAgFnSc2d1	někdejší
místní	místní	k2eAgFnSc2d1	místní
dráhy	dráha	k1gFnSc2	dráha
Černovice	Černovice	k1gFnSc2	Černovice
-	-	kIx~	-
Líšeň	Líšeň	k1gFnSc1	Líšeň
<g/>
.	.	kIx.	.
</s>
<s>
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
trať	trať	k1gFnSc1	trať
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
bývalé	bývalý	k2eAgFnSc2d1	bývalá
místní	místní	k2eAgFnSc2d1	místní
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
-	-	kIx~	-
Líšeň	Líšeň	k1gFnSc1	Líšeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejezdí	jezdit	k5eNaImIp3nS	jezdit
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nenašly	najít	k5eNaPmAgInP	najít
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
uvězněny	uvěznit	k5eAaPmNgInP	uvěznit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
halách	hala	k1gFnPc6	hala
(	(	kIx(	(
<g/>
Technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
nechalo	nechat	k5eAaPmAgNnS	nechat
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
nákladního	nákladní	k2eAgInSc2d1	nákladní
automobilu	automobil	k1gInSc2	automobil
převézt	převézt	k5eAaPmF	převézt
vůz	vůz	k1gInSc4	vůz
koňky	koňka	k1gFnSc2	koňka
a	a	k8xC	a
soupravu	souprava	k1gFnSc4	souprava
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
do	do	k7c2	do
medlánecké	medlánecký	k2eAgFnSc2d1	medlánecká
vozovny	vozovna	k1gFnSc2	vozovna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
tyto	tento	k3xDgInPc1	tento
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
vozy	vůz	k1gInPc1	vůz
vyjíždět	vyjíždět	k5eAaImF	vyjíždět
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
historické	historický	k2eAgFnPc1d1	historická
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
ve	v	k7c6	v
vozovně	vozovna	k1gFnSc6	vozovna
Medlánky	Medlánka	k1gFnSc2	Medlánka
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
další	další	k2eAgFnSc4d1	další
tramvaj	tramvaj	k1gFnSc4	tramvaj
<g/>
,	,	kIx,	,
zapůjčenou	zapůjčený	k2eAgFnSc4d1	zapůjčená
TMB	TMB	kA	TMB
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaPmAgInS	využívat
na	na	k7c6	na
nostalgické	nostalgický	k2eAgFnSc6d1	nostalgická
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
DPMB	DPMB	kA	DPMB
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
motorový	motorový	k2eAgInSc1d1	motorový
vůz	vůz	k1gInSc1	vůz
č.	č.	k?	č.
107	[number]	k4	107
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
(	(	kIx(	(
<g/>
výrobce	výrobce	k1gMnSc2	výrobce
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlečný	vlečný	k2eAgInSc1d1	vlečný
vůz	vůz	k1gInSc1	vůz
č.	č.	k?	č.
215	[number]	k4	215
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
(	(	kIx(	(
<g/>
výrobce	výrobce	k1gMnSc1	výrobce
Ringhoffer	Ringhoffer	k1gMnSc1	Ringhoffer
<g/>
)	)	kIx)	)
a	a	k8xC	a
kavárenská	kavárenský	k2eAgFnSc1d1	kavárenská
tramvaj	tramvaj	k1gFnSc1	tramvaj
č.	č.	k?	č.
4058	[number]	k4	4058
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
typ	typ	k1gInSc1	typ
4MT	[number]	k4	4MT
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgFnSc1d1	vlastní
výroba	výroba	k1gFnSc1	výroba
podle	podle	k7c2	podle
dokumentace	dokumentace	k1gFnSc2	dokumentace
z	z	k7c2	z
Královopolské	královopolský	k2eAgFnSc2d1	Královopolská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
DPMB	DPMB	kA	DPMB
vlastní	vlastní	k2eAgFnSc4d1	vlastní
sbírku	sbírka	k1gFnSc4	sbírka
historických	historický	k2eAgNnPc2d1	historické
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
retro	retro	k1gNnPc2	retro
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůz	vůz	k1gInSc4	vůz
T3	T3	k1gFnSc2	T3
č.	č.	k?	č.
1525	[number]	k4	1525
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
tramvají	tramvaj	k1gFnSc7	tramvaj
K2YU	K2YU	k1gFnPc2	K2YU
č.	č.	k?	č.
1123	[number]	k4	1123
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
vozidly	vozidlo	k1gNnPc7	vozidlo
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
vlečný	vlečný	k2eAgInSc4d1	vlečný
vůz	vůz	k1gInSc4	vůz
č.	č.	k?	č.
313	[number]	k4	313
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
(	(	kIx(	(
<g/>
výrobce	výrobce	k1gMnSc2	výrobce
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
<g/>
)	)	kIx)	)
a	a	k8xC	a
tramvaje	tramvaj	k1gFnPc1	tramvaj
T2	T2	k1gFnPc2	T2
(	(	kIx(	(
<g/>
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nepotřebného	potřebný	k2eNgInSc2d1	nepotřebný
služebního	služební	k2eAgInSc2d1	služební
vozu	vůz	k1gInSc2	vůz
č.	č.	k?	č.
685	[number]	k4	685
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
)	)	kIx)	)
a	a	k8xC	a
T3R	T3R	k1gFnSc1	T3R
č.	č.	k?	č.
1615	[number]	k4	1615
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
modernizace	modernizace	k1gFnSc2	modernizace
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
T3	T3	k1gFnSc2	T3
č.	č.	k?	č.
1525	[number]	k4	1525
a	a	k8xC	a
K2YU	K2YU	k1gFnSc1	K2YU
č.	č.	k?	č.
1123	[number]	k4	1123
byly	být	k5eAaImAgFnP	být
veřejnosti	veřejnost	k1gFnPc1	veřejnost
poprvé	poprvé	k6eAd1	poprvé
představeny	představen	k2eAgInPc4d1	představen
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
na	na	k7c4	na
pravidelně	pravidelně	k6eAd1	pravidelně
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
Dopravní	dopravní	k2eAgFnSc3d1	dopravní
nostalgii	nostalgie	k1gFnSc3	nostalgie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
město	město	k1gNnSc1	město
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
během	během	k7c2	během
letního	letní	k2eAgNnSc2d1	letní
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc4	první
sobotu	sobota	k1gFnSc4	sobota
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
č.	č.	k?	č.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
