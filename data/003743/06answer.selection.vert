<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
ve	v	k7c6	v
vážném	vážný	k2eAgNnSc6d1	vážné
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
stran	strana	k1gFnPc2	strana
německé	německý	k2eAgFnSc2d1	německá
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
motivy	motiv	k1gInPc4	motiv
politické	politický	k2eAgInPc4d1	politický
(	(	kIx(	(
<g/>
revizionismus	revizionismus	k1gInSc4	revizionismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ideologické	ideologický	k2eAgInPc1d1	ideologický
(	(	kIx(	(
<g/>
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
bývalá	bývalý	k2eAgNnPc4d1	bývalé
území	území	k1gNnPc4	území
německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgInPc1d1	strategický
(	(	kIx(	(
<g/>
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
potenciál	potenciál	k1gInSc1	potenciál
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
poloha	poloha	k1gFnSc1	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
