<p>
<s>
Benátecký	benátecký	k2eAgInSc1d1	benátecký
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
251	[number]	k4	251
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Benátský	benátský	k2eAgInSc1d1	benátský
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mladá	mladý	k2eAgFnSc1d1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
zjz	zjz	k?	zjz
<g/>
.	.	kIx.	.
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Lipník	Lipník	k1gInSc1	Lipník
<g/>
,	,	kIx,	,
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Staré	Staré	k2eAgFnSc2d1	Staré
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
významný	významný	k2eAgInSc1d1	významný
vrchol	vrchol	k1gInSc1	vrchol
Vrutické	Vrutický	k2eAgFnSc2d1	Vrutický
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
vrchu	vrch	k1gInSc2	vrch
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
vrchem	vrch	k1gInSc7	vrch
souvisí	souviset	k5eAaImIp3nP	souviset
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Pod	pod	k7c7	pod
Benáteckým	benátecký	k2eAgInSc7d1	benátecký
vrchem	vrch	k1gInSc7	vrch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
2,5	[number]	k4	2,5
km	km	kA	km
na	na	k7c4	na
JJV	JJV	kA	JJV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
zařazení	zařazení	k1gNnSc4	zařazení
==	==	k?	==
</s>
</p>
<p>
<s>
Vrch	vrch	k1gInSc1	vrch
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gFnSc4	podcelka
Dolnojizerská	Dolnojizerský	k2eAgFnSc1d1	Dolnojizerský
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc6	okrsek
Vrutická	Vrutický	k2eAgFnSc1d1	Vrutický
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
a	a	k8xC	a
podokrsku	podokrska	k1gFnSc4	podokrska
Jiřická	Jiřická	k1gFnSc1	Jiřická
plošina	plošina	k1gFnSc1	plošina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Automobilem	automobil	k1gInSc7	automobil
lze	lze	k6eAd1	lze
přijet	přijet	k5eAaPmF	přijet
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
Benátky	Benátky	k1gFnPc1	Benátky
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
–	–	k?	–
Boží	božit	k5eAaImIp3nS	božit
Dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
těsném	těsný	k2eAgNnSc6d1	těsné
jižním	jižní	k2eAgNnSc6d1	jižní
sousedství	sousedství	k1gNnSc6	sousedství
vrch	vrch	k1gInSc1	vrch
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Východním	východní	k2eAgNnSc7d1	východní
sousedstvím	sousedství	k1gNnSc7	sousedství
vrchu	vrch	k1gInSc2	vrch
vede	vést	k5eAaImIp3nS	vést
modrá	modrý	k2eAgFnSc1d1	modrá
turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Benátecký	benátecký	k2eAgInSc4d1	benátecký
vrch	vrch	k1gInSc4	vrch
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
