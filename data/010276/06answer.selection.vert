<s>
Benátecký	benátecký	k2eAgInSc1d1	benátecký
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
251	[number]	k4	251
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Benátský	benátský	k2eAgInSc1d1	benátský
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mladá	mladý	k2eAgFnSc1d1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
zjz	zjz	k?	zjz
<g/>
.	.	kIx.	.
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Lipník	Lipník	k1gInSc1	Lipník
<g/>
,	,	kIx,	,
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Staré	Staré	k2eAgFnSc2d1	Staré
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
.	.	kIx.	.
</s>
