<s>
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
byl	být	k5eAaImAgInS
moravský	moravský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Karolinka	Karolinka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
a	a	k8xC
zanikl	zaniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
2000	#num#	k4
sloučením	sloučení	k1gNnSc7
s	s	k7c7
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
do	do	k7c2
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
+	+	kIx~
Karolinka	Karolinka	k1gFnSc1
<g/>
.	.	kIx.
</s>