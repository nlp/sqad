<s>
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
</s>
<s>
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
Fotbalový	fotbalový	k2eAgMnSc1d1
stadion	stadion	k1gNnSc4
v	v	k7c6
KarolinceNázev	KarolinceNázev	k1gFnSc6
</s>
<s>
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
Země	země	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Karolinka	Karolinka	k1gFnSc1
Založen	založen	k2eAgMnSc1d1
</s>
<s>
1931	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
2000	#num#	k4
Asociace	asociace	k1gFnSc1
</s>
<s>
ČMFS	ČMFS	kA
Stadion	stadion	k1gInSc1
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
,	,	kIx,
Karolinka	Karolinka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kapacita	kapacita	k1gFnSc1
</s>
<s>
1	#num#	k4
000	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
byl	být	k5eAaImAgInS
moravský	moravský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Karolinka	Karolinka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
a	a	k8xC
zanikl	zaniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
2000	#num#	k4
sloučením	sloučení	k1gNnSc7
s	s	k7c7
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
do	do	k7c2
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
+	+	kIx~
Karolinka	Karolinka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svá	svůj	k3xOyFgNnPc4
utkání	utkání	k1gNnPc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
v	v	k7c6
Karolince	Karolinka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezoně	sezona	k1gFnSc6
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgInS
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1931	#num#	k4
–	–	k?
SK	Sk	kA
Valašská	valašský	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Nový	Nový	k1gMnSc1
Hrozenkov	Hrozenkov	k1gInSc1
(	(	kIx(
<g/>
Sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Valašská	valašský	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Nový	Nový	k1gMnSc1
Hrozenkov	Hrozenkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1938	#num#	k4
–	–	k?
zánik	zánik	k1gInSc1
</s>
<s>
1940	#num#	k4
–	–	k?
obnovena	obnoven	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
jako	jako	k8xC,k8xS
MNS	MNS	kA
Nový	nový	k2eAgInSc1d1
Hrozenkov	Hrozenkov	k1gInSc1
(	(	kIx(
<g/>
Mládež	mládež	k1gFnSc1
národního	národní	k2eAgNnSc2d1
souručenství	souručenství	k1gNnSc2
Nový	nový	k2eAgInSc4d1
Hrozenkov	Hrozenkov	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1941	#num#	k4
–	–	k?
SK	Sk	kA
Karolinina	Karolinin	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
(	(	kIx(
<g/>
Sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Karolinina	Karolinin	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
–	–	k?
Sokol	Sokol	k1gInSc1
Karolinina	Karolinin	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
</s>
<s>
1951	#num#	k4
–	–	k?
Sokol	Sokol	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
</s>
<s>
1953	#num#	k4
–	–	k?
TJ	tj	kA
Jiskra	jiskra	k1gFnSc1
Karolinka	Karolinka	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Jiskra	jiskra	k1gFnSc1
Karolinka	Karolinka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
–	–	k?
TJ	tj	kA
Sklo	sklo	k1gNnSc1
Karolinka	Karolinka	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Sklo	sklo	k1gNnSc1
Karolinka	Karolinka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
–	–	k?
TJ	tj	kA
MS	MS	kA
Karolinka	Karolinka	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Moravské	moravský	k2eAgFnSc2d1
sklárny	sklárna	k1gFnSc2
Karolinka	Karolinka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
–	–	k?
Sport	sport	k1gInSc1
Karolinka	Karolinka	k1gFnSc1
</s>
<s>
2000	#num#	k4
–	–	k?
sloučen	sloučen	k2eAgInSc1d1
s	s	k7c7
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
do	do	k7c2
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
+	+	kIx~
Karolinka	Karolinka	k1gFnSc1
</s>
<s>
Stadion	stadion	k1gInSc1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1986	#num#	k4
byl	být	k5eAaImAgInS
dostavěn	dostavěn	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
areál	areál	k1gInSc1
s	s	k7c7
travnatou	travnatý	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
o	o	k7c6
rozměrech	rozměr	k1gInPc6
104	#num#	k4
<g/>
×	×	k?
<g/>
70	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
zmodernizovány	zmodernizován	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
mj.	mj.	kA
šatny	šatna	k1gFnPc1
<g/>
,	,	kIx,
sprchy	sprcha	k1gFnPc1
<g/>
,	,	kIx,
klubovna	klubovna	k1gFnSc1
a	a	k8xC
bufet	bufet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadion	stadion	k1gInSc1
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřen	k2eAgInSc1d1
atraktivním	atraktivní	k2eAgNnSc7d1
mezinárodním	mezinárodní	k2eAgNnSc7d1
utkáním	utkání	k1gNnSc7
Interpoháru	Interpohár	k1gInSc2
mezi	mezi	k7c7
úřadujícím	úřadující	k2eAgMnSc7d1
československým	československý	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
TJ	tj	kA
Vítkovice	Vítkovice	k1gInPc4
a	a	k8xC
švédským	švédský	k2eAgMnSc7d1
IFK	IFK	kA
Göteborg	Göteborg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zápas	zápas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
řídil	řídit	k5eAaImAgMnS
Dušan	Dušan	k1gMnSc1
Krchňák	krchňák	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
v	v	k7c4
sobotu	sobota	k1gFnSc4
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986	#num#	k4
před	před	k7c7
3	#num#	k4
tisícovkami	tisícovka	k1gFnPc7
diváků	divák	k1gMnPc2
a	a	k8xC
Vítkovice	Vítkovice	k1gInPc1
zvítězily	zvítězit	k5eAaPmAgInP
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
poločas	poločas	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Branky	branka	k1gFnSc2
domácích	domácí	k1gFnPc2
vstřelili	vstřelit	k5eAaPmAgMnP
Zdeněk	Zdeněk	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
<g/>
,	,	kIx,
Bohuš	Bohuš	k1gMnSc1
Keler	Keler	k1gMnSc1
a	a	k8xC
Luděk	Luděk	k1gMnSc1
Kovačík	Kovačík	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c4
hosty	host	k1gMnPc4
skóroval	skórovat	k5eAaBmAgInS
Bjur	Bjur	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1974	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Severomoravského	severomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s>
62615655727	#num#	k4
<g/>
+	+	kIx~
<g/>
3036	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
530	#num#	k4
</s>
<s>
..	..	k?
<g/>
.26	.26	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
530	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
430	#num#	k4
</s>
<s>
..	..	k?
<g/>
.27	.27	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
430	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
430	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
33087153547	#num#	k4
<g/>
−	−	k?
<g/>
1223	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
42687113244	#num#	k4
<g/>
−	−	k?
<g/>
1223	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
42694133145	#num#	k4
<g/>
−	−	k?
<g/>
1422	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
42614211661	#num#	k4
<g/>
−	−	k?
<g/>
456	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
526	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
526	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
530116132949	#num#	k4
<g/>
−	−	k?
<g/>
2028	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
53098133744	#num#	k4
<g/>
−	−	k?
<g/>
726	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
530109113840	#num#	k4
<g/>
−	−	k?
<g/>
229	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
530	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Severomoravského	severomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s>
626	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
626	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
62677122647	#num#	k4
<g/>
−	−	k?
<g/>
2121	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
62686122544	#num#	k4
<g/>
−	−	k?
<g/>
1922	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
62696113441	#num#	k4
<g/>
−	−	k?
<g/>
733	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
A	a	k9
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
62635181554	#num#	k4
<g/>
−	−	k?
<g/>
3914	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
B	B	kA
třída	třída	k1gFnSc1
Středomoravské	středomoravský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
726104123347	#num#	k4
<g/>
−	−	k?
<g/>
1434	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
přihlášeno	přihlásit	k5eAaPmNgNnS
v	v	k7c6
žádné	žádný	k3yNgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Vsetínska	Vsetínsko	k1gNnSc2
</s>
<s>
9	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
Vsetínska	Vsetínsko	k1gNnSc2
</s>
<s>
8	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Fotbalový	fotbalový	k2eAgInSc4d1
stadion	stadion	k1gInSc4
v	v	k7c6
Karolince	Karolinka	k1gFnSc6
<g/>
,	,	kIx,
europlan-online	europlan-onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
fotbalu	fotbal	k1gInSc2
v	v	k7c6
Karolince	Karolinka	k1gFnSc6
<g/>
,	,	kIx,
karolinka	karolinka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Ve	v	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
Interpoháru	Interpohár	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
Cheb	Cheb	k1gInSc1
mužstvem	mužstvo	k1gNnSc7
překvapení	překvapení	k1gNnPc2
–	–	k?
šest	šest	k4xCc4
bodů	bod	k1gInPc2
na	na	k7c4
naše	náš	k3xOp1gNnSc4
konto	konto	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archiv	archiv	k1gInSc1
<g/>
.	.	kIx.
<g/>
ucl	ucl	k?
<g/>
.	.	kIx.
<g/>
cas	cas	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
29.06	29.06	k4
<g/>
.1986	.1986	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
fcvkk	fcvkk	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
kobeřické	kobeřická	k1gFnSc2
kopané	kopaná	k1gFnSc2
<g/>
:	:	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
sokolkoberice	sokolkoberika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
kobeřické	kobeřická	k1gFnSc2
kopané	kopaná	k1gFnSc2
<g/>
:	:	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
sokolkoberice	sokolkoberika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
kobeřické	kobeřická	k1gFnSc2
kopané	kopaná	k1gFnSc2
<g/>
:	:	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
sokolkoberice	sokolkoberika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
31	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
5.08	5.08	k4
<g/>
.1993	.1993	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
14	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
↑	↑	k?
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
28	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
(	(	kIx(
<g/>
21.07	21.07	k4
<g/>
.1994	.1994	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
41	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
↑	↑	k?
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
(	(	kIx(
<g/>
13.07	13.07	k4
<g/>
.1995	.1995	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
↑	↑	k?
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
4.07	4.07	k4
<g/>
.1996	.1996	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
↑	↑	k?
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
3.07	3.07	k4
<g/>
.1997	.1997	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
1896	#num#	k4
–	–	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc7
1997	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
31	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
5.08	5.08	k4
<g/>
.1993	.1993	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
14	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
28	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
(	(	kIx(
<g/>
21.07	21.07	k4
<g/>
.1994	.1994	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
41	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
(	(	kIx(
<g/>
13.07	13.07	k4
<g/>
.1995	.1995	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
4.07	4.07	k4
<g/>
.1996	.1996	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Gól	gól	k1gInSc1
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
3.07	3.07	k4
<g/>
.1997	.1997	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
FC	FC	kA
Velké	velký	k2eAgFnPc4d1
Karlovice	Karlovice	k1gFnPc4
+	+	kIx~
Karolinka	Karolinka	k1gFnSc1
Archivováno	archivován	k2eAgNnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
velkekarlovice	velkekarlovice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
