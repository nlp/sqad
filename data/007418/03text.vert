<s>
Skyfall	Skyfalla	k1gFnPc2	Skyfalla
je	být	k5eAaImIp3nS	být
dvacátá	dvacátý	k4xOgNnPc4	dvacátý
třetí	třetí	k4xOgFnSc1	třetí
filmová	filmový	k2eAgFnSc1d1	filmová
bondovka	bondovka	k?	bondovka
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
Eon	Eon	k1gFnSc1	Eon
Productions	Productionsa	k1gFnPc2	Productionsa
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gMnSc1	Pictures
a	a	k8xC	a
Sony	Sony	kA	Sony
Pictures	Pictures	k1gMnSc1	Pictures
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
ji	on	k3xPp3gFnSc4	on
režisér	režisér	k1gMnSc1	režisér
Sam	Sam	k1gMnSc1	Sam
Mendes	Mendes	k1gMnSc1	Mendes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
John	John	k1gMnSc1	John
Logan	Logan	k1gMnSc1	Logan
<g/>
,	,	kIx,	,
Neal	Neal	k1gMnSc1	Neal
Purvis	Purvis	k1gFnSc2	Purvis
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Wade	Wade	k?	Wade
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bonda	k1gMnSc1	Bonda
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
potřetí	potřetí	k4xO	potřetí
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Craig	Craig	k1gMnSc1	Craig
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
Bondově	Bondův	k2eAgNnSc6d1	Bondovo
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
centrálu	centrála	k1gFnSc4	centrála
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
spiknutí	spiknutí	k1gNnSc2	spiknutí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
bývalého	bývalý	k2eAgMnSc2d1	bývalý
britského	britský	k2eAgMnSc2d1	britský
agenta	agent	k1gMnSc2	agent
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc2d1	hlavní
záporné	záporný	k2eAgFnPc4d1	záporná
postavy	postava	k1gFnPc4	postava
Raoula	Raoulum	k1gNnSc2	Raoulum
Silvy	Silva	k1gFnSc2	Silva
<g/>
,	,	kIx,	,
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Javiera	Javier	k1gMnSc4	Javier
Bardema	Bardem	k1gMnSc4	Bardem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
svou	svůj	k3xOyFgFnSc4	svůj
bývalou	bývalý	k2eAgFnSc4d1	bývalá
řídící	řídící	k2eAgFnSc4d1	řídící
důstojnici	důstojnice	k1gFnSc4	důstojnice
M	M	kA	M
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
odplatu	odplata	k1gFnSc4	odplata
za	za	k7c4	za
zradu	zrada	k1gFnSc4	zrada
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
měla	mít	k5eAaImAgFnS	mít
kdysi	kdysi	k6eAd1	kdysi
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bondovce	bondovce	k?	bondovce
se	se	k3xPyFc4	se
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
představily	představit	k5eAaPmAgInP	představit
dvě	dva	k4xCgFnPc4	dva
vracející	vracející	k2eAgFnPc4d1	vracející
se	se	k3xPyFc4	se
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
absentovaly	absentovat	k5eAaImAgFnP	absentovat
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
dvou	dva	k4xCgNnPc6	dva
dílech	dílo	k1gNnPc6	dílo
série	série	k1gFnSc2	série
<g/>
:	:	kIx,	:
Q	Q	kA	Q
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
hrál	hrát	k5eAaImAgInS	hrát
Ben	Ben	k1gInSc1	Ben
Whishaw	Whishaw	k1gFnSc2	Whishaw
a	a	k8xC	a
slečna	slečna	k1gFnSc1	slečna
Eve	Eve	k1gFnSc2	Eve
Moneypenny	Moneypenna	k1gFnSc2	Moneypenna
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Naomie	Naomie	k1gFnSc1	Naomie
Harrisová	Harrisový	k2eAgFnSc1d1	Harrisová
<g/>
.	.	kIx.	.
</s>
<s>
Skyfall	Skyfall	k1gInSc1	Skyfall
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
snímkem	snímek	k1gInSc7	snímek
pro	pro	k7c4	pro
Judi	Jude	k1gFnSc4	Jude
Denchovou	Denchová	k1gFnSc4	Denchová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
úloze	úloha	k1gFnSc6	úloha
M	M	kA	M
objevila	objevit	k5eAaPmAgFnS	objevit
celkem	celkem	k6eAd1	celkem
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
postavu	postava	k1gFnSc4	postava
šéfa	šéf	k1gMnSc2	šéf
MI6	MI6	k1gMnSc2	MI6
převzal	převzít	k5eAaPmAgMnS	převzít
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
Gareth	Gareth	k1gInSc1	Gareth
Mallory	Mallora	k1gFnSc2	Mallora
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnPc1	role
smyslné	smyslný	k2eAgFnPc1d1	smyslná
Bond	bond	k1gInSc4	bond
girl	girl	k1gFnSc2	girl
Sévérine	Sévérin	k1gInSc5	Sévérin
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Bérénice	Bérénice	k1gFnSc1	Bérénice
Marloheová	Marloheová	k1gFnSc1	Marloheová
<g/>
.	.	kIx.	.
</s>
<s>
Mendes	Mendes	k1gInSc1	Mendes
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
přizván	přizvat	k5eAaPmNgMnS	přizvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
Quantum	Quantum	k1gNnSc1	Quantum
of	of	k?	of
Solace	Solace	k1gFnSc1	Solace
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
filmu	film	k1gInSc6	film
však	však	k9	však
byly	být	k5eAaImAgFnP	být
pozastaveny	pozastavit	k5eAaPmNgFnP	pozastavit
pro	pro	k7c4	pro
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
studia	studio	k1gNnSc2	studio
MGM	MGM	kA	MGM
až	až	k6eAd1	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
;	;	kIx,	;
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
konzultanta	konzultant	k1gMnSc2	konzultant
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
rozpracovaný	rozpracovaný	k2eAgInSc1d1	rozpracovaný
scénář	scénář	k1gInSc1	scénář
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Petera	Peter	k1gMnSc2	Peter
Morgana	morgan	k1gMnSc2	morgan
dopsali	dopsat	k5eAaPmAgMnP	dopsat
do	do	k7c2	do
finální	finální	k2eAgFnSc2d1	finální
verze	verze	k1gFnSc2	verze
Logan	Logana	k1gFnPc2	Logana
<g/>
,	,	kIx,	,
Purvis	Purvis	k1gFnPc2	Purvis
a	a	k8xC	a
Wade	Wade	k?	Wade
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
klapka	klapka	k1gFnSc1	klapka
padla	padnout	k5eAaImAgFnS	padnout
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
a	a	k8xC	a
natáčení	natáčení	k1gNnSc1	natáčení
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Charlese	Charles	k1gMnSc2	Charles
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
choti	choť	k1gFnSc2	choť
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Camilly	Camilla	k1gFnSc2	Camilla
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
Royal	Royal	k1gInSc1	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
premiéra	premiéra	k1gFnSc1	premiéra
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c6	v
kinosálech	kinosál	k1gInPc6	kinosál
s	s	k7c7	s
formátem	formát	k1gInSc7	formát
velkorozměrného	velkorozměrný	k2eAgInSc2d1	velkorozměrný
kinematografického	kinematografický	k2eAgInSc2d1	kinematografický
systému	systém	k1gInSc2	systém
IMAX	IMAX	kA	IMAX
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
film	film	k1gInSc1	film
nebyl	být	k5eNaImAgInS	být
snímán	snímat	k5eAaImNgInS	snímat
kamerami	kamera	k1gFnPc7	kamera
IMAX	IMAX	kA	IMAX
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
tržby	tržba	k1gFnPc1	tržba
výše	výše	k1gFnSc2	výše
jedné	jeden	k4xCgFnSc2	jeden
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
Skyfall	Skyfall	k1gInSc1	Skyfall
stal	stát	k5eAaPmAgInS	stát
čtrnáctým	čtrnáctý	k4xOgInSc7	čtrnáctý
filmem	film	k1gInSc7	film
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
překročil	překročit	k5eAaPmAgMnS	překročit
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
také	také	k9	také
druhým	druhý	k4xOgInSc7	druhý
nejvýdělečnějším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
snímkem	snímek	k1gInSc7	snímek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
sedmým	sedmý	k4xOgInSc7	sedmý
nejvýnosnějším	výnosný	k2eAgInSc7d3	nejvýnosnější
filmem	film	k1gInSc7	film
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
nejvýdělečnějším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
snímkem	snímek	k1gInSc7	snímek
vzešlým	vzešlý	k2eAgInSc7d1	vzešlý
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
i	i	k9	i
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
Sony	Sony	kA	Sony
Pictures	Pictures	k1gInSc1	Pictures
a	a	k8xC	a
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
prvenství	prvenství	k1gNnSc4	prvenství
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
také	také	k9	také
v	v	k7c6	v
bondovské	bondovský	k2eAgFnSc6d1	bondovská
sérii	série	k1gFnSc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Skyfall	Skyfall	k1gMnSc1	Skyfall
obdržel	obdržet	k5eAaPmAgMnS	obdržet
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
cen	cena	k1gFnPc2	cena
BAFTA	BAFTA	kA	BAFTA
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pěti	pět	k4xCc2	pět
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
proměnil	proměnit	k5eAaPmAgMnS	proměnit
na	na	k7c4	na
85	[number]	k4	85
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
udílení	udílení	k1gNnSc2	udílení
Cen	cena	k1gFnPc2	cena
Akademie	akademie	k1gFnSc2	akademie
dvě	dva	k4xCgFnPc4	dva
–	–	k?	–
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Skyfall	Skyfall	k1gInSc4	Skyfall
<g/>
"	"	kIx"	"
a	a	k8xC	a
střih	střih	k1gInSc4	střih
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
70	[number]	k4	70
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
udílení	udílení	k1gNnSc1	udílení
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
MI6	MI6	k1gFnSc2	MI6
James	James	k1gMnSc1	James
Bond	bond	k1gInSc1	bond
(	(	kIx(	(
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Craig	Craig	k1gMnSc1	Craig
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kolegyně	kolegyně	k1gFnSc1	kolegyně
Eve	Eve	k1gFnSc1	Eve
(	(	kIx(	(
<g/>
Naomie	Naomie	k1gFnSc1	Naomie
Harrisová	Harrisový	k2eAgFnSc1d1	Harrisová
<g/>
)	)	kIx)	)
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
v	v	k7c6	v
istanbulských	istanbulský	k2eAgFnPc6d1	Istanbulská
ulicích	ulice	k1gFnPc6	ulice
žoldáka	žoldák	k1gMnSc2	žoldák
Patriceho	Patrice	k1gMnSc2	Patrice
(	(	kIx(	(
<g/>
Ola	Ola	k1gFnSc1	Ola
Rapace	Rapace	k1gFnSc2	Rapace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
zašifrovaná	zašifrovaný	k2eAgNnPc1d1	zašifrované
počítačová	počítačový	k2eAgNnPc1d1	počítačové
data	datum	k1gNnPc1	datum
s	s	k7c7	s
identitou	identita	k1gFnSc7	identita
skrytých	skrytý	k2eAgInPc2d1	skrytý
agentů	agens	k1gInPc2	agens
NATO	NATO	kA	NATO
infiltrovaných	infiltrovaný	k2eAgInPc2d1	infiltrovaný
do	do	k7c2	do
teroristických	teroristický	k2eAgFnPc2d1	teroristická
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Patrice	patrice	k1gFnSc1	patrice
zraní	zranit	k5eAaPmIp3nS	zranit
Bonda	Bond	k1gMnSc4	Bond
do	do	k7c2	do
ramena	rameno	k1gNnSc2	rameno
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
honička	honička	k1gFnSc1	honička
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
kontaktním	kontaktní	k2eAgInSc7d1	kontaktní
bojem	boj	k1gInSc7	boj
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Eve	Eve	k?	Eve
omylem	omyl	k1gInSc7	omyl
agenta	agent	k1gMnSc2	agent
postřelí	postřelit	k5eAaPmIp3nS	postřelit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
padá	padat	k5eAaImIp3nS	padat
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
debaklu	debakl	k1gInSc6	debakl
operace	operace	k1gFnSc2	operace
si	se	k3xPyFc3	se
M	M	kA	M
(	(	kIx(	(
<g/>
Judi	Judi	k1gNnSc1	Judi
Denchová	Denchová	k1gFnSc1	Denchová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šéfku	šéfka	k1gFnSc4	šéfka
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
zavolá	zavolat	k5eAaPmIp3nS	zavolat
na	na	k7c4	na
kobereček	kobereček	k1gInSc4	kobereček
předseda	předseda	k1gMnSc1	předseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
Gareth	Garetha	k1gFnPc2	Garetha
Mallory	Mallora	k1gFnSc2	Mallora
(	(	kIx(	(
<g/>
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sdělí	sdělit	k5eAaPmIp3nP	sdělit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
M	M	kA	M
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
centrály	centrála	k1gFnSc2	centrála
a	a	k8xC	a
usmrcení	usmrcení	k1gNnSc6	usmrcení
několika	několik	k4yIc2	několik
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Velitelství	velitelství	k1gNnSc1	velitelství
se	se	k3xPyFc4	se
operativně	operativně	k6eAd1	operativně
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
časů	čas	k1gInPc2	čas
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
nových	nový	k2eAgFnPc6d1	nová
událostech	událost	k1gFnPc6	událost
a	a	k8xC	a
"	"	kIx"	"
<g/>
zmrtvýchvstalý	zmrtvýchvstalý	k2eAgMnSc1d1	zmrtvýchvstalý
<g/>
"	"	kIx"	"
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nesloží	složit	k5eNaPmIp3nP	složit
fyzické	fyzický	k2eAgFnPc1d1	fyzická
ani	ani	k8xC	ani
psychologické	psychologický	k2eAgInPc1d1	psychologický
testy	test	k1gInPc1	test
<g/>
,	,	kIx,	,
M	M	kA	M
jej	on	k3xPp3gMnSc4	on
přijímá	přijímat	k5eAaImIp3nS	přijímat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Střepina	střepina	k1gFnSc1	střepina
z	z	k7c2	z
istanbulské	istanbulský	k2eAgFnSc2d1	Istanbulská
přestřelky	přestřelka	k1gFnSc2	přestřelka
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
ramenu	rameno	k1gNnSc6	rameno
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vystopovat	vystopovat	k5eAaPmF	vystopovat
identitu	identita	k1gFnSc4	identita
Patriceho	Patrice	k1gMnSc2	Patrice
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
nájemný	nájemný	k2eAgInSc1d1	nájemný
zabiják	zabiják	k1gInSc1	zabiják
další	další	k2eAgFnSc4d1	další
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Agentovým	agentův	k2eAgInSc7d1	agentův
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zjistit	zjistit	k5eAaPmF	zjistit
jméno	jméno	k1gNnSc4	jméno
jeho	on	k3xPp3gMnSc2	on
zaměstnavatele	zaměstnavatel	k1gMnSc2	zaměstnavatel
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
ukradený	ukradený	k2eAgInSc4d1	ukradený
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
nájemný	nájemný	k2eAgMnSc1d1	nájemný
vrah	vrah	k1gMnSc1	vrah
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
,	,	kIx,	,
zápasí	zápasit	k5eAaImIp3nS	zápasit
v	v	k7c6	v
šanghajském	šanghajský	k2eAgInSc6d1	šanghajský
mrakodrapu	mrakodrap	k1gInSc6	mrakodrap
s	s	k7c7	s
Bondem	bond	k1gInSc7	bond
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
britskému	britský	k2eAgMnSc3d1	britský
agentovi	agent	k1gMnSc3	agent
podaří	podařit	k5eAaPmIp3nS	podařit
vytěžit	vytěžit	k5eAaPmF	vytěžit
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
Patrice	patrice	k1gFnPc4	patrice
padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
vybavení	vybavení	k1gNnSc2	vybavení
získává	získávat	k5eAaImIp3nS	získávat
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
odměna	odměna	k1gFnSc1	odměna
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
čeká	čekat	k5eAaImIp3nS	čekat
v	v	k7c6	v
macajském	macajský	k2eAgNnSc6d1	macajský
kasinu	kasino	k1gNnSc6	kasino
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c6	o
proplacení	proplacení	k1gNnSc6	proplacení
Patriceho	Patrice	k1gMnSc2	Patrice
"	"	kIx"	"
<g/>
žetonu	žeton	k1gInSc3	žeton
<g/>
"	"	kIx"	"
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
atraktivní	atraktivní	k2eAgFnSc7d1	atraktivní
Sévérine	Sévérin	k1gInSc5	Sévérin
(	(	kIx(	(
<g/>
Bérénice	Bérénice	k1gFnSc1	Bérénice
Marloheová	Marloheová	k1gFnSc1	Marloheová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
šanghajském	šanghajský	k2eAgInSc6d1	šanghajský
pokoji	pokoj	k1gInSc6	pokoj
stála	stát	k5eAaImAgFnS	stát
vedle	vedle	k7c2	vedle
cíle	cíl	k1gInSc2	cíl
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Naléhá	naléhat	k5eAaBmIp3nS	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gInSc4	on
dovedla	dovést	k5eAaPmAgFnS	dovést
za	za	k7c7	za
šéfem	šéf	k1gMnSc7	šéf
<g/>
,	,	kIx,	,
Raoulem	Raoul	k1gMnSc7	Raoul
Silvou	Silva	k1gFnSc7	Silva
(	(	kIx(	(
<g/>
Javier	Javier	k1gInSc1	Javier
Bardem	bard	k1gMnSc7	bard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dívky	dívka	k1gFnSc2	dívka
získává	získávat	k5eAaImIp3nS	získávat
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
hrozbou	hrozba	k1gFnSc7	hrozba
opodál	opodál	k6eAd1	opodál
stojících	stojící	k2eAgInPc2d1	stojící
bodygardů	bodygard	k1gInPc2	bodygard
a	a	k8xC	a
také	také	k9	také
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
připojit	připojit	k5eAaPmF	připojit
na	na	k7c6	na
jachtě	jachta	k1gFnSc6	jachta
plující	plující	k2eAgFnSc6d1	plující
za	za	k7c7	za
požadovaným	požadovaný	k2eAgInSc7d1	požadovaný
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Sévérine	Sévérin	k1gInSc5	Sévérin
se	s	k7c7	s
však	však	k9	však
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
domáhá	domáhat	k5eAaImIp3nS	domáhat
Silvovy	Silvův	k2eAgFnSc2d1	Silvova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
agent	agent	k1gMnSc1	agent
007	[number]	k4	007
v	v	k7c6	v
kasinu	kasino	k1gNnSc6	kasino
zlikviduje	zlikvidovat	k5eAaPmIp3nS	zlikvidovat
ochranku	ochranka	k1gFnSc4	ochranka
<g/>
,	,	kIx,	,
účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
cesty	cesta	k1gFnPc1	cesta
na	na	k7c4	na
přilehlý	přilehlý	k2eAgInSc4d1	přilehlý
opuštěný	opuštěný	k2eAgInSc4d1	opuštěný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zajati	zajat	k2eAgMnPc1d1	zajat
žoldáky	žoldák	k1gMnPc4	žoldák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gFnPc4	on
dovedou	dovést	k5eAaPmIp3nP	dovést
k	k	k7c3	k
Silvovi	Silva	k1gMnSc3	Silva
<g/>
,	,	kIx,	,
bývalému	bývalý	k2eAgMnSc3d1	bývalý
agentu	agent	k1gMnSc3	agent
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
M	M	kA	M
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
čínské	čínský	k2eAgFnSc6d1	čínská
misi	mise	k1gFnSc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Silva	Silva	k1gFnSc1	Silva
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
šéfka	šéfka	k1gFnSc1	šéfka
zradila	zradit	k5eAaPmAgFnS	zradit
a	a	k8xC	a
zaprodala	zaprodat	k5eAaPmAgFnS	zaprodat
<g/>
.	.	kIx.	.
</s>
<s>
Obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
kybernetický	kybernetický	k2eAgInSc4d1	kybernetický
terorismus	terorismus	k1gInSc4	terorismus
a	a	k8xC	a
naplánoval	naplánovat	k5eAaBmAgInS	naplánovat
výbuch	výbuch	k1gInSc1	výbuch
londýnské	londýnský	k2eAgFnSc2d1	londýnská
centrály	centrála	k1gFnSc2	centrála
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Sévérine	Sévérin	k1gInSc5	Sévérin
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gNnSc7	on
zastřelena	zastřelen	k2eAgFnSc1d1	zastřelena
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Bleskově	bleskově	k6eAd1	bleskově
zneškodňuje	zneškodňovat	k5eAaImIp3nS	zneškodňovat
ochranku	ochranka	k1gFnSc4	ochranka
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přivolanými	přivolaný	k2eAgFnPc7d1	přivolaná
posilami	posila	k1gFnPc7	posila
odváží	odvázat	k5eAaPmIp3nP	odvázat
Silvu	Silva	k1gFnSc4	Silva
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Q	Q	kA	Q
(	(	kIx(	(
<g/>
Ben	Ben	k1gInSc1	Ben
Whishaw	Whishaw	k1gFnSc2	Whishaw
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
Silvova	Silvův	k2eAgInSc2d1	Silvův
laptopu	laptop	k1gInSc2	laptop
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
teroristovi	terorista	k1gMnSc6	terorista
nevědomě	vědomě	k6eNd1	vědomě
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vlomit	vlomit	k5eAaPmF	vlomit
se	se	k3xPyFc4	se
do	do	k7c2	do
systému	systém	k1gInSc2	systém
MI6	MI6	k1gFnSc2	MI6
a	a	k8xC	a
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
cely	cela	k1gFnSc2	cela
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bývalý	bývalý	k2eAgMnSc1d1	bývalý
agent	agent	k1gMnSc1	agent
zajetí	zajetí	k1gNnSc2	zajetí
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
centrále	centrála	k1gFnSc6	centrála
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
plánu	plán	k1gInSc2	plán
na	na	k7c6	na
zavraždění	zavraždění	k1gNnSc6	zavraždění
M.	M.	kA	M.
Bond	bond	k1gInSc4	bond
jej	on	k3xPp3gMnSc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
do	do	k7c2	do
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
podzemí	podzemí	k1gNnSc2	podzemí
a	a	k8xC	a
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Silvovi	Silva	k1gMnSc3	Silva
se	se	k3xPyFc4	se
však	však	k9	však
daří	dařit	k5eAaImIp3nS	dařit
proniknout	proniknout	k5eAaPmF	proniknout
až	až	k9	až
do	do	k7c2	do
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
M	M	kA	M
právě	právě	k6eAd1	právě
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
–	–	k?	–
při	při	k7c6	při
veřejném	veřejný	k2eAgNnSc6d1	veřejné
slyšení	slyšení	k1gNnSc6	slyšení
–	–	k?	–
<g/>
,	,	kIx,	,
z	z	k7c2	z
odhalení	odhalení	k1gNnSc2	odhalení
identity	identita	k1gFnSc2	identita
agentů	agent	k1gMnPc2	agent
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
namísto	namísto	k7c2	namísto
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
Silvova	Silvův	k2eAgInSc2d1	Silvův
pokusu	pokus	k1gInSc2	pokus
zastřelit	zastřelit	k5eAaPmF	zastřelit
M	M	kA	M
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Eve	Eve	k1gMnSc2	Eve
a	a	k8xC	a
Malloryho	Mallory	k1gMnSc2	Mallory
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
hrozbu	hrozba	k1gFnSc4	hrozba
odvrátit	odvrátit	k5eAaPmF	odvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Šéfka	šéfka	k1gFnSc1	šéfka
MI6	MI6	k1gFnSc2	MI6
je	být	k5eAaImIp3nS	být
urychleně	urychleně	k6eAd1	urychleně
odvezena	odvezen	k2eAgFnSc1d1	odvezena
pobočníkem	pobočník	k1gMnSc7	pobočník
Billem	Bill	k1gMnSc7	Bill
Tannerem	Tanner	k1gMnSc7	Tanner
(	(	kIx(	(
<g/>
Rory	Rora	k1gMnSc2	Rora
Kinnear	Kinnear	k1gMnSc1	Kinnear
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
007	[number]	k4	007
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
poté	poté	k6eAd1	poté
cestuje	cestovat	k5eAaImIp3nS	cestovat
na	na	k7c4	na
opuštěné	opuštěný	k2eAgNnSc4d1	opuštěné
skotské	skotský	k2eAgNnSc4d1	skotské
sídlo	sídlo	k1gNnSc4	sídlo
Skyfall	Skyfalla	k1gFnPc2	Skyfalla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
osiření	osiření	k1gNnSc2	osiření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Malloryho	Mallory	k1gMnSc4	Mallory
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
Bond	bond	k1gInSc1	bond
žádá	žádat	k5eAaImIp3nS	žádat
Q	Q	kA	Q
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
jejich	jejich	k3xOp3gFnSc4	jejich
únikovou	únikový	k2eAgFnSc4d1	úniková
trasu	trasa	k1gFnSc4	trasa
Silvovi	Silva	k1gMnSc3	Silva
monitorovat	monitorovat	k5eAaImF	monitorovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	on	k3xPp3gMnPc4	on
bude	být	k5eAaImBp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vystopovat	vystopovat	k5eAaPmF	vystopovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
Skyfallu	Skyfall	k1gInSc6	Skyfall
se	se	k3xPyFc4	se
agent	agent	k1gMnSc1	agent
a	a	k8xC	a
M	M	kA	M
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	s	k7c7	s
starým	starý	k1gMnSc7	starý
správcem	správce	k1gMnSc7	správce
Kincadem	Kincad	k1gInSc7	Kincad
(	(	kIx(	(
<g/>
Albert	Albert	k1gMnSc1	Albert
Finney	Finnea	k1gFnSc2	Finnea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nekvalitně	kvalitně	k6eNd1	kvalitně
ozbrojeni	ozbrojit	k5eAaPmNgMnP	ozbrojit
<g/>
,	,	kIx,	,
nastraží	nastražit	k5eAaPmIp3nS	nastražit
řadu	řada	k1gFnSc4	řada
pastí	past	k1gFnPc2	past
<g/>
,	,	kIx,	,
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
příchod	příchod	k1gInSc4	příchod
Silvovy	Silvův	k2eAgFnSc2d1	Silvova
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kontaktu	kontakt	k1gInSc6	kontakt
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
intenzivním	intenzivní	k2eAgInPc3d1	intenzivní
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Postřelená	postřelený	k2eAgFnSc1d1	postřelená
M	M	kA	M
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
prchá	prchat	k5eAaImIp3nS	prchat
tajným	tajný	k2eAgInSc7d1	tajný
tunelem	tunel	k1gInSc7	tunel
do	do	k7c2	do
odlehlé	odlehlý	k2eAgFnSc2d1	odlehlá
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
útok	útok	k1gInSc1	útok
na	na	k7c4	na
dům	dům	k1gInSc4	dům
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
z	z	k7c2	z
vrtulníku	vrtulník	k1gInSc2	vrtulník
za	za	k7c4	za
pozemní	pozemní	k2eAgFnPc4d1	pozemní
podpory	podpora	k1gFnPc4	podpora
Silvových	Silvův	k2eAgInPc2d1	Silvův
granátů	granát	k1gInPc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
Bond	bond	k1gInSc1	bond
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
dynamitem	dynamit	k1gInSc7	dynamit
odpálit	odpálit	k5eAaPmF	odpálit
kanystry	kanystr	k1gInPc4	kanystr
plné	plný	k2eAgFnSc2d1	plná
paliva	palivo	k1gNnPc4	palivo
a	a	k8xC	a
utíká	utíkat	k5eAaImIp3nS	utíkat
do	do	k7c2	do
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pád	pád	k1gInSc4	pád
vrtulníku	vrtulník	k1gInSc2	vrtulník
<g/>
,	,	kIx,	,
destrukci	destrukce	k1gFnSc4	destrukce
domu	dům	k1gInSc2	dům
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
řady	řada	k1gFnSc2	řada
útočníků	útočník	k1gMnPc2	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
živý	živý	k2eAgMnSc1d1	živý
Silva	Silva	k1gFnSc1	Silva
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
Kincadovu	Kincadův	k2eAgFnSc4d1	Kincadův
baterku	baterka	k1gFnSc4	baterka
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c7	za
jejím	její	k3xOp3gNnSc7	její
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
zastraší	zastrašit	k5eAaPmIp3nS	zastrašit
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
M	M	kA	M
pistoli	pistole	k1gFnSc4	pistole
a	a	k8xC	a
přikládá	přikládat	k5eAaImIp3nS	přikládat
svou	svůj	k3xOyFgFnSc4	svůj
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
oba	dva	k4xCgMnPc1	dva
jednou	jeden	k4xCgFnSc7	jeden
ranou	rána	k1gFnSc7	rána
zabila	zabít	k5eAaPmAgFnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Bondovi	Bonda	k1gMnSc3	Bonda
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
zamrzlého	zamrzlý	k2eAgNnSc2d1	zamrzlé
jezera	jezero	k1gNnSc2	jezero
podaří	podařit	k5eAaPmIp3nS	podařit
přemoci	přemoct	k5eAaPmF	přemoct
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
útočníků	útočník	k1gMnPc2	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kaple	kaple	k1gFnSc2	kaple
dorazí	dorazit	k5eAaPmIp3nP	dorazit
včas	včas	k6eAd1	včas
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
probodl	probodnout	k5eAaPmAgMnS	probodnout
Silvu	Silva	k1gFnSc4	Silva
<g/>
.	.	kIx.	.
</s>
<s>
M	M	kA	M
však	však	k9	však
také	také	k9	také
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
postřelení	postřelení	k1gNnSc2	postřelení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smutečním	smuteční	k2eAgInSc6d1	smuteční
obřadu	obřad	k1gInSc6	obřad
M	M	kA	M
se	se	k3xPyFc4	se
Bond	bond	k1gInSc1	bond
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
nového	nový	k2eAgMnSc2d1	nový
šéfa	šéf	k1gMnSc2	šéf
MI6	MI6	k1gMnSc2	MI6
Malloryho	Mallory	k1gMnSc2	Mallory
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
tituluje	titulovat	k5eAaImIp3nS	titulovat
"	"	kIx"	"
<g/>
M	M	kA	M
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předpokoji	předpokoj	k1gInSc6	předpokoj
sedí	sedit	k5eAaImIp3nS	sedit
agentka	agentka	k1gFnSc1	agentka
Eve	Eve	k1gFnSc1	Eve
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
boku	bok	k1gInSc6	bok
bojoval	bojovat	k5eAaImAgInS	bojovat
v	v	k7c6	v
istanbulských	istanbulský	k2eAgFnPc6d1	Istanbulská
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
agentovi	agent	k1gMnSc3	agent
007	[number]	k4	007
formálně	formálně	k6eAd1	formálně
představuje	představovat	k5eAaImIp3nS	představovat
jako	jako	k9	jako
Eve	Eve	k1gFnSc1	Eve
Moneypenny	Moneypenna	k1gFnSc2	Moneypenna
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
sekretářka	sekretářka	k1gFnSc1	sekretářka
šéfa	šéf	k1gMnSc2	šéf
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kancelář	kancelář	k1gFnSc1	kancelář
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
za	za	k7c4	za
nasazení	nasazení	k1gNnSc4	nasazení
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Obsazení	obsazení	k1gNnSc1	obsazení
bondovky	bondovky	k?	bondovky
Skyfall	Skyfalla	k1gFnPc2	Skyfalla
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Corinthia	Corinthia	k1gFnSc1	Corinthia
Hotelu	hotel	k1gInSc3	hotel
<g/>
,	,	kIx,	,
na	na	k7c4	na
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
padesát	padesát	k4xCc1	padesát
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sean	Sean	k1gInSc4	Sean
Connery	Conner	k1gMnPc7	Conner
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
agenta	agent	k1gMnSc2	agent
007	[number]	k4	007
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
No	no	k9	no
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Craig	Craig	k1gMnSc1	Craig
–	–	k?	–
James	James	k1gInSc1	James
Bond	bond	k1gInSc1	bond
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
007	[number]	k4	007
s	s	k7c7	s
povolením	povolení	k1gNnSc7	povolení
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Sam	Sam	k1gMnSc1	Sam
Mendes	Mendes	k1gMnSc1	Mendes
postavu	postava	k1gFnSc4	postava
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
zkušenost	zkušenost	k1gFnSc1	zkušenost
únavy	únava	k1gFnSc2	únava
<g/>
,	,	kIx,	,
nudy	nuda	k1gFnSc2	nuda
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc2	deprese
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
nesnází	nesnáz	k1gFnSc7	nesnáz
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Javier	Javier	k1gMnSc1	Javier
Bardem	bard	k1gMnSc7	bard
–	–	k?	–
Raoul	Raoul	k1gInSc1	Raoul
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Tiago	Tiago	k1gMnSc1	Tiago
Rodriguez	Rodriguez	k1gMnSc1	Rodriguez
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
záporný	záporný	k2eAgMnSc1d1	záporný
Bondův	Bondův	k2eAgMnSc1d1	Bondův
protihráč	protihráč	k1gMnSc1	protihráč
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
agent	agent	k1gMnSc1	agent
MI6	MI6	k1gMnSc1	MI6
a	a	k8xC	a
podřízený	podřízený	k1gMnSc1	podřízený
M	M	kA	M
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
kybernetického	kybernetický	k2eAgInSc2d1	kybernetický
terorismu	terorismus	k1gInSc2	terorismus
a	a	k8xC	a
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
pomstít	pomstít	k5eAaPmF	pomstít
se	se	k3xPyFc4	se
M.	M.	kA	M.
Bardem	bard	k1gMnSc7	bard
postavu	postav	k1gInSc2	postav
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
záporňák	záporňák	k1gMnSc1	záporňák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Craig	Craig	k1gMnSc1	Craig
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bond	bond	k1gInSc1	bond
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
naváže	navázat	k5eAaPmIp3nS	navázat
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
vztah	vztah	k1gInSc1	vztah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Judi	Judi	k6eAd1	Judi
Denchová	Denchový	k2eAgFnSc1d1	Denchová
−	−	k?	−
M	M	kA	M
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
MI6	MI6	k1gFnSc1	MI6
a	a	k8xC	a
Bondova	Bondův	k2eAgFnSc1d1	Bondova
řídící	řídící	k2eAgFnSc1d1	řídící
důstojnice	důstojnice	k1gFnSc1	důstojnice
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
hrála	hrát	k5eAaImAgFnS	hrát
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
posedmé	posedmé	k4xO	posedmé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
–	–	k?	–
Mallory	Mallora	k1gFnSc2	Mallora
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgMnSc1d1	vládní
agent	agent	k1gMnSc1	agent
s	s	k7c7	s
oprávněním	oprávnění	k1gNnSc7	oprávnění
úkolovat	úkolovat	k5eAaImF	úkolovat
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Finney	Finnea	k1gFnSc2	Finnea
–	–	k?	–
Kincade	Kincad	k1gInSc5	Kincad
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc4	správce
Skyfallu	Skyfalla	k1gMnSc4	Skyfalla
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
krátce	krátce	k6eAd1	krátce
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
role	role	k1gFnSc2	role
obsadit	obsadit	k5eAaPmF	obsadit
Seana	Seaen	k2eAgMnSc4d1	Seaen
Conneryho	Connery	k1gMnSc4	Connery
<g/>
.	.	kIx.	.
</s>
<s>
Naomie	Naomie	k1gFnSc1	Naomie
Harrisová	Harrisový	k2eAgFnSc1d1	Harrisová
–	–	k?	–
Eve	Eve	k1gFnSc1	Eve
Moneypenny	Moneypenna	k1gFnSc2	Moneypenna
<g/>
..	..	k?	..
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
herečka	herečka	k1gFnSc1	herečka
bude	být	k5eAaImBp3nS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Eve	Eve	k1gFnSc2	Eve
<g/>
,	,	kIx,	,
agentky	agentka	k1gFnSc2	agentka
MI6	MI6	k1gFnSc2	MI6
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
úzce	úzko	k6eAd1	úzko
spolupracující	spolupracující	k2eAgMnPc1d1	spolupracující
s	s	k7c7	s
Bondem	bond	k1gInSc7	bond
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
probíhajícím	probíhající	k2eAgFnPc3d1	probíhající
neověřeným	ověřený	k2eNgFnPc3d1	neověřená
zprávám	zpráva	k1gFnPc3	zpráva
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
Harrisová	Harrisový	k2eAgFnSc1d1	Harrisová
hrát	hrát	k5eAaImF	hrát
postavu	postava	k1gFnSc4	postava
slečny	slečna	k1gFnSc2	slečna
Moneypenny	Moneypenna	k1gFnSc2	Moneypenna
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
produkčního	produkční	k2eAgInSc2d1	produkční
týmu	tým	k1gInSc2	tým
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
Eve	Eve	k1gFnSc1	Eve
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
věří	věřit	k5eAaImIp3nS	věřit
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
Bond	bond	k1gInSc1	bond
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
žačkou	žačka	k1gFnSc7	žačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rory	Rory	k1gInPc1	Rory
Kinnear	Kinneara	k1gFnPc2	Kinneara
–	–	k?	–
Bill	Bill	k1gMnSc1	Bill
Tanner	Tanner	k1gMnSc1	Tanner
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
štábu	štáb	k1gInSc2	štáb
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
pobočník	pobočník	k1gMnSc1	pobočník
M.	M.	kA	M.
Bérénice	Bérénice	k1gFnSc1	Bérénice
Marloheová	Marloheová	k1gFnSc1	Marloheová
–	–	k?	–
Sévérine	Sévérin	k1gInSc5	Sévérin
<g/>
,	,	kIx,	,
Bond	bond	k1gInSc4	bond
girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Silva	Silva	k1gFnSc1	Silva
zachránil	zachránit	k5eAaPmAgMnS	zachránit
z	z	k7c2	z
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
masem	maso	k1gNnSc7	maso
v	v	k7c6	v
Macau	Macao	k1gNnSc6	Macao
a	a	k8xC	a
poté	poté	k6eAd1	poté
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k8xC	jako
prostředník	prostředník	k1gInSc4	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pouhé	pouhý	k2eAgFnSc6d1	pouhá
zmínce	zmínka	k1gFnSc6	zmínka
Silvova	Silvův	k2eAgNnSc2d1	Silvovo
jména	jméno	k1gNnSc2	jméno
byla	být	k5eAaImAgFnS	být
vystrašená	vystrašený	k2eAgFnSc1d1	vystrašená
a	a	k8xC	a
projevila	projevit	k5eAaPmAgFnS	projevit
touhu	touha	k1gFnSc4	touha
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
charakterizovala	charakterizovat	k5eAaBmAgFnS	charakterizovat
postavu	postava	k1gFnSc4	postava
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
okouzlující	okouzlující	k2eAgFnSc4d1	okouzlující
a	a	k8xC	a
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
čerpala	čerpat	k5eAaImAgFnS	čerpat
od	od	k7c2	od
Xenie	Xenie	k1gFnSc2	Xenie
Onatoppuové	Onatoppuová	k1gFnSc2	Onatoppuová
(	(	kIx(	(
<g/>
Famke	Famke	k1gFnSc1	Famke
Janssenová	Janssenová	k1gFnSc1	Janssenová
<g/>
)	)	kIx)	)
–	–	k?	–
záporné	záporný	k2eAgInPc4d1	záporný
Bond	bond	k1gInSc4	bond
girl	girl	k1gFnSc2	girl
z	z	k7c2	z
filmu	film	k1gInSc2	film
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Helen	Helena	k1gFnPc2	Helena
McCroryová	McCroryová	k1gFnSc1	McCroryová
–	–	k?	–
Clair	Clair	k1gMnSc1	Clair
Dowar	Dowar	k1gMnSc1	Dowar
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
M.	M.	kA	M.
Sam	Sam	k1gMnSc1	Sam
Mendes	Mendes	k1gMnSc1	Mendes
ji	on	k3xPp3gFnSc4	on
osobně	osobně	k6eAd1	osobně
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Ola	Ola	k1gFnSc1	Ola
Rapace	Rapace	k1gFnSc2	Rapace
–	–	k?	–
Patrice	patrice	k1gFnSc2	patrice
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
žoldnéř	žoldnéř	k1gMnSc1	žoldnéř
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc1	osobnost
"	"	kIx"	"
<g/>
jen	jen	k9	jen
s	s	k7c7	s
pár	pár	k4xCyI	pár
slovy	slovo	k1gNnPc7	slovo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
milující	milující	k2eAgNnSc1d1	milující
násilí	násilí	k1gNnSc1	násilí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Whishaw	Whishaw	k1gFnSc2	Whishaw
–	–	k?	–
Q.	Q.	kA	Q.
Postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
absentovala	absentovat	k5eAaImAgFnS	absentovat
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
dvou	dva	k4xCgInPc6	dva
filmech	film	k1gInPc6	film
série	série	k1gFnSc2	série
Casino	Casino	k1gNnSc1	Casino
Royale	Royala	k1gFnSc3	Royala
a	a	k8xC	a
Quantum	Quantum	k1gNnSc4	Quantum
of	of	k?	of
Solace	Solace	k1gFnSc1	Solace
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Woodeson	Woodeson	k1gMnSc1	Woodeson
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Buckhurst	Buckhurst	k1gMnSc1	Buckhurst
–	–	k?	–
Ronson	Ronson	k1gMnSc1	Ronson
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
MI6	MI6	k1gMnSc1	MI6
smrtelně	smrtelně	k6eAd1	smrtelně
zraněný	zraněný	k2eAgInSc4d1	zraněný
Patricem	Patric	k1gMnSc7	Patric
<g/>
.	.	kIx.	.
</s>
<s>
Elize	elize	k1gFnSc1	elize
du	du	k?	du
Toitová	Toitový	k2eAgFnSc1d1	Toitový
–	–	k?	–
Vanessa	Vanessa	k1gFnSc1	Vanessa
<g/>
,	,	kIx,	,
asistentka	asistentka	k1gFnSc1	asistentka
M.	M.	kA	M.
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
Jens	Jens	k1gInSc1	Jens
Hultén	Hultén	k1gInSc1	Hultén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
komplice	komplic	k1gMnSc4	komplic
postavy	postava	k1gFnSc2	postava
Javiera	Javier	k1gMnSc4	Javier
Bardema	Bardem	k1gMnSc4	Bardem
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Loyd	Loyd	k1gMnSc1	Loyd
Holmes	Holmes	k1gMnSc1	Holmes
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
bondovce	bondovce	k?	bondovce
roli	role	k1gFnSc6	role
agenta	agent	k1gMnSc2	agent
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
modelka	modelka	k1gFnSc1	modelka
Tonia	Tonia	k1gFnSc1	Tonia
Sotiropoulouová	Sotiropoulouová	k1gFnSc1	Sotiropoulouová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
postavu	postava	k1gFnSc4	postava
Sévérine	Sévérin	k1gInSc5	Sévérin
sdělila	sdělit	k5eAaPmAgNnP	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zahraje	zahrát	k5eAaPmIp3nS	zahrát
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
Skyfall	Skyfalla	k1gFnPc2	Skyfalla
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pozdržena	pozdržet	k5eAaPmNgFnS	pozdržet
pro	pro	k7c4	pro
finanční	finanční	k2eAgFnPc4d1	finanční
potíže	potíž	k1gFnPc4	potíž
společnosti	společnost	k1gFnSc2	společnost
Metro-Goldwyn-Mayer	Metro-Goldwyn-Mayra	k1gFnPc2	Metro-Goldwyn-Mayra
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc4	problém
přesto	přesto	k8xC	přesto
vyústily	vyústit	k5eAaPmAgFnP	vyústit
do	do	k7c2	do
předprodukčních	předprodukční	k2eAgFnPc2d1	předprodukční
prací	práce	k1gFnPc2	práce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
odvrácen	odvrácen	k2eAgInSc1d1	odvrácen
bankrot	bankrot	k1gInSc1	bankrot
MGM	MGM	kA	MGM
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
Broccoliových	Broccoliový	k2eAgFnPc2d1	Broccoliová
k	k	k7c3	k
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
oznámení	oznámení	k1gNnSc3	oznámení
premiéry	premiéra	k1gFnSc2	premiéra
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
produkce	produkce	k1gFnSc2	produkce
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oznámení	oznámení	k1gNnSc3	oznámení
nového	nový	k2eAgInSc2d1	nový
termínu	termín	k1gInSc2	termín
britské	britský	k2eAgFnSc2d1	britská
a	a	k8xC	a
irské	irský	k2eAgFnSc2d1	irská
premiéry	premiéra	k1gFnSc2	premiéra
již	již	k9	již
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedení	uvedení	k1gNnSc1	uvedení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozpočet	rozpočet	k1gInSc1	rozpočet
projektu	projekt	k1gInSc2	projekt
činil	činit	k5eAaImAgInS	činit
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
miliónů	milión	k4xCgInPc2	milión
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
vůči	vůči	k7c3	vůči
předchozí	předchozí	k2eAgFnSc3d1	předchozí
bondovce	bondovce	k?	bondovce
Quantum	Quantum	k1gNnSc4	Quantum
of	of	k?	of
Solace	Solace	k1gFnSc1	Solace
pokles	pokles	k1gInSc4	pokles
o	o	k7c4	o
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
celoročních	celoroční	k2eAgFnPc2d1	celoroční
oslav	oslava	k1gFnPc2	oslava
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
premiéry	premiéra	k1gFnSc2	premiéra
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
No	no	k9	no
a	a	k9	a
celé	celý	k2eAgFnSc2d1	celá
filmové	filmový	k2eAgFnSc2d1	filmová
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
producenta	producent	k1gMnSc2	producent
Michaela	Michael	k1gMnSc2	Michael
G.	G.	kA	G.
Wilsona	Wilson	k1gMnSc2	Wilson
byl	být	k5eAaImAgInS	být
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
štáb	štáb	k1gInSc1	štáb
připraven	připraven	k2eAgInSc1d1	připraven
doprovázet	doprovázet	k5eAaImF	doprovázet
produkci	produkce	k1gFnSc4	produkce
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oslav	oslava	k1gFnPc2	oslava
výročí	výročí	k1gNnSc4	výročí
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
vznik	vznik	k1gInSc1	vznik
bondovky	bondovky	k?	bondovky
<g/>
.	.	kIx.	.
</s>
<s>
Skyfall	Skyfall	k1gInSc1	Skyfall
také	také	k9	také
představuje	představovat	k5eAaImIp3nS	představovat
první	první	k4xOgInSc4	první
bondovku	bondovku	k?	bondovku
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
promítána	promítat	k5eAaImNgFnS	promítat
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
IMAX	IMAX	kA	IMAX
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
několik	několik	k4yIc4	několik
zpravodajských	zpravodajský	k2eAgInPc2d1	zpravodajský
serverů	server	k1gInPc2	server
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
neověřenou	ověřený	k2eNgFnSc4d1	neověřená
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
srbském	srbský	k2eAgInSc6d1	srbský
deníku	deník	k1gInSc6	deník
Blic	Blic	k1gFnSc4	Blic
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
23	[number]	k4	23
<g/>
.	.	kIx.	.
bondovka	bondovka	k?	bondovka
nést	nést	k5eAaImF	nést
název	název	k1gInSc4	název
Carte	Cart	k1gInSc5	Cart
Blanche	Blanch	k1gInPc4	Blanch
a	a	k8xC	a
představovat	představovat	k5eAaImF	představovat
adaptaci	adaptace	k1gFnSc4	adaptace
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
od	od	k7c2	od
Jefferyho	Jeffery	k1gMnSc2	Jeffery
Deavera	Deaver	k1gMnSc2	Deaver
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
pak	pak	k6eAd1	pak
tuto	tento	k3xDgFnSc4	tento
spekulaci	spekulace	k1gFnSc4	spekulace
oficiálně	oficiálně	k6eAd1	oficiálně
<g />
.	.	kIx.	.
</s>
<s>
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Eon	Eon	k1gFnSc1	Eon
Productions	Productions	k1gInSc1	Productions
sdělením	sdělení	k1gNnSc7	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
film	film	k1gInSc1	film
nebude	být	k5eNaImBp3nS	být
jmenovat	jmenovat	k5eAaBmF	jmenovat
"	"	kIx"	"
<g/>
Carte	Cart	k1gMnSc5	Cart
Blanche	Blanchus	k1gMnSc5	Blanchus
a	a	k8xC	a
neponese	nést	k5eNaImIp3nS	nést
žádnou	žádný	k3yNgFnSc4	žádný
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
Deaverovou	Deaverův	k2eAgFnSc7d1	Deaverova
knihou	kniha	k1gFnSc7	kniha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
registrace	registrace	k1gFnSc1	registrace
patnácti	patnáct	k4xCc2	patnáct
domén	doména	k1gFnPc2	doména
včetně	včetně	k7c2	včetně
'	'	kIx"	'
<g/>
jamesbond-skyfall	jamesbondkyfalla	k1gFnPc2	jamesbond-skyfalla
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
skyfallthefilm	skyfallthefilm	k1gInSc1	skyfallthefilm
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
'	'	kIx"	'
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
MGM	MGM	kA	MGM
a	a	k8xC	a
Sony	Sony	kA	Sony
Pictures	Pictures	k1gInSc1	Pictures
u	u	k7c2	u
registrátora	registrátor	k1gMnSc2	registrátor
MarkMonitor	MarkMonitor	k1gInSc4	MarkMonitor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
vedl	vést	k5eAaImAgInS	vést
masmédia	masmédium	k1gNnPc4	masmédium
k	k	k7c3	k
předpokladu	předpoklad	k1gInSc3	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
snímek	snímek	k1gInSc1	snímek
ponese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Skyfall	Skyfall	k1gInSc1	Skyfall
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
chvíli	chvíle	k1gFnSc4	chvíle
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
Eon	Eon	k1gFnPc1	Eon
Productions	Productions	k1gInSc1	Productions
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
ani	ani	k8xC	ani
MGM	MGM	kA	MGM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
se	se	k3xPyFc4	se
režisér	režisér	k1gMnSc1	režisér
Sam	Sam	k1gMnSc1	Sam
Mendes	Mendes	k1gMnSc1	Mendes
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
Broccoliová	Broccoliový	k2eAgFnSc1d1	Broccoliová
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
prohlídku	prohlídka	k1gFnSc4	prohlídka
lokací	lokace	k1gFnPc2	lokace
do	do	k7c2	do
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
předprodukčních	předprodukční	k2eAgFnPc2d1	předprodukční
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
natáčení	natáčení	k1gNnSc1	natáčení
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
scény	scéna	k1gFnSc2	scéna
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
novodillíjském	novodillíjský	k2eAgInSc6d1	novodillíjský
obvodu	obvod	k1gInSc6	obvod
Sarojini	Sarojin	k1gMnPc1	Sarojin
Nagar	Nagara	k1gFnPc2	Nagara
a	a	k8xC	a
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Goa	Goa	k1gFnSc1	Goa
<g/>
–	–	k?	–
<g/>
Ahmadábád	Ahmadábáda	k1gFnPc2	Ahmadábáda
<g/>
.	.	kIx.	.
</s>
<s>
Štáb	štáb	k1gInSc4	štáb
přesto	přesto	k8xC	přesto
provázely	provázet	k5eAaImAgInP	provázet
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
povolení	povolení	k1gNnSc4	povolení
do	do	k7c2	do
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
částí	část	k1gFnPc2	část
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
Konkan	Konkana	k1gFnPc2	Konkana
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obdobnými	obdobný	k2eAgInPc7d1	obdobný
problémy	problém	k1gInPc7	problém
se	se	k3xPyFc4	se
potýkaly	potýkat	k5eAaImAgFnP	potýkat
produkce	produkce	k1gFnPc1	produkce
natáčející	natáčející	k2eAgFnSc2d1	natáčející
snímky	snímka	k1gFnSc2	snímka
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
povstal	povstat	k5eAaPmAgMnS	povstat
a	a	k8xC	a
Mission	Mission	k1gInSc4	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
–	–	k?	–
Ghost	Ghost	k1gInSc1	Ghost
Protocol	Protocol	k1gInSc1	Protocol
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc1	povolení
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
uděleno	udělit	k5eAaPmNgNnS	udělit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
indických	indický	k2eAgInPc2d1	indický
orgánů	orgán	k1gInPc2	orgán
s	s	k7c7	s
několika	několik	k4yIc7	několik
omezeními	omezení	k1gNnPc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
stanoviska	stanovisko	k1gNnSc2	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
produkční	produkční	k2eAgInSc1d1	produkční
štáb	štáb	k1gInSc1	štáb
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
na	na	k7c6	na
indických	indický	k2eAgFnPc6d1	indická
lokacích	lokace	k1gFnPc6	lokace
nenatáčet	natáčet	k5eNaImF	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
uváděla	uvádět	k5eAaImAgFnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
natáčet	natáčet	k5eAaImF	natáčet
na	na	k7c6	na
istanbulském	istanbulský	k2eAgInSc6d1	istanbulský
Hipodromu	hipodrom	k1gInSc6	hipodrom
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
Sultanahmet	Sultanahmeta	k1gFnPc2	Sultanahmeta
Meydanı	Meydanı	k1gFnPc2	Meydanı
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Boží	boží	k2eAgFnSc2d1	boží
Moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Fethiye	Fethiy	k1gInSc2	Fethiy
a	a	k8xC	a
na	na	k7c6	na
železničním	železniční	k2eAgInSc6d1	železniční
viaduktu	viadukt	k1gInSc6	viadukt
Varda	Vardo	k1gNnSc2	Vardo
poblíž	poblíž	k7c2	poblíž
Adany	Adana	k1gFnSc2	Adana
<g/>
,,	,,	k?	,,
když	když	k8xS	když
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
doba	doba	k1gFnSc1	doba
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
tureckých	turecký	k2eAgFnPc6d1	turecká
lokacích	lokace	k1gFnPc6	lokace
měla	mít	k5eAaImAgFnS	mít
činit	činit	k5eAaImF	činit
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
datovaná	datovaný	k2eAgFnSc1d1	datovaná
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
uváděla	uvádět	k5eAaImAgFnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
scény	scéna	k1gFnPc1	scéna
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
nasnímány	nasnímat	k5eAaPmNgInP	nasnímat
na	na	k7c6	na
Hašimě	Hašima	k1gFnSc6	Hašima
–	–	k?	–
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
ostrovu	ostrov	k1gInSc3	ostrov
blízko	blízko	k7c2	blízko
pobřeží	pobřeží	k1gNnSc2	pobřeží
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Mendes	Mendes	k1gMnSc1	Mendes
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
bondovce	bondovce	k?	bondovce
objeví	objevit	k5eAaPmIp3nS	objevit
také	také	k9	také
území	území	k1gNnSc4	území
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
když	když	k8xS	když
část	část	k1gFnSc1	část
děje	děj	k1gInSc2	děj
bude	být	k5eAaImBp3nS	být
nasnímána	nasnímat	k5eAaPmNgFnS	nasnímat
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
filmu	film	k1gInSc2	film
také	také	k9	také
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Duntrune	Duntrun	k1gInSc5	Duntrun
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgMnSc7d1	ležící
ve	v	k7c6	v
středozápadním	středozápadní	k2eAgInSc6d1	středozápadní
skotském	skotský	k2eAgInSc6d1	skotský
regionu	region	k1gInSc6	region
Argyll	Argylla	k1gFnPc2	Argylla
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
natáčení	natáčení	k1gNnSc2	natáčení
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
samotného	samotný	k2eAgNnSc2d1	samotné
filmování	filmování	k1gNnSc2	filmování
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
na	na	k7c4	na
133	[number]	k4	133
natáčecích	natáčecí	k2eAgInPc2d1	natáčecí
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
došlo	dojít	k5eAaPmAgNnS	dojít
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
na	na	k7c6	na
lokacích	lokace	k1gFnPc6	lokace
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Scény	scéna	k1gFnPc1	scéna
zachycovaly	zachycovat	k5eAaImAgFnP	zachycovat
Southwark	Southwark	k1gInSc4	Southwark
a	a	k8xC	a
Whitehall	Whitehall	k1gInSc4	Whitehall
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
smithfieldský	smithfieldský	k2eAgInSc4d1	smithfieldský
masový	masový	k2eAgInSc4d1	masový
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
nemocnici	nemocnice	k1gFnSc4	nemocnice
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
Canary	Canara	k1gFnSc2	Canara
Wharf	Wharf	k1gMnSc1	Wharf
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
energetiky	energetika	k1gFnSc2	energetika
a	a	k8xC	a
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
stanici	stanice	k1gFnSc6	stanice
metra	metro	k1gNnSc2	metro
Charing	Charing	k1gInSc4	Charing
Cross	Crossa	k1gFnPc2	Crossa
a	a	k8xC	a
greenwichskou	greenwichský	k2eAgFnSc4d1	greenwichská
kolej	kolej	k1gFnSc4	kolej
Old	Olda	k1gFnPc2	Olda
Royal	Royal	k1gInSc4	Royal
Naval	navalit	k5eAaPmRp2nS	navalit
College	Collegus	k1gMnSc5	Collegus
<g/>
..	..	k?	..
Dalšími	další	k2eAgFnPc7d1	další
lokacemi	lokace	k1gFnPc7	lokace
bondovky	bondovky	k?	bondovky
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
Cadogan	Cadogana	k1gFnPc2	Cadogana
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Tower	Tower	k1gMnSc1	Tower
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
Parliament	Parliament	k1gMnSc1	Parliament
Square	square	k1gInSc1	square
<g/>
.	.	kIx.	.
</s>
<s>
Vauxhall	Vauxhall	k1gInSc1	Vauxhall
Bridge	Bridge	k1gNnSc2	Bridge
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
uzavřen	uzavřen	k2eAgMnSc1d1	uzavřen
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
následek	následek	k1gInSc4	následek
filmového	filmový	k2eAgInSc2d1	filmový
výbuchu	výbuch	k1gInSc2	výbuch
v	v	k7c6	v
centrále	centrála	k1gFnSc6	centrála
MI6	MI6	k1gFnSc2	MI6
na	na	k7c4	na
Vauxhall	Vauxhall	k1gInSc4	Vauxhall
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bondovky	bondovky	k?	bondovky
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
explozi	exploze	k1gFnSc3	exploze
této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
–	–	k?	–
nasnímané	nasnímaný	k2eAgNnSc1d1	nasnímané
na	na	k7c6	na
velkorozměrné	velkorozměrný	k2eAgFnSc6d1	velkorozměrná
replice	replika	k1gFnSc6	replika
<g/>
,	,	kIx,	,
výbuch	výbuch	k1gInSc1	výbuch
ve	v	k7c6	v
Skyfallu	Skyfallo	k1gNnSc6	Skyfallo
bude	být	k5eAaImBp3nS	být
do	do	k7c2	do
filmu	film	k1gInSc2	film
digitálně	digitálně	k6eAd1	digitálně
doplněn	doplnit	k5eAaPmNgInS	doplnit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postprodukce	postprodukce	k1gFnSc2	postprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
fotografií	fotografia	k1gFnPc2	fotografia
odhalila	odhalit	k5eAaPmAgFnS	odhalit
přítomnost	přítomnost	k1gFnSc1	přítomnost
vozu	vůz	k1gInSc2	vůz
Aston	Astona	k1gFnPc2	Astona
Martin	Martin	k1gMnSc1	Martin
DB5	DB5	k1gMnSc1	DB5
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
natáčení	natáčení	k1gNnSc2	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
štáb	štáb	k1gInSc1	štáb
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
práce	práce	k1gFnPc1	práce
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tureckých	turecký	k2eAgMnPc2d1	turecký
teenagerú	teenagerú	k?	teenagerú
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
filmování	filmování	k1gNnSc2	filmování
na	na	k7c6	na
kolejišti	kolejiště	k1gNnSc6	kolejiště
v	v	k7c6	v
Adaně	Adana	k1gFnSc6	Adana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhal	probíhat	k5eAaImAgInS	probíhat
nácvik	nácvik	k1gInSc1	nácvik
bojových	bojový	k2eAgFnPc2d1	bojová
scén	scéna	k1gFnPc2	scéna
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
Istanbulu	Istanbul	k1gInSc2	Istanbul
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
dubnového	dubnový	k2eAgNnSc2d1	dubnové
filmování	filmování	k1gNnSc2	filmování
scén	scéna	k1gFnPc2	scéna
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Filmaři	filmař	k1gMnPc1	filmař
natáčeli	natáčet	k5eAaImAgMnP	natáčet
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
lokacích	lokace	k1gFnPc6	lokace
<g/>
:	:	kIx,	:
Egyptský	egyptský	k2eAgInSc4d1	egyptský
bazar	bazar	k1gInSc4	bazar
(	(	kIx(	(
<g/>
Mı	Mı	k1gMnSc5	Mı
Çarşı	Çarşı	k1gMnSc5	Çarşı
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yeni	Yene	k1gFnSc4	Yene
Camii	Camie	k1gFnSc4	Camie
<g/>
,	,	kIx,	,
Imperial	Imperial	k1gInSc4	Imperial
Post	posta	k1gFnPc2	posta
Office	Office	kA	Office
<g/>
,	,	kIx,	,
Hipodrom	hipodrom	k1gInSc4	hipodrom
a	a	k8xC	a
Velký	velký	k2eAgInSc4d1	velký
bazar	bazar	k1gInSc4	bazar
<g/>
.	.	kIx.	.
</s>
<s>
Majitelům	majitel	k1gMnPc3	majitel
dočasně	dočasně	k6eAd1	dočasně
zavřených	zavřený	k2eAgInPc2d1	zavřený
obchodů	obchod	k1gInPc2	obchod
byla	být	k5eAaImAgFnS	být
vyplacena	vyplatit	k5eAaPmNgFnS	vyplatit
kompenzace	kompenzace	k1gFnSc1	kompenzace
418	[number]	k4	418
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
poškození	poškození	k1gNnSc4	poškození
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
motocyklové	motocyklový	k2eAgFnSc2d1	motocyklová
honičky	honička	k1gFnSc2	honička
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
G.	G.	kA	G.
Wilson	Wilson	k1gMnSc1	Wilson
tato	tento	k3xDgNnPc1	tento
nařčení	nařčení	k1gNnPc4	nařčení
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
akcí	akce	k1gFnSc7	akce
tým	tým	k1gInSc1	tým
demontoval	demontovat	k5eAaBmAgInS	demontovat
části	část	k1gFnPc4	část
střech	střecha	k1gFnPc2	střecha
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Skyfall	Skyfalla	k1gFnPc2	Skyfalla
(	(	kIx(	(
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
a	a	k8xC	a
Skyfall	Skyfall	k1gInSc4	Skyfall
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Sam	Sam	k1gMnSc1	Sam
Mendes	Mendes	k1gMnSc1	Mendes
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c2	za
autora	autor	k1gMnSc2	autor
hudby	hudba	k1gFnSc2	hudba
skladatele	skladatel	k1gMnSc2	skladatel
Thomase	Thomas	k1gMnSc2	Thomas
Newmana	Newman	k1gMnSc2	Newman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
již	již	k6eAd1	již
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
filmech	film	k1gInPc6	film
Americká	americký	k2eAgFnSc1d1	americká
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
Road	Road	k1gInSc1	Road
to	ten	k3xDgNnSc1	ten
Perdition	Perdition	k1gInSc4	Perdition
<g/>
,	,	kIx,	,
Jarhead	Jarhead	k1gInSc4	Jarhead
a	a	k8xC	a
Nouzový	nouzový	k2eAgInSc4d1	nouzový
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Newman	Newman	k1gMnSc1	Newman
tak	tak	k9	tak
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Davida	David	k1gMnSc4	David
Arnolda	Arnold	k1gMnSc4	Arnold
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
předchozím	předchozí	k2eAgInPc3d1	předchozí
pěti	pět	k4xCc3	pět
bondovkám	bondovkám	k?	bondovkám
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
následně	následně	k6eAd1	následně
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
režisér	režisér	k1gMnSc1	režisér
Newmana	Newman	k1gMnSc4	Newman
vybral	vybrat	k5eAaPmAgMnS	vybrat
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jeho	on	k3xPp3gInSc2	on
závazku	závazek	k1gInSc2	závazek
vůči	vůči	k7c3	vůči
Dannymu	Dannym	k1gInSc2	Dannym
Boylovi	Boyl	k1gMnSc3	Boyl
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
hudebně	hudebně	k6eAd1	hudebně
angažoval	angažovat	k5eAaBmAgInS	angažovat
na	na	k7c6	na
Zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
londýnských	londýnský	k2eAgFnPc2d1	londýnská
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Silva	Silva	k1gFnSc1	Silva
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Bondovi	Bonda	k1gMnSc3	Bonda
opuštěný	opuštěný	k2eAgInSc4d1	opuštěný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
zazní	zaznět	k5eAaImIp3nS	zaznět
píseň	píseň	k1gFnSc4	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
"	"	kIx"	"
<g/>
Boum	Bouma	k1gFnPc2	Bouma
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
Treneta	Trenet	k1gMnSc2	Trenet
<g/>
.	.	kIx.	.
</s>
<s>
Coververze	Coververze	k1gFnSc1	Coververze
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Boom	boom	k1gInSc1	boom
Boom	boom	k1gInSc1	boom
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
od	od	k7c2	od
The	The	k1gFnSc2	The
Animals	Animals	k1gInSc1	Animals
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Lee	Lea	k1gFnSc3	Lea
Hooker	Hookero	k1gNnPc2	Hookero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
během	během	k7c2	během
Silvova	Silvův	k2eAgInSc2d1	Silvův
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Skyfall	Skyfall	k1gInSc4	Skyfall
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
Adele	Adele	k1gFnSc2	Adele
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
stálým	stálý	k2eAgMnSc7d1	stálý
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
Paulem	Paul	k1gMnSc7	Paul
Epworthem	Epworth	k1gInSc7	Epworth
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaBmAgFnS	napsat
a	a	k8xC	a
nahrála	nahrát	k5eAaBmAgFnS	nahrát
titulní	titulní	k2eAgFnSc4d1	titulní
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Skyfall	Skyfall	k1gInSc4	Skyfall
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c4	v
0.07	[number]	k4	0.07
hodin	hodina	k1gFnPc2	hodina
britského	britský	k2eAgInSc2d1	britský
letního	letní	k2eAgInSc2d1	letní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
producenti	producent	k1gMnPc1	producent
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
"	"	kIx"	"
<g/>
James	James	k1gInSc1	James
Bond	bond	k1gInSc1	bond
Day	Day	k1gFnSc1	Day
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Den	den	k1gInSc1	den
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bondo	k1gNnSc2	Bondo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
přesně	přesně	k6eAd1	přesně
padesát	padesát	k4xCc1	padesát
let	léto	k1gNnPc2	léto
od	od	k7c2	od
premiéry	premiéra	k1gFnSc2	premiéra
první	první	k4xOgFnSc2	první
filmové	filmový	k2eAgFnSc2d1	filmová
bondovky	bondovky	k?	bondovky
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
No	no	k9	no
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgInS	být
Skyfall	Skyfall	k1gInSc1	Skyfall
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
nosičích	nosič	k1gInPc6	nosič
DVD	DVD	kA	DVD
a	a	k8xC	a
Blu-ray	Blua	k2eAgInPc1d1	Blu-ra
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyšel	vyjít	k5eAaPmAgInS	vyjít
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
