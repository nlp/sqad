<s>
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc2	rouge
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Bâton-Rouge	Bâton-Rouge	k1gFnSc1	Bâton-Rouge
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
<g/>
,	,	kIx,	,
petrochemické	petrochemický	k2eAgNnSc4d1	petrochemické
a	a	k8xC	a
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc2	rouge
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
francouzským	francouzský	k2eAgMnSc7d1	francouzský
průzkumníkem	průzkumník	k1gMnSc7	průzkumník
Pierre	Pierr	k1gMnSc5	Pierr
Le	Le	k1gMnSc5	Le
Moyne	Moyn	k1gMnSc5	Moyn
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Iberville	Ibervill	k1gMnSc2	Ibervill
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
technologicky	technologicky	k6eAd1	technologicky
se	se	k3xPyFc4	se
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
měst	město	k1gNnPc2	město
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Velkoměstská	velkoměstský	k2eAgFnSc1d1	velkoměstská
aglomerace	aglomerace	k1gFnSc1	aglomerace
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc1	rouge
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
v	v	k7c6	v
USA	USA	kA	USA
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
měla	mít	k5eAaImAgFnS	mít
600	[number]	k4	600
000	[number]	k4	000
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
už	už	k9	už
770	[number]	k4	770
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
204,8	[number]	k4	204,8
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
199	[number]	k4	199
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
5,8	[number]	k4	5,8
km2	km2	k4	km2
(	(	kIx(	(
<g/>
2,81	[number]	k4	2,81
%	%	kIx~	%
<g/>
)	)	kIx)	)
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
229	[number]	k4	229
493	[number]	k4	493
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sídlilo	sídlit	k5eAaImAgNnS	sídlit
227	[number]	k4	227
818	[number]	k4	818
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
88	[number]	k4	88
973	[number]	k4	973
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
52	[number]	k4	52
672	[number]	k4	672
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
1144,7	[number]	k4	1144,7
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
39,4	[number]	k4	39,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
54,5	[number]	k4	54,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
24,4	[number]	k4	24,4
%	%	kIx~	%
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
17,5	[number]	k4	17,5
%	%	kIx~	%
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
27,2	[number]	k4	27,2
%	%	kIx~	%
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
19,4	[number]	k4	19,4
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
11,4	[number]	k4	11,4
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
30	[number]	k4	30
let	léto	k1gNnPc2	léto
Lynn	Lynna	k1gFnPc2	Lynna
Whitfieldová	Whitfieldová	k1gFnSc1	Whitfieldová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Pruitt	Pruitt	k1gMnSc1	Pruitt
Taylor	Taylor	k1gMnSc1	Taylor
Vince	Vince	k?	Vince
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Shane	Shan	k1gInSc5	Shan
West	West	k2eAgMnSc1d1	West
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Cameron	Cameron	k1gMnSc1	Cameron
Richardson	Richardson	k1gMnSc1	Richardson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Carly	Carla	k1gFnSc2	Carla
Pattersonová	Pattersonová	k1gFnSc1	Pattersonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gymnastka	gymnastka	k1gFnSc1	gymnastka
a	a	k8xC	a
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
Aix-en-Provence	Aixn-Provence	k1gFnSc1	Aix-en-Provence
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Malatya	Malaty	k1gInSc2	Malaty
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Port-au-Prince	Portu-Prinec	k1gInSc2	Port-au-Prinec
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc4	Haiti
Tchaj-čung	Tchaj-čunga	k1gFnPc2	Tchaj-čunga
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
</s>
