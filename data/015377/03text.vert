<s>
Deoxycytidin	Deoxycytidin	k1gInSc1
</s>
<s>
Deoxycytidin	Deoxycytidin	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
[	[	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
R	R	kA
<g/>
,4	,4	k4
<g/>
S	s	k7c7
<g/>
,5	,5	k4
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-hydroxy-	-hydroxy-	k?
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
hydroxymethyl	hydroxymethyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
tetrahydrofuran-	tetrahydrofuran-	k?
<g/>
2	#num#	k4
<g/>
-yl	-yl	k?
<g/>
]	]	kIx)
<g/>
pyrimidin-	pyrimidin-	k?
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
-on	-on	k?
</s>
<s>
Triviální	triviální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Deoxycytidin	Deoxycytidin	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C9H13N3O4	C9H13N3O4	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
951-77-9	951-77-9	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
227,217	227,217	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Deoxycytidin	Deoxycytidin	k1gInSc1
(	(	kIx(
<g/>
dC	dC	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pyrimidinový	pyrimidinový	k2eAgInSc1d1
nukleosid	nukleosid	k1gInSc1
složený	složený	k2eAgInSc1d1
z	z	k7c2
cytosinu	cytosin	k2eAgFnSc4d1
napojeného	napojený	k2eAgMnSc4d1
na	na	k7c4
cukr	cukr	k1gInSc4
deoxyribózu	deoxyribóza	k1gFnSc4
glykosidickou	glykosidický	k2eAgFnSc7d1
vazbou	vazba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deoxycytidin	Deoxycytidin	k1gInSc1
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
jako	jako	k9
stavební	stavební	k2eAgFnSc1d1
částice	částice	k1gFnSc1
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
tato	tento	k3xDgFnSc1
molekula	molekula	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
místo	místo	k7c2
deoxyribózy	deoxyribóza	k1gFnSc2
ribózu	ribóza	k1gFnSc4
<g/>
,	,	kIx,
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
cytidin	cytidin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
derivátem	derivát	k1gInSc7
je	být	k5eAaImIp3nS
také	také	k9
například	například	k6eAd1
dCMP	dCMP	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
deoxycytidinmonofosfát	deoxycytidinmonofosfát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
deoxycytidin	deoxycytidin	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Složky	složka	k1gFnPc1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
Nukleové	nukleový	k2eAgFnSc2d1
báze	báze	k1gFnSc2
</s>
<s>
Purinové	purinový	k2eAgNnSc1d1
</s>
<s>
Adenin	adenin	k1gInSc1
•	•	k?
Guanin	guanin	k1gInSc1
Pyrimidinové	Pyrimidinový	k2eAgInPc1d1
</s>
<s>
Thymin	Thymin	k2eAgMnSc1d1
•	•	k?
Uracil	Uracil	k1gMnSc1
•	•	k?
Cytosin	Cytosin	k1gMnSc1
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1
modifikace	modifikace	k1gFnPc1
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
</s>
<s>
V	v	k7c6
RNA	RNA	kA
</s>
<s>
Adenin	adenin	k1gInSc1
→	→	k?
Xantin	Xantin	k1gInSc1
<g/>
,	,	kIx,
Hypoxantin	Hypoxantin	k2eAgInSc1d1
•	•	k?
Guanin	guanin	k1gInSc1
→	→	k?
7	#num#	k4
<g/>
-methylguanin	-methylguanin	k2eAgMnSc1d1
•	•	k?
Uracil	Uracil	k1gMnSc1
→	→	k?
Thymin	Thymin	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
-methyluracil	-methyluracila	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dihydrouridin	Dihydrouridin	k2eAgInSc1d1
V	v	k7c6
DNA	DNA	kA
</s>
<s>
Adenin	adenin	k1gInSc1
→	→	k?
N	N	kA
<g/>
6	#num#	k4
<g/>
-methyladenin	-methyladenin	k2eAgMnSc1d1
•	•	k?
Thymin	Thymin	k1gInSc1
→	→	k?
báze	báze	k1gFnSc2
J	J	kA
•	•	k?
Cytosin	Cytosin	k1gInSc1
→	→	k?
4	#num#	k4
<g/>
-methylcytosin	-methylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-methylcytosin	-methylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-hydroxymethylcytosin	-hydroxymethylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-formylcytosin	-formylcytosina	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-karboxylcytosin	-karboxylcytosina	k1gFnPc2
</s>
<s>
Nukleosidy	Nukleosida	k1gFnPc1
</s>
<s>
Adenosin	Adenosin	k2eAgInSc1d1
•	•	k?
Uridin	Uridin	k2eAgInSc1d1
•	•	k?
Guanosin	Guanosin	k1gInSc1
•	•	k?
Cytidin	Cytidin	k1gInSc1
•	•	k?
modifikované	modifikovaný	k2eAgInPc1d1
<g/>
:	:	kIx,
Pseudouridin	Pseudouridin	k1gInSc1
<g/>
,	,	kIx,
Ribothymidin	Ribothymidin	k1gInSc1
<g/>
,	,	kIx,
Inosin	Inosin	k1gInSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
bázi	báze	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
hypoxantin	hypoxantin	k1gInSc1
<g/>
)	)	kIx)
Deoxynukleosidy	Deoxynukleosida	k1gFnPc1
</s>
<s>
Deoxyadenosin	Deoxyadenosin	k1gMnSc1
•	•	k?
Deoxyguanosin	Deoxyguanosin	k1gMnSc1
•	•	k?
Deoxycytidin	Deoxycytidin	k1gInSc1
•	•	k?
Deoxythymidin	Deoxythymidin	k1gInSc1
•	•	k?
modifikované	modifikovaný	k2eAgInPc1d1
<g/>
:	:	kIx,
Deoxyuridin	Deoxyuridin	k1gInSc1
Ribonukleotidy	Ribonukleotida	k1gFnSc2
</s>
<s>
AMP	AMP	kA
(	(	kIx(
<g/>
cAMP	camp	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
UMP	UMP	kA
•	•	k?
GMP	GMP	kA
(	(	kIx(
<g/>
cGMP	cGMP	k?
<g/>
)	)	kIx)
•	•	k?
CMP	CMP	kA
•	•	k?
ADP	ADP	kA
•	•	k?
UDP	UDP	kA
(	(	kIx(
<g/>
m	m	kA
<g/>
5	#num#	k4
<g/>
UDP	UDP	kA
<g/>
)	)	kIx)
•	•	k?
GDP	GDP	kA
•	•	k?
CDP	CDP	kA
•	•	k?
ATP	atp	kA
•	•	k?
UTP	UTP	kA
•	•	k?
GTP	GTP	kA
•	•	k?
CTP	CTP	kA
Deoxyribonukleotidy	Deoxyribonukleotida	k1gFnSc2
</s>
<s>
dAMP	dAMP	k?
•	•	k?
dTMP	dTMP	k?
•	•	k?
dUMP	dUMP	k?
•	•	k?
dGMP	dGMP	k?
•	•	k?
dCMP	dCMP	k?
•	•	k?
dADP	dADP	k?
•	•	k?
dTDP	dTDP	k?
•	•	k?
dUDP	dUDP	k?
•	•	k?
dGDP	dGDP	k?
•	•	k?
dCDP	dCDP	k?
•	•	k?
dATP	dATP	k?
•	•	k?
dTTP	dTTP	k?
•	•	k?
dUTP	dUTP	k?
(	(	kIx(
<g/>
m	m	kA
<g/>
5	#num#	k4
<g/>
UTP	UTP	kA
<g/>
)	)	kIx)
•	•	k?
dGTP	dGTP	k?
•	•	k?
dCTP	dCTP	k?
Analogy	analog	k1gInPc7
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
</s>
<s>
GNA	GNA	kA
•	•	k?
LNA	lnout	k5eAaImSgInS
•	•	k?
PNA	pnout	k5eAaImSgInS
•	•	k?
TNA	tnout	k5eAaBmSgInS
•	•	k?
morpholino	morpholin	k2eAgNnSc1d1
•	•	k?
syntetické	syntetický	k2eAgInPc4d1
purinové	purinový	k2eAgInPc4d1
analogy	analog	k1gInPc4
•	•	k?
syntetické	syntetický	k2eAgInPc4d1
pyrimidinové	pyrimidinový	k2eAgInPc4d1
analogy	analog	k1gInPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Chemie	chemie	k1gFnSc1
</s>
