<s>
Parníky	parník	k1gInPc1	parník
brázdily	brázdit	k5eAaImAgInP	brázdit
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
na	na	k7c4	na
Slapskou	slapský	k2eAgFnSc4d1	Slapská
přehradu	přehrada	k1gFnSc4	přehrada
nebo	nebo	k8xC	nebo
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
od	od	k7c2	od
saských	saský	k2eAgInPc2d1	saský
Drážďan	Drážďany	k1gInPc2	Drážďany
přes	přes	k7c4	přes
Děčín	Děčín	k1gInSc4	Děčín
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc4	Litoměřice
a	a	k8xC	a
Mělník	Mělník	k1gInSc1	Mělník
a	a	k8xC	a
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
až	až	k9	až
po	po	k7c4	po
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
