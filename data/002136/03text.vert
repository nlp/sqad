<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
(	(	kIx(	(
<g/>
上	上	k?	上
Šang-chaj	Šanghaj	k1gMnSc1	Šang-chaj
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
shà	shà	k?	shà
hǎ	hǎ	k?	hǎ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
významné	významný	k2eAgNnSc1d1	významné
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
centrum	centrum	k1gNnSc1	centrum
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
do	do	k7c2	do
Východočínského	východočínský	k2eAgNnSc2d1	Východočínské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
obratem	obrat	k1gInSc7	obrat
nákladu	náklad	k1gInSc2	náklad
380	[number]	k4	380
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
největšího	veliký	k2eAgInSc2d3	veliký
kontejnerového	kontejnerový	k2eAgInSc2d1	kontejnerový
přístavu	přístav	k1gInSc2	přístav
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
čtveřici	čtveřice	k1gFnSc4	čtveřice
měst	město	k1gNnPc2	město
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statutem	statut	k1gInSc7	statut
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČLR	ČLR	kA	ČLR
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
provinciím	provincie	k1gFnPc3	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
znaky	znak	k1gInPc7	znak
"	"	kIx"	"
<g/>
nahoře	nahoře	k6eAd1	nahoře
<g/>
/	/	kIx~	/
<g/>
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
vykládat	vykládat	k5eAaImF	vykládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
směřující	směřující	k2eAgInSc1d1	směřující
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
a	a	k8xC	a
přesný	přesný	k2eAgInSc1d1	přesný
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
užívá	užívat	k5eAaImIp3nS	užívat
též	též	k9	též
zkratek	zkratka	k1gFnPc2	zkratka
Chu	Chu	k1gMnSc1	Chu
(	(	kIx(	(
<g/>
TZ	TZ	kA	TZ
<g/>
:	:	kIx,	:
滬	滬	k?	滬
<g/>
,	,	kIx,	,
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
沪	沪	k?	沪
<g/>
,	,	kIx,	,
Hù	Hù	k1gFnSc1	Hù
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šen	Šen	k1gFnSc1	Šen
(	(	kIx(	(
<g/>
申	申	k?	申
<g/>
,	,	kIx,	,
Shē	Shē	k1gMnSc1	Shē
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Chuang-pchu	Chuangcha	k1gFnSc4	Chuang-pcha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
šířku	šířka	k1gFnSc4	šířka
kolem	kolem	k7c2	kolem
400	[number]	k4	400
m.	m.	k?	m.
Terén	terén	k1gInSc1	terén
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
4	[number]	k4	4
m.	m.	k?	m.
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgInSc1d1	dopravní
hluk	hluk	k1gInSc1	hluk
a	a	k8xC	a
silné	silný	k2eAgNnSc1d1	silné
znečištění	znečištění	k1gNnSc1	znečištění
vzduchu	vzduch	k1gInSc2	vzduch
i	i	k8xC	i
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
-	-	kIx~	-
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
s	s	k7c7	s
nejznečištěnějším	znečištěný	k2eAgNnSc7d3	nejznečištěnější
ovzduším	ovzduší	k1gNnSc7	ovzduší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc7d1	městská
květinou	květina	k1gFnSc7	květina
je	být	k5eAaImIp3nS	být
magnólie	magnólie	k1gFnSc1	magnólie
(	(	kIx(	(
<g/>
Magnolia	Magnolia	k1gFnSc1	Magnolia
denudata	denudata	k1gFnSc1	denudata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
subtropické	subtropický	k2eAgNnSc1d1	subtropické
se	s	k7c7	s
zimními	zimní	k2eAgFnPc7d1	zimní
teplotami	teplota	k1gFnPc7	teplota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
-1	-1	k4	-1
<g/>
°	°	k?	°
až	až	k9	až
8	[number]	k4	8
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
pak	pak	k6eAd1	pak
teploty	teplota	k1gFnPc1	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
28	[number]	k4	28
<g/>
°	°	k?	°
až	až	k9	až
35	[number]	k4	35
<g/>
°	°	k?	°
a	a	k8xC	a
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
nepříjemně	příjemně	k6eNd1	příjemně
stoupá	stoupat	k5eAaImIp3nS	stoupat
přes	přes	k7c4	přes
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
15,3	[number]	k4	15,3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
roční	roční	k2eAgInSc4d1	roční
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
1	[number]	k4	1
135	[number]	k4	135
mm	mm	kA	mm
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
největší	veliký	k2eAgInSc1d3	veliký
část	část	k1gFnSc4	část
spadne	spadnout	k5eAaPmIp3nS	spadnout
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
110	[number]	k4	110
dnů	den	k1gInPc2	den
s	s	k7c7	s
deštěm	dešť	k1gInSc7	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
období	období	k1gNnSc1	období
k	k	k7c3	k
návštěvě	návštěva	k1gFnSc3	návštěva
nastává	nastávat	k5eAaImIp3nS	nastávat
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
zvaném	zvaný	k2eAgMnSc6d1	zvaný
Chua-tching	Chuaching	k1gInSc4	Chua-tching
(	(	kIx(	(
<g/>
華	華	k?	華
<g/>
)	)	kIx)	)
-	-	kIx~	-
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
dynastie	dynastie	k1gFnSc2	dynastie
Sung	Sung	k1gMnSc1	Sung
(	(	kIx(	(
<g/>
960	[number]	k4	960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1074	[number]	k4	1074
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
zřízení	zřízení	k1gNnSc4	zřízení
vlastního	vlastní	k2eAgInSc2d1	vlastní
berního	berní	k2eAgInSc2d1	berní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1553	[number]	k4	1553
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
hradbami	hradba	k1gFnPc7	hradba
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
pirátům	pirát	k1gMnPc3	pirát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
časem	časem	k6eAd1	časem
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
ke	k	k7c3	k
200	[number]	k4	200
000	[number]	k4	000
a	a	k8xC	a
kromě	kromě	k7c2	kromě
obchodu	obchod	k1gInSc2	obchod
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
textilní	textilní	k2eAgFnSc1d1	textilní
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
přístavem	přístav	k1gInSc7	přístav
pouze	pouze	k6eAd1	pouze
oblastního	oblastní	k2eAgInSc2d1	oblastní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strategická	strategický	k2eAgFnSc1d1	strategická
poloha	poloha	k1gFnSc1	poloha
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
největší	veliký	k2eAgFnSc2d3	veliký
čínské	čínský	k2eAgFnSc2d1	čínská
řeky	řeka	k1gFnSc2	řeka
učinila	učinit	k5eAaPmAgFnS	učinit
ze	z	k7c2	z
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
středisek	středisko	k1gNnPc2	středisko
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Nankingská	nankingský	k2eAgFnSc1d1	Nankingská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
po	po	k7c4	po
prohrané	prohraný	k2eAgInPc4d1	prohraný
první	první	k4xOgInPc4	první
opiové	opiový	k2eAgInPc4d1	opiový
válce	válec	k1gInPc4	válec
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
Čína	Čína	k1gFnSc1	Čína
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
stanovila	stanovit	k5eAaPmAgFnS	stanovit
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
smluvních	smluvní	k2eAgInPc2d1	smluvní
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
obchodu	obchod	k1gInSc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
cizinecká	cizinecký	k2eAgFnSc1d1	cizinecká
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
nepodléhající	podléhající	k2eNgFnSc4d1	nepodléhající
čínským	čínský	k2eAgInPc3d1	čínský
úřadům	úřad	k1gInPc3	úřad
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
spravovaná	spravovaný	k2eAgFnSc1d1	spravovaná
tzv.	tzv.	kA	tzv.
Šanghajskou	šanghajský	k2eAgFnSc7d1	Šanghajská
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
západních	západní	k2eAgInPc2d1	západní
vlivů	vliv	k1gInPc2	vliv
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
výrazně	výrazně	k6eAd1	výrazně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
za	za	k7c4	za
tchajpchingského	tchajpchingský	k1gMnSc4	tchajpchingský
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
díky	díky	k7c3	díky
přílivu	příliv	k1gInSc3	příliv
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
venkova	venkov	k1gInSc2	venkov
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
cizinecké	cizinecký	k2eAgFnSc2d1	cizinecká
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
;	;	kIx,	;
podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
za	za	k7c2	za
boxerského	boxerský	k2eAgNnSc2d1	boxerské
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
překročilo	překročit	k5eAaPmAgNnS	překročit
hranici	hranice	k1gFnSc6	hranice
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
kosmopolitní	kosmopolitní	k2eAgFnSc1d1	kosmopolitní
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
dvě	dva	k4xCgFnPc4	dva
moderní	moderní	k2eAgFnPc4d1	moderní
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
symbolem	symbol	k1gInSc7	symbol
moderní	moderní	k2eAgFnSc2d1	moderní
a	a	k8xC	a
prosperující	prosperující	k2eAgFnSc2d1	prosperující
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
působištěm	působiště	k1gNnSc7	působiště
drogových	drogový	k2eAgInPc2d1	drogový
gangů	gang	k1gInPc2	gang
a	a	k8xC	a
synonymem	synonymum	k1gNnSc7	synonymum
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
vydával	vydávat	k5eAaImAgInS	vydávat
Čchen	Čchen	k1gInSc1	Čchen
Tu-siou	Tuiá	k1gFnSc4	Tu-siá
vlivný	vlivný	k2eAgInSc1d1	vlivný
pokrokový	pokrokový	k2eAgInSc1d1	pokrokový
časopis	časopis	k1gInSc1	časopis
Mládí	mládí	k1gNnSc1	mládí
(	(	kIx(	(
<g/>
青	青	k?	青
<g/>
,	,	kIx,	,
Čching-nien	Čchingien	k1gInSc1	Čching-nien
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
mezi	mezi	k7c7	mezi
dějišti	dějiště	k1gNnSc3	dějiště
největších	veliký	k2eAgFnPc2d3	veliký
demonstrací	demonstrace	k1gFnPc2	demonstrace
proti	proti	k7c3	proti
postoupení	postoupení	k1gNnSc3	postoupení
bývalých	bývalý	k2eAgFnPc2d1	bývalá
německých	německý	k2eAgFnPc2d1	německá
koncesí	koncese	k1gFnPc2	koncese
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
Japonsku	Japonsko	k1gNnSc6	Japonsko
(	(	kIx(	(
<g/>
Hnutí	hnutí	k1gNnSc1	hnutí
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
marxistický	marxistický	k2eAgInSc1d1	marxistický
vliv	vliv	k1gInSc1	vliv
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
založena	založen	k2eAgFnSc1d1	založena
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1927	[number]	k4	1927
šanghajští	šanghajský	k2eAgMnPc1d1	šanghajský
dělníci	dělník	k1gMnPc1	dělník
pod	pod	k7c7	pod
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
vedením	vedení	k1gNnSc7	vedení
pokusili	pokusit	k5eAaPmAgMnP	pokusit
ovládnout	ovládnout	k5eAaPmF	ovládnout
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zmasakrováni	zmasakrován	k2eAgMnPc1d1	zmasakrován
Čankajškem	Čankajšek	k1gInSc7	Čankajšek
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
gangu	gang	k1gInSc2	gang
<g/>
;	;	kIx,	;
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudu	soud	k1gInSc2	soud
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
popraveno	popravit	k5eAaPmNgNnS	popravit
na	na	k7c4	na
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
cizineckých	cizinecký	k2eAgFnPc2d1	cizinecká
čtvrtí	čtvrt	k1gFnPc2	čtvrt
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
Velká	velká	k1gFnSc1	velká
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
čítající	čítající	k2eAgFnSc4d1	čítající
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Ťiang	Ťiang	k1gInSc1	Ťiang
<g/>
-	-	kIx~	-
<g/>
su	su	k?	su
a	a	k8xC	a
postaveno	postaven	k2eAgNnSc4d1	postaveno
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
provincii	provincie	k1gFnSc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Tříměsíční	tříměsíční	k2eAgFnSc1d1	tříměsíční
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1937	[number]	k4	1937
vylodila	vylodit	k5eAaPmAgNnP	vylodit
japonská	japonský	k2eAgNnPc1d1	Japonské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
střetů	střet	k1gInPc2	střet
druhé	druhý	k4xOgFnSc2	druhý
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
město	město	k1gNnSc4	město
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
200	[number]	k4	200
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
obránci	obránce	k1gMnPc1	obránce
museli	muset	k5eAaImAgMnP	muset
přes	přes	k7c4	přes
statečný	statečný	k2eAgInSc4d1	statečný
odpor	odpor	k1gInSc4	odpor
nakonec	nakonec	k6eAd1	nakonec
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937-1945	[number]	k4	1937-1945
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
okupována	okupovat	k5eAaBmNgFnS	okupovat
císařským	císařský	k2eAgNnSc7d1	císařské
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
též	též	k9	též
stala	stát	k5eAaPmAgFnS	stát
cílem	cíl	k1gInSc7	cíl
tisíců	tisíc	k4xCgInPc2	tisíc
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
je	být	k5eAaImIp3nS	být
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
pod	pod	k7c7	pod
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
řada	řada	k1gFnSc1	řada
západních	západní	k2eAgFnPc2d1	západní
firem	firma	k1gFnPc2	firma
do	do	k7c2	do
Hongkongu	Hongkong	k1gInSc2	Hongkong
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
v	v	k7c6	v
roli	role	k1gFnSc6	role
obchodní	obchodní	k2eAgFnSc2d1	obchodní
metropole	metropol	k1gFnSc2	metropol
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
výsadní	výsadní	k2eAgNnSc1d1	výsadní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
postavení	postavení	k1gNnSc1	postavení
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Číny	Čína	k1gFnSc2	Čína
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neotřeseno	otřást	k5eNaPmNgNnS	otřást
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
Hongkong	Hongkong	k1gInSc4	Hongkong
<g/>
,	,	kIx,	,
regionem	region	k1gInSc7	region
se	s	k7c7	s
suverénně	suverénně	k6eAd1	suverénně
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
město	město	k1gNnSc1	město
spíše	spíše	k9	spíše
stagnovalo	stagnovat	k5eAaImAgNnS	stagnovat
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
přišel	přijít	k5eAaPmAgInS	přijít
po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
zóny	zóna	k1gFnSc2	zóna
Pchu-tung	Pchuunga	k1gFnPc2	Pchu-tunga
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
velkolepé	velkolepý	k2eAgInPc4d1	velkolepý
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
těžko	těžko	k6eAd1	těžko
kontrolovatelný	kontrolovatelný	k2eAgInSc1d1	kontrolovatelný
příliv	příliv	k1gInSc1	příliv
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
venkovských	venkovský	k2eAgFnPc2d1	venkovská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
provincií	provincie	k1gFnPc2	provincie
An	An	k1gFnSc2	An
<g/>
-	-	kIx~	-
<g/>
chuej	chuej	k1gInSc1	chuej
<g/>
,	,	kIx,	,
Ťiang	Ťiang	k1gInSc1	Ťiang
<g/>
-	-	kIx~	-
<g/>
su	su	k?	su
a	a	k8xC	a
Če	Če	k1gMnSc1	Če
<g/>
-	-	kIx~	-
<g/>
ťiang	ťiang	k1gMnSc1	ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vnějších	vnější	k2eAgNnPc2d1	vnější
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
)	)	kIx)	)
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
550	[number]	k4	550
km2	km2	k4	km2
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
19	[number]	k4	19
263	[number]	k4	263
459	[number]	k4	459
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
:	:	kIx,	:
16	[number]	k4	16
843	[number]	k4	843
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
aglomerace	aglomerace	k1gFnSc1	aglomerace
spadající	spadající	k2eAgFnSc1d1	spadající
do	do	k7c2	do
správního	správní	k2eAgInSc2d1	správní
obvodu	obvod	k1gInSc2	obvod
<g />
.	.	kIx.	.
</s>
<s>
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
venkovský	venkovský	k2eAgInSc1d1	venkovský
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
7	[number]	k4	7
0	[number]	k4	0
<g/>
37,50	[number]	k4	37,50
km2	km2	k4	km2
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
24	[number]	k4	24
200	[number]	k4	200
000	[number]	k4	000
(	(	kIx(	(
<g/>
1.6	[number]	k4	1.6
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
:	:	kIx,	:
3	[number]	k4	3
298	[number]	k4	298
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
čísla	číslo	k1gNnPc1	číslo
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc4	počet
úředně	úředně	k6eAd1	úředně
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
"	"	kIx"	"
<g/>
načerno	načerno	k6eAd1	načerno
<g/>
"	"	kIx"	"
dalších	další	k2eAgInPc2d1	další
4-5	[number]	k4	4-5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
deseti	deset	k4xCc2	deset
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
označovaných	označovaný	k2eAgMnPc2d1	označovaný
Pchu-si	Pchue	k1gFnSc4	Pchu-se
(	(	kIx(	(
<g/>
浦	浦	k?	浦
<g/>
,	,	kIx,	,
Pǔ	Pǔ	k1gMnSc1	Pǔ
<g/>
)	)	kIx)	)
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Chuang-pchu-ťiang	Chuangchu-ťianga	k1gFnPc2	Chuang-pchu-ťianga
(	(	kIx(	(
<g/>
黄	黄	k?	黄
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
desátý	desátý	k4xOgInSc1	desátý
obvod	obvod	k1gInSc1	obvod
Pchu-tung	Pchuung	k1gInSc1	Pchu-tung
(	(	kIx(	(
<g/>
浦	浦	k?	浦
<g/>
,	,	kIx,	,
Pǔ	Pǔ	k1gFnSc1	Pǔ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
je	být	k5eAaImIp3nS	být
hypermoderní	hypermoderní	k2eAgFnSc1d1	hypermoderní
čtvrť	čtvrť	k1gFnSc1	čtvrť
na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc1	osm
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
venkovský	venkovský	k2eAgInSc1d1	venkovský
okres	okres	k1gInSc1	okres
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
vnější	vnější	k2eAgNnSc4d1	vnější
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
-	-	kIx~	-
10	[number]	k4	10
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
čchü	čchü	k?	čchü
<g/>
)	)	kIx)	)
Chuang-pchu	Chuangcha	k1gMnSc4	Chuang-pcha
(	(	kIx(	(
<g/>
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
黄	黄	k?	黄
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Huángpǔ	Huángpǔ	k1gMnSc1	Huángpǔ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Lü-wan	Lüan	k1gMnSc1	Lü-wan
(	(	kIx(	(
<g/>
卢	卢	k?	卢
Lúwā	Lúwā	k1gMnSc1	Lúwā
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Sü-chuej	Sühuej	k1gInSc1	Sü-chuej
(	(	kIx(	(
<g/>
徐	徐	k?	徐
Xúhuì	Xúhuì	k1gMnSc1	Xúhuì
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Čchang-ning	Čchanging	k1gInSc1	Čchang-ning
(	(	kIx(	(
<g/>
长	长	k?	长
Chángníng	Chángníng	k1gMnSc1	Chángníng
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Ťing-an	Ťingn	k1gInSc1	Ťing-an
(	(	kIx(	(
<g/>
静	静	k?	静
Jì	Jì	k1gFnSc1	Jì
<g/>
'	'	kIx"	'
<g/>
ā	ā	k?	ā
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Pchu-tchuo	Pchuchuo	k1gMnSc1	Pchu-tchuo
(	(	kIx(	(
<g/>
普	普	k?	普
Pǔ	Pǔ	k1gMnSc1	Pǔ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Ča-pej	Čaej	k1gMnSc1	Ča-pej
(	(	kIx(	(
<g/>
闸	闸	k?	闸
Zháběi	Zhábě	k1gMnSc5	Zhábě
Qū	Qū	k1gMnSc5	Qū
<g/>
)	)	kIx)	)
Chung-kchou	Chungchý	k2eAgFnSc7d1	Chung-kchý
(	(	kIx(	(
<g/>
虹	虹	k?	虹
Hóngkǒ	Hóngkǒ	k1gMnSc1	Hóngkǒ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Jang-pchu	Jangch	k1gInSc2	Jang-pch
(	(	kIx(	(
<g/>
杨	杨	k?	杨
Yángpǔ	Yángpǔ	k1gMnSc1	Yángpǔ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
Pchu-tung	Pchuung	k1gMnSc1	Pchu-tung
(	(	kIx(	(
<g/>
浦	浦	k?	浦
Pǔ	Pǔ	k1gMnSc1	Pǔ
Xī	Xī	k1gMnSc1	Xī
<g />
.	.	kIx.	.
</s>
<s>
Qū	Qū	k?	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
okres	okres	k1gInSc1	okres
Čchuan-ša	Čchuan-š	k1gInSc2	Čchuan-š
(	(	kIx(	(
<g/>
Chuansha	Chuansha	k1gFnSc1	Chuansha
<g/>
)	)	kIx)	)
Okrajové	okrajový	k2eAgFnPc1d1	okrajová
části	část	k1gFnPc1	část
-	-	kIx~	-
8	[number]	k4	8
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
čchü	čchü	k?	čchü
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
okres	okres	k1gInSc1	okres
(	(	kIx(	(
<g/>
sien	siena	k1gFnPc2	siena
<g/>
)	)	kIx)	)
Pao-šan	Pao-šan	k1gInSc1	Pao-šan
(	(	kIx(	(
<g/>
宝	宝	k?	宝
Bǎ	Bǎ	k1gMnSc1	Bǎ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
1988	[number]	k4	1988
Min-chang	Minhang	k1gInSc1	Min-chang
(	(	kIx(	(
<g/>
闵	闵	k?	闵
Mǐ	Mǐ	k1gMnSc1	Mǐ
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
1992	[number]	k4	1992
Ťia-ting	Ťiaing	k1gInSc1	Ťia-ting
(	(	kIx(	(
<g/>
嘉	嘉	k?	嘉
Jiā	Jiā	k1gMnSc1	Jiā
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
1992	[number]	k4	1992
Ťin-šan	Ťin-šany	k1gInPc2	Ťin-šany
(	(	kIx(	(
<g/>
金	金	k?	金
Jī	Jī	k1gMnSc1	Jī
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
1992	[number]	k4	1992
Sung-ťiang	Sung-ťiang	k1gInSc1	Sung-ťiang
(	(	kIx(	(
<g/>
松	松	k?	松
Sō	Sō	k1gMnSc1	Sō
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
1997	[number]	k4	1997
Čching-pchu	Čchingch	k1gInSc2	Čching-pch
(	(	kIx(	(
<g/>
青	青	k?	青
Qī	Qī	k1gMnSc1	Qī
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
1999	[number]	k4	1999
Nan-chuej	Nanhuej	k1gInSc1	Nan-chuej
(	(	kIx(	(
<g/>
南	南	k?	南
Nánhuì	Nánhuì	k1gMnSc1	Nánhuì
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
2001	[number]	k4	2001
Feng-sien	Fengien	k1gInSc1	Feng-sien
(	(	kIx(	(
<g/>
奉	奉	k?	奉
Fè	Fè	k1gMnSc1	Fè
Qū	Qū	k1gMnSc1	Qū
<g/>
)	)	kIx)	)
–	–	k?	–
2001	[number]	k4	2001
Osm	osm	k4xCc1	osm
z	z	k7c2	z
devíti	devět	k4xCc2	devět
okresů	okres	k1gInPc2	okres
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1988	[number]	k4	1988
až	až	k6eAd1	až
2001	[number]	k4	2001
změněno	změnit	k5eAaPmNgNnS	změnit
v	v	k7c4	v
městské	městský	k2eAgInPc4d1	městský
obvody	obvod	k1gInPc4	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
zbývajícím	zbývající	k2eAgInSc7d1	zbývající
venkovským	venkovský	k2eAgInSc7d1	venkovský
okresem	okres	k1gInSc7	okres
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
Čchung-ming	Čchunging	k1gInSc1	Čchung-ming
(	(	kIx(	(
<g/>
崇	崇	k?	崇
Chóngmíng	Chóngmíng	k1gMnSc1	Chóngmíng
Xià	Xià	k1gMnSc1	Xià
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zajímající	zajímající	k2eAgFnSc4d1	zajímající
plochu	plocha	k1gFnSc4	plocha
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Šanghajské	šanghajský	k2eAgNnSc1d1	Šanghajské
metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
čtrnáct	čtrnáct	k4xCc4	čtrnáct
linek	linka	k1gFnPc2	linka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
další	další	k2eAgFnSc1d1	další
trasa	trasa	k1gFnSc1	trasa
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
stávajících	stávající	k2eAgMnPc2d1	stávající
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
šanghajské	šanghajský	k2eAgNnSc1d1	Šanghajské
letiště	letiště	k1gNnSc1	letiště
Chung-čchiao	Chung-čchiao	k1gMnSc1	Chung-čchiao
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
převážně	převážně	k6eAd1	převážně
vnitrostátní	vnitrostátní	k2eAgFnSc3d1	vnitrostátní
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
provozu	provoz	k1gInSc2	provoz
nové	nový	k2eAgNnSc4d1	nové
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
východní	východní	k2eAgFnSc2d1	východní
čtvrti	čtvrt	k1gFnSc2	čtvrt
Pchu-tung	Pchuunga	k1gFnPc2	Pchu-tunga
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
Pchu-tung	Pchuung	k1gMnSc1	Pchu-tung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
letiště	letiště	k1gNnSc1	letiště
spojuje	spojovat	k5eAaImIp3nS	spojovat
od	od	k7c2	od
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
první	první	k4xOgFnSc1	první
komerční	komerční	k2eAgFnSc1d1	komerční
linka	linka	k1gFnSc1	linka
magnetického	magnetický	k2eAgInSc2d1	magnetický
rychlovlaku	rychlovlak	k1gInSc2	rychlovlak
Transrapid	Transrapida	k1gFnPc2	Transrapida
německé	německý	k2eAgFnSc2d1	německá
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Rychlovlak	rychlovlak	k1gInSc1	rychlovlak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlosti	rychlost	k1gFnSc2	rychlost
450	[number]	k4	450
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
32	[number]	k4	32
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
trasu	trasa	k1gFnSc4	trasa
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
necelých	celý	k2eNgFnPc2d1	necelá
8	[number]	k4	8
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
však	však	k9	však
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
dle	dle	k7c2	dle
stanoveného	stanovený	k2eAgInSc2d1	stanovený
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
také	také	k6eAd1	také
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
a	a	k8xC	a
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Šangaj	Šangaj	k1gFnSc1	Šangaj
je	být	k5eAaImIp3nS	být
také	také	k9	také
významnou	významný	k2eAgFnSc7d1	významná
křižovatkou	křižovatka	k1gFnSc7	křižovatka
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
začíná	začínat	k5eAaImIp3nS	začínat
nebo	nebo	k8xC	nebo
jím	on	k3xPp3gNnSc7	on
prochází	procházet	k5eAaImIp3nS	procházet
mnoho	mnoho	k4c1	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
dálkových	dálkový	k2eAgFnPc2d1	dálková
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
protkáno	protkán	k2eAgNnSc1d1	protkáno
sítí	sítí	k1gNnSc1	sítí
místních	místní	k2eAgFnPc2d1	místní
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
řeky	řeka	k1gFnSc2	řeka
Chuang-pchu	Chuangch	k1gInSc2	Chuang-pch
spojuje	spojovat	k5eAaImIp3nS	spojovat
pět	pět	k4xCc1	pět
mostů	most	k1gInPc2	most
a	a	k8xC	a
několik	několik	k4yIc1	několik
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
most	most	k1gInSc1	most
a	a	k8xC	a
tunel	tunel	k1gInSc1	tunel
překonává	překonávat	k5eAaImIp3nS	překonávat
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
také	také	k9	také
řeku	řeka	k1gFnSc4	řeka
Jang-c	Jang	k1gFnSc4	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgInSc1d1	silniční
provoz	provoz	k1gInSc1	provoz
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
zácpy	zácpa	k1gFnPc1	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
aut	auto	k1gNnPc2	auto
ve	v	k7c6	v
městě	město	k1gNnSc6	město
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
místní	místní	k2eAgFnSc1d1	místní
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
přidělování	přidělování	k1gNnSc6	přidělování
poznávacích	poznávací	k2eAgFnPc2d1	poznávací
značek	značka	k1gFnPc2	značka
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
automobily	automobil	k1gInPc4	automobil
formou	forma	k1gFnSc7	forma
aukce	aukce	k1gFnPc1	aukce
<g/>
,	,	kIx,	,
když	když	k8xS	když
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
asi	asi	k9	asi
8000	[number]	k4	8000
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
minimální	minimální	k2eAgFnSc1d1	minimální
cena	cena	k1gFnSc1	cena
značky	značka	k1gFnSc2	značka
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
75	[number]	k4	75
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
jüanů	jüan	k1gInPc2	jüan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
285	[number]	k4	285
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
čínských	čínský	k2eAgNnPc2d1	čínské
měst	město	k1gNnPc2	město
neoplývá	oplývat	k5eNaImIp3nS	oplývat
středověkými	středověký	k2eAgFnPc7d1	středověká
památkami	památka	k1gFnPc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Dominantu	dominanta	k1gFnSc4	dominanta
města	město	k1gNnSc2	město
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
čtvrti	čtvrt	k1gFnSc6	čtvrt
Chuang-pchu	Chuangch	k1gInSc2	Chuang-pch
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
Bund	bund	k1gInSc4	bund
-	-	kIx~	-
nábřeží	nábřeží	k1gNnSc4	nábřeží
lemované	lemovaný	k2eAgFnSc2d1	lemovaná
výstavnými	výstavný	k2eAgFnPc7d1	výstavná
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
hotelů	hotel	k1gInPc2	hotel
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bund	bund	k1gInSc1	bund
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
památkově	památkově	k6eAd1	památkově
chráněn	chránit	k5eAaImNgInS	chránit
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
vyšších	vysoký	k2eAgInPc2d2	vyšší
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
není	být	k5eNaImIp3nS	být
povolena	povolen	k2eAgFnSc1d1	povolena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
moderní	moderní	k2eAgFnSc1d1	moderní
čtvrť	čtvrť	k1gFnSc1	čtvrť
Pchu-tung	Pchuung	k1gInSc1	Pchu-tung
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
mrakodrapy	mrakodrap	k1gInPc7	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Vypíná	vypínat	k5eAaImIp3nS	vypínat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
468	[number]	k4	468
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
Perla	perla	k1gFnSc1	perla
Orientu	Orient	k1gInSc2	Orient
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
dominanty	dominanta	k1gFnPc4	dominanta
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Ťin	Ťin	k1gFnSc2	Ťin
Mao	Mao	k1gFnSc2	Mao
s	s	k7c7	s
88	[number]	k4	88
podlažími	podlaží	k1gNnPc7	podlaží
(	(	kIx(	(
<g/>
Číňané	Číňan	k1gMnPc1	Číňan
považují	považovat	k5eAaImIp3nP	považovat
osmičku	osmička	k1gFnSc4	osmička
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
šťastné	šťastný	k2eAgNnSc4d1	šťastné
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
492	[number]	k4	492
<g/>
-metrový	etrový	k2eAgInSc1d1	-metrový
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Shanghai	Shangha	k1gFnSc2	Shangha
World	World	k1gMnSc1	World
Financial	Financial	k1gMnSc1	Financial
Center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
nejnovější	nový	k2eAgFnSc7d3	nejnovější
dominantou	dominanta	k1gFnSc7	dominanta
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
632	[number]	k4	632
<g/>
-metrová	etrová	k1gFnSc1	-metrová
Shanghai	Shangha	k1gFnSc2	Shangha
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Chuang-pchu	Chuangch	k1gInSc6	Chuang-pch
dominuje	dominovat	k5eAaImIp3nS	dominovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Šanghajského	šanghajský	k2eAgNnSc2d1	Šanghajské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
s	s	k7c7	s
unikátní	unikátní	k2eAgFnSc7d1	unikátní
kombinací	kombinace	k1gFnSc7	kombinace
kruhového	kruhový	k2eAgInSc2d1	kruhový
a	a	k8xC	a
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
dávnou	dávný	k2eAgFnSc4d1	dávná
čínskou	čínský	k2eAgFnSc4d1	čínská
představu	představa	k1gFnSc4	představa
kruhového	kruhový	k2eAgNnSc2d1	kruhové
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgFnPc1d1	bohatá
uměleckohistorické	uměleckohistorický	k2eAgFnPc1d1	Uměleckohistorická
sbírky	sbírka	k1gFnPc1	sbírka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
předměty	předmět	k1gInPc4	předmět
od	od	k7c2	od
nejstarších	starý	k2eAgInPc2d3	nejstarší
bronzů	bronz	k1gInPc2	bronz
z	z	k7c2	z
období	období	k1gNnSc2	období
Šang	Šanga	k1gFnPc2	Šanga
až	až	k9	až
po	po	k7c4	po
nábytek	nábytek	k1gInSc4	nábytek
z	z	k7c2	z
časů	čas	k1gInPc2	čas
dynastie	dynastie	k1gFnSc2	dynastie
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Nankingská	nankingský	k2eAgFnSc1d1	Nankingská
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
obchodních	obchodní	k2eAgFnPc2d1	obchodní
tepen	tepna	k1gFnPc2	tepna
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
buddhistickému	buddhistický	k2eAgInSc3d1	buddhistický
chrámu	chrám	k1gInSc2	chrám
Ťing-an	Ťingna	k1gFnPc2	Ťing-ana
(	(	kIx(	(
<g/>
靜	靜	k?	靜
<g/>
,	,	kIx,	,
Ťing-an	Ťingn	k1gInSc1	Ťing-an
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
Chrám	chrám	k1gInSc4	chrám
Klidu	klid	k1gInSc2	klid
a	a	k8xC	a
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obnovenému	obnovený	k2eAgInSc3d1	obnovený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
automobilový	automobilový	k2eAgInSc1d1	automobilový
okruh	okruh	k1gInSc1	okruh
pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
5,5	[number]	k4	5,5
km	km	kA	km
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
připomínající	připomínající	k2eAgInSc1d1	připomínající
znak	znak	k1gInSc1	znak
"	"	kIx"	"
<g/>
šang	šang	k1gInSc1	šang
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
上	上	k?	上
<g/>
)	)	kIx)	)
z	z	k7c2	z
názvu	název	k1gInSc2	název
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
poprvé	poprvé	k6eAd1	poprvé
jela	jet	k5eAaImAgFnS	jet
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hostila	hostit	k5eAaImAgFnS	hostit
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
Expo	Expo	k1gNnSc1	Expo
<g/>
.	.	kIx.	.
</s>
