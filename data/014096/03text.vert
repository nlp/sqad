<s>
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
markaDeutsche	markaDeutsche	k1gFnSc1
Mark	Mark	k1gMnSc1
(	(	kIx(
<g/>
němčina	němčina	k1gFnSc1
<g/>
)	)	kIx)
Jednomarková	jednomarkový	k2eAgFnSc1d1
mince	mince	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
Země	zem	k1gFnSc2
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
DEM	DEM	kA
Symbol	symbol	k1gInSc1
</s>
<s>
DM	dm	kA
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Fenik	fenik	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
Pf	pf	kA
<g/>
;	;	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
DM	dm	kA
Bankovky	bankovka	k1gFnSc2
</s>
<s>
½	½	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
DM	dm	kA
</s>
<s>
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Deutsche	Deutsche	k1gFnSc1
Mark	Mark	k1gMnSc1
či	či	k8xC
hovorově	hovorově	k6eAd1
D-Mark	D-Mark	k1gInSc1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
DEM	DEM	kA
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
také	také	k6eAd1
používáno	používat	k5eAaImNgNnS
DM	dm	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
oficiálním	oficiální	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
v	v	k7c6
Západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
později	pozdě	k6eAd2
ve	v	k7c6
Spolkové	spolkový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
až	až	k6eAd1
do	do	k7c2
nahrazení	nahrazení	k1gNnSc2
eurem	euro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
platidlo	platidlo	k1gNnSc1
ji	on	k3xPp3gFnSc4
mezi	mezi	k7c7
lety	let	k1gInPc7
1999	#num#	k4
až	až	k9
2002	#num#	k4
používalo	používat	k5eAaImAgNnS
také	také	k6eAd1
Kosovo	Kosův	k2eAgNnSc1d1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pevný	pevný	k2eAgInSc1d1
směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
k	k	k7c3
euru	euro	k1gNnSc3
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
na	na	k7c4
1	#num#	k4
EUR	euro	k1gNnPc2
=	=	kIx~
1,955	1,955	k4
<g/>
83	#num#	k4
DEM	DEM	kA
a	a	k8xC
1	#num#	k4
DEM	DEM	kA
=	=	kIx~
0,51129	0,51129	k4
EUR	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Označení	označení	k1gNnSc1
nové	nový	k2eAgFnSc2d1
měny	měna	k1gFnSc2
Trizóny	Trizóna	k1gFnSc2
vzešlo	vzejít	k5eAaPmAgNnS
z	z	k7c2
návrhu	návrh	k1gInSc2
Edwarda	Edward	k1gMnSc2
A.	A.	kA
Tenenbauma	Tenenbaum	k1gMnSc2
</s>
<s>
Na	na	k7c6
mincích	mince	k1gFnPc6
byla	být	k5eAaImAgNnP
uvedena	uvést	k5eAaPmNgNnP
písmena	písmeno	k1gNnPc1
mincovny	mincovna	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
PísmenoMěstoDoba	PísmenoMěstoDoba	k1gFnSc1
ražby	ražba	k1gFnSc2
mincí	mince	k1gFnPc2
</s>
<s>
ABerlín	ABerlín	k1gInSc1
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
DMnichov	DMnichov	k1gInSc1
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
FStuttgart	FStuttgart	k1gInSc1
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
GKarlsruhe	GKarlsruhe	k1gFnSc1
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
JHamburk	JHamburk	k1gInSc1
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ANDERSON	Anderson	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balkans	Balkans	k1gInSc1
slip	slip	k1gInSc4
quietly	quiést	k5eAaPmAgFnP
into	into	k6eAd1
eurozone	eurozon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-01-02	2002-01-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Euro	euro	k1gNnSc1
</s>
<s>
Říšská	říšský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
</s>
<s>
Východoněmecká	východoněmecký	k2eAgFnSc1d1
marka	marka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Historické	historický	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
Německa	Německo	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnPc1
nahrazené	nahrazený	k2eAgFnPc1d1
eurem	euro	k1gNnSc7
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Estonská	estonský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Francouzský	francouzský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Italská	italský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Kyperská	kyperský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Litevský	litevský	k2eAgInSc1d1
litas	litas	k1gInSc1
•	•	k?
Lotyšský	lotyšský	k2eAgInSc1d1
lat	lat	k1gInSc1
•	•	k?
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Maltská	maltský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Monacký	monacký	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Nizozemský	nizozemský	k2eAgInSc1d1
gulden	gulden	k1gInSc1
•	•	k?
Portugalské	portugalský	k2eAgNnSc4d1
escudo	escudo	k1gNnSc4
•	•	k?
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
drachma	drachma	k1gFnSc1
•	•	k?
Sanmarinská	Sanmarinský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Slovinský	slovinský	k2eAgInSc1d1
tolar	tolar	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
peseta	peseta	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4012555-5	4012555-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
94004493	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
94004493	#num#	k4
</s>
