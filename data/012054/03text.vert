<p>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
I.	I.	kA	I.
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
852	[number]	k4	852
a	a	k8xC	a
855	[number]	k4	855
–	–	k?	–
mezi	mezi	k7c7	mezi
888	[number]	k4	888
a	a	k8xC	a
890	[number]	k4	890
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Borzivogius	Borzivogius	k1gInSc1	Borzivogius
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
historicky	historicky	k6eAd1	historicky
doložený	doložený	k2eAgMnSc1d1	doložený
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Ludmilou	Ludmila	k1gFnSc7	Ludmila
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc7d2	pozdější
světicí	světice	k1gFnSc7	světice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgInPc4	první
kostely	kostel	k1gInPc4	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
přenesl	přenést	k5eAaPmAgMnS	přenést
knížecí	knížecí	k2eAgNnSc4d1	knížecí
sídlo	sídlo	k1gNnSc4	sídlo
z	z	k7c2	z
Levého	levý	k2eAgInSc2d1	levý
Hradce	Hradec	k1gInSc2	Hradec
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
vnukem	vnuk	k1gMnSc7	vnuk
byl	být	k5eAaImAgMnS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
je	být	k5eAaImIp3nS	být
vykreslen	vykreslit	k5eAaPmNgMnS	vykreslit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
vládce	vládce	k1gMnSc1	vládce
Čechů	Čech	k1gMnPc2	Čech
v	v	k7c6	v
ludmilské	ludmilský	k2eAgFnSc6d1	ludmilská
legendě	legenda	k1gFnSc6	legenda
Fuit	Fuit	k1gMnSc1	Fuit
in	in	k?	in
provincia	provincia	k1gFnSc1	provincia
Boemorum	Boemorum	k1gInSc1	Boemorum
<g/>
,	,	kIx,	,
Kristiánově	Kristiánův	k2eAgFnSc6d1	Kristiánova
legendě	legenda	k1gFnSc6	legenda
i	i	k8xC	i
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Kosmovy	Kosmův	k2eAgFnSc2d1	Kosmova
kroniky	kronika	k1gFnSc2	kronika
byl	být	k5eAaImAgMnS	být
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
bájného	bájný	k2eAgMnSc2d1	bájný
knížete	kníže	k1gMnSc2	kníže
Hostivíta	Hostivít	k1gMnSc2	Hostivít
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
potomek	potomek	k1gMnSc1	potomek
mýtické	mýtický	k2eAgFnSc2d1	mýtická
kněžny	kněžna	k1gFnSc2	kněžna
Libuše	Libuše	k1gFnSc2	Libuše
a	a	k8xC	a
Přemysla	Přemysl	k1gMnSc2	Přemysl
Oráče	oráč	k1gMnSc2	oráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
franské	franský	k2eAgInPc1d1	franský
Fuldské	Fuldský	k2eAgInPc1d1	Fuldský
letopisy	letopis	k1gInPc1	letopis
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
píší	psát	k5eAaImIp3nP	psát
jako	jako	k9	jako
o	o	k7c6	o
knížeti	kníže	k1gMnSc6	kníže
Goriweiovi	Goriweius	k1gMnSc6	Goriweius
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
872	[number]	k4	872
tyto	tento	k3xDgInPc1	tento
letopisy	letopis	k1gInPc1	letopis
uvádí	uvádět	k5eAaImIp3nP	uvádět
jména	jméno	k1gNnPc4	jméno
několika	několik	k4yIc2	několik
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
květnu	květen	k1gInSc6	květen
poslal	poslat	k5eAaPmAgMnS	poslat
proti	proti	k7c3	proti
moravským	moravský	k2eAgMnPc3d1	moravský
Slovanům	Slovan	k1gMnPc3	Slovan
Durynky	Durynk	k1gMnPc7	Durynk
a	a	k8xC	a
Sasy	Sas	k1gMnPc7	Sas
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
s	s	k7c7	s
sebou	se	k3xPyFc7	se
neměli	mít	k5eNaImAgMnP	mít
krále	král	k1gMnPc4	král
a	a	k8xC	a
nechtěli	chtít	k5eNaImAgMnP	chtít
být	být	k5eAaImF	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
svorní	svorný	k2eAgMnPc1d1	svorný
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
před	před	k7c7	před
nepřáteli	nepřítel	k1gMnPc7	nepřítel
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
ztratili	ztratit	k5eAaPmAgMnP	ztratit
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
svých	svůj	k3xOyFgMnPc2	svůj
[	[	kIx(	[
<g/>
lidí	člověk	k1gMnPc2	člověk
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
s	s	k7c7	s
hanbou	hanba	k1gFnSc7	hanba
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Ba	ba	k9	ba
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc1	některý
hrabata	hrabě	k1gNnPc1	hrabě
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
ženičkami	ženička	k1gFnPc7	ženička
té	ten	k3xDgFnSc2	ten
země	zem	k1gFnSc2	zem
zbita	zbít	k5eAaPmNgFnS	zbít
a	a	k8xC	a
kyji	kyj	k1gInSc6	kyj
shazována	shazován	k2eAgFnSc1d1	shazována
s	s	k7c7	s
koní	koní	k2eAgFnSc7d1	koní
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byli	být	k5eAaImAgMnP	být
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
Franků	Frank	k1gMnPc2	Frank
posláni	poslat	k5eAaPmNgMnP	poslat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Karlomanovi	Karlomanův	k2eAgMnPc1d1	Karlomanův
proti	proti	k7c3	proti
uvedeným	uvedený	k2eAgMnPc3d1	uvedený
Slovanům	Slovan	k1gMnPc3	Slovan
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
byli	být	k5eAaImAgMnP	být
určeni	určit	k5eAaPmNgMnP	určit
proti	proti	k7c3	proti
Čechům	Čech	k1gMnPc3	Čech
<g/>
.	.	kIx.	.
<g/>
Ti	ten	k3xDgMnPc1	ten
a	a	k8xC	a
pět	pět	k4xCc4	pět
knížat	kníže	k1gNnPc2	kníže
těchto	tento	k3xDgNnPc2	tento
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
Svatoslav	Svatoslav	k1gMnSc1	Svatoslav
<g/>
,	,	kIx,	,
Vitislav	Vitislav	k1gMnSc1	Vitislav
<g/>
,	,	kIx,	,
Heriman	Heriman	k1gMnSc1	Heriman
<g/>
,	,	kIx,	,
Spytimír	Spytimír	k1gMnSc1	Spytimír
<g/>
,	,	kIx,	,
Mojslav	Mojslav	k1gMnSc1	Mojslav
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Goriwei	Goriwei	k1gNnSc2	Goriwei
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
lidu	lid	k1gInSc2	lid
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
obrátili	obrátit	k5eAaPmAgMnP	obrátit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiná	k1gFnPc4	jiná
pobili	pobít	k5eAaPmAgMnP	pobít
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
zranili	zranit	k5eAaPmAgMnP	zranit
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
utonuli	utonout	k5eAaPmAgMnP	utonout
též	též	k9	též
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
mohli	moct	k5eAaImAgMnP	moct
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Goriwei	Goriwe	k1gFnSc2	Goriwe
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
dopsáno	dopsán	k2eAgNnSc1d1	dopsáno
jako	jako	k8xC	jako
šesté	šestý	k4xOgNnSc1	šestý
a	a	k8xC	a
uvedeno	uvést	k5eAaPmNgNnS	uvést
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
opisů	opis	k1gInPc2	opis
análů	anály	k1gInPc2	anály
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
připsané	připsaný	k2eAgNnSc1d1	připsané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bořivojův	Bořivojův	k2eAgInSc4d1	Bořivojův
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Bořivojovým	Bořivojův	k2eAgNnSc7d1	Bořivojovo
sídlem	sídlo	k1gNnSc7	sídlo
snad	snad	k9	snad
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
Levý	levý	k2eAgInSc1d1	levý
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nebyli	být	k5eNaImAgMnP	být
jediným	jediný	k2eAgInSc7d1	jediný
důležitým	důležitý	k2eAgInSc7d1	důležitý
rodem	rod	k1gInSc7	rod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
velkomoravských	velkomoravský	k2eAgNnPc2d1	velkomoravské
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
postupným	postupný	k2eAgNnSc7d1	postupné
budováním	budování	k1gNnSc7	budování
mocenského	mocenský	k2eAgNnSc2d1	mocenské
postavení	postavení	k1gNnSc2	postavení
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
získali	získat	k5eAaPmAgMnP	získat
pozici	pozice	k1gFnSc4	pozice
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
totiž	totiž	k9	totiž
ovládali	ovládat	k5eAaImAgMnP	ovládat
středočeský	středočeský	k2eAgInSc4d1	středočeský
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
klíčový	klíčový	k2eAgInSc1d1	klíčový
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgNnPc4d1	jediné
dvě	dva	k4xCgNnPc4	dva
jistěji	jistě	k6eAd2	jistě
doložená	doložený	k2eAgNnPc4d1	doložené
mocenská	mocenský	k2eAgNnPc4d1	mocenské
centra	centrum	k1gNnPc4	centrum
známe	znát	k5eAaImIp1nP	znát
jen	jen	k9	jen
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
jižních	jižní	k2eAgFnPc2d1	jižní
a	a	k8xC	a
severozápadních	severozápadní	k2eAgFnPc2d1	severozápadní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Velkomoravský	velkomoravský	k2eAgInSc1d1	velkomoravský
svého	svůj	k3xOyFgMnSc4	svůj
zhruba	zhruba	k6eAd1	zhruba
patnáctiletého	patnáctiletý	k2eAgMnSc4d1	patnáctiletý
chráněnce	chráněnec	k1gMnSc4	chráněnec
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roku	rok	k1gInSc6	rok
867	[number]	k4	867
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
knížetem	kníže	k1gNnSc7wR	kníže
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
jistě	jistě	k9	jistě
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
ze	z	k7c2	z
zkušenějších	zkušený	k2eAgMnPc2d2	zkušenější
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
argumentů	argument	k1gInPc2	argument
zastánců	zastánce	k1gMnPc2	zastánce
teorie	teorie	k1gFnSc2	teorie
příbuzenství	příbuzenství	k1gNnSc1	příbuzenství
velkomoravských	velkomoravský	k2eAgMnPc2d1	velkomoravský
Mojmírovců	Mojmírovec	k1gMnPc2	Mojmírovec
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
možná	možná	k9	možná
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
Svatoplukově	Svatoplukův	k2eAgInSc6d1	Svatoplukův
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
874	[number]	k4	874
nebo	nebo	k8xC	nebo
875	[number]	k4	875
byl	být	k5eAaImAgMnS	být
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Ludmilou	Ludmila	k1gFnSc7	Ludmila
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
svatou	svatá	k1gFnSc4	svatá
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Pšovanů	Pšovan	k1gMnPc2	Pšovan
nebo	nebo	k8xC	nebo
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Srbů	Srb	k1gMnPc2	Srb
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupná	postupný	k2eAgFnSc1d1	postupná
eliminace	eliminace	k1gFnSc1	eliminace
knížecích	knížecí	k2eAgMnPc2d1	knížecí
rodů	rod	k1gInPc2	rod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zdaleka	zdaleka	k6eAd1	zdaleka
neprobíhala	probíhat	k5eNaImAgFnS	probíhat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
jistě	jistě	k6eAd1	jistě
sehrávala	sehrávat	k5eAaImAgFnS	sehrávat
sňatková	sňatkový	k2eAgFnSc1d1	sňatková
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
sňatek	sňatek	k1gInSc1	sňatek
Ludmily	Ludmila	k1gFnSc2	Ludmila
a	a	k8xC	a
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
několik	několik	k4yIc1	několik
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
doloženi	doložen	k2eAgMnPc1d1	doložen
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
následníci	následník	k1gMnPc1	následník
Spytihněv	Spytihněv	k1gFnPc2	Spytihněv
I.	I.	kA	I.
a	a	k8xC	a
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
Vratislavovými	Vratislavův	k2eAgMnPc7d1	Vratislavův
syny	syn	k1gMnPc7	syn
a	a	k8xC	a
Bořivojovými	Bořivojův	k2eAgMnPc7d1	Bořivojův
vnuky	vnuk	k1gMnPc7	vnuk
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
Svatý	svatý	k1gMnSc1	svatý
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Kristiánovy	Kristiánův	k2eAgFnSc2d1	Kristiánova
legendy	legenda	k1gFnSc2	legenda
přijal	přijmout	k5eAaPmAgMnS	přijmout
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
křest	křest	k1gInSc1	křest
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
od	od	k7c2	od
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
legendy	legenda	k1gFnSc2	legenda
musel	muset	k5eAaImAgMnS	muset
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgMnS	dostavit
ke	k	k7c3	k
knížeti	kníže	k1gMnSc3	kníže
Svatoplukovi	Svatopluk	k1gMnSc3	Svatopluk
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
sedět	sedět	k5eAaImF	sedět
po	po	k7c6	po
způsobu	způsob	k1gInSc6	způsob
pohanů	pohan	k1gMnPc2	pohan
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
před	před	k7c7	před
stolem	stol	k1gInSc7	stol
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
křesťané	křesťan	k1gMnPc1	křesťan
seděli	sedět	k5eAaImAgMnP	sedět
na	na	k7c6	na
sedadlech	sedadlo	k1gNnPc6	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Metoděj	Metoděj	k1gMnSc1	Metoděj
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jaká	jaký	k3yIgFnSc1	jaký
běda	běda	k1gFnSc1	běda
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
tak	tak	k8xC	tak
vynikající	vynikající	k2eAgMnSc1d1	vynikající
<g/>
,	,	kIx,	,
a	a	k8xC	a
nestydíš	stydět	k5eNaImIp2nS	stydět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
žes	žes	kA	žes
vyhoštěn	vyhoštěn	k2eAgInSc4d1	vyhoštěn
ze	z	k7c2	z
sedadel	sedadlo	k1gNnPc2	sedadlo
knížecích	knížecí	k2eAgMnPc2d1	knížecí
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
sám	sám	k3xTgInSc4	sám
také	také	k9	také
vévodskou	vévodský	k2eAgFnSc4d1	vévodská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
hodnost	hodnost	k1gFnSc4	hodnost
máš	mít	k5eAaImIp2nS	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
raději	rád	k6eAd2	rád
chceš	chtít	k5eAaImIp2nS	chtít
pro	pro	k7c4	pro
hanebnou	hanebný	k2eAgFnSc4d1	hanebná
modloslužbu	modloslužba	k1gFnSc4	modloslužba
s	s	k7c7	s
pasáky	pasák	k1gMnPc7	pasák
sviní	svinit	k5eAaImIp3nP	svinit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
sedět	sedět	k5eAaImF	sedět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
dal	dát	k5eAaPmAgInS	dát
pohnout	pohnout	k5eAaPmF	pohnout
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijal	přijmout	k5eAaPmAgInS	přijmout
křest	křest	k1gInSc1	křest
<g/>
.	.	kIx.	.
<g/>
Datování	datování	k1gNnSc1	datování
křtu	křest	k1gInSc2	křest
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
k	k	k7c3	k
roku	rok	k1gInSc3	rok
883	[number]	k4	883
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
r.	r.	kA	r.
845	[number]	k4	845
pokřtěno	pokřtěn	k2eAgNnSc1d1	pokřtěno
14	[number]	k4	14
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
(	(	kIx(	(
<g/>
duces	duces	k1gInSc1	duces
Boemanorum	Boemanorum	k1gNnSc1	Boemanorum
<g/>
)	)	kIx)	)
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
bavorském	bavorský	k2eAgNnSc6d1	bavorské
Řezně	Řezno	k1gNnSc6	Řezno
(	(	kIx(	(
<g/>
Regensburg	Regensburg	k1gMnSc1	Regensburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bořivojova	Bořivojův	k2eAgFnSc1d1	Bořivojova
konverze	konverze	k1gFnSc1	konverze
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
skutečný	skutečný	k2eAgInSc4d1	skutečný
počátek	počátek	k1gInSc4	počátek
christianizace	christianizace	k1gFnSc2	christianizace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Křtem	křest	k1gInSc7	křest
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
čelil	čelit	k5eAaImAgMnS	čelit
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
expanzivním	expanzivní	k2eAgFnPc3d1	expanzivní
snahám	snaha	k1gFnPc3	snaha
východofranských	východofranský	k2eAgMnPc2d1	východofranský
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
bavorských	bavorský	k2eAgMnPc2d1	bavorský
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zakladatelská	zakladatelský	k2eAgFnSc1d1	zakladatelská
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
o	o	k7c6	o
Bořivojovi	Bořivoj	k1gMnSc6	Bořivoj
příliš	příliš	k6eAd1	příliš
jistého	jistý	k2eAgMnSc2d1	jistý
neví	vědět	k5eNaImIp3nS	vědět
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
zahalena	zahalit	k5eAaPmNgFnS	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
doložit	doložit	k5eAaPmF	doložit
jeho	jeho	k3xOp3gFnSc4	jeho
zakladatelskou	zakladatelský	k2eAgFnSc4d1	zakladatelská
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
Levém	levý	k2eAgInSc6d1	levý
Hradci	Hradec	k1gInSc6	Hradec
postavit	postavit	k5eAaPmF	postavit
první	první	k4xOgInSc4	první
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
rotundu	rotunda	k1gFnSc4	rotunda
zasvěcenou	zasvěcený	k2eAgFnSc4d1	zasvěcená
svatému	svatý	k2eAgMnSc3d1	svatý
Klimentovi	Kliment	k1gMnSc3	Kliment
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
začalo	začít	k5eAaPmAgNnS	začít
pomalu	pomalu	k6eAd1	pomalu
šířit	šířit	k5eAaImF	šířit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
přijetí	přijetí	k1gNnSc1	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
až	až	k9	až
ke	k	k7c3	k
vzpouře	vzpoura	k1gFnSc3	vzpoura
českých	český	k2eAgMnPc2d1	český
velmožů	velmož	k1gMnPc2	velmož
a	a	k8xC	a
Bořivojovu	Bořivojův	k2eAgNnSc3d1	Bořivojovo
krátkodobému	krátkodobý	k2eAgNnSc3d1	krátkodobé
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
byl	být	k5eAaImAgInS	být
knížetem	kníže	k1gNnSc7wR	kníže
Strojmír	Strojmíra	k1gFnPc2	Strojmíra
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
žijící	žijící	k2eAgMnSc1d1	žijící
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
Svatoplukovou	Svatoplukův	k2eAgFnSc7d1	Svatoplukova
pomocí	pomoc	k1gFnSc7	pomoc
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
povstání	povstání	k1gNnSc2	povstání
potlačil	potlačit	k5eAaPmAgMnS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
naopak	naopak	k6eAd1	naopak
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strojmíra	Strojmíra	k1gFnSc1	Strojmíra
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
sami	sám	k3xTgMnPc1	sám
Čechové	Čech	k1gMnPc1	Čech
a	a	k8xC	a
potom	potom	k6eAd1	potom
Bořivoje	Bořivoj	k1gMnSc4	Bořivoj
znovu	znovu	k6eAd1	znovu
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
povolali	povolat	k5eAaPmAgMnP	povolat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dal	dát	k5eAaPmAgMnS	dát
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
další	další	k2eAgInSc1d1	další
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
kostelík	kostelík	k1gInSc1	kostelík
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
tentokrát	tentokrát	k6eAd1	tentokrát
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc6	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
vrcholku	vrcholek	k1gInSc2	vrcholek
Žiži	Žiži	k1gNnSc2	Žiži
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
názoru	názor	k1gInSc2	názor
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Petr	Petr	k1gMnSc1	Petr
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
)	)	kIx)	)
předkřesťanským	předkřesťanský	k2eAgNnSc7d1	předkřesťanské
posvátným	posvátný	k2eAgNnSc7d1	posvátné
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
stál	stát	k5eAaImAgInS	stát
také	také	k9	také
kamenný	kamenný	k2eAgInSc1d1	kamenný
knížecí	knížecí	k2eAgInSc1d1	knížecí
stolec	stolec	k1gInSc1	stolec
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
byli	být	k5eAaImAgMnP	být
obřadně	obřadně	k6eAd1	obřadně
usazováni	usazován	k2eAgMnPc1d1	usazován
noví	nový	k2eAgMnPc1d1	nový
vládcové	vládce	k1gMnPc1	vládce
<g/>
,	,	kIx,	,
vybraní	vybraný	k2eAgMnPc1d1	vybraný
předáky	předák	k1gMnPc7	předák
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
historici	historik	k1gMnPc1	historik
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sem	sem	k6eAd1	sem
poté	poté	k6eAd1	poté
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
z	z	k7c2	z
Levého	levý	k2eAgInSc2d1	levý
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
budování	budování	k1gNnSc1	budování
hradiště	hradiště	k1gNnSc2	hradiště
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
ostrožně	ostrožna	k1gFnSc6	ostrožna
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
spíše	spíše	k9	spíše
Bořivojovu	Bořivojův	k2eAgMnSc3d1	Bořivojův
nástupci	nástupce	k1gMnSc3	nástupce
Spytihněvovi	Spytihněva	k1gMnSc3	Spytihněva
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
==	==	k?	==
</s>
</p>
<p>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
888	[number]	k4	888
<g/>
-	-	kIx~	-
<g/>
890	[number]	k4	890
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
třetího	třetí	k4xOgNnSc2	třetí
desetiletí	desetiletí	k1gNnSc2	desetiletí
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
převzal	převzít	k5eAaPmAgMnS	převzít
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
vládu	vláda	k1gFnSc4	vláda
velkomoravský	velkomoravský	k2eAgMnSc1d1	velkomoravský
kníže	kníže	k1gMnSc1	kníže
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
894	[number]	k4	894
se	se	k3xPyFc4	se
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
stal	stát	k5eAaPmAgMnS	stát
Bořivojův	Bořivojův	k2eAgMnSc1d1	Bořivojův
syn	syn	k1gMnSc1	syn
Spytihněv	Spytihněv	k1gFnSc2	Spytihněv
I.	I.	kA	I.
Není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
pochován	pochován	k2eAgMnSc1d1	pochován
<g/>
,	,	kIx,	,
hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
jím	on	k3xPp3gMnSc7	on
založeném	založený	k2eAgInSc6d1	založený
kostele	kostel	k1gInSc6	kostel
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
zůstal	zůstat	k5eAaPmAgInS	zůstat
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
jeho	jeho	k3xOp3gInSc2	jeho
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
určil	určit	k5eAaPmAgMnS	určit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
tzv.	tzv.	kA	tzv.
hrob	hrob	k1gInSc1	hrob
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
svatovítské	svatovítský	k2eAgFnSc2d1	Svatovítská
rotundy	rotunda	k1gFnSc2	rotunda
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosterní	kosterní	k2eAgInPc4d1	kosterní
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
nepatří	patřit	k5eNaImIp3nP	patřit
Bořivojovi	Bořivojův	k2eAgMnPc1d1	Bořivojův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
byl	být	k5eAaImAgMnS	být
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc4	takový
tvrzení	tvrzení	k1gNnPc4	tvrzení
těžko	těžko	k6eAd1	těžko
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
prokazatelná	prokazatelný	k2eAgFnSc1d1	prokazatelná
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
určité	určitý	k2eAgInPc4d1	určitý
argumenty	argument	k1gInPc4	argument
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antropologických	antropologický	k2eAgInPc2d1	antropologický
výzkumů	výzkum	k1gInPc2	výzkum
ostatků	ostatek	k1gInPc2	ostatek
sdílí	sdílet	k5eAaImIp3nS	sdílet
lebka	lebka	k1gFnSc1	lebka
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
K1	K1	k1gFnSc4	K1
s	s	k7c7	s
lebkou	lebka	k1gFnSc7	lebka
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
59	[number]	k4	59
v	v	k7c6	v
Sadech	sad	k1gInPc6	sad
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
(	(	kIx(	(
<g/>
považovaného	považovaný	k2eAgMnSc2d1	považovaný
zejména	zejména	k9	zejména
historikem	historik	k1gMnSc7	historik
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Emilem	Emil	k1gMnSc7	Emil
Havlíkem	Havlík	k1gMnSc7	Havlík
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k8xC	i
archeologem	archeolog	k1gMnSc7	archeolog
Luďkem	Luděk	k1gMnSc7	Luděk
Galuškou	Galuška	k1gMnSc7	Galuška
za	za	k7c4	za
hrob	hrob	k1gInSc4	hrob
knížete	kníže	k1gMnSc2	kníže
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
)	)	kIx)	)
ojedinělou	ojedinělý	k2eAgFnSc4d1	ojedinělá
anomálii	anomálie	k1gFnSc4	anomálie
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
kostry	kostra	k1gFnPc1	kostra
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
stejnou	stejný	k2eAgFnSc4d1	stejná
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
tehdy	tehdy	k6eAd1	tehdy
běžně	běžně	k6eAd1	běžně
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
byl	být	k5eAaImAgMnS	být
Svatoplukův	Svatoplukův	k2eAgMnSc1d1	Svatoplukův
blízký	blízký	k2eAgMnSc1d1	blízký
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
snad	snad	k9	snad
syn	syn	k1gMnSc1	syn
velkomoravského	velkomoravský	k2eAgMnSc2d1	velkomoravský
knížete	kníže	k1gMnSc2	kníže
Rostislava	Rostislav	k1gMnSc2	Rostislav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
–	–	k?	–
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
Zrození	zrození	k1gNnSc1	zrození
státu	stát	k1gInSc2	stát
872	[number]	k4	872
<g/>
–	–	k?	–
<g/>
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
po	po	k7c4	po
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Vládkyně	vládkyně	k1gFnSc1	vládkyně
a	a	k8xC	a
světice	světice	k1gFnSc1	světice
<g/>
,	,	kIx,	,
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
133	[number]	k4	133
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902129	[number]	k4	902129
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
-	-	kIx~	-
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Boh	Boh	k1gFnSc1	Boh
<g/>
–	–	k?	–
<g/>
Bož	Boža	k1gFnPc2	Boža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
78	[number]	k4	78
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Vlček	Vlček	k1gMnSc1	Vlček
-	-	kIx~	-
Genetické	genetický	k2eAgFnSc2d1	genetická
vazby	vazba	k1gFnSc2	vazba
u	u	k7c2	u
nejstarších	starý	k2eAgMnPc2d3	nejstarší
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Moravia	Moravius	k1gMnSc2	Moravius
magna	magn	k1gMnSc2	magn
</s>
</p>
