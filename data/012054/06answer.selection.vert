<s>
Kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Ludmilou	Ludmila	k1gFnSc7	Ludmila
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc7d2	pozdější
světicí	světice	k1gFnSc7	světice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgInPc4	první
kostely	kostel	k1gInPc4	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
přenesl	přenést	k5eAaPmAgMnS	přenést
knížecí	knížecí	k2eAgNnSc4d1	knížecí
sídlo	sídlo	k1gNnSc4	sídlo
z	z	k7c2	z
Levého	levý	k2eAgInSc2d1	levý
Hradce	Hradec	k1gInSc2	Hradec
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
