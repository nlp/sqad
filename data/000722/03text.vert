<s>
E.T.	E.T.	k?	E.T.
-	-	kIx~	-
Mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
(	(	kIx(	(
<g/>
E.T.	E.T.	k1gMnSc1	E.T.
the	the	k?	the
Extra-Terrestrial	Extra-Terrestrial	k1gMnSc1	Extra-Terrestrial
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
sci-fi	scii	k1gFnPc4	sci-fi
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
filmu	film	k1gInSc2	film
napsala	napsat	k5eAaPmAgFnS	napsat
Melissa	Melissa	k1gFnSc1	Melissa
Mathisonová	Mathisonová	k1gFnSc1	Mathisonová
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
jej	on	k3xPp3gInSc4	on
Steven	Steven	k2eAgInSc4d1	Steven
Spielberg	Spielberg	k1gInSc4	Spielberg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
popisováni	popisován	k2eAgMnPc1d1	popisován
jako	jako	k8xC	jako
mírumilovní	mírumilovný	k2eAgMnPc1d1	mírumilovný
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
malého	malý	k2eAgMnSc2d1	malý
chlapce	chlapec	k1gMnSc2	chlapec
Elliota	Elliot	k1gMnSc2	Elliot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
s	s	k7c7	s
mimozemšťanem	mimozemšťan	k1gMnSc7	mimozemšťan
ztraceným	ztracený	k2eAgMnSc7d1	ztracený
při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mimozemšťanovi	mimozemšťanův	k2eAgMnPc1d1	mimozemšťanův
stýská	stýskat	k5eAaImIp3nS	stýskat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
najít	najít	k5eAaPmF	najít
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
dostat	dostat	k5eAaPmF	dostat
zpět	zpět	k6eAd1	zpět
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
E.T.	E.T.	k1gFnSc2	E.T.
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
italský	italský	k2eAgMnSc1d1	italský
mistr	mistr	k1gMnSc1	mistr
speciálních	speciální	k2eAgInPc2d1	speciální
efektů	efekt	k1gInPc2	efekt
Carlo	Carlo	k1gNnSc4	Carlo
Rambaldi	Rambald	k1gMnPc1	Rambald
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
hororového	hororový	k2eAgMnSc2d1	hororový
režiséra	režisér	k1gMnSc2	režisér
Daria	Daria	k1gFnSc1	Daria
Argenta	argentum	k1gNnSc2	argentum
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
oceněn	ocenit	k5eAaPmNgInS	ocenit
i	i	k9	i
Oscarem	Oscar	k1gInSc7	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
byl	být	k5eAaImAgMnS	být
ovládán	ovládat	k5eAaImNgMnS	ovládat
zevnitř	zevnitř	k6eAd1	zevnitř
liliputem	liliput	k1gMnSc7	liliput
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
málem	málem	k6eAd1	málem
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
speciálních	speciální	k2eAgInPc6d1	speciální
efektech	efekt	k1gInPc6	efekt
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
mimozemšťana	mimozemšťan	k1gMnSc4	mimozemšťan
rozsvěcel	rozsvěcet	k5eAaImAgInS	rozsvěcet
a	a	k8xC	a
zhasínal	zhasínat	k5eAaImAgInS	zhasínat
lampy	lampa	k1gFnPc4	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
manipulací	manipulace	k1gFnPc2	manipulace
se	se	k3xPyFc4	se
vnitřek	vnitřek	k1gInSc1	vnitřek
vzňal	vznít	k5eAaPmAgInS	vznít
<g/>
.	.	kIx.	.
</s>
<s>
E.T.	E.T.	k?	E.T.
-	-	kIx~	-
Mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejvýdělečnějším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
filmem	film	k1gInSc7	film
historie	historie	k1gFnSc2	historie
kinematografie	kinematografie	k1gFnSc2	kinematografie
(	(	kIx(	(
<g/>
při	při	k7c6	při
započítání	započítání	k1gNnSc6	započítání
inflace	inflace	k1gFnSc2	inflace
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
poslední	poslední	k2eAgFnSc4d1	poslední
filmem	film	k1gInSc7	film
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
utržily	utržit	k5eAaPmAgFnP	utržit
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
dolarů	dolar	k1gInPc2	dolar
tržeb	tržba	k1gFnPc2	tržba
(	(	kIx(	(
<g/>
po	po	k7c6	po
přepočtení	přepočtení	k1gNnSc6	přepočtení
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
dolaru	dolar	k1gInSc2	dolar
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
1981	[number]	k4	1981
za	za	k7c2	za
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
režisér	režisér	k1gMnSc1	režisér
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nápad	nápad	k1gInSc1	nápad
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
provedl	provést	k5eAaPmAgMnS	provést
některé	některý	k3yIgFnPc4	některý
změny	změna	k1gFnPc4	změna
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
necelých	celý	k2eNgFnPc2d1	necelá
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgInP	být
součástí	součást	k1gFnSc7	součást
originálního	originální	k2eAgNnSc2d1	originální
vydání	vydání	k1gNnSc2	vydání
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
čtyři	čtyři	k4xCgInPc4	čtyři
Oscary	Oscar	k1gInPc4	Oscar
<g/>
:	:	kIx,	:
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
