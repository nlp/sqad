<s>
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
Savojský	savojský	k2eAgMnSc1d1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
(	(	kIx(
<g/>
1699	#num#	k4
<g/>
–	–	k?
<g/>
1715	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1699	#num#	k4
<g/>
Turín	Turín	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1715	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c6
Turíně	Turín	k1gInSc6
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
pravé	pravý	k2eAgFnPc1d1
neštovice	neštovice	k1gFnPc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Bazilika	bazilika	k1gFnSc1
Superga	Superga	k1gFnSc1
Povolání	povolání	k1gNnPc1
</s>
<s>
regent	regent	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Orléanská	Orléanský	k2eAgFnSc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Karel	Karel	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Adelaide	Adelaid	k1gInSc5
Savojská	savojský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
a	a	k8xC
Vittorio	Vittorio	k6eAd1
Francesco	Francesco	k1gNnSc1
<g/>
,	,	kIx,
Marquis	Marquis	k1gFnSc1
of	of	k?
Susa	Susa	k1gFnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnSc3
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
regent	regent	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
Savojský	savojský	k2eAgInSc1d1
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Vittorio	Vittorio	k6eAd1
Amedeo	Amedeo	k6eAd1
Filippo	Filippa	k1gFnSc5
Giuseppe	Giusepp	k1gInSc5
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1699	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1715	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
nejstarším	starý	k2eAgMnSc7d3
synem	syn	k1gMnSc7
Viktora	Viktor	k1gMnSc2
Amadea	Amadeus	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
francouzské	francouzský	k2eAgFnPc1d1
manželky	manželka	k1gFnPc1
Anny	Anna	k1gFnSc2
Marie	Maria	k1gFnSc2
Orleánské	orleánský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Turíně	Turín	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1699	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pokřtěn	pokřtít	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
Jan	Jan	k1gMnSc1
Filip	Filip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Královském	královský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1715	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
neštovice	neštovice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
Bazilice	bazilika	k1gFnSc6
Superga	Superga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1717	#num#	k4
až	až	k9
1731	#num#	k4
na	na	k7c4
objednávku	objednávka	k1gFnSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Savoy	Savoa	k1gFnPc4
4	#num#	k4
<g/>
.	.	kIx.
genealogy	genealog	k1gMnPc7
<g/>
.	.	kIx.
<g/>
euweb	euwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AMMON	AMMON	kA
<g/>
,	,	kIx,
Christoph	Christoph	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
von	von	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genealogie	genealogie	k1gFnSc1
ascendante	ascendant	k1gMnSc5
jusqu	jusqu	k5eAaPmIp1nS
<g/>
'	'	kIx"
<g/>
au	au	k0
quatrieme	quatriit	k5eAaImRp1nP,k5eAaPmRp1nP
degre	degr	k1gInSc5
inclusivement	inclusivement	k1gInSc4
de	de	k?
tous	tous	k1gInSc1
les	les	k1gInSc1
Rois	Rois	k1gInSc1
et	et	k?
Princes	princes	k1gInSc1
de	de	k?
maisons	maisons	k1gInSc1
souveraines	souveraines	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Europe	Europ	k1gInSc5
actuellement	actuellement	k1gMnSc1
vivans	vivansit	k5eAaPmRp2nS
<g/>
,	,	kIx,
reduite	reduit	k1gMnSc5
en	en	k?
114	#num#	k4
tables	tables	k1gInSc1
....	....	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Aux	Aux	k1gFnSc1
Depens	Depens	k1gInSc1
de	de	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
Auteur	Auteur	k1gMnSc1
<g/>
,	,	kIx,
Se	s	k7c7
Vend	Vend	k1gMnSc1
Chez	Chez	k1gMnSc1
Etienne	Etienn	k1gInSc5
de	de	k?
Bourdeaux	Bourdeaux	k1gInSc1
144	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gMnSc1
<g/>
:	:	kIx,
AINPAAAAcAAJ	AINPAAAAcAAJ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
(	(	kIx(
<g/>
1699	#num#	k4
<g/>
–	–	k?
<g/>
1715	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
5094	#num#	k4
2127	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
316737173	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Novověk	novověk	k1gInSc1
</s>
