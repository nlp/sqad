<s>
Viktor	Viktor	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Královském	královský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1715	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
neštovice	neštovice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
Bazilice	bazilika	k1gFnSc6
Superga	Superga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1717	#num#	k4
až	až	k9
1731	#num#	k4
na	na	k7c4
objednávku	objednávka	k1gFnSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>