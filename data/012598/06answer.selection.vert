<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Macůška	Macůška	k1gFnSc1	Macůška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
propast	propast	k1gFnSc1	propast
v	v	k7c6	v
Hranickém	hranický	k2eAgInSc6d1	hranický
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Bečvy	Bečva	k1gFnSc2	Bečva
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Hůrka	hůrka	k1gFnSc1	hůrka
u	u	k7c2	u
Hranic	Hranice	k1gFnPc2	Hranice
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
Hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
