<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Macůška	Macůška	k1gFnSc1	Macůška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
propast	propast	k1gFnSc1	propast
v	v	k7c6	v
Hranickém	hranický	k2eAgInSc6d1	hranický
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Bečvy	Bečva	k1gFnSc2	Bečva
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Hůrka	hůrka	k1gFnSc1	hůrka
u	u	k7c2	u
Hranic	Hranice	k1gFnPc2	Hranice
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
Hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
suché	suchý	k2eAgFnSc2d1	suchá
části	část	k1gFnSc2	část
propasti	propast	k1gFnSc2	propast
je	být	k5eAaImIp3nS	být
69,5	[number]	k4	69,5
m	m	kA	m
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
zatopené	zatopený	k2eAgFnSc2d1	zatopená
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
404	[number]	k4	404
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejhlubší	hluboký	k2eAgFnSc4d3	nejhlubší
zatopenou	zatopený	k2eAgFnSc4d1	zatopená
jeskyni	jeskyně	k1gFnSc4	jeskyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
