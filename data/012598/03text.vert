<p>
<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Macůška	Macůška	k1gFnSc1	Macůška
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
propast	propast	k1gFnSc1	propast
v	v	k7c6	v
Hranickém	hranický	k2eAgInSc6d1	hranický
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Bečvy	Bečva	k1gFnSc2	Bečva
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Hůrka	hůrka	k1gFnSc1	hůrka
u	u	k7c2	u
Hranic	Hranice	k1gFnPc2	Hranice
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
Hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
suché	suchý	k2eAgFnSc2d1	suchá
části	část	k1gFnSc2	část
propasti	propast	k1gFnSc2	propast
je	být	k5eAaImIp3nS	být
69,5	[number]	k4	69,5
m	m	kA	m
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
zatopené	zatopený	k2eAgFnSc2d1	zatopená
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
404	[number]	k4	404
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejhlubší	hluboký	k2eAgFnSc4d3	nejhlubší
zatopenou	zatopený	k2eAgFnSc4d1	zatopená
jeskyni	jeskyně	k1gFnSc4	jeskyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
Hranické	hranický	k2eAgFnSc3d1	Hranická
propasti	propast	k1gFnSc3	propast
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
od	od	k7c2	od
budovy	budova	k1gFnSc2	budova
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
propast	propast	k1gFnSc1	propast
je	být	k5eAaImIp3nS	být
obehnaná	obehnaný	k2eAgFnSc1d1	obehnaná
plotem	plot	k1gInSc7	plot
jako	jako	k8xC	jako
ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
pádu	pád	k1gInSc3	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
skočil	skočit	k5eAaPmAgMnS	skočit
na	na	k7c4	na
koni	kůň	k1gMnSc3	kůň
velkomoravský	velkomoravský	k2eAgMnSc1d1	velkomoravský
vládce	vládce	k1gMnSc1	vládce
Mojmír	Mojmír	k1gMnSc1	Mojmír
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Mojmír	Mojmír	k1gMnSc1	Mojmír
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
totéž	týž	k3xTgNnSc1	týž
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
temnotě	temnota	k1gFnSc6	temnota
udělali	udělat	k5eAaPmAgMnP	udělat
jeho	jeho	k3xOp3gMnPc1	jeho
pronásledovatelé	pronásledovatel	k1gMnPc1	pronásledovatel
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zrádných	zrádný	k2eAgMnPc2d1	zrádný
velmožů	velmož	k1gMnPc2	velmož
ohrožujících	ohrožující	k2eAgFnPc2d1	ohrožující
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přístupných	přístupný	k2eAgFnPc2d1	přístupná
Zbrašovských	Zbrašovský	k2eAgFnPc2d1	Zbrašovský
aragonitových	aragonitový	k2eAgFnPc2d1	aragonitová
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hloubka	hloubka	k1gFnSc1	hloubka
propasti	propast	k1gFnSc2	propast
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
473,5	[number]	k4	473,5
m	m	kA	m
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc1	její
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zatopena	zatopen	k2eAgFnSc1d1	zatopena
Hranickým	hranický	k2eAgNnSc7d1	hranické
jezírkem	jezírko	k1gNnSc7	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Propast	propast	k1gFnSc1	propast
má	mít	k5eAaImIp3nS	mít
elipsovitý	elipsovitý	k2eAgInSc4d1	elipsovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
směrem	směr	k1gInSc7	směr
JV-	JV-	k1gFnSc2	JV-
<g/>
SZ.	SZ.	k1gFnSc2	SZ.
Její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
v	v	k7c6	v
nejdelším	dlouhý	k2eAgNnSc6d3	nejdelší
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
110	[number]	k4	110
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
asi	asi	k9	asi
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
návštěvníků	návštěvník	k1gMnPc2	návštěvník
je	být	k5eAaImIp3nS	být
nejzajímavějším	zajímavý	k2eAgInSc7d3	nejzajímavější
údajem	údaj	k1gInSc7	údaj
hloubka	hloubka	k1gFnSc1	hloubka
propasti	propast	k1gFnSc2	propast
včetně	včetně	k7c2	včetně
zatopené	zatopený	k2eAgFnSc2d1	zatopená
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
členitý	členitý	k2eAgInSc4d1	členitý
krasový	krasový	k2eAgInSc4d1	krasový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Krasové	krasový	k2eAgInPc1d1	krasový
jevy	jev	k1gInPc1	jev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
závrty	závrt	k1gInPc1	závrt
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
okolí	okolí	k1gNnSc6	okolí
propasti	propast	k1gFnSc2	propast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
48	[number]	k4	48
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
jezírka	jezírko	k1gNnSc2	jezírko
lze	lze	k6eAd1	lze
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
sifonu	sifon	k1gInSc2	sifon
Zubatice	Zubatice	k1gFnSc2	Zubatice
vystoupat	vystoupat	k5eAaPmF	vystoupat
až	až	k9	až
do	do	k7c2	do
suchých	suchý	k2eAgFnPc2d1	suchá
jeskyní	jeskyně	k1gFnPc2	jeskyně
(	(	kIx(	(
<g/>
Rotunda	rotunda	k1gFnSc1	rotunda
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
Nebe	nebe	k1gNnSc1	nebe
I-III	I-III	k1gFnPc2	I-III
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jsou	být	k5eAaImIp3nP	být
průběžně	průběžně	k6eAd1	průběžně
monitorovány	monitorovat	k5eAaImNgInP	monitorovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
měření	měření	k1gNnSc2	měření
teploty	teplota	k1gFnSc2	teplota
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Rotunda	rotunda	k1gFnSc1	rotunda
suchá	suchý	k2eAgFnSc1d1	suchá
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
pronikají	pronikat	k5eAaImIp3nP	pronikat
velmi	velmi	k6eAd1	velmi
úzkým	úzký	k2eAgInSc7d1	úzký
průlezem	průlez	k1gInSc7	průlez
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
Jezírka	jezírko	k1gNnSc2	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Musejí	muset	k5eAaImIp3nP	muset
přitom	přitom	k6eAd1	přitom
překonat	překonat	k5eAaPmF	překonat
asi	asi	k9	asi
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
skalního	skalní	k2eAgInSc2d1	skalní
masivu	masiv	k1gInSc2	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
netopýrů	netopýr	k1gMnPc2	netopýr
je	být	k5eAaImIp3nS	být
monitorován	monitorován	k2eAgMnSc1d1	monitorován
a	a	k8xC	a
zkoumán	zkoumán	k2eAgMnSc1d1	zkoumán
odborníky	odborník	k1gMnPc7	odborník
z	z	k7c2	z
Biologického	biologický	k2eAgInSc2d1	biologický
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hloubka	hloubka	k1gFnSc1	hloubka
==	==	k?	==
</s>
</p>
<p>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
suché	suchý	k2eAgFnSc2d1	suchá
části	část	k1gFnSc2	část
propasti	propast	k1gFnSc2	propast
je	být	k5eAaImIp3nS	být
69,5	[number]	k4	69,5
m.	m.	k?	m.
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Jezírko	jezírko	k1gNnSc1	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
byla	být	k5eAaImAgFnS	být
propast	propast	k1gFnSc1	propast
zmapována	zmapovat	k5eAaPmNgFnS	zmapovat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
–	–	k?	–
<g/>
170	[number]	k4	170
m	m	kA	m
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ponor	ponor	k1gInSc1	ponor
následoval	následovat	k5eAaImAgInS	následovat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
–	–	k?	–
<g/>
181	[number]	k4	181
m	m	kA	m
(	(	kIx(	(
<g/>
Starnawski	Starnawski	k1gNnSc1	Starnawski
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
ponor	ponor	k1gInSc1	ponor
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
–	–	k?	–
<g/>
217	[number]	k4	217
m	m	kA	m
(	(	kIx(	(
<g/>
Starnawski	Starnawski	k1gNnSc4	Starnawski
s	s	k7c7	s
šestičlenným	šestičlenný	k2eAgInSc7d1	šestičlenný
týmem	tým	k1gInSc7	tým
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
polských	polský	k2eAgMnPc2d1	polský
potápěčů	potápěč	k1gMnPc2	potápěč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
i	i	k9	i
zkoumání	zkoumání	k1gNnSc1	zkoumání
pomocí	pomocí	k7c2	pomocí
robotu	robot	k1gInSc2	robot
Hyball	Hyballa	k1gFnPc2	Hyballa
a	a	k8xC	a
robotu	robota	k1gFnSc4	robota
HBZS	HBZS	kA	HBZS
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Robot	robot	k1gMnSc1	robot
(	(	kIx(	(
<g/>
R.	R.	kA	R.
<g/>
O.V.	O.V.	k1gMnSc1	O.V.
<g/>
)	)	kIx)	)
Hyball	Hyball	k1gMnSc1	Hyball
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
–	–	k?	–
<g/>
205	[number]	k4	205
m	m	kA	m
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dna	dno	k1gNnPc4	dno
nebylo	být	k5eNaImAgNnS	být
dosaženo	dosažen	k2eAgNnSc1d1	dosaženo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
dalšího	další	k2eAgInSc2d1	další
průzkumu	průzkum	k1gInSc2	průzkum
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
taková	takový	k3xDgFnSc1	takový
konfigurace	konfigurace	k1gFnSc1	konfigurace
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
robotu	robot	k1gInSc2	robot
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
hloubky	hloubka	k1gFnSc2	hloubka
propasti	propast	k1gFnSc2	propast
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
akci	akce	k1gFnSc6	akce
speleopotápěčů	speleopotápěč	k1gMnPc2	speleopotápěč
z	z	k7c2	z
organizace	organizace	k1gFnSc2	organizace
ZO	ZO	kA	ZO
ČSS	ČSS	kA	ČSS
7-02	[number]	k4	7-02
Hranický	hranický	k2eAgInSc4d1	hranický
kras	kras	k1gInSc4	kras
Olomouc	Olomouc	k1gFnSc1	Olomouc
spustil	spustit	k5eAaPmAgInS	spustit
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Starnawski	Starnawsk	k1gFnSc2	Starnawsk
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
217	[number]	k4	217
metrů	metr	k1gInPc2	metr
sondu	sonda	k1gFnSc4	sonda
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
373	[number]	k4	373
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
potom	potom	k6eAd1	potom
krátce	krátce	k6eAd1	krátce
ještě	ještě	k6eAd1	ještě
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
225	[number]	k4	225
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
dosažená	dosažený	k2eAgFnSc1d1	dosažená
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
potápěčem	potápěč	k1gInSc7	potápěč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
naměřil	naměřit	k5eAaBmAgInS	naměřit
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Starnawski	Starnawsk	k1gFnSc2	Starnawsk
opět	opět	k6eAd1	opět
pomocí	pomocí	k7c2	pomocí
sondy	sonda	k1gFnSc2	sonda
novou	nový	k2eAgFnSc4d1	nová
maximální	maximální	k2eAgFnSc4d1	maximální
hloubku	hloubka	k1gFnSc4	hloubka
zatopené	zatopený	k2eAgFnSc2d1	zatopená
části	část	k1gFnSc2	část
Hranické	hranický	k2eAgFnSc2d1	Hranická
propasti	propast	k1gFnSc2	propast
–	–	k?	–
384	[number]	k4	384
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2016	[number]	k4	2016
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ROV	rov	k1gInSc4	rov
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
firmou	firma	k1gFnSc7	firma
GRALmarine	GRALmarin	k1gInSc5	GRALmarin
hloubky	hloubek	k1gInPc4	hloubek
404	[number]	k4	404
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
dna	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc7	svůj
hloubkou	hloubka	k1gFnSc7	hloubka
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
italskou	italský	k2eAgFnSc4d1	italská
Pozzo	Pozza	k1gFnSc5	Pozza
del	del	k?	del
Merro	Merro	k1gNnSc4	Merro
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
evidovanou	evidovaný	k2eAgFnSc7d1	evidovaná
jako	jako	k8xC	jako
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
zatopená	zatopený	k2eAgFnSc1d1	zatopená
propast	propast	k1gFnSc1	propast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
naměřenou	naměřený	k2eAgFnSc7d1	naměřená
hloubkou	hloubka	k1gFnSc7	hloubka
392	[number]	k4	392
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
hloubka	hloubka	k1gFnSc1	hloubka
suché	suchý	k2eAgFnSc2d1	suchá
i	i	k8xC	i
mokré	mokrý	k2eAgFnSc2d1	mokrá
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
473,5	[number]	k4	473,5
m	m	kA	m
(	(	kIx(	(
<g/>
69,5	[number]	k4	69,5
<g/>
+	+	kIx~	+
<g/>
404	[number]	k4	404
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
třeba	třeba	k6eAd1	třeba
učinit	učinit	k5eAaImF	učinit
řadu	řada	k1gFnSc4	řada
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Geologové	geolog	k1gMnPc1	geolog
dnes	dnes	k6eAd1	dnes
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
mezi	mezi	k7c7	mezi
800	[number]	k4	800
a	a	k8xC	a
1200	[number]	k4	1200
m	m	kA	m
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Jezírku	jezírko	k1gNnSc6	jezírko
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jiří	Jiří	k1gMnSc1	Jiří
Pogoda	Pogoda	k1gMnSc1	Pogoda
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
při	při	k7c6	při
sólovém	sólový	k2eAgInSc6d1	sólový
ponoru	ponor	k1gInSc6	ponor
spustil	spustit	k5eAaPmAgMnS	spustit
sondu	sonda	k1gFnSc4	sonda
speciální	speciální	k2eAgFnSc2d1	speciální
konstrukce	konstrukce	k1gFnSc2	konstrukce
(	(	kIx(	(
<g/>
kluzák	kluzák	k1gInSc1	kluzák
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Zubatice	Zubatice	k1gFnSc2	Zubatice
do	do	k7c2	do
celkové	celkový	k2eAgFnSc2d1	celková
hloubky	hloubka	k1gFnSc2	hloubka
vody	voda	k1gFnSc2	voda
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
dna	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
měření	měření	k1gNnSc4	měření
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
neúplné	úplný	k2eNgFnPc1d1	neúplná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
málo	málo	k6eAd1	málo
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyly	být	k5eNaImAgFnP	být
dále	daleko	k6eAd2	daleko
ověřeny	ověřen	k2eAgFnPc1d1	ověřena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
dnešního	dnešní	k2eAgNnSc2d1	dnešní
poznání	poznání	k1gNnSc2	poznání
je	být	k5eAaImIp3nS	být
Pogodovo	Pogodův	k2eAgNnSc1d1	Pogodův
měření	měření	k1gNnSc1	měření
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nepřehlednosti	nepřehlednost	k1gFnSc3	nepřehlednost
restrikce	restrikce	k1gFnSc2	restrikce
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
205	[number]	k4	205
metrů	metr	k1gInPc2	metr
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
sonda	sonda	k1gFnSc1	sonda
mohla	moct	k5eAaImAgFnS	moct
klesnout	klesnout	k5eAaPmF	klesnout
níže	nízce	k6eAd2	nízce
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
změť	změť	k1gFnSc4	změť
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
terénních	terénní	k2eAgFnPc2d1	terénní
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
rekordy	rekord	k1gInPc1	rekord
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
TV	TV	kA	TV
DIVE	div	k1gInSc5	div
-	-	kIx~	-
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
ponor	ponor	k1gInSc4	ponor
Krystof	Krystof	k1gInSc1	Krystof
Starnawski	Starnawsk	k1gFnSc2	Starnawsk
</s>
</p>
<p>
<s>
ZO	ZO	kA	ZO
ČSS	ČSS	kA	ČSS
7-02	[number]	k4	7-02
Hranický	hranický	k2eAgInSc1d1	hranický
kras	kras	k1gInSc1	kras
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
Potápěčská	potápěčský	k2eAgFnSc1d1	potápěčská
lokalita	lokalita	k1gFnSc1	lokalita
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
</s>
</p>
<p>
<s>
Hranická	hranický	k2eAgFnSc1d1	Hranická
propast	propast	k1gFnSc1	propast
-	-	kIx~	-
popis	popis	k1gInSc1	popis
lokality	lokalita	k1gFnSc2	lokalita
</s>
</p>
