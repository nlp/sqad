<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Fadrný	Fadrný	k1gMnSc1	Fadrný
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1972	[number]	k4	1972
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
kapelník	kapelník	k1gMnSc1	kapelník
skupiny	skupina	k1gFnSc2	skupina
Svítání	svítání	k1gNnPc2	svítání
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
7	[number]	k4	7
let	léto	k1gNnPc2	léto
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
filharmonickém	filharmonický	k2eAgInSc6d1	filharmonický
sboru	sbor	k1gInSc6	sbor
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Gaudeamus	Gaudeamus	k1gInSc1	Gaudeamus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
většiny	většina	k1gFnSc2	většina
skladeb	skladba	k1gFnPc2	skladba
skupiny	skupina	k1gFnSc2	skupina
Svítání	svítání	k1gNnSc2	svítání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
bratranec	bratranec	k1gMnSc1	bratranec
Addie	Addie	k1gFnSc2	Addie
Fadrny	Fadrna	k1gFnSc2	Fadrna
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
The	The	k1gFnSc2	The
Bohemian	bohemian	k1gInSc1	bohemian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Fadrný	Fadrný	k1gMnSc1	Fadrný
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
Jiřím	Jiří	k1gMnSc6	Jiří
Fadrném	Fadrný	k1gMnSc6	Fadrný
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
Svítání	svítání	k1gNnSc2	svítání
</s>
</p>
