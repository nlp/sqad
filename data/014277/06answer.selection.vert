<s>
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Mortensen	Mortensen	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1856	#num#	k4
Jonstrup	Jonstrup	k1gInSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1921	#num#	k4
Viborg	Viborg	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
dánský	dánský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
ornitologie	ornitologie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
položil	položit	k5eAaPmAgInS
základy	základ	k1gInPc4
vědeckého	vědecký	k2eAgNnSc2d1
kroužkování	kroužkování	k1gNnSc2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
okroužkoval	okroužkovat	k5eAaPmAgMnS
hliníkovými	hliníkový	k2eAgFnPc7d1
kroužky	kroužek	k1gInPc1
prvních	první	k4xOgInPc2
pár	pár	k4xCyI
špačků	špaček	k1gInPc2
<g/>
.	.	kIx.
</s>