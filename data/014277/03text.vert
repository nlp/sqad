<s>
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Mortensen	Mortensen	k1gInSc4
</s>
<s>
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Mortensen	Mortensna	k1gFnPc2
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
MortensenNarození	MortensenNarození	k1gNnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1856	#num#	k4
Jonstrup	Jonstrup	k1gInSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1921	#num#	k4
Viborg	Viborg	k1gInSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
dánská	dánský	k2eAgFnSc1d1
Obor	obor	k1gInSc1
</s>
<s>
ornitologie	ornitologie	k1gFnSc1
Známý	známý	k2eAgMnSc1d1
díky	díky	k7c3
</s>
<s>
základy	základ	k1gInPc1
vědeckého	vědecký	k2eAgNnSc2d1
kroužkování	kroužkování	k1gNnSc2
ptáků	pták	k1gMnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Mortensen	Mortensen	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1856	#num#	k4
Jonstrup	Jonstrup	k1gInSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1921	#num#	k4
Viborg	Viborg	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
dánský	dánský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
ornitologie	ornitologie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
položil	položit	k5eAaPmAgInS
základy	základ	k1gInPc4
vědeckého	vědecký	k2eAgNnSc2d1
kroužkování	kroužkování	k1gNnSc2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
okroužkoval	okroužkovat	k5eAaPmAgMnS
hliníkovými	hliníkový	k2eAgFnPc7d1
kroužky	kroužek	k1gInPc1
prvních	první	k4xOgInPc2
pár	pár	k4xCyI
špačků	špaček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
ptáků	pták	k1gMnPc2
odchytil	odchytit	k5eAaPmAgInS
v	v	k7c6
ptačích	ptačí	k2eAgFnPc6d1
budkách	budka	k1gFnPc6
s	s	k7c7
automatickým	automatický	k2eAgInSc7d1
uzavíracím	uzavírací	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
kroužkoval	kroužkovat	k5eAaImAgInS
i	i	k9
jiné	jiný	k2eAgInPc4d1
druhy	druh	k1gInPc4
ptáků	pták	k1gMnPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
čápi	čáp	k1gMnPc1
<g/>
,	,	kIx,
volavky	volavka	k1gFnPc1
<g/>
,	,	kIx,
racci	racek	k1gMnPc1
a	a	k8xC
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
kachen	kachna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
osobně	osobně	k6eAd1
okroužkoval	okroužkovat	k5eAaPmAgMnS
více	hodně	k6eAd2
než	než	k8xS
6000	#num#	k4
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroužky	kroužek	k1gInPc7
vyráběl	vyrábět	k5eAaImAgMnS
stříháním	stříhání	k1gNnPc3
z	z	k7c2
hliníkového	hliníkový	k2eAgInSc2d1
plechu	plech	k1gInSc2
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
kroužek	kroužek	k1gInSc1
měl	mít	k5eAaImAgInS
na	na	k7c4
sobě	se	k3xPyFc3
vyraženou	vyražený	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
a	a	k8xC
pořadové	pořadový	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mortensen	Mortensna	k1gFnPc2
původně	původně	k6eAd1
zkoušel	zkoušet	k5eAaImAgMnS
pro	pro	k7c4
kroužky	kroužek	k1gInPc4
i	i	k8xC
plech	plech	k1gInSc1
ze	z	k7c2
slitiny	slitina	k1gFnSc2
zinku	zinek	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ty	ten	k3xDgInPc1
byly	být	k5eAaImAgInP
příliš	příliš	k6eAd1
těžké	těžký	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Preuss	Preussa	k1gFnPc2
<g/>
,	,	kIx,
Niels	Nielsa	k1gFnPc2
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Mortensen	Mortensen	k2eAgMnSc1d1
<g/>
:	:	kIx,
aspects	aspects	k1gInSc1
of	of	k?
his	his	k1gNnSc7
life	lif	k1gFnSc2
and	and	k?
of	of	k?
the	the	k?
history	histor	k1gInPc1
of	of	k?
bird	bird	k1gInSc1
ringing	ringing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ardea	Ardeum	k1gNnPc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
ardeajournal	ardeajournat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
natuurinfo	natuurinfo	k6eAd1
<g/>
.	.	kIx.
<g/>
nl	nl	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hans	Hans	k1gMnSc1
Christian	Christian	k1gMnSc1
Mortensen	Mortensen	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
anglický	anglický	k2eAgInSc1d1
podrobný	podrobný	k2eAgInSc1d1
životopis	životopis	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
173248381	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
4549	#num#	k4
0761	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
95048395	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14356915	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
95048395	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
|	|	kIx~
Dánsko	Dánsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
