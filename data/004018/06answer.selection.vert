<s>
Modráskovití	Modráskovitý	k2eAgMnPc1d1	Modráskovitý
(	(	kIx(	(
<g/>
Lycaenidae	Lycaenidae	k1gNnSc7	Lycaenidae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
malých	malý	k2eAgMnPc2d1	malý
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
okolo	okolo	k7c2	okolo
6000	[number]	k4	6000
druhů	druh	k1gInPc2	druh
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
asi	asi	k9	asi
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
