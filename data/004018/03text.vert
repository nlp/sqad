<s>
Modráskovití	Modráskovitý	k2eAgMnPc1d1	Modráskovitý
(	(	kIx(	(
<g/>
Lycaenidae	Lycaenidae	k1gNnSc7	Lycaenidae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
malých	malý	k2eAgMnPc2d1	malý
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
okolo	okolo	k7c2	okolo
6000	[number]	k4	6000
druhů	druh	k1gInPc2	druh
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
asi	asi	k9	asi
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
40	[number]	k4	40
%	%	kIx~	%
známých	známý	k2eAgInPc2d1	známý
motýlích	motýlí	k2eAgInPc2d1	motýlí
druhů	druh	k1gInPc2	druh
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
jsou	být	k5eAaImIp3nP	být
malí	malý	k2eAgMnPc1d1	malý
nebo	nebo	k8xC	nebo
nejvýše	vysoce	k6eAd3	vysoce
středně	středně	k6eAd1	středně
velcí	velký	k2eAgMnPc1d1	velký
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
modře	modro	k6eAd1	modro
nebo	nebo	k8xC	nebo
červeně	červeně	k6eAd1	červeně
kovově	kovově	k6eAd1	kovově
lesklí	lesklý	k2eAgMnPc1d1	lesklý
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
nebo	nebo	k8xC	nebo
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
<s>
Lipteninae	Lipteninae	k1gFnSc1	Lipteninae
Poritiinae	Poritiinae	k1gFnSc1	Poritiinae
Liphyrinae	Liphyrinae	k1gFnSc1	Liphyrinae
Liphyra	Liphyra	k1gFnSc1	Liphyra
brassolis	brassolis	k1gFnSc1	brassolis
Miletinae	Miletinae	k1gFnSc1	Miletinae
Curetinae	Curetinae	k1gFnSc1	Curetinae
Curetis	Curetis	k1gFnSc1	Curetis
thetis	thetis	k1gFnSc1	thetis
Theclinae	Theclina	k1gFnSc2	Theclina
Satyrium	Satyrium	k1gNnSc1	Satyrium
pruni	prunit	k5eAaPmRp2nS	prunit
Atlides	Atlides	k1gMnSc1	Atlides
halesus	halesus	k1gMnSc1	halesus
Eumaeus	Eumaeus	k1gMnSc1	Eumaeus
atala	atala	k1gMnSc1	atala
Lycaeninae	Lycaeninae	k1gNnSc4	Lycaeninae
Lycaena	Lycaen	k1gMnSc2	Lycaen
boldenarum	boldenarum	k1gInSc4	boldenarum
Lycaena	Lycaeno	k1gNnSc2	Lycaeno
feredayi	fereday	k1gFnSc2	fereday
Lycaena	Lycaen	k1gMnSc2	Lycaen
rauparaha	rauparah	k1gMnSc2	rauparah
Lycaena	Lycaen	k1gMnSc2	Lycaen
salustius	salustius	k1gMnSc1	salustius
Lycaena	Lycaen	k1gMnSc2	Lycaen
dispar	dispar	k1gMnSc1	dispar
Lycaena	Lycaen	k1gMnSc2	Lycaen
phlaeas	phlaeas	k1gMnSc1	phlaeas
Talicada	Talicada	k1gFnSc1	Talicada
nyseus	nyseus	k1gMnSc1	nyseus
Polyommatinae	Polyommatina	k1gInSc2	Polyommatina
Celastrina	Celastrina	k1gMnSc1	Celastrina
neglecta	neglecta	k1gMnSc1	neglecta
Celastrina	Celastrina	k1gMnSc1	Celastrina
ladon	ladon	k1gMnSc1	ladon
Cupido	Cupida	k1gFnSc5	Cupida
comyntas	comyntas	k1gInSc1	comyntas
Cupido	Cupida	k1gFnSc5	Cupida
minimus	minimus	k1gInSc1	minimus
Euphilotes	Euphilotes	k1gMnSc1	Euphilotes
battoides	battoides	k1gMnSc1	battoides
allyni	allyně	k1gFnSc4	allyně
Euphilotes	Euphilotesa	k1gFnPc2	Euphilotesa
pallescens	pallescens	k6eAd1	pallescens
arenamontana	arenamontana	k1gFnSc1	arenamontana
Polyommatus	Polyommatus	k1gMnSc1	Polyommatus
icarus	icarus	k1gMnSc1	icarus
–	–	k?	–
modrásek	modrásek	k1gMnSc1	modrásek
jehlicový	jehlicový	k2eAgMnSc1d1	jehlicový
Polyommatus	Polyommatus	k1gMnSc1	Polyommatus
semiargus	semiargus	k1gMnSc1	semiargus
Glaucopsyche	Glaucopsyche	k1gFnPc2	Glaucopsyche
lygdamus	lygdamus	k1gMnSc1	lygdamus
Glaucopsyche	Glaucopsych	k1gFnSc2	Glaucopsych
lygdamus	lygdamus	k1gMnSc1	lygdamus
palosverdesensis	palosverdesensis	k1gFnSc2	palosverdesensis
Glaucopsyche	Glaucopsyche	k1gFnSc1	Glaucopsyche
xerces	xerces	k1gMnSc1	xerces
(	(	kIx(	(
<g/>
vyhynulý	vyhynulý	k2eAgMnSc1d1	vyhynulý
<g/>
)	)	kIx)	)
Maculinea	Maculinea	k1gMnSc1	Maculinea
arion	arion	k1gMnSc1	arion
–	–	k?	–
modrásek	modrásek	k1gMnSc1	modrásek
černoskvrnný	černoskvrnný	k2eAgMnSc1d1	černoskvrnný
Icaricia	Icaricium	k1gNnSc2	Icaricium
icarioides	icarioides	k1gInSc1	icarioides
fenderi	fendere	k1gFnSc4	fendere
Pseudozizeeria	Pseudozizeerium	k1gNnSc2	Pseudozizeerium
maha	mah	k1gInSc2	mah
</s>
