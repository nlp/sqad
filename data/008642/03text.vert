<p>
<s>
Koryčany	Koryčan	k1gMnPc4	Koryčan
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Koritschan	Koritschany	k1gInPc2	Koritschany
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
okresu	okres	k1gInSc2	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Chřibů	Chřiby	k1gInPc2	Chřiby
<g/>
,	,	kIx,	,
11	[number]	k4	11
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Kyjovka	kyjovka	k1gFnSc1	kyjovka
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1321	[number]	k4	1321
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
městečko	městečko	k1gNnSc1	městečko
jsou	být	k5eAaImIp3nP	být
uváděny	uváděn	k2eAgInPc1d1	uváděn
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1358	[number]	k4	1358
<g/>
–	–	k?	–
<g/>
1611	[number]	k4	1611
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
panství	panství	k1gNnSc2	panství
hradu	hrad	k1gInSc2	hrad
Cimburku	Cimburk	k1gInSc2	Cimburk
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
držela	držet	k5eAaImAgFnS	držet
koryčanské	koryčanský	k2eAgNnSc4d1	koryčanský
panství	panství	k1gNnSc4	panství
Marie	Maria	k1gFnSc2	Maria
Regina	Regina	k1gFnSc1	Regina
Bertoletti	Bertoletť	k1gFnSc2	Bertoletť
z	z	k7c2	z
Bartenfeldu	Bartenfeld	k1gInSc2	Bartenfeld
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
je	on	k3xPp3gMnPc4	on
koupili	koupit	k5eAaPmAgMnP	koupit
Horečtí	Horecký	k2eAgMnPc1d1	Horecký
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Emerich	Emerich	k1gMnSc1	Emerich
Horecký	Horecký	k2eAgMnSc1d1	Horecký
však	však	k9	však
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
panství	panství	k1gNnSc2	panství
prodal	prodat	k5eAaPmAgMnS	prodat
Karlu	Karel	k1gMnSc3	Karel
Josefovi	Josef	k1gMnSc3	Josef
z	z	k7c2	z
Gillernu	Gillerna	k1gFnSc4	Gillerna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Kristiánu	Kristián	k1gMnSc6	Kristián
z	z	k7c2	z
Gillernu	Gillerna	k1gFnSc4	Gillerna
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
zdědila	zdědit	k5eAaPmAgFnS	zdědit
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1796	[number]	k4	1796
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
udělil	udělit	k5eAaPmAgMnS	udělit
obci	obec	k1gFnSc3	obec
Koryčany	Koryčan	k1gMnPc4	Koryčan
právo	právo	k1gNnSc4	právo
pořádat	pořádat	k5eAaImF	pořádat
čtyři	čtyři	k4xCgInPc4	čtyři
výroční	výroční	k2eAgInPc4d1	výroční
trhy	trh	k1gInPc4	trh
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc7	první
v	v	k7c4	v
den	den	k1gInSc4	den
po	po	k7c6	po
Novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
v	v	k7c4	v
pondělí	pondělí	k1gNnSc4	pondělí
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
průvodní	průvodní	k2eAgFnSc6d1	průvodní
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
po	po	k7c6	po
sv.	sv.	kA	sv.
Trojici	trojice	k1gFnSc4	trojice
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
po	po	k7c6	po
sv.	sv.	kA	sv.
Vavřinci	Vavřinec	k1gMnSc6	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Pergamen	pergamen	k1gInSc1	pergamen
s	s	k7c7	s
panovnickou	panovnický	k2eAgFnSc7d1	panovnická
pečetí	pečeť	k1gFnSc7	pečeť
přivěšenou	přivěšený	k2eAgFnSc7d1	přivěšená
na	na	k7c6	na
hedvábné	hedvábný	k2eAgFnSc6d1	hedvábná
šňůře	šňůra	k1gFnSc6	šňůra
v	v	k7c6	v
dřevěném	dřevěný	k2eAgNnSc6d1	dřevěné
pouzdru	pouzdro	k1gNnSc6	pouzdro
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
koupil	koupit	k5eAaPmAgMnS	koupit
panství	panství	k1gNnSc4	panství
Salomon	Salomon	k1gMnSc1	Salomon
Mayer	Mayer	k1gMnSc1	Mayer
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
prodal	prodat	k5eAaPmAgMnS	prodat
Vilému	Viléma	k1gFnSc4	Viléma
Figdorovi	Figdor	k1gMnSc3	Figdor
a	a	k8xC	a
Heřmanu	Heřman	k1gMnSc3	Heřman
Wittgesteinovi	Wittgestein	k1gMnSc3	Wittgestein
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
drželi	držet	k5eAaImAgMnP	držet
panství	panství	k1gNnSc4	panství
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
hrabata	hrabě	k1gNnPc1	hrabě
Trautmandorf-Weinsberg	Trautmandorf-Weinsberg	k1gMnSc1	Trautmandorf-Weinsberg
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
Ludvík	Ludvík	k1gMnSc1	Ludvík
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
.	.	kIx.	.
</s>
<s>
Pošta	pošta	k1gFnSc1	pošta
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
Michael	Michael	k1gMnSc1	Michael
Thonet	Thonet	k1gMnSc1	Thonet
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
postavil	postavit	k5eAaPmAgMnS	postavit
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
ohýbaný	ohýbaný	k2eAgInSc4d1	ohýbaný
nábytek	nábytek	k1gInSc4	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
bylo	být	k5eAaImAgNnS	být
započato	započat	k2eAgNnSc1d1	započato
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
z	z	k7c2	z
Nemotic	Nemotice	k1gFnPc2	Nemotice
do	do	k7c2	do
Koryčan	Koryčan	k1gMnSc1	Koryčan
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
i	i	k9	i
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
koupaliště	koupaliště	k1gNnSc1	koupaliště
<g/>
.	.	kIx.	.
</s>
<s>
Nádrž	nádrž	k1gFnSc1	nádrž
byla	být	k5eAaImAgFnS	být
betonová	betonový	k2eAgFnSc1d1	betonová
45	[number]	k4	45
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
7000	[number]	k4	7000
hl.	hl.	k?	hl.
Stavbu	stavba	k1gFnSc4	stavba
prováděl	provádět	k5eAaImAgMnS	provádět
a	a	k8xC	a
dozoroval	dozorovat	k5eAaImAgMnS	dozorovat
Okrašlovací	okrašlovací	k2eAgInSc4d1	okrašlovací
spolek	spolek	k1gInSc4	spolek
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
<g/>
.	.	kIx.	.
</s>
<s>
Kousek	kousek	k6eAd1	kousek
od	od	k7c2	od
koupaliště	koupaliště	k1gNnSc2	koupaliště
byla	být	k5eAaImAgFnS	být
letní	letní	k2eAgFnSc1d1	letní
restaurace	restaurace	k1gFnSc1	restaurace
"	"	kIx"	"
<g/>
Zdravá	zdravý	k2eAgFnSc1d1	zdravá
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
s	s	k7c7	s
tanečním	taneční	k2eAgInSc7d1	taneční
parketem	parket	k1gInSc7	parket
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
tam	tam	k6eAd1	tam
pořádány	pořádán	k2eAgInPc4d1	pořádán
občasné	občasný	k2eAgInPc4d1	občasný
hudební	hudební	k2eAgInPc4d1	hudební
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
i	i	k9	i
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k6eAd1	vedle
restaurace	restaurace	k1gFnSc1	restaurace
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc4	dva
kuželny	kuželna	k1gFnPc4	kuželna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Zdravé	zdravý	k2eAgFnSc2d1	zdravá
vody	voda	k1gFnSc2	voda
byly	být	k5eAaImAgFnP	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
také	také	k9	také
několik	několik	k4yIc1	několik
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
atrakcí	atrakce	k1gFnPc2	atrakce
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgMnPc4	který
pečoval	pečovat	k5eAaImAgMnS	pečovat
děda	děda	k1gMnSc1	děda
Elšíček	Elšíček	k1gMnSc1	Elšíček
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
dětmi	dítě	k1gFnPc7	dítě
nazýván	nazývat	k5eAaImNgMnS	nazývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vousek	vouska	k1gFnPc2	vouska
a	a	k8xC	a
dýmkou	dýmka	k1gFnSc7	dýmka
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
vydal	vydat	k5eAaPmAgInS	vydat
Okrašlovací	okrašlovací	k2eAgInSc1d1	okrašlovací
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
malou	malý	k2eAgFnSc4d1	malá
knížečku	knížečka	k1gFnSc4	knížečka
Koryčany	Koryčan	k1gMnPc7	Koryčan
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
61	[number]	k4	61
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
nich	on	k3xPp3gFnPc2	on
23	[number]	k4	23
fotografií	fotografia	k1gFnPc2	fotografia
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
vhodným	vhodný	k2eAgInSc7d1	vhodný
textem	text	k1gInSc7	text
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Knížečka	knížečka	k1gFnSc1	knížečka
je	být	k5eAaImIp3nS	být
zvláštího	zvláští	k2eAgInSc2d1	zvláští
malého	malý	k2eAgInSc2d1	malý
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
ručně	ručně	k6eAd1	ručně
malovanou	malovaný	k2eAgFnSc4d1	malovaná
mapku	mapka	k1gFnSc4	mapka
okolí	okolí	k1gNnSc2	okolí
Koryčan	Koryčan	k1gMnSc1	Koryčan
<g/>
,	,	kIx,	,
turistické	turistický	k2eAgFnPc1d1	turistická
značky	značka	k1gFnPc1	značka
s	s	k7c7	s
trasou	trasa	k1gFnSc7	trasa
a	a	k8xC	a
"	"	kIx"	"
<g/>
Zalesňovací	zalesňovací	k2eAgNnSc1d1	zalesňovací
a	a	k8xC	a
okrašlovací	okrašlovací	k2eAgNnSc1d1	okrašlovací
desatero	desatero	k1gNnSc1	desatero
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
knížečky	knížečka	k1gFnSc2	knížečka
je	být	k5eAaImIp3nS	být
také	také	k9	také
19	[number]	k4	19
inzerátů	inzerát	k1gInPc2	inzerát
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
místních	místní	k2eAgMnPc2d1	místní
podnikatelů	podnikatel	k1gMnPc2	podnikatel
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
výstavba	výstavba	k1gFnSc1	výstavba
vodárenské	vodárenský	k2eAgFnSc2d1	vodárenská
nádrže	nádrž	k1gFnSc2	nádrž
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kyjovce	kyjovka	k1gFnSc6	kyjovka
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
Koryčan	Koryčan	k1gMnSc1	Koryčan
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
a	a	k8xC	a
do	do	k7c2	do
trvalého	trvalý	k2eAgInSc2d1	trvalý
provozu	provoz	k1gInSc2	provoz
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Rady	rada	k1gFnSc2	rada
MNV	MNV	kA	MNV
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
byly	být	k5eAaImAgFnP	být
Koryčany	Koryčan	k1gMnPc4	Koryčan
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
povýšeny	povýšen	k2eAgInPc1d1	povýšen
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
prohlášení	prohlášení	k1gNnSc1	prohlášení
obce	obec	k1gFnSc2	obec
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
na	na	k7c6	na
MěNV	MěNV	k1gFnSc6	MěNV
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
MNV	MNV	kA	MNV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byly	být	k5eAaImAgInP	být
ke	k	k7c3	k
Koryčanům	Koryčan	k1gMnPc3	Koryčan
připojeny	připojen	k2eAgFnPc4d1	připojena
sousední	sousední	k2eAgFnPc4d1	sousední
obce	obec	k1gFnPc4	obec
Lískovec	Lískovec	k1gInSc1	Lískovec
a	a	k8xC	a
Jestřabice	Jestřabice	k1gFnSc1	Jestřabice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samospráva	samospráva	k1gFnSc1	samospráva
==	==	k?	==
</s>
</p>
<p>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
obce	obec	k1gFnSc2	obec
má	mít	k5eAaImIp3nS	mít
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
5	[number]	k4	5
radních	radní	k1gMnPc2	radní
<g/>
.	.	kIx.	.
</s>
<s>
Voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
1	[number]	k4	1
176	[number]	k4	176
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
49,43	[number]	k4	49,43
%	%	kIx~	%
<g/>
)	)	kIx)	)
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
získala	získat	k5eAaPmAgFnS	získat
31,15	[number]	k4	31,15
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
uskupení	uskupený	k2eAgMnPc1d1	uskupený
Starostové	Starosta	k1gMnPc1	Starosta
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
(	(	kIx(	(
<g/>
21,2	[number]	k4	21,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
17,75	[number]	k4	17,75
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
(	(	kIx(	(
<g/>
16,55	[number]	k4	16,55
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandát	mandát	k1gInSc1	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
(	(	kIx(	(
<g/>
16,55	[number]	k4	16,55
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
3	[number]	k4	3
členy	člen	k1gMnPc4	člen
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
1	[number]	k4	1
člena	člen	k1gMnSc2	člen
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
1	[number]	k4	1
člena	člen	k1gMnSc4	člen
KDU-ČSL	KDU-ČSL	k1gMnSc4	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Šmela	Šmela	k1gMnSc1	Šmela
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
1	[number]	k4	1
149	[number]	k4	149
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
48,58	[number]	k4	48,58
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
Starostové	Starosta	k1gMnPc1	Starosta
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
28,27	[number]	k4	28,27
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
ČSSD	ČSSD	kA	ČSSD
24,79	[number]	k4	24,79
%	%	kIx~	%
(	(	kIx(	(
<g/>
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ZVUK	zvuk	k1gInSc1	zvuk
12	[number]	k4	12
získal	získat	k5eAaPmAgInS	získat
19,47	[number]	k4	19,47
%	%	kIx~	%
(	(	kIx(	(
<g/>
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
15,2	[number]	k4	15,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
12,27	[number]	k4	12,27
%	%	kIx~	%
(	(	kIx(	(
<g/>
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starostkou	starostka	k1gFnSc7	starostka
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
Ing.	ing.	kA	ing.
Hana	Hana	k1gFnSc1	Hana
Jamborová	Jamborová	k1gFnSc1	Jamborová
(	(	kIx(	(
<g/>
Starostové	Starosta	k1gMnPc1	Starosta
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
jsou	být	k5eAaImIp3nP	být
Koryčany	Koryčan	k1gMnPc7	Koryčan
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
senátního	senátní	k2eAgInSc2d1	senátní
obvodu	obvod	k1gInSc2	obvod
č.	č.	k?	č.
76	[number]	k4	76
–	–	k?	–
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Miloš	Miloš	k1gMnSc1	Miloš
Malý	Malý	k1gMnSc1	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Dobrovolného	Dobrovolného	k2eAgInSc2d1	Dobrovolného
svazku	svazek	k1gInSc2	svazek
obcí	obec	k1gFnPc2	obec
Koryčanska	Koryčansko	k1gNnSc2	Koryčansko
a	a	k8xC	a
Zdounecka	Zdounecko	k1gNnSc2	Zdounecko
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Zdounkách	Zdounk	k1gInPc6	Zdounk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
patřily	patřit	k5eAaImAgFnP	patřit
Koryčany	Koryčan	k1gMnPc4	Koryčan
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
-	-	kIx~	-
hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Big-beatové	Bigeatový	k2eAgFnSc2d1	Big-beatový
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
Meopius	Meopius	k1gInSc1	Meopius
-	-	kIx~	-
Olin	Olin	k2eAgMnSc1d1	Olin
Berka	Berka	k1gMnSc1	Berka
<g/>
,	,	kIx,	,
Jenda	Jenda	k1gMnSc1	Jenda
Šmehlík	Šmehlík	k1gMnSc1	Šmehlík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šmarhák	Šmarhák	k1gMnSc1	Šmarhák
<g/>
,	,	kIx,	,
Laďa	Laďa	k?	Laďa
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Hlavinka	hlavinka	k1gFnSc1	hlavinka
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
Junior	junior	k1gMnSc1	junior
Hop	hop	k0	hop
-	-	kIx~	-
Miloš	Miloš	k1gMnSc1	Miloš
Bártek	Bártek	k1gMnSc1	Bártek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Jarek	Jarek	k1gMnSc1	Jarek
Budík	budík	k1gInSc1	budík
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horenský	horenský	k2eAgMnSc1d1	horenský
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
The	The	k1gFnSc2	The
Frees	Freesa	k1gFnPc2	Freesa
-	-	kIx~	-
Jarek	Jarka	k1gFnPc2	Jarka
Budík	budík	k1gInSc1	budík
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Olin	Olin	k2eAgMnSc1d1	Olin
Berka	Berka	k1gMnSc1	Berka
<g/>
,	,	kIx,	,
Jarek	Jarek	k1gMnSc1	Jarek
Mušálek	Mušálek	k1gMnSc1	Mušálek
<g/>
.	.	kIx.	.
</s>
<s>
Zvukař	zvukař	k1gMnSc1	zvukař
Jarek	Jarek	k1gMnSc1	Jarek
Judas	Judas	k1gMnSc1	Judas
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
Free	Fre	k1gFnSc2	Fre
Five	Five	k1gFnPc2	Five
-	-	kIx~	-
Jarek	Jarka	k1gFnPc2	Jarka
Budík	budík	k1gInSc1	budík
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Olin	Olin	k2eAgMnSc1d1	Olin
Berka	Berka	k1gMnSc1	Berka
<g/>
,	,	kIx,	,
Mirek	Mirek	k1gMnSc1	Mirek
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
.	.	kIx.	.
</s>
<s>
Zvukař	zvukař	k1gMnSc1	zvukař
Jarek	Jarek	k1gMnSc1	Jarek
Judas	Judas	k1gMnSc1	Judas
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horenský	horenský	k2eAgMnSc1d1	horenský
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
Free	Free	k1gFnSc1	Free
Five	Five	k1gFnSc1	Five
(	(	kIx(	(
<g/>
Oáza	oáza	k1gFnSc1	oáza
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jarek	Jarka	k1gFnPc2	Jarka
Budík	budík	k1gInSc1	budík
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Mirek	Mirek	k1gMnSc1	Mirek
Fridrich	Fridrich	k1gMnSc1	Fridrich
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
Sekvence	sekvence	k1gFnSc1	sekvence
-	-	kIx~	-
Olin	Olin	k2eAgMnSc1d1	Olin
Berka	Berka	k1gMnSc1	Berka
<g/>
,	,	kIx,	,
Jenda	Jenda	k1gMnSc1	Jenda
Šmehlík	Šmehlík	k1gMnSc1	Šmehlík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Jurák	Jurák	k1gMnSc1	Jurák
<g/>
,	,	kIx,	,
Slávek	Slávek	k1gMnSc1	Slávek
Machálek	Machálek	k1gMnSc1	Machálek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Stuchlík	Stuchlík	k1gMnSc1	Stuchlík
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
Oáza	oáza	k1gFnSc1	oáza
2	[number]	k4	2
(	(	kIx(	(
<g/>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Everest	Everest	k1gInSc1	Everest
́	́	k?	́
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Galandr	Galandr	k1gInSc1	Galandr
<g/>
,	,	kIx,	,
Mirek	Mirek	k1gMnSc1	Mirek
Maruška	Maruška	k1gFnSc1	Maruška
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Půček	Půček	k1gMnSc1	Půček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Řihák	Řihák	k1gMnSc1	Řihák
</s>
</p>
<p>
<s>
198	[number]	k4	198
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
Saturejka	saturejka	k1gFnSc1	saturejka
-	-	kIx~	-
Milan	Milan	k1gMnSc1	Milan
Katolický	katolický	k2eAgMnSc1d1	katolický
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Havránek	Havránek	k1gMnSc1	Havránek
,	,	kIx,	,
Jarek	Jarek	k1gMnSc1	Jarek
Holásek	Holásek	k1gMnSc1	Holásek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Povolný	Povolný	k1gMnSc1	Povolný
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Pavla	Pavla	k1gFnSc1	Pavla
Smetanová	Smetanová	k1gFnSc1	Smetanová
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
při	při	k7c6	při
koryčanské	koryčanský	k2eAgFnSc6d1	koryčanský
pouti	pouť	k1gFnSc6	pouť
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
společné	společný	k2eAgNnSc1d1	společné
vystoupení	vystoupení	k1gNnSc1	vystoupení
čtyř	čtyři	k4xCgFnPc2	čtyři
beatových	beatový	k2eAgFnPc2d1	beatová
skupin	skupina	k1gFnPc2	skupina
Free	Free	k1gFnSc1	Free
Five	Five	k1gFnSc1	Five
<g/>
,	,	kIx,	,
Sekvence	sekvence	k1gFnSc1	sekvence
<g/>
,	,	kIx,	,
Oáza	oáza	k1gFnSc1	oáza
2	[number]	k4	2
a	a	k8xC	a
Saturejka	saturejka	k1gFnSc1	saturejka
<g/>
.	.	kIx.	.
</s>
<s>
Společné	společný	k2eAgNnSc1d1	společné
vystoupení	vystoupení	k1gNnSc1	vystoupení
opakováno	opakovat	k5eAaImNgNnS	opakovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc4d1	další
hudební	hudební	k2eAgInPc4d1	hudební
soubory	soubor	k1gInPc4	soubor
===	===	k?	===
</s>
</p>
<p>
<s>
Dechový	dechový	k2eAgInSc1d1	dechový
soubor	soubor	k1gInSc1	soubor
UP	UP	kA	UP
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
Miloš	Miloš	k1gMnSc1	Miloš
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Jan	Jan	k1gMnSc1	Jan
Duchalík	Duchalík	k1gMnSc1	Duchalík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
UP	UP	kA	UP
závodu	závod	k1gInSc3	závod
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Tomáše	Tomáš	k1gMnSc2	Tomáš
Koláčka	Koláček	k1gMnSc2	Koláček
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
hráli	hrát	k5eAaImAgMnP	hrát
Milan	Milan	k1gMnSc1	Milan
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Berka	Berka	k1gMnSc1	Berka
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Helena	Helena	k1gFnSc1	Helena
Petruchová	Petruchová	k1gFnSc1	Petruchová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	malý	k2eAgInSc1d1	malý
dechový	dechový	k2eAgInSc1d1	dechový
soubor	soubor	k1gInSc1	soubor
Kutalka	Kutalka	k1gFnSc1	Kutalka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pištělák	Pištělák	k1gMnSc1	Pištělák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
působila	působit	k5eAaImAgFnS	působit
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
taneční	taneční	k2eAgFnSc2d1	taneční
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
Bohema	Bohemum	k1gNnSc2	Bohemum
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Ladislav	Ladislava	k1gFnPc2	Ladislava
Luska	luska	k1gFnSc1	luska
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Škrabal	Škrabal	k1gMnSc1	Škrabal
<g/>
,	,	kIx,	,
Drahomír	Drahomír	k1gMnSc1	Drahomír
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Plaček	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Plaček	Plaček	k1gMnSc1	Plaček
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Irena	Irena	k1gFnSc1	Irena
Berková	Berková	k1gFnSc1	Berková
<g/>
.	.	kIx.	.
</s>
<s>
Hrávala	hrávat	k5eAaImAgFnS	hrávat
na	na	k7c6	na
plesech	pleso	k1gNnPc6	pleso
<g/>
,	,	kIx,	,
zábavách	zábava	k1gFnPc6	zábava
<g/>
,	,	kIx,	,
srazech	sraz	k1gInPc6	sraz
a	a	k8xC	a
družebních	družební	k2eAgInPc6d1	družební
večerech	večer	k1gInPc6	večer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
místní	místní	k2eAgFnSc1d1	místní
Základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
Československého	československý	k2eAgInSc2d1	československý
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
místní	místní	k2eAgFnSc1d1	místní
Základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
Juveny	Juvena	k1gFnSc2	Juvena
(	(	kIx(	(
<g/>
svazu	svaz	k1gInSc2	svaz
vesnické	vesnický	k2eAgFnSc2d1	vesnická
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
místní	místní	k2eAgFnSc1d1	místní
Základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
jednotného	jednotný	k2eAgInSc2d1	jednotný
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
bylo	být	k5eAaImAgNnS	být
pořádání	pořádání	k1gNnSc4	pořádání
I.	I.	kA	I.
Krajského	krajský	k2eAgInSc2d1	krajský
festivalu	festival	k1gInSc2	festival
vesnické	vesnický	k2eAgFnSc2d1	vesnická
mládeže	mládež	k1gFnSc2	mládež
místní	místní	k2eAgFnSc7d1	místní
organizací	organizace	k1gFnSc7	organizace
Juveny	Juvena	k1gFnSc2	Juvena
ve	v	k7c6	v
dnech	den	k1gInPc6	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
sportovních	sportovní	k2eAgFnPc2d1	sportovní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgInPc2d1	kulturní
soutěží	soutěž	k1gFnSc7	soutěž
–	–	k?	–
přehlídka	přehlídka	k1gFnSc1	přehlídka
divadel	divadlo	k1gNnPc2	divadlo
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
beatových	beatový	k2eAgFnPc2d1	beatová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
byla	být	k5eAaImAgFnS	být
masová	masový	k2eAgFnSc1d1	masová
účast	účast	k1gFnSc1	účast
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
jako	jako	k8xS	jako
soutěžících	soutěžící	k1gMnPc2	soutěžící
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Kazem	kaz	k1gInSc7	kaz
na	na	k7c4	na
celé	celý	k2eAgFnPc4d1	celá
události	událost	k1gFnPc4	událost
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
částečně	částečně	k6eAd1	částečně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
sportovní	sportovní	k2eAgNnSc4d1	sportovní
klání	klání	k1gNnSc4	klání
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
byl	být	k5eAaImAgInS	být
festival	festival	k1gInSc1	festival
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
a	a	k8xC	a
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
kladně	kladně	k6eAd1	kladně
hodnocenou	hodnocený	k2eAgFnSc7d1	hodnocená
akcí	akce	k1gFnSc7	akce
<g/>
.	.	kIx.	.
</s>
<s>
Přípravný	přípravný	k2eAgInSc1d1	přípravný
výbor	výbor	k1gInSc1	výbor
SSM	SSM	kA	SSM
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
již	již	k6eAd1	již
vydávat	vydávat	k5eAaPmF	vydávat
festival	festival	k1gInSc4	festival
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
jen	jen	k6eAd1	jen
přípravným	přípravný	k2eAgInSc7d1	přípravný
výborem	výbor	k1gInSc7	výbor
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
celostátní	celostátní	k2eAgFnSc2d1	celostátní
ustavující	ustavující	k2eAgFnSc2d1	ustavující
konference	konference	k1gFnSc2	konference
SSM	SSM	kA	SSM
byla	být	k5eAaImAgFnS	být
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
na	na	k7c6	na
základě	základ	k1gInSc6	základ
usnesení	usnesení	k1gNnSc2	usnesení
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
KSČ	KSČ	kA	KSČ
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
festival	festival	k1gInSc4	festival
jednoznačně	jednoznačně	k6eAd1	jednoznačně
akcí	akce	k1gFnPc2	akce
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgFnPc4d1	existující
a	a	k8xC	a
fungující	fungující	k2eAgFnPc4d1	fungující
Juveny	Juvena	k1gFnPc4	Juvena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
hnutí	hnutí	k1gNnPc1	hnutí
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
hesla	heslo	k1gNnPc4	heslo
"	"	kIx"	"
<g/>
Mladí	mladý	k2eAgMnPc1d1	mladý
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
SHM	SHM	kA	SHM
Klub	klub	k1gInSc1	klub
Koryčany	Koryčan	k1gMnPc4	Koryčan
pořádá	pořádat	k5eAaImIp3nS	pořádat
každoroční	každoroční	k2eAgFnSc2d1	každoroční
mikulášské	mikulášský	k2eAgFnSc2d1	Mikulášská
besídky	besídka	k1gFnSc2	besídka
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgInPc1d1	dětský
dny	den	k1gInPc1	den
či	či	k8xC	či
různé	různý	k2eAgInPc1d1	různý
turnaje	turnaj	k1gInPc1	turnaj
(	(	kIx(	(
<g/>
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc1	florbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
krožků	krožek	k1gInPc2	krožek
zejména	zejména	k9	zejména
nabízí	nabízet	k5eAaImIp3nS	nabízet
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
mládeži	mládež	k1gFnSc3	mládež
florbal	florbal	k1gInSc4	florbal
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
či	či	k8xC	či
scholu	schola	k1gFnSc4	schola
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2	[number]	k4	2
837	[number]	k4	837
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
1	[number]	k4	1
285	[number]	k4	285
(	(	kIx(	(
<g/>
45,3	[number]	k4	45,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
674	[number]	k4	674
(	(	kIx(	(
<g/>
23,8	[number]	k4	23,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
moravské	moravský	k2eAgFnSc3d1	Moravská
<g/>
,	,	kIx,	,
29	[number]	k4	29
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
<g/>
,	,	kIx,	,
7	[number]	k4	7
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
<g/>
,	,	kIx,	,
6	[number]	k4	6
ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
<g/>
,	,	kIx,	,
2	[number]	k4	2
polské	polský	k2eAgInPc1d1	polský
<g/>
,	,	kIx,	,
1	[number]	k4	1
slezské	slezský	k2eAgInPc1d1	slezský
a	a	k8xC	a
1	[number]	k4	1
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
738	[number]	k4	738
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
26	[number]	k4	26
%	%	kIx~	%
<g/>
)	)	kIx)	)
svou	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
738	[number]	k4	738
(	(	kIx(	(
<g/>
26	[number]	k4	26
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
Koryčan	Koryčan	k1gMnSc1	Koryčan
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
.	.	kIx.	.
432	[number]	k4	432
(	(	kIx(	(
<g/>
15,2	[number]	k4	15,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
5	[number]	k4	5
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
2	[number]	k4	2
k	k	k7c3	k
Českobratrské	českobratrský	k2eAgFnSc3d1	Českobratrská
církvi	církev	k1gFnSc3	církev
evangelické	evangelický	k2eAgFnSc3d1	evangelická
a	a	k8xC	a
2	[number]	k4	2
ke	k	k7c3	k
Svědkům	svědek	k1gMnPc3	svědek
Jehovovým	Jehovův	k2eAgMnPc3d1	Jehovův
<g/>
.	.	kIx.	.
774	[number]	k4	774
(	(	kIx(	(
<g/>
27,3	[number]	k4	27,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
1	[number]	k4	1
325	[number]	k4	325
(	(	kIx(	(
<g/>
46,7	[number]	k4	46,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
víry	víra	k1gFnSc2	víra
neodpovědělo	odpovědět	k5eNaPmAgNnS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Koryčanská	Koryčanský	k2eAgFnSc1d1	Koryčanská
farnost	farnost	k1gFnSc1	farnost
s	s	k7c7	s
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc4	Vavřinec
a	a	k8xC	a
filiálními	filiální	k2eAgInPc7d1	filiální
kostely	kostel	k1gInPc7	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
Jestřabicích	Jestřabice	k1gFnPc6	Jestřabice
a	a	k8xC	a
sv.	sv.	kA	sv.
Klementa	Klement	k1gMnSc2	Klement
na	na	k7c6	na
Stupavě	Stupavě	k1gMnSc6	Stupavě
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
děkanátu	děkanát	k1gInSc2	děkanát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
polesím	polesí	k1gNnSc7	polesí
západně	západně	k6eAd1	západně
od	od	k7c2	od
Koryčan	Koryčan	k1gMnSc1	Koryčan
<g/>
.	.	kIx.	.
</s>
<s>
Sklárna	sklárna	k1gFnSc1	sklárna
na	na	k7c6	na
říčce	říčka	k1gFnSc6	říčka
Stupavě	Stupavě	k1gFnSc2	Stupavě
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
již	již	k6eAd1	již
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sklárnu	sklárna	k1gFnSc4	sklárna
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
od	od	k7c2	od
tamní	tamní	k2eAgFnSc2d1	tamní
vrchnosti	vrchnost	k1gFnSc2	vrchnost
baronů	baron	k1gMnPc2	baron
Münch-Bellinghausenů	Münch-Bellinghausen	k1gMnPc2	Münch-Bellinghausen
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
Izák	Izák	k1gMnSc1	Izák
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
po	po	k7c6	po
Strání	stráň	k1gFnPc2	stráň
a	a	k8xC	a
Staré	Staré	k2eAgFnPc1d1	Staré
Huti	huť	k1gFnPc1	huť
jako	jako	k9	jako
svojí	svojit	k5eAaImIp3nP	svojit
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc4	třetí
sklárnu	sklárna	k1gFnSc4	sklárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
byly	být	k5eAaImAgFnP	být
skelné	skelný	k2eAgFnPc1d1	skelná
hutě	huť	k1gFnPc1	huť
Reichem	Reichem	k?	Reichem
zmodernizovány	zmodernizován	k2eAgFnPc1d1	zmodernizována
<g/>
.	.	kIx.	.
</s>
<s>
Sklárna	sklárna	k1gFnSc1	sklárna
však	však	k9	však
později	pozdě	k6eAd2	pozdě
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
založil	založit	k5eAaPmAgMnS	založit
Michael	Michael	k1gMnSc1	Michael
Thonet	Thonet	k1gMnSc1	Thonet
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
ohýbaný	ohýbaný	k2eAgInSc4d1	ohýbaný
nábytek	nábytek	k1gInSc4	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
První	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
měla	mít	k5eAaImAgFnS	mít
továrna	továrna	k1gFnSc1	továrna
až	až	k9	až
1500	[number]	k4	1500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Koryčany	Koryčan	k1gMnPc7	Koryčan
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
regionálních	regionální	k2eAgFnPc2d1	regionální
a	a	k8xC	a
lokálních	lokální	k2eAgFnPc2d1	lokální
silnic	silnice	k1gFnPc2	silnice
vedoucích	vedoucí	k1gMnPc2	vedoucí
Středomoravskými	středomoravský	k2eAgFnPc7d1	Středomoravská
Karpatami	Karpata	k1gFnPc7	Karpata
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
432	[number]	k4	432
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
429	[number]	k4	429
a	a	k8xC	a
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
43231	[number]	k4	43231
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
byla	být	k5eAaImAgFnS	být
provozována	provozován	k2eAgFnSc1d1	provozována
nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
č.	č.	k?	č.
345	[number]	k4	345
z	z	k7c2	z
Nemotic	Nemotice	k1gFnPc2	Nemotice
(	(	kIx(	(
<g/>
odbočka	odbočka	k1gFnSc1	odbočka
z	z	k7c2	z
Vlárské	vlárský	k2eAgFnSc2d1	Vlárská
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
využívána	využívat	k5eAaPmNgFnS	využívat
i	i	k9	i
na	na	k7c4	na
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1967	[number]	k4	1967
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
naposledy	naposledy	k6eAd1	naposledy
jela	jet	k5eAaImAgFnS	jet
parní	parní	k2eAgFnSc1d1	parní
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
<g/>
,	,	kIx,	,
místními	místní	k2eAgFnPc7d1	místní
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Helenka	Helenka	k1gFnSc1	Helenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6,20	[number]	k4	6,20
hodin	hodina	k1gFnPc2	hodina
ještě	ještě	k6eAd1	ještě
vyjela	vyjet	k5eAaPmAgFnS	vyjet
z	z	k7c2	z
Koryčan	Koryčan	k1gMnSc1	Koryčan
do	do	k7c2	do
Nemotic	Nemotice	k1gFnPc2	Nemotice
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
vlak	vlak	k1gInSc1	vlak
tažen	táhnout	k5eAaImNgInS	táhnout
motorovým	motorový	k2eAgInSc7d1	motorový
vozem	vůz	k1gInSc7	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Helenka	Helenka	k1gFnSc1	Helenka
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
potřeby	potřeba	k1gFnSc2	potřeba
stát	stát	k5eAaImF	stát
na	na	k7c6	na
odstavné	odstavný	k2eAgFnSc6d1	odstavná
koleji	kolej	k1gFnSc6	kolej
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
příležitostně	příležitostně	k6eAd1	příležitostně
používána	používán	k2eAgFnSc1d1	používána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
vedou	vést	k5eAaImIp3nP	vést
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
č.	č.	k?	č.
800700	[number]	k4	800700
z	z	k7c2	z
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
č.	č.	k?	č.
750901	[number]	k4	750901
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
do	do	k7c2	do
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
č.	č.	k?	č.
750900	[number]	k4	750900
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
do	do	k7c2	do
Starých	Starých	k2eAgFnPc2d1	Starých
Hutí	huť	k1gFnPc2	huť
a	a	k8xC	a
č.	č.	k?	č.
729650	[number]	k4	729650
z	z	k7c2	z
Koryčan	Koryčan	k1gMnSc1	Koryčan
do	do	k7c2	do
Nesovic	Nesovice	k1gFnPc2	Nesovice
a	a	k8xC	a
Bohdalic-Pavlovic	Bohdalic-Pavlovice	k1gFnPc2	Bohdalic-Pavlovice
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
je	být	k5eAaImIp3nS	být
zapojena	zapojen	k2eAgFnSc1d1	zapojena
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
jako	jako	k8xS	jako
linka	linka	k1gFnSc1	linka
650	[number]	k4	650
<g/>
.	.	kIx.	.
</s>
<s>
Koryčany	Koryčan	k1gMnPc4	Koryčan
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
mimo	mimo	k7c4	mimo
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
integrovaných	integrovaný	k2eAgFnPc2d1	integrovaná
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
jako	jako	k8xC	jako
tarifní	tarifní	k2eAgFnSc1d1	tarifní
zóna	zóna	k1gFnSc1	zóna
666	[number]	k4	666
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koryčany	Koryčan	k1gMnPc7	Koryčan
jsou	být	k5eAaImIp3nP	být
turistickým	turistický	k2eAgInSc7d1	turistický
východištěm	východiště	k1gNnSc7	východiště
do	do	k7c2	do
západních	západní	k2eAgInPc2d1	západní
Chřibů	Chřiby	k1gInPc2	Chřiby
<g/>
,	,	kIx,	,
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
vychází	vycházet	k5eAaImIp3nS	vycházet
zeleně	zeleně	k6eAd1	zeleně
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
a	a	k8xC	a
žlutě	žlutě	k6eAd1	žlutě
značené	značený	k2eAgFnSc2d1	značená
turistické	turistický	k2eAgFnSc2d1	turistická
trasy	trasa	k1gFnSc2	trasa
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
postavený	postavený	k2eAgInSc1d1	postavený
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1350	[number]	k4	1350
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Koryčany	Koryčan	k1gMnPc4	Koryčan
a	a	k8xC	a
zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
–	–	k?	–
původně	původně	k6eAd1	původně
barokní	barokní	k2eAgInSc4d1	barokní
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
parkem	park	k1gInSc7	park
postavený	postavený	k2eAgInSc1d1	postavený
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
sloužil	sloužit	k5eAaImAgInS	sloužit
resortu	resort	k1gInSc3	resort
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
soukromému	soukromý	k2eAgNnSc3d1	soukromé
majiteli	majitel	k1gMnSc3	majitel
a	a	k8xC	a
již	již	k6eAd1	již
neslouží	sloužit	k5eNaImIp3nS	sloužit
jako	jako	k9	jako
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc3	veřejnost
není	být	k5eNaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
<g/>
.	.	kIx.	.
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
provedení	provedení	k1gNnSc6	provedení
revitalizace	revitalizace	k1gFnSc2	revitalizace
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
prospěšnou	prospěšný	k2eAgFnSc7d1	prospěšná
společností	společnost	k1gFnSc7	společnost
Koryčany	Koryčan	k1gMnPc4	Koryčan
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
téměř	téměř	k6eAd1	téměř
450	[number]	k4	450
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vysázeno	vysázet	k5eAaPmNgNnS	vysázet
15818	[number]	k4	15818
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
185	[number]	k4	185
biologických	biologický	k2eAgFnPc2d1	biologická
ošetření	ošetření	k1gNnPc2	ošetření
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
broukoviště	broukoviště	k1gNnPc1	broukoviště
a	a	k8xC	a
ošetřeny	ošetřen	k2eAgInPc1d1	ošetřen
trávníky	trávník	k1gInPc1	trávník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Cimburk	Cimburk	k1gInSc1	Cimburk
–	–	k?	–
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
lese	les	k1gInSc6	les
nad	nad	k7c7	nad
koryčanskou	koryčanský	k2eAgFnSc7d1	koryčanský
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
opravována	opravovat	k5eAaImNgFnS	opravovat
a	a	k8xC	a
udržována	udržovat	k5eAaImNgFnS	udržovat
spolkem	spolek	k1gInSc7	spolek
Polypeje	Polypej	k1gInSc2	Polypej
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
přístupný	přístupný	k2eAgInSc1d1	přístupný
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
prohlídkové	prohlídkový	k2eAgFnPc4d1	prohlídková
trasy	trasa	k1gFnPc4	trasa
<g/>
,	,	kIx,	,
konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
různé	různý	k2eAgFnPc1d1	různá
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
šermířské	šermířský	k2eAgInPc4d1	šermířský
turnaje	turnaj	k1gInPc4	turnaj
<g/>
,	,	kIx,	,
setkávání	setkávání	k1gNnSc4	setkávání
malých	malý	k2eAgInPc2d1	malý
pivovarů	pivovar	k1gInPc2	pivovar
a	a	k8xC	a
Cimburský	Cimburský	k2eAgInSc1d1	Cimburský
košt	košt	k1gInSc1	košt
vín	víno	k1gNnPc2	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
s	s	k7c7	s
nejstarším	starý	k2eAgInSc7d3	nejstarší
náhrobkem	náhrobek	k1gInSc7	náhrobek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
200	[number]	k4	200
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnPc1	kaple
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc4	Florián
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1713	[number]	k4	1713
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
</s>
</p>
<p>
<s>
Hrobka	hrobka	k1gFnSc1	hrobka
rodiny	rodina	k1gFnSc2	rodina
Thonetů	Thonet	k1gInPc2	Thonet
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
</s>
</p>
<p>
<s>
Hrob	hrob	k1gInSc1	hrob
a	a	k8xC	a
pomník	pomník	k1gInSc1	pomník
Františka	František	k1gMnSc4	František
Pivody	Pivoda	k1gFnSc2	Pivoda
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
</s>
</p>
<p>
<s>
Urna	urna	k1gFnSc1	urna
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Mastníka	Mastník	k1gMnSc2	Mastník
na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Joseph	Josepha	k1gFnPc2	Josepha
von	von	k1gInSc1	von
Gillern	Gillern	k1gMnSc1	Gillern
(	(	kIx(	(
<g/>
1691	[number]	k4	1691
<g/>
–	–	k?	–
<g/>
1759	[number]	k4	1759
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
rada	rada	k1gMnSc1	rada
a	a	k8xC	a
vyjednavač	vyjednavač	k1gMnSc1	vyjednavač
v	v	k7c6	v
První	první	k4xOgFnSc6	první
slezské	slezský	k2eAgFnSc6d1	Slezská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Halabala	Halabala	k?	Halabala
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nábytkový	nábytkový	k2eAgMnSc1d1	nábytkový
návrhář	návrhář	k1gMnSc1	návrhář
</s>
</p>
<p>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Kolisch	Kolisch	k1gMnSc1	Kolisch
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
evropského	evropský	k2eAgNnSc2d1	Evropské
agenturního	agenturní	k2eAgNnSc2d1	agenturní
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Koryčan	Koryčan	k1gMnSc1	Koryčan
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mastník	Mastník	k1gMnSc1	Mastník
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
české	český	k2eAgFnSc2d1	Česká
sekce	sekce	k1gFnSc2	sekce
vysílání	vysílání	k1gNnSc2	vysílání
britského	britský	k2eAgInSc2d1	britský
rozhlasu	rozhlas	k1gInSc2	rozhlas
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
urnu	urna	k1gFnSc4	urna
na	na	k7c4	na
hrobu	hrob	k1gInSc2	hrob
rodičů	rodič	k1gMnPc2	rodič
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Pivoda	Pivoda	k1gMnSc1	Pivoda
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
</s>
</p>
<p>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Rosenfeld	Rosenfeld	k1gMnSc1	Rosenfeld
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	s	k7c7	s
židovskými	židovský	k2eAgNnPc7d1	Židovské
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Koryčan	Koryčan	k1gMnSc1	Koryčan
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schwarz	Schwarz	k1gMnSc1	Schwarz
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Koryčan	Koryčan	k1gMnSc1	Koryčan
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Thonet	Thonet	k1gMnSc1	Thonet
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
–	–	k?	–
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
ohýbaný	ohýbaný	k2eAgInSc4d1	ohýbaný
nábytek	nábytek	k1gInSc4	nábytek
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tomášek	Tomášek	k1gMnSc1	Tomášek
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
Stará	starý	k2eAgFnSc1d1	stará
Hráz	hráz	k1gFnSc4	hráz
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Kazatelna	kazatelna	k1gFnSc1	kazatelna
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Kozel	Kozel	k1gMnSc1	Kozel
</s>
</p>
<p>
<s>
Evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Chřiby	Chřiby	k1gInPc4	Chřiby
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Koryčany	Koryčan	k1gMnPc4	Koryčan
</s>
</p>
<p>
<s>
Blišice	Blišice	k1gFnSc1	Blišice
</s>
</p>
<p>
<s>
Jestřabice	Jestřabice	k1gFnSc1	Jestřabice
</s>
</p>
<p>
<s>
Lískovec	Lískovec	k1gInSc1	Lískovec
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GALUŠKA	GALUŠKA	kA	GALUŠKA
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Velkomoravské	velkomoravský	k2eAgFnPc1d1	Velkomoravská
pověsti	pověst	k1gFnPc1	pověst
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnPc1	tradice
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
Chřibů	Chřiby	k1gInPc2	Chřiby
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
koryčanského	koryčanský	k2eAgInSc2d1	koryčanský
a	a	k8xC	a
velehradského	velehradský	k2eAgInSc2d1	velehradský
katastru	katastr	k1gInSc2	katastr
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Libor	Libor	k1gMnSc1	Libor
Vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROTHSCHEINOVÁ	ROTHSCHEINOVÁ	kA	ROTHSCHEINOVÁ
<g/>
,	,	kIx,	,
Terezie	Terezie	k1gFnSc1	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Koryčany	Koryčan	k1gMnPc7	Koryčan
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
pozůstalostních	pozůstalostní	k2eAgInPc2d1	pozůstalostní
inventářů	inventář	k1gInPc2	inventář
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cimburk	Cimburk	k1gInSc1	Cimburk
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Koryčany	Koryčan	k1gMnPc7	Koryčan
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Koryčanech	Koryčan	k1gMnPc6	Koryčan
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Nemotice	Nemotice	k1gFnSc2	Nemotice
<g/>
–	–	k?	–
<g/>
Koryčany	Koryčan	k1gMnPc4	Koryčan
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Koryčany	Koryčan	k1gMnPc4	Koryčan
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Koryčany	Koryčan	k1gMnPc4	Koryčan
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koryčany	Koryčan	k1gMnPc4	Koryčan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Koryčany	Koryčan	k1gMnPc7	Koryčan
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Koryčany	Koryčan	k1gMnPc4	Koryčan
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Koryčany	Koryčan	k1gMnPc4	Koryčan
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Koryčany	Koryčan	k1gMnPc7	Koryčan
</s>
</p>
