<s>
Koryčany	Koryčan	k1gMnPc4	Koryčan
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Koritschan	Koritschany	k1gInPc2	Koritschany
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
okresu	okres	k1gInSc2	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Chřibů	Chřiby	k1gInPc2	Chřiby
<g/>
,	,	kIx,	,
11	[number]	k4	11
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
