<s>
Chřestýšovití	Chřestýšovití	k1gMnPc1	Chřestýšovití
(	(	kIx(	(
<g/>
Crotalinae	Crotalinae	k1gInSc1	Crotalinae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
podčeleď	podčeleď	k1gFnSc4	podčeleď
hadů	had	k1gMnPc2	had
čeledi	čeleď	k1gFnSc2	čeleď
zmijovitých	zmijovitý	k2eAgMnPc2d1	zmijovitý
(	(	kIx(	(
<g/>
Viperidae	Viperida	k1gMnSc2	Viperida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
18	[number]	k4	18
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
154	[number]	k4	154
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
výzkumů	výzkum	k1gInPc2	výzkum
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zmijí	zmije	k1gFnPc2	zmije
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
nozdrami	nozdra	k1gFnPc7	nozdra
<g/>
)	)	kIx)	)
termoreceptory	termoreceptor	k1gInPc1	termoreceptor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
zjistit	zjistit	k5eAaPmF	zjistit
i	i	k9	i
nepatrné	patrný	k2eNgInPc4d1	patrný
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jim	on	k3xPp3gMnPc3	on
bezpečně	bezpečně	k6eAd1	bezpečně
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
většinou	většina	k1gFnSc7	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
obratlovci	obratlovec	k1gMnPc1	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc7d1	podobná
schopností	schopnost	k1gFnSc7	schopnost
jsou	být	k5eAaImIp3nP	být
vybaveni	vybaven	k2eAgMnPc1d1	vybaven
nepříbuzní	příbuzný	k2eNgMnPc1d1	nepříbuzný
hroznýšovití	hroznýšovití	k1gMnPc1	hroznýšovití
a	a	k8xC	a
krajty	krajta	k1gFnPc1	krajta
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
čeledi	čeleď	k1gFnPc1	čeleď
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
chřestýšovitých	chřestýšovití	k1gMnPc2	chřestýšovití
<g/>
,	,	kIx,	,
labiální	labiální	k2eAgFnPc1d1	labiální
tepločivné	tepločivný	k2eAgFnPc1d1	tepločivný
jamky	jamka	k1gFnPc1	jamka
(	(	kIx(	(
<g/>
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
"	"	kIx"	"
<g/>
rtů	ret	k1gInPc2	ret
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praví	pravý	k2eAgMnPc1d1	pravý
chřestýši	chřestýš	k1gMnPc1	chřestýš
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
evolučně	evolučně	k6eAd1	evolučně
nejmladší	mladý	k2eAgMnPc4d3	nejmladší
hady	had	k1gMnPc4	had
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
velice	velice	k6eAd1	velice
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
skupinu	skupina	k1gFnSc4	skupina
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgInPc7	který
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
velké	velká	k1gFnPc4	velká
(	(	kIx(	(
<g/>
chřestýš	chřestýš	k1gMnSc1	chřestýš
diamantový	diamantový	k2eAgMnSc1d1	diamantový
-	-	kIx~	-
až	až	k9	až
2,44	[number]	k4	2,44
m	m	kA	m
<g/>
)	)	kIx)	)
i	i	k9	i
malé	malý	k2eAgMnPc4d1	malý
hady	had	k1gMnPc4	had
(	(	kIx(	(
<g/>
stěží	stěží	k6eAd1	stěží
okolo	okolo	k7c2	okolo
50	[number]	k4	50
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Crotalus	Crotalus	k1gInSc1	Crotalus
(	(	kIx(	(
<g/>
chřestýši	chřestýš	k1gMnPc7	chřestýš
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sistrurus	Sistrurus	k1gInSc1	Sistrurus
(	(	kIx(	(
<g/>
chřestýšci	chřestýšek	k1gMnPc1	chřestýšek
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
chřestidlo	chřestidlo	k1gNnSc1	chřestidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dalo	dát	k5eAaPmAgNnS	dát
název	název	k1gInSc4	název
celé	celá	k1gFnSc2	celá
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zrohovatělé	zrohovatělý	k2eAgInPc1d1	zrohovatělý
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
má	mít	k5eAaImIp3nS	mít
had	had	k1gMnSc1	had
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyrušení	vyrušení	k1gNnSc6	vyrušení
třese	třást	k5eAaImIp3nS	třást
had	had	k1gMnSc1	had
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
chřestivý	chřestivý	k2eAgInSc1d1	chřestivý
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
upozornění	upozornění	k1gNnSc4	upozornění
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nepřibližovali	přibližovat	k5eNaImAgMnP	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
chřestýšovití	chřestýšovití	k1gMnPc1	chřestýšovití
(	(	kIx(	(
<g/>
chřestýšovci	chřestýšovec	k1gMnPc1	chřestýšovec
<g/>
,	,	kIx,	,
křovináři	křovinář	k1gMnPc1	křovinář
<g/>
,	,	kIx,	,
ploskolebci	ploskolebec	k1gMnPc1	ploskolebec
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
chřestidlo	chřestidlo	k1gNnSc1	chřestidlo
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Sistrurus	Sistrurus	k1gInSc1	Sistrurus
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
hlavy	hlava	k1gFnSc2	hlava
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
symetrické	symetrický	k2eAgInPc4d1	symetrický
štítky	štítek	k1gInPc4	štítek
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
hadi	had	k1gMnPc1	had
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Crotalus	Crotalus	k1gInSc1	Crotalus
mají	mít	k5eAaImIp3nP	mít
štítky	štítek	k1gInPc4	štítek
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
rozpadlé	rozpadlý	k2eAgFnSc6d1	rozpadlá
na	na	k7c4	na
drobné	drobný	k2eAgFnPc4d1	drobná
šupinky	šupinka	k1gFnPc4	šupinka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
chřestýš	chřestýš	k1gMnSc1	chřestýš
brazilský	brazilský	k2eAgMnSc1d1	brazilský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
krásně	krásně	k6eAd1	krásně
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
krásně	krásně	k6eAd1	krásně
zbarvené	zbarvený	k2eAgInPc1d1	zbarvený
stromové	stromový	k2eAgInPc1d1	stromový
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
toxicita	toxicita	k1gFnSc1	toxicita
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
dodržovat	dodržovat	k5eAaImF	dodržovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
opatrnost	opatrnost	k1gFnSc4	opatrnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vejcoživorodí	vejcoživorodý	k2eAgMnPc1d1	vejcoživorodý
a	a	k8xC	a
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
křovinářů	křovinář	k1gMnPc2	křovinář
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
ploskolebci	ploskolebec	k1gMnPc1	ploskolebec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
tzv.	tzv.	kA	tzv.
vlněním	vlnění	k1gNnSc7	vlnění
U	u	k7c2	u
velkých	velký	k2eAgMnPc2d1	velký
hadů	had	k1gMnPc2	had
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
)	)	kIx)	)
=	=	kIx~	=
přímočaře	přímočaro	k6eAd1	přímočaro
pomocí	pomocí	k7c2	pomocí
břišních	břišní	k2eAgInPc2d1	břišní
štítků	štítek	k1gInPc2	štítek
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
tahací	tahací	k2eAgFnSc1d1	tahací
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
"	"	kIx"	"
postranní	postranní	k2eAgNnSc1d1	postranní
posouvání	posouvání	k1gNnSc1	posouvání
-	-	kIx~	-
např.	např.	kA	např.
chřestýš	chřestýš	k1gMnSc1	chřestýš
rohatý	rohatý	k1gMnSc1	rohatý
Podčeleď	podčeleď	k1gFnSc1	podčeleď
<g/>
:	:	kIx,	:
Chřestýšovití	Chřestýšovitý	k2eAgMnPc1d1	Chřestýšovitý
(	(	kIx(	(
<g/>
Crotalinae	Crotalinae	k1gNnSc7	Crotalinae
<g/>
)	)	kIx)	)
-	-	kIx~	-
18	[number]	k4	18
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
154	[number]	k4	154
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
)	)	kIx)	)
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Agkistrodon	Agkistrodon	k1gMnSc1	Agkistrodon
-	-	kIx~	-
ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Atropoides	Atropoides	k1gInSc1	Atropoides
-	-	kIx~	-
bez	bez	k7c2	bez
českého	český	k2eAgInSc2d1	český
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Bothriechis	Bothriechis	k1gFnSc1	Bothriechis
-	-	kIx~	-
křovinář	křovinář	k1gMnSc1	křovinář
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Bothriopsis	Bothriopsis	k1gFnSc1	Bothriopsis
-	-	kIx~	-
bez	bez	k7c2	bez
českého	český	k2eAgInSc2d1	český
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Bothrops	Bothrops	k1gInSc1	Bothrops
-	-	kIx~	-
křovinář	křovinář	k1gMnSc1	křovinář
32	[number]	k4	32
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc4	rod
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Calloselasma	Calloselasm	k1gMnSc4	Calloselasm
-	-	kIx~	-
ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
1	[number]	k4	1
druh	druh	k1gInSc1	druh
Rod	rod	k1gInSc4	rod
<g/>
:	:	kIx,	:
Cerrophidion	Cerrophidion	k1gInSc4	Cerrophidion
-	-	kIx~	-
křovinář	křovinář	k1gMnSc1	křovinář
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Crotalus	Crotalus	k1gMnSc1	Crotalus
-	-	kIx~	-
chřestýš	chřestýš	k1gMnSc1	chřestýš
27	[number]	k4	27
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Deinagkistrodon	Deinagkistrodon	k1gMnSc1	Deinagkistrodon
-	-	kIx~	-
ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
1	[number]	k4	1
druh	druh	k1gInSc1	druh
Rod	rod	k1gInSc4	rod
<g/>
:	:	kIx,	:
Gloydius	Gloydius	k1gMnSc1	Gloydius
-	-	kIx~	-
ploskolebec	ploskolebec	k1gMnSc1	ploskolebec
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Hypnale	Hypnale	k1gFnSc1	Hypnale
-	-	kIx~	-
ostrolebec	ostrolebec	k1gInSc1	ostrolebec
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Lachesis	Lachesis	k1gFnSc1	Lachesis
-	-	kIx~	-
křovinář	křovinář	k1gMnSc1	křovinář
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Ophryacus	Ophryacus	k1gInSc1	Ophryacus
-	-	kIx~	-
bez	bez	k7c2	bez
českého	český	k2eAgInSc2d1	český
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Ovophis	Ovophis	k1gFnSc1	Ovophis
-	-	kIx~	-
chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Porthidium	Porthidium	k1gNnSc1	Porthidium
-	-	kIx~	-
křovinář	křovinář	k1gMnSc1	křovinář
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Sistrurus	Sistrurus	k1gInSc1	Sistrurus
-	-	kIx~	-
chřestýšek	chřestýšek	k1gInSc1	chřestýšek
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Trimeresurus	Trimeresurus	k1gMnSc1	Trimeresurus
-	-	kIx~	-
chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
35	[number]	k4	35
druhů	druh	k1gInPc2	druh
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Tropiodolaemus	Tropiodolaemus	k1gMnSc1	Tropiodolaemus
-	-	kIx~	-
chřestýšovec	chřestýšovec	k1gMnSc1	chřestýšovec
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
</s>
