<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
osmanské	osmanský	k2eAgNnSc1d1	osmanské
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
ا	ا	k?	ا
elifbâ	elifbâ	k?	elifbâ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
verze	verze	k1gFnSc1	verze
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
turečtině	turečtina	k1gFnSc6	turečtina
za	za	k7c2	za
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mustafa	Mustaf	k1gMnSc4	Mustaf
Kemal	Kemal	k1gInSc4	Kemal
Atatürk	Atatürk	k1gInSc1	Atatürk
zavedl	zavést	k5eAaPmAgInS	zavést
používání	používání	k1gNnSc4	používání
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
