<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
osmanské	osmanský	k2eAgNnSc1d1	osmanské
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
ا	ا	k?	ا
elifbâ	elifbâ	k?	elifbâ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
verze	verze	k1gFnSc1	verze
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
turečtině	turečtina	k1gFnSc6	turečtina
za	za	k7c2	za
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mustafa	Mustaf	k1gMnSc4	Mustaf
Kemal	Kemal	k1gInSc4	Kemal
Atatürk	Atatürk	k1gInSc1	Atatürk
zavedl	zavést	k5eAaPmAgInS	zavést
používání	používání	k1gNnSc4	používání
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
arabském	arabský	k2eAgNnSc6d1	arabské
písmu	písmo	k1gNnSc6	písmo
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
písmen	písmeno	k1gNnPc2	písmeno
čtyři	čtyři	k4xCgFnPc4	čtyři
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
<g/>
,	,	kIx,	,
začáteční	začáteční	k2eAgFnSc4d1	začáteční
<g/>
,	,	kIx,	,
středovou	středový	k2eAgFnSc4d1	středová
a	a	k8xC	a
koncovou	koncový	k2eAgFnSc4d1	koncová
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
turečtina	turečtina	k1gFnSc1	turečtina
také	také	k9	také
používala	používat	k5eAaImAgFnS	používat
číslice	číslice	k1gFnSc1	číslice
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
odlišné	odlišný	k2eAgFnPc1d1	odlišná
od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
našich	náš	k3xOp1gFnPc2	náš
"	"	kIx"	"
<g/>
arabských	arabský	k2eAgFnPc2d1	arabská
<g/>
"	"	kIx"	"
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
arabských	arabský	k2eAgFnPc2d1	arabská
číslic	číslice	k1gFnPc2	číslice
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
název	název	k1gInSc4	název
v	v	k7c6	v
turečtině	turečtina	k1gFnSc6	turečtina
<g/>
.	.	kIx.	.
٠	٠	k?	٠
0	[number]	k4	0
(	(	kIx(	(
<g/>
sı	sı	k?	sı
<g/>
)	)	kIx)	)
۱	۱	k?	۱
1	[number]	k4	1
(	(	kIx(	(
<g/>
bir	bir	k?	bir
<g/>
)	)	kIx)	)
۲	۲	k?	۲
2	[number]	k4	2
(	(	kIx(	(
<g/>
iki	iki	k?	iki
<g/>
)	)	kIx)	)
٣	٣	k?	٣
3	[number]	k4	3
(	(	kIx(	(
<g/>
üç	üç	k?	üç
<g/>
)	)	kIx)	)
٤	٤	k?	٤
4	[number]	k4	4
(	(	kIx(	(
<g/>
dört	dörta	k1gFnPc2	dörta
<g/>
)	)	kIx)	)
٥	٥	k?	٥
5	[number]	k4	5
(	(	kIx(	(
<g/>
beş	beş	k?	beş
<g/>
)	)	kIx)	)
٦	٦	k?	٦
6	[number]	k4	6
(	(	kIx(	(
<g/>
altı	altı	k?	altı
<g/>
)	)	kIx)	)
٧	٧	k?	٧
7	[number]	k4	7
(	(	kIx(	(
<g/>
yedi	yedi	k1gNnSc2	yedi
<g/>
)	)	kIx)	)
٨	٨	k?	٨
8	[number]	k4	8
(	(	kIx(	(
<g/>
sekiz	sekiza	k1gFnPc2	sekiza
<g/>
)	)	kIx)	)
٩	٩	k?	٩
9	[number]	k4	9
(	(	kIx(	(
<g/>
dokuz	dokuza	k1gFnPc2	dokuza
<g/>
)	)	kIx)	)
۱	۱	k?	۱
<g/>
٠	٠	k?	٠
10	[number]	k4	10
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
turečtina	turečtina	k1gFnSc1	turečtina
</s>
