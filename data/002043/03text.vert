<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
epický	epický	k2eAgInSc4d1	epický
román	román	k1gInSc4	román
žánru	žánr	k1gInSc2	žánr
hrdinská	hrdinský	k2eAgFnSc1d1	hrdinská
fantasy	fantas	k1gInPc4	fantas
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgInSc4d1	napsaný
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
fantasy	fantas	k1gInPc4	fantas
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
zakládající	zakládající	k2eAgNnSc4d1	zakládající
dílo	dílo	k1gNnSc4	dílo
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
nejprodávanější	prodávaný	k2eAgInSc4d3	nejprodávanější
román	román	k1gInSc4	román
vůbec	vůbec	k9	vůbec
-	-	kIx~	-
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
International	International	k1gFnSc2	International
Fantasy	fantas	k1gInPc1	fantas
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
trilogii	trilogie	k1gFnSc4	trilogie
<g/>
,	,	kIx,	,
Tolkien	Tolkien	k1gInSc1	Tolkien
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
koncipoval	koncipovat	k5eAaBmAgMnS	koncipovat
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
svazků	svazek	k1gInPc2	svazek
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnSc2	jejich
pojmenování	pojmenování	k1gNnSc2	pojmenování
(	(	kIx(	(
<g/>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
a	a	k8xC	a
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
prosadil	prosadit	k5eAaPmAgMnS	prosadit
vydavatel	vydavatel	k1gMnSc1	vydavatel
z	z	k7c2	z
obchodních	obchodní	k2eAgInPc2d1	obchodní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
napsal	napsat	k5eAaPmAgInS	napsat
své	svůj	k3xOyFgInPc4	svůj
smyšlené	smyšlený	k2eAgInPc4d1	smyšlený
příběhy	příběh	k1gInPc4	příběh
včetně	včetně	k7c2	včetně
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
jako	jako	k8xC	jako
mytologii	mytologie	k1gFnSc4	mytologie
pro	pro	k7c4	pro
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
značně	značně	k6eAd1	značně
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
za	za	k7c7	za
severogermánskými	severogermánský	k2eAgMnPc7d1	severogermánský
bratranci	bratranec	k1gMnPc7	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
světy	svět	k1gInPc4	svět
a	a	k8xC	a
legendy	legenda	k1gFnPc4	legenda
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
umělé	umělý	k2eAgInPc4d1	umělý
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemá	mít	k5eNaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
stojí	stát	k5eAaImIp3nP	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Příběh	příběh	k1gInSc1	příběh
popisuje	popisovat	k5eAaImIp3nS	popisovat
světový	světový	k2eAgInSc1d1	světový
konflikt	konflikt	k1gInSc1	konflikt
dobra	dobro	k1gNnSc2	dobro
se	s	k7c7	s
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
dobra	dobro	k1gNnSc2	dobro
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
příběhu	příběh	k1gInSc6	příběh
je	být	k5eAaImIp3nS	být
zničit	zničit	k5eAaPmF	zničit
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
Sauronem	Sauron	k1gMnSc7	Sauron
představuje	představovat	k5eAaImIp3nS	představovat
děsivou	děsivý	k2eAgFnSc4d1	děsivá
ničivou	ničivý	k2eAgFnSc4d1	ničivá
sílu	síla	k1gFnSc4	síla
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
celou	celý	k2eAgFnSc4d1	celá
Středozem	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
sledujeme	sledovat	k5eAaImIp1nP	sledovat
sjednocení	sjednocení	k1gNnSc4	sjednocení
mnoha	mnoho	k4c2	mnoho
národů	národ	k1gInPc2	národ
a	a	k8xC	a
ras	rasa	k1gFnPc2	rasa
Středozemě	Středozem	k1gFnSc2	Středozem
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Temným	temný	k2eAgMnSc7d1	temný
pánem	pán	k1gMnSc7	pán
Sauronem	Sauron	k1gMnSc7	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
dějových	dějový	k2eAgFnPc6d1	dějová
liniích	linie	k1gFnPc6	linie
je	být	k5eAaImIp3nS	být
představeno	představen	k2eAgNnSc1d1	představeno
jak	jak	k8xS	jak
kolektivní	kolektivní	k2eAgNnSc1d1	kolektivní
hrdinství	hrdinství	k1gNnSc1	hrdinství
vojsk	vojsko	k1gNnPc2	vojsko
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
putování	putování	k1gNnSc1	putování
a	a	k8xC	a
individuální	individuální	k2eAgNnSc1d1	individuální
hrdinství	hrdinství	k1gNnSc1	hrdinství
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gInPc2	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
to	ten	k3xDgNnSc4	ten
autor	autor	k1gMnSc1	autor
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
,	,	kIx,	,
k	k	k7c3	k
atmosféře	atmosféra	k1gFnSc3	atmosféra
knih	kniha	k1gFnPc2	kniha
výrazně	výrazně	k6eAd1	výrazně
přispěla	přispět	k5eAaPmAgFnS	přispět
doba	doba	k1gFnSc1	doba
jejich	jejich	k3xOp3gFnPc2	jejich
vzniku-období	vznikubdobit	k5eAaPmIp3nS	vzniku-obdobit
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
roků	rok	k1gInPc2	rok
před	před	k7c7	před
i	i	k9	i
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
volně	volně	k6eAd1	volně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
návaznosti	návaznost	k1gFnSc2	návaznost
příběhu	příběh	k1gInSc2	příběh
provedl	provést	k5eAaPmAgMnS	provést
Tolkien	Tolkien	k1gInSc4	Tolkien
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
v	v	k7c6	v
textu	text	k1gInSc6	text
Hobita	hobit	k1gMnSc4	hobit
určité	určitý	k2eAgFnSc2d1	určitá
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
romány	román	k1gInPc1	román
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
dějin	dějiny	k1gFnPc2	dějiny
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
světa	svět	k1gInSc2	svět
Arda	Ard	k1gInSc2	Ard
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Silmarillion	Silmarillion	k1gInSc4	Silmarillion
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
postupně	postupně	k6eAd1	postupně
připravuje	připravovat	k5eAaImIp3nS	připravovat
k	k	k7c3	k
vydání	vydání	k1gNnSc2	vydání
autorův	autorův	k2eAgMnSc1d1	autorův
syn	syn	k1gMnSc1	syn
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
-	-	kIx~	-
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
a	a	k8xC	a
část	část	k1gFnSc1	část
druhého	druhý	k4xOgInSc2	druhý
jako	jako	k8xS	jako
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Ralph	Ralph	k1gMnSc1	Ralph
Bakshi	Baksh	k1gFnSc2	Baksh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
jako	jako	k8xS	jako
televizní	televizní	k2eAgInSc1d1	televizní
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
(	(	kIx(	(
<g/>
režiséři	režisér	k1gMnPc1	režisér
Bass	Bass	k1gMnSc1	Bass
a	a	k8xC	a
Rankin	Rankin	k1gMnSc1	Rankin
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
celý	celý	k2eAgInSc4d1	celý
příběh	příběh	k1gInSc4	příběh
Peterem	Peter	k1gMnSc7	Peter
Jacksonem	Jackson	k1gMnSc7	Jackson
jako	jako	k8xS	jako
filmová	filmový	k2eAgFnSc1d1	filmová
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
také	také	k9	také
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hry	hra	k1gFnPc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
často	často	k6eAd1	často
hledají	hledat	k5eAaImIp3nP	hledat
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
Tolkienových	Tolkienův	k2eAgInPc6d1	Tolkienův
příbězích	příběh	k1gInPc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
pořídila	pořídit	k5eAaPmAgFnS	pořídit
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
potřebu	potřeba	k1gFnSc4	potřeba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
Stanislava	Stanislava	k1gFnSc1	Stanislava
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
,	,	kIx,	,
pracovnice	pracovnice	k1gFnSc1	pracovnice
knihovny	knihovna	k1gFnSc2	knihovna
anglistiky	anglistika	k1gFnSc2	anglistika
FF	ff	kA	ff
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
záhy	záhy	k6eAd1	záhy
získala	získat	k5eAaPmAgFnS	získat
příslib	příslib	k1gInSc4	příslib
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
obav	obava	k1gFnPc2	obava
o	o	k7c4	o
možné	možný	k2eAgFnPc4d1	možná
politické	politický	k2eAgFnPc4d1	politická
konotace	konotace	k1gFnPc4	konotace
vydán	vydán	k2eAgMnSc1d1	vydán
až	až	k8xS	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
nicméně	nicméně	k8xC	nicméně
strojopis	strojopis	k1gInSc4	strojopis
překladu	překlad	k1gInSc2	překlad
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
popudu	popud	k1gInSc2	popud
překladatelky	překladatelka	k1gFnSc2	překladatelka
koloval	kolovat	k5eAaImAgInS	kolovat
mezi	mezi	k7c4	mezi
disidenty	disident	k1gMnPc4	disident
<g/>
.	.	kIx.	.
</s>
