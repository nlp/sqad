<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
ministát	ministát	k1gInSc4	ministát
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
Návětrné	návětrný	k2eAgInPc4d1	návětrný
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
Martinikem	Martinik	k1gMnSc7	Martinik
a	a	k8xC	a
Svatým	svatý	k1gMnSc7	svatý
Vincencem	Vincenc	k1gMnSc7	Vincenc
<g/>
.	.	kIx.	.
</s>
