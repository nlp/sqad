<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
ministát	ministát	k1gInSc4	ministát
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
Návětrné	návětrný	k2eAgInPc4d1	návětrný
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
Martinikem	Martinik	k1gMnSc7	Martinik
a	a	k8xC	a
Svatým	svatý	k1gMnSc7	svatý
Vincencem	Vincenc	k1gMnSc7	Vincenc
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Barbadosem	Barbados	k1gInSc7	Barbados
<g/>
,	,	kIx,	,
Martinikem	Martinik	k1gInSc7	Martinik
<g/>
,	,	kIx,	,
Dominikou	Dominika	k1gFnSc7	Dominika
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
státy	stát	k1gInPc7	stát
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
pás	pás	k1gInSc4	pás
ostrovů	ostrov	k1gInPc2	ostrov
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Pevninská	pevninský	k2eAgFnSc1d1	pevninská
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
616	[number]	k4	616
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Castries	Castries	k1gInSc1	Castries
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
veškeré	veškerý	k3xTgFnSc2	veškerý
populace	populace	k1gFnSc2	populace
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
několik	několik	k4yIc4	několik
sopek	sopka	k1gFnPc2	sopka
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Mount	Mount	k1gInSc4	Mount
Gimie	Gimie	k1gFnSc2	Gimie
či	či	k8xC	či
Qualibou	Qualiba	k1gFnSc7	Qualiba
(	(	kIx(	(
<g/>
950	[number]	k4	950
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vybouchla	vybouchnout	k5eAaPmAgFnS	vybouchnout
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
z	z	k7c2	z
části	část	k1gFnSc2	část
vymýcen	vymýcen	k2eAgMnSc1d1	vymýcen
<g/>
.	.	kIx.	.
</s>
<s>
Nejpůsobivější	působivý	k2eAgInPc1d3	nejpůsobivější
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
staré	starý	k2eAgInPc1d1	starý
sopečné	sopečný	k2eAgInPc1d1	sopečný
kužely	kužel	k1gInPc1	kužel
Gros	gros	k1gNnSc2	gros
Piton	Piton	k1gInSc1	Piton
a	a	k8xC	a
Petit	petit	k1gInSc1	petit
Piton	Pitona	k1gFnPc2	Pitona
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
vrcholy	vrchol	k1gInPc1	vrchol
jsou	být	k5eAaImIp3nP	být
zalesněné	zalesněný	k2eAgInPc1d1	zalesněný
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
vrcholy	vrchol	k1gInPc1	vrchol
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
vrcholků	vrcholek	k1gInPc2	vrcholek
Pitons	Pitonsa	k1gFnPc2	Pitonsa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnSc2	hora
protíná	protínat	k5eAaImIp3nS	protínat
několik	několik	k4yIc4	několik
krátkých	krátký	k2eAgFnPc2d1	krátká
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
tvoří	tvořit	k5eAaImIp3nP	tvořit
široká	široký	k2eAgNnPc1d1	široké
<g/>
,	,	kIx,	,
úrodná	úrodný	k2eAgNnPc1d1	úrodné
údolí	údolí	k1gNnPc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
nádherné	nádherný	k2eAgFnPc4d1	nádherná
pláže	pláž	k1gFnPc4	pláž
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
průzračným	průzračný	k2eAgNnSc7d1	průzračné
a	a	k8xC	a
teplým	teplý	k2eAgNnSc7d1	teplé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
<g/>
Délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
158	[number]	k4	158
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgInPc4d1	tropický
ovlivněné	ovlivněný	k2eAgInPc4d1	ovlivněný
pasáty	pasát	k1gInPc4	pasát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
od	od	k7c2	od
severovýchodu	severovýchod	k1gInSc2	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
obdobím	období	k1gNnSc7	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInSc2	měsíc
prosinec	prosinec	k1gInSc4	prosinec
až	až	k9	až
květen	květen	k1gInSc4	květen
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
trvá	trvat	k5eAaImIp3nS	trvat
období	období	k1gNnSc4	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
denní	denní	k2eAgFnPc1d1	denní
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
29	[number]	k4	29
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
noční	noční	k2eAgFnSc2d1	noční
teploty	teplota	k1gFnSc2	teplota
okolo	okolo	k7c2	okolo
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
300	[number]	k4	300
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
3	[number]	k4	3
810	[number]	k4	810
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
leží	ležet	k5eAaImIp3nS	ležet
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k7c2	blízko
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
výrazněji	výrazně	k6eAd2	výrazně
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
byli	být	k5eAaImAgMnP	být
Arawakové	Arawakový	k2eAgMnPc4d1	Arawakový
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
200	[number]	k4	200
-	-	kIx~	-
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Awarakové	Awarakový	k2eAgNnSc1d1	Awarakový
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
800	[number]	k4	800
-	-	kIx~	-
1000	[number]	k4	1000
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
Kariby	Karib	k1gMnPc7	Karib
<g/>
.	.	kIx.	.
</s>
<s>
Karibové	Karib	k1gMnPc1	Karib
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
složitou	složitý	k2eAgFnSc4d1	složitá
prosperující	prosperující	k2eAgFnSc4d1	prosperující
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
Evropany	Evropan	k1gMnPc7	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kolonie	kolonie	k1gFnSc1	kolonie
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
Karibové	Karib	k1gMnPc1	Karib
byli	být	k5eAaImAgMnP	být
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sem	sem	k6eAd1	sem
osadníci	osadník	k1gMnPc1	osadník
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
Helenou	Helena	k1gFnSc7	Helena
Západní	západní	k2eAgFnSc2d1	západní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
asociací	asociace	k1gFnSc7	asociace
s	s	k7c7	s
Trojskou	trojský	k2eAgFnSc7d1	Trojská
Helenou	Helena	k1gFnSc7	Helena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
příznačné	příznačný	k2eAgNnSc1d1	příznačné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
vedli	vést	k5eAaImAgMnP	vést
o	o	k7c4	o
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
mnoho	mnoho	k4c4	mnoho
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
změnil	změnit	k5eAaPmAgInS	změnit
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
svého	svůj	k3xOyFgMnSc4	svůj
vlastníka	vlastník	k1gMnSc4	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
připadl	připadnout	k5eAaPmAgMnS	připadnout
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
získal	získat	k5eAaPmAgInS	získat
autonomii	autonomie	k1gFnSc4	autonomie
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
skoro	skoro	k6eAd1	skoro
90	[number]	k4	90
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
menšina	menšina	k1gFnSc1	menšina
Indů	Ind	k1gMnPc2	Ind
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
příjmů	příjem	k1gInPc2	příjem
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
turismus	turismus	k1gInSc1	turismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
celkovém	celkový	k2eAgNnSc6d1	celkové
HDP	HDP	kA	HDP
podílí	podílet	k5eAaImIp3nS	podílet
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
z	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Třetina	třetina	k1gFnSc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
vývozním	vývozní	k2eAgInSc7d1	vývozní
artiklem	artikl	k1gInSc7	artikl
jsou	být	k5eAaImIp3nP	být
banány	banán	k1gInPc4	banán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
kokosový	kokosový	k2eAgInSc1d1	kokosový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
a	a	k8xC	a
mango	mango	k1gNnSc1	mango
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
dovážet	dovážet	k5eAaImF	dovážet
(	(	kIx(	(
<g/>
především	především	k9	především
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Trinidadu	Trinidad	k1gInSc2	Trinidad
a	a	k8xC	a
Tobaga	Tobaga	k1gFnSc1	Tobaga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
využívat	využívat	k5eAaImF	využívat
geotermální	geotermální	k2eAgFnSc4d1	geotermální
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
174	[number]	k4	174
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozprostřených	rozprostřený	k2eAgMnPc2d1	rozprostřený
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
a	a	k8xC	a
venkovských	venkovský	k2eAgFnPc6d1	venkovská
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Castries	Castriesa	k1gFnPc2	Castriesa
žije	žít	k5eAaImIp3nS	žít
32,4	[number]	k4	32,4
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Emigrace	emigrace	k1gFnSc1	emigrace
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
Svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1,2	[number]	k4	1,2
%	%	kIx~	%
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Migranti	migrant	k1gMnPc1	migrant
většinou	většinou	k6eAd1	většinou
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
anglofonní	anglofonní	k2eAgFnPc4d1	anglofonní
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
ze	z	k7c2	z
Svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
místem	místo	k1gNnSc7	místo
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
afrického	africký	k2eAgMnSc2d1	africký
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
africko-evropského	africkovropský	k2eAgInSc2d1	africko-evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Karibské	karibský	k2eAgNnSc4d1	Karibské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
činní	činný	k2eAgMnPc1d1	činný
2	[number]	k4	2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římským	římský	k2eAgMnPc3d1	římský
katolíkům	katolík	k1gMnPc3	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
Svaté	svatý	k2eAgFnSc2d1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
95	[number]	k4	95
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
ovšem	ovšem	k9	ovšem
používá	používat	k5eAaImIp3nS	používat
kreolštinu	kreolština	k1gFnSc4	kreolština
tzv.	tzv.	kA	tzv.
patois	patois	k1gFnSc1	patois
<g/>
,	,	kIx,	,
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
např.	např.	kA	např.
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
17	[number]	k4	17
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
komora	komora	k1gFnSc1	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
11	[number]	k4	11
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
regionálních	regionální	k2eAgNnPc2d1	regionální
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Organizace	organizace	k1gFnSc1	organizace
východokaribských	východokaribský	k2eAgInPc2d1	východokaribský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
OECS	OECS	kA	OECS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karibského	karibský	k2eAgNnSc2d1	Karibské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
CARICOM	CARICOM	kA	CARICOM
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bolívarovského	Bolívarovský	k2eAgInSc2d1	Bolívarovský
svazu	svaz	k1gInSc2	svaz
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
ALBA	alba	k1gFnSc1	alba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
CELAC	CELAC	kA	CELAC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
ACS-AEC	ACS-AEC	k1gFnPc2	ACS-AEC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Regionální	regionální	k2eAgInSc1d1	regionální
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
RSS	RSS	kA	RSS
<g/>
)	)	kIx)	)
či	či	k8xC	či
Petrocaribe	Petrocarib	k1gInSc5	Petrocarib
<g/>
.	.	kIx.	.
</s>
