<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
vlastnost	vlastnost	k1gFnSc1	vlastnost
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
chameleon	chameleon	k1gMnSc1	chameleon
pohnul	pohnout	k5eAaPmAgMnS	pohnout
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
objekt	objekt	k1gInSc4	objekt
dívat	dívat	k5eAaImF	dívat
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
<g/>
.	.	kIx.	.
</s>
