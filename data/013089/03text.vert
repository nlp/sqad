<p>
<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
romantická	romantický	k2eAgFnSc1d1	romantická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
baletem	balet	k1gInSc7	balet
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Karla	Karel	k1gMnSc2	Karel
Šebora	Šebor	k1gMnSc2	Šebor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Böhma	Böhm	k1gMnSc2	Böhm
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
dramatu	drama	k1gNnSc2	drama
spisovatele	spisovatel	k1gMnSc2	spisovatel
Františka	František	k1gMnSc2	František
Šíra	Šír	k1gMnSc2	Šír
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Šebor	Šebor	k1gMnSc1	Šebor
napsal	napsat	k5eAaPmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
operu	opera	k1gFnSc4	opera
Drahomíra	Drahomír	k1gMnSc2	Drahomír
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
své	svůj	k3xOyFgFnSc2	svůj
operní	operní	k2eAgFnSc2d1	operní
prvotiny	prvotina	k1gFnSc2	prvotina
Templáři	templář	k1gMnPc1	templář
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
také	také	k6eAd1	také
místo	místo	k7c2	místo
druhého	druhý	k4xOgInSc2	druhý
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
třetího	třetí	k4xOgInSc2	třetí
<g/>
)	)	kIx)	)
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
sbormistra	sbormistr	k1gMnSc2	sbormistr
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Látkově	látkově	k6eAd1	látkově
i	i	k9	i
stylově	stylově	k6eAd1	stylově
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
Templáře	templář	k1gMnPc4	templář
již	již	k6eAd1	již
opětovnou	opětovný	k2eAgFnSc7d1	opětovná
volbou	volba	k1gFnSc7	volba
námětu	námět	k1gInSc2	námět
z	z	k7c2	z
dávné	dávný	k2eAgFnSc2d1	dávná
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
formou	forma	k1gFnSc7	forma
divadelně	divadelně	k6eAd1	divadelně
účinné	účinný	k2eAgFnSc2d1	účinná
velké	velký	k2eAgFnSc2d1	velká
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
však	však	k9	však
projevily	projevit	k5eAaPmAgInP	projevit
vliv	vliv	k1gInSc4	vliv
italské	italský	k2eAgFnPc1d1	italská
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
soustředění	soustředění	k1gNnSc6	soustředění
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
postavě	postava	k1gFnSc3	postava
<g/>
,	,	kIx,	,
vykreslené	vykreslený	k2eAgFnSc3d1	vykreslená
jako	jako	k8xS	jako
fanatická	fanatický	k2eAgFnSc1d1	fanatická
pohanka	pohanka	k1gFnSc1	pohanka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kouzelnice	kouzelnice	k1gFnSc1	kouzelnice
podobná	podobný	k2eAgFnSc1d1	podobná
Médei	Médea	k1gFnSc3	Médea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
patří	patřit	k5eAaImIp3nS	patřit
navíc	navíc	k6eAd1	navíc
do	do	k7c2	do
oboru	obor	k1gInSc2	obor
dramatické	dramatický	k2eAgFnSc2d1	dramatická
koloratury	koloratura	k1gFnSc2	koloratura
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
opeře	opera	k1gFnSc6	opera
nezvyklý	zvyklý	k2eNgInSc1d1	nezvyklý
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
pro	pro	k7c4	pro
vysoký	vysoký	k2eAgInSc4d1	vysoký
mezzosoprán	mezzosoprán	k1gInSc4	mezzosoprán
<g/>
.	.	kIx.	.
<g/>
Známou	známý	k2eAgFnSc4d1	známá
legendární	legendární	k2eAgFnSc4d1	legendární
látku	látka	k1gFnSc4	látka
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
vraždy	vražda	k1gFnSc2	vražda
svatořečeného	svatořečený	k2eAgMnSc2d1	svatořečený
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
libretista	libretista	k1gMnSc1	libretista
Jindřich	Jindřich	k1gMnSc1	Jindřich
Böhm	Böhm	k1gMnSc1	Böhm
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
předlohy	předloha	k1gFnSc2	předloha
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
národního	národní	k2eAgMnSc2d1	národní
buditele	buditel	k1gMnSc2	buditel
Františka	František	k1gMnSc2	František
Šíra	Šír	k1gMnSc2	Šír
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pojetí	pojetí	k1gNnSc1	pojetí
ještě	ještě	k9	ještě
bez	bez	k7c2	bez
nuancí	nuance	k1gFnPc2	nuance
přebírá	přebírat	k5eAaImIp3nS	přebírat
barokní	barokní	k2eAgFnSc4d1	barokní
legendu	legenda	k1gFnSc4	legenda
s	s	k7c7	s
jednoznačně	jednoznačně	k6eAd1	jednoznačně
negativním	negativní	k2eAgNnSc7d1	negativní
zpodobením	zpodobení	k1gNnSc7	zpodobení
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
a	a	k8xC	a
českého	český	k2eAgNnSc2d1	české
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
i	i	k8xC	i
soudobá	soudobý	k2eAgFnSc1d1	soudobá
kritika	kritika	k1gFnSc1	kritika
vytýkala	vytýkat	k5eAaImAgFnS	vytýkat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
byl	být	k5eAaImAgMnS	být
zcel	zcenout	k5eAaPmAgInS	zcenout
nehistorickým	historický	k2eNgNnSc7d1	nehistorické
vystupováním	vystupování	k1gNnSc7	vystupování
kněžny	kněžna	k1gFnSc2	kněžna
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
jako	jako	k8xC	jako
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
hábitu	hábit	k1gInSc6	hábit
s	s	k7c7	s
čarodějnou	čarodějný	k2eAgFnSc7d1	čarodějná
hůlkou	hůlka	k1gFnSc7	hůlka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
duchy	duch	k1gMnPc7	duch
temnot	temnota	k1gFnPc2	temnota
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
pohoršen	pohoršen	k2eAgMnSc1d1	pohoršen
<g/>
.	.	kIx.	.
</s>
<s>
Fabule	fabule	k1gFnSc1	fabule
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
podobná	podobný	k2eAgFnSc1d1	podobná
opeře	opera	k1gFnSc3	opera
Drahomíra	Drahomír	k1gMnSc2	Drahomír
Františka	František	k1gMnSc2	František
Škroupa	Škroup	k1gMnSc2	Škroup
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
dekády	dekáda	k1gFnPc4	dekáda
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
však	však	k9	však
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
řadu	řada	k1gFnSc4	řada
efektů	efekt	k1gInPc2	efekt
(	(	kIx(	(
<g/>
příležitosti	příležitost	k1gFnSc2	příležitost
pro	pro	k7c4	pro
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
i	i	k8xC	i
pohanské	pohanský	k2eAgInPc4d1	pohanský
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc4	sbor
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
alegorický	alegorický	k2eAgInSc1d1	alegorický
sen	sen	k1gInSc1	sen
i	i	k8xC	i
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
kontrast	kontrast	k1gInSc1	kontrast
scén	scéna	k1gFnPc2	scéna
nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
propadu	propad	k1gInSc2	propad
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
do	do	k7c2	do
pekel	peklo	k1gNnPc2	peklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
charakterizované	charakterizovaný	k2eAgFnPc4d1	charakterizovaná
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
dramatický	dramatický	k2eAgInSc4d1	dramatický
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Drahomíru	Drahomíra	k1gFnSc4	Drahomíra
velmi	velmi	k6eAd1	velmi
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bouřlivým	bouřlivý	k2eAgInSc7d1	bouřlivý
potleskem	potlesk	k1gInSc7	potlesk
a	a	k8xC	a
četným	četný	k2eAgNnSc7d1	četné
vyvoláváním	vyvolávání	k1gNnSc7	vyvolávání
<g/>
"	"	kIx"	"
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
<g/>
Kritika	kritika	k1gFnSc1	kritika
Šeborovu	Šeborův	k2eAgFnSc4d1	Šeborův
hudbu	hudba	k1gFnSc4	hudba
přivítala	přivítat	k5eAaPmAgFnS	přivítat
vcelku	vcelku	k6eAd1	vcelku
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
uznávala	uznávat	k5eAaImAgFnS	uznávat
značný	značný	k2eAgInSc4d1	značný
pokrok	pokrok	k1gInSc4	pokrok
od	od	k7c2	od
Templářů	templář	k1gMnPc2	templář
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
hudebně-dramatické	hudebněramatický	k2eAgFnSc6d1	hudebně-dramatická
jednotnosti	jednotnost	k1gFnSc6	jednotnost
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
zdařilejší	zdařilý	k2eAgFnSc4d2	zdařilejší
charakterizaci	charakterizace	k1gFnSc4	charakterizace
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
titulní	titulní	k2eAgInPc1d1	titulní
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybný	pochybný	k2eNgInSc1d1	nepochybný
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
efektní	efektní	k2eAgNnSc4d1	efektní
uspořádání	uspořádání	k1gNnSc4	uspořádání
divadelní	divadelní	k2eAgFnSc2d1	divadelní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
melodická	melodický	k2eAgFnSc1d1	melodická
invence	invence	k1gFnSc1	invence
a	a	k8xC	a
zejména	zejména	k9	zejména
"	"	kIx"	"
<g/>
čilý	čilý	k2eAgInSc4d1	čilý
a	a	k8xC	a
bohatý	bohatý	k2eAgInSc4d1	bohatý
rytmický	rytmický	k2eAgInSc4d1	rytmický
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
spatřován	spatřován	k2eAgInSc1d1	spatřován
Šeborův	Šeborův	k2eAgInSc1d1	Šeborův
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Poukazovalo	poukazovat	k5eAaImAgNnS	poukazovat
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
strukturní	strukturní	k2eAgFnSc4d1	strukturní
jednotvárnost	jednotvárnost	k1gFnSc4	jednotvárnost
a	a	k8xC	a
šablonovitost	šablonovitost	k1gFnSc4	šablonovitost
uzavřených	uzavřený	k2eAgNnPc2d1	uzavřené
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
italizující	italizující	k2eAgFnSc1d1	italizující
stretty	stretta	k1gFnPc1	stretta
<g/>
,	,	kIx,	,
unisona	unisono	k1gNnPc1	unisono
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
a	a	k8xC	a
nezpěvnost	nezpěvnost	k1gFnSc4	nezpěvnost
sólových	sólový	k2eAgFnPc2d1	sólová
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
instrumentaci	instrumentace	k1gFnSc6	instrumentace
kritika	kritika	k1gFnSc1	kritika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Ludevít	Ludevít	k1gMnSc1	Ludevít
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
)	)	kIx)	)
Šeborovi	Šebor	k1gMnSc3	Šebor
přiznávala	přiznávat	k5eAaImAgFnS	přiznávat
"	"	kIx"	"
<g/>
značnou	značný	k2eAgFnSc4d1	značná
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
hlučnosti	hlučnost	k1gFnSc3	hlučnost
a	a	k8xC	a
lomoznosti	lomoznost	k1gFnSc3	lomoznost
<g/>
,	,	kIx,	,
a	a	k8xC	a
doporučovala	doporučovat	k5eAaImAgFnS	doporučovat
její	její	k3xOp3gNnSc4	její
odlehčení	odlehčení	k1gNnSc4	odlehčení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
melodie	melodie	k1gFnSc2	melodie
a	a	k8xC	a
celé	celý	k2eAgFnPc4d1	celá
faktury	faktura	k1gFnPc4	faktura
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
vytýkána	vytýkán	k2eAgFnSc1d1	vytýkána
nepůvodnost	nepůvodnost	k1gFnSc1	nepůvodnost
a	a	k8xC	a
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
vzorech	vzor	k1gInPc6	vzor
(	(	kIx(	(
<g/>
Meyerbeer	Meyerbeer	k1gMnSc1	Meyerbeer
<g/>
,	,	kIx,	,
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Glinka	Glinka	k1gMnSc1	Glinka
a	a	k8xC	a
zejména	zejména	k9	zejména
Verdi	Verd	k1gMnPc1	Verd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
nedostatek	nedostatek	k1gInSc4	nedostatek
uměleckého	umělecký	k2eAgInSc2d1	umělecký
ideálu	ideál	k1gInSc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
míst	místo	k1gNnPc2	místo
byl	být	k5eAaImAgInS	být
vyzdvižen	vyzdvihnout	k5eAaPmNgInS	vyzdvihnout
part	part	k1gInSc4	part
Slavíny	Slavín	k1gInPc7	Slavín
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sóla	sólo	k1gNnPc1	sólo
(	(	kIx(	(
<g/>
modlitba	modlitba	k1gFnSc1	modlitba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
její	její	k3xOp3gInPc4	její
duety	duet	k1gInPc4	duet
s	s	k7c7	s
Milínem	Milín	k1gInSc7	Milín
a	a	k8xC	a
Krůvojem	Krůvoj	k1gInSc7	Krůvoj
<g/>
,	,	kIx,	,
vstupní	vstupní	k2eAgInSc1d1	vstupní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
kvartet	kvartet	k1gInSc1	kvartet
a	a	k8xC	a
cappella	cappella	k1gFnSc1	cappella
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
a	a	k8xC	a
také	také	k9	také
baletní	baletní	k2eAgFnSc1d1	baletní
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
však	však	k9	však
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
radila	radit	k5eAaImAgFnS	radit
k	k	k7c3	k
důkladným	důkladný	k2eAgInPc3d1	důkladný
škrtům	škrt	k1gInPc3	škrt
<g/>
.	.	kIx.	.
<g/>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
první	první	k4xOgFnSc6	první
inscenaci	inscenace	k1gFnSc6	inscenace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
zařazena	zařadit	k5eAaPmNgFnS	zařadit
opět	opět	k6eAd1	opět
na	na	k7c4	na
hrací	hrací	k2eAgInSc4d1	hrací
plán	plán	k1gInSc4	plán
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
20	[number]	k4	20
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
inscenována	inscenován	k2eAgFnSc1d1	inscenována
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
první	první	k4xOgNnPc4	první
obsazení	obsazení	k1gNnPc4	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaImRp2nS	dít
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Svatoháj	svatoháj	k1gInSc4	svatoháj
v	v	k7c6	v
rozkošné	rozkošný	k2eAgFnSc6d1	rozkošná
krajině	krajina	k1gFnSc6	krajina
na	na	k7c6	na
Boleslavsku	Boleslavsko	k1gNnSc6	Boleslavsko
<g/>
)	)	kIx)	)
V	v	k7c6	v
posvátném	posvátný	k2eAgInSc6d1	posvátný
háji	háj	k1gInSc6	háj
poblíž	poblíž	k7c2	poblíž
hradu	hrad	k1gInSc2	hrad
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
<g/>
,	,	kIx,	,
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
panujícího	panující	k2eAgMnSc2d1	panující
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pohanský	pohanský	k2eAgInSc1d1	pohanský
obřad	obřad	k1gInSc1	obřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
<g/>
,	,	kIx,	,
příchylník	příchylník	k1gInSc1	příchylník
ovdovělé	ovdovělý	k2eAgFnSc2d1	ovdovělá
kněžny-matky	kněžnyatka	k1gFnSc2	kněžny-matka
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Vděkem	vděk	k1gInSc7	vděk
duše	duše	k1gFnSc2	duše
rozjařených	rozjařený	k2eAgFnPc2d1	rozjařená
plesá	plesat	k5eAaImIp3nS	plesat
lidu	lid	k1gInSc2	lid
tvého	tvůj	k1gMnSc2	tvůj
dav	dav	k1gInSc1	dav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pohanů	pohan	k1gMnPc2	pohan
<g/>
,	,	kIx,	,
Týra	Týrus	k1gMnSc2	Týrus
<g/>
,	,	kIx,	,
předvádí	předvádět	k5eAaImIp3nS	předvádět
vetřelce	vetřelec	k1gMnPc4	vetřelec
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
bojovník	bojovník	k1gMnSc1	bojovník
a	a	k8xC	a
Václavův	Václavův	k2eAgMnSc1d1	Václavův
družiník	družiník	k1gMnSc1	družiník
jménem	jméno	k1gNnSc7	jméno
Milín	Milín	k1gMnSc1	Milín
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
povýšeně	povýšeně	k6eAd1	povýšeně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
hněvem	hněv	k1gInSc7	hněv
přítomných	přítomný	k2eAgMnPc2d1	přítomný
pohanů	pohan	k1gMnPc2	pohan
se	se	k3xPyFc4	se
dovolává	dovolávat	k5eAaImIp3nS	dovolávat
svého	svůj	k3xOyFgNnSc2	svůj
práva	právo	k1gNnSc2	právo
jakožto	jakožto	k8xS	jakožto
hosta	host	k1gMnSc4	host
u	u	k7c2	u
knížete	kníže	k1gNnSc2wR	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Krůvojově	Krůvojův	k2eAgFnSc3d1	Krůvojův
dceři	dcera	k1gFnSc3	dcera
Slavíně	Slavín	k1gInSc6	Slavín
Milínův	Milínův	k2eAgInSc1d1	Milínův
hrdinný	hrdinný	k2eAgInSc1d1	hrdinný
zjev	zjev	k1gInSc1	zjev
imponuje	imponovat	k5eAaImIp3nS	imponovat
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozzuřený	rozzuřený	k2eAgInSc1d1	rozzuřený
dav	dav	k1gInSc1	dav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
žádá	žádat	k5eAaImIp3nS	žádat
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
oběť	oběť	k1gFnSc4	oběť
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
otci	otec	k1gMnSc3	otec
podaří	podařit	k5eAaPmIp3nS	podařit
společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
uklidnit	uklidnit	k5eAaPmF	uklidnit
a	a	k8xC	a
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
posvátné	posvátný	k2eAgNnSc1d1	posvátné
místo	místo	k1gNnSc1	místo
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
poskvrněno	poskvrnit	k5eAaPmNgNnS	poskvrnit
krveprolitím	krveprolití	k1gNnSc7	krveprolití
(	(	kIx(	(
<g/>
scéna	scéna	k1gFnSc1	scéna
Kdo	kdo	k3yRnSc1	kdo
ruší	rušit	k5eAaImIp3nS	rušit
poklidný	poklidný	k2eAgInSc4d1	poklidný
ten	ten	k3xDgInSc4	ten
radovánek	radovánka	k1gFnPc2	radovánka
<g/>
?	?	kIx.	?
</s>
<s>
se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
Milína	Milín	k1gInSc2	Milín
Jsem	být	k5eAaImIp1nS	být
věrný	věrný	k2eAgMnSc1d1	věrný
sluha	sluha	k1gMnSc1	sluha
svého	své	k1gNnSc2	své
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
sborem	sborem	k6eAd1	sborem
Ha	ha	kA	ha
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
zrádce	zrádce	k1gMnSc1	zrádce
<g/>
,	,	kIx,	,
svatokrádce	svatokrádce	k1gMnSc1	svatokrádce
<g/>
,	,	kIx,	,
zpěvem	zpěv	k1gInSc7	zpěv
Milína	Milín	k1gInSc2	Milín
Knížete	kníže	k1gNnSc2wR	kníže
jsem	být	k5eAaImIp1nS	být
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
ku	k	k7c3	k
hostinám	hostina	k1gFnPc3	hostina
<g/>
,	,	kIx,	,
sborem	sborem	k6eAd1	sborem
Potupná	potupný	k2eAgFnSc1d1	potupná
poslyšte	poslyšet	k5eAaPmRp2nP	poslyšet
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
ansámblem	ansámbl	k1gInSc7	ansámbl
Ustaňte	ustat	k5eAaPmRp2nP	ustat
již	již	k6eAd1	již
od	od	k7c2	od
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
zlosti	zlost	k1gFnSc2	zlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Milín	Milín	k1gInSc1	Milín
děkuje	děkovat	k5eAaImIp3nS	děkovat
Slavíně	Slavín	k1gInSc6	Slavín
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
Můj	můj	k3xOp1gInSc1	můj
strážný	strážný	k2eAgMnSc5d1	strážný
anděli	anděl	k1gMnSc5	anděl
<g/>
!	!	kIx.	!
</s>
<s>
o	o	k7c6	o
děvo	děva	k1gFnSc5	děva
z	z	k7c2	z
roje	roj	k1gInSc2	roj
víl	víla	k1gFnPc2	víla
<g/>
...	...	k?	...
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
zříti	zřít	k5eAaImF	zřít
v	v	k7c4	v
oko	oko	k1gNnSc4	oko
tvé	tvůj	k3xOp2gInPc4	tvůj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
vytratí	vytratit	k5eAaPmIp3nP	vytratit
<g/>
,	,	kIx,	,
když	když	k8xS	když
svaté	svatý	k2eAgNnSc1d1	svaté
místo	místo	k1gNnSc1	místo
navštíví	navštívit	k5eAaPmIp3nS	navštívit
osamělá	osamělý	k2eAgFnSc1d1	osamělá
kněžna	kněžna	k1gFnSc1	kněžna
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
slávě	sláva	k1gFnSc6	sláva
a	a	k8xC	a
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
přišla	přijít	k5eAaPmAgFnS	přijít
se	se	k3xPyFc4	se
zeptat	zeptat	k5eAaPmF	zeptat
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jí	on	k3xPp3gFnSc3	on
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnPc1	její
přání	přání	k1gNnPc1	přání
budou	být	k5eAaImBp3nP	být
plněna	plněn	k2eAgNnPc1d1	plněno
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
nepostaví	postavit	k5eNaPmIp3nP	postavit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Drahomíra	drahomíra	k1gMnSc1	drahomíra
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starší	k1gMnSc2	starší
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
(	(	kIx(	(
<g/>
arioso	arioso	k1gNnSc1	arioso
Již	již	k6eAd1	již
déle	dlouho	k6eAd2	dlouho
nemohu	moct	k5eNaImIp1nS	moct
ji	on	k3xPp3gFnSc4	on
nésti	nést	k5eAaImF	nést
<g/>
,	,	kIx,	,
melodram	melodram	k1gInSc1	melodram
Tvá	tvůj	k3xOp2gNnPc4	tvůj
než	než	k8xS	než
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
proti	proti	k7c3	proti
tobě	ty	k3xPp2nSc3	ty
a	a	k8xC	a
árie	árie	k1gFnPc1	árie
Vrženy	vržen	k2eAgFnPc1d1	vržena
jsou	být	k5eAaImIp3nP	být
kostky	kostka	k1gFnPc1	kostka
mé	můj	k3xOp1gNnSc4	můj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
pohany	pohana	k1gFnPc4	pohana
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
klenba	klenba	k1gFnSc1	klenba
sloupová	sloupový	k2eAgFnSc1d1	sloupová
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
knížecím	knížecí	k2eAgInSc6d1	knížecí
<g/>
)	)	kIx)	)
Slavína	Slavín	k1gInSc2	Slavín
se	se	k3xPyFc4	se
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
se	s	k7c7	s
zlou	zlý	k2eAgFnSc7d1	zlá
předtuchou	předtucha	k1gFnSc7	předtucha
a	a	k8xC	a
viděla	vidět	k5eAaImAgFnS	vidět
dva	dva	k4xCgMnPc4	dva
biřice	biřic	k1gMnPc4	biřic
spěchat	spěchat	k5eAaImF	spěchat
z	z	k7c2	z
komnaty	komnata	k1gFnSc2	komnata
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Přikvapí	přikvapit	k5eAaPmIp3nS	přikvapit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
Milín	Milín	k1gInSc1	Milín
a	a	k8xC	a
utěšuje	utěšovat	k5eAaImIp3nS	utěšovat
ji	on	k3xPp3gFnSc4	on
<g/>
:	:	kIx,	:
kdo	kdo	k3yRnSc1	kdo
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
pravého	pravý	k2eAgMnSc4d1	pravý
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
se	se	k3xPyFc4	se
obávat	obávat	k5eAaImF	obávat
(	(	kIx(	(
<g/>
recitativ	recitativ	k1gInSc1	recitativ
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
spánek	spánek	k1gInSc1	spánek
prchá	prchat	k5eAaImIp3nS	prchat
z	z	k7c2	z
oka	oko	k1gNnSc2	oko
mého	můj	k3xOp1gNnSc2	můj
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
duet	duet	k1gInSc1	duet
Proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
zbožný	zbožný	k2eAgMnSc1d1	zbožný
bál	bát	k5eAaImAgMnS	bát
se	se	k3xPyFc4	se
duch	duch	k1gMnSc1	duch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
uryjí	urýt	k5eAaPmIp3nP	urýt
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
aby	aby	kYmCp3nP	aby
vyslechli	vyslechnout	k5eAaPmAgMnP	vyslechnout
rozhněvaného	rozhněvaný	k2eAgMnSc4d1	rozhněvaný
knížete	kníže	k1gMnSc4	kníže
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dal	dát	k5eAaPmAgMnS	dát
nařídit	nařídit	k5eAaPmF	nařídit
vraždu	vražda	k1gFnSc4	vražda
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
pochyby	pochyba	k1gFnPc4	pochyba
a	a	k8xC	a
svaluje	svalovat	k5eAaImIp3nS	svalovat
zločin	zločin	k1gInSc1	zločin
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
jeho	jeho	k3xOp3gNnSc4	jeho
zbrojnoš	zbrojnoš	k1gMnSc1	zbrojnoš
Hněvsa	Hněvs	k1gMnSc2	Hněvs
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
n	n	k0	n
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
:	:	kIx,	:
jen	jen	k9	jen
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
Týrou	Týra	k1gMnSc7	Týra
vztáhli	vztáhnout	k5eAaPmAgMnP	vztáhnout
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
zazněl	zaznít	k5eAaPmAgInS	zaznít
hromový	hromový	k2eAgInSc1d1	hromový
varovný	varovný	k2eAgInSc1d1	varovný
hlas	hlas	k1gInSc1	hlas
a	a	k8xC	a
zbraň	zbraň	k1gFnSc1	zbraň
jim	on	k3xPp3gFnPc3	on
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
ke	k	k7c3	k
hradní	hradní	k2eAgFnSc3d1	hradní
kapli	kaple	k1gFnSc3	kaple
<g/>
,	,	kIx,	,
a	a	k8xC	a
běží	běžet	k5eAaImIp3nS	běžet
vykonat	vykonat	k5eAaPmF	vykonat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
záměr	záměr	k1gInSc4	záměr
(	(	kIx(	(
<g/>
scéna	scéna	k1gFnSc1	scéna
Strach	strach	k1gInSc1	strach
a	a	k8xC	a
hrůza	hrůza	k1gFnSc1	hrůza
z	z	k7c2	z
uhlů	uhel	k1gInPc2	uhel
všech	všecek	k3xTgInPc6	všecek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Milín	Milín	k1gInSc1	Milín
se	se	k3xPyFc4	se
loučí	loučit	k5eAaImIp3nS	loučit
se	s	k7c7	s
Slavínou	Slavína	k1gFnSc7	Slavína
a	a	k8xC	a
spěchá	spěchat	k5eAaImIp3nS	spěchat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
(	(	kIx(	(
<g/>
duetino	duetina	k1gFnSc5	duetina
Bratrova	bratrův	k2eAgFnSc1d1	bratrova
kde	kde	k6eAd1	kde
láska	láska	k1gFnSc1	láska
vraždu	vražda	k1gFnSc4	vražda
káže	kázat	k5eAaImIp3nS	kázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
se	se	k3xPyFc4	se
Slavína	Slavín	k1gInSc2	Slavín
modlí	modlit	k5eAaImIp3nS	modlit
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
vidění	vidění	k1gNnSc4	vidění
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
modlitba	modlitba	k1gFnSc1	modlitba
Slunce	slunce	k1gNnSc1	slunce
boha	bůh	k1gMnSc2	bůh
velikého	veliký	k2eAgMnSc2d1	veliký
všecko	všecek	k3xTgNnSc4	všecek
budiž	budiž	k9	budiž
k	k	k7c3	k
živosti	živost	k1gFnSc3	živost
a	a	k8xC	a
scéna	scéna	k1gFnSc1	scéna
vidění	vidění	k1gNnSc2	vidění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
posílá	posílat	k5eAaImIp3nS	posílat
Slavínu	Slavín	k1gInSc3	Slavín
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
a	a	k8xC	a
zpravuje	zpravovat	k5eAaImIp3nS	zpravovat
matku	matka	k1gFnSc4	matka
o	o	k7c6	o
Václavově	Václavův	k2eAgFnSc6d1	Václavova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
se	se	k3xPyFc4	se
raduje	radovat	k5eAaImIp3nS	radovat
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
cítí	cítit	k5eAaImIp3nS	cítit
vinu	vina	k1gFnSc4	vina
a	a	k8xC	a
proklíná	proklínat	k5eAaImIp3nS	proklínat
svůj	svůj	k3xOyFgInSc4	svůj
zločin	zločin	k1gInSc4	zločin
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ho	on	k3xPp3gMnSc4	on
naváděla	navádět	k5eAaImAgFnS	navádět
(	(	kIx(	(
<g/>
duet	duet	k1gInSc4	duet
Nuž	nuž	k9	nuž
<g/>
,	,	kIx,	,
pověz	povědět	k5eAaPmRp2nS	povědět
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc5	syn
<g/>
...	...	k?	...
Rozkoš	rozkoš	k1gFnSc1	rozkoš
popohání	popohánět	k5eAaImIp3nS	popohánět
krev	krev	k1gFnSc4	krev
mi	já	k3xPp1nSc3	já
ku	k	k7c3	k
plesání	plesání	k1gNnSc3	plesání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
půtka	půtka	k1gFnSc1	půtka
<g/>
,	,	kIx,	,
pohanští	pohanský	k2eAgMnPc1d1	pohanský
zbrojenci	zbrojenec	k1gMnPc1	zbrojenec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Krůvojem	Krůvoj	k1gInSc7	Krůvoj
a	a	k8xC	a
děvice	děvice	k1gFnPc1	děvice
přinášejí	přinášet	k5eAaImIp3nP	přinášet
kněžně	kněžna	k1gFnSc3	kněžna
a	a	k8xC	a
knížeti	kníže	k1gMnSc3	kníže
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
věrní	věrný	k2eAgMnPc1d1	věrný
vedení	vedený	k2eAgMnPc1d1	vedený
Milínem	Milín	k1gMnSc7	Milín
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
ansámbl	ansámbl	k1gInSc1	ansámbl
Kopí	kopit	k5eAaImIp3nS	kopit
a	a	k8xC	a
meče	meč	k1gInPc4	meč
kruté	krutý	k2eAgInPc4d1	krutý
do	do	k7c2	do
seče	seč	k1gFnSc2	seč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
chce	chtít	k5eAaImIp3nS	chtít
Milína	Milín	k1gMnSc4	Milín
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
,	,	kIx,	,
Slavína	Slavín	k1gInSc2	Slavín
doufá	doufat	k5eAaImIp3nS	doufat
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
,	,	kIx,	,
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslav	Boleslav	k1gMnSc1	Boleslav
v	v	k7c6	v
boji	boj	k1gInSc6	boj
setřese	setřást	k5eAaPmIp3nS	setřást
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
chce	chtít	k5eAaImIp3nS	chtít
po	po	k7c6	po
boji	boj	k1gInSc6	boj
ukončit	ukončit	k5eAaPmF	ukončit
rozkol	rozkol	k1gInSc4	rozkol
a	a	k8xC	a
napravit	napravit	k5eAaPmF	napravit
svůj	svůj	k3xOyFgInSc4	svůj
zločin	zločin	k1gInSc4	zločin
(	(	kIx(	(
<g/>
kvartet	kvartet	k1gInSc1	kvartet
Zde	zde	k6eAd1	zde
palmy	palma	k1gFnSc2	palma
nechci	chtít	k5eNaImIp1nS	chtít
dobývati	dobývat	k5eAaImF	dobývat
<g/>
...	...	k?	...
Vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
přejte	přát	k5eAaImRp2nP	přát
bozi	bůh	k1gMnPc1	bůh
nám	my	k3xPp1nPc3	my
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
síň	síň	k1gFnSc1	síň
korunní	korunní	k2eAgFnSc1d1	korunní
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
knížecím	knížecí	k2eAgInSc6d1	knížecí
<g/>
)	)	kIx)	)
Lid	Lido	k1gNnPc2	Lido
vítá	vítat	k5eAaImIp3nS	vítat
zpět	zpět	k6eAd1	zpět
vítězného	vítězný	k2eAgMnSc4d1	vítězný
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
mu	on	k3xPp3gMnSc3	on
podává	podávat	k5eAaImIp3nS	podávat
vavřín	vavřín	k1gInSc4	vavřín
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
jej	on	k3xPp3gInSc4	on
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
přestol	přestol	k1gInSc4	přestol
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Ať	ať	k8xS	ať
chvála	chvála	k1gFnSc1	chvála
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc1	čest
a	a	k8xC	a
vítězosláva	vítězosláva	k1gFnSc1	vítězosláva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
ujímá	ujímat	k5eAaImIp3nS	ujímat
vlády	vláda	k1gFnSc2	vláda
<g/>
;	;	kIx,	;
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
díle	díl	k1gInSc6	díl
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
matku	matka	k1gFnSc4	matka
však	však	k9	však
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
Boleslava	Boleslav	k1gMnSc2	Boleslav
K	k	k7c3	k
vladařskému	vladařský	k2eAgNnSc3d1	vladařské
nastolení	nastolení	k1gNnSc3	nastolení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
je	být	k5eAaImIp3nS	být
zaražena	zaražen	k2eAgFnSc1d1	zaražena
<g/>
.	.	kIx.	.
</s>
<s>
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
jí	on	k3xPp3gFnSc3	on
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bohům	bůh	k1gMnPc3	bůh
na	na	k7c6	na
usmíření	usmíření	k1gNnSc6	usmíření
dala	dát	k5eAaPmAgFnS	dát
lidskou	lidský	k2eAgFnSc4d1	lidská
oběť	oběť	k1gFnSc4	oběť
–	–	k?	–
vězně	vězeň	k1gMnSc4	vězeň
Milína	Milín	k1gMnSc4	Milín
(	(	kIx(	(
<g/>
scéna	scéna	k1gFnSc1	scéna
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
pyšné	pyšný	k2eAgNnSc1d1	pyšné
počínání	počínání	k1gNnSc1	počínání
<g/>
:	:	kIx,	:
výhost	výhost	k1gInSc1	výhost
matce	matka	k1gFnSc3	matka
dává	dávat	k5eAaImIp3nS	dávat
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavína	Slavín	k1gInSc2	Slavín
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
snaží	snažit	k5eAaImIp3nS	snažit
otce	otec	k1gMnSc4	otec
zadržet	zadržet	k5eAaPmF	zadržet
a	a	k8xC	a
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Milína	Milína	k1gFnSc1	Milína
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
však	však	k9	však
jen	jen	k6eAd1	jen
otcových	otcův	k2eAgFnPc2d1	otcova
výčitek	výčitka	k1gFnPc2	výčitka
<g/>
.	.	kIx.	.
</s>
<s>
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
navíc	navíc	k6eAd1	navíc
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběť	oběť	k1gFnSc1	oběť
musí	muset	k5eAaImIp3nS	muset
vykonat	vykonat	k5eAaPmF	vykonat
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
můj	můj	k1gMnSc5	můj
milý	milý	k1gMnSc5	milý
otče	otec	k1gMnSc5	otec
drahý	drahý	k1gMnSc5	drahý
<g/>
,	,	kIx,	,
zapuď	zapudit	k5eAaPmRp2nS	zapudit
zlobu	zloba	k1gFnSc4	zloba
hněvivou	hněvivý	k2eAgFnSc4d1	hněvivá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Proměna	proměna	k1gFnSc1	proměna
–	–	k?	–
Temná	temnat	k5eAaImIp3nS	temnat
prostora	prostora	k1gFnSc1	prostora
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
)	)	kIx)	)
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
pohanský	pohanský	k2eAgInSc1d1	pohanský
obětní	obětní	k2eAgInSc1d1	obětní
obřad	obřad	k1gInSc1	obřad
(	(	kIx(	(
<g/>
tanec	tanec	k1gInSc1	tanec
kopiníků	kopiník	k1gMnPc2	kopiník
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Točte	točit	k5eAaImRp2nP	točit
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
strání	stráň	k1gFnSc7	stráň
a	a	k8xC	a
dolem	dol	k1gInSc7	dol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
přivléká	přivlékat	k5eAaImIp3nS	přivlékat
Slavínu	Slavín	k1gInSc2	Slavín
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
oběť	oběť	k1gFnSc4	oběť
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
Milína	Milína	k1gFnSc1	Milína
objímá	objímat	k5eAaImIp3nS	objímat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
Krůvoje	Krůvoj	k1gInSc2	Krůvoj
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
Drahomíry	Drahomíra	k1gFnPc4	Drahomíra
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
obětována	obětován	k2eAgFnSc1d1	obětována
i	i	k8xC	i
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
však	však	k9	však
přichází	přicházet	k5eAaImIp3nS	přicházet
Boleslav	Boleslav	k1gMnSc1	Boleslav
v	v	k7c6	v
průvodu	průvod	k1gInSc6	průvod
vojínů	vojín	k1gMnPc2	vojín
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
;	;	kIx,	;
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
zcela	zcela	k6eAd1	zcela
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
obřad	obřad	k1gInSc4	obřad
ruší	rušit	k5eAaImIp3nS	rušit
a	a	k8xC	a
Milína	Milína	k1gFnSc1	Milína
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
<g/>
.	.	kIx.	.
</s>
<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
<g/>
,	,	kIx,	,
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
pohané	pohan	k1gMnPc1	pohan
jsou	být	k5eAaImIp3nP	být
rozezleni	rozezlen	k2eAgMnPc1d1	rozezlen
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
hotoví	hotovit	k5eAaImIp3nP	hotovit
k	k	k7c3	k
rozhodnému	rozhodný	k2eAgInSc3d1	rozhodný
boji	boj	k1gInSc3	boj
(	(	kIx(	(
<g/>
Boleslavův	Boleslavův	k2eAgInSc4d1	Boleslavův
recitativ	recitativ	k1gInSc4	recitativ
Ustaňte	ustat	k5eAaPmRp2nP	ustat
<g/>
!	!	kIx.	!
</s>
<s>
Nezvratná	zvratný	k2eNgFnSc1d1	nezvratná
mi	já	k3xPp1nSc3	já
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
víra	víra	k1gFnSc1	víra
se	se	k3xPyFc4	se
vzňala	vznít	k5eAaPmAgFnS	vznít
a	a	k8xC	a
tercet	tercet	k1gInSc1	tercet
se	se	k3xPyFc4	se
sborem	sborem	k6eAd1	sborem
Soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
já	já	k3xPp1nSc1	já
pronáším	pronášet	k5eAaImIp1nS	pronášet
nad	nad	k7c7	nad
životem	život	k1gInSc7	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Letní	letní	k2eAgFnSc1d1	letní
krajina	krajina	k1gFnSc1	krajina
blíže	blíž	k1gFnSc2	blíž
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
Poražený	poražený	k2eAgInSc1d1	poražený
Krůvoj	Krůvoj	k1gInSc1	Krůvoj
přísahá	přísahat	k5eAaImIp3nS	přísahat
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
boj	boj	k1gInSc4	boj
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
pomoci	pomoct	k5eAaPmF	pomoct
nastolit	nastolit	k5eAaPmF	nastolit
Drahomířinu	Drahomířin	k2eAgFnSc4d1	Drahomířina
přímou	přímý	k2eAgFnSc4d1	přímá
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
recitativ	recitativ	k1gInSc4	recitativ
Dostáti	dostát	k5eAaPmF	dostát
musím	muset	k5eAaImIp1nS	muset
pomstě	pomsta	k1gFnSc3	pomsta
své	svůj	k3xOyFgFnSc2	svůj
co	co	k3yRnSc4	co
bojovník	bojovník	k1gMnSc1	bojovník
a	a	k8xC	a
árie	árie	k1gFnSc1	árie
Svítej	svítat	k5eAaImRp2nS	svítat
<g/>
,	,	kIx,	,
svítek	svítek	k1gInSc1	svítek
<g/>
,	,	kIx,	,
slunko	slunko	k1gNnSc1	slunko
milé	milá	k1gFnSc2	milá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
satební	satební	k2eAgInSc4d1	satební
průvod	průvod	k1gInSc4	průvod
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc4	sbor
Zazpívejme	zazpívat	k5eAaPmRp1nP	zazpívat
<g/>
,	,	kIx,	,
zaplesejme	zaplesat	k5eAaPmRp1nP	zaplesat
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgInS	přijít
radovánku	radovánka	k1gFnSc4	radovánka
čas	čas	k1gInSc1	čas
a	a	k8xC	a
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
doufá	doufat	k5eAaImIp3nS	doufat
v	v	k7c4	v
mír	mír	k1gInSc4	mír
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
odevzdává	odevzdávat	k5eAaImIp3nS	odevzdávat
Slavínu	Slavín	k1gInSc2	Slavín
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Milínovi	Milínův	k2eAgMnPc1d1	Milínův
(	(	kIx(	(
<g/>
Boleslavův	Boleslavův	k2eAgInSc4d1	Boleslavův
zpěv	zpěv	k1gInSc4	zpěv
Bůh	bůh	k1gMnSc1	bůh
pomozi	pomoze	k1gFnSc3	pomoze
naší	náš	k3xOp1gFnSc3	náš
píli	píle	k1gFnSc3	píle
<g/>
,	,	kIx,	,
tercet	tercet	k1gInSc4	tercet
Mé	můj	k3xOp1gNnSc1	můj
požehnání	požehnání	k1gNnSc1	požehnání
budiž	budiž	k9	budiž
vám	vy	k3xPp2nPc3	vy
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Jeden	jeden	k4xCgInSc1	jeden
věčný	věčný	k2eAgInSc4d1	věčný
bože	bůh	k1gMnSc5	bůh
sám	sám	k3xTgMnSc1	sám
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
...	...	k?	...
Hospodine	Hospodin	k1gMnSc5	Hospodin
<g/>
,	,	kIx,	,
pomiluj	pomilovat	k5eAaPmRp2nS	pomilovat
ny	ny	k?	ny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
<g/>
,	,	kIx,	,
odhodlána	odhodlán	k2eAgFnSc1d1	odhodlána
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
za	za	k7c4	za
zničení	zničení	k1gNnSc4	zničení
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
;	;	kIx,	;
před	před	k7c7	před
Krůvojovýma	Krůvojův	k2eAgNnPc7d1	Krůvojův
očima	oko	k1gNnPc7	oko
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vozem	vůz	k1gInSc7	vůz
propadne	propadnout	k5eAaPmIp3nS	propadnout
do	do	k7c2	do
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Drahomířin	Drahomířin	k2eAgInSc1d1	Drahomířin
recitativ	recitativ	k1gInSc1	recitativ
a	a	k8xC	a
árie	árie	k1gFnSc1	árie
Ha	ha	kA	ha
<g/>
!	!	kIx.	!
</s>
<s>
bezpoklidný	bezpoklidný	k2eAgMnSc1d1	bezpoklidný
můj	můj	k1gMnSc1	můj
je	být	k5eAaImIp3nS	být
světem	svět	k1gInSc7	svět
strašný	strašný	k2eAgInSc4d1	strašný
hon	hon	k1gInSc4	hon
<g/>
...	...	k?	...
Slyšte	slyšet	k5eAaImRp2nP	slyšet
<g/>
,	,	kIx,	,
bozi	bůh	k1gMnPc1	bůh
<g/>
!	!	kIx.	!
</s>
<s>
mé	můj	k3xOp1gNnSc1	můj
to	ten	k3xDgNnSc1	ten
plémě	plémě	k1gNnSc1	plémě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávky	nahrávka	k1gFnSc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
opery	opera	k1gFnSc2	opera
byla	být	k5eAaImAgFnS	být
dosud	dosud	k6eAd1	dosud
nahrána	nahrát	k5eAaBmNgFnS	nahrát
předehra	předehra	k1gFnSc1	předehra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
orchestr	orchestr	k1gInSc4	orchestr
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Vašata	Vašat	k1gMnSc2	Vašat
<g/>
;	;	kIx,	;
a	a	k8xC	a
baletní	baletní	k2eAgFnSc1d1	baletní
hudba	hudba	k1gFnSc1	hudba
natočení	natočení	k1gNnSc2	natočení
tamtéž	tamtéž	k6eAd1	tamtéž
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
nahrávky	nahrávka	k1gFnPc1	nahrávka
osud	osud	k1gInSc4	osud
nebyly	být	k5eNaImAgFnP	být
vydány	vydán	k2eAgFnPc1d1	vydána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Libreto	libreto	k1gNnSc1	libreto
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Kramerius	Kramerius	k1gMnSc1	Kramerius
</s>
</p>
<p>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
kritika	kritika	k1gFnSc1	kritika
v	v	k7c6	v
Politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
kritika	kritika	k1gFnSc1	kritika
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
kritika	kritika	k1gFnSc1	kritika
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
2	[number]	k4	2
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
