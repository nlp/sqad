<s>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
romantická	romantický	k2eAgFnSc1d1	romantická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
baletem	balet	k1gInSc7	balet
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Karla	Karel	k1gMnSc2	Karel
Šebora	Šebor	k1gMnSc2	Šebor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Böhma	Böhm	k1gMnSc2	Böhm
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
dramatu	drama	k1gNnSc2	drama
spisovatele	spisovatel	k1gMnSc2	spisovatel
Františka	František	k1gMnSc2	František
Šíra	Šír	k1gMnSc2	Šír
<g/>
.	.	kIx.	.
</s>
