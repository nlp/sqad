<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Lagunové	lagunový	k2eAgInPc1d1	lagunový
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Elliceovy	Elliceův	k2eAgInPc4d1	Elliceův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Havají	Havaj	k1gFnSc7	Havaj
a	a	k8xC	a
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Domorodý	domorodý	k2eAgInSc1d1	domorodý
název	název	k1gInSc1	název
Tuvalu	Tuval	k1gInSc2	Tuval
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
osídlení	osídlení	k1gNnSc4	osídlení
většiny	většina	k1gFnSc2	většina
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
tuvalštině	tuvalština	k1gFnSc6	tuvalština
shluk	shluk	k1gInSc1	shluk
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
osmi	osm	k4xCc2	osm
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
osídlených	osídlený	k2eAgInPc2d1	osídlený
<g/>
,	,	kIx,	,
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
Vatikánu	Vatikán	k1gInSc2	Vatikán
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
(	(	kIx(	(
<g/>
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
tvořící	tvořící	k2eAgInPc1d1	tvořící
tento	tento	k3xDgInSc4	tento
malý	malý	k2eAgInSc4d1	malý
stát	stát	k1gInSc4	stát
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
ohroženy	ohrozit	k5eAaPmNgInP	ohrozit
zvedáním	zvedání	k1gNnSc7	zvedání
hladiny	hladina	k1gFnSc2	hladina
světových	světový	k2eAgInPc2d1	světový
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
během	během	k7c2	během
příštích	příští	k2eAgNnPc2d1	příští
desetiletí	desetiletí	k1gNnPc2	desetiletí
mohou	moct	k5eAaImIp3nP	moct
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
nebo	nebo	k8xC	nebo
Niue	Niue	k1gFnSc4	Niue
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
ostrov	ostrov	k1gInSc1	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
(	(	kIx(	(
<g/>
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přidružený	přidružený	k2eAgMnSc1d1	přidružený
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
není	být	k5eNaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
zvedáním	zvedání	k1gNnSc7	zvedání
hladiny	hladina	k1gFnSc2	hladina
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
fidžijský	fidžijský	k2eAgInSc4d1	fidžijský
ostrov	ostrov	k1gInSc4	ostrov
Kioa	Kious	k1gMnSc2	Kious
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
již	již	k6eAd1	již
Tuvalané	Tuvalaný	k2eAgInPc1d1	Tuvalaný
pronajímají	pronajímat	k5eAaImIp3nP	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalané	Tuvalaný	k2eAgFnPc1d1	Tuvalaný
jsou	být	k5eAaImIp3nP	být
Polynésané	Polynésan	k1gMnPc1	Polynésan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ostrovy	ostrov	k1gInPc4	ostrov
osídlili	osídlit	k5eAaPmAgMnP	osídlit
před	před	k7c7	před
2000	[number]	k4	2000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Tuvalu	Tuvala	k1gFnSc4	Tuvala
spatřil	spatřit	k5eAaPmAgMnS	spatřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1568	[number]	k4	1568
Španěl	Španěl	k1gMnSc1	Španěl
Alvaro	Alvara	k1gFnSc5	Alvara
de	de	k?	de
Mendañ	Mendañ	k1gMnSc3	Mendañ
y	y	k?	y
Neyra	Neyra	k1gMnSc1	Neyra
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Nui	Nui	k1gFnSc2	Nui
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
se	se	k3xPyFc4	se
vylodit	vylodit	k5eAaPmF	vylodit
<g/>
.	.	kIx.	.
</s>
<s>
Žádní	žádný	k3yNgMnPc1	žádný
další	další	k2eAgMnPc1d1	další
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
neobjevili	objevit	k5eNaPmAgMnP	objevit
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
velrybáři	velrybář	k1gMnPc1	velrybář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ostrov	ostrov	k1gInSc4	ostrov
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
kvůli	kvůli	k7c3	kvůli
obtížím	obtíž	k1gFnPc3	obtíž
při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
<g/>
.	.	kIx.	.
</s>
<s>
Peruánští	peruánský	k2eAgMnPc1d1	peruánský
lovci	lovec	k1gMnPc1	lovec
otroků	otrok	k1gMnPc2	otrok
pročesávali	pročesávat	k5eAaImAgMnP	pročesávat
Pacifik	Pacifik	k1gInSc4	Pacifik
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1862	[number]	k4	1862
–	–	k?	–
1864	[number]	k4	1864
a	a	k8xC	a
Tuvalu	Tuval	k1gInSc2	Tuval
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpostiženějších	postižený	k2eAgNnPc2d3	nejpostiženější
souostroví	souostroví	k1gNnPc2	souostroví
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
zajatými	zajatý	k2eAgMnPc7d1	zajatý
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
Funafuti	Funafuť	k1gFnSc2	Funafuť
a	a	k8xC	a
Nukulaelae	Nukulaela	k1gFnSc2	Nukulaela
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nikdo	nikdo	k3yNnSc1	nikdo
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
protestanští	protestanský	k2eAgMnPc1d1	protestanský
kongregacionalisté	kongregacionalista	k1gMnPc1	kongregacionalista
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
misijní	misijní	k2eAgFnSc2d1	misijní
společnosti	společnost	k1gFnSc2	společnost
počali	počnout	k5eAaPmAgMnP	počnout
proces	proces	k1gInSc4	proces
evangelizace	evangelizace	k1gFnSc2	evangelizace
Tuvalu	Tuval	k1gInSc2	Tuval
<g/>
.	.	kIx.	.
</s>
<s>
Konverzi	konverze	k1gFnSc4	konverze
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
dokončili	dokončit	k5eAaPmAgMnP	dokončit
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
počali	počnout	k5eAaPmAgMnP	počnout
usazovat	usazovat	k5eAaImF	usazovat
evropští	evropský	k2eAgMnPc1d1	evropský
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
doufali	doufat	k5eAaImAgMnP	doufat
ve	v	k7c4	v
velké	velký	k2eAgInPc4d1	velký
výnosy	výnos	k1gInPc4	výnos
z	z	k7c2	z
místního	místní	k2eAgNnSc2d1	místní
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
také	také	k9	také
přinesli	přinést	k5eAaPmAgMnP	přinést
<g/>
,	,	kIx,	,
v	v	k7c4	v
Tichomoří	Tichomoří	k1gNnSc4	Tichomoří
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neznámé	známý	k2eNgFnSc2d1	neznámá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
Tuvalu	Tuval	k1gInSc6	Tuval
způsobily	způsobit	k5eAaPmAgFnP	způsobit
mnohá	mnohé	k1gNnPc4	mnohé
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
ostrovy	ostrov	k1gInPc1	ostrov
staly	stát	k5eAaPmAgInP	stát
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
Gilbertovy	Gilbertův	k2eAgInPc4d1	Gilbertův
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
Elliceovy	Elliceův	k2eAgInPc4d1	Elliceův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Tuvalu	Tuval	k1gMnSc3	Tuval
začalo	začít	k5eAaPmAgNnS	začít
nazývat	nazývat	k5eAaImF	nazývat
Elliceovy	Elliceův	k2eAgInPc4d1	Elliceův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Protektorát	protektorát	k1gInSc1	protektorát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kolonií	kolonie	k1gFnSc7	kolonie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Tuvalu	Tuvala	k1gFnSc4	Tuvala
vybráno	vybrat	k5eAaPmNgNnS	vybrat
jako	jako	k8xC	jako
operační	operační	k2eAgFnSc1d1	operační
základna	základna	k1gFnSc1	základna
spojeneckých	spojenecký	k2eAgNnPc2d1	spojenecké
vojsk	vojsko	k1gNnPc2	vojsko
bojujících	bojující	k2eAgNnPc2d1	bojující
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
s	s	k7c7	s
Japonci	Japonec	k1gMnPc7	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
vojáků	voják	k1gMnPc2	voják
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
až	až	k9	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Etnické	etnický	k2eAgInPc1d1	etnický
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Polynésané	Polynésan	k1gMnPc1	Polynésan
Elliceových	Elliceův	k2eAgInPc2d1	Elliceův
ostrovů	ostrov	k1gInPc2	ostrov
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
oddělení	oddělení	k1gNnSc4	oddělení
od	od	k7c2	od
Mikronésanů	Mikronésan	k1gMnPc2	Mikronésan
z	z	k7c2	z
Gilbertových	Gilbertův	k2eAgInPc2d1	Gilbertův
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Kiribati	Kiribati	k1gMnSc2	Kiribati
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
Elliceovy	Elliceův	k2eAgInPc1d1	Elliceův
ostrovy	ostrov	k1gInPc1	ostrov
staly	stát	k5eAaPmAgFnP	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kolonií	kolonie	k1gFnSc7	kolonie
Tuvalu	Tuval	k1gInSc2	Tuval
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
byla	být	k5eAaImAgFnS	být
stvrzena	stvrdit	k5eAaPmNgFnS	stvrdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalský	Tuvalský	k2eAgInSc1d1	Tuvalský
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgFnPc2d3	nejmenší
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
26	[number]	k4	26
km2	km2	k4	km2
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Nanumanga	Nanumanga	k1gFnSc1	Nanumanga
<g/>
,	,	kIx,	,
Niutao	Niutao	k1gNnSc1	Niutao
<g/>
,	,	kIx,	,
Niulakita	Niulakita	k1gFnSc1	Niulakita
<g/>
)	)	kIx)	)
a	a	k8xC	a
šesti	šest	k4xCc2	šest
atolů	atol	k1gInPc2	atol
(	(	kIx(	(
<g/>
Funafuti	Funafuť	k1gFnSc2	Funafuť
<g/>
,	,	kIx,	,
Nanumea	Nanumeum	k1gNnSc2	Nanumeum
<g/>
,	,	kIx,	,
Nui	Nui	k1gFnSc2	Nui
<g/>
,	,	kIx,	,
Nukufetau	Nukufetaus	k1gInSc2	Nukufetaus
<g/>
,	,	kIx,	,
Nukulaelae	Nukulaela	k1gInSc2	Nukulaela
a	a	k8xC	a
Vaitupu	Vaitup	k1gInSc2	Vaitup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
chudou	chudý	k2eAgFnSc4d1	chudá
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
a	a	k8xC	a
půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
stěží	stěží	k6eAd1	stěží
použitelná	použitelný	k2eAgFnSc1d1	použitelná
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
tuvalská	tuvalský	k2eAgFnSc1d1	tuvalský
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
že	že	k8xS	že
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
potřebovat	potřebovat	k5eAaImF	potřebovat
být	být	k5eAaImF	být
evakuovány	evakuovat	k5eAaBmNgInP	evakuovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zvedající	zvedající	k2eAgFnSc2d1	zvedající
se	se	k3xPyFc4	se
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Předpoklady	předpoklad	k1gInPc4	předpoklad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
během	během	k7c2	během
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
mohla	moct	k5eAaImAgFnS	moct
celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
zmizet	zmizet	k5eAaPmF	zmizet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
optimistických	optimistický	k2eAgInPc2d1	optimistický
závěrů	závěr	k1gInPc2	závěr
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
za	za	k7c4	za
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
..	..	k?	..
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
s	s	k7c7	s
přijmutím	přijmutí	k1gNnSc7	přijmutí
kvóty	kvóta	k1gFnSc2	kvóta
75	[number]	k4	75
evakuovaných	evakuovaný	k2eAgMnPc2d1	evakuovaný
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
Austrálie	Austrálie	k1gFnSc1	Austrálie
Tuvalskou	Tuvalský	k2eAgFnSc4d1	Tuvalský
petici	petice	k1gFnSc4	petice
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Tuvalanů	Tuvalan	k1gMnPc2	Tuvalan
(	(	kIx(	(
<g/>
12	[number]	k4	12
373	[number]	k4	373
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
osmi	osm	k4xCc2	osm
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
devět	devět	k4xCc1	devět
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
atoly	atol	k1gInPc1	atol
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc1d1	místní
správní	správní	k2eAgFnPc1d1	správní
oblasti	oblast	k1gFnPc1	oblast
stávající	stávající	k2eAgFnSc1d1	stávající
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgInSc2	jeden
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
po	po	k7c4	po
sčítání	sčítání	k1gNnSc4	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Funafuti	Funafuť	k1gFnSc2	Funafuť
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
4492	[number]	k4	4492
<g/>
)	)	kIx)	)
Nanumea	Nanume	k1gInSc2	Nanume
(	(	kIx(	(
<g/>
664	[number]	k4	664
<g/>
)	)	kIx)	)
Nui	Nui	k1gFnSc2	Nui
(	(	kIx(	(
<g/>
548	[number]	k4	548
<g/>
)	)	kIx)	)
Nukufetau	Nukufetaus	k1gInSc2	Nukufetaus
(	(	kIx(	(
<g/>
586	[number]	k4	586
<g/>
)	)	kIx)	)
Nukulaelae	Nukulaela	k1gInSc2	Nukulaela
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
393	[number]	k4	393
<g/>
)	)	kIx)	)
Vaitupu	Vaitup	k1gInSc2	Vaitup
(	(	kIx(	(
<g/>
1,591	[number]	k4	1,591
<g/>
)	)	kIx)	)
Místní	místní	k2eAgFnSc2d1	místní
správní	správní	k2eAgFnSc2d1	správní
oblasti	oblast	k1gFnSc2	oblast
stávající	stávající	k2eAgFnSc2d1	stávající
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
ostrova	ostrov	k1gInSc2	ostrov
Nanumanga	Nanumang	k1gMnSc2	Nanumang
(	(	kIx(	(
<g/>
589	[number]	k4	589
<g/>
)	)	kIx)	)
Niulakita	Niulakitum	k1gNnSc2	Niulakitum
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
Niutao	Niutao	k6eAd1	Niutao
(	(	kIx(	(
<g/>
663	[number]	k4	663
<g/>
)	)	kIx)	)
Nejmenší	malý	k2eAgInSc1d3	nejmenší
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Niulakita	Niulakita	k1gMnSc1	Niulakita
byl	být	k5eAaImAgMnS	být
neobydlený	obydlený	k2eNgMnSc1d1	neobydlený
až	až	k9	až
do	do	k7c2	do
přesídlení	přesídlení	k1gNnSc2	přesídlení
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
Niutao	Niutao	k1gMnSc1	Niutao
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
tedy	tedy	k9	tedy
jméno	jméno	k1gNnSc4	jméno
Tuvalu	Tuvala	k1gFnSc4	Tuvala
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgNnSc4d1	znamenající
"	"	kIx"	"
<g/>
osm	osm	k4xCc4	osm
stojících	stojící	k2eAgInPc2d1	stojící
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
původních	původní	k2eAgInPc2d1	původní
osm	osm	k4xCc4	osm
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Niulakita	Niulakita	k1gFnSc1	Niulakita
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
počítána	počítán	k2eAgFnSc1d1	počítána
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Niutao	Niutao	k6eAd1	Niutao
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
Nukulaelae	Nukulaelae	k1gNnSc1	Nukulaelae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
Tuvalu	Tuval	k1gInSc6	Tuval
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
Polynéské	polynéský	k2eAgNnSc4d1	polynéské
etnikum	etnikum	k1gNnSc4	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
97	[number]	k4	97
<g/>
%	%	kIx~	%
Tuvalanů	Tuvalan	k1gInPc2	Tuvalan
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
Tuvalské	Tuvalský	k2eAgFnSc2d1	Tuvalský
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
protestantské	protestantský	k2eAgFnSc2d1	protestantská
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
bylo	být	k5eAaImAgNnS	být
smíšeno	smíšen	k2eAgNnSc1d1	smíšeno
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
prvky	prvek	k1gInPc1	prvek
původního	původní	k2eAgNnSc2d1	původní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalštinou	Tuvalština	k1gFnSc7	Tuvalština
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
gilberština	gilberština	k1gFnSc1	gilberština
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
Kiribati	Kiribati	k1gFnSc2	Kiribati
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
některými	některý	k3yIgMnPc7	některý
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
Nui	Nui	k1gFnPc2	Nui
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
také	také	k9	také
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
silná	silný	k2eAgFnSc1d1	silná
devastace	devastace	k1gFnSc1	devastace
ostrovního	ostrovní	k2eAgNnSc2d1	ostrovní
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
rozloze	rozloha	k1gFnSc3	rozloha
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
nejmenším	malý	k2eAgInSc7d3	nejmenší
hrubým	hrubý	k2eAgInSc7d1	hrubý
domácím	domácí	k2eAgInSc7d1	domácí
produktem	produkt	k1gInSc7	produkt
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
sklizeň	sklizeň	k1gFnSc1	sklizeň
kokosových	kokosový	k2eAgInPc2d1	kokosový
ořechů	ořech	k1gInPc2	ořech
<g/>
,	,	kIx,	,
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
batátů	batáty	k1gInPc2	batáty
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
také	také	k9	také
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
tuvalského	tuvalský	k2eAgInSc2d1	tuvalský
rozpočtu	rozpočet	k1gInSc2	rozpočet
z	z	k7c2	z
20	[number]	k4	20
<g/>
%	%	kIx~	%
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
internetové	internetový	k2eAgFnSc2d1	internetová
domény	doména	k1gFnSc2	doména
.	.	kIx.	.
<g/>
tv	tv	k?	tv
Tradiční	tradiční	k2eAgInSc1d1	tradiční
společenský	společenský	k2eAgInSc1d1	společenský
systém	systém	k1gInSc1	systém
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
Tuvalu	Tuvala	k1gFnSc4	Tuvala
stále	stále	k6eAd1	stále
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
salanga	salanga	k1gFnSc1	salanga
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vykonávající	vykonávající	k2eAgFnSc1d1	vykonávající
pro	pro	k7c4	pro
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
rybaření	rybařený	k2eAgMnPc1d1	rybařený
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
domů	dům	k1gInPc2	dům
nebo	nebo	k8xC	nebo
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rodinné	rodinný	k2eAgFnPc1d1	rodinná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nP	dědit
z	z	k7c2	z
otce	otec	k1gMnSc2	otec
na	na	k7c4	na
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalská	Tuvalský	k2eAgFnSc1d1	Tuvalský
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
především	především	k9	především
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
tanci	tanec	k1gInPc7	tanec
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
fatele	fatel	k1gInPc1	fatel
<g/>
,	,	kIx,	,
fakanu	fakanu	k?	fakanu
a	a	k8xC	a
fakaseasea	fakasease	k1gInSc2	fakasease
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
k	k	k7c3	k
oslavování	oslavování	k1gNnSc3	oslavování
vládců	vládce	k1gMnPc2	vládce
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
prominentních	prominentní	k2eAgFnPc2d1	prominentní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
hudba	hudba	k1gFnSc1	hudba
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
básně	báseň	k1gFnPc1	báseň
přednášené	přednášený	k2eAgFnPc1d1	přednášená
v	v	k7c6	v
sérii	série	k1gFnSc6	série
monotónních	monotónní	k2eAgFnPc2d1	monotónní
recitací	recitace	k1gFnPc2	recitace
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
již	již	k6eAd1	již
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
pracovní	pracovní	k2eAgFnPc4d1	pracovní
písně	píseň	k1gFnPc4	píseň
zpívané	zpívaný	k2eAgFnPc1d1	zpívaná
ženami	žena	k1gFnPc7	žena
k	k	k7c3	k
podpoření	podpoření	k1gNnSc3	podpoření
pracujících	pracující	k2eAgMnPc2d1	pracující
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Společenského	společenský	k2eAgNnSc2d1	společenské
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Realm	Realm	k1gMnSc1	Realm
<g/>
)	)	kIx)	)
a	a	k8xC	a
uznává	uznávat	k5eAaImIp3nS	uznávat
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
královnu	královna	k1gFnSc4	královna
Tuvalu	Tuval	k1gInSc2	Tuval
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Fale	falos	k1gInSc5	falos
I	i	k9	i
Fono	Fono	k1gNnSc4	Fono
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
každé	každý	k3xTgFnSc3	každý
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
členi	člen	k1gMnPc1	člen
volí	volit	k5eAaImIp3nP	volit
premiéra	premiér	k1gMnSc4	premiér
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
starší	starší	k1gMnPc1	starší
mají	mít	k5eAaImIp3nP	mít
neformální	formální	k2eNgFnSc4d1	neformální
autoritu	autorita	k1gFnSc4	autorita
na	na	k7c6	na
lokální	lokální	k2eAgFnSc6d1	lokální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tuvalu	Tuval	k1gInSc2	Tuval
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tuvalu	Tuval	k1gInSc2	Tuval
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Tuvalu	Tuval	k1gInSc2	Tuval
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
East	East	k1gMnSc1	East
Asia	Asia	k1gMnSc1	Asia
and	and	k?	and
Pacific	Pacific	k1gMnSc1	Pacific
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Tuvalu	Tuval	k1gInSc2	Tuval
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Tuvalu	Tuval	k1gInSc2	Tuval
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c4	v
Kuala	Kualo	k1gNnPc4	Kualo
Lumpur	Lumpura	k1gFnPc2	Lumpura
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Tuvalu	Tuval	k1gInSc2	Tuval
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
MACDONALD	Macdonald	k1gMnSc1	Macdonald
<g/>
,	,	kIx,	,
Barrie	Barrie	k1gFnSc1	Barrie
K.	K.	kA	K.
Tuvalu	Tuval	k1gInSc2	Tuval
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
