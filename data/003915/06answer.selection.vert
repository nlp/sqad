<s>
Domorodý	domorodý	k2eAgInSc1d1	domorodý
název	název	k1gInSc1	název
Tuvalu	Tuval	k1gInSc2	Tuval
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
osídlení	osídlení	k1gNnSc4	osídlení
většiny	většina	k1gFnSc2	většina
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
tuvalštině	tuvalština	k1gFnSc6	tuvalština
shluk	shluk	k1gInSc1	shluk
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
osmi	osm	k4xCc2	osm
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
osídlených	osídlený	k2eAgInPc2d1	osídlený
<g/>
,	,	kIx,	,
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
