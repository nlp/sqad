<s>
Prýt	prýt	k1gInSc1	prýt
(	(	kIx(	(
<g/>
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zdřevnatělé	zdřevnatělý	k2eAgFnSc2d1	zdřevnatělá
nevětvené	větvený	k2eNgFnSc2d1	nevětvená
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
-	-	kIx~	-
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
větve	větev	k1gFnPc4	větev
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
keře	keř	k1gInSc2	keř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
větvení	větvení	k1gNnSc3	větvení
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k6eAd1	již
u	u	k7c2	u
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
