<s>
Strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gInSc1	arbor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
růstová	růstový	k2eAgFnSc1d1	růstová
forma	forma	k1gFnSc1	forma
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Prýt	prýt	k1gInSc1	prýt
(	(	kIx(	(
<g/>
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zdřevnatělé	zdřevnatělý	k2eAgFnSc2d1	zdřevnatělá
nevětvené	větvený	k2eNgFnSc2d1	nevětvená
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
-	-	kIx~	-
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
větve	větev	k1gFnPc4	větev
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
keře	keř	k1gInSc2	keř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
větvení	větvení	k1gNnSc3	větvení
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k6eAd1	již
u	u	k7c2	u
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
větvení	větvení	k1gNnSc3	větvení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Přesnou	přesný	k2eAgFnSc4d1	přesná
definici	definice	k1gFnSc4	definice
pojmu	pojem	k1gInSc2	pojem
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
keř	keř	k1gInSc1	keř
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vymezit	vymezit	k5eAaPmF	vymezit
kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
diverzitě	diverzita	k1gFnSc3	diverzita
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
používat	používat	k5eAaImF	používat
termín	termín	k1gInSc4	termín
stromovitá	stromovitý	k2eAgFnSc1d1	stromovitá
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Různý	různý	k2eAgInSc1d1	různý
způsob	způsob	k1gInSc1	způsob
větvení	větvení	k1gNnPc2	větvení
dává	dávat	k5eAaImIp3nS	dávat
každému	každý	k3xTgInSc3	každý
druhu	druh	k1gInSc3	druh
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
strom	strom	k1gInSc1	strom
roste	růst	k5eAaImIp3nS	růst
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
roste	růst	k5eAaImIp3nS	růst
osamoceně	osamoceně	k6eAd1	osamoceně
nebo	nebo	k8xC	nebo
uvnitř	uvnitř	k7c2	uvnitř
porostu	porost	k1gInSc2	porost
-	-	kIx~	-
lesa	les	k1gInSc2	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
botanika	botanika	k1gFnSc1	botanika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
od	od	k7c2	od
tenké	tenký	k2eAgFnSc2d1	tenká
několikamilimetrové	několikamilimetrový	k2eAgFnSc2d1	několikamilimetrová
<g/>
,	,	kIx,	,
až	až	k8xS	až
po	po	k7c4	po
rozbrázděnou	rozbrázděný	k2eAgFnSc4d1	rozbrázděná
mnohavrstevnou	mnohavrstevný	k2eAgFnSc4d1	mnohavrstevná
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
rozbrázděná	rozbrázděný	k2eAgFnSc1d1	rozbrázděná
kůra	kůra	k1gFnSc1	kůra
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
borka	borka	k1gFnSc1	borka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kůrou	kůra	k1gFnSc7	kůra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lýko	lýko	k1gNnSc1	lýko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jedinou	jediný	k2eAgFnSc7d1	jediná
živou	živý	k2eAgFnSc7d1	živá
částí	část	k1gFnSc7	část
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Lýkem	lýko	k1gNnSc7	lýko
prochází	procházet	k5eAaImIp3nS	procházet
sítkovice	sítkovice	k1gFnSc2	sítkovice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozvádí	rozvádět	k5eAaImIp3nP	rozvádět
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
rostlině	rostlina	k1gFnSc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
hmota	hmota	k1gFnSc1	hmota
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
ligninu	lignin	k1gInSc2	lignin
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lýkem	lýko	k1gNnSc7	lýko
a	a	k8xC	a
dřevní	dřevní	k2eAgFnSc7d1	dřevní
částí	část	k1gFnSc7	část
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
kambium	kambium	k1gNnSc1	kambium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
růstu	růst	k1gInSc6	růst
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
roste	růst	k5eAaImIp3nS	růst
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
běžným	běžný	k2eAgInSc7d1	běžný
prodlužovacím	prodlužovací	k2eAgInSc7d1	prodlužovací
růstem	růst	k1gInSc7	růst
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
růst	růst	k1gInSc1	růst
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sekundárnímu	sekundární	k2eAgNnSc3d1	sekundární
tloustnutí	tloustnutí	k1gNnSc3	tloustnutí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sekundárním	sekundární	k2eAgNnSc6d1	sekundární
tloustnutí	tloustnutí	k1gNnSc6	tloustnutí
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
pletiv	pletivo	k1gNnPc2	pletivo
<g/>
,	,	kIx,	,
sekundárních	sekundární	k2eAgInPc2d1	sekundární
meristémů	meristém	k1gInPc2	meristém
–	–	k?	–
kambium	kambium	k1gNnSc4	kambium
a	a	k8xC	a
felogén	felogén	k1gInSc4	felogén
<g/>
.	.	kIx.	.
</s>
<s>
Kambium	kambium	k1gNnSc1	kambium
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
jako	jako	k9	jako
prstenec	prstenec	k1gInSc1	prstenec
meristématických	meristématický	k2eAgFnPc2d1	meristématický
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jak	jak	k6eAd1	jak
v	v	k7c6	v
cévních	cévní	k2eAgInPc6d1	cévní
svazcích	svazek	k1gInPc6	svazek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fascikulární	fascikulární	k2eAgNnSc1d1	fascikulární
kambium	kambium	k1gNnSc1	kambium
-	-	kIx~	-
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
vnější	vnější	k2eAgFnSc4d1	vnější
lýkovou	lýkový	k2eAgFnSc4d1	lýková
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
dřevní	dřevní	k2eAgFnSc4d1	dřevní
část	část	k1gFnSc4	část
cévního	cévní	k2eAgInSc2d1	cévní
svazku	svazek	k1gInSc2	svazek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gMnPc4	on
(	(	kIx(	(
<g/>
interfascikulární	interfascikulární	k2eAgNnSc1d1	interfascikulární
kambium	kambium	k1gNnSc1	kambium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tloustnutí	tloustnutí	k1gNnSc1	tloustnutí
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
fascikulární	fascikulární	k2eAgNnSc1d1	fascikulární
kambium	kambium	k1gNnSc1	kambium
produkuje	produkovat	k5eAaImIp3nS	produkovat
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
lýko	lýko	k1gNnSc1	lýko
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgInSc1d1	sekundární
floém	floém	k1gInSc1	floém
<g/>
)	)	kIx)	)
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
kmene	kmen	k1gInSc2	kmen
dřevo	dřevo	k1gNnSc1	dřevo
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgInSc1d1	sekundární
xylém	xylém	k1gInSc1	xylém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
především	především	k9	především
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
v	v	k7c6	v
dřevní	dřevní	k2eAgFnSc6d1	dřevní
části	část	k1gFnSc6	část
vznikají	vznikat	k5eAaImIp3nP	vznikat
řidší	řídký	k2eAgFnPc4d2	řidší
cévy	céva	k1gFnPc4	céva
o	o	k7c6	o
větším	veliký	k2eAgInSc6d2	veliký
průměru	průměr	k1gInSc6	průměr
–	–	k?	–
světlejší	světlý	k2eAgNnSc4d2	světlejší
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
pomaleji	pomale	k6eAd2	pomale
pak	pak	k6eAd1	pak
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
(	(	kIx(	(
<g/>
hustší	hustý	k2eAgFnSc2d2	hustší
cévy	céva	k1gFnSc2	céva
o	o	k7c6	o
menším	malý	k2eAgInSc6d2	menší
průměru	průměr	k1gInSc6	průměr
–	–	k?	–
tmavší	tmavý	k2eAgNnSc4d2	tmavší
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
ustává	ustávat	k5eAaImIp3nS	ustávat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
letokruhy	letokruh	k1gInPc7	letokruh
<g/>
.	.	kIx.	.
</s>
<s>
Felogén	Felogén	k1gInSc1	Felogén
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
lýkové	lýkový	k2eAgFnSc2d1	lýková
části	část	k1gFnSc2	část
a	a	k8xC	a
také	také	k9	také
produkuje	produkovat	k5eAaImIp3nS	produkovat
buňky	buňka	k1gFnPc4	buňka
ven	ven	k6eAd1	ven
i	i	k8xC	i
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
buňky	buňka	k1gFnPc4	buňka
korku	korek	k1gInSc2	korek
(	(	kIx(	(
<g/>
felému	felém	k1gInSc2	felém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
pak	pak	k6eAd1	pak
buňky	buňka	k1gFnPc4	buňka
felodermu	feloderm	k1gInSc2	feloderm
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chloroplasty	chloroplast	k1gInPc4	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
nad	nad	k7c7	nad
felogénem	felogén	k1gInSc7	felogén
postupně	postupně	k6eAd1	postupně
odumírají	odumírat	k5eAaImIp3nP	odumírat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vznikající	vznikající	k2eAgInSc1d1	vznikající
korek	korek	k1gInSc1	korek
jim	on	k3xPp3gMnPc3	on
zamezí	zamezit	k5eAaPmIp3nS	zamezit
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
živin	živina	k1gFnPc2	živina
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
ze	z	k7c2	z
sítkovic	sítkovice	k1gFnPc2	sítkovice
a	a	k8xC	a
cév	céva	k1gFnPc2	céva
nedostanou	dostat	k5eNaPmIp3nP	dostat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
stromy	strom	k1gInPc7	strom
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
takřka	takřka	k6eAd1	takřka
výhradně	výhradně	k6eAd1	výhradně
pouze	pouze	k6eAd1	pouze
rostliny	rostlina	k1gFnPc1	rostlina
krytosemenné	krytosemenný	k2eAgFnSc2d1	krytosemenná
dvouděložné	dvouděložná	k1gFnSc2	dvouděložná
(	(	kIx(	(
<g/>
Magnoliopsida	Magnoliopsida	k1gFnSc1	Magnoliopsida
<g/>
)	)	kIx)	)
a	a	k8xC	a
nahosemenné	nahosemenný	k2eAgFnPc1d1	nahosemenná
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
jde	jít	k5eAaImIp3nS	jít
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c4	o
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
)	)	kIx)	)
–	–	k?	–
jinany	jinan	k1gInPc4	jinan
(	(	kIx(	(
<g/>
Gingkoopsida	Gingkoopsida	k1gFnSc1	Gingkoopsida
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
jehličnany	jehličnan	k1gInPc1	jehličnan
(	(	kIx(	(
<g/>
Pinopsida	Pinopsida	k1gFnSc1	Pinopsida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stromové	stromový	k2eAgFnPc4d1	stromová
formy	forma	k1gFnPc4	forma
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
i	i	k9	i
třída	třída	k1gFnSc1	třída
cykasů	cykas	k1gInPc2	cykas
(	(	kIx(	(
<g/>
Cycadopsida	Cycadopsida	k1gFnSc1	Cycadopsida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dominovala	dominovat	k5eAaImAgFnS	dominovat
ve	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc4	její
druhové	druhový	k2eAgNnSc4d1	druhové
spektrum	spektrum	k1gNnSc4	spektrum
výrazně	výrazně	k6eAd1	výrazně
omezeno	omezit	k5eAaPmNgNnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoděložné	jednoděložný	k2eAgFnPc1d1	jednoděložná
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Liliopsida	Liliopsida	k1gFnSc1	Liliopsida
<g/>
)	)	kIx)	)
druhotného	druhotný	k2eAgNnSc2d1	druhotné
tloustnutí	tloustnutí	k1gNnSc2	tloustnutí
schopné	schopný	k2eAgNnSc4d1	schopné
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
dřevnatění	dřevnatěný	k2eAgMnPc1d1	dřevnatěný
u	u	k7c2	u
palem	palma	k1gFnPc2	palma
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
primárního	primární	k2eAgNnSc2d1	primární
tloustnutí	tloustnutí	k1gNnSc2	tloustnutí
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
více	hodně	k6eAd2	hodně
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
mezi	mezi	k7c7	mezi
tropickými	tropický	k2eAgInPc7d1	tropický
druhy	druh	k1gInPc7	druh
a	a	k8xC	a
mezi	mezi	k7c7	mezi
vývojově	vývojově	k6eAd1	vývojově
primitivnějšími	primitivní	k2eAgFnPc7d2	primitivnější
čeleděmi	čeleď	k1gFnPc7	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Stromové	stromový	k2eAgFnPc1d1	stromová
formy	forma	k1gFnPc1	forma
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k8xC	i
četní	četný	k2eAgMnPc1d1	četný
zástupci	zástupce	k1gMnPc1	zástupce
výtrusných	výtrusný	k2eAgFnPc2d1	výtrusná
rostlin	rostlina	k1gFnPc2	rostlina
–	–	k?	–
plavuně	plavuň	k1gFnSc2	plavuň
<g/>
,	,	kIx,	,
přesličky	přeslička	k1gFnSc2	přeslička
<g/>
,	,	kIx,	,
kapradiny	kapradina	k1gFnSc2	kapradina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
zuhelnatělých	zuhelnatělý	k2eAgInPc2d1	zuhelnatělý
kmenů	kmen	k1gInPc2	kmen
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
přežilo	přežít	k5eAaPmAgNnS	přežít
jen	jen	k9	jen
několik	několik	k4yIc4	několik
rodů	rod	k1gInPc2	rod
stromových	stromový	k2eAgFnPc2d1	stromová
kapradin	kapradina	k1gFnPc2	kapradina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rody	rod	k1gInPc7	rod
Cyathea	Cyatheum	k1gNnSc2	Cyatheum
nebo	nebo	k8xC	nebo
Dicksonia	Dicksonium	k1gNnSc2	Dicksonium
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
velké	velký	k2eAgInPc4d1	velký
rozměry	rozměr	k1gInPc4	rozměr
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
poměrně	poměrně	k6eAd1	poměrně
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
průměrně	průměrně	k6eAd1	průměrně
vzrostlý	vzrostlý	k2eAgInSc1d1	vzrostlý
stoletý	stoletý	k2eAgInSc1d1	stoletý
soliterní	soliterní	k2eAgInSc1d1	soliterní
strom	strom	k1gInSc1	strom
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
letní	letní	k2eAgInSc4d1	letní
den	den	k1gInSc4	den
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
asi	asi	k9	asi
1000	[number]	k4	1000
litrů	litr	k1gInPc2	litr
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
za	za	k7c4	za
stejné	stejný	k2eAgNnSc4d1	stejné
období	období	k1gNnSc4	období
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
asi	asi	k9	asi
300	[number]	k4	300
litrů	litr	k1gInPc2	litr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
potřebě	potřeba	k1gFnSc3	potřeba
chlazení	chlazení	k1gNnSc6	chlazení
tentýž	týž	k3xTgInSc1	týž
strom	strom	k1gInSc1	strom
odpaří	odpařit	k5eAaPmIp3nS	odpařit
za	za	k7c4	za
den	den	k1gInSc4	den
přes	přes	k7c4	přes
100	[number]	k4	100
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
energii	energie	k1gFnSc4	energie
asi	asi	k9	asi
70	[number]	k4	70
kWh	kwh	kA	kwh
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
ve	v	k7c4	v
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
místo	místo	k6eAd1	místo
v	v	k7c4	v
teplo	teplo	k1gNnSc4	teplo
–	–	k?	–
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
funguje	fungovat	k5eAaImIp3nS	fungovat
stejně	stejně	k6eAd1	stejně
účinně	účinně	k6eAd1	účinně
jako	jako	k8xS	jako
klimatizační	klimatizační	k2eAgFnSc1d1	klimatizační
jednotka	jednotka	k1gFnSc1	jednotka
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
obnovitelný	obnovitelný	k2eAgInSc4d1	obnovitelný
konstrukční	konstrukční	k2eAgInSc4d1	konstrukční
materiál	materiál	k1gInSc4	materiál
využívaný	využívaný	k2eAgInSc4d1	využívaný
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc6	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
papírenství	papírenství	k1gNnSc2	papírenství
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
o	o	k7c6	o
dřevu	dřevo	k1gNnSc6	dřevo
jako	jako	k8xC	jako
o	o	k7c6	o
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
surovině	surovina	k1gFnSc6	surovina
značujeme	značovat	k5eAaPmIp1nP	značovat
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
dříví	dříví	k1gNnSc4	dříví
<g/>
.	.	kIx.	.
</s>
<s>
Odvětvené	odvětvený	k2eAgInPc1d1	odvětvený
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
délkách	délka	k1gFnPc6	délka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
surové	surový	k2eAgInPc1d1	surový
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Příčným	příčný	k2eAgNnSc7d1	příčné
rozřezáním	rozřezání	k1gNnSc7	rozřezání
surových	surový	k2eAgInPc2d1	surový
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
tříděním	třídění	k1gNnSc7	třídění
podle	podle	k7c2	podle
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
vznikají	vznikat	k5eAaImIp3nP	vznikat
sortimenty	sortiment	k1gInPc1	sortiment
dříví	dříví	k1gNnSc2	dříví
<g/>
.	.	kIx.	.
</s>
<s>
Nejkvalitnější	kvalitní	k2eAgInPc1d3	nejkvalitnější
sortimenty	sortiment	k1gInPc1	sortiment
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
sportovního	sportovní	k2eAgNnSc2d1	sportovní
náčiní	náčiní	k1gNnSc2	náčiní
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
speciální	speciální	k2eAgFnPc4d1	speciální
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Sortimenty	sortiment	k1gInPc1	sortiment
průměrné	průměrný	k2eAgFnSc2d1	průměrná
kvality	kvalita	k1gFnSc2	kvalita
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
zpracování	zpracování	k1gNnSc6	zpracování
(	(	kIx(	(
<g/>
rozřezáním	rozřezání	k1gNnSc7	rozřezání
na	na	k7c6	na
pilách	pila	k1gFnPc6	pila
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
řezivo	řezivo	k1gNnSc4	řezivo
<g/>
)	)	kIx)	)
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgInPc1d2	nižší
nároky	nárok	k1gInPc1	nárok
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
na	na	k7c4	na
sortimenty	sortiment	k1gInPc4	sortiment
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
papírenský	papírenský	k2eAgInSc4d1	papírenský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
nejméně	málo	k6eAd3	málo
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
dříví	dříví	k1gNnSc1	dříví
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
(	(	kIx(	(
<g/>
kusové	kusový	k2eAgNnSc1d1	kusové
dříví	dříví	k1gNnSc1	dříví
<g/>
,	,	kIx,	,
štěpka	štěpka	k1gFnSc1	štěpka
<g/>
,	,	kIx,	,
pelety	peleta	k1gFnPc1	peleta
<g/>
,	,	kIx,	,
brikety	briketa	k1gFnPc1	briketa
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
obnovitelný	obnovitelný	k2eAgInSc4d1	obnovitelný
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
jsou	být	k5eAaImIp3nP	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
růstovou	růstový	k2eAgFnSc7d1	růstová
formou	forma	k1gFnSc7	forma
většiny	většina	k1gFnSc2	většina
přírodních	přírodní	k2eAgInPc2d1	přírodní
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
by	by	kYmCp3nS	by
přirozená	přirozený	k2eAgFnSc1d1	přirozená
vegetace	vegetace	k1gFnSc1	vegetace
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
člověka	člověk	k1gMnSc2	člověk
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
99	[number]	k4	99
%	%	kIx~	%
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
stromů	strom	k1gInPc2	strom
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c6	na
3,04	[number]	k4	3,04
bilionu	bilion	k4xCgInSc2	bilion
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
200	[number]	k4	200
stromů	strom	k1gInPc2	strom
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
hektar	hektar	k1gInSc4	hektar
souše	souš	k1gFnSc2	souš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
stromů	strom	k1gInPc2	strom
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
bylo	být	k5eAaImAgNnS	být
prudce	prudko	k6eAd1	prudko
sníženo	snížit	k5eAaPmNgNnS	snížit
člověkem	člověk	k1gMnSc7	člověk
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
zemědělství	zemědělství	k1gNnSc3	zemědělství
a	a	k8xC	a
využití	využití	k1gNnSc3	využití
dřeva	dřevo	k1gNnSc2	dřevo
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
na	na	k7c4	na
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Ekologická	ekologický	k2eAgFnSc1d1	ekologická
výhoda	výhoda	k1gFnSc1	výhoda
získávání	získávání	k1gNnSc2	získávání
obživy	obživa	k1gFnSc2	obživa
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
spočívá	spočívat	k5eAaImIp3nS	spočívat
mj.	mj.	kA	mj.
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
půda	půda	k1gFnSc1	půda
není	být	k5eNaImIp3nS	být
narušována	narušovat	k5eAaImNgFnS	narušovat
každoroční	každoroční	k2eAgFnSc7d1	každoroční
opakovanou	opakovaný	k2eAgFnSc7d1	opakovaná
orbou	orba	k1gFnSc7	orba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
obejít	obejít	k5eAaPmF	obejít
i	i	k9	i
bez	bez	k7c2	bez
chemizace	chemizace	k1gFnSc2	chemizace
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stromů	strom	k1gInPc2	strom
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
původní	původní	k2eAgFnSc1d1	původní
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
mají	mít	k5eAaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
především	především	k9	především
na	na	k7c4	na
<g/>
:	:	kIx,	:
propustnost	propustnost	k1gFnSc1	propustnost
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
v	v	k7c6	v
porostech	porost	k1gInPc6	porost
se	s	k7c7	s
stromovým	stromový	k2eAgNnSc7d1	stromové
patrem	patro	k1gNnSc7	patro
je	být	k5eAaImIp3nS	být
podíl	podíl	k1gInSc1	podíl
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
záření	záření	k1gNnSc2	záření
oproti	oproti	k7c3	oproti
nezastíněným	zastíněný	k2eNgInPc3d1	nezastíněný
porostům	porost	k1gInPc3	porost
snížena	snížit	k5eAaPmNgFnS	snížit
cca	cca	kA	cca
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
(	(	kIx(	(
<g/>
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
deštném	deštný	k2eAgInSc6d1	deštný
pralese	prales	k1gInSc6	prales
až	až	k9	až
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
bilanci	bilance	k1gFnSc6	bilance
uhlíku	uhlík	k1gInSc2	uhlík
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
akumuluje	akumulovat	k5eAaBmIp3nS	akumulovat
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
)	)	kIx)	)
koloběh	koloběh	k1gInSc1	koloběh
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
výpar	výpar	k1gInSc1	výpar
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
výrazně	výrazně	k6eAd1	výrazně
posiluje	posilovat	k5eAaImIp3nS	posilovat
podíl	podíl	k1gInSc4	podíl
vody	voda	k1gFnSc2	voda
procházející	procházející	k2eAgInSc4d1	procházející
malým	malý	k2eAgInSc7d1	malý
koloběhem	koloběh	k1gInSc7	koloběh
vody	voda	k1gFnSc2	voda
oproti	oproti	k7c3	oproti
velkému	velký	k2eAgInSc3d1	velký
koloběhu	koloběh	k1gInSc3	koloběh
<g/>
)	)	kIx)	)
fyzikálně-chemické	fyzikálněhemický	k2eAgFnSc3d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnSc3	vlastnost
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
<g/>
:	:	kIx,	:
vylučování	vylučování	k1gNnSc4	vylučování
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
látek	látka	k1gFnPc2	látka
–	–	k?	–
alelopatie	alelopatie	k1gFnSc2	alelopatie
spad	spad	k1gInSc4	spad
listů	list	k1gInPc2	list
či	či	k8xC	či
jehlic	jehlice	k1gFnPc2	jehlice
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
pH	ph	kA	ph
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc6	rychlost
dekompozice	dekompozice	k1gFnSc2	dekompozice
růst	růst	k1gInSc4	růst
kořenů	kořen	k1gInPc2	kořen
–	–	k?	–
čerpání	čerpání	k1gNnSc2	čerpání
živin	živina	k1gFnPc2	živina
či	či	k8xC	či
odčerpání	odčerpání	k1gNnSc2	odčerpání
části	část	k1gFnSc2	část
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
biologické	biologický	k2eAgFnPc4d1	biologická
meliorace	meliorace	k1gFnPc4	meliorace
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
využívané	využívaný	k2eAgFnPc1d1	využívaná
v	v	k7c4	v
lesnictví	lesnictví	k1gNnSc4	lesnictví
<g/>
)	)	kIx)	)
biologické	biologický	k2eAgFnPc1d1	biologická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
<g/>
:	:	kIx,	:
zachování	zachování	k1gNnSc4	zachování
bohatství	bohatství	k1gNnSc2	bohatství
půdní	půdní	k2eAgFnSc2d1	půdní
flóry	flóra	k1gFnSc2	flóra
zachování	zachování	k1gNnSc4	zachování
bohatství	bohatství	k1gNnSc2	bohatství
půdní	půdní	k2eAgFnSc2d1	půdní
fauny	fauna	k1gFnSc2	fauna
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
s	s	k7c7	s
půdou	půda	k1gFnSc7	půda
využívanou	využívaný	k2eAgFnSc7d1	využívaná
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
obilnin	obilnina	k1gFnPc2	obilnina
a	a	k8xC	a
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
)	)	kIx)	)
celkový	celkový	k2eAgInSc1d1	celkový
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
faunu	fauna	k1gFnSc4	fauna
a	a	k8xC	a
flóru	flóra	k1gFnSc4	flóra
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Relativní	relativní	k2eAgFnSc1d1	relativní
ekologická	ekologický	k2eAgFnSc1d1	ekologická
výhoda	výhoda	k1gFnSc1	výhoda
získávání	získávání	k1gNnSc2	získávání
obživy	obživa	k1gFnSc2	obživa
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinkách	skupinka	k1gFnPc6	skupinka
mezi	mezi	k7c7	mezi
původní	původní	k2eAgFnSc7d1	původní
vegetací	vegetace	k1gFnSc7	vegetace
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
spočívá	spočívat	k5eAaImIp3nS	spočívat
zejména	zejména	k9	zejména
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
půda	půda	k1gFnSc1	půda
není	být	k5eNaImIp3nS	být
narušována	narušovat	k5eAaImNgFnS	narušovat
každoroční	každoroční	k2eAgFnSc7d1	každoroční
opakovanou	opakovaný	k2eAgFnSc7d1	opakovaná
orbou	orba	k1gFnSc7	orba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
obejít	obejít	k5eAaPmF	obejít
i	i	k9	i
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
chemizace	chemizace	k1gFnSc2	chemizace
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stromů	strom	k1gInPc2	strom
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
původní	původní	k2eAgFnSc1d1	původní
nebo	nebo	k8xC	nebo
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativně	relativně	k6eAd1	relativně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
stromy	strom	k1gInPc4	strom
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
svého	svůj	k3xOyFgNnSc2	svůj
obydlí	obydlí	k1gNnSc2	obydlí
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
a	a	k8xC	a
sklizeň	sklizeň	k1gFnSc1	sklizeň
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
zpracování	zpracování	k1gNnSc4	zpracování
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
malopěstitelé	malopěstitel	k1gMnPc1	malopěstitel
<g/>
,	,	kIx,	,
odpadá	odpadat	k5eAaImIp3nS	odpadat
také	také	k9	také
zátěž	zátěž	k1gFnSc1	zátěž
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
z	z	k7c2	z
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
tyto	tento	k3xDgInPc4	tento
způsoby	způsob	k1gInPc4	způsob
obživy	obživa	k1gFnSc2	obživa
preferovat	preferovat	k5eAaImF	preferovat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
úzkého	úzký	k2eAgNnSc2d1	úzké
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
hlediska	hledisko	k1gNnSc2	hledisko
málo	málo	k6eAd1	málo
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pěstování	pěstování	k1gNnSc1	pěstování
stromů	strom	k1gInPc2	strom
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
monokulturních	monokulturní	k2eAgFnPc6d1	monokulturní
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
likvidován	likvidovat	k5eAaBmNgInS	likvidovat
původní	původní	k2eAgInSc1d1	původní
cenný	cenný	k2eAgInSc1d1	cenný
biotop	biotop	k1gInSc1	biotop
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tropický	tropický	k2eAgInSc1d1	tropický
prales	prales	k1gInSc1	prales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nP	působit
značné	značný	k2eAgFnPc4d1	značná
ekologické	ekologický	k2eAgFnPc4d1	ekologická
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
Palma	palma	k1gFnSc1	palma
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Stromy	strom	k1gInPc1	strom
mají	mít	k5eAaImIp3nP	mít
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
dlouhověkosti	dlouhověkost	k1gFnSc3	dlouhověkost
a	a	k8xC	a
velikosti	velikost	k1gFnSc3	velikost
odedávna	odedávna	k6eAd1	odedávna
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
duchovní	duchovní	k2eAgInPc4d1	duchovní
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
i	i	k8xC	i
jako	jako	k9	jako
orientační	orientační	k2eAgInPc4d1	orientační
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
primitivních	primitivní	k2eAgNnPc2d1	primitivní
náboženství	náboženství	k1gNnPc2	náboženství
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
míře	míra	k1gFnSc6	míra
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Odedávna	odedávna	k6eAd1	odedávna
se	se	k3xPyFc4	se
stromy	strom	k1gInPc1	strom
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
podél	podél	k7c2	podél
významných	významný	k2eAgFnPc2d1	významná
stezek	stezka	k1gFnPc2	stezka
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozcestích	rozcestí	k1gNnPc6	rozcestí
<g/>
,	,	kIx,	,
u	u	k7c2	u
drobných	drobný	k2eAgFnPc2d1	drobná
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
kapličky	kaplička	k1gFnPc1	kaplička
<g/>
,	,	kIx,	,
křížky	křížek	k1gInPc1	křížek
<g/>
,	,	kIx,	,
kostelíky	kostelík	k1gInPc1	kostelík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
byly	být	k5eAaImAgInP	být
zakládány	zakládán	k2eAgInPc1d1	zakládán
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
vysazovány	vysazovat	k5eAaImNgInP	vysazovat
různé	různý	k2eAgInPc1d1	různý
původní	původní	k2eAgInPc1d1	původní
i	i	k8xC	i
nepůvodní	původní	k2eNgInPc1d1	nepůvodní
druhy	druh	k1gInPc1	druh
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
speciálně	speciálně	k6eAd1	speciálně
vyšlechtěných	vyšlechtěný	k2eAgInPc2d1	vyšlechtěný
kultivarů	kultivar	k1gInPc2	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
duby	dub	k1gInPc1	dub
<g/>
)	)	kIx)	)
na	na	k7c6	na
hrázích	hráz	k1gFnPc6	hráz
rybníků	rybník	k1gInPc2	rybník
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
zpevňovat	zpevňovat	k5eAaImF	zpevňovat
svými	svůj	k3xOyFgFnPc7	svůj
kořeny	kořen	k1gInPc7	kořen
těleso	těleso	k1gNnSc1	těleso
hráze	hráze	k1gFnSc1	hráze
<g/>
.	.	kIx.	.
</s>
<s>
Liniové	liniový	k2eAgFnPc1d1	liniová
výsadby	výsadba	k1gFnPc1	výsadba
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
pohledovému	pohledový	k2eAgNnSc3d1	pohledové
oddělení	oddělení	k1gNnSc3	oddělení
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
kulturní	kulturní	k2eAgFnSc2d1	kulturní
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
hluku	hluk	k1gInSc2	hluk
a	a	k8xC	a
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovné	rovný	k2eAgFnSc6d1	rovná
krajině	krajina	k1gFnSc6	krajina
pak	pak	k6eAd1	pak
plní	plnit	k5eAaImIp3nP	plnit
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
větrolamy	větrolam	k1gInPc4	větrolam
<g/>
.	.	kIx.	.
</s>
<s>
Kácení	kácení	k1gNnSc1	kácení
dřevin	dřevina	k1gFnPc2	dřevina
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
ČNR	ČNR	kA	ČNR
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
platném	platný	k2eAgNnSc6d1	platné
znění	znění	k1gNnSc6	znění
ze	z	k7c2	z
dne	den	k1gInSc2	den
1.12	[number]	k4	1.12
<g/>
.2009	.2009	k4	.2009
a	a	k8xC	a
prováděcí	prováděcí	k2eAgFnSc1d1	prováděcí
vyhláškou	vyhláška	k1gFnSc7	vyhláška
MŽP	MŽP	kA	MŽP
189	[number]	k4	189
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Vlastník	vlastník	k1gMnSc1	vlastník
pozemku	pozemek	k1gInSc2	pozemek
smí	smět	k5eAaImIp3nS	smět
pokácet	pokácet	k5eAaPmF	pokácet
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obvod	obvod	k1gInSc1	obvod
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
80	[number]	k4	80
cm	cm	kA	cm
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1,30	[number]	k4	1,30
<g/>
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
větších	veliký	k2eAgInPc2d2	veliký
stromů	strom	k1gInPc2	strom
musí	muset	k5eAaImIp3nS	muset
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strom	strom	k1gInSc1	strom
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
životy	život	k1gInPc4	život
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
obyvatel	obyvatel	k1gMnPc2	obyvatel
nebo	nebo	k8xC	nebo
majetek	majetek	k1gInSc1	majetek
(	(	kIx(	(
<g/>
většího	veliký	k2eAgInSc2d2	veliký
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
lze	lze	k6eAd1	lze
strom	strom	k1gInSc1	strom
pokácet	pokácet	k5eAaPmF	pokácet
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
strom	strom	k1gInSc4	strom
před	před	k7c7	před
pokácením	pokácení	k1gNnSc7	pokácení
zdokumentovat	zdokumentovat	k5eAaPmF	zdokumentovat
a	a	k8xC	a
do	do	k7c2	do
15	[number]	k4	15
dnů	den	k1gInPc2	den
pokácení	pokácení	k1gNnPc2	pokácení
ohlásit	ohlásit	k5eAaPmF	ohlásit
obecnímu	obecní	k2eAgInSc3d1	obecní
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
případně	případně	k6eAd1	případně
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
neoprávněnosti	neoprávněnost	k1gFnSc6	neoprávněnost
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
připustit	připustit	k5eAaPmF	připustit
preventivní	preventivní	k2eAgNnSc4d1	preventivní
kácení	kácení	k1gNnSc4	kácení
zdravých	zdravý	k2eAgFnPc2d1	zdravá
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
působit	působit	k5eAaImF	působit
škodu	škoda	k1gFnSc4	škoda
například	například	k6eAd1	například
pádem	pád	k1gInSc7	pád
při	při	k7c6	při
větrném	větrný	k2eAgNnSc6d1	větrné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
stromy	strom	k1gInPc1	strom
může	moct	k5eAaImIp3nS	moct
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
za	za	k7c4	za
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
alej	alej	k1gFnSc4	alej
či	či	k8xC	či
stromořadí	stromořadí	k1gNnSc4	stromořadí
<g/>
.	.	kIx.	.
</s>
<s>
Nelesní	lesní	k2eNgFnSc1d1	nelesní
zeleň	zeleň	k1gFnSc1	zeleň
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
součástí	součást	k1gFnSc7	součást
územních	územní	k2eAgInPc2d1	územní
systémů	systém	k1gInPc2	systém
ekologické	ekologický	k2eAgFnSc2d1	ekologická
stability	stabilita	k1gFnSc2	stabilita
(	(	kIx(	(
<g/>
především	především	k9	především
biokoridorů	biokoridor	k1gInPc2	biokoridor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
významných	významný	k2eAgInPc2d1	významný
krajinných	krajinný	k2eAgInPc2d1	krajinný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
lesních	lesní	k2eAgInPc2d1	lesní
stromů	strom	k1gInPc2	strom
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
především	především	k6eAd1	především
pomocí	pomocí	k7c2	pomocí
lesního	lesní	k2eAgInSc2d1	lesní
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
každý	každý	k3xTgInSc1	každý
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
významným	významný	k2eAgMnSc7d1	významný
krajinným	krajinný	k2eAgInSc7d1	krajinný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
životní	životní	k2eAgFnSc2d1	životní
formy	forma	k1gFnSc2	forma
fanerofyty	fanerofyt	k1gInPc1	fanerofyt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
zimu	zima	k1gFnSc4	zima
<g/>
)	)	kIx)	)
přežívají	přežívat	k5eAaImIp3nP	přežívat
pomocí	pomocí	k7c2	pomocí
pupenů	pupen	k1gInPc2	pupen
uložených	uložený	k2eAgInPc2d1	uložený
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc4	rostlina
vytrvalé	vytrvalý	k2eAgFnPc4d1	vytrvalá
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
velice	velice	k6eAd1	velice
dlouhověké	dlouhověký	k2eAgFnPc1d1	dlouhověká
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
dožít	dožít	k5eAaPmF	dožít
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rekordmanem	rekordman	k1gMnSc7	rekordman
je	být	k5eAaImIp3nS	být
smrk	smrk	k1gInSc4	smrk
ztepilý	ztepilý	k2eAgInSc4d1	ztepilý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Starý	starý	k2eAgMnSc1d1	starý
Tjikko	Tjikko	k1gNnSc1	Tjikko
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
Tjikko	Tjikko	k1gNnSc1	Tjikko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
9550	[number]	k4	9550
lety	let	k1gInPc7	let
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
žijícím	žijící	k2eAgInSc7d1	žijící
stromem	strom	k1gInSc7	strom
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Fulufjället	Fulufjället	k1gInSc1	Fulufjället
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
stáří	stáří	k1gNnSc4	stáří
měří	měřit	k5eAaImIp3nS	měřit
pouhých	pouhý	k2eAgInPc2d1	pouhý
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgNnSc2	ten
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tzv.	tzv.	kA	tzv.
klonováním	klonování	k1gNnSc7	klonování
a	a	k8xC	a
vrstvením	vrstvení	k1gNnSc7	vrstvení
<g/>
.	.	kIx.	.
</s>
<s>
Klonování	klonování	k1gNnSc1	klonování
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kmen	kmen	k1gInSc1	kmen
stromu	strom	k1gInSc2	strom
po	po	k7c6	po
několika	několik	k4yIc6	několik
stech	sto	k4xCgNnPc6	sto
letech	léto	k1gNnPc6	léto
uhynul	uhynout	k5eAaPmAgMnS	uhynout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gInPc1	jeho
kořeny	kořen	k1gInPc1	kořen
přežívají	přežívat	k5eAaImIp3nP	přežívat
oněch	onen	k3xDgInPc2	onen
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vrstvení	vrstvení	k1gNnSc1	vrstvení
pak	pak	k6eAd1	pak
nastává	nastávat	k5eAaImIp3nS	nastávat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
větev	větev	k1gFnSc1	větev
stromu	strom	k1gInSc2	strom
dotkne	dotknout	k5eAaPmIp3nS	dotknout
země	zem	k1gFnPc1	zem
a	a	k8xC	a
"	"	kIx"	"
<g/>
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
kořenem	kořen	k1gInSc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
počítali	počítat	k5eAaImAgMnP	počítat
jen	jen	k9	jen
stáří	stáří	k1gNnSc4	stáří
podzemního	podzemní	k2eAgInSc2d1	podzemní
systému	systém	k1gInSc2	systém
samotného	samotný	k2eAgNnSc2d1	samotné
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
jen	jen	k9	jen
výhonků	výhonek	k1gInPc2	výhonek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
prvenství	prvenství	k1gNnSc1	prvenství
patřilo	patřit	k5eAaImAgNnS	patřit
stromu	strom	k1gInSc2	strom
Pando	panda	k1gFnSc5	panda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
to	ten	k3xDgNnSc1	ten
také	také	k9	také
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
část	část	k1gFnSc4	část
lesa	les	k1gInSc2	les
tvořeného	tvořený	k2eAgInSc2d1	tvořený
jedním	jeden	k4xCgInSc7	jeden
kořenovým	kořenový	k2eAgInSc7d1	kořenový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
stromy	strom	k1gInPc1	strom
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
lesa	les	k1gInSc2	les
Fishlake	Fishlake	k1gFnSc1	Fishlake
National	National	k1gFnSc1	National
Forest	Forest	k1gFnSc1	Forest
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
našli	najít	k5eAaPmAgMnP	najít
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
stromy	strom	k1gInPc1	strom
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
původního	původní	k2eAgInSc2d1	původní
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tu	tu	k6eAd1	tu
kdysi	kdysi	k6eAd1	kdysi
před	před	k7c7	před
desítkami	desítka	k1gFnPc7	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
vyrašil	vyrašit	k5eAaPmAgInS	vyrašit
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
tohoto	tento	k3xDgInSc2	tento
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
asi	asi	k9	asi
80	[number]	k4	80
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
dlouhověkým	dlouhověký	k2eAgInSc7d1	dlouhověký
druhem	druh	k1gInSc7	druh
tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Taxus	Taxus	k1gInSc1	Taxus
baccata	baccat	k1gMnSc2	baccat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
i	i	k9	i
přes	přes	k7c4	přes
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
krátkověké	krátkověký	k2eAgInPc1d1	krátkověký
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
pionýrských	pionýrský	k2eAgFnPc2d1	Pionýrská
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bříza	bříza	k1gFnSc1	bříza
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
(	(	kIx(	(
<g/>
Betula	Betula	k1gFnSc1	Betula
pendula	pendula	k1gFnSc1	pendula
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
dožije	dožít	k5eAaPmIp3nS	dožít
přes	přes	k7c4	přes
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
rekordmanem	rekordman	k1gMnSc7	rekordman
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Karlova	Karlův	k2eAgFnSc1d1	Karlova
lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Klokočově	klokočově	k6eAd1	klokočově
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
průzkumů	průzkum	k1gInPc2	průzkum
odhadován	odhadovat	k5eAaImNgInS	odhadovat
asi	asi	k9	asi
na	na	k7c4	na
800	[number]	k4	800
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
členěny	členit	k5eAaImNgFnP	členit
podle	podle	k7c2	podle
dosahované	dosahovaný	k2eAgFnSc2d1	dosahovaná
výšky	výška	k1gFnSc2	výška
do	do	k7c2	do
podkategorií	podkategorie	k1gFnPc2	podkategorie
<g/>
:	:	kIx,	:
stromek	stromek	k1gInSc1	stromek
(	(	kIx(	(
<g/>
arbor	arbor	k1gInSc1	arbor
parva	parva	k?	parva
<g/>
)	)	kIx)	)
do	do	k7c2	do
7	[number]	k4	7
m	m	kA	m
nízký	nízký	k2eAgInSc4d1	nízký
strom	strom	k1gInSc4	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gMnSc1	arbor
nana	nana	k1gMnSc1	nana
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
m	m	kA	m
středně	středně	k6eAd1	středně
vysoký	vysoký	k2eAgInSc1d1	vysoký
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gInSc1	arbor
mediocris	mediocris	k1gFnSc2	mediocris
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
m	m	kA	m
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g />
.	.	kIx.	.
</s>
<s>
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gMnSc1	arbor
alta	alta	k1gMnSc1	alta
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
m	m	kA	m
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc1d1	vysoký
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gInSc1	arbor
altissima	altissimum	k1gNnSc2	altissimum
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
50	[number]	k4	50
m	m	kA	m
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
se	s	k7c7	s
stromy	strom	k1gInPc7	strom
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zlomí	zlomit	k5eAaPmIp3nP	zlomit
od	od	k7c2	od
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
42	[number]	k4	42
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
žijícím	žijící	k2eAgInSc7d1	žijící
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
sekvoj	sekvoj	k1gInSc1	sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Sequoia	Sequoia	k1gFnSc1	Sequoia
sempervirens	sempervirensa	k1gFnPc2	sempervirensa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Hyperion	Hyperion	k1gInSc1	Hyperion
<g/>
"	"	kIx"	"
v	v	k7c4	v
Redwood	Redwood	k1gInSc4	Redwood
National	National	k1gFnSc2	National
and	and	k?	and
State	status	k1gInSc5	status
Parks	Parks	k1gInSc4	Parks
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
115,5	[number]	k4	115,5
m.	m.	k?	m.
Strom	strom	k1gInSc1	strom
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
objemem	objem	k1gInSc7	objem
je	být	k5eAaImIp3nS	být
sekvojovec	sekvojovec	k1gInSc1	sekvojovec
obrovský	obrovský	k2eAgInSc1d1	obrovský
(	(	kIx(	(
<g/>
Sequoiadendron	Sequoiadendron	k1gInSc1	Sequoiadendron
giganteum	giganteum	k1gNnSc1	giganteum
<g/>
)	)	kIx)	)
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
Generál	generál	k1gMnSc1	generál
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
"	"	kIx"	"
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Sequoia	Sequoium	k1gNnSc2	Sequoium
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
objemu	objem	k1gInSc2	objem
1473,4	[number]	k4	1473,4
m3	m3	k4	m3
<g/>
,	,	kIx,	,
výšky	výška	k1gFnPc1	výška
83,8	[number]	k4	83,8
m	m	kA	m
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
u	u	k7c2	u
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
11	[number]	k4	11
m	m	kA	m
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
hlavní	hlavní	k2eAgFnSc2d1	hlavní
větve	větev	k1gFnSc2	větev
je	být	k5eAaImIp3nS	být
2,1	[number]	k4	2,1
m.	m.	k?	m.
Strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
největšího	veliký	k2eAgInSc2d3	veliký
známého	známý	k2eAgInSc2d1	známý
obvodu	obvod	k1gInSc2	obvod
kmene	kmen	k1gInSc2	kmen
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tisovec	tisovec	k1gInSc1	tisovec
Montezumův	Montezumův	k2eAgInSc1d1	Montezumův
(	(	kIx(	(
<g/>
Taxodium	Taxodium	k1gNnSc1	Taxodium
mucronatum	mucronatum	k1gNnSc1	mucronatum
<g/>
)	)	kIx)	)
zvaný	zvaný	k2eAgInSc1d1	zvaný
El	Ela	k1gFnPc2	Ela
Árbol	Árbol	k1gInSc4	Árbol
del	del	k?	del
Tule	Tula	k1gFnSc3	Tula
neboli	neboli	k8xC	neboli
Tulský	tulský	k2eAgInSc1d1	tulský
strom	strom	k1gInSc1	strom
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Mexiku	Mexiko	k1gNnSc6	Mexiko
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
kmene	kmen	k1gInSc2	kmen
42	[number]	k4	42
m	m	kA	m
a	a	k8xC	a
průměrem	průměr	k1gInSc7	průměr
14	[number]	k4	14
m.	m.	k?	m.
Nejmenší	malý	k2eAgInSc1d3	nejmenší
známý	známý	k2eAgInSc1d1	známý
kvetoucí	kvetoucí	k2eAgInSc1d1	kvetoucí
strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
jedinec	jedinec	k1gMnSc1	jedinec
druhu	druh	k1gInSc2	druh
Monanthes	Monanthes	k1gMnSc1	Monanthes
polyphylla	polyphylla	k1gMnSc1	polyphylla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
1,8	[number]	k4	1,8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
českým	český	k2eAgInSc7d1	český
smrkem	smrk	k1gInSc7	smrk
byl	být	k5eAaImAgMnS	být
Král	Král	k1gMnSc1	Král
smrků	smrk	k1gInPc2	smrk
v	v	k7c6	v
Boubínském	boubínský	k2eAgInSc6d1	boubínský
pralese	prales	k1gInSc6	prales
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
pádem	pád	k1gInSc7	pád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
výšku	výška	k1gFnSc4	výška
57,2	[number]	k4	57,2
m	m	kA	m
a	a	k8xC	a
objem	objem	k1gInSc1	objem
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Krále	Král	k1gMnSc2	Král
smrků	smrk	k1gInPc2	smrk
z	z	k7c2	z
Boubína	Boubín	k1gInSc2	Boubín
již	již	k6eAd1	již
přerostl	přerůst	k5eAaPmAgInS	přerůst
tzv.	tzv.	kA	tzv.
Těptínský	Těptínský	k2eAgInSc4d1	Těptínský
smrk	smrk	k1gInSc4	smrk
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
350	[number]	k4	350
m	m	kA	m
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Kamenického	Kamenického	k2eAgInSc2d1	Kamenického
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
Těptína	Těptín	k1gInSc2	Těptín
(	(	kIx(	(
<g/>
východně	východně	k6eAd1	východně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
měřil	měřit	k5eAaImAgInS	měřit
58	[number]	k4	58
m	m	kA	m
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
515	[number]	k4	515
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Těptínský	Těptínský	k2eAgInSc1d1	Těptínský
smrk	smrk	k1gInSc1	smrk
byl	být	k5eAaImAgInS	být
vyvrácen	vyvrátit	k5eAaPmNgInS	vyvrátit
při	při	k7c6	při
vichřicí	vichřice	k1gFnSc7	vichřice
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
těptínských	těptínský	k2eAgInPc2d1	těptínský
smrků	smrk	k1gInPc2	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
českým	český	k2eAgInSc7d1	český
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
64,1	[number]	k4	64,1
metru	metr	k1gInSc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
douglaska	douglaska	k1gFnSc1	douglaska
tisolistá	tisolistý	k2eAgFnSc1d1	tisolistá
podle	podle	k7c2	podle
měření	měření	k1gNnSc2	měření
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
Vlastiboři	Vlastiboř	k1gFnSc6	Vlastiboř
na	na	k7c6	na
Jablonecku	Jablonecko	k1gNnSc6	Jablonecko
ve	v	k7c6	v
tříhektarovém	tříhektarový	k2eAgInSc6d1	tříhektarový
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vysoká	vysoký	k2eAgFnSc1d1	vysoká
skoro	skoro	k6eAd1	skoro
jako	jako	k8xS	jako
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Přírůstek	přírůstek	k1gInSc1	přírůstek
tvoří	tvořit	k5eAaImIp3nS	tvořit
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
asi	asi	k9	asi
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Stromem	strom	k1gInSc7	strom
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
objemem	objem	k1gInSc7	objem
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
topol	topol	k1gInSc1	topol
černý	černý	k2eAgInSc1d1	černý
v	v	k7c6	v
Lochovicích	Lochovice	k1gFnPc6	Lochovice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
obvod	obvod	k1gInSc1	obvod
15,7	[number]	k4	15,7
m.	m.	k?	m.
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vyvrácen	vyvrácen	k2eAgInSc4d1	vyvrácen
vichřicí	vichřice	k1gFnSc7	vichřice
<g/>
.	.	kIx.	.
</s>
<s>
Stromem	strom	k1gInSc7	strom
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
obvodem	obvod	k1gInSc7	obvod
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
Tatrovická	Tatrovický	k2eAgFnSc1d1	Tatrovická
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
obvod	obvod	k1gInSc4	obvod
16,5	[number]	k4	16,5
m	m	kA	m
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
srostlicí	srostlice	k1gFnSc7	srostlice
10	[number]	k4	10
kmenů	kmen	k1gInPc2	kmen
vyrůstajících	vyrůstající	k2eAgInPc2d1	vyrůstající
ze	z	k7c2	z
společné	společný	k2eAgFnSc2d1	společná
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
kolem	kolem	k7c2	kolem
původního	původní	k2eAgInSc2d1	původní
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
obvod	obvod	k1gInSc1	obvod
1122	[number]	k4	1122
cm	cm	kA	cm
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
rozlámána	rozlámat	k5eAaPmNgFnS	rozlámat
při	při	k7c6	při
vichřici	vichřice	k1gFnSc6	vichřice
<g/>
.	.	kIx.	.
</s>
<s>
Stromem	strom	k1gInSc7	strom
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
obvodem	obvod	k1gInSc7	obvod
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
topol	topol	k1gInSc1	topol
černý	černý	k2eAgInSc1d1	černý
v	v	k7c6	v
Lochovicích	Lochovice	k1gFnPc6	Lochovice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
obvod	obvod	k1gInSc4	obvod
15,7	[number]	k4	15,7
m	m	kA	m
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejspíš	nejspíš	k9	nejspíš
největším	veliký	k2eAgInSc7d3	veliký
dosaženým	dosažený	k2eAgInSc7d1	dosažený
obvodem	obvod	k1gInSc7	obvod
u	u	k7c2	u
topolu	topol	k1gInSc2	topol
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
znám	znám	k2eAgInSc1d1	znám
topol	topol	k1gInSc1	topol
o	o	k7c6	o
větším	veliký	k2eAgInSc6d2	veliký
obvodu	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
než	než	k8xS	než
měl	mít	k5eAaImAgMnS	mít
tento	tento	k3xDgMnSc1	tento
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
formou	forma	k1gFnSc7	forma
vzrůstu	vzrůst	k1gInSc2	vzrůst
měl	mít	k5eAaImAgMnS	mít
tento	tento	k3xDgInSc4	tento
topol	topol	k1gInSc4	topol
největší	veliký	k2eAgInSc1d3	veliký
obvod	obvod	k1gInSc1	obvod
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
