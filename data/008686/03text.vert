<p>
<s>
Gouffre	Gouffr	k1gMnSc5	Gouffr
Mirolda	Mirolda	k1gMnSc1	Mirolda
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
nejhlubší	hluboký	k2eAgFnSc2d3	nejhlubší
jeskyně	jeskyně	k1gFnSc2	jeskyně
v	v	k7c6	v
Savojských	savojský	k2eAgFnPc6d1	Savojská
Alpách	Alpy	k1gFnPc6	Alpy
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
dostal	dostat	k5eAaPmAgInS	dostat
francouzsko-anglický	francouzskonglický	k2eAgInSc1d1	francouzsko-anglický
speleopotápěčský	speleopotápěčský	k2eAgInSc1d1	speleopotápěčský
tým	tým	k1gInSc1	tým
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
103	[number]	k4	103
<g/>
hodinovém	hodinový	k2eAgInSc6d1	hodinový
pobytu	pobyt	k1gInSc6	pobyt
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
1626	[number]	k4	1626
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
jeskyně	jeskyně	k1gFnSc2	jeskyně
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
okamžiku	okamžik	k1gInSc6	okamžik
stala	stát	k5eAaPmAgFnS	stát
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
objevech	objev	k1gInPc6	objev
je	být	k5eAaImIp3nS	být
Gouffre	Gouffr	k1gInSc5	Gouffr
Mirolda	Mirolda	k1gMnSc1	Mirolda
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
hloubkou	hloubka	k1gFnSc7	hloubka
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mezi	mezi	k7c7	mezi
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
(	(	kIx(	(
<g/>
po	po	k7c6	po
dvoukilometrové	dvoukilometrový	k2eAgFnSc6d1	dvoukilometrová
abcházské	abcházský	k2eAgFnSc6d1	abcházská
jeskyni	jeskyně	k1gFnSc6	jeskyně
Voronija	Voronijum	k1gNnSc2	Voronijum
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k1gNnSc1	málo
hlubší	hluboký	k2eAgFnSc4d2	hlubší
jeskyni	jeskyně	k1gFnSc4	jeskyně
Lamprechtsofen	Lamprechtsofna	k1gFnPc2	Lamprechtsofna
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
aktuálně	aktuálně	k6eAd1	aktuálně
naměřená	naměřený	k2eAgFnSc1d1	naměřená
hloubka	hloubka	k1gFnSc1	hloubka
jeskyně	jeskyně	k1gFnSc2	jeskyně
1	[number]	k4	1
733	[number]	k4	733
m	m	kA	m
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
nejhlubšími	hluboký	k2eAgFnPc7d3	nejhlubší
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Deset	deset	k4xCc1	deset
nejhlubších	hluboký	k2eAgFnPc2d3	nejhlubší
jeskyní	jeskyně	k1gFnPc2	jeskyně
světa	svět	k1gInSc2	svět
</s>
</p>
