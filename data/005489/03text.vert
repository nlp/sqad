<s>
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc1	Chile
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
na	na	k7c4	na
úpatí	úpatí	k1gNnSc4	úpatí
And	Anda	k1gFnPc2	Anda
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
město	město	k1gNnSc4	město
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1541	[number]	k4	1541
jej	on	k3xPp3gInSc4	on
založil	založit	k5eAaPmAgMnS	založit
španělský	španělský	k2eAgMnSc1d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Pedro	Pedro	k1gNnSc1	Pedro
de	de	k?	de
Valdivia	Valdivius	k1gMnSc2	Valdivius
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bránila	bránit	k5eAaImAgFnS	bránit
obsazená	obsazený	k2eAgNnPc1d1	obsazené
a	a	k8xC	a
dobytá	dobytý	k2eAgNnPc1d1	dobyté
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
guvernéra	guvernér	k1gMnSc2	guvernér
a	a	k8xC	a
střediskem	středisko	k1gNnSc7	středisko
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
pro	pro	k7c4	pro
zdejší	zdejší	k2eAgFnSc4d1	zdejší
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc4	město
poničilo	poničit	k5eAaPmAgNnS	poničit
několik	několik	k4yIc1	několik
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
město	město	k1gNnSc4	město
napadaly	napadat	k5eAaBmAgInP	napadat
nájezdy	nájezd	k1gInPc1	nájezd
Araukánců	Araukánec	k1gMnPc2	Araukánec
<g/>
,	,	kIx,	,
indiánských	indiánský	k2eAgMnPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zde	zde	k6eAd1	zde
před	před	k7c7	před
kolonizací	kolonizace	k1gFnSc7	kolonizace
žily	žít	k5eAaImAgFnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
metropolí	metropol	k1gFnSc7	metropol
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
Bernardem	Bernard	k1gMnSc7	Bernard
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Higginsem	Higgins	k1gMnSc7	Higgins
a	a	k8xC	a
José	José	k1gNnSc7	José
de	de	k?	de
San	San	k1gFnSc2	San
Martínem	Martín	k1gInSc7	Martín
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
Santiago	Santiago	k1gNnSc1	Santiago
povýšilo	povýšit	k5eAaPmAgNnS	povýšit
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Republiky	republika	k1gFnSc2	republika
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Santiagu	Santiago	k1gNnSc6	Santiago
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
odlišit	odlišit	k5eAaPmF	odlišit
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
od	od	k7c2	od
nových	nový	k2eAgFnPc2d1	nová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejzajímavějších	zajímavý	k2eAgNnPc2d3	nejzajímavější
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
návrší	návrší	k1gNnPc4	návrší
Santa	Sant	k1gMnSc2	Sant
Lucía	Lucíus	k1gMnSc2	Lucíus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úpatí	úpatí	k1gNnSc4	úpatí
tohoto	tento	k3xDgInSc2	tento
opevněného	opevněný	k2eAgInSc2d1	opevněný
kopce	kopec	k1gInSc2	kopec
stojí	stát	k5eAaImIp3nS	stát
Neptunova	Neptunův	k2eAgFnSc1d1	Neptunova
fontána	fontána	k1gFnSc1	fontána
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
indiánského	indiánský	k2eAgNnSc2d1	indiánské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
mozaiku	mozaika	k1gFnSc4	mozaika
chilská	chilský	k2eAgFnSc1d1	chilská
básnířka	básnířka	k1gFnSc1	básnířka
Gabriela	Gabriela	k1gFnSc1	Gabriela
Mistralová	Mistralová	k1gFnSc1	Mistralová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
získala	získat	k5eAaPmAgFnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
Bellavista	Bellavist	k1gMnSc2	Bellavist
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
svými	svůj	k3xOyFgNnPc7	svůj
romantickými	romantický	k2eAgNnPc7d1	romantické
zákoutími	zákoutí	k1gNnPc7	zákoutí
a	a	k8xC	a
klidnými	klidný	k2eAgFnPc7d1	klidná
uličkami	ulička	k1gFnPc7	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
chilský	chilský	k2eAgMnSc1d1	chilský
básník	básník	k1gMnSc1	básník
Pablo	Pablo	k1gNnSc1	Pablo
Neruda	Neruda	k1gMnSc1	Neruda
(	(	kIx(	(
<g/>
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
postavil	postavit	k5eAaPmAgMnS	postavit
své	svůj	k3xOyFgFnSc3	svůj
milované	milovaný	k2eAgFnSc3d1	milovaná
ženě	žena	k1gFnSc3	žena
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
jeho	jeho	k3xOp3gNnSc4	jeho
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
staveb	stavba	k1gFnPc2	stavba
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
četných	četný	k2eAgNnPc6d1	četné
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc1	Chile
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
33	[number]	k4	33
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
deštivé	deštivý	k2eAgNnSc1d1	deštivé
počasí	počasí	k1gNnSc1	počasí
–	–	k?	–
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
bývají	bývat	k5eAaImIp3nP	bývat
kolem	kolem	k7c2	kolem
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
několikastupňové	několikastupňový	k2eAgInPc1d1	několikastupňový
mrazy	mráz	k1gInPc1	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
inverzím	inverze	k1gFnPc3	inverze
bývá	bývat	k5eAaImIp3nS	bývat
kvalita	kvalita	k1gFnSc1	kvalita
vzduchu	vzduch	k1gInSc2	vzduch
často	často	k6eAd1	často
horší	zlý	k2eAgMnSc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
obvykle	obvykle	k6eAd1	obvykle
téměř	téměř	k6eAd1	téměř
neprší	pršet	k5eNaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
stoupají	stoupat	k5eAaImIp3nP	stoupat
až	až	k6eAd1	až
ke	k	k7c3	k
35	[number]	k4	35
o	o	k7c4	o
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
312	[number]	k4	312
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
systém	systém	k1gInSc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Transantiago	Transantiago	k1gNnSc1	Transantiago
<g/>
"	"	kIx"	"
tvořený	tvořený	k2eAgMnSc1d1	tvořený
převážně	převážně	k6eAd1	převážně
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
propojen	propojit	k5eAaPmNgInS	propojit
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
metrem	metro	k1gNnSc7	metro
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc7d3	veliký
městskou	městský	k2eAgFnSc7d1	městská
železniční	železniční	k2eAgFnSc7d1	železniční
sítí	síť	k1gFnSc7	síť
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Letiště	letiště	k1gNnSc1	letiště
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc1	Chile
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
připojení	připojení	k1gNnSc4	připojení
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
americkými	americký	k2eAgFnPc7d1	americká
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
lokalitami	lokalita	k1gFnPc7	lokalita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
36	[number]	k4	36
samosprávnými	samosprávný	k2eAgFnPc7d1	samosprávná
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
comuna	comuna	k1gFnSc1	comuna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
čtvrtím	čtvrt	k1gFnPc3	čtvrt
<g/>
.	.	kIx.	.
4	[number]	k4	4
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
provincie	provincie	k1gFnSc2	provincie
Santiago	Santiago	k1gNnSc4	Santiago
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
dnes	dnes	k6eAd1	dnes
nejlidnatější	lidnatý	k2eAgFnSc4d3	nejlidnatější
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Puente	Puent	k1gMnSc5	Puent
Alto	Alta	k1gMnSc5	Alta
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
obcemi	obec	k1gFnPc7	obec
Santiaga	Santiago	k1gNnSc2	Santiago
panuje	panovat	k5eAaImIp3nS	panovat
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
sociální	sociální	k2eAgFnSc1d1	sociální
nerovnost	nerovnost	k1gFnSc1	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
horami	hora	k1gFnPc7	hora
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zejména	zejména	k9	zejména
rezidenční	rezidenční	k2eAgFnPc1d1	rezidenční
čtvrtě	čtvrt	k1gFnPc1	čtvrt
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
elity	elita	k1gFnSc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Chudé	Chudé	k2eAgFnPc1d1	Chudé
čtvrtě	čtvrt	k1gFnPc1	čtvrt
leží	ležet	k5eAaImIp3nP	ležet
zejména	zejména	k9	zejména
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
