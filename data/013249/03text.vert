<p>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
či	či	k8xC	či
Juro	Jura	k1gMnSc5	Jura
Jánošak	Jánošak	k1gInSc4	Jánošak
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Jerzy	Jerza	k1gFnPc1	Jerza
Janosik	Janosikum	k1gNnPc2	Janosikum
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Jánosik	Jánosik	k1gInSc1	Jánosik
György	Györg	k1gInPc1	Györg
(	(	kIx(	(
<g/>
1688	[number]	k4	1688
Terchová	Terchová	k1gFnSc1	Terchová
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1713	[number]	k4	1713
Liptovský	liptovský	k2eAgInSc1d1	liptovský
Mikuláš	mikuláš	k1gInSc1	mikuláš
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
slovenský	slovenský	k2eAgMnSc1d1	slovenský
zbojník	zbojník	k1gMnSc1	zbojník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
narození	narození	k1gNnSc1	narození
===	===	k?	===
</s>
</p>
<p>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
kysucké	kysucký	k2eAgFnSc6d1	kysucký
vsi	ves	k1gFnSc6	ves
Terchová	Terchová	k1gFnSc1	Terchová
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
kopaničářské	kopaničářský	k2eAgFnSc6d1	kopaničářská
samotě	samota	k1gFnSc6	samota
U	u	k7c2	u
Jánošů	Jánoš	k1gMnPc2	Jánoš
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pokřtěný	pokřtěný	k2eAgInSc1d1	pokřtěný
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
ve	v	k7c6	v
Varíně	Varína	k1gFnSc6	Varína
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1688	[number]	k4	1688
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Martin	Martin	k1gMnSc1	Martin
Jánošík	Jánošík	k1gMnSc1	Jánošík
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
rozená	rozený	k2eAgFnSc1d1	rozená
Cesneková	Cesneková	k1gFnSc1	Cesneková
<g/>
,	,	kIx,	,
kmotry	kmotra	k1gFnPc1	kmotra
Jakub	Jakub	k1gMnSc1	Jakub
Merjad	Merjad	k1gInSc1	Merjad
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
Krištofiková	Krištofikový	k2eAgFnSc1d1	Krištofikový
z	z	k7c2	z
Terchové	Terchová	k1gFnSc2	Terchová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stejné	stejný	k2eAgFnSc2d1	stejná
matriky	matrika	k1gFnSc2	matrika
měl	mít	k5eAaImAgMnS	mít
Juraj	Juraj	k1gMnSc1	Juraj
sourozence	sourozenec	k1gMnSc4	sourozenec
Jana	Jan	k1gMnSc4	Jan
<g/>
,	,	kIx,	,
Martina	Martin	k1gMnSc4	Martin
<g/>
,	,	kIx,	,
Adama	Adam	k1gMnSc4	Adam
a	a	k8xC	a
Barbaru	Barbara	k1gFnSc4	Barbara
<g/>
.	.	kIx.	.
</s>
<s>
Chlapců	chlapec	k1gMnPc2	chlapec
totožného	totožný	k2eAgNnSc2d1	totožné
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jiných	jiný	k2eAgInPc2d1	jiný
dobových	dobový	k2eAgInPc2d1	dobový
dokumentů	dokument	k1gInPc2	dokument
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
datum	datum	k1gNnSc4	datum
Jánošíkova	Jánošíkův	k2eAgInSc2d1	Jánošíkův
křtu	křest	k1gInSc2	křest
16	[number]	k4	16
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1688	[number]	k4	1688
<g/>
,	,	kIx,	,
za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
rodiče	rodič	k1gMnSc2	rodič
Michala	Michal	k1gMnSc2	Michal
Jánošíka	Jánošík	k1gMnSc2	Jánošík
a	a	k8xC	a
Barbaru	Barbara	k1gFnSc4	Barbara
rozenou	rozený	k2eAgFnSc4d1	rozená
Cingelovou	Cingelová	k1gFnSc4	Cingelová
<g/>
,	,	kIx,	,
za	za	k7c4	za
kmotry	kmotra	k1gFnPc4	kmotra
Pavla	Pavel	k1gMnSc2	Pavel
Laurinčíka	Laurinčík	k1gMnSc2	Laurinčík
a	a	k8xC	a
Dorotu	Dorota	k1gFnSc4	Dorota
Bobanovou	Bobanová	k1gFnSc4	Bobanová
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgMnPc4	všechen
z	z	k7c2	z
Terchové	Terchová	k1gFnSc2	Terchová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
měl	mít	k5eAaImAgInS	mít
Juraj	Juraj	k1gInSc1	Juraj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Barbaru	Barbara	k1gFnSc4	Barbara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
angažmá	angažmá	k1gNnSc1	angažmá
(	(	kIx(	(
<g/>
1706	[number]	k4	1706
<g/>
–	–	k?	–
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
Jánošík	Jánošík	k1gMnSc1	Jánošík
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
oddílů	oddíl	k1gInPc2	oddíl
podporujících	podporující	k2eAgInPc2d1	podporující
Františka	Františka	k1gFnSc1	Františka
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákócziho	Rákóczi	k1gMnSc2	Rákóczi
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1703	[number]	k4	1703
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gInPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Jánošík	Jánošík	k1gMnSc1	Jánošík
strávil	strávit	k5eAaPmAgMnS	strávit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
pěším	pěší	k2eAgInSc6d1	pěší
pluku	pluk	k1gInSc6	pluk
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
plukovníka	plukovník	k1gMnSc2	plukovník
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Winklera	Winkler	k1gMnSc2	Winkler
(	(	kIx(	(
<g/>
Viliam	Viliam	k1gMnSc1	Viliam
Vinkler	Vinkler	k1gMnSc1	Vinkler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
po	po	k7c6	po
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
porážce	porážka	k1gFnSc6	porážka
kuruckého	kurucký	k2eAgNnSc2d1	kurucké
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Trenčína	Trenčín	k1gInSc2	Trenčín
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1708	[number]	k4	1708
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Terchové	Terchová	k1gFnSc2	Terchová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
zanedlouho	zanedlouho	k6eAd1	zanedlouho
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
–	–	k?	–
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
přinucení	přinucení	k1gNnSc2	přinucení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
posádkou	posádka	k1gFnSc7	posádka
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
v	v	k7c6	v
Bytči	Bytč	k1gFnSc6	Bytč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
tenkrát	tenkrát	k6eAd1	tenkrát
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
zbojník	zbojník	k1gMnSc1	zbojník
Tomáš	Tomáš	k1gMnSc1	Tomáš
Uhorčík	Uhorčík	k1gMnSc1	Uhorčík
<g/>
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc4	tvrzení
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uhorčíkovi	Uhorčík	k1gMnSc3	Uhorčík
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ověřitelné	ověřitelný	k2eAgNnSc1d1	ověřitelné
a	a	k8xC	a
zakládá	zakládat	k5eAaImIp3nS	zakládat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
zmínce	zmínka	k1gFnSc6	zmínka
v	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
protokolu	protokol	k1gInSc6	protokol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jánošík	Jánošík	k1gMnSc1	Jánošík
vězni	vězeň	k1gMnPc1	vězeň
"	"	kIx"	"
<g/>
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
mnohá	mnohý	k2eAgNnPc4d1	mnohé
dobrodiní	dobrodiní	k1gNnPc4	dobrodiní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
protokolu	protokol	k1gInSc2	protokol
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
Uhorčíkově	Uhorčíkův	k2eAgFnSc3d1	Uhorčíkův
útěku	útěk	k1gInSc3	útěk
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
Jánošík	Jánošík	k1gMnSc1	Jánošík
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
vykoupen	vykoupen	k2eAgInSc1d1	vykoupen
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
propuštěn	propuštěn	k2eAgInSc4d1	propuštěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zboj	zboj	k1gInSc1	zboj
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1713	[number]	k4	1713
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Jánošíkově	Jánošíkův	k2eAgInSc6d1	Jánošíkův
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Terchové	Terchová	k1gFnSc2	Terchová
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1711	[number]	k4	1711
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
Uhorčík	Uhorčík	k1gMnSc1	Uhorčík
a	a	k8xC	a
přizval	přizvat	k5eAaPmAgInS	přizvat
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
zboji	zboj	k1gInSc6	zboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
Jánošík	Jánošík	k1gMnSc1	Jánošík
s	s	k7c7	s
Uhorčíkovou	Uhorčíkův	k2eAgFnSc7d1	Uhorčíkův
družinou	družina	k1gFnSc7	družina
účastnil	účastnit	k5eAaImAgMnS	účastnit
přepadení	přepadení	k1gNnSc4	přepadení
bohatého	bohatý	k2eAgMnSc2d1	bohatý
žilinského	žilinský	k2eAgMnSc2d1	žilinský
kupce	kupec	k1gMnSc2	kupec
Jana	Jan	k1gMnSc2	Jan
Šipoše	Šipoš	k1gMnSc2	Šipoš
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
Strečno	Strečno	k1gNnSc1	Strečno
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Uhorčík	Uhorčík	k1gMnSc1	Uhorčík
následně	následně	k6eAd1	následně
Jánošíkovi	Jánošík	k1gMnSc3	Jánošík
předal	předat	k5eAaPmAgInS	předat
vedení	vedení	k1gNnSc4	vedení
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
jablunkovského	jablunkovský	k2eAgMnSc2d1	jablunkovský
ovčáka	ovčák	k1gMnSc2	ovčák
Stoligy	Stoliga	k1gFnSc2	Stoliga
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Klenovci	Klenovec	k1gMnPc7	Klenovec
v	v	k7c6	v
Malohontské	Malohontský	k2eAgFnSc6d1	Malohontský
župě	župa	k1gFnSc6	župa
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Martin	Martin	k1gMnSc1	Martin
Mravec	Mravec	k1gMnSc1	Mravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Družina	družina	k1gFnSc1	družina
pod	pod	k7c7	pod
Jánošíkovým	Jánošíkův	k2eAgNnSc7d1	Jánošíkovo
vedením	vedení	k1gNnSc7	vedení
působila	působit	k5eAaImAgFnS	působit
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
první	první	k4xOgFnSc7	první
akcí	akce	k1gFnSc7	akce
bylo	být	k5eAaImAgNnS	být
přepadení	přepadení	k1gNnSc3	přepadení
židovských	židovský	k2eAgMnPc2d1	židovský
kupců	kupec	k1gMnPc2	kupec
ve	v	k7c6	v
Vsetíně	Vsetín	k1gInSc6	Vsetín
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1711	[number]	k4	1711
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
přepadla	přepadnout	k5eAaPmAgFnS	přepadnout
a	a	k8xC	a
okradla	okrást	k5eAaPmAgFnS	okrást
několik	několik	k4yIc4	několik
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ženu	žena	k1gFnSc4	žena
faráře	farář	k1gMnSc2	farář
<g/>
,	,	kIx,	,
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
císařském	císařský	k2eAgInSc6d1	císařský
důstojníkovi	důstojníkův	k2eAgMnPc1d1	důstojníkův
či	či	k8xC	či
statkářku	statkářka	k1gFnSc4	statkářka
<g/>
.	.	kIx.	.
</s>
<s>
Zbojníci	zbojník	k1gMnPc1	zbojník
operovali	operovat	k5eAaImAgMnP	operovat
v	v	k7c6	v
Trenčínské	trenčínský	k2eAgFnSc6d1	Trenčínská
<g/>
,	,	kIx,	,
Nitranské	nitranský	k2eAgFnSc6d1	Nitranská
<g/>
,	,	kIx,	,
Turčanské	Turčanský	k2eAgFnSc6d1	Turčanský
<g/>
,	,	kIx,	,
Liptovské	liptovský	k2eAgFnSc6d1	Liptovská
<g/>
,	,	kIx,	,
Oravské	oravský	k2eAgFnSc6d1	Oravská
<g/>
,	,	kIx,	,
Zvolenské	zvolenský	k2eAgFnSc3d1	Zvolenská
a	a	k8xC	a
Malohontské	Malohontský	k2eAgFnSc3d1	Malohontský
župě	župa	k1gFnSc3	župa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Podhalí	Podhalý	k2eAgMnPc5d1	Podhalý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbojnickou	zbojnický	k2eAgFnSc4d1	Zbojnická
družinu	družina	k1gFnSc4	družina
nelze	lze	k6eNd1	lze
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
homogenní	homogenní	k2eAgFnSc4d1	homogenní
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
Jánošíkovu	Jánošíkův	k2eAgFnSc4d1	Jánošíkova
tvořili	tvořit	k5eAaImAgMnP	tvořit
Slováci	Slovák	k1gMnPc1	Slovák
<g/>
,	,	kIx,	,
Moravané	Moravan	k1gMnPc1	Moravan
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Jedni	jeden	k4xCgMnPc1	jeden
členové	člen	k1gMnPc1	člen
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
odcházeli	odcházet	k5eAaImAgMnP	odcházet
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
přícházeli	přícházet	k5eAaImAgMnP	přícházet
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
zájemce	zájemce	k1gMnSc1	zájemce
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
Jánošík	Jánošík	k1gMnSc1	Jánošík
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
"	"	kIx"	"
<g/>
horní	horní	k2eAgMnPc1d1	horní
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
"	"	kIx"	"
zbojničili	zbojničit	k5eAaImAgMnP	zbojničit
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
svátku	svátek	k1gInSc2	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
)	)	kIx)	)
rozcházeli	rozcházet	k5eAaImAgMnP	rozcházet
sloužit	sloužit	k5eAaImF	sloužit
do	do	k7c2	do
vsí	ves	k1gFnPc2	ves
a	a	k8xC	a
na	na	k7c4	na
samoty	samota	k1gFnPc4	samota
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xC	jako
čeledínové	čeledín	k1gMnPc1	čeledín
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
svatého	svatý	k2eAgMnSc4d1	svatý
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
na	na	k7c6	na
smluveném	smluvený	k2eAgNnSc6d1	smluvené
místě	místo	k1gNnSc6	místo
opět	opět	k6eAd1	opět
sejdou	sejít	k5eAaPmIp3nP	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Jánošík	Jánošík	k1gMnSc1	Jánošík
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
zimu	zima	k1gFnSc4	zima
zpočátku	zpočátku	k6eAd1	zpočátku
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
v	v	k7c6	v
Terchové	Terchová	k1gFnSc6	Terchová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Kokavě	Kokava	k1gFnSc6	Kokava
nedaleko	nedaleko	k7c2	nedaleko
Klenovce	klenovka	k1gFnSc3	klenovka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
u	u	k7c2	u
Uhorčíka	Uhorčík	k1gMnSc2	Uhorčík
alias	alias	k9	alias
Mravce	Mravce	k1gMnSc1	Mravce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dopadení	dopadení	k1gNnPc1	dopadení
a	a	k8xC	a
soud	soud	k1gInSc1	soud
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
Jánošíka	Jánošík	k1gMnSc4	Jánošík
dopadli	dopadnout	k5eAaPmAgMnP	dopadnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
Uhorčíkem	Uhorčík	k1gInSc7	Uhorčík
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1712	[number]	k4	1712
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Hrachově	hrachově	k6eAd1	hrachově
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
díky	díky	k7c3	díky
intervenci	intervence	k1gFnSc3	intervence
Malohontského	Malohontský	k2eAgMnSc2d1	Malohontský
podžupana	podžupan	k1gMnSc2	podžupan
Pavla	Pavel	k1gMnSc2	Pavel
Lányiho	Lányi	k1gMnSc2	Lányi
<g/>
,	,	kIx,	,
podporovatele	podporovatel	k1gMnSc2	podporovatel
kuruckého	kurucký	k2eAgNnSc2d1	kurucké
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
byl	být	k5eAaImAgMnS	být
Jánošík	Jánošík	k1gMnSc1	Jánošík
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
liptovskými	liptovský	k2eAgMnPc7d1	liptovský
hajduky	hajduk	k1gMnPc7	hajduk
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1713	[number]	k4	1713
společně	společně	k6eAd1	společně
s	s	k7c7	s
Uhorčíkem	Uhorčík	k1gInSc7	Uhorčík
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
chalupě	chalupa	k1gFnSc6	chalupa
v	v	k7c6	v
Klenovci	Klenovec	k1gMnSc6	Klenovec
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Vranovo	Vranův	k2eAgNnSc1d1	Vranovo
v	v	k7c6	v
Palúdzce	Palúdzka	k1gFnSc6	Palúdzka
u	u	k7c2	u
Liptovského	liptovský	k2eAgMnSc2d1	liptovský
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Liptovském	liptovský	k2eAgInSc6d1	liptovský
Mikuláši	mikuláš	k1gInSc6	mikuláš
ve	v	k7c6	v
dnech	den	k1gInPc6	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1713	[number]	k4	1713
<g/>
.	.	kIx.	.
</s>
<s>
Soudu	soud	k1gInSc2	soud
předsedal	předsedat	k5eAaImAgMnS	předsedat
liptovský	liptovský	k2eAgMnSc1d1	liptovský
podžupan	podžupan	k1gMnSc1	podžupan
Ladislav	Ladislav	k1gMnSc1	Ladislav
Okolicsányi	Okolicsány	k1gFnSc2	Okolicsány
<g/>
,	,	kIx,	,
žalobcem	žalobce	k1gMnSc7	žalobce
byl	být	k5eAaImAgMnS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Čemický	Čemický	k2eAgMnSc1d1	Čemický
<g/>
.	.	kIx.	.
</s>
<s>
Jánošík	Jánošík	k1gMnSc1	Jánošík
byl	být	k5eAaImAgMnS	být
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
zástupci	zástupce	k1gMnPc1	zástupce
trenčínské	trenčínský	k2eAgFnSc2d1	Trenčínská
a	a	k8xC	a
liptovské	liptovský	k2eAgFnSc2d1	Liptovská
župy	župa	k1gFnSc2	župa
a	a	k8xC	a
konfrontován	konfrontován	k2eAgInSc1d1	konfrontován
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
okradených	okradený	k2eAgFnPc2d1	okradená
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
loupežných	loupežný	k2eAgNnPc2d1	loupežné
přepadení	přepadení	k1gNnPc2	přepadení
a	a	k8xC	a
spojení	spojení	k1gNnPc1	spojení
s	s	k7c7	s
kuruckými	kurucký	k2eAgMnPc7d1	kurucký
povstalci	povstalec	k1gMnPc7	povstalec
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
emigraci	emigrace	k1gFnSc6	emigrace
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
faráře	farář	k1gMnSc2	farář
Juraje	Juraj	k1gInSc2	Juraj
Vrtíka	Vrtík	k1gMnSc2	Vrtík
z	z	k7c2	z
Domaniže	Domaniž	k1gFnSc2	Domaniž
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
některým	některý	k3yIgInPc3	některý
bodům	bod	k1gInPc3	bod
obžaloby	obžaloba	k1gFnSc2	obžaloba
se	se	k3xPyFc4	se
Jánošík	Jánošík	k1gMnSc1	Jánošík
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
–	–	k?	–
jako	jako	k8xC	jako
vraždu	vražda	k1gFnSc4	vražda
domanižského	domanižský	k2eAgMnSc2d1	domanižský
kněze	kněz	k1gMnSc2	kněz
–	–	k?	–
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
se	se	k3xPyFc4	se
nevyjádřil	vyjádřit	k5eNaPmAgInS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
obhájce	obhájce	k1gMnSc1	obhájce
ex	ex	k6eAd1	ex
offo	offo	k6eAd1	offo
Baltazar	Baltazar	k1gMnSc1	Baltazar
Palugyai	Palugya	k1gFnSc2	Palugya
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
požadoval	požadovat	k5eAaImAgMnS	požadovat
milost	milost	k1gFnSc4	milost
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
byl	být	k5eAaImAgMnS	být
ke	k	k7c3	k
zboji	zboj	k1gInSc3	zboj
sveden	sveden	k2eAgInSc1d1	sveden
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
dokázána	dokázán	k2eAgFnSc1d1	dokázána
jediná	jediný	k2eAgFnSc1d1	jediná
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
přiznal	přiznat	k5eAaPmAgMnS	přiznat
a	a	k8xC	a
činů	čin	k1gInPc2	čin
lituje	litovat	k5eAaImIp3nS	litovat
<g/>
.	.	kIx.	.
</s>
<s>
Prokurátor	prokurátor	k1gMnSc1	prokurátor
však	však	k9	však
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c6	na
prokázaném	prokázaný	k2eAgNnSc6d1	prokázané
zbojnictví	zbojnictví	k1gNnSc6	zbojnictví
i	i	k8xC	i
vraždě	vražda	k1gFnSc6	vražda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obojí	oboj	k1gFnSc7	oboj
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zákonů	zákon	k1gInPc2	zákon
hrdelním	hrdelní	k2eAgInSc7d1	hrdelní
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
byl	být	k5eAaImAgMnS	být
podroben	podrobit	k5eAaPmNgMnS	podrobit
lehkému	lehký	k2eAgInSc3d1	lehký
i	i	k9	i
těžkému	těžký	k2eAgNnSc3d1	těžké
útrpnému	útrpný	k2eAgNnSc3d1	útrpné
právu	právo	k1gNnSc3	právo
a	a	k8xC	a
po	po	k7c6	po
zapsání	zapsání	k1gNnSc6	zapsání
dodatečných	dodatečný	k2eAgInPc2d1	dodatečný
přiznání	přiznání	k1gNnSc2	přiznání
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
zavěšením	zavěšení	k1gNnPc3	zavěšení
na	na	k7c4	na
hák	hák	k1gInSc4	hák
za	za	k7c4	za
levý	levý	k2eAgInSc4d1	levý
bok	bok	k1gInSc4	bok
<g/>
.	.	kIx.	.
</s>
<s>
Poneváč	Poneváč	k1gInSc1	Poneváč
predepsaný	predepsaný	k2eAgMnSc1d1	predepsaný
obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
Juro	Jura	k1gMnSc5	Jura
Janošak	Janošak	k1gInSc1	Janošak
<g/>
,	,	kIx,	,
zavrhnuvše	zavrhnout	k5eAaPmDgFnP	zavrhnout
prikázaní	prikázaný	k2eAgMnPc1d1	prikázaný
jak	jak	k8xS	jak
božské	božská	k1gFnPc1	božská
tak	tak	k9	tak
též	též	k9	též
i	i	k9	i
zákon	zákon	k1gInSc1	zákon
krajinský	krajinský	k2eAgInSc1d1	krajinský
<g/>
,	,	kIx,	,
predo	predo	k1gNnSc1	predo
dvema	dvemum	k1gNnSc2	dvemum
roky	rok	k1gInPc4	rok
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
zbojstvo	zbojstvo	k1gNnSc4	zbojstvo
a	a	k8xC	a
vudcem	vudec	k1gInSc7	vudec
aneb	aneb	k?	aneb
hajtmanem	hajtman	k1gMnSc7	hajtman
takovým	takový	k3xDgMnSc7	takový
se	se	k3xPyFc4	se
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
tovariši	tovariš	k1gInPc7	tovariš
svými	svůj	k3xOyFgMnPc7	svůj
na	na	k7c4	na
cestách	cesta	k1gFnPc6	cesta
zastavujíce	zastavovat	k5eAaImSgFnP	zastavovat
mnohých	mnohý	k2eAgMnPc2d1	mnohý
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
statek	statek	k1gInSc4	statek
<g/>
,	,	kIx,	,
áno	áno	k?	áno
i	i	k8xC	i
jako	jako	k9	jako
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
vlastného	vlastný	k2eAgNnSc2d1	vlastné
vyznania	vyznanium	k1gNnSc2	vyznanium
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tovariši	tovarisat	k5eAaPmIp1nSwK	tovarisat
kdy	kdy	k6eAd1	kdy
on	on	k3xPp3gMnSc1	on
též	též	k6eAd1	též
bol	bol	k1gInSc1	bol
prítomný	prítomný	k2eAgInSc1d1	prítomný
<g/>
,	,	kIx,	,
pána	pán	k1gMnSc2	pán
patera	pater	k1gMnSc2	pater
z	z	k7c2	z
Domaníže	Domaníž	k1gFnSc2	Domaníž
prostrelili	prostrelit	k5eAaPmAgMnP	prostrelit
a	a	k8xC	a
bezbožne	bezbožnout	k5eAaImIp3nS	bezbožnout
zamordovali	zamordovat	k5eAaPmAgMnP	zamordovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
též	též	k9	též
i	i	k9	i
jinších	jinší	k2eAgNnPc2d1	jinší
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jest	být	k5eAaImIp3nS	být
predepsané	predepsaný	k2eAgNnSc1d1	predepsaný
<g/>
,	,	kIx,	,
zlých	zlý	k2eAgNnPc2d1	zlé
skutkuv	skutkuvum	k1gNnPc2	skutkuvum
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgInS	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
pre	pre	k?	pre
takové	takový	k3xDgFnPc4	takový
veliké	veliký	k2eAgInPc1d1	veliký
zlé	zlý	k2eAgInPc1d1	zlý
účinky	účinek	k1gInPc1	účinek
a	a	k8xC	a
prikázaní	prikázaný	k2eAgMnPc1d1	prikázaný
prestúpení	prestúpený	k2eAgMnPc1d1	prestúpený
má	mít	k5eAaImIp3nS	mít
byť	byť	k8xS	byť
na	na	k7c4	na
hák	hák	k1gInSc4	hák
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
boku	bok	k1gInSc6	bok
prehnatý	prehnatý	k2eAgMnSc1d1	prehnatý
a	a	k8xC	a
tak	tak	k6eAd1	tak
na	na	k7c4	na
príklad	príklad	k1gInSc4	príklad
jinších	jinší	k2eAgFnPc2d1	jinší
takovich	takovicha	k1gFnPc2	takovicha
zločincov	zločincovo	k1gNnPc2	zločincovo
má	mít	k5eAaImIp3nS	mít
byť	byť	k8xS	byť
zavesený	zavesený	k2eAgMnSc1d1	zavesený
<g/>
.	.	kIx.	.
<g/>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozsudek	rozsudek	k1gInSc1	rozsudek
byl	být	k5eAaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
na	na	k7c6	na
městském	městský	k2eAgNnSc6d1	Městské
popravišti	popraviště	k1gNnSc6	popraviště
Šibeničky	šibenička	k1gFnSc2	šibenička
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
popravy	poprava	k1gFnSc2	poprava
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
považují	považovat	k5eAaImIp3nP	považovat
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
Melicherčík	Melicherčík	k1gMnSc1	Melicherčík
<g/>
,	,	kIx,	,
Kočiš	kočiš	k1gMnSc1	kočiš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprava	poprava	k1gFnSc1	poprava
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
podle	podle	k7c2	podle
dobových	dobový	k2eAgFnPc2d1	dobová
zvyklostí	zvyklost	k1gFnPc2	zvyklost
ještě	ještě	k9	ještě
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odsouzený	odsouzený	k1gMnSc1	odsouzený
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
těžkém	těžký	k2eAgNnSc6d1	těžké
mučení	mučení	k1gNnSc6	mučení
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
a	a	k8xC	a
neunikl	uniknout	k5eNaPmAgMnS	uniknout
tak	tak	k6eAd1	tak
spravedlivému	spravedlivý	k2eAgInSc3d1	spravedlivý
trestu	trest	k1gInSc3	trest
(	(	kIx(	(
<g/>
Sroka	Sroka	k1gFnSc1	Sroka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
polského	polský	k2eAgMnSc2d1	polský
historika	historik	k1gMnSc2	historik
Stanisława	Stanisławus	k1gMnSc2	Stanisławus
Dzięciołowského	Dzięciołowský	k1gMnSc2	Dzięciołowský
byl	být	k5eAaImAgMnS	být
Jánošík	Jánošík	k1gMnSc1	Jánošík
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
buď	buď	k8xC	buď
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
hřbitova	hřbitov	k1gInSc2	hřbitov
v	v	k7c6	v
Liptovském	liptovský	k2eAgInSc6d1	liptovský
Mikuláši	mikuláš	k1gInSc6	mikuláš
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
kuruckém	kurucký	k2eAgInSc6d1	kurucký
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
nedalekých	daleký	k2eNgInPc6d1	nedaleký
Čemicích	Čemik	k1gInPc6	Čemik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prameny	pramen	k1gInPc1	pramen
==	==	k?	==
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
jedinými	jediný	k2eAgInPc7d1	jediný
dochovanými	dochovaný	k2eAgInPc7d1	dochovaný
prameny	pramen	k1gInPc7	pramen
o	o	k7c6	o
životě	život	k1gInSc6	život
legendárního	legendární	k2eAgMnSc2d1	legendární
zbojníka	zbojník	k1gMnSc2	zbojník
jsou	být	k5eAaImIp3nP	být
protokoly	protokol	k1gInPc1	protokol
ze	z	k7c2	z
soudních	soudní	k2eAgNnPc2d1	soudní
řízení	řízení	k1gNnPc2	řízení
s	s	k7c7	s
Jurajem	Juraj	k1gMnSc7	Juraj
Jánošíkem	Jánošík	k1gMnSc7	Jánošík
a	a	k8xC	a
Tomášem	Tomáš	k1gMnSc7	Tomáš
Uhorčíkem	Uhorčík	k1gMnSc7	Uhorčík
<g/>
,	,	kIx,	,
matrika	matrika	k1gFnSc1	matrika
křtů	křest	k1gInPc2	křest
z	z	k7c2	z
farnosti	farnost	k1gFnSc2	farnost
ve	v	k7c6	v
Varíně	Varín	k1gInSc6	Varín
<g/>
,	,	kIx,	,
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
propuštění	propuštění	k1gNnSc4	propuštění
z	z	k7c2	z
císařské	císařský	k2eAgFnSc2d1	císařská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
doplňujících	doplňující	k2eAgInPc2d1	doplňující
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
lze	lze	k6eAd1	lze
poskládat	poskládat	k5eAaPmF	poskládat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc1d1	omezený
obrázek	obrázek	k1gInSc1	obrázek
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
působení	působení	k1gNnSc6	působení
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obraz	obraz	k1gInSc1	obraz
národního	národní	k2eAgMnSc2d1	národní
hrdiny	hrdina	k1gMnSc2	hrdina
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
budován	budovat	k5eAaImNgInS	budovat
teprve	teprve	k6eAd1	teprve
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
obrozenci	obrozenec	k1gMnPc7	obrozenec
a	a	k8xC	a
během	běh	k1gInSc7	běh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
utvrzován	utvrzovat	k5eAaImNgInS	utvrzovat
socialistickými	socialistický	k2eAgMnPc7d1	socialistický
historiky	historik	k1gMnPc7	historik
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
často	často	k6eAd1	často
nekritickému	kritický	k2eNgNnSc3d1	nekritické
přejímání	přejímání	k1gNnSc3	přejímání
konstruktů	konstrukt	k1gInPc2	konstrukt
a	a	k8xC	a
tezí	teze	k1gFnPc2	teze
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
často	často	k6eAd1	často
ignorovaly	ignorovat	k5eAaImAgFnP	ignorovat
dobový	dobový	k2eAgInSc4d1	dobový
kontext	kontext	k1gInSc4	kontext
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nezakrytě	zakrytě	k6eNd1	zakrytě
sloužily	sloužit	k5eAaImAgFnP	sloužit
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
ideologií	ideologie	k1gFnPc2	ideologie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Juraje	Juraj	k1gInSc2	Juraj
Jánošíka	Jánošík	k1gMnSc2	Jánošík
spojeno	spojit	k5eAaPmNgNnS	spojit
poměrně	poměrně	k6eAd1	poměrně
mnoho	mnoho	k4c1	mnoho
domněnek	domněnka	k1gFnPc2	domněnka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
pramenných	pramenný	k2eAgInPc6d1	pramenný
materiálech	materiál	k1gInPc6	materiál
žádnou	žádný	k3yNgFnSc4	žádný
oporu	opora	k1gFnSc4	opora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
slovenského	slovenský	k2eAgNnSc2d1	slovenské
lidového	lidový	k2eAgNnSc2d1	lidové
podání	podání	k1gNnSc2	podání
byl	být	k5eAaImAgInS	být
Juro	Jura	k1gMnSc5	Jura
Jánošík	Jánošík	k1gMnSc1	Jánošík
obdařen	obdařit	k5eAaPmNgInS	obdařit
magickými	magický	k2eAgInPc7d1	magický
předměty	předmět	k1gInPc7	předmět
(	(	kIx(	(
<g/>
kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
valaška	valaška	k1gFnSc1	valaška
<g/>
,	,	kIx,	,
čarovný	čarovný	k2eAgInSc1d1	čarovný
opasek	opasek	k1gInSc1	opasek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
dodávaly	dodávat	k5eAaImAgFnP	dodávat
nadpřirozené	nadpřirozený	k2eAgFnPc1d1	nadpřirozená
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Okrádal	okrádat	k5eAaImAgMnS	okrádat
především	především	k9	především
šlechtice	šlechtic	k1gMnSc4	šlechtic
<g/>
,	,	kIx,	,
trestal	trestat	k5eAaImAgMnS	trestat
panské	panský	k2eAgMnPc4d1	panský
dráby	dráb	k1gMnPc4	dráb
a	a	k8xC	a
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
lupu	lup	k1gInSc2	lup
vyděloval	vydělovat	k5eAaImAgMnS	vydělovat
část	část	k1gFnSc4	část
pro	pro	k7c4	pro
chudé	chudý	k2eAgNnSc4d1	chudé
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bohatým	bohatý	k2eAgMnSc7d1	bohatý
bral	brát	k5eAaImAgMnS	brát
a	a	k8xC	a
chudým	chudý	k2eAgMnPc3d1	chudý
dával	dávat	k5eAaImAgMnS	dávat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
vrchnosti	vrchnost	k1gFnSc3	vrchnost
byl	být	k5eAaImAgInS	být
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
obrozenci	obrozenec	k1gMnPc1	obrozenec
představován	představován	k2eAgMnSc1d1	představován
jako	jako	k8xS	jako
romantický	romantický	k2eAgMnSc1d1	romantický
hrdina	hrdina	k1gMnSc1	hrdina
bojující	bojující	k2eAgInSc4d1	bojující
za	za	k7c4	za
slovenský	slovenský	k2eAgInSc4d1	slovenský
národ	národ	k1gInSc4	národ
proti	proti	k7c3	proti
maďarskému	maďarský	k2eAgInSc3d1	maďarský
útlaku	útlak	k1gInSc3	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Socialističtí	socialistický	k2eAgMnPc1d1	socialistický
badatelé	badatel	k1gMnPc1	badatel
jej	on	k3xPp3gMnSc4	on
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k8xC	jako
bojovníka	bojovník	k1gMnSc2	bojovník
proti	proti	k7c3	proti
feudálnímu	feudální	k2eAgInSc3d1	feudální
útlaku	útlak	k1gInSc3	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podhalských	podhalský	k2eAgFnPc2d1	podhalský
pověstí	pověst	k1gFnPc2	pověst
působil	působit	k5eAaImAgMnS	působit
legendární	legendární	k2eAgMnSc1d1	legendární
zbojník	zbojník	k1gMnSc1	zbojník
i	i	k9	i
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc4	jeho
popularitu	popularita	k1gFnSc4	popularita
značně	značně	k6eAd1	značně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
film	film	k1gInSc1	film
Janosik	Janosika	k1gFnPc2	Janosika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Jerzy	Jerza	k1gFnSc2	Jerza
Passendorfer	Passendorfer	k1gMnSc1	Passendorfer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
Emil	Emil	k1gMnSc1	Emil
Horský	Horský	k1gMnSc1	Horský
<g/>
.	.	kIx.	.
</s>
<s>
Zbojství	zbojství	k1gNnSc1	zbojství
Jura	Jura	k1gMnSc1	Jura
Janošíka	Janošík	k1gMnSc2	Janošík
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
výslech	výslech	k1gInSc1	výslech
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc1	odsouzení
1713	[number]	k4	1713
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
lid	lid	k1gInSc1	lid
<g/>
.	.	kIx.	.
</s>
<s>
XXI	XXI	kA	XXI
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
1911	[number]	k4	1911
Janošík	Janošík	k1gMnSc1	Janošík
a	a	k8xC	a
zbojníci	zbojník	k1gMnPc1	zbojník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vyobrazenim	vyobrazeni	k1gNnSc7	vyobrazeni
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Česky	česky	k6eAd1	česky
lid	lid	k1gInSc1	lid
<g/>
"	"	kIx"	"
XX	XX	kA	XX
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
Miloš	Miloš	k1gMnSc1	Miloš
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
národní	národní	k2eAgFnSc2d1	národní
knihy	kniha	k1gFnSc2	kniha
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Václav	Václav	k1gMnSc1	Václav
Cibula	Cibulum	k1gNnSc2	Cibulum
–	–	k?	–
Jánošík	Jánošík	k1gMnSc1	Jánošík
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
<g/>
náklad	náklad	k1gInSc1	náklad
35	[number]	k4	35
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Neznámy	neznámo	k1gNnPc7	neznámo
Jánošík	Jánošík	k1gMnSc1	Jánošík
(	(	kIx(	(
<g/>
Jozef	Jozef	k1gMnSc1	Jozef
Kočiš	kočiš	k1gMnSc1	kočiš
<g/>
)	)	kIx)	)
–	–	k?	–
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Osveta	Osveta	k1gFnSc1	Osveta
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Arnošt	Arnošt	k1gMnSc1	Arnošt
Caha	Caha	k1gMnSc1	Caha
–	–	k?	–
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
zbojnickém	zbojnický	k2eAgMnSc6d1	zbojnický
hrdinovi	hrdina	k1gMnSc6	hrdina
</s>
</p>
<p>
<s>
MAHEN	MAHEN	kA	MAHEN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jánošík	Jánošík	k1gMnSc1	Jánošík
:	:	kIx,	:
tragedie	tragedie	k1gFnSc1	tragedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
dějstvích	dějství	k1gNnPc6	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
:	:	kIx,	:
Plaček	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
Mahenovy	Mahenův	k2eAgFnSc2d1	Mahenova
tragédie	tragédie	k1gFnSc2	tragédie
o	o	k7c6	o
Jánošíkovi	Jánošík	k1gMnSc6	Jánošík
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
letech	let	k1gInPc6	let
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1713	[number]	k4	1713
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
JIRÁSEK	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jos	Jos	k1gFnSc1	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
375	[number]	k4	375
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
O	o	k7c6	o
Janošíkovi	Janošík	k1gMnSc6	Janošík
<g/>
,	,	kIx,	,
s.	s.	k?	s.
329	[number]	k4	329
<g/>
-	-	kIx~	-
<g/>
341	[number]	k4	341
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KENDROVÁ	KENDROVÁ	kA	KENDROVÁ
<g/>
,	,	kIx,	,
Katarína	Katarína	k1gFnSc1	Katarína
<g/>
:	:	kIx,	:
Jánošíkovský	jánošíkovský	k2eAgInSc1d1	jánošíkovský
motív	motív	k1gInSc1	motív
v	v	k7c6	v
drevorezbe	drevorezbat	k5eAaPmIp3nS	drevorezbat
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MÚZA	Múza	k1gFnSc1	Múza
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
na	na	k7c6	na
<g/>
:	:	kIx,	:
http://www.floowie.com/sk/citaj/muza/#/strana/22/zvacsenie/100/	[url]	k4	http://www.floowie.com/sk/citaj/muza/#/strana/22/zvacsenie/100/
</s>
</p>
<p>
<s>
ZACHAROVÁ	ZACHAROVÁ	kA	ZACHAROVÁ
<g/>
,	,	kIx,	,
Katarína	Katarína	k1gFnSc1	Katarína
<g/>
:	:	kIx,	:
Odkliaty	Odkliata	k1gFnSc2	Odkliata
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MÚZA	Múza	k1gFnSc1	Múza
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
na	na	k7c6	na
<g/>
:	:	kIx,	:
http://www.floowie.com/sk/citaj/muza-2013/#/strana/2/zvacsenie/100/	[url]	k4	http://www.floowie.com/sk/citaj/muza-2013/#/strana/2/zvacsenie/100/
</s>
</p>
<p>
<s>
===	===	k?	===
Opera	opera	k1gFnSc1	opera
===	===	k?	===
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
Juro	Jura	k1gMnSc5	Jura
Jánošík	Jánošík	k1gMnSc1	Jánošík
–	–	k?	–
opera	opera	k1gFnSc1	opera
Jána	Ján	k1gMnSc4	Ján
Cikkera	Cikker	k1gMnSc4	Cikker
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
Jánošík	Jánošík	k1gMnSc1	Jánošík
–	–	k?	–
první	první	k4xOgMnSc1	první
slovenský	slovenský	k2eAgInSc1d1	slovenský
dlouhometrážní	dlouhometrážní	k2eAgInSc1d1	dlouhometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc1d1	americké
financování	financování	k1gNnSc1	financování
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jerry	Jerra	k1gFnSc2	Jerra
Siakeľ	Siakeľ	k1gMnSc1	Siakeľ
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
Jánošík	Jánošík	k1gMnSc1	Jánošík
–	–	k?	–
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
slovenský	slovenský	k2eAgInSc1d1	slovenský
film	film	k1gInSc1	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
Paľo	Paľo	k1gMnSc1	Paľo
Bielik	Bielik	k1gMnSc1	Bielik
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
Janosik	Janosik	k1gMnSc1	Janosik
–	–	k?	–
první	první	k4xOgMnSc1	první
polský	polský	k2eAgInSc1d1	polský
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Włodzimierz	Włodzimierz	k1gMnSc1	Włodzimierz
Haupt	Haupt	k1gMnSc1	Haupt
a	a	k8xC	a
Halina	Halina	k1gFnSc1	Halina
Bieliński	Bielińsk	k1gFnSc2	Bielińsk
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Jánošík	Jánošík	k1gMnSc1	Jánošík
I	i	k9	i
<g/>
–	–	k?	–
<g/>
II	II	kA	II
–	–	k?	–
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
slovenský	slovenský	k2eAgInSc4d1	slovenský
film	film	k1gInSc4	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Paľo	Paľo	k1gMnSc1	Paľo
Bielik	Bielik	k1gMnSc1	Bielik
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Kuchta	Kuchta	k1gMnSc1	Kuchta
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Janosik	Janosik	k1gInSc1	Janosik
–	–	k?	–
polský	polský	k2eAgInSc1d1	polský
film	film	k1gInSc1	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jerzy	Jerza	k1gFnPc1	Jerza
Passendorfer	Passendorfero	k1gNnPc2	Passendorfero
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Perepeczko	Perepeczka	k1gFnSc5	Perepeczka
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Janosik	Janosik	k1gInSc1	Janosik
–	–	k?	–
polský	polský	k2eAgInSc1d1	polský
13	[number]	k4	13
<g/>
dílný	dílný	k2eAgInSc1d1	dílný
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jerzy	Jerza	k1gFnPc1	Jerza
Passendorfer	Passendorfero	k1gNnPc2	Passendorfero
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Perepeczko	Perepeczka	k1gFnSc5	Perepeczka
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
Zbojník	zbojník	k1gMnSc1	zbojník
Jurko	Jurko	k1gMnSc1	Jurko
–	–	k?	–
slovenský	slovenský	k2eAgInSc1d1	slovenský
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Viktor	Viktor	k1gMnSc1	Viktor
Kubal	Kubal	k1gMnSc1	Kubal
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Juro	Jura	k1gMnSc5	Jura
Jánošík	Jánošík	k1gMnSc1	Jánošík
–	–	k?	–
slovenský	slovenský	k2eAgInSc1d1	slovenský
TV	TV	kA	TV
film	film	k1gInSc1	film
–	–	k?	–
inscenace	inscenace	k1gFnSc2	inscenace
Cikkerovy	Cikkerův	k2eAgFnSc2d1	Cikkerův
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
opery	opera	k1gFnSc2	opera
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jozef	Jozef	k1gMnSc1	Jozef
Zachar	Zachar	k1gMnSc1	Zachar
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Zbojník	zbojník	k1gMnSc1	zbojník
Jurošík	Jurošík	k1gMnSc1	Jurošík
–	–	k?	–
slovenský	slovenský	k2eAgInSc1d1	slovenský
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
dílný	dílný	k2eAgInSc1d1	dílný
animovaný	animovaný	k2eAgInSc1d1	animovaný
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Beran	Beran	k1gMnSc1	Beran
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Jánošík	Jánošík	k1gMnSc1	Jánošík
–	–	k?	–
Pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
historie	historie	k1gFnSc1	historie
–	–	k?	–
Slovensko-polsko-český	Slovenskoolsko-český	k2eAgInSc1d1	Slovensko-polsko-český
film	film	k1gInSc1	film
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Holland	Hollanda	k1gFnPc2	Hollanda
a	a	k8xC	a
Kasia	Kasium	k1gNnSc2	Kasium
Adamik	Adamik	k1gMnSc1	Adamik
<g/>
,	,	kIx,	,
Jánošík	Jánošík	k1gMnSc1	Jánošík
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Jiráček	Jiráček	k1gMnSc1	Jiráček
</s>
</p>
<p>
<s>
===	===	k?	===
Numismatický	numismatický	k2eAgInSc4d1	numismatický
námět	námět	k1gInSc4	námět
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
dvoukoruna	dvoukoruna	k1gFnSc1	dvoukoruna
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Jánošíka	Jánošík	k1gMnSc2	Jánošík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ondráš	Ondrat	k5eAaImIp2nS	Ondrat
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Uhorčík	Uhorčík	k1gMnSc1	Uhorčík
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc4d1	Česká
<g/>
/	/	kIx~	/
<g/>
O	o	k7c6	o
Janošíkovi	Janošík	k1gMnSc6	Janošík
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Povážského	povážský	k2eAgNnSc2d1	Povážské
musea	museum	k1gNnSc2	museum
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Janošíkovi	Janošíkův	k2eAgMnPc1d1	Janošíkův
</s>
</p>
<p>
<s>
Jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
o	o	k7c6	o
Jánošíkovi	Jánošík	k1gMnSc6	Jánošík
a	a	k8xC	a
Ilčíkovi	Ilčík	k1gMnSc6	Ilčík
?	?	kIx.	?
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
o	o	k7c6	o
filmech	film	k1gInPc6	film
o	o	k7c6	o
Jánošíkovi	Jánošík	k1gMnSc6	Jánošík
</s>
</p>
