<s>
Make	Make	k1gInSc1	Make
a	a	k8xC	a
Jazz	jazz	k1gInSc1	jazz
Noise	Nois	k1gMnSc2	Nois
Here	Her	k1gMnSc2	Her
je	být	k5eAaImIp3nS	být
živé	živý	k2eAgNnSc1d1	živé
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
většinou	většinou	k6eAd1	většinou
napsal	napsat	k5eAaBmAgMnS	napsat
sám	sám	k3xTgMnSc1	sám
Zappa	Zapp	k1gMnSc2	Zapp
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
skladbu	skladba	k1gFnSc4	skladba
napsal	napsat	k5eAaBmAgMnS	napsat
Igor	Igor	k1gMnSc1	Igor
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
Béla	Béla	k1gMnSc1	Béla
Bartók	Bartók	k1gMnSc1	Bartók
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
napsal	napsat	k5eAaBmAgMnS	napsat
Frank	Frank	k1gMnSc1	Frank
Zappa	Zapp	k1gMnSc2	Zapp
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Stinkfoot	Stinkfoot	k1gInSc1	Stinkfoot
<g/>
"	"	kIx"	"
–	–	k?	–
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
"	"	kIx"	"
<g/>
When	When	k1gMnSc1	When
Yuppies	Yuppies	k1gMnSc1	Yuppies
Go	Go	k1gMnSc1	Go
to	ten	k3xDgNnSc4	ten
Hell	Hell	k1gMnSc1	Hell
<g/>
"	"	kIx"	"
–	–	k?	–
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
"	"	kIx"	"
<g/>
Fire	Fir	k1gInSc2	Fir
and	and	k?	and
Chains	Chains	k1gInSc1	Chains
<g/>
"	"	kIx"	"
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
"	"	kIx"	"
<g/>
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Make	Make	k1gFnSc7	Make
the	the	k?	the
Water	Water	k1gMnSc1	Water
Turn	Turn	k1gMnSc1	Turn
Black	Black	k1gMnSc1	Black
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
"	"	kIx"	"
<g/>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
You	You	k1gFnPc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
a	a	k8xC	a
Beast	Beast	k1gFnSc1	Beast
<g/>
"	"	kIx"	"
–	–	k?	–
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
Orange	Orang	k1gFnSc2	Orang
County	Counta	k1gFnSc2	Counta
Lumber	Lumber	k1gMnSc1	Lumber
<g />
.	.	kIx.	.
</s>
<s>
Truck	truck	k1gInSc1	truck
<g/>
"	"	kIx"	"
–	–	k?	–
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
"	"	kIx"	"
<g/>
Oh	oh	k0	oh
No	no	k9	no
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
"	"	kIx"	"
<g/>
Theme	Them	k1gInSc5	Them
from	from	k1gInSc1	from
Lumpy	lump	k1gMnPc7	lump
Gravy	Grava	k1gFnSc2	Grava
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
"	"	kIx"	"
<g/>
Eat	Eat	k1gFnSc7	Eat
That	That	k2eAgInSc4d1	That
Question	Question	k1gInSc4	Question
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Black	Black	k1gInSc1	Black
Napkins	Napkins	k1gInSc1	Napkins
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc2	Big
Swifty	Swifta	k1gFnSc2	Swifta
<g/>
"	"	kIx"	"
–	–	k?	–
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
"	"	kIx"	"
<g/>
King	King	k1gInSc1	King
Kong	Kongo	k1gNnPc2	Kongo
<g/>
"	"	kIx"	"
–	–	k?	–
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
"	"	kIx"	"
<g/>
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Work	Work	k1gInSc1	Work
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Black	Black	k1gInSc1	Black
Page	Page	k1gFnSc1	Page
(	(	kIx(	(
<g/>
new	new	k?	new
age	age	k?	age
version	version	k1gInSc1	version
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
"	"	kIx"	"
<g/>
T	T	kA	T
<g/>
'	'	kIx"	'
<g/>
Mershi	Mershi	k1gNnPc1	Mershi
Duween	Duwena	k1gFnPc2	Duwena
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
"	"	kIx"	"
<g/>
Dupree	Dupre	k1gInSc2	Dupre
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Paradise	Paradise	k1gFnSc1	Paradise
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
"	"	kIx"	"
<g/>
City	city	k1gNnSc1	city
of	of	k?	of
Tiny	Tina	k1gFnSc2	Tina
Lights	Lightsa	k1gFnPc2	Lightsa
<g/>
"	"	kIx"	"
–	–	k?	–
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
"	"	kIx"	"
<g/>
Royal	Royal	k1gMnSc1	Royal
March	March	k1gMnSc1	March
from	from	k1gMnSc1	from
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Histoire	Histoir	k1gInSc5	Histoir
du	du	k?	du
Soldat	Soldat	k1gMnPc7	Soldat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Igor	Igor	k1gMnSc1	Igor
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
)	)	kIx)	)
–	–	k?	–
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
59	[number]	k4	59
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Theme	Them	k1gMnSc5	Them
from	from	k1gMnSc1	from
the	the	k?	the
Bartok	Bartok	k1gInSc1	Bartok
Piano	piano	k1gNnSc1	piano
Concerto	Concerta	k1gFnSc5	Concerta
#	#	kIx~	#
<g/>
3	[number]	k4	3
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Béla	Béla	k1gMnSc1	Béla
Bartók	Bartók	k1gMnSc1	Bartók
<g/>
)	)	kIx)	)
–	–	k?	–
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
"	"	kIx"	"
<g/>
Sinister	Sinister	k1gInSc1	Sinister
Footwear	Footwear	k1gInSc1	Footwear
2	[number]	k4	2
<g/>
nd	nd	k?	nd
mvt	mvt	k?	mvt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
"	"	kIx"	"
<g/>
Stevie	Stevie	k1gFnSc2	Stevie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Spanking	Spanking	k1gInSc1	Spanking
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
"	"	kIx"	"
<g/>
Alien	Alien	k1gInSc1	Alien
Orifice	Orifice	k1gFnSc2	Orifice
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
"	"	kIx"	"
<g/>
Cruisin	Cruisin	k1gInSc1	Cruisin
<g/>
'	'	kIx"	'
for	forum	k1gNnPc2	forum
Burgers	Burgersa	k1gFnPc2	Burgersa
<g/>
"	"	kIx"	"
–	–	k?	–
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
"	"	kIx"	"
<g/>
Advance	Advanec	k1gInSc2	Advanec
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
–	–	k?	–
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
"	"	kIx"	"
<g/>
Strictly	Strictly	k1gFnSc1	Strictly
<g />
.	.	kIx.	.
</s>
<s>
Genteel	Genteel	k1gInSc1	Genteel
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
Frank	Frank	k1gMnSc1	Frank
Zappa	Zappa	k1gFnSc1	Zappa
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
Paul	Paul	k1gMnSc1	Paul
Carman	Carman	k1gMnSc1	Carman
–	–	k?	–
alt	alt	k1gInSc1	alt
saxofon	saxofon	k1gInSc1	saxofon
Kurt	kurt	k1gInSc1	kurt
McGettrick	McGettrick	k1gInSc1	McGettrick
–	–	k?	–
bariton	bariton	k1gInSc1	bariton
saxofon	saxofon	k1gInSc1	saxofon
Scott	Scott	k2eAgInSc1d1	Scott
Thunes	Thunes	k1gInSc1	Thunes
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
Albert	Albert	k1gMnSc1	Albert
Wing	Wing	k1gMnSc1	Wing
–	–	k?	–
tenor	tenor	k1gInSc1	tenor
saxofon	saxofon	k1gInSc1	saxofon
Ed	Ed	k1gFnSc1	Ed
Mann	Mann	k1gMnSc1	Mann
–	–	k?	–
perkuse	perkuse	k1gFnSc2	perkuse
Chad	Chad	k1gMnSc1	Chad
Wackerman	Wackerman	k1gMnSc1	Wackerman
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
Ike	Ike	k1gMnSc1	Ike
Willis	Willis	k1gFnSc2	Willis
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Walt	Walt	k1gMnSc1	Walt
Fowler	Fowler	k1gMnSc1	Fowler
–	–	k?	–
trubka	trubka	k1gFnSc1	trubka
Mike	Mik	k1gInSc2	Mik
Keneally	Kenealla	k1gFnSc2	Kenealla
–	–	k?	–
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Bruce	Bruce	k1gMnSc1	Bruce
Fowler	Fowler	k1gMnSc1	Fowler
–	–	k?	–
pozoun	pozoun	k1gInSc1	pozoun
Robert	Robert	k1gMnSc1	Robert
Martin	Martin	k1gMnSc1	Martin
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
Informace	informace	k1gFnSc2	informace
</s>
