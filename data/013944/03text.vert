<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
Petra	Petr	k1gMnSc2
Čajánka	Čajánek	k1gMnSc2
z	z	k7c2
MS	MS	kA
2001	#num#	k4
<g/>
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
je	být	k5eAaImIp3nS
soutěž	soutěž	k1gFnSc1
mužských	mužský	k2eAgNnPc2d1
reprezentačních	reprezentační	k2eAgNnPc2d1
mužstev	mužstvo	k1gNnPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
federace	federace	k1gFnSc2
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
IIHF	IIHF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
her	hra	k1gFnPc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympiády	olympiáda	k1gFnSc2
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
však	však	k9
bylo	být	k5eAaImAgNnS
uznáno	uznat	k5eAaPmNgNnS
až	až	k9
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
bylo	být	k5eAaImAgNnS
MS	MS	kA
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
součástí	součást	k1gFnPc2
Zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
odděleně	odděleně	k6eAd1
od	od	k7c2
ZOH	ZOH	kA
<g/>
,	,	kIx,
kromě	kromě	k7c2
let	léto	k1gNnPc2
1980	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
a	a	k8xC
1988	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
MS	MS	kA
v	v	k7c6
roce	rok	k1gInSc6
ZOH	ZOH	kA
nekonalo	konat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
1929	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
rozhodla	rozhodnout	k5eAaPmAgFnS
IIHF	IIHF	kA
pořádat	pořádat	k5eAaImF
MS	MS	kA
od	od	k7c2
1930	#num#	k4
každoročně	každoročně	k6eAd1
společně	společně	k6eAd1
s	s	k7c7
ME	ME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
ročnících	ročník	k1gInPc6
MS	MS	kA
startovaly	startovat	k5eAaBmAgInP
různé	různý	k2eAgInPc1d1
počty	počet	k1gInPc1
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
i	i	k9
hrací	hrací	k2eAgInPc1d1
systémy	systém	k1gInPc1
byly	být	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
<g/>
,	,	kIx,
hrálo	hrát	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
či	či	k8xC
více	hodně	k6eAd2
skupinách	skupina	k1gFnPc6
<g/>
,	,	kIx,
podle	podle	k7c2
počtu	počet	k1gInSc2
přihlášených	přihlášený	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
byli	být	k5eAaImAgMnP
poprvé	poprvé	k6eAd1
účastníci	účastník	k1gMnPc1
rozděleni	rozdělit	k5eAaPmNgMnP
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
podle	podle	k7c2
výkonnosti	výkonnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1961	#num#	k4
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
žebříček	žebříček	k1gInSc1
výkonnosti	výkonnost	k1gFnSc2
mužstev	mužstvo	k1gNnPc2
a	a	k8xC
hrálo	hrát	k5eAaImAgNnS
se	se	k3xPyFc4
ve	v	k7c6
třech	tři	k4xCgFnPc6
skupinách	skupina	k1gFnPc6
–	–	k?
A	A	kA
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
C	C	kA
–	–	k?
s	s	k7c7
přímým	přímý	k2eAgInSc7d1
postupem	postup	k1gInSc7
a	a	k8xC
sestupem	sestup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
skupiny	skupina	k1gFnPc4
A	a	k9
získává	získávat	k5eAaImIp3nS
titul	titul	k1gInSc1
mistrů	mistr	k1gMnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
účastníků	účastník	k1gMnPc2
skupiny	skupina	k1gFnSc2
A	a	k9
byl	být	k5eAaImAgInS
pevně	pevně	k6eAd1
stanoven	stanovit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1961	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
8	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
6	#num#	k4
mužstev	mužstvo	k1gNnPc2
(	(	kIx(
<g/>
hrálo	hrát	k5eAaImAgNnS
se	se	k3xPyFc4
dvoukolově	dvoukolově	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
znovu	znovu	k6eAd1
8	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
12	#num#	k4
mužstev	mužstvo	k1gNnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
bylo	být	k5eAaImAgNnS
zatím	zatím	k6eAd1
poslední	poslední	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
na	na	k7c4
16	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
MS	MS	kA
1992	#num#	k4
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
hrací	hrací	k2eAgInSc1d1
model	model	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mužstva	mužstvo	k1gNnSc2
po	po	k7c6
zápasech	zápas	k1gInPc6
v	v	k7c6
základních	základní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
vyřazovacích	vyřazovací	k2eAgInPc2d1
bojů	boj	k1gInPc2
play-off	play-off	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
MS	MS	kA
1997	#num#	k4
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
hrací	hrací	k2eAgInSc1d1
systém	systém	k1gInSc1
s	s	k7c7
menšími	malý	k2eAgFnPc7d2
změnami	změna	k1gFnPc7
používaný	používaný	k2eAgInSc1d1
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2001	#num#	k4
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
struktura	struktura	k1gFnSc1
nižších	nízký	k2eAgFnPc2d2
výkonnostních	výkonnostní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
B	B	kA
a	a	k8xC
C	C	kA
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
na	na	k7c4
divizi	divize	k1gFnSc4
I.	I.	kA
a	a	k8xC
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
každá	každý	k3xTgFnSc1
divize	divize	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
po	po	k7c6
šesti	šest	k4xCc6
účastnících	účastnící	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
přibyla	přibýt	k5eAaPmAgFnS
ještě	ještě	k9
divize	divize	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
další	další	k2eAgFnSc3d1
důležité	důležitý	k2eAgFnSc3d1
změně	změna	k1gFnSc3
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elitní	elitní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
už	už	k6eAd1
není	být	k5eNaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
4	#num#	k4
skupin	skupina	k1gFnPc2
po	po	k7c4
4	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
do	do	k7c2
2	#num#	k4
skupin	skupina	k1gFnPc2
po	po	k7c4
8	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
4	#num#	k4
nejlepší	dobrý	k2eAgFnSc7d3
postupují	postupovat	k5eAaImIp3nP
přímo	přímo	k6eAd1
do	do	k7c2
play-off	play-off	k1gMnSc1
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
z	z	k7c2
každé	každý	k3xTgFnSc2
skupiny	skupina	k1gFnSc2
přímo	přímo	k6eAd1
sestupuje	sestupovat	k5eAaImIp3nS
do	do	k7c2
Divize	divize	k1gFnSc2
I.	I.	kA
Tato	tento	k3xDgFnSc1
změna	změna	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
podle	podle	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
upevnit	upevnit	k5eAaPmF
pozici	pozice	k1gFnSc4
silných	silný	k2eAgInPc2d1
týmů	tým	k1gInPc2
a	a	k8xC
zmenšit	zmenšit	k5eAaPmF
význam	význam	k1gInSc4
překvapivých	překvapivý	k2eAgFnPc2d1
výher	výhra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
také	také	k9
v	v	k7c6
počtu	počet	k1gInSc6
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
šesti	šest	k4xCc2
zápasů	zápas	k1gInPc2
se	se	k3xPyFc4
nyní	nyní	k6eAd1
hraje	hrát	k5eAaImIp3nS
sedm	sedm	k4xCc4
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
až	až	k9
16	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
vyřazení	vyřazení	k1gNnSc1
ve	v	k7c6
skupinách	skupina	k1gFnPc6
<g/>
,	,	kIx,
sestup	sestup	k1gInSc1
do	do	k7c2
Divize	divize	k1gFnSc2
I.	I.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
místo	místo	k7c2
sedmi	sedm	k4xCc2
zápasů	zápas	k1gInPc2
osm	osm	k4xCc4
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
vyřazení	vyřazení	k1gNnSc1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
místo	místo	k7c2
devíti	devět	k4xCc2
zápasů	zápas	k1gInPc2
deset	deset	k4xCc4
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
až	až	k9
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
zápas	zápas	k1gInSc1
o	o	k7c4
medaile	medaile	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
už	už	k9
skupiny	skupina	k1gFnPc1
A	A	kA
a	a	k8xC
B	B	kA
u	u	k7c2
Divize	divize	k1gFnSc2
I	I	kA
a	a	k8xC
II	II	kA
nejsou	být	k5eNaImIp3nP
rovnocenné	rovnocenný	k2eAgInPc1d1
<g/>
,	,	kIx,
nejlepších	dobrý	k2eAgInPc2d3
6	#num#	k4
týmů	tým	k1gInPc2
Divize	divize	k1gFnSc1
I	i	k9
hraje	hrát	k5eAaImIp3nS
ve	v	k7c6
skupině	skupina	k1gFnSc6
A	a	k9
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
první	první	k4xOgFnSc2
2	#num#	k4
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
Elitní	elitní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
sestupuje	sestupovat	k5eAaImIp3nS
do	do	k7c2
skupiny	skupina	k1gFnSc2
B.	B.	kA
</s>
<s>
Historicky	historicky	k6eAd1
největší	veliký	k2eAgFnSc1d3
návštěva	návštěva	k1gFnSc1
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
byla	být	k5eAaImAgFnS
na	na	k7c6
turnaji	turnaj	k1gInSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
návštěvnost	návštěvnost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
hodnoty	hodnota	k1gFnPc4
741	#num#	k4
690	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
11	#num#	k4
589	#num#	k4
diváků	divák	k1gMnPc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
se	se	k3xPyFc4
pořádá	pořádat	k5eAaImIp3nS
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
RokZlatoStříbroBronzMísto	RokZlatoStříbroBronzMísta	k1gFnSc5
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1920	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Winnipeg	Winnipeg	k1gInSc1
Falcons	Falcons	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Antverpy	Antverpy	k1gFnPc1
(	(	kIx(
<g/>
Belgie	Belgie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
LOH	LOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Toronto	Toronto	k1gNnSc1
Granites	Granitesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Chamonix	Chamonix	k1gNnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Toronto	Toronto	k1gNnSc1
Varsity	Varsit	k1gInPc4
Grads	Grads	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Mořic	Mořic	k1gMnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Toronto	Toronto	k1gNnSc1
CCMs	CCMsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Chamonix	Chamonix	k1gNnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Berlín	Berlín	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Manitoba	Manitoba	k1gFnSc1
Grads	Gradsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Krynica	Krynic	k2eAgFnSc1d1
Zdrój	Zdrój	k1gFnSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Winnipeg	Winnipeg	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Lake	Lake	k1gInSc1
Placid	Placid	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
</s>
<s>
USA	USA	kA
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
Rangers	Rangersa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Toronto	Toronto	k1gNnSc1
National	National	k1gFnPc1
Sea	Sea	k1gMnSc1
Fleas	Fleas	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Saskatoon	Saskatoon	k1gNnSc1
Quakers	Quakersa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Winnipeg	Winnipeg	k1gInSc1
Monarchs	Monarchs	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Davos	Davos	k1gInSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Port	port	k1gInSc1
Arthur	Arthura	k1gFnPc2
Bearcats	Bearcatsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Kimberley	Kimberley	k1gInPc1
Dynamiters	Dynamitersa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Londýn	Londýn	k1gInSc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Sudbury	Sudbur	k1gInPc1
Wolves	Wolvesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Trail	Trail	k1gInSc1
Smoke	Smoke	k1gNnSc1
Eaters	Eaters	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Basilej	Basilej	k1gFnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1940	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
se	se	k3xPyFc4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
nekonala	konat	k5eNaImAgFnS
kvůli	kvůli	k7c3
druhé	druhý	k4xOgFnSc3
světové	světový	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Ottawa	Ottawa	k1gFnSc1
RCAF	RCAF	kA
Flyers	Flyersa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Mořic	Mořic	k1gMnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Sudbury	Sudbur	k1gInPc1
Wolves	Wolvesa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Edmonton	Edmonton	k1gInSc1
Mercurys	Mercurys	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Londýn	Londýn	k1gInSc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Lethbridge	Lethbridge	k1gInSc1
Maple	Maple	k1gNnSc1
Leafs	Leafs	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Paříž	Paříž	k1gFnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Edmonton	Edmonton	k1gInSc1
Mercurys	Mercurys	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
Drammen	Drammen	k1gInSc1
<g/>
,	,	kIx,
Sandvika	Sandvika	k1gFnSc1
<g/>
,	,	kIx,
Lillestrø	Lillestrø	k1gInSc1
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
–	–	k?
<g/>
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Basilej	Basilej	k1gFnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Penticton	Penticton	k1gInSc1
V	V	kA
<g/>
'	'	kIx"
<g/>
es	es	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Krefeld	Krefeld	k1gInSc1
<g/>
,	,	kIx,
Dortmund	Dortmund	k1gInSc1
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
Düsseldorf	Düsseldorf	k1gMnSc1
(	(	kIx(
<g/>
SRN	SRN	kA
<g/>
)	)	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
USA	USA	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Whitby	Whitb	k1gInPc1
Dunlops	Dunlopsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Oslo	Oslo	k1gNnSc1
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Belleville	Belleville	k1gNnSc1
McFarlands	McFarlandsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
Ml.	ml.	kA
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Kitchener-Waterloo	Kitchener-Waterloo	k6eAd1
Dutchmen	Dutchmen	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Squaw	squaw	k1gFnSc1
Valley	Vallea	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Trail	Trail	k1gInSc1
Smoke	Smoke	k1gNnSc1
Eaters	Eaters	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Lausanne	Lausanne	k1gNnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Colorado	Colorado	k1gNnSc1
Springs	Springs	k1gInSc1
<g/>
,	,	kIx,
Denver	Denver	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Innsbruck	Innsbruck	k1gInSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Tampere	Tamprat	k5eAaPmIp3nS
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
<g/>
,	,	kIx,
Záhřeb	Záhřeb	k1gInSc1
(	(	kIx(
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Grenoble	Grenoble	k1gInSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
ZOH	ZOH	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Bern	Bern	k1gInSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Düsseldorf	Düsseldorf	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Katovice	Katovice	k1gFnPc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
</s>
<s>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Göteborg	Göteborg	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
49	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Düsseldorf	Düsseldorf	k1gInSc1
<g/>
,	,	kIx,
Dortmund	Dortmund	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Södertälje	Södertälje	k1gFnSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Fribourg	Fribourg	k1gInSc1
<g/>
,	,	kIx,
Bern	Bern	k1gInSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Dortmund	Dortmund	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
Bolzano	Bolzana	k1gFnSc5
<g/>
,	,	kIx,
Canazei	Canaze	k1gMnPc5
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
59	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Gävle	Gävle	k1gFnSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Basilej	Basilej	k1gFnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
Lillehammer	Lillehammer	k1gInSc1
<g/>
,	,	kIx,
Hamar	Hamar	k1gInSc1
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
Hannover	Hannover	k1gInSc1
<g/>
,	,	kIx,
Norimberk	Norimberk	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Göteborg	Göteborg	k1gInSc1
<g/>
,	,	kIx,
Karlstad	Karlstad	k1gInSc1
<g/>
,	,	kIx,
Jönköping	Jönköping	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Innsbruck	Innsbruck	k1gInSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Riga	Riga	k1gFnSc1
(	(	kIx(
<g/>
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Mytišči	Mytišč	k1gFnSc6
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Halifax	Halifax	k1gInSc1
<g/>
,	,	kIx,
Quebec	Quebec	k1gInSc1
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Bern	Bern	k1gInSc1
<g/>
,	,	kIx,
Kloten	Kloten	k2eAgInSc1d1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
Mannheim	Mannheim	k1gInSc1
<g/>
,	,	kIx,
Gelsenkirchen	Gelsenkirchen	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Helsinky	Helsinky	k1gFnPc1
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Minsk	Minsk	k1gInSc1
(	(	kIx(
<g/>
Bělorusko	Bělorusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
81	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Paříž	Paříž	k1gFnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Kodaň	Kodaň	k1gFnSc1
<g/>
,	,	kIx,
Herning	Herning	k1gInSc1
(	(	kIx(
<g/>
Dánsko	Dánsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
83	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
</s>
<s>
Zrušeno	zrušit	k5eAaPmNgNnS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
covidu-	covidu-	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Lausanne	Lausanne	k1gNnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2021	#num#	k4
</s>
<s>
Riga	Riga	k1gFnSc1
(	(	kIx(
<g/>
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
85	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2022	#num#	k4
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2023	#num#	k4
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2024	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2025	#num#	k4
</s>
<s>
bude	být	k5eAaImBp3nS
oznámeno	oznámit	k5eAaPmNgNnS
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
bude	být	k5eAaImBp3nS
oznámeno	oznámit	k5eAaPmNgNnS
(	(	kIx(
<g/>
Dánsko	Dánsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Medailový	medailový	k2eAgInSc1d1
stav	stav	k1gInSc1
podle	podle	k7c2
zemí	zem	k1gFnPc2
do	do	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
včetně	včetně	k7c2
<g/>
)	)	kIx)
</s>
<s>
№	№	k?
<g/>
ZeměZlatoStříbroBronzCelkem	ZeměZlatoStříbroBronzCelek	k1gInSc7
</s>
<s>
1SSSR	1SSSR	k4
/	/	kIx~
Rusko	Rusko	k1gNnSc1
<g/>
27101047	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
227534	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Rusko	Rusko	k1gNnSc1
<g/>
53513	#num#	k4
</s>
<s>
2	#num#	k4
Kanada	Kanada	k1gFnSc1
<g/>
2615950	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
/	/	kIx~
Česko	Česko	k1gNnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Československo	Československo	k1gNnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Česko	Česko	k1gNnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
4	#num#	k4
Švédsko	Švédsko	k1gNnSc1
<g/>
11191747	#num#	k4
</s>
<s>
5	#num#	k4
Finsko	Finsko	k1gNnSc1
<g/>
38314	#num#	k4
</s>
<s>
6	#num#	k4
USA29819	USA29819	k1gFnSc1
</s>
<s>
7	#num#	k4
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
1225	#num#	k4
</s>
<s>
8	#num#	k4
Slovensko	Slovensko	k1gNnSc1
<g/>
1214	#num#	k4
</s>
<s>
9	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
3811	#num#	k4
</s>
<s>
10	#num#	k4
Německo	Německo	k1gNnSc1
<g/>
0	#num#	k4
<g/>
224	#num#	k4
</s>
<s>
11	#num#	k4
Rakousko	Rakousko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
a	a	k8xC
pořadí	pořadí	k1gNnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1920	#num#	k4
</s>
<s>
1924	#num#	k4
</s>
<s>
1928	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
1932	#num#	k4
</s>
<s>
1933	#num#	k4
</s>
<s>
1934	#num#	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
1936	#num#	k4
</s>
<s>
1937	#num#	k4
</s>
<s>
1938	#num#	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
–	–	k?
<g/>
8	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
58	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
N	N	kA
<g/>
6	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
4108	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
N	N	kA
<g/>
–	–	k?
<g/>
595445	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
N	N	kA
<g/>
65	#num#	k4
<g/>
–	–	k?
<g/>
54464	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
44	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
56105	#num#	k4
<g/>
–	–	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
N	N	kA
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
4767712	#num#	k4
<g/>
–	–	k?
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
N	N	kA
<g/>
5447	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
N	N	kA
<g/>
896	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
N	N	kA
<g/>
67	#num#	k4
<g/>
–	–	k?
<g/>
76128577	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
66	#num#	k4
<g/>
N	N	kA
<g/>
69	#num#	k4
<g/>
–	–	k?
<g/>
117	#num#	k4
<g/>
N	N	kA
<g/>
–	–	k?
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
N	N	kA
<g/>
–	–	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
N	N	kA
<g/>
–	–	k?
<g/>
9	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
77	#num#	k4
<g/>
N	N	kA
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
121114	#num#	k4
<g/>
N	N	kA
<g/>
–	–	k?
<g/>
12	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
N	N	kA
<g/>
–	–	k?
<g/>
1010	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
91011	#num#	k4
<g/>
–	–	k?
<g/>
1113	#num#	k4
<g/>
–	–	k?
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
11	#num#	k4
<g/>
–	–	k?
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
914	#num#	k4
<g/>
–	–	k?
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
15	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
13	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
14	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1920	#num#	k4
</s>
<s>
1924	#num#	k4
</s>
<s>
1928	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
1932	#num#	k4
</s>
<s>
1933	#num#	k4
</s>
<s>
1934	#num#	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
1936	#num#	k4
</s>
<s>
1937	#num#	k4
</s>
<s>
1938	#num#	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
N	N	kA
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
určeno	určit	k5eAaPmNgNnS
další	další	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1947	#num#	k4
</s>
<s>
1948	#num#	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
1951	#num#	k4
</s>
<s>
1952	#num#	k4
</s>
<s>
1953	#num#	k4
</s>
<s>
1954	#num#	k4
</s>
<s>
1955	#num#	k4
</s>
<s>
1956	#num#	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
1958	#num#	k4
</s>
<s>
1959	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1962	#num#	k4
</s>
<s>
1963	#num#	k4
</s>
<s>
1964	#num#	k4
</s>
<s>
1965	#num#	k4
</s>
<s>
1966	#num#	k4
</s>
<s>
1967	#num#	k4
</s>
<s>
1968	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
444	#num#	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
–	–	k?
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
5455455444	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
4	#num#	k4
<g/>
A	a	k8xC
<g/>
4544	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
546	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
546756655	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
8566	#num#	k4
<g/>
–	–	k?
<g/>
76868711987	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
455789	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
117108101415	#num#	k4
<g/>
–	–	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
86	#num#	k4
<g/>
–	–	k?
<g/>
12116	#num#	k4
<g/>
–	–	k?
<g/>
11107	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
–	–	k?
<g/>
1410161313131413	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
–	–	k?
<g/>
466774567766	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
8649	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
–	–	k?
<g/>
959108121111	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
45105	#num#	k4
<g/>
–	–	k?
<g/>
10815	#num#	k4
<g/>
–	–	k?
<g/>
1416	#num#	k4
<g/>
–	–	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
8124	#num#	k4
<g/>
–	–	k?
<g/>
107	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
15161713	#num#	k4
<g/>
–	–	k?
</s>
<s>
NDR	NDR	kA
</s>
<s>
–	–	k?
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
5578	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
67	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
786811	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
–	–	k?
<g/>
129989	#num#	k4
<g/>
–	–	k?
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1947	#num#	k4
</s>
<s>
1948	#num#	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
1951	#num#	k4
</s>
<s>
1952	#num#	k4
</s>
<s>
1953	#num#	k4
</s>
<s>
1954	#num#	k4
</s>
<s>
1955	#num#	k4
</s>
<s>
1956	#num#	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
1958	#num#	k4
</s>
<s>
1959	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1962	#num#	k4
</s>
<s>
1963	#num#	k4
</s>
<s>
1964	#num#	k4
</s>
<s>
1965	#num#	k4
</s>
<s>
1966	#num#	k4
</s>
<s>
1967	#num#	k4
</s>
<s>
1968	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
–	–	k?
<g/>
971114	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
–	–	k?
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
810137	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
181221	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
–	–	k?
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
–	–	k?
<g/>
1112	#num#	k4
<g/>
–	–	k?
<g/>
101012	#num#	k4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
–	–	k?
<g/>
1710	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
99158	#num#	k4
<g/>
–	–	k?
<g/>
161114	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
13	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
–	–	k?
<g/>
13141511129	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
–	–	k?
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
1418	#num#	k4
<g/>
–	–	k?
<g/>
1818	#num#	k4
<g/>
–	–	k?
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
14	#num#	k4
<g/>
–	–	k?
<g/>
1716121516	#num#	k4
<g/>
–	–	k?
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
19	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
–	–	k?
</s>
<s>
JAR	jar	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
19	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
–	–	k?
</s>
<s>
A	a	k9
<g/>
:	:	kIx,
Československo	Československo	k1gNnSc1
z	z	k7c2
turnaje	turnaj	k1gInSc2
odstoupilo	odstoupit	k5eAaPmAgNnS
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
anulovány	anulovat	k5eAaBmNgInP
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
1971	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1973	#num#	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
1975	#num#	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
1977	#num#	k4
</s>
<s>
1978	#num#	k4
</s>
<s>
1979	#num#	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
1982	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1985	#num#	k4
</s>
<s>
1986	#num#	k4
</s>
<s>
1987	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
56	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
44464	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
444448	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
54444445575657545565	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
676887646675894676547	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1085569867567657767786	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
1612761315912131113111414109812974	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
8687555710981011109898141212	#num#	k4
</s>
<s>
NDR	NDR	kA
</s>
<s>
7599767898101296811131313	#num#	k4
<g/>
–	–	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1416141514161315181520978111014101099	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
119101315131511121412141212151710981010	#num#	k4
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
910111298101315161915181618152018171420	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
1213151010121198121113131520201926201918	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
1820202016111414161798161714131517161514	#num#	k4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
151112111210121011101416171313161715151615	#num#	k4
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
131513141114171717181517101112141114111313	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
1716	#num#	k4
<g/>
–	–	k?
<g/>
2019191921222121202117121211121111	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
1971	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1973	#num#	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
1975	#num#	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
1977	#num#	k4
</s>
<s>
1978	#num#	k4
</s>
<s>
1979	#num#	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
1982	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1985	#num#	k4
</s>
<s>
1986	#num#	k4
</s>
<s>
1987	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
171817181718181814131719211816222120232224	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
191419171817161620212222222222192321222017	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
161920	#num#	k4
<g/>
–	–	k?
<g/>
201818151919181619191819	#num#	k4
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
2019211921	#num#	k4
<g/>
–	–	k?
<g/>
202019191620192021211816181716	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
18	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
–	–	k?
<g/>
2124	#num#	k4
<g/>
–	–	k?
<g/>
2324	#num#	k4
<g/>
–	–	k?
<g/>
27262121	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
2122	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
–	–	k?
<g/>
2324	#num#	k4
<g/>
–	–	k?
<g/>
2425242525	#num#	k4
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
22	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
–	–	k?
<g/>
2423232222212322	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
21	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
–	–	k?
<g/>
26252427	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
222324	#num#	k4
<g/>
–	–	k?
<g/>
23232424	#num#	k4
<g/>
–	–	k?
<g/>
2828	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
25	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
–	–	k?
<g/>
252623252426	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
–	–	k?
<g/>
2729	#num#	k4
<g/>
–	–	k?
</s>
<s>
JAR	jar	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
28	#num#	k4
</s>
<s>
Hongkong	Hongkong	k1gInSc1
</s>
<s>
–	–	k?
<g/>
28	#num#	k4
<g/>
–	–	k?
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
29	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
–	–	k?
<g/>
30	#num#	k4
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
31	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
32	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
:	:	kIx,
od	od	k7c2
1992	#num#	k4
Rusko	Rusko	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
74545756	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
46445647	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
5544551166105	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
67444	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
755456756	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
21131097774586131012	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
6466126547136756413	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
121312141548691088898795	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
59981111201788791517910154	#num#	k4
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
K2221151389914171418106119810	K2221151389914171418106119810	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
8677810131212152319181412161815	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
21141413791181311979101311711	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
111110912211210152220201711148119	#num#	k4
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
9811121614101311121011161815171417	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
10108111013151520191816202018141214	#num#	k4
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
16171718202017212218111214131012138	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
22232322211714141091214111216192019	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
232422211415191821211713121521201716	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
131219161822181919232525252624232223	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
242527232218212317131517131617151918	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
141515171723232018141921192120222322	#num#	k4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
171618202416161616161615242222212121	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
151816191924252424242422222525262524	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
212626242625242523202124212319181620	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
181920262526263029252626272927292831	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
192025272728282625283230283028323438	#num#	k4
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
K2824252319222227292223232423273129	K2824252319222227292223232423273129	k4
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
KK31292827272828302829261926242426	KK31292827272828302829261926242426	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
202729343533313335353436353238383635	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
K3130282929292724262732292729252628	K3130282929292724262732292729252628	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
293033333031302930272927343130282925	#num#	k4
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
26	#num#	k4
<g/>
–	–	k?
<g/>
4135343336	#num#	k4
<g/>
–	–	k?
<g/>
413942	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
273336363434343633363633313331302732	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
<g/>
28303230	#num#	k4
<g/>
–	–	k?
<g/>
3234323131323535333027	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
283234323636353537313028363432313233	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
302932313132323131333335383733343330	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
313435353335333432343738302834363839	#num#	k4
</s>
<s>
JAR	jar	k1gFnSc1
</s>
<s>
323537	#num#	k4
<g/>
–	–	k?
<g/>
3737363736373840424043424044	#num#	k4
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
–	–	k?
<g/>
403838383941394136373534	#num#	k4
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
4040424043413837353737	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
–	–	k?
<g/>
39	#num#	k4
<g/>
K	k	k7c3
<g/>
3838373939434137373940394136	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
KK	KK	kA
<g/>
–	–	k?
<g/>
K	k	k7c3
<g/>
39393842	#num#	k4
<g/>
–	–	k?
<g/>
394342404239444240	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
K	K	kA
<g/>
–	–	k?
<g/>
38	#num#	k4
<g/>
K	K	kA
<g/>
–	–	k?
<g/>
4039	#num#	k4
<g/>
–	–	k?
<g/>
454443	#num#	k4
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
41	#num#	k4
<g/>
–	–	k?
<g/>
404239434542434345	#num#	k4
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
44444441404541	#num#	k4
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
454543	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
4446	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
–	–	k?
<g/>
46	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
47	#num#	k4
<g/>
–	–	k?
</s>
<s>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
:	:	kIx,
do	do	k7c2
2002	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
SrbskoK	SrbskoK	k1gFnPc2
<g/>
:	:	kIx,
kvalifikace	kvalifikace	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2025	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
2022	#num#	k4
</s>
<s>
2023	#num#	k4
</s>
<s>
2024	#num#	k4
</s>
<s>
2025	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
6565	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
44645	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
55554	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
466	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
9111081168	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
1089991499	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
USA	USA	kA
</s>
<s>
876457	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
7445774	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
7129141078116	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
6810121110111312	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
1414147712131518	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
129138121491215	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
13101111131310810	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
11131213148121011	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
192022262424231713	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
151815171520171416	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
2022	#num#	k4
</s>
<s>
2023	#num#	k4
</s>
<s>
2024	#num#	k4
</s>
<s>
2025	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
181518152118161814	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
161716181617152120	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
201919211815212021	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
171617161716191917	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
222321222321181619	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
232424231919202224	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
282120192022242425	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
212223202223222627	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
252727252525252322	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
242525272829282928	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
313129242626272830	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
292626282928292723	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
2022	#num#	k4
</s>
<s>
2023	#num#	k4
</s>
<s>
2024	#num#	k4
</s>
<s>
2025	#num#	k4
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
272928292727262526	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
263034353230343532	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
302832323435303031	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
323333313132313129	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
343231303333333436	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
353530333031323334	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
363638383534353233	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
333436373638363637	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
413935343937373735	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
373837363736393939	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
383740413840424241	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
404242424139383840	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
2022	#num#	k4
</s>
<s>
2023	#num#	k4
</s>
<s>
2024	#num#	k4
</s>
<s>
2025	#num#	k4
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
48464542434138	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
434139404241404342	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
JAR	jar	k1gFnSc1
</s>
<s>
424041394043454546	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
394444	#num#	k4
<g/>
–	–	k?
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
444343434344414044	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
–	–	k?
<g/>
4743	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Hongkong	Hongkong	k1gInSc1
</s>
<s>
–	–	k?
<g/>
444446444648	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
–	–	k?
<g/>
464445	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
454545	#num#	k4
<g/>
–	–	k?
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
–	–	k?
<g/>
464546	#num#	k4
<g/>
–	–	k?
<g/>
474947	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
4745484850	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
464647	#num#	k4
<g/>
–	–	k?
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
–	–	k?
<g/>
49	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
–	–	k?
<g/>
5051	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
–	–	k?
<g/>
52	#num#	k4
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
–	–	k?
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
–	–	k?
</s>
<s>
Historická	historický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
podle	podle	k7c2
získaných	získaný	k2eAgInPc2d1
bodů	bod	k1gInPc2
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
včetně	včetně	k7c2
<g/>
)	)	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
úspěšnost	úspěšnost	k1gFnSc4
zemí	zem	k1gFnPc2
podle	podle	k7c2
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
remíz	remíza	k1gFnPc2
a	a	k8xC
porážek	porážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1920	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
byly	být	k5eAaImAgFnP
za	za	k7c4
vítězství	vítězství	k1gNnSc4
dva	dva	k4xCgInPc4
body	bod	k1gInPc4
<g/>
,	,	kIx,
remízu	remíza	k1gFnSc4
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
a	a	k8xC
porážku	porážka	k1gFnSc4
nula	nula	k1gFnSc1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
jsou	být	k5eAaImIp3nP
za	za	k7c4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
normální	normální	k2eAgFnSc6d1
hrací	hrací	k2eAgFnSc6d1
době	doba	k1gFnSc6
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnSc4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
a	a	k8xC
samostatných	samostatný	k2eAgInPc6d1
nájezdech	nájezd	k1gInPc6
dva	dva	k4xCgInPc4
body	bod	k1gInPc4
<g/>
,	,	kIx,
porážky	porážka	k1gFnPc4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
a	a	k8xC
samostatných	samostatný	k2eAgInPc6d1
nájezdech	nájezd	k1gInPc6
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
a	a	k8xC
za	za	k7c4
porážku	porážka	k1gFnSc4
v	v	k7c6
normální	normální	k2eAgFnSc6d1
hrací	hrací	k2eAgFnSc6d1
době	doba	k1gFnSc6
nula	nula	k1gFnSc1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Remízy	remíza	k1gFnPc1
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Účast	účast	k1gFnSc1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Výhry	výhra	k1gFnPc1
</s>
<s>
VP	VP	kA
</s>
<s>
Remízy	remíza	k1gFnPc1
</s>
<s>
PP	PP	kA
</s>
<s>
Porážky	porážka	k1gFnPc1
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanada	Kanada	k1gFnSc1
<g/>
7256539873191202974	#num#	k4
<g/>
:	:	kIx,
<g/>
1175927	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
SSSR	SSSR	kA
/	/	kIx~
Rusko	Rusko	k1gNnSc1
<g/>
615143966325752827	#num#	k4
<g/>
:	:	kIx,
<g/>
977926	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
342972520190261975	#num#	k4
<g/>
:	:	kIx,
<g/>
535523	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Rusko	Rusko	k1gNnSc1
<g/>
27217144613549852	#num#	k4
<g/>
:	:	kIx,
<g/>
442403	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Československo	Československo	k1gNnSc1
/	/	kIx~
Česko	Česko	k1gNnSc1
<g/>
78626391144881652717	#num#	k4
<g/>
:	:	kIx,
<g/>
1406917	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Československo	Československo	k1gNnSc1
<g/>
5240625503701141947	#num#	k4
<g/>
:	:	kIx,
<g/>
955547	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
Česko	Česko	k1gNnSc1
26	#num#	k4
220	#num#	k4
136	#num#	k4
14	#num#	k4
11	#num#	k4
8	#num#	k4
51	#num#	k4
770	#num#	k4
<g/>
:	:	kIx,
<g/>
451370	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
<g/>
77635374115561892625	#num#	k4
<g/>
:	:	kIx,
<g/>
1622901	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
<g/>
7054025393982312076	#num#	k4
<g/>
:	:	kIx,
<g/>
1916624	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finsko	Finsko	k1gNnSc1
<g/>
65547238165352351820	#num#	k4
<g/>
:	:	kIx,
<g/>
1892624	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
<g/>
63472138743102741164	#num#	k4
<g/>
:	:	kIx,
<g/>
1950368	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
51356132824141781087	#num#	k4
<g/>
:	:	kIx,
<g/>
1365356	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensko	Slovensko	k1gNnSc1
<g/>
23165736111065494	#num#	k4
<g/>
:	:	kIx,
<g/>
438209	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norsko	Norsko	k1gNnSc1
<g/>
36234575108154549	#num#	k4
<g/>
:	:	kIx,
<g/>
1048166	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
2716655591186399	#num#	k4
<g/>
:	:	kIx,
<g/>
524164	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko	Rakousko	k1gNnSc1
<g/>
33203511162133469	#num#	k4
<g/>
:	:	kIx,
<g/>
870133	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bělorusko	Bělorusko	k1gNnSc1
<g/>
191254142771286	#num#	k4
<g/>
:	:	kIx,
<g/>
397120	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
<g/>
3217541734121348	#num#	k4
<g/>
:	:	kIx,
<g/>
734120	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Itálie	Itálie	k1gFnSc1
<g/>
30184402232117383	#num#	k4
<g/>
:	:	kIx,
<g/>
865111	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polsko	Polsko	k1gNnSc1
<g/>
30216460160154495	#num#	k4
<g/>
:	:	kIx,
<g/>
1226108	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánsko	Dánsko	k1gNnSc1
<g/>
1710724112565229	#num#	k4
<g/>
:	:	kIx,
<g/>
45095	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
15924504043301	#num#	k4
<g/>
:	:	kIx,
<g/>
36094	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NDR	NDR	kA
<g/>
1411826010082271	#num#	k4
<g/>
:	:	kIx,
<g/>
65062	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maďarsko	Maďarsko	k1gNnSc1
<g/>
13721607049132	#num#	k4
<g/>
:	:	kIx,
<g/>
23540	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajina	Ukrajina	k1gFnSc1
<g/>
951110403693	#num#	k4
<g/>
:	:	kIx,
<g/>
20627	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
<g/>
13651007048176	#num#	k4
<g/>
:	:	kIx,
<g/>
41927	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumunsko	Rumunsko	k1gNnSc1
<g/>
10601301046126	#num#	k4
<g/>
:	:	kIx,
<g/>
37227	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovinsko	Slovinsko	k1gNnSc1
<g/>
956703541111	#num#	k4
<g/>
:	:	kIx,
<g/>
25224	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jugoslávie	Jugoslávie	k1gFnSc1
<g/>
3198020968	#num#	k4
<g/>
:	:	kIx,
<g/>
13118	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazachstán	Kazachstán	k1gInSc1
<g/>
844113387	#num#	k4
<g/>
:	:	kIx,
<g/>
20314	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgie	Belgie	k1gFnSc1
<g/>
124460103783	#num#	k4
<g/>
:	:	kIx,
<g/>
42013	#num#	k4
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
42220002034	#num#	k4
<g/>
:	:	kIx,
<g/>
1564	#num#	k4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litva	Litva	k1gFnSc1
<g/>
14100033	#num#	k4
<g/>
:	:	kIx,
<g/>
332	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
17000074	#num#	k4
<g/>
:	:	kIx,
<g/>
480	#num#	k4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
<g/>
160000610	#num#	k4
<g/>
:	:	kIx,
<g/>
870	#num#	k4
</s>
<s>
VP	VP	kA
–	–	k?
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
nebo	nebo	k8xC
po	po	k7c6
samostatných	samostatný	k2eAgInPc6d1
nájezdech	nájezd	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
PP	PP	kA
–	–	k?
Porážky	porážka	k1gFnSc2
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
nebo	nebo	k8xC
po	po	k7c6
samostatných	samostatný	k2eAgInPc6d1
nájezdech	nájezd	k1gInPc6
</s>
<s>
Poznámky	poznámka	k1gFnPc4
k	k	k7c3
tabulce	tabulka	k1gFnSc3
<g/>
:	:	kIx,
</s>
<s>
1951	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
a	a	k8xC
1961	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
–	–	k?
skupiny	skupina	k1gFnSc2
B	B	kA
<g/>
,	,	kIx,
C	C	kA
a	a	k8xC
D	D	kA
nejsou	být	k5eNaImIp3nP
započítány	započítat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
–	–	k?
účast	účast	k1gFnSc1
Československa	Československo	k1gNnSc2
je	být	k5eAaImIp3nS
započtena	započíst	k5eAaPmNgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
výsledků	výsledek	k1gInPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
mužstvo	mužstvo	k1gNnSc1
pro	pro	k7c4
státní	státní	k2eAgInSc4d1
smutek	smutek	k1gInSc4
odstoupilo	odstoupit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
–	–	k?
Finsko	Finsko	k1gNnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
kontumačně	kontumačně	k6eAd1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
–	–	k?
Švédsko	Švédsko	k1gNnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
kontumačně	kontumačně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
–	–	k?
ČSSR	ČSSR	kA
–	–	k?
USA	USA	kA
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
kontumačně	kontumačně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
–	–	k?
Finsko	Finsko	k1gNnSc1
–	–	k?
USA	USA	kA
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
kontumačně	kontumačně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
od	od	k7c2
1992	#num#	k4
–	–	k?
u	u	k7c2
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
skončil	skončit	k5eAaPmAgMnS
na	na	k7c4
samostatné	samostatný	k2eAgInPc4d1
nájezdy	nájezd	k1gInPc4
je	být	k5eAaImIp3nS
skóre	skóre	k1gNnSc1
započítáno	započítat	k5eAaPmNgNnS
včetně	včetně	k7c2
rozhodujícího	rozhodující	k2eAgInSc2d1
samostatného	samostatný	k2eAgInSc2d1
nájezdu	nájezd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
–	–	k?
Česko	Česko	k1gNnSc1
–	–	k?
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
–	–	k?
Švédsko	Švédsko	k1gNnSc1
(	(	kIx(
<g/>
semifinále	semifinále	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
–	–	k?
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
finále	finále	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
play-off	play-off	k1gInSc4
bylo	být	k5eAaImAgNnS
hráno	hrát	k5eAaImNgNnS
na	na	k7c4
dva	dva	k4xCgInPc4
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
prodloužení	prodloužení	k1gNnSc4
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
rozhodovalo	rozhodovat	k5eAaImAgNnS
o	o	k7c6
vítězi	vítěz	k1gMnSc6
série	série	k1gFnSc2
<g/>
,	,	kIx,
IIHF	IIHF	kA
považuje	považovat	k5eAaImIp3nS
jako	jako	k9
samostatný	samostatný	k2eAgInSc4d1
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pořadatelství	pořadatelství	k1gNnSc1
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
12	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Gävle	Gävle	k1gFnSc1
<g/>
,	,	kIx,
Göteborg	Göteborg	k1gInSc1
<g/>
,	,	kIx,
Jönköping	Jönköping	k1gInSc1
<g/>
,	,	kIx,
Karlstad	Karlstad	k1gInSc1
<g/>
,	,	kIx,
Malmö	Malmö	k1gFnSc1
<g/>
,	,	kIx,
Södertälje	Södertälje	k1gFnSc1
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1
</s>
<s>
1949	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2025	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Česko	Česko	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
1933	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2024	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Basilej	Basilej	k1gFnSc1
<g/>
,	,	kIx,
Bern	Bern	k1gInSc1
<g/>
,	,	kIx,
Davos	Davos	k1gInSc1
<g/>
,	,	kIx,
Fribourg	Fribourg	k1gInSc1
<g/>
,	,	kIx,
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Lausanne	Lausanne	k1gNnSc1
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Mořic	Mořic	k1gMnSc1
<g/>
,	,	kIx,
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Kloten	Kloten	k2eAgInSc1d1
</s>
<s>
1928	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
Dortmund	Dortmund	k1gInSc1
<g/>
,	,	kIx,
Düsseldorf	Düsseldorf	k1gInSc1
<g/>
,	,	kIx,
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1
<g/>
,	,	kIx,
Hannover	Hannover	k1gInSc1
<g/>
,	,	kIx,
Krefeld	Krefeld	k1gInSc1
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Norimberk	Norimberk	k1gInSc1
<g/>
,	,	kIx,
Mannheim	Mannheim	k1gInSc1
</s>
<s>
1930	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
9	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Tampere	Tamper	k1gMnSc5
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
</s>
<s>
1965	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2022	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
/	/	kIx~
SSSR	SSSR	kA
<g/>
/	/	kIx~
<g/>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Mytišči	Mytišč	k1gInSc3
</s>
<s>
1957	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2023	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Innsbruck	Innsbruck	k1gInSc1
</s>
<s>
1930	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Chamonix	Chamonix	k1gNnSc1
<g/>
,	,	kIx,
Grenoble	Grenoble	k1gInSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
1924	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
/	/	kIx~
Československo	Československo	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
</s>
<s>
1959	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
<g/>
,	,	kIx,
Lake	Lake	k1gFnPc2
Placid	Placida	k1gFnPc2
<g/>
,	,	kIx,
Squaw	squaw	k1gFnSc2
Valley	Vallea	k1gFnSc2
</s>
<s>
1932	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Bolzano	Bolzana	k1gFnSc5
<g/>
,	,	kIx,
Canazei	Canaze	k1gFnSc5
<g/>
,	,	kIx,
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
1934	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
Drammen	Drammen	k1gInSc1
<g/>
,	,	kIx,
Hamar	Hamar	k1gInSc1
<g/>
,	,	kIx,
Lillehammer	Lillehammer	k1gInSc1
<g/>
,	,	kIx,
Oslo	Oslo	k1gNnSc1
</s>
<s>
1952	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Londýn	Londýn	k1gInSc1
</s>
<s>
1937	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Katovice	Katovice	k1gFnPc1
<g/>
,	,	kIx,
Krynica	Krynic	k2eAgFnSc1d1
Zdrój	Zdrój	k1gFnSc1
</s>
<s>
1931	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Riga	Riga	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Kodaň	Kodaň	k1gFnSc1
<g/>
,	,	kIx,
Herning	Herning	k1gInSc1
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
2025	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Antverpy	Antverpy	k1gFnPc1
</s>
<s>
1920	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
/	/	kIx~
Jugoslávie	Jugoslávie	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
</s>
<s>
1966	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
</s>
<s>
Halifax	Halifax	k1gInSc1
<g/>
,	,	kIx,
Québec	Québec	k1gInSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Minsk	Minsk	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Tabulka	tabulka	k1gFnSc1
změn	změna	k1gFnPc2
pořadatelů	pořadatel	k1gMnPc2
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
</s>
<s>
MS	MS	kA
</s>
<s>
Původní	původní	k2eAgMnSc1d1
pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Nový	nový	k2eAgMnSc1d1
pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Důvod	důvod	k1gInSc1
změny	změna	k1gFnSc2
</s>
<s>
1940	#num#	k4
<g/>
Německo	Německo	k1gNnSc1
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
1944	#num#	k4
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
1947	#num#	k4
<g/>
Velká	velký	k2eAgFnSc1d1
BritánieČeskoslovenskoVelká	BritánieČeskoslovenskoVelký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
se	se	k3xPyFc4
zřekla	zřeknout	k5eAaPmAgFnS
pořadatelství	pořadatelství	k1gNnSc2
</s>
<s>
1958	#num#	k4
<g/>
RakouskoNorskoNedostavěná	RakouskoNorskoNedostavěný	k2eAgFnSc1d1
hala	hala	k1gFnSc1
</s>
<s>
1969	#num#	k4
<g/>
ČeskoslovenskoŠvédskoInvaze	ČeskoslovenskoŠvédskoInvaha	k1gFnSc6
vojsk	vojsko	k1gNnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
do	do	k7c2
Československa	Československo	k1gNnSc2
</s>
<s>
1970	#num#	k4
<g/>
KanadaŠvédskoRoztržka	KanadaŠvédskoRoztržka	k1gFnSc1
mezi	mezi	k7c7
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
IIHF	IIHF	kA
ohledně	ohledně	k7c2
startu	start	k1gInSc2
profesionálů	profesionál	k1gMnPc2
na	na	k7c6
MS	MS	kA
</s>
<s>
1976	#num#	k4
<g/>
RakouskoPolskoKonání	RakouskoPolskoKonání	k1gNnSc1
ZOH	ZOH	kA
v	v	k7c6
Innsbrucku	Innsbrucko	k1gNnSc6
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
s	s	k7c7
Mistrovstvím	mistrovství	k1gNnSc7
světa	svět	k1gInSc2
</s>
<s>
2003	#num#	k4
<g/>
ČeskoFinskoNedostavěná	ČeskoFinskoNedostavěný	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
,	,	kIx,
prohození	prohození	k1gNnSc1
pořadatelství	pořadatelství	k1gNnSc2
</s>
<s>
2020	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
–	–	k?
<g/>
Pandemie	pandemie	k1gFnSc1
covidu-	covidu-	k?
<g/>
19	#num#	k4
</s>
<s>
2021	#num#	k4
<g/>
Bělorusko	Bělorusko	k1gNnSc1
a	a	k8xC
LotyšskoLotyšskoProtesty	LotyšskoLotyšskoProtest	k1gInPc1
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
V	v	k7c6
roce	rok	k1gInSc6
Olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
se	se	k3xPyFc4
do	do	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
za	za	k7c4
mistra	mistr	k1gMnSc4
světa	svět	k1gInSc2
automaticky	automaticky	k6eAd1
považoval	považovat	k5eAaImAgMnS
vítěz	vítěz	k1gMnSc1
olympijského	olympijský	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
v	v	k7c6
elitní	elitní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
2012	#num#	k4
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgInSc7d1
pořádajícím	pořádající	k2eAgNnSc6d1
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
Švédsko	Švédsko	k1gNnSc4
<g/>
1	#num#	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
pořádá	pořádat	k5eAaImIp3nS
společně	společně	k6eAd1
Švédsko	Švédsko	k1gNnSc1
s	s	k7c7
Dánskem	Dánsko	k1gNnSc7
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
hráno	hrát	k5eAaImNgNnS
jen	jen	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
hráno	hrát	k5eAaImNgNnS
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
i	i	k8xC
Slovenska	Slovensko	k1gNnSc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
pořádaly	pořádat	k5eAaImAgInP
společně	společně	k6eAd1
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
a	a	k8xC
Německo	Německo	k1gNnSc1
<g/>
1	#num#	k4
2	#num#	k4
pořádaly	pořádat	k5eAaImAgInP
společně	společně	k6eAd1
Německo	Německo	k1gNnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
↑	↑	k?
v	v	k7c6
rámci	rámec	k1gInSc6
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
RAMPA	rampa	k1gFnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
;	;	kIx,
ZELENKO	Zelenka	k1gMnSc5
<g/>
,	,	kIx,
Anton	Anton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
oficiální	oficiální	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
hokeji	hokej	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
ruší	rušit	k5eAaImIp3nS
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
IIHF	IIHF	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-21	2020-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
divize	divize	k1gFnSc2
I	i	k8xC
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
divize	divize	k1gFnSc2
II	II	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
divize	divize	k1gFnSc2
III	III	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
–	–	k?
divize	divize	k1gFnSc2
IV	IV	kA
</s>
<s>
Herní	herní	k2eAgInSc1d1
systém	systém	k1gInSc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
na	na	k7c6
webu	web	k1gInSc6
iRozhlas	iRozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
<g/>
(	(	kIx(
<g/>
elitní	elitní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2000	#num#	k4
MS	MS	kA
Skupina	skupina	k1gFnSc1
A	a	k9
<g/>
)	)	kIx)
</s>
<s>
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1930	#num#	k4
•	•	k?
1931	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1933	#num#	k4
•	•	k?
1934	#num#	k4
•	•	k?
1935	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1937	#num#	k4
•	•	k?
1938	#num#	k4
•	•	k?
1939	#num#	k4
•	•	k?
1940	#num#	k4
•	•	k?
1941	#num#	k4
•	•	k?
1942	#num#	k4
•	•	k?
1943	#num#	k4
•	•	k?
1944	#num#	k4
•	•	k?
1945	#num#	k4
•	•	k?
1946	#num#	k4
•	•	k?
1947	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1959	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1961	#num#	k4
•	•	k?
1962	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1963	#num#	k4
•	•	k?
1964	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1965	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1966	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1967	#num#	k4
•	•	k?
1968	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
Itálie	Itálie	k1gFnSc2
1994	#num#	k4
•	•	k?
Švédsko	Švédsko	k1gNnSc1
1995	#num#	k4
•	•	k?
Rakousko	Rakousko	k1gNnSc1
1996	#num#	k4
•	•	k?
Finsko	Finsko	k1gNnSc1
1997	#num#	k4
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
1998	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
Norsko	Norsko	k1gNnSc1
1999	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
Rusko	Rusko	k1gNnSc1
2000	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
Německo	Německo	k1gNnSc1
2001	#num#	k4
(	(	kIx(
<g/>
kvalif	kvalif	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Švédsko	Švédsko	k1gNnSc1
2002	#num#	k4
(	(	kIx(
<g/>
kvalif	kvalif	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Finsko	Finsko	k1gNnSc1
2003	#num#	k4
(	(	kIx(
<g/>
kvalif	kvalif	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Česko	Česko	k1gNnSc1
2004	#num#	k4
(	(	kIx(
<g/>
kvalif	kvalif	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Rakousko	Rakousko	k1gNnSc1
2005	#num#	k4
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
2006	#num#	k4
•	•	k?
Rusko	Rusko	k1gNnSc1
2007	#num#	k4
•	•	k?
Kanada	Kanada	k1gFnSc1
2008	#num#	k4
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
2009	#num#	k4
•	•	k?
Německo	Německo	k1gNnSc1
2010	#num#	k4
•	•	k?
Slovensko	Slovensko	k1gNnSc1
2011	#num#	k4
•	•	k?
Finsko	Finsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Švédsko	Švédsko	k1gNnSc1
2012	#num#	k4
•	•	k?
Švédsko	Švédsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Finsko	Finsko	k1gNnSc1
2013	#num#	k4
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
2014	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
2015	#num#	k4
•	•	k?
Rusko	Rusko	k1gNnSc1
2016	#num#	k4
•	•	k?
Německo	Německo	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
Francie	Francie	k1gFnSc1
2017	#num#	k4
•	•	k?
Dánsko	Dánsko	k1gNnSc1
2018	#num#	k4
•	•	k?
Slovensko	Slovensko	k1gNnSc1
2019	#num#	k4
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
2020	#num#	k4
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
2021	#num#	k4
•	•	k?
Finsko	Finsko	k1gNnSc1
2022	#num#	k4
•	•	k?
Rusko	Rusko	k1gNnSc1
2023	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
2024	#num#	k4
•	•	k?
Švédsko	Švédsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Dánsko	Dánsko	k1gNnSc1
2025	#num#	k4
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
divize	divize	k1gFnSc1
I	i	k9
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2000	#num#	k4
MS	MS	kA
Skupina	skupina	k1gFnSc1
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1959	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1961	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1962	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1965	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1966	#num#	k4
(	(	kIx(
<g/>
kval	kvala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
1967	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
(	(	kIx(
<g/>
divize	divize	k1gFnSc2
II	II	kA
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2000	#num#	k4
MS	MS	kA
Skupina	skupina	k1gFnSc1
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
1961	#num#	k4
•	•	k?
1962	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1965	#num#	k4
•	•	k?
1966	#num#	k4
•	•	k?
1967	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1994	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgMnS
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
(	(	kIx(
<g/>
divize	divize	k1gFnSc2
III	III	kA
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2000	#num#	k4
MS	MS	kA
Skupina	skupina	k1gFnSc1
D	D	kA
<g/>
)	)	kIx)
</s>
<s>
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
kval	kval	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
(	(	kIx(
<g/>
kval	kvanout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
(	(	kIx(
<g/>
divize	divize	k1gFnSc1
IV	Iva	k1gFnPc2
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
•	•	k?
2021	#num#	k4
Soupisky	soupiska	k1gFnSc2
</s>
<s>
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
MS	MS	kA
2017	#num#	k4
skupina	skupina	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
skupina	skupina	k1gFnSc1
B	B	kA
•	•	k?
MS	MS	kA
2018	#num#	k4
skupina	skupina	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
skupina	skupina	k1gFnSc1
B	B	kA
•	•	k?
MS	MS	kA
<g />
.	.	kIx.
</s>
<s hack="1">
2019	#num#	k4
skupina	skupina	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
skupina	skupina	k1gFnSc1
B	B	kA
•	•	k?
MS	MS	kA
2021	#num#	k4
skupina	skupina	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
skupina	skupina	k1gFnSc1
B	B	kA
Česko	Česko	k1gNnSc1
proti	proti	k7c3
soupeřům	soupeř	k1gMnPc3
v	v	k7c6
play	play	k0
off	off	k?
na	na	k7c4
MS	MS	kA
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
•	•	k?
Seznam	seznam	k1gInSc1
hokejových	hokejový	k2eAgFnPc2d1
reprezentací	reprezentace	k1gFnPc2
•	•	k?
Vítězové	vítěz	k1gMnPc1
kanadského	kanadský	k2eAgNnSc2d1
bodování	bodování	k1gNnSc2
•	•	k?
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
•	•	k?
Nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
•	•	k?
Seznam	seznam	k1gInSc1
československých	československý	k2eAgMnPc2d1
ledních	lední	k2eAgMnPc2d1
hokejistů	hokejista	k1gMnPc2
•	•	k?
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
ledních	lední	k2eAgMnPc2d1
hokejistů	hokejista	k1gMnPc2
<g/>
;	;	kIx,
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
roky	rok	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
MS	MS	kA
konalo	konat	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
IIHF	IIHF	kA
<g/>
)	)	kIx)
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
MS	MS	kA
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
•	•	k?
MS	MS	kA
žen	žena	k1gFnPc2
•	•	k?
MS	MS	kA
žen	žena	k1gFnPc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
•	•	k?
Euro	euro	k1gNnSc4
Hockey	Hockea	k1gFnSc2
Tour	Toura	k1gFnPc2
•	•	k?
Euro	euro	k1gNnSc4
Hockey	Hockea	k1gFnSc2
Challenge	Challeng	k1gFnSc2
•	•	k?
IIHF	IIHF	kA
Challenge	Challenge	k1gInSc1
Cup	cup	k1gInSc1
of	of	k?
Asia	Asium	k1gNnSc2
•	•	k?
Asijské	asijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Žebříček	žebříček	k1gInSc1
IIHF	IIHF	kA
•	•	k?
Seznam	seznam	k1gInSc1
reprezentací	reprezentace	k1gFnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
inline	inlinout	k5eAaPmIp3nS
hokeji	hokej	k1gInSc3
Klubové	klubový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
ženský	ženský	k2eAgInSc1d1
Champions	Champions	k1gInSc1
Cup	cup	k1gInSc1
•	•	k?
Kontinentální	kontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
Zaniklé	zaniklý	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
žen	žena	k1gFnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
pobřeží	pobřeží	k1gNnSc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
IIHF	IIHF	kA
Superpohár	superpohár	k1gInSc1
•	•	k?
Super	super	k1gInSc1
six	six	k?
•	•	k?
Hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
IIHF	IIHF	kA
•	•	k?
Victoria	Victorium	k1gNnPc1
Cup	cup	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2004226026	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151230103	#num#	k4
</s>
