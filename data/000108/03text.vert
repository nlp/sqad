<s>
Lipari	Lipari	k1gNnSc1	Lipari
(	(	kIx(	(
<g/>
37,3	[number]	k4	37,3
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zbytek	zbytek	k1gInSc1	zbytek
souostroví	souostroví	k1gNnSc2	souostroví
mimo	mimo	k7c4	mimo
Saliny	salina	k1gFnPc4	salina
<g/>
,	,	kIx,	,
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
městu	město	k1gNnSc3	město
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
Lipari	Lipar	k1gFnSc2	Lipar
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
historickým	historický	k2eAgNnSc7d1	historické
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
pevností	pevnost	k1gFnSc7	pevnost
a	a	k8xC	a
katedrálou	katedrála	k1gFnSc7	katedrála
je	být	k5eAaImIp3nS	být
také	také	k9	také
největší	veliký	k2eAgFnSc7d3	veliký
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lipari	Lipar	k1gInSc6	Lipar
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
200	[number]	k4	200
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pevnost	pevnost	k1gFnSc4	pevnost
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
řeckých	řecký	k2eAgInPc6d1	řecký
základech	základ	k1gInPc6	základ
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
stavební	stavební	k2eAgFnSc7d1	stavební
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
z	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
normanských	normanský	k2eAgInPc6d1	normanský
základech	základ	k1gInPc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
průzkumy	průzkum	k1gInPc1	průzkum
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
obývání	obývání	k1gNnSc4	obývání
ostrova	ostrov	k1gInSc2	ostrov
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Monte	Mont	k1gInSc5	Mont
Chirica	Chiric	k1gInSc2	Chiric
vysoký	vysoký	k2eAgInSc4d1	vysoký
602	[number]	k4	602
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
obsidiánu	obsidián	k1gInSc2	obsidián
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
drobné	drobný	k2eAgInPc1d1	drobný
černé	černý	k2eAgInPc1d1	černý
úlomky	úlomek	k1gInPc1	úlomek
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
bez	bez	k7c2	bez
vegetace	vegetace	k1gFnSc2	vegetace
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
barvou	barva	k1gFnSc7	barva
pemzy	pemza	k1gFnSc2	pemza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
těží	těžet	k5eAaImIp3nS	těžet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
nabízí	nabízet	k5eAaImIp3nS	nabízet
pláže	pláž	k1gFnPc4	pláž
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
okružních	okružní	k2eAgFnPc2d1	okružní
jízd	jízda	k1gFnPc2	jízda
loděmi	loď	k1gFnPc7	loď
<g/>
,	,	kIx,	,
procházka	procházka	k1gFnSc1	procházka
na	na	k7c4	na
Belvedere	Belveder	k1gInSc5	Belveder
Quattrocchi	Quattrocchi	k1gNnPc4	Quattrocchi
s	s	k7c7	s
pěknými	pěkný	k2eAgInPc7d1	pěkný
rozhledy	rozhled	k1gInPc7	rozhled
<g/>
.	.	kIx.	.
</s>
