<p>
<s>
Šlapací	šlapací	k2eAgNnSc1d1	šlapací
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
motor	motor	k1gInSc1	motor
poháněný	poháněný	k2eAgInSc1d1	poháněný
zpravidla	zpravidla	k6eAd1	zpravidla
lidskou	lidský	k2eAgFnSc7d1	lidská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
šlapací	šlapací	k2eAgFnSc1d1	šlapací
kola	kola	k1gFnSc1	kola
užívala	užívat	k5eAaImAgFnS	užívat
pro	pro	k7c4	pro
vytahování	vytahování	k1gNnSc4	vytahování
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
studní	studna	k1gFnPc2	studna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pohon	pohon	k1gInSc1	pohon
jeřábů	jeřáb	k1gInPc2	jeřáb
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
mletí	mletí	k1gNnSc3	mletí
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Šlapacích	šlapací	k2eAgNnPc2d1	šlapací
kol	kolo	k1gNnPc2	kolo
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starých	starý	k2eAgMnPc2d1	starý
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Římanů	Říman	k1gMnPc2	Říman
například	například	k6eAd1	například
pro	pro	k7c4	pro
odvodňování	odvodňování	k1gNnSc4	odvodňování
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
renesance	renesance	k1gFnSc2	renesance
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
součástí	součást	k1gFnSc7	součást
jeřábů	jeřáb	k1gInPc2	jeřáb
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raně	raně	k6eAd1	raně
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
šlapací	šlapací	k2eAgFnSc1d1	šlapací
kola	kola	k1gFnSc1	kola
používala	používat	k5eAaImAgFnS	používat
jako	jako	k9	jako
forma	forma	k1gFnSc1	forma
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
pro	pro	k7c4	pro
trestance	trestanec	k1gMnPc4	trestanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
Žentour	žentour	k1gInSc1	žentour
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
šlapací	šlapací	k2eAgFnSc2d1	šlapací
kolo	kolo	k1gNnSc4	kolo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
