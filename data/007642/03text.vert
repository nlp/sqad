<s>
Gobi	Gobi	k1gFnSc1	Gobi
(	(	kIx(	(
<g/>
mongolsky	mongolsky	k6eAd1	mongolsky
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Gov	Gov	k1gFnSc1	Gov
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
戈	戈	k?	戈
<g/>
,	,	kIx,	,
pinyin	pinyin	k2eAgMnSc1d1	pinyin
Gē	Gē	k1gMnSc1	Gē
Shā	Shā	k1gMnSc1	Shā
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Ke-pi	Ke	k1gFnSc2	Ke-p
ša-mo	šao	k6eAd1	ša-mo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
a	a	k8xC	a
největších	veliký	k2eAgFnPc2d3	veliký
pouští	poušť	k1gFnPc2	poušť
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
suchým	suchý	k2eAgFnPc3d1	suchá
oblastem	oblast	k1gFnPc3	oblast
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
značně	značně	k6eAd1	značně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
jižním	jižní	k2eAgNnSc6d1	jižní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
ohraničena	ohraničen	k2eAgFnSc1d1	ohraničena
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Altaj	Altaj	k1gInSc4	Altaj
a	a	k8xC	a
travnatými	travnatý	k2eAgFnPc7d1	travnatá
stepmi	step	k1gFnPc7	step
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
náhorní	náhorní	k2eAgFnSc7d1	náhorní
plošinou	plošina	k1gFnSc7	plošina
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Čínskou	čínský	k2eAgFnSc7d1	čínská
nížinou	nížina	k1gFnSc7	nížina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Gobi	Gobi	k1gFnPc2	Gobi
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
jednotně	jednotně	k6eAd1	jednotně
definovány	definovat	k5eAaBmNgFnP	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
nejednoznačnost	nejednoznačnost	k1gFnSc1	nejednoznačnost
samotného	samotný	k2eAgInSc2d1	samotný
výrazu	výraz	k1gInSc2	výraz
г	г	k?	г
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
i	i	k8xC	i
Vnějším	vnější	k2eAgNnSc6d1	vnější
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
neoznačuje	označovat	k5eNaImIp3nS	označovat
pouze	pouze	k6eAd1	pouze
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obecněji	obecně	k6eAd2	obecně
typ	typ	k1gInSc4	typ
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
Gobi	Gobi	k1gFnSc2	Gobi
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
veškeré	veškerý	k3xTgFnPc4	veškerý
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
od	od	k7c2	od
Pamíru	Pamír	k1gInSc2	Pamír
(	(	kIx(	(
<g/>
77	[number]	k4	77
<g/>
°	°	k?	°
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
k	k	k7c3	k
Chinganu	Chingan	k1gMnSc3	Chingan
(	(	kIx(	(
<g/>
116	[number]	k4	116
až	až	k9	až
118	[number]	k4	118
<g/>
°	°	k?	°
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
Altaje	Altaj	k1gInSc2	Altaj
<g/>
,	,	kIx,	,
Changaje	Changaj	k1gInSc2	Changaj
a	a	k8xC	a
úpatí	úpatí	k1gNnSc6	úpatí
Zabajkalských	zabajkalský	k2eAgFnPc2d1	zabajkalský
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
po	po	k7c4	po
Kchun-lun	Kchunun	k1gInSc4	Kchun-lun
a	a	k8xC	a
Severočínskou	severočínský	k2eAgFnSc4d1	Severočínská
nížinu	nížina	k1gFnSc4	nížina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
vymezení	vymezení	k1gNnSc1	vymezení
by	by	kYmCp3nS	by
pod	pod	k7c7	pod
Gobi	Gobi	k1gFnPc7	Gobi
řadilo	řadit	k5eAaImAgNnS	řadit
mj.	mj.	kA	mj.
i	i	k9	i
poušť	poušť	k1gFnSc1	poušť
Taklamakan	Taklamakany	k1gInPc2	Taklamakany
<g/>
,	,	kIx,	,
Džungarskou	Džungarský	k2eAgFnSc4d1	Džungarská
pánev	pánev	k1gFnSc4	pánev
a	a	k8xC	a
Ordoskou	Ordoska	k1gFnSc7	Ordoska
plošinu	plošina	k1gFnSc4	plošina
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc4	tento
pouště	poušť	k1gFnPc4	poušť
nicméně	nicméně	k8xC	nicméně
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
pánve	pánev	k1gFnPc4	pánev
a	a	k8xC	a
přinejmenším	přinejmenším	k6eAd1	přinejmenším
z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Théodore	Théodor	k1gMnSc5	Théodor
Monod	Monoda	k1gFnPc2	Monoda
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
omezil	omezit	k5eAaPmAgInS	omezit
Gobi	Gobi	k1gNnSc3	Gobi
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
a	a	k8xC	a
sever	sever	k1gInSc1	sever
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgNnSc4d1	pouštní
pohoří	pohoří	k1gNnSc4	pohoří
Alašan	Alašana	k1gFnPc2	Alašana
a	a	k8xC	a
Pej-šan	Pej-šana	k1gFnPc2	Pej-šana
na	na	k7c6	na
západě	západ	k1gInSc6	západ
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
už	už	k6eAd1	už
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatné	samostatný	k2eAgInPc4d1	samostatný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Gobi	Gobi	k1gFnSc2	Gobi
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
severomongolskou	severomongolský	k2eAgFnSc4d1	severomongolský
step	step	k1gFnSc4	step
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ji	on	k3xPp3gFnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
Gobi	Gobi	k1gFnSc1	Gobi
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
70	[number]	k4	70
km	km	kA	km
k	k	k7c3	k
Pekingu	Peking	k1gInSc3	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
300	[number]	k4	300
000	[number]	k4	000
km2	km2	k4	km2
se	se	k3xPyFc4	se
Gobi	Gobi	k1gFnSc1	Gobi
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
pouště	poušť	k1gFnPc4	poušť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
primát	primát	k1gInSc4	primát
si	se	k3xPyFc3	se
Gobi	Gobi	k1gNnSc1	Gobi
drží	držet	k5eAaImIp3nS	držet
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
geografickému	geografický	k2eAgNnSc3d1	geografické
postavení	postavení	k1gNnSc3	postavení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejseverněji	severně	k6eAd3	severně
položenou	položený	k2eAgFnSc4d1	položená
poušť	poušť	k1gFnSc4	poušť
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
prozkoumanou	prozkoumaný	k2eAgFnSc4d1	prozkoumaná
část	část	k1gFnSc4	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
<g/>
%	%	kIx~	%
plochy	plocha	k1gFnPc1	plocha
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
oblasti	oblast	k1gFnSc2	oblast
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
pánevné	pánevný	k2eAgFnSc6d1	pánevný
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
teplotách	teplota	k1gFnPc6	teplota
zde	zde	k6eAd1	zde
panujících	panující	k2eAgMnPc2d1	panující
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
Gobi	Gobi	k1gFnSc2	Gobi
rozhodně	rozhodně	k6eAd1	rozhodně
není	být	k5eNaImIp3nS	být
poušť	poušť	k1gFnSc1	poušť
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
monotónní	monotónní	k2eAgFnSc4d1	monotónní
polopoušť	polopoušť	k1gFnSc4	polopoušť
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
step	step	k1gInSc1	step
<g/>
.	.	kIx.	.
</s>
<s>
Pouhá	pouhý	k2eAgNnPc4d1	pouhé
tři	tři	k4xCgNnPc4	tři
procenta	procento	k1gNnPc4	procento
území	území	k1gNnSc6	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
typické	typický	k2eAgFnPc1d1	typická
písečné	písečný	k2eAgFnPc1d1	písečná
duny	duna	k1gFnPc1	duna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
els	els	k?	els
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
holá	holý	k2eAgFnSc1d1	holá
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pustině	pustina	k1gFnSc6	pustina
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jezery	jezero	k1gNnPc7	jezero
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
slanými	slaný	k2eAgInPc7d1	slaný
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejchladnějších	chladný	k2eAgFnPc2d3	nejchladnější
pouští	poušť	k1gFnPc2	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
mezi	mezi	k7c7	mezi
-40	-40	k4	-40
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
až	až	k9	až
po	po	k7c4	po
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
srážky	srážka	k1gFnPc1	srážka
nepřesáhnou	přesáhnout	k5eNaPmIp3nP	přesáhnout
76	[number]	k4	76
mm	mm	kA	mm
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
řadí	řadit	k5eAaImIp3nS	řadit
poušť	poušť	k1gFnSc1	poušť
mezi	mezi	k7c4	mezi
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejsušších	suchý	k2eAgFnPc2d3	nejsušší
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
obydlenosti	obydlenost	k1gFnSc6	obydlenost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
extrémní	extrémní	k2eAgInPc4d1	extrémní
parametry	parametr	k1gInPc4	parametr
však	však	k9	však
nemůžeme	moct	k5eNaImIp1nP	moct
na	na	k7c4	na
poušť	poušť	k1gFnSc4	poušť
Gobi	Gobi	k1gFnSc4	Gobi
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c6	na
nekonečné	konečný	k2eNgFnSc6d1	nekonečná
oblasti	oblast	k1gFnSc6	oblast
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
(	(	kIx(	(
<g/>
písečné	písečný	k2eAgFnPc1d1	písečná
oblasti	oblast	k1gFnPc1	oblast
zabírají	zabírat	k5eAaImIp3nP	zabírat
maximálně	maximálně	k6eAd1	maximálně
3	[number]	k4	3
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
)	)	kIx)	)
-	-	kIx~	-
setkáváme	setkávat	k5eAaImIp1nP	setkávat
i	i	k9	i
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
biodiverzitou	biodiverzita	k1gFnSc7	biodiverzita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
obrovském	obrovský	k2eAgNnSc6d1	obrovské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nechybí	chybět	k5eNaImIp3nS	chybět
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
lesy	les	k1gInPc4	les
či	či	k8xC	či
stepi	step	k1gFnPc4	step
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
vzniku	vznik	k1gInSc2	vznik
pouště	poušť	k1gFnSc2	poušť
sahá	sahat	k5eAaImIp3nS	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
300	[number]	k4	300
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
v	v	k7c6	v
karbonu	karbon	k1gInSc6	karbon
byla	být	k5eAaImAgFnS	být
Gobi	Gobi	k1gFnSc4	Gobi
mořským	mořský	k2eAgInSc7d1	mořský
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potom	potom	k6eAd1	potom
nastala	nastat	k5eAaPmAgFnS	nastat
horotvorná	horotvorný	k2eAgFnSc1d1	horotvorná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vyvrásnění	vyvrásnění	k1gNnSc2	vyvrásnění
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Altaj	Altaj	k1gInSc1	Altaj
či	či	k8xC	či
Ťan-šan	Ťan-šan	k1gInSc1	Ťan-šan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
geologické	geologický	k2eAgFnSc6d1	geologická
etapě	etapa	k1gFnSc6	etapa
druhohorách	druhohory	k1gFnPc6	druhohory
se	se	k3xPyFc4	se
Gobi	Gobi	k1gFnSc1	Gobi
stala	stát	k5eAaPmAgFnS	stát
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
hojných	hojný	k2eAgInPc6d1	hojný
počtech	počet	k1gInPc6	počet
obývali	obývat	k5eAaImAgMnP	obývat
také	také	k9	také
neptačí	ptačí	k2eNgMnPc1d1	neptačí
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yQgFnPc6	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaBmF	nalézt
mnoho	mnoho	k4c4	mnoho
fosilních	fosilní	k2eAgInPc2d1	fosilní
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Proslulé	proslulý	k2eAgFnPc1d1	proslulá
paleontologické	paleontologický	k2eAgFnPc1d1	paleontologická
expedice	expedice	k1gFnPc1	expedice
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
byly	být	k5eAaImAgFnP	být
zahájeny	zahájen	k2eAgMnPc4d1	zahájen
Američany	Američan	k1gMnPc4	Američan
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
období	období	k1gNnSc2	období
křídy	křída	k1gFnSc2	křída
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
65	[number]	k4	65
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
dramatickým	dramatický	k2eAgFnPc3d1	dramatická
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
změnu	změna	k1gFnSc4	změna
srážkového	srážkový	k2eAgInSc2d1	srážkový
režimu	režim	k1gInSc2	režim
-	-	kIx~	-
výrazné	výrazný	k2eAgNnSc1d1	výrazné
omezení	omezení	k1gNnSc1	omezení
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c6	na
druhotném	druhotný	k2eAgNnSc6d1	druhotné
vyschnutí	vyschnutí	k1gNnSc6	vyschnutí
řečišť	řečiště	k1gNnPc2	řečiště
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
biotopu	biotop	k1gInSc2	biotop
z	z	k7c2	z
bujné	bujný	k2eAgFnSc2d1	bujná
vegetace	vegetace	k1gFnSc2	vegetace
na	na	k7c4	na
trávu	tráva	k1gFnSc4	tráva
a	a	k8xC	a
keře	keř	k1gInPc4	keř
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flora	flora	k1gFnSc1	flora
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
pouštní	pouštní	k2eAgFnSc6d1	pouštní
oblasti	oblast	k1gFnSc6	oblast
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nS	patřit
stepní	stepní	k2eAgFnSc1d1	stepní
tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
tamaryšek	tamaryšek	k1gInSc1	tamaryšek
<g/>
,	,	kIx,	,
divoký	divoký	k2eAgInSc1d1	divoký
tymián	tymián	k1gInSc1	tymián
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
říše	říš	k1gFnSc2	říš
živočišné	živočišný	k2eAgFnSc2d1	živočišná
máme	mít	k5eAaImIp1nP	mít
možnosti	možnost	k1gFnPc4	možnost
potkat	potkat	k5eAaPmF	potkat
ovce	ovce	k1gFnPc4	ovce
argali	argat	k5eAaImAgMnP	argat
<g/>
,	,	kIx,	,
sněžného	sněžný	k2eAgMnSc4d1	sněžný
leoparda	leopard	k1gMnSc4	leopard
<g/>
,	,	kIx,	,
kozorožce	kozorožec	k1gMnSc4	kozorožec
horského	horský	k2eAgMnSc4d1	horský
<g/>
,	,	kIx,	,
gobijského	gobijský	k2eAgMnSc4d1	gobijský
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc1d1	vzácný
druh	druh	k1gInSc1	druh
koně	kůň	k1gMnSc2	kůň
Převalského	převalský	k2eAgMnSc2d1	převalský
a	a	k8xC	a
mnohé	mnohý	k2eAgNnSc1d1	mnohé
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
bohatému	bohatý	k2eAgInSc3d1	bohatý
životu	život	k1gInSc3	život
a	a	k8xC	a
množství	množství	k1gNnSc3	množství
ojedinělých	ojedinělý	k2eAgInPc2d1	ojedinělý
druhů	druh	k1gInPc2	druh
byly	být	k5eAaImAgFnP	být
velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
Gobi	Gobi	k1gFnSc2	Gobi
prohlášeny	prohlášen	k2eAgFnPc1d1	prohlášena
za	za	k7c4	za
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Velká	velký	k2eAgFnSc1d1	velká
gobijská	gobijský	k2eAgFnSc1d1	gobijský
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
pouště	poušť	k1gFnSc2	poušť
Gobi	Gobi	k1gFnSc2	Gobi
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
5	[number]	k4	5
300	[number]	k4	300
000	[number]	k4	000
ha	ha	kA	ha
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
nalezištěm	naleziště	k1gNnSc7	naleziště
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
tyrkysu	tyrkys	k1gInSc2	tyrkys
<g/>
,	,	kIx,	,
achátu	achát	k1gInSc2	achát
<g/>
,	,	kIx,	,
křišťálu	křišťál	k1gInSc2	křišťál
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poušti	poušť	k1gFnSc3	poušť
Gobi	Gobi	k1gFnSc2	Gobi
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
tajemném	tajemný	k2eAgMnSc6d1	tajemný
živočichu	živočich	k1gMnSc6	živočich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
nářečí	nářečí	k1gNnSc6	nářečí
Olgoj	Olgoj	k1gInSc1	Olgoj
Chorchoj	Chorchoj	k1gInSc1	Chorchoj
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
1	[number]	k4	1
až	až	k9	až
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
dlouhého	dlouhý	k2eAgMnSc2d1	dlouhý
slepého	slepý	k2eAgMnSc2d1	slepý
červa	červ	k1gMnSc2	červ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
žít	žít	k5eAaImF	žít
v	v	k7c6	v
písečných	písečný	k2eAgFnPc6d1	písečná
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
mít	mít	k5eAaImF	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
i	i	k8xC	i
lidská	lidský	k2eAgNnPc4d1	lidské
úmrtí	úmrtí	k1gNnSc4	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gobi	Gobi	k1gFnSc2	Gobi
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Článek	článek	k1gInSc4	článek
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
o	o	k7c6	o
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
pouště	poušť	k1gFnSc2	poušť
Gobi	Gobi	k1gFnSc2	Gobi
</s>
