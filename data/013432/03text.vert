<p>
<s>
Norbert	Norbert	k1gMnSc1	Norbert
Javůrek	Javůrek	k1gMnSc1	Javůrek
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1839	[number]	k4	1839
Vojnův	Vojnův	k2eAgInSc1d1	Vojnův
Městec	Městec	k1gInSc1	Městec
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
Split	Split	k1gInSc1	Split
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Organizoval	organizovat	k5eAaBmAgMnS	organizovat
koncerty	koncert	k1gInPc4	koncert
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
harmonizací	harmonizace	k1gFnSc7	harmonizace
Sušilových	Sušilův	k2eAgFnPc2d1	Sušilova
moravských	moravský	k2eAgFnPc2d1	Moravská
národních	národní	k2eAgFnPc2d1	národní
písní	píseň	k1gFnPc2	píseň
i	i	k9	i
některými	některý	k3yIgFnPc7	některý
vlastními	vlastní	k2eAgFnPc7d1	vlastní
skladbami	skladba	k1gFnPc7	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1839	[number]	k4	1839
ve	v	k7c4	v
Vojnově	vojnově	k6eAd1	vojnově
Městci	Městec	k1gInSc5	Městec
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
měl	mít	k5eAaImAgInS	mít
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
–	–	k?	–
v	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
skladby	skladba	k1gFnSc2	skladba
Beethovena	Beethoven	k1gMnSc2	Beethoven
a	a	k8xC	a
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidal	přidat	k5eAaPmAgMnS	přidat
Dreyschocka	Dreyschocko	k1gNnPc4	Dreyschocko
<g/>
,	,	kIx,	,
Thalberga	Thalberga	k1gFnSc1	Thalberga
a	a	k8xC	a
Liszta	Liszta	k1gMnSc1	Liszta
<g/>
.	.	kIx.	.
<g/>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rovněž	rovněž	k9	rovněž
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
medicinu	medicin	k2eAgFnSc4d1	medicina
na	na	k7c6	na
Josefínské	josefínský	k2eAgFnSc6d1	josefínská
akademii	akademie	k1gFnSc6	akademie
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc6	rok
1860	[number]	k4	1860
získal	získat	k5eAaPmAgInS	získat
místo	místo	k1gNnSc4	místo
jako	jako	k8xS	jako
c.	c.	k?	c.
k.	k.	k?	k.
vrchní	vrchní	k2eAgMnSc1d1	vrchní
lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgMnS	zapojit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
do	do	k7c2	do
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtenářském	čtenářský	k2eAgInSc6d1	čtenářský
spolku	spolek	k1gInSc6	spolek
pořádal	pořádat	k5eAaImAgMnS	pořádat
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
rovněž	rovněž	k6eAd1	rovněž
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
improvizovanými	improvizovaný	k2eAgFnPc7d1	improvizovaná
skladbami	skladba	k1gFnPc7	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
byly	být	k5eAaImAgFnP	být
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gInPc1	jeho
Kozácké	kozácký	k2eAgInPc4d1	kozácký
večery	večer	k1gInPc4	večer
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
maloruské	maloruský	k2eAgFnPc4d1	maloruská
(	(	kIx(	(
<g/>
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
<g/>
)	)	kIx)	)
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
;	;	kIx,	;
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
také	také	k6eAd1	také
písním	píseň	k1gFnPc3	píseň
bulharským	bulharský	k2eAgFnPc3d1	bulharská
<g/>
,	,	kIx,	,
ruským	ruský	k2eAgFnPc3d1	ruská
a	a	k8xC	a
srbským	srbský	k2eAgFnPc3d1	Srbská
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtenářském	čtenářský	k2eAgInSc6d1	čtenářský
spolku	spolek	k1gInSc6	spolek
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Sušilem	Sušil	k1gMnSc7	Sušil
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
harmonizovat	harmonizovat	k5eAaBmF	harmonizovat
jím	jíst	k5eAaImIp1nS	jíst
sebrané	sebraný	k2eAgFnPc4d1	sebraná
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Sušil	sušit	k5eAaImAgMnS	sušit
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInPc4	jeho
výsledky	výsledek	k1gInPc4	výsledek
nadšený	nadšený	k2eAgInSc1d1	nadšený
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
upravit	upravit	k5eAaPmF	upravit
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
skladbu	skladba	k1gFnSc4	skladba
Putovali	putovat	k5eAaImAgMnP	putovat
hudci	hudec	k1gMnPc1	hudec
<g/>
.	.	kIx.	.
</s>
<s>
Javůrkovy	Javůrkův	k2eAgFnPc1d1	Javůrkova
úpravy	úprava	k1gFnPc1	úprava
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
Sušilova	Sušilův	k2eAgNnSc2d1	Sušilovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Beseda	beseda	k1gFnSc1	beseda
brněnská	brněnský	k2eAgFnSc1d1	brněnská
vydala	vydat	k5eAaPmAgFnS	vydat
dva	dva	k4xCgInPc4	dva
sešity	sešit	k1gInPc4	sešit
těchto	tento	k3xDgFnPc2	tento
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velké	velký	k2eAgInPc4d1	velký
náklady	náklad	k1gInPc4	náklad
se	se	k3xPyFc4	se
ale	ale	k9	ale
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
vydání	vydání	k1gNnPc2	vydání
muselo	muset	k5eAaImAgNnS	muset
upustit	upustit	k5eAaPmF	upustit
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
veřejně	veřejně	k6eAd1	veřejně
činný	činný	k2eAgMnSc1d1	činný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc3	první
slovanské	slovanský	k2eAgFnSc3d1	Slovanská
besedě	beseda	k1gFnSc3	beseda
tam	tam	k6eAd1	tam
složil	složit	k5eAaPmAgMnS	složit
píseň	píseň	k1gFnSc4	píseň
Na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
autorem	autor	k1gMnSc7	autor
skladeb	skladba	k1gFnPc2	skladba
Píseň	píseň	k1gFnSc1	píseň
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
nebo	nebo	k8xC	nebo
Na	na	k7c4	na
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
jako	jako	k8xC	jako
operní	operní	k2eAgMnSc1d1	operní
referent	referent	k1gMnSc1	referent
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
korespondentem	korespondent	k1gMnSc7	korespondent
Moravské	moravský	k2eAgFnSc2d1	Moravská
orlice	orlice	k1gFnSc2	orlice
v	v	k7c6	v
prusko-rakouské	pruskoakouský	k2eAgFnSc6d1	prusko-rakouská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgMnS	být
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
města	město	k1gNnSc2	město
Budva	Budv	k1gMnSc2	Budv
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejjižnější	jižní	k2eAgFnSc6d3	nejjižnější
části	část	k1gFnSc6	část
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
tam	tam	k6eAd1	tam
jako	jako	k9	jako
plukovní	plukovní	k2eAgMnSc1d1	plukovní
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
sbíral	sbírat	k5eAaImAgMnS	sbírat
srbské	srbský	k2eAgFnSc2d1	Srbská
lidové	lidový	k2eAgFnSc2d1	lidová
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
oblibu	obliba	k1gFnSc4	obliba
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
oceněn	ocenit	k5eAaPmNgInS	ocenit
zlatým	zlatý	k2eAgInSc7d1	zlatý
záslužným	záslužný	k2eAgInSc7d1	záslužný
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
medailí	medaile	k1gFnSc7	medaile
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
potlačení	potlačení	k1gNnSc3	potlačení
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Budvě	Budva	k1gFnSc6	Budva
<g/>
:	:	kIx,	:
Když	když	k8xS	když
se	se	k3xPyFc4	se
povstalci	povstalec	k1gMnPc1	povstalec
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
k	k	k7c3	k
městu	město	k1gNnSc3	město
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
vojáků	voják	k1gMnPc2	voják
byla	být	k5eAaImAgFnS	být
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
i	i	k9	i
se	s	k7c7	s
zraněnými	zraněný	k1gMnPc7	zraněný
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
na	na	k7c4	na
stráž	stráž	k1gFnSc4	stráž
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
místní	místní	k2eAgFnSc1d1	místní
posádka	posádka	k1gFnSc1	posádka
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
povstalci	povstalec	k1gMnPc1	povstalec
nezaútočili	zaútočit	k5eNaPmAgMnP	zaútočit
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
jej	on	k3xPp3gInSc4	on
přeložili	přeložit	k5eAaPmAgMnP	přeložit
do	do	k7c2	do
Mariboru	Maribor	k1gInSc2	Maribor
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Judenburgu	Judenburg	k1gInSc2	Judenburg
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
tam	tam	k6eAd1	tam
šťastně	šťastně	k6eAd1	šťastně
až	až	k9	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
kvůli	kvůli	k7c3	kvůli
závisti	závist	k1gFnSc3	závist
nadřízeného	nadřízený	k2eAgInSc2d1	nadřízený
převeleli	převelet	k5eAaPmAgMnP	převelet
do	do	k7c2	do
Brucku	Bruck	k1gInSc2	Bruck
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
přesun	přesun	k1gInSc4	přesun
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
posádce	posádka	k1gFnSc3	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
armády	armáda	k1gFnSc2	armáda
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnPc4	jeho
žádosti	žádost	k1gFnPc4	žádost
příznivě	příznivě	k6eAd1	příznivě
nakloněno	nakloněn	k2eAgNnSc1d1	nakloněno
<g/>
;	;	kIx,	;
než	než	k8xS	než
ale	ale	k9	ale
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
projevila	projevit	k5eAaPmAgFnS	projevit
se	se	k3xPyFc4	se
u	u	k7c2	u
Javůrka	Javůrek	k1gMnSc2	Javůrek
duševní	duševní	k2eAgFnSc1d1	duševní
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
časově	časově	k6eAd1	časově
neomezenou	omezený	k2eNgFnSc4d1	neomezená
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
léčení	léčení	k1gNnSc6	léčení
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
mírně	mírně	k6eAd1	mírně
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1879	[number]	k4	1879
navštívil	navštívit	k5eAaPmAgMnS	navštívit
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
přitom	přitom	k6eAd1	přitom
u	u	k7c2	u
bratra	bratr	k1gMnSc2	bratr
Antonína	Antonín	k1gMnSc2	Antonín
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc4d1	místní
spolek	spolek	k1gInSc4	spolek
mu	on	k3xPp3gMnSc3	on
chtěl	chtít	k5eAaImAgMnS	chtít
uspořádat	uspořádat	k5eAaPmF	uspořádat
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
silnými	silný	k2eAgInPc7d1	silný
dojmy	dojem	k1gInPc7	dojem
neutrpěla	utrpět	k5eNaPmAgFnS	utrpět
jeho	jeho	k3xOp3gFnSc1	jeho
psychika	psychika	k1gFnSc1	psychika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dál	daleko	k6eAd2	daleko
skládal	skládat	k5eAaImAgMnS	skládat
a	a	k8xC	a
harmonizoval	harmonizovat	k5eAaBmAgMnS	harmonizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
díla	dílo	k1gNnPc1	dílo
s	s	k7c7	s
námětem	námět	k1gInSc7	námět
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
Slavkova	Slavkov	k1gInSc2	Slavkov
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
manželky	manželka	k1gFnSc2	manželka
do	do	k7c2	do
Splitu	Split	k1gInSc2	Split
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
zdánlivě	zdánlivě	k6eAd1	zdánlivě
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
přijímat	přijímat	k5eAaImF	přijímat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
tam	tam	k6eAd1	tam
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
kruhu	kruh	k1gInSc6	kruh
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
knižně	knižně	k6eAd1	knižně
vydaných	vydaný	k2eAgFnPc2d1	vydaná
prací	práce	k1gFnPc2	práce
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgFnPc1d1	Moravská
národní	národní	k2eAgFnPc1d1	národní
písně	píseň	k1gFnPc1	píseň
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Fr.	Fr.	k1gFnSc2	Fr.
Sušila	sušit	k5eAaImAgFnS	sušit
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
národní	národní	k2eAgFnPc1d1	národní
písně	píseň	k1gFnPc1	píseň
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
na	na	k7c6	na
Slezsku	Slezsko	k1gNnSc6	Slezsko
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
P.	P.	kA	P.
Fr.	Fr.	k1gFnSc1	Fr.
Sušila	sušit	k5eAaImAgFnS	sušit
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oceňovaná	oceňovaný	k2eAgFnSc1d1	oceňovaná
sbírka	sbírka	k1gFnSc1	sbírka
75	[number]	k4	75
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
nákladem	náklad	k1gInSc7	náklad
Matice	matice	k1gFnSc2	matice
hudební	hudební	k2eAgFnSc2d1	hudební
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
materiálů	materiál	k1gInPc2	materiál
z	z	k7c2	z
Digitálního	digitální	k2eAgInSc2d1	digitální
archivu	archiv	k1gInSc2	archiv
časopisů	časopis	k1gInPc2	časopis
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.	v.	k?	v.
v.	v.	k?	v.
i.	i.	k?	i.
(	(	kIx(	(
<g/>
http://archiv.ucl.cas.cz/	[url]	k?	http://archiv.ucl.cas.cz/
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
http://kramerius.nkp.cz	[url]	k1gMnSc1	http://kramerius.nkp.cz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Norbert	Norberta	k1gFnPc2	Norberta
Javůrek	javůrek	k1gInSc4	javůrek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Norbert	Norbert	k1gMnSc1	Norbert
Javůrek	Javůrek	k1gMnSc1	Javůrek
</s>
</p>
