<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
algebraická	algebraický	k2eAgFnSc1d1	algebraická
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
a	a	k8xC	a
formalizuje	formalizovat	k5eAaBmIp3nS	formalizovat
koncept	koncept	k1gInSc4	koncept
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
jako	jako	k9	jako
množina	množina	k1gFnSc1	množina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
binární	binární	k2eAgFnSc7d1	binární
operací	operace	k1gFnSc7	operace
splňující	splňující	k2eAgInPc1d1	splňující
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgInPc1d1	uvedený
axiomy	axiom	k1gInPc1	axiom
<g/>
.	.	kIx.	.
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
grup	grupa	k1gFnPc2	grupa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
grup	grupa	k1gFnPc2	grupa
jsou	být	k5eAaImIp3nP	být
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
sčítání	sčítání	k1gNnPc1	sčítání
<g/>
,	,	kIx,	,
nenulová	nulový	k2eNgNnPc1d1	nenulové
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
násobení	násobení	k1gNnSc1	násobení
<g/>
,	,	kIx,	,
symetrie	symetrie	k1gFnSc1	symetrie
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
geometrických	geometrický	k2eAgInPc2d1	geometrický
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
množiny	množina	k1gFnSc2	množina
regulárních	regulární	k2eAgFnPc2d1	regulární
matic	matice	k1gFnPc2	matice
a	a	k8xC	a
automorfismy	automorfismus	k1gInPc4	automorfismus
různých	různý	k2eAgFnPc2d1	různá
algebraických	algebraický	k2eAgFnPc2d1	algebraická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jejího	její	k3xOp3gInSc2	její
zrodu	zrod	k1gInSc2	zrod
stál	stát	k5eAaImAgMnS	stát
matematik	matematik	k1gMnSc1	matematik
Évariste	Évarist	k1gMnSc5	Évarist
Galois	Galois	k1gInSc1	Galois
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
polynomiální	polynomiální	k2eAgFnPc1d1	polynomiální
rovnice	rovnice	k1gFnPc1	rovnice
nelze	lze	k6eNd1	lze
obecně	obecně	k6eAd1	obecně
řešit	řešit	k5eAaImF	řešit
pomocí	pomocí	k7c2	pomocí
odmocnin	odmocnina	k1gFnPc2	odmocnina
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
našly	najít	k5eAaPmAgFnP	najít
později	pozdě	k6eAd2	pozdě
uplatnění	uplatnění	k1gNnSc4	uplatnění
také	také	k6eAd1	také
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
algebraické	algebraický	k2eAgFnSc6d1	algebraická
topologii	topologie	k1gFnSc6	topologie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
matematických	matematický	k2eAgInPc6d1	matematický
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
výsledkům	výsledek	k1gInPc3	výsledek
matematiky	matematika	k1gFnSc2	matematika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
grupy	grupa	k1gFnSc2	grupa
abstraktně	abstraktně	k6eAd1	abstraktně
popisuje	popisovat	k5eAaImIp3nS	popisovat
či	či	k8xC	či
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
mnoho	mnoho	k4c1	mnoho
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
významné	významný	k2eAgNnSc4d1	významné
uplatnění	uplatnění	k1gNnSc4	uplatnění
i	i	k9	i
v	v	k7c6	v
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
oborech	obor	k1gInPc6	obor
–	–	k?	–
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
informatice	informatika	k1gFnSc6	informatika
a	a	k8xC	a
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentace	reprezentace	k1gFnPc1	reprezentace
grup	grupa	k1gFnPc2	grupa
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
teoriích	teorie	k1gFnPc6	teorie
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
částicová	částicový	k2eAgFnSc1d1	částicová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
anebo	anebo	k8xC	anebo
teorie	teorie	k1gFnSc2	teorie
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
se	se	k3xPyFc4	se
grupy	grupa	k1gFnPc1	grupa
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
například	například	k6eAd1	například
v	v	k7c6	v
kryptografii	kryptografie	k1gFnSc6	kryptografie
<g/>
,	,	kIx,	,
kódování	kódování	k1gNnSc1	kódování
anebo	anebo	k8xC	anebo
zpracování	zpracování	k1gNnSc1	zpracování
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
používá	používat	k5eAaImIp3nS	používat
grupy	grupa	k1gFnPc4	grupa
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
symetrií	symetrie	k1gFnPc2	symetrie
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
krystalových	krystalový	k2eAgFnPc2d1	krystalová
mřížek	mřížka	k1gFnPc2	mřížka
v	v	k7c6	v
krystalografii	krystalografie	k1gFnSc6	krystalografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
grupy	grupa	k1gFnSc2	grupa
==	==	k?	==
</s>
</p>
<p>
<s>
Grupou	grupa	k1gFnSc7	grupa
nazýváme	nazývat	k5eAaImIp1nP	nazývat
množinu	množina	k1gFnSc4	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
binární	binární	k2eAgFnSc7d1	binární
operací	operace	k1gFnSc7	operace
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
grupová	grupový	k2eAgFnSc1d1	grupová
operace	operace	k1gFnSc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
libovolným	libovolný	k2eAgInSc7d1	libovolný
dvěma	dva	k4xCgInPc3	dva
prvkům	prvek	k1gInPc3	prvek
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
prvek	prvek	k1gInSc1	prvek
téže	tenže	k3xDgFnSc2	tenže
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Značení	značení	k1gNnSc1	značení
grupové	grupový	k2eAgFnSc2d1	grupová
operace	operace	k1gFnSc2	operace
se	se	k3xPyFc4	se
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
jako	jako	k9	jako
násobení	násobení	k1gNnSc1	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
resp.	resp.	kA	resp.
jenom	jenom	k9	jenom
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
ab	ab	k?	ab
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
v	v	k7c6	v
Abelových	Abelův	k2eAgFnPc6d1	Abelova
grupách	grupa	k1gFnPc6	grupa
často	často	k6eAd1	často
jako	jako	k8xC	jako
sčítání	sčítání	k1gNnSc4	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
pomocí	pomocí	k7c2	pomocí
dalších	další	k2eAgInPc2d1	další
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gFnSc1	circ
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
*	*	kIx~	*
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
součet	součet	k1gInSc1	součet
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
grupy	grupa	k1gFnSc2	grupa
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
grupová	grupový	k2eAgFnSc1d1	grupová
operace	operace	k1gFnSc1	operace
splňovala	splňovat	k5eAaImAgFnS	splňovat
určité	určitý	k2eAgFnPc4d1	určitá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
axiomy	axiom	k1gInPc1	axiom
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UzavřenostPro	UzavřenostPro	k6eAd1	UzavřenostPro
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
i	i	k9	i
složení	složení	k1gNnSc1	složení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
prvkem	prvek	k1gInSc7	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
AsociativitaPro	AsociativitaPro	k1gNnSc1	AsociativitaPro
všechny	všechen	k3xTgMnPc4	všechen
prvky	prvek	k1gInPc7	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
výsledek	výsledek	k1gInSc4	výsledek
složení	složení	k1gNnSc2	složení
tří	tři	k4xCgInPc2	tři
prvků	prvek	k1gInPc2	prvek
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
psát	psát	k5eAaImF	psát
složení	složení	k1gNnSc4	složení
tří	tři	k4xCgInPc2	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
i	i	k9	i
bez	bez	k7c2	bez
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
<g/>
Existence	existence	k1gFnSc2	existence
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
prvkuExistuje	prvkuExistovat	k5eAaPmIp3nS	prvkuExistovat
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
e	e	k0	e
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgInSc4d1	cdot
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
anebo	anebo	k8xC	anebo
jednotkový	jednotkový	k2eAgInSc1d1	jednotkový
prvek	prvek	k1gInSc1	prvek
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
také	také	k9	také
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
<g/>
Existence	existence	k1gFnPc4	existence
inverzního	inverzní	k2eAgInSc2d1	inverzní
prvkuPro	prvkuPro	k6eAd1	prvkuPro
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
existuje	existovat	k5eAaImIp3nS	existovat
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
jejich	jejich	k3xOp3gNnSc4	jejich
složení	složení	k1gNnSc4	složení
v	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc4d1	rovno
neutrálnímu	neutrální	k2eAgInSc3d1	neutrální
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
inverzní	inverzní	k2eAgInSc4d1	inverzní
prvek	prvek	k1gInSc4	prvek
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
jenom	jenom	k6eAd1	jenom
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
že	že	k9	že
inverzní	inverzní	k2eAgInSc4d1	inverzní
prvek	prvek	k1gInSc4	prvek
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
grupách	grupa	k1gFnPc6	grupa
obecně	obecně	k6eAd1	obecně
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
prvky	prvek	k1gInPc4	prvek
skládáme	skládat	k5eAaImIp1nP	skládat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obecně	obecně	k6eAd1	obecně
nemusí	muset	k5eNaImIp3nS	muset
platit	platit	k5eAaImF	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
tato	tento	k3xDgFnSc1	tento
rovnost	rovnost	k1gFnSc1	rovnost
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
komutativní	komutativní	k2eAgFnSc1d1	komutativní
grupa	grupa	k1gFnSc1	grupa
nebo	nebo	k8xC	nebo
také	také	k9	také
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nosič	nosič	k1gInSc1	nosič
nebo	nebo	k8xC	nebo
nosná	nosný	k2eAgFnSc1d1	nosná
množina	množina	k1gFnSc1	množina
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Označíme	označit	k5eAaPmIp1nP	označit
<g/>
-li	i	k?	-li
operaci	operace	k1gFnSc4	operace
jako	jako	k8xS	jako
sčítání	sčítání	k1gNnSc4	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
aditivní	aditivní	k2eAgFnSc6d1	aditivní
grupě	grupa	k1gFnSc6	grupa
a	a	k8xC	a
píšeme	psát	k5eAaImIp1nP	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
aditivní	aditivní	k2eAgFnSc1d1	aditivní
notace	notace	k1gFnSc1	notace
pro	pro	k7c4	pro
grupy	grupa	k1gFnPc4	grupa
Abelovy	Abelův	k2eAgFnPc4d1	Abelova
a	a	k8xC	a
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Označíme	označit	k5eAaPmIp1nP	označit
<g/>
-li	i	k?	-li
operaci	operace	k1gFnSc4	operace
jako	jako	k8xC	jako
násobení	násobení	k1gNnSc4	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
multiplikativní	multiplikativní	k2eAgFnSc6d1	multiplikativní
grupě	grupa	k1gFnSc6	grupa
a	a	k8xC	a
píšeme	psát	k5eAaImIp1nP	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
často	často	k6eAd1	často
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
}	}	kIx)	}
</s>
</p>
<p>
<s>
nepíše	psát	k5eNaImIp3nS	psát
a	a	k8xC	a
součin	součin	k1gInSc1	součin
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ab	ab	k?	ab
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
multiplikativní	multiplikativní	k2eAgFnSc2d1	multiplikativní
grupy	grupa	k1gFnSc2	grupa
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnPc4	definice
pomocí	pomocí	k7c2	pomocí
tří	tři	k4xCgFnPc2	tři
operací	operace	k1gFnPc2	operace
===	===	k?	===
</s>
</p>
<p>
<s>
Ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
lze	lze	k6eAd1	lze
grupu	grupa	k1gFnSc4	grupa
definovat	definovat	k5eAaBmF	definovat
pomocí	pomocí	k7c2	pomocí
</s>
</p>
<p>
<s>
nulární	nulární	k2eAgFnSc1d1	nulární
operace	operace	k1gFnSc1	operace
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
představující	představující	k2eAgInSc1d1	představující
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
unární	unární	k2eAgFnSc1d1	unární
operace	operace	k1gFnSc1	operace
−	−	k?	−
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
každému	každý	k3xTgInSc3	každý
prvku	prvek	k1gInSc3	prvek
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
prvek	prvek	k1gInSc1	prvek
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
inverzní	inverzní	k2eAgNnSc1d1	inverzní
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
<s>
binární	binární	k2eAgFnPc1d1	binární
operace	operace	k1gFnPc1	operace
<g/>
,	,	kIx,	,
<g/>
které	který	k3yRgFnPc1	který
splňují	splňovat	k5eAaImIp3nP	splňovat
axiomy	axiom	k1gInPc4	axiom
uvedené	uvedený	k2eAgFnSc2d1	uvedená
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
,0	,0	k4	,0
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Axiomy	axiom	k1gInPc1	axiom
grupy	grupa	k1gFnSc2	grupa
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
existenční	existenční	k2eAgInPc1d1	existenční
kvantifikátory	kvantifikátor	k1gInPc1	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
všech	všecek	k3xTgFnPc2	všecek
grup	grupa	k1gFnPc2	grupa
proto	proto	k8xC	proto
tvoří	tvořit	k5eAaImIp3nS	tvořit
varietu	varieta	k1gFnSc4	varieta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
lze	lze	k6eAd1	lze
na	na	k7c4	na
grupy	grupa	k1gFnPc4	grupa
vztáhnout	vztáhnout	k5eAaPmF	vztáhnout
mnohé	mnohý	k2eAgInPc1d1	mnohý
výsledky	výsledek	k1gInPc1	výsledek
dokázané	dokázaný	k2eAgInPc1d1	dokázaný
v	v	k7c6	v
univerzální	univerzální	k2eAgFnSc6d1	univerzální
algebře	algebra	k1gFnSc6	algebra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ilustrativní	ilustrativní	k2eAgInPc1d1	ilustrativní
příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejjednodušších	jednoduchý	k2eAgInPc2d3	nejjednodušší
příkladů	příklad	k1gInPc2	příklad
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
množině	množina	k1gFnSc6	množina
binární	binární	k2eAgFnSc2d1	binární
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
součtem	součet	k1gInSc7	součet
dvou	dva	k4xCgNnPc2	dva
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
je	být	k5eAaImIp3nS	být
asociativní	asociativní	k2eAgNnSc1d1	asociativní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
existuje	existovat	k5eAaImIp3nS	existovat
opačné	opačný	k2eAgNnSc4d1	opačné
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-a	-a	k?	-a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-a	-a	k?	-a
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
-a	-a	k?	-a
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Axiomy	axiom	k1gInPc1	axiom
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Dihedrální	Dihedrální	k2eAgFnSc1d1	Dihedrální
grupa	grupa	k1gFnSc1	grupa
D4	D4	k1gFnSc2	D4
===	===	k?	===
</s>
</p>
<p>
<s>
Symetrie	symetrie	k1gFnPc1	symetrie
čtverce	čtverec	k1gInSc2	čtverec
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
jako	jako	k9	jako
rotace	rotace	k1gFnPc1	rotace
<g/>
,	,	kIx,	,
zrcadlení	zrcadlení	k1gNnSc6	zrcadlení
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gNnSc4	jejich
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
čtverec	čtverec	k1gInSc1	čtverec
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
takových	takový	k3xDgFnPc2	takový
symetrií	symetrie	k1gFnPc2	symetrie
tvoří	tvořit	k5eAaImIp3nP	tvořit
grupu	grupa	k1gFnSc4	grupa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc4	osm
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc4	popis
těchto	tento	k3xDgFnPc2	tento
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Identita	identita	k1gFnSc1	identita
(	(	kIx(	(
<g/>
id	idy	k1gFnPc2	idy
<g/>
)	)	kIx)	)
nechává	nechávat	k5eAaImIp3nS	nechávat
čtverec	čtverec	k1gInSc1	čtverec
nezměněn	změnit	k5eNaPmNgInS	změnit
</s>
</p>
<p>
<s>
Rotace	rotace	k1gFnSc1	rotace
čtverce	čtverec	k1gInSc2	čtverec
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
a	a	k8xC	a
270	[number]	k4	270
<g/>
°	°	k?	°
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Překlopení	překlopení	k1gNnSc1	překlopení
(	(	kIx(	(
<g/>
také	také	k9	také
reflexe	reflexe	k1gFnSc1	reflexe
nebo	nebo	k8xC	nebo
zrcadlení	zrcadlení	k1gNnSc1	zrcadlení
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
vertikální	vertikální	k2eAgFnSc2d1	vertikální
a	a	k8xC	a
horizontální	horizontální	k2eAgFnSc2d1	horizontální
střední	střední	k2eAgFnSc2d1	střední
úsečky	úsečka	k1gFnSc2	úsečka
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgFnPc2	dva
diagonál	diagonála	k1gFnPc2	diagonála
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Binární	binární	k2eAgFnSc4d1	binární
operaci	operace	k1gFnSc4	operace
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
grupě	grupa	k1gFnSc6	grupa
definujeme	definovat	k5eAaBmIp1nP	definovat
jako	jako	k9	jako
skládání	skládání	k1gNnSc4	skládání
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
:	:	kIx,	:
osm	osm	k4xCc1	osm
symetrií	symetrie	k1gFnPc2	symetrie
jsou	být	k5eAaImIp3nP	být
zobrazení	zobrazený	k2eAgMnPc1d1	zobrazený
ze	z	k7c2	z
čtverce	čtverec	k1gInSc2	čtverec
na	na	k7c4	na
čtverec	čtverec	k1gInSc4	čtverec
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
symetrie	symetrie	k1gFnPc1	symetrie
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
složit	složit	k5eAaPmF	složit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledek	výsledek	k1gInSc1	výsledek
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
symetrie	symetrie	k1gFnSc1	symetrie
čtverce	čtverec	k1gInSc2	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
operace	operace	k1gFnSc2	operace
"	"	kIx"	"
<g/>
nejdříve	dříve	k6eAd3	dříve
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
značení	značení	k1gNnSc1	značení
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
skládání	skládání	k1gNnSc4	skládání
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
vpravo	vpravo	k6eAd1	vpravo
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
výsledky	výsledek	k1gInPc4	výsledek
všech	všecek	k3xTgNnPc2	všecek
možných	možný	k2eAgNnPc2d1	možné
složení	složení	k1gNnPc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výsledek	výsledek	k1gInSc1	výsledek
složení	složení	k1gNnSc2	složení
rotace	rotace	k1gFnSc2	rotace
o	o	k7c4	o
270	[number]	k4	270
<g/>
°	°	k?	°
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
a	a	k8xC	a
horizontálního	horizontální	k2eAgNnSc2d1	horizontální
překlopení	překlopení	k1gNnSc2	překlopení
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgMnSc1d1	stejný
jako	jako	k8xS	jako
překlopení	překlopení	k1gNnSc1	překlopení
kolem	kolem	k7c2	kolem
diagonály	diagonála	k1gFnSc2	diagonála
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
zvýrazněno	zvýraznit	k5eAaPmNgNnS	zvýraznit
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
grupa	grupa	k1gFnSc1	grupa
není	být	k5eNaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≠	≠	k?	≠
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Koncept	koncept	k1gInSc1	koncept
grupy	grupa	k1gFnSc2	grupa
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
oblastí	oblast	k1gFnPc2	oblast
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
motivace	motivace	k1gFnSc1	motivace
pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
grup	grupa	k1gFnPc2	grupa
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
řešit	řešit	k5eAaImF	řešit
polynomiální	polynomiální	k2eAgFnPc4d1	polynomiální
rovnice	rovnice	k1gFnPc4	rovnice
stupně	stupeň	k1gInSc2	stupeň
vyššího	vysoký	k2eAgNnSc2d2	vyšší
než	než	k8xS	než
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kvadratické	kvadratický	k2eAgFnPc1d1	kvadratická
rovnice	rovnice	k1gFnPc1	rovnice
uměli	umět	k5eAaImAgMnP	umět
lidé	člověk	k1gMnPc1	člověk
řešit	řešit	k5eAaImF	řešit
už	už	k9	už
v	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
civilizacích	civilizace	k1gFnPc6	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Lodovico	Lodovico	k1gMnSc1	Lodovico
Ferrari	Ferrari	k1gMnSc1	Ferrari
uměl	umět	k5eAaImAgMnS	umět
řešit	řešit	k5eAaImF	řešit
polynomiální	polynomiální	k2eAgFnSc2d1	polynomiální
rovnice	rovnice	k1gFnSc2	rovnice
stupně	stupeň	k1gInSc2	stupeň
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnPc4	řešení
publikoval	publikovat	k5eAaBmAgMnS	publikovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Gerolamo	Gerolama	k1gFnSc5	Gerolama
Cardanem	Cardan	k1gInSc7	Cardan
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Ars	Ars	k1gMnSc2	Ars
Magna	Magn	k1gMnSc2	Magn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1545	[number]	k4	1545
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
polynomiální	polynomiální	k2eAgFnPc4d1	polynomiální
rovnice	rovnice	k1gFnPc4	rovnice
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
však	však	k9	však
obecně	obecně	k6eAd1	obecně
nelze	lze	k6eNd1	lze
řešení	řešení	k1gNnSc4	řešení
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
operace	operace	k1gFnSc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
odčítání	odčítání	k1gNnSc2	odčítání
<g/>
,	,	kIx,	,
násobení	násobení	k1gNnSc2	násobení
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc2	dělení
a	a	k8xC	a
odmocninu	odmocnina	k1gFnSc4	odmocnina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Historickou	historický	k2eAgFnSc7d1	historická
terminologií	terminologie	k1gFnSc7	terminologie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
řešení	řešení	k1gNnSc2	řešení
pomocí	pomocí	k7c2	pomocí
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
terminologie	terminologie	k1gFnSc1	terminologie
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
algebraicky	algebraicky	k6eAd1	algebraicky
řešitelné	řešitelný	k2eAgFnSc6d1	řešitelná
rovnici	rovnice	k1gFnSc6	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
Évariste	Évarist	k1gMnSc5	Évarist
Galois	Galois	k1gFnSc3	Galois
<g/>
,	,	kIx,	,
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
starší	starý	k2eAgFnPc4d2	starší
práce	práce	k1gFnPc4	práce
Ruffiniho	Ruffini	k1gMnSc2	Ruffini
a	a	k8xC	a
Lagrangeho	Lagrange	k1gMnSc2	Lagrange
<g/>
,	,	kIx,	,
nalezl	nalézt	k5eAaBmAgMnS	nalézt
kritérium	kritérium	k1gNnSc4	kritérium
pro	pro	k7c4	pro
algebraickou	algebraický	k2eAgFnSc4d1	algebraická
řešitelnost	řešitelnost	k1gFnSc4	řešitelnost
polynomiálních	polynomiální	k2eAgFnPc2d1	polynomiální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
takového	takový	k3xDgNnSc2	takový
řešení	řešení	k1gNnSc2	řešení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
grupě	grupa	k1gFnSc6	grupa
symetrií	symetrie	k1gFnPc2	symetrie
kořenů	kořen	k1gInPc2	kořen
daného	daný	k2eAgInSc2d1	daný
polynomu	polynom	k1gInSc2	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
jisté	jistý	k2eAgFnPc4d1	jistá
permutace	permutace	k1gFnPc4	permutace
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galoisovy	Galoisův	k2eAgFnPc1d1	Galoisova
myšlenky	myšlenka	k1gFnPc1	myšlenka
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnSc2	jeho
současníky	současník	k1gMnPc4	současník
odmítnuty	odmítnut	k2eAgMnPc4d1	odmítnut
a	a	k8xC	a
publikovány	publikován	k2eAgMnPc4d1	publikován
až	až	k9	až
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Obecnější	obecní	k2eAgFnPc1d2	obecní
permutační	permutační	k2eAgFnPc1d1	permutační
grupy	grupa	k1gFnPc1	grupa
byly	být	k5eAaImAgFnP	být
zkoumány	zkoumat	k5eAaImNgFnP	zkoumat
Augustinem	Augustin	k1gMnSc7	Augustin
Cauchym	Cauchymum	k1gNnPc2	Cauchymum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
definici	definice	k1gFnSc4	definice
konečné	konečný	k2eAgFnSc2d1	konečná
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
také	také	k9	také
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
grupa	grupa	k1gFnSc1	grupa
<g/>
"	"	kIx"	"
zavedl	zavést	k5eAaPmAgMnS	zavést
Arthur	Arthur	k1gMnSc1	Arthur
Cayley	Caylea	k1gFnSc2	Caylea
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
On	on	k3xPp3gMnSc1	on
the	the	k?	the
theory	theor	k1gInPc1	theor
of	of	k?	of
groups	groups	k1gInSc1	groups
<g/>
,	,	kIx,	,
as	as	k1gInSc1	as
depending	depending	k1gInSc1	depending
on	on	k3xPp3gMnSc1	on
the	the	k?	the
symbolic	symbolice	k1gFnPc2	symbolice
equation	equation	k1gInSc4	equation
θ	θ	k?	θ
=	=	kIx~	=
1	[number]	k4	1
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
byla	být	k5eAaImAgFnS	být
druhou	druhý	k4xOgFnSc7	druhý
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
byly	být	k5eAaImAgFnP	být
grupy	grupa	k1gFnPc1	grupa
systematicky	systematicky	k6eAd1	systematicky
využívány	využíván	k2eAgFnPc1d1	využívána
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
grupy	grupa	k1gFnPc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
geometrických	geometrický	k2eAgInPc2d1	geometrický
prostorů	prostor	k1gInPc2	prostor
zavedené	zavedený	k2eAgFnSc2d1	zavedená
Felixem	Felix	k1gMnSc7	Felix
Kleinem	Klein	k1gMnSc7	Klein
v	v	k7c6	v
Erlangenském	Erlangenský	k2eAgInSc6d1	Erlangenský
programu	program	k1gInSc6	program
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Klein	Klein	k1gMnSc1	Klein
využil	využít	k5eAaPmAgMnS	využít
teorii	teorie	k1gFnSc4	teorie
grup	grupa	k1gFnPc2	grupa
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
a	a	k8xC	a
kategorizaci	kategorizace	k1gFnSc4	kategorizace
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
objevivších	objevivší	k2eAgFnPc2d1	objevivší
geometrií	geometrie	k1gFnPc2	geometrie
jako	jako	k8xS	jako
hyperbolická	hyperbolický	k2eAgFnSc1d1	hyperbolická
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
projektivní	projektivní	k2eAgFnSc1d1	projektivní
geometrie	geometrie	k1gFnSc1	geometrie
a	a	k8xC	a
starší	starý	k2eAgFnSc1d2	starší
Eukleidova	Eukleidův	k2eAgFnSc1d1	Eukleidova
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tento	tento	k3xDgInSc4	tento
koncept	koncept	k1gInSc4	koncept
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
Sophus	Sophus	k1gMnSc1	Sophus
Lie	Lie	k1gMnSc1	Lie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
pojem	pojem	k1gInSc4	pojem
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
byla	být	k5eAaImAgFnS	být
teorie	teorie	k1gFnSc1	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgFnPc1d1	jistá
struktury	struktura	k1gFnPc1	struktura
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
Abelovým	Abelův	k2eAgFnPc3d1	Abelova
grupám	grupa	k1gFnPc3	grupa
byly	být	k5eAaImAgInP	být
implicitně	implicitně	k6eAd1	implicitně
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
Gaussově	Gaussův	k2eAgInSc6d1	Gaussův
číselně	číselně	k6eAd1	číselně
teoretickém	teoretický	k2eAgInSc6d1	teoretický
díle	díl	k1gInSc6	díl
Disquisitiones	Disquisitionesa	k1gFnPc2	Disquisitionesa
Arithmeticae	Arithmetica	k1gInSc2	Arithmetica
a	a	k8xC	a
explicitněji	explicitně	k6eAd2	explicitně
je	být	k5eAaImIp3nS	být
používal	používat	k5eAaImAgMnS	používat
i	i	k9	i
Leopold	Leopold	k1gMnSc1	Leopold
Kronecker	Kronecker	k1gMnSc1	Kronecker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Kummer	Kummer	k1gMnSc1	Kummer
v	v	k7c6	v
raných	raný	k2eAgInPc6d1	raný
pokusech	pokus	k1gInPc6	pokus
dokázat	dokázat	k5eAaPmF	dokázat
Velkou	velký	k2eAgFnSc4d1	velká
Fermatovu	Fermatův	k2eAgFnSc4d1	Fermatova
větu	věta	k1gFnSc4	věta
zavedl	zavést	k5eAaPmAgInS	zavést
grupy	grupa	k1gFnSc2	grupa
popisující	popisující	k2eAgInSc1d1	popisující
faktorizaci	faktorizace	k1gFnSc3	faktorizace
na	na	k7c4	na
prvočísla	prvočíslo	k1gNnPc4	prvočíslo
<g/>
.	.	kIx.	.
<g/>
Spojování	spojování	k1gNnSc4	spojování
těchto	tento	k3xDgInPc2	tento
různých	různý	k2eAgInPc2d1	různý
motivů	motiv	k1gInPc2	motiv
do	do	k7c2	do
jednotné	jednotný	k2eAgFnSc2d1	jednotná
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
začalo	začít	k5eAaPmAgNnS	začít
Jordanovou	Jordanův	k2eAgFnSc7d1	Jordanova
publikací	publikace	k1gFnSc7	publikace
Traité	Traitý	k2eAgNnSc1d1	Traité
des	des	k1gNnSc1	des
substitutions	substitutions	k1gInSc1	substitutions
et	et	k?	et
des	des	k1gNnSc2	des
équations	équationsa	k1gFnPc2	équationsa
algébriques	algébriquesa	k1gFnPc2	algébriquesa
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Walther	Walthra	k1gFnPc2	Walthra
von	von	k1gInSc1	von
Dyck	Dyck	k1gMnSc1	Dyck
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
zavedl	zavést	k5eAaPmAgInS	zavést
první	první	k4xOgFnSc4	první
moderní	moderní	k2eAgFnSc4d1	moderní
definici	definice	k1gFnSc4	definice
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
získaly	získat	k5eAaPmAgInP	získat
grupy	grupa	k1gFnPc4	grupa
široké	široký	k2eAgNnSc4d1	široké
přijetí	přijetí	k1gNnSc4	přijetí
díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Frobenia	Frobenium	k1gNnSc2	Frobenium
a	a	k8xC	a
Williama	William	k1gMnSc4	William
Burnsidea	Burnsideus	k1gMnSc4	Burnsideus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c4	na
teorii	teorie	k1gFnSc4	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
článkům	článek	k1gInPc3	článek
Richarda	Richard	k1gMnSc2	Richard
Brauera	Brauera	k1gFnSc1	Brauera
(	(	kIx(	(
<g/>
modulární	modulární	k2eAgFnSc1d1	modulární
teorie	teorie	k1gFnSc1	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
<g/>
)	)	kIx)	)
a	a	k8xC	a
Issaie	Issaie	k1gFnSc1	Issaie
Schura	Schura	k1gFnSc1	Schura
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
a	a	k8xC	a
obecněji	obecně	k6eAd2	obecně
lokálně	lokálně	k6eAd1	lokálně
kompaktních	kompaktní	k2eAgFnPc2d1	kompaktní
grup	grupa	k1gFnPc2	grupa
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
Hermannem	Hermann	k1gMnSc7	Hermann
Weylem	Weyl	k1gMnSc7	Weyl
<g/>
,	,	kIx,	,
Élie	Élie	k1gFnPc2	Élie
Cartanem	Cartan	k1gInSc7	Cartan
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
algebraický	algebraický	k2eAgInSc1d1	algebraický
protějšek	protějšek	k1gInSc1	protějšek
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
algebraických	algebraický	k2eAgFnPc2d1	algebraická
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
prvně	prvně	k?	prvně
popsána	popsán	k2eAgFnSc1d1	popsána
Chevalleyem	Chevalleyum	k1gNnSc7	Chevalleyum
(	(	kIx(	(
<g/>
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Armandem	Armand	k1gInSc7	Armand
Borelem	Borel	k1gInSc7	Borel
a	a	k8xC	a
Jacquesem	Jacques	k1gMnSc7	Jacques
Titsem	Tits	k1gMnSc7	Tits
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
61	[number]	k4	61
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
Rok	rok	k1gInSc1	rok
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
a	a	k8xC	a
teoretici	teoretik	k1gMnPc1	teoretik
jako	jako	k9	jako
Daniel	Daniel	k1gMnSc1	Daniel
Gorenstein	Gorenstein	k1gMnSc1	Gorenstein
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
G.	G.	kA	G.
Thompson	Thompson	k1gMnSc1	Thompson
a	a	k8xC	a
Walter	Walter	k1gMnSc1	Walter
Feit	Feit	k1gMnSc1	Feit
založili	založit	k5eAaPmAgMnP	založit
spolupráci	spolupráce	k1gFnSc6	spolupráce
která	který	k3yQgFnSc1	který
<g/>
,	,	kIx,	,
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
mnohých	mnohý	k2eAgMnPc2d1	mnohý
jiných	jiný	k2eAgMnPc2d1	jiný
matematiků	matematik	k1gMnPc2	matematik
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
klasifikaci	klasifikace	k1gFnSc3	klasifikace
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
předčil	předčit	k5eAaBmAgInS	předčit
předchozí	předchozí	k2eAgNnPc4d1	předchozí
matematická	matematický	k2eAgNnPc4d1	matematické
úsilí	úsilí	k1gNnSc4	úsilí
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
délkou	délka	k1gFnSc7	délka
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
tak	tak	k9	tak
počtem	počet	k1gInSc7	počet
zainteresovaných	zainteresovaný	k2eAgMnPc2d1	zainteresovaný
matematiků	matematik	k1gMnPc2	matematik
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
klasifikace	klasifikace	k1gFnSc1	klasifikace
hotova	hotov	k2eAgFnSc1d1	hotova
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
důkaz	důkaz	k1gInSc4	důkaz
této	tento	k3xDgFnSc2	tento
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
rozvíjející	rozvíjející	k2eAgFnSc1d1	rozvíjející
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
řadu	řada	k1gFnSc4	řada
souvisejících	související	k2eAgFnPc2d1	související
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
grupové	grupový	k2eAgInPc1d1	grupový
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kapitole	kapitola	k1gFnSc6	kapitola
budeme	být	k5eAaImBp1nP	být
pro	pro	k7c4	pro
grupovou	grupový	k2eAgFnSc4d1	grupová
operaci	operace	k1gFnSc4	operace
používat	používat	k5eAaImF	používat
symbol	symbol	k1gInSc4	symbol
pro	pro	k7c4	pro
součin	součin	k1gInSc4	součin
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc1	složení
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
budeme	být	k5eAaImBp1nP	být
značit	značit	k5eAaImF	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
budeme	být	k5eAaImBp1nP	být
používat	používat	k5eAaImF	používat
symbol	symbol	k1gInSc4	symbol
pro	pro	k7c4	pro
součet	součet	k1gInSc4	součet
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
a	a	k8xC	a
psát	psát	k5eAaImF	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Řád	řád	k1gInSc1	řád
prvku	prvek	k1gInSc2	prvek
a	a	k8xC	a
grupy	grupa	k1gFnSc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Řádem	řád	k1gInSc7	řád
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mohutnost	mohutnost	k1gFnSc1	mohutnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
G	G	kA	G
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
její	její	k3xOp3gFnPc1	její
nosné	nosný	k2eAgFnPc1d1	nosná
množiny	množina	k1gFnPc1	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řádem	řád	k1gInSc7	řád
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nejmenší	malý	k2eAgNnSc4d3	nejmenší
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
g	g	kA	g
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
součin	součin	k1gInSc1	součin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
krát	krát	k6eAd1	krát
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
takové	takový	k3xDgNnSc1	takový
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cyklická	cyklický	k2eAgFnSc1d1	cyklická
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
cyklická	cyklický	k2eAgFnSc1d1	cyklická
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
generována	generován	k2eAgFnSc1d1	generována
jedním	jeden	k4xCgInSc7	jeden
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
napsat	napsat	k5eAaPmF	napsat
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
nějaké	nějaký	k3yIgNnSc4	nějaký
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc1	výraz
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vynásoben	vynásoben	k2eAgMnSc1d1	vynásoben
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-n	-n	k?	-n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vynásoben	vynásoben	k2eAgMnSc1d1	vynásoben
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
krát	krát	k6eAd1	krát
pro	pro	k7c4	pro
nějaké	nějaký	k3yIgNnSc4	nějaký
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Konečnou	konečný	k2eAgFnSc4d1	konečná
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
grupu	grupa	k1gFnSc4	grupa
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
množinou	množina	k1gFnSc7	množina
řešení	řešení	k1gNnSc2	řešení
rovnice	rovnice	k1gFnPc1	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
znázorněno	znázorněn	k2eAgNnSc1d1	znázorněno
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Grupové	Grupový	k2eAgNnSc1d1	Grupový
násobení	násobení	k1gNnSc1	násobení
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
násobení	násobení	k1gNnSc1	násobení
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
reprezentaci	reprezentace	k1gFnSc4	reprezentace
představuje	představovat	k5eAaImIp3nS	představovat
množina	množina	k1gFnSc1	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
modulo	modout	k5eAaPmAgNnS	modout
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
grupa	grupa	k1gFnSc1	grupa
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
izomorfní	izomorfní	k2eAgFnSc3d1	izomorfní
grupě	grupa	k1gFnSc3	grupa
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
izomorfní	izomorfní	k2eAgFnSc3d1	izomorfní
množině	množina	k1gFnSc3	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Grupu	grupa	k1gFnSc4	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nazýváme	nazývat	k5eAaImIp1nP	nazývat
Abelovou	Abelová	k1gFnSc4	Abelová
(	(	kIx(	(
<g/>
také	také	k9	také
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
-li	i	k?	-li
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
norském	norský	k2eAgMnSc6d1	norský
matematikovi	matematik	k1gMnSc6	matematik
Henrikovi	Henrik	k1gMnSc6	Henrik
Abelovi	Abel	k1gMnSc6	Abel
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
jsou	být	k5eAaImIp3nP	být
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
množiny	množina	k1gFnPc1	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
vektorové	vektorový	k2eAgFnPc4d1	vektorová
prostory	prostora	k1gFnPc4	prostora
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
nenulová	nulový	k2eNgNnPc1d1	nenulové
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
násobení	násobení	k1gNnSc2	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
modul	modul	k1gInSc1	modul
nad	nad	k7c7	nad
okruhem	okruh	k1gInSc7	okruh
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
modul	modul	k1gInSc1	modul
nad	nad	k7c7	nad
okruhem	okruh	k1gInSc7	okruh
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečné	Konečné	k2eAgFnPc1d1	Konečné
Abelovy	Abelův	k2eAgFnPc1d1	Abelova
grupy	grupa	k1gFnPc1	grupa
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jednoduše	jednoduše	k6eAd1	jednoduše
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
konečná	konečný	k2eAgFnSc1d1	konečná
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
izomorfní	izomorfní	k2eAgFnSc3d1	izomorfní
direktní	direktní	k2eAgFnSc3d1	direktní
sumě	suma	k1gFnSc3	suma
cyklických	cyklický	k2eAgFnPc2d1	cyklická
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
řády	řád	k1gInPc1	řád
jsou	být	k5eAaImIp3nP	být
mocniny	mocnina	k1gFnPc4	mocnina
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
tohoto	tento	k3xDgNnSc2	tento
tvrzení	tvrzení	k1gNnSc2	tvrzení
popisuje	popisovat	k5eAaImIp3nS	popisovat
čínská	čínský	k2eAgFnSc1d1	čínská
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
zbytcích	zbytek	k1gInPc6	zbytek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
popsána	popsat	k5eAaPmNgFnS	popsat
už	už	k9	už
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Sun-c	Sun	k1gInSc1	Sun-c
<g/>
'	'	kIx"	'
suan-ťing	suan-ťing	k1gInSc1	suan-ťing
čínského	čínský	k2eAgMnSc2d1	čínský
matematika	matematik	k1gMnSc2	matematik
Sun-c	Sun	k1gFnSc1	Sun-c
<g/>
'	'	kIx"	'
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
.	.	kIx.	.
<g/>
Obecněji	obecně	k6eAd2	obecně
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
konečně	konečně	k6eAd1	konečně
generovaná	generovaný	k2eAgFnSc1d1	generovaná
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
volných	volný	k2eAgFnPc2d1	volná
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
(	(	kIx(	(
<g/>
izomorfních	izomorfní	k2eAgFnPc2d1	izomorfní
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
a	a	k8xC	a
cyklických	cyklický	k2eAgFnPc2d1	cyklická
grup	grupa	k1gFnPc2	grupa
řádů	řád	k1gInPc2	řád
mocnin	mocnina	k1gFnPc2	mocnina
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
konečně	konečně	k6eAd1	konečně
generovány	generován	k2eAgFnPc1d1	generována
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
příkladem	příklad	k1gInSc7	příklad
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
jsou	být	k5eAaImIp3nP	být
Prüferovy	Prüferův	k2eAgFnPc1d1	Prüferův
grupy	grupa	k1gFnPc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Prüferova	Prüferův	k2eAgFnSc1d1	Prüferův
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spočetná	spočetný	k2eAgFnSc1d1	spočetná
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
-tou	o	k2eAgFnSc4d1	-to
odmocninu	odmocnina	k1gFnSc4	odmocnina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
grupy	grupa	k1gFnPc1	grupa
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
klasifikaci	klasifikace	k1gFnSc6	klasifikace
nekonečných	konečný	k2eNgFnPc2d1	nekonečná
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
===	===	k?	===
</s>
</p>
<p>
<s>
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
taková	takový	k3xDgFnSc1	takový
podmnožina	podmnožina	k1gFnSc1	podmnožina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
splňuje	splňovat	k5eAaImIp3nS	splňovat
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h_	h_	k?	h_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
h_	h_	k?	h_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h_	h_	k?	h_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
h_	h_	k?	h_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
grupou	grupa	k1gFnSc7	grupa
(	(	kIx(	(
<g/>
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
podgrupa	podgrupa	k1gFnSc1	podgrupa
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
množinu	množina	k1gFnSc4	množina
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
grupu	grupa	k1gFnSc4	grupa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	on	k3xPp3gInPc4	on
vždy	vždy	k6eAd1	vždy
podgrupou	podgrupat	k5eAaPmIp3nP	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jednoprvková	jednoprvkový	k2eAgFnSc1d1	jednoprvková
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jenom	jenom	k9	jenom
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
podgrupou	podgrupat	k5eAaPmIp3nP	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
podgrupy	podgrupa	k1gFnPc1	podgrupa
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
triviální	triviální	k2eAgFnPc1d1	triviální
podgrupy	podgrupa	k1gFnPc1	podgrupa
<g/>
;	;	kIx,	;
podgrupy	podgrupa	k1gFnPc1	podgrupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
triviální	triviální	k2eAgFnPc1d1	triviální
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
podgrupy	podgrupa	k1gFnPc4	podgrupa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
také	také	k9	také
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Znalost	znalost	k1gFnSc1	znalost
struktury	struktura	k1gFnSc2	struktura
podgrup	podgrup	k1gMnSc1	podgrup
dané	daný	k2eAgFnSc2d1	daná
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
porozumění	porozumění	k1gNnSc4	porozumění
grupy	grupa	k1gFnSc2	grupa
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
grupa	grupa	k1gFnSc1	grupa
obecně	obecně	k6eAd1	obecně
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určena	určit	k5eAaPmNgFnS	určit
strukturou	struktura	k1gFnSc7	struktura
svých	svůj	k3xOyFgInPc2	svůj
vlastních	vlastní	k2eAgInPc2d1	vlastní
podgrup	podgrup	k1gInSc4	podgrup
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
dihedrální	dihedrální	k2eAgFnSc2d1	dihedrální
grupy	grupa	k1gFnSc2	grupa
D4	D4	k1gFnSc3	D4
popsaném	popsaný	k2eAgInSc6d1	popsaný
výše	výše	k1gFnSc1	výše
identita	identita	k1gFnSc1	identita
a	a	k8xC	a
otočení	otočení	k1gNnSc1	otočení
tvoří	tvořit	k5eAaImIp3nS	tvořit
podgrupu	podgrup	k1gInSc3	podgrup
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
zvýrazněnou	zvýrazněný	k2eAgFnSc7d1	zvýrazněná
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
násobení	násobení	k1gNnSc2	násobení
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
D4	D4	k1gFnPc2	D4
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
libovolných	libovolný	k2eAgFnPc2d1	libovolná
rotací	rotace	k1gFnPc2	rotace
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
opět	opět	k6eAd1	opět
rotace	rotace	k1gFnSc1	rotace
a	a	k8xC	a
inverze	inverze	k1gFnSc1	inverze
k	k	k7c3	k
rotaci	rotace	k1gFnSc3	rotace
je	být	k5eAaImIp3nS	být
také	také	k9	také
rotace	rotace	k1gFnSc1	rotace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
podgrup	podgrup	k1gMnSc1	podgrup
dihedrální	dihedrální	k2eAgFnSc2d1	dihedrální
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
rotacemi	rotace	k1gFnPc7	rotace
písmena	písmeno	k1gNnSc2	písmeno
F	F	kA	F
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
políčku	políček	k1gInSc2	políček
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
řádku	řádek	k1gInSc6	řádek
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
množinu	množina	k1gFnSc4	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
podgrupu	podgrup	k1gInSc3	podgrup
generovanou	generovaný	k2eAgFnSc4d1	generovaná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejmenší	malý	k2eAgFnSc1d3	nejmenší
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množinu	množina	k1gFnSc4	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
konečných	konečný	k2eAgInPc2d1	konečný
součinů	součin	k1gInPc2	součin
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
inverzí	inverze	k1gFnSc7	inverze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
podgrupa	podgrupa	k1gFnSc1	podgrupa
generovaná	generovaný	k2eAgFnSc1d1	generovaná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
také	také	k9	také
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
jak	jak	k8xC	jak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tak	tak	k9	tak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
k	k	k7c3	k
sobě	se	k3xPyFc3	se
inverzní	inverzní	k2eAgInSc4d1	inverzní
a	a	k8xC	a
libovolný	libovolný	k2eAgInSc4d1	libovolný
součin	součin	k1gInSc4	součin
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
prvkem	prvek	k1gInSc7	prvek
množiny	množina	k1gFnSc2	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c4	v
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
podgrupu	podgrup	k1gInSc6	podgrup
(	(	kIx(	(
<g/>
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
znázorňujícím	znázorňující	k2eAgInSc6d1	znázorňující
podgrupy	podgrup	k1gInPc7	podgrup
D4	D4	k1gMnPc2	D4
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
levému	levý	k2eAgNnSc3d1	levé
políčku	políčko	k1gNnSc3	políčko
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
řádku	řádek	k1gInSc6	řádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podgrupa	podgrupa	k1gFnSc1	podgrupa
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
polojednoduchá	polojednoduchý	k2eAgFnSc1d1	polojednoduchý
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
grupa	grupa	k1gFnSc1	grupa
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
vlastní	vlastní	k2eAgFnPc4d1	vlastní
normální	normální	k2eAgFnPc4d1	normální
podgrupy	podgrupa	k1gFnPc4	podgrupa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
prostá	prostý	k2eAgFnSc1d1	prostá
grupa	grupa	k1gFnSc1	grupa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
grupa	grupa	k1gFnSc1	grupa
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
vlastní	vlastní	k2eAgFnPc4d1	vlastní
normální	normální	k2eAgFnPc4d1	normální
Abelovy	Abelův	k2eAgFnPc4d1	Abelova
podgrupy	podgrupa	k1gFnPc4	podgrupa
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
polojednoduchá	polojednoduchý	k2eAgFnSc1d1	polojednoduchý
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
také	také	k9	také
poloprostá	poloprostý	k2eAgFnSc1d1	poloprostý
grupa	grupa	k1gFnSc1	grupa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
Lieova	Lieův	k2eAgFnSc1d1	Lieova
grupa	grupa	k1gFnSc1	grupa
jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
vlastní	vlastní	k2eAgFnPc4d1	vlastní
normální	normální	k2eAgFnPc4d1	normální
podgrupy	podgrupa	k1gFnPc4	podgrupa
kromě	kromě	k7c2	kromě
diskrétních	diskrétní	k2eAgInPc2d1	diskrétní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Homomorfismus	Homomorfismus	k1gInSc1	Homomorfismus
a	a	k8xC	a
izomorfismus	izomorfismus	k1gInSc1	izomorfismus
grup	grupa	k1gFnPc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Grupový	Grupový	k2eAgInSc1d1	Grupový
homomorfismus	homomorfismus	k1gInSc1	homomorfismus
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc4	zobrazení
mezi	mezi	k7c7	mezi
grupami	grupa	k1gFnPc7	grupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
grupovou	grupový	k2eAgFnSc4d1	grupová
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Explicitně	explicitně	k6eAd1	explicitně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
:	:	kIx,	:
<g/>
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
homomorfismus	homomorfismus	k1gInSc1	homomorfismus
mezi	mezi	k7c7	mezi
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
2	[number]	k4	2
prvky	prvek	k1gInPc4	prvek
g	g	kA	g
<g/>
,	,	kIx,	,
k	k	k7c3	k
z	z	k7c2	z
G	G	kA	G
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
<g/>
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
grupový	grupový	k2eAgInSc1d1	grupový
homomorfismus	homomorfismus	k1gInSc1	homomorfismus
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
také	také	k9	také
inverzní	inverzní	k2eAgInSc1d1	inverzní
prvek	prvek	k1gInSc1	prvek
na	na	k7c4	na
inverzní	inverzní	k2eAgFnSc4d1	inverzní
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
))	))	k?	))
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Homomorfismus	Homomorfismus	k1gInSc1	Homomorfismus
tedy	tedy	k9	tedy
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
grupovými	grupův	k2eAgInPc7d1	grupův
axiomy	axiom	k1gInPc7	axiom
<g/>
.	.	kIx.	.
<g/>
Dvě	dva	k4xCgFnPc1	dva
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
izomorfní	izomorfní	k2eAgFnPc1d1	izomorfní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existují	existovat	k5eAaImIp3nP	existovat
grupové	grupový	k2eAgInPc1d1	grupový
homomorfismy	homomorfismus	k1gInPc1	homomorfismus
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
:	:	kIx,	:
<g/>
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
:	:	kIx,	:
<g/>
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
složení	složení	k1gNnSc1	složení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gFnSc1	circ
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
id_	id_	k?	id_
<g/>
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gInSc1	circ
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
id_	id_	k?	id_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
identity	identita	k1gFnPc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc4	zobrazení
a	a	k8xC	a
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
izomorfismus	izomorfismus	k1gInSc1	izomorfismus
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
izomorfní	izomorfní	k2eAgFnPc1d1	izomorfní
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
objekty	objekt	k1gInPc1	objekt
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
stejnou	stejný	k2eAgFnSc4d1	stejná
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vlastnost	vlastnost	k1gFnSc1	vlastnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
g	g	kA	g
<g/>
=	=	kIx~	=
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgFnPc4d1	ekvivalentní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
<g/>
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Izomorfismus	izomorfismus	k1gInSc1	izomorfismus
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
automorfismus	automorfismus	k1gInSc1	automorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
určuje	určovat	k5eAaImIp3nS	určovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
automorfismus	automorfismus	k1gInSc4	automorfismus
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Automorfismus	Automorfismus	k1gInSc1	Automorfismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
třídy	třída	k1gFnSc2	třída
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
situacích	situace	k1gFnPc6	situace
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
považovat	považovat	k5eAaImF	považovat
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
grupy	grupa	k1gFnSc2	grupa
za	za	k7c4	za
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jenom	jenom	k9	jenom
o	o	k7c4	o
násobek	násobek	k1gInSc4	násobek
nějaké	nějaký	k3yIgFnSc2	nějaký
dané	daný	k2eAgFnSc2d1	daná
podgrupy	podgrupa	k1gFnSc2	podgrupa
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
například	například	k6eAd1	například
grupu	grupa	k1gFnSc4	grupa
D4	D4	k1gFnSc2	D4
popsanou	popsaný	k2eAgFnSc4d1	popsaná
výše	výše	k1gFnPc4	výše
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
podgrupu	podgrupat	k5eAaPmIp1nS	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
nějaké	nějaký	k3yIgNnSc4	nějaký
překlopení	překlopení	k1gNnSc4	překlopení
čtverce	čtverec	k1gInSc2	čtverec
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
fh	fh	k?	fh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
žádnou	žádný	k3yNgFnSc7	žádný
rotací	rotace	k1gFnSc7	rotace
už	už	k6eAd1	už
nemůžeme	moct	k5eNaImIp1nP	moct
docílit	docílit	k5eAaPmF	docílit
zpátky	zpátky	k6eAd1	zpátky
konfiguraci	konfigurace	k1gFnSc4	konfigurace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
překlopení	překlopení	k1gNnSc1	překlopení
a	a	k8xC	a
rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
překlopení	překlopení	k1gNnSc1	překlopení
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
tedy	tedy	k9	tedy
nehraje	hrát	k5eNaImIp3nS	hrát
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
všímáme	všímat	k5eAaImIp1nP	všímat
jenom	jenom	k9	jenom
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bylo	být	k5eAaImAgNnS	být
nebo	nebo	k8xC	nebo
nebylo	být	k5eNaImAgNnS	být
aplikováno	aplikován	k2eAgNnSc1d1	aplikováno
nějaké	nějaký	k3yIgNnSc4	nějaký
překlopení	překlopení	k1gNnSc4	překlopení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkladové	rozkladový	k2eAgFnPc1d1	rozkladová
třídy	třída	k1gFnPc1	třída
formalizují	formalizovat	k5eAaBmIp3nP	formalizovat
tuto	tento	k3xDgFnSc4	tento
ideu	idea	k1gFnSc4	idea
<g/>
.	.	kIx.	.
</s>
<s>
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
definuje	definovat	k5eAaBmIp3nS	definovat
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
pravé	pravý	k2eAgFnSc2d1	pravá
a	a	k8xC	a
levé	levý	k2eAgFnSc2d1	levá
rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
třídy	třída	k1gFnSc2	třída
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gH	gH	k?	gH
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
h	h	k?	h
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
Hg	Hg	k1gMnSc1	Hg
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
g	g	kA	g
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Rozkladové	rozkladový	k2eAgFnPc1d1	rozkladová
třídy	třída	k1gFnPc1	třída
pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
podgrupu	podgrupat	k5eAaPmIp1nS	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tvoří	tvořit	k5eAaImIp3nP	tvořit
rozklad	rozklad	k1gInSc4	rozklad
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
disjunktní	disjunktní	k2eAgFnPc4d1	disjunktní
podmnožiny	podmnožina	k1gFnPc4	podmnožina
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
sjednocení	sjednocení	k1gNnSc2	sjednocení
všech	všecek	k3xTgFnPc2	všecek
levých	levý	k2eAgFnPc2d1	levá
rozkladových	rozkladový	k2eAgFnPc2d1	rozkladová
tříd	třída	k1gFnPc2	třída
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
libovolné	libovolný	k2eAgFnPc4d1	libovolná
dvě	dva	k4xCgFnPc4	dva
levé	levá	k1gFnPc4	levá
rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
rovnají	rovnat	k5eAaImIp3nP	rovnat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jsou	být	k5eAaImIp3nP	být
disjunktní	disjunktní	k2eAgFnPc1d1	disjunktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
=	=	kIx~	=
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nastává	nastávat	k5eAaImIp3nS	nastávat
právě	právě	k6eAd1	právě
když	když	k8xS	když
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
když	když	k8xS	když
se	se	k3xPyFc4	se
příslušné	příslušný	k2eAgInPc1d1	příslušný
prvky	prvek	k1gInPc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
liší	lišit	k5eAaImIp3nS	lišit
jenom	jenom	k9	jenom
o	o	k7c4	o
prvek	prvek	k1gInSc4	prvek
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Analogická	analogický	k2eAgNnPc1d1	analogické
tvrzení	tvrzení	k1gNnPc1	tvrzení
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
pravé	pravý	k2eAgFnPc4d1	pravá
rozkladové	rozkladový	k2eAgFnPc4d1	rozkladová
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
a	a	k8xC	a
levé	levý	k2eAgFnPc1d1	levá
rozkladové	rozkladový	k2eAgFnPc1d1	rozkladová
třídy	třída	k1gFnPc1	třída
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
rovnost	rovnost	k1gFnSc1	rovnost
platit	platit	k5eAaImF	platit
nemusí	muset	k5eNaImIp3nS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rovnají	rovnat	k5eAaImIp3nP	rovnat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gH	gH	k?	gH
<g/>
=	=	kIx~	=
<g/>
Hg	Hg	k1gMnSc1	Hg
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nazývá	nazývat	k5eAaImIp3nS	nazývat
normální	normální	k2eAgMnSc1d1	normální
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
levých	levý	k2eAgFnPc2d1	levá
rozkladových	rozkladový	k2eAgFnPc2d1	rozkladová
tříd	třída	k1gFnPc2	třída
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
pravých	pravý	k2eAgFnPc2d1	pravá
rozkladových	rozkladový	k2eAgFnPc2d1	rozkladová
tříd	třída	k1gFnPc2	třída
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
backslash	backslash	k1gMnSc1	backslash
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
grupy	grupa	k1gFnSc2	grupa
D4	D4	k1gFnSc2	D4
z	z	k7c2	z
úvodu	úvod	k1gInSc2	úvod
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
podgrupy	podgrupa	k1gFnSc2	podgrupa
rotací	rotace	k1gFnPc2	rotace
R	R	kA	R
<g/>
,	,	kIx,	,
levé	levý	k2eAgFnSc2d1	levá
rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gR	gR	k?	gR
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
množina	množina	k1gFnSc1	množina
R	R	kA	R
všech	všecek	k3xTgFnPc2	všecek
rotací	rotace	k1gFnPc2	rotace
(	(	kIx(	(
<g/>
a	a	k8xC	a
identita	identita	k1gFnSc1	identita
<g/>
)	)	kIx)	)
pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvkem	prvek	k1gInSc7	prvek
R	R	kA	R
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c4	v
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
všech	všecek	k3xTgNnPc2	všecek
překlopení	překlopení	k1gNnPc2	překlopení
(	(	kIx(	(
<g/>
zvýrazněna	zvýrazněn	k2eAgFnSc1d1	zvýrazněna
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
)	)	kIx)	)
pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nějaké	nějaký	k3yIgNnSc4	nějaký
překlopení	překlopení	k1gNnSc4	překlopení
<g/>
.	.	kIx.	.
</s>
<s>
Levé	levý	k2eAgFnPc1d1	levá
rozkladové	rozkladový	k2eAgFnPc1d1	rozkladová
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnSc6	D_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Normální	normální	k2eAgFnSc1d1	normální
podgrupa	podgrupa	k1gFnSc1	podgrupa
a	a	k8xC	a
faktorová	faktorový	k2eAgFnSc1d1	Faktorová
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
normální	normální	k2eAgFnPc4d1	normální
podgrupou	podgrupat	k5eAaPmIp3nP	podgrupat
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
existuje	existovat	k5eAaImIp3nS	existovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
levé	levý	k2eAgFnSc2d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc2d1	pravá
rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
rovnají	rovnat	k5eAaImIp3nP	rovnat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gN	gN	k?	gN
<g/>
=	=	kIx~	=
<g/>
Ng	Ng	k1gMnSc1	Ng
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc4	jádro
nějakého	nějaký	k3yIgInSc2	nějaký
homomorfismu	homomorfismus	k1gInSc2	homomorfismus
grup	grupa	k1gFnPc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
K	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
podgrupa	podgrupa	k1gFnSc1	podgrupa
Abelovy	Abelův	k2eAgFnSc2d1	Abelova
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgMnSc1d1	normální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgFnSc1d1	normální
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zavést	zavést	k5eAaPmF	zavést
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
rozkladových	rozkladový	k2eAgFnPc2d1	rozkladová
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
gN	gN	k?	gN
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
strukturu	struktura	k1gFnSc4	struktura
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Grupová	Grupový	k2eAgFnSc1d1	Grupový
operace	operace	k1gFnSc1	operace
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
gN	gN	k?	gN
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
(	(	kIx(	(
<g/>
hN	hN	k?	hN
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
gh	gh	k?	gh
<g/>
)	)	kIx)	)
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
,	,	kIx,	,
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
faktorgrupa	faktorgrupa	k1gFnSc1	faktorgrupa
<g/>
.	.	kIx.	.
</s>
<s>
Rozkladová	rozkladový	k2eAgFnSc1d1	rozkladová
třída	třída	k1gFnSc1	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
eN	eN	k?	eN
<g/>
=	=	kIx~	=
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
této	tento	k3xDgFnSc2	tento
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
inverze	inverze	k1gFnSc2	inverze
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gN	gN	k?	gN
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
gN	gN	k?	gN
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zobrazení	zobrazení	k1gNnSc4	zobrazení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
jeho	jeho	k3xOp3gFnSc4	jeho
rozkladovou	rozkladový	k2eAgFnSc4d1	rozkladová
třídu	třída	k1gFnSc4	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
gN	gN	k?	gN
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
homomorfismus	homomorfismus	k1gInSc4	homomorfismus
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
grupy	grupa	k1gFnSc2	grupa
D4	D4	k1gFnSc2	D4
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
id	idy	k1gFnPc2	idy
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
normální	normální	k2eAgMnSc1d1	normální
a	a	k8xC	a
rozkladové	rozkladový	k2eAgFnPc1d1	rozkladová
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
překlopení	překlopení	k1gNnPc2	překlopení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grupová	Grupový	k2eAgFnSc1d1	Grupový
operace	operace	k1gFnSc1	operace
na	na	k7c6	na
faktorové	faktorový	k2eAgFnSc6d1	Faktorová
grupě	grupa	k1gFnSc6	grupa
je	být	k5eAaImIp3nS	být
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
tabulkou	tabulka	k1gFnSc7	tabulka
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
idR	idR	k?	idR
<g/>
=	=	kIx~	=
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Podgrupa	Podgrupa	k1gFnSc1	Podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Abelova	Abelův	k2eAgFnSc1d1	Abelova
<g/>
,	,	kIx,	,
a	a	k8xC	a
faktorová	faktorový	k2eAgFnSc1d1	Faktorová
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnSc6	D_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
také	také	k9	také
Abelova	Abelův	k2eAgFnSc1d1	Abelova
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
D4	D4	k1gFnSc1	D4
není	být	k5eNaImIp3nS	být
Abelova	Abelův	k2eAgFnSc1d1	Abelova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generování	generování	k1gNnSc1	generování
a	a	k8xC	a
prezentace	prezentace	k1gFnSc1	prezentace
grupy	grupa	k1gFnSc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Faktorové	faktorový	k2eAgFnPc1d1	Faktorová
grupy	grupa	k1gFnPc1	grupa
a	a	k8xC	a
podgrupy	podgrupa	k1gFnPc1	podgrupa
tvoří	tvořit	k5eAaImIp3nP	tvořit
spolu	spolu	k6eAd1	spolu
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
každou	každý	k3xTgFnSc4	každý
grupu	grupa	k1gFnSc4	grupa
popsat	popsat	k5eAaPmF	popsat
její	její	k3xOp3gFnSc7	její
prezentací	prezentace	k1gFnSc7	prezentace
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
grupu	grupa	k1gFnSc4	grupa
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zadat	zadat	k5eAaPmF	zadat
jako	jako	k8xC	jako
faktor	faktor	k1gInSc1	faktor
volné	volný	k2eAgFnSc2d1	volná
grupy	grupa	k1gFnSc2	grupa
nad	nad	k7c7	nad
nějakou	nějaký	k3yIgFnSc7	nějaký
generující	generující	k2eAgFnSc7d1	generující
množinou	množina	k1gFnSc7	množina
podle	podle	k7c2	podle
normální	normální	k2eAgFnSc2d1	normální
podgrupy	podgrupa	k1gFnSc2	podgrupa
generovanou	generovaný	k2eAgFnSc4d1	generovaná
relacemi	relace	k1gFnPc7	relace
<g/>
.	.	kIx.	.
</s>
<s>
Relace	relace	k1gFnPc1	relace
jsou	být	k5eAaImIp3nP	být
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
rovnají	rovnat	k5eAaImIp3nP	rovnat
neutrálnímu	neutrální	k2eAgInSc3d1	neutrální
prvku	prvek	k1gInSc3	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
zadána	zadán	k2eAgFnSc1d1	zadána
generátory	generátor	k1gInPc7	generátor
a	a	k8xC	a
relacemi	relace	k1gFnPc7	relace
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langla	k1gFnSc3	langla
Gen	gen	kA	gen
<g/>
|	|	kIx~	|
<g/>
Rel	Rel	k1gMnSc1	Rel
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Gen	gen	kA	gen
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
generátorů	generátor	k1gInPc2	generátor
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Rel	Rel	k1gFnSc3	Rel
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
množina	množina	k1gFnSc1	množina
relací	relace	k1gFnPc2	relace
<g/>
.	.	kIx.	.
<g/>
Dihedrální	Dihedrální	k2eAgFnSc1d1	Dihedrální
grupa	grupa	k1gFnSc1	grupa
D4	D4	k1gFnSc2	D4
je	být	k5eAaImIp3nS	být
generována	generován	k2eAgFnSc1d1	generována
například	například	k6eAd1	například
prvky	prvek	k1gInPc7	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
symetrie	symetrie	k1gFnSc1	symetrie
čtverce	čtverec	k1gInSc2	čtverec
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
složení	složení	k1gNnSc3	složení
konečně	konečně	k6eAd1	konečně
mnoha	mnoho	k4c2	mnoho
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
symetrií	symetrie	k1gFnPc2	symetrie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
inverzí	inverze	k1gFnPc2	inverze
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
relacemi	relace	k1gFnPc7	relace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
grupa	grupa	k1gFnSc1	grupa
úplně	úplně	k6eAd1	úplně
popsána	popsat	k5eAaPmNgFnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnSc6	D_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Prezentace	prezentace	k1gFnSc1	prezentace
grupy	grupa	k1gFnSc2	grupa
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
Cayleyho	Cayley	k1gMnSc2	Cayley
grafu	graf	k1gInSc2	graf
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
graficky	graficky	k6eAd1	graficky
popsat	popsat	k5eAaPmF	popsat
diskrétní	diskrétní	k2eAgFnPc4d1	diskrétní
grupy	grupa	k1gFnPc4	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
G	G	kA	G
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
posloupnost	posloupnost	k1gFnSc4	posloupnost
jejich	jejich	k3xOp3gInSc4	jejich
podgrup	podgrup	k1gInSc4	podgrup
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⊃	⊃	k?	⊃
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⊃	⊃	k?	⊃
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⊃	⊃	k?	⊃
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
⊃	⊃	k?	⊃
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
=	=	kIx~	=
<g/>
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
supset	supset	k1gMnSc1	supset
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
supset	supset	k1gMnSc1	supset
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
supset	supset	k1gInSc1	supset
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
\	\	kIx~	\
<g/>
supset	supset	k1gMnSc1	supset
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
takových	takový	k3xDgInPc2	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gFnSc7	G_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgFnSc1d1	normální
podgrupa	podgrupa	k1gFnSc1	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gFnSc7	G_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
faktorová	faktorový	k2eAgFnSc1d1	Faktorová
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gFnSc7	G_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
G_	G_	k1gFnSc1	G_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Abelova	Abelův	k2eAgFnSc1d1	Abelova
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgFnSc1d1	poslední
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
grupa	grupa	k1gFnSc1	grupa
triviální	triviální	k2eAgFnSc1d1	triviální
<g/>
.	.	kIx.	.
<g/>
Například	například	k6eAd1	například
výše	vysoce	k6eAd2	vysoce
diskutovaná	diskutovaný	k2eAgFnSc1d1	diskutovaná
grupa	grupa	k1gFnSc1	grupa
D4	D4	k1gFnSc2	D4
je	být	k5eAaImIp3nS	být
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
komutativní	komutativní	k2eAgMnSc1d1	komutativní
podgrupu	podgrupat	k5eAaPmIp1nS	podgrupat
R	R	kA	R
a	a	k8xC	a
faktor	faktor	k1gInSc1	faktor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gMnSc6	D_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgNnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
alternující	alternující	k2eAgFnSc1d1	alternující
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
60	[number]	k4	60
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
<g/>
Slovo	slovo	k1gNnSc4	slovo
řešitelná	řešitelný	k2eAgNnPc4d1	řešitelné
má	mít	k5eAaImIp3nS	mít
historickou	historický	k2eAgFnSc4d1	historická
souvislost	souvislost	k1gFnSc4	souvislost
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
existence	existence	k1gFnSc2	existence
řešení	řešení	k1gNnSc2	řešení
polynomiálních	polynomiální	k2eAgFnPc2d1	polynomiální
rovnic	rovnice	k1gFnPc2	rovnice
pomocí	pomocí	k7c2	pomocí
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Galois	Galois	k1gInSc1	Galois
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc1	takový
řešení	řešení	k1gNnSc1	řešení
existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
grupa	grupa	k1gFnSc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
kořenů	kořen	k1gInPc2	kořen
polynomu	polynom	k1gInSc2	polynom
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
<g/>
)	)	kIx)	)
výše	výše	k1gFnSc1	výše
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
a	a	k8xC	a
aplikace	aplikace	k1gFnPc1	aplikace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
systémy	systém	k1gInPc1	systém
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
celá	celý	k2eAgNnPc1d1	celé
nebo	nebo	k8xC	nebo
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
mají	mít	k5eAaImIp3nP	mít
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
strukturu	struktura	k1gFnSc4	struktura
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jako	jako	k9	jako
například	například	k6eAd1	například
u	u	k7c2	u
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
má	mít	k5eAaImIp3nS	mít
jak	jak	k8xS	jak
sčítání	sčítání	k1gNnSc4	sčítání
tak	tak	k6eAd1	tak
i	i	k8xC	i
násobení	násobení	k1gNnSc6	násobení
grupovou	grupový	k2eAgFnSc4d1	grupová
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
číselné	číselný	k2eAgInPc1d1	číselný
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zobecnit	zobecnit	k5eAaPmF	zobecnit
na	na	k7c4	na
algebraické	algebraický	k2eAgFnPc4d1	algebraická
struktury	struktura	k1gFnPc4	struktura
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
okruhy	okruh	k1gInPc1	okruh
<g/>
,	,	kIx,	,
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
moduly	modul	k1gInPc1	modul
<g/>
,	,	kIx,	,
vektorové	vektorový	k2eAgInPc1d1	vektorový
prostory	prostor	k1gInPc1	prostor
a	a	k8xC	a
algebry	algebra	k1gFnPc1	algebra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
sčítáním	sčítání	k1gNnSc7	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
násobení	násobení	k1gNnSc4	násobení
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
netvoří	tvořit	k5eNaImIp3nS	tvořit
grupu	grupa	k1gFnSc4	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Asociativita	asociativita	k1gFnSc1	asociativita
je	být	k5eAaImIp3nS	být
splněna	splněn	k2eAgFnSc1d1	splněna
<g/>
,	,	kIx,	,
jednotkový	jednotkový	k2eAgInSc1d1	jednotkový
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
číslům	číslo	k1gNnPc3	číslo
obecně	obecně	k6eAd1	obecně
neexistují	existovat	k5eNaImIp3nP	existovat
inverzní	inverzní	k2eAgInPc4d1	inverzní
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
už	už	k9	už
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
rovnice	rovnice	k1gFnSc1	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ax	ax	k?	ax
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nemá	mít	k5eNaImIp3nS	mít
řešení	řešení	k1gNnSc4	řešení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
nenulovým	nulový	k2eNgNnPc3d1	nenulové
číslům	číslo	k1gNnPc3	číslo
existovaly	existovat	k5eAaImAgFnP	existovat
inverzní	inverzní	k2eAgInPc4d1	inverzní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
zavést	zavést	k5eAaPmF	zavést
zlomky	zlomek	k1gInPc4	zlomek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Zlomky	zlomek	k1gInPc1	zlomek
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
množina	množina	k1gFnSc1	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
nenulových	nulový	k2eNgNnPc2d1	nenulové
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
násobení	násobení	k1gNnSc2	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Součin	součin	k1gInSc1	součin
dvou	dva	k4xCgNnPc2	dva
nenulových	nulový	k2eNgNnPc2d1	nenulové
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
nenulové	nulový	k2eNgNnSc4d1	nenulové
racionální	racionální	k2eAgNnSc4d1	racionální
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
inverzní	inverzní	k2eAgInSc1d1	inverzní
prvek	prvek	k1gInSc1	prvek
k	k	k7c3	k
nenulovému	nulový	k2eNgNnSc3d1	nenulové
číslu	číslo	k1gNnSc3	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nenulové	nulový	k2eNgNnSc1d1	nenulové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
také	také	k9	také
grupu	grupa	k1gFnSc4	grupa
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecněji	obecně	k6eAd2	obecně
<g/>
,	,	kIx,	,
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
tělesa	těleso	k1gNnSc2	těleso
tvoří	tvořit	k5eAaImIp3nP	tvořit
vždy	vždy	k6eAd1	vždy
grupu	grupa	k1gFnSc4	grupa
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
a	a	k8xC	a
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
nenulových	nulový	k2eNgInPc2d1	nenulový
prvků	prvek	k1gInPc2	prvek
tělesa	těleso	k1gNnSc2	těleso
tvoří	tvořit	k5eAaImIp3nP	tvořit
grupu	grupa	k1gFnSc4	grupa
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
násobení	násobení	k1gNnSc3	násobení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
modulární	modulární	k2eAgFnSc7d1	modulární
aritmetikou	aritmetika	k1gFnSc7	aritmetika
zavést	zavést	k5eAaPmF	zavést
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
násobení	násobení	k1gNnSc1	násobení
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
jako	jako	k8xS	jako
třídy	třída	k1gFnSc2	třída
ekvivalence	ekvivalence	k1gFnSc1	ekvivalence
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
s	s	k7c7	s
ekvivalencí	ekvivalence	k1gFnSc7	ekvivalence
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
∼	∼	k?	∼
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
sim	sima	k1gFnPc2	sima
m	m	kA	m
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
právě	právě	k9	právě
když	když	k8xS	když
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dělí	dělit	k5eAaImIp3nS	dělit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m-n	m	k?	m-n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
a	a	k8xC	a
násobením	násobení	k1gNnSc7	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
konečného	konečný	k2eAgNnSc2d1	konečné
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
multiplikativní	multiplikativní	k2eAgFnSc1d1	multiplikativní
grupa	grupa	k1gFnSc1	grupa
nenulových	nulový	k2eNgInPc2d1	nenulový
prvků	prvek	k1gInPc2	prvek
konečného	konečný	k2eAgNnSc2d1	konečné
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
grupy	grupa	k1gFnPc1	grupa
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
asymetrické	asymetrický	k2eAgFnSc6d1	asymetrická
kryptografii	kryptografie	k1gFnSc6	kryptografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
vpravo	vpravo	k6eAd1	vpravo
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
multiplikativní	multiplikativní	k2eAgFnSc4d1	multiplikativní
grupu	grupa	k1gFnSc4	grupa
nenulových	nulový	k2eNgFnPc2d1	nenulová
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
modulo	modout	k5eAaPmAgNnS	modout
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
5	[number]	k4	5
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Rovnost	rovnost	k1gFnSc1	rovnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
například	například	k6eAd1	například
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{{	{{	k?	{{
<g/>
mod	mod	k?	mod
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,5	,5	k4	,5
<g/>
=	=	kIx~	=
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{{	{{	k?	{{
<g/>
mod	mod	k?	mod
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,5	,5	k4	,5
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}}}}}	}}}}}	k?	}}}}}
</s>
</p>
<p>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
pravek	pravek	k1gInSc1	pravek
má	mít	k5eAaImIp3nS	mít
inverzní	inverzní	k2eAgInSc1d1	inverzní
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,4	,4	k4	,4
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
a	a	k8xC	a
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
generuje	generovat	k5eAaImIp3nS	generovat
celou	celý	k2eAgFnSc4d1	celá
grupu	grupa	k1gFnSc4	grupa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pozůstávají	pozůstávat	k5eAaImIp3nP	pozůstávat
z	z	k7c2	z
nějakých	nějaký	k3yIgNnPc2	nějaký
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
popisují	popisovat	k5eAaImIp3nP	popisovat
následující	následující	k2eAgInPc4d1	následující
příklady	příklad	k1gInPc4	příklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
Gaussových	Gaussův	k2eAgNnPc2d1	Gaussovo
čísel	číslo	k1gNnPc2	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
do	do	k7c2	do
komplexní	komplexní	k2eAgFnSc2d1	komplexní
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
invertibilních	invertibilní	k2eAgInPc2d1	invertibilní
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
množině	množina	k1gFnSc6	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
násobení	násobení	k1gNnSc4	násobení
grupu	grupa	k1gFnSc4	grupa
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
grupa	grupa	k1gFnSc1	grupa
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
násobením	násobení	k1gNnSc7	násobení
tvoří	tvořit	k5eAaImIp3nS	tvořit
grupu	grupa	k1gFnSc4	grupa
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množina	množina	k1gFnSc1	množina
kvaternionů	kvaternion	k1gInPc2	kvaternion
normy	norma	k1gFnSc2	norma
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
násobením	násobení	k1gNnSc7	násobení
tvoří	tvořit	k5eAaImIp3nS	tvořit
grupu	grupa	k1gFnSc4	grupa
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvaternionová	Kvaternionový	k2eAgFnSc1d1	Kvaternionový
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
podgrupa	podgrupa	k1gFnSc1	podgrupa
o	o	k7c6	o
osmi	osm	k4xCc6	osm
prvcích	prvek	k1gInPc6	prvek
<g/>
,	,	kIx,	,
generována	generován	k2eAgFnSc1d1	generována
prvky	prvek	k1gInPc4	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
<g/>
j	j	k?	j
<g/>
,	,	kIx,	,
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
v	v	k7c6	v
grupě	grupa	k1gFnSc6	grupa
nenulových	nulový	k2eNgInPc2d1	nenulový
kvaternionů	kvaternion	k1gInPc2	kvaternion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Grupy	grupa	k1gFnPc1	grupa
symetrií	symetrie	k1gFnSc7	symetrie
===	===	k?	===
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
je	být	k5eAaImIp3nS	být
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
symetrie	symetrie	k1gFnPc4	symetrie
daného	daný	k2eAgInSc2d1	daný
matematického	matematický	k2eAgInSc2d1	matematický
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
geometrického	geometrický	k2eAgInSc2d1	geometrický
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
grupa	grupa	k1gFnSc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
čtverce	čtverec	k1gInSc2	čtverec
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
algebraického	algebraický	k2eAgNnSc2d1	algebraické
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kořeny	kořen	k1gInPc1	kořen
polynomu	polynom	k1gInSc2	polynom
<g/>
.	.	kIx.	.
<g/>
Teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápana	chápan	k1gMnSc4	chápan
jako	jako	k8xS	jako
studium	studium	k1gNnSc4	studium
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
například	například	k6eAd1	například
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
grupou	grupa	k1gFnSc7	grupa
symetrie	symetrie	k1gFnSc2	symetrie
nějakého	nějaký	k3yIgInSc2	nějaký
grafu	graf	k1gInSc2	graf
<g/>
.	.	kIx.	.
</s>
<s>
Symetrie	symetrie	k1gFnSc1	symetrie
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
často	často	k6eAd1	často
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
studium	studium	k1gNnSc4	studium
geometrických	geometrický	k2eAgInPc2d1	geometrický
<g/>
,	,	kIx,	,
analytických	analytický	k2eAgInPc2d1	analytický
anebo	anebo	k8xC	anebo
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
grupě	grupa	k1gFnSc6	grupa
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
akci	akce	k1gFnSc4	akce
na	na	k7c6	na
objektu	objekt	k1gInSc6	objekt
X	X	kA	X
pokud	pokud	k8xS	pokud
každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
grupy	grupa	k1gFnSc2	grupa
provede	provést	k5eAaPmIp3nS	provést
s	s	k7c7	s
objektem	objekt	k1gInSc7	objekt
operaci	operace	k1gFnSc4	operace
kompatibilní	kompatibilní	k2eAgFnSc4d1	kompatibilní
s	s	k7c7	s
grupovou	grupový	k2eAgFnSc7d1	grupová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Symetrie	symetrie	k1gFnSc1	symetrie
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podgrupa	podgrupa	k1gFnSc1	podgrupa
všech	všecek	k3xTgInPc2	všecek
takových	takový	k3xDgInPc2	takový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nechávají	nechávat	k5eAaImIp3nP	nechávat
X	X	kA	X
nezměněn	změnit	k5eNaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Symetrie	symetrie	k1gFnSc1	symetrie
dláždění	dláždění	k1gNnSc2	dláždění
roviny	rovina	k1gFnSc2	rovina
====	====	k?	====
</s>
</p>
<p>
<s>
Rovinné	rovinný	k2eAgFnPc1d1	rovinná
krystalografické	krystalografický	k2eAgFnPc1d1	krystalografická
grupy	grupa	k1gFnPc1	grupa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Wallpaper	Wallpaper	k1gInSc1	Wallpaper
groups	groupsa	k1gFnPc2	groupsa
<g/>
)	)	kIx)	)
popisují	popisovat	k5eAaImIp3nP	popisovat
symetrie	symetrie	k1gFnPc1	symetrie
periodických	periodický	k2eAgNnPc2d1	periodické
dláždění	dláždění	k1gNnPc2	dláždění
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
vzorek	vzorek	k1gInSc1	vzorek
tvořen	tvořit	k5eAaImNgInS	tvořit
kytkou	kytka	k1gFnSc7	kytka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
periodicky	periodicky	k6eAd1	periodicky
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
tohoto	tento	k3xDgInSc2	tento
vzoru	vzor	k1gInSc2	vzor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
takové	takový	k3xDgFnPc4	takový
transformace	transformace	k1gFnPc4	transformace
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
převádí	převádět	k5eAaImIp3nP	převádět
vzor	vzor	k1gInSc4	vzor
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
jenom	jenom	k9	jenom
s	s	k7c7	s
translací	translace	k1gFnSc7	translace
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
rotace	rotace	k1gFnPc4	rotace
ani	ani	k8xC	ani
zrcadlení	zrcadlení	k1gNnPc4	zrcadlení
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1	jiné
periodická	periodický	k2eAgNnPc1d1	periodické
dláždění	dláždění	k1gNnPc1	dláždění
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nekonečný	konečný	k2eNgInSc1d1	nekonečný
čtverečkový	čtverečkový	k2eAgInSc1d1	čtverečkový
papír	papír	k1gInSc1	papír
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
grupu	grupa	k1gFnSc4	grupa
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
translací	translace	k1gFnPc2	translace
roviny	rovina	k1gFnSc2	rovina
i	i	k8xC	i
různé	různý	k2eAgFnPc1d1	různá
rotace	rotace	k1gFnPc1	rotace
<g/>
,	,	kIx,	,
zrcadlení	zrcadlení	k1gNnSc1	zrcadlení
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Různých	různý	k2eAgFnPc2d1	různá
neizomorfních	izomorfní	k2eNgFnPc2d1	izomorfní
rovinných	rovinný	k2eAgFnPc2d1	rovinná
krystalografických	krystalografický	k2eAgFnPc2d1	krystalografická
grup	grupa	k1gFnPc2	grupa
existuje	existovat	k5eAaImIp3nS	existovat
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vzory	vzor	k1gInPc1	vzor
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
často	často	k6eAd1	často
v	v	k7c6	v
islámské	islámský	k2eAgFnSc6d1	islámská
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
například	například	k6eAd1	například
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Alhambra	Alhambr	k1gInSc2	Alhambr
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
rovinných	rovinný	k2eAgFnPc2d1	rovinná
krystalografických	krystalografický	k2eAgFnPc2d1	krystalografická
grup	grupa	k1gFnPc2	grupa
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
17	[number]	k4	17
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
poprvé	poprvé	k6eAd1	poprvé
E.	E.	kA	E.
<g/>
Fedorov	Fedorov	k1gInSc4	Fedorov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgNnPc2	tento
dláždění	dláždění	k1gNnPc2	dláždění
roviny	rovina	k1gFnSc2	rovina
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
neperiodická	periodický	k2eNgNnPc1d1	neperiodické
dláždění	dláždění	k1gNnPc1	dláždění
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
grupa	grupa	k1gFnSc1	grupa
symetrií	symetrie	k1gFnSc7	symetrie
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádnou	žádný	k3yNgFnSc4	žádný
translaci	translace	k1gFnSc4	translace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
slavné	slavný	k2eAgNnSc1d1	slavné
Penroseho	Penrose	k1gMnSc2	Penrose
pokrytí	pokrytí	k1gNnSc4	pokrytí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
neperiodické	periodický	k2eNgNnSc1d1	neperiodické
dláždění	dláždění	k1gNnSc1	dláždění
roviny	rovina	k1gFnSc2	rovina
pomocí	pomocí	k7c2	pomocí
konečného	konečný	k2eAgInSc2d1	konečný
počtu	počet	k1gInSc2	počet
typů	typ	k1gInPc2	typ
dlaždiček	dlaždička	k1gFnPc2	dlaždička
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
grupa	grupa	k1gFnSc1	grupa
symetrie	symetrie	k1gFnSc2	symetrie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
například	například	k6eAd1	například
otočení	otočení	k1gNnSc1	otočení
o	o	k7c4	o
pětinu	pětina	k1gFnSc4	pětina
kruhu	kruh	k1gInSc2	kruh
kolem	kolem	k7c2	kolem
nějakého	nějaký	k3yIgInSc2	nějaký
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
periodická	periodický	k2eAgNnPc1d1	periodické
dláždění	dláždění	k1gNnPc1	dláždění
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
grupy	grupa	k1gFnPc4	grupa
symetrií	symetrie	k1gFnPc2	symetrie
můžeme	moct	k5eAaImIp1nP	moct
studovat	studovat	k5eAaImF	studovat
i	i	k9	i
v	v	k7c6	v
neeukleidovských	eukleidovský	k2eNgFnPc6d1	neeukleidovská
geometriích	geometrie	k1gFnPc6	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hyperbolickou	hyperbolický	k2eAgFnSc4d1	hyperbolická
rovinu	rovina	k1gFnSc4	rovina
lze	lze	k6eAd1	lze
pravidelně	pravidelně	k6eAd1	pravidelně
pokrýt	pokrýt	k5eAaPmF	pokrýt
rovnostrannými	rovnostranný	k2eAgInPc7d1	rovnostranný
trojúhelníky	trojúhelník	k1gInPc7	trojúhelník
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
7	[number]	k4	7
trojúhelníkům	trojúhelník	k1gInPc3	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Příslušná	příslušný	k2eAgFnSc1d1	příslušná
grupa	grupa	k1gFnSc1	grupa
symetrie	symetrie	k1gFnSc2	symetrie
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
všemi	všecek	k3xTgFnPc7	všecek
symetriemi	symetrie	k1gFnPc7	symetrie
této	tento	k3xDgFnSc2	tento
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
převádějí	převádět	k5eAaImIp3nP	převádět
toto	tento	k3xDgNnSc4	tento
pokrytí	pokrytí	k1gNnSc4	pokrytí
samo	sám	k3xTgNnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
znázorněno	znázorněn	k2eAgNnSc1d1	znázorněno
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
takových	takový	k3xDgNnPc2	takový
pokrytí	pokrytí	k1gNnPc2	pokrytí
<g/>
.	.	kIx.	.
</s>
<s>
Příslušná	příslušný	k2eAgFnSc1d1	příslušná
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
2,3	[number]	k4	2,3
<g/>
,7	,7	k4	,7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
vrchol	vrchol	k1gInSc4	vrchol
nějakého	nějaký	k3yIgInSc2	nějaký
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
pak	pak	k6eAd1	pak
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
grupě	grupa	k1gFnSc6	grupa
prvek	prvek	k1gInSc1	prvek
řádu	řád	k1gInSc6	řád
7	[number]	k4	7
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
otočí	otočit	k5eAaPmIp3nS	otočit
<g/>
"	"	kIx"	"
rovinu	rovina	k1gFnSc4	rovina
kolem	kolem	k7c2	kolem
daného	daný	k2eAgInSc2d1	daný
bodu	bod	k1gInSc2	bod
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kruhu	kruh	k1gInSc3	kruh
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
převede	převést	k5eAaPmIp3nS	převést
dláždění	dláždění	k1gNnSc1	dláždění
samo	sám	k3xTgNnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Symetrie	symetrie	k1gFnSc2	symetrie
v	v	k7c6	v
krystalografii	krystalografie	k1gFnSc6	krystalografie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgInPc6d1	chemický
oborech	obor	k1gInPc6	obor
jako	jako	k8xS	jako
krystalografie	krystalografie	k1gFnPc1	krystalografie
popisují	popisovat	k5eAaImIp3nP	popisovat
prostorová	prostorový	k2eAgFnSc1d1	prostorová
grupa	grupa	k1gFnSc1	grupa
a	a	k8xC	a
bodová	bodový	k2eAgFnSc1d1	bodová
grupa	grupa	k1gFnSc1	grupa
molekulární	molekulární	k2eAgFnSc2d1	molekulární
symetrie	symetrie	k1gFnSc2	symetrie
a	a	k8xC	a
symetrie	symetrie	k1gFnSc2	symetrie
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
symetrie	symetrie	k1gFnPc1	symetrie
určují	určovat	k5eAaImIp3nP	určovat
chemické	chemický	k2eAgFnPc4d1	chemická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
kvantově	kvantově	k6eAd1	kvantově
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
analýzu	analýza	k1gFnSc4	analýza
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
kvantovými	kvantový	k2eAgInPc7d1	kvantový
stavy	stav	k1gInPc7	stav
nemůžou	nemůžou	k?	nemůžou
nastat	nastat	k5eAaPmF	nastat
jenom	jenom	k9	jenom
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
symetrií	symetrie	k1gFnPc2	symetrie
daných	daný	k2eAgInPc2d1	daný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejenom	nejenom	k6eAd1	nejenom
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
grupy	grupa	k1gFnPc1	grupa
užitečné	užitečný	k2eAgFnPc1d1	užitečná
na	na	k7c4	na
popis	popis	k1gInSc4	popis
symetrií	symetrie	k1gFnPc2	symetrie
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
překvapivě	překvapivě	k6eAd1	překvapivě
dokážou	dokázat	k5eAaPmIp3nP	dokázat
i	i	k9	i
predikovat	predikovat	k5eAaBmF	predikovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
molekuly	molekula	k1gFnPc1	molekula
můžou	můžou	k?	můžou
svoji	svůj	k3xOyFgFnSc4	svůj
symetrii	symetrie	k1gFnSc4	symetrie
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Jahn-Tellerův	Jahn-Tellerův	k2eAgInSc1d1	Jahn-Tellerův
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
deformace	deformace	k1gFnPc4	deformace
molekuly	molekula	k1gFnSc2	molekula
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabude	nabýt	k5eAaPmIp3nS	nabýt
určitý	určitý	k2eAgInSc4d1	určitý
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
symetrie	symetrie	k1gFnSc1	symetrie
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
nižších	nízký	k2eAgFnPc2d2	nižší
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
vzájemně	vzájemně	k6eAd1	vzájemně
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
a	a	k8xC	a
souvisejí	souviset	k5eAaImIp3nP	souviset
se	se	k3xPyFc4	se
symetrií	symetrie	k1gFnSc7	symetrie
původní	původní	k2eAgFnSc3d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
grupa	grupa	k1gFnSc1	grupa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
změn	změna	k1gFnPc2	změna
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dějou	dít	k5eAaImIp3nP	dít
u	u	k7c2	u
fázového	fázový	k2eAgInSc2d1	fázový
přechodu	přechod	k1gInSc2	přechod
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
typu	typ	k1gInSc2	typ
mřížky	mřížka	k1gFnSc2	mřížka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Transformační	transformační	k2eAgFnSc2d1	transformační
grupy	grupa	k1gFnSc2	grupa
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
====	====	k?	====
</s>
</p>
<p>
<s>
Geometrické	geometrický	k2eAgFnPc1d1	geometrická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
akce	akce	k1gFnPc1	akce
grupy	grupa	k1gFnSc2	grupa
nemění	měnit	k5eNaImIp3nP	měnit
<g/>
,	,	kIx,	,
studuje	studovat	k5eAaImIp3nS	studovat
geometrická	geometrický	k2eAgFnSc1d1	geometrická
teorie	teorie	k1gFnSc1	teorie
invariantů	invariant	k1gInPc2	invariant
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Klein	Klein	k1gMnSc1	Klein
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
přednášce	přednáška	k1gFnSc6	přednáška
v	v	k7c4	v
Erlangen	Erlangen	k1gInSc4	Erlangen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
[	[	kIx(	[
<g/>
1872	[number]	k4	1872
<g/>
]	]	kIx)	]
definoval	definovat	k5eAaBmAgInS	definovat
geometrii	geometrie	k1gFnSc4	geometrie
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
symetrie	symetrie	k1gFnSc2	symetrie
nějaké	nějaký	k3yIgFnSc2	nějaký
geometrie	geometrie	k1gFnSc2	geometrie
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
transformací	transformace	k1gFnPc2	transformace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
Eukleidovu	Eukleidův	k2eAgFnSc4d1	Eukleidova
geometrii	geometrie	k1gFnSc4	geometrie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Eukleidova	Eukleidův	k2eAgFnSc1d1	Eukleidova
grupa	grupa	k1gFnSc1	grupa
Euc	Euc	k1gFnSc2	Euc
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
translací	translace	k1gFnPc2	translace
<g/>
,	,	kIx,	,
rotací	rotace	k1gFnPc2	rotace
a	a	k8xC	a
zrcadlení	zrcadlení	k1gNnSc2	zrcadlení
n-rozměrného	nozměrný	k2eAgInSc2d1	n-rozměrný
Eukleidova	Eukleidův	k2eAgInSc2d1	Eukleidův
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
této	tento	k3xDgFnSc2	tento
grupy	grupa	k1gFnSc2	grupa
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
velikosti	velikost	k1gFnPc1	velikost
a	a	k8xC	a
úhly	úhel	k1gInPc1	úhel
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
pro	pro	k7c4	pro
projektivní	projektivní	k2eAgFnSc4d1	projektivní
geometrii	geometrie	k1gFnSc4	geometrie
pozůstává	pozůstávat	k5eAaImIp3nS	pozůstávat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
grupa	grupa	k1gFnSc1	grupa
symetrie	symetrie	k1gFnSc1	symetrie
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
kolineací	kolineace	k1gFnPc2	kolineace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
projektivní	projektivní	k2eAgFnSc2d1	projektivní
invarianty	invarianta	k1gFnSc2	invarianta
(	(	kIx(	(
<g/>
převádí	převádět	k5eAaImIp3nS	převádět
projektivní	projektivní	k2eAgFnPc4d1	projektivní
přímky	přímka	k1gFnPc4	přímka
na	na	k7c4	na
projektivní	projektivní	k2eAgFnPc4d1	projektivní
přímky	přímka	k1gFnPc4	přímka
a	a	k8xC	a
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
</s>
</p>
<p>
<s>
dvoupoměr	dvoupoměr	k1gInSc1	dvoupoměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc4	tento
grupy	grupa	k1gFnPc4	grupa
symetrií	symetrie	k1gFnPc2	symetrie
nějaké	nějaký	k3yIgFnSc2	nějaký
geometrie	geometrie	k1gFnSc2	geometrie
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
transformační	transformační	k2eAgFnSc2d1	transformační
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
pro	pro	k7c4	pro
běžné	běžný	k2eAgFnPc4d1	běžná
geometrie	geometrie	k1gFnPc4	geometrie
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tzv.	tzv.	kA	tzv.
Lieovy	Lieův	k2eAgFnSc2d1	Lieova
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Lieova	Lieův	k2eAgFnSc1d1	Lieova
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
transformační	transformační	k2eAgFnSc1d1	transformační
grupa	grupa	k1gFnSc1	grupa
nějakého	nějaký	k3yIgInSc2	nějaký
geometrického	geometrický	k2eAgInSc2d1	geometrický
prostoru	prostor	k1gInSc2	prostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tranzitivní	tranzitivní	k2eAgFnSc4d1	tranzitivní
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
</s>
</p>
<p>
<s>
definovat	definovat	k5eAaBmF	definovat
podgrupu	podgrupat	k5eAaPmIp1nS	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
všech	všecek	k3xTgFnPc2	všecek
transformací	transformace	k1gFnPc2	transformace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
jistý	jistý	k2eAgInSc4d1	jistý
bod	bod	k1gInSc4	bod
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
prostorem	prostor	k1gInSc7	prostor
rozkladových	rozkladový	k2eAgFnPc2d1	rozkladová
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
≃	≃	k?	≃
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
simeq	simeq	k?	simeq
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
H.	H.	kA	H.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
popis	popis	k1gInSc1	popis
geometrie	geometrie	k1gFnSc2	geometrie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Kleinova	Kleinův	k2eAgFnSc1d1	Kleinova
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
volba	volba	k1gFnSc1	volba
grup	grupa	k1gFnPc2	grupa
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
H	H	kA	H
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
Eukleidovskou	eukleidovský	k2eAgFnSc4d1	eukleidovská
<g/>
,	,	kIx,	,
afinní	afinní	k2eAgFnSc4d1	afinní
a	a	k8xC	a
projektivní	projektivní	k2eAgFnSc4d1	projektivní
geometrii	geometrie	k1gFnSc4	geometrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
některé	některý	k3yIgFnPc4	některý
geometrické	geometrický	k2eAgFnPc4d1	geometrická
struktury	struktura	k1gFnPc4	struktura
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
transformační	transformační	k2eAgFnSc4d1	transformační
grupu	grupa	k1gFnSc4	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Zobecnění	zobecnění	k1gNnSc1	zobecnění
těchto	tento	k3xDgFnPc2	tento
idejí	idea	k1gFnPc2	idea
na	na	k7c4	na
širší	široký	k2eAgFnSc4d2	širší
třídu	třída	k1gFnSc4	třída
geometrií	geometrie	k1gFnPc2	geometrie
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
zakřivené	zakřivený	k2eAgInPc4d1	zakřivený
prostory	prostor	k1gInPc4	prostor
v	v	k7c6	v
Riemannově	Riemannův	k2eAgFnSc6d1	Riemannova
geometrii	geometrie	k1gFnSc6	geometrie
rozpracoval	rozpracovat	k5eAaPmAgInS	rozpracovat
Élie	Élie	k1gInSc1	Élie
Cartan	Cartan	k1gInSc1	Cartan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
lineární	lineární	k2eAgFnSc1d1	lineární
grupa	grupa	k1gFnSc1	grupa
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
===	===	k?	===
</s>
</p>
<p>
<s>
Maticové	maticový	k2eAgFnPc1d1	maticová
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
matic	matice	k1gFnPc2	matice
a	a	k8xC	a
grupová	grupový	k2eAgFnSc1d1	grupová
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
maticové	maticový	k2eAgNnSc4d1	maticové
násobení	násobení	k1gNnSc4	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
lineární	lineární	k2eAgFnSc1d1	lineární
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
GL	GL	kA	GL
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
regulárních	regulární	k2eAgFnPc2d1	regulární
reálných	reálný	k2eAgFnPc2d1	reálná
čtvercových	čtvercový	k2eAgFnPc2d1	čtvercová
matic	matice	k1gFnPc2	matice
dimenze	dimenze	k1gFnSc2	dimenze
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
podgrupy	podgrupa	k1gFnPc1	podgrupa
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
maticové	maticový	k2eAgFnPc1d1	maticová
grupy	grupa	k1gFnPc1	grupa
anebo	anebo	k8xC	anebo
lineární	lineární	k2eAgFnPc1d1	lineární
grupy	grupa	k1gFnPc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Dihedrální	Dihedrální	k2eAgFnSc1d1	Dihedrální
grupa	grupa	k1gFnSc1	grupa
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
jako	jako	k9	jako
maticová	maticový	k2eAgFnSc1d1	maticová
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
symetrie	symetrie	k1gFnSc1	symetrie
čtverce	čtverec	k1gInSc2	čtverec
jako	jako	k8xC	jako
otočení	otočení	k1gNnSc2	otočení
nebo	nebo	k8xC	nebo
překlopení	překlopení	k1gNnSc2	překlopení
můžeme	moct	k5eAaImIp1nP	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
maticí	matice	k1gFnSc7	matice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
důležitá	důležitý	k2eAgFnSc1d1	důležitá
maticová	maticový	k2eAgFnSc1d1	maticová
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnSc1d1	speciální
ortogonální	ortogonální	k2eAgFnSc1d1	ortogonální
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
SO	So	kA	So
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
rotace	rotace	k1gFnPc4	rotace
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
rozměrném	rozměrný	k2eAgInSc6d1	rozměrný
Eukleidově	Eukleidův	k2eAgInSc6d1	Eukleidův
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
aplikace	aplikace	k1gFnSc1	aplikace
grupových	grupův	k2eAgInPc2d1	grupův
konceptů	koncept	k1gInPc2	koncept
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
důležitý	důležitý	k2eAgInSc4d1	důležitý
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
hlubší	hluboký	k2eAgNnSc4d2	hlubší
porozumění	porozumění	k1gNnSc4	porozumění
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
studuje	studovat	k5eAaImIp3nS	studovat
grupy	grupa	k1gFnPc4	grupa
pomocí	pomocí	k7c2	pomocí
jejich	jejich	k3xOp3gFnPc2	jejich
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
vektorových	vektorový	k2eAgInPc6d1	vektorový
prostorech	prostor	k1gInPc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1	reprezentace
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
V	V	kA	V
je	být	k5eAaImIp3nS	být
grupový	grupový	k2eAgInSc1d1	grupový
homomorfismus	homomorfismus	k1gInSc1	homomorfismus
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
:	:	kIx,	:
<g/>
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
GL	GL	kA	GL
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
obecné	obecný	k2eAgFnPc1d1	obecná
lineární	lineární	k2eAgFnPc1d1	lineární
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
GL	GL	kA	GL
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
grupová	grupová	k1gFnSc1	grupová
operace	operace	k1gFnSc2	operace
na	na	k7c4	na
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zadána	zadat	k5eAaPmNgFnS	zadat
abstraktním	abstraktní	k2eAgInSc7d1	abstraktní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
skládání	skládání	k1gNnSc4	skládání
lineárních	lineární	k2eAgNnPc2d1	lineární
zobrazení	zobrazení	k1gNnPc2	zobrazení
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
násobení	násobení	k1gNnSc1	násobení
matic	matice	k1gFnPc2	matice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
explicitní	explicitní	k2eAgInPc4d1	explicitní
počty	počet	k1gInPc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Grupová	Grupový	k2eAgFnSc1d1	Grupový
akce	akce	k1gFnSc1	akce
na	na	k7c6	na
nějakém	nějaký	k3yIgInSc6	nějaký
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
prostředkem	prostředek	k1gInSc7	prostředek
jak	jak	k8xC	jak
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
daného	daný	k2eAgInSc2d1	daný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
grupy	grupa	k1gFnSc2	grupa
samotné	samotný	k2eAgFnSc2d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
teorii	teorie	k1gFnSc4	teorie
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
algebraických	algebraický	k2eAgFnPc2d1	algebraická
grup	grupa	k1gFnPc2	grupa
a	a	k8xC	a
topologických	topologický	k2eAgFnPc2d1	topologická
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
(	(	kIx(	(
<g/>
lokálně	lokálně	k6eAd1	lokálně
<g/>
)	)	kIx)	)
kompaktních	kompaktní	k2eAgFnPc2d1	kompaktní
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
<g/>
Reprezentace	reprezentace	k1gFnSc1	reprezentace
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
mají	mít	k5eAaImIp3nP	mít
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
reprezentací	reprezentace	k1gFnPc2	reprezentace
grup	grupa	k1gFnPc2	grupa
v	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
nenulové	nulový	k2eNgFnSc2d1	nenulová
charakteristiky	charakteristika	k1gFnSc2	charakteristika
má	mít	k5eAaImIp3nS	mít
aplikace	aplikace	k1gFnSc1	aplikace
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
partie	partie	k1gFnPc1	partie
teorie	teorie	k1gFnSc2	teorie
reprezentací	reprezentace	k1gFnPc2	reprezentace
jsou	být	k5eAaImIp3nP	být
zobecněním	zobecnění	k1gNnSc7	zobecnění
klasické	klasický	k2eAgFnSc2d1	klasická
harmonické	harmonický	k2eAgFnSc2d1	harmonická
analýzy	analýza	k1gFnSc2	analýza
studující	studující	k2eAgFnSc2d1	studující
funkce	funkce	k1gFnSc2	funkce
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
řešení	řešení	k1gNnSc4	řešení
polynomických	polynomický	k2eAgFnPc2d1	polynomická
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
řešení	řešení	k1gNnSc4	řešení
kvadratické	kvadratický	k2eAgFnSc2d1	kvadratická
rovnice	rovnice	k1gFnSc2	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
px	px	k?	px
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
±	±	k?	±
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1,2	[number]	k4	1,2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
-p	-p	k?	-p
<g/>
\	\	kIx~	\
<g/>
pm	pm	k?	pm
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
q	q	k?	q
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
vzorce	vzorec	k1gInPc1	vzorec
jsou	být	k5eAaImIp3nP	být
známe	znát	k5eAaImIp1nP	znát
pro	pro	k7c4	pro
kubické	kubický	k2eAgFnPc4d1	kubická
a	a	k8xC	a
kvartické	kvartický	k2eAgFnPc4d1	kvartický
rovnice	rovnice	k1gFnPc4	rovnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistují	existovat	k5eNaImIp3nP	existovat
pro	pro	k7c4	pro
rovnice	rovnice	k1gFnPc4	rovnice
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
<g/>
Výměna	výměna	k1gFnSc1	výměna
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
výrazu	výraz	k1gInSc6	výraz
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
permutace	permutace	k1gFnSc1	permutace
obou	dva	k4xCgInPc2	dva
kořenů	kořen	k1gInPc2	kořen
rovnice	rovnice	k1gFnSc2	rovnice
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
grupová	grupový	k2eAgFnSc1d1	grupová
operace	operace	k1gFnSc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k2eAgFnPc1d1	Kořena
původní	původní	k2eAgFnPc1d1	původní
rovnice	rovnice	k1gFnPc1	rovnice
splňují	splňovat	k5eAaImIp3nP	splňovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-p	-p	k?	-p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
výměna	výměna	k1gFnSc1	výměna
kořenů	kořen	k1gInPc2	kořen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nemění	měnit	k5eNaImIp3nS	měnit
jejich	jejich	k3xOp3gNnSc1	jejich
součet	součet	k1gInSc1	součet
a	a	k8xC	a
součin	součin	k1gInSc1	součin
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obecný	obecný	k2eAgInSc4d1	obecný
polynom	polynom	k1gInSc4	polynom
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
definovat	definovat	k5eAaBmF	definovat
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
jako	jako	k8xS	jako
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
takových	takový	k3xDgFnPc2	takový
permutací	permutace	k1gFnPc2	permutace
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
že	že	k8xS	že
racionální	racionální	k2eAgInPc1d1	racionální
výrazy	výraz	k1gInPc1	výraz
kořenů	kořen	k1gInPc2	kořen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
nějaký	nějaký	k3yIgInSc4	nějaký
racionální	racionální	k2eAgInSc4d1	racionální
výraz	výraz	k1gInSc4	výraz
koeficientů	koeficient	k1gInPc2	koeficient
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-p	-p	k?	-p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abstraktní	abstraktní	k2eAgFnPc1d1	abstraktní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
Galoisovy	Galoisův	k2eAgFnPc4d1	Galoisova
grupy	grupa	k1gFnPc4	grupa
asociované	asociovaný	k2eAgFnPc4d1	asociovaná
s	s	k7c7	s
polynomem	polynom	k1gInSc7	polynom
dávají	dávat	k5eAaImIp3nP	dávat
kritérium	kritérium	k1gNnSc4	kritérium
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
polynom	polynom	k1gInSc4	polynom
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
vyjádřitelné	vyjádřitelný	k2eAgInPc4d1	vyjádřitelný
z	z	k7c2	z
koeficientů	koeficient	k1gInPc2	koeficient
pomocí	pomocí	k7c2	pomocí
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pomocí	pomocí	k7c2	pomocí
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
násobení	násobení	k1gNnSc2	násobení
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
-tých	ých	k1gInSc1	-tých
odmocnin	odmocnina	k1gFnPc2	odmocnina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
když	když	k8xS	když
příslušná	příslušný	k2eAgFnSc1d1	příslušná
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
polynomy	polynom	k1gInPc4	polynom
stupně	stupeň	k1gInSc2	stupeň
5	[number]	k4	5
však	však	k9	však
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
pozůstává	pozůstávat	k5eAaImIp3nS	pozůstávat
se	se	k3xPyFc4	se
všech	všecek	k3xTgInPc2	všecek
permutací	permutace	k1gFnSc7	permutace
pěti	pět	k4xCc2	pět
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Permutační	Permutační	k2eAgFnSc1d1	Permutační
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
však	však	k9	však
není	být	k5eNaImIp3nS	být
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
a	a	k8xC	a
proto	proto	k8xC	proto
obecný	obecný	k2eAgInSc1d1	obecný
vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
rovnice	rovnice	k1gFnPc4	rovnice
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
pouze	pouze	k6eAd1	pouze
sčítání	sčítání	k1gNnSc4	sčítání
<g/>
,	,	kIx,	,
násobení	násobení	k1gNnSc4	násobení
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc4	dělení
a	a	k8xC	a
odmocňování	odmocňování	k1gNnSc4	odmocňování
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
algebře	algebra	k1gFnSc6	algebra
se	se	k3xPyFc4	se
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
definuje	definovat	k5eAaBmIp3nS	definovat
obecněji	obecně	k6eAd2	obecně
pro	pro	k7c4	pro
tělesa	těleso	k1gNnPc4	těleso
jejich	jejich	k3xOp3gNnSc2	jejich
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nadtěleso	nadtělesa	k1gFnSc5	nadtělesa
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příslušná	příslušný	k2eAgFnSc1d1	příslušná
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Gal	Gal	k1gMnSc1	Gal
<g/>
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
automorfismů	automorfismus	k1gInPc2	automorfismus
tělesa	těleso	k1gNnSc2	těleso
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemění	měnit	k5eNaImIp3nP	měnit
prvky	prvek	k1gInPc7	prvek
tělesa	těleso	k1gNnSc2	těleso
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
věta	věta	k1gFnSc1	věta
Galoisovy	Galoisův	k2eAgFnSc2d1	Galoisova
teorie	teorie	k1gFnSc2	teorie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podgrupy	podgrup	k1gInPc1	podgrup
Galoisovy	Galoisův	k2eAgFnSc2d1	Galoisova
grupy	grupa	k1gFnSc2	grupa
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
mezitělesům	mezitěles	k1gMnPc3	mezitěles
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
⊆	⊆	k?	⊆
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
K	K	kA	K
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Grupy	grupa	k1gFnPc1	grupa
v	v	k7c6	v
algebraické	algebraický	k2eAgFnSc6d1	algebraická
topologii	topologie	k1gFnSc6	topologie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
algebraické	algebraický	k2eAgFnSc6d1	algebraická
topologii	topologie	k1gFnSc6	topologie
se	se	k3xPyFc4	se
topologickým	topologický	k2eAgInPc3d1	topologický
prostorům	prostor	k1gInPc3	prostor
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
různé	různý	k2eAgFnPc4d1	různá
grupy	grupa	k1gFnPc4	grupa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgMnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
Camille	Camille	k1gInSc4	Camille
Jordan	Jordan	k1gMnSc1	Jordan
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
definoval	definovat	k5eAaBmAgInS	definovat
Henri	Henre	k1gFnSc4	Henre
Poincaré	Poincarý	k2eAgInPc1d1	Poincarý
<g/>
.	.	kIx.	.
<g/>
Prvky	prvek	k1gInPc1	prvek
fundamentální	fundamentální	k2eAgFnSc2d1	fundamentální
grupy	grupa	k1gFnSc2	grupa
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
jako	jako	k9	jako
smyčky	smyčka	k1gFnPc1	smyčka
(	(	kIx(	(
<g/>
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
křivky	křivka	k1gFnPc1	křivka
<g/>
)	)	kIx)	)
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
smyčky	smyčka	k1gFnPc1	smyčka
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
stejný	stejný	k2eAgInSc4d1	stejný
prvek	prvek	k1gInSc4	prvek
fundamentální	fundamentální	k2eAgFnSc2d1	fundamentální
grupy	grupa	k1gFnSc2	grupa
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
převést	převést	k5eAaPmF	převést
spojitou	spojitý	k2eAgFnSc7d1	spojitá
deformací	deformace	k1gFnSc7	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrativní	ilustrativní	k2eAgInSc1d1	ilustrativní
obrázek	obrázek	k1gInSc1	obrázek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
křivku	křivka	k1gFnSc4	křivka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
bez	bez	k7c2	bez
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
křivka	křivka	k1gFnSc1	křivka
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
triviální	triviální	k2eAgInSc4d1	triviální
a	a	k8xC	a
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
fundamentální	fundamentální	k2eAgFnSc2d1	fundamentální
grupy	grupa	k1gFnSc2	grupa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
spojitě	spojitě	k6eAd1	spojitě
stáhnout	stáhnout	k5eAaPmF	stáhnout
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
oranžová	oranžový	k2eAgFnSc1d1	oranžová
křivka	křivka	k1gFnSc1	křivka
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
nedá	dát	k5eNaPmIp3nS	dát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uvnitř	uvnitř	k7c2	uvnitř
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
díra	díra	k1gFnSc1	díra
(	(	kIx(	(
<g/>
chybějící	chybějící	k2eAgInSc1d1	chybějící
bod	bod	k1gInSc1	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
grupa	grupa	k1gFnSc1	grupa
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yRgFnSc2	který
odstraníme	odstranit	k5eAaPmIp1nP	odstranit
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
cyklická	cyklický	k2eAgFnSc1d1	cyklická
grupa	grupa	k1gFnSc1	grupa
generována	generovat	k5eAaImNgFnS	generovat
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
křivkou	křivka	k1gFnSc7	křivka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
definují	definovat	k5eAaBmIp3nP	definovat
vyšší	vysoký	k2eAgFnPc1d2	vyšší
homotopické	homotopický	k2eAgFnPc1d1	homotopický
grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
odhalit	odhalit	k5eAaPmF	odhalit
díry	díra	k1gFnSc2	díra
různých	různý	k2eAgFnPc2d1	různá
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Homotopické	Homotopický	k2eAgFnPc1d1	Homotopický
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
topologické	topologický	k2eAgFnPc1d1	topologická
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
homotopické	homotopický	k2eAgInPc4d1	homotopický
invarianty	invariant	k1gInPc4	invariant
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
topologicky	topologicky	k6eAd1	topologicky
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
(	(	kIx(	(
<g/>
homeomorfní	homeomorfní	k2eAgInPc4d1	homeomorfní
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
homotopické	homotopický	k2eAgFnSc2d1	homotopický
resp.	resp.	kA	resp.
homotopicky	homotopicky	k6eAd1	homotopicky
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
izomorfní	izomorfní	k2eAgFnPc4d1	izomorfní
homotopické	homotopický	k2eAgFnPc4d1	homotopický
grupy	grupa	k1gFnPc4	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Spojitá	spojitý	k2eAgNnPc4d1	spojité
zobrazení	zobrazení	k1gNnPc4	zobrazení
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
indukují	indukovat	k5eAaBmIp3nP	indukovat
přirozeným	přirozený	k2eAgInSc7d1	přirozený
způsobem	způsob	k1gInSc7	způsob
homomorfismy	homomorfismus	k1gInPc4	homomorfismus
jejich	jejich	k3xOp3gFnPc2	jejich
homotopických	homotopický	k2eAgFnPc2d1	homotopický
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Homotopické	Homotopický	k2eAgFnPc1d1	Homotopický
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
kovariantního	kovariantní	k2eAgInSc2d1	kovariantní
funktoru	funktor	k1gInSc2	funktor
<g/>
.	.	kIx.	.
<g/>
Výpočet	výpočet	k1gInSc1	výpočet
vyšších	vysoký	k2eAgFnPc2d2	vyšší
homotopických	homotopický	k2eAgFnPc2d1	homotopický
grup	grupa	k1gFnPc2	grupa
je	být	k5eAaImIp3nS	být
však	však	k9	však
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
nejsou	být	k5eNaImIp3nP	být
obecně	obecně	k6eAd1	obecně
známy	znám	k2eAgFnPc1d1	známa
ani	ani	k8xC	ani
homotopické	homotopický	k2eAgFnPc1d1	homotopický
grupy	grupa	k1gFnPc1	grupa
sfér	sféra	k1gFnPc2	sféra
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
výpočet	výpočet	k1gInSc1	výpočet
je	být	k5eAaImIp3nS	být
algoritmicky	algoritmicky	k6eAd1	algoritmicky
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
homologické	homologický	k2eAgFnPc4d1	homologická
a	a	k8xC	a
kohomologické	kohomologický	k2eAgFnPc4d1	kohomologický
grupy	grupa	k1gFnPc4	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
homotopické	homotopický	k2eAgInPc1d1	homotopický
invarianty	invariant	k1gInPc1	invariant
<g/>
.	.	kIx.	.
</s>
<s>
Homologie	homologie	k1gFnSc1	homologie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
té	ten	k3xDgFnSc2	ten
dimenze	dimenze	k1gFnSc2	dimenze
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kovariantní	kovariantní	k2eAgInSc4d1	kovariantní
funktor	funktor	k1gInSc4	funktor
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kontravariantní	kontravariantní	k2eAgInSc4d1	kontravariantní
funktor	funktor	k1gInSc4	funktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využitím	využití	k1gNnSc7	využití
homotopických	homotopický	k2eAgFnPc2d1	homotopický
a	a	k8xC	a
homologických	homologický	k2eAgFnPc2d1	homologická
grup	grupa	k1gFnPc2	grupa
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
řešit	řešit	k5eAaImF	řešit
širokou	široký	k2eAgFnSc4d1	široká
třídu	třída	k1gFnSc4	třída
topologických	topologický	k2eAgFnPc2d1	topologická
problému	problém	k1gInSc3	problém
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
dokázat	dokázat	k5eAaPmF	dokázat
neexistence	neexistence	k1gFnSc1	neexistence
rozšíření	rozšíření	k1gNnSc2	rozšíření
spojitého	spojitý	k2eAgNnSc2d1	spojité
zobrazení	zobrazení	k1gNnSc2	zobrazení
s	s	k7c7	s
podprostoru	podprostor	k1gInSc3	podprostor
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
identita	identita	k1gFnSc1	identita
na	na	k7c6	na
sféře	sféra	k1gFnSc6	sféra
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
rozšířit	rozšířit	k5eAaPmF	rozšířit
na	na	k7c6	na
zobrazení	zobrazení	k1gNnSc6	zobrazení
celé	celý	k2eAgFnSc2d1	celá
koule	koule	k1gFnSc2	koule
na	na	k7c4	na
sféru	sféra	k1gFnSc4	sféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokazovat	dokazovat	k5eAaImF	dokazovat
různé	různý	k2eAgFnPc4d1	různá
věty	věta	k1gFnPc4	věta
o	o	k7c6	o
pevných	pevný	k2eAgInPc6d1	pevný
bodech	bod	k1gInPc6	bod
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Brouwerova	Brouwerův	k2eAgFnSc1d1	Brouwerův
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
pevném	pevný	k2eAgInSc6d1	pevný
bodu	bod	k1gInSc6	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokázat	dokázat	k5eAaPmF	dokázat
základní	základní	k2eAgFnSc4d1	základní
větu	věta	k1gFnSc4	věta
algebry	algebra	k1gFnSc2	algebra
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřené	otevřený	k2eAgFnPc1d1	otevřená
množiny	množina	k1gFnPc1	množina
v	v	k7c6	v
Eukleidovských	eukleidovský	k2eAgInPc6d1	eukleidovský
prostorech	prostor	k1gInPc6	prostor
jsou	být	k5eAaImIp3nP	být
homeomorfní	homeomorfní	k2eAgNnSc4d1	homeomorfní
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
dimenzi	dimenze	k1gFnSc4	dimenze
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
topologický	topologický	k2eAgInSc1d1	topologický
invariant	invariant	k1gInSc1	invariant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
využití	využití	k1gNnPc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
teoretických	teoretický	k2eAgFnPc2d1	teoretická
i	i	k8xC	i
praktických	praktický	k2eAgFnPc2d1	praktická
aplikací	aplikace	k1gFnPc2	aplikace
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgFnPc1d1	Konečné
grupy	grupa	k1gFnPc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Mathiovy	Mathiův	k2eAgFnPc1d1	Mathiův
grupy	grupa	k1gFnPc1	grupa
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
v	v	k7c6	v
kódování	kódování	k1gNnSc6	kódování
a	a	k8xC	a
v	v	k7c6	v
korekci	korekce	k1gFnSc6	korekce
chyb	chyba	k1gFnPc2	chyba
přenášených	přenášený	k2eAgFnPc2d1	přenášená
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Multiplikativní	multiplikativní	k2eAgFnPc1d1	multiplikativní
grupy	grupa	k1gFnPc1	grupa
konečných	konečný	k2eAgNnPc2d1	konečné
těles	těleso	k1gNnPc2	těleso
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
v	v	k7c6	v
cyklickém	cyklický	k2eAgNnSc6d1	cyklické
kódování	kódování	k1gNnSc1	kódování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
v	v	k7c6	v
CD	CD	kA	CD
přehrávačích	přehrávač	k1gInPc6	přehrávač
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
klasickou	klasický	k2eAgFnSc4d1	klasická
Galoisovu	Galoisův	k2eAgFnSc4d1	Galoisova
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
grupově	grupově	k6eAd1	grupově
teoretická	teoretický	k2eAgNnPc4d1	teoretické
kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
vlastnosti	vlastnost	k1gFnPc4	vlastnost
řešení	řešení	k1gNnSc4	řešení
jistých	jistý	k2eAgFnPc2d1	jistá
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
se	se	k3xPyFc4	se
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
využívají	využívat	k5eAaPmIp3nP	využívat
v	v	k7c6	v
algebraické	algebraický	k2eAgFnSc6d1	algebraická
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Kryptografie	kryptografie	k1gFnSc1	kryptografie
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
přístup	přístup	k1gInSc4	přístup
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
s	s	k7c7	s
výpočetní	výpočetní	k2eAgFnSc7d1	výpočetní
teorií	teorie	k1gFnSc7	teorie
grup	grupa	k1gFnPc2	grupa
implementovanou	implementovaný	k2eAgFnSc4d1	implementovaná
pro	pro	k7c4	pro
konečné	konečný	k2eAgFnPc4d1	konečná
grupy	grupa	k1gFnPc4	grupa
<g/>
.	.	kIx.	.
<g/>
Aplikace	aplikace	k1gFnSc1	aplikace
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
nejsou	být	k5eNaImIp3nP	být
omezeny	omezit	k5eAaPmNgInP	omezit
na	na	k7c4	na
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
konceptů	koncept	k1gInPc2	koncept
také	také	k9	také
čerpají	čerpat	k5eAaImIp3nP	čerpat
vědy	věda	k1gFnSc2	věda
jako	jako	k8xS	jako
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgFnSc2d1	Konečné
grupy	grupa	k1gFnSc2	grupa
==	==	k?	==
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c4	mnoho
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gInPc2	jejich
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
řád	řád	k1gInSc1	řád
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
permutací	permutace	k1gFnSc7	permutace
n-prvkové	nrvkový	k2eAgFnSc2d1	n-prvkový
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
symetrická	symetrický	k2eAgFnSc1d1	symetrická
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
grupu	grupa	k1gFnSc4	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
můžeme	moct	k5eAaImIp1nP	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
jako	jako	k8xS	jako
množinu	množina	k1gFnSc4	množina
permutací	permutace	k1gFnPc2	permutace
tří	tři	k4xCgInPc2	tři
písmen	písmeno	k1gNnPc2	písmeno
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
pozůstává	pozůstávat	k5eAaImIp3nS	pozůstávat
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
ACB	ACB	kA	ACB
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
CBA	CBA	kA	CBA
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Symetrické	symetrický	k2eAgFnPc1d1	symetrická
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgInSc7d1	základní
příkladem	příklad	k1gInSc7	příklad
konečných	konečný	k2eAgMnPc2d1	konečný
grupy	grupa	k1gFnSc2	grupa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
každá	každý	k3xTgFnSc1	každý
konečná	konečný	k2eAgFnSc1d1	konečná
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
podgrupa	podgrupa	k1gFnSc1	podgrupa
symetrické	symetrický	k2eAgFnSc2d1	symetrická
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
vhodné	vhodný	k2eAgNnSc4d1	vhodné
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Cayleyho	Cayley	k1gMnSc2	Cayley
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
množina	množina	k1gFnSc1	množina
symetrií	symetrie	k1gFnPc2	symetrie
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
dihedrální	dihedrální	k2eAgFnSc1d1	dihedrální
grupa	grupa	k1gFnSc1	grupa
D4	D4	k1gFnSc2	D4
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
je	být	k5eAaImIp3nS	být
grupou	grupa	k1gFnSc7	grupa
symetrií	symetrie	k1gFnPc2	symetrie
čtverce	čtverec	k1gInSc2	čtverec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgNnSc1d3	nejmenší
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
součin	součin	k1gInSc1	součin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kopií	kopie	k1gFnSc7	kopie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc4d1	rovno
neutrálnímu	neutrální	k2eAgInSc3d1	neutrální
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
každého	každý	k3xTgInSc2	každý
prvku	prvek	k1gInSc2	prvek
konečné	konečný	k2eAgFnSc2d1	konečná
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
určena	určit	k5eAaPmNgFnS	určit
svým	svůj	k3xOyFgInSc7	svůj
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
strukturou	struktura	k1gFnSc7	struktura
svých	svůj	k3xOyFgInPc2	svůj
podgrup	podgrup	k1gInSc1	podgrup
<g/>
.	.	kIx.	.
</s>
<s>
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
věta	věta	k1gFnSc1	věta
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
konečnou	konečný	k2eAgFnSc4d1	konečná
grupu	grupa	k1gFnSc4	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
počet	počet	k1gInSc4	počet
prvků	prvek	k1gInPc2	prvek
její	její	k3xOp3gFnSc2	její
libovolné	libovolný	k2eAgFnSc2d1	libovolná
podgrupy	podgrupa	k1gFnSc2	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dělí	dělit	k5eAaImIp3nS	dělit
počet	počet	k1gInSc1	počet
prvků	prvek	k1gInPc2	prvek
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Sylowovy	Sylowův	k2eAgFnPc1d1	Sylowův
věty	věta	k1gFnPc1	věta
dávají	dávat	k5eAaImIp3nP	dávat
část	část	k1gFnSc4	část
obráceného	obrácený	k2eAgNnSc2d1	obrácené
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dihedrální	Dihedrální	k2eAgFnSc1d1	Dihedrální
grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
uvedena	uveden	k2eAgFnSc1d1	uvedena
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
konečné	konečný	k2eAgFnSc2d1	konečná
grupy	grupa	k1gFnSc2	grupa
řádu	řád	k1gInSc2	řád
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
řád	řád	k1gInSc4	řád
podgrupy	podgrupa	k1gFnSc2	podgrupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kterou	který	k3yQgFnSc4	který
generuje	generovat	k5eAaImIp3nS	generovat
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
libovolné	libovolný	k2eAgFnSc2d1	libovolná
reflexe	reflexe	k1gFnSc2	reflexe
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
řády	řád	k1gInPc1	řád
dělí	dělit	k5eAaImIp3nP	dělit
číslo	číslo	k1gNnSc4	číslo
8	[number]	k4	8
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
grupy	grupa	k1gFnPc1	grupa
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
částečně	částečně	k6eAd1	částečně
popsat	popsat	k5eAaPmF	popsat
grafem	graf	k1gInSc7	graf
cyklů	cyklus	k1gInPc2	cyklus
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
vrcholy	vrchol	k1gInPc1	vrchol
grafu	graf	k1gInSc2	graf
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
prvkům	prvek	k1gInPc3	prvek
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
cyklickým	cyklický	k2eAgMnSc7d1	cyklický
podgrupám	podgrupat	k5eAaPmIp1nS	podgrupat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
hrany	hrana	k1gFnPc4	hrana
od	od	k7c2	od
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
od	od	k7c2	od
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
vpravo	vpravo	k6eAd1	vpravo
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
graf	graf	k1gInSc1	graf
cyklů	cyklus	k1gInPc2	cyklus
Dihedrální	Dihedrální	k2eAgFnSc2d1	Dihedrální
grupy	grupa	k1gFnSc2	grupa
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
grupy	grupa	k1gFnPc4	grupa
řádu	řád	k1gInSc2	řád
menšího	malý	k2eAgNnSc2d2	menší
než	než	k8xS	než
16	[number]	k4	16
určuje	určovat	k5eAaImIp3nS	určovat
graf	graf	k1gInSc1	graf
cyklů	cyklus	k1gInPc2	cyklus
grupu	grupa	k1gFnSc4	grupa
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
důležité	důležitý	k2eAgInPc1d1	důležitý
příklady	příklad	k1gInPc1	příklad
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
jsou	být	k5eAaImIp3nP	být
multiplikativní	multiplikativní	k2eAgFnPc1d1	multiplikativní
grupy	grupa	k1gFnPc1	grupa
konečných	konečný	k2eAgNnPc2d1	konečné
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
grupy	grupa	k1gFnSc2	grupa
regulárních	regulární	k2eAgFnPc2d1	regulární
<g/>
,	,	kIx,	,
ortogonálních	ortogonální	k2eAgFnPc2d1	ortogonální
respektive	respektive	k9	respektive
symplektických	symplektický	k2eAgFnPc2d1	symplektická
matic	matice	k1gFnPc2	matice
nad	nad	k7c7	nad
konečnými	konečný	k2eAgNnPc7d1	konečné
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikace	klasifikace	k1gFnPc1	klasifikace
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
co	co	k9	co
klasifikace	klasifikace	k1gFnSc1	klasifikace
konečných	konečný	k2eAgFnPc2d1	konečná
Abelových	Abelův	k2eAgFnPc2d1	Abelova
grup	grupa	k1gFnPc2	grupa
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
klasifikaci	klasifikace	k1gFnSc6	klasifikace
všech	všecek	k3xTgFnPc2	všecek
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
hluboké	hluboký	k2eAgInPc4d1	hluboký
a	a	k8xC	a
složité	složitý	k2eAgInPc4d1	složitý
matematické	matematický	k2eAgInPc4d1	matematický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lagrangeovy	Lagrangeův	k2eAgFnSc2d1	Lagrangeova
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
konečné	konečný	k2eAgFnSc2d1	konečná
grupy	grupa	k1gFnSc2	grupa
prvočíselného	prvočíselný	k2eAgInSc2d1	prvočíselný
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
nutně	nutně	k6eAd1	nutně
cyklické	cyklický	k2eAgFnPc1d1	cyklická
a	a	k8xC	a
tedy	tedy	k9	tedy
izomorfní	izomorfní	k2eAgFnSc3d1	izomorfní
grupě	grupa	k1gFnSc3	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
O	o	k7c6	o
grupách	grupa	k1gFnPc6	grupa
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
víme	vědět	k5eAaImIp1nP	vědět
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
Abelovy	Abelův	k2eAgInPc1d1	Abelův
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
už	už	k9	už
ale	ale	k8xC	ale
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
grupy	grupa	k1gFnPc4	grupa
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
příklad	příklad	k1gInSc4	příklad
dihedrální	dihedrální	k2eAgFnSc2d1	dihedrální
grupy	grupa	k1gFnSc2	grupa
D4	D4	k1gFnSc2	D4
řádu	řád	k1gInSc2	řád
8	[number]	k4	8
=	=	kIx~	=
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
nízkých	nízký	k2eAgInPc2d1	nízký
řádů	řád	k1gInPc2	řád
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
popsat	popsat	k5eAaPmF	popsat
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
počítačových	počítačový	k2eAgInPc2d1	počítačový
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
např.	např.	kA	např.
computer	computer	k1gInSc1	computer
algebra	algebra	k1gFnSc1	algebra
system	syst	k1gInSc7	syst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
známe	znát	k5eAaImIp1nP	znát
až	až	k6eAd1	až
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
2000	[number]	k4	2000
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
izomorfismus	izomorfismus	k1gInSc4	izomorfismus
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
všech	všecek	k3xTgFnPc2	všecek
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
však	však	k9	však
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezistupeň	mezistupeň	k1gInSc1	mezistupeň
v	v	k7c6	v
porozumění	porozumění	k1gNnSc6	porozumění
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
představuje	představovat	k5eAaImIp3nS	představovat
klasifikace	klasifikace	k1gFnSc1	klasifikace
konečných	konečný	k2eAgFnPc2d1	konečná
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Netriviální	triviální	k2eNgFnSc1d1	netriviální
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jediné	jediné	k1gNnSc4	jediné
její	její	k3xOp3gFnSc2	její
normální	normální	k2eAgFnSc2d1	normální
podgrupy	podgrupa	k1gFnSc2	podgrupa
jsou	být	k5eAaImIp3nP	být
grupa	grupa	k1gFnSc1	grupa
triviální	triviální	k2eAgFnSc1d1	triviální
(	(	kIx(	(
<g/>
jednoprvková	jednoprvkový	k2eAgFnSc1d1	jednoprvková
<g/>
)	)	kIx)	)
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
–	–	k?	–
<g/>
Hölderova	Hölderův	k2eAgFnSc1d1	Hölderův
věta	věta	k1gFnSc1	věta
popisuje	popisovat	k5eAaImIp3nS	popisovat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
grupy	grupa	k1gFnPc4	grupa
jako	jako	k8xC	jako
základní	základní	k2eAgInPc4d1	základní
prvky	prvek	k1gInPc4	prvek
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
obecných	obecný	k2eAgFnPc2d1	obecná
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
<g/>
Dokončení	dokončení	k1gNnSc1	dokončení
seznamu	seznam	k1gInSc2	seznam
všech	všecek	k3xTgFnPc2	všecek
konečných	konečný	k2eAgFnPc2d1	konečná
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
grup	grupa	k1gFnPc2	grupa
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
současné	současný	k2eAgFnSc2d1	současná
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věta	věta	k1gFnSc1	věta
o	o	k7c6	o
klasifikaci	klasifikace	k1gFnSc6	klasifikace
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
konečných	konečný	k2eAgFnPc2d1	konečná
grup	grupa	k1gFnPc2	grupa
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
konečná	konečný	k2eAgFnSc1d1	konečná
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
grupa	grupa	k1gFnSc1	grupa
spadá	spadat	k5eAaImIp3nS	spadat
buďto	buďto	k8xC	buďto
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
18	[number]	k4	18
nekonečných	konečný	k2eNgFnPc2d1	nekonečná
skupin	skupina	k1gFnPc2	skupina
grup	grupa	k1gFnPc2	grupa
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
26	[number]	k4	26
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
sporadických	sporadický	k2eAgFnPc2d1	sporadická
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
plně	plně	k6eAd1	plně
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
všechny	všechen	k3xTgFnPc4	všechen
konečné	konečný	k2eAgFnPc4d1	konečná
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
grupy	grupa	k1gFnPc4	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ohromné	ohromný	k2eAgFnSc3d1	ohromná
náročnosti	náročnost	k1gFnSc3	náročnost
jejího	její	k3xOp3gInSc2	její
důkazu	důkaz	k1gInSc2	důkaz
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
také	také	k9	také
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
Enormous	Enormous	k1gInSc1	Enormous
theorem	theor	k1gInSc7	theor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důkaz	důkaz	k1gInSc4	důkaz
této	tento	k3xDgFnSc2	tento
věty	věta	k1gFnSc2	věta
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
uveřejněn	uveřejněn	k2eAgInSc1d1	uveřejněn
v	v	k7c6	v
celku	celek	k1gInSc6	celek
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
článků	článek	k1gInPc2	článek
od	od	k7c2	od
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
autorů	autor	k1gMnPc2	autor
uveřejněných	uveřejněný	k2eAgMnPc2d1	uveřejněný
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
matematických	matematický	k2eAgInPc6d1	matematický
časopisech	časopis	k1gInPc6	časopis
převážně	převážně	k6eAd1	převážně
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1955	[number]	k4	1955
a	a	k8xC	a
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
důkazu	důkaz	k1gInSc2	důkaz
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
000	[number]	k4	000
stran	strana	k1gFnPc2	strana
tištěného	tištěný	k2eAgInSc2d1	tištěný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
rozsáhlost	rozsáhlost	k1gFnSc1	rozsáhlost
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
věty	věta	k1gFnSc2	věta
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
barvách	barva	k1gFnPc6	barva
<g/>
)	)	kIx)	)
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
důkazu	důkaz	k1gInSc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
matematik	matematik	k1gMnSc1	matematik
totiž	totiž	k9	totiž
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nepřečetl	přečíst	k5eNaPmAgInS	přečíst
tento	tento	k3xDgInSc1	tento
důkaz	důkaz	k1gInSc1	důkaz
celý	celý	k2eAgInSc1d1	celý
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
část	část	k1gFnSc1	část
důkazu	důkaz	k1gInSc2	důkaz
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
téměř	téměř	k6eAd1	téměř
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
však	však	k9	však
byla	být	k5eAaImAgFnS	být
mnoha	mnoho	k4c3	mnoho
matematiky	matematika	k1gFnSc2	matematika
přečtena	přečíst	k5eAaPmNgFnS	přečíst
a	a	k8xC	a
uznána	uznat	k5eAaPmNgFnS	uznat
za	za	k7c4	za
správnou	správný	k2eAgFnSc4d1	správná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
důkaz	důkaz	k1gInSc1	důkaz
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
správný	správný	k2eAgInSc4d1	správný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Grupy	grupa	k1gFnPc1	grupa
s	s	k7c7	s
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
strukturou	struktura	k1gFnSc7	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
grup	grupa	k1gFnPc2	grupa
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
příklady	příklad	k1gInPc1	příklad
jiných	jiný	k2eAgFnPc2d1	jiná
matematických	matematický	k2eAgFnPc2d1	matematická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
teorie	teorie	k1gFnSc2	teorie
kategorií	kategorie	k1gFnPc2	kategorie
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
grupové	grupový	k2eAgInPc1d1	grupový
objekty	objekt	k1gInPc1	objekt
nějaké	nějaký	k3yIgFnSc2	nějaký
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
objekty	objekt	k1gInPc1	objekt
a	a	k8xC	a
morfismy	morfismus	k1gInPc1	morfismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
s	s	k7c7	s
grupovou	grupová	k1gFnSc7	grupová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Topologické	topologický	k2eAgFnSc2d1	topologická
grupy	grupa	k1gFnSc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
topologické	topologický	k2eAgInPc1d1	topologický
prostory	prostor	k1gInPc1	prostor
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
grupovým	grupův	k2eAgNnSc7d1	grupův
násobením	násobení	k1gNnSc7	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
takovou	takový	k3xDgFnSc4	takový
grupu	grupa	k1gFnSc4	grupa
nazvali	nazvat	k5eAaPmAgMnP	nazvat
topologickou	topologický	k2eAgFnSc4d1	topologická
grupu	grupa	k1gFnSc4	grupa
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
obě	dva	k4xCgFnPc1	dva
operace	operace	k1gFnPc1	operace
vzájemně	vzájemně	k6eAd1	vzájemně
kompatibilní	kompatibilní	k2eAgFnPc1d1	kompatibilní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
grupové	grupový	k2eAgNnSc1d1	grupový
násobení	násobení	k1gNnSc1	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
h	h	k?	h
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
závisí	záviset	k5eAaImIp3nS	záviset
spojitě	spojitě	k6eAd1	spojitě
na	na	k7c4	na
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
také	také	k9	také
inverze	inverze	k1gFnSc1	inverze
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
spojitou	spojitý	k2eAgFnSc7d1	spojitá
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Nejzákladnějším	základní	k2eAgInSc7d3	nejzákladnější
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
nenulová	nulový	k2eNgNnPc1d1	nenulové
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
s	s	k7c7	s
násobením	násobení	k1gNnSc7	násobení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
libovolné	libovolný	k2eAgNnSc4d1	libovolné
topologické	topologický	k2eAgNnSc4d1	topologické
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
anebo	anebo	k8xC	anebo
p-adická	pdický	k2eAgNnPc4d1	p-adický
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
grupy	grupa	k1gFnPc1	grupa
jsou	být	k5eAaImIp3nP	být
lokálně	lokálně	k6eAd1	lokálně
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
invariantní	invariantní	k2eAgFnSc4d1	invariantní
Haarovu	Haarův	k2eAgFnSc4d1	Haarův
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
grupě	grupa	k1gFnSc6	grupa
integrovat	integrovat	k5eAaBmF	integrovat
a	a	k8xC	a
studovat	studovat	k5eAaImF	studovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
grupy	grupa	k1gFnSc2	grupa
pomocí	pomocí	k7c2	pomocí
harmonické	harmonický	k2eAgFnSc2d1	harmonická
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Invariance	invariance	k1gFnSc1	invariance
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
dx	dx	k?	dx
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
dx	dx	k?	dx
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
prvek	prvek	k1gInSc4	prvek
grupy	grupa	k1gFnSc2	grupa
c.	c.	k?	c.
</s>
</p>
<p>
<s>
Maticové	maticový	k2eAgFnPc4d1	maticová
grupy	grupa	k1gFnPc4	grupa
nad	nad	k7c7	nad
těmito	tento	k3xDgNnPc7	tento
tělesy	těleso	k1gNnPc7	těleso
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
lokálně	lokálně	k6eAd1	lokálně
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
topologické	topologický	k2eAgFnPc1d1	topologická
grupy	grupa	k1gFnPc1	grupa
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
adély	adél	k1gInPc4	adél
a	a	k8xC	a
adelické	adelický	k2eAgFnPc4d1	adelický
algebraické	algebraický	k2eAgFnPc4d1	algebraická
grupy	grupa	k1gFnPc4	grupa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
<g/>
Galoisovy	Galoisův	k2eAgFnSc2d1	Galoisova
grupy	grupa	k1gFnSc2	grupa
rozšíření	rozšíření	k1gNnSc2	rozšíření
těles	těleso	k1gNnPc2	těleso
nekonečného	konečný	k2eNgInSc2d1	nekonečný
stupně	stupeň	k1gInSc2	stupeň
jako	jako	k8xC	jako
například	například	k6eAd1	například
absolutní	absolutní	k2eAgFnSc1d1	absolutní
Galoisova	Galoisův	k2eAgFnSc1d1	Galoisova
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
přirozeně	přirozeně	k6eAd1	přirozeně
vybavit	vybavit	k5eAaPmF	vybavit
tzv.	tzv.	kA	tzv.
Krullovou	Krullův	k2eAgFnSc7d1	Krullův
topologií	topologie	k1gFnSc7	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Zobecněním	zobecnění	k1gNnSc7	zobecnění
těchto	tento	k3xDgFnPc2	tento
idejí	idea	k1gFnPc2	idea
adaptovaným	adaptovaný	k2eAgFnPc3d1	adaptovaná
na	na	k7c6	na
potřeby	potřeba	k1gFnPc4	potřeba
algebraické	algebraický	k2eAgFnSc2d1	algebraická
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
etální	etální	k2eAgFnSc1d1	etální
fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lieovy	Lieův	k2eAgFnSc2d1	Lieova
grupy	grupa	k1gFnSc2	grupa
===	===	k?	===
</s>
</p>
<p>
<s>
Lieovy	Lieův	k2eAgFnPc4d1	Lieova
grupy	grupa	k1gFnPc4	grupa
(	(	kIx(	(
<g/>
pojmenovány	pojmenován	k2eAgFnPc4d1	pojmenována
po	po	k7c6	po
Sophusi	Sophuse	k1gFnSc6	Sophuse
Lieovi	Lieův	k2eAgMnPc1d1	Lieův
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
současně	současně	k6eAd1	současně
strukturu	struktura	k1gFnSc4	struktura
hladké	hladký	k2eAgFnSc2d1	hladká
variety	varieta	k1gFnSc2	varieta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jsou	být	k5eAaImIp3nP	být
lokálně	lokálně	k6eAd1	lokálně
difeomorfní	difeomorfní	k2eAgInSc1d1	difeomorfní
Eukleidovskému	eukleidovský	k2eAgInSc3d1	eukleidovský
prostoru	prostor	k1gInSc3	prostor
dané	daný	k2eAgFnSc2d1	daná
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
variety	varieta	k1gFnSc2	varieta
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
grupy	grupa	k1gFnSc2	grupa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
násobení	násobený	k2eAgMnPc1d1	násobený
a	a	k8xC	a
inverze	inverze	k1gFnSc1	inverze
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
diferencovatelné	diferencovatelný	k2eAgInPc1d1	diferencovatelný
<g/>
)	)	kIx)	)
zobrazení	zobrazení	k1gNnSc6	zobrazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
Lieovy	Lieův	k2eAgFnSc2d1	Lieova
grupy	grupa	k1gFnSc2	grupa
je	být	k5eAaImIp3nS	být
obecná	obecný	k2eAgFnSc1d1	obecná
lineární	lineární	k2eAgFnSc1d1	lineární
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
regulárních	regulární	k2eAgFnPc2d1	regulární
reálných	reálný	k2eAgFnPc2d1	reálná
nebo	nebo	k8xC	nebo
komplexních	komplexní	k2eAgFnPc2d1	komplexní
matic	matice	k1gFnPc2	matice
dimenze	dimenze	k1gFnSc2	dimenze
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
otevřená	otevřený	k2eAgFnSc1d1	otevřená
množina	množina	k1gFnSc1	množina
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
všech	všecek	k3xTgFnPc2	všecek
matic	matice	k1gFnPc2	matice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
nerovností	nerovnost	k1gFnSc7	nerovnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
det	det	k?	det
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
≠	≠	k?	≠
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
det	det	k?	det
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
matice	matice	k1gFnSc1	matice
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obecné	obecný	k2eAgFnSc2d1	obecná
lineární	lineární	k2eAgFnSc2d1	lineární
grupy	grupa	k1gFnSc2	grupa
existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgFnPc4d1	další
série	série	k1gFnPc4	série
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
klasické	klasický	k2eAgFnSc2d1	klasická
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Speciální	speciální	k2eAgFnSc1d1	speciální
lineární	lineární	k2eAgFnSc1d1	lineární
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pozůstává	pozůstávat	k5eAaImIp3nS	pozůstávat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
matic	matice	k1gFnPc2	matice
s	s	k7c7	s
determinantem	determinant	k1gInSc7	determinant
rovným	rovný	k2eAgInSc7d1	rovný
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
ortogonální	ortogonální	k2eAgFnSc2d1	ortogonální
lineární	lineární	k2eAgFnSc2d1	lineární
grupy	grupa	k1gFnSc2	grupa
<g/>
,	,	kIx,	,
unitární	unitární	k2eAgFnSc2d1	unitární
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
symplektické	symplektický	k2eAgFnSc2d1	symplektická
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lieovy	Lieův	k2eAgFnPc1d1	Lieova
grupy	grupa	k1gFnPc1	grupa
mají	mít	k5eAaImIp3nP	mít
úzkou	úzký	k2eAgFnSc4d1	úzká
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
Lieovýma	Lieův	k2eAgFnPc7d1	Lieova
algebrami	algebra	k1gFnPc7	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Lieova	Lieův	k2eAgFnSc1d1	Lieova
algebra	algebra	k1gFnSc1	algebra
Lieovy	Lieův	k2eAgFnSc2d1	Lieova
grupy	grupa	k1gFnSc2	grupa
popisuje	popisovat	k5eAaImIp3nS	popisovat
lokální	lokální	k2eAgFnPc4d1	lokální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Wilhelm	Wilhelm	k6eAd1	Wilhelm
Killing	Killing	k1gInSc1	Killing
a	a	k8xC	a
Élie	Élie	k1gFnSc7	Élie
Cartan	Cartan	k1gInSc4	Cartan
popsali	popsat	k5eAaPmAgMnP	popsat
klasifikaci	klasifikace	k1gFnSc4	klasifikace
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
Lieových	Lieův	k2eAgFnPc2d1	Lieova
algeber	algebra	k1gFnPc2	algebra
nad	nad	k7c7	nad
komplexními	komplexní	k2eAgNnPc7d1	komplexní
čísly	číslo	k1gNnPc7	číslo
a	a	k8xC	a
reálnými	reálný	k2eAgNnPc7d1	reálné
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
komplexní	komplexní	k2eAgFnSc1d1	komplexní
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
Lieova	Lieův	k2eAgFnSc1d1	Lieova
algebra	algebra	k1gFnSc1	algebra
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
4	[number]	k4	4
nekonečných	konečný	k2eNgFnPc2d1	nekonečná
sérií	série	k1gFnPc2	série
anebo	anebo	k8xC	anebo
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
Lieových	Lieův	k2eAgFnPc2d1	Lieova
algeber	algebra	k1gFnPc2	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
algebrám	algebra	k1gFnPc3	algebra
náleží	náležet	k5eAaImIp3nP	náležet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
až	až	k8xS	až
na	na	k7c6	na
nakrytí	nakrytí	k1gNnSc6	nakrytí
<g/>
)	)	kIx)	)
speciální	speciální	k2eAgFnPc1d1	speciální
lineární	lineární	k2eAgFnPc1d1	lineární
grupy	grupa	k1gFnPc1	grupa
<g/>
,	,	kIx,	,
ortogonální	ortogonální	k2eAgFnPc1d1	ortogonální
lineární	lineární	k2eAgFnPc1d1	lineární
grupy	grupa	k1gFnPc1	grupa
liché	lichý	k2eAgFnPc1d1	lichá
a	a	k8xC	a
sudé	sudý	k2eAgFnPc1d1	sudá
dimenze	dimenze	k1gFnPc1	dimenze
<g/>
,	,	kIx,	,
symplektické	symplektický	k2eAgFnPc1d1	symplektická
grupy	grupa	k1gFnPc1	grupa
a	a	k8xC	a
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
Lieovy	Lieův	k2eAgFnPc1d1	Lieova
grupy	grupa	k1gFnPc1	grupa
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc4	všechen
nad	nad	k7c7	nad
komplexními	komplexní	k2eAgNnPc7d1	komplexní
čísly	číslo	k1gNnPc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
komplexním	komplexní	k2eAgFnPc3d1	komplexní
grupám	grupa	k1gFnPc3	grupa
existuje	existovat	k5eAaImIp3nS	existovat
vícero	vícero	k1gNnSc1	vícero
reálných	reálný	k2eAgFnPc2d1	reálná
forem	forma	k1gFnPc2	forma
(	(	kIx(	(
<g/>
ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
jedna	jeden	k4xCgFnSc1	jeden
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Lieovy	Lieův	k2eAgFnPc1d1	Lieova
grupy	grupa	k1gFnPc1	grupa
mají	mít	k5eAaImIp3nP	mít
zásadní	zásadní	k2eAgFnSc4d1	zásadní
důležitost	důležitost	k1gFnSc4	důležitost
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
:	:	kIx,	:
Noetherové	Noetherové	k2eAgFnSc1d1	Noetherové
věta	věta	k1gFnSc1	věta
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
symetrie	symetrie	k1gFnSc2	symetrie
a	a	k8xC	a
kvantity	kvantita	k1gFnSc2	kvantita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
translace	translace	k1gFnSc1	translace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnPc1d1	základní
symetrie	symetrie	k1gFnPc1	symetrie
zákonů	zákon	k1gInPc2	zákon
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
příklad	příklad	k1gInSc1	příklad
tvoří	tvořit	k5eAaImIp3nP	tvořit
Lorentzovy	Lorentzův	k2eAgFnPc1d1	Lorentzova
transformace	transformace	k1gFnPc1	transformace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
měření	měření	k1gNnSc2	měření
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
času	čas	k1gInSc2	čas
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
takových	takový	k3xDgFnPc2	takový
transformací	transformace	k1gFnPc2	transformace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lorentzova	Lorentzův	k2eAgFnSc1d1	Lorentzova
grupa	grupa	k1gFnSc1	grupa
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
rotační	rotační	k2eAgFnSc1d1	rotační
symetrie	symetrie	k1gFnSc1	symetrie
Minkowského	Minkowský	k2eAgInSc2d1	Minkowský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
časoprostoru	časoprostor	k1gInSc2	časoprostor
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Grupa	grupa	k1gFnSc1	grupa
všech	všecek	k3xTgFnPc2	všecek
symetrií	symetrie	k1gFnPc2	symetrie
Minkowského	Minkowský	k2eAgInSc2d1	Minkowský
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
translace	translace	k1gFnSc1	translace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Poincarého	Poincarý	k2eAgInSc2d1	Poincarý
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Lieova	Lieův	k2eAgFnSc1d1	Lieova
grupa	grupa	k1gFnSc1	grupa
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Unitární	unitární	k2eAgFnPc1d1	unitární
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
SU	SU	k?	SU
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
SU	SU	k?	SU
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k9	jako
grupy	grupa	k1gFnPc1	grupa
symetrií	symetrie	k1gFnPc2	symetrie
některých	některý	k3yIgFnPc2	některý
částicových	částicový	k2eAgFnPc2d1	částicová
teorií	teorie	k1gFnPc2	teorie
a	a	k8xC	a
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
Lieovy	Lieův	k2eAgFnPc1d1	Lieova
grupy	grupa	k1gFnPc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc6	E_
<g/>
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
často	často	k6eAd1	často
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc6d1	kvantová
gravitaci	gravitace	k1gFnSc6	gravitace
<g/>
.	.	kIx.	.
<g/>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
studia	studio	k1gNnSc2	studio
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
je	být	k5eAaImIp3nS	být
studium	studium	k1gNnSc1	studium
jejich	jejich	k3xOp3gFnPc2	jejich
reprezentací	reprezentace	k1gFnPc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
reprezentace	reprezentace	k1gFnPc1	reprezentace
mají	mít	k5eAaImIp3nP	mít
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
také	také	k9	také
zobecnit	zobecnit	k5eAaPmF	zobecnit
klasickou	klasický	k2eAgFnSc4d1	klasická
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
analýzu	analýza	k1gFnSc4	analýza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
funkce	funkce	k1gFnPc4	funkce
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jejich	jejich	k3xOp3gFnSc2	jejich
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
<g/>
,	,	kIx,	,
na	na	k7c4	na
funkce	funkce	k1gFnPc4	funkce
definované	definovaný	k2eAgFnPc4d1	definovaná
na	na	k7c6	na
Lieových	Lieův	k2eAgFnPc6d1	Lieova
grupách	grupa	k1gFnPc6	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zobecnění	zobecnění	k1gNnSc2	zobecnění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
algebře	algebra	k1gFnSc6	algebra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
obecnější	obecní	k2eAgFnSc2d2	obecní
struktury	struktura	k1gFnSc2	struktura
vynecháním	vynechání	k1gNnSc7	vynechání
některých	některý	k3yIgInPc2	některý
axiomů	axiom	k1gInPc2	axiom
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
například	například	k6eAd1	například
vynecháme	vynechat	k5eAaPmIp1nP	vynechat
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
grupy	grupa	k1gFnSc2	grupa
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
prvku	prvek	k1gInSc3	prvek
existoval	existovat	k5eAaImAgMnS	existovat
inverzní	inverzní	k2eAgInSc4d1	inverzní
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
výsledná	výsledný	k2eAgFnSc1d1	výsledná
algebraická	algebraický	k2eAgFnSc1d1	algebraická
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
monoid	monoid	k1gInSc1	monoid
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nuly	nula	k1gFnSc2	nula
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
tvoří	tvořit	k5eAaImIp3nP	tvořit
monoid	monoid	k1gInSc4	monoid
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
obecná	obecný	k2eAgFnSc1d1	obecná
metoda	metoda	k1gFnSc1	metoda
jak	jak	k6eAd1	jak
formálně	formálně	k6eAd1	formálně
přidat	přidat	k5eAaPmF	přidat
inverzní	inverzní	k2eAgInPc4d1	inverzní
prvky	prvek	k1gInPc4	prvek
k	k	k7c3	k
libovolnému	libovolný	k2eAgInSc3d1	libovolný
komutativnímu	komutativní	k2eAgInSc3d1	komutativní
monoidu	monoid	k1gInSc3	monoid
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
odvozena	odvozen	k2eAgNnPc1d1	odvozeno
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
od	od	k7c2	od
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
∖	∖	k?	∖
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
backslash	backslash	k1gInSc1	backslash
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
takto	takto	k6eAd1	takto
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
grupa	grupa	k1gFnSc1	grupa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Grothendieckova	Grothendieckův	k2eAgFnSc1d1	Grothendieckův
grupa	grupa	k1gFnSc1	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
algebraické	algebraický	k2eAgFnSc2d1	algebraická
struktury	struktura	k1gFnSc2	struktura
je	být	k5eAaImIp3nS	být
kvazigrupa	kvazigrupa	k1gFnSc1	kvazigrupa
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yQgFnPc4	který
sice	sice	k8xC	sice
neexistuje	existovat	k5eNaImIp3nS	existovat
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
rovnice	rovnice	k1gFnPc1	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
mají	mít	k5eAaImIp3nP	mít
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
pouze	pouze	k6eAd1	pouze
binární	binární	k2eAgFnSc1d1	binární
operace	operace	k1gFnSc1	operace
bez	bez	k7c2	bez
žádných	žádný	k3yNgInPc2	žádný
dalších	další	k2eAgInPc2d1	další
předpokladů	předpoklad	k1gInPc2	předpoklad
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
grupoid	grupoid	k1gInSc1	grupoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
matematické	matematický	k2eAgInPc1d1	matematický
pojmy	pojem	k1gInPc1	pojem
zobecňující	zobecňující	k2eAgFnSc4d1	zobecňující
grupu	grupa	k1gFnSc4	grupa
jsou	být	k5eAaImIp3nP	být
morfismy	morfismus	k1gInPc4	morfismus
nějaké	nějaký	k3yIgFnSc2	nějaký
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Morfismy	Morfismus	k1gInPc1	Morfismus
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
skládat	skládat	k5eAaImF	skládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
složení	složení	k1gNnSc1	složení
splňuje	splňovat	k5eAaImIp3nS	splňovat
asociativní	asociativní	k2eAgInSc4d1	asociativní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
obecně	obecně	k6eAd1	obecně
nemusí	muset	k5eNaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
inverzní	inverzní	k2eAgInPc4d1	inverzní
morfismy	morfismus	k1gInPc4	morfismus
a	a	k8xC	a
také	také	k9	také
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
složit	složit	k5eAaPmF	složit
libovolné	libovolný	k2eAgInPc4d1	libovolný
morfismy	morfismus	k1gInPc4	morfismus
(	(	kIx(	(
<g/>
jenom	jenom	k9	jenom
prvky	prvek	k1gInPc4	prvek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Mor	mor	k1gInSc1	mor
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Mor	mor	k1gInSc1	mor
<g/>
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
morfismus	morfismus	k1gInSc4	morfismus
izomorfismem	izomorfismus	k1gInSc7	izomorfismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
grupoid	grupoid	k1gInSc1	grupoid
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Morfismy	Morfismus	k1gInPc1	Morfismus
tohoto	tento	k3xDgInSc2	tento
objektu	objekt	k1gInSc2	objekt
splňují	splňovat	k5eAaImIp3nP	splňovat
asociativitu	asociativita	k1gFnSc4	asociativita
<g/>
,	,	kIx,	,
existenci	existence	k1gFnSc4	existence
neutrálního	neutrální	k2eAgMnSc2d1	neutrální
i	i	k8xC	i
inverzního	inverzní	k2eAgInSc2d1	inverzní
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
opět	opět	k6eAd1	opět
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skládat	skládat	k5eAaImF	skládat
jenom	jenom	k9	jenom
takové	takový	k3xDgInPc4	takový
morfismy	morfismus	k1gInPc4	morfismus
<g/>
,	,	kIx,	,
že	že	k8xS	že
složení	složení	k1gNnSc1	složení
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Libovolný	libovolný	k2eAgInSc1d1	libovolný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
konceptů	koncept	k1gInPc2	koncept
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dále	daleko	k6eAd2	daleko
zobecňovat	zobecňovat	k5eAaImF	zobecňovat
na	na	k7c4	na
obecnou	obecný	k2eAgFnSc4d1	obecná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
-ární	-ární	k2eAgFnSc4d1	-ární
operaci	operace	k1gFnSc4	operace
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
vstup	vstup	k1gInSc1	vstup
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgNnSc7d1	vhodné
zobecněním	zobecnění	k1gNnSc7	zobecnění
axiomů	axiom	k1gInPc2	axiom
grupy	grupa	k1gFnSc2	grupa
dostáváme	dostávat	k5eAaImIp1nP	dostávat
tzv.	tzv.	kA	tzv.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
-ární	-ární	k2eAgFnSc4d1	-ární
grupu	grupa	k1gFnSc4	grupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Group	Group	k1gInSc1	Group
(	(	kIx(	(
<g/>
mathematics	mathematics	k1gInSc1	mathematics
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČeskáALEXANDROV	ČeskáALEXANDROV	k?	ČeskáALEXANDROV
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
M.	M.	kA	M.
Volf	Volf	k1gMnSc1	Volf
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
:	:	kIx,	:
Mir	mir	k1gInSc1	mir
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
120	[number]	k4	120
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BOČEK	Boček	k1gMnSc1	Boček
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
<g/>
;	;	kIx,	;
ŠEDIVÝ	Šedivý	k1gMnSc1	Šedivý
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
geometrických	geometrický	k2eAgNnPc2d1	geometrické
zobrazení	zobrazení	k1gNnPc2	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
213	[number]	k4	213
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DRÁPAL	Drápal	k1gMnSc1	Drápal
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
(	(	kIx(	(
<g/>
základní	základní	k2eAgInPc1d1	základní
aspekty	aspekt	k1gInPc1	aspekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
207	[number]	k4	207
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
162	[number]	k4	162
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LIVIO	LIVIO	kA	LIVIO
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
<g/>
.	.	kIx.	.
</s>
<s>
Neřešitelná	řešitelný	k2eNgFnSc1d1	neřešitelná
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7363	[number]	k4	7363
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LITZMAN	LITZMAN	kA	LITZMAN
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
;	;	kIx,	;
SEKANINA	Sekanina	k1gMnSc1	Sekanina
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
grup	grupa	k1gFnPc2	grupa
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
276	[number]	k4	276
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Algebra	algebra	k1gFnSc1	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
560	[number]	k4	560
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
301	[number]	k4	301
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
grup	grupa	k1gFnPc2	grupa
a	a	k8xC	a
grupy	grupa	k1gFnSc2	grupa
krystalografické	krystalografický	k2eAgFnSc2d1	krystalografická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
119	[number]	k4	119
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788024604060	[number]	k4	9788024604060
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RACHŮNEK	RACHŮNEK	kA	RACHŮNEK
<g/>
,,	,,	k?	,,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Grupy	grupa	k1gFnPc1	grupa
a	a	k8xC	a
okruhy	okruh	k1gInPc1	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
106	[number]	k4	106
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788024409986	[number]	k4	9788024409986
<g/>
.	.	kIx.	.
</s>
<s>
AnglickáCORNWELL	AnglickáCORNWELL	k?	AnglickáCORNWELL
<g/>
,	,	kIx,	,
J.	J.	kA	J.
F.	F.	kA	F.
Group	Group	k1gMnSc1	Group
theory	theora	k1gFnSc2	theora
in	in	k?	in
physics	physics	k1gInSc1	physics
<g/>
:	:	kIx,	:
an	an	k?	an
introduction	introduction	k1gInSc1	introduction
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Diego	Diego	k1gMnSc1	Diego
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
349	[number]	k4	349
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780121898007	[number]	k4	9780121898007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LESK	lesk	k1gInSc1	lesk
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
M.	M.	kA	M.
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
symmetry	symmetr	k1gInPc4	symmetr
and	and	k?	and
group	group	k1gInSc4	group
theory	theora	k1gFnSc2	theora
for	forum	k1gNnPc2	forum
chemists	chemists	k6eAd1	chemists
<g/>
.	.	kIx.	.
</s>
<s>
Dordrecht	Dordrecht	k1gInSc1	Dordrecht
;	;	kIx,	;
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Kluwer	Kluwer	k1gInSc1	Kluwer
Academic	Academice	k1gFnPc2	Academice
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781402021503	[number]	k4	9781402021503
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCWEENY	MCWEENY	kA	MCWEENY
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Symmetry	Symmetr	k1gInPc1	Symmetr
<g/>
:	:	kIx,	:
an	an	k?	an
introduction	introduction	k1gInSc1	introduction
to	ten	k3xDgNnSc4	ten
group	group	k1gMnSc1	group
theory	theora	k1gFnSc2	theora
and	and	k?	and
its	its	k?	its
applications	applications	k1gInSc1	applications
<g/>
.	.	kIx.	.
</s>
<s>
Mineola	Mineola	k1gFnSc1	Mineola
<g/>
:	:	kIx,	:
Dover	Dover	k1gInSc1	Dover
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780486421827	[number]	k4	9780486421827
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MILLER	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Willard	Willard	k1gMnSc1	Willard
<g/>
.	.	kIx.	.
</s>
<s>
Symmetry	Symmetr	k1gInPc1	Symmetr
groups	groupsa	k1gFnPc2	groupsa
and	and	k?	and
their	theira	k1gFnPc2	theira
applications	applications	k6eAd1	applications
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780124974609	[number]	k4	9780124974609
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STERNBERG	STERNBERG	kA	STERNBERG
<g/>
,	,	kIx,	,
Shlomo	Shloma	k1gFnSc5	Shloma
<g/>
.	.	kIx.	.
</s>
<s>
Group	Group	k1gMnSc1	Group
theory	theora	k1gFnSc2	theora
and	and	k?	and
physics	physics	k1gInSc1	physics
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
444	[number]	k4	444
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9780521558853	[number]	k4	9780521558853
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
grupa	grupa	k1gFnSc1	grupa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
grupa	grupa	k1gFnSc1	grupa
ve	v	k7c6	v
WikislovníkuČeskéMotl	WikislovníkuČeskéMotl	k1gFnSc6	WikislovníkuČeskéMotl
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Zahradník	Zahradník	k1gMnSc1	Zahradník
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Pěstujeme	pěstovat	k5eAaImIp1nP	pěstovat
lineární	lineární	k2eAgFnSc4d1	lineární
algebru	algebra	k1gFnSc4	algebra
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
Grupa	grupa	k1gFnSc1	grupa
(	(	kIx(	(
<g/>
skripta	skriptum	k1gNnSc2	skriptum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Kuřil	Kuřil	k1gMnSc1	Kuřil
<g/>
,	,	kIx,	,
Základy	základ	k1gInPc1	základ
teorie	teorie	k1gFnSc2	teorie
grup	grupa	k1gFnPc2	grupa
(	(	kIx(	(
<g/>
učební	učební	k2eAgInSc1d1	učební
text	text	k1gInSc1	text
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
Elementární	elementární	k2eAgFnSc1d1	elementární
teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Horálková	Horálková	k1gFnSc1	Horálková
<g/>
,	,	kIx,	,
Grupy	grupa	k1gFnPc4	grupa
symetrií	symetrie	k1gFnPc2	symetrie
<g/>
,	,	kIx,	,
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
"	"	kIx"	"
<g/>
šnEk	šnek	k1gMnSc1	šnek
<g/>
"	"	kIx"	"
Opršal	Opršal	k1gMnSc1	Opršal
<g/>
,	,	kIx,	,
Rubikova	Rubikův	k2eAgFnSc1d1	Rubikova
teorie	teorie	k1gFnSc1	teorie
grup	grupa	k1gFnPc2	grupa
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Krystalografické	krystalografický	k2eAgInPc4d1	krystalografický
společnostiAnglickéKnihy	společnostiAnglickéKnih	k1gInPc4	společnostiAnglickéKnih
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
grup	grupa	k1gFnPc2	grupa
na	na	k7c4	na
e-books	eooks	k1gInSc4	e-books
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
W.	W.	kA	W.
K.	K.	kA	K.
Firk	Firk	k1gMnSc1	Firk
(	(	kIx(	(
<g/>
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
Groups	Groups	k1gInSc1	Groups
<g/>
,	,	kIx,	,
Invariants	Invariants	k1gInSc1	Invariants
&	&	k?	&
Particles	Particles	k1gInSc1	Particles
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
<p>
<s>
The	The	k?	The
Small	Small	k1gInSc1	Small
Groups	Groupsa	k1gFnPc2	Groupsa
library	librara	k1gFnSc2	librara
<g/>
,	,	kIx,	,
popis	popis	k1gInSc4	popis
grup	grupa	k1gFnPc2	grupa
malých	malý	k2eAgInPc2d1	malý
řádů	řád	k1gInPc2	řád
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Group	Group	k1gMnSc1	Group
Tables	Tables	k1gMnSc1	Tables
and	and	k?	and
Subgroup	Subgroup	k1gInSc1	Subgroup
Diagrams	Diagrams	k1gInSc1	Diagrams
</s>
</p>
<p>
<s>
Lie	Lie	k?	Lie
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
na	na	k7c4	na
počty	počet	k1gInPc4	počet
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
reprezentacemi	reprezentace	k1gFnPc7	reprezentace
Lieových	Lieův	k2eAgFnPc2d1	Lieova
grup	grupa	k1gFnPc2	grupa
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
malých	malý	k2eAgFnPc2d1	malá
grup	grupa	k1gFnPc2	grupa
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
30	[number]	k4	30
</s>
</p>
<p>
<s>
Grupa	grupa	k1gFnSc1	grupa
na	na	k7c4	na
MathWorld	MathWorld	k1gInSc4	MathWorld
</s>
</p>
