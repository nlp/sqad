<s>
Grupa	grupa	k1gFnSc1	grupa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
algebraická	algebraický	k2eAgFnSc1d1	algebraická
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
a	a	k8xC	a
formalizuje	formalizovat	k5eAaBmIp3nS	formalizovat
koncept	koncept	k1gInSc4	koncept
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
