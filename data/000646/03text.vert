<s>
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1847	[number]	k4	1847
Vrbice	vrbice	k1gFnSc1	vrbice
u	u	k7c2	u
Leštiny	Leština	k1gFnSc2	Leština
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1902	[number]	k4	1902
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
menšinový	menšinový	k2eAgMnSc1d1	menšinový
aktivista	aktivista	k1gMnSc1	aktivista
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
ve	v	k7c6	v
Vrbici	vrbice	k1gFnSc6	vrbice
u	u	k7c2	u
Leštiny	Leština	k1gFnSc2	Leština
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Jana	Jan	k1gMnSc2	Jan
Kurze	kurz	k1gInSc6	kurz
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1799	[number]	k4	1799
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c6	na
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1	Karlo-Ferdinandova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k9	jako
mladík	mladík	k1gMnSc1	mladík
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
suploval	suplovat	k5eAaImAgInS	suplovat
na	na	k7c6	na
akademickém	akademický	k2eAgNnSc6d1	akademické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1872	[number]	k4	1872
narodil	narodit	k5eAaPmAgMnS	narodit
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
známý	známý	k2eAgMnSc1d1	známý
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
syny	syn	k1gMnPc4	syn
Viléma	Vilém	k1gMnSc4	Vilém
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc4	Jan
<g/>
,	,	kIx,	,
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
<g/>
,	,	kIx,	,
Artura	Artur	k1gMnSc4	Artur
a	a	k8xC	a
Otakara	Otakar	k1gMnSc4	Otakar
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Irenu	Irena	k1gFnSc4	Irena
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Eleonorou	Eleonora	k1gFnSc7	Eleonora
Tomíčkovou	Tomíčkův	k2eAgFnSc7d1	Tomíčkova
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Ignáce	Ignác	k1gMnSc2	Ignác
Tomíčka	Tomíček	k1gMnSc2	Tomíček
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc2	soudce
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Strašecí	Strašecí	k1gNnSc6	Strašecí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
za	za	k7c2	za
ředitele	ředitel	k1gMnSc2	ředitel
Lindnera	Lindner	k1gMnSc2	Lindner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
studijním	studijní	k2eAgInSc7d1	studijní
účelem	účel	k1gInSc7	účel
strávil	strávit	k5eAaPmAgMnS	strávit
léto	léto	k1gNnSc4	léto
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
na	na	k7c6	na
zoologické	zoologický	k2eAgFnSc6d1	zoologická
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Terstu	Terst	k1gInSc6	Terst
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
reálku	reálka	k1gFnSc4	reálka
v	v	k7c6	v
Ječné	ječný	k2eAgFnSc6d1	Ječná
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
spolcích	spolek	k1gInPc6	spolek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Sokola	Sokol	k1gMnSc2	Sokol
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyrš	Tyrš	k1gMnSc1	Tyrš
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
byl	být	k5eAaImAgInS	být
za	za	k7c7	za
mladočechy	mladočech	k1gMnPc7	mladočech
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
parlamentu	parlament	k1gInSc2	parlament
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
poslanec	poslanec	k1gMnSc1	poslanec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
městskou	městský	k2eAgFnSc4d1	městská
kurii	kurie	k1gFnSc4	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Domažlice	Domažlice	k1gFnPc1	Domažlice
atd.	atd.	kA	atd.
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
kurie	kurie	k1gFnSc1	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
,	,	kIx,	,
Klatovy	Klatovy	k1gInPc1	Klatovy
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ho	on	k3xPp3gNnSc4	on
na	na	k7c6	na
poslaneckém	poslanecký	k2eAgNnSc6d1	poslanecké
křesle	křeslo	k1gNnSc6	křeslo
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgInS	být
volen	volit	k5eAaImNgInS	volit
za	za	k7c4	za
oblasti	oblast	k1gFnPc4	oblast
Pošumaví	Pošumaví	k1gNnSc2	Pošumaví
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
působil	působit	k5eAaImAgMnS	působit
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
jako	jako	k8xC	jako
menšinový	menšinový	k2eAgMnSc1d1	menšinový
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
bojující	bojující	k2eAgMnSc1d1	bojující
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
českojazyčného	českojazyčný	k2eAgNnSc2d1	českojazyčné
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
posilování	posilování	k1gNnSc2	posilování
českých	český	k2eAgFnPc2d1	Česká
etnických	etnický	k2eAgFnPc2d1	etnická
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
měl	mít	k5eAaImAgMnS	mít
v	v	k7c4	v
České	český	k2eAgMnPc4d1	český
Kubici	Kubica	k1gMnPc4	Kubica
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
českou	český	k2eAgFnSc7d1	Česká
výspou	výspa	k1gFnSc7	výspa
na	na	k7c6	na
Domažlicku	Domažlicko	k1gNnSc6	Domažlicko
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
aktivizovat	aktivizovat	k5eAaImF	aktivizovat
místní	místní	k2eAgInSc4d1	místní
český	český	k2eAgInSc4d1	český
spolkový	spolkový	k2eAgInSc4d1	spolkový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
práci	práce	k1gFnSc4	práce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
i	i	k9	i
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Volšovy	Volšův	k2eAgInPc1d1	Volšův
poblíž	poblíž	k6eAd1	poblíž
Sušice	Sušice	k1gFnPc4	Sušice
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
soustřeďoval	soustřeďovat	k5eAaImAgMnS	soustřeďovat
i	i	k9	i
na	na	k7c4	na
obhajobu	obhajoba	k1gFnSc4	obhajoba
zájmů	zájem	k1gInPc2	zájem
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
menšinová	menšinový	k2eAgNnPc4d1	menšinové
<g/>
,	,	kIx,	,
jazyková	jazykový	k2eAgNnPc4d1	jazykové
a	a	k8xC	a
školská	školský	k2eAgNnPc4d1	školské
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
opavské	opavský	k2eAgNnSc1d1	Opavské
českojazyčné	českojazyčný	k2eAgNnSc1d1	českojazyčné
gymnázium	gymnázium	k1gNnSc1	gymnázium
nebo	nebo	k8xC	nebo
slovinské	slovinský	k2eAgNnSc1d1	slovinské
školství	školství	k1gNnSc1	školství
v	v	k7c4	v
Celje	Celj	k1gMnPc4	Celj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k6eAd1	rovněž
zvolen	zvolit	k5eAaPmNgInS	zvolit
na	na	k7c4	na
Český	český	k2eAgInSc4d1	český
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
za	za	k7c4	za
kurii	kurie	k1gFnSc4	kurie
městskou	městský	k2eAgFnSc4d1	městská
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc4	obvod
Strakonice	Strakonice	k1gFnPc4	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obvodu	obvod	k1gInSc6	obvod
po	po	k7c6	po
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Karel	Karel	k1gMnSc1	Karel
Viškovský	Viškovský	k2eAgMnSc1d1	Viškovský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
činorodým	činorodý	k2eAgMnSc7d1	činorodý
publicistou	publicista	k1gMnSc7	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
článků	článek	k1gInPc2	článek
v	v	k7c6	v
denním	denní	k2eAgInSc6d1	denní
tisku	tisk	k1gInSc6	tisk
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
učebních	učební	k2eAgFnPc2d1	učební
tiskovin	tiskovina	k1gFnPc2	tiskovina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
vedl	vést	k5eAaImAgInS	vést
časopis	časopis	k1gInSc1	časopis
Z	z	k7c2	z
říše	říš	k1gFnSc2	říš
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
ustavující	ustavující	k2eAgFnSc7d1	ustavující
valnou	valný	k2eAgFnSc7d1	valná
hromadou	hromada	k1gFnSc7	hromada
do	do	k7c2	do
výboru	výbor	k1gInSc2	výbor
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
redigoval	redigovat	k5eAaImAgInS	redigovat
po	po	k7c4	po
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
Časopis	časopis	k1gInSc1	časopis
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
přičiňoval	přičiňovat	k5eAaImAgMnS	přičiňovat
se	se	k3xPyFc4	se
o	o	k7c4	o
feriální	feriální	k2eAgNnSc4d1	feriální
cestování	cestování	k1gNnSc4	cestování
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
.	.	kIx.	.
</s>
<s>
Organizování	organizování	k1gNnSc1	organizování
studentských	studentský	k2eAgFnPc2d1	studentská
nocleháren	noclehárna	k1gFnPc2	noclehárna
a	a	k8xC	a
letních	letní	k2eAgInPc2d1	letní
bytů	byt	k1gInPc2	byt
po	po	k7c6	po
českém	český	k2eAgInSc6d1	český
venkově	venkov	k1gInSc6	venkov
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc4	jeho
prací	prací	k2eAgNnSc1d1	prací
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Pasovským	Pasovský	k1gMnSc7	Pasovský
byl	být	k5eAaImAgMnS	být
iniciátorem	iniciátor	k1gMnSc7	iniciátor
stavby	stavba	k1gFnSc2	stavba
Petřínské	petřínský	k2eAgFnPc1d1	Petřínská
rozhledny	rozhledna	k1gFnPc1	rozhledna
a	a	k8xC	a
lanové	lanový	k2eAgFnPc1d1	lanová
dráhy	dráha	k1gFnPc1	dráha
na	na	k7c4	na
Petřín	Petřín	k1gInSc4	Petřín
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kurz	Kurz	k1gMnSc1	Kurz
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
této	tento	k3xDgFnSc2	tento
rozhledny	rozhledna	k1gFnSc2	rozhledna
ve	v	k7c6	v
fejetonu	fejeton	k1gInSc6	fejeton
"	"	kIx"	"
<g/>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
obrázek	obrázek	k1gInSc1	obrázek
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
hotové	hotový	k2eAgFnPc4d1	hotová
stavby	stavba	k1gFnPc4	stavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
a	a	k8xC	a
lanovky	lanovka	k1gFnSc2	lanovka
na	na	k7c4	na
srpen	srpen	k1gInSc4	srpen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
připadala	připadat	k5eAaPmAgFnS	připadat
mnohým	mnohý	k2eAgMnPc3d1	mnohý
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
jako	jako	k8xS	jako
nemístná	místný	k2eNgFnSc1d1	nemístná
fantazie	fantazie	k1gFnSc1	fantazie
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
když	když	k8xS	když
rozpočet	rozpočet	k1gInSc1	rozpočet
činil	činit	k5eAaImAgInS	činit
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vilému	Vilém	k1gMnSc6	Vilém
Kurzovi	Kurz	k1gMnSc6	Kurz
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jiná	jiný	k2eAgFnSc1d1	jiná
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
stavbu	stavba	k1gFnSc4	stavba
rovněž	rovněž	k9	rovněž
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Čerchov	Čerchov	k1gInSc4	Čerchov
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
ho	on	k3xPp3gNnSc4	on
postihla	postihnout	k5eAaPmAgFnS	postihnout
rodinná	rodinný	k2eAgFnSc1d1	rodinná
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
utopila	utopit	k5eAaPmAgFnS	utopit
v	v	k7c6	v
Berounce	Berounka	k1gFnSc6	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Kurz	Kurz	k1gMnSc1	Kurz
měl	mít	k5eAaImAgMnS	mít
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
diagnostikovánu	diagnostikován	k2eAgFnSc4d1	diagnostikována
srdeční	srdeční	k2eAgFnSc4d1	srdeční
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
veřejného	veřejný	k2eAgInSc2d1	veřejný
i	i	k8xC	i
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
byl	být	k5eAaImAgInS	být
upoután	upoutat	k5eAaPmNgInS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
několikrát	několikrát	k6eAd1	několikrát
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
posledním	poslední	k2eAgInSc6d1	poslední
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
ulehl	ulehnout	k5eAaPmAgMnS	ulehnout
a	a	k8xC	a
již	již	k6eAd1	již
lůžko	lůžko	k1gNnSc4	lůžko
neopustil	opustit	k5eNaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hrobu	hrob	k1gInSc2	hrob
Miroslava	Miroslav	k1gMnSc4	Miroslav
Tyrše	Tyrš	k1gMnSc4	Tyrš
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
byl	být	k5eAaImAgMnS	být
pradědem	praděd	k1gMnSc7	praděd
známého	známý	k1gMnSc2	známý
klavíristy	klavírista	k1gMnSc2	klavírista
Pavla	Pavel	k1gMnSc2	Pavel
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
let	léto	k1gNnPc2	léto
studijních	studijní	k2eAgMnPc2d1	studijní
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
se	s	k7c7	s
zoologickými	zoologický	k2eAgFnPc7d1	zoologická
studiemi	studie	k1gFnPc7	studie
o	o	k7c6	o
korýších	korýš	k1gMnPc6	korýš
(	(	kIx(	(
<g/>
perloočkách	perloočka	k1gFnPc6	perloočka
a	a	k8xC	a
příživných	příživný	k2eAgFnPc6d1	příživná
buchankách	buchanka	k1gFnPc6	buchanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
Zprávách	zpráva	k1gFnPc6	zpráva
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Zeitschrift	Zeitschrifta	k1gFnPc2	Zeitschrifta
fůr	fůra	k1gFnPc2	fůra
wissenschaftliche	wissenschaftlich	k1gFnSc2	wissenschaftlich
Zoologie	zoologie	k1gFnSc2	zoologie
(	(	kIx(	(
<g/>
Androgyne-Missbildung	Androgyne-Missbildung	k1gInSc1	Androgyne-Missbildung
bei	bei	k?	bei
Cladoceren	Cladocerna	k1gFnPc2	Cladocerna
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
;	;	kIx,	;
Dodekas	Dodekas	k1gInSc1	Dodekas
neuer	neura	k1gFnPc2	neura
Cladoceren	Cladocerna	k1gFnPc2	Cladocerna
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
;	;	kIx,	;
Studien	Studien	k2eAgInSc1d1	Studien
über	über	k1gInSc1	über
die	die	k?	die
Familie	Familie	k1gFnSc2	Familie
der	drát	k5eAaImRp2nS	drát
Lernäopoden	Lernäopodna	k1gFnPc2	Lernäopodna
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
;	;	kIx,	;
Eunicicola	Eunicicola	k1gFnSc1	Eunicicola
Clausii	Clausie	k1gFnSc4	Clausie
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
;	;	kIx,	;
Über	Über	k1gInSc1	Über
limicole	limicole	k1gFnSc2	limicole
Cladoceren	Cladocerna	k1gFnPc2	Cladocerna
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
popularizování	popularizování	k1gNnSc3	popularizování
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
článků	článek	k1gInPc2	článek
do	do	k7c2	do
Osvěty	osvěta	k1gFnSc2	osvěta
<g/>
,	,	kIx,	,
Světozoru	světozor	k1gInSc2	světozor
<g/>
,	,	kIx,	,
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Ruchu	ruch	k1gInSc2	ruch
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
publikoval	publikovat	k5eAaBmAgInS	publikovat
hojně	hojně	k6eAd1	hojně
článků	článek	k1gInPc2	článek
v	v	k7c6	v
pedagogických	pedagogický	k2eAgInPc6d1	pedagogický
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
Škole	škola	k1gFnSc3	škola
a	a	k8xC	a
životě	život	k1gInSc6	život
a	a	k8xC	a
Paedagogii	Paedagogie	k1gFnSc6	Paedagogie
<g/>
.	.	kIx.	.
</s>
<s>
Transparentní	transparentní	k2eAgInPc1d1	transparentní
obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
(	(	kIx(	(
<g/>
5	[number]	k4	5
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Herbář	herbář	k1gInSc1	herbář
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
Botanika	botanik	k1gMnSc2	botanik
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
třídy	třída	k1gFnPc4	třída
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
Geologický	geologický	k2eAgInSc4d1	geologický
nástin	nástin	k1gInSc4	nástin
okolí	okolí	k1gNnSc2	okolí
Kutnohorského	kutnohorský	k2eAgNnSc2d1	kutnohorské
Základy	základ	k1gInPc1	základ
tělocviku	tělocvik	k1gInSc2	tělocvik
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
sešit	sešit	k1gInSc1	sešit
-	-	kIx~	-
dokončil	dokončit	k5eAaPmAgInS	dokončit
dílo	dílo	k1gNnSc4	dílo
Miroslava	Miroslav	k1gMnSc2	Miroslav
Tyrše	Tyrš	k1gMnSc2	Tyrš
<g/>
)	)	kIx)	)
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
tělocviku	tělocvik	k1gInSc2	tělocvik
školského	školský	k2eAgInSc2d1	školský
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
Učebná	učebné	k1gNnPc4	učebné
kniha	kniha	k1gFnSc1	kniha
tělocviku	tělocvik	k1gInSc2	tělocvik
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Sportová	Sportový	k2eAgFnSc1d1	Sportový
knížka	knížka	k1gFnSc1	knížka
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
365	[number]	k4	365
odpoledních	odpolední	k2eAgInPc2d1	odpolední
výletů	výlet	k1gInPc2	výlet
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Výlety	výlet	k1gInPc7	výlet
v	v	k7c6	v
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
Českém	český	k2eAgInSc6d1	český
Lese	les	k1gInSc6	les
ze	z	k7c2	z
Špičáku	špičák	k1gInSc2	špičák
Vzhůru	vzhůru	k6eAd1	vzhůru
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
!	!	kIx.	!
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Učebná	učebné	k1gNnPc1	učebné
kniha	kniha	k1gFnSc1	kniha
tělocviku	tělocvik	k1gInSc6	tělocvik
pro	pro	k7c4	pro
ústavy	ústav	k1gInPc4	ústav
učitelské	učitelský	k2eAgFnSc2d1	učitelská
a	a	k8xC	a
rukověť	rukověť	k1gFnSc1	rukověť
pro	pro	k7c4	pro
učitele	učitel	k1gMnSc4	učitel
tělocviku	tělocvik	k1gInSc2	tělocvik
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
obecných	obecný	k2eAgFnPc6d1	obecná
a	a	k8xC	a
měšťanských	měšťanský	k2eAgFnPc6d1	měšťanská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
:	:	kIx,	:
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
za	za	k7c4	za
příležitosti	příležitost	k1gFnPc4	příležitost
jich	on	k3xPp3gFnPc2	on
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1891	[number]	k4	1891
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Sportová	Sportový	k2eAgFnSc1d1	Sportový
knížka	knížka	k1gFnSc1	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Budiž	budiž	k9	budiž
světlo	světlo	k1gNnSc1	světlo
:	:	kIx,	:
pokroky	pokrok	k1gInPc4	pokrok
a	a	k8xC	a
vynálezy	vynález	k1gInPc4	vynález
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
osvětlovacím	osvětlovací	k2eAgInSc6d1	osvětlovací
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgNnSc1d1	domácí
umění	umění	k1gNnSc1	umění
:	:	kIx,	:
rady	rada	k1gFnPc1	rada
a	a	k8xC	a
pokyny	pokyn	k1gInPc1	pokyn
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KURZ	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
365	[number]	k4	365
odpoledních	odpolední	k2eAgInPc2d1	odpolední
výletů	výlet	k1gInPc2	výlet
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1908	[number]	k4	1908
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
