<p>
<s>
Liptovské	liptovský	k2eAgFnPc1d1	Liptovská
Tatry	Tatra	k1gFnPc1	Tatra
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
okrsků	okrsek	k1gInPc2	okrsek
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
hřebeni	hřeben	k1gInSc6	hřeben
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nP	procházet
jimi	on	k3xPp3gNnPc7	on
slovensko-polská	slovenskoolský	k2eAgFnSc1d1	slovensko-polská
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
údolími	údolí	k1gNnPc7	údolí
<g/>
:	:	kIx,	:
Račkova	Račkův	k2eAgFnSc1d1	Račkova
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Bystrá	bystrý	k2eAgFnSc1d1	bystrá
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Kamenistá	Kamenistý	k2eAgFnSc1d1	Kamenistá
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Tichá	Tichá	k1gFnSc1	Tichá
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Dolina	dolina	k1gFnSc1	dolina
Tomanowa	Tomanowa	k1gFnSc1	Tomanowa
(	(	kIx(	(
<g/>
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomanovská	Tomanovský	k2eAgFnSc1d1	Tomanovská
dolina	dolina	k1gFnSc1	dolina
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolina	dolina	k1gFnSc1	dolina
Pyszniańska	Pyszniańska	k1gFnSc1	Pyszniańska
<g/>
,	,	kIx,	,
Dolina	dolina	k1gFnSc1	dolina
Starorobociańska	Starorobociańska	k1gFnSc1	Starorobociańska
<g/>
,	,	kIx,	,
Dolina	dolina	k1gFnSc1	dolina
Chochołowska	Chochołowsk	k1gInSc2	Chochołowsk
Wyżnia	Wyżnium	k1gNnSc2	Wyżnium
a	a	k8xC	a
Doliny	dolina	k1gFnSc2	dolina
Chochołowskiej	Chochołowskiej	k1gInSc1	Chochołowskiej
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
Bystrá	bystrý	k2eAgFnSc1d1	bystrá
,	,	kIx,	,
2248	[number]	k4	2248
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
celých	celý	k2eAgFnPc2d1	celá
Západních	západní	k2eAgFnPc2d1	západní
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
jsou	být	k5eAaImIp3nP	být
Jamnickým	Jamnický	k2eAgNnSc7d1	Jamnický
sedlem	sedlo	k1gNnSc7	sedlo
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
m	m	kA	m
<g/>
)	)	kIx)	)
odděleny	oddělit	k5eAaPmNgInP	oddělit
od	od	k7c2	od
Roháčů	roháč	k1gInPc2	roháč
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
Tomanovské	Tomanovský	k2eAgNnSc1d1	Tomanovské
sedlo	sedlo	k1gNnSc1	sedlo
(	(	kIx(	(
<g/>
1686	[number]	k4	1686
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
masivu	masiv	k1gInSc2	masiv
Červených	Červených	k2eAgInPc2d1	Červených
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Volovce	Volovec	k1gInSc2	Volovec
navazuje	navazovat	k5eAaImIp3nS	navazovat
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
okrsek	okrsek	k1gInSc1	okrsek
Lúčna	Lúčno	k1gNnSc2	Lúčno
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Grześ	Grześ	k1gMnSc2	Grześ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1653	[number]	k4	1653
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
Siwa	Siwa	k1gMnSc1	Siwa
Przełęcz	Przełęcz	k1gMnSc1	Przełęcz
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
navazuje	navazovat	k5eAaImIp3nS	navazovat
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
okrsek	okrsek	k1gInSc4	okrsek
a	a	k8xC	a
hřeben	hřeben	k1gInSc4	hřeben
Ornak	Ornak	k1gInSc1	Ornak
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
<g/>
:	:	kIx,	:
Zadni	Zadn	k1gMnPc1	Zadn
Ornak	Ornak	k1gMnSc1	Ornak
<g/>
,	,	kIx,	,
1867	[number]	k4	1867
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Liptovských	liptovský	k2eAgFnPc2d1	Liptovská
Tater	Tatra	k1gFnPc2	Tatra
leží	ležet	k5eAaImIp3nS	ležet
řada	řada	k1gFnSc1	řada
kopců	kopec	k1gInPc2	kopec
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
nad	nad	k7c4	nad
2000	[number]	k4	2000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
Zachodnie	Zachodnie	k1gFnSc2	Zachodnie
słowackie	słowackie	k1gFnSc2	słowackie
i	i	k8xC	i
polskie	polskie	k1gFnSc2	polskie
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
turystyczna	turystyczna	k1gFnSc1	turystyczna
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
:	:	kIx,	:
Wyd	Wyd	k1gMnSc1	Wyd
<g/>
.	.	kIx.	.
</s>
<s>
Kartograficzne	Kartograficznout	k5eAaPmIp3nS	Kartograficznout
Polkart	Polkart	k1gInSc4	Polkart
Anna	Anna	k1gFnSc1	Anna
Siwicka	Siwicka	k1gFnSc1	Siwicka
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
87873	[number]	k4	87873
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
