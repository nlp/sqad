<p>
<s>
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
skupina	skupina	k1gFnSc1	skupina
[	[	kIx(	[
<g/>
višegrádská	višegrádský	k2eAgFnSc1d1	višegrádský
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
čtyřka	čtyřka	k1gFnSc1	čtyřka
nebo	nebo	k8xC	nebo
V	v	k7c6	v
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aliance	aliance	k1gFnSc1	aliance
čtyř	čtyři	k4xCgInPc2	čtyři
států	stát	k1gInPc2	stát
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Aliance	aliance	k1gFnSc1	aliance
státu	stát	k1gInSc2	stát
V4	V4	k1gFnSc1	V4
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
setkáním	setkání	k1gNnSc7	setkání
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
v	v	k7c6	v
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
městě	město	k1gNnSc6	město
Visegrád	Visegrád	k1gInSc1	Visegrád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
těsné	těsný	k2eAgFnSc6d1	těsná
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
či	či	k8xC	či
obchodních	obchodní	k2eAgFnPc6d1	obchodní
otázkách	otázka	k1gFnPc6	otázka
a	a	k8xC	a
na	na	k7c6	na
věčném	věčné	k1gNnSc6	věčné
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
o	o	k7c6	o
656	[number]	k4	656
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
další	další	k2eAgFnSc2d1	další
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
trojka	trojka	k1gFnSc1	trojka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
deset	deset	k4xCc1	deset
dní	den	k1gInPc2	den
před	před	k7c7	před
zánikem	zánik	k1gInSc7	zánik
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
maďarského	maďarský	k2eAgMnSc2d1	maďarský
premiéra	premiér	k1gMnSc2	premiér
Józsefa	József	k1gMnSc2	József
Antalla	Antall	k1gMnSc2	Antall
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
ČSFR	ČSFR	kA	ČSFR
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
a	a	k8xC	a
polského	polský	k2eAgMnSc2d1	polský
prezidenta	prezident	k1gMnSc2	prezident
Lecha	Lech	k1gMnSc2	Lech
Wałęsy	Wałęsa	k1gFnSc2	Wałęsa
ve	v	k7c6	v
Visegrádu	Visegrád	k1gInSc2	Visegrád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
setkání	setkání	k1gNnSc6	setkání
politikové	politik	k1gMnPc1	politik
podepsali	podepsat	k5eAaPmAgMnP	podepsat
deklaraci	deklarace	k1gFnSc4	deklarace
blízké	blízký	k2eAgFnSc2d1	blízká
spolupráce	spolupráce	k1gFnSc2	spolupráce
tří	tři	k4xCgFnPc2	tři
středoevropských	středoevropský	k2eAgFnPc2d1	středoevropská
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
evropské	evropský	k2eAgFnSc3d1	Evropská
integraci	integrace	k1gFnSc3	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
kooperace	kooperace	k1gFnSc1	kooperace
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
totalitárního	totalitární	k2eAgInSc2d1	totalitární
systému	systém	k1gInSc2	systém
ke	k	k7c3	k
svobodné	svobodný	k2eAgFnSc3d1	svobodná
<g/>
,	,	kIx,	,
pluralitní	pluralitní	k2eAgFnSc3d1	pluralitní
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc3d1	demokratická
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
země	zem	k1gFnPc1	zem
Visegrádu	Visegrád	k1gInSc2	Visegrád
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c4	o
zánik	zánik	k1gInSc4	zánik
RVHP	RVHP	kA	RVHP
a	a	k8xC	a
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
ČSFR	ČSFR	kA	ČSFR
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
aliance	aliance	k1gFnSc2	aliance
změnilo	změnit	k5eAaPmAgNnS	změnit
ve	v	k7c4	v
Visegrádskou	visegrádský	k2eAgFnSc4d1	Visegrádská
čtyřku	čtyřka	k1gFnSc4	čtyřka
(	(	kIx(	(
<g/>
V	v	k7c6	v
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
členství	členství	k1gNnSc1	členství
se	se	k3xPyFc4	se
převedlo	převést	k5eAaPmAgNnS	převést
na	na	k7c4	na
oba	dva	k4xCgInPc4	dva
nástupnické	nástupnický	k2eAgInPc4d1	nástupnický
státy	stát	k1gInPc4	stát
Česko	Česko	k1gNnSc1	Česko
i	i	k8xC	i
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
spolupráce	spolupráce	k1gFnSc1	spolupráce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
ustala	ustat	k5eAaPmAgFnS	ustat
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
zahraničně-politické	zahraničněolitický	k2eAgFnPc1d1	zahraničně-politická
aktivity	aktivita	k1gFnPc1	aktivita
tohoto	tento	k3xDgInSc2	tento
spolku	spolek	k1gInSc2	spolek
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c6	na
prosazování	prosazování	k1gNnSc6	prosazování
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
regionu	region	k1gInSc6	region
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Regionálního	regionální	k2eAgNnSc2d1	regionální
partnerství	partnerství	k1gNnSc2	partnerství
<g/>
,	,	kIx,	,
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
skupina	skupina	k1gFnSc1	skupina
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
programu	program	k1gInSc2	program
V	v	k7c6	v
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
visegrádský	visegrádský	k2eAgInSc1d1	visegrádský
fond	fond	k1gInSc1	fond
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
visegrádský	visegrádský	k2eAgInSc1d1	visegrádský
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gFnSc1	International
Visegrád	Visegrád	k1gInSc4	Visegrád
Fund	fund	k1gInSc1	fund
(	(	kIx(	(
<g/>
IVF	IVF	kA	IVF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Štiřín	Štiřín	k1gInSc1	Štiřín
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
rozvoje	rozvoj	k1gInSc2	rozvoj
užší	úzký	k2eAgFnSc2d2	užší
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
posilování	posilování	k1gNnSc2	posilování
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
V	v	k7c4	v
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
finančně	finančně	k6eAd1	finančně
podporuje	podporovat	k5eAaImIp3nS	podporovat
projekty	projekt	k1gInPc4	projekt
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
oblastech	oblast	k1gFnPc6	oblast
-	-	kIx~	-
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
výměny	výměna	k1gFnSc2	výměna
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
přeshraniční	přeshraniční	k2eAgFnSc2d1	přeshraniční
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
stejně	stejně	k6eAd1	stejně
vysokými	vysoký	k2eAgInPc7d1	vysoký
příspěvky	příspěvek	k1gInPc7	příspěvek
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
přebytek	přebytek	k1gInSc1	přebytek
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
přispěly	přispět	k5eAaPmAgInP	přispět
dohromady	dohromady	k6eAd1	dohromady
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
800	[number]	k4	800
tisíc	tisíc	k4xCgInSc4	tisíc
eur	euro	k1gNnPc2	euro
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
5	[number]	k4	5
miliony	milion	k4xCgInPc7	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
1,25	[number]	k4	1,25
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
1,75	[number]	k4	1,75
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hospodaření	hospodaření	k1gNnSc1	hospodaření
fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přehledných	přehledný	k2eAgFnPc6d1	přehledná
tabulkách	tabulka	k1gFnPc6	tabulka
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
bojová	bojový	k2eAgFnSc1d1	bojová
skupina	skupina	k1gFnSc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
sestavení	sestavení	k1gNnSc6	sestavení
společného	společný	k2eAgNnSc2d1	společné
bojového	bojový	k2eAgNnSc2d1	bojové
uskupení	uskupení	k1gNnSc2	uskupení
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Visegrádskou	visegrádský	k2eAgFnSc4d1	Visegrádská
bojovou	bojový	k2eAgFnSc4d1	bojová
skupinu	skupina	k1gFnSc4	skupina
vede	vést	k5eAaImIp3nS	vést
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
překročil	překročit	k5eAaPmAgInS	překročit
3700	[number]	k4	3700
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
bojové	bojový	k2eAgFnSc6d1	bojová
pohotovosti	pohotovost	k1gFnSc6	pohotovost
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
spojeném	spojený	k2eAgNnSc6d1	spojené
vojenském	vojenský	k2eAgNnSc6d1	vojenské
uskupení	uskupení	k1gNnSc6	uskupení
uvnitř	uvnitř	k7c2	uvnitř
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
společná	společný	k2eAgNnPc4d1	společné
vojenská	vojenský	k2eAgNnPc4d1	vojenské
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
,	,	kIx,	,
koordinované	koordinovaný	k2eAgNnSc4d1	koordinované
zajišťování	zajišťování	k1gNnSc4	zajišťování
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
společný	společný	k2eAgInSc4d1	společný
vývoj	vývoj	k1gInSc4	vývoj
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
čtyřech	čtyři	k4xCgInPc6	čtyři
evropských	evropský	k2eAgInPc6d1	evropský
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předsednictví	předsednictví	k1gNnSc2	předsednictví
==	==	k?	==
</s>
</p>
<p>
<s>
Předsednictví	předsednictví	k1gNnSc1	předsednictví
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
1	[number]	k4	1
rok	rok	k1gInSc4	rok
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
se	se	k3xPyFc4	se
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
–	–	k?	–
československé	československý	k2eAgNnSc4d1	Československé
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Pavlak	Pavlak	k1gMnSc1	Pavlak
/	/	kIx~	/
Hanna	Hanen	k2eAgFnSc1d1	Hanna
Suchocka	Suchocka	k1gFnSc1	Suchocka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
József	József	k1gMnSc1	József
Antall	Antall	k1gMnSc1	Antall
/	/	kIx~	/
Péter	Péter	k1gInSc1	Péter
Boross	Boross	k1gInSc1	Boross
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Jozef	Jozef	k1gMnSc1	Jozef
Moravčík	Moravčík	k1gMnSc1	Moravčík
/	/	kIx~	/
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Włodzimierz	Włodzimierz	k1gMnSc1	Włodzimierz
Cimoszewicz	Cimoszewicz	k1gMnSc1	Cimoszewicz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Gyula	Gyula	k1gMnSc1	Gyula
Horn	Horn	k1gMnSc1	Horn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
/	/	kIx~	/
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Jerzy	Jerz	k1gInPc4	Jerz
Buzek	Buzek	k1gInSc1	Buzek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
/	/	kIx~	/
Péter	Péter	k1gMnSc1	Péter
Medgyessy	Medgyessa	k1gFnSc2	Medgyessa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Belka	Belka	k1gMnSc1	Belka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Gyurcsány	Gyurcsán	k2eAgFnPc4d1	Gyurcsán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Gordon	Gordon	k1gInSc1	Gordon
Bajnai	Bajna	k1gFnSc2	Bajna
/	/	kIx~	/
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
–	–	k?	–
slovenské	slovenský	k2eAgNnSc1d1	slovenské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Iveta	Iveta	k1gFnSc1	Iveta
Radičová	Radičová	k1gFnSc1	Radičová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Beata	Beata	k1gFnSc1	Beata
Szydłová	Szydłová	k1gFnSc1	Szydłová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
–	–	k?	–
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
–	–	k?	–
slovenské	slovenský	k2eAgNnSc4d1	slovenské
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Peter	Peter	k1gMnSc1	Peter
Pellegrini	Pellegrin	k2eAgMnPc1d1	Pellegrin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
–	–	k?	–
české	český	k2eAgNnSc4d1	české
předsednictví	předsednictví	k1gNnSc4	předsednictví
(	(	kIx(	(
<g/>
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2020	[number]	k4	2020
<g/>
/	/	kIx~	/
<g/>
2021	[number]	k4	2021
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
předsednictví	předsednictví	k1gNnSc1	předsednictví
(	(	kIx(	(
<g/>
Mateusz	Mateusz	k1gInSc1	Mateusz
Morawiecki	Morawieck	k1gFnSc2	Morawieck
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
přizvání	přizvání	k1gNnSc4	přizvání
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
do	do	k7c2	do
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
tyto	tento	k3xDgFnPc4	tento
země	zem	k1gFnPc1	zem
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
přizval	přizvat	k5eAaPmAgMnS	přizvat
ke	k	k7c3	k
společným	společný	k2eAgFnPc3d1	společná
schůzkám	schůzka	k1gFnPc3	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
přitom	přitom	k6eAd1	přitom
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Visegrádu	Visegrád	k1gInSc2	Visegrád
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
proti	proti	k7c3	proti
pevnému	pevný	k2eAgNnSc3d1	pevné
rozšíření	rozšíření	k1gNnSc3	rozšíření
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
přišel	přijít	k5eAaPmAgMnS	přijít
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
s	s	k7c7	s
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
Svobodnými	svobodný	k2eAgFnPc7d1	svobodná
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
rozšíření	rozšíření	k1gNnSc4	rozšíření
skupiny	skupina	k1gFnSc2	skupina
o	o	k7c4	o
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
viděli	vidět	k5eAaImAgMnP	vidět
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Unie	unie	k1gFnSc1	unie
protipól	protipól	k1gInSc1	protipól
k	k	k7c3	k
politice	politika	k1gFnSc3	politika
Angely	Angela	k1gFnSc2	Angela
Merkelové	Merkelová	k1gFnSc2	Merkelová
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
pozice	pozice	k1gFnSc2	pozice
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
reformovat	reformovat	k5eAaBmF	reformovat
Unii	unie	k1gFnSc4	unie
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
ale	ale	k9	ale
nápad	nápad	k1gInSc4	nápad
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
společným	společný	k2eAgInSc7d1	společný
zájmem	zájem	k1gInSc7	zájem
skupiny	skupina	k1gFnSc2	skupina
V4	V4	k1gFnSc2	V4
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
jednotná	jednotný	k2eAgFnSc1d1	jednotná
Unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
plán	plán	k1gInSc4	plán
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
rakouští	rakouský	k2eAgMnPc1d1	rakouský
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KONTLER	KONTLER	kA	KONTLER
<g/>
,	,	kIx,	,
László	László	k1gFnSc1	László
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
613	[number]	k4	613
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
616	[number]	k4	616
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
Trojmoří	Trojmoř	k1gFnPc2	Trojmoř
</s>
</p>
<p>
<s>
Středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
obranná	obranný	k2eAgFnSc1d1	obranná
spolupráce	spolupráce	k1gFnSc1	spolupráce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Visegrad	Visegrad	k1gInSc1	Visegrad
Group	Group	k1gInSc1	Group
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Visegrad	Visegrad	k1gInSc1	Visegrad
Internet	Internet	k1gInSc1	Internet
Magazine	Magazin	k1gInSc5	Magazin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
International	International	k1gFnSc1	International
Visegrad	Visegrad	k1gInSc1	Visegrad
Fund	fund	k1gInSc1	fund
(	(	kIx(	(
<g/>
IVF	IVF	kA	IVF
<g/>
)	)	kIx)	)
</s>
</p>
