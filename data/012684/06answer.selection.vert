<s>
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
skupina	skupina	k1gFnSc1	skupina
[	[	kIx(	[
<g/>
višegrádská	višegrádský	k2eAgFnSc1d1	višegrádský
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Visegrádská	visegrádský	k2eAgFnSc1d1	Visegrádská
čtyřka	čtyřka	k1gFnSc1	čtyřka
nebo	nebo	k8xC	nebo
V	v	k7c6	v
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aliance	aliance	k1gFnSc1	aliance
čtyř	čtyři	k4xCgInPc2	čtyři
států	stát	k1gInPc2	stát
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
