<s>
Telnet	Telnet	k1gInSc1	Telnet
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
Telecommunication	Telecommunication	k1gInSc4	Telecommunication
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
protokolu	protokol	k1gInSc2	protokol
používaného	používaný	k2eAgInSc2d1	používaný
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pomocí	pomocí	k7c2	pomocí
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
aplikace	aplikace	k1gFnSc2	aplikace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
vzdálenému	vzdálený	k2eAgInSc3d1	vzdálený
počítači	počítač	k1gInSc3	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
telnet	telneta	k1gFnPc2	telneta
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
aplikační	aplikační	k2eAgFnSc6d1	aplikační
vrstvě	vrstva	k1gFnSc6	vrstva
používaný	používaný	k2eAgInSc4d1	používaný
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
pro	pro	k7c4	pro
realizaci	realizace	k1gFnSc4	realizace
spojení	spojení	k1gNnSc2	spojení
typu	typ	k1gInSc2	typ
klient-server	klienterver	k1gMnSc1	klient-server
protokolem	protokol	k1gInSc7	protokol
TCP	TCP	kA	TCP
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přenáší	přenášet	k5eAaImIp3nS	přenášet
osmibitové	osmibitový	k2eAgInPc4d1	osmibitový
znaky	znak	k1gInPc4	znak
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
(	(	kIx(	(
<g/>
duplexní	duplexní	k2eAgNnPc1d1	duplexní
spojení	spojení	k1gNnPc1	spojení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Serverová	serverový	k2eAgFnSc1d1	serverová
část	část	k1gFnSc1	část
standardně	standardně	k6eAd1	standardně
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
na	na	k7c6	na
portu	port	k1gInSc6	port
číslo	číslo	k1gNnSc1	číslo
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
protokolu	protokol	k1gInSc2	protokol
je	být	k5eAaImIp3nS	být
vyjednání	vyjednání	k1gNnSc4	vyjednání
nastavení	nastavení	k1gNnSc2	nastavení
určitých	určitý	k2eAgFnPc2d1	určitá
voleb	volba	k1gFnPc2	volba
důležitých	důležitý	k2eAgFnPc2d1	důležitá
pro	pro	k7c4	pro
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Telnet	Telnet	k1gInSc1	Telnet
je	být	k5eAaImIp3nS	být
též	též	k9	též
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
realizoval	realizovat	k5eAaBmAgInS	realizovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
počítači	počítač	k1gInPc7	počítač
pomocí	pomocí	k7c2	pomocí
telnet	telneta	k1gFnPc2	telneta
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
součástí	součást	k1gFnSc7	součást
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
unixových	unixový	k2eAgInPc2d1	unixový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
protokolem	protokol	k1gInSc7	protokol
telnet	telnet	k5eAaBmF	telnet
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
vzdálenému	vzdálený	k2eAgInSc3d1	vzdálený
počítači	počítač	k1gInSc3	počítač
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
jako	jako	k8xC	jako
emulace	emulace	k1gFnSc2	emulace
terminálu	terminál	k1gInSc2	terminál
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
možnost	možnost	k1gFnSc4	možnost
práce	práce	k1gFnSc2	práce
uživatele	uživatel	k1gMnSc2	uživatel
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
počítači	počítač	k1gInSc6	počítač
v	v	k7c6	v
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Telnet	Telnet	k1gInSc1	Telnet
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
nástupcem	nástupce	k1gMnSc7	nástupce
terminálů	terminál	k1gInPc2	terminál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
uživatelé	uživatel	k1gMnPc1	uživatel
připojovali	připojovat	k5eAaImAgMnP	připojovat
ke	k	k7c3	k
vzdálenému	vzdálený	k2eAgInSc3d1	vzdálený
počítači	počítač	k1gInSc3	počítač
pomocí	pomocí	k7c2	pomocí
sériové	sériový	k2eAgFnSc2d1	sériová
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
telnetu	telnet	k1gInSc2	telnet
je	být	k5eAaImIp3nS	být
absence	absence	k1gFnSc1	absence
šifrování	šifrování	k1gNnSc2	šifrování
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
dnes	dnes	k6eAd1	dnes
uživatelé	uživatel	k1gMnPc1	uživatel
místo	místo	k7c2	místo
telnetu	telnet	k1gInSc2	telnet
používají	používat	k5eAaImIp3nP	používat
protokol	protokol	k1gInSc4	protokol
SSH	SSH	kA	SSH
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
program	program	k1gInSc1	program
telnet	telneta	k1gFnPc2	telneta
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
ruční	ruční	k2eAgFnSc4d1	ruční
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
počítačovými	počítačový	k2eAgInPc7d1	počítačový
programy	program	k1gInPc7	program
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
simulace	simulace	k1gFnSc1	simulace
připojení	připojení	k1gNnSc2	připojení
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
k	k	k7c3	k
webovému	webový	k2eAgInSc3d1	webový
serveru	server	k1gInSc3	server
<g/>
,	,	kIx,	,
při	při	k7c6	při
simulaci	simulace	k1gFnSc6	simulace
SMTP	SMTP	kA	SMTP
protokolu	protokol	k1gInSc3	protokol
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
RFC	RFC	kA	RFC
854	[number]	k4	854
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
tři	tři	k4xCgFnPc1	tři
základní	základní	k2eAgFnPc1d1	základní
služby	služba	k1gFnPc1	služba
<g/>
:	:	kIx,	:
Síťový	síťový	k2eAgInSc1d1	síťový
virtuální	virtuální	k2eAgInSc1d1	virtuální
terminál	terminál	k1gInSc1	terminál
(	(	kIx(	(
<g/>
NVT	NVT	kA	NVT
-	-	kIx~	-
Network	network	k1gInSc1	network
Virtual	Virtual	k1gMnSc1	Virtual
Terminal	Terminal	k1gMnSc1	Terminal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
standardní	standardní	k2eAgNnSc4d1	standardní
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
klienta	klient	k1gMnSc2	klient
<g/>
/	/	kIx~	/
<g/>
serveru	server	k1gInSc2	server
o	o	k7c6	o
nastavení	nastavení	k1gNnSc6	nastavení
určitých	určitý	k2eAgFnPc2d1	určitá
voleb	volba	k1gFnPc2	volba
Symetrické	symetrický	k2eAgNnSc1d1	symetrické
zobrazení	zobrazení	k1gNnSc1	zobrazení
terminálu	terminál	k1gInSc2	terminál
a	a	k8xC	a
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Síťový	síťový	k2eAgInSc1d1	síťový
virtuální	virtuální	k2eAgInSc1d1	virtuální
terminál	terminál	k1gInSc1	terminál
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
průhlednost	průhlednost	k1gFnSc4	průhlednost
všech	všecek	k3xTgFnPc2	všecek
operací	operace	k1gFnPc2	operace
vůči	vůči	k7c3	vůči
uživateli	uživatel	k1gMnSc3	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
komunikujícími	komunikující	k2eAgNnPc7d1	komunikující
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgInSc1d1	virtuální
terminál	terminál	k1gInSc1	terminál
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obecnou	obecný	k2eAgFnSc4d1	obecná
sadu	sada	k1gFnSc4	sada
příkazů	příkaz	k1gInPc2	příkaz
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
využívá	využívat	k5eAaImIp3nS	využívat
spolehlivé	spolehlivý	k2eAgFnPc4d1	spolehlivá
přenosové	přenosový	k2eAgFnPc4d1	přenosová
služby	služba	k1gFnPc4	služba
TCP	TCP	kA	TCP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
poloduplexním	poloduplexní	k2eAgInSc7d1	poloduplexní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Definovaný	definovaný	k2eAgInSc1d1	definovaný
formát	formát	k1gInSc1	formát
NVT	NVT	kA	NVT
používá	používat	k5eAaImIp3nS	používat
sedmibitový	sedmibitový	k2eAgInSc4d1	sedmibitový
kód	kód	k1gInSc4	kód
ASCII	ascii	kA	ascii
pro	pro	k7c4	pro
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
Telnet	Telnet	k1gInSc1	Telnet
operovat	operovat	k5eAaImF	operovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
NVT	NVT	kA	NVT
generuje	generovat	k5eAaImIp3nS	generovat
všech	všecek	k3xTgInPc2	všecek
128	[number]	k4	128
kódů	kód	k1gInPc2	kód
ASCII	ascii	kA	ascii
pomocí	pomocí	k7c2	pomocí
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
klávesových	klávesový	k2eAgFnPc2d1	klávesová
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
obě	dva	k4xCgFnPc1	dva
komunikující	komunikující	k2eAgFnPc1d1	komunikující
strany	strana	k1gFnPc1	strana
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
protější	protější	k2eAgFnSc1d1	protější
strana	strana	k1gFnSc1	strana
vybavena	vybavit	k5eAaPmNgFnS	vybavit
NVT	NVT	kA	NVT
<g/>
,	,	kIx,	,
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
nejdříve	dříve	k6eAd3	dříve
výměna	výměna	k1gFnSc1	výměna
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
na	na	k7c6	na
určitých	určitý	k2eAgInPc6d1	určitý
parametrech	parametr	k1gInPc6	parametr
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
neumí	umět	k5eNaImIp3nS	umět
použít	použít	k5eAaPmF	použít
danou	daný	k2eAgFnSc4d1	daná
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
požadavek	požadavek	k1gInSc4	požadavek
zamítne	zamítnout	k5eAaPmIp3nS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
však	však	k9	však
musí	muset	k5eAaImIp3nP	muset
dodržet	dodržet	k5eAaPmF	dodržet
minimální	minimální	k2eAgInSc4d1	minimální
standard	standard	k1gInSc4	standard
NVT	NVT	kA	NVT
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
možné	možný	k2eAgInPc1d1	možný
požadavky	požadavek	k1gInPc1	požadavek
<g/>
:	:	kIx,	:
WILL	WILL	kA	WILL
-	-	kIx~	-
odesílatel	odesílatel	k1gMnSc1	odesílatel
chce	chtít	k5eAaImIp3nS	chtít
danou	daný	k2eAgFnSc4d1	daná
volbu	volba	k1gFnSc4	volba
zapnout	zapnout	k5eAaPmF	zapnout
DO	do	k7c2	do
-	-	kIx~	-
odesílatel	odesílatel	k1gMnSc1	odesílatel
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příjemce	příjemce	k1gMnSc1	příjemce
danou	daný	k2eAgFnSc4d1	daná
volbu	volba	k1gFnSc4	volba
zapnul	zapnout	k5eAaPmAgMnS	zapnout
WONT	WONT	kA	WONT
-	-	kIx~	-
odesílatel	odesílatel	k1gMnSc1	odesílatel
chce	chtít	k5eAaImIp3nS	chtít
danou	daný	k2eAgFnSc4d1	daná
volbu	volba	k1gFnSc4	volba
vypnout	vypnout	k5eAaPmF	vypnout
DONT	DONT	kA	DONT
-	-	kIx~	-
odesílatel	odesílatel	k1gMnSc1	odesílatel
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příjemce	příjemce	k1gMnSc1	příjemce
danou	daný	k2eAgFnSc4d1	daná
volbu	volba	k1gFnSc4	volba
vypnul	vypnout	k5eAaPmAgInS	vypnout
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
občas	občas	k6eAd1	občas
pletou	plést	k5eAaImIp3nP	plést
protokol	protokol	k1gInSc4	protokol
telnet	telnet	k5eAaBmF	telnet
s	s	k7c7	s
programem	program	k1gInSc7	program
telnet	telnet	k1gMnSc1	telnet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
klient	klient	k1gMnSc1	klient
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
na	na	k7c4	na
telnetový	telnetový	k2eAgInSc4d1	telnetový
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
server	server	k1gInSc4	server
používající	používající	k2eAgInSc1d1	používající
plaintext	plaintext	k1gInSc1	plaintext
TCP	TCP	kA	TCP
protokol	protokol	k1gInSc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
telnet	telnet	k5eAaBmF	telnet
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
uživatelů	uživatel	k1gMnPc2	uživatel
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
byla	být	k5eAaImAgFnS	být
buď	buď	k8xC	buď
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
nebyla	být	k5eNaImAgFnS	být
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
brána	brát	k5eAaImNgFnS	brát
tak	tak	k6eAd1	tak
vážně	vážně	k6eAd1	vážně
jako	jako	k8xS	jako
po	po	k7c6	po
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
rozmachu	rozmach	k1gInSc6	rozmach
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc1	počet
uživatelů	uživatel	k1gMnPc2	uživatel
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
rostl	růst	k5eAaImAgInS	růst
také	také	k9	také
počet	počet	k1gInSc1	počet
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
nabourat	nabourat	k5eAaPmF	nabourat
do	do	k7c2	do
serverů	server	k1gInPc2	server
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
potřeba	potřeba	k1gFnSc1	potřeba
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
.	.	kIx.	.
</s>
<s>
Experti	expert	k1gMnPc1	expert
na	na	k7c4	na
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
již	již	k6eAd1	již
nadále	nadále	k6eAd1	nadále
nedoporučovali	doporučovat	k5eNaImAgMnP	doporučovat
použití	použití	k1gNnSc4	použití
telnetu	telnet	k1gInSc2	telnet
pro	pro	k7c4	pro
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
serverům	server	k1gInPc3	server
pro	pro	k7c4	pro
následují	následovat	k5eAaImIp3nP	následovat
důvody	důvod	k1gInPc4	důvod
<g/>
:	:	kIx,	:
Telnet	Telnet	k1gMnSc1	Telnet
standardně	standardně	k6eAd1	standardně
nešifroval	šifrovat	k5eNaBmAgMnS	šifrovat
žádná	žádný	k3yNgNnPc4	žádný
data	datum	k1gNnPc4	datum
odesílaná	odesílaný	k2eAgNnPc4d1	odesílané
do	do	k7c2	do
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
hesel	heslo	k1gNnPc2	heslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
dala	dát	k5eAaPmAgFnS	dát
komunikace	komunikace	k1gFnPc4	komunikace
přímo	přímo	k6eAd1	přímo
odposlouchávat	odposlouchávat	k5eAaImF	odposlouchávat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
takto	takto	k6eAd1	takto
odposlechnutá	odposlechnutý	k2eAgNnPc4d1	odposlechnuté
hesla	heslo	k1gNnPc4	heslo
zneužít	zneužít	k5eAaPmF	zneužít
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
kdo	kdo	k3yInSc1	kdo
měl	mít	k5eAaImAgMnS	mít
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
routeru	router	k1gInSc2	router
<g/>
,	,	kIx,	,
switche	switche	k1gFnSc4	switche
nebo	nebo	k8xC	nebo
hubu	huba	k1gFnSc4	huba
umístěného	umístěný	k2eAgMnSc2d1	umístěný
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
telnet	telnet	k1gInSc1	telnet
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
zachytit	zachytit	k5eAaPmF	zachytit
celou	celý	k2eAgFnSc4d1	celá
jejich	jejich	k3xOp3gFnSc4	jejich
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
hesla	heslo	k1gNnSc2	heslo
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
použitím	použití	k1gNnSc7	použití
několika	několik	k4yIc2	několik
běžných	běžný	k2eAgInPc2d1	běžný
programů	program	k1gInPc2	program
jako	jako	k8xS	jako
tcpdump	tcpdump	k1gInSc4	tcpdump
a	a	k8xC	a
Wireshark	Wireshark	k1gInSc4	Wireshark
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
implementací	implementace	k1gFnPc2	implementace
TELNETu	TELNETus	k1gInSc2	TELNETus
postrádala	postrádat	k5eAaImAgNnP	postrádat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
ověřování	ověřování	k1gNnSc4	ověřování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohlo	moct	k5eNaImAgNnS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
odposlouchávání	odposlouchávání	k1gNnSc3	odposlouchávání
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
běžně	běžně	k6eAd1	běžně
užívaných	užívaný	k2eAgMnPc2d1	užívaný
klientů	klient	k1gMnPc2	klient
telnetu	telnet	k1gInSc2	telnet
měla	mít	k5eAaImAgNnP	mít
několik	několik	k4yIc4	několik
zranitelných	zranitelný	k2eAgNnPc2d1	zranitelné
míst	místo	k1gNnPc2	místo
zjištěných	zjištěný	k2eAgNnPc2d1	zjištěné
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
nedostatky	nedostatek	k1gInPc4	nedostatek
způsobily	způsobit	k5eAaPmAgFnP	způsobit
rapidní	rapidní	k2eAgNnSc4d1	rapidní
opouštění	opouštění	k1gNnSc4	opouštění
od	od	k7c2	od
použití	použití	k1gNnSc2	použití
telnetu	telnet	k1gInSc2	telnet
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
internetu	internet	k1gInSc6	internet
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
protokolu	protokol	k1gInSc2	protokol
ssh	ssh	k?	ssh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
většinu	většina	k1gFnSc4	většina
funkcí	funkce	k1gFnPc2	funkce
telnetu	telnet	k1gInSc2	telnet
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
silné	silný	k2eAgNnSc4d1	silné
šifrování	šifrování	k1gNnSc4	šifrování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bránilo	bránit	k5eAaImAgNnS	bránit
v	v	k7c6	v
získávání	získávání	k1gNnSc6	získávání
hesel	heslo	k1gNnPc2	heslo
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
původních	původní	k2eAgInPc2d1	původní
internetových	internetový	k2eAgInPc2d1	internetový
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
telnetu	telnet	k1gInSc2	telnet
dala	dát	k5eAaPmAgFnS	dát
TLS	TLS	kA	TLS
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
SASL	SASL	kA	SASL
ověřování	ověřování	k1gNnSc4	ověřování
a	a	k8xC	a
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
opravit	opravit	k5eAaPmF	opravit
původní	původní	k2eAgInPc4d1	původní
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
byly	být	k5eAaImAgFnP	být
části	část	k1gFnPc1	část
protokolu	protokol	k1gInSc2	protokol
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
tyto	tyt	k2eAgNnSc4d1	tyto
rozšíření	rozšíření	k1gNnSc4	rozšíření
nepodporovaly	podporovat	k5eNaImAgFnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Klientským	klientský	k2eAgInSc7d1	klientský
programem	program	k1gInSc7	program
telnetu	telnet	k1gInSc2	telnet
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
testovací	testovací	k2eAgInPc4d1	testovací
účely	účel	k1gInPc4	účel
připojit	připojit	k5eAaPmF	připojit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
textově	textově	k6eAd1	textově
orientovanou	orientovaný	k2eAgFnSc4d1	orientovaná
službu	služba	k1gFnSc4	služba
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k9	tak
například	například	k6eAd1	například
simulovat	simulovat	k5eAaImF	simulovat
činnost	činnost	k1gFnSc4	činnost
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
-	-	kIx~	-
zadáním	zadání	k1gNnSc7	zadání
příkazu	příkaz	k1gInSc2	příkaz
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
telnet	telnet	k1gInSc1	telnet
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
80	[number]	k4	80
GET	GET	kA	GET
/	/	kIx~	/
HTTP	HTTP	kA	HTTP
<g/>
/	/	kIx~	/
<g/>
1.0	[number]	k4	1.0
Host	host	k1gMnSc1	host
<g/>
:	:	kIx,	:
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
a	a	k8xC	a
stisknutím	stisknutí	k1gNnSc7	stisknutí
2	[number]	k4	2
<g/>
×	×	k?	×
Enter	Entero	k1gNnPc2	Entero
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
HTTP	HTTP	kA	HTTP
protokolu	protokol	k1gInSc2	protokol
provede	provést	k5eAaPmIp3nS	provést
nejprve	nejprve	k6eAd1	nejprve
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
webovému	webový	k2eAgInSc3d1	webový
serveru	server	k1gInSc3	server
a	a	k8xC	a
pak	pak	k6eAd1	pak
žádost	žádost	k1gFnSc4	žádost
o	o	k7c6	o
zobrazení	zobrazení	k1gNnSc6	zobrazení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
stránky	stránka	k1gFnSc2	stránka
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
postupem	postup	k1gInSc7	postup
můžeme	moct	k5eAaImIp1nP	moct
snadno	snadno	k6eAd1	snadno
v	v	k7c6	v
případě	případ	k1gInSc6	případ
problémů	problém	k1gInPc2	problém
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nějaká	nějaký	k3yIgFnSc1	nějaký
služba	služba	k1gFnSc1	služba
vůbec	vůbec	k9	vůbec
běží	běžet	k5eAaImIp3nS	běžet
a	a	k8xC	a
zda	zda	k8xS	zda
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
rysech	rys	k1gInPc6	rys
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
-	-	kIx~	-
šifrovaný	šifrovaný	k2eAgInSc4d1	šifrovaný
přenos	přenos	k1gInSc4	přenos
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
-	-	kIx~	-
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
bezpečnější	bezpečný	k2eAgNnSc1d2	bezpečnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
telnet	telnet	k1gInSc1	telnet
</s>
