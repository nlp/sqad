<s>
Dle	dle	k7c2	dle
úzu	úzus	k1gInSc2	úzus
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívá	užívat	k5eAaImIp3nS	užívat
obecná	obecný	k2eAgFnSc1d1	obecná
zkratka	zkratka	k1gFnSc1	zkratka
prof.	prof.	kA	prof.
umístěná	umístěný	k2eAgFnSc1d1	umístěná
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
jako	jako	k9	jako
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
např.	např.	kA	např.
doc.	doc.	kA	doc.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zkratka	zkratka	k1gFnSc1	zkratka
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jí	on	k3xPp3gFnSc3	on
nezačíná	začínat	k5eNaImIp3nS	začínat
větný	větný	k2eAgInSc4d1	větný
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
