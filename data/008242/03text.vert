<p>
<s>
Profesor	profesor	k1gMnSc1	profesor
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
vědecko-pedagogická	vědeckoedagogický	k2eAgFnSc1d1	vědecko-pedagogická
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
umělecko-pedagogická	uměleckoedagogický	k2eAgFnSc1d1	umělecko-pedagogický
<g/>
)	)	kIx)	)
hodnost	hodnost	k1gFnSc4	hodnost
vysokoškolského	vysokoškolský	k2eAgMnSc4d1	vysokoškolský
pedagoga	pedagog	k1gMnSc4	pedagog
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
docent	docent	k1gMnSc1	docent
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
profiteri	profiteri	k1gNnSc2	profiteri
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přibližně	přibližně	k6eAd1	přibližně
znamená	znamenat	k5eAaImIp3nS	znamenat
veřejně	veřejně	k6eAd1	veřejně
vyznávat	vyznávat	k5eAaImF	vyznávat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
úzu	úzus	k1gInSc2	úzus
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívá	užívat	k5eAaImIp3nS	užívat
obecná	obecný	k2eAgFnSc1d1	obecná
zkratka	zkratka	k1gFnSc1	zkratka
prof.	prof.	kA	prof.
umístěná	umístěný	k2eAgFnSc1d1	umístěná
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
jako	jako	k9	jako
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
např.	např.	kA	např.
doc.	doc.	kA	doc.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zkratka	zkratka	k1gFnSc1	zkratka
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jí	on	k3xPp3gFnSc3	on
nezačíná	začínat	k5eNaImIp3nS	začínat
větný	větný	k2eAgInSc4d1	větný
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
této	tento	k3xDgFnSc2	tento
hodnosti	hodnost	k1gFnSc2	hodnost
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pozice	pozice	k1gFnSc1	pozice
professor	professora	k1gFnPc2	professora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Česka	Česko	k1gNnSc2	Česko
nemívá	mívat	k5eNaImIp3nS	mívat
charakter	charakter	k1gInSc1	charakter
doživotního	doživotní	k2eAgMnSc2d1	doživotní
"	"	kIx"	"
<g/>
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
příslušným	příslušný	k2eAgNnSc7d1	příslušné
funkčním	funkční	k2eAgNnSc7d1	funkční
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k8xC	jako
tradiční	tradiční	k2eAgNnSc1d1	tradiční
označení	označení	k1gNnSc1	označení
učitele	učitel	k1gMnSc2	učitel
na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
gymnáziích	gymnázium	k1gNnPc6	gymnázium
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
jiných	jiný	k2eAgFnPc6d1	jiná
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
používáno	používán	k2eAgNnSc1d1	používáno
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
jakožto	jakožto	k8xS	jakožto
označení	označení	k1gNnSc1	označení
regulovaného	regulovaný	k2eAgNnSc2d1	regulované
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
Research	Research	k1gMnSc1	Research
Professor	Professor	k1gMnSc1	Professor
jsou	být	k5eAaImIp3nP	být
ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
označováni	označován	k2eAgMnPc1d1	označován
doktoři	doktor	k1gMnPc1	doktor
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
primárně	primárně	k6eAd1	primárně
jen	jen	k9	jen
vědečtí	vědecký	k2eAgMnPc1d1	vědecký
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
i	i	k9	i
pedagogičtí	pedagogický	k2eAgMnPc1d1	pedagogický
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Činnost	činnost	k1gFnSc4	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Profesoři	profesor	k1gMnPc1	profesor
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
vědecké	vědecký	k2eAgMnPc4d1	vědecký
(	(	kIx(	(
<g/>
či	či	k8xC	či
umělecké	umělecký	k2eAgNnSc1d1	umělecké
<g/>
)	)	kIx)	)
a	a	k8xC	a
pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
činnosti	činnost	k1gFnSc3	činnost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
základní	základní	k2eAgInSc4d1	základní
či	či	k8xC	či
aplikovaný	aplikovaný	k2eAgInSc4d1	aplikovaný
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
teoretický	teoretický	k2eAgInSc4d1	teoretický
<g/>
,	,	kIx,	,
klinický	klinický	k2eAgInSc4d1	klinický
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgInSc4d1	experimentální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
vědecké	vědecký	k2eAgInPc1d1	vědecký
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
školí	školit	k5eAaImIp3nP	školit
studenty	student	k1gMnPc4	student
v	v	k7c6	v
doktorských	doktorský	k2eAgInPc6d1	doktorský
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
(	(	kIx(	(
<g/>
doktorandy	doktorand	k1gMnPc4	doktorand
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
PhD	PhD	k1gMnSc7	PhD
studenty	student	k1gMnPc7	student
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
výuku	výuka	k1gFnSc4	výuka
klíčových	klíčový	k2eAgInPc2d1	klíčový
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
státní	státní	k2eAgFnSc1d1	státní
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vést	vést	k5eAaImF	vést
výuku	výuka	k1gFnSc4	výuka
nebo	nebo	k8xC	nebo
přednášky	přednáška	k1gFnPc4	přednáška
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vést	vést	k5eAaImF	vést
semináře	seminář	k1gInPc1	seminář
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
cvičení	cvičení	k1gNnSc1	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
publikují	publikovat	k5eAaBmIp3nP	publikovat
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c4	v
roli	role	k1gFnSc4	role
garantů	garant	k1gMnPc2	garant
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
či	či	k8xC	či
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
–	–	k?	–
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
zdokonalování	zdokonalování	k1gNnSc6	zdokonalování
<g/>
;	;	kIx,	;
profesoři	profesor	k1gMnPc1	profesor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
klíčoví	klíčový	k2eAgMnPc1d1	klíčový
pro	pro	k7c4	pro
akreditace	akreditace	k1gFnPc4	akreditace
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
drží	držet	k5eAaImIp3nS	držet
obor	obor	k1gInSc1	obor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
rolích	role	k1gFnPc6	role
školitelů	školitel	k1gMnPc2	školitel
doktorandů	doktorand	k1gMnPc2	doktorand
při	při	k7c6	při
disertačních	disertační	k2eAgFnPc6d1	disertační
pracích	práce	k1gFnPc6	práce
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
mohou	moct	k5eAaImIp3nP	moct
předsedat	předsedat	k5eAaImF	předsedat
různým	různý	k2eAgFnPc3d1	různá
komisím	komise	k1gFnPc3	komise
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
účastnit	účastnit	k5eAaImF	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnPc1	vedoucí
či	či	k8xC	či
oponenti	oponent	k1gMnPc1	oponent
rigorózních	rigorózní	k2eAgFnPc2d1	rigorózní
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
magisterských	magisterský	k2eAgFnPc2d1	magisterská
(	(	kIx(	(
<g/>
diplomových	diplomový	k2eAgFnPc2d1	Diplomová
<g/>
)	)	kIx)	)
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
též	též	k9	též
i	i	k9	i
bakalářských	bakalářský	k2eAgFnPc2d1	Bakalářská
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zasedají	zasedat	k5eAaImIp3nP	zasedat
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
(	(	kIx(	(
<g/>
či	či	k8xC	či
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
<g/>
)	)	kIx)	)
radách	rada	k1gFnPc6	rada
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
(	(	kIx(	(
<g/>
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
<g/>
,	,	kIx,	,
či	či	k8xC	či
akademických	akademický	k2eAgFnPc6d1	akademická
<g/>
)	)	kIx)	)
radách	rada	k1gFnPc6	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
různých	různý	k2eAgNnPc6d1	různé
kolegiích	kolegium	k1gNnPc6	kolegium
atd.	atd.	kA	atd.
Další	další	k2eAgFnSc7d1	další
náplní	náplň	k1gFnSc7	náplň
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
tvorba	tvorba	k1gFnSc1	tvorba
vysokoškolských	vysokoškolský	k2eAgNnPc2d1	vysokoškolské
skript	skripta	k1gNnPc2	skripta
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
odborných	odborný	k2eAgInPc2d1	odborný
textů	text	k1gInPc2	text
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
vysokoškolských	vysokoškolský	k2eAgInPc2d1	vysokoškolský
předmětů	předmět	k1gInPc2	předmět
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
kurzů	kurz	k1gInPc2	kurz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
materiálů	materiál	k1gInPc2	materiál
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
,	,	kIx,	,
zkoušení	zkoušení	k1gNnPc4	zkoušení
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
směřování	směřování	k1gNnSc2	směřování
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgNnPc4d1	zahraniční
hostování	hostování	k1gNnPc4	hostování
<g/>
,	,	kIx,	,
oponentura	oponentura	k1gFnSc1	oponentura
habilitačních	habilitační	k2eAgFnPc2d1	habilitační
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
konferencích	konference	k1gFnPc6	konference
v	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
či	či	k8xC	či
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
členství	členství	k1gNnSc4	členství
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
typu	typ	k1gInSc2	typ
edičních	ediční	k2eAgFnPc2d1	ediční
rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
recenzentů	recenzent	k1gMnPc2	recenzent
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
jakožto	jakožto	k8xS	jakožto
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
též	též	k9	též
mohou	moct	k5eAaImIp3nP	moct
zastávat	zastávat	k5eAaImF	zastávat
případně	případně	k6eAd1	případně
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
řídící	řídící	k2eAgNnSc1d1	řídící
(	(	kIx(	(
<g/>
manažerské	manažerský	k2eAgNnSc1d1	manažerské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pozice	pozice	k1gFnSc1	pozice
akademických	akademický	k2eAgMnPc2d1	akademický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
např.	např.	kA	např.
funkce	funkce	k1gFnSc1	funkce
rektorů	rektor	k1gMnPc2	rektor
<g/>
,	,	kIx,	,
prorektorů	prorektor	k1gMnPc2	prorektor
<g/>
,	,	kIx,	,
děkanů	děkan	k1gMnPc2	děkan
<g/>
,	,	kIx,	,
proděkanů	proděkan	k1gMnPc2	proděkan
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
klinikách	klinika	k1gFnPc6	klinika
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
přednostů	přednosta	k1gMnPc2	přednosta
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesorem	profesor	k1gMnSc7	profesor
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
či	či	k8xC	či
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc1	osobnost
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
uznávanými	uznávaný	k2eAgInPc7d1	uznávaný
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
docentem	docent	k1gMnSc7	docent
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
předpisů	předpis	k1gInPc2	předpis
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
habilitací	habilitace	k1gFnPc2	habilitace
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc1	osobnost
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
uznávanými	uznávaný	k2eAgInPc7d1	uznávaný
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
===	===	k?	===
</s>
</p>
<p>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
§	§	k?	§
73	[number]	k4	73
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
obor	obor	k1gInSc4	obor
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
navržen	navrhnout	k5eAaPmNgInS	navrhnout
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
nebo	nebo	k8xC	nebo
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
radou	rada	k1gFnSc7	rada
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
řízení	řízení	k1gNnSc4	řízení
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
oboru	obor	k1gInSc6	obor
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řízení	řízení	k1gNnSc2	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
podléhá	podléhat	k5eAaImIp3nS	podléhat
kontrasignaci	kontrasignace	k1gFnSc4	kontrasignace
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Řízení	řízení	k1gNnSc1	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
akreditovaný	akreditovaný	k2eAgInSc4d1	akreditovaný
doktorský	doktorský	k2eAgInSc4d1	doktorský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
obor	obor	k1gInSc1	obor
jmenování	jmenování	k1gNnSc2	jmenování
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
jeho	jeho	k3xOp3gFnSc1	jeho
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc1	její
součást	součást	k1gFnSc1	součást
disponovala	disponovat	k5eAaBmAgFnS	disponovat
platnou	platný	k2eAgFnSc7d1	platná
akreditací	akreditace	k1gFnSc7	akreditace
pro	pro	k7c4	pro
konání	konání	k1gNnSc4	konání
řízení	řízení	k1gNnSc2	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
<g/>
Návrh	návrh	k1gInSc1	návrh
vědecké	vědecký	k2eAgFnSc2d1	vědecká
(	(	kIx(	(
<g/>
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
)	)	kIx)	)
rady	rada	k1gFnSc2	rada
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
obor	obor	k1gInSc4	obor
se	se	k3xPyFc4	se
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
podává	podávat	k5eAaImIp3nS	podávat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ministra	ministr	k1gMnSc2	ministr
(	(	kIx(	(
<g/>
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
či	či	k8xC	či
obrany	obrana	k1gFnSc2	obrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
kandidát	kandidát	k1gMnSc1	kandidát
úspěšně	úspěšně	k6eAd1	úspěšně
projde	projít	k5eAaPmIp3nS	projít
řízením	řízení	k1gNnSc7	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
vrátí	vrátit	k5eAaPmIp3nS	vrátit
návrh	návrh	k1gInSc4	návrh
na	na	k7c6	na
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
vědecké	vědecký	k2eAgFnSc2d1	vědecká
(	(	kIx(	(
<g/>
umělecké	umělecký	k2eAgFnSc3d1	umělecká
<g/>
)	)	kIx)	)
radě	rada	k1gFnSc3	rada
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebyl	být	k5eNaImAgMnS	být
dodržen	dodržen	k2eAgInSc4d1	dodržen
postup	postup	k1gInSc4	postup
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
profesorem	profesor	k1gMnSc7	profesor
stanovený	stanovený	k2eAgInSc4d1	stanovený
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vrácení	vrácení	k1gNnSc1	vrácení
návrhu	návrh	k1gInSc2	návrh
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odůvodněno	odůvodnit	k5eAaPmNgNnS	odůvodnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
správní	správní	k2eAgInSc1d1	správní
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
tak	tak	k9	tak
nejprve	nejprve	k6eAd1	nejprve
prochází	procházet	k5eAaImIp3nS	procházet
určitou	určitý	k2eAgFnSc7d1	určitá
kontrolou	kontrola	k1gFnSc7	kontrola
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
zpravena	zpraven	k2eAgFnSc1d1	zpravena
Vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
jednání	jednání	k1gNnSc6	jednání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ministr	ministr	k1gMnSc1	ministr
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
jmenování	jmenování	k1gNnSc4	jmenování
předkládá	předkládat	k5eAaImIp3nS	předkládat
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
přijímá	přijímat	k5eAaImIp3nS	přijímat
usnesení	usnesení	k1gNnSc4	usnesení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
či	či	k8xC	či
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spolupodepsal	spolupodepsat	k5eAaPmAgMnS	spolupodepsat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
přitom	přitom	k6eAd1	přitom
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
63	[number]	k4	63
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
Ústavy	ústava	k1gFnSc2	ústava
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
platnosti	platnost	k1gFnSc3	platnost
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
spolupodpis	spolupodpis	k1gInSc1	spolupodpis
(	(	kIx(	(
<g/>
kontrasignaci	kontrasignace	k1gFnSc6	kontrasignace
<g/>
)	)	kIx)	)
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
za	za	k7c4	za
které	který	k3yRgInPc4	který
nese	nést	k5eAaImIp3nS	nést
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
dekret	dekret	k1gInSc4	dekret
tak	tak	k6eAd1	tak
nese	nést	k5eAaImIp3nS	nést
podpis	podpis	k1gInSc1	podpis
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
profesorů	profesor	k1gMnPc2	profesor
se	se	k3xPyFc4	se
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jmenování	jmenování	k1gNnSc1	jmenování
profesorů	profesor	k1gMnPc2	profesor
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
v	v	k7c6	v
datu	datum	k1gNnSc6	datum
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
termín	termín	k1gInSc1	termín
pak	pak	k6eAd1	pak
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
datu	datum	k1gNnSc6	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
jmenování	jmenování	k1gNnSc1	jmenování
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
možné	možný	k2eAgNnSc1d1	možné
profesora	profesor	k1gMnSc4	profesor
formálně	formálně	k6eAd1	formálně
oslovovat	oslovovat	k5eAaImF	oslovovat
touto	tento	k3xDgFnSc7	tento
hodností	hodnost	k1gFnSc7	hodnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
užívat	užívat	k5eAaImF	užívat
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
až	až	k9	až
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
místa	místo	k1gNnSc2	místo
profesora	profesor	k1gMnSc2	profesor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
MgA.	MgA.	k1gMnSc1	MgA.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
–	–	k?	–
pane	pan	k1gMnSc5	pan
profesore	profesor	k1gMnSc5	profesor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
docent	docent	k1gMnSc1	docent
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
získanému	získaný	k2eAgInSc3d1	získaný
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
za	za	k7c4	za
rovnocenný	rovnocenný	k2eAgInSc4d1	rovnocenný
pokládá	pokládat	k5eAaImIp3nS	pokládat
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
titul	titul	k1gInSc4	titul
získaný	získaný	k2eAgInSc4d1	získaný
v	v	k7c4	v
SR	SR	kA	SR
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypršela	vypršet	k5eAaPmAgFnS	vypršet
platnost	platnost	k1gFnSc4	platnost
příslušné	příslušný	k2eAgFnSc2d1	příslušná
mezivládní	mezivládní	k2eAgFnSc2d1	mezivládní
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řízení	řízení	k1gNnSc1	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
===	===	k?	===
</s>
</p>
<p>
<s>
Řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
předchází	předcházet	k5eAaImIp3nS	předcházet
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc3	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc4	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
jmenovací	jmenovací	k2eAgNnSc1d1	jmenovací
řízení	řízení	k1gNnSc1	řízení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
§	§	k?	§
74	[number]	k4	74
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
se	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
docent	docent	k1gMnSc1	docent
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
též	též	k9	též
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
renomované	renomovaný	k2eAgFnSc6d1	renomovaná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
význačnou	význačný	k2eAgFnSc7d1	význačná
a	a	k8xC	a
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
nebo	nebo	k8xC	nebo
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
osobností	osobnost	k1gFnSc7	osobnost
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
který	který	k3yQgInSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
projde	projít	k5eAaPmIp3nS	projít
řízením	řízení	k1gNnSc7	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Uchazeč	uchazeč	k1gMnSc1	uchazeč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
řízením	řízení	k1gNnSc7	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc6	jmenování
profesorem	profesor	k1gMnSc7	profesor
úspěšně	úspěšně	k6eAd1	úspěšně
projde	projít	k5eAaPmIp3nS	projít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
následně	následně	k6eAd1	následně
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
zastávat	zastávat	k5eAaImF	zastávat
pozici	pozice	k1gFnSc4	pozice
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
profesorem	profesor	k1gMnSc7	profesor
se	se	k3xPyFc4	se
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
nebo	nebo	k8xC	nebo
umělecká	umělecký	k2eAgFnSc1d1	umělecká
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
uchazeče	uchazeč	k1gMnSc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovací	jmenovací	k2eAgNnSc1d1	jmenovací
řízení	řízení	k1gNnSc1	řízení
se	se	k3xPyFc4	se
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
uchazeče	uchazeč	k1gMnSc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Příslušný	příslušný	k2eAgInSc1d1	příslušný
návrh	návrh	k1gInSc1	návrh
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
alespoň	alespoň	k9	alespoň
dvěma	dva	k4xCgInPc7	dva
dalšími	další	k2eAgInPc7d1	další
profesory	profesor	k1gMnPc4	profesor
stejného	stejný	k2eAgInSc2d1	stejný
či	či	k8xC	či
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
oboru	obor	k1gInSc2	obor
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc1	jejich
stanovisky	stanovisko	k1gNnPc7	stanovisko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
návrhem	návrh	k1gInSc7	návrh
děkana	děkan	k1gMnSc2	děkan
či	či	k8xC	či
rektora	rektor	k1gMnSc2	rektor
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc4	který
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
dané	daný	k2eAgFnSc2d1	daná
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
následně	následně	k6eAd1	následně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
u	u	k7c2	u
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
umělecká	umělecký	k2eAgFnSc1d1	umělecká
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rada	rada	k1gFnSc1	rada
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
docenti	docent	k1gMnPc1	docent
nebo	nebo	k8xC	nebo
jiní	jiný	k2eAgMnPc1d1	jiný
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
daného	daný	k2eAgInSc2d1	daný
nebo	nebo	k8xC	nebo
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
oboru	obor	k1gInSc2	obor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
doktoři	doktor	k1gMnPc1	doktor
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
projednání	projednání	k1gNnSc6	projednání
a	a	k8xC	a
tajném	tajný	k2eAgNnSc6d1	tajné
hlasování	hlasování	k1gNnSc6	hlasování
přednese	přednést	k5eAaPmIp3nS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
uchazeč	uchazeč	k1gMnSc1	uchazeč
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
zohledňují	zohledňovat	k5eAaImIp3nP	zohledňovat
a	a	k8xC	a
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
<g/>
:	:	kIx,	:
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
počty	počet	k1gInPc1	počet
doktorandů	doktorand	k1gMnPc2	doktorand
a	a	k8xC	a
doktorů	doktor	k1gMnPc2	doktor
<g/>
,	,	kIx,	,
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
činnost	činnost	k1gFnSc1	činnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
též	též	k9	též
Hirschův	Hirschův	k2eAgInSc1d1	Hirschův
index	index	k1gInSc1	index
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
umělecká	umělecký	k2eAgFnSc1d1	umělecká
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
účasti	účast	k1gFnPc1	účast
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
grantů	grant	k1gInPc2	grant
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
GA	GA	kA	GA
ČR	ČR	kA	ČR
/	/	kIx~	/
TA	ten	k3xDgFnSc1	ten
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
hostování	hostování	k1gNnSc6	hostování
<g/>
,	,	kIx,	,
absolvované	absolvovaný	k2eAgFnSc3d1	absolvovaná
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
či	či	k8xC	či
tuzemské	tuzemský	k2eAgFnPc4d1	tuzemská
stáže	stáž	k1gFnPc4	stáž
<g/>
,	,	kIx,	,
účasti	účast	k1gFnPc4	účast
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
činnosti	činnost	k1gFnPc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
návrh	návrh	k1gInSc1	návrh
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
<g/>
,	,	kIx,	,
uchazeč	uchazeč	k1gMnSc1	uchazeč
musí	muset	k5eAaImIp3nS	muset
přednést	přednést	k5eAaPmF	přednést
přednášku	přednáška	k1gFnSc4	přednáška
(	(	kIx(	(
<g/>
profesorská	profesorský	k2eAgFnSc1d1	profesorská
přednáška	přednáška	k1gFnSc1	přednáška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
předloží	předložit	k5eAaPmIp3nS	předložit
svou	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
výuky	výuka	k1gFnSc2	výuka
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
vědecké	vědecký	k2eAgFnPc1d1	vědecká
(	(	kIx(	(
<g/>
či	či	k8xC	či
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
definitivní	definitivní	k2eAgNnSc1d1	definitivní
tajné	tajný	k2eAgNnSc1d1	tajné
hlasování	hlasování	k1gNnSc1	hlasování
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
kvalifikační	kvalifikační	k2eAgFnSc1d1	kvalifikační
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
řízení	řízení	k1gNnSc6	řízení
nepředkládá	předkládat	k5eNaImIp3nS	předkládat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
procesnímu	procesní	k2eAgInSc3d1	procesní
postupu	postup	k1gInSc3	postup
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
může	moct	k5eAaImIp3nS	moct
uchazeč	uchazeč	k1gMnSc1	uchazeč
podat	podat	k5eAaPmF	podat
námitky	námitka	k1gFnPc4	námitka
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
profesorem	profesor	k1gMnSc7	profesor
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
správní	správní	k2eAgInSc1d1	správní
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
<g/>
Vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
oborech	obor	k1gInPc6	obor
může	moct	k5eAaImIp3nS	moct
příslušná	příslušný	k2eAgFnSc1d1	příslušná
umělecká	umělecký	k2eAgFnSc1d1	umělecká
(	(	kIx(	(
<g/>
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
)	)	kIx)	)
rada	rada	k1gFnSc1	rada
prominout	prominout	k5eAaPmF	prominout
uchazeči	uchazeč	k1gMnPc1	uchazeč
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
požadavek	požadavek	k1gInSc1	požadavek
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc4	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ve	v	k7c6	v
fakultativním	fakultativní	k2eAgNnSc6d1	fakultativní
rigorózním	rigorózní	k2eAgNnSc6d1	rigorózní
řízení	řízení	k1gNnSc6	řízení
nebo	nebo	k8xC	nebo
habilitačním	habilitační	k2eAgNnSc6d1	habilitační
řízení	řízení	k1gNnSc6	řízení
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
finančními	finanční	k2eAgInPc7d1	finanční
poplatky	poplatek	k1gInPc7	poplatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
–	–	k?	–
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovací	jmenovací	k2eAgNnSc1d1	jmenovací
řízení	řízení	k1gNnSc1	řízení
by	by	kYmCp3nS	by
zpravidla	zpravidla	k6eAd1	zpravidla
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vedeno	vést	k5eAaImNgNnS	vést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
do	do	k7c2	do
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
zahájení	zahájení	k1gNnSc2	zahájení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podrobnosti	podrobnost	k1gFnPc1	podrobnost
bývají	bývat	k5eAaImIp3nP	bývat
upraveny	upravit	k5eAaPmNgFnP	upravit
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
předpisech	předpis	k1gInPc6	předpis
(	(	kIx(	(
<g/>
jmenovacích	jmenovací	k2eAgInPc6d1	jmenovací
řádech	řád	k1gInPc6	řád
<g/>
)	)	kIx)	)
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Professor	Professor	k1gInSc1	Professor
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
veřejný	veřejný	k2eAgInSc4d1	veřejný
<g/>
,	,	kIx,	,
vládou	vláda	k1gFnSc7	vláda
ustanovený	ustanovený	k2eAgMnSc1d1	ustanovený
a	a	k8xC	a
placený	placený	k2eAgMnSc1d1	placený
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořili	tvořit	k5eAaImAgMnP	tvořit
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
profesoři	profesor	k1gMnPc1	profesor
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
fakultě	fakulta	k1gFnSc6	fakulta
profesorský	profesorský	k2eAgInSc4d1	profesorský
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
dělili	dělit	k5eAaImAgMnP	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
professores	professores	k1gMnSc1	professores
ordinarii	ordinarius	k1gMnPc7	ordinarius
–	–	k?	–
profesory	profesor	k1gMnPc4	profesor
řádné	řádný	k2eAgMnPc4d1	řádný
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgFnPc1d1	vybavená
určitými	určitý	k2eAgFnPc7d1	určitá
právy	práv	k2eAgMnPc4d1	práv
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
voleni	volit	k5eAaImNgMnP	volit
děkany	děkan	k1gMnPc7	děkan
a	a	k8xC	a
rektory	rektor	k1gMnPc7	rektor
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
professores	professores	k1gInSc4	professores
extraordinarii	extraordinarie	k1gFnSc4	extraordinarie
–	–	k?	–
profesory	profesor	k1gMnPc4	profesor
mimořádné	mimořádný	k2eAgInPc4d1	mimořádný
<g/>
,	,	kIx,	,
vyučující	vyučující	k2eAgInPc4d1	vyučující
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nebyly	být	k5eNaImAgFnP	být
státem	stát	k1gInSc7	stát
systemizovány	systemizovat	k5eAaBmNgFnP	systemizovat
<g/>
,	,	kIx,	,
již	již	k9	již
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
voleni	volit	k5eAaImNgMnP	volit
děkany	děkan	k1gMnPc7	děkan
a	a	k8xC	a
rektory	rektor	k1gMnPc7	rektor
<g/>
.	.	kIx.	.
<g/>
Univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
profesoři	profesor	k1gMnPc1	profesor
byli	být	k5eAaImAgMnP	být
též	též	k9	též
zváni	zván	k2eAgMnPc1d1	zván
doctores	doctores	k1gMnSc1	doctores
nebo	nebo	k8xC	nebo
magistri	magistr	k1gMnPc1	magistr
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
byli	být	k5eAaImAgMnP	být
zváni	zvát	k5eAaImNgMnP	zvát
scholares	scholares	k1gMnSc1	scholares
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
existovali	existovat	k5eAaImAgMnP	existovat
professores	professores	k1gMnSc1	professores
honorarii	honorarie	k1gFnSc4	honorarie
<g/>
,	,	kIx,	,
profesoři	profesor	k1gMnPc1	profesor
čestní	čestný	k2eAgMnPc1d1	čestný
<g/>
,	,	kIx,	,
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
povinností	povinnost	k1gFnSc7	povinnost
<g/>
,	,	kIx,	,
vyučovat	vyučovat	k5eAaImF	vyučovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
suplující	suplující	k2eAgMnPc1d1	suplující
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
řádné	řádný	k2eAgMnPc4d1	řádný
profesory	profesor	k1gMnPc4	profesor
a	a	k8xC	a
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
dostávali	dostávat	k5eAaImAgMnP	dostávat
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
odměnu	odměna	k1gFnSc4	odměna
k	k	k7c3	k
platu	plat	k1gInSc3	plat
(	(	kIx(	(
<g/>
remuneraci	remunerace	k1gFnSc4	remunerace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
i	i	k9	i
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
nepamětný	pamětný	k2eNgInSc1d1	pamětný
zvyk	zvyk	k1gInSc1	zvyk
titulovat	titulovat	k5eAaImF	titulovat
"	"	kIx"	"
<g/>
profesory	profesor	k1gMnPc7	profesor
<g/>
"	"	kIx"	"
i	i	k9	i
učitele	učitel	k1gMnPc4	učitel
na	na	k7c6	na
gymnáziích	gymnázium	k1gNnPc6	gymnázium
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgNnSc1d1	pracovní
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
přiznával	přiznávat	k5eAaImAgInS	přiznávat
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaným	vzdělaný	k2eAgMnPc3d1	vzdělaný
pedagogům	pedagog	k1gMnPc3	pedagog
československých	československý	k2eAgFnPc2d1	Československá
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
ještě	ještě	k9	ještě
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
profesory	profesor	k1gMnPc7	profesor
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zpravidla	zpravidla	k6eAd1	zpravidla
titulují	titulovat	k5eAaImIp3nP	titulovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
jest	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
profesor	profesor	k1gMnSc1	profesor
<g/>
]	]	kIx)	]
titulem	titul	k1gInSc7	titul
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
a	a	k8xC	a
maje	mít	k5eAaImSgMnS	mít
sloužiti	sloužit	k5eAaImF	sloužit
ku	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
lesku	lesk	k1gInSc2	lesk
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Vědecko-pedagogický	vědeckoedagogický	k2eAgInSc1d1	vědecko-pedagogický
titul	titul	k1gInSc1	titul
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
doživotní	doživotní	k2eAgNnSc1d1	doživotní
označení	označení	k1gNnSc1	označení
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
"	"	kIx"	"
<g/>
prof.	prof.	kA	prof.
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Prof.	prof.	kA	prof.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
zákonu	zákon	k1gInSc6	zákon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
udílel	udílet	k5eAaImAgMnS	udílet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
do	do	k7c2	do
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
či	či	k8xC	či
zákonů	zákon	k1gInPc2	zákon
dřívějších	dřívější	k2eAgInPc2d1	dřívější
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
zákony	zákon	k1gInPc1	zákon
uvedené	uvedený	k2eAgFnPc1d1	uvedená
nechaly	nechat	k5eAaPmAgFnP	nechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
uvedené	uvedený	k2eAgFnPc1d1	uvedená
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1969	[number]	k4	1969
mohl	moct	k5eAaImAgInS	moct
navíc	navíc	k6eAd1	navíc
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
navrhovat	navrhovat	k5eAaImF	navrhovat
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
i	i	k8xC	i
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
tzv.	tzv.	kA	tzv.
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
řádného	řádný	k2eAgNnSc2d1	řádné
jmenovacího	jmenovací	k2eAgNnSc2d1	jmenovací
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
uvedené	uvedený	k2eAgFnSc2d1	uvedená
taktéž	taktéž	k?	taktéž
umožňoval	umožňovat	k5eAaImAgMnS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
navrhovat	navrhovat	k5eAaImF	navrhovat
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
titul	titul	k1gInSc1	titul
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
i	i	k9	i
docent	docent	k1gMnSc1	docent
<g/>
)	)	kIx)	)
též	též	k9	též
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
ČSR	ČSR	kA	ČSR
(	(	kIx(	(
<g/>
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
řádného	řádný	k2eAgNnSc2d1	řádné
jmenovacího	jmenovací	k2eAgNnSc2d1	jmenovací
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgFnP	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
vědecko-pedagogické	vědeckoedagogický	k2eAgInPc4d1	vědecko-pedagogický
tituly	titul	k1gInPc4	titul
takto	takto	k6eAd1	takto
přiznané	přiznaný	k2eAgInPc4d1	přiznaný
mezi	mezi	k7c7	mezi
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1969	[number]	k4	1969
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
odňaty	odňat	k2eAgInPc1d1	odňat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jmenování	jmenování	k1gNnSc4	jmenování
profesorem	profesor	k1gMnSc7	profesor
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
povinnost	povinnost	k1gFnSc1	povinnost
nejprve	nejprve	k6eAd1	nejprve
úspěšně	úspěšně	k6eAd1	úspěšně
absolvovat	absolvovat	k5eAaPmF	absolvovat
jmenovací	jmenovací	k2eAgNnSc4d1	jmenovací
řízení	řízení	k1gNnSc4	řízení
bez	bez	k7c2	bez
uvedené	uvedený	k2eAgFnSc2d1	uvedená
možnosti	možnost	k1gFnSc2	možnost
(	(	kIx(	(
<g/>
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
)	)	kIx)	)
ministra	ministr	k1gMnSc2	ministr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
profesory	profesor	k1gMnPc7	profesor
i	i	k9	i
nadále	nadále	k6eAd1	nadále
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
návrhu	návrh	k1gInSc2	návrh
předloženého	předložený	k2eAgInSc2d1	předložený
ministrem	ministr	k1gMnSc7	ministr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
již	již	k9	již
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
ingerence	ingerence	k1gFnSc2	ingerence
ministra	ministr	k1gMnSc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
již	již	k6eAd1	již
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c6	na
principech	princip	k1gInPc6	princip
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
udílel	udílet	k5eAaImAgMnS	udílet
titul	titul	k1gInSc4	titul
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
též	též	k9	též
docent	docent	k1gMnSc1	docent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
však	však	k9	však
již	již	k9	již
zkratky	zkratka	k1gFnPc4	zkratka
těchto	tento	k3xDgInPc2	tento
titulů	titul	k1gInPc2	titul
nekodifikoval	kodifikovat	k5eNaBmAgInS	kodifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
zákony	zákon	k1gInPc1	zákon
tyto	tento	k3xDgMnPc4	tento
nechaly	nechat	k5eAaPmAgInP	nechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
uvedené	uvedený	k2eAgFnPc1d1	uvedená
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
<g/>
Nový	nový	k2eAgInSc1d1	nový
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
již	již	k6eAd1	již
uvedené	uvedený	k2eAgNnSc1d1	uvedené
za	za	k7c4	za
tituly	titul	k1gInPc4	titul
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
neuvádí	uvádět	k5eNaImIp3nP	uvádět
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gFnPc1	jejich
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
titulem	titul	k1gInSc7	titul
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
po	po	k7c6	po
jmenování	jmenování	k1gNnSc6	jmenování
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
získání	získání	k1gNnSc6	získání
doživotního	doživotní	k2eAgNnSc2d1	doživotní
–	–	k?	–
nyní	nyní	k6eAd1	nyní
"	"	kIx"	"
<g/>
označení	označení	k1gNnSc1	označení
<g/>
"	"	kIx"	"
–	–	k?	–
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovací	jmenovací	k2eAgNnSc4d1	jmenovací
řízení	řízení	k1gNnSc4	řízení
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zákoně	zákon	k1gInSc6	zákon
zpřesněna	zpřesněn	k2eAgFnSc1d1	zpřesněna
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
příslušná	příslušný	k2eAgFnSc1d1	příslušná
komise	komise	k1gFnSc1	komise
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
i	i	k9	i
po	po	k7c6	po
novelizování	novelizování	k1gNnSc6	novelizování
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
současný	současný	k2eAgInSc1d1	současný
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
po	po	k7c6	po
Boloňském	boloňský	k2eAgInSc6d1	boloňský
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vysoké	vysoká	k1gFnSc3	vysoká
školství	školství	k1gNnSc1	školství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
sjednocuje	sjednocovat	k5eAaImIp3nS	sjednocovat
<g/>
,	,	kIx,	,
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k9	i
nadále	nadále	k6eAd1	nadále
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
ustanovováním	ustanovování	k1gNnSc7	ustanovování
na	na	k7c4	na
pracovní	pracovní	k2eAgNnSc4d1	pracovní
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
(	(	kIx(	(
<g/>
či	či	k8xC	či
docenta	docent	k1gMnSc4	docent
<g/>
)	)	kIx)	)
a	a	k8xC	a
jmenováním	jmenování	k1gNnSc7	jmenování
profesorem	profesor	k1gMnSc7	profesor
(	(	kIx(	(
<g/>
docentem	docent	k1gMnSc7	docent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přiznáním	přiznání	k1gNnSc7	přiznání
doživotního	doživotní	k2eAgNnSc2d1	doživotní
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
docent	docent	k1gMnSc1	docent
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
osvědčuje	osvědčovat	k5eAaImIp3nS	osvědčovat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
dané	daný	k2eAgFnSc2d1	daná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovací	jmenovací	k2eAgNnSc1d1	jmenovací
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
či	či	k8xC	či
habilitační	habilitační	k2eAgNnSc1d1	habilitační
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
fakultativní	fakultativní	k2eAgNnSc4d1	fakultativní
rigorózní	rigorózní	k2eAgNnSc4d1	rigorózní
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
event.	event.	k?	event.
i	i	k8xC	i
řízení	řízení	k1gNnSc4	řízení
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
vědeckého	vědecký	k2eAgInSc2d1	vědecký
titulu	titul	k1gInSc2	titul
<g/>
)	)	kIx)	)
nepředstavuje	představovat	k5eNaImIp3nS	představovat
však	však	k9	však
dle	dle	k7c2	dle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
další	další	k2eAgNnSc4d1	další
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
stupněm	stupeň	k1gInSc7	stupeň
je	být	k5eAaImIp3nS	být
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
je	být	k5eAaImIp3nS	být
přiznáván	přiznáván	k2eAgInSc1d1	přiznáván
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
doktorského	doktorský	k2eAgInSc2d1	doktorský
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
,	,	kIx,	,
doktorské	doktorský	k2eAgNnSc1d1	doktorské
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
průkaze	průkaz	k1gInSc6	průkaz
se	se	k3xPyFc4	se
tak	tak	k9	tak
označení	označení	k1gNnSc1	označení
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
docent	docent	k1gMnSc1	docent
<g/>
)	)	kIx)	)
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
současných	současný	k2eAgMnPc2d1	současný
držitelů	držitel	k1gMnPc2	držitel
tedy	tedy	k8xC	tedy
event.	event.	k?	event.
uvádí	uvádět	k5eAaImIp3nS	uvádět
plně	plně	k6eAd1	plně
vypsané	vypsaný	k2eAgNnSc1d1	vypsané
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
jakožto	jakožto	k8xS	jakožto
označení	označení	k1gNnSc2	označení
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
stále	stále	k6eAd1	stále
jako	jako	k8xC	jako
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
doživotní	doživotní	k2eAgNnSc1d1	doživotní
označení	označení	k1gNnSc1	označení
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
uvedené	uvedený	k2eAgFnSc2d1	uvedená
historické	historický	k2eAgFnSc2d1	historická
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c4	na
(	(	kIx(	(
<g/>
řádné	řádný	k2eAgMnPc4d1	řádný
<g/>
)	)	kIx)	)
profesory	profesor	k1gMnPc4	profesor
a	a	k8xC	a
mimořádné	mimořádný	k2eAgMnPc4d1	mimořádný
profesory	profesor	k1gMnPc4	profesor
opět	opět	k6eAd1	opět
zákonem	zákon	k1gInSc7	zákon
zavedlo	zavést	k5eAaPmAgNnS	zavést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
též	též	k9	též
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavení	postavení	k1gNnSc1	postavení
hostujících	hostující	k2eAgMnPc2d1	hostující
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
emeritních	emeritní	k2eAgMnPc2d1	emeritní
profesorů	profesor	k1gMnPc2	profesor
stanoví	stanovit	k5eAaPmIp3nS	stanovit
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
předpis	předpis	k1gInSc1	předpis
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
===	===	k?	===
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
systém	systém	k1gInSc1	systém
obsazování	obsazování	k1gNnSc2	obsazování
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
jmenování	jmenování	k1gNnSc4	jmenování
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
bývá	bývat	k5eAaImIp3nS	bývat
kritizován	kritizován	k2eAgMnSc1d1	kritizován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ačkoli	ačkoli	k8xS	ačkoli
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc4d1	poslední
<g/>
/	/	kIx~	/
<g/>
finální	finální	k2eAgFnSc4d1	finální
(	(	kIx(	(
<g/>
terminal	terminat	k5eAaPmAgInS	terminat
degree	degree	k1gFnSc4	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k9	jako
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
možnou	možný	k2eAgFnSc4d1	možná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
,	,	kIx,	,
doktorský	doktorský	k2eAgInSc4d1	doktorský
program	program	k1gInSc4	program
(	(	kIx(	(
<g/>
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
doktorského	doktorský	k2eAgInSc2d1	doktorský
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
blíže	blíž	k1gFnSc2	blíž
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
nadále	nadále	k6eAd1	nadále
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc6	stát
bráno	brát	k5eAaImNgNnS	brát
de	de	k?	de
facto	facto	k1gNnSc1	facto
jako	jako	k8xC	jako
získání	získání	k1gNnSc1	získání
dalšího	další	k2eAgInSc2d1	další
titulu	titul	k1gInSc2	titul
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
přiznání	přiznání	k1gNnSc1	přiznání
doživotního	doživotní	k2eAgNnSc2d1	doživotní
označení	označení	k1gNnSc2	označení
<g/>
)	)	kIx)	)
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
historicky	historicky	k6eAd1	historicky
během	během	k7c2	během
akademického	akademický	k2eAgInSc2d1	akademický
obřadu	obřad	k1gInSc2	obřad
podobného	podobný	k2eAgInSc2d1	podobný
promoci	promoce	k1gFnSc3	promoce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
jakožto	jakožto	k8xS	jakožto
ad-hoc	adoc	k6eAd1	ad-hoc
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
např.	např.	kA	např.
rektora	rektor	k1gMnSc2	rektor
zakládající	zakládající	k2eAgInSc4d1	zakládající
pracovněprávní	pracovněprávní	k2eAgInSc4d1	pracovněprávní
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
vůči	vůči	k7c3	vůči
vysoké	vysoký	k2eAgFnSc3d1	vysoká
škole	škola	k1gFnSc3	škola
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
jmenování	jmenování	k1gNnSc4	jmenování
profesorem	profesor	k1gMnSc7	profesor
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vůči	vůči	k7c3	vůči
škole	škola	k1gFnSc3	škola
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
dotyčný	dotyčný	k2eAgInSc1d1	dotyčný
zaměstnán	zaměstnán	k2eAgInSc1d1	zaměstnán
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
odborných	odborný	k2eAgMnPc2d1	odborný
asistentů	asistent	k1gMnPc2	asistent
<g/>
,	,	kIx,	,
asistentů	asistent	k1gMnPc2	asistent
či	či	k8xC	či
lektorů	lektor	k1gMnPc2	lektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
brán	brát	k5eAaImNgMnS	brát
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
jakožto	jakožto	k8xS	jakožto
další	další	k2eAgInSc4d1	další
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
historicky	historicky	k6eAd1	historicky
před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Praxe	praxe	k1gFnSc1	praxe
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
řádném	řádný	k2eAgNnSc6d1	řádné
zakončení	zakončení	k1gNnSc6	zakončení
jmenovacího	jmenovací	k2eAgNnSc2d1	jmenovací
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
jmenování	jmenování	k1gNnSc2	jmenování
profesora	profesor	k1gMnSc2	profesor
(	(	kIx(	(
<g/>
prezidentem	prezident	k1gMnSc7	prezident
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
profesor	profesor	k1gMnSc1	profesor
jmenován	jmenován	k2eAgMnSc1d1	jmenován
na	na	k7c4	na
příslušné	příslušný	k2eAgNnSc4d1	příslušné
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
pozici	pozice	k1gFnSc4	pozice
<g/>
)	)	kIx)	)
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
případně	případně	k6eAd1	případně
odvolán	odvolat	k5eAaPmNgMnS	odvolat
a	a	k8xC	a
již	již	k6eAd1	již
by	by	kYmCp3nS	by
profesorem	profesor	k1gMnSc7	profesor
nebyl	být	k5eNaImAgInS	být
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
tohoto	tento	k3xDgNnSc2	tento
doživotního	doživotní	k2eAgNnSc2d1	doživotní
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
jmenováním	jmenování	k1gNnSc7	jmenování
<g/>
"	"	kIx"	"
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
profesor	profesor	k1gMnSc1	profesor
může	moct	k5eAaImIp3nS	moct
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
pracovní	pracovní	k2eAgFnSc4d1	pracovní
pozici	pozice	k1gFnSc4	pozice
shodně	shodně	k6eAd1	shodně
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
nutné	nutný	k2eAgNnSc1d1	nutné
absolvovat	absolvovat	k5eAaPmF	absolvovat
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
pracovním	pracovní	k2eAgNnSc6d1	pracovní
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
stále	stále	k6eAd1	stále
kvalifikován	kvalifikován	k2eAgMnSc1d1	kvalifikován
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
běžné	běžný	k2eAgFnPc4d1	běžná
např.	např.	kA	např.
v	v	k7c6	v
anglosaském	anglosaský	k2eAgInSc6d1	anglosaský
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
však	však	k9	však
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
jmenování	jmenování	k1gNnSc6	jmenování
obdrží	obdržet	k5eAaPmIp3nS	obdržet
jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
diplom	diplom	k1gInSc4	diplom
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
akademických	akademický	k2eAgInPc2d1	akademický
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
tradicí	tradice	k1gFnSc7	tradice
zavedl	zavést	k5eAaPmAgMnS	zavést
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
profesor	profesor	k1gMnSc1	profesor
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
docent	docent	k1gMnSc1	docent
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vědecko-pedagogické	vědeckoedagogický	k2eAgInPc4d1	vědecko-pedagogický
tituly	titul	k1gInPc4	titul
<g/>
"	"	kIx"	"
oddělené	oddělený	k2eAgInPc1d1	oddělený
od	od	k7c2	od
příslušného	příslušný	k2eAgNnSc2d1	příslušné
pracovního	pracovní	k2eAgNnSc2d1	pracovní
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
udělení	udělení	k1gNnSc1	udělení
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
kvalifikačním	kvalifikační	k2eAgInSc7d1	kvalifikační
předpokladem	předpoklad	k1gInSc7	předpoklad
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
pracovního	pracovní	k2eAgInSc2d1	pracovní
<g />
.	.	kIx.	.
</s>
<s>
místa	místo	k1gNnPc1	místo
stejného	stejný	k2eAgNnSc2d1	stejné
označení	označení	k1gNnSc2	označení
<g/>
;	;	kIx,	;
a	a	k8xC	a
že	že	k8xS	že
uváděné	uváděný	k2eAgNnSc1d1	uváděné
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnocení	hodnocení	k1gNnSc1	hodnocení
způsobilosti	způsobilost	k1gFnSc2	způsobilost
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
učitele	učitel	k1gMnSc2	učitel
musí	muset	k5eAaImIp3nS	muset
vycházet	vycházet	k5eAaImF	vycházet
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
základních	základní	k2eAgNnPc2d1	základní
hledisek	hledisko	k1gNnPc2	hledisko
politické	politický	k2eAgFnSc2d1	politická
<g/>
,	,	kIx,	,
ideologické	ideologický	k2eAgFnSc2d1	ideologická
a	a	k8xC	a
odborné	odborný	k2eAgFnSc2d1	odborná
vyspělosti	vyspělost	k1gFnSc2	vyspělost
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
naprosto	naprosto	k6eAd1	naprosto
rovnocenná	rovnocenný	k2eAgFnSc1d1	rovnocenná
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
zavedení	zavedení	k1gNnSc1	zavedení
titulů-kvalifikací	titulůvalifikace	k1gFnPc2	titulů-kvalifikace
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
posílení	posílení	k1gNnSc2	posílení
stranické	stranický	k2eAgFnSc2d1	stranická
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
kádrovou	kádrový	k2eAgFnSc7d1	kádrová
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
též	též	k9	též
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
jmenování	jmenování	k1gNnSc4	jmenování
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
ani	ani	k8xC	ani
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
termínu	termín	k1gInSc6	termín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
profesoři	profesor	k1gMnPc1	profesor
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
nevešli	vejít	k5eNaPmAgMnP	vejít
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
auly	aula	k1gFnSc2	aula
Karolina	Karolinum	k1gNnSc2	Karolinum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
promoce	promoce	k1gFnSc1	promoce
koná	konat	k5eAaImIp3nS	konat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Například	například	k6eAd1	například
Pavel	Pavel	k1gMnSc1	Pavel
Mertlík	Mertlík	k1gInSc4	Mertlík
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesor	profesor	k1gMnSc1	profesor
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
doživotní	doživotní	k2eAgInSc1d1	doživotní
titul	titul	k1gInSc1	titul
udělovaný	udělovaný	k2eAgInSc1d1	udělovaný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
funkční	funkční	k2eAgNnSc4d1	funkční
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Biochemik	biochemik	k1gMnSc1	biochemik
Jan	Jan	k1gMnSc1	Jan
Konvalinka	Konvalinka	k1gMnSc1	Konvalinka
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Současný	současný	k2eAgInSc1d1	současný
systém	systém	k1gInSc1	systém
habilitace-profesura	habilitacerofesur	k1gMnSc2	habilitace-profesur
považuji	považovat	k5eAaImIp1nS	považovat
za	za	k7c4	za
umělou	umělý	k2eAgFnSc4d1	umělá
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yIgFnSc2	který
si	se	k3xPyFc3	se
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
bráníme	bránit	k5eAaImIp1nP	bránit
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
před	před	k7c7	před
konkurencí	konkurence	k1gFnSc7	konkurence
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Uváděné	uváděný	k2eAgInPc4d1	uváděný
doživotní	doživotní	k2eAgInPc4d1	doživotní
profesorské	profesorský	k2eAgInPc4d1	profesorský
tituly	titul	k1gInPc4	titul
např.	např.	kA	např.
již	již	k9	již
měla	mít	k5eAaImAgFnS	mít
zamýšlet	zamýšlet	k5eAaImF	zamýšlet
zrušit	zrušit	k5eAaPmF	zrušit
Vláda	vláda	k1gFnSc1	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
<g/>
Obdobný	obdobný	k2eAgInSc1d1	obdobný
systém	systém	k1gInSc1	systém
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
též	též	k9	též
u	u	k7c2	u
nižší	nízký	k2eAgFnSc2d2	nižší
hodnosti	hodnost	k1gFnSc2	hodnost
–	–	k?	–
docenta	docent	k1gMnSc2	docent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
de	de	k?	de
facto	facto	k1gNnSc1	facto
stále	stále	k6eAd1	stále
brán	brán	k2eAgMnSc1d1	brán
jako	jako	k8xC	jako
titul	titul	k1gInSc1	titul
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
rektor	rektor	k1gMnSc1	rektor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Bek	bek	k1gMnSc1	bek
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
rozhovorů	rozhovor	k1gInPc2	rozhovor
pro	pro	k7c4	pro
Mladou	mladý	k2eAgFnSc4d1	mladá
frontu	fronta	k1gFnSc4	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
situaci	situace	k1gFnSc6	situace
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
příznivcem	příznivec	k1gMnSc7	příznivec
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
profesory	profesor	k1gMnPc7	profesor
ustanovovaly	ustanovovat	k5eAaImAgFnP	ustanovovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
je	on	k3xPp3gFnPc4	on
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
ani	ani	k8xC	ani
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
samy	sám	k3xTgFnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgFnSc1d1	klíčová
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
úloha	úloha	k1gFnSc1	úloha
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
rektor	rektor	k1gMnSc1	rektor
přítomen	přítomen	k2eAgMnSc1d1	přítomen
jen	jen	k9	jen
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Současné	současný	k2eAgNnSc1d1	současné
uspořádaní	uspořádaný	k2eAgMnPc1d1	uspořádaný
[	[	kIx(	[
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
přežitkem	přežitek	k1gInSc7	přežitek
poměrně	poměrně	k6eAd1	poměrně
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
profesoři	profesor	k1gMnPc1	profesor
byli	být	k5eAaImAgMnP	být
vlastně	vlastně	k9	vlastně
státními	státní	k2eAgMnPc7d1	státní
úředníky	úředník	k1gMnPc7	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Univerzity	univerzita	k1gFnPc1	univerzita
tehdy	tehdy	k6eAd1	tehdy
nebyly	být	k5eNaImAgFnP	být
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
právními	právní	k2eAgFnPc7d1	právní
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
státního	státní	k2eAgMnSc2d1	státní
úředníka	úředník	k1gMnSc2	úředník
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
logické	logický	k2eAgNnSc1d1	logické
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
jmenování	jmenování	k1gNnSc1	jmenování
je	být	k5eAaImIp3nS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
prezidentem	prezident	k1gMnSc7	prezident
jako	jako	k8xS	jako
představitelem	představitel	k1gMnSc7	představitel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
univerzity	univerzita	k1gFnSc2	univerzita
samostatné	samostatný	k2eAgFnSc2d1	samostatná
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
mimo	mimo	k7c4	mimo
právnickou	právnický	k2eAgFnSc4d1	právnická
osobu	osoba	k1gFnSc4	osoba
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
její	její	k3xOp3gMnPc4	její
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
pracovním	pracovní	k2eAgNnSc7d1	pracovní
zařazením	zařazení	k1gNnSc7	zařazení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nepovažuji	považovat	k5eNaImIp1nS	považovat
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
logické	logický	k2eAgInPc4d1	logický
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
generály	generál	k1gMnPc4	generál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
u	u	k7c2	u
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
prezident	prezident	k1gMnSc1	prezident
není	být	k5eNaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
můj	můj	k3xOp1gInSc1	můj
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
určitě	určitě	k6eAd1	určitě
menšinový	menšinový	k2eAgMnSc1d1	menšinový
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
není	být	k5eNaImIp3nS	být
osamocený	osamocený	k2eAgMnSc1d1	osamocený
<g/>
.	.	kIx.	.
</s>
<s>
Zakládá	zakládat	k5eAaImIp3nS	zakládat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
zkušenosti	zkušenost	k1gFnPc4	zkušenost
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
systém	systém	k1gInSc1	systém
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
např.	např.	kA	např.
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
doživotní	doživotní	k2eAgInSc4d1	doživotní
<g />
.	.	kIx.	.
</s>
<s>
kvaziaristokratické	kvaziaristokratický	k2eAgInPc1d1	kvaziaristokratický
tituly	titul	k1gInPc1	titul
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
docent	docent	k1gMnSc1	docent
<g/>
/	/	kIx~	/
<g/>
profesor	profesor	k1gMnSc1	profesor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
pracovní	pracovní	k2eAgFnPc4d1	pracovní
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgFnPc4d1	funkční
<g/>
)	)	kIx)	)
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
proběhlé	proběhlý	k2eAgFnSc2d1	proběhlá
reformy	reforma	k1gFnSc2	reforma
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
137	[number]	k4	137
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uvedené	uvedený	k2eAgNnSc1d1	uvedené
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zda	zda	k8xS	zda
profesory	profesor	k1gMnPc4	profesor
bude	být	k5eAaImBp3nS	být
dál	daleko	k6eAd2	daleko
jmenovat	jmenovat	k5eAaImF	jmenovat
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
či	či	k8xC	či
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
samotné	samotný	k2eAgFnPc1d1	samotná
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
)	)	kIx)	)
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reformy	reforma	k1gFnSc2	reforma
uvedl	uvést	k5eAaPmAgInS	uvést
k	k	k7c3	k
nastavení	nastavení	k1gNnSc3	nastavení
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
principů	princip	k1gInPc2	princip
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
též	též	k9	též
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
vysoké	vysoký	k2eAgNnSc1d1	vysoké
školství	školství	k1gNnSc1	školství
[	[	kIx(	[
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
]	]	kIx)	]
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
ani	ani	k8xC	ani
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
ani	ani	k8xC	ani
vědu	věda	k1gFnSc4	věda
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
školy	škola	k1gFnPc1	škola
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
něj	on	k3xPp3gMnSc4	on
usilují	usilovat	k5eAaImIp3nP	usilovat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
růst	růst	k1gInSc4	růst
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
titulů	titul	k1gInPc2	titul
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
<g/>
Václav	Václav	k1gMnSc1	Václav
Bělohradský	Bělohradský	k1gMnSc1	Bělohradský
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
rozhovorů	rozhovor	k1gInPc2	rozhovor
k	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
nastavení	nastavení	k1gNnSc3	nastavení
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
univerzity	univerzita	k1gFnPc1	univerzita
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
odtržené	odtržený	k2eAgInPc1d1	odtržený
od	od	k7c2	od
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
od	od	k7c2	od
případných	případný	k2eAgMnPc2d1	případný
čtenářů	čtenář	k1gMnPc2	čtenář
z	z	k7c2	z
akademických	akademický	k2eAgFnPc2d1	akademická
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Funguje	fungovat	k5eAaImIp3nS	fungovat
'	'	kIx"	'
<g/>
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
škvár	škvár	k1gInSc1	škvár
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
dostanete	dostat	k5eAaPmIp2nP	dostat
nějaký	nějaký	k3yIgInSc4	nějaký
grant	grant	k1gInSc4	grant
–	–	k?	–
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	on	k3xPp3gInPc4	on
odnesete	odnést	k5eAaPmIp2nP	odnést
do	do	k7c2	do
sklepa	sklep	k1gInSc2	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
nastaven	nastavit	k5eAaPmNgInS	nastavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
produkuje	produkovat	k5eAaImIp3nS	produkovat
texty	text	k1gInPc4	text
bez	bez	k7c2	bez
čtenářů	čtenář	k1gMnPc2	čtenář
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc4d1	základní
podmínku	podmínka	k1gFnSc4	podmínka
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohledem	pohled	k1gInSc7	pohled
zvenku	zvenku	k6eAd1	zvenku
jednou	jeden	k4xCgFnSc7	jeden
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
blbosti	blbost	k1gFnPc1	blbost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
Rudolf	Rudolf	k1gMnSc1	Rudolf
Haňka	Haňka	k1gMnSc1	Haňka
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
profesoři	profesor	k1gMnPc1	profesor
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
jejími	její	k3xOp3gMnPc7	její
profesory	profesor	k1gMnPc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přežitek	přežitek	k1gInSc4	přežitek
rakousko-uherského	rakouskoherský	k2eAgInSc2d1	rakousko-uherský
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
profesory	profesor	k1gMnPc4	profesor
vždy	vždy	k6eAd1	vždy
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
císař	císař	k1gMnSc1	císař
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
profesora	profesor	k1gMnSc4	profesor
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
profesora	profesor	k1gMnSc4	profesor
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc4	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
profesory	profesor	k1gMnPc7	profesor
podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
začal	začít	k5eAaPmAgMnS	začít
jmenovat	jmenovat	k5eAaImF	jmenovat
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zákonu	zákon	k1gInSc6	zákon
již	již	k6eAd1	již
stálo	stát	k5eAaImAgNnS	stát
<g/>
:	:	kIx,	:
na	na	k7c6	na
doporučení	doporučení	k1gNnSc6	doporučení
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
a	a	k8xC	a
1953	[number]	k4	1953
ministr	ministr	k1gMnSc1	ministr
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
koncept	koncept	k1gInSc4	koncept
státního	státní	k2eAgMnSc2d1	státní
docenta	docent	k1gMnSc2	docent
a	a	k8xC	a
státního	státní	k2eAgMnSc2d1	státní
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
spíše	spíše	k9	spíše
odměňovat	odměňovat	k5eAaImF	odměňovat
věrné	věrný	k2eAgMnPc4d1	věrný
straníky	straník	k1gMnPc4	straník
<g/>
,	,	kIx,	,
dát	dát	k5eAaPmF	dát
jim	on	k3xPp3gFnPc3	on
nějaký	nějaký	k3yIgInSc4	nějaký
ten	ten	k3xDgInSc4	ten
zlatý	zlatý	k2eAgInSc4d1	zlatý
řetěz	řetěz	k1gInSc4	řetěz
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ten	ten	k3xDgInSc1	ten
systém	systém	k1gInSc1	systém
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
modernímu	moderní	k2eAgNnSc3d1	moderní
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byli	být	k5eAaImAgMnP	být
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
profesoři	profesor	k1gMnPc1	profesor
průměrně	průměrně	k6eAd1	průměrně
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
63	[number]	k4	63
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
51	[number]	k4	51
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
mladých	mladý	k2eAgMnPc2d1	mladý
profesorů	profesor	k1gMnPc2	profesor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
IT	IT	kA	IT
<g/>
;	;	kIx,	;
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
věkem	věk	k1gInSc7	věk
profesorů	profesor	k1gMnPc2	profesor
začíná	začínat	k5eAaImIp3nS	začínat
Česko	Česko	k1gNnSc1	Česko
dohánět	dohánět	k5eAaImF	dohánět
zahraničí	zahraničí	k1gNnSc4	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dosud	dosud	k6eAd1	dosud
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Martin	Martin	k1gMnSc1	Martin
Zatloukal	Zatloukal	k1gMnSc1	Zatloukal
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
DSc	DSc	k1gFnSc1	DSc
<g/>
.	.	kIx.	.
–	–	k?	–
v	v	k7c6	v
32	[number]	k4	32
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
termínů	termín	k1gInPc2	termín
jmenování	jmenování	k1gNnSc4	jmenování
profesorů	profesor	k1gMnPc2	profesor
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
53	[number]	k4	53
kandidátů	kandidát	k1gMnPc2	kandidát
9	[number]	k4	9
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
nejstaršímu	starý	k2eAgMnSc3d3	nejstarší
kandidátovi	kandidát	k1gMnSc3	kandidát
bylo	být	k5eAaImAgNnS	být
73	[number]	k4	73
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
bylo	být	k5eAaImAgNnS	být
pět	pět	k4xCc1	pět
čtyřicátníků	čtyřicátník	k1gMnPc2	čtyřicátník
–	–	k?	–
nejvíce	hodně	k6eAd3	hodně
kandidátů	kandidát	k1gMnPc2	kandidát
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
UK	UK	kA	UK
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
VUT	VUT	kA	VUT
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
VŠB-TUO	VŠB-TUO	k1gMnSc1	VŠB-TUO
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
návrhy	návrh	k1gInPc4	návrh
převažovaly	převažovat	k5eAaImAgFnP	převažovat
osobnosti	osobnost	k1gFnPc1	osobnost
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
a	a	k8xC	a
lékařství	lékařství	k1gNnSc1	lékařství
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
dekret	dekret	k1gInSc4	dekret
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
nově	nově	k6eAd1	nově
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
profesorům	profesor	k1gMnPc3	profesor
a	a	k8xC	a
profesorkám	profesorka	k1gFnPc3	profesorka
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
předávání	předávání	k1gNnSc1	předávání
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
též	též	k9	též
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
;	;	kIx,	;
historicky	historicky	k6eAd1	historicky
jmenování	jmenování	k1gNnSc4	jmenování
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
pražského	pražský	k2eAgNnSc2d1	Pražské
Karolina	Karolinum	k1gNnSc2	Karolinum
při	při	k7c6	při
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
akademickém	akademický	k2eAgInSc6d1	akademický
obřadu	obřad	k1gInSc6	obřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
akademickými	akademický	k2eAgMnPc7d1	akademický
pracovníky	pracovník	k1gMnPc7	pracovník
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
34	[number]	k4	34
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
navíc	navíc	k6eAd1	navíc
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vyšších	vysoký	k2eAgInPc2d2	vyšší
titulů	titul	k1gInPc2	titul
–	–	k?	–
mezi	mezi	k7c7	mezi
docenty	docent	k1gMnPc7	docent
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
26	[number]	k4	26
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
%	%	kIx~	%
profesorů	profesor	k1gMnPc2	profesor
jsou	být	k5eAaImIp3nP	být
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
výdělek	výdělek	k1gInSc1	výdělek
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
pedagogů	pedagog	k1gMnPc2	pedagog
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
podle	podle	k7c2	podle
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
33	[number]	k4	33
500	[number]	k4	500
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
brali	brát	k5eAaImAgMnP	brát
odborní	odborný	k2eAgMnPc1d1	odborný
asistenti	asistent	k1gMnPc1	asistent
mladší	mladý	k2eAgMnPc1d2	mladší
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
profesoři	profesor	k1gMnPc1	profesor
měli	mít	k5eAaImAgMnP	mít
průměrně	průměrně	k6eAd1	průměrně
přes	přes	k7c4	přes
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
působilo	působit	k5eAaImAgNnS	působit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
profesorek	profesorka	k1gFnPc2	profesorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
každoročně	každoročně	k6eAd1	každoročně
jmenováno	jmenovat	k5eAaBmNgNnS	jmenovat
kolem	kolem	k7c2	kolem
150	[number]	k4	150
nových	nový	k2eAgFnPc2d1	nová
profesorek	profesorka	k1gFnPc2	profesorka
a	a	k8xC	a
profesorů	profesor	k1gMnPc2	profesor
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
působilo	působit	k5eAaImAgNnS	působit
zhruba	zhruba	k6eAd1	zhruba
1900	[number]	k4	1900
profesorů	profesor	k1gMnPc2	profesor
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
pracovním	pracovní	k2eAgInSc6d1	pracovní
úvazku	úvazek	k1gInSc6	úvazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
UK	UK	kA	UK
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
působilo	působit	k5eAaImAgNnS	působit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
asi	asi	k9	asi
500	[number]	k4	500
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
profesorek	profesorka	k1gFnPc2	profesorka
<g/>
,	,	kIx,	,
na	na	k7c6	na
MU	MU	kA	MU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žen-profesorek	Ženrofesorek	k1gInSc1	Žen-profesorek
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
,	,	kIx,	,
statisticky	statisticky	k6eAd1	statisticky
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ČR	ČR	kA	ČR
kupříkladu	kupříkladu	k6eAd1	kupříkladu
ke	k	k7c3	k
skandinávským	skandinávský	k2eAgFnPc3d1	skandinávská
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
pozice	pozice	k1gFnSc2	pozice
profesora	profesor	k1gMnSc2	profesor
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
jsou	být	k5eAaImIp3nP	být
důležití	důležitý	k2eAgMnPc1d1	důležitý
jako	jako	k8xC	jako
garanti	garant	k1gMnPc1	garant
při	při	k7c6	při
akreditaci	akreditace	k1gFnSc6	akreditace
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
pří	přít	k5eAaImIp3nP	přít
jmenování	jmenování	k1gNnSc4	jmenování
v	v	k7c6	v
jarním	jarní	k2eAgInSc6d1	jarní
termínu	termín	k1gInSc6	termín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
nejvíc	hodně	k6eAd3	hodně
nových	nový	k2eAgMnPc2d1	nový
profesorů	profesor	k1gMnPc2	profesor
tradičně	tradičně	k6eAd1	tradičně
UK	UK	kA	UK
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
JU	ju	k0	ju
<g/>
,	,	kIx,	,
MU	MU	kA	MU
<g/>
,	,	kIx,	,
MENDELU	Mendel	k1gMnSc3	Mendel
<g/>
,	,	kIx,	,
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
UP	UP	kA	UP
(	(	kIx(	(
<g/>
shodně	shodně	k6eAd1	shodně
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
novými	nový	k2eAgMnPc7d1	nový
profesory	profesor	k1gMnPc7	profesor
10	[number]	k4	10
žen	žena	k1gFnPc2	žena
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
64	[number]	k4	64
jmenovaných	jmenovaná	k1gFnPc2	jmenovaná
<g/>
;	;	kIx,	;
s	s	k7c7	s
19	[number]	k4	19
novými	nový	k2eAgMnPc7d1	nový
profesory	profesor	k1gMnPc7	profesor
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgNnSc4d3	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
tradičně	tradičně	k6eAd1	tradičně
UK	UK	kA	UK
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MU	MU	kA	MU
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
VUT	VUT	kA	VUT
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvíce	hodně	k6eAd3	hodně
byly	být	k5eAaImAgInP	být
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
obory	obor	k1gInPc1	obor
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reformy	reforma	k1gFnSc2	reforma
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
profesory	profesor	k1gMnPc4	profesor
dál	daleko	k6eAd2	daleko
jmenovat	jmenovat	k5eAaImF	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
příp	příp	kA	příp
<g/>
.	.	kIx.	.
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
či	či	k8xC	či
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
to	ten	k3xDgNnSc1	ten
budou	být	k5eAaImBp3nP	být
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
u	u	k7c2	u
docentů	docent	k1gMnPc2	docent
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgFnPc1d1	samotná
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
)	)	kIx)	)
–	–	k?	–
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
byl	být	k5eAaImAgInS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
výdělek	výdělek	k1gInSc1	výdělek
profesora	profesor	k1gMnSc2	profesor
61	[number]	k4	61
854	[number]	k4	854
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
docenta	docent	k1gMnSc2	docent
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
44	[number]	k4	44
268	[number]	k4	268
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
odborného	odborný	k2eAgMnSc4d1	odborný
asistenta	asistent	k1gMnSc4	asistent
32	[number]	k4	32
601	[number]	k4	601
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
asistenta	asistent	k1gMnSc2	asistent
23	[number]	k4	23
964	[number]	k4	964
Kč	Kč	kA	Kč
a	a	k8xC	a
lektora	lektor	k1gMnSc2	lektor
19	[number]	k4	19
260	[number]	k4	260
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
podnikohospodářské	podnikohospodářský	k2eAgFnSc2d1	podnikohospodářská
VŠE	všechen	k3xTgNnSc4	všechen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
průměrný	průměrný	k2eAgInSc1d1	průměrný
výdělek	výdělek	k1gInSc1	výdělek
profesora	profesor	k1gMnSc2	profesor
78	[number]	k4	78
178	[number]	k4	178
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
docenta	docent	k1gMnSc2	docent
pak	pak	k8xC	pak
pouze	pouze	k6eAd1	pouze
43	[number]	k4	43
218	[number]	k4	218
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
vydělal	vydělat	k5eAaPmAgInS	vydělat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
český	český	k2eAgMnSc1d1	český
akademický	akademický	k2eAgMnSc1d1	akademický
pracovník	pracovník	k1gMnSc1	pracovník
na	na	k7c6	na
veřejné	veřejný	k2eAgFnSc6d1	veřejná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
36	[number]	k4	36
636	[number]	k4	636
Kč	Kč	kA	Kč
hrubého	hrubý	k2eAgInSc2d1	hrubý
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nadtarifů	nadtarif	k1gInPc2	nadtarif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesoři	profesor	k1gMnPc1	profesor
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
na	na	k7c4	na
průměr	průměr	k1gInSc4	průměr
60	[number]	k4	60
098	[number]	k4	098
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
docenti	docent	k1gMnPc1	docent
na	na	k7c6	na
45	[number]	k4	45
551	[number]	k4	551
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
odborní	odborný	k2eAgMnPc1d1	odborný
asistenti	asistent	k1gMnPc1	asistent
pak	pak	k6eAd1	pak
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c4	na
31	[number]	k4	31
041	[number]	k4	041
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedení	vedení	k1gNnSc1	vedení
MŠMT	MŠMT	kA	MŠMT
uvádí	uvádět	k5eAaImIp3nS	uvádět
ten	ten	k3xDgInSc4	ten
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
nemohou	moct	k5eNaImIp3nP	moct
mít	mít	k5eAaImF	mít
z	z	k7c2	z
principu	princip	k1gInSc2	princip
vysoké	vysoký	k2eAgInPc1d1	vysoký
výdělky	výdělek	k1gInPc1	výdělek
profesorů	profesor	k1gMnPc2	profesor
(	(	kIx(	(
<g/>
i	i	k9	i
docentů	docent	k1gMnPc2	docent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
hodně	hodně	k6eAd1	hodně
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc1	peníz
dostávají	dostávat	k5eAaImIp3nP	dostávat
podle	podle	k7c2	podle
nízkého	nízký	k2eAgInSc2d1	nízký
koeficientu	koeficient	k1gInSc2	koeficient
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
omezené	omezený	k2eAgFnPc4d1	omezená
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
spoluprací	spolupráce	k1gFnSc7	spolupráce
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2004	[number]	k4	2004
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c4	v
ČR	ČR	kA	ČR
bezkonkurenční	bezkonkurenční	k2eAgNnSc1d1	bezkonkurenční
prvenství	prvenství	k1gNnSc1	prvenství
v	v	k7c6	v
ohodnocení	ohodnocení	k1gNnSc6	ohodnocení
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
pedagogů	pedagog	k1gMnPc2	pedagog
VŠCHT	VŠCHT	kA	VŠCHT
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
přes	přes	k7c4	přes
34	[number]	k4	34
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
celkem	celkem	k6eAd1	celkem
1550	[number]	k4	1550
profesorů	profesor	k1gMnPc2	profesor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
označilo	označit	k5eAaPmAgNnS	označit
Česko	Česko	k1gNnSc1	Česko
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Zemi	zem	k1gFnSc4	zem
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dle	dle	k7c2	dle
nich	on	k3xPp3gFnPc2	on
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
Česko	Česko	k1gNnSc1	Česko
odhadem	odhad	k1gInSc7	odhad
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
víc	hodně	k6eAd2	hodně
profesorů	profesor	k1gMnPc2	profesor
než	než	k8xS	než
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
nebo	nebo	k8xC	nebo
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
víc	hodně	k6eAd2	hodně
profesorů	profesor	k1gMnPc2	profesor
než	než	k8xS	než
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Učitelé	učitel	k1gMnPc1	učitel
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
pobírali	pobírat	k5eAaImAgMnP	pobírat
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
výplaty	výplata	k1gFnPc4	výplata
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
7196	[number]	k4	7196
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
(	(	kIx(	(
<g/>
6955	[number]	k4	6955
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
JAR	jar	k1gFnSc1	jar
(	(	kIx(	(
<g/>
6531	[number]	k4	6531
USD	USD	kA	USD
<g/>
)	)	kIx)	)
–	–	k?	–
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
z	z	k7c2	z
28	[number]	k4	28
uváděných	uváděný	k2eAgInPc2d1	uváděný
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
hluboko	hluboko	k6eAd1	hluboko
za	za	k7c4	za
ČR	ČR	kA	ČR
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
platy	plat	k1gInPc1	plat
učitelů	učitel	k1gMnPc2	učitel
například	například	k6eAd1	například
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Profesor	profesor	k1gMnSc1	profesor
techniky	technika	k1gFnSc2	technika
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
i	i	k9	i
na	na	k7c4	na
stotisícové	stotisícový	k2eAgFnPc4d1	stotisícová
výplaty	výplata	k1gFnPc4	výplata
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
docent	docent	k1gMnSc1	docent
humanitní	humanitní	k2eAgFnSc2d1	humanitní
fakulty	fakulta	k1gFnSc2	fakulta
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
něco	něco	k6eAd1	něco
pod	pod	k7c4	pod
třicet	třicet	k4xCc4	třicet
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
USA	USA	kA	USA
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
uvedl	uvést	k5eAaPmAgMnS	uvést
profesor	profesor	k1gMnSc1	profesor
Philip	Philip	k1gMnSc1	Philip
Altbach	Altbach	k1gMnSc1	Altbach
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ani	ani	k8xC	ani
plat	plat	k1gInSc4	plat
profesora	profesor	k1gMnSc2	profesor
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
srovnat	srovnat	k5eAaPmF	srovnat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vám	vy	k3xPp2nPc3	vy
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
trochu	trochu	k6eAd1	trochu
slušná	slušný	k2eAgFnSc1d1	slušná
právnická	právnický	k2eAgFnSc1d1	právnická
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
pracovníci	pracovník	k1gMnPc1	pracovník
<g />
.	.	kIx.	.
</s>
<s>
místní	místní	k2eAgFnSc2d1	místní
školské	školský	k2eAgFnSc2d1	školská
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
USA	USA	kA	USA
si	se	k3xPyFc3	se
vydělají	vydělat	k5eAaPmIp3nP	vydělat
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
učí	učit	k5eAaImIp3nP	učit
<g/>
;	;	kIx,	;
skutečně	skutečně	k6eAd1	skutečně
špičkoví	špičkový	k2eAgMnPc1d1	špičkový
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
součástí	součást	k1gFnSc7	součást
globálního	globální	k2eAgInSc2d1	globální
akademického	akademický	k2eAgInSc2d1	akademický
pracovního	pracovní	k2eAgInSc2d1	pracovní
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
platy	plat	k1gInPc1	plat
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opravdu	opravdu	k6eAd1	opravdu
hodně	hodně	k6eAd1	hodně
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
učitelů	učitel	k1gMnPc2	učitel
to	ten	k3xDgNnSc1	ten
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Vysokoškolští	vysokoškolský	k2eAgMnPc1d1	vysokoškolský
pedagogové	pedagog	k1gMnPc1	pedagog
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
platem	plat	k1gInSc7	plat
(	(	kIx(	(
<g/>
v	v	k7c6	v
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
5313	[number]	k4	5313
USD	USD	kA	USD
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
střední	střední	k2eAgFnSc6d1	střední
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
SRN	SRN	kA	SRN
(	(	kIx(	(
<g/>
5141	[number]	k4	5141
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g/>
4940	[number]	k4	4940
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UK	UK	kA	UK
(	(	kIx(	(
<g/>
5943	[number]	k4	5943
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
(	(	kIx(	(
<g/>
6054	[number]	k4	6054
USD	USD	kA	USD
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vedou	vést	k5eAaImIp3nP	vést
trochu	trochu	k6eAd1	trochu
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
3484	[number]	k4	3484
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
3473	[number]	k4	3473
USD	USD	kA	USD
<g/>
)	)	kIx)	)
příliš	příliš	k6eAd1	příliš
požitků	požitek	k1gInPc2	požitek
vysoce	vysoce	k6eAd1	vysoce
kvalifikovaným	kvalifikovaný	k2eAgMnPc3d1	kvalifikovaný
vysokoškolským	vysokoškolský	k2eAgMnPc3d1	vysokoškolský
učitelům	učitel	k1gMnPc3	učitel
nenabízejí	nabízet	k5eNaImIp3nP	nabízet
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
udávána	udáván	k2eAgFnSc1d1	udávána
hodnota	hodnota	k1gFnSc1	hodnota
2495	[number]	k4	2495
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
profesorů	profesor	k1gMnPc2	profesor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Docent	docent	k1gMnSc1	docent
</s>
</p>
<p>
<s>
Hirschův	Hirschův	k2eAgInSc1d1	Hirschův
index	index	k1gInSc1	index
</s>
</p>
<p>
<s>
Impakt	Impakt	k1gInSc1	Impakt
faktor	faktor	k1gInSc1	faktor
</s>
</p>
<p>
<s>
RIV	RIV	kA	RIV
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
of	of	k?	of
Science	Science	k1gFnSc1	Science
</s>
</p>
<p>
<s>
Scopus	Scopus	k1gInSc1	Scopus
(	(	kIx(	(
<g/>
databáze	databáze	k1gFnSc1	databáze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grantová	grantový	k2eAgFnSc1d1	Grantová
agentura	agentura	k1gFnSc1	agentura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Technologická	technologický	k2eAgFnSc1d1	technologická
agentura	agentura	k1gFnSc1	agentura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
profesor	profesor	k1gMnSc1	profesor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Professor	Professora	k1gFnPc2	Professora
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
profesor	profesor	k1gMnSc1	profesor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Akreditované	akreditovaný	k2eAgInPc1d1	akreditovaný
obory	obor	k1gInPc1	obor
habilitačního	habilitační	k2eAgNnSc2d1	habilitační
a	a	k8xC	a
jmenovacího	jmenovací	k2eAgNnSc2d1	jmenovací
řízení	řízení	k1gNnSc2	řízení
–	–	k?	–
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jmenovacích	jmenovací	k2eAgNnPc2d1	jmenovací
řízení	řízení	k1gNnSc2	řízení
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
profesorů	profesor	k1gMnPc2	profesor
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
–	–	k?	–
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
profesorů	profesor	k1gMnPc2	profesor
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
zkompilovaný	zkompilovaný	k2eAgMnSc1d1	zkompilovaný
a	a	k8xC	a
s	s	k7c7	s
odstraněnými	odstraněný	k2eAgFnPc7d1	odstraněná
chybami	chyba	k1gFnPc7	chyba
<g/>
,	,	kIx,	,
se	s	k7c7	s
statistikami	statistika	k1gFnPc7	statistika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
profesorů	profesor	k1gMnPc2	profesor
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993-2002	[number]	k4	1993-2002
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
profesorů	profesor	k1gMnPc2	profesor
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003-2012	[number]	k4	2003-2012
</s>
</p>
