<p>
<s>
Mauricius	Mauricius	k1gInSc1	Mauricius
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Île	Île	k1gFnSc6	Île
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
,	,	kIx,	,
kreolsky	kreolsky	k6eAd1	kreolsky
Moris	Moris	k1gFnSc1	Moris
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Mauritius	Mauritius	k1gInSc4	Mauritius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sopečný	sopečný	k2eAgInSc4d1	sopečný
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
Maskarénského	Maskarénský	k2eAgNnSc2d1	Maskarénský
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Mauricius	Mauricius	k1gInSc1	Mauricius
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
Mauricijské	Mauricijský	k2eAgFnSc2d1	Mauricijská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
její	její	k3xOp3gNnSc4	její
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Port	porta	k1gFnPc2	porta
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Mauricius	Mauricius	k1gInSc1	Mauricius
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
korálovými	korálový	k2eAgInPc7d1	korálový
útesy	útes	k1gInPc7	útes
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
malých	malý	k2eAgInPc2d1	malý
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
větší	veliký	k2eAgFnSc7d2	veliký
pevninou	pevnina	k1gFnSc7	pevnina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
zámořský	zámořský	k2eAgInSc1d1	zámořský
departement	departement	k1gInSc1	departement
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Mauricia	Mauricius	k1gMnSc2	Mauricius
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
172	[number]	k4	172
km	km	kA	km
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
území	území	k1gNnSc6	území
Mauricijské	Mauricijský	k2eAgFnSc2d1	Mauricijská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
asi	asi	k9	asi
400	[number]	k4	400
km	km	kA	km
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
metropole	metropol	k1gFnSc2	metropol
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Mauricius	Mauricius	k1gMnSc1	Mauricius
leží	ležet	k5eAaImIp3nS	ležet
korálový	korálový	k2eAgInSc4d1	korálový
atol	atol	k1gInSc4	atol
Cargados	Cargados	k1gMnSc1	Cargados
Carajos	Carajos	k1gMnSc1	Carajos
(	(	kIx(	(
<g/>
Saint	Saint	k1gMnSc1	Saint
Brandon	Brandon	k1gMnSc1	Brandon
<g/>
)	)	kIx)	)
a	a	k8xC	a
560	[number]	k4	560
km	km	kA	km
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
ostrov	ostrov	k1gInSc4	ostrov
Rodrigues	Rodriguesa	k1gFnPc2	Rodriguesa
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
Agaléga	Agalég	k1gMnSc2	Agalég
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnSc7	součást
Mauricijské	Mauricijský	k2eAgFnSc2d1	Mauricijská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Mauricia	Mauricium	k1gNnSc2	Mauricium
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1064	[number]	k4	1064
km	km	kA	km
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
1122	[number]	k4	1122
km	km	kA	km
)	)	kIx)	)
tedy	tedy	k9	tedy
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
než	než	k8xS	než
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
868	[number]	k4	868
km	km	kA	km
od	od	k7c2	od
Mauricia	Mauricium	k1gNnSc2	Mauricium
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnPc4	geologie
a	a	k8xC	a
geomorfologie	geomorfologie	k1gFnPc4	geomorfologie
===	===	k?	===
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
15	[number]	k4	15
až	až	k8xS	až
8	[number]	k4	8
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mauriciu	Mauricium	k1gNnSc6	Mauricium
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stopy	stopa	k1gFnPc4	stopa
zdejší	zdejší	k2eAgFnSc2d1	zdejší
sopečné	sopečný	k2eAgFnSc2d1	sopečná
minulosti	minulost	k1gFnSc2	minulost
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
velké	velký	k2eAgInPc1d1	velký
kaldery	kalder	k1gInPc1	kalder
<g/>
,	,	kIx,	,
tvary	tvar	k1gInPc1	tvar
místních	místní	k2eAgInPc2d1	místní
horských	horský	k2eAgInPc2d1	horský
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
skalních	skalní	k2eAgInPc2d1	skalní
suků	suk	k1gInPc2	suk
či	či	k8xC	či
věží	věž	k1gFnPc2	věž
nebo	nebo	k8xC	nebo
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
sopečný	sopečný	k2eAgInSc1d1	sopečný
kráter	kráter	k1gInSc1	kráter
Trou-aux-Cerfs	Trouux-Cerfs	k1gInSc1	Trou-aux-Cerfs
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
přímo	přímo	k6eAd1	přímo
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
Curepipe	Curepip	k1gInSc5	Curepip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Mauricia	Mauricium	k1gNnSc2	Mauricium
je	být	k5eAaImIp3nS	být
Piton	Piton	k1gNnSc4	Piton
de	de	k?	de
la	la	k1gNnSc2	la
Petite	petit	k1gInSc5	petit
Riviè	Riviè	k1gMnSc5	Riviè
Noire	Noir	k1gMnSc5	Noir
(	(	kIx(	(
<g/>
828	[number]	k4	828
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
uprostřed	uprostřed	k7c2	uprostřed
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Black	Black	k1gMnSc1	Black
River	River	k1gMnSc1	River
Gorges	Gorges	k1gMnSc1	Gorges
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
ostatních	ostatní	k2eAgInPc2d1	ostatní
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Mount	Mounta	k1gFnPc2	Mounta
Cocotte	Cocott	k1gInSc5	Cocott
(	(	kIx(	(
<g/>
780	[number]	k4	780
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhé	druhý	k4xOgFnSc2	druhý
dva	dva	k4xCgInPc4	dva
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
vrcholy	vrchol	k1gInPc4	vrchol
ostrova	ostrov	k1gInSc2	ostrov
Le	Le	k1gFnSc6	Le
Pouce	pouka	k1gFnSc6	pouka
a	a	k8xC	a
Pieter	Pieter	k1gMnSc1	Pieter
Both	Both	k1gMnSc1	Both
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
shodně	shodně	k6eAd1	shodně
820	[number]	k4	820
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
kilometrů	kilometr	k1gInPc2	kilometr
severněji	severně	k6eAd2	severně
poblíž	poblíž	k7c2	poblíž
metropole	metropol	k1gFnSc2	metropol
Port	porta	k1gFnPc2	porta
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc4d1	centrální
náhorní	náhorní	k2eAgFnSc4d1	náhorní
plošinu	plošina	k1gFnSc4	plošina
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
nížinaté	nížinatý	k2eAgNnSc1d1	nížinaté
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
lemované	lemovaný	k2eAgNnSc1d1	lemované
v	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
vodách	voda	k1gFnPc6	voda
korálovými	korálový	k2eAgInPc7d1	korálový
útesy	útes	k1gInPc7	útes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
na	na	k7c6	na
Mauriciu	Mauricium	k1gNnSc6	Mauricium
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
-	-	kIx~	-
vlhké	vlhký	k2eAgNnSc4d1	vlhké
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
stabilními	stabilní	k2eAgFnPc7d1	stabilní
teplotami	teplota	k1gFnPc7	teplota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
20	[number]	k4	20
(	(	kIx(	(
<g/>
minima	minimum	k1gNnSc2	minimum
<g/>
)	)	kIx)	)
až	až	k9	až
30	[number]	k4	30
(	(	kIx(	(
<g/>
maxima	maximum	k1gNnSc2	maximum
<g/>
)	)	kIx)	)
°	°	k?	°
<g/>
C.	C.	kA	C.
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Mauritius	Mauritius	k1gInSc1	Mauritius
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
nižší	nízký	k2eAgFnPc1d2	nižší
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
měsících	měsíc	k1gInPc6	měsíc
denní	denní	k2eAgFnSc1d1	denní
maxima	maxima	k1gFnSc1	maxima
zpravidla	zpravidla	k6eAd1	zpravidla
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c7	pod
26	[number]	k4	26
-	-	kIx~	-
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Relativně	relativně	k6eAd1	relativně
nejdeštivějším	deštivý	k2eAgInSc7d3	nejdeštivější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
únor	únor	k1gInSc1	únor
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc1d1	stabilní
je	být	k5eAaImIp3nS	být
i	i	k9	i
doba	doba	k1gFnSc1	doba
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
celoročně	celoročně	k6eAd1	celoročně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
80	[number]	k4	80
a	a	k8xC	a
85	[number]	k4	85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
na	na	k7c6	na
Mauriciu	Mauricium	k1gNnSc6	Mauricium
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
asi	asi	k9	asi
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
indického	indický	k2eAgMnSc4d1	indický
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nejsou	být	k5eNaImIp3nP	být
konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
Mauriciu	Mauricium	k1gNnSc6	Mauricium
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatel	obyvatel	k1gMnSc1	obyvatel
nezjišťuje	zjišťovat	k5eNaImIp3nS	zjišťovat
jejich	jejich	k3xOp3gInSc4	jejich
etnický	etnický	k2eAgInSc4d1	etnický
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc4	druhý
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
86.5	[number]	k4	86.5
<g/>
%	%	kIx~	%
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozmanitému	rozmanitý	k2eAgInSc3d1	rozmanitý
původu	původ	k1gInSc3	původ
ale	ale	k8xC	ale
používá	používat	k5eAaImIp3nS	používat
mauricijskou	mauricijský	k2eAgFnSc4d1	mauricijská
kreolštinu	kreolština	k1gFnSc4	kreolština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
status	status	k1gInSc4	status
oficiálního	oficiální	k2eAgInSc2d1	oficiální
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Převažujícím	převažující	k2eAgNnSc7d1	převažující
náboženstvím	náboženství	k1gNnSc7	náboženství
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
hinduismus	hinduismus	k1gInSc1	hinduismus
(	(	kIx(	(
<g/>
51,9	[number]	k4	51,9
<g/>
%	%	kIx~	%
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
křesťanství	křesťanství	k1gNnSc1	křesťanství
(	(	kIx(	(
<g/>
31,4	[number]	k4	31,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
15,3	[number]	k4	15,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
představují	představovat	k5eAaImIp3nP	představovat
vyznavači	vyznavač	k1gMnPc1	vyznavač
buddhismu	buddhismus	k1gInSc2	buddhismus
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
četné	četný	k2eAgInPc1d1	četný
přístavy	přístav	k1gInPc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
letiště	letiště	k1gNnSc1	letiště
Port	porta	k1gFnPc2	porta
Luis	Luisa	k1gFnPc2	Luisa
Sir	sir	k1gMnSc1	sir
Seewoosagur	Seewoosagur	k1gMnSc1	Seewoosagur
Rangoolam	Rangoolam	k1gInSc4	Rangoolam
Airport	Airport	k1gInSc4	Airport
(	(	kIx(	(
<g/>
MRU	MRU	k?	MRU
<g/>
/	/	kIx~	/
<g/>
FIMP	FIMP	kA	FIMP
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrova	ostrov	k1gInSc2	ostrov
poblíž	poblíž	k7c2	poblíž
jeho	jeho	k3xOp3gNnSc2	jeho
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnPc1	první
železnice	železnice	k1gFnPc1	železnice
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
druhé	druhý	k4xOgFnSc2	druhý
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
první	první	k4xOgFnSc1	první
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výstavba	výstavba	k1gFnSc1	výstavba
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
mezi	mezi	k7c7	mezi
metropolí	metropol	k1gFnSc7	metropol
Port	port	k1gInSc4	port
Louis	louis	k1gInSc7	louis
a	a	k8xC	a
městem	město	k1gNnSc7	město
Rose	Rose	k1gMnSc1	Rose
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
září	září	k1gNnSc6	září
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
vybudování	vybudování	k1gNnSc1	vybudování
mostu	most	k1gInSc2	most
mezi	mezi	k7c7	mezi
Coromandelem	Coromandel	k1gInSc7	Coromandel
a	a	k8xC	a
Sorè	Sorè	k1gFnSc7	Sorè
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
hotova	hotov	k2eAgFnSc1d1	hotova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
19	[number]	k4	19
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc2d1	moderní
nízkopodlažní	nízkopodlažní	k2eAgFnSc2d1	nízkopodlažní
soupravy	souprava	k1gFnSc2	souprava
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Urbos	Urbos	k1gInSc1	Urbos
100	[number]	k4	100
<g/>
"	"	kIx"	"
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
španělské	španělský	k2eAgFnSc2d1	španělská
firmy	firma	k1gFnSc2	firma
Construcciones	Construcciones	k1gMnSc1	Construcciones
y	y	k?	y
Auxiliar	Auxiliar	k1gMnSc1	Auxiliar
de	de	k?	de
Ferrocarriles	Ferrocarriles	k1gMnSc1	Ferrocarriles
(	(	kIx(	(
<g/>
CAF	CAF	kA	CAF
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgNnSc2d1	nové
dopravního	dopravní	k2eAgNnSc2d1	dopravní
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
přetížeností	přetíženost	k1gFnSc7	přetíženost
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
portlouiské	portlouiský	k2eAgFnSc2d1	portlouiský
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
činit	činit	k5eAaImF	činit
18,8	[number]	k4	18,8
miliard	miliarda	k4xCgFnPc2	miliarda
mauricijských	mauricijský	k2eAgFnPc2d1	mauricijská
rupií	rupie	k1gFnPc2	rupie
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
jízdného	jízdné	k1gNnSc2	jízdné
Metro	metro	k1gNnSc1	metro
Expressem	express	k1gInSc7	express
bude	být	k5eAaImBp3nS	být
odpovídat	odpovídat	k5eAaImF	odpovídat
běžnému	běžný	k2eAgInSc3d1	běžný
tarifu	tarif	k1gInSc3	tarif
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
,	,	kIx,	,
studenty	student	k1gMnPc4	student
a	a	k8xC	a
zdravotně	zdravotně	k6eAd1	zdravotně
postižené	postižený	k2eAgFnPc4d1	postižená
osoby	osoba	k1gFnPc4	osoba
bude	být	k5eAaImBp3nS	být
přeprava	přeprava	k1gFnSc1	přeprava
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
ostrova	ostrov	k1gInSc2	ostrov
Mauricius	Mauricius	k1gInSc1	Mauricius
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Île	Île	k1gMnSc2	Île
Maurice	Maurika	k1gFnSc6	Maurika
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Agaléga	Agaléga	k1gFnSc1	Agaléga
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Maurice	Maurika	k1gFnSc6	Maurika
(	(	kIx(	(
<g/>
pays	pays	k6eAd1	pays
<g/>
)	)	kIx)	)
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
koloniální	koloniální	k2eAgFnSc1d1	koloniální
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
Dronte	Dront	k1gMnSc5	Dront
mauricijský	mauricijský	k2eAgInSc4d1	mauricijský
</s>
</p>
