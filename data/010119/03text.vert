<p>
<s>
Zelda	Zelda	k1gFnSc1	Zelda
Sayre	Sayr	k1gInSc5	Sayr
Fitzgeraldová	Fitzgeraldová	k1gFnSc1	Fitzgeraldová
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Montgomery	Montgomera	k1gFnPc1	Montgomera
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gNnSc1	Alabama
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Asheville	Asheville	k1gFnSc1	Asheville
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Zelda	Zelda	k1gFnSc1	Zelda
Sayreová	Sayreová	k1gFnSc1	Sayreová
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
spisovatele	spisovatel	k1gMnSc2	spisovatel
F.	F.	kA	F.
Scotta	Scott	k1gMnSc2	Scott
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ikonou	ikona	k1gFnSc7	ikona
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
–	–	k?	–
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
ji	on	k3xPp3gFnSc4	on
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc7	první
americkou	americký	k2eAgFnSc7d1	americká
žabkou	žabka	k1gFnSc7	žabka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
jeho	on	k3xPp3gInSc2	on
prvního	první	k4xOgInSc2	první
románu	román	k1gInSc2	román
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
ráje	rája	k1gFnSc2	rája
se	se	k3xPyFc4	se
Fitzgeraldovi	Fitzgeraldův	k2eAgMnPc1d1	Fitzgeraldův
stali	stát	k5eAaPmAgMnP	stát
celebritami	celebrita	k1gFnPc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
viděli	vidět	k5eAaImAgMnP	vidět
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
doby	doba	k1gFnSc2	doba
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
bouřících	bouřící	k2eAgNnPc2d1	bouřící
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
mladí	mladý	k1gMnPc1	mladý
<g/>
,	,	kIx,	,
bohatí	bohatý	k2eAgMnPc1d1	bohatý
<g/>
,	,	kIx,	,
krásní	krásný	k2eAgMnPc1d1	krásný
a	a	k8xC	a
energetičtí	energetický	k2eAgMnPc1d1	energetický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Zelda	Zelda	k1gFnSc1	Zelda
Sayreová	Sayreová	k1gFnSc1	Sayreová
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
bohaté	bohatý	k2eAgFnSc6d1	bohatá
a	a	k8xC	a
pedantské	pedantský	k2eAgFnSc6d1	pedantská
jižanské	jižanský	k2eAgFnSc6d1	jižanská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
už	už	k6eAd1	už
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
drzé	drzý	k2eAgNnSc1d1	drzé
chování	chování	k1gNnSc1	chování
stalo	stát	k5eAaPmAgNnS	stát
předmětem	předmět	k1gInSc7	předmět
montgomerských	montgomerský	k2eAgInPc2d1	montgomerský
drbů	drb	k1gInPc2	drb
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dokončila	dokončit	k5eAaPmAgFnS	dokončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
potkala	potkat	k5eAaPmAgFnS	potkat
při	při	k7c6	při
tanci	tanec	k1gInSc6	tanec
F.	F.	kA	F.
Scotta	Scotta	k1gMnSc1	Scotta
Fitzgeralda	Fitzgeralda	k1gMnSc1	Fitzgeralda
<g/>
.	.	kIx.	.
</s>
<s>
Smršť	smršť	k1gFnSc1	smršť
námluv	námluva	k1gFnPc2	námluva
začala	začít	k5eAaPmAgFnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
veřejně	veřejně	k6eAd1	veřejně
přiznával	přiznávat	k5eAaImAgMnS	přiznávat
své	svůj	k3xOyFgNnSc4	svůj
pobláznění	pobláznění	k1gNnSc4	pobláznění
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
vídání	vídání	k1gNnSc6	vídání
se	se	k3xPyFc4	se
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
boje	boj	k1gInPc4	boj
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
rozchod	rozchod	k1gInSc4	rozchod
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vzali	vzít	k5eAaPmAgMnP	vzít
a	a	k8xC	a
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
desetiletí	desetiletí	k1gNnSc2	desetiletí
strávili	strávit	k5eAaPmAgMnP	strávit
jako	jako	k9	jako
literární	literární	k2eAgFnPc4d1	literární
celebrity	celebrita	k1gFnPc4	celebrita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
známými	známý	k2eAgMnPc7d1	známý
utečenci	utečenec	k1gMnPc7	utečenec
ztracené	ztracený	k2eAgFnSc2d1	ztracená
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Scott	Scott	k1gMnSc1	Scott
obdržel	obdržet	k5eAaPmAgMnS	obdržet
příznivou	příznivý	k2eAgFnSc4d1	příznivá
kritiku	kritika	k1gFnSc4	kritika
za	za	k7c4	za
Velkého	velký	k2eAgMnSc4d1	velký
Gatsbyho	Gatsby	k1gMnSc4	Gatsby
a	a	k8xC	a
povídky	povídka	k1gFnPc4	povídka
a	a	k8xC	a
pár	pár	k4xCyI	pár
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
literárními	literární	k2eAgFnPc7d1	literární
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgMnPc7	jaký
byl	být	k5eAaImAgMnS	být
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
spletí	spleť	k1gFnSc7	spleť
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
,	,	kIx,	,
zášti	zášť	k1gFnSc2	zášť
a	a	k8xC	a
prudkosti	prudkost	k1gFnSc2	prudkost
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
používal	používat	k5eAaImAgMnS	používat
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
materiál	materiál	k1gInSc4	materiál
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
románům	román	k1gInPc3	román
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
bral	brát	k5eAaImAgMnS	brát
útržky	útržek	k1gInPc4	útržek
ze	z	k7c2	z
Zeldina	Zeldin	k2eAgInSc2d1	Zeldin
deníku	deník	k1gInSc2	deník
a	a	k8xC	a
připisoval	připisovat	k5eAaImAgMnS	připisovat
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgFnPc3	svůj
fiktivním	fiktivní	k2eAgFnPc3d1	fiktivní
hrdinkám	hrdinka	k1gFnPc3	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
Zelda	Zelda	k1gFnSc1	Zelda
hledala	hledat	k5eAaImAgFnS	hledat
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
články	článek	k1gInPc4	článek
do	do	k7c2	do
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
27	[number]	k4	27
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
posedlá	posedlý	k2eAgFnSc1d1	posedlá
kariérou	kariéra	k1gFnSc7	kariéra
baleríny	balerína	k1gFnPc4	balerína
a	a	k8xC	a
cvičila	cvičit	k5eAaImAgFnS	cvičit
do	do	k7c2	do
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
bouřlivém	bouřlivý	k2eAgNnSc6d1	bouřlivé
manželství	manželství	k1gNnSc6	manželství
<g/>
,	,	kIx,	,
Scottův	Scottův	k2eAgInSc1d1	Scottův
vzrůstající	vzrůstající	k2eAgInSc4d1	vzrůstající
alkoholismus	alkoholismus	k1gInSc4	alkoholismus
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
nevyrovnanost	nevyrovnanost	k1gFnSc4	nevyrovnanost
byly	být	k5eAaImAgFnP	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
Zeldina	Zeldin	k2eAgInSc2d1	Zeldin
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
sanatoria	sanatorium	k1gNnSc2	sanatorium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
se	s	k7c7	s
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
marylandské	marylandský	k2eAgFnSc6d1	marylandská
klinice	klinika	k1gFnSc6	klinika
napsala	napsat	k5eAaPmAgFnS	napsat
poloautobiografický	poloautobiografický	k2eAgInSc4d1	poloautobiografický
román	román	k1gInSc4	román
Zachraň	zachránit	k5eAaPmRp2nS	zachránit
mi	já	k3xPp1nSc3	já
valčík	valčík	k1gInSc4	valčík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gInSc1	Scott
byl	být	k5eAaImAgInS	být
rozčílený	rozčílený	k2eAgMnSc1d1	rozčílený
<g/>
,	,	kIx,	,
že	že	k8xS	že
použila	použít	k5eAaPmAgFnS	použít
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
on	on	k3xPp3gMnSc1	on
udělal	udělat	k5eAaPmAgMnS	udělat
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
románu	román	k1gInSc6	román
Něžná	něžný	k2eAgFnSc1d1	něžná
je	být	k5eAaImIp3nS	být
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
vydaném	vydaný	k2eAgInSc6d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
romány	román	k1gInPc1	román
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kontrastní	kontrastní	k2eAgInSc4d1	kontrastní
portrét	portrét	k1gInSc4	portrét
rozpadlého	rozpadlý	k2eAgNnSc2d1	rozpadlé
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
odešel	odejít	k5eAaPmAgMnS	odejít
Scott	Scott	k1gMnSc1	Scott
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
psaní	psaní	k1gNnSc4	psaní
scénářů	scénář	k1gInPc2	scénář
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
si	se	k3xPyFc3	se
aférku	aférka	k1gFnSc4	aférka
s	s	k7c7	s
filmovou	filmový	k2eAgFnSc7d1	filmová
sloupkařkou	sloupkařka	k1gFnSc7	sloupkařka
Sheilah	Sheilah	k1gInSc1	Sheilah
Grahamovou	Grahamová	k1gFnSc4	Grahamová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
Zelda	Zelda	k1gFnSc1	Zelda
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Highland	Highlanda	k1gFnPc2	Highlanda
Mental	Mental	k1gMnSc1	Mental
Hospital	Hospital	k1gMnSc1	Hospital
v	v	k7c6	v
Asheville	Ashevilla	k1gFnSc6	Ashevilla
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
naposledy	naposledy	k6eAd1	naposledy
viděl	vidět	k5eAaImAgInS	vidět
Zeldu	Zelda	k1gFnSc4	Zelda
rok	rok	k1gInSc1	rok
a	a	k8xC	a
půl	půl	k1xP	půl
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInSc4d1	zbývající
roky	rok	k1gInPc4	rok
strávila	strávit	k5eAaPmAgFnS	strávit
Zelda	Zelda	k1gFnSc1	Zelda
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
románu	román	k1gInSc6	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nikdy	nikdy	k6eAd1	nikdy
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
také	také	k9	také
malovala	malovat	k5eAaImAgFnS	malovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
postihl	postihnout	k5eAaPmAgInS	postihnout
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
přebývala	přebývat	k5eAaImAgFnS	přebývat
<g/>
,	,	kIx,	,
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Fitzgeraldovy	Fitzgeraldův	k2eAgInPc4d1	Fitzgeraldův
opět	opět	k6eAd1	opět
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
:	:	kIx,	:
pár	pár	k4xCyI	pár
byl	být	k5eAaImAgInS	být
předmětem	předmět	k1gInSc7	předmět
populárních	populární	k2eAgFnPc2d1	populární
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
a	a	k8xC	a
zájmu	zájem	k1gInSc2	zájem
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vzorem	vzor	k1gInSc7	vzor
éry	éra	k1gFnSc2	éra
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
bouřlivých	bouřlivý	k2eAgNnPc2d1	bouřlivé
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ztracené	ztracený	k2eAgFnPc4d1	ztracená
generace	generace	k1gFnPc4	generace
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
Zelda	Zelda	k1gFnSc1	Zelda
posmrtně	posmrtně	k6eAd1	posmrtně
novou	nový	k2eAgFnSc4d1	nová
roli	role	k1gFnSc4	role
<g/>
:	:	kIx,	:
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
populární	populární	k2eAgFnSc2d1	populární
biografie	biografie	k1gFnSc2	biografie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k9	jako
oběť	oběť	k1gFnSc1	oběť
arogantního	arogantní	k2eAgMnSc2d1	arogantní
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ikonou	ikona	k1gFnSc7	ikona
feministek	feministka	k1gFnPc2	feministka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zelda	Zelda	k1gMnSc1	Zelda
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zelda	Zelda	k1gFnSc1	Zelda
Fitzgeraldová	Fitzgeraldový	k2eAgFnSc1d1	Fitzgeraldový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zelda	Zelda	k1gFnSc1	Zelda
Fitzgeraldová	Fitzgeraldový	k2eAgFnSc1d1	Fitzgeraldový
</s>
</p>
