<s>
Které	který	k3yIgNnSc1	který
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
<g/>
?	?	kIx.	?
</s>
