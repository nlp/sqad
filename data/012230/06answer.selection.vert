<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
také	také	k9	také
IR	Ir	k1gMnSc1	Ir
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
infrared	infrared	k1gInSc1	infrared
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
