<p>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
také	také	k9	také
IR	Ir	k1gMnSc1	Ir
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
infrared	infrared	k1gInSc1	infrared
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
značí	značit	k5eAaImIp3nS	značit
"	"	kIx"	"
<g/>
pod	pod	k7c4	pod
červenou	červená	k1gFnSc4	červená
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
infra	infr	k1gInSc2	infr
=	=	kIx~	=
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
zabírá	zabírat	k5eAaImIp3nS	zabírat
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
3	[number]	k4	3
dekády	dekáda	k1gFnSc2	dekáda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
mezi	mezi	k7c7	mezi
760	[number]	k4	760
nm	nm	k?	nm
a	a	k8xC	a
1	[number]	k4	1
mm	mm	kA	mm
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
energii	energie	k1gFnSc4	energie
fotonů	foton	k1gInPc2	foton
mezi	mezi	k7c7	mezi
0,0012	[number]	k4	0,0012
a	a	k8xC	a
1,63	[number]	k4	1,63
eV.	eV.	k?	eV.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dané	daný	k2eAgNnSc1d1	dané
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
schéma	schéma	k1gNnSc1	schéma
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
blízké	blízký	k2eAgFnPc1d1	blízká
(	(	kIx(	(
<g/>
near	near	k1gInSc1	near
<g/>
)	)	kIx)	)
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
neboli	neboli	k8xC	neboli
NIRIR-A	NIRIR-A	k1gFnSc1	NIRIR-A
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
DIN	din	k1gInSc1	din
<g/>
,	,	kIx,	,
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
0,76	[number]	k4	0,76
<g/>
–	–	k?	–
<g/>
1,4	[number]	k4	1,4
μ	μ	k?	μ
<g/>
,	,	kIx,	,
definováno	definován	k2eAgNnSc1d1	definováno
podle	podle	k7c2	podle
vodní	vodní	k2eAgFnSc2d1	vodní
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
optických	optický	k2eAgInPc2d1	optický
vlákenIR	vlákenIR	k?	vlákenIR
krátké	krátký	k2eAgFnSc2d1	krátká
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
short	short	k1gInSc1	short
wave	wavat	k5eAaPmIp3nS	wavat
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
SWIRIR-B	SWIRIR-B	k1gFnSc1	SWIRIR-B
podle	podle	k7c2	podle
DIN	din	k1gInSc4	din
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
1,4	[number]	k4	1,4
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
μ	μ	k?	μ
<g/>
,	,	kIx,	,
při	při	k7c6	při
1450	[number]	k4	1450
nm	nm	k?	nm
značně	značně	k6eAd1	značně
roste	růst	k5eAaImIp3nS	růst
vodní	vodní	k2eAgFnSc4d1	vodní
absorpceIR	absorpceIR	k?	absorpceIR
střední	střední	k2eAgFnSc2d1	střední
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
medium	medium	k1gNnSc1	medium
wave	wavat	k5eAaPmIp3nS	wavat
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
MWIRIR-C	MWIRIR-C	k1gFnSc1	MWIRIR-C
podle	podle	k7c2	podle
DIN	din	k1gInSc4	din
<g/>
,	,	kIx,	,
též	též	k9	též
prostřední	prostřední	k2eAgFnSc6d1	prostřední
(	(	kIx(	(
<g/>
intermediate-IR	intermediate-IR	k?	intermediate-IR
neboli	neboli	k8xC	neboli
IIR	IIR	kA	IIR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
μ	μ	k?	μ
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
long	long	k1gInSc1	long
wave	wavat	k5eAaPmIp3nS	wavat
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
LWIRIR-C	LWIRIR-C	k1gFnSc1	LWIRIR-C
podle	podle	k7c2	podle
DIN	din	k1gInSc4	din
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
μ	μ	k?	μ
(	(	kIx(	(
<g/>
far	fara	k1gFnPc2	fara
<g/>
)	)	kIx)	)
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
neboli	neboli	k8xC	neboli
FIR	FIR	kA	FIR
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
μ	μ	k?	μ
často	často	k6eAd1	často
používané	používaný	k2eAgNnSc4d1	používané
rozdělení	rozdělení	k1gNnSc4	rozdělení
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
blízké	blízký	k2eAgInPc4d1	blízký
(	(	kIx(	(
<g/>
0,76	[number]	k4	0,76
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
μ	μ	k?	μ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
μ	μ	k?	μ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
Pásmu	pásmo	k1gNnSc6	pásmo
mezi	mezi	k7c4	mezi
100	[number]	k4	100
μ	μ	k?	μ
a	a	k8xC	a
1	[number]	k4	1
mm	mm	kA	mm
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
submilimetrové	submilimetrový	k2eAgFnSc2d1	submilimetrová
vlny	vlna	k1gFnSc2	vlna
nebo	nebo	k8xC	nebo
terahertzové	terahertzové	k2eAgNnSc2d1	terahertzové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
==	==	k?	==
</s>
</p>
<p>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
"	"	kIx"	"
<g/>
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrchy	povrch	k1gInPc1	povrch
těles	těleso	k1gNnPc2	těleso
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
absorpce	absorpce	k1gFnSc1	absorpce
libovolného	libovolný	k2eAgNnSc2d1	libovolné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
IR	Ir	k1gMnSc1	Ir
záření	záření	k1gNnSc2	záření
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
%	%	kIx~	%
zahřívání	zahřívání	k1gNnSc4	zahřívání
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
způsoben	způsoben	k2eAgInSc1d1	způsoben
viditelným	viditelný	k2eAgNnSc7d1	viditelné
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekty	objekt	k1gInPc1	objekt
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
emitují	emitovat	k5eAaBmIp3nP	emitovat
nejvíce	nejvíce	k6eAd1	nejvíce
záření	záření	k1gNnSc1	záření
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
pásmu	pásmo	k1gNnSc6	pásmo
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aplikace	aplikace	k1gFnPc4	aplikace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikace	komunikace	k1gFnSc1	komunikace
===	===	k?	===
</s>
</p>
<p>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
IA	ia	k0	ia
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
či	či	k8xC	či
dálkové	dálkový	k2eAgInPc4d1	dálkový
ovladače	ovladač	k1gInPc4	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
vysílají	vysílat	k5eAaImIp3nP	vysílat
LED	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Telekomunikační	telekomunikační	k2eAgNnPc1d1	telekomunikační
pásma	pásmo	k1gNnPc1	pásmo
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
optické	optický	k2eAgFnSc2d1	optická
komunikace	komunikace	k1gFnSc2	komunikace
se	se	k3xPyFc4	se
IR	Ir	k1gMnSc1	Ir
záření	záření	k1gNnSc2	záření
dělí	dělit	k5eAaImIp3nS	dělit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
O-pásmo	Oásmo	k6eAd1	O-pásmo
1260	[number]	k4	1260
<g/>
–	–	k?	–
<g/>
1360	[number]	k4	1360
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
238	[number]	k4	238
<g/>
–	–	k?	–
<g/>
220	[number]	k4	220
THz	THz	k1gFnSc1	THz
</s>
</p>
<p>
<s>
E-pásmo	Eásmo	k6eAd1	E-pásmo
1360	[number]	k4	1360
<g/>
–	–	k?	–
<g/>
1460	[number]	k4	1460
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
220-206	[number]	k4	220-206
THz	THz	k1gFnPc2	THz
</s>
</p>
<p>
<s>
S-pásmo	Sásmo	k6eAd1	S-pásmo
1460	[number]	k4	1460
<g/>
–	–	k?	–
<g/>
1530	[number]	k4	1530
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
206-196	[number]	k4	206-196
THz	THz	k1gFnPc2	THz
</s>
</p>
<p>
<s>
C-pásmo	Cásmo	k6eAd1	C-pásmo
1530	[number]	k4	1530
<g/>
–	–	k?	–
<g/>
1565	[number]	k4	1565
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
196-191	[number]	k4	196-191
THz	THz	k1gFnPc2	THz
</s>
</p>
<p>
<s>
L-pásmo	Lásmo	k6eAd1	L-pásmo
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1625	[number]	k4	1625
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
191-185	[number]	k4	191-185
THz	THz	k1gFnPc2	THz
</s>
</p>
<p>
<s>
U-pásmo	Uásmo	k6eAd1	U-pásmo
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1675	[number]	k4	1675
nm	nm	k?	nm
<g/>
,	,	kIx,	,
f	f	k?	f
=	=	kIx~	=
185-179	[number]	k4	185-179
THz	THz	k1gFnPc2	THz
</s>
</p>
<p>
<s>
===	===	k?	===
Spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
===	===	k?	===
</s>
</p>
<p>
<s>
Infračervená	infračervený	k2eAgFnSc1d1	infračervená
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
metoda	metoda	k1gFnSc1	metoda
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
metody	metoda	k1gFnPc4	metoda
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
kvalitativní	kvalitativní	k2eAgFnSc1d1	kvalitativní
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
velice	velice	k6eAd1	velice
přesnou	přesný	k2eAgFnSc4d1	přesná
identifikaci	identifikace	k1gFnSc4	identifikace
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
i	i	k9	i
pro	pro	k7c4	pro
kvantitativní	kvantitativní	k2eAgFnSc4d1	kvantitativní
analýzu	analýza	k1gFnSc4	analýza
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Země	zem	k1gFnPc1	zem
jako	jako	k8xC	jako
vysílač	vysílač	k1gInSc1	vysílač
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
==	==	k?	==
</s>
</p>
<p>
<s>
Zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
viditelné	viditelný	k2eAgNnSc1d1	viditelné
záření	záření	k1gNnSc1	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
mnoho	mnoho	k6eAd1	mnoho
energie	energie	k1gFnSc1	energie
jako	jako	k8xC	jako
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
skrze	skrze	k?	skrze
atmosféru	atmosféra	k1gFnSc4	atmosféra
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
plyny	plyn	k1gInPc1	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
toto	tento	k3xDgNnSc4	tento
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
je	on	k3xPp3gNnPc4	on
zpět	zpět	k6eAd1	zpět
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
včetně	včetně	k7c2	včetně
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
udržuje	udržovat	k5eAaImIp3nS	udržovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
o	o	k7c4	o
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
teplejší	teplý	k2eAgFnSc1d2	teplejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nP	kdyby
plyny	plyn	k1gInPc1	plyn
pohlcující	pohlcující	k2eAgFnSc2d1	pohlcující
infračervené	infračervený	k2eAgFnSc2d1	infračervená
záření	záření	k1gNnSc1	záření
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Absolutně	absolutně	k6eAd1	absolutně
černé	černý	k2eAgNnSc1d1	černé
těleso	těleso	k1gNnSc1	těleso
</s>
</p>
<p>
<s>
Infračervená	infračervený	k2eAgFnSc1d1	infračervená
astronomie	astronomie	k1gFnSc1	astronomie
</s>
</p>
<p>
<s>
Infračervená	infračervený	k2eAgFnSc1d1	infračervená
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Teplomet	teplomet	k1gInSc1	teplomet
</s>
</p>
