<s>
Acetochlor	Acetochlor	k1gMnSc1	Acetochlor
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-chloro-N-	hloro-N-	k?	-chloro-N-
<g/>
(	(	kIx(	(
<g/>
ethoxymethyl	ethoxymethyl	k1gInSc1	ethoxymethyl
<g/>
)	)	kIx)	)
<g/>
-N-	-N-	k?	-N-
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-ethyl-	thyl-	k?	-ethyl-
<g/>
6	[number]	k4	6
<g/>
-methylfenyl	ethylfenyl	k1gInSc1	-methylfenyl
<g/>
)	)	kIx)	)
<g/>
acetamid	acetamid	k1gInSc1	acetamid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
herbicid	herbicid	k1gInSc1	herbicid
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
agrochemickými	agrochemický	k2eAgFnPc7d1	agrochemická
firmami	firma	k1gFnPc7	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
a	a	k8xC	a
Zeneca	Zenecum	k1gNnSc2	Zenecum
<g/>
.	.	kIx.	.
</s>
