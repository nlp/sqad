<p>
<s>
Acetochlor	Acetochlor	k1gMnSc1	Acetochlor
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-chloro-N-	hloro-N-	k?	-chloro-N-
<g/>
(	(	kIx(	(
<g/>
ethoxymethyl	ethoxymethyl	k1gInSc1	ethoxymethyl
<g/>
)	)	kIx)	)
<g/>
-N-	-N-	k?	-N-
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-ethyl-	thyl-	k?	-ethyl-
<g/>
6	[number]	k4	6
<g/>
-methylfenyl	ethylfenyl	k1gInSc1	-methylfenyl
<g/>
)	)	kIx)	)
<g/>
acetamid	acetamid	k1gInSc1	acetamid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
herbicid	herbicid	k1gInSc1	herbicid
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
agrochemickými	agrochemický	k2eAgFnPc7d1	agrochemická
firmami	firma	k1gFnPc7	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
a	a	k8xC	a
Zeneca	Zenecum	k1gNnSc2	Zenecum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
plevelům	plevel	k1gInPc3	plevel
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
s	s	k7c7	s
kukuřicí	kukuřice	k1gFnSc7	kukuřice
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
atrazinu	atrazinout	k5eAaPmIp1nS	atrazinout
<g/>
.	.	kIx.	.
</s>
<s>
Acetochlor	Acetochlor	k1gInSc1	Acetochlor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
klasifikován	klasifikován	k2eAgInSc4d1	klasifikován
společností	společnost	k1gFnSc7	společnost
EPA	EPA	kA	EPA
jako	jako	k8xC	jako
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
karcinogen	karcinogen	k1gInSc1	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
byl	být	k5eAaImAgInS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc4	Protection
Agency	Agenca	k1gFnSc2	Agenca
-	-	kIx~	-
Acetochlor	Acetochlor	k1gInSc1	Acetochlor
</s>
</p>
