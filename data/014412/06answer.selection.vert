<s>
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
European	European	k1gInSc1
Telecommunications	Telecommunicationsa	k1gFnPc2
Standards	Standardsa	k1gFnPc2
Institute	institut	k1gInSc5
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
<g/>
,	,	kIx,
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
standardizaci	standardizace	k1gFnSc4
v	v	k7c6
telekomunikačním	telekomunikační	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
výrobci	výrobce	k1gMnPc1
zařízení	zařízení	k1gNnSc2
a	a	k8xC
síťoví	síťový	k2eAgMnPc1d1
operátoři	operátor	k1gMnPc1
<g/>
)	)	kIx)
v	v	k7c6
Evropě	Evropa	k1gFnSc6
s	s	k7c7
celosvětovým	celosvětový	k2eAgInSc7d1
dosahem	dosah	k1gInSc7
<g/>
.	.	kIx.
</s>