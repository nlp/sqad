<s>
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
Vznik	vznik	k1gInSc1
</s>
<s>
1988	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
etsi	etsi	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
European	European	k1gInSc1
Telecommunications	Telecommunicationsa	k1gFnPc2
Standards	Standardsa	k1gFnPc2
Institute	institut	k1gInSc5
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
<g/>
,	,	kIx,
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
standardizaci	standardizace	k1gFnSc4
v	v	k7c6
telekomunikačním	telekomunikační	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
výrobci	výrobce	k1gMnPc1
zařízení	zařízení	k1gNnSc2
a	a	k8xC
síťoví	síťový	k2eAgMnPc1d1
operátoři	operátor	k1gMnPc1
<g/>
)	)	kIx)
v	v	k7c6
Evropě	Evropa	k1gFnSc6
s	s	k7c7
celosvětovým	celosvětový	k2eAgInSc7d1
dosahem	dosah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgInPc4d3
výsledky	výsledek	k1gInPc4
ETSI	ETSI	kA
patří	patřit	k5eAaImIp3nS
standardizace	standardizace	k1gFnPc4
mobilní	mobilní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
GSM	GSM	kA
<g/>
,	,	kIx,
profesionálního	profesionální	k2eAgInSc2d1
rádiového	rádiový	k2eAgInSc2d1
systému	systém	k1gInSc2
TETRA	tetra	k1gFnSc1
a	a	k8xC
požadavků	požadavek	k1gInPc2
na	na	k7c4
Short	Short	k1gInSc4
Range	Range	k1gNnSc2
Device	device	k1gInSc2
včetně	včetně	k7c2
LPD	LPD	kA
<g/>
433	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
významné	významný	k2eAgFnPc4d1
standardizační	standardizační	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
spadající	spadající	k2eAgFnPc4d1
pod	pod	k7c7
ETSI	ETSI	kA
patří	patřit	k5eAaImIp3nS
TISPAN	TISPAN	kA
(	(	kIx(
<g/>
pro	pro	k7c4
konvergenci	konvergence	k1gFnSc4
pevných	pevný	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
Internetu	Internet	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
M2M	M2M	k1gFnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
machine-to-machine	machine-to-machinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ETSI	ETSI	kA
podnítila	podnítit	k5eAaPmAgFnS
založení	založení	k1gNnSc4
3GPP	3GPP	k4
a	a	k8xC
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
partnerských	partnerský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ETSI	ETSI	kA
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
organizací	organizace	k1gFnSc7
CEPT	CEPT	kA
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
uznána	uznat	k5eAaPmNgFnS
Evropskou	evropský	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
a	a	k8xC
sekretariátem	sekretariát	k1gInSc7
EFTA	EFTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
ETSI	ETSI	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Sophia	Sophius	k1gMnSc2
Antipolis	Antipolis	k1gFnSc6
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ETSI	ETSI	kA
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
standardizaci	standardizace	k1gFnSc4
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
(	(	kIx(
<g/>
ICT	ICT	kA
<g/>
)	)	kIx)
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
technologie	technologie	k1gFnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
telekomunikace	telekomunikace	k1gFnPc4
<g/>
,	,	kIx,
televizní	televizní	k2eAgNnSc4d1
a	a	k8xC
rozhlasové	rozhlasový	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
a	a	k8xC
příbuzné	příbuzný	k1gMnPc4
oblasti	oblast	k1gFnSc2
jako	jako	k8xS,k8xC
inteligentní	inteligentní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
a	a	k8xC
lékařská	lékařský	k2eAgFnSc1d1
elektronika	elektronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ETSI	ETSI	kA
má	mít	k5eAaImIp3nS
740	#num#	k4
členů	člen	k1gMnPc2
v	v	k7c6
62	#num#	k4
zemích	zem	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
i	i	k9
mimo	mimo	k7c4
ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
síťových	síťový	k2eAgMnPc2d1
operátorů	operátor	k1gMnPc2
<g/>
,	,	kIx,
správců	správce	k1gMnPc2
<g/>
,	,	kIx,
poskytovatelů	poskytovatel	k1gMnPc2
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
výzkumných	výzkumný	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
a	a	k8xC
uživatelů	uživatel	k1gMnPc2
—	—	k?
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgMnPc4
klíčové	klíčový	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
ICT	ICT	kA
<g/>
.	.	kIx.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1
ETSI	ETSI	kA
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
více	hodně	k6eAd2
než	než	k8xS
22	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příspěvky	příspěvek	k1gInPc7
pocházejí	pocházet	k5eAaImIp3nP
od	od	k7c2
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
obchodní	obchodní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
prodej	prodej	k1gInSc4
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
testování	testování	k1gNnSc4
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
organizování	organizování	k1gNnSc1
fór	fór	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ze	z	k7c2
smluv	smlouva	k1gFnPc2
o	o	k7c4
dílo	dílo	k1gNnSc4
a	a	k8xC
z	z	k7c2
financování	financování	k1gNnSc2
partnery	partner	k1gMnPc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
40	#num#	k4
<g/>
%	%	kIx~
příjmů	příjem	k1gInPc2
jde	jít	k5eAaImIp3nS
na	na	k7c4
financování	financování	k1gNnSc4
provozních	provozní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
,	,	kIx,
zbývajících	zbývající	k2eAgInPc2d1
60	#num#	k4
<g/>
%	%	kIx~
na	na	k7c4
pracovní	pracovní	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
kompetenčních	kompetenční	k2eAgNnPc2d1
center	centrum	k1gNnPc2
a	a	k8xC
speciálních	speciální	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
</s>
<s>
členové	člen	k1gMnPc1
přidružení	přidružení	k1gNnSc2
členové	člen	k1gMnPc1
pozorovatelé	pozorovatel	k1gMnPc1
</s>
<s>
Plnoprávnými	plnoprávný	k2eAgInPc7d1
členy	člen	k1gInPc7
ETSI	ETSI	kA
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
EU	EU	kA
<g/>
,	,	kIx,
Andorra	Andorra	k1gFnSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
a	a	k8xC
Gruzie	Gruzie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přidruženými	přidružený	k2eAgInPc7d1
členy	člen	k1gInPc7
ETSI	ETSI	kA
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Lesotho	Lesot	k1gMnSc4
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
Jemen	Jemen	k1gInSc1
<g/>
,	,	kIx,
Katar	katar	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Hongkongu	Hongkong	k1gInSc2
a	a	k8xC
Macaa	Macao	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc1
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc1
a	a	k8xC
Indonésie	Indonésie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.etsi.org	www.etsi.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.etsi.org	www.etsi.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ETSI	ETSI	kA
membership	membership	k1gMnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
European	European	k1gInSc4
Telecommunications	Telecommunications	k1gInSc1
Standards	Standards	k1gInSc1
Institute	institut	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
</s>
<s>
HiperMAN	HiperMAN	k?
</s>
<s>
ISDN	ISDN	kA
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telekomunikační	telekomunikační	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
WWW	WWW	kA
servee	servee	k1gFnSc1
</s>
<s>
NORMAPME	NORMAPME	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
The	The	k1gMnSc1
European	European	k1gMnSc1
Office	Office	kA
of	of	k?
Crafts	Crafts	k1gInSc1
<g/>
,	,	kIx,
Trades	Trades	k1gInSc1
and	and	k?
Small	Small	k1gInSc1
and	and	k?
Medium	medium	k1gNnSc1
sized	sized	k1gMnSc1
Enterprises	Enterprises	k1gMnSc1
for	forum	k1gNnPc2
Standardisation	Standardisation	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1088811205	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1401	#num#	k4
0734	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
97002739	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
147031580	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
97002739	#num#	k4
</s>
