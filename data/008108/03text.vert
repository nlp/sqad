<s>
Menopauza	menopauza	k1gFnSc1	menopauza
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
menstruace	menstruace	k1gFnSc1	menstruace
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přirozený	přirozený	k2eAgInSc4d1	přirozený
stav	stav	k1gInSc4	stav
v	v	k7c6	v
životě	život	k1gInSc6	život
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
funkce	funkce	k1gFnSc2	funkce
vaječníků	vaječník	k1gMnPc2	vaječník
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
tvorba	tvorba	k1gFnSc1	tvorba
hormonů	hormon	k1gInPc2	hormon
estrogenu	estrogen	k1gInSc2	estrogen
a	a	k8xC	a
progesteronu	progesteron	k1gInSc2	progesteron
<g/>
.	.	kIx.	.
</s>
<s>
Zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
se	se	k3xPyFc4	se
menstruační	menstruační	k2eAgInSc1d1	menstruační
cyklus	cyklus	k1gInSc1	cyklus
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
ztráta	ztráta	k1gFnSc1	ztráta
plodnosti	plodnost	k1gFnSc2	plodnost
–	–	k?	–
žena	žena	k1gFnSc1	žena
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
otěhotnět	otěhotnět	k5eAaPmF	otěhotnět
(	(	kIx(	(
<g/>
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
umělého	umělý	k2eAgNnSc2d1	umělé
oplodnění	oplodnění	k1gNnSc2	oplodnění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menopauza	menopauza	k1gFnSc1	menopauza
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
45	[number]	k4	45
<g/>
.	.	kIx.	.
až	až	k9	až
55	[number]	k4	55
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
věku	věk	k1gInSc2	věk
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
(	(	kIx(	(
<g/>
i	i	k8xC	i
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
)	)	kIx)	)
odchylky	odchylka	k1gFnPc4	odchylka
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
menopauzy	menopauza	k1gFnSc2	menopauza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
potíží	potíž	k1gFnPc2	potíž
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc4d1	noční
pocení	pocení	k1gNnSc3	pocení
<g/>
,	,	kIx,	,
návaly	nával	k1gInPc7	nával
horka	horko	k1gNnSc2	horko
<g/>
,	,	kIx,	,
nespavost	nespavost	k1gFnSc1	nespavost
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
poruchy	porucha	k1gFnPc1	porucha
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
nervozita	nervozita	k1gFnSc1	nervozita
a	a	k8xC	a
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
<g/>
,	,	kIx,	,
špatné	špatný	k2eAgNnSc1d1	špatné
libido	libido	k1gNnSc1	libido
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
i	i	k8xC	i
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zpravidla	zpravidla	k6eAd1	zpravidla
šestým	šestý	k4xOgInSc7	šestý
a	a	k8xC	a
sedmým	sedmý	k4xOgInSc7	sedmý
rokem	rok	k1gInSc7	rok
před	před	k7c7	před
poslední	poslední	k2eAgFnSc7d1	poslední
menstruací	menstruace	k1gFnSc7	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
negativní	negativní	k2eAgInPc1d1	negativní
příznaky	příznak	k1gInPc1	příznak
lze	lze	k6eAd1	lze
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
eliminovat	eliminovat	k5eAaBmF	eliminovat
doplňky	doplněk	k1gInPc4	doplněk
stravy	strava	k1gFnSc2	strava
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
fytoestrogenů	fytoestrogen	k1gInPc2	fytoestrogen
ze	z	k7c2	z
sóji	sója	k1gFnSc2	sója
a	a	k8xC	a
z	z	k7c2	z
červeného	červený	k2eAgInSc2d1	červený
jetele	jetel	k1gInSc2	jetel
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
z	z	k7c2	z
mateří	mateří	k2eAgFnSc2d1	mateří
kašičky	kašička	k1gFnSc2	kašička
a	a	k8xC	a
drmku	drmk	k1gInSc2	drmk
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
HRT	HRT	kA	HRT
–	–	k?	–
hormonální	hormonální	k2eAgFnSc2d1	hormonální
substituční	substituční	k2eAgFnSc2d1	substituční
terapie	terapie	k1gFnSc2	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
období	období	k1gNnSc2	období
přechodu	přechod	k1gInSc2	přechod
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
přibývat	přibývat	k5eAaImF	přibývat
na	na	k7c4	na
hmotnosti	hmotnost	k1gFnPc4	hmotnost
a	a	k8xC	a
kila	kilo	k1gNnPc1	kilo
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
daří	dařit	k5eAaImIp3nS	dařit
shazovat	shazovat	k5eAaImF	shazovat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hůře	zle	k6eAd2	zle
<g/>
.	.	kIx.	.
</s>
<s>
Začít	začít	k5eAaPmF	začít
se	se	k3xPyFc4	se
víc	hodně	k6eAd2	hodně
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
se	se	k3xPyFc4	se
podporuje	podporovat	k5eAaImIp3nS	podporovat
růst	růst	k1gInSc1	růst
svalové	svalový	k2eAgFnSc2d1	svalová
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lépe	dobře	k6eAd2	dobře
odbourávat	odbourávat	k5eAaImF	odbourávat
i	i	k9	i
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
zpevňujete	zpevňovat	k5eAaImIp2nP	zpevňovat
kostru	kostra	k1gFnSc4	kostra
a	a	k8xC	a
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
estrogenů	estrogen	k1gInPc2	estrogen
v	v	k7c6	v
přechodu	přechod	k1gInSc6	přechod
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rychlejším	rychlý	k2eAgInPc3d2	rychlejší
projevům	projev	k1gInPc3	projev
stárnutí	stárnutí	k1gNnSc2	stárnutí
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Žen	žena	k1gFnPc2	žena
fyzicky	fyzicky	k6eAd1	fyzicky
aktivních	aktivní	k2eAgInPc2d1	aktivní
<g/>
,	,	kIx,	,
dbajících	dbající	k2eAgInPc2d1	dbající
a	a	k8xC	a
pečujících	pečující	k2eAgInPc2d1	pečující
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
zdraví	zdravit	k5eAaImIp3nP	zdravit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
projevy	projev	k1gInPc1	projev
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Klimakterium	klimakterium	k1gNnSc1	klimakterium
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Menopauza	menopauza	k1gFnSc1	menopauza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
menopauza	menopauza	k1gFnSc1	menopauza
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
