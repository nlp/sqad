<p>
<s>
Sauna	sauna	k1gFnSc1	sauna
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc4d1	původní
finské	finský	k2eAgNnSc4d1	finské
slovo	slovo	k1gNnSc4	slovo
označující	označující	k2eAgNnSc4d1	označující
malé	malý	k2eAgNnSc4d1	malé
stavení	stavení	k1gNnSc4	stavení
nebo	nebo	k8xC	nebo
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vytopení	vytopení	k1gNnSc6	vytopení
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
ohřívárna	ohřívárna	k1gFnSc1	ohřívárna
při	při	k7c6	při
saunování	saunování	k1gNnSc6	saunování
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
sauny	sauna	k1gFnSc2	sauna
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
vykládaný	vykládaný	k2eAgInSc1d1	vykládaný
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
působí	působit	k5eAaImIp3nS	působit
dobře	dobře	k6eAd1	dobře
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
jsou	být	k5eAaImIp3nP	být
saunová	saunový	k2eAgNnPc1d1	saunové
kamna	kamna	k1gNnPc1	kamna
(	(	kIx(	(
<g/>
kiuas	kiuas	k1gInSc1	kiuas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obvykle	obvykle	k6eAd1	obvykle
vyhřejí	vyhřát	k5eAaPmIp3nP	vyhřát
místnost	místnost	k1gFnSc4	místnost
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
od	od	k7c2	od
60	[number]	k4	60
do	do	k7c2	do
120	[number]	k4	120
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
sauny	sauna	k1gFnPc4	sauna
i	i	k9	i
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
130	[number]	k4	130
až	až	k9	až
140	[number]	k4	140
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
sauně	sauna	k1gFnSc6	sauna
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
kamnech	kamna	k1gNnPc6	kamna
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
nalévá	nalévat	k5eAaImIp3nS	nalévat
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
pára	pára	k1gFnSc1	pára
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
saunových	saunův	k2eAgNnPc6d1	saunův
kamnech	kamna	k1gNnPc6	kamna
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
topí	topit	k5eAaImIp3nS	topit
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgNnPc1d1	časté
jsou	být	k5eAaImIp3nP	být
elektrická	elektrický	k2eAgNnPc1d1	elektrické
kamna	kamna	k1gNnPc1	kamna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
naftovými	naftový	k2eAgNnPc7d1	naftové
kamny	kamna	k1gNnPc7	kamna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primitivní	primitivní	k2eAgFnSc1d1	primitivní
sauna	sauna	k1gFnSc1	sauna
může	moct	k5eAaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
saunových	saunův	k2eAgNnPc2d1	saunův
kamen	kamna	k1gNnPc2	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
dobře	dobře	k6eAd1	dobře
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vytápěna	vytápěn	k2eAgFnSc1d1	vytápěna
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
rozžhavenými	rozžhavený	k2eAgMnPc7d1	rozžhavený
venku	venku	k6eAd1	venku
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
ohni	oheň	k1gInSc6	oheň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejspíš	nejspíš	k9	nejspíš
i	i	k9	i
původní	původní	k2eAgInSc1d1	původní
princip	princip	k1gInSc1	princip
vytápění	vytápění	k1gNnSc2	vytápění
sauny	sauna	k1gFnSc2	sauna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infrasauna	Infrasauna	k1gFnSc1	Infrasauna
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
ohřívání	ohřívání	k1gNnSc3	ohřívání
těla	tělo	k1gNnSc2	tělo
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Saunování	saunování	k1gNnSc2	saunování
==	==	k?	==
</s>
</p>
<p>
<s>
Ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
těla	tělo	k1gNnSc2	tělo
nad	nad	k7c4	nad
normální	normální	k2eAgFnSc4d1	normální
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
je	být	k5eAaImIp3nS	být
příjemné	příjemný	k2eAgNnSc1d1	příjemné
a	a	k8xC	a
relaxující	relaxující	k2eAgNnSc1d1	relaxující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
unavené	unavený	k2eAgInPc1d1	unavený
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
zklidňuje	zklidňovat	k5eAaImIp3nS	zklidňovat
stresovaný	stresovaný	k2eAgInSc4d1	stresovaný
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tělo	tělo	k1gNnSc1	tělo
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
úlevu	úleva	k1gFnSc4	úleva
přinášející	přinášející	k2eAgFnSc2d1	přinášející
látky	látka	k1gFnSc2	látka
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
endorfiny	endorfin	k1gInPc1	endorfin
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
máte	mít	k5eAaImIp2nP	mít
vyhřívat	vyhřívat	k5eAaImF	vyhřívat
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
zde	zde	k6eAd1	zde
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
pravidlo	pravidlo	k1gNnSc4	pravidlo
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
pocity	pocit	k1gInPc4	pocit
pálení	pálení	k1gNnSc2	pálení
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
kůže	kůže	k1gFnSc2	kůže
obličeje	obličej	k1gInSc2	obličej
a	a	k8xC	a
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
pocitu	pocit	k1gInSc2	pocit
horka	horko	k1gNnSc2	horko
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dochází	docházet	k5eAaImIp3nS	docházet
kolem	kolem	k7c2	kolem
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
podle	podle	k7c2	podle
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
prohřívárně	prohřívárna	k1gFnSc6	prohřívárna
sauny	sauna	k1gFnSc2	sauna
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
předržení	předržení	k1gNnSc1	předržení
jak	jak	k6eAd1	jak
v	v	k7c6	v
horku	horko	k1gNnSc6	horko
tak	tak	k6eAd1	tak
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
je	být	k5eAaImIp3nS	být
riziková	rizikový	k2eAgFnSc1d1	riziková
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgInSc4d2	lepší
pocit	pocit	k1gInSc4	pocit
ze	z	k7c2	z
saunování	saunování	k1gNnSc2	saunování
se	se	k3xPyFc4	se
saunující	saunující	k2eAgFnPc1d1	saunující
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
často	často	k6eAd1	často
šlehají	šlehat	k5eAaImIp3nP	šlehat
březovými	březový	k2eAgFnPc7d1	Březová
metlami	metla	k1gFnPc7	metla
s	s	k7c7	s
listím	listí	k1gNnSc7	listí
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
vihta	vihta	k1gMnSc1	vihta
nebo	nebo	k8xC	nebo
vasta	vasta	k1gMnSc1	vasta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přivádějí	přivádět	k5eAaImIp3nP	přivádět
si	se	k3xPyFc3	se
jím	jíst	k5eAaImIp1nS	jíst
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
vdechují	vdechovat	k5eAaImIp3nP	vdechovat
eterické	eterický	k2eAgFnPc1d1	eterická
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
rozdrcených	rozdrcený	k2eAgInPc2d1	rozdrcený
březových	březový	k2eAgInPc2d1	březový
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
k	k	k7c3	k
prokrvení	prokrvení	k1gNnSc3	prokrvení
kůže	kůže	k1gFnSc2	kůže
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgFnPc1d1	různá
pomůcky	pomůcka	k1gFnPc1	pomůcka
(	(	kIx(	(
<g/>
kartáče	kartáč	k1gInPc1	kartáč
<g/>
,	,	kIx,	,
žínky	žínka	k1gFnPc1	žínka
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
cílená	cílený	k2eAgFnSc1d1	cílená
automasáž	automasáž	k1gFnSc1	automasáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochlazení	ochlazení	k1gNnSc1	ochlazení
těla	tělo	k1gNnSc2	tělo
po	po	k7c6	po
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
buď	buď	k8xC	buď
prostým	prostý	k2eAgInSc7d1	prostý
pobytem	pobyt	k1gInSc7	pobyt
na	na	k7c6	na
studeném	studený	k2eAgInSc6d1	studený
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
prudkým	prudký	k2eAgNnSc7d1	prudké
snížením	snížení	k1gNnSc7	snížení
teploty	teplota	k1gFnSc2	teplota
pomocí	pomocí	k7c2	pomocí
sprchy	sprcha	k1gFnSc2	sprcha
či	či	k8xC	či
nejlépe	dobře	k6eAd3	dobře
skokem	skokem	k6eAd1	skokem
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
či	či	k8xC	či
bazénu	bazén	k1gInSc2	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Skoky	skok	k1gInPc1	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
běžně	běžně	k6eAd1	běžně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
do	do	k7c2	do
ledu	led	k1gInSc2	led
vyseká	vysekat	k5eAaPmIp3nS	vysekat
díra	díra	k1gFnSc1	díra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
avanto	avant	k2eAgNnSc1d1	avant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
nevypadá	vypadat	k5eNaImIp3nS	vypadat
<g/>
,	,	kIx,	,
prudké	prudký	k2eAgNnSc1d1	prudké
ochlazení	ochlazení	k1gNnSc1	ochlazení
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
příjemné	příjemný	k2eAgInPc4d1	příjemný
pocity	pocit	k1gInPc4	pocit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
finské	finský	k2eAgNnSc1d1	finské
jezero	jezero	k1gNnSc1	jezero
nahrazováno	nahrazován	k2eAgNnSc1d1	nahrazováno
v	v	k7c6	v
saunách	sauna	k1gFnPc6	sauna
ochlazovacím	ochlazovací	k2eAgInSc7d1	ochlazovací
bazénkem	bazének	k1gInSc7	bazének
a	a	k8xC	a
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Skoky	skok	k1gInPc1	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
po	po	k7c6	po
hlavě	hlava	k1gFnSc6	hlava
se	se	k3xPyFc4	se
nedoporučují	doporučovat	k5eNaImIp3nP	doporučovat
starším	starý	k2eAgInSc7d2	starší
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
nemocným	nemocný	k1gMnSc7	nemocný
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
krevním	krevní	k2eAgInSc7d1	krevní
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prudké	Prudké	k2eAgMnPc1d1	Prudké
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
a	a	k8xC	a
ochlazení	ochlazení	k1gNnSc1	ochlazení
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgNnSc4d1	obvyklé
pravidlo	pravidlo	k1gNnSc4	pravidlo
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
líbí	líbit	k5eAaImIp3nS	líbit
je	on	k3xPp3gNnSc4	on
dodržováno	dodržován	k2eAgNnSc4d1	dodržováno
i	i	k9	i
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
saunování	saunování	k1gNnSc1	saunování
výrazně	výrazně	k6eAd1	výrazně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
člověka	člověk	k1gMnSc2	člověk
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Finové	Fin	k1gMnPc1	Fin
se	se	k3xPyFc4	se
saunují	saunovat	k5eAaPmIp3nP	saunovat
většinou	většinou	k6eAd1	většinou
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
.	.	kIx.	.
</s>
<s>
Nudita	nudita	k1gFnSc1	nudita
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
z	z	k7c2	z
hygienických	hygienický	k2eAgInPc2d1	hygienický
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
veřejné	veřejný	k2eAgFnSc6d1	veřejná
sauně	sauna	k1gFnSc6	sauna
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
výhodná	výhodný	k2eAgFnSc1d1	výhodná
z	z	k7c2	z
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejlépe	dobře	k6eAd3	dobře
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ochranu	ochrana	k1gFnSc4	ochrana
kůže	kůže	k1gFnSc2	kůže
vůči	vůči	k7c3	vůči
horku	horko	k1gNnSc3	horko
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
sauna	sauna	k1gFnSc1	sauna
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
a	a	k8xC	a
smíšená	smíšený	k2eAgFnSc1d1	smíšená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
si	se	k3xPyFc3	se
dopředu	dopředu	k6eAd1	dopředu
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
plavky	plavka	k1gFnPc1	plavka
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
se	se	k3xPyFc4	se
nedopustili	dopustit	k5eNaPmAgMnP	dopustit
faux	faux	k1gInSc4	faux
pas	pas	k1gNnSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Ručníky	ručník	k1gInPc1	ručník
se	se	k3xPyFc4	se
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
většinou	většinou	k6eAd1	většinou
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
by	by	kYmCp3nP	by
překážely	překážet	k5eAaImAgInP	překážet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
povinné	povinný	k2eAgNnSc1d1	povinné
používat	používat	k5eAaImF	používat
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgInPc4d1	velký
ručníky	ručník	k1gInPc4	ručník
či	či	k8xC	či
osušky	osuška	k1gFnPc4	osuška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
saunující	saunující	k2eAgMnSc1d1	saunující
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgMnS	moct
položit	položit	k5eAaPmF	položit
i	i	k9	i
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
si	se	k3xPyFc3	se
jimi	on	k3xPp3gMnPc7	on
podložit	podložit	k5eAaPmF	podložit
záda	záda	k1gNnPc4	záda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
saunování	saunování	k1gNnSc6	saunování
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
vhodné	vhodný	k2eAgNnSc1d1	vhodné
se	se	k3xPyFc4	se
utřít	utřít	k5eAaPmF	utřít
do	do	k7c2	do
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
saunová	saunový	k2eAgFnSc1d1	saunová
čepice	čepice	k1gFnSc1	čepice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
vlasy	vlas	k1gInPc4	vlas
před	před	k7c7	před
horkem	horko	k1gNnSc7	horko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	s	k7c7	s
vlasy	vlas	k1gInPc7	vlas
a	a	k8xC	a
kůže	kůže	k1gFnSc1	kůže
hlavy	hlava	k1gFnSc2	hlava
dokážou	dokázat	k5eAaPmIp3nP	dokázat
ubránit	ubránit	k5eAaPmF	ubránit
vlivu	vliv	k1gInSc3	vliv
horka	horko	k1gNnSc2	horko
naprosto	naprosto	k6eAd1	naprosto
dostatečně	dostatečně	k6eAd1	dostatečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
přínosy	přínos	k1gInPc1	přínos
saunování	saunování	k1gNnSc2	saunování
==	==	k?	==
</s>
</p>
<p>
<s>
Sauna	sauna	k1gFnSc1	sauna
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
Termoterapie	termoterapie	k1gFnSc2	termoterapie
tedy	tedy	k8xC	tedy
léčebné	léčebný	k2eAgFnSc2d1	léčebná
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
příznivé	příznivý	k2eAgInPc4d1	příznivý
účinky	účinek	k1gInPc4	účinek
mírného	mírný	k2eAgNnSc2d1	mírné
přehřátí	přehřátí	k1gNnSc2	přehřátí
tkání	tkáň	k1gFnPc2	tkáň
nebo	nebo	k8xC	nebo
celého	celý	k2eAgInSc2d1	celý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
termoterapie	termoterapie	k1gFnSc2	termoterapie
jsou	být	k5eAaImIp3nP	být
Termální	termální	k2eAgFnPc1d1	termální
koupele	koupel	k1gFnPc1	koupel
<g/>
,	,	kIx,	,
Parafinové	parafinový	k2eAgInPc1d1	parafinový
zábaly	zábal	k1gInPc1	zábal
nebo	nebo	k8xC	nebo
prosté	prostý	k2eAgNnSc1d1	prosté
napařování	napařování	k1gNnSc1	napařování
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Termoterapie	termoterapie	k1gFnSc1	termoterapie
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
Hormeze	Hormeze	k1gFnSc2	Hormeze
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
příznivého	příznivý	k2eAgInSc2d1	příznivý
účinku	účinek	k1gInSc2	účinek
mírné	mírný	k2eAgFnSc2d1	mírná
zátěže	zátěž	k1gFnSc2	zátěž
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
posiluje	posilovat	k5eAaImIp3nS	posilovat
odolnost	odolnost	k1gFnSc4	odolnost
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
jeho	jeho	k3xOp3gNnSc4	jeho
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
civilizačními	civilizační	k2eAgFnPc7d1	civilizační
chorobami	choroba	k1gFnPc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
zátěží	zátěž	k1gFnSc7	zátěž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
přehřátí	přehřátí	k1gNnSc2	přehřátí
organismu	organismus	k1gInSc2	organismus
nad	nad	k7c4	nad
normální	normální	k2eAgFnSc4d1	normální
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sauna	sauna	k1gFnSc1	sauna
může	moct	k5eAaImIp3nS	moct
snižovat	snižovat	k5eAaImF	snižovat
hladiny	hladina	k1gFnPc4	hladina
cukru	cukr	k1gInSc2	cukr
u	u	k7c2	u
diabetiků	diabetik	k1gMnPc2	diabetik
<g/>
,	,	kIx,	,
posilovat	posilovat	k5eAaImF	posilovat
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
bránit	bránit	k5eAaImF	bránit
jeho	jeho	k3xOp3gNnPc4	jeho
selhání	selhání	k1gNnPc4	selhání
u	u	k7c2	u
kardiaků	kardiak	k1gMnPc2	kardiak
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
prokrvení	prokrvení	k1gNnSc4	prokrvení
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
třeba	třeba	k6eAd1	třeba
hojení	hojení	k1gNnSc4	hojení
obtížně	obtížně	k6eAd1	obtížně
hojitelných	hojitelný	k2eAgFnPc2d1	hojitelný
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
příznivě	příznivě	k6eAd1	příznivě
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
žádná	žádný	k3yNgFnSc1	žádný
známá	známý	k2eAgFnSc1d1	známá
studie	studie	k1gFnSc1	studie
nezkoumala	zkoumat	k5eNaImAgFnS	zkoumat
vliv	vliv	k1gInSc4	vliv
saunování	saunování	k1gNnPc2	saunování
na	na	k7c4	na
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
<g/>
,	,	kIx,	,
rakovinu	rakovina	k1gFnSc4	rakovina
nebo	nebo	k8xC	nebo
imunitu	imunita	k1gFnSc4	imunita
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Saunování	saunování	k1gNnSc1	saunování
v	v	k7c6	v
době	doba	k1gFnSc6	doba
těhotenství	těhotenství	k1gNnSc2	těhotenství
či	či	k8xC	či
kojení	kojení	k1gNnSc2	kojení
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
saunování	saunování	k1gNnSc1	saunování
snižuje	snižovat	k5eAaImIp3nS	snižovat
plodnost	plodnost	k1gFnSc1	plodnost
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Saunování	saunování	k1gNnSc1	saunování
sice	sice	k8xC	sice
snižuje	snižovat	k5eAaImIp3nS	snižovat
množství	množství	k1gNnSc1	množství
spermií	spermie	k1gFnPc2	spermie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
po	po	k7c6	po
3	[number]	k4	3
měsících	měsíc	k1gInPc6	měsíc
vynechání	vynechání	k1gNnSc2	vynechání
sauny	sauna	k1gFnSc2	sauna
upraví	upravit	k5eAaPmIp3nS	upravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Omezení	omezení	k1gNnSc1	omezení
pro	pro	k7c4	pro
saunující	saunující	k2eAgFnSc4d1	saunující
==	==	k?	==
</s>
</p>
<p>
<s>
Saunování	saunování	k1gNnSc1	saunování
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
při	při	k7c6	při
chorobách	choroba	k1gFnPc6	choroba
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
však	však	k9	však
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mírné	mírný	k2eAgNnSc1d1	mírné
rozumné	rozumný	k2eAgNnSc1d1	rozumné
saunování	saunování	k1gNnSc1	saunování
může	moct	k5eAaImIp3nS	moct
kardiakům	kardiak	k1gMnPc3	kardiak
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
prospět	prospět	k5eAaPmF	prospět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
pro	pro	k7c4	pro
zběhlé	zběhlý	k2eAgMnPc4d1	zběhlý
návštěvníky	návštěvník	k1gMnPc4	návštěvník
sauny	sauna	k1gFnSc2	sauna
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
roztopené	roztopený	k2eAgFnSc6d1	roztopená
sauně	sauna	k1gFnSc6	sauna
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kožních	kožní	k2eAgFnPc6d1	kožní
chorobách	choroba	k1gFnPc6	choroba
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
se	se	k3xPyFc4	se
poradit	poradit	k5eAaPmF	poradit
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
s	s	k7c7	s
takovouto	takovýto	k3xDgFnSc7	takovýto
chorobou	choroba	k1gFnSc7	choroba
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
sauny	sauna	k1gFnSc2	sauna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
chodit	chodit	k5eAaImF	chodit
ani	ani	k8xC	ani
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gInSc1	jeho
termoregulační	termoregulační	k2eAgInSc1d1	termoregulační
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
narušen	narušen	k2eAgInSc1d1	narušen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
nakazit	nakazit	k5eAaPmF	nakazit
spolusaunující	spolusaunující	k2eAgMnSc1d1	spolusaunující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
sauna	sauna	k1gFnSc1	sauna
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
tělo	tělo	k1gNnSc4	tělo
extrémní	extrémní	k2eAgFnSc1d1	extrémní
zátěž	zátěž	k1gFnSc1	zátěž
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
člověk	člověk	k1gMnSc1	člověk
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
velmi	velmi	k6eAd1	velmi
unavený	unavený	k2eAgMnSc1d1	unavený
nebo	nebo	k8xC	nebo
nevyspalý	vyspalý	k2eNgMnSc1d1	nevyspalý
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
ani	ani	k9	ani
hladový	hladový	k2eAgMnSc1d1	hladový
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
přejezený	přejezený	k2eAgInSc1d1	přejezený
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
mít	mít	k5eAaImF	mít
žízeň	žízeň	k1gFnSc4	žízeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
během	během	k7c2	během
saunování	saunování	k1gNnSc2	saunování
hodně	hodně	k6eAd1	hodně
potí	potit	k5eAaImIp3nS	potit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
tělu	tělo	k1gNnSc3	tělo
dodávat	dodávat	k5eAaImF	dodávat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
saunování	saunování	k1gNnSc2	saunování
<g/>
.	.	kIx.	.
</s>
<s>
Finové	Fin	k1gMnPc1	Fin
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
běžně	běžně	k6eAd1	běžně
popíjejí	popíjet	k5eAaImIp3nP	popíjet
i	i	k9	i
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
opije	opít	k5eAaPmIp3nS	opít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
saunování	saunování	k1gNnSc6	saunování
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
jejich	jejich	k3xOp3gNnSc4	jejich
malé	malý	k2eAgNnSc4d1	malé
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
vnímavost	vnímavost	k1gFnSc4	vnímavost
vůči	vůči	k7c3	vůči
změnám	změna	k1gFnPc3	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
neměly	mít	k5eNaImAgFnP	mít
saunovat	saunovat	k5eAaBmF	saunovat
v	v	k7c6	v
příliš	příliš	k6eAd1	příliš
teplých	teplý	k2eAgFnPc6d1	teplá
saunách	sauna	k1gFnPc6	sauna
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vystavovat	vystavovat	k5eAaImF	vystavovat
velkým	velký	k2eAgFnPc3d1	velká
změnám	změna	k1gFnPc3	změna
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vzít	vzít	k5eAaPmF	vzít
do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
i	i	k8xC	i
několikaměsíční	několikaměsíční	k2eAgNnSc1d1	několikaměsíční
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
malé	malý	k2eAgNnSc1d1	malé
dítě	dítě	k1gNnSc1	dítě
ještě	ještě	k9	ještě
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
dobře	dobře	k6eAd1	dobře
ovládat	ovládat	k5eAaImF	ovládat
svoji	svůj	k3xOyFgFnSc4	svůj
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
jeho	jeho	k3xOp3gNnSc3	jeho
přehřátí	přehřátí	k1gNnSc3	přehřátí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
chodit	chodit	k5eAaImF	chodit
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Srdeční	srdeční	k2eAgInSc1d1	srdeční
tep	tep	k1gInSc1	tep
se	se	k3xPyFc4	se
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
o	o	k7c4	o
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
saunovat	saunovat	k5eAaBmF	saunovat
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
prvním	první	k4xOgInSc7	první
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
potírny	potírna	k1gFnSc2	potírna
se	se	k3xPyFc4	se
umyjte	umýt	k5eAaPmRp2nP	umýt
mýdlem	mýdlo	k1gNnSc7	mýdlo
a	a	k8xC	a
osprchujte	osprchovat	k5eAaPmRp2nP	osprchovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
potírny	potírna	k1gFnSc2	potírna
setřete	setřít	k5eAaPmIp2nP	setřít
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
sauna	sauna	k1gFnSc1	sauna
není	být	k5eNaImIp3nS	být
pára	pára	k1gFnSc1	pára
<g/>
!	!	kIx.	!
</s>
<s>
Do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
vezměte	vzít	k5eAaPmRp2nP	vzít
jen	jen	k9	jen
ručník	ručník	k1gInSc4	ručník
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
oblečení	oblečení	k1gNnSc4	oblečení
na	na	k7c6	na
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
sednete	sednout	k5eAaPmIp2nP	sednout
nebo	nebo	k8xC	nebo
lehnete	lehnout	k5eAaPmIp2nP	lehnout
a	a	k8xC	a
masážní	masážní	k2eAgFnSc4d1	masážní
houbu	houba	k1gFnSc4	houba
nebo	nebo	k8xC	nebo
kartáč	kartáč	k1gInSc4	kartáč
<g/>
,	,	kIx,	,
ne	ne	k9	ne
z	z	k7c2	z
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
dýchejte	dýchat	k5eAaImRp2nP	dýchat
jen	jen	k6eAd1	jen
ústy	ústa	k1gNnPc7	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
vzduch	vzduch	k1gInSc1	vzduch
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
nosem	nos	k1gInSc7	nos
vysušuje	vysušovat	k5eAaImIp3nS	vysušovat
sliznici	sliznice	k1gFnSc4	sliznice
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bolest	bolest	k1gFnSc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pocení	pocení	k1gNnSc6	pocení
masírujte	masírovat	k5eAaImRp2nP	masírovat
povrch	povrch	k1gInSc4	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
minutách	minuta	k1gFnPc6	minuta
vyjděte	vyjít	k5eAaPmRp2nP	vyjít
z	z	k7c2	z
potírny	potírna	k1gFnSc2	potírna
<g/>
,	,	kIx,	,
opláchněte	opláchnout	k5eAaPmRp2nP	opláchnout
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
sprchou	sprcha	k1gFnSc7	sprcha
a	a	k8xC	a
ochlaďte	ochladit	k5eAaPmRp2nP	ochladit
v	v	k7c6	v
bazénku	bazének	k1gInSc6	bazének
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
i	i	k9	i
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pocitu	pocit	k1gInSc6	pocit
chladu	chlad	k1gInSc2	chlad
se	se	k3xPyFc4	se
vraťte	vrátit	k5eAaPmRp2nP	vrátit
do	do	k7c2	do
sauny	sauna	k1gFnSc2	sauna
<g/>
.	.	kIx.	.
</s>
<s>
Proceduru	procedura	k1gFnSc4	procedura
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
chlad	chlad	k1gInSc4	chlad
opakujte	opakovat	k5eAaImRp2nP	opakovat
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
sauna	sauna	k1gFnSc1	sauna
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
saunování	saunování	k1gNnSc6	saunování
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
hodně	hodně	k6eAd1	hodně
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
minerálku	minerálka	k1gFnSc4	minerálka
<g/>
,	,	kIx,	,
džus	džus	k1gInSc4	džus
<g/>
,	,	kIx,	,
chladný	chladný	k2eAgInSc4d1	chladný
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
je	být	k5eAaImIp3nS	být
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sauna	sauna	k1gFnSc1	sauna
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgNnSc1d1	kulturní
dědictví	dědictví	k1gNnSc1	dědictví
==	==	k?	==
</s>
</p>
<p>
<s>
Sauna	sauna	k1gFnSc1	sauna
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
národní	národní	k2eAgFnSc2d1	národní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stejně	stejně	k6eAd1	stejně
posvátné	posvátný	k2eAgNnSc4d1	posvátné
místo	místo	k1gNnSc4	místo
jako	jako	k8xS	jako
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
za	za	k7c4	za
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
hádat	hádat	k5eAaImF	hádat
nebo	nebo	k8xC	nebo
nadávat	nadávat	k5eAaImF	nadávat
<g/>
.	.	kIx.	.
</s>
<s>
Sauny	sauna	k1gFnPc1	sauna
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
sauna	sauna	k1gFnSc1	sauna
běžným	běžný	k2eAgNnSc7d1	běžné
zařízením	zařízení	k1gNnSc7	zařízení
bytu	byt	k1gInSc2	byt
zabudovaným	zabudovaný	k2eAgMnPc3d1	zabudovaný
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
vedle	vedle	k7c2	vedle
koupelny	koupelna	k1gFnSc2	koupelna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
mnohapodlažních	mnohapodlažní	k2eAgInPc6d1	mnohapodlažní
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
chaty	chata	k1gFnSc2	chata
u	u	k7c2	u
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
saun	sauna	k1gFnPc2	sauna
než	než	k8xS	než
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
i	i	k9	i
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
sauny	sauna	k1gFnPc1	sauna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
společných	společný	k2eAgFnPc2d1	společná
veřejných	veřejný	k2eAgFnPc2d1	veřejná
saun	sauna	k1gFnPc2	sauna
se	se	k3xPyFc4	se
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
zásadně	zásadně	k6eAd1	zásadně
chodí	chodit	k5eAaImIp3nP	chodit
bez	bez	k7c2	bez
plavek	plavka	k1gFnPc2	plavka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
se	se	k3xPyFc4	se
sauna	sauna	k1gFnSc1	sauna
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
masově	masově	k6eAd1	masově
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přišla	přijít	k5eAaPmAgFnS	přijít
i	i	k9	i
do	do	k7c2	do
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
nebo	nebo	k8xC	nebo
i	i	k8xC	i
bytě	byt	k1gInSc6	byt
4	[number]	k4	4
z	z	k7c2	z
5	[number]	k4	5
finských	finský	k2eAgFnPc2d1	finská
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Saunu	sauna	k1gFnSc4	sauna
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
většina	většina	k1gFnSc1	většina
finských	finský	k2eAgFnPc2d1	finská
firem	firma	k1gFnPc2	firma
nebo	nebo	k8xC	nebo
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
parlamentu	parlament	k1gInSc6	parlament
i	i	k8xC	i
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejhlouběji	hluboko	k6eAd3	hluboko
položená	položený	k2eAgFnSc1d1	položená
sauna	sauna	k1gFnSc1	sauna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
1	[number]	k4	1
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
v	v	k7c6	v
dole	dol	k1gInSc6	dol
v	v	k7c6	v
Pyhäsalmi	Pyhäsal	k1gFnPc7	Pyhäsal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
zinek	zinek	k1gInSc1	zinek
a	a	k8xC	a
měď	měď	k1gFnSc1	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
finským	finský	k2eAgInSc7d1	finský
koníčkem	koníček	k1gInSc7	koníček
jsou	být	k5eAaImIp3nP	být
sauny	sauna	k1gFnPc1	sauna
postavené	postavený	k2eAgFnPc1d1	postavená
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
přívěsu	přívěs	k1gInSc2	přívěs
za	za	k7c4	za
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
(	(	kIx(	(
<g/>
kärrysauna	kärrysauna	k1gFnSc1	kärrysauna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
vyhřívány	vyhříván	k2eAgFnPc1d1	vyhřívána
kamny	kamna	k1gNnPc7	kamna
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
existuje	existovat	k5eAaImIp3nS	existovat
podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
i	i	k8xC	i
saunový	saunový	k2eAgMnSc1d1	saunový
elf	elf	k1gMnSc1	elf
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
Saunatonttu	Saunatontt	k1gMnSc3	Saunatontt
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
umí	umět	k5eAaImIp3nS	umět
kouzlit	kouzlit	k5eAaImF	kouzlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sauna	sauna	k1gFnSc1	sauna
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
především	především	k9	především
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
saun	sauna	k1gFnPc2	sauna
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
původní	původní	k2eAgFnSc6d1	původní
finské	finský	k2eAgFnSc6d1	finská
sauně	sauna	k1gFnSc6	sauna
nepodobají	podobat	k5eNaImIp3nP	podobat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
jen	jen	k9	jen
několik	několik	k4yIc4	několik
°	°	k?	°
<g/>
C	C	kA	C
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplotou	teplota	k1gFnSc7	teplota
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlhkost	vlhkost	k1gFnSc1	vlhkost
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
stoprocentní	stoprocentní	k2eAgFnSc1d1	stoprocentní
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
působí	působit	k5eAaImIp3nS	působit
blahodárně	blahodárně	k6eAd1	blahodárně
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
a	a	k8xC	a
prevenci	prevence	k1gFnSc6	prevence
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovéto	takovýto	k3xDgFnSc6	takovýto
sauně	sauna	k1gFnSc6	sauna
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
prokrví	prokrvit	k5eAaPmIp3nS	prokrvit
podkoží	podkoží	k1gNnSc1	podkoží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
neprohřeje	prohřát	k5eNaPmIp3nS	prohřát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
druhem	druh	k1gInSc7	druh
sauny	sauna	k1gFnSc2	sauna
bývá	bývat	k5eAaImIp3nS	bývat
suchá	suchý	k2eAgFnSc1d1	suchá
sauna	sauna	k1gFnSc1	sauna
(	(	kIx(	(
<g/>
též	též	k9	též
švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
kamna	kamna	k1gNnPc4	kamna
se	se	k3xPyFc4	se
nepřilévá	přilévat	k5eNaImIp3nS	přilévat
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
ani	ani	k8xC	ani
zkoušet	zkoušet	k5eAaImF	zkoušet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
používaná	používaný	k2eAgNnPc1d1	používané
kamna	kamna	k1gNnPc1	kamna
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nebývají	bývat	k5eNaImIp3nP	bývat
stavěná	stavěný	k2eAgFnSc1d1	stavěná
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
zranění	zranění	k1gNnSc1	zranění
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
sauny	sauna	k1gFnSc2	sauna
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nevyužívá	využívat	k5eNaImIp3nS	využívat
žádné	žádný	k3yNgFnPc4	žádný
vlhkosti	vlhkost	k1gFnPc4	vlhkost
a	a	k8xC	a
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
není	být	k5eNaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
se	se	k3xPyFc4	se
zpotit	zpotit	k5eAaPmF	zpotit
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
i	i	k9	i
sauny	sauna	k1gFnPc1	sauna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
kamna	kamna	k1gNnPc4	kamna
voda	voda	k1gFnSc1	voda
přilévá	přilévat	k5eAaImIp3nS	přilévat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
blízká	blízký	k2eAgFnSc1d1	blízká
finské	finský	k2eAgFnSc3d1	finská
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sauna	sauna	k1gFnSc1	sauna
(	(	kIx(	(
<g/>
saun	sauna	k1gFnPc2	sauna
<g/>
)	)	kIx)	)
právě	právě	k9	právě
takovou	takový	k3xDgFnSc7	takový
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Estonská	estonský	k2eAgFnSc1d1	Estonská
sauna	sauna	k1gFnSc1	sauna
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
finská	finský	k2eAgFnSc1d1	finská
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
běžně	běžně	k6eAd1	běžně
součástí	součást	k1gFnSc7	součást
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
jen	jen	k9	jen
v	v	k7c6	v
obytných	obytný	k2eAgFnPc6d1	obytná
budovách	budova	k1gFnPc6	budova
vystavěných	vystavěný	k2eAgFnPc6d1	vystavěná
za	za	k7c2	za
sovětského	sovětský	k2eAgInSc2d1	sovětský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nP	stavit
podobné	podobný	k2eAgFnPc1d1	podobná
sauny	sauna	k1gFnPc1	sauna
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
finské	finský	k2eAgInPc1d1	finský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
vody	voda	k1gFnSc2	voda
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
polévání	polévání	k1gNnSc4	polévání
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
i	i	k8xC	i
k	k	k7c3	k
mytí	mytí	k1gNnSc3	mytí
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
sauna	sauna	k1gFnSc1	sauna
snesitelnější	snesitelný	k2eAgFnSc1d2	snesitelnější
i	i	k9	i
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
zkušené	zkušený	k2eAgMnPc4d1	zkušený
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
ruské	ruský	k2eAgFnSc6d1	ruská
sauně	sauna	k1gFnSc6	sauna
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
používané	používaný	k2eAgFnSc2d1	používaná
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
obvykle	obvykle	k6eAd1	obvykle
pod	pod	k7c7	pod
prkennou	prkenný	k2eAgFnSc7d1	prkenná
podlahou	podlaha	k1gFnSc7	podlaha
jímka	jímka	k1gFnSc1	jímka
na	na	k7c4	na
odvod	odvod	k1gInSc4	odvod
popř.	popř.	kA	popř.
vsakování	vsakování	k1gNnPc4	vsakování
přebytečné	přebytečný	k2eAgFnSc2d1	přebytečná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
nápojem	nápoj	k1gInSc7	nápoj
používaným	používaný	k2eAgInSc7d1	používaný
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
sauně	sauna	k1gFnSc6	sauna
k	k	k7c3	k
doplňování	doplňování	k1gNnSc3	doplňování
tekutin	tekutina	k1gFnPc2	tekutina
je	být	k5eAaImIp3nS	být
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
sauna	sauna	k1gFnSc1	sauna
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
baňa	baňa	k1gFnSc1	baňa
(	(	kIx(	(
<g/>
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc4d1	běžná
turecké	turecký	k2eAgFnPc4d1	turecká
lázně	lázeň	k1gFnPc4	lázeň
hamam	hamam	k6eAd1	hamam
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
sauně	sauna	k1gFnSc3	sauna
moc	moc	k6eAd1	moc
nepodobají	podobat	k5eNaImIp3nP	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
byly	být	k5eAaImAgFnP	být
i	i	k9	i
antické	antický	k2eAgFnPc1d1	antická
římské	římský	k2eAgFnPc1d1	římská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
lázněmi	lázeň	k1gFnPc7	lázeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc1	postup
opačný	opačný	k2eAgInSc1d1	opačný
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
sauně	sauna	k1gFnSc6	sauna
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
ohřeje	ohřát	k5eAaPmIp3nS	ohřát
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
chladí	chladit	k5eAaImIp3nS	chladit
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
sauny	sauna	k1gFnPc1	sauna
jako	jako	k8xS	jako
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
jen	jen	k9	jen
obvykle	obvykle	k6eAd1	obvykle
nebývají	bývat	k5eNaImIp3nP	bývat
tak	tak	k6eAd1	tak
teplé	teplý	k2eAgInPc1d1	teplý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
nižší	nízký	k2eAgFnSc1d2	nižší
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
sauny	sauna	k1gFnSc2	sauna
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
sauny	sauna	k1gFnSc2	sauna
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ibráhím	Ibráhet	k5eAaImIp1nS	Ibráhet
ibn	ibn	k?	ibn
Jákúb	Jákúb	k1gMnSc1	Jákúb
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
poselstva	poselstvo	k1gNnSc2	poselstvo
kalifa	kalif	k1gMnSc2	kalif
al-Hakama	al-Hakam	k1gMnSc2	al-Hakam
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prošlo	projít	k5eAaPmAgNnS	projít
Čechami	Čechy	k1gFnPc7	Čechy
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
965	[number]	k4	965
a	a	k8xC	a
966	[number]	k4	966
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
saunu	sauna	k1gFnSc4	sauna
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
budky	budka	k1gFnPc4	budka
s	s	k7c7	s
trámy	trám	k1gInPc7	trám
vycpanými	vycpaný	k2eAgInPc7d1	vycpaný
mechem	mech	k1gInSc7	mech
<g/>
,	,	kIx,	,
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
kamna	kamna	k1gNnPc4	kamna
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimiž	jenž	k3xRgMnPc7	jenž
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
ve	v	k7c6	v
stropě	strop	k1gInSc6	strop
pro	pro	k7c4	pro
odcházející	odcházející	k2eAgInSc4d1	odcházející
dým	dým	k1gInSc4	dým
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
byly	být	k5eAaImAgFnP	být
nádrže	nádrž	k1gFnPc1	nádrž
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
rozpálená	rozpálený	k2eAgNnPc1d1	rozpálené
kamna	kamna	k1gNnPc1	kamna
polévají	polévat	k5eAaImIp3nP	polévat
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
uvnitř	uvnitř	k6eAd1	uvnitř
pak	pak	k6eAd1	pak
věchýtky	věchýtka	k1gFnSc2	věchýtka
trávy	tráva	k1gFnSc2	tráva
rozhání	rozhánět	k5eAaImIp3nS	rozhánět
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Pára	pára	k1gFnSc1	pára
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odstraní	odstranit	k5eAaPmIp3nS	odstranit
všechny	všechen	k3xTgFnPc4	všechen
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
vyrážce	vyrážka	k1gFnSc6	vyrážka
či	či	k8xC	či
vředech	vřed	k1gInPc6	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
záznam	záznam	k1gInSc1	záznam
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
sauny	sauna	k1gFnSc2	sauna
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spisu	spis	k1gInSc6	spis
Život	život	k1gInSc1	život
a	a	k8xC	a
umučení	umučení	k1gNnSc1	umučení
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
břevnovský	břevnovský	k2eAgInSc4d1	břevnovský
mnich	mnich	k1gInSc4	mnich
Kristián	Kristián	k1gMnSc1	Kristián
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůdce	vůdce	k1gMnSc1	vůdce
vrahů	vrah	k1gMnPc2	vrah
byl	být	k5eAaImAgMnS	být
nalezen	naleznout	k5eAaPmNgMnS	naleznout
ležíc	ležet	k5eAaImSgFnS	ležet
v	v	k7c6	v
parní	parní	k2eAgFnSc6d1	parní
lázni	lázeň	k1gFnSc6	lázeň
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
stuba	stuba	k1gFnSc1	stuba
<g/>
,	,	kIx,	,
jizba	jizba	k1gFnSc1	jizba
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInPc1	první
popisy	popis	k1gInPc1	popis
sauny	sauna	k1gFnSc2	sauna
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
parní	parní	k2eAgFnSc1d1	parní
lázeň	lázeň	k1gFnSc1	lázeň
vcelku	vcelku	k6eAd1	vcelku
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Klaus	Klaus	k1gMnSc1	Klaus
Magnus	Magnus	k1gMnSc1	Magnus
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikde	nikde	k6eAd1	nikde
není	být	k5eNaImIp3nS	být
parní	parní	k2eAgFnSc1d1	parní
lázeň	lázeň	k1gFnSc1	lázeň
tak	tak	k6eAd1	tak
důležitá	důležitý	k2eAgFnSc1d1	důležitá
jako	jako	k8xC	jako
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
saunu	sauna	k1gFnSc4	sauna
měla	mít	k5eAaImAgFnS	mít
prakticky	prakticky	k6eAd1	prakticky
každá	každý	k3xTgFnSc1	každý
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Finy	Fin	k1gMnPc4	Fin
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gInSc2	jejich
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkávali	setkávat	k5eAaImAgMnP	setkávat
a	a	k8xC	a
mluvili	mluvit	k5eAaImAgMnP	mluvit
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
sauna	sauna	k1gFnSc1	sauna
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
nejčistší	čistý	k2eAgNnSc4d3	nejčistší
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
například	například	k6eAd1	například
i	i	k9	i
při	při	k7c6	při
porodech	porod	k1gInPc6	porod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
sauna	sauna	k1gFnSc1	sauna
se	se	k3xPyFc4	se
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
sauny	sauna	k1gFnSc2	sauna
lišila	lišit	k5eAaImAgFnS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
tzv.	tzv.	kA	tzv.
kouřová	kouřový	k2eAgFnSc1d1	kouřová
sauna	sauna	k1gFnSc1	sauna
(	(	kIx(	(
<g/>
savusauna	savusauna	k1gFnSc1	savusauna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k6eAd1	uvnitř
celá	celý	k2eAgFnSc1d1	celá
černá	černá	k1gFnSc1	černá
od	od	k7c2	od
sazí	saze	k1gFnPc2	saze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vyhřívaní	vyhřívaný	k2eAgMnPc1d1	vyhřívaný
se	se	k3xPyFc4	se
kouř	kouř	k1gInSc1	kouř
valí	valit	k5eAaImIp3nS	valit
z	z	k7c2	z
kamen	kamna	k1gNnPc2	kamna
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
zásoba	zásoba	k1gFnSc1	zásoba
kamenů	kámen	k1gInPc2	kámen
v	v	k7c6	v
kamnech	kamna	k1gNnPc6	kamna
zahřátá	zahřátý	k2eAgNnPc4d1	zahřáté
<g/>
,	,	kIx,	,
přestane	přestat	k5eAaPmIp3nS	přestat
se	se	k3xPyFc4	se
topit	topit	k5eAaImF	topit
<g/>
,	,	kIx,	,
sauna	sauna	k1gFnSc1	sauna
se	se	k3xPyFc4	se
umyje	umýt	k5eAaPmIp3nS	umýt
od	od	k7c2	od
sazí	saze	k1gFnPc2	saze
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
začít	začít	k5eAaPmF	začít
saunovat	saunovat	k5eAaBmF	saunovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
saze	saze	k1gFnSc1	saze
špiní	špinit	k5eAaImIp3nS	špinit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sterilní	sterilní	k2eAgFnPc1d1	sterilní
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
saunou	sauna	k1gFnSc7	sauna
můžete	moct	k5eAaImIp2nP	moct
stále	stále	k6eAd1	stále
setkat	setkat	k5eAaPmF	setkat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
relativně	relativně	k6eAd1	relativně
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nejstarší	starý	k2eAgFnSc1d3	nejstarší
fungující	fungující	k2eAgFnSc1d1	fungující
veřejná	veřejný	k2eAgFnSc1d1	veřejná
sauna	sauna	k1gFnSc1	sauna
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Rajaportin	Rajaportin	k1gMnSc1	Rajaportin
sauna	sauna	k1gFnSc1	sauna
v	v	k7c6	v
Tampere	Tamper	k1gMnSc5	Tamper
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sauna	sauna	k1gFnSc1	sauna
kouřová	kouřový	k2eAgFnSc1d1	kouřová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
tradují	tradovat	k5eAaImIp3nP	tradovat
humorné	humorný	k2eAgInPc1d1	humorný
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
finští	finský	k2eAgMnPc1d1	finský
vojáci	voják	k1gMnPc1	voják
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
nechtěně	chtěně	k6eNd1	chtěně
za	za	k7c7	za
frontovou	frontový	k2eAgFnSc7d1	frontová
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
oddávali	oddávat	k5eAaImAgMnP	oddávat
sauně	sauna	k1gFnSc3	sauna
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
bojovali	bojovat	k5eAaImAgMnP	bojovat
s	s	k7c7	s
Rusy	Rus	k1gMnPc4	Rus
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hormeze	Hormézt	k5eAaPmIp3nS	Hormézt
</s>
</p>
<p>
<s>
Termoterapie	termoterapie	k1gFnSc1	termoterapie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sauna	sauna	k1gFnSc1	sauna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sauna	sauna	k1gFnSc1	sauna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
MUDr.	MUDr.	kA	MUDr.
Antonín	Antonín	k1gMnSc1	Antonín
Mikolášek	Mikolášek	k1gMnSc1	Mikolášek
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
sauna	sauna	k1gFnSc1	sauna
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
portál	portál	k1gInSc1	portál
o	o	k7c6	o
saunování	saunování	k1gNnSc6	saunování
</s>
</p>
<p>
<s>
Saunování	saunování	k1gNnSc1	saunování
</s>
</p>
