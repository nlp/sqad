<p>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS	Dwaynout
Douglas	Douglas	k1gMnSc1	Douglas
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Hayward	Hayward	k1gInSc1	Hayward
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k1gMnSc1	známý
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
zápasnickou	zápasnický	k2eAgFnSc4d1	zápasnická
přezdívkou	přezdívka	k1gFnSc7	přezdívka
''	''	k?	''
<g/>
The	The	k1gFnSc1	The
Rock	rock	k1gInSc1	rock
<g/>
''	''	k?	''
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgMnSc1d1	profesionální
wrestler	wrestler	k1gMnSc1	wrestler
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
praotce	praotec	k1gMnSc2	praotec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
opět	opět	k6eAd1	opět
působí	působit	k5eAaImIp3nP	působit
ve	v	k7c6	v
WWE	WWE	kA	WWE
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
zápasníků	zápasník	k1gMnPc2	zápasník
ve	v	k7c6	v
wrestlingu	wrestling	k1gInSc6	wrestling
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
také	také	k9	také
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
The	The	k1gFnSc2	The
Rock	rock	k1gInSc1	rock
dal	dát	k5eAaPmAgInS	dát
na	na	k7c4	na
wrestling	wrestling	k1gInSc4	wrestling
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
7	[number]	k4	7
<g/>
×	×	k?	×
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
role	role	k1gFnSc2	role
Škorpiona	škorpion	k1gMnSc2	škorpion
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Mumie	mumie	k1gFnSc2	mumie
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Brendana	Brendan	k1gMnSc2	Brendan
Frasera	Fraser	k1gMnSc2	Fraser
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
nejdražší	drahý	k2eAgFnSc2d3	nejdražší
role	role	k1gFnSc2	role
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Král	Král	k1gMnSc1	Král
Škorpion	škorpion	k1gMnSc1	škorpion
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
filmovým	filmový	k2eAgMnSc7d1	filmový
nástupcem	nástupce	k1gMnSc7	nástupce
Arnolda	Arnold	k1gMnSc2	Arnold
Schwarzenegra	Schwarzenegr	k1gMnSc2	Schwarzenegr
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
seriálech	seriál	k1gInPc6	seriál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Kouzelníci	kouzelník	k1gMnPc1	kouzelník
z	z	k7c2	z
Waverly	Waverla	k1gFnSc2	Waverla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Griffinovi	Griffinův	k2eAgMnPc1d1	Griffinův
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
filmy	film	k1gInPc4	film
patří	patřit	k5eAaImIp3nS	patřit
např	např	kA	např
filmová	filmový	k2eAgFnSc1d1	filmová
série	série	k1gFnSc1	série
Rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
Zběsile	zběsile	k6eAd1	zběsile
<g/>
,	,	kIx,	,
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
hlídka	hlídka	k1gFnSc1	hlídka
a	a	k8xC	a
Jumanji	Jumanj	k1gFnSc6	Jumanj
<g/>
:	:	kIx,	:
<g/>
Vítejte	vítat	k5eAaImRp2nP	vítat
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
ve	v	k7c6	v
spoustě	spousta	k1gFnSc6	spousta
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
filmech	film	k1gInPc6	film
o	o	k7c6	o
wrestlingu	wrestling	k1gInSc6	wrestling
a	a	k8xC	a
dokumentech	dokument	k1gInPc6	dokument
<g/>
..	..	k?	..
Hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
akčních	akční	k2eAgInPc6d1	akční
filmech	film	k1gInPc6	film
a	a	k8xC	a
komediích	komedie	k1gFnPc6	komedie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
sérii	série	k1gFnSc6	série
Rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
Zběsile	zběsile	k6eAd1	zběsile
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pobřežní	pobřežní	k2eAgFnSc3d1	pobřežní
Hlídce	hlídka	k1gFnSc3	hlídka
<g/>
,	,	kIx,	,
Jumanji	Jumanj	k1gFnSc6	Jumanj
<g/>
:	:	kIx,	:
<g/>
Vítejte	vítat	k5eAaImRp2nP	vítat
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
a	a	k8xC	a
Rampage	Rampage	k1gFnSc6	Rampage
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
den	den	k1gInSc4	den
po	po	k7c4	po
jeho	jeho	k3xOp3gNnPc2	jeho
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozeninách	narozeniny	k1gFnPc6	narozeniny
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Danny	Dann	k1gMnPc7	Dann
Garciou	Garciá	k1gFnSc4	Garciá
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
dceru	dcera	k1gFnSc4	dcera
Simone	Simon	k1gMnSc5	Simon
Alexandru	Alexandr	k1gMnSc3	Alexandr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Johnson	Johnson	k1gMnSc1	Johnson
a	a	k8xC	a
Garcia	Garcia	k1gFnSc1	Garcia
se	se	k3xPyFc4	se
po	po	k7c6	po
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
manželství	manželství	k1gNnSc2	manželství
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
rozvod	rozvod	k1gInSc1	rozvod
byl	být	k5eAaImAgInS	být
přátelský	přátelský	k2eAgInSc1d1	přátelský
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
chystají	chystat	k5eAaImIp3nP	chystat
strávit	strávit	k5eAaPmF	strávit
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
napsat	napsat	k5eAaBmF	napsat
Joe	Joe	k1gMnSc1	Joe
Layden	Laydna	k1gFnPc2	Laydna
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rock	rock	k1gInSc1	rock
Says	Says	k1gInSc1	Says
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
bestsellerů	bestseller	k1gInPc2	bestseller
od	od	k7c2	od
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
přítel	přítel	k1gMnSc1	přítel
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
guvernérem	guvernér	k1gMnSc7	guvernér
Arnoldem	Arnold	k1gMnSc7	Arnold
Schwarzeneggerem	Schwarzenegger	k1gMnSc7	Schwarzenegger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ve	v	k7c6	v
wrestlingu	wrestling	k1gInSc6	wrestling
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ukončovací	ukončovací	k2eAgInPc1d1	ukončovací
chvaty	chvat	k1gInPc1	chvat
===	===	k?	===
</s>
</p>
<p>
<s>
People	People	k6eAd1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Elbow	Elbow	k1gFnSc7	Elbow
/	/	kIx~	/
Corporate	Corporat	k1gMnSc5	Corporat
Elbow	Elbow	k1gMnSc5	Elbow
</s>
</p>
<p>
<s>
Rock	rock	k1gInSc1	rock
Bottom	Bottom	k1gInSc1	Bottom
(	(	kIx(	(
<g/>
Lifting	Lifting	k1gInSc1	Lifting
side	side	k1gNnSc1	side
slam	slam	k1gInSc1	slam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Running	Running	k1gInSc1	Running
shoulderbreaker	shoulderbreaker	k1gInSc1	shoulderbreaker
-	-	kIx~	-
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
chvaty	chvat	k1gInPc1	chvat
===	===	k?	===
</s>
</p>
<p>
<s>
Diving	Diving	k1gInSc1	Diving
Crossbody	Crossboda	k1gFnSc2	Crossboda
</s>
</p>
<p>
<s>
Dropkick	Dropkick	k6eAd1	Dropkick
</s>
</p>
<p>
<s>
Float-over	Floatver	k1gMnSc1	Float-over
DDT	DDT	kA	DDT
</s>
</p>
<p>
<s>
Flowing	Flowing	k1gInSc1	Flowing
snap	snap	k1gInSc1	snap
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
kip-up	kipp	k1gInSc1	kip-up
</s>
</p>
<p>
<s>
Flying	Flying	k1gInSc1	Flying
clothesline	clotheslin	k1gInSc5	clotheslin
</s>
</p>
<p>
<s>
Powerslam	Powerslam	k1gInSc1	Powerslam
-	-	kIx~	-
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Running	Running	k1gInSc1	Running
swinging	swinging	k1gInSc1	swinging
neckbreaker	neckbreaker	k1gInSc1	neckbreaker
</s>
</p>
<p>
<s>
Running	Running	k1gInSc1	Running
thrust	thrust	k1gInSc1	thrust
lariat	lariat	k2eAgInSc1d1	lariat
</s>
</p>
<p>
<s>
Samoan	Samoan	k1gMnSc1	Samoan
drop	drop	k1gMnSc1	drop
</s>
</p>
<p>
<s>
Sharpshooter	Sharpshooter	k1gInSc1	Sharpshooter
-	-	kIx~	-
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k9	jako
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
Owenovi	Owen	k1gMnSc3	Owen
Hartovi	Hart	k1gMnSc3	Hart
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Snap	Snap	k1gMnSc1	Snap
overhead	overhead	k6eAd1	overhead
belly-to-belly	bellyoell	k1gMnPc4	belly-to-bell
suplex	suplex	k1gInSc1	suplex
</s>
</p>
<p>
<s>
Spinebuster	Spinebuster	k1gMnSc1	Spinebuster
</s>
</p>
<p>
<s>
===	===	k?	===
Manažeři	manažer	k1gMnPc1	manažer
===	===	k?	===
</s>
</p>
<p>
<s>
DebraVince	DebraVinko	k6eAd1	DebraVinko
McMahonShane	McMahonShan	k1gInSc5	McMahonShan
McMahon	McMahon	k1gMnSc1	McMahon
</s>
</p>
<p>
<s>
===	===	k?	===
Přezdívky	přezdívka	k1gFnSc2	přezdívka
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
People	People	k1gMnPc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Champion	Champion	k1gInSc1	Champion
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Brahma	Brahmum	k1gNnPc1	Brahmum
Bull	bulla	k1gFnPc2	bulla
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Corporate	Corporat	k1gInSc5	Corporat
Champion	Champion	k1gInSc1	Champion
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Great	Great	k2eAgMnSc1d1	Great
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Most	most	k1gInSc1	most
Electrifying	Electrifying	k1gInSc1	Electrifying
Man	mana	k1gFnPc2	mana
in	in	k?	in
Sports	Sports	k1gInSc1	Sports
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Most	most	k1gInSc1	most
Electrifying	Electrifying	k1gInSc1	Electrifying
Man	mana	k1gFnPc2	mana
in	in	k?	in
All	All	k1gMnSc1	All
of	of	k?	of
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
"	"	kIx"	"
<g/>
Theme	Them	k1gInSc5	Them
songy	song	k1gInPc1	song
</s>
</p>
<p>
<s>
World	World	k6eAd1	World
Wrestling	Wrestling	k1gInSc1	Wrestling
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
/	/	kIx~	/
<g/>
Federation	Federation	k1gInSc1	Federation
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
Rocky	rock	k1gInPc1	rock
Malvia	Malvius	k1gMnSc2	Malvius
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Destiny	Destin	k2eAgFnPc4d1	Destin
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
The	The	k1gFnSc1	The
Rock	rock	k1gInSc1	rock
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nation	Nation	k1gInSc1	Nation
Of	Of	k1gMnSc1	Of
Domination	Domination	k1gInSc1	Domination
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Do	do	k7c2	do
You	You	k1gFnSc2	You
Smell	Smell	k1gMnSc1	Smell
It	It	k1gMnSc1	It
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Know	Know	k1gFnSc1	Know
Your	Your	k1gInSc1	Your
Role	role	k1gFnSc1	role
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Know	Know	k1gFnSc1	Know
Your	Your	k1gInSc1	Your
Role	role	k1gFnSc1	role
<g/>
"	"	kIx"	"
od	od	k7c2	od
Method	Methoda	k1gFnPc2	Methoda
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
If	If	k1gMnSc1	If
You	You	k1gMnSc1	You
Smell	Smell	k1gMnSc1	Smell
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Is	Is	k1gMnSc1	Is
Cookin	Cookin	k1gMnSc1	Cookin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
<g/>
;	;	kIx,	;
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Electrifying	Electrifying	k1gInSc4	Electrifying
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jima	Jimus	k1gMnSc2	Jimus
Johnstona	Johnston	k1gMnSc2	Johnston
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
-současná	oučasný	k2eAgFnSc1d1	-současný
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Šampionáty	šampionát	k1gInPc4	šampionát
a	a	k8xC	a
ocenění	ocenění	k1gNnSc4	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Wrestling	Wrestling	k1gInSc4	Wrestling
Illustrated	Illustrated	k1gMnSc1	Illustrated
</s>
</p>
<p>
<s>
PWI	PWI	kA	PWI
Zápas	zápas	k1gInSc1	zápas
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Mankind	Mankind	k1gInSc1	Mankind
v	v	k7c6	v
"	"	kIx"	"
<g/>
I	i	k9	i
Quit	Quit	k1gInSc1	Quit
<g/>
"	"	kIx"	"
zápase	zápas	k1gInSc6	zápas
na	na	k7c4	na
Royal	Royal	k1gInSc4	Royal
Rumble	Rumble	k1gFnSc2	Rumble
</s>
</p>
<p>
<s>
PWI	PWI	kA	PWI
Zápas	zápas	k1gInSc1	zápas
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Hulk	Hulk	k1gMnSc1	Hulk
Hogan	Hogan	k1gMnSc1	Hogan
na	na	k7c6	na
WrestleManii	WrestleManie	k1gFnSc6	WrestleManie
X8	X8	k1gFnSc2	X8
</s>
</p>
<p>
<s>
PWI	PWI	kA	PWI
Nejpopulárnější	populární	k2eAgInSc4d3	nejpopulárnější
wrestler	wrestler	k1gInSc4	wrestler
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PWI	PWI	kA	PWI
Wrestler	Wrestler	k1gInSc1	Wrestler
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
top	topit	k5eAaImRp2nS	topit
500	[number]	k4	500
samostatných	samostatný	k2eAgInPc2d1	samostatný
wrestlerů	wrestler	k1gInPc2	wrestler
PWI	PWI	kA	PWI
500	[number]	k4	500
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
76	[number]	k4	76
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
top	topit	k5eAaImRp2nS	topit
500	[number]	k4	500
samostatných	samostatný	k2eAgInPc2d1	samostatný
wrestlerů	wrestler	k1gInPc2	wrestler
"	"	kIx"	"
<g/>
PWI	PWI	kA	PWI
Years	Years	k1gInSc1	Years
<g/>
"	"	kIx"	"
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Wrestling	Wrestling	k1gInSc1	Wrestling
Association	Association	k1gInSc4	Association
</s>
</p>
<p>
<s>
USWA	USWA	kA	USWA
Světový	světový	k2eAgInSc4d1	světový
Tag	tag	k1gInSc4	tag
Teamový	teamový	k2eAgMnSc1d1	teamový
šampion	šampion	k1gMnSc1	šampion
(	(	kIx(	(
<g/>
2	[number]	k4	2
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
Bartem	Bart	k1gMnSc7	Bart
Sawyerem	Sawyer	k1gMnSc7	Sawyer
</s>
</p>
<p>
<s>
World	World	k6eAd1	World
Wrestling	Wrestling	k1gInSc1	Wrestling
Federation	Federation	k1gInSc1	Federation
/	/	kIx~	/
World	World	k1gInSc1	World
Wrestling	Wrestling	k1gInSc1	Wrestling
Entertainment	Entertainment	k1gInSc1	Entertainment
</s>
</p>
<p>
<s>
WCW	WCW	kA	WCW
<g/>
/	/	kIx~	/
<g/>
Světový	světový	k2eAgInSc1d1	světový
šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
2	[number]	k4	2
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WWF	WWF	kA	WWF
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
8	[number]	k4	8
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WWF	WWF	kA	WWF
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
2	[number]	k4	2
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WWF	WWF	kA	WWF
Tag	tag	k1gInSc4	tag
Teamový	teamový	k2eAgInSc1d1	teamový
šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
5	[number]	k4	5
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
Mankindem	Mankind	k1gMnSc7	Mankind
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Undertakerem	Undertaker	k1gMnSc7	Undertaker
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chrisem	Chris	k1gInSc7	Chris
Jerichem	Jericho	k1gNnSc7	Jericho
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
Rumble	Rumble	k1gMnSc1	Rumble
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
Slammy	Slamma	k1gFnSc2	Slamma
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
Novou	nový	k2eAgFnSc4d1	nová
senzaci	senzace	k1gFnSc4	senzace
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slammy	Slamma	k1gFnPc1	Slamma
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
Měniče	měnič	k1gMnPc4	měnič
hry	hra	k1gFnSc2	hra
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Cenou	cena	k1gFnSc7	cena
</s>
</p>
<p>
<s>
Slammy	Slamma	k1gFnPc1	Slamma
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
Hádejte	hádat	k5eAaImRp2nP	hádat
kdo	kdo	k3yQnSc1	kdo
je	on	k3xPp3gFnPc4	on
zpět	zpět	k6eAd1	zpět
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šestinásobný	šestinásobný	k2eAgInSc1d1	šestinásobný
Triple	tripl	k1gInSc5	tripl
Crown	Crowno	k1gNnPc2	Crowno
šampion	šampion	k1gMnSc1	šampion
</s>
</p>
<p>
<s>
Wrestling	Wrestling	k1gInSc1	Wrestling
Observer	Observer	k1gMnSc1	Observer
Newsletter	Newsletter	k1gMnSc1	Newsletter
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
upoutávka	upoutávka	k1gFnSc1	upoutávka
pro	pro	k7c4	pro
podnik	podnik	k1gInSc4	podnik
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
gimmick	gimmick	k1gMnSc1	gimmick
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
rozhovory	rozhovor	k1gInPc1	rozhovor
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
charisma	charisma	k1gNnSc1	charisma
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zlepšení	zlepšení	k1gNnSc1	zlepšení
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
Wrestling	Wrestling	k1gInSc1	Wrestling
Observer	Observer	k1gMnSc1	Observer
Newsletter	Newsletter	k1gMnSc1	Newsletter
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dwayne	Dwayn	k1gInSc5	Dwayn
Johnson	Johnson	k1gInSc4	Johnson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS	Dwaynout
Johnson	Johnson	k1gMnSc1	Johnson
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS	Dwaynout
Johnson	Johnson	k1gMnSc1	Johnson
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS	Dwaynout
Johnson	Johnson	k1gMnSc1	Johnson
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
