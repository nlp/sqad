<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgMnSc1d1	profesionální
wrestler	wrestler	k1gMnSc1	wrestler
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
praotce	praotec	k1gMnSc2	praotec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
opět	opět	k6eAd1	opět
působí	působit	k5eAaImIp3nP	působit
ve	v	k7c6	v
WWE	WWE	kA	WWE
<g/>
?	?	kIx.	?
</s>
