<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
London	London	k1gMnSc1	London
Eye	Eye	k1gFnSc2	Eye
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc1d1	oficiální
označení	označení	k1gNnSc1	označení
Coca-Cola	cocaola	k1gFnSc1	coca-cola
London	London	k1gMnSc1	London
Eye	Eye	k1gMnSc1	Eye
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
označováno	označován	k2eAgNnSc4d1	označováno
Millennium	millennium	k1gNnSc4	millennium
Wheel	Wheela	k1gFnPc2	Wheela
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Kolo	kolo	k1gNnSc1	kolo
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
největší	veliký	k2eAgFnSc4d3	veliký
vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
i	i	k8xC	i
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
135	[number]	k4	135
m	m	kA	m
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Jubilee	Jubilee	k1gFnSc1	Jubilee
Gardens	Gardens	k1gInSc1	Gardens
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
nábřeží	nábřeží	k1gNnSc6	nábřeží
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
v	v	k7c6	v
Londýnském	londýnský	k2eAgInSc6d1	londýnský
obvodu	obvod	k1gInSc6	obvod
Lambeth	Lambetha	k1gFnPc2	Lambetha
mezi	mezi	k7c7	mezi
Westminsterským	Westminsterský	k2eAgInSc7d1	Westminsterský
a	a	k8xC	a
Hungerfordským	Hungerfordský	k2eAgInSc7d1	Hungerfordský
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
County	Count	k1gInPc7	Count
Hall	Halla	k1gFnPc2	Halla
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
Temže	Temže	k1gFnSc2	Temže
stojí	stát	k5eAaImIp3nS	stát
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
návrhu	návrh	k1gInSc2	návrh
kola	kola	k1gFnSc1	kola
byli	být	k5eAaImAgMnP	být
architekti	architekt	k1gMnPc1	architekt
David	David	k1gMnSc1	David
Marks	Marksa	k1gFnPc2	Marksa
<g/>
,	,	kIx,	,
Jule	Jula	k1gFnSc6	Jula
Barfieldová	Barfieldová	k1gFnSc1	Barfieldová
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
Cook	Cook	k1gMnSc1	Cook
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Sparrowhawk	Sparrowhawk	k1gMnSc1	Sparrowhawk
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc1d1	Steven
Chilton	Chilton	k1gInSc1	Chilton
a	a	k8xC	a
Nic	nic	k6eAd1	nic
Bailey	Baile	k2eAgFnPc1d1	Baile
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
otáčecí	otáčecí	k2eAgFnSc4d1	otáčecí
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
vlastního	vlastní	k2eAgNnSc2d1	vlastní
kola	kolo	k1gNnSc2	kolo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
ložiska	ložisko	k1gNnPc1	ložisko
a	a	k8xC	a
náboj	náboj	k1gInSc1	náboj
kola	kolo	k1gNnSc2	kolo
byly	být	k5eAaImAgInP	být
vyrobeny	vyroben	k2eAgInPc1d1	vyroben
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
hřídel	hřídel	k1gFnSc1	hřídel
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
kolo	kolo	k1gNnSc1	kolo
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kabiny	kabina	k1gFnPc4	kabina
pro	pro	k7c4	pro
cestující	cestující	k1gFnPc4	cestující
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
lana	lano	k1gNnSc2	lano
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
nese	nést	k5eAaImIp3nS	nést
32	[number]	k4	32
klimatizovaných	klimatizovaný	k2eAgFnPc2d1	klimatizovaná
kabinek	kabinka	k1gFnPc2	kabinka
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
uchycených	uchycený	k2eAgInPc2d1	uchycený
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
kruhu	kruh	k1gInSc6	kruh
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
kabin	kabina	k1gFnPc2	kabina
je	být	k5eAaImIp3nS	být
0,26	[number]	k4	0,26
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
asi	asi	k9	asi
0,07	[number]	k4	0,07
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
)	)	kIx)	)
takže	takže	k8xS	takže
jedna	jeden	k4xCgFnSc1	jeden
otáčka	otáčka	k1gFnSc1	otáčka
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
běžně	běžně	k6eAd1	běžně
nezastavuje	zastavovat	k5eNaImIp3nS	zastavovat
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnPc1	cestující
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
pohodlně	pohodlně	k6eAd1	pohodlně
vystoupit	vystoupit	k5eAaPmF	vystoupit
a	a	k8xC	a
nastoupit	nastoupit	k5eAaPmF	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zastavení	zastavení	k1gNnSc1	zastavení
pohybu	pohyb	k1gInSc2	pohyb
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
invalidních	invalidní	k2eAgMnPc2d1	invalidní
nebo	nebo	k8xC	nebo
starších	starý	k2eAgMnPc2d2	starší
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
není	být	k5eNaImIp3nS	být
prvním	první	k4xOgInSc7	první
podobným	podobný	k2eAgInSc7d1	podobný
typem	typ	k1gInSc7	typ
kola	kolo	k1gNnSc2	kolo
stojícím	stojící	k2eAgInSc7d1	stojící
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgNnSc1d2	menší
kolo	kolo	k1gNnSc1	kolo
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
naproti	naproti	k6eAd1	naproti
stanice	stanice	k1gFnPc1	stanice
Earls	Earlsa	k1gFnPc2	Earlsa
Court	Courta	k1gFnPc2	Courta
<g/>
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
bylo	být	k5eAaImAgNnS	být
sestaveno	sestavit	k5eAaPmNgNnS	sestavit
po	po	k7c6	po
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
montáže	montáž	k1gFnSc2	montáž
dopraveny	dopraven	k2eAgInPc4d1	dopraven
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
po	po	k7c6	po
Temži	Temže	k1gFnSc6	Temže
a	a	k8xC	a
sestaveny	sestavit	k5eAaPmNgInP	sestavit
na	na	k7c6	na
plovoucích	plovoucí	k2eAgInPc6d1	plovoucí
pontonech	ponton	k1gInPc6	ponton
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bylo	být	k5eAaImAgNnS	být
kolo	kolo	k1gNnSc1	kolo
sestaveno	sestavit	k5eAaPmNgNnS	sestavit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyzvedáváno	vyzvedávat	k5eAaImNgNnS	vyzvedávat
jeřáby	jeřáb	k1gMnPc7	jeřáb
do	do	k7c2	do
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
bylo	být	k5eAaImAgNnS	být
vztyčováno	vztyčovat	k5eAaImNgNnS	vztyčovat
rychlostí	rychlost	k1gFnSc7	rychlost
2	[number]	k4	2
stupně	stupeň	k1gInSc2	stupeň
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
až	až	k9	až
do	do	k7c2	do
náklonu	náklon	k1gInSc2	náklon
65	[number]	k4	65
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
postaveno	postavit	k5eAaPmNgNnS	postavit
asi	asi	k9	asi
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stavbaři	stavbař	k1gMnPc1	stavbař
museli	muset	k5eAaImAgMnP	muset
provést	provést	k5eAaPmF	provést
přípravy	příprava	k1gFnSc2	příprava
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
fázi	fáze	k1gFnSc4	fáze
vztyčování	vztyčování	k1gNnSc2	vztyčování
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
kola	kolo	k1gNnSc2	kolo
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
700	[number]	k4	700
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
pohledu	pohled	k1gInSc2	pohled
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
lité	litý	k2eAgFnPc1d1	litá
ocelové	ocelový	k2eAgFnPc1d1	ocelová
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc4d1	hlavní
hřídel	hřídel	k1gFnSc4	hřídel
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
závěsy	závěs	k1gInPc1	závěs
a	a	k8xC	a
klouby	kloub	k1gInPc1	kloub
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
českou	český	k2eAgFnSc7d1	Česká
firmou	firma	k1gFnSc7	firma
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
bylo	být	k5eAaImAgNnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
zpřístupněno	zpřístupněn	k2eAgNnSc1d1	zpřístupněno
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
provozováno	provozován	k2eAgNnSc1d1	provozováno
společností	společnost	k1gFnSc7	společnost
Tussauds	Tussaudsa	k1gFnPc2	Tussaudsa
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
financuje	financovat	k5eAaBmIp3nS	financovat
je	být	k5eAaImIp3nS	být
British	British	k1gInSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
-	-	kIx~	-
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
jiná	jiný	k2eAgFnSc1d1	jiná
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
výraznou	výrazný	k2eAgFnSc7d1	výrazná
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
společností	společnost	k1gFnSc7	společnost
Pringels	Pringels	k1gInSc1	Pringels
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
světovou	světový	k2eAgFnSc7d1	světová
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
proletělo	proletět	k5eAaPmAgNnS	proletět
asi	asi	k9	asi
8,5	[number]	k4	8,5
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
i	i	k9	i
vlivem	vlivem	k7c2	vlivem
tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
bylo	být	k5eAaImAgNnS	být
původní	původní	k2eAgNnSc4d1	původní
povolení	povolení	k1gNnSc4	povolení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
platnost	platnost	k1gFnSc1	platnost
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
upraveno	upravit	k5eAaPmNgNnS	upravit
lambethskou	lambethský	k2eAgFnSc7d1	lambethský
radnicí	radnice	k1gFnSc7	radnice
na	na	k7c4	na
povolení	povolení	k1gNnSc4	povolení
trvalé	trvalá	k1gFnSc2	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
překonáno	překonán	k2eAgNnSc1d1	překonáno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
záměr	záměr	k1gInSc1	záměr
stavby	stavba	k1gFnSc2	stavba
dvou	dva	k4xCgFnPc2	dva
vyšších	vysoký	k2eAgFnPc2d2	vyšší
kol	kola	k1gFnPc2	kola
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
(	(	kIx(	(
<g/>
170	[number]	k4	170
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
(	(	kIx(	(
<g/>
200	[number]	k4	200
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
kola	kolo	k1gNnSc2	kolo
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
British	British	k1gInSc4	British
Airways	Airways	k1gInSc1	Airways
(	(	kIx(	(
<g/>
33	[number]	k4	33
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tussauds	Tussauds	k1gInSc1	Tussauds
Group	Group	k1gInSc1	Group
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
popularitě	popularita	k1gFnSc3	popularita
je	být	k5eAaImIp3nS	být
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
finančně	finančně	k6eAd1	finančně
ztrátové	ztrátový	k2eAgFnPc4d1	ztrátová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
dluhy	dluh	k1gInPc1	dluh
asi	asi	k9	asi
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
úroky	úrok	k1gInPc7	úrok
z	z	k7c2	z
nákladných	nákladný	k2eAgFnPc2d1	nákladná
půjček	půjčka	k1gFnPc2	půjčka
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
poplatky	poplatek	k1gInPc4	poplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4	[number]	k4	4
%	%	kIx~	%
z	z	k7c2	z
ceny	cena	k1gFnSc2	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
účtuje	účtovat	k5eAaImIp3nS	účtovat
společnost	společnost	k1gFnSc4	společnost
Tussaud	Tussauda	k1gFnPc2	Tussauda
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Waterloo	Waterloo	k1gNnSc1	Waterloo
station	station	k1gInSc1	station
Westminster	Westminster	k1gMnSc1	Westminster
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc1	trasa
Jubilee	Jubile	k1gFnPc1	Jubile
<g/>
,	,	kIx,	,
District	District	k1gInSc1	District
a	a	k8xC	a
Circle	Circle	k1gInSc1	Circle
)	)	kIx)	)
Waterloo	Waterloo	k1gNnSc1	Waterloo
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc1	trasa
Waterloo	Waterloo	k1gNnSc2	Waterloo
&	&	k?	&
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Bakerloo	Bakerloo	k1gNnSc1	Bakerloo
<g/>
,	,	kIx,	,
Jubilee	Jubilee	k1gNnSc1	Jubilee
a	a	k8xC	a
Northern	Northern	k1gNnSc1	Northern
)	)	kIx)	)
Waterloo	Waterloo	k1gNnSc1	Waterloo
Pier	pier	k1gInSc4	pier
Ruské	ruský	k2eAgNnSc1d1	ruské
kolo	kolo	k1gNnSc1	kolo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
oko	oko	k1gNnSc4	oko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Londýnské	londýnský	k2eAgNnSc4d1	Londýnské
oko	oko	k1gNnSc4	oko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.londoneye.com/	[url]	k?	http://www.londoneye.com/
-	-	kIx~	-
WWW	WWW	kA	WWW
stránky	stránka	k1gFnPc1	stránka
London	London	k1gMnSc1	London
Eye	Eye	k1gMnSc1	Eye
http://www.michaelpead.co.uk/photography/london/londoneye.shtml	[url]	k1gMnSc1	http://www.michaelpead.co.uk/photography/london/londoneye.shtml
-	-	kIx~	-
WWW	WWW	kA	WWW
stránky	stránka	k1gFnPc4	stránka
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
Michaela	Michael	k1gMnSc2	Michael
Peada	Pead	k1gMnSc2	Pead
http://www.skyscrapernews.com/buildings.php?id=284	[url]	k4	http://www.skyscrapernews.com/buildings.php?id=284
-	-	kIx~	-
kolekce	kolekce	k1gFnSc1	kolekce
obrázků	obrázek	k1gInPc2	obrázek
London	London	k1gMnSc1	London
Eye	Eye	k1gMnSc1	Eye
</s>
