<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgInSc4d1
</s>
<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc5d1
</s>
<s>
sasko-meiningenský	sasko-meiningenský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1649	#num#	k4
</s>
<s>
Gotha	Gotha	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1706	#num#	k4
</s>
<s>
Meiningen	Meiningen	k1gInSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Hedvika	Hedvika	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Sasko-MeiningenskýBernard	Sasko-MeiningenskýBernard	k1gMnSc1
Sasko-MeiningenskýJan	Sasko-MeiningenskýJan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Sasko-MeiningenskýMarie	Sasko-MeiningenskýMarie	k1gFnSc2
Alžběta	Alžběta	k1gFnSc1
Sasko-MeiningenskáJan	Sasko-MeiningenskáJan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Sasko-MeiningenskýFridrich	Sasko-MeiningenskýFridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-MeiningenskýJiří	Sasko-MeiningenskýJiř	k1gFnPc2
Arnošt	Arnošt	k1gMnSc1
Sasko-MeiningenskýAlžběta	Sasko-MeiningenskýAlžběta	k1gMnSc1
Ernestina	Ernestina	k1gFnSc1
Sasko-MeiningenskáEleonora	Sasko-MeiningenskáEleonor	k1gMnSc2
Frederika	Frederik	k1gMnSc2
Sasko-MeiningenskáAntonín	Sasko-MeiningenskáAntonín	k1gMnSc1
August	August	k1gMnSc1
Sasko-MeiningenskýVilemína	Sasko-MeiningenskýVilemín	k1gInSc2
Luisa	Luisa	k1gFnSc1
Sasko-MeiningenskáAntonín	Sasko-MeiningenskáAntonín	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Wettinové	Wettin	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Gothajský	sasko-gothajský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Sasko-Altenburská	Sasko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1649	#num#	k4
<g/>
,	,	kIx,
Gotha	Gotha	k1gFnSc1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1706	#num#	k4
<g/>
,	,	kIx,
Meiningen	Meiningen	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
sasko-meiningenským	sasko-meiningenský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Bernard	Bernard	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
šestý	šestý	k4xOgMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
třetí	třetí	k4xOgMnSc1
přeživší	přeživší	k2eAgMnSc1d1
syn	syn	k1gMnSc1
vévody	vévoda	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
I.	I.	kA
Sasko-Gothajského	sasko-gothajský	k2eAgInSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
Žofie	Žofie	k1gFnSc2
Sasko-Altenburské	Sasko-Altenburský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1675	#num#	k4
řídil	řídit	k5eAaImAgMnS
Bernard	Bernard	k1gMnSc1
společně	společně	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
bratry	bratr	k1gMnPc7
celé	celá	k1gFnSc2
vévodství	vévodství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1680	#num#	k4
<g/>
)	)	kIx)
však	však	k9
bylo	být	k5eAaImAgNnS
vévodství	vévodství	k1gNnSc1
rozděleno	rozdělen	k2eAgNnSc1d1
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
této	tento	k3xDgFnSc2
divizní	divizní	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
Beernard	Beernard	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
Meiningen	Meiningen	k1gInSc4
<g/>
,	,	kIx,
Wasungen	Wasungen	k1gInSc1
<g/>
,	,	kIx,
Salzungen	Salzungen	k1gInSc1
<g/>
,	,	kIx,
Untermaßfeld	Untermaßfeld	k1gInSc1
<g/>
,	,	kIx,
Frauenbreitungen	Frauenbreitungen	k1gInSc1
a	a	k8xC
Ichtershausen	Ichtershausen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernard	Bernard	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zakladatelem	zakladatel	k1gMnSc7
sasko-meiningenské	sasko-meiningenský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Okamžitě	okamžitě	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
stavbu	stavba	k1gFnSc4
oficiálního	oficiální	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
v	v	k7c6
Meiningenu	Meiningen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1692	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
nazvána	nazván	k2eAgFnSc1d1
palác	palác	k1gInSc1
Elisabethenburg	Elisabethenburg	k1gInSc4
na	na	k7c4
počest	počest	k1gFnSc4
Bernardovy	Bernardův	k2eAgFnSc2d1
druhé	druhý	k4xOgFnSc2
manželky	manželka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
finanční	finanční	k2eAgFnSc1d1
stabilita	stabilita	k1gFnSc1
Bernardova	Bernardův	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
pozoruhodná	pozoruhodný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
prodej	prodej	k1gInSc4
komorního	komorní	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
a	a	k8xC
dodatečný	dodatečný	k2eAgInSc4d1
poplatek	poplatek	k1gInSc4
za	za	k7c4
daně	daň	k1gFnPc4
obyvatelstvu	obyvatelstvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Bernardova	Bernardův	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
vůle	vůle	k1gFnSc1
nařídila	nařídit	k5eAaPmAgFnS
nedělitelnost	nedělitelnost	k1gFnSc4
vévodství	vévodství	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
primogenituru	primogenitura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gMnPc3
synům	syn	k1gMnPc3
vládnout	vládnout	k5eAaImF
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
vévodství	vévodství	k1gNnSc2
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
dvaadvacetiletý	dvaadvacetiletý	k2eAgMnSc1d1
Bernard	Bernard	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1671	#num#	k4
na	na	k7c6
zámku	zámek	k1gInSc6
Friedenstein	Friedensteina	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Gotha	Gotha	k1gFnSc1
s	s	k7c7
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
starší	starý	k2eAgFnSc7d2
Marií	Maria	k1gFnSc7
Hedvikou	Hedvika	k1gFnSc7
<g/>
,	,	kIx,
nejmladší	mladý	k2eAgFnSc7d3
dcerou	dcera	k1gFnSc7
lankraběte	lankrabě	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesensko-Darmstadtského	Hesensko-Darmstadtský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
spolu	spolu	k6eAd1
měli	mít	k5eAaImAgMnP
sedm	sedm	k4xCc1
dětíː	dětíː	k?
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1672	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1724	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bernard	Bernard	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1673	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1694	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1674	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1675	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1676	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1676	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1677	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1678	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1679	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1746	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1680	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1699	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Hedvika	Hedvika	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1680	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
32	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1681	#num#	k4
se	se	k3xPyFc4
jednatřicetiletý	jednatřicetiletý	k2eAgMnSc1d1
vdovec	vdovec	k1gMnSc1
v	v	k7c6
Schöningenu	Schöningen	k1gInSc6
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
o	o	k7c4
devět	devět	k4xCc4
let	léto	k1gNnPc2
mladší	mladý	k2eAgFnSc3d2
Alžbětou	Alžběta	k1gFnSc7
Eleonorou	Eleonora	k1gFnSc7
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc7d3
dcerou	dcera	k1gFnSc7
vévody	vévoda	k1gMnSc2
Antona	Anton	k1gMnSc2
Ulricha	Ulrich	k1gMnSc2
Brunšvicko-Wolfenbüttelského	Brunšvicko-Wolfenbüttelský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
spolu	spolu	k6eAd1
měli	mít	k5eAaImAgMnP
pět	pět	k4xCc4
dětíː	dětíː	k?
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Ernestina	Ernestina	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1681	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1766	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1683	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1739	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
August	August	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1684	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1684	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilemína	Vilemína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1686	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1753	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1687	#num#	k4
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1763	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jan	Jan	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Sibyla	Sibyla	k1gFnSc1
Klévská	Klévský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Výmarský	sasko-výmarský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcký	falcký	k2eAgInSc1d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Zuzana	Zuzana	k1gFnSc1
Simmernská	Simmernská	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Braniborsko-Kulmbašská	Braniborsko-Kulmbašský	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Gothajský	sasko-gothajský	k2eAgInSc5d1
</s>
<s>
Jan	Jan	k1gMnSc1
V.	V.	kA
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc5d1
</s>
<s>
Jáchym	Jáchym	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Anhaltský	Anhaltský	k2eAgMnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Braniborská	braniborský	k2eAgFnSc1d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
Anhaltská	Anhaltský	k2eAgFnSc1d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Braniborsko-Ansbašská	Braniborsko-Ansbašský	k2eAgFnSc1d1
</s>
<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Sasko-Výmarský	sasko-výmarský	k2eAgInSc1d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Zuzana	Zuzana	k1gFnSc1
Simmernská	Simmernská	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Filip	Filip	k1gMnSc1
Sasko-Altenburský	Sasko-Altenburský	k2eAgMnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Falcko-Neuburský	Falcko-Neuburský	k2eAgMnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Falcko-Neuburská	Falcko-Neuburský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Klevská	Klevský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Sasko-Altenburská	Sasko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Julius	Julius	k1gMnSc1
Brunšvicko-Lüneburský	Brunšvicko-Lüneburský	k2eAgMnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Julius	Julius	k1gMnSc1
Brunšvicko-Lüneburský	Brunšvicko-Lüneburský	k2eAgMnSc1d1
</s>
<s>
Hedvika	Hedvika	k1gFnSc1
Braniborská	braniborský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Frederik	Frederik	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Meklenburská	meklenburský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bernhard	Bernharda	k1gFnPc2
I	I	kA
<g/>
,	,	kIx,
Duke	Duke	k1gFnSc1
of	of	k?
Saxe-Meiningen	Saxe-Meiningen	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Sasko-meiningenský	Sasko-meiningenský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Nový	nový	k2eAgInSc1d1
titul	titul	k1gInSc1
</s>
<s>
1675	#num#	k4
<g/>
–	–	k?
<g/>
1706	#num#	k4
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Arnošt	Arnošt	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119549999	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5338	#num#	k4
0693	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2005006710	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
64819927	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2005006710	#num#	k4
</s>
