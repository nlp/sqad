<p>
<s>
Louisiana	Louisiana	k1gFnSc1	Louisiana
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
luˌ	luˌ	k?	luˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západních	západní	k2eAgInPc2d1	západní
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Louisiana	Louisiana	k1gFnSc1	Louisiana
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Mississippi	Mississippi	k1gFnSc7	Mississippi
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Arkansasem	Arkansas	k1gInSc7	Arkansas
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Texasem	Texas	k1gInSc7	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mexický	mexický	k2eAgInSc4d1	mexický
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
135	[number]	k4	135
382	[number]	k4	382
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Louisiana	Louisiana	k1gFnSc1	Louisiana
31	[number]	k4	31
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
4,7	[number]	k4	4,7
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
41	[number]	k4	41
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc2	rouge
s	s	k7c7	s
230	[number]	k4	230
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
New	New	k1gMnPc1	New
Orleans	Orleans	k1gInSc4	Orleans
s	s	k7c7	s
390	[number]	k4	390
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Shreveport	Shreveport	k1gInSc1	Shreveport
(	(	kIx(	(
<g/>
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lafayette	Lafayett	k1gMnSc5	Lafayett
(	(	kIx(	(
<g/>
130	[number]	k4	130
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lake	Lake	k1gNnSc1	Lake
Charles	Charles	k1gMnSc1	Charles
(	(	kIx(	(
<g/>
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Louisianě	Louisianě	k6eAd1	Louisianě
patří	patřit	k5eAaImIp3nS	patřit
639	[number]	k4	639
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Driskill	Driskilla	k1gFnPc2	Driskilla
Mountain	Mountain	k1gInSc4	Mountain
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
163	[number]	k4	163
m	m	kA	m
na	na	k7c6	na
severu	sever	k1gInSc6	sever
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pearl	Pearla	k1gFnPc2	Pearla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
Red	Red	k1gFnSc1	Red
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Louisiany	Louisiana	k1gFnSc2	Louisiana
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
španělští	španělský	k2eAgMnPc1d1	španělský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1528	[number]	k4	1528
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
trvalé	trvalý	k2eAgNnSc4d1	trvalé
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
zde	zde	k6eAd1	zde
však	však	k9	však
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
zabrali	zabrat	k5eAaPmAgMnP	zabrat
velkou	velká	k1gFnSc4	velká
část	část	k1gFnSc4	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Mississippi	Mississippi	k1gFnSc2	Mississippi
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
tzv.	tzv.	kA	tzv.
Nové	Nová	k1gFnSc2	Nová
Francie	Francie	k1gFnSc2	Francie
tvořila	tvořit	k5eAaImAgFnS	tvořit
kolonie	kolonie	k1gFnSc1	kolonie
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nárokovaná	nárokovaný	k2eAgFnSc1d1	nárokovaná
oblast	oblast	k1gFnSc1	oblast
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k9	až
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
zakládané	zakládaný	k2eAgFnPc1d1	zakládaná
evropské	evropský	k2eAgFnPc1d1	Evropská
osady	osada	k1gFnPc1	osada
se	se	k3xPyFc4	se
však	však	k9	však
koncentrovaly	koncentrovat	k5eAaBmAgFnP	koncentrovat
kolem	kolem	k7c2	kolem
delty	delta	k1gFnSc2	delta
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
získali	získat	k5eAaPmAgMnP	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
začlenili	začlenit	k5eAaPmAgMnP	začlenit
do	do	k7c2	do
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
francouzského	francouzský	k2eAgNnSc2d1	francouzské
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
se	se	k3xPyFc4	se
kolonie	kolonie	k1gFnSc1	kolonie
Louisiana	Louisiana	k1gFnSc1	Louisiana
vrátila	vrátit	k5eAaPmAgFnS	vrátit
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
celou	celá	k1gFnSc4	celá
odkoupily	odkoupit	k5eAaPmAgInP	odkoupit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
delty	delta	k1gFnSc2	delta
Mississippi	Mississippi	k1gFnSc2	Mississippi
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
orleanské	orleanský	k2eAgNnSc1d1	orleanský
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
převzalo	převzít	k5eAaPmAgNnS	převzít
název	název	k1gInSc4	název
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Louisiana	Louisiana	k1gFnSc1	Louisiana
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1812	[number]	k4	1812
stala	stát	k5eAaPmAgFnS	stát
18	[number]	k4	18
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Louisiana	Louisiana	k1gFnSc1	Louisiana
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
součástí	součást	k1gFnPc2	součást
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
připojena	připojen	k2eAgFnSc1d1	připojena
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
533	[number]	k4	533
372	[number]	k4	372
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Stát	stát	k1gInSc1	stát
Louisiana	Louisian	k1gMnSc2	Louisian
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
specifický	specifický	k2eAgInSc1d1	specifický
původem	původ	k1gInSc7	původ
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
region	region	k1gInSc4	region
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
zachoval	zachovat	k5eAaPmAgInS	zachovat
se	se	k3xPyFc4	se
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
i	i	k9	i
název	název	k1gInSc1	název
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
nebo	nebo	k8xC	nebo
Baton	baton	k1gInSc1	baton
Rouge	rouge	k1gFnSc2	rouge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkého	velký	k2eAgNnSc2d1	velké
hromadného	hromadný	k2eAgNnSc2d1	hromadné
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
oblasti	oblast	k1gFnSc3	oblast
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odsunovali	odsunovat	k5eAaImAgMnP	odsunovat
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgMnPc4d1	mluvící
obyvatele	obyvatel	k1gMnPc4	obyvatel
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Akádie	Akádie	k1gFnSc2	Akádie
<g/>
,	,	kIx,	,
dnešních	dnešní	k2eAgFnPc2d1	dnešní
kanadských	kanadský	k2eAgFnPc2d1	kanadská
provincií	provincie	k1gFnPc2	provincie
Nový	nový	k2eAgInSc4d1	nový
Brunšvik	Brunšvik	k1gInSc4	Brunšvik
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc4	ostrov
prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
francouzští	francouzský	k2eAgMnPc1d1	francouzský
Akáďané	Akáďan	k1gMnPc1	Akáďan
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
v	v	k7c6	v
bažinaté	bažinatý	k2eAgFnSc6d1	bažinatá
deltě	delta	k1gFnSc6	delta
největší	veliký	k2eAgFnSc2d3	veliký
americké	americký	k2eAgFnSc2d1	americká
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
okresech	okres	k1gInPc6	okres
převažují	převažovat	k5eAaImIp3nP	převažovat
francouzská	francouzský	k2eAgNnPc4d1	francouzské
příjmení	příjmení	k1gNnPc4	příjmení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
doma	doma	k6eAd1	doma
mluví	mluvit	k5eAaImIp3nS	mluvit
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kreolským	kreolský	k2eAgInSc7d1	kreolský
jazykem	jazyk	k1gInSc7	jazyk
vycházejícím	vycházející	k2eAgInSc7d1	vycházející
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
louisianská	louisianský	k2eAgFnSc1d1	louisianská
kreolština	kreolština	k1gFnSc1	kreolština
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
obývá	obývat	k5eAaImIp3nS	obývat
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
černochů	černoch	k1gMnPc2	černoch
<g/>
,	,	kIx,	,
potomků	potomek	k1gMnPc2	potomek
afrických	africký	k2eAgMnPc2d1	africký
otroků	otrok	k1gMnPc2	otrok
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
a	a	k8xC	a
původního	původní	k2eAgNnSc2d1	původní
britského	britský	k2eAgNnSc2d1	Britské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc4	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
převážně	převážně	k6eAd1	převážně
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
recepty	recept	k1gInPc7	recept
na	na	k7c4	na
pokrmy	pokrm	k1gInPc4	pokrm
vyrobené	vyrobený	k2eAgInPc4d1	vyrobený
z	z	k7c2	z
čerstvých	čerstvý	k2eAgInPc2d1	čerstvý
mořských	mořský	k2eAgInPc2d1	mořský
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
oslavami	oslava	k1gFnPc7	oslava
masopustu	masopust	k1gInSc2	masopust
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mardi	Mard	k1gMnPc1	Mard
Gras	Grasa	k1gFnPc2	Grasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Velký	velký	k2eAgInSc1d1	velký
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
života	život	k1gInSc2	život
zdejších	zdejší	k2eAgMnPc2d1	zdejší
lidí	člověk	k1gMnPc2	člověk
znamenal	znamenat	k5eAaImAgInS	znamenat
katastrofální	katastrofální	k2eAgInSc1d1	katastrofální
hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpustošil	zpustošit	k5eAaPmAgInS	zpustošit
pobřeží	pobřeží	k1gNnSc2	pobřeží
Alabamy	Alabam	k1gInPc4	Alabam
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnPc4	Mississippi
a	a	k8xC	a
Louisiany	Louisian	k1gInPc4	Louisian
a	a	k8xC	a
zaplavil	zaplavit	k5eAaPmAgMnS	zaplavit
město	město	k1gNnSc4	město
New	New	k1gMnSc2	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
provalily	provalit	k5eAaPmAgFnP	provalit
ochranné	ochranný	k2eAgFnPc1d1	ochranná
hráze	hráz	k1gFnPc1	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jih	jih	k1gInSc1	jih
Louisiany	Louisiana	k1gFnSc2	Louisiana
opět	opět	k6eAd1	opět
probouzí	probouzet	k5eAaImIp3nS	probouzet
k	k	k7c3	k
životu	život	k1gInSc3	život
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
investicemi	investice	k1gFnPc7	investice
plynoucími	plynoucí	k2eAgInPc7d1	plynoucí
od	od	k7c2	od
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Pitt	Pitt	k1gMnSc1	Pitt
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
staví	stavit	k5eAaBmIp3nS	stavit
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
nejchudší	chudý	k2eAgMnPc4d3	nejchudší
obyvatele	obyvatel	k1gMnPc4	obyvatel
domy	dům	k1gInPc1	dům
se	s	k7c7	s
solárními	solární	k2eAgInPc7d1	solární
panely	panel	k1gInPc7	panel
plné	plný	k2eAgFnSc2d1	plná
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
62,6	[number]	k4	62,6
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
32,0	[number]	k4	32,0
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
0,7	[number]	k4	0,7
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
1,5	[number]	k4	1,5
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,0	[number]	k4	0,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
1,5	[number]	k4	1,5
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
1,6	[number]	k4	1,6
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
4,2	[number]	k4	4,2
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
<g/>
Cajunové	Cajun	k1gMnPc1	Cajun
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
francouzských	francouzský	k2eAgInPc2d1	francouzský
Akáďanů	Akáďan	k1gInPc2	Akáďan
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
udrželi	udržet	k5eAaPmAgMnP	udržet
svou	svůj	k3xOyFgFnSc4	svůj
svéráznou	svérázný	k2eAgFnSc4d1	svérázná
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
90	[number]	k4	90
%	%	kIx~	%
</s>
</p>
<p>
<s>
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
60	[number]	k4	60
%	%	kIx~	%
</s>
</p>
<p>
<s>
baptisté	baptista	k1gMnPc1	baptista
–	–	k?	–
38	[number]	k4	38
%	%	kIx~	%
</s>
</p>
<p>
<s>
metodisté	metodista	k1gMnPc1	metodista
–	–	k?	–
4	[number]	k4	4
%	%	kIx~	%
</s>
</p>
<p>
<s>
letniční	letniční	k2eAgFnSc2d1	letniční
církve	církev	k1gFnSc2	církev
–	–	k?	–
2	[number]	k4	2
%	%	kIx~	%
</s>
</p>
<p>
<s>
jiní	jiný	k2eAgMnPc1d1	jiný
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
16	[number]	k4	16
%	%	kIx~	%
</s>
</p>
<p>
<s>
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
30	[number]	k4	30
%	%	kIx~	%
</s>
</p>
<p>
<s>
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
</s>
</p>
<p>
<s>
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
–	–	k?	–
<	<	kIx(	<
<g/>
1	[number]	k4	1
%	%	kIx~	%
</s>
</p>
<p>
<s>
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
–	–	k?	–
10	[number]	k4	10
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Kriminalita	kriminalita	k1gFnSc1	kriminalita
==	==	k?	==
</s>
</p>
<p>
<s>
Louisiana	Louisiana	k1gFnSc1	Louisiana
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
žebříčku	žebříček	k1gInSc2	žebříček
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vražd	vražda	k1gFnPc2	vražda
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
větší	veliký	k2eAgNnPc4d2	veliký
města	město	k1gNnPc4	město
jako	jako	k8xS	jako
New	New	k1gFnSc4	New
Orleans	Orleans	k1gInSc4	Orleans
a	a	k8xC	a
Baton	baton	k1gInSc4	baton
Rouge	rouge	k1gFnSc2	rouge
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gInPc4	jejich
sociálně	sociálně	k6eAd1	sociálně
vyloučené	vyloučený	k2eAgFnPc1d1	vyloučená
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
rušitelům	rušitel	k1gMnPc3	rušitel
zákona	zákon	k1gInSc2	zákon
nijak	nijak	k6eAd1	nijak
milosrdný	milosrdný	k2eAgMnSc1d1	milosrdný
-	-	kIx~	-
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
853	[number]	k4	853
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Louisiana	Louisiana	k1gFnSc1	Louisiana
na	na	k7c4	na
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
tresty	trest	k1gInPc4	trest
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
i	i	k9	i
pronikání	pronikání	k1gNnSc4	pronikání
soukromého	soukromý	k2eAgInSc2d1	soukromý
kapitálu	kapitál	k1gInSc2	kapitál
do	do	k7c2	do
vězeňského	vězeňský	k2eAgInSc2d1	vězeňský
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
budování	budování	k1gNnSc2	budování
soukromých	soukromý	k2eAgFnPc2d1	soukromá
věznic	věznice	k1gFnPc2	věznice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vězeňsko-průmyslový	vězeňskorůmyslový	k2eAgInSc1d1	vězeňsko-průmyslový
komplex	komplex	k1gInSc1	komplex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
182	[number]	k4	182
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Reaganovy	Reaganův	k2eAgFnSc2d1	Reaganova
války	válka	k1gFnSc2	válka
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
celonárodně	celonárodně	k6eAd1	celonárodně
novelizovány	novelizován	k2eAgInPc1d1	novelizován
tresty	trest	k1gInPc1	trest
za	za	k7c2	za
držení	držení	k1gNnSc2	držení
a	a	k8xC	a
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vyšším	vysoký	k2eAgFnPc3d2	vyšší
dobám	doba	k1gFnPc3	doba
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
odsouzených	odsouzený	k1gMnPc2	odsouzený
si	se	k3xPyFc3	se
tresty	trest	k1gInPc4	trest
odpykává	odpykávat	k5eAaImIp3nS	odpykávat
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
protidrogových	protidrogový	k2eAgInPc2d1	protidrogový
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
nebo	nebo	k8xC	nebo
vězení	vězení	k1gNnSc1	vězení
drženo	držet	k5eAaImNgNnS	držet
1,42	[number]	k4	1,42
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
50	[number]	k4	50
100	[number]	k4	100
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
držena	držet	k5eAaImNgFnS	držet
v	v	k7c6	v
soukromě	soukromě	k6eAd1	soukromě
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
for-profit	forrofita	k1gFnPc2	for-profita
(	(	kIx(	(
<g/>
ziskem	zisk	k1gInSc7	zisk
motivovaných	motivovaný	k2eAgFnPc6d1	motivovaná
<g/>
)	)	kIx)	)
věznicích	věznice	k1gFnPc6	věznice
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
vlastníky	vlastník	k1gMnPc4	vlastník
patří	patřit	k5eAaImIp3nP	patřit
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
stát	stát	k1gInSc4	stát
<g/>
)	)	kIx)	)
šerifové	šerif	k1gMnPc1	šerif
odlehlých	odlehlý	k2eAgInPc2d1	odlehlý
venkovských	venkovský	k2eAgInPc2d1	venkovský
okrsků	okrsek	k1gInPc2	okrsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
druhý	druhý	k4xOgInSc1	druhý
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
most	most	k1gInSc1	most
světa	svět	k1gInSc2	svět
-	-	kIx~	-
Lake	Lake	k1gInSc1	Lake
Pontchartrain	Pontchartraina	k1gFnPc2	Pontchartraina
Causeway	Causewaa	k1gFnSc2	Causewaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Louisiana	Louisiana	k1gFnSc1	Louisiana
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
USA	USA	kA	USA
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
evropské	evropský	k2eAgNnSc4d1	Evropské
právo	právo	k1gNnSc4	právo
francouzského	francouzský	k2eAgInSc2d1	francouzský
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
tvoří	tvořit	k5eAaImIp3nP	tvořit
angloamerické	angloamerický	k2eAgNnSc4d1	angloamerické
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Louisiana	Louisian	k1gMnSc2	Louisian
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
státu	stát	k1gInSc2	stát
Louisiana	Louisian	k1gMnSc2	Louisian
</s>
</p>
