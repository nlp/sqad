<p>
<s>
Polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
region	region	k1gInSc4	region
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
jsou	být	k5eAaImIp3nP	být
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
zčásti	zčásti	k6eAd1	zčásti
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
severní	severní	k2eAgFnSc1d1	severní
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Arktida	Arktida	k1gFnSc1	Arktida
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
jako	jako	k8xS	jako
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
vymezení	vymezení	k1gNnSc1	vymezení
jižní	jižní	k2eAgFnSc2d1	jižní
polární	polární	k2eAgFnSc2d1	polární
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
(	(	kIx(	(
<g/>
pevnina	pevnina	k1gFnSc1	pevnina
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
okolní	okolní	k2eAgNnSc1d1	okolní
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
jsou	být	k5eAaImIp3nP	být
poměry	poměr	k1gInPc1	poměr
složitější	složitý	k2eAgInPc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
pevnin	pevnina	k1gFnPc2	pevnina
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
části	část	k1gFnPc1	část
Arktidy	Arktida	k1gFnSc2	Arktida
oteplují	oteplovat	k5eAaImIp3nP	oteplovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
severní	severní	k2eAgFnSc1d1	severní
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
geograficky	geograficky	k6eAd1	geograficky
a	a	k8xC	a
klimatologicky	klimatologicky	k6eAd1	klimatologicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geografické	geografický	k2eAgNnSc4d1	geografické
vymezení	vymezení	k1gNnSc4	vymezení
===	===	k?	===
</s>
</p>
<p>
<s>
Polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
vymezena	vymezit	k5eAaPmNgFnS	vymezit
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
na	na	k7c4	na
66	[number]	k4	66
<g/>
°	°	k?	°
33	[number]	k4	33
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
66,55	[number]	k4	66,55
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
2	[number]	k4	2
602	[number]	k4	602
km	km	kA	km
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
a	a	k8xC	a
asi	asi	k9	asi
7	[number]	k4	7
383	[number]	k4	383
km	km	kA	km
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
poloha	poloha	k1gFnSc1	poloha
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
sklonem	sklon	k1gInSc7	sklon
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
vůči	vůči	k7c3	vůči
ekliptice	ekliptika	k1gFnSc3	ekliptika
(	(	kIx(	(
<g/>
asi	asi	k9	asi
23	[number]	k4	23
<g/>
°	°	k?	°
26	[number]	k4	26
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
pomalu	pomalu	k6eAd1	pomalu
mění	měnit	k5eAaImIp3nS	měnit
<g/>
:	:	kIx,	:
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
posouvá	posouvat	k5eAaImIp3nS	posouvat
asi	asi	k9	asi
o	o	k7c4	o
14,4	[number]	k4	14,4
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólům	pól	k1gInPc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c6	o
letním	letní	k2eAgInSc6d1	letní
slunovratu	slunovrat	k1gInSc6	slunovrat
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
Slunce	slunce	k1gNnSc2	slunce
vůbec	vůbec	k9	vůbec
nezapadne	zapadnout	k5eNaPmIp3nS	zapadnout
a	a	k8xC	a
o	o	k7c6	o
zimním	zimní	k2eAgInSc6d1	zimní
slunovratu	slunovrat	k1gInSc6	slunovrat
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
nevyjde	vyjít	k5eNaPmIp3nS	vyjít
nad	nad	k7c4	nad
obzor	obzor	k1gInSc4	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
lomu	lom	k1gInSc3	lom
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
pozorovat	pozorovat	k5eAaImF	pozorovat
asi	asi	k9	asi
od	od	k7c2	od
66	[number]	k4	66
stupně	stupeň	k1gInSc2	stupeň
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
od	od	k7c2	od
67	[number]	k4	67
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
zřetelnější	zřetelný	k2eAgInSc1d2	zřetelnější
<g/>
:	:	kIx,	:
asi	asi	k9	asi
od	od	k7c2	od
73	[number]	k4	73
<g/>
°	°	k?	°
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
tma	tma	k6eAd1	tma
<g/>
,	,	kIx,	,
od	od	k7c2	od
79	[number]	k4	79
<g/>
°	°	k?	°
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
nejjasnější	jasný	k2eAgFnPc4d3	nejjasnější
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
od	od	k7c2	od
85	[number]	k4	85
<g/>
°	°	k?	°
nelze	lze	k6eNd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
ani	ani	k8xC	ani
žádné	žádný	k3yNgNnSc4	žádný
svítání	svítání	k1gNnSc4	svítání
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
polární	polární	k2eAgInSc1d1	polární
den	den	k1gInSc4	den
a	a	k8xC	a
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
stále	stále	k6eAd1	stále
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c6	na
pólu	pól	k1gInSc6	pól
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
denní	denní	k2eAgInSc1d1	denní
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
mizí	mizet	k5eAaImIp3nS	mizet
a	a	k8xC	a
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
i	i	k9	i
"	"	kIx"	"
<g/>
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
trvají	trvat	k5eAaImIp3nP	trvat
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klimatologické	klimatologický	k2eAgNnSc4d1	klimatologické
vymezení	vymezení	k1gNnSc4	vymezení
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
polární	polární	k2eAgFnPc1d1	polární
oblasti	oblast	k1gFnPc1	oblast
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
obecně	obecně	k6eAd1	obecně
chladným	chladný	k2eAgNnSc7d1	chladné
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polární	polární	k2eAgFnSc6d1	polární
oblasti	oblast	k1gFnSc6	oblast
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pevninách	pevnina	k1gFnPc6	pevnina
a	a	k8xC	a
mořských	mořský	k2eAgInPc6d1	mořský
proudech	proud	k1gInPc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
Hudsonův	Hudsonův	k2eAgInSc1d1	Hudsonův
záliv	záliv	k1gInSc1	záliv
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
polovinu	polovina	k1gFnSc4	polovina
roku	rok	k1gInSc2	rok
zamrzlý	zamrzlý	k2eAgInSc1d1	zamrzlý
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
například	například	k6eAd1	například
norský	norský	k2eAgInSc1d1	norský
přístav	přístav	k1gInSc1	přístav
Hammerfest	Hammerfest	k1gInSc1	Hammerfest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
km	km	kA	km
severněji	severně	k6eAd2	severně
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
klimatologii	klimatologie	k1gFnSc6	klimatologie
za	za	k7c4	za
polární	polární	k2eAgFnSc4d1	polární
oblast	oblast	k1gFnSc4	oblast
pokládá	pokládat	k5eAaImIp3nS	pokládat
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
nejteplejším	teplý	k2eAgInSc6d3	nejteplejší
měsíci	měsíc	k1gInSc6	měsíc
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
červenec	červenec	k1gInSc1	červenec
<g/>
)	)	kIx)	)
střední	střední	k2eAgFnSc1d1	střední
teplota	teplota	k1gFnSc1	teplota
nevystoupí	vystoupit	k5eNaPmIp3nS	vystoupit
nad	nad	k7c7	nad
10	[number]	k4	10
<g/>
°	°	k?	°
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Klimatologický	klimatologický	k2eAgInSc1d1	klimatologický
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
zhruba	zhruba	k6eAd1	zhruba
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
od	od	k7c2	od
oblasti	oblast	k1gFnSc2	oblast
tundry	tundra	k1gFnSc2	tundra
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
severněji	severně	k6eAd2	severně
rostou	růst	k5eAaImIp3nP	růst
už	už	k6eAd1	už
jen	jen	k9	jen
některé	některý	k3yIgFnPc4	některý
trávy	tráva	k1gFnPc4	tráva
<g/>
,	,	kIx,	,
mechy	mech	k1gInPc4	mech
a	a	k8xC	a
lišejníky	lišejník	k1gInPc4	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drsných	drsný	k2eAgFnPc6d1	drsná
podmínkách	podmínka	k1gFnPc6	podmínka
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
žijí	žít	k5eAaImIp3nP	žít
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
–	–	k?	–
na	na	k7c6	na
severu	sever	k1gInSc6	sever
například	například	k6eAd1	například
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
tučňák	tučňák	k1gMnSc1	tučňák
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
hmyz	hmyz	k1gInSc1	hmyz
–	–	k?	–
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
těmto	tento	k3xDgFnPc3	tento
podmínkám	podmínka	k1gFnPc3	podmínka
přizpůsobeni	přizpůsoben	k2eAgMnPc1d1	přizpůsoben
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
trvale	trvale	k6eAd1	trvale
obydlené	obydlený	k2eAgNnSc1d1	obydlené
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
norský	norský	k2eAgInSc1d1	norský
Longyearbyen	Longyearbyen	k1gInSc1	Longyearbyen
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svalbard	Svalbarda	k1gFnPc2	Svalbarda
ve	v	k7c6	v
Špicberkách	Špicberky	k1gInPc6	Špicberky
na	na	k7c4	na
78	[number]	k4	78
<g/>
°	°	k?	°
13	[number]	k4	13
<g/>
́	́	k?	́
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
;	;	kIx,	;
Antarktida	Antarktida	k1gFnSc1	Antarktida
není	být	k5eNaImIp3nS	být
–	–	k?	–
kromě	kromě	k7c2	kromě
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
stanic	stanice	k1gFnPc2	stanice
–	–	k?	–
trvale	trvale	k6eAd1	trvale
osídlena	osídlen	k2eAgFnSc1d1	osídlena
<g/>
.	.	kIx.	.
</s>
<s>
Klimatická	klimatický	k2eAgNnPc1d1	klimatické
pozorování	pozorování	k1gNnPc1	pozorování
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
tzv.	tzv.	kA	tzv.
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Klimatologie	klimatologie	k1gFnSc1	klimatologie
</s>
</p>
<p>
<s>
Polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
</s>
</p>
<p>
<s>
Zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
pól	pól	k1gInSc1	pól
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polární	polární	k2eAgFnSc2d1	polární
podnebí	podnebí	k1gNnSc4	podnebí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgInS	Internationat
Polar	Polar	k1gInSc1	Polar
Foundation	Foundation	k1gInSc1	Foundation
</s>
</p>
<p>
<s>
Arctic	Arctice	k1gFnPc2	Arctice
Environmental	Environmental	k1gMnSc1	Environmental
Atlas	Atlas	k1gMnSc1	Atlas
(	(	kIx(	(
<g/>
UNDP	UNDP	kA	UNDP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Earth	Earth	k1gInSc1	Earth
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Polar	Polar	k1gInSc1	Polar
Regions	Regions	k1gInSc1	Regions
on	on	k3xPp3gMnSc1	on
Windows	Windows	kA	Windows
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Universe	Universe	k1gFnSc5	Universe
</s>
</p>
<p>
<s>
Arctic	Arctice	k1gFnPc2	Arctice
Studies	Studiesa	k1gFnPc2	Studiesa
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Smithsonian	Smithsonian	k1gInSc1	Smithsonian
Institution	Institution	k1gInSc1	Institution
</s>
</p>
<p>
<s>
Scott	Scott	k1gMnSc1	Scott
Polar	Polar	k1gMnSc1	Polar
Research	Research	k1gMnSc1	Research
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc1	Cambridge
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Dvakráte	Dvakráte	k1gMnSc1	Dvakráte
posunuté	posunutý	k2eAgNnSc4d1	posunuté
jaro	jaro	k1gNnSc4	jaro
<g/>
:	:	kIx,	:
ekologické	ekologický	k2eAgFnSc2d1	ekologická
sondy	sonda	k1gFnSc2	sonda
k	k	k7c3	k
Sahaře	Sahara	k1gFnSc3	Sahara
a	a	k8xC	a
polárnímu	polární	k2eAgInSc3d1	polární
kruhu	kruh	k1gInSc3	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
–	–	k?	–
151	[number]	k4	151
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Lennart	Lennart	k1gInSc1	Lennart
Meri	Mer	k1gFnSc2	Mer
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
klenbou	klenba	k1gFnSc7	klenba
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
1983	[number]	k4	1983
–	–	k?	–
351	[number]	k4	351
s.	s.	k?	s.
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Polargebiet	Polargebieta	k1gFnPc2	Polargebieta
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
