<s>
Dysprosium	Dysprosium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Dy	Dy	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Dysprosium	Dysprosium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
měkký	měkký	k2eAgInSc1d1
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
člen	člen	k1gInSc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>