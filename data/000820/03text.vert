<s>
Antonio	Antonio	k1gMnSc1	Antonio
Bonfini	Bonfin	k2eAgMnPc1d1	Bonfin
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Antonius	Antonius	k1gMnSc1	Antonius
Bonfinius	Bonfinius	k1gMnSc1	Bonfinius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
1427	[number]	k4	1427
Patrignone	Patrignon	k1gInSc5	Patrignon
<g/>
;	;	kIx,	;
†	†	k?	†
1503	[number]	k4	1503
Budín	Budín	k1gInSc1	Budín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
humanista	humanista	k1gMnSc1	humanista
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
jako	jako	k8xC	jako
historik	historik	k1gMnSc1	historik
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Bonfini	Bonfin	k1gMnPc1	Bonfin
byl	být	k5eAaImAgMnS	být
Korvínem	Korvín	k1gMnSc7	Korvín
pověřen	pověřit	k5eAaPmNgMnS	pověřit
vytvořit	vytvořit	k5eAaPmF	vytvořit
kroniku	kronika	k1gFnSc4	kronika
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Rerum	Rerum	k1gNnSc4	Rerum
Ungaricarum	Ungaricarum	k1gInSc1	Ungaricarum
decades	decades	k1gInSc1	decades
(	(	kIx(	(
<g/>
1497	[number]	k4	1497
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
uherskou	uherský	k2eAgFnSc4d1	uherská
humanistickou	humanistický	k2eAgFnSc4d1	humanistická
historiografii	historiografie	k1gFnSc4	historiografie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
úplné	úplný	k2eAgFnPc1d1	úplná
vydaní	vydaný	k2eAgMnPc1d1	vydaný
díla	dílo	k1gNnPc4	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1568	[number]	k4	1568
zásluhou	zásluha	k1gFnSc7	zásluha
J.	J.	kA	J.
Sambuca	Sambucus	k1gMnSc2	Sambucus
<g/>
.	.	kIx.	.
</s>
