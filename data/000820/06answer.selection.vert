<s>
Antonio	Antonio	k1gMnSc1	Antonio
Bonfini	Bonfin	k2eAgMnPc1d1	Bonfin
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Antonius	Antonius	k1gMnSc1	Antonius
Bonfinius	Bonfinius	k1gMnSc1	Bonfinius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
1427	[number]	k4	1427
Patrignone	Patrignon	k1gInSc5	Patrignon
<g/>
;	;	kIx,	;
†	†	k?	†
1503	[number]	k4	1503
Budín	Budín	k1gInSc1	Budín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
humanista	humanista	k1gMnSc1	humanista
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
jako	jako	k8xS	jako
historik	historik	k1gMnSc1	historik
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
