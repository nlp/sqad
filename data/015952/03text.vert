<s>
Románi	Román	k1gMnPc1
</s>
<s>
Románské	románský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
užívají	užívat	k5eAaImIp3nP
románské	románský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
španělština	španělština	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
portugalština	portugalština	k1gFnSc1
<g/>
,	,	kIx,
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
rumunština	rumunština	k1gFnSc1
</s>
<s>
Románi	Románi	k1gMnPc1
je	on	k3xPp3gNnSc4
označení	označení	k1gNnSc4
skupiny	skupina	k1gFnSc2
národů	národ	k1gInPc2
mluvících	mluvící	k2eAgInPc2d1
románskymi	románsky	k1gFnPc7
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
počítáni	počítán	k2eAgMnPc1d1
<g/>
:	:	kIx,
Španělé	Španěl	k1gMnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Katalánců	Katalánec	k1gMnPc2
a	a	k8xC
Galicijců	Galicijce	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Italové	Ital	k1gMnPc1
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
<g/>
,	,	kIx,
Portugalci	Portugalec	k1gMnPc1
<g/>
,	,	kIx,
Rumuni	Rumun	k1gMnPc1
(	(	kIx(
<g/>
také	také	k9
Moldavané	Moldavan	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
Valoni	Valon	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Románské	románský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Románské	románský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
se	se	k3xPyFc4
vyvinuly	vyvinout	k5eAaPmAgInP
z	z	k7c2
latiny	latina	k1gFnSc2
a	a	k8xC
vznikly	vzniknout	k5eAaPmAgInP
tedy	tedy	k9
na	na	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
ovládala	ovládat	k5eAaImAgFnS
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náleží	náležet	k5eAaImIp3nS
do	do	k7c2
skupiny	skupina	k1gFnSc2
indoevropských	indoevropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románskými	románský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
mluví	mluvit	k5eAaImIp3nS
asi	asi	k9
800	#num#	k4
miliónů	milión	k4xCgInPc2
lidí	člověk	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělština	španělština	k1gFnSc1
a	a	k8xC
portugalština	portugalština	k1gFnSc1
se	se	k3xPyFc4
prostřednictvím	prostřednictvím	k7c2
kolonizace	kolonizace	k1gFnSc2
rozšířily	rozšířit	k5eAaPmAgInP
do	do	k7c2
severní	severní	k2eAgFnSc2d1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc2d1
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
do	do	k7c2
některých	některý	k3yIgNnPc2
území	území	k1gNnPc2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Karibiku	Karibik	k1gInSc2
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gFnSc4
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
náboženské	náboženský	k2eAgNnSc4d1
vyznání	vyznání	k1gNnSc4
i	i	k8xC
genetické	genetický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
hluboce	hluboko	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
románský	románský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
říkat	říkat	k5eAaImF
latinská	latinský	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
má	mít	k5eAaImIp3nS
románské	románský	k2eAgMnPc4d1
předky	předek	k1gMnPc4
<g/>
,	,	kIx,
hlavně	hlavně	k9
španělské	španělský	k2eAgNnSc1d1
a	a	k8xC
portugalské	portugalský	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
částečně	částečně	k6eAd1
i	i	k9
italské	italský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
