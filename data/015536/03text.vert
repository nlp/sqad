<s>
Brad	brada	k1gFnPc2
Wilk	Wilka	k1gFnPc2
</s>
<s>
Brad	brada	k1gFnPc2
Wilk	Wilk	k1gMnSc1
Brad	brada	k1gFnPc2
Wilk	Wilk	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Bradley	Bradley	k1gInPc1
J.	J.	kA
Wilk	Wilk	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1968	#num#	k4
(	(	kIx(
<g/>
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Portland	Portland	k1gInSc1
<g/>
,	,	kIx,
Oregon	Oregon	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc7
</s>
<s>
alternativní	alternativní	k2eAgInSc1d1
metalrap	metalrap	k1gInSc1
metalfunk	metalfunk	k6eAd1
metalalternativní	metalalternativní	k2eAgInSc1d1
rockhard	rockhard	k1gInSc1
rockheavy	rockheava	k1gFnSc2
metal	metat	k5eAaImAgInS
Povolání	povolání	k1gNnSc4
</s>
<s>
hudebník	hudebník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
bicí	bicí	k2eAgFnPc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
1988	#num#	k4
<g/>
−	−	k?
<g/>
dosud	dosud	k6eAd1
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Epic	Epic	k1gFnSc1
Records	Recordsa	k1gFnPc2
<g/>
,	,	kIx,
Interscope	Interscop	k1gInSc5
Records	Records	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Rage	Rage	k6eAd1
Against	Against	k1gMnSc1
the	the	k?
MachineAudioslaveBlack	MachineAudioslaveBlack	k1gMnSc1
Sabbath	Sabbath	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brad	brada	k1gFnPc2
Wilk	Wilka	k1gFnPc2
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1968	#num#	k4
Portland	Portland	k1gInSc1
<g/>
,	,	kIx,
Oregon	Oregon	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
bubeník	bubeník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
spoluzaložil	spoluzaložit	k5eAaPmAgInS
skupinu	skupina	k1gFnSc4
Rage	Rage	k1gInSc1
Against	Against	k1gMnSc1
the	the	k?
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
hrál	hrát	k5eAaImAgInS
do	do	k7c2
jejího	její	k3xOp3gInSc2
rozpadu	rozpad	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgNnP
obnovena	obnovit	k5eAaPmNgNnP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
jejím	její	k3xOp3gMnSc7
členem	člen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neaktivním	aktivní	k2eNgNnSc6d1
období	období	k1gNnSc6
skupiny	skupina	k1gFnSc2
hrál	hrát	k5eAaImAgMnS
se	s	k7c7
skupinou	skupina	k1gFnSc7
Audioslave	Audioslav	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
přidal	přidat	k5eAaPmAgMnS
do	do	k7c2
skupiny	skupina	k1gFnSc2
Prophets	Prophets	k1gInSc1
of	of	k?
Rage	Rage	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracoval	spolupracovat	k5eAaImAgInS
se	s	k7c7
skupinou	skupina	k1gFnSc7
Black	Black	k1gMnSc1
Sabbath	Sabbath	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
nahrál	nahrát	k5eAaBmAgMnS,k5eAaPmAgMnS
album	album	k1gNnSc4
13	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
rodiny	rodina	k1gFnSc2
polsko-židovského	polsko-židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dětství	dětství	k1gNnSc2
žil	žít	k5eAaImAgMnS
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Kalifornie	Kalifornie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bicí	bicí	k2eAgFnSc6d1
začal	začít	k5eAaPmAgInS
hrát	hrát	k5eAaImF
poprvé	poprvé	k6eAd1
ve	v	k7c6
13	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
vzory	vzor	k1gInPc1
jsou	být	k5eAaImIp3nP
John	John	k1gMnSc1
Bonham	Bonham	k1gInSc1
<g/>
,	,	kIx,
Keith	Keith	k1gMnSc1
Moon	Moon	k1gMnSc1
a	a	k8xC
Elvin	Elvin	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mládí	mládí	k1gNnSc6
byl	být	k5eAaImAgInS
velkým	velký	k2eAgMnSc7d1
fanouškem	fanoušek	k1gMnSc7
Van	vana	k1gFnPc2
Halen	halit	k5eAaImNgInS
<g/>
,	,	kIx,
ve	v	k7c6
13	#num#	k4
letech	léto	k1gNnPc6
je	být	k5eAaImIp3nS
viděl	vidět	k5eAaImAgMnS
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
koncertě	koncert	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1997	#num#	k4
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
diagnostkován	diagnostkován	k2eAgInSc1d1
diabetes	diabetes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vegetarián	vegetarián	k1gMnSc1
a	a	k8xC
praktikující	praktikující	k2eAgMnSc1d1
buddhista	buddhista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BLACK	BLACK	kA
SABBATH	SABBATH	kA
<g/>
:	:	kIx,
New	New	k1gMnSc6
Album	album	k1gNnSc1
Title	titla	k1gFnSc3
Announced	Announced	k1gInSc1
<g/>
;	;	kIx,
Recording	Recording	k1gInSc1
Drummer	Drummer	k1gMnSc1
Revealed	Revealed	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blabbermouth	Blabbermouth	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
2013-01-12	2013-01-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2012713289	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1121255094	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
7277	#num#	k4
5633	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010006582	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
259489326	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010006582	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
