<s>
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
pol.	pol.	k?	pol.
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
PR	pr	k0	pr
<g/>
,	,	kIx,	,
pol.	pol.	k?	pol.
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
RP	RP	kA	RP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
a	a	k8xC	a
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
Kaliningradská	kaliningradský	k2eAgFnSc1d1	Kaliningradská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
má	mít	k5eAaImIp3nS	mít
Polsko	Polsko	k1gNnSc1	Polsko
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
se	s	k7c7	s
770	[number]	k4	770
km	km	kA	km
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc4	hora
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
christianizace	christianizace	k1gFnSc2	christianizace
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
roku	rok	k1gInSc3	rok
1569	[number]	k4	1569
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mocných	mocný	k2eAgInPc2d1	mocný
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
trojím	trojí	k4xRgInSc6	trojí
dělení	dělení	k1gNnSc6	dělení
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
1772	[number]	k4	1772
<g/>
-	-	kIx~	-
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
jako	jako	k8xS	jako
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zemi	zem	k1gFnSc4	zem
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
postihla	postihnout	k5eAaPmAgFnS	postihnout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
38	[number]	k4	38
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
osmý	osmý	k4xOgMnSc1	osmý
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
dobám	doba	k1gFnPc3	doba
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
nábožensky	nábožensky	k6eAd1	nábožensky
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
do	do	k7c2	do
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgMnS	být
Krakov	Krakov	k1gInSc4	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
a	a	k8xC	a
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
názvu	název	k1gInSc2	název
kmene	kmen	k1gInSc2	kmen
Polanů	Polan	k1gMnPc2	Polan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Velkopolsku	Velkopolsko	k1gNnSc6	Velkopolsko
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Polané	polaný	k2eAgNnSc1d1	Polané
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
obývající	obývající	k2eAgFnPc4d1	obývající
otevřené	otevřený	k2eAgFnPc4d1	otevřená
pole	pole	k1gFnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
toho	ten	k3xDgInSc2	ten
kmene	kmen	k1gInSc2	kmen
bylo	být	k5eAaImAgNnS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Vislanů	Vislan	k1gMnPc2	Vislan
či	či	k8xC	či
Mazovčanů	Mazovčan	k1gMnPc2	Mazovčan
obývajících	obývající	k2eAgMnPc2d1	obývající
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
terra	terr	k1gMnSc2	terr
Poloniae	Polonia	k1gMnSc2	Polonia
-	-	kIx~	-
Polská	polský	k2eAgFnSc1d1	polská
zem	zem	k1gFnSc1	zem
nebo	nebo	k8xC	nebo
Regnum	Regnum	k1gNnSc1	Regnum
Poloniae	Polonia	k1gInSc2	Polonia
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
jako	jako	k8xC	jako
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
962	[number]	k4	962
<g/>
/	/	kIx~	/
<g/>
963	[number]	k4	963
a	a	k8xC	a
týkají	týkat	k5eAaImIp3nP	týkat
se	se	k3xPyFc4	se
knížete	kníže	k1gMnSc2	kníže
Měška	Měšek	k1gMnSc2	Měšek
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInPc3	první
historicky	historicky	k6eAd1	historicky
doloženým	doložený	k2eAgMnSc7d1	doložený
vládcem	vládce	k1gMnSc7	vládce
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
965	[number]	k4	965
Mieszko	Mieszka	k1gFnSc5	Mieszka
I	i	k9	i
/	/	kIx~	/
<g/>
Měško	Měško	k1gNnSc1	Měško
I	i	k8xC	i
<g/>
/	/	kIx~	/
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
českou	český	k2eAgFnSc4d1	Česká
princeznu	princezna	k1gFnSc4	princezna
Doubravu	Doubrava	k1gFnSc4	Doubrava
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
966	[number]	k4	966
za	za	k7c4	za
prostřednictví	prostřednictví	k1gNnSc4	prostřednictví
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
křesťanství	křesťanství	k1gNnPc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
Piastovci	Piastovec	k1gInSc3	Piastovec
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1025	[number]	k4	1025
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgInSc1d1	chrabrý
nechal	nechat	k5eAaPmAgInS	nechat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
polský	polský	k2eAgMnSc1d1	polský
vládce	vládce	k1gMnSc1	vládce
korunovat	korunovat	k5eAaBmF	korunovat
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
;	;	kIx,	;
titul	titul	k1gInSc1	titul
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Vladislav	Vladislava	k1gFnPc2	Vladislava
I.	I.	kA	I.
Lokýtek	lokýtek	k1gInSc1	lokýtek
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
sjednotit	sjednotit	k5eAaPmF	sjednotit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozdrobený	rozdrobený	k2eAgInSc4d1	rozdrobený
polský	polský	k2eAgInSc4d1	polský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1385	[number]	k4	1385
bylo	být	k5eAaImAgNnS	být
smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Krevě	Kreva	k1gFnSc6	Kreva
dohodnuto	dohodnut	k2eAgNnSc1d1	dohodnuto
spojení	spojení	k1gNnSc1	spojení
polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
s	s	k7c7	s
litevským	litevský	k2eAgNnSc7d1	litevské
velkoknížectvím	velkoknížectví	k1gNnSc7	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
polský	polský	k2eAgInSc4d1	polský
trůn	trůn	k1gInSc4	trůn
usedli	usednout	k5eAaPmAgMnP	usednout
litevští	litevský	k2eAgMnPc1d1	litevský
velkovévodové	velkovévoda	k1gMnPc1	velkovévoda
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
soustátí	soustátí	k1gNnSc1	soustátí
existovalo	existovat	k5eAaImAgNnS	existovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
volné	volný	k2eAgFnSc2d1	volná
polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
tzv.	tzv.	kA	tzv.
lublinskou	lublinský	k2eAgFnSc7d1	Lublinská
unií	unie	k1gFnSc7	unie
těsněji	těsně	k6eAd2	těsně
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozkvětu	rozkvět	k1gInSc3	rozkvět
polsko-litevského	polskoitevský	k2eAgNnSc2d1	polsko-litevské
dvojstátí	dvojstátí	k1gNnSc2	dvojstátí
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
provázelo	provázet	k5eAaImAgNnS	provázet
oslabování	oslabování	k1gNnSc4	oslabování
moci	moct	k5eAaImF	moct
panovníka	panovník	k1gMnSc4	panovník
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
růst	růst	k1gInSc4	růst
vlivu	vliv	k1gInSc2	vliv
početné	početný	k2eAgFnSc2d1	početná
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
nazýván	nazýván	k2eAgInSc4d1	nazýván
Rzecz	Rzecz	k1gInSc4	Rzecz
Pospolita	Pospolita	k1gFnSc1	Pospolita
Obojga	Obojga	k1gFnSc1	Obojga
Narodów	Narodów	k1gFnSc1	Narodów
(	(	kIx(	(
<g/>
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
obou	dva	k4xCgFnPc2	dva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
latinského	latinský	k2eAgInSc2d1	latinský
res	res	k?	res
publica	publicum	k1gNnPc4	publicum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Polskem	Polsko	k1gNnSc7	Polsko
otřáslo	otřást	k5eAaPmAgNnS	otřást
Chmelnického	Chmelnický	k2eAgNnSc2d1	Chmelnický
povstání	povstání	k1gNnSc2	povstání
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
nájezdy	nájezd	k1gInPc7	nájezd
krymských	krymský	k2eAgInPc2d1	krymský
Tatarů	tatar	k1gInPc2	tatar
<g/>
.	.	kIx.	.
</s>
<s>
Posilování	posilování	k1gNnSc1	posilování
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
rozbroje	rozbroj	k1gInPc4	rozbroj
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
oslabily	oslabit	k5eAaPmAgInP	oslabit
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1772	[number]	k4	1772
<g/>
,	,	kIx,	,
1793	[number]	k4	1793
a	a	k8xC	a
1795	[number]	k4	1795
si	se	k3xPyFc3	se
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
v	v	k7c6	v
trojím	trojí	k4xRgNnSc6	trojí
dělení	dělení	k1gNnSc6	dělení
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
Polsko	Polsko	k1gNnSc1	Polsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
1815	[number]	k4	1815
zřídil	zřídit	k5eAaPmAgMnS	zřídit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
na	na	k7c4	na
části	část	k1gFnPc4	část
území	území	k1gNnPc2	území
předtím	předtím	k6eAd1	předtím
zabraným	zabraný	k2eAgNnSc7d1	zabrané
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Napoleonovým	Napoleonův	k2eAgMnSc7d1	Napoleonův
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
porážce	porážka	k1gFnSc6	porážka
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
autonomní	autonomní	k2eAgNnSc1d1	autonomní
Království	království	k1gNnSc1	království
polské	polský	k2eAgNnSc1d1	polské
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgNnSc1d1	podléhající
ruskému	ruský	k2eAgMnSc3d1	ruský
carovi	car	k1gMnSc3	car
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
Listopadové	listopadový	k2eAgNnSc1d1	listopadové
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
národních	národní	k2eAgNnPc2d1	národní
povstání	povstání	k1gNnPc2	povstání
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
carskou	carský	k2eAgFnSc7d1	carská
armádou	armáda	k1gFnSc7	armáda
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
;	;	kIx,	;
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
jeho	jeho	k3xOp3gFnSc2	jeho
autonomie	autonomie	k1gFnSc2	autonomie
knížectví	knížectví	k1gNnSc6	knížectví
omezena	omezit	k5eAaPmNgFnS	omezit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
zcela	zcela	k6eAd1	zcela
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
obsazená	obsazený	k2eAgFnSc1d1	obsazená
Rakouskem	Rakousko	k1gNnSc7	Rakousko
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Království	království	k1gNnSc2	království
haličsko-vladiměřské	haličskoladiměřský	k2eAgNnSc1d1	haličsko-vladiměřský
(	(	kIx(	(
<g/>
s	s	k7c7	s
autonomní	autonomní	k2eAgFnSc7d1	autonomní
vládou	vláda	k1gFnSc7	vláda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Krakov	Krakov	k1gInSc1	Krakov
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1846	[number]	k4	1846
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
Krakovskou	krakovský	k2eAgFnSc7d1	Krakovská
republikou	republika	k1gFnSc7	republika
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
tří	tři	k4xCgFnPc2	tři
sousedních	sousední	k2eAgFnPc2d1	sousední
mocností	mocnost	k1gFnPc2	mocnost
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
Krakovského	krakovský	k2eAgNnSc2d1	Krakovské
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Rakouské	rakouský	k2eAgFnSc3d1	rakouská
Haliči	Halič	k1gFnSc3	Halič
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
obývali	obývat	k5eAaImAgMnP	obývat
převážně	převážně	k6eAd1	převážně
Malorusové	Malorus	k1gMnPc1	Malorus
(	(	kIx(	(
<g/>
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
národnostní	národnostní	k2eAgFnPc4d1	národnostní
třenice	třenice	k1gFnPc4	třenice
a	a	k8xC	a
relativní	relativní	k2eAgFnSc4d1	relativní
zaostalost	zaostalost	k1gFnSc4	zaostalost
byla	být	k5eAaImAgFnS	být
rakouská	rakouský	k2eAgFnSc1d1	rakouská
část	část	k1gFnSc1	část
s	s	k7c7	s
městy	město	k1gNnPc7	město
Krakov	Krakov	k1gInSc1	Krakov
a	a	k8xC	a
Lvov	Lvov	k1gInSc1	Lvov
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
mírnějšímu	mírný	k2eAgInSc3d2	mírnější
pronásledování	pronásledování	k1gNnSc4	pronásledování
hlavní	hlavní	k2eAgFnSc7d1	hlavní
scénou	scéna	k1gFnSc7	scéna
polské	polský	k2eAgFnSc2d1	polská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
obsazené	obsazený	k2eAgNnSc1d1	obsazené
Pruskem	Prusko	k1gNnSc7	Prusko
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
Poznaňské	poznaňský	k2eAgNnSc1d1	poznaňské
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
(	(	kIx(	(
<g/>
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Provincie	provincie	k1gFnSc2	provincie
Poznaň	Poznaň	k1gFnSc1	Poznaň
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
autonomie	autonomie	k1gFnSc1	autonomie
rovněž	rovněž	k9	rovněž
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
rozvinuté	rozvinutý	k2eAgNnSc1d1	rozvinuté
Slezsko	Slezsko	k1gNnSc1	Slezsko
náleželo	náležet	k5eAaImAgNnS	náležet
Prusku	Prusko	k1gNnSc3	Prusko
již	již	k6eAd1	již
od	od	k7c2	od
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Pomořansko	Pomořansko	k1gNnSc1	Pomořansko
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
ještě	ještě	k6eAd1	ještě
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Pruský	pruský	k2eAgInSc1d1	pruský
zábor	zábor	k1gInSc1	zábor
měl	mít	k5eAaImAgInS	mít
oproti	oproti	k7c3	oproti
ruskému	ruský	k2eAgMnSc3d1	ruský
a	a	k8xC	a
rakouskému	rakouský	k2eAgInSc3d1	rakouský
více	hodně	k6eAd2	hodně
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Druhá	druhý	k4xOgFnSc1	druhý
Polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
represe	represe	k1gFnPc1	represe
proti	proti	k7c3	proti
Polákům	Polák	k1gMnPc3	Polák
a	a	k8xC	a
polským	polský	k2eAgMnPc3d1	polský
občanům	občan	k1gMnPc3	občan
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
a	a	k8xC	a
Německé	německý	k2eAgInPc1d1	německý
zločiny	zločin	k1gInPc1	zločin
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
většiny	většina	k1gFnSc2	většina
polského	polský	k2eAgNnSc2d1	polské
území	území	k1gNnSc2	území
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patřícího	patřící	k2eAgMnSc2d1	patřící
Rusku	Ruska	k1gFnSc4	Ruska
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
autonomní	autonomní	k2eAgNnSc1d1	autonomní
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
jako	jako	k8xC	jako
loutkový	loutkový	k2eAgInSc1d1	loutkový
stát	stát	k1gInSc1	stát
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
opět	opět	k6eAd1	opět
získalo	získat	k5eAaPmAgNnS	získat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
suverenitu	suverenita	k1gFnSc4	suverenita
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc4	nezávislost
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
i	i	k9	i
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
sovětským	sovětský	k2eAgNnSc7d1	sovětské
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
bojovalo	bojovat	k5eAaImAgNnS	bojovat
též	též	k9	též
s	s	k7c7	s
Ukrajinci	Ukrajinec	k1gMnPc7	Ukrajinec
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Litevci	Litevec	k1gMnPc7	Litevec
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
jako	jako	k8xC	jako
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
neochotě	neochota	k1gFnSc3	neochota
polských	polský	k2eAgFnPc2d1	polská
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
dohodnout	dohodnout	k5eAaPmF	dohodnout
<g/>
,	,	kIx,	,
nacionálnímu	nacionální	k2eAgNnSc3d1	nacionální
napětí	napětí	k1gNnSc3	napětí
a	a	k8xC	a
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
potížím	potíž	k1gFnPc3	potíž
byly	být	k5eAaImAgFnP	být
polské	polský	k2eAgFnPc4d1	polská
vlády	vláda	k1gFnPc4	vláda
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
;	;	kIx,	;
ozývalo	ozývat	k5eAaImAgNnS	ozývat
se	se	k3xPyFc4	se
volání	volání	k1gNnSc1	volání
po	po	k7c6	po
vládě	vláda	k1gFnSc6	vláda
pevné	pevný	k2eAgFnSc2d1	pevná
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
roku	rok	k1gInSc3	rok
1926	[number]	k4	1926
vyslyšel	vyslyšet	k5eAaPmAgMnS	vyslyšet
maršál	maršál	k1gMnSc1	maršál
Józef	Józef	k1gMnSc1	Józef
Piłsudski	Piłsudsk	k1gMnSc3	Piłsudsk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
provedl	provést	k5eAaPmAgMnS	provést
puč	puč	k1gInSc4	puč
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
diktaturu	diktatura	k1gFnSc4	diktatura
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sanační	sanační	k2eAgInSc1d1	sanační
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
perzekuci	perzekuce	k1gFnSc3	perzekuce
a	a	k8xC	a
potlačování	potlačování	k1gNnSc3	potlačování
demokratických	demokratický	k2eAgInPc2d1	demokratický
principů	princip	k1gInPc2	princip
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
měl	mít	k5eAaImAgInS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
maršál	maršál	k1gMnSc1	maršál
Edward	Edward	k1gMnSc1	Edward
Śmigły-Rydz	Śmigły-Rydz	k1gMnSc1	Śmigły-Rydz
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Józef	Józef	k1gMnSc1	Józef
Beck	Beck	k1gMnSc1	Beck
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
okupováno	okupován	k2eAgNnSc1d1	okupováno
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
mnoho	mnoho	k6eAd1	mnoho
polských	polský	k2eAgNnPc2d1	polské
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Varšava	Varšava	k1gFnSc1	Varšava
po	po	k7c6	po
Varšavském	varšavský	k2eAgNnSc6d1	Varšavské
povstání	povstání	k1gNnSc6	povstání
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
dějištěm	dějiště	k1gNnSc7	dějiště
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
Polska	Polska	k1gFnSc1	Polska
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
etnických	etnický	k2eAgMnPc2d1	etnický
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
polské	polský	k2eAgFnSc2d1	polská
inteligence	inteligence	k1gFnSc2	inteligence
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
příkaz	příkaz	k1gInSc4	příkaz
povražděno	povražděn	k2eAgNnSc1d1	povražděno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
Katyňském	Katyňský	k2eAgInSc6d1	Katyňský
masakru	masakr	k1gInSc6	masakr
a	a	k8xC	a
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
předních	přední	k2eAgMnPc2d1	přední
Poláků	Polák	k1gMnPc2	Polák
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
Němci	Němec	k1gMnPc1	Němec
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
operace	operace	k1gFnSc1	operace
Tannenberg	Tannenberg	k1gInSc1	Tannenberg
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nacistického	nacistický	k2eAgInSc2d1	nacistický
plánu	plán	k1gInSc2	plán
genocidy	genocida	k1gFnSc2	genocida
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
oblastech	oblast	k1gFnPc6	oblast
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
nacionalisty	nacionalista	k1gMnPc4	nacionalista
z	z	k7c2	z
UPA	UPA	kA	UPA
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
přijala	přijmout	k5eAaPmAgFnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
Polska	Polsko	k1gNnSc2	Polsko
povede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
Curzonově	Curzonův	k2eAgFnSc6d1	Curzonova
linii	linie	k1gFnSc6	linie
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
záboru	zábor	k1gInSc2	zábor
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
oblastech	oblast	k1gFnPc6	oblast
osídlených	osídlený	k2eAgFnPc6d1	osídlená
převážně	převážně	k6eAd1	převážně
Bělorusy	Bělorus	k1gMnPc7	Bělorus
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc7	Ukrajinec
anektoval	anektovat	k5eAaBmAgMnS	anektovat
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
odškodnění	odškodnění	k1gNnPc1	odškodnění
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgNnP	být
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
naopak	naopak	k6eAd1	naopak
připojena	připojit	k5eAaPmNgFnS	připojit
západní	západní	k2eAgFnSc1d1	západní
"	"	kIx"	"
<g/>
nová	nový	k2eAgNnPc1d1	nové
území	území	k1gNnPc1	území
<g/>
"	"	kIx"	"
až	až	k9	až
po	po	k7c6	po
dnešní	dnešní	k2eAgFnSc6d1	dnešní
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
Nise	Nisa	k1gFnSc6	Nisa
a	a	k8xC	a
Odře	Odra	k1gFnSc6	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Posun	posun	k1gInSc1	posun
hranic	hranice	k1gFnPc2	hranice
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
obrovské	obrovský	k2eAgInPc4d1	obrovský
nucené	nucený	k2eAgInPc4d1	nucený
přesuny	přesun	k1gInPc4	přesun
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Poláci	Polák	k1gMnPc1	Polák
přicházeli	přicházet	k5eAaImAgMnP	přicházet
z	z	k7c2	z
území	území	k1gNnSc2	území
anektovaných	anektovaný	k2eAgInPc2d1	anektovaný
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
osidlovali	osidlovat	k5eAaImAgMnP	osidlovat
zejména	zejména	k9	zejména
západní	západní	k2eAgNnSc4d1	západní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
bylo	být	k5eAaImAgNnS	být
vyhnáno	vyhnán	k2eAgNnSc1d1	vyhnáno
německé	německý	k2eAgNnSc1d1	německé
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
prakticky	prakticky	k6eAd1	prakticky
veškeré	veškerý	k3xTgNnSc4	veškerý
<g/>
)	)	kIx)	)
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
během	během	k7c2	během
Operace	operace	k1gFnSc2	operace
Visla	Visla	k1gFnSc1	Visla
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
domovů	domov	k1gInPc2	domov
násilně	násilně	k6eAd1	násilně
vysídleno	vysídlen	k2eAgNnSc4d1	vysídleno
ukrajinské	ukrajinský	k2eAgNnSc4d1	ukrajinské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Polská	polský	k2eAgFnSc1d1	polská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Polsko	Polsko	k1gNnSc1	Polsko
pod	pod	k7c7	pod
sovětským	sovětský	k2eAgInSc7d1	sovětský
vlivem	vliv	k1gInSc7	vliv
stalo	stát	k5eAaPmAgNnS	stát
komunistickým	komunistický	k2eAgInSc7d1	komunistický
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
a	a	k8xC	a
znárodňování	znárodňování	k1gNnSc1	znárodňování
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
státům	stát	k1gInPc3	stát
byl	být	k5eAaImAgInS	být
však	však	k9	však
režim	režim	k1gInSc1	režim
tolerantnější	tolerantní	k2eAgFnSc2d2	tolerantnější
k	k	k7c3	k
tradičně	tradičně	k6eAd1	tradičně
vlivné	vlivný	k2eAgFnSc3d1	vlivná
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Poznaňské	poznaňský	k2eAgNnSc1d1	poznaňské
povstání	povstání	k1gNnSc1	povstání
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
bylo	být	k5eAaImAgNnS	být
násilně	násilně	k6eAd1	násilně
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
uvolnění	uvolnění	k1gNnSc1	uvolnění
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
provázeno	provázet	k5eAaImNgNnS	provázet
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
stagnací	stagnace	k1gFnSc7	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
vlně	vlna	k1gFnSc6	vlna
stávek	stávka	k1gFnPc2	stávka
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
nezávislé	závislý	k2eNgInPc1d1	nezávislý
odbory	odbor	k1gInPc1	odbor
Solidarita	solidarita	k1gFnSc1	solidarita
(	(	kIx(	(
<g/>
Solidarność	Solidarność	k1gFnSc1	Solidarność
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
staly	stát	k5eAaPmAgFnP	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
politickou	politický	k2eAgFnSc7d1	politická
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
po	po	k7c4	po
zesílení	zesílení	k1gNnSc4	zesílení
jejich	jejich	k3xOp3gInPc2	jejich
protestů	protest	k1gInPc2	protest
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
generál	generál	k1gMnSc1	generál
Jaruzelski	Jaruzelsk	k1gFnSc2	Jaruzelsk
válečný	válečný	k2eAgInSc1d1	válečný
stav	stav	k1gInSc1	stav
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
postkomunistických	postkomunistický	k2eAgFnPc6d1	postkomunistická
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Třetí	třetí	k4xOgFnSc1	třetí
Polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
přesná	přesný	k2eAgNnPc1d1	přesné
data	datum	k1gNnPc1	datum
počátku	počátek	k1gInSc2	počátek
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
III	III	kA	III
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
vítězství	vítězství	k1gNnSc3	vítězství
Solidarity	solidarita	k1gFnSc2	solidarita
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
(	(	kIx(	(
<g/>
polo	polo	k6eAd1	polo
<g/>
)	)	kIx)	)
<g/>
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
jmenování	jmenování	k1gNnSc1	jmenování
Tadeusze	Tadeusze	k1gFnSc2	Tadeusze
Mazowieckého	Mazowiecký	k2eAgInSc2d1	Mazowiecký
premiérem	premiér	k1gMnSc7	premiér
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
novelizace	novelizace	k1gFnSc1	novelizace
<g />
.	.	kIx.	.
</s>
<s>
ústavy	ústava	k1gFnSc2	ústava
měnící	měnící	k2eAgFnSc2d1	měnící
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
na	na	k7c4	na
Rzeczpospolita	Rzeczpospolit	k2eAgNnPc4d1	Rzeczpospolit
Polska	Polsko	k1gNnPc4	Polsko
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
exilový	exilový	k2eAgMnSc1d1	exilový
prezident	prezident	k1gMnSc1	prezident
předal	předat	k5eAaPmAgMnS	předat
odznaky	odznak	k1gInPc4	odznak
moci	moct	k5eAaImF	moct
během	během	k7c2	během
inaugurace	inaugurace	k1gFnSc2	inaugurace
polského	polský	k2eAgMnSc2d1	polský
prezidenta	prezident	k1gMnSc2	prezident
Lecha	Lech	k1gMnSc2	Lech
Wałęsy	Wałęsa	k1gFnSc2	Wałęsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wałęsa	Wałęsa	k1gFnSc1	Wałęsa
pronesl	pronést	k5eAaPmAgMnS	pronést
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
touto	tento	k3xDgFnSc7	tento
chvílí	chvíle	k1gFnSc7	chvíle
slavnostně	slavnostně	k6eAd1	slavnostně
začíná	začínat	k5eAaImIp3nS	začínat
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
Polska	Polsko	k1gNnSc2	Polsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
Polsko	Polsko	k1gNnSc1	Polsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
šokovou	šokový	k2eAgFnSc4d1	šoková
terapii	terapie	k1gFnSc4	terapie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
transformovat	transformovat	k5eAaBmF	transformovat
svoji	svůj	k3xOyFgFnSc4	svůj
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
10	[number]	k4	10
zemí	zem	k1gFnPc2	zem
přistoupivších	přistoupivší	k2eAgFnPc2d1	přistoupivší
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
premiérem	premiér	k1gMnSc7	premiér
Jarosław	Jarosław	k1gFnSc2	Jarosław
Kaczyński	Kaczyńsk	k1gFnSc2	Kaczyńsk
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
jeho	jeho	k3xOp3gMnPc3	jeho
dvojče	dvojče	k1gNnSc1	dvojče
Lech	lecha	k1gFnPc2	lecha
Kaczyński	Kaczyński	k1gNnSc2	Kaczyński
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
a	a	k8xC	a
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
bratři	bratr	k1gMnPc1	bratr
Kaczyńští	Kaczyńský	k2eAgMnPc1d1	Kaczyńský
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
strana	strana	k1gFnSc1	strana
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
(	(	kIx(	(
<g/>
PiS	Pisa	k1gFnPc2	Pisa
<g/>
)	)	kIx)	)
operovali	operovat	k5eAaImAgMnP	operovat
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
IV	IV	kA	IV
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
republika	republika	k1gFnSc1	republika
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
opravit	opravit	k5eAaPmF	opravit
všechno	všechen	k3xTgNnSc4	všechen
špatné	špatný	k2eAgNnSc4d1	špatné
třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
skoncovat	skoncovat	k5eAaPmF	skoncovat
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
provést	provést	k5eAaPmF	provést
důslednou	důsledný	k2eAgFnSc4d1	důsledná
dekomunizaci	dekomunizace	k1gFnSc4	dekomunizace
<g/>
,	,	kIx,	,
rozbít	rozbít	k5eAaPmF	rozbít
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
předešlého	předešlý	k2eAgInSc2d1	předešlý
režimu	režim	k1gInSc2	režim
apod.	apod.	kA	apod.
Strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
oboje	oboj	k1gFnPc4	oboj
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
svých	svůj	k3xOyFgMnPc2	svůj
představitelů	představitel	k1gMnPc2	představitel
budovat	budovat	k5eAaImF	budovat
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nastolila	nastolit	k5eAaPmAgFnS	nastolit
silně	silně	k6eAd1	silně
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
<g/>
,	,	kIx,	,
nacionalistický	nacionalistický	k2eAgInSc4d1	nacionalistický
a	a	k8xC	a
euroskeptický	euroskeptický	k2eAgInSc4d1	euroskeptický
politický	politický	k2eAgInSc4d1	politický
kurz	kurz	k1gInSc4	kurz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
úrovni	úroveň	k1gFnSc6	úroveň
budil	budit	k5eAaImAgMnS	budit
kontroverze	kontroverze	k1gFnPc4	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Občanské	občanský	k2eAgFnSc2d1	občanská
platformy	platforma	k1gFnSc2	platforma
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
posunulo	posunout	k5eAaPmAgNnS	posunout
zemi	zem	k1gFnSc4	zem
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
neoliberalismu	neoliberalismus	k1gInSc3	neoliberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
zahynul	zahynout	k5eAaPmAgMnS	zahynout
prezident	prezident	k1gMnSc1	prezident
Lech	Lech	k1gMnSc1	Lech
Kaczyński	Kaczyńske	k1gFnSc4	Kaczyńske
ve	v	k7c6	v
Smolensku	Smolensko	k1gNnSc6	Smolensko
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgMnPc2d1	další
polských	polský	k2eAgMnPc2d1	polský
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
příslušníky	příslušník	k1gMnPc7	příslušník
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
velení	velení	k1gNnSc2	velení
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
pozemních	pozemní	k2eAgFnPc2d1	pozemní
<g/>
,	,	kIx,	,
leteckých	letecký	k2eAgFnPc2d1	letecká
<g/>
,	,	kIx,	,
námořních	námořní	k2eAgFnPc2d1	námořní
i	i	k8xC	i
speciálních	speciální	k2eAgFnPc2d1	speciální
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
uctít	uctít	k5eAaPmF	uctít
památku	památka	k1gFnSc4	památka
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
Poláků	Polák	k1gMnPc2	Polák
během	během	k7c2	během
Katyňského	Katyňský	k2eAgInSc2d1	Katyňský
masakru	masakr	k1gInSc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
po	po	k7c6	po
8	[number]	k4	8
letech	let	k1gInPc6	let
vlády	vláda	k1gFnSc2	vláda
Občanské	občanský	k2eAgFnSc2d1	občanská
platformy	platforma	k1gFnSc2	platforma
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostalo	dostat	k5eAaPmAgNnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
Beata	Beata	k1gFnSc1	Beata
Szydłová	Szydłová	k1gFnSc1	Szydłová
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Jarosław	Jarosław	k1gFnSc2	Jarosław
Kaczyński	Kaczyńsk	k1gFnSc2	Kaczyńsk
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kandidát	kandidát	k1gMnSc1	kandidát
Práva	právo	k1gNnSc2	právo
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Andrzej	Andrzej	k1gMnSc1	Andrzej
Duda	Duda	k1gMnSc1	Duda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
312	[number]	k4	312
679	[number]	k4	679
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Polsko	Polsko	k1gNnSc4	Polsko
9	[number]	k4	9
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
z	z	k7c2	z
rozlohy	rozloha	k1gFnSc2	rozloha
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
zabírá	zabírat	k5eAaImIp3nS	zabírat
7,1	[number]	k4	7,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
činí	činit	k5eAaImIp3nS	činit
3511	[number]	k4	3511
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
440	[number]	k4	440
km	km	kA	km
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
mořské	mořský	k2eAgFnPc4d1	mořská
hranice	hranice	k1gFnPc4	hranice
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
hranicí	hranice	k1gFnSc7	hranice
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
770	[number]	k4	770
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
467	[number]	k4	467
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
(	(	kIx(	(
<g/>
796	[number]	k4	796
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
(	(	kIx(	(
<g/>
541	[number]	k4	541
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
(	(	kIx(	(
<g/>
535	[number]	k4	535
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
(	(	kIx(	(
<g/>
418	[number]	k4	418
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
(	(	kIx(	(
<g/>
104	[number]	k4	104
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
Kaliningradská	kaliningradský	k2eAgFnSc1d1	Kaliningradská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
210	[number]	k4	210
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
poměrně	poměrně	k6eAd1	poměrně
členitá	členitý	k2eAgFnSc1d1	členitá
jižní	jižní	k2eAgFnSc1d1	jižní
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
horských	horský	k2eAgNnPc6d1	horské
pásmech	pásmo	k1gNnPc6	pásmo
a	a	k8xC	a
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
neměnila	měnit	k5eNaImAgFnS	měnit
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
hranice	hranice	k1gFnSc2	hranice
jsou	být	k5eAaImIp3nP	být
teprve	teprve	k6eAd1	teprve
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
často	často	k6eAd1	často
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
směru	směr	k1gInSc6	směr
nebo	nebo	k8xC	nebo
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
(	(	kIx(	(
<g/>
Bug	Bug	k1gFnSc1	Bug
<g/>
,	,	kIx,	,
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
Odra	Odra	k1gFnSc1	Odra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
Slezskem	Slezsko	k1gNnSc7	Slezsko
(	(	kIx(	(
<g/>
s	s	k7c7	s
Jesenickem	Jesenicko	k1gNnSc7	Jesenicko
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Rychlebských	Rychlebský	k2eAgFnPc2d1	Rychlebská
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Zlatohorska	Zlatohorsko	k1gNnSc2	Zlatohorsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Krnovskem	Krnovsko	k1gNnSc7	Krnovsko
a	a	k8xC	a
s	s	k7c7	s
Opavskem	Opavsko	k1gNnSc7	Opavsko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
s	s	k7c7	s
Hlučínskem	Hlučínsko	k1gNnSc7	Hlučínsko
a	a	k8xC	a
s	s	k7c7	s
Českotěšínskem	Českotěšínsko	k1gNnSc7	Českotěšínsko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
také	také	k9	také
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
směru	směr	k1gInSc6	směr
nebo	nebo	k8xC	nebo
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
(	(	kIx(	(
<g/>
Opavice	Opavice	k1gFnSc1	Opavice
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
Olše	olše	k1gFnSc1	olše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
řek	řeka	k1gFnPc2	řeka
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
státu	stát	k1gInSc2	stát
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
nížinami	nížina	k1gFnPc7	nížina
a	a	k8xC	a
rovinami	rovina	k1gFnPc7	rovina
Polské	polský	k2eAgFnSc2d1	polská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
území	území	k1gNnPc2	území
leží	ležet	k5eAaImIp3nP	ležet
do	do	k7c2	do
200	[number]	k4	200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Severní	severní	k2eAgNnSc4d1	severní
Polsko	Polsko	k1gNnSc4	Polsko
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
i	i	k8xC	i
Mazurská	mazurský	k2eAgFnSc1d1	Mazurská
jezerní	jezerní	k2eAgFnSc1d1	jezerní
plošina	plošina	k1gFnSc1	plošina
jako	jako	k8xS	jako
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
pleistocenního	pleistocenní	k2eAgNnSc2d1	pleistocenní
zalednění	zalednění	k1gNnSc2	zalednění
<g/>
;	;	kIx,	;
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
písečně	písečně	k6eAd1	písečně
přesypy	přesyp	k1gInPc1	přesyp
a	a	k8xC	a
kosy	kosa	k1gFnPc1	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Středovýchodní	středovýchodní	k2eAgFnSc1d1	středovýchodní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
nízké	nízký	k2eAgFnPc4d1	nízká
vrchoviny	vrchovina	k1gFnPc4	vrchovina
Malopolská	malopolský	k2eAgFnSc1d1	Malopolská
vrchovina	vrchovina	k1gFnSc1	vrchovina
a	a	k8xC	a
Lublinská	lublinský	k2eAgFnSc1d1	Lublinská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
Polska	Polsko	k1gNnSc2	Polsko
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
v	v	k7c4	v
příhraniční	příhraniční	k2eAgInSc4d1	příhraniční
pás	pás	k1gInSc4	pás
prvohorních	prvohorní	k2eAgInPc2d1	prvohorní
horských	horský	k2eAgInPc2d1	horský
masivů	masiv	k1gInPc2	masiv
Sudety	Sudety	k1gFnPc1	Sudety
(	(	kIx(	(
<g/>
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Soví	soví	k2eAgFnPc1d1	soví
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
pískovcové	pískovcový	k2eAgFnPc1d1	pískovcová
Stolové	stolový	k2eAgFnPc1d1	stolová
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Bystřické	Bystřické	k2eAgFnPc1d1	Bystřické
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
Rychlebské	Rychlebský	k2eAgFnPc1d1	Rychlebská
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
Zlatohorská	Zlatohorský	k2eAgFnSc1d1	Zlatohorská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetihorní	třetihorní	k2eAgInPc1d1	třetihorní
Karpaty	Karpaty	k1gInPc1	Karpaty
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pohoří	pohoří	k1gNnSc4	pohoří
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
a	a	k8xC	a
Nízkých	nízký	k2eAgFnPc2d1	nízká
Beskyd	Beskydy	k1gFnPc2	Beskydy
<g/>
,	,	kIx,	,
Tatry	Tatra	k1gFnSc2	Tatra
a	a	k8xC	a
Bukovské	Bukovské	k2eAgInPc4d1	Bukovské
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
hustou	hustý	k2eAgFnSc4d1	hustá
říční	říční	k2eAgFnSc4d1	říční
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
splavné	splavný	k2eAgFnPc1d1	splavná
řeky	řeka	k1gFnPc1	řeka
Visla	Visla	k1gFnSc1	Visla
a	a	k8xC	a
Odra	Odra	k1gFnSc1	Odra
odvádějí	odvádět	k5eAaImIp3nP	odvádět
vodu	voda	k1gFnSc4	voda
asi	asi	k9	asi
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
leží	ležet	k5eAaImIp3nS	ležet
vysoko	vysoko	k6eAd1	vysoko
položená	položená	k1gFnSc1	položená
jezera	jezero	k1gNnSc2	jezero
(	(	kIx(	(
<g/>
plesa	pleso	k1gNnSc2	pleso
<g/>
)	)	kIx)	)
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
styku	styk	k1gInSc6	styk
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
východoevropského	východoevropský	k2eAgInSc2d1	východoevropský
a	a	k8xC	a
oceánského	oceánský	k2eAgInSc2d1	oceánský
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
chladné	chladný	k2eAgFnPc1d1	chladná
zimy	zima	k1gFnPc1	zima
s	s	k7c7	s
vydatnými	vydatný	k2eAgFnPc7d1	vydatná
sněhovými	sněhový	k2eAgFnPc7d1	sněhová
srážkami	srážka	k1gFnPc7	srážka
a	a	k8xC	a
horká	horký	k2eAgNnPc4d1	horké
vlhká	vlhký	k2eAgNnPc4d1	vlhké
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
regionálně	regionálně	k6eAd1	regionálně
-8	-8	k4	-8
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
20	[number]	k4	20
<g/>
/	/	kIx~	/
<g/>
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
600	[number]	k4	600
mm	mm	kA	mm
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
1200	[number]	k4	1200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Polsku	Polska	k1gFnSc4	Polska
chráněno	chránit	k5eAaImNgNnS	chránit
101	[number]	k4	101
588	[number]	k4	588
km2	km2	k4	km2
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
32,1	[number]	k4	32,1
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
zejména	zejména	k9	zejména
do	do	k7c2	do
23	[number]	k4	23
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
:	:	kIx,	:
Babiogórski	Babiogórske	k1gFnSc4	Babiogórske
<g/>
,	,	kIx,	,
Białowieski	Białowieski	k1gNnPc4	Białowieski
<g/>
,	,	kIx,	,
Biebrzański	Biebrzańsk	k1gFnPc4	Biebrzańsk
<g/>
,	,	kIx,	,
Bieszczadzki	Bieszczadzk	k1gFnPc4	Bieszczadzk
<g/>
,	,	kIx,	,
Bory	bor	k1gInPc4	bor
Tucholskie	Tucholskie	k1gFnSc2	Tucholskie
<g/>
,	,	kIx,	,
Drawieński	Drawieński	k1gNnSc2	Drawieński
<g/>
,	,	kIx,	,
Gorczański	Gorczański	k1gNnSc2	Gorczański
<g/>
,	,	kIx,	,
Gór	Gór	k1gMnSc1	Gór
Stołowych	Stołowych	k1gMnSc1	Stołowych
<g/>
,	,	kIx,	,
Kampinoski	Kampinosk	k1gMnPc5	Kampinosk
<g/>
,	,	kIx,	,
Karkonoski	Karkonosk	k1gMnPc5	Karkonosk
<g/>
,	,	kIx,	,
Magurski	Magursk	k1gMnPc5	Magursk
<g/>
,	,	kIx,	,
Narwiański	Narwiańsk	k1gMnPc5	Narwiańsk
<g/>
,	,	kIx,	,
Ojcowski	Ojcowsk	k1gMnPc5	Ojcowsk
<g/>
,	,	kIx,	,
Pieniński	Pienińsk	k1gMnPc5	Pienińsk
<g/>
,	,	kIx,	,
Poleski	Polesk	k1gMnPc5	Polesk
<g/>
,	,	kIx,	,
Roztoczański	Roztoczańsk	k1gMnPc5	Roztoczańsk
<g/>
,	,	kIx,	,
Słowiński	Słowińsk	k1gMnPc5	Słowińsk
<g/>
,	,	kIx,	,
Swiętokrzyski	Swiętokrzysk	k1gMnPc5	Swiętokrzysk
<g/>
,	,	kIx,	,
Tatrzański	Tatrzańsk	k1gMnPc5	Tatrzańsk
<g/>
,	,	kIx,	,
Ujście	Ujście	k1gFnSc2	Ujście
Warty	Warta	k1gFnSc2	Warta
<g/>
,	,	kIx,	,
Wielkopolski	Wielkopolsk	k1gFnSc2	Wielkopolsk
<g/>
,	,	kIx,	,
Wigierski	Wigiersk	k1gFnSc2	Wigiersk
<g/>
,	,	kIx,	,
Woliński	Wolińsk	k1gFnPc4	Wolińsk
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
opět	opět	k6eAd1	opět
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
<g/>
:	:	kIx,	:
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gInSc4	on
Sejm	Sejm	k1gInSc4	Sejm
(	(	kIx(	(
<g/>
460	[number]	k4	460
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
(	(	kIx(	(
<g/>
100	[number]	k4	100
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
Polsko	Polsko	k1gNnSc1	Polsko
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
generální	generální	k2eAgFnSc7d1	generální
konzulát	konzulát	k1gInSc4	konzulát
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
konzulát	konzulát	k1gInSc4	konzulát
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
má	mít	k5eAaImIp3nS	mít
Polsko	Polsko	k1gNnSc4	Polsko
trojstupňové	trojstupňový	k2eAgNnSc1d1	trojstupňové
členění	členění	k1gNnSc1	členění
na	na	k7c6	na
vojvodství	vojvodství	k1gNnSc6	vojvodství
(	(	kIx(	(
<g/>
województwo	województwo	k1gMnSc1	województwo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
(	(	kIx(	(
<g/>
powiat	powiat	k1gInSc1	powiat
<g/>
)	)	kIx)	)
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
gmina	gmina	k1gFnSc1	gmina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozdílnosti	rozdílnost	k1gFnSc3	rozdílnost
chápání	chápání	k1gNnSc2	chápání
českého	český	k2eAgInSc2d1	český
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
obec	obec	k1gFnSc1	obec
<g/>
"	"	kIx"	"
oproti	oproti	k7c3	oproti
polskému	polský	k2eAgMnSc3d1	polský
-	-	kIx~	-
gmina	gmina	k1gFnSc1	gmina
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
ekvivalentní	ekvivalentní	k2eAgFnSc2d1	ekvivalentní
působnosti	působnost	k1gFnSc2	působnost
pověřeného	pověřený	k2eAgInSc2d1	pověřený
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
-	-	kIx~	-
se	se	k3xPyFc4	se
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
gmina	gmina	k1gFnSc1	gmina
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
následujících	následující	k2eAgNnPc2d1	následující
16	[number]	k4	16
vojvodství	vojvodství	k1gNnPc2	vojvodství
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nominální	nominální	k2eAgFnSc1d1	nominální
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
US	US	kA	US
<g/>
$	$	kIx~	$
474.783	[number]	k4	474.783
mln	mln	k?	mln
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
-	-	kIx~	-
567,4	[number]	k4	567,4
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
US	US	kA	US
<g/>
$	$	kIx~	$
1.005.449	[number]	k4	1.005.449
mln	mln	k?	mln
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
služby	služba	k1gFnSc2	služba
63	[number]	k4	63
%	%	kIx~	%
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
33,6	[number]	k4	33,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
služby	služba	k1gFnSc2	služba
61,4	[number]	k4	61,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
26,2	[number]	k4	26,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
12,4	[number]	k4	12,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vyspělým	vyspělý	k2eAgInPc3d1	vyspělý
státům	stát	k1gInPc3	stát
bývalého	bývalý	k2eAgInSc2d1	bývalý
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
Indexem	index	k1gInSc7	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
kvality	kvalita	k1gFnSc2	kvalita
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
na	na	k7c6	na
území	území	k1gNnSc6	území
určitého	určitý	k2eAgInSc2d1	určitý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
gramotnost	gramotnost	k1gFnSc4	gramotnost
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
porodnost	porodnost	k1gFnSc1	porodnost
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
vypracované	vypracovaný	k2eAgFnSc2d1	vypracovaná
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
US	US	kA	US
<g/>
$	$	kIx~	$
678,6	[number]	k4	678,6
mld.	mld.	k?	mld.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
hlavně	hlavně	k9	hlavně
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
6,1	[number]	k4	6,1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
4	[number]	k4	4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
importu	import	k1gInSc6	import
znovu	znovu	k6eAd1	znovu
převládá	převládat	k5eAaImIp3nS	převládat
hlavně	hlavně	k9	hlavně
Německo	Německo	k1gNnSc1	Německo
z	z	k7c2	z
24	[number]	k4	24
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Ruska	Ruska	k1gFnSc1	Ruska
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
4	[number]	k4	4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
průmyslově-zemědělský	průmyslověemědělský	k2eAgInSc4d1	průmyslově-zemědělský
stát	stát	k1gInSc4	stát
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
těžbou	těžba	k1gFnSc7	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
úspěšně	úspěšně	k6eAd1	úspěšně
provedený	provedený	k2eAgInSc4d1	provedený
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
centrálně	centrálně	k6eAd1	centrálně
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
k	k	k7c3	k
tržnímu	tržní	k2eAgInSc3d1	tržní
způsobu	způsob	k1gInSc3	způsob
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
těžební	těžební	k2eAgInSc4d1	těžební
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc1	strojírenství
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgNnPc1d1	osobní
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hutnictví	hutnictví	k1gNnSc1	hutnictví
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
černé	černý	k2eAgNnSc1d1	černé
a	a	k8xC	a
hnědé	hnědý	k2eAgNnSc1d1	hnědé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
magnezit	magnezit	k1gInSc1	magnezit
<g/>
,	,	kIx,	,
kaolin	kaolin	k1gInSc1	kaolin
a	a	k8xC	a
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převažuje	převažovat	k5eAaImIp3nS	převažovat
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
produkce	produkce	k1gFnSc1	produkce
nad	nad	k7c4	nad
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
představuje	představovat	k5eAaImIp3nS	představovat
skoro	skoro	k6eAd1	skoro
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
plochy	plocha	k1gFnPc4	plocha
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
13	[number]	k4	13
%	%	kIx~	%
a	a	k8xC	a
lesy	les	k1gInPc1	les
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
len	len	k1gInSc1	len
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
chmel	chmel	k1gInSc1	chmel
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
žita	žito	k1gNnSc2	žito
<g/>
,	,	kIx,	,
lnu	len	k1gInSc2	len
<g/>
,	,	kIx,	,
brambor	brambora	k1gFnPc2	brambora
a	a	k8xC	a
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
se	se	k3xPyFc4	se
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
a	a	k8xC	a
rybí	rybí	k2eAgInPc1d1	rybí
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
Polska	Polsko	k1gNnSc2	Polsko
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
neefektivity	neefektivita	k1gFnSc2	neefektivita
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
si	se	k3xPyFc3	se
půjčoval	půjčovat	k5eAaImAgInS	půjčovat
mnoho	mnoho	k4c4	mnoho
peněz	peníze	k1gInPc2	peníze
od	od	k7c2	od
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
splatit	splatit	k5eAaPmF	splatit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
hnutí	hnutí	k1gNnSc4	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
stanné	stanný	k2eAgNnSc1d1	stanné
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Stály	stát	k5eAaImAgFnP	stát
se	se	k3xPyFc4	se
fronty	fronta	k1gFnPc1	fronta
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
potraviny	potravina	k1gFnPc4	potravina
jako	jako	k8xS	jako
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
HDP	HDP	kA	HDP
Polska	Polsko	k1gNnSc2	Polsko
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
v	v	k7c6	v
dolarech	dolar	k1gInPc6	dolar
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
1700	[number]	k4	1700
$	$	kIx~	$
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
HDP	HDP	kA	HDP
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
3100	[number]	k4	3100
$	$	kIx~	$
a	a	k8xC	a
HDP	HDP	kA	HDP
Rakouska	Rakousko	k1gNnSc2	Rakousko
19	[number]	k4	19
200	[number]	k4	200
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
zažívá	zažívat	k5eAaImIp3nS	zažívat
Polsko	Polsko	k1gNnSc1	Polsko
silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
2007	[number]	k4	2007
polská	polský	k2eAgFnSc1d1	polská
ekonomika	ekonomika	k1gFnSc1	ekonomika
narostla	narůst	k5eAaPmAgFnS	narůst
o	o	k7c4	o
177	[number]	k4	177
<g/>
%	%	kIx~	%
-	-	kIx~	-
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
EU	EU	kA	EU
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
růst	růst	k5eAaImF	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
i	i	k9	i
během	během	k7c2	během
světové	světový	k2eAgFnSc2d1	světová
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
nevyrovnala	vyrovnat	k5eNaPmAgFnS	vyrovnat
západním	západní	k2eAgFnPc3d1	západní
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činil	činit	k5eAaImAgInS	činit
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
přibližně	přibližně	k6eAd1	přibližně
26	[number]	k4	26
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
47	[number]	k4	47
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
Poláků	Polák	k1gMnPc2	Polák
proto	proto	k8xC	proto
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
natrvalo	natrvalo	k6eAd1	natrvalo
usadí	usadit	k5eAaPmIp3nP	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
tržního	tržní	k2eAgNnSc2d1	tržní
hospodářství	hospodářství	k1gNnSc2	hospodářství
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
rychle	rychle	k6eAd1	rychle
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
nerovnost	nerovnost	k1gFnSc1	nerovnost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
regiony	region	k1gInPc7	region
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
vzrůst	vzrůst	k1gInSc1	vzrůst
HDP	HDP	kA	HDP
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
Varšava	Varšava	k1gFnSc1	Varšava
(	(	kIx(	(
<g/>
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
Mazovským	Mazovský	k2eAgNnSc7d1	Mazovský
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
pak	pak	k6eAd1	pak
venkovské	venkovský	k2eAgInPc4d1	venkovský
regiony	region	k1gInPc4	region
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
činil	činit	k5eAaImAgMnS	činit
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
Mazovském	Mazovský	k2eAgNnSc6d1	Mazovský
vojvodství	vojvodství	k1gNnSc6	vojvodství
128	[number]	k4	128
%	%	kIx~	%
celopolského	celopolský	k2eAgInSc2d1	celopolský
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
nejchudším	chudý	k2eAgNnSc6d3	nejchudší
Podkarpatském	podkarpatský	k2eAgNnSc6d1	Podkarpatské
vojvodství	vojvodství	k1gNnSc6	vojvodství
76	[number]	k4	76
%	%	kIx~	%
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
160	[number]	k4	160
%	%	kIx~	%
vs	vs	k?	vs
<g/>
.	.	kIx.	.
68	[number]	k4	68
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
dostatku	dostatek	k1gInSc6	dostatek
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
spalování	spalování	k1gNnSc3	spalování
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
doposud	doposud	k6eAd1	doposud
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
stavělo	stavět	k5eAaImAgNnS	stavět
v	v	k7c6	v
Žarnovci	Žarnovec	k1gMnSc6	Žarnovec
poblíž	poblíž	k7c2	poblíž
Gdyně	Gdyně	k1gFnSc2	Gdyně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Polska	Polsko	k1gNnSc2	Polsko
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
atomovou	atomový	k2eAgFnSc4d1	atomová
elektrárnu	elektrárna	k1gFnSc4	elektrárna
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
1600	[number]	k4	1600
megawattů	megawatt	k1gInPc2	megawatt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
černobylské	černobylský	k2eAgFnSc6d1	Černobylská
havárii	havárie	k1gFnSc6	havárie
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
zastavena	zastavit	k5eAaPmNgFnS	zastavit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pouze	pouze	k6eAd1	pouze
ruiny	ruina	k1gFnSc2	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
hlavními	hlavní	k2eAgInPc7d1	hlavní
tahy	tah	k1gInPc7	tah
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
rozestavěnosti	rozestavěnost	k1gFnSc2	rozestavěnost
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
hotová	hotový	k2eAgFnSc1d1	hotová
je	být	k5eAaImIp3nS	být
jihopolská	jihopolský	k2eAgFnSc1d1	jihopolský
dálnice	dálnice	k1gFnSc1	dálnice
A4	A4	k1gFnSc2	A4
(	(	kIx(	(
<g/>
670	[number]	k4	670
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spojnice	spojnice	k1gFnSc2	spojnice
E	E	kA	E
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
dálnici	dálnice	k1gFnSc4	dálnice
od	od	k7c2	od
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
města	město	k1gNnPc4	město
Legnica	Legnicum	k1gNnSc2	Legnicum
<g/>
,	,	kIx,	,
Vratislav	Vratislava	k1gFnPc2	Vratislava
<g/>
,	,	kIx,	,
Katovice	Katovice	k1gFnPc1	Katovice
<g/>
,	,	kIx,	,
Krakov	Krakov	k1gInSc1	Krakov
a	a	k8xC	a
Řešov	Řešov	k1gInSc1	Řešov
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
východozápadní	východozápadní	k2eAgFnSc7d1	východozápadní
dálnicí	dálnice	k1gFnSc7	dálnice
je	být	k5eAaImIp3nS	být
A2	A2	k1gFnSc1	A2
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
620	[number]	k4	620
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
Berlína	Berlín	k1gInSc2	Berlín
přes	přes	k7c4	přes
Poznaň	Poznaň	k1gFnSc4	Poznaň
<g/>
,	,	kIx,	,
Lodž	Lodž	k1gFnSc4	Lodž
<g/>
,	,	kIx,	,
Varšavu	Varšava	k1gFnSc4	Varšava
a	a	k8xC	a
Siedlce	Siedlec	k1gMnPc4	Siedlec
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
budována	budován	k2eAgFnSc1d1	budována
trasa	trasa	k1gFnSc1	trasa
A1	A1	k1gMnSc1	A1
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
75	[number]	k4	75
<g/>
,	,	kIx,	,
568	[number]	k4	568
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
-	-	kIx~	-
Toruň	Toruň	k1gFnSc1	Toruň
-	-	kIx~	-
Lodž	Lodž	k1gFnSc1	Lodž
-	-	kIx~	-
Katovice	Katovice	k1gFnPc1	Katovice
-	-	kIx~	-
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
1660	[number]	k4	1660
km	km	kA	km
dálnic	dálnice	k1gFnPc2	dálnice
z	z	k7c2	z
plánovaných	plánovaný	k2eAgNnPc2d1	plánované
asi	asi	k9	asi
2027	[number]	k4	2027
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
mělo	mít	k5eAaImAgNnS	mít
oproti	oproti	k7c3	oproti
většině	většina	k1gFnSc3	většina
sousedů	soused	k1gMnPc2	soused
méně	málo	k6eAd2	málo
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
stereotyp	stereotyp	k1gInSc4	stereotyp
"	"	kIx"	"
<g/>
špatných	špatný	k2eAgFnPc2d1	špatná
polských	polský	k2eAgFnPc2d1	polská
silnic	silnice	k1gFnPc2	silnice
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgMnSc1d1	starý
několik	několik	k4yIc4	několik
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
dominantní	dominantní	k2eAgInSc4d1	dominantní
podíl	podíl	k1gInSc4	podíl
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
registrováno	registrovat	k5eAaBmNgNnS	registrovat
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
automobilů	automobil	k1gInPc2	automobil
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
oproti	oproti	k7c3	oproti
počátku	počátek	k1gInSc3	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
sítí	síť	k1gFnSc7	síť
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
silnice	silnice	k1gFnSc1	silnice
S3	S3	k1gMnPc2	S3
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
hraničního	hraniční	k2eAgInSc2d1	hraniční
přechodu	přechod	k1gInSc2	přechod
Lubawka-Královec	Lubawka-Královec	k1gMnSc1	Lubawka-Královec
do	do	k7c2	do
Svinoústí	Svinoúst	k1gFnPc2	Svinoúst
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Polsku	Polska	k1gFnSc4	Polska
plánováno	plánovat	k5eAaImNgNnS	plánovat
5650	[number]	k4	5650
km	km	kA	km
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
společně	společně	k6eAd1	společně
s	s	k7c7	s
dálnicemi	dálnice	k1gFnPc7	dálnice
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
7650	[number]	k4	7650
km	km	kA	km
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
obnovené	obnovený	k2eAgNnSc1d1	obnovené
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
zdědilo	zdědit	k5eAaPmAgNnS	zdědit
nerovnoměrnou	rovnoměrný	k2eNgFnSc4d1	nerovnoměrná
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
od	od	k7c2	od
(	(	kIx(	(
<g/>
pruského	pruský	k2eAgInSc2d1	pruský
<g/>
)	)	kIx)	)
západu	západ	k1gInSc2	západ
k	k	k7c3	k
(	(	kIx(	(
<g/>
ruskému	ruský	k2eAgMnSc3d1	ruský
<g/>
)	)	kIx)	)
východu	východ	k1gInSc3	východ
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
úpadku	úpadek	k1gInSc6	úpadek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
mnoho	mnoho	k4c1	mnoho
převážně	převážně	k6eAd1	převážně
lokálních	lokální	k2eAgFnPc2d1	lokální
tratí	trať	k1gFnPc2	trať
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výkony	výkon	k1gInPc1	výkon
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
snižovaly	snižovat	k5eAaImAgFnP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
hlavně	hlavně	k9	hlavně
dálková	dálkový	k2eAgFnSc1d1	dálková
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
vlaky	vlak	k1gInPc1	vlak
Pendolino	Pendolin	k2eAgNnSc1d1	Pendolino
<g/>
)	)	kIx)	)
a	a	k8xC	a
příměstská	příměstský	k2eAgFnSc1d1	příměstská
železnice	železnice	k1gFnSc1	železnice
<g/>
;	;	kIx,	;
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
byla	být	k5eAaImAgFnS	být
napojena	napojen	k2eAgFnSc1d1	napojena
např.	např.	kA	např.
některá	některý	k3yIgNnPc4	některý
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
polský	polský	k2eAgMnSc1d1	polský
provozovatel	provozovatel	k1gMnSc1	provozovatel
dráhy	dráha	k1gFnSc2	dráha
PKP	PKP	kA	PKP
Polskie	Polskie	k1gFnSc1	Polskie
Linie	linie	k1gFnSc1	linie
Kolejowe	Kolejowe	k1gFnSc1	Kolejowe
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
19	[number]	k4	19
201	[number]	k4	201
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
provozovatelé	provozovatel	k1gMnPc1	provozovatel
spravují	spravovat	k5eAaImIp3nP	spravovat
lokální	lokální	k2eAgFnPc4d1	lokální
sítě	síť	k1gFnPc4	síť
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
tratě	trať	k1gFnPc1	trať
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
elektrifikované	elektrifikovaný	k2eAgFnPc1d1	elektrifikovaná
a	a	k8xC	a
dvoukolejné	dvoukolejný	k2eAgFnPc1d1	dvoukolejná
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
vedou	vést	k5eAaImIp3nP	vést
hlavní	hlavní	k2eAgInPc1d1	hlavní
železniční	železniční	k2eAgInPc1d1	železniční
tahy	tah	k1gInPc1	tah
v	v	k7c6	v
podobných	podobný	k2eAgInPc6d1	podobný
směrech	směr	k1gInPc6	směr
jako	jako	k8xS	jako
dálniční	dálniční	k2eAgFnPc4d1	dálniční
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejmodernější	moderní	k2eAgFnSc7d3	nejmodernější
trasou	trasa	k1gFnSc7	trasa
je	být	k5eAaImIp3nS	být
Centralna	Centralna	k1gFnSc1	Centralna
Magistrala	Magistrala	k1gFnSc1	Magistrala
Kolejowa	Kolejowa	k1gFnSc1	Kolejowa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spojuje	spojovat	k5eAaImIp3nS	spojovat
Slezsko	Slezsko	k1gNnSc4	Slezsko
a	a	k8xC	a
Krakov	Krakov	k1gInSc1	Krakov
s	s	k7c7	s
Varšavou	Varšava	k1gFnSc7	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
také	také	k9	také
několik	několik	k4yIc1	několik
úzkorozchodných	úzkorozchodný	k2eAgFnPc2d1	úzkorozchodná
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
vedle	vedle	k7c2	vedle
několika	několik	k4yIc2	několik
krátkých	krátká	k1gFnPc2	krátká
širokorozchodných	širokorozchodný	k2eAgFnPc2d1	širokorozchodná
tratí	trať	k1gFnPc2	trať
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
také	také	k9	také
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
širokorozchodná	širokorozchodná	k1gFnSc1	širokorozchodná
trať	trať	k1gFnSc1	trať
Linia	Linia	k1gFnSc1	Linia
Hutnicza	Hutnicza	k1gFnSc1	Hutnicza
Szerokotorowa	Szerokotorowa	k1gFnSc1	Szerokotorowa
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
osobními	osobní	k2eAgMnPc7d1	osobní
dopravci	dopravce	k1gMnPc7	dopravce
jsou	být	k5eAaImIp3nP	být
PKP	PKP	kA	PKP
Intercity	Intercit	k1gInPc4	Intercit
(	(	kIx(	(
<g/>
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
státního	státní	k2eAgInSc2d1	státní
holdingu	holding	k1gInSc2	holding
Polskie	Polskie	k1gFnSc2	Polskie
Koleje	kolej	k1gFnSc2	kolej
Państwowe	Państwow	k1gFnSc2	Państwow
<g/>
)	)	kIx)	)
a	a	k8xC	a
Przewozy	Przewoz	k1gInPc1	Przewoz
Regionalne	Regionaln	k1gInSc5	Regionaln
vlastněné	vlastněný	k2eAgFnPc1d1	vlastněná
vojvodstvími	vojvodství	k1gNnPc7	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
o	o	k7c4	o
něco	něco	k3yInSc4	něco
řidší	řídký	k2eAgFnSc1d2	řidší
než	než	k8xS	než
v	v	k7c6	v
ČR	ČR	kA	ČR
či	či	k8xC	či
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
Trojměstí	trojměstí	k1gNnSc6	trojměstí
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
železnice	železnice	k1gFnSc1	železnice
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
příměstské	příměstský	k2eAgFnSc2d1	příměstská
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nákladní	nákladní	k2eAgFnSc6d1	nákladní
dopravě	doprava	k1gFnSc6	doprava
je	být	k5eAaImIp3nS	být
dominantním	dominantní	k2eAgMnSc7d1	dominantní
dopravcem	dopravce	k1gMnSc7	dopravce
PKP	PKP	kA	PKP
Cargo	Cargo	k6eAd1	Cargo
z	z	k7c2	z
holdingu	holding	k1gInSc2	holding
PKP	PKP	kA	PKP
s	s	k7c7	s
tržním	tržní	k2eAgInSc7d1	tržní
podílem	podíl	k1gInSc7	podíl
cca	cca	kA	cca
65	[number]	k4	65
%	%	kIx~	%
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
však	však	k9	však
nabývají	nabývat	k5eAaImIp3nP	nabývat
jiní	jiný	k2eAgMnPc1d1	jiný
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
letišť	letiště	k1gNnPc2	letiště
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediné	k1gNnSc7	jediné
celosvětově	celosvětově	k6eAd1	celosvětově
významným	významný	k2eAgNnSc7d1	významné
letištěm	letiště	k1gNnSc7	letiště
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnPc4	letiště
Frédérica	Frédéricus	k1gMnSc2	Frédéricus
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odbavuje	odbavovat	k5eAaImIp3nS	odbavovat
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgMnPc2	všecek
leteckých	letecký	k2eAgMnPc2d1	letecký
pasažérů	pasažér	k1gMnPc2	pasažér
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
10,6	[number]	k4	10,6
milionu	milion	k4xCgInSc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nad	nad	k7c4	nad
milion	milion	k4xCgInSc4	milion
pasažérů	pasažér	k1gMnPc2	pasažér
ročně	ročně	k6eAd1	ročně
odbaví	odbavit	k5eAaPmIp3nS	odbavit
ještě	ještě	k9	ještě
letiště	letiště	k1gNnSc1	letiště
Krakov-Balice	Krakov-Balice	k1gFnSc2	Krakov-Balice
<g/>
,	,	kIx,	,
Katovice-Pyrzowice	Katovice-Pyrzowice	k1gFnSc2	Katovice-Pyrzowice
<g/>
,	,	kIx,	,
Gdaňsk-Rębiechowo	Gdaňsk-Rębiechowo	k1gNnSc1	Gdaňsk-Rębiechowo
<g/>
,	,	kIx,	,
Vratislav-Strachowice	Vratislav-Strachowice	k1gFnSc1	Vratislav-Strachowice
a	a	k8xC	a
Poznaň-Ławica	Poznaň-Ławica	k1gFnSc1	Poznaň-Ławica
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
dopravcem	dopravce	k1gMnSc7	dopravce
jsou	být	k5eAaImIp3nP	být
Polskie	Polskie	k1gFnPc1	Polskie
Linie	linie	k1gFnSc2	linie
Lotnicze	Lotnicze	k1gFnSc1	Lotnicze
LOT	Lot	k1gMnSc1	Lot
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
postihlo	postihnout	k5eAaPmAgNnS	postihnout
polskou	polský	k2eAgFnSc4d1	polská
leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
největší	veliký	k2eAgNnSc1d3	veliký
neštěstí	neštěstí	k1gNnSc1	neštěstí
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
polského	polský	k2eAgInSc2d1	polský
vojenského	vojenský	k2eAgInSc2d1	vojenský
leteckého	letecký	k2eAgInSc2d1	letecký
speciálu	speciál	k1gInSc2	speciál
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
letoun	letoun	k1gInSc1	letoun
TU	tu	k6eAd1	tu
154	[number]	k4	154
při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
ve	v	k7c6	v
špatných	špatný	k2eAgFnPc6d1	špatná
podmínkách	podmínka	k1gFnPc6	podmínka
střetl	střetnout	k5eAaPmAgMnS	střetnout
se	s	k7c7	s
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
okamžitě	okamžitě	k6eAd1	okamžitě
hořet	hořet	k5eAaImF	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
byl	být	k5eAaImAgMnS	být
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Lech	Lech	k1gMnSc1	Lech
Kaczyński	Kaczyńske	k1gFnSc4	Kaczyńske
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
představitelé	představitel	k1gMnPc1	představitel
Polské	polský	k2eAgFnSc2d1	polská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Havárii	havárie	k1gFnSc4	havárie
poblíž	poblíž	k7c2	poblíž
ruského	ruský	k2eAgNnSc2d1	ruské
letiště	letiště	k1gNnSc2	letiště
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Smolensk	Smolensk	k1gInSc1	Smolensk
nikdo	nikdo	k3yNnSc1	nikdo
nepřežil	přežít	k5eNaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Cestující	cestující	k1gMnPc1	cestující
mířili	mířit	k5eAaImAgMnP	mířit
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
uctít	uctít	k5eAaPmF	uctít
památku	památka	k1gFnSc4	památka
zesnulých	zesnulá	k1gFnPc2	zesnulá
při	při	k7c6	při
katyňském	katyňský	k2eAgInSc6d1	katyňský
masakru	masakr	k1gInSc6	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
kontextu	kontext	k1gInSc6	kontext
rozvinuta	rozvinout	k5eAaPmNgFnS	rozvinout
průměrně	průměrně	k6eAd1	průměrně
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
především	především	k9	především
autobusy	autobus	k1gInPc1	autobus
a	a	k8xC	a
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
a	a	k8xC	a
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
sítě	síť	k1gFnSc2	síť
měst	město	k1gNnPc2	město
Gdyně	Gdyně	k1gFnSc1	Gdyně
<g/>
,	,	kIx,	,
Lublin	Lublin	k1gInSc1	Lublin
a	a	k8xC	a
Tychy	Tycha	k1gFnPc1	Tycha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
velmi	velmi	k6eAd1	velmi
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
je	být	k5eAaImIp3nS	být
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
systém	systém	k1gInSc1	systém
v	v	k7c6	v
Hornoslezské	hornoslezský	k2eAgFnSc6d1	Hornoslezská
konurbaci	konurbace	k1gFnSc6	konurbace
okolo	okolo	k7c2	okolo
Katovic	Katovice	k1gFnPc2	Katovice
<g/>
;	;	kIx,	;
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Lodži	Lodž	k1gFnSc3	Lodž
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
meziměstská	meziměstský	k2eAgFnSc1d1	meziměstská
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
linek	linka	k1gFnPc2	linka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poznani	Poznaň	k1gFnSc6	Poznaň
<g/>
,	,	kIx,	,
Krakově	Krakov	k1gInSc6	Krakov
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
jsou	být	k5eAaImIp3nP	být
budovány	budován	k2eAgFnPc1d1	budována
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
rychlodráhy	rychlodráha	k1gFnPc1	rychlodráha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
funguje	fungovat	k5eAaImIp3nS	fungovat
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
fungují	fungovat	k5eAaImIp3nP	fungovat
také	také	k6eAd1	také
přívozy	přívoz	k1gInPc1	přívoz
-	-	kIx~	-
například	například	k6eAd1	například
pro	pro	k7c4	pro
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
část	část	k1gFnSc4	část
Svinoústí	Svinoúst	k1gFnPc2	Svinoúst
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
budovány	budovat	k5eAaImNgFnP	budovat
cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgNnSc4	jenž
má	mít	k5eAaImIp3nS	mít
rovinaté	rovinatý	k2eAgNnSc4d1	rovinaté
Polsko	Polsko	k1gNnSc4	Polsko
příznivý	příznivý	k2eAgInSc1d1	příznivý
terén	terén	k1gInSc1	terén
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
námořními	námořní	k2eAgInPc7d1	námořní
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
<g/>
,	,	kIx,	,
Gdyně	Gdyně	k1gFnSc1	Gdyně
<g/>
,	,	kIx,	,
Štětín	Štětín	k1gInSc1	Štětín
<g/>
,	,	kIx,	,
Svinoústí	Svinoúst	k1gFnPc2	Svinoúst
a	a	k8xC	a
Kolobřeh	Kolobřeh	k1gInSc1	Kolobřeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
trajektové	trajektový	k2eAgFnPc1d1	trajektová
linky	linka	k1gFnPc1	linka
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnSc1d1	říční
nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
především	především	k9	především
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
drasticky	drasticky	k6eAd1	drasticky
omezila	omezit	k5eAaPmAgFnS	omezit
etnickou	etnický	k2eAgFnSc4d1	etnická
pestrost	pestrost	k1gFnSc4	pestrost
předválečného	předválečný	k2eAgNnSc2d1	předválečné
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
tvořili	tvořit	k5eAaImAgMnP	tvořit
Poláci	Polák	k1gMnPc1	Polák
68,5	[number]	k4	68,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
98,7	[number]	k4	98,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
deportací	deportace	k1gFnPc2	deportace
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
genocid	genocida	k1gFnPc2	genocida
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
složení	složení	k1gNnSc2	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
přineslo	přinést	k5eAaPmAgNnS	přinést
i	i	k8xC	i
poválečné	poválečný	k2eAgNnSc4d1	poválečné
měnění	měnění	k1gNnSc4	měnění
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Polsko	Polsko	k1gNnSc1	Polsko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
a	a	k8xC	a
běloruský	běloruský	k2eAgInSc4d1	běloruský
východ	východ	k1gInSc4	východ
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
získalo	získat	k5eAaPmAgNnS	získat
další	další	k2eAgFnPc4d1	další
německá	německý	k2eAgNnPc4d1	německé
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
vysídleni	vysídlit	k5eAaPmNgMnP	vysídlit
<g/>
.	.	kIx.	.
</s>
<s>
Kašubové	Kašub	k1gMnPc1	Kašub
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Goralé	goral	k1gMnPc1	goral
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
a	a	k8xC	a
Slezané	Slezan	k1gMnPc1	Slezan
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
etnické	etnický	k2eAgFnPc4d1	etnická
podskupiny	podskupina	k1gFnPc4	podskupina
polského	polský	k2eAgInSc2d1	polský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Slezanů	Slezan	k1gMnPc2	Slezan
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
hovoří	hovořit	k5eAaImIp3nS	hovořit
slezštinou	slezština	k1gFnSc7	slezština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
velmi	velmi	k6eAd1	velmi
početná	početný	k2eAgFnSc1d1	početná
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
používá	používat	k5eAaImIp3nS	používat
slezská	slezský	k2eAgFnSc1d1	Slezská
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
představovali	představovat	k5eAaImAgMnP	představovat
před	před	k7c4	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
největší	veliký	k2eAgFnSc4d3	veliký
židovskou	židovský	k2eAgFnSc4d1	židovská
komunitu	komunita	k1gFnSc4	komunita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
čítající	čítající	k2eAgNnSc1d1	čítající
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
3,3	[number]	k4	3,3
miliony	milion	k4xCgInPc4	milion
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
německými	německý	k2eAgMnPc7d1	německý
nacisty	nacista	k1gMnPc7	nacista
povražděno	povraždit	k5eAaPmNgNnS	povraždit
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
žije	žít	k5eAaImIp3nS	žít
především	především	k9	především
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zelova	Zelov	k1gInSc2	Zelov
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
českobratrských	českobratrský	k2eAgInPc2d1	českobratrský
exulantů	exulant	k1gMnPc2	exulant
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
žila	žít	k5eAaImAgFnS	žít
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
tzv.	tzv.	kA	tzv.
Českého	český	k2eAgInSc2d1	český
koutku	koutek	k1gInSc2	koutek
v	v	k7c6	v
Kladsku	Kladsko	k1gNnSc6	Kladsko
<g/>
.	.	kIx.	.
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1	lehká
tatarská	tatarský	k2eAgFnSc1d1	tatarská
jízda	jízda	k1gFnSc1	jízda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
součástí	součást	k1gFnPc2	součást
polsko-litevských	polskoitevský	k2eAgNnPc2d1	polsko-litevské
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
starousedlá	starousedlý	k2eAgFnSc1d1	starousedlá
menšina	menšina	k1gFnSc1	menšina
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokles	pokles	k1gInSc1	pokles
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
katolických	katolický	k2eAgInPc2d1	katolický
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
obnoven	obnovit	k5eAaPmNgInS	obnovit
zákaz	zákaz	k1gInSc1	zákaz
potratů	potrat	k1gInPc2	potrat
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
94,8	[number]	k4	94,8
%	%	kIx~	%
Slezané	Slezan	k1gMnPc1	Slezan
2,2	[number]	k4	2,2
%	%	kIx~	%
Kašubové	Kašub	k1gMnPc1	Kašub
0,6	[number]	k4	0,6
%	%	kIx~	%
Němci	Němec	k1gMnSc3	Němec
0,4	[number]	k4	0,4
%	%	kIx~	%
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
0,1	[number]	k4	0,1
%	%	kIx~	%
Bělorusové	Bělorus	k1gMnPc1	Bělorus
0,1	[number]	k4	0,1
%	%	kIx~	%
ostatní	ostatní	k2eAgNnSc1d1	ostatní
a	a	k8xC	a
nezjištěno	zjištěn	k2eNgNnSc1d1	nezjištěno
1,8	[number]	k4	1,8
%	%	kIx~	%
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
aglomerace	aglomerace	k1gFnPc1	aglomerace
jsou	být	k5eAaImIp3nP	být
katovická	katovický	k2eAgNnPc4d1	katovické
a	a	k8xC	a
varšavská	varšavský	k2eAgNnPc4d1	Varšavské
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
větší	veliký	k2eAgFnPc1d2	veliký
aglomerace	aglomerace	k1gFnPc1	aglomerace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
okolo	okolo	k7c2	okolo
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
Lodže	Lodž	k1gFnSc2	Lodž
<g/>
.	.	kIx.	.
</s>
<s>
Přímořská	přímořský	k2eAgNnPc1d1	přímořské
města	město	k1gNnPc1	město
Gdyně	Gdyně	k1gFnSc2	Gdyně
<g/>
,	,	kIx,	,
Sopoty	sopot	k1gInPc4	sopot
a	a	k8xC	a
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
Trojměstí	trojměstí	k1gNnSc1	trojměstí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
polských	polský	k2eAgMnPc2d1	polský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
při	při	k7c6	při
christianizaci	christianizace	k1gFnSc6	christianizace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
polské	polský	k2eAgFnPc1d1	polská
literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgFnP	psát
latinsky	latinsky	k6eAd1	latinsky
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
životopisy	životopis	k1gInPc4	životopis
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
kázání	kázání	k1gNnPc1	kázání
a	a	k8xC	a
liturgické	liturgický	k2eAgFnPc1d1	liturgická
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
a	a	k8xC	a
kroniky	kronika	k1gFnPc1	kronika
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
polské	polský	k2eAgInPc1d1	polský
překlady	překlad	k1gInPc1	překlad
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
původní	původní	k2eAgInSc4d1	původní
text	text	k1gInSc4	text
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
mariánská	mariánský	k2eAgFnSc1d1	Mariánská
píseň	píseň	k1gFnSc1	píseň
Bogurodzica	Bogurodzic	k1gInSc2	Bogurodzic
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
literatuře	literatura	k1gFnSc6	literatura
dominuje	dominovat	k5eAaImIp3nS	dominovat
nejprve	nejprve	k6eAd1	nejprve
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
polská	polský	k2eAgNnPc1d1	polské
díla	dílo	k1gNnPc1	dílo
Mikołaje	Mikołaj	k1gInSc2	Mikołaj
Reje	rej	k1gInSc2	rej
<g/>
,	,	kIx,	,
Łukasze	Łukasze	k1gFnSc1	Łukasze
Górnického	Górnický	k2eAgNnSc2d1	Górnický
a	a	k8xC	a
zejména	zejména	k9	zejména
Jana	Jan	k1gMnSc4	Jan
Kochanowského	Kochanowský	k2eAgMnSc4d1	Kochanowský
<g/>
,	,	kIx,	,
proslulého	proslulý	k2eAgMnSc4d1	proslulý
tvůrce	tvůrce	k1gMnSc4	tvůrce
frašek	fraška	k1gFnPc2	fraška
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
baroka	baroko	k1gNnSc2	baroko
opět	opět	k6eAd1	opět
sílí	sílet	k5eAaImIp3nS	sílet
náboženská	náboženský	k2eAgFnSc1d1	náboženská
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
část	část	k1gFnSc4	část
byla	být	k5eAaImAgFnS	být
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osvícenství	osvícenství	k1gNnSc6	osvícenství
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
kromě	kromě	k7c2	kromě
typických	typický	k2eAgInPc2d1	typický
žánrů	žánr	k1gInPc2	žánr
klasicismu	klasicismus	k1gInSc2	klasicismus
také	také	k6eAd1	také
naučná	naučný	k2eAgFnSc1d1	naučná
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
literatura	literatura	k1gFnSc1	literatura
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vynikali	vynikat	k5eAaImAgMnP	vynikat
např.	např.	kA	např.
Ignacy	Ignac	k2eAgFnPc4d1	Ignac
Krasicki	Krasick	k1gFnPc4	Krasick
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
J.	J.	kA	J.
U.	U.	kA	U.
Niemcewicz	Niemcewicz	k1gMnSc1	Niemcewicz
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Potocki	Potock	k1gFnSc2	Potock
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgNnSc7d1	slavné
obdobím	období	k1gNnSc7	období
polské	polský	k2eAgFnSc2d1	polská
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgInS	být
romantismus	romantismus	k1gInSc1	romantismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
romantismem	romantismus	k1gInSc7	romantismus
německým	německý	k2eAgInSc7d1	německý
a	a	k8xC	a
anglickým	anglický	k2eAgInSc7d1	anglický
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
zánikem	zánik	k1gInSc7	zánik
polské	polský	k2eAgFnSc2d1	polská
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
často	často	k6eAd1	často
nesla	nést	k5eAaImAgFnS	nést
mesianistické	mesianistický	k2eAgInPc4d1	mesianistický
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaPmIp3nS	spadat
sem	sem	k6eAd1	sem
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
(	(	kIx(	(
<g/>
s	s	k7c7	s
významným	významný	k2eAgInSc7d1	významný
veršovaným	veršovaný	k2eAgInSc7d1	veršovaný
eposem	epos	k1gInSc7	epos
Pan	Pan	k1gMnSc1	Pan
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Juliusz	Juliusz	k1gMnSc1	Juliusz
Słowacki	Słowack	k1gFnSc2	Słowack
<g/>
,	,	kIx,	,
Zygmunt	Zygmunt	k1gInSc4	Zygmunt
Krasiński	Krasińsk	k1gFnSc2	Krasińsk
<g/>
,	,	kIx,	,
Cyprian	Cyprian	k1gMnSc1	Cyprian
Kamil	Kamil	k1gMnSc1	Kamil
Norwid	Norwid	k1gInSc1	Norwid
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
lidovou	lidový	k2eAgFnSc7d1	lidová
poesií	poesie	k1gFnSc7	poesie
a	a	k8xC	a
legendami	legenda	k1gFnPc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
Lednovém	lednový	k2eAgNnSc6d1	lednové
povstání	povstání	k1gNnSc6	povstání
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
převládat	převládat	k5eAaImF	převládat
realismus	realismus	k1gInSc4	realismus
-	-	kIx~	-
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
období	období	k1gNnSc4	období
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
dominují	dominovat	k5eAaImIp3nP	dominovat
Bolesław	Bolesław	k1gMnPc1	Bolesław
Prus	Prus	k1gMnSc1	Prus
<g/>
,	,	kIx,	,
Henryk	Henryk	k1gMnSc1	Henryk
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
a	a	k8xC	a
Eliza	Eliza	k1gFnSc1	Eliza
Orzeszkowa	Orzeszkow	k1gInSc2	Orzeszkow
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
historické	historický	k2eAgInPc4d1	historický
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgInPc4d1	společenský
a	a	k8xC	a
psychologické	psychologický	k2eAgInPc4d1	psychologický
romány	román	k1gInPc4	román
<g/>
;	;	kIx,	;
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
Maria	Maria	k1gFnSc1	Maria
Konopnicka	Konopnicka	k1gFnSc1	Konopnicka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
krizi	krize	k1gFnSc3	krize
pozitivistické	pozitivistický	k2eAgFnSc2d1	pozitivistická
ideologie	ideologie	k1gFnSc2	ideologie
a	a	k8xC	a
angažovanosti	angažovanost	k1gFnSc2	angažovanost
<g/>
;	;	kIx,	;
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
autonomii	autonomie	k1gFnSc4	autonomie
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
modernistické	modernistický	k2eAgInPc1d1	modernistický
směry	směr	k1gInPc1	směr
(	(	kIx(	(
<g/>
dekadence	dekadence	k1gFnSc1	dekadence
<g/>
,	,	kIx,	,
parnasismus	parnasismus	k1gInSc1	parnasismus
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
hnutí	hnutí	k1gNnSc4	hnutí
Mladé	mladá	k1gFnSc2	mladá
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
Młoda	Młoda	k1gFnSc1	Młoda
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
Stanisław	Stanisław	k1gFnSc1	Stanisław
Przybyszewski	Przybyszewsk	k1gFnSc2	Przybyszewsk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasprowicz	Kasprowicz	k1gMnSc1	Kasprowicz
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
Staff	Staff	k1gMnSc1	Staff
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
;	;	kIx,	;
realistický	realistický	k2eAgInSc1d1	realistický
směr	směr	k1gInSc1	směr
udržuje	udržovat	k5eAaImIp3nS	udržovat
Władysław	Władysław	k1gFnSc4	Władysław
Reymont	Reymonta	k1gFnPc2	Reymonta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
významní	významný	k2eAgMnPc1d1	významný
autoři	autor	k1gMnPc1	autor
Witkacy	Witkaca	k1gFnSc2	Witkaca
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Schulz	Schulz	k1gMnSc1	Schulz
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
tragicky	tragicky	k6eAd1	tragicky
předčasně	předčasně	k6eAd1	předčasně
zemřelí	zemřelý	k2eAgMnPc1d1	zemřelý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Julian	Julian	k1gMnSc1	Julian
Tuwim	Tuwim	k1gMnSc1	Tuwim
a	a	k8xC	a
světově	světově	k6eAd1	světově
známý	známý	k2eAgMnSc1d1	známý
Witold	Witold	k1gMnSc1	Witold
Gombrowicz	Gombrowicz	k1gMnSc1	Gombrowicz
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
vpádem	vpád	k1gInSc7	vpád
nacistů	nacista	k1gMnPc2	nacista
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc7d2	pozdější
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
diktaturou	diktatura	k1gFnSc7	diktatura
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
volili	volit	k5eAaImAgMnP	volit
emigraci	emigrace	k1gFnSc4	emigrace
(	(	kIx(	(
<g/>
Gombrowicz	Gombrowicz	k1gMnSc1	Gombrowicz
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Sławomir	Sławomir	k1gMnSc1	Sławomir
Mrożek	Mrożek	k1gMnSc1	Mrożek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Zbigniew	Zbigniew	k1gMnSc1	Zbigniew
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
prosadit	prosadit	k5eAaPmF	prosadit
ve	v	k7c6	v
ztížených	ztížený	k2eAgFnPc6d1	ztížená
podmínkách	podmínka	k1gFnPc6	podmínka
-	-	kIx~	-
např.	např.	kA	např.
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
Stanisław	Stanisław	k1gMnSc1	Stanisław
Lem	lem	k1gInSc1	lem
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
Różewicz	Różewicz	k1gMnSc1	Różewicz
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
Ryszard	Ryszard	k1gMnSc1	Ryszard
Kapuściński	Kapuściński	k1gNnPc2	Kapuściński
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ve	v	k7c6	v
změněných	změněný	k2eAgInPc6d1	změněný
poměrech	poměr	k1gInPc6	poměr
objevují	objevovat	k5eAaImIp3nP	objevovat
nová	nový	k2eAgNnPc4d1	nové
témata	téma	k1gNnPc4	téma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
feminismus	feminismus	k1gInSc1	feminismus
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc1	droga
<g/>
)	)	kIx)	)
a	a	k8xC	a
autoři	autor	k1gMnPc1	autor
-	-	kIx~	-
Stefan	Stefan	k1gMnSc1	Stefan
Chwin	Chwin	k1gMnSc1	Chwin
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Tokarczuk	Tokarczuk	k1gInSc1	Tokarczuk
<g/>
,	,	kIx,	,
Manuela	Manuela	k1gFnSc1	Manuela
Gretkowska	Gretkowska	k1gFnSc1	Gretkowska
<g/>
,	,	kIx,	,
Marcin	Marcin	k1gInSc1	Marcin
Świetlicki	Świetlick	k1gFnSc2	Świetlick
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
Stasiuk	Stasiuk	k1gMnSc1	Stasiuk
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
fantasy	fantas	k1gInPc1	fantas
Andrzej	Andrzej	k1gFnSc4	Andrzej
Sapkowski	Sapkowsk	k1gFnPc1	Sapkowsk
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejmladší	mladý	k2eAgFnSc3d3	nejmladší
generaci	generace	k1gFnSc3	generace
polské	polský	k2eAgFnSc2d1	polská
literatury	literatura	k1gFnSc2	literatura
patří	patřit	k5eAaImIp3nS	patřit
Dorota	Dorota	k1gFnSc1	Dorota
Masłowska	Masłowska	k1gFnSc1	Masłowska
<g/>
,	,	kIx,	,
Wojciech	Wojciech	k1gInSc1	Wojciech
Kuczok	Kuczok	k1gInSc1	Kuczok
<g/>
,	,	kIx,	,
Michał	Michał	k1gMnSc1	Michał
Witkowski	Witkowsk	k1gFnSc2	Witkowsk
či	či	k8xC	či
Jacek	Jacek	k1gMnSc1	Jacek
Dehnel	Dehnel	k1gMnSc1	Dehnel
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
laureáty	laureát	k1gMnPc7	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
:	:	kIx,	:
Henryk	Henryk	k1gMnSc1	Henryk
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
<g/>
,	,	kIx,	,
Władysław	Władysław	k1gMnSc1	Władysław
Reymont	Reymont	k1gMnSc1	Reymont
<g/>
,	,	kIx,	,
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
<g/>
,	,	kIx,	,
Wisława	Wisława	k1gFnSc1	Wisława
Szymborská	Szymborský	k2eAgFnSc1d1	Szymborská
(	(	kIx(	(
<g/>
nominován	nominovat	k5eAaBmNgMnS	nominovat
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Stefan	Stefan	k1gMnSc1	Stefan
Żeromski	Żeromsk	k1gFnSc2	Żeromsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
polské	polský	k2eAgFnPc4d1	polská
ceny	cena	k1gFnPc4	cena
patří	patřit	k5eAaImIp3nS	patřit
cena	cena	k1gFnSc1	cena
Nike	Nike	k1gFnSc1	Nike
či	či	k8xC	či
cena	cena	k1gFnSc1	cena
Kościelských	Kościelský	k2eAgNnPc2d1	Kościelský
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
poměrně	poměrně	k6eAd1	poměrně
mnoho	mnoho	k4c1	mnoho
slavných	slavný	k2eAgMnPc2d1	slavný
filmových	filmový	k2eAgMnPc2d1	filmový
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Andrzej	Andrzej	k1gMnSc1	Andrzej
Wajda	Wajda	k1gMnSc1	Wajda
(	(	kIx(	(
<g/>
sedmkrát	sedmkrát	k6eAd1	sedmkrát
nominovaný	nominovaný	k2eAgMnSc1d1	nominovaný
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
získal	získat	k5eAaPmAgMnS	získat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Zanussi	Zanusse	k1gFnSc4	Zanusse
<g/>
,	,	kIx,	,
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Hollandová	Hollandový	k2eAgFnSc1d1	Hollandová
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Kieślowski	Kieślowsk	k1gFnSc2	Kieślowsk
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Polański	Polańsk	k1gFnSc2	Polańsk
<g/>
,	,	kIx,	,
Jerzy	Jerza	k1gFnPc1	Jerza
Kawalerowicz	Kawalerowicz	k1gMnSc1	Kawalerowicz
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
Munk	Munk	k1gMnSc1	Munk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lodži	Lodž	k1gFnSc6	Lodž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc4	první
stálý	stálý	k2eAgInSc4d1	stálý
biograf	biograf	k1gInSc4	biograf
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
filmová	filmový	k2eAgFnSc1d1	filmová
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
Varšavský	varšavský	k2eAgInSc1d1	varšavský
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
polské	polský	k2eAgMnPc4d1	polský
hudební	hudební	k2eAgMnPc4d1	hudební
skladatele	skladatel	k1gMnPc4	skladatel
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Karol	Karol	k1gInSc1	Karol
Szymanowski	Szymanowsk	k1gFnSc2	Szymanowsk
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gInSc4	Krzysztof
Penderecki	Pendereck	k1gFnSc2	Pendereck
<g/>
,	,	kIx,	,
Zbigniew	Zbigniew	k1gMnSc1	Zbigniew
Preisner	Preisner	k1gMnSc1	Preisner
<g/>
,	,	kIx,	,
Wojciech	Wojciech	k1gInSc1	Wojciech
Kilar	Kilara	k1gFnPc2	Kilara
<g/>
,	,	kIx,	,
Ignacy	Ignaca	k1gFnPc1	Ignaca
Jan	Jan	k1gMnSc1	Jan
Paderewski	Paderewsk	k1gFnPc4	Paderewsk
<g/>
,	,	kIx,	,
Henryk	Henryk	k1gInSc4	Henryk
Wieniawski	Wieniawsk	k1gFnSc2	Wieniawsk
<g/>
,	,	kIx,	,
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Komeda	Komeda	k1gMnSc1	Komeda
<g/>
,	,	kIx,	,
Stanisław	Stanisław	k1gMnSc1	Stanisław
Moniuszko	Moniuszka	k1gFnSc5	Moniuszka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kaczmarek	Kaczmarka	k1gFnPc2	Kaczmarka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Polska	Polsko	k1gNnSc2	Polsko
prosadili	prosadit	k5eAaPmAgMnP	prosadit
např.	např.	kA	např.
Czesław	Czesław	k1gMnPc1	Czesław
Niemen	Niemen	k2eAgMnSc1d1	Niemen
<g/>
,	,	kIx,	,
Maryla	Maryla	k1gFnSc1	Maryla
Rodowicz	Rodowicz	k1gMnSc1	Rodowicz
<g/>
,	,	kIx,	,
Jacek	Jacek	k1gMnSc1	Jacek
Kaczmarski	Kaczmarsk	k1gFnSc2	Kaczmarsk
či	či	k8xC	či
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
mnoha	mnoho	k4c2	mnoho
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
pořádá	pořádat	k5eAaImIp3nS	pořádat
nespočet	nespočet	k1gInSc1	nespočet
hudebních	hudební	k2eAgInPc2d1	hudební
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patří	patřit	k5eAaImIp3nP	patřit
Krajowy	Krajowa	k1gFnPc1	Krajowa
festiwal	festiwal	k1gInSc4	festiwal
Piosenki	Piosenk	k1gFnSc2	Piosenk
Polskiej	Polskiej	k1gInSc1	Polskiej
<g/>
,	,	kIx,	,
Sopot	sopot	k1gInSc1	sopot
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Coke	Coke	k1gInSc1	Coke
live	liv	k1gFnSc2	liv
music	musice	k1gFnPc2	musice
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Open	Open	k1gInSc1	Open
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Sopot	sopot	k1gInSc1	sopot
Top	topit	k5eAaImRp2nS	topit
trendy	trend	k1gInPc4	trend
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc1	festival
současné	současný	k2eAgFnSc2d1	současná
hudby	hudba	k1gFnSc2	hudba
Warszawska	Warszawsek	k1gMnSc2	Warszawsek
Jesień	Jesień	k1gMnSc2	Jesień
(	(	kIx(	(
<g/>
Varšavský	varšavský	k2eAgInSc1d1	varšavský
podzim	podzim	k1gInSc1	podzim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malíři	malíř	k1gMnPc1	malíř
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Matejko	Matejka	k1gFnSc5	Matejka
<g/>
,	,	kIx,	,
Jacek	Jacek	k1gMnSc1	Jacek
Malczewski	Malczewsk	k1gFnSc2	Malczewsk
<g/>
,	,	kIx,	,
Eligiusz	Eligiusz	k1gInSc4	Eligiusz
Niewiadomski	Niewiadomsk	k1gFnSc2	Niewiadomsk
<g/>
,	,	kIx,	,
Witold	Witold	k1gMnSc1	Witold
Wojtkiewicz	Wojtkiewicz	k1gMnSc1	Wojtkiewicz
<g/>
,	,	kIx,	,
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
Makowski	Makowsk	k1gFnSc2	Makowsk
<g/>
,	,	kIx,	,
Władysław	Władysław	k1gFnSc2	Władysław
Strzemiński	Strzemińsk	k1gFnSc2	Strzemińsk
<g/>
,	,	kIx,	,
Jerzy	Jerza	k1gFnSc2	Jerza
Nowosielski	Nowosielsk	k1gFnSc2	Nowosielsk
<g/>
,	,	kIx,	,
Jerzy	Jerza	k1gFnPc1	Jerza
Duda-Gracz	Duda-Gracz	k1gMnSc1	Duda-Gracz
<g/>
,	,	kIx,	,
Zdzisław	Zdzisław	k1gMnSc1	Zdzisław
Beksiński	Beksińsk	k1gFnSc2	Beksińsk
<g/>
,	,	kIx,	,
Franciszek	Franciszek	k1gInSc4	Franciszek
Starowieyski	Starowieysk	k1gFnSc2	Starowieysk
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Opałka	Opałka	k1gMnSc1	Opałka
Sochaři	sochař	k1gMnSc3	sochař
<g/>
:	:	kIx,	:
Ksawery	Ksawera	k1gFnSc2	Ksawera
Dunikowski	Dunikowsk	k1gFnSc2	Dunikowsk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Abakanowiczová	Abakanowiczová	k1gFnSc1	Abakanowiczová
<g/>
,	,	kIx,	,
Alina	Alien	k2eAgFnSc1d1	Alina
Szapocznikow	Szapocznikow	k1gFnSc1	Szapocznikow
<g/>
,	,	kIx,	,
Władysław	Władysław	k1gFnSc1	Władysław
Hasior	Hasiora	k1gFnPc2	Hasiora
Mezi	mezi	k7c4	mezi
proslulé	proslulý	k2eAgMnPc4d1	proslulý
polské	polský	k2eAgMnPc4d1	polský
vědce	vědec	k1gMnPc4	vědec
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
(	(	kIx(	(
<g/>
astronom	astronom	k1gMnSc1	astronom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
(	(	kIx(	(
<g/>
laureátka	laureátka	k1gFnSc1	laureátka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bronisław	Bronisław	k1gMnSc1	Bronisław
Malinowski	Malinowsk	k1gFnSc2	Malinowsk
(	(	kIx(	(
<g/>
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Banach	Banach	k1gMnSc1	Banach
a	a	k8xC	a
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Kuratowski	Kuratowsk	k1gFnSc2	Kuratowsk
(	(	kIx(	(
<g/>
matematici	matematik	k1gMnPc1	matematik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Ingarden	Ingardno	k1gNnPc2	Ingardno
a	a	k8xC	a
Leszek	Leszka	k1gFnPc2	Leszka
Kołakowski	Kołakowsk	k1gMnPc1	Kołakowsk
(	(	kIx(	(
<g/>
filosofové	filosof	k1gMnPc1	filosof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
vzoru	vzor	k1gInSc6	vzor
sloučeno	sloučen	k2eAgNnSc1d1	sloučeno
pod	pod	k7c4	pod
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
Polskou	polský	k2eAgFnSc4d1	polská
akademii	akademie	k1gFnSc4	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Školský	školský	k2eAgInSc1d1	školský
systém	systém	k1gInSc1	systém
po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
stanoví	stanovit	k5eAaPmIp3nS	stanovit
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
gymnázia	gymnázium	k1gNnPc4	gymnázium
(	(	kIx(	(
<g/>
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
žák	žák	k1gMnSc1	žák
přihlásit	přihlásit	k5eAaPmF	přihlásit
na	na	k7c4	na
lyceum	lyceum	k1gNnSc4	lyceum
<g/>
,	,	kIx,	,
technikum	technikum	k1gNnSc1	technikum
nebo	nebo	k8xC	nebo
učiliště	učiliště	k1gNnSc1	učiliště
<g/>
,	,	kIx,	,
z	z	k7c2	z
lycea	lyceum	k1gNnSc2	lyceum
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
hlásí	hlásit	k5eAaImIp3nS	hlásit
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgFnSc1d2	starší
je	být	k5eAaImIp3nS	být
však	však	k9	však
Jagellonská	jagellonský	k2eAgFnSc1d1	Jagellonská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1364	[number]	k4	1364
(	(	kIx(	(
<g/>
po	po	k7c6	po
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
je	být	k5eAaImIp3nS	být
tak	tak	k8xC	tak
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
polské	polský	k2eAgFnSc2d1	polská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
je	být	k5eAaImIp3nS	být
zřízení	zřízení	k1gNnSc1	zřízení
Komise	komise	k1gFnSc2	komise
národního	národní	k2eAgNnSc2d1	národní
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgFnPc1d1	považovaná
za	za	k7c4	za
první	první	k4xOgNnSc4	první
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
osvěty	osvěta	k1gFnSc2	osvěta
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejpopulárnějším	populární	k2eAgInPc3d3	nejpopulárnější
sportům	sport	k1gInPc3	sport
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
patří	patřit	k5eAaImIp3nS	patřit
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc1	volejbal
<g/>
,	,	kIx,	,
plochá	plochý	k2eAgFnSc1d1	plochá
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
nebo	nebo	k8xC	nebo
klasické	klasický	k2eAgNnSc4d1	klasické
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Legia	Legia	k1gFnSc1	Legia
Warszawa	Warszawa	k1gFnSc1	Warszawa
<g/>
,	,	kIx,	,
Lech	Lech	k1gMnSc1	Lech
Poznaň	Poznaň	k1gFnSc1	Poznaň
nebo	nebo	k8xC	nebo
Wisla	Wisla	k1gMnSc1	Wisla
Kraków	Kraków	k1gMnSc1	Kraków
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
těchto	tento	k3xDgInPc2	tento
týmů	tým	k1gInPc2	tým
jsou	být	k5eAaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
jako	jako	k8xC	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
Hooligans	Hooligansa	k1gFnPc2	Hooligansa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
především	především	k6eAd1	především
ve	v	k7c6	v
volejbalu	volejbal	k1gInSc6	volejbal
a	a	k8xC	a
ploché	plochý	k2eAgFnSc6d1	plochá
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgMnPc7d1	současný
známými	známý	k2eAgMnPc7d1	známý
sportovci	sportovec	k1gMnPc7	sportovec
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Robert	Robert	k1gMnSc1	Robert
Lewandowski	Lewandowsk	k1gFnSc2	Lewandowsk
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lukasz	Lukasz	k1gMnSc1	Lukasz
Fabianski	Fabiansk	k1gFnSc2	Fabiansk
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marcin	Marcin	k1gMnSc1	Marcin
Gortat	Gortat	k1gMnSc1	Gortat
(	(	kIx(	(
<g/>
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Justyna	Justyna	k1gFnSc1	Justyna
Kowalczyk	Kowalczyk	k1gMnSc1	Kowalczyk
(	(	kIx(	(
<g/>
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Radwańska	Radwańska	k1gFnSc1	Radwańska
(	(	kIx(	(
<g/>
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jerzy	Jerz	k1gInPc1	Jerz
Janowicz	Janowicz	k1gInSc1	Janowicz
(	(	kIx(	(
<g/>
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jarosław	Jarosław	k1gMnSc1	Jarosław
Hampel	Hampel	k1gMnSc1	Hampel
(	(	kIx(	(
<g/>
plochá	plochý	k2eAgFnSc1d1	plochá
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Tomasz	Tomasz	k1gMnSc1	Tomasz
Adamek	Adamek	k1gMnSc1	Adamek
(	(	kIx(	(
<g/>
box	box	k1gInSc1	box
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velké	velká	k1gFnPc4	velká
legendy	legenda	k1gFnSc2	legenda
sportu	sport	k1gInSc2	sport
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Adam	Adam	k1gMnSc1	Adam
Małysz	Małysz	k1gMnSc1	Małysz
(	(	kIx(	(
<g/>
skoky	skok	k1gInPc1	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ebi	Ebi	k1gFnSc1	Ebi
Smolarek	Smolarka	k1gFnPc2	Smolarka
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jerzy	Jerza	k1gFnSc2	Jerza
Dudek	Dudek	k1gMnSc1	Dudek
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Kubica	Kubica	k1gMnSc1	Kubica
(	(	kIx(	(
<g/>
formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
rally	ralla	k1gFnSc2	ralla
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Tomasz	Tomasz	k1gInSc1	Tomasz
Gollob	Golloba	k1gFnPc2	Golloba
(	(	kIx(	(
<g/>
plochá	plochý	k2eAgFnSc1d1	plochá
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
Kolem	kolem	k7c2	kolem
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
jel	jet	k5eAaImAgInS	jet
Závod	závod	k1gInSc1	závod
míru	míra	k1gFnSc4	míra
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgInS	stát
největší	veliký	k2eAgInSc1d3	veliký
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
otevřen	otevřít	k5eAaPmNgInS	otevřít
Národní	národní	k2eAgInSc1d1	národní
stadion	stadion	k1gInSc1	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Stálé	stálý	k2eAgInPc1d1	stálý
svátky	svátek	k1gInPc1	svátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
Nowy	Nowy	k1gInPc4	Nowy
Rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Tři	tři	k4xCgMnPc4	tři
králové	král	k1gMnPc1	král
(	(	kIx(	(
<g/>
Trzech	Trzech	k1gInSc1	Trzech
Króli	Króle	k1gFnSc4	Króle
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Svátek	svátek	k1gInSc1	svátek
práce	práce	k1gFnSc1	práce
(	(	kIx(	(
<g/>
Święto	Święto	k1gNnSc1	Święto
Pracy	Praca	k1gFnSc2	Praca
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1791	[number]	k4	1791
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Uchwalenie	Uchwalenie	k1gFnSc1	Uchwalenie
Konstytucji	Konstytucj	k1gInSc6	Konstytucj
3	[number]	k4	3
Maja	Maja	k1gFnSc1	Maja
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Polské	polský	k2eAgFnSc2d1	polská
Armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Wniebowzięcie	Wniebowzięcie	k1gFnSc2	Wniebowzięcie
Najświętszej	Najświętszej	k1gFnSc2	Najświętszej
Maryi	Mary	k1gFnSc2	Mary
Panny	Panna	k1gFnSc2	Panna
<g/>
,	,	kIx,	,
Święto	Święto	k1gNnSc4	Święto
Wojska	Wojsek	k1gMnSc2	Wojsek
Polskiego	Polskiego	k1gMnSc1	Polskiego
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
Wszystkich	Wszystkich	k1gMnSc1	Wszystkich
Świętych	Świętych	k1gMnSc1	Świętych
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Polska	Polsko	k1gNnSc2	Polsko
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Odzyskanie	Odzyskanie	k1gFnPc4	Odzyskanie
Niepodległości	Niepodległośce	k1gFnSc4	Niepodległośce
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
Boże	Boże	k1gFnSc1	Boże
Narodzenie	Narodzenie	k1gFnSc1	Narodzenie
<g/>
)	)	kIx)	)
Pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
svátky	svátek	k1gInPc1	svátek
Velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
neděle	neděle	k1gFnSc2	neděle
(	(	kIx(	(
<g/>
Zmartwychwstanie	Zmartwychwstanie	k1gFnSc1	Zmartwychwstanie
Pańskie	Pańskie	k1gFnSc1	Pańskie
-	-	kIx~	-
Niedziela	Niedziela	k1gFnSc1	Niedziela
Wielkanocna	Wielkanocna	k1gFnSc1	Wielkanocna
<g/>
)	)	kIx)	)
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
pondělí	pondělí	k1gNnSc1	pondělí
(	(	kIx(	(
<g/>
Poniedziałek	Poniedziałek	k1gInSc1	Poniedziałek
Wielkanocny	Wielkanocna	k1gFnSc2	Wielkanocna
<g/>
)	)	kIx)	)
Seslání	seslání	k1gNnSc1	seslání
Ducha	duch	k1gMnSc2	duch
<g />
.	.	kIx.	.
</s>
<s>
Svatého	svatý	k2eAgMnSc4d1	svatý
-	-	kIx~	-
(	(	kIx(	(
<g/>
Zesłania	Zesłanium	k1gNnPc4	Zesłanium
Ducha	duch	k1gMnSc2	duch
Świętego	Świętego	k1gMnSc1	Świętego
-	-	kIx~	-
Zielone	Zielon	k1gInSc5	Zielon
Świątki	Świątki	k1gNnPc7	Świątki
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
50	[number]	k4	50
dnů	den	k1gInPc2	den
po	po	k7c4	po
Velikonoční	velikonoční	k2eAgFnSc4d1	velikonoční
neděli	neděle	k1gFnSc4	neděle
Těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
Krve	krev	k1gFnSc2	krev
Páně	páně	k2eAgFnSc2d1	páně
-	-	kIx~	-
Boží	boží	k2eAgNnSc1d1	boží
tělo	tělo	k1gNnSc1	tělo
(	(	kIx(	(
<g/>
Ciała	Ciała	k1gFnSc1	Ciała
i	i	k8xC	i
Krwi	Krwi	k1gNnSc1	Krwi
Pańskiej	Pańskiej	k1gInSc1	Pańskiej
-	-	kIx~	-
Boże	Boże	k1gFnSc1	Boże
Ciało	Ciało	k1gMnSc1	Ciało
<g/>
)	)	kIx)	)
-	-	kIx~	-
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
60	[number]	k4	60
dnů	den	k1gInPc2	den
po	po	k7c4	po
Velikonoční	velikonoční	k2eAgFnSc4d1	velikonoční
neděli	neděle	k1gFnSc4	neděle
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
polskou	polský	k2eAgFnSc7d1	polská
tenistkou	tenistka	k1gFnSc7	tenistka
je	být	k5eAaImIp3nS	být
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Radwańská	Radwańská	k1gFnSc1	Radwańská
<g/>
.	.	kIx.	.
</s>
