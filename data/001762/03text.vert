<s>
Oscar	Oscar	k1gMnSc1	Oscar
Fingal	Fingal	k1gMnSc1	Fingal
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Flahertie	Flahertie	k1gFnSc1	Flahertie
Wills	Willsa	k1gFnPc2	Willsa
Wilde	Wild	k1gInSc5	Wild
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
Dublin	Dublin	k1gInSc1	Dublin
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
působící	působící	k2eAgMnSc1d1	působící
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
William	William	k1gInSc4	William
byl	být	k5eAaImAgMnS	být
přední	přední	k2eAgMnSc1d1	přední
irský	irský	k2eAgMnSc1d1	irský
oční	oční	k2eAgMnSc1d1	oční
a	a	k8xC	a
ušní	ušní	k2eAgMnSc1d1	ušní
chirurg	chirurg	k1gMnSc1	chirurg
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Jane	Jan	k1gMnSc5	Jan
byla	být	k5eAaImAgFnS	být
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
a	a	k8xC	a
irskou	irský	k2eAgFnSc7d1	irská
nacionalistkou	nacionalistka	k1gFnSc7	nacionalistka
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgFnSc7d1	píšící
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Speranza	Speranza	k1gFnSc1	Speranza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
klasickou	klasický	k2eAgFnSc4d1	klasická
filologii	filologie	k1gFnSc4	filologie
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
a	a	k8xC	a
na	na	k7c6	na
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
verše	verš	k1gInPc4	verš
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
dekadentními	dekadentní	k2eAgInPc7d1	dekadentní
názory	názor	k1gInPc7	názor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
měly	mít	k5eAaImAgInP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
svobodně	svobodně	k6eAd1	svobodně
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
pokryteckou	pokrytecký	k2eAgFnSc4d1	pokrytecká
morálku	morálka	k1gFnSc4	morálka
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
vědomě	vědomě	k6eAd1	vědomě
šokoval	šokovat	k5eAaBmAgMnS	šokovat
prudérní	prudérní	k2eAgFnSc4d1	prudérní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Constance	Constanec	k1gMnPc4	Constanec
Lloydovou	Lloydová	k1gFnSc4	Lloydová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
finanční	finanční	k2eAgNnSc4d1	finanční
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
žít	žít	k5eAaImF	žít
v	v	k7c6	v
relativním	relativní	k2eAgInSc6d1	relativní
luxusu	luxus	k1gInSc6	luxus
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
byl	být	k5eAaImAgInS	být
citlivým	citlivý	k2eAgMnSc7d1	citlivý
a	a	k8xC	a
milujícím	milující	k2eAgMnSc7d1	milující
otcem	otec	k1gMnSc7	otec
dvou	dva	k4xCgInPc2	dva
synů	syn	k1gMnPc2	syn
-	-	kIx~	-
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Vyvyana	Vyvyan	k1gMnSc2	Vyvyan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
udržoval	udržovat	k5eAaImAgInS	udržovat
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Alfredem	Alfred	k1gMnSc7	Alfred
"	"	kIx"	"
<g/>
Bosie	Bosie	k1gFnSc1	Bosie
<g/>
"	"	kIx"	"
Douglasem	Douglasma	k1gFnPc2	Douglasma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naráželo	narážet	k5eAaImAgNnS	narážet
na	na	k7c4	na
nevoli	nevole	k1gFnSc4	nevole
Bosieho	Bosie	k1gMnSc2	Bosie
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
markýze	markýz	k1gMnSc2	markýz
Queensberryho	Queensberry	k1gMnSc2	Queensberry
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1895	[number]	k4	1895
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vyvolat	vyvolat	k5eAaPmF	vyvolat
skandál	skandál	k1gInSc4	skandál
na	na	k7c6	na
premiéře	premiéra	k1gFnSc6	premiéra
Wildeovy	Wildeův	k2eAgFnSc2d1	Wildeova
hry	hra	k1gFnSc2	hra
Jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
míti	mít	k5eAaImF	mít
Filipa	Filip	k1gMnSc4	Filip
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Bosie	Bosie	k1gFnSc2	Bosie
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Wildea	Wildeus	k1gMnSc4	Wildeus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
otce	otec	k1gMnSc4	otec
žaloval	žalovat	k5eAaImAgMnS	žalovat
pro	pro	k7c4	pro
urážku	urážka	k1gFnSc4	urážka
na	na	k7c6	na
cti	čest	k1gFnSc6	čest
<g/>
.	.	kIx.	.
</s>
<s>
Probíhající	probíhající	k2eAgInSc1d1	probíhající
soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
však	však	k9	však
posléze	posléze	k6eAd1	posléze
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Markýz	markýz	k1gMnSc1	markýz
Queensberry	Queensberra	k1gFnSc2	Queensberra
byl	být	k5eAaImAgMnS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
Wilde	Wild	k1gInSc5	Wild
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zatčen	zatknout	k5eAaPmNgInS	zatknout
pro	pro	k7c4	pro
přečin	přečin	k1gInSc4	přečin
proti	proti	k7c3	proti
mravopočestnosti	mravopočestnost	k1gFnSc3	mravopočestnost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
započal	započnout	k5eAaPmAgInS	započnout
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
byl	být	k5eAaImAgInS	být
Wilde	Wild	k1gInSc5	Wild
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgInS	odbýt
ve	v	k7c6	v
Wandsworthu	Wandsworth	k1gInSc6	Wandsworth
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Readingu	Reading	k1gInSc6	Reading
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Constance	Constanec	k1gInSc2	Constanec
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
a	a	k8xC	a
změnila	změnit	k5eAaPmAgFnS	změnit
sobě	se	k3xPyFc3	se
a	a	k8xC	a
synům	syn	k1gMnPc3	syn
příjmení	příjmení	k1gNnSc4	příjmení
na	na	k7c6	na
Hollandovi	Holland	k1gMnSc6	Holland
<g/>
.	.	kIx.	.
</s>
<s>
Constance	Constance	k1gFnSc1	Constance
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Janově	Janov	k1gInSc6	Janov
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Cyril	Cyril	k1gMnSc1	Cyril
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vyvyan	Vyvyan	k1gInSc1	Vyvyan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
a	a	k8xC	a
překladatelem	překladatel	k1gMnSc7	překladatel
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	svůj	k3xOyFgFnSc2	svůj
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
na	na	k7c4	na
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Melmotha	Melmoth	k1gMnSc4	Melmoth
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
potuloval	potulovat	k5eAaImAgMnS	potulovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
převážně	převážně	k6eAd1	převážně
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
s	s	k7c7	s
podlomeným	podlomený	k2eAgNnSc7d1	podlomené
zdravím	zdraví	k1gNnSc7	zdraví
na	na	k7c4	na
meningitidu	meningitida	k1gFnSc4	meningitida
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alsace	Alsace	k1gFnSc2	Alsace
v	v	k7c6	v
rue	rue	k?	rue
des	des	k1gNnSc2	des
Beaux-Arts	Beaux-Artsa	k1gFnPc2	Beaux-Artsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gNnSc4	jeho
přátelé	přítel	k1gMnPc1	přítel
Robert	Robert	k1gMnSc1	Robert
Ross	Ross	k1gInSc1	Ross
a	a	k8xC	a
Regie	Regie	k1gFnSc1	Regie
Sherard	Sherarda	k1gFnPc2	Sherarda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
však	však	k9	však
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
uloženy	uložen	k2eAgInPc1d1	uložen
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c4	v
Bagneux	Bagneux	k1gInSc4	Bagneux
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
lord	lord	k1gMnSc1	lord
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
Poems	Poems	k1gInSc1	Poems
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
dekadentních	dekadentní	k2eAgInPc2d1	dekadentní
básní	básnit	k5eAaImIp3nS	básnit
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
žaláři	žalář	k1gInSc6	žalář
v	v	k7c6	v
Readingu	Reading	k1gInSc6	Reading
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
Reading	Reading	k1gInSc1	Reading
Gaol	Gaola	k1gFnPc2	Gaola
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
aneb	aneb	k?	aneb
Nihilistka	nihilistka	k1gFnSc1	nihilistka
(	(	kIx(	(
<g/>
Vera	Vera	k1gMnSc1	Vera
<g/>
,	,	kIx,	,
or	or	k?	or
The	The	k1gFnPc1	The
Nihilists	Nihilistsa	k1gFnPc2	Nihilistsa
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
Vévodkyně	vévodkyně	k1gFnPc4	vévodkyně
z	z	k7c2	z
Padovy	Padova	k1gFnSc2	Padova
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Duchess	Duchess	k1gInSc1	Duchess
of	of	k?	of
Padua	Padua	k1gFnSc1	Padua
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Salome	Salom	k1gInSc5	Salom
(	(	kIx(	(
<g/>
Salomé	Salomý	k2eAgInPc1d1	Salomý
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
verze	verze	k1gFnSc1	verze
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Vějíř	vějíř	k1gInSc1	vějíř
lady	lady	k1gFnSc2	lady
Windermerové	Windermerová	k1gFnSc2	Windermerová
(	(	kIx(	(
<g/>
Lady	lady	k1gFnSc1	lady
Windermere	Windermer	k1gInSc5	Windermer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fan	Fana	k1gFnPc2	Fana
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Bezvýznamná	bezvýznamný	k2eAgFnSc1d1	bezvýznamná
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
A	a	k9	a
Woman	Woman	k1gMnSc1	Woman
of	of	k?	of
No	no	k9	no
Importance	importance	k1gFnSc1	importance
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Ideální	ideální	k2eAgMnSc1d1	ideální
manžel	manžel	k1gMnSc1	manžel
(	(	kIx(	(
<g/>
An	An	k1gMnSc1	An
Ideal	Ideal	k1gMnSc1	Ideal
Husband	Husbanda	k1gFnPc2	Husbanda
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
míti	mít	k5eAaImF	mít
Filipa	Filip	k1gMnSc4	Filip
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Importance	importance	k1gFnSc2	importance
of	of	k?	of
Being	Being	k1gMnSc1	Being
Earnest	Earnest	k1gMnSc1	Earnest
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
Strašidlo	strašidlo	k1gNnSc4	strašidlo
cantervillské	cantervillský	k2eAgFnSc2d1	cantervillský
(	(	kIx(	(
<g/>
The	The	k1gFnSc2	The
Canterville	Canterville	k1gFnSc2	Canterville
Ghost	Ghost	k1gFnSc1	Ghost
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
Šťastný	Šťastný	k1gMnSc1	Šťastný
princ	princ	k1gMnSc1	princ
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
pohádky	pohádka	k1gFnPc4	pohádka
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Happy	Happa	k1gFnSc2	Happa
Prince	princ	k1gMnSc2	princ
and	and	k?	and
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
Portrét	portrét	k1gInSc1	portrét
pana	pan	k1gMnSc2	pan
W.	W.	kA	W.
H.	H.	kA	H.
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Portrait	Portrait	k1gMnSc1	Portrait
of	of	k?	of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
W.	W.	kA	W.
H.	H.	kA	H.
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
Zločin	zločin	k1gInSc1	zločin
lorda	lord	k1gMnSc2	lord
Arthura	Arthur	k1gMnSc2	Arthur
Savila	Savil	k1gMnSc2	Savil
(	(	kIx(	(
<g/>
Lord	lord	k1gMnSc1	lord
Arthur	Arthur	k1gMnSc1	Arthur
Saville	Saville	k1gInSc1	Saville
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Crime	Crim	k1gInSc5	Crim
and	and	k?	and
other	other	k1gMnSc1	other
Stories	Stories	k1gMnSc1	Stories
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Intence	intence	k1gFnSc1	intence
(	(	kIx(	(
<g/>
Intentions	Intentions	k1gInSc1	Intentions
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Picture	Pictur	k1gMnSc5	Pictur
of	of	k?	of
Dorian	Dorian	k1gInSc4	Dorian
Gray	Graa	k1gMnSc2	Graa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
-	-	kIx~	-
jediný	jediný	k2eAgInSc1d1	jediný
román	román	k1gInSc1	román
Dům	dům	k1gInSc1	dům
granátových	granátový	k2eAgNnPc2d1	granátové
jablek	jablko	k1gNnPc2	jablko
(	(	kIx(	(
<g/>
A	a	k9	a
House	house	k1gNnSc1	house
of	of	k?	of
Pomegranates	Pomegranates	k1gMnSc1	Pomegranates
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Lidská	lidský	k2eAgFnSc1d1	lidská
duše	duše	k1gFnSc1	duše
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Soul	Soul	k1gInSc1	Soul
of	of	k?	of
Man	Man	k1gMnSc1	Man
Under	Under	k1gMnSc1	Under
Socialism	Socialism	k1gMnSc1	Socialism
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
De	De	k?	De
Profundis	Profundis	k1gInSc1	Profundis
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
Kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ministrant	ministrant	k1gMnSc1	ministrant
(	(	kIx(	(
<g/>
Priest	Priest	k1gMnSc1	Priest
and	and	k?	and
the	the	k?	the
acolyte	acolyt	k1gInSc5	acolyt
<g/>
)	)	kIx)	)
Sfinga	sfinga	k1gFnSc1	sfinga
bez	bez	k7c2	bez
záhady	záhada	k1gFnSc2	záhada
Slavík	slavík	k1gInSc1	slavík
a	a	k8xC	a
růže	růž	k1gInPc1	růž
Neobyčejný	obyčejný	k2eNgInSc1d1	neobyčejný
model	model	k1gInSc4	model
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
zdrojích	zdroj	k1gInPc6	zdroj
(	(	kIx(	(
<g/>
které	který	k3yRgFnSc3	který
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
vycházejí	vycházet	k5eAaImIp3nP	vycházet
jen	jen	k9	jen
z	z	k7c2	z
tvrzení	tvrzení	k1gNnSc2	tvrzení
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
k	k	k7c3	k
českému	český	k2eAgNnSc3d1	české
vydání	vydání	k1gNnSc3	vydání
textu	text	k1gInSc2	text
z	z	k7c2	z
r.	r.	kA	r.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
též	též	k9	též
někdy	někdy	k6eAd1	někdy
připisováno	připisován	k2eAgNnSc1d1	připisováno
autorství	autorství	k1gNnSc1	autorství
krátké	krátký	k2eAgFnSc2d1	krátká
pornografické	pornografický	k2eAgFnSc2d1	pornografická
novely	novela	k1gFnSc2	novela
Lady	lady	k1gFnPc2	lady
Pokingham	Pokingham	k1gInSc1	Pokingham
<g/>
,	,	kIx,	,
Or	Or	k1gMnSc1	Or
They	Thea	k1gFnSc2	Thea
All	All	k1gFnSc2	All
Do	do	k7c2	do
It.	It.	k1gFnSc2	It.
Novela	novela	k1gFnSc1	novela
anonymně	anonymně	k6eAd1	anonymně
vycházela	vycházet	k5eAaImAgFnS	vycházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
časopise	časopis	k1gInSc6	časopis
The	The	k1gMnSc1	The
Pearl	Pearl	k1gMnSc1	Pearl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
vydáních	vydání	k1gNnPc6	vydání
však	však	k8xC	však
není	být	k5eNaImIp3nS	být
Wildovo	Wildův	k2eAgNnSc1d1	Wildovo
autorství	autorství	k1gNnSc1	autorství
zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lady	Lada	k1gFnSc2	Lada
Fuckingham	Fuckingham	k1gInSc1	Fuckingham
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
za	za	k7c7	za
pseudonymem	pseudonym	k1gInSc7	pseudonym
překladatele	překladatel	k1gMnSc2	překladatel
"	"	kIx"	"
<g/>
Voloďa	Voloďa	k1gMnSc1	Voloďa
Miljuchin	Miljuchin	k1gMnSc1	Miljuchin
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
skrýval	skrývat	k5eAaImAgInS	skrývat
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
bibliografické	bibliografický	k2eAgFnSc2d1	bibliografická
databáze	databáze	k1gFnSc2	databáze
Historického	historický	k2eAgInSc2d1	historický
ústavu	ústav	k1gInSc2	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Miltner	Miltner	k1gMnSc1	Miltner
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
indolog-lingvista	indologingvista	k1gMnSc1	indolog-lingvista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
