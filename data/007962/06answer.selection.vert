<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
981	[number]	k4	981
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
české	český	k2eAgFnPc1d1	Česká
(	(	kIx(	(
<g/>
Lutomisl	Lutomisl	k1gInSc1	Lutomisl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k6eAd1	až
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
