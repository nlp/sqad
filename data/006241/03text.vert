<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
PrF	PrF	k1gFnSc2	PrF
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
fakult	fakulta	k1gFnPc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
školou	škola	k1gFnSc7	škola
normativní	normativní	k2eAgFnSc1d1	normativní
teorie	teorie	k1gFnSc1	teorie
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
klasicizující	klasicizující	k2eAgFnSc6d1	klasicizující
budově	budova	k1gFnSc6	budova
na	na	k7c4	na
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
právní	právní	k2eAgNnSc1d1	právní
vzdělání	vzdělání	k1gNnSc1	vzdělání
na	na	k7c6	na
bakalářské	bakalářský	k2eAgFnSc6d1	Bakalářská
(	(	kIx(	(
<g/>
Bc.	Bc.	k1gFnSc6	Bc.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
magisterské	magisterský	k2eAgInPc1d1	magisterský
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
i	i	k9	i
doktorské	doktorský	k2eAgInPc1d1	doktorský
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
českých	český	k2eAgFnPc2d1	Česká
veřejných	veřejný	k2eAgFnPc2d1	veřejná
právnických	právnický	k2eAgFnPc2d1	právnická
fakult	fakulta	k1gFnPc2	fakulta
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
výuky	výuka	k1gFnSc2	výuka
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
ale	ale	k9	ale
právo	právo	k1gNnSc1	právo
přednášelo	přednášet	k5eAaImAgNnS	přednášet
jen	jen	k9	jen
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sem	sem	k6eAd1	sem
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
její	její	k3xOp3gFnSc2	její
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
citelná	citelný	k2eAgFnSc1d1	citelná
potřeba	potřeba	k1gFnSc1	potřeba
existence	existence	k1gFnSc2	existence
nejen	nejen	k6eAd1	nejen
právnických	právnický	k2eAgFnPc2d1	právnická
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgFnPc4	veškerý
snahy	snaha	k1gFnPc4	snaha
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
moravského	moravský	k2eAgNnSc2d1	Moravské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
angažoval	angažovat	k5eAaBmAgMnS	angažovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
však	však	k9	však
vyšly	vyjít	k5eAaPmAgFnP	vyjít
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
zachovat	zachovat	k5eAaPmF	zachovat
převážně	převážně	k6eAd1	převážně
německý	německý	k2eAgInSc4d1	německý
charakter	charakter	k1gInSc4	charakter
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc1	některý
právní	právní	k2eAgInPc1d1	právní
obory	obor	k1gInPc1	obor
byly	být	k5eAaImAgInP	být
vyučovány	vyučovat	k5eAaImNgInP	vyučovat
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lékařskou	lékařský	k2eAgFnSc7d1	lékařská
zahájily	zahájit	k5eAaPmAgFnP	zahájit
výuku	výuka	k1gFnSc4	výuka
ještě	ještě	k9	ještě
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
/	/	kIx~	/
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
<g/>
,	,	kIx,	,
profesorský	profesorský	k2eAgInSc4d1	profesorský
sbor	sbor	k1gInSc4	sbor
dále	daleko	k6eAd2	daleko
tvořili	tvořit	k5eAaImAgMnP	tvořit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Baxa	Baxa	k1gMnSc1	Baxa
pro	pro	k7c4	pro
právní	právní	k2eAgFnPc4d1	právní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vacek	Vacek	k1gMnSc1	Vacek
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgFnSc4d1	srovnávací
pravovědu	pravověda	k1gFnSc4	pravověda
a	a	k8xC	a
církevní	církevní	k2eAgNnSc4d1	církevní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
nauky	nauka	k1gFnPc4	nauka
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Sedláček	Sedláček	k1gMnSc1	Sedláček
pro	pro	k7c4	pro
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kallab	Kallab	k1gMnSc1	Kallab
pro	pro	k7c4	pro
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dominik	Dominik	k1gMnSc1	Dominik
pro	pro	k7c4	pro
správní	správní	k2eAgNnSc4d1	správní
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
i	i	k9	i
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
umístění	umístění	k1gNnSc1	umístění
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
provizorní	provizorní	k2eAgNnSc4d1	provizorní
sídlo	sídlo	k1gNnSc4	sídlo
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
alumnátu	alumnát	k1gInSc2	alumnát
na	na	k7c4	na
Antonínské	Antonínský	k2eAgInPc4d1	Antonínský
1	[number]	k4	1
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
rektorát	rektorát	k1gInSc1	rektorát
VUT	VUT	kA	VUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
bylo	být	k5eAaImAgNnS	být
vystavět	vystavět	k5eAaPmF	vystavět
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Kraví	kraví	k2eAgFnSc2d1	kraví
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
souvisejících	související	k2eAgInPc6d1	související
pozemcích	pozemek	k1gInPc6	pozemek
mezi	mezi	k7c7	mezi
Žabovřeskami	Žabovřesky	k1gFnPc7	Žabovřesky
a	a	k8xC	a
Veveřím	veveří	k2eAgInSc7d1	veveří
celou	celý	k2eAgFnSc7d1	celá
akademickou	akademický	k2eAgFnSc7d1	akademická
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
budovy	budova	k1gFnPc4	budova
české	český	k2eAgFnSc2d1	Česká
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
nejen	nejen	k6eAd1	nejen
celá	celý	k2eAgFnSc1d1	celá
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
nebo	nebo	k8xC	nebo
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
však	však	k9	však
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
provázely	provázet	k5eAaImAgInP	provázet
značné	značný	k2eAgInPc1d1	značný
průtahy	průtah	k1gInPc1	průtah
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
krizi	krize	k1gFnSc3	krize
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
na	na	k7c6	na
zamýšleném	zamýšlený	k2eAgNnSc6d1	zamýšlené
Akademickém	akademický	k2eAgNnSc6d1	akademické
náměstí	náměstí	k1gNnSc6	náměstí
podařilo	podařit	k5eAaPmAgNnS	podařit
realizovat	realizovat	k5eAaBmF	realizovat
právě	právě	k6eAd1	právě
jen	jen	k9	jen
budovu	budova	k1gFnSc4	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
pražského	pražský	k2eAgMnSc2d1	pražský
architekta	architekt	k1gMnSc2	architekt
Aloise	Alois	k1gMnSc4	Alois
Dryáka	Dryák	k1gMnSc4	Dryák
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
členů	člen	k1gInPc2	člen
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
fakulta	fakulta	k1gFnSc1	fakulta
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
klasicizující	klasicizující	k2eAgFnSc6d1	klasicizující
budově	budova	k1gFnSc6	budova
s	s	k7c7	s
vertikálně	vertikálně	k6eAd1	vertikálně
členěnou	členěný	k2eAgFnSc7d1	členěná
fasádou	fasáda	k1gFnSc7	fasáda
obloženou	obložený	k2eAgFnSc4d1	obložená
travertinem	travertin	k1gInSc7	travertin
a	a	k8xC	a
červenými	červený	k2eAgFnPc7d1	červená
cihlami	cihla	k1gFnPc7	cihla
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
plně	plně	k6eAd1	plně
vyhovovala	vyhovovat	k5eAaImAgNnP	vyhovovat
jejím	její	k3xOp3gFnPc3	její
potřebám	potřeba	k1gFnPc3	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
přednáškové	přednáškový	k2eAgFnPc1d1	přednášková
i	i	k8xC	i
seminární	seminární	k2eAgFnPc1d1	seminární
místnosti	místnost	k1gFnPc1	místnost
<g/>
,	,	kIx,	,
pracovny	pracovna	k1gFnPc1	pracovna
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
tělocvična	tělocvična	k1gFnSc1	tělocvična
a	a	k8xC	a
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
celouniverzitní	celouniverzitní	k2eAgFnSc1d1	celouniverzitní
aula	aula	k1gFnSc1	aula
(	(	kIx(	(
<g/>
auditorium	auditorium	k1gNnSc1	auditorium
maximum	maximum	k1gNnSc1	maximum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
čelní	čelní	k2eAgFnSc4d1	čelní
stěnu	stěna	k1gFnSc4	stěna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
s	s	k7c7	s
ukrytím	ukrytí	k1gNnSc7	ukrytí
během	během	k7c2	během
protektorátního	protektorátní	k2eAgNnSc2d1	protektorátní
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
zdobí	zdobit	k5eAaImIp3nS	zdobit
obraz	obraz	k1gInSc1	obraz
Antonína	Antonín	k1gMnSc2	Antonín
Procházky	Procházka	k1gMnSc2	Procházka
Prométheus	Prométheus	k1gMnSc1	Prométheus
přinášející	přinášející	k2eAgFnSc2d1	přinášející
lidstvu	lidstvo	k1gNnSc3	lidstvo
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
také	také	k9	také
rektorát	rektorát	k1gInSc1	rektorát
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc1	období
významným	významný	k2eAgNnSc7d1	významné
střediskem	středisko	k1gNnSc7	středisko
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
normativní	normativní	k2eAgFnSc2d1	normativní
teorie	teorie	k1gFnSc2	teorie
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zde	zde	k6eAd1	zde
založil	založit	k5eAaPmAgMnS	založit
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
<g/>
,	,	kIx,	,
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
normativní	normativní	k2eAgFnSc4d1	normativní
školu	škola	k1gFnSc4	škola
Hanse	hansa	k1gFnSc6	hansa
Kelsena	Kelsen	k2eAgFnSc1d1	Kelsena
a	a	k8xC	a
formovala	formovat	k5eAaImAgFnS	formovat
mnoho	mnoho	k4c4	mnoho
právníků	právník	k1gMnPc2	právník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
fakultě	fakulta	k1gFnSc6	fakulta
vystudovali	vystudovat	k5eAaPmAgMnP	vystudovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
mezi	mezi	k7c7	mezi
skutečností	skutečnost	k1gFnSc7	skutečnost
a	a	k8xC	a
právní	právní	k2eAgFnSc7d1	právní
normou	norma	k1gFnSc7	norma
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
čistou	čistý	k2eAgFnSc4d1	čistá
metodologii	metodologie	k1gFnSc4	metodologie
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
jen	jen	k6eAd1	jen
na	na	k7c6	na
zkoumání	zkoumání	k1gNnSc6	zkoumání
závaznosti	závaznost	k1gFnSc2	závaznost
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teleologickou	teleologický	k2eAgFnSc4d1	teleologická
národohospodářskou	národohospodářský	k2eAgFnSc4d1	Národohospodářská
školu	škola	k1gFnSc4	škola
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zakladatelského	zakladatelský	k2eAgInSc2d1	zakladatelský
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
dále	daleko	k6eAd2	daleko
učil	učít	k5eAaPmAgMnS	učít
např.	např.	kA	např.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Neubauer	Neubauer	k1gMnSc1	Neubauer
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
zde	zde	k6eAd1	zde
zaváděl	zavádět	k5eAaImAgMnS	zavádět
Michal	Michal	k1gMnSc1	Michal
Arturovič	Arturovič	k1gMnSc1	Arturovič
Zimmermann	Zimmermann	k1gMnSc1	Zimmermann
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Loevenstein	Loevenstein	k1gMnSc1	Loevenstein
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vybral	vybrat	k5eAaPmAgMnS	vybrat
učili	učit	k5eAaImAgMnP	učit
finanční	finanční	k2eAgNnSc4d1	finanční
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Rouček	Rouček	k1gMnSc1	Rouček
právo	právo	k1gNnSc4	právo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
směnečné	směnečný	k2eAgFnSc2d1	směnečná
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kubeš	Kubeš	k1gMnSc1	Kubeš
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vážný	vážný	k1gMnSc1	vážný
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
Bulín	Bulín	k1gMnSc1	Bulín
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Gerlich	Gerlich	k1gMnSc1	Gerlich
civilní	civilní	k2eAgNnSc4d1	civilní
řízení	řízení	k1gNnSc4	řízení
soudní	soudní	k2eAgNnSc4d1	soudní
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
Jan	Jan	k1gMnSc1	Jan
Vážný	vážný	k1gMnSc1	vážný
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čáda	Čád	k1gInSc2	Čád
právní	právní	k2eAgFnPc1d1	právní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgNnSc1d1	církevní
právo	právo	k1gNnSc1	právo
Rudolf	Rudolf	k1gMnSc1	Rudolf
Wierer	Wierer	k1gMnSc1	Wierer
a	a	k8xC	a
statistickou	statistický	k2eAgFnSc4d1	statistická
vědu	věda	k1gFnSc4	věda
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
Dobroslav	Dobroslav	k1gMnSc1	Dobroslav
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
okupaci	okupace	k1gFnSc6	okupace
a	a	k8xC	a
protestech	protest	k1gInPc6	protest
českých	český	k2eAgMnPc2d1	český
studentů	student	k1gMnPc2	student
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
české	český	k2eAgFnPc1d1	Česká
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
a	a	k8xC	a
budovu	budova	k1gFnSc4	budova
brněnské	brněnský	k2eAgFnSc2d1	brněnská
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
si	se	k3xPyFc3	se
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
moravské	moravský	k2eAgNnSc4d1	Moravské
zemské	zemský	k2eAgNnSc4d1	zemské
velitelství	velitelství	k1gNnSc4	velitelství
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zde	zde	k6eAd1	zde
také	také	k9	také
vyšetřovalo	vyšetřovat	k5eAaImAgNnS	vyšetřovat
a	a	k8xC	a
lidový	lidový	k2eAgInSc1d1	lidový
soud	soud	k1gInSc1	soud
zde	zde	k6eAd1	zde
vynášel	vynášet	k5eAaImAgInS	vynášet
rozsudky	rozsudek	k1gInPc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Jan	Jan	k1gMnSc1	Jan
Vážný	vážný	k1gMnSc1	vážný
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Baxa	Baxa	k1gMnSc1	Baxa
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
budovu	budova	k1gFnSc4	budova
chtěla	chtít	k5eAaImAgFnS	chtít
naopak	naopak	k6eAd1	naopak
zabrat	zabrat	k5eAaPmF	zabrat
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
nakonec	nakonec	k6eAd1	nakonec
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
v	v	k7c6	v
technickém	technický	k2eAgNnSc6d1	technické
vybavení	vybavení	k1gNnSc6	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
navíc	navíc	k6eAd1	navíc
zůstali	zůstat	k5eAaPmAgMnP	zůstat
jen	jen	k9	jen
dva	dva	k4xCgMnPc1	dva
řádní	řádný	k2eAgMnPc1d1	řádný
(	(	kIx(	(
<g/>
Weyr	Weyr	k1gMnSc1	Weyr
a	a	k8xC	a
Čáda	Čáda	k1gMnSc1	Čáda
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
mimořádní	mimořádný	k2eAgMnPc1d1	mimořádný
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc4d1	nutné
učitelský	učitelský	k2eAgInSc4d1	učitelský
sbor	sbor	k1gInSc4	sbor
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ale	ale	k9	ale
situace	situace	k1gFnSc1	situace
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
filozofii	filozofie	k1gFnSc4	filozofie
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čáda	Čáda	k1gMnSc1	Čáda
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
právním	právní	k2eAgMnSc7d1	právní
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
právu	právo	k1gNnSc3	právo
ústavnímu	ústavní	k2eAgNnSc3d1	ústavní
<g/>
,	,	kIx,	,
správnímu	správní	k2eAgNnSc3d1	správní
právu	právo	k1gNnSc3	právo
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pošvář	Pošvář	k1gInSc4	Pošvář
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kučera	Kučera	k1gMnSc1	Kučera
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ečer	Ečer	k1gMnSc1	Ečer
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
finanční	finanční	k2eAgMnSc1d1	finanční
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chytil	Chytil	k1gMnSc1	Chytil
vedl	vést	k5eAaImAgMnS	vést
ústav	ústav	k1gInSc4	ústav
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
Bulín	Bulín	k1gMnSc1	Bulín
ústav	ústav	k1gInSc4	ústav
civilního	civilní	k2eAgNnSc2d1	civilní
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Cvetler	Cvetler	k1gMnSc1	Cvetler
učil	učit	k5eAaImAgMnS	učit
právo	právo	k1gNnSc4	právo
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
František	františek	k1gInSc1	františek
Kop	kopa	k1gFnPc2	kopa
právo	právo	k1gNnSc1	právo
církevní	církevní	k2eAgMnSc1d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Obnovit	obnovit	k5eAaPmF	obnovit
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
ústavy	ústav	k1gInPc1	ústav
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
statistiky	statistika	k1gFnSc2	statistika
a	a	k8xC	a
kriminologie	kriminologie	k1gFnSc2	kriminologie
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
stabilizace	stabilizace	k1gFnSc2	stabilizace
a	a	k8xC	a
slibného	slibný	k2eAgInSc2d1	slibný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
zapisovalo	zapisovat	k5eAaImAgNnS	zapisovat
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInPc2	tisíc
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
po	po	k7c6	po
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
suspendováno	suspendovat	k5eAaPmNgNnS	suspendovat
devět	devět	k4xCc1	devět
vyučujících	vyučující	k2eAgMnPc2d1	vyučující
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
profesoři	profesor	k1gMnPc1	profesor
Weyr	Weyr	k1gMnSc1	Weyr
a	a	k8xC	a
Kubeš	Kubeš	k1gMnSc1	Kubeš
byli	být	k5eAaImAgMnP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
značné	značný	k2eAgFnSc3d1	značná
persekuci	persekuce	k1gFnSc3	persekuce
<g/>
.	.	kIx.	.
</s>
<s>
Fakultní	fakultní	k2eAgInSc1d1	fakultní
akční	akční	k2eAgInSc1d1	akční
výbor	výbor	k1gInSc1	výbor
tehdy	tehdy	k6eAd1	tehdy
vedl	vést	k5eAaImAgInS	vést
Jaromír	Jaromír	k1gMnSc1	Jaromír
Blažke	Blažke	k1gFnPc2	Blažke
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převzal	převzít	k5eAaPmAgInS	převzít
ústav	ústav	k1gInSc4	ústav
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
musela	muset	k5eAaImAgFnS	muset
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
ukončit	ukončit	k5eAaPmF	ukončit
i	i	k9	i
řada	řada	k1gFnSc1	řada
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
jich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
přešetření	přešetření	k1gNnSc1	přešetření
studijního	studijní	k2eAgInSc2d1	studijní
prospěchu	prospěch	k1gInSc2	prospěch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
čistky	čistka	k1gFnPc1	čistka
však	však	k9	však
nestačily	stačit	k5eNaBmAgFnP	stačit
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
celá	celý	k2eAgFnSc1d1	celá
fakulta	fakulta	k1gFnSc1	fakulta
zrušena	zrušit	k5eAaPmNgFnS	zrušit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
studium	studium	k1gNnSc1	studium
práv	právo	k1gNnPc2	právo
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
život	život	k1gInSc4	život
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
ani	ani	k8xC	ani
nevstoupily	vstoupit	k5eNaPmAgFnP	vstoupit
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zřízené	zřízený	k2eAgFnSc2d1	zřízená
katedry	katedra	k1gFnSc2	katedra
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
profesorů	profesor	k1gMnPc2	profesor
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
Slovanského	slovanský	k2eAgInSc2d1	slovanský
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
fakulty	fakulta	k1gFnSc2	fakulta
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
na	na	k7c6	na
základě	základ	k1gInSc6	základ
změněné	změněný	k2eAgFnSc2d1	změněná
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
během	během	k7c2	během
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
příslušné	příslušný	k2eAgNnSc1d1	příslušné
vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výuka	výuka	k1gFnSc1	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
profesorů	profesor	k1gMnPc2	profesor
se	se	k3xPyFc4	se
ale	ale	k9	ale
vrátili	vrátit	k5eAaPmAgMnP	vrátit
už	už	k9	už
jen	jen	k9	jen
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
Bulín	Bulín	k1gMnSc1	Bulín
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Cvetler	Cvetler	k1gMnSc1	Cvetler
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pošvář	Pošvář	k1gMnSc1	Pošvář
<g/>
,	,	kIx,	,
také	také	k9	také
budova	budova	k1gFnSc1	budova
nebyla	být	k5eNaImAgFnS	být
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
akademií	akademie	k1gFnSc7	akademie
fakultě	fakulta	k1gFnSc6	fakulta
vrácena	vrátit	k5eAaPmNgFnS	vrátit
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
provizorní	provizorní	k2eAgFnSc1d1	provizorní
a	a	k8xC	a
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
vyhovující	vyhovující	k2eAgNnSc4d1	vyhovující
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Cyrilometodějské	cyrilometodějský	k2eAgFnSc2d1	Cyrilometodějská
záložny	záložna	k1gFnSc2	záložna
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nesla	nést	k5eAaImAgFnS	nést
označení	označení	k1gNnSc4	označení
podle	podle	k7c2	podle
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
přejmenované	přejmenovaný	k2eAgFnSc2d1	přejmenovaná
univerzity	univerzita	k1gFnSc2	univerzita
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
University	universita	k1gFnSc2	universita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Děkanem	děkan	k1gMnSc7	děkan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Klokočka	Klokočka	k1gFnSc1	Klokočka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
během	během	k7c2	během
normalizace	normalizace	k1gFnSc2	normalizace
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
zcela	zcela	k6eAd1	zcela
rozešla	rozejít	k5eAaPmAgFnS	rozejít
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
tradicí	tradice	k1gFnSc7	tradice
svobodného	svobodný	k2eAgNnSc2d1	svobodné
vědecko-pedagogického	vědeckoedagogický	k2eAgNnSc2d1	vědecko-pedagogické
pracoviště	pracoviště	k1gNnSc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
nicméně	nicméně	k8xC	nicméně
působil	působit	k5eAaImAgMnS	působit
procesualista	procesualista	k1gMnSc1	procesualista
Josef	Josef	k1gMnSc1	Josef
Macur	Macur	k1gMnSc1	Macur
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
původního	původní	k2eAgInSc2d1	původní
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
akademického	akademický	k2eAgInSc2d1	akademický
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
obměnilo	obměnit	k5eAaPmAgNnS	obměnit
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
fakulty	fakulta	k1gFnSc2	fakulta
i	i	k8xC	i
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kateder	katedra	k1gFnPc2	katedra
<g/>
,	,	kIx,	,
na	na	k7c4	na
katedru	katedra	k1gFnSc4	katedra
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
politologie	politologie	k1gFnSc2	politologie
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
vrátil	vrátit	k5eAaPmAgMnS	vrátit
profesor	profesor	k1gMnSc1	profesor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Klokočka	Klokočka	k1gFnSc1	Klokočka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
změněna	změněn	k2eAgFnSc1d1	změněna
koncepce	koncepce	k1gFnSc1	koncepce
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
fakultě	fakulta	k1gFnSc6	fakulta
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vrácena	vrácen	k2eAgFnSc1d1	vrácena
její	její	k3xOp3gFnSc1	její
budova	budova	k1gFnSc1	budova
na	na	k7c4	na
Veveří	veveří	k2eAgMnPc4d1	veveří
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
nemovitá	movitý	k2eNgFnSc1d1	nemovitá
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
působil	působit	k5eAaImAgInS	působit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
politologický	politologický	k2eAgInSc1d1	politologický
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
vstupní	vstupní	k2eAgFnSc2d1	vstupní
haly	hala	k1gFnSc2	hala
instalována	instalován	k2eAgFnSc1d1	instalována
plastika	plastika	k1gFnSc1	plastika
Zákoník	zákoník	k1gInSc1	zákoník
od	od	k7c2	od
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Preclíka	Preclík	k1gMnSc2	Preclík
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
poslanci	poslanec	k1gMnPc1	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
jako	jako	k8xC	jako
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
typicky	typicky	k6eAd1	typicky
pravicově	pravicově	k6eAd1	pravicově
oportunistická	oportunistický	k2eAgFnSc1d1	oportunistická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgNnP	být
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
fakulty	fakulta	k1gFnSc2	fakulta
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
socha	socha	k1gFnSc1	socha
prezidenta	prezident	k1gMnSc2	prezident
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
replika	replika	k1gFnSc1	replika
originálu	originál	k1gInSc2	originál
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
především	především	k9	především
členové	člen	k1gMnPc1	člen
Československé	československý	k2eAgFnSc2d1	Československá
obce	obec	k1gFnSc2	obec
legionářské	legionářský	k2eAgFnSc2d1	legionářská
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc2	sdružení
československých	československý	k2eAgMnPc2d1	československý
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
letců	letec	k1gMnPc2	letec
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
MU	MU	kA	MU
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
typu	typ	k1gInSc2	typ
imatrikulace	imatrikulace	k1gFnSc1	imatrikulace
nebo	nebo	k8xC	nebo
promoce	promoce	k1gFnSc1	promoce
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
akademické	akademický	k2eAgFnPc4d1	akademická
insignie	insignie	k1gFnPc4	insignie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
žezlo	žezlo	k1gNnSc1	žezlo
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
hlavici	hlavice	k1gFnSc6	hlavice
je	být	k5eAaImIp3nS	být
alegorická	alegorický	k2eAgFnSc1d1	alegorická
postava	postava	k1gFnSc1	postava
Spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
a	a	k8xC	a
řetěz	řetěz	k1gInSc1	řetěz
děkana	děkan	k1gMnSc2	děkan
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
medaili	medaile	k1gFnSc4	medaile
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
aversu	avers	k1gInSc6	avers
poprsí	poprsí	k1gNnSc2	poprsí
Albína	Albín	k1gMnSc2	Albín
Bráfa	Bráf	k1gMnSc2	Bráf
a	a	k8xC	a
na	na	k7c6	na
reversu	revers	k1gInSc6	revers
liktorské	liktorský	k2eAgInPc4d1	liktorský
pruty	prut	k1gInPc4	prut
se	s	k7c7	s
sekyrou	sekyra	k1gFnSc7	sekyra
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Lex	Lex	k1gFnSc1	Lex
dura	dur	k2eAgFnSc1d1	dura
sed	sed	k1gInSc4	sed
lex	lex	k?	lex
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
označení	označení	k1gNnSc3	označení
diplomů	diplom	k1gInPc2	diplom
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
dokumentů	dokument	k1gInPc2	dokument
fakulta	fakulta	k1gFnSc1	fakulta
používá	používat	k5eAaImIp3nS	používat
historické	historický	k2eAgFnPc4d1	historická
pečeti	pečeť	k1gFnPc4	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Barvou	barva	k1gFnSc7	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
2593	[number]	k4	2593
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
barvě	barva	k1gFnSc6	barva
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
provedeno	proveden	k2eAgNnSc1d1	provedeno
logo	logo	k1gNnSc1	logo
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
postava	postava	k1gFnSc1	postava
Spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
paprsky	paprsek	k1gInPc7	paprsek
a	a	k8xC	a
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykian	k1gMnSc2	Masarykian
Brunensis	Brunensis	k1gFnSc2	Brunensis
<g/>
.	.	kIx.	.
</s>
<s>
Facultas	Facultas	k1gMnSc1	Facultas
iuridica	iuridica	k1gMnSc1	iuridica
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
získat	získat	k5eAaPmF	získat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
právníka	právník	k1gMnSc2	právník
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc4	studium
<g/>
:	:	kIx,	:
v	v	k7c6	v
tříletém	tříletý	k2eAgInSc6d1	tříletý
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
programu	program	k1gInSc6	program
Právní	právní	k2eAgFnSc1d1	právní
specializace	specializace	k1gFnSc1	specializace
(	(	kIx(	(
<g/>
titul	titul	k1gInSc1	titul
Bc.	Bc.	k1gFnSc2	Bc.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oborech	obor	k1gInPc6	obor
Právní	právní	k2eAgInPc1d1	právní
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
nemovitostem	nemovitost	k1gFnPc3	nemovitost
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
podnikání	podnikání	k1gNnSc1	podnikání
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
praxe	praxe	k1gFnSc1	praxe
přípravného	přípravný	k2eAgInSc2d1	přípravný
<g />
.	.	kIx.	.
</s>
<s>
řízení	řízení	k1gNnSc1	řízení
trestního	trestní	k2eAgInSc2d1	trestní
<g/>
,	,	kIx,	,
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgMnSc1d2	vyšší
justiční	justiční	k2eAgMnSc1d1	justiční
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
Mezinárodněprávní	mezinárodněprávní	k2eAgFnPc4d1	mezinárodněprávní
obchodní	obchodní	k2eAgNnPc4d1	obchodní
studia	studio	k1gNnPc4	studio
a	a	k8xC	a
Obchodněprávní	Obchodněprávní	k2eAgNnPc1d1	Obchodněprávní
studia	studio	k1gNnPc1	studio
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c6	v
tříletém	tříletý	k2eAgInSc6d1	tříletý
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
programu	program	k1gInSc6	program
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
oborech	obor	k1gInPc6	obor
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
Teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
praxe	praxe	k1gFnSc1	praxe
trestního	trestní	k2eAgInSc2d1	trestní
a	a	k8xC	a
správního	správní	k2eAgInSc2d1	správní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
v	v	k7c6	v
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
magisterském	magisterský	k2eAgInSc6d1	magisterský
programu	program	k1gInSc6	program
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
právní	právní	k2eAgFnSc4d1	právní
<g />
.	.	kIx.	.
</s>
<s>
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
oboru	obor	k1gInSc6	obor
Právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
titul	titul	k1gInSc1	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
;	;	kIx,	;
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vykonat	vykonat	k5eAaPmF	vykonat
nepovinnou	povinný	k2eNgFnSc4d1	nepovinná
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
dvouletém	dvouletý	k2eAgInSc6d1	dvouletý
navazujícím	navazující	k2eAgInSc6d1	navazující
magisterském	magisterský	k2eAgInSc6d1	magisterský
programu	program	k1gInSc6	program
a	a	k8xC	a
oboru	obor	k1gInSc6	obor
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
(	(	kIx(	(
<g/>
titul	titul	k1gInSc1	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ve	v	k7c6	v
čtyřletém	čtyřletý	k2eAgInSc6d1	čtyřletý
doktorském	doktorský	k2eAgInSc6d1	doktorský
programu	program	k1gInSc6	program
<g />
.	.	kIx.	.
</s>
<s>
Teoretické	teoretický	k2eAgFnPc1d1	teoretická
právní	právní	k2eAgFnPc1d1	právní
vědy	věda	k1gFnPc1	věda
(	(	kIx(	(
<g/>
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oborech	obor	k1gInPc6	obor
Dějiny	dějiny	k1gFnPc1	dějiny
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Finanční	finanční	k2eAgNnSc1d1	finanční
právo	právo	k1gNnSc1	právo
a	a	k8xC	a
finanční	finanční	k2eAgFnPc1d1	finanční
vědy	věda	k1gFnPc1	věda
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
právo	právo	k1gNnSc4	právo
soukromé	soukromý	k2eAgNnSc4d1	soukromé
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
a	a	k8xC	a
evropské	evropský	k2eAgNnSc4d1	Evropské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Obchodní	obchodní	k2eAgNnSc4d1	obchodní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Pracovní	pracovní	k2eAgNnSc4d1	pracovní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Správní	správní	k2eAgNnSc4d1	správní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
Trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
státověda	státověda	k1gFnSc1	státověda
a	a	k8xC	a
Právo	právo	k1gNnSc1	právo
informačních	informační	k2eAgFnPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Přijímací	přijímací	k2eAgNnSc1d1	přijímací
řízení	řízení	k1gNnSc1	řízení
probíhá	probíhat	k5eAaImIp3nS	probíhat
formou	forma	k1gFnSc7	forma
celouniverzitního	celouniverzitní	k2eAgInSc2d1	celouniverzitní
písemného	písemný	k2eAgInSc2d1	písemný
testu	test	k1gInSc2	test
studijních	studijní	k2eAgInPc2d1	studijní
předpokladů	předpoklad	k1gInPc2	předpoklad
(	(	kIx(	(
<g/>
TSP	TSP	kA	TSP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
šesti	šest	k4xCc2	šest
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgMnSc1	který
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
testuje	testovat	k5eAaImIp3nS	testovat
verbální	verbální	k2eAgNnSc4d1	verbální
<g/>
,	,	kIx,	,
numerické	numerický	k2eAgNnSc4d1	numerické
a	a	k8xC	a
kritické	kritický	k2eAgNnSc4d1	kritické
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
analytické	analytický	k2eAgNnSc4d1	analytické
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
úsudky	úsudek	k1gInPc4	úsudek
<g/>
,	,	kIx,	,
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
představivost	představivost	k1gFnSc4	představivost
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc4d1	kulturní
přehled	přehled	k1gInSc4	přehled
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
akademický	akademický	k2eAgInSc4d1	akademický
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
se	se	k3xPyFc4	se
do	do	k7c2	do
bakalářských	bakalářský	k2eAgInPc2d1	bakalářský
programů	program	k1gInPc2	program
Právní	právní	k2eAgFnSc1d1	právní
specializace	specializace	k1gFnSc1	specializace
a	a	k8xC	a
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
hlásilo	hlásit	k5eAaImAgNnS	hlásit
912	[number]	k4	912
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
847	[number]	k4	847
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
,	,	kIx,	,
přijato	přijmout	k5eAaPmNgNnS	přijmout
bylo	být	k5eAaImAgNnS	být
191	[number]	k4	191
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
98	[number]	k4	98
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
právní	právní	k2eAgFnSc1d1	právní
věda	věda	k1gFnSc1	věda
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
2798	[number]	k4	2798
přihlášek	přihláška	k1gFnPc2	přihláška
a	a	k8xC	a
přijato	přijmout	k5eAaPmNgNnS	přijmout
bylo	být	k5eAaImAgNnS	být
483	[number]	k4	483
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
,	,	kIx,	,
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
zde	zde	k6eAd1	zde
tedy	tedy	k8xC	tedy
činila	činit	k5eAaImAgFnS	činit
17	[number]	k4	17
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Magisterští	magisterský	k2eAgMnPc1d1	magisterský
studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
povinně	povinně	k6eAd1	povinně
volitelnými	volitelný	k2eAgInPc7d1	volitelný
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaměřené	zaměřený	k2eAgMnPc4d1	zaměřený
na	na	k7c4	na
simulovaná	simulovaný	k2eAgNnPc4d1	simulované
soudní	soudní	k2eAgNnPc4d1	soudní
jednání	jednání	k1gNnPc4	jednání
či	či	k8xC	či
rozhodčí	rozhodčí	k1gMnPc4	rozhodčí
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
konstrukci	konstrukce	k1gFnSc3	konstrukce
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
tvorbu	tvorba	k1gFnSc4	tvorba
práva	právo	k1gNnSc2	právo
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
které	který	k3yRgNnSc1	který
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
právních	právní	k2eAgFnPc2d1	právní
klinik	klinika	k1gFnPc2	klinika
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
seznámit	seznámit	k5eAaPmF	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
různých	různý	k2eAgFnPc2d1	různá
institucí	instituce	k1gFnPc2	instituce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
studia	studio	k1gNnSc2	studio
absolvovat	absolvovat	k5eAaPmF	absolvovat
odbornou	odborný	k2eAgFnSc4d1	odborná
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Erasmus	Erasmus	k1gMnSc1	Erasmus
<g/>
+	+	kIx~	+
vyjíždět	vyjíždět	k5eAaImF	vyjíždět
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
fakulty	fakulta	k1gFnPc4	fakulta
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
řady	řada	k1gFnPc4	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
soutěží	soutěž	k1gFnPc2	soutěž
Moot	Moot	k2eAgInSc4d1	Moot
court	court	k1gInSc4	court
a	a	k8xC	a
soutěžit	soutěžit	k5eAaImF	soutěžit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
pracemi	práce	k1gFnPc7	práce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Studentské	studentský	k2eAgFnSc2d1	studentská
vědecké	vědecký	k2eAgFnSc2d1	vědecká
a	a	k8xC	a
odborné	odborný	k2eAgFnSc2d1	odborná
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
SVOČ	SVOČ	kA	SVOČ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
IUS	IUS	kA	IUS
et	et	k?	et
SOCIETAS	SOCIETAS	kA	SOCIETAS
a	a	k8xC	a
Cena	cena	k1gFnSc1	cena
Františka	František	k1gMnSc2	František
Weyra	Weyr	k1gMnSc2	Weyr
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
operačního	operační	k2eAgInSc2d1	operační
programu	program	k1gInSc2	program
Vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pro	pro	k7c4	pro
konkurenceschopnost	konkurenceschopnost	k1gFnSc4	konkurenceschopnost
a	a	k8xC	a
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
workshopů	workshop	k1gInPc2	workshop
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
např.	např.	kA	např.
Dny	den	k1gInPc7	den
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
zaobírající	zaobírající	k2eAgMnPc4d1	zaobírající
se	se	k3xPyFc4	se
aktuálními	aktuální	k2eAgInPc7d1	aktuální
právními	právní	k2eAgInPc7d1	právní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
Cyberspace	Cyberspace	k1gFnSc1	Cyberspace
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
IT	IT	kA	IT
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
COFOLA	COFOLA	kA	COFOLA
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konference	konference	k1gFnSc2	konference
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
vědce	vědec	k1gMnPc4	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
absolvovat	absolvovat	k5eAaPmF	absolvovat
dvouleté	dvouletý	k2eAgInPc4d1	dvouletý
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
praktické	praktický	k2eAgInPc4d1	praktický
programy	program	k1gInPc4	program
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
obchodní	obchodní	k2eAgNnSc4d1	obchodní
právo	právo	k1gNnSc4	právo
nebo	nebo	k8xC	nebo
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
britské	britský	k2eAgFnSc2d1	britská
Nottingham	Nottingham	k1gInSc4	Nottingham
Trent	Trent	k1gMnSc1	Trent
University	universita	k1gFnSc2	universita
tituly	titul	k1gInPc4	titul
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
nebo	nebo	k8xC	nebo
MPA	MPA	kA	MPA
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
dále	daleko	k6eAd2	daleko
realizuje	realizovat	k5eAaBmIp3nS	realizovat
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
zpracování	zpracování	k1gNnSc1	zpracování
posudků	posudek	k1gInPc2	posudek
a	a	k8xC	a
expertiz	expertiza	k1gFnPc2	expertiza
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
osobní	osobní	k2eAgFnSc2d1	osobní
konzultace	konzultace	k1gFnSc2	konzultace
s	s	k7c7	s
akademickými	akademický	k2eAgMnPc7d1	akademický
pracovníky	pracovník	k1gMnPc7	pracovník
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
právní	právní	k2eAgNnSc4d1	právní
poradenství	poradenství	k1gNnSc4	poradenství
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
přístupna	přístupen	k2eAgFnSc1d1	přístupna
i	i	k8xC	i
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
s	s	k7c7	s
půjčovnou	půjčovna	k1gFnSc7	půjčovna
a	a	k8xC	a
čítárnou	čítárna	k1gFnSc7	čítárna
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
působí	působit	k5eAaImIp3nS	působit
Evropské	evropský	k2eAgNnSc1d1	Evropské
dokumentační	dokumentační	k2eAgNnSc1d1	dokumentační
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
informace	informace	k1gFnPc4	informace
jak	jak	k8xS	jak
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c6	o
Radě	rada	k1gFnSc6	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
řady	řada	k1gFnSc2	řada
odborných	odborný	k2eAgFnPc2d1	odborná
monografií	monografie	k1gFnPc2	monografie
fakulta	fakulta	k1gFnSc1	fakulta
vydává	vydávat	k5eAaImIp3nS	vydávat
Časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
praxi	praxe	k1gFnSc4	praxe
<g/>
,	,	kIx,	,
Masaryk	Masaryk	k1gMnSc1	Masaryk
University	universita	k1gFnSc2	universita
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Law	Law	k1gMnSc1	Law
and	and	k?	and
Technology	technolog	k1gMnPc4	technolog
a	a	k8xC	a
Revue	revue	k1gFnPc4	revue
pro	pro	k7c4	pro
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srovnání	srovnání	k1gNnSc2	srovnání
všech	všecek	k3xTgFnPc2	všecek
českých	český	k2eAgFnPc2d1	Česká
právnických	právnický	k2eAgFnPc2d1	právnická
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
výsledku	výsledek	k1gInSc2	výsledek
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ze	z	k7c2	z
srovnání	srovnání	k1gNnSc2	srovnání
Hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
novin	novina	k1gFnPc2	novina
vyšla	vyjít	k5eAaPmAgFnS	vyjít
opět	opět	k6eAd1	opět
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
Hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
novinami	novina	k1gFnPc7	novina
opět	opět	k6eAd1	opět
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
samosprávným	samosprávný	k2eAgInSc7d1	samosprávný
orgánem	orgán	k1gInSc7	orgán
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
dvacetičlenný	dvacetičlenný	k2eAgInSc1d1	dvacetičlenný
akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
celou	celý	k2eAgFnSc4d1	celá
akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
komora	komora	k1gFnSc1	komora
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
)	)	kIx)	)
a	a	k8xC	a
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
studentská	studentský	k2eAgFnSc1d1	studentská
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řádné	řádný	k2eAgNnSc1d1	řádné
zasedání	zasedání	k1gNnSc1	zasedání
senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
statutu	statut	k1gInSc2	statut
fakulty	fakulta	k1gFnSc2	fakulta
musí	muset	k5eAaImIp3nS	muset
konat	konat	k5eAaImF	konat
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
semestr	semestr	k1gInSc4	semestr
<g/>
,	,	kIx,	,
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
se	se	k3xPyFc4	se
řádná	řádný	k2eAgNnPc1d1	řádné
zasedání	zasedání	k1gNnPc1	zasedání
senátu	senát	k1gInSc2	senát
konala	konat	k5eAaImAgFnS	konat
vždy	vždy	k6eAd1	vždy
každé	každý	k3xTgNnSc4	každý
druhé	druhý	k4xOgNnSc4	druhý
pondělí	pondělí	k1gNnSc4	pondělí
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
působí	působit	k5eAaImIp3nS	působit
rovněž	rovněž	k9	rovněž
Akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
měla	mít	k5eAaImAgFnS	mít
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
tří	tři	k4xCgNnPc2	tři
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
dvou	dva	k4xCgMnPc2	dva
studentů	student	k1gMnPc2	student
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
ovšem	ovšem	k9	ovšem
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektor	rektor	k1gMnSc1	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
který	který	k3yIgInSc1	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
jménem	jméno	k1gNnSc7	jméno
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
kolegium	kolegium	k1gNnSc1	kolegium
děkana	děkan	k1gMnSc2	děkan
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
proděkanů	proděkan	k1gMnPc2	proděkan
<g/>
,	,	kIx,	,
tajemníka	tajemník	k1gMnSc2	tajemník
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
správu	správa	k1gFnSc4	správa
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
proděkani	proděkan	k1gMnPc1	proděkan
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
děkanem	děkan	k1gMnSc7	děkan
a	a	k8xC	a
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
svěřeném	svěřený	k2eAgInSc6d1	svěřený
úseku	úsek	k1gInSc6	úsek
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
přestupky	přestupek	k1gInPc4	přestupek
studentů	student	k1gMnPc2	student
projednává	projednávat	k5eAaImIp3nS	projednávat
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc1d1	vědecký
záměr	záměr	k1gInSc1	záměr
fakulty	fakulta	k1gFnSc2	fakulta
formuluje	formulovat	k5eAaImIp3nS	formulovat
její	její	k3xOp3gFnSc1	její
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
členy	člen	k1gMnPc4	člen
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
děkan	děkan	k1gMnSc1	děkan
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
významných	významný	k2eAgMnPc2d1	významný
představitelů	představitel	k1gMnPc2	představitel
právních	právní	k2eAgInPc2d1	právní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejméně	málo	k6eAd3	málo
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
externích	externí	k2eAgNnPc2d1	externí
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
na	na	k7c4	na
katedry	katedra	k1gFnPc4	katedra
a	a	k8xC	a
ústavy	ústav	k1gInPc4	ústav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
výuku	výuka	k1gFnSc4	výuka
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
účelová	účelový	k2eAgNnPc4d1	účelové
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
děkanát	děkanát	k1gInSc4	děkanát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
pracoviště	pracoviště	k1gNnPc1	pracoviště
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nadále	nadále	k6eAd1	nadále
dělit	dělit	k5eAaImF	dělit
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
útvarů	útvar	k1gInPc2	útvar
zřizovaných	zřizovaný	k2eAgInPc2d1	zřizovaný
vedoucím	vedoucí	k1gMnSc7	vedoucí
pracoviště	pracoviště	k1gNnSc2	pracoviště
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
souhlasu	souhlas	k1gInSc6	souhlas
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
oddělení	oddělení	k1gNnSc1	oddělení
celouniverzitního	celouniverzitní	k2eAgNnSc2d1	celouniverzitní
Centra	centrum	k1gNnSc2	centrum
jazykového	jazykový	k2eAgNnSc2d1	jazykové
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Katedry	katedra	k1gFnPc1	katedra
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
organizační	organizační	k2eAgFnSc7d1	organizační
složkou	složka	k1gFnSc7	složka
fakulty	fakulta	k1gFnSc2	fakulta
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
katedře	katedra	k1gFnSc6	katedra
působí	působit	k5eAaImIp3nP	působit
alespoň	alespoň	k9	alespoň
tři	tři	k4xCgMnPc1	tři
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Katedry	katedra	k1gFnPc1	katedra
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
MU	MU	kA	MU
jsou	být	k5eAaImIp3nP	být
katedra	katedra	k1gFnSc1	katedra
dějin	dějiny	k1gFnPc2	dějiny
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
právní	právní	k2eAgFnSc2d1	právní
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
obchodního	obchodní	k2eAgNnSc2d1	obchodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
pracovního	pracovní	k2eAgNnSc2d1	pracovní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
politologie	politologie	k1gFnSc1	politologie
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
správní	správní	k2eAgFnSc2d1	správní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
správního	správní	k2eAgNnSc2d1	správní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
a	a	k8xC	a
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
finančního	finanční	k2eAgNnSc2d1	finanční
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
katedra	katedra	k1gFnSc1	katedra
práva	právo	k1gNnSc2	právo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
pozemkového	pozemkový	k2eAgNnSc2d1	pozemkové
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ústavy	ústava	k1gFnPc1	ústava
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nP	působit
dva	dva	k4xCgInPc1	dva
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
Ústav	ústav	k1gInSc1	ústav
dovednostní	dovednostní	k2eAgFnSc2d1	dovednostní
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
inovace	inovace	k1gFnSc2	inovace
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Katedry	katedra	k1gFnPc1	katedra
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
výuku	výuka	k1gFnSc4	výuka
předmětů	předmět	k1gInPc2	předmět
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
akademickými	akademický	k2eAgFnPc7d1	akademická
i	i	k8xC	i
neakademickými	akademický	k2eNgMnPc7d1	akademický
pracovníky	pracovník	k1gMnPc7	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
akademické	akademický	k2eAgMnPc4d1	akademický
pracovníky	pracovník	k1gMnPc4	pracovník
patří	patřit	k5eAaImIp3nP	patřit
učitelé	učitel	k1gMnPc1	učitel
(	(	kIx(	(
<g/>
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
docenti	docent	k1gMnPc1	docent
<g/>
,	,	kIx,	,
odborní	odborný	k2eAgMnPc1d1	odborný
asistenti	asistent	k1gMnPc1	asistent
<g/>
,	,	kIx,	,
asistenti	asistent	k1gMnPc1	asistent
<g/>
,	,	kIx,	,
lektoři	lektor	k1gMnPc1	lektor
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
vědečtí	vědecký	k2eAgMnPc1d1	vědecký
a	a	k8xC	a
výzkumní	výzkumní	k2eAgMnPc1d1	výzkumní
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
interní	interní	k2eAgMnPc1d1	interní
studenti	student	k1gMnPc1	student
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
lektory	lektor	k1gMnPc4	lektor
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
katedra	katedra	k1gFnSc1	katedra
má	mít	k5eAaImIp3nS	mít
děkanem	děkan	k1gMnSc7	děkan
jmenovaného	jmenovaný	k1gMnSc2	jmenovaný
vedoucího	vedoucí	k1gMnSc2	vedoucí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vybírán	vybírán	k2eAgMnSc1d1	vybírán
zejména	zejména	k9	zejména
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
či	či	k8xC	či
docentů	docent	k1gMnPc2	docent
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
projednání	projednání	k1gNnSc6	projednání
katedrou	katedra	k1gFnSc7	katedra
a	a	k8xC	a
akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
účelová	účelový	k2eAgNnPc4d1	účelové
zařízení	zařízení	k1gNnPc4	zařízení
patří	patřit	k5eAaImIp3nS	patřit
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Markéta	Markéta	k1gFnSc1	Markéta
Selucká	Selucký	k2eAgFnSc1d1	Selucká
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
děkanka	děkanka	k1gFnSc1	děkanka
JUDr.	JUDr.	kA	JUDr.
Jana	Jana	k1gFnSc1	Jana
Jurníková	Jurníková	k1gFnSc1	Jurníková
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
magisterský	magisterský	k2eAgInSc4d1	magisterský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Mgr.	Mgr.	kA	Mgr.
Martin	Martin	k1gMnSc1	Martin
<g />
.	.	kIx.	.
</s>
<s>
Škop	Škop	k1gMnSc1	Škop
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
a	a	k8xC	a
navazující	navazující	k2eAgNnSc4d1	navazující
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Věra	Věra	k1gFnSc1	Věra
Kalvodová	Kalvodová	k1gFnSc1	Kalvodová
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
rigorózní	rigorózní	k2eAgNnSc4d1	rigorózní
řízení	řízení	k1gNnSc4	řízení
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Svatoň	Svatoň	k1gMnSc1	Svatoň
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
strategii	strategie	k1gFnSc4	strategie
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
fakulty	fakulta	k1gFnSc2	fakulta
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Kotásek	Kotásek	k1gMnSc1	Kotásek
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Ing.	ing.	kA	ing.
Michal	Michal	k1gMnSc1	Michal
Radvan	Radvan	k1gMnSc1	Radvan
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
a	a	k8xC	a
vnější	vnější	k2eAgInPc4d1	vnější
vztahy	vztah	k1gInPc4	vztah
Ing.	ing.	kA	ing.
Blanka	Blanka	k1gFnSc1	Blanka
Přikrylová	Přikrylová	k1gFnSc1	Přikrylová
–	–	k?	–
tajemnice	tajemnice	k1gFnSc2	tajemnice
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Ing.	ing.	kA	ing.
Josef	Josef	k1gMnSc1	Josef
Šilhán	šilhán	k2eAgMnSc1d1	šilhán
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
</s>
