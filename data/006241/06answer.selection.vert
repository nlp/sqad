<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
<g/>
,	,	kIx,	,
profesorský	profesorský	k2eAgInSc4d1	profesorský
sbor	sbor	k1gInSc4	sbor
dále	daleko	k6eAd2	daleko
tvořili	tvořit	k5eAaImAgMnP	tvořit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Baxa	Baxa	k1gMnSc1	Baxa
pro	pro	k7c4	pro
právní	právní	k2eAgFnPc4d1	právní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vacek	Vacek	k1gMnSc1	Vacek
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgFnSc4d1	srovnávací
pravovědu	pravověda	k1gFnSc4	pravověda
a	a	k8xC	a
církevní	církevní	k2eAgNnSc4d1	církevní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
nauky	nauka	k1gFnPc4	nauka
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Sedláček	Sedláček	k1gMnSc1	Sedláček
pro	pro	k7c4	pro
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kallab	Kallab	k1gMnSc1	Kallab
pro	pro	k7c4	pro
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dominik	Dominik	k1gMnSc1	Dominik
pro	pro	k7c4	pro
správní	správní	k2eAgNnSc4d1	správní
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
i	i	k9	i
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
