<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
a	a	k8xC	a
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Pavlem	Pavel	k1gMnSc7	Pavel
<g/>
)	)	kIx)	)
překladem	překlad	k1gInSc7	překlad
sedmidílné	sedmidílný	k2eAgFnSc2d1	sedmidílná
ságy	sága	k1gFnSc2	sága
Joanne	Joann	k1gInSc5	Joann
Rowlingové	Rowlingový	k2eAgMnPc4d1	Rowlingový
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
studium	studium	k1gNnSc4	studium
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
pobýval	pobývat	k5eAaImAgMnS	pobývat
pracovně	pracovně	k6eAd1	pracovně
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
publikuje	publikovat	k5eAaBmIp3nS	publikovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
známým	známý	k1gMnSc7	známý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
především	především	k6eAd1	především
jako	jako	k8xS	jako
překladatel	překladatel	k1gMnSc1	překladatel
ze	z	k7c2	z
španělštiny	španělština	k1gFnSc2	španělština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
např.	např.	kA	např.
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
a	a	k8xC	a
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Miguela	Miguel	k1gMnSc2	Miguel
Littína	Littín	k1gMnSc2	Littín
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
Gabriela	Gabriela	k1gFnSc1	Gabriela
Garcíi	Garcí	k1gFnSc2	Garcí
Márqueze	Márqueze	k1gFnSc2	Márqueze
<g/>
,	,	kIx,	,
také	také	k9	také
díla	dílo	k1gNnSc2	dílo
Maria	Mario	k1gMnSc2	Mario
Vargasse	Vargass	k1gMnSc2	Vargass
Llosy	Llosa	k1gFnSc2	Llosa
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
Sheckleyův	Sheckleyův	k2eAgInSc4d1	Sheckleyův
sci-fi	scii	k1gNnPc6	sci-fi
román	román	k1gInSc4	román
Desátá	desátá	k1gFnSc1	desátá
oběť	oběť	k1gFnSc1	oběť
<g/>
.	.	kIx.	.
<g/>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Pavlem	Pavel	k1gMnSc7	Pavel
přeložili	přeložit	k5eAaPmAgMnP	přeložit
sedmidílnou	sedmidílný	k2eAgFnSc4d1	sedmidílná
ságu	sága	k1gFnSc4	sága
Joanne	Joann	k1gInSc5	Joann
Rowlingové	Rowlingový	k2eAgMnPc4d1	Rowlingový
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
oslovilo	oslovit	k5eAaPmAgNnS	oslovit
Vladímíra	Vladímír	k1gMnSc4	Vladímír
Medka	Medek	k1gMnSc4	Medek
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
překladu	překlad	k1gInSc2	překlad
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
ságy	sága	k1gFnSc2	sága
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
i	i	k9	i
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
překladu	překlad	k1gInSc6	překlad
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
ságy	sága	k1gFnSc2	sága
pak	pak	k6eAd1	pak
přizval	přizvat	k5eAaPmAgMnS	přizvat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
dohnat	dohnat	k5eAaPmF	dohnat
skluz	skluz	k1gInSc4	skluz
ve	v	k7c6	v
vydávání	vydávání	k1gNnSc6	vydávání
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
světovým	světový	k2eAgMnPc3d1	světový
nakladatelům	nakladatel	k1gMnPc3	nakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
díl	díl	k1gInSc1	díl
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
přeložil	přeložit	k5eAaPmAgMnS	přeložit
opět	opět	k6eAd1	opět
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Medek	Medek	k1gMnSc1	Medek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ujal	ujmout	k5eAaPmAgMnS	ujmout
překladu	překlad	k1gInSc2	překlad
zbylých	zbylý	k2eAgInPc2d1	zbylý
dílů	díl	k1gInPc2	díl
ságy	sága	k1gFnSc2	sága
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
bratří	bratr	k1gMnPc2	bratr
Medků	Medek	k1gMnPc2	Medek
je	být	k5eAaImIp3nS	být
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
<g/>
;	;	kIx,	;
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
nejen	nejen	k6eAd1	nejen
zohlednili	zohlednit	k5eAaPmAgMnP	zohlednit
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
věnovali	věnovat	k5eAaImAgMnP	věnovat
velkou	velký	k2eAgFnSc4d1	velká
péči	péče	k1gFnSc4	péče
například	například	k6eAd1	například
i	i	k8xC	i
překladu	překlad	k1gInSc6	překlad
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
neologismů	neologismus	k1gInPc2	neologismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokázali	dokázat	k5eAaPmAgMnP	dokázat
také	také	k9	také
postihnout	postihnout	k5eAaPmF	postihnout
proměnu	proměna	k1gFnSc4	proměna
vypravěčského	vypravěčský	k2eAgInSc2d1	vypravěčský
stylu	styl	k1gInSc2	styl
od	od	k7c2	od
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
až	až	k9	až
po	po	k7c4	po
thriller	thriller	k1gInSc4	thriller
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
řešící	řešící	k2eAgFnPc1d1	řešící
závažné	závažný	k2eAgFnPc4d1	závažná
existenciální	existenciální	k2eAgFnPc4d1	existenciální
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
</s>
</p>
<p>
<s>
Překladatel	překladatel	k1gMnSc1	překladatel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medek	Medek	k1gMnSc1	Medek
<g/>
:	:	kIx,	:
Nejraději	rád	k6eAd3	rád
mám	mít	k5eAaImIp1nS	mít
Ufňukanou	ufňukaný	k2eAgFnSc4d1	ufňukaná
Uršulu	Uršula	k1gFnSc4	Uršula
</s>
</p>
