<s>
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
výstavní	výstavní	k2eAgInSc4d1	výstavní
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
km	km	kA	km
zjz	zjz	k?	zjz
<g/>
.	.	kIx.	.
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
patří	patřit	k5eAaImIp3nS	patřit
společnosti	společnost	k1gFnSc2	společnost
Veletrhy	veletrh	k1gInPc1	veletrh
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zde	zde	k6eAd1	zde
celoročně	celoročně	k6eAd1	celoročně
provozuje	provozovat	k5eAaImIp3nS	provozovat
veletrhy	veletrh	k1gInPc4	veletrh
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc4	výstava
<g/>
,	,	kIx,	,
přehlídky	přehlídka	k1gFnPc4	přehlídka
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
další	další	k2eAgFnPc4d1	další
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnPc1	příprava
výstavby	výstavba	k1gFnSc2	výstavba
nového	nový	k2eAgNnSc2d1	nové
veletržního	veletržní	k2eAgNnSc2d1	veletržní
výstaviště	výstaviště	k1gNnSc2	výstaviště
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
ještě	ještě	k9	ještě
do	do	k7c2	do
časů	čas	k1gInPc2	čas
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
přerušeny	přerušit	k5eAaPmNgInP	přerušit
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
nastala	nastat	k5eAaPmAgFnS	nastat
krátká	krátká	k1gFnSc1	krátká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
recese	recese	k1gFnSc2	recese
způsobená	způsobený	k2eAgFnSc1d1	způsobená
ztrátou	ztráta	k1gFnSc7	ztráta
trhů	trh	k1gInPc2	trh
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
rozhodnul	rozhodnout	k5eAaPmAgInS	rozhodnout
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
výbor	výbor	k1gInSc1	výbor
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
nového	nový	k2eAgNnSc2d1	nové
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zakoupeny	zakoupen	k2eAgInPc1d1	zakoupen
pozemky	pozemek	k1gInPc1	pozemek
v	v	k7c6	v
Pisárecké	pisárecký	k2eAgFnSc6d1	Pisárecká
kotlině	kotlina	k1gFnSc6	kotlina
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
58	[number]	k4	58
ha	ha	kA	ha
od	od	k7c2	od
brněnského	brněnský	k2eAgMnSc2d1	brněnský
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
podnikatele	podnikatel	k1gMnSc2	podnikatel
Viktora	Viktor	k1gMnSc2	Viktor
Bauera	Bauer	k1gMnSc2	Bauer
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bauerova	Bauerův	k2eAgFnSc1d1	Bauerova
rampa	rampa	k1gFnSc1	rampa
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Bauer	Bauer	k1gMnSc1	Bauer
zde	zde	k6eAd1	zde
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
cukrovar	cukrovar	k1gInSc4	cukrovar
a	a	k8xC	a
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
bydlel	bydlet	k5eAaImAgMnS	bydlet
nedaleko	daleko	k6eNd1	daleko
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
ulici	ulice	k1gFnSc6	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
čp.	čp.	k?	čp.
39	[number]	k4	39
<g/>
/	/	kIx~	/
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
urbanistické	urbanistický	k2eAgNnSc4d1	Urbanistické
řešení	řešení	k1gNnSc4	řešení
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
Architektonická	architektonický	k2eAgFnSc1d1	architektonická
koncepce	koncepce	k1gFnSc1	koncepce
areálu	areál	k1gInSc2	areál
a	a	k8xC	a
hlavního	hlavní	k2eAgInSc2d1	hlavní
pavilonu	pavilon	k1gInSc2	pavilon
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vítězného	vítězný	k2eAgInSc2d1	vítězný
soutěžního	soutěžní	k2eAgInSc2d1	soutěžní
návrhu	návrh	k1gInSc2	návrh
pražského	pražský	k2eAgMnSc2d1	pražský
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Kalouse	Kalous	k1gMnSc2	Kalous
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
realizaci	realizace	k1gFnSc6	realizace
výstaviště	výstaviště	k1gNnSc2	výstaviště
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
nový	nový	k2eAgInSc1d1	nový
urbanistický	urbanistický	k2eAgInSc1d1	urbanistický
plán	plán	k1gInSc1	plán
brněnského	brněnský	k2eAgMnSc2d1	brněnský
architekta	architekt	k1gMnSc2	architekt
Emila	Emil	k1gMnSc2	Emil
Králíka	Králík	k1gMnSc2	Králík
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
areálu	areál	k1gInSc2	areál
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
výstaviště	výstaviště	k1gNnSc2	výstaviště
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1928	[number]	k4	1928
akcí	akce	k1gFnPc2	akce
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Výstava	výstava	k1gFnSc1	výstava
soudobé	soudobý	k2eAgFnSc2d1	soudobá
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1929	[number]	k4	1929
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
také	také	k9	také
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
ve	v	k7c6	v
výstavišti	výstaviště	k1gNnSc6	výstaviště
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
360	[number]	k4	360
000	[number]	k4	000
m2	m2	k4	m2
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
v	v	k7c6	v
pavilonech	pavilon	k1gInPc6	pavilon
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
m2	m2	k4	m2
<g/>
)	)	kIx)	)
získalo	získat	k5eAaPmAgNnS	získat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
funkcionalistických	funkcionalistický	k2eAgInPc2d1	funkcionalistický
stavebních	stavební	k2eAgInPc2d1	stavební
komplexů	komplex	k1gInPc2	komplex
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
pavilony	pavilon	k1gInPc1	pavilon
byly	být	k5eAaImAgInP	být
dílem	dílo	k1gNnSc7	dílo
známých	známý	k2eAgMnPc2d1	známý
architektů	architekt	k1gMnPc2	architekt
-	-	kIx~	-
například	například	k6eAd1	například
čelný	čelný	k2eAgMnSc1d1	čelný
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
pavilonů	pavilon	k1gInPc2	pavilon
<g/>
,	,	kIx,	,
zachoval	zachovat	k5eAaPmAgInS	zachovat
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
pavilon	pavilon	k1gInSc1	pavilon
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
budova	budova	k1gFnSc1	budova
výstavní	výstavní	k2eAgFnSc2d1	výstavní
pošty	pošta	k1gFnSc2	pošta
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
pavilon	pavilon	k1gInSc1	pavilon
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
a	a	k8xC	a
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
děl	dělo	k1gNnPc2	dělo
české	český	k2eAgFnSc2d1	Česká
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Exponáty	exponát	k1gInPc1	exponát
výstavy	výstava	k1gFnSc2	výstava
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
několik	několik	k4yIc1	několik
moderních	moderní	k2eAgInPc2d1	moderní
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
výstavištěm	výstaviště	k1gNnSc7	výstaviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
výstavní	výstavní	k2eAgInSc4d1	výstavní
areál	areál	k1gInSc4	areál
využívala	využívat	k5eAaPmAgFnS	využívat
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
obnova	obnova	k1gFnSc1	obnova
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
brigádnicky	brigádnicky	k6eAd1	brigádnicky
podílelo	podílet	k5eAaImAgNnS	podílet
mnoho	mnoho	k4c1	mnoho
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Brňanů	Brňan	k1gMnPc2	Brňan
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
dnešních	dnešní	k2eAgInPc2d1	dnešní
strojírenských	strojírenský	k2eAgInPc2d1	strojírenský
veletrhů	veletrh	k1gInPc2	veletrh
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
výstavy	výstava	k1gFnPc1	výstava
československého	československý	k2eAgNnSc2d1	Československé
strojírenství	strojírenství	k1gNnSc2	strojírenství
pořádané	pořádaný	k2eAgNnSc4d1	pořádané
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
veletrh	veletrh	k1gInSc1	veletrh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
strojírenské	strojírenský	k2eAgInPc1d1	strojírenský
veletrhy	veletrh	k1gInPc1	veletrh
konají	konat	k5eAaImIp3nP	konat
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
termín	termín	k1gInSc4	termín
konání	konání	k1gNnSc2	konání
koordinován	koordinovat	k5eAaBmNgInS	koordinovat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
veletrhy	veletrh	k1gInPc7	veletrh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
posunut	posunout	k5eAaPmNgInS	posunout
i	i	k9	i
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
koná	konat	k5eAaImIp3nS	konat
průměrně	průměrně	k6eAd1	průměrně
40	[number]	k4	40
výstavních	výstavní	k2eAgFnPc2d1	výstavní
akcí	akce	k1gFnPc2	akce
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejrozsáhlejším	rozsáhlý	k2eAgFnPc3d3	nejrozsáhlejší
patří	patřit	k5eAaImIp3nP	patřit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
veletrh	veletrh	k1gInSc1	veletrh
nebo	nebo	k8xC	nebo
výstava	výstava	k1gFnSc1	výstava
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
Invex	Invex	k1gInSc1	Invex
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
výstaviště	výstaviště	k1gNnSc2	výstaviště
provozuje	provozovat	k5eAaImIp3nS	provozovat
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Veletrhy	veletrh	k1gInPc1	veletrh
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
většinu	většina	k1gFnSc4	většina
drží	držet	k5eAaImIp3nS	držet
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
drobní	drobný	k2eAgMnPc1d1	drobný
akcionáři	akcionář	k1gMnPc1	akcionář
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
předchůdkyní	předchůdkyně	k1gFnSc7	předchůdkyně
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
společnost	společnost	k1gFnSc1	společnost
BVV	BVV	kA	BVV
-	-	kIx~	-
Brněnské	brněnský	k2eAgInPc1d1	brněnský
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
veletrhy	veletrh	k1gInPc7	veletrh
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
i	i	k9	i
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
pavilonů	pavilon	k1gInPc2	pavilon
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
nejvýraznější	výrazný	k2eAgInPc4d3	nejvýraznější
je	být	k5eAaImIp3nS	být
pavilon	pavilon	k1gInSc4	pavilon
Z	Z	kA	Z
se	s	k7c7	s
samonosnou	samonosný	k2eAgFnSc7d1	samonosná
ocelovou	ocelový	k2eAgFnSc7d1	ocelová
kupolí	kupole	k1gFnSc7	kupole
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
122	[number]	k4	122
m.	m.	k?	m.
Jednopatrová	jednopatrový	k2eAgFnSc1d1	jednopatrová
kruhová	kruhový	k2eAgFnSc1d1	kruhová
budova	budova	k1gFnSc1	budova
u	u	k7c2	u
hlavního	hlavní	k2eAgInSc2d1	hlavní
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
výšková	výškový	k2eAgFnSc1d1	výšková
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
vstup	vstup	k1gInSc4	vstup
bylo	být	k5eAaImAgNnS	být
instalováno	instalovat	k5eAaBmNgNnS	instalovat
sousoší	sousoší	k1gNnSc1	sousoší
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
Nový	nový	k2eAgInSc1d1	nový
věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
dovezeno	dovézt	k5eAaPmNgNnS	dovézt
sem	sem	k6eAd1	sem
z	z	k7c2	z
bruselského	bruselský	k2eAgNnSc2d1	bruselské
výstaviště	výstaviště	k1gNnSc2	výstaviště
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získalo	získat	k5eAaPmAgNnS	získat
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
výstaviště	výstaviště	k1gNnSc2	výstaviště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
výstavby	výstavba	k1gFnSc2	výstavba
areálu	areál	k1gInSc2	areál
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
45	[number]	k4	45
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
věže	věž	k1gFnSc2	věž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
architekt	architekt	k1gMnSc1	architekt
Bohumír	Bohumír	k1gMnSc1	Bohumír
Čermák	Čermák	k1gMnSc1	Čermák
jakožto	jakožto	k8xS	jakožto
součást	součást	k1gFnSc1	součást
pavilonu	pavilon	k1gInSc2	pavilon
G.	G.	kA	G.
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
výtah	výtah	k1gInSc1	výtah
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
<g/>
,	,	kIx,	,
vysokorychlostním	vysokorychlostní	k2eAgInSc7d1	vysokorychlostní
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
závažná	závažný	k2eAgNnPc4d1	závažné
poškození	poškození	k1gNnPc4	poškození
nosné	nosný	k2eAgFnSc2d1	nosná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
deformovat	deformovat	k5eAaImF	deformovat
celou	celý	k2eAgFnSc4d1	celá
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
oddělen	oddělit	k5eAaPmNgInS	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
stav	stav	k1gInSc1	stav
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
navrátil	navrátit	k5eAaPmAgInS	navrátit
až	až	k9	až
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
věž	věž	k1gFnSc1	věž
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInSc1d3	nejnovější
a	a	k8xC	a
také	také	k9	také
největší	veliký	k2eAgInSc4d3	veliký
pavilon	pavilon	k1gInSc4	pavilon
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
je	být	k5eAaImIp3nS	být
pavilon	pavilon	k1gInSc1	pavilon
P	P	kA	P
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dokoupil	Dokoupil	k1gMnSc1	Dokoupil
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
obří	obří	k2eAgFnSc2d1	obří
haly	hala	k1gFnSc2	hala
stála	stát	k5eAaImAgFnS	stát
necelou	celý	k2eNgFnSc4d1	necelá
miliardu	miliarda	k4xCgFnSc4	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
hrubou	hrubý	k2eAgFnSc7d1	hrubá
výstavní	výstavní	k2eAgFnSc7d1	výstavní
plochou	plocha	k1gFnSc7	plocha
přes	přes	k7c4	přes
15	[number]	k4	15
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
výstavní	výstavní	k2eAgFnSc7d1	výstavní
halou	hala	k1gFnSc7	hala
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
po	po	k7c6	po
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
největší	veliký	k2eAgFnSc7d3	veliký
halou	hala	k1gFnSc7	hala
vůbec	vůbec	k9	vůbec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
délku	délka	k1gFnSc4	délka
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
214	[number]	k4	214
m	m	kA	m
a	a	k8xC	a
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
90	[number]	k4	90
m.	m.	k?	m.
Zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
pavilonu	pavilon	k1gInSc2	pavilon
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
738	[number]	k4	738
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
obestavěný	obestavěný	k2eAgInSc1d1	obestavěný
prostor	prostor	k1gInSc1	prostor
332	[number]	k4	332
026	[number]	k4	026
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
výstavní	výstavní	k2eAgFnSc1d1	výstavní
plocha	plocha	k1gFnSc1	plocha
15	[number]	k4	15
285	[number]	k4	285
m	m	kA	m
<g/>
2	[number]	k4	2
a	a	k8xC	a
čistá	čistý	k2eAgFnSc1d1	čistá
výstavná	výstavný	k2eAgFnSc1d1	výstavná
plocha	plocha	k1gFnSc1	plocha
10	[number]	k4	10
015	[number]	k4	015
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Světlá	světlý	k2eAgFnSc1d1	světlá
výška	výška	k1gFnSc1	výška
stropní	stropní	k2eAgFnSc2d1	stropní
konstrukce	konstrukce	k1gFnSc2	konstrukce
haly	hala	k1gFnSc2	hala
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
m.	m.	k?	m.
</s>
