<p>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
ს	ს	k?	ს
<g/>
,	,	kIx,	,
transliterace	transliterace	k1gFnSc1	transliterace
<g/>
:	:	kIx,	:
Sakartvelo	Sakartvela	k1gFnSc5	Sakartvela
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
při	při	k7c6	při
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severovýchodě	severovýchod	k1gInSc6	severovýchod
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
Arménie	Arménie	k1gFnSc2	Arménie
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Gruzie	Gruzie	k1gFnSc2	Gruzie
jsou	být	k5eAaImIp3nP	být
autonomní	autonomní	k2eAgFnPc1d1	autonomní
republiky	republika	k1gFnPc1	republika
Adžárie	Adžárie	k1gFnSc2	Adžárie
a	a	k8xC	a
Abcházie	Abcházie	k1gFnSc2	Abcházie
<g/>
.	.	kIx.	.
</s>
<s>
Abchazské	abchazský	k2eAgNnSc4d1	abchazské
území	území	k1gNnSc4	území
a	a	k8xC	a
také	také	k9	také
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
jsou	být	k5eAaImIp3nP	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
ovládány	ovládán	k2eAgInPc4d1	ovládán
a	a	k8xC	a
vojensky	vojensky	k6eAd1	vojensky
obsazeny	obsadit	k5eAaPmNgInP	obsadit
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
vláda	vláda	k1gFnSc1	vláda
nemá	mít	k5eNaImIp3nS	mít
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Gruzínsky	gruzínsky	k6eAd1	gruzínsky
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
zní	znět	k5eAaImIp3nS	znět
Sakartvelo	Sakartvela	k1gFnSc5	Sakartvela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
Gruzie	Gruzie	k1gFnSc1	Gruzie
řekne	říct	k5eAaPmIp3nS	říct
Georgia	Georgia	k1gFnSc1	Georgia
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
podle	podle	k7c2	podle
patrona	patron	k1gMnSc2	patron
země	zem	k1gFnSc2	zem
svatého	svatý	k1gMnSc2	svatý
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Georgios	Georgios	k1gInSc1	Georgios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
(	(	kIx(	(
<g/>
Г	Г	k?	Г
–	–	k?	–
Gruzija	Gruzij	k1gInSc2	Gruzij
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
požádala	požádat	k5eAaPmAgFnS	požádat
pobaltské	pobaltský	k2eAgFnPc4d1	pobaltská
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ruský	ruský	k2eAgInSc1d1	ruský
název	název	k1gInSc1	název
nepoužívaly	používat	k5eNaImAgFnP	používat
a	a	k8xC	a
přešly	přejít	k5eAaPmAgFnP	přejít
na	na	k7c4	na
doporučené	doporučený	k2eAgNnSc4d1	Doporučené
jméno	jméno	k1gNnSc4	jméno
Georgia	Georgium	k1gNnSc2	Georgium
pro	pro	k7c4	pro
estonštinu	estonština	k1gFnSc4	estonština
a	a	k8xC	a
Georgija	Georgijum	k1gNnPc4	Georgijum
pro	pro	k7c4	pro
litevštinu	litevština	k1gFnSc4	litevština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kavkazská	kavkazský	k2eAgFnSc1d1	kavkazská
Ibérie	Ibérie	k1gFnSc1	Ibérie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Gruzie	Gruzie	k1gFnSc1	Gruzie
obydlená	obydlený	k2eAgFnSc1d1	obydlená
různými	různý	k2eAgInPc7d1	různý
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgInP	být
Kartvelské	Kartvelský	k2eAgInPc4d1	Kartvelský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
Kolchové	Kolch	k1gMnPc1	Kolch
a	a	k8xC	a
Iberové	Iber	k1gMnPc1	Iber
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Urartské	Urartský	k2eAgFnSc2d1	Urartský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
starověký	starověký	k2eAgInSc4d1	starověký
gruzínský	gruzínský	k2eAgInSc4d1	gruzínský
stát	stát	k1gInSc4	stát
Kartli	Kartle	k1gFnSc3	Kartle
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
existoval	existovat	k5eAaImAgInS	existovat
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
je	být	k5eAaImIp3nS	být
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
Kavkazská	kavkazský	k2eAgFnSc1d1	kavkazská
Ibérie	Ibérie	k1gFnSc1	Ibérie
<g/>
.	.	kIx.	.
</s>
<s>
Kavkazská	kavkazský	k2eAgFnSc1d1	kavkazská
Ibérie	Ibérie	k1gFnSc1	Ibérie
společně	společně	k6eAd1	společně
s	s	k7c7	s
přímořským	přímořský	k2eAgInSc7d1	přímořský
státem	stát	k1gInSc7	stát
Kolchida	Kolchida	k1gFnSc1	Kolchida
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
gruzínského	gruzínský	k2eAgInSc2d1	gruzínský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Římané	Říman	k1gMnPc1	Říman
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejvýchodnějších	východní	k2eAgFnPc2d3	nejvýchodnější
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
se	se	k3xPyFc4	se
Gruzie	Gruzie	k1gFnSc1	Gruzie
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Byzance	Byzanc	k1gFnPc1	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
340	[number]	k4	340
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
oslabování	oslabování	k1gNnPc2	oslabování
Byzantského	byzantský	k2eAgNnSc2d1	byzantské
impéria	impérium	k1gNnSc2	impérium
se	se	k3xPyFc4	se
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
nejdříve	dříve	k6eAd3	dříve
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
přišli	přijít	k5eAaPmAgMnP	přijít
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
iberského	iberský	k2eAgNnSc2d1	Iberské
knížectví	knížectví	k1gNnSc2	knížectví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
645	[number]	k4	645
a	a	k8xC	a
donutili	donutit	k5eAaPmAgMnP	donutit
jeho	on	k3xPp3gMnSc4	on
eristaviho	eristavi	k1gMnSc4	eristavi
(	(	kIx(	(
<g/>
knížete	kníže	k1gMnSc4	kníže
<g/>
)	)	kIx)	)
Štěpána	Štěpán	k1gMnSc4	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
opustit	opustit	k5eAaPmF	opustit
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
příslušnost	příslušnost	k1gFnSc4	příslušnost
a	a	k8xC	a
uznat	uznat	k5eAaPmF	uznat
arabského	arabský	k2eAgMnSc4d1	arabský
chalífu	chalífa	k1gMnSc4	chalífa
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
nového	nový	k2eAgMnSc4d1	nový
nadřízeného	nadřízený	k1gMnSc4	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
vlivu	vliv	k1gInSc2	vliv
se	se	k3xPyFc4	se
Ibérie	Ibérie	k1gFnSc1	Ibérie
vymanila	vymanit	k5eAaPmAgFnS	vymanit
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
eristavi	eristaev	k1gFnSc6	eristaev
Ašot	Ašot	k1gInSc1	Ašot
I.	I.	kA	I.
z	z	k7c2	z
jihozápadogruzínského	jihozápadogruzínský	k2eAgInSc2d1	jihozápadogruzínský
rodu	rod	k1gInSc2	rod
Bagrationů	Bagration	k1gInPc2	Bagration
využil	využít	k5eAaPmAgMnS	využít
oslabení	oslabení	k1gNnSc4	oslabení
Arabské	arabský	k2eAgFnSc2d1	arabská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
dědičný	dědičný	k2eAgInSc4d1	dědičný
vládnoucí	vládnoucí	k2eAgInSc4d1	vládnoucí
rod	rod	k1gInSc4	rod
nad	nad	k7c7	nad
iberským	iberský	k2eAgNnSc7d1	Iberské
knížectvím	knížectví	k1gNnSc7	knížectví
opět	opět	k6eAd1	opět
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Ardanase	Ardanasa	k1gFnSc3	Ardanasa
II	II	kA	II
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
formálně	formálně	k6eAd1	formálně
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
byzantský	byzantský	k2eAgMnSc1d1	byzantský
vazal	vazal	k1gMnSc1	vazal
přijal	přijmout	k5eAaPmAgMnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
888	[number]	k4	888
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
<g/>
leté	letý	k2eAgFnSc6d1	letá
odmlce	odmlka	k1gFnSc6	odmlka
iberskou	iberský	k2eAgFnSc4d1	iberská
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Korunován	korunovat	k5eAaBmNgInS	korunovat
byl	být	k5eAaImAgInS	být
už	už	k9	už
ne	ne	k9	ne
jako	jako	k9	jako
vládce	vládce	k1gMnSc4	vládce
Ibérie	Ibérie	k1gFnSc2	Ibérie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
všech	všecek	k3xTgMnPc2	všecek
Gruzínců	Gruzínec	k1gMnPc2	Gruzínec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
potomek	potomek	k1gMnSc1	potomek
Bagrat	Bagrat	k1gMnSc1	Bagrat
III	III	kA	III
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
dokázal	dokázat	k5eAaPmAgInS	dokázat
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
sjednotit	sjednotit	k5eAaPmF	sjednotit
okolní	okolní	k2eAgNnPc1d1	okolní
rozdrobená	rozdrobený	k2eAgNnPc1d1	rozdrobené
knížectví	knížectví	k1gNnPc1	knížectví
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
pod	pod	k7c4	pod
jedinou	jediný	k2eAgFnSc4d1	jediná
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základ	základ	k1gInSc4	základ
existence	existence	k1gFnSc2	existence
Gruzínského	gruzínský	k2eAgNnSc2d1	gruzínské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
před	před	k7c7	před
nadvládou	nadvláda	k1gFnSc7	nadvláda
Ruska	Rusko	k1gNnSc2	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
Bagrationů	Bagration	k1gInPc2	Bagration
od	od	k7c2	od
konce	konec	k1gInSc2	konec
11	[number]	k4	11
<g/>
.	.	kIx.	.
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vláda	vláda	k1gFnSc1	vláda
Davida	David	k1gMnSc2	David
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
vnučky	vnuček	k1gMnPc7	vnuček
Tamary	Tamara	k1gFnSc2	Tamara
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholným	vrcholný	k2eAgFnPc3d1	vrcholná
epochám	epocha	k1gFnPc3	epocha
gruzínských	gruzínský	k2eAgFnPc2d1	gruzínská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
násilně	násilně	k6eAd1	násilně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
mongolskou	mongolský	k2eAgFnSc7d1	mongolská
invazí	invaze	k1gFnSc7	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
ránu	rána	k1gFnSc4	rána
jednotě	jednota	k1gFnSc6	jednota
Gruzie	Gruzie	k1gFnSc2	Gruzie
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
středoasijský	středoasijský	k2eAgMnSc1d1	středoasijský
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Tamerlán	Tamerlán	k2eAgMnSc1d1	Tamerlán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
čase	čas	k1gInSc6	čas
rozdrobena	rozdrobit	k5eAaPmNgFnS	rozdrobit
na	na	k7c4	na
drobná	drobný	k2eAgNnPc4d1	drobné
knížectví	knížectví	k1gNnPc4	knížectví
(	(	kIx(	(
<g/>
abchazské	abchazský	k2eAgNnSc4d1	abchazské
<g/>
,	,	kIx,	,
kachetinské	kachetinský	k2eAgNnSc4d1	kachetinský
<g/>
,	,	kIx,	,
eretínské	eretínský	k2eAgNnSc4d1	eretínský
a	a	k8xC	a
tao-klardžetské	taolardžetský	k2eAgNnSc4d1	tao-klardžetský
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
hlavní	hlavní	k2eAgNnPc4d1	hlavní
království	království	k1gNnPc4	království
–	–	k?	–
Imeretie	Imeretie	k1gFnSc2	Imeretie
<g/>
,	,	kIx,	,
Kachetie	Kachetie	k1gFnSc2	Kachetie
a	a	k8xC	a
Kartlie	Kartlie	k1gFnSc2	Kartlie
<g/>
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
jak	jak	k6eAd1	jak
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
východu	východ	k1gInSc2	východ
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
–	–	k?	–
Turci	Turek	k1gMnPc1	Turek
a	a	k8xC	a
Peršané	Peršan	k1gMnPc1	Peršan
získávali	získávat	k5eAaImAgMnP	získávat
postupně	postupně	k6eAd1	postupně
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
knížectvích	knížectví	k1gNnPc6	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgNnPc2	dva
velkých	velký	k2eAgNnPc2d1	velké
impérií	impérium	k1gNnPc2	impérium
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
tak	tak	k9	tak
část	část	k1gFnSc4	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
islamizovat	islamizovat	k5eAaBmF	islamizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
načas	načas	k6eAd1	načas
obnovil	obnovit	k5eAaPmAgMnS	obnovit
slávu	sláva	k1gFnSc4	sláva
Gruzie	Gruzie	k1gFnSc2	Gruzie
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Giorgi	Giorg	k1gFnSc2	Giorg
Saakadze	Saakadze	k1gFnSc2	Saakadze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zavraždění	zavraždění	k1gNnSc6	zavraždění
persko-turecká	perskourecký	k2eAgFnSc1d1	persko-turecký
agrese	agrese	k1gFnSc1	agrese
ještě	ještě	k6eAd1	ještě
zesílila	zesílit	k5eAaPmAgFnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
smlouva	smlouva	k1gFnSc1	smlouva
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
východních	východní	k2eAgMnPc2d1	východní
vládců	vládce	k1gMnPc2	vládce
o	o	k7c6	o
suverénnosti	suverénnost	k1gFnSc6	suverénnost
ruského	ruský	k2eAgNnSc2d1	ruské
carství	carství	k1gNnSc2	carství
nad	nad	k7c7	nad
těmito	tento	k3xDgNnPc7	tento
knížectvími	knížectví	k1gNnPc7	knížectví
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získali	získat	k5eAaPmAgMnP	získat
Rusové	Rus	k1gMnPc1	Rus
fakticky	fakticky	k6eAd1	fakticky
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
nadvlády	nadvláda	k1gFnSc2	nadvláda
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
se	se	k3xPyFc4	se
východní	východní	k2eAgNnPc4d1	východní
dvě	dva	k4xCgNnPc4	dva
knížectví	knížectví	k1gNnPc4	knížectví
formálně	formálně	k6eAd1	formálně
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
a	a	k8xC	a
Gruzíni	Gruzín	k1gMnPc1	Gruzín
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
začleňováni	začleňovat	k5eAaImNgMnP	začleňovat
do	do	k7c2	do
carské	carský	k2eAgFnSc2d1	carská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgMnSc1d1	slavný
carský	carský	k2eAgMnSc1d1	carský
generál	generál	k1gMnSc1	generál
Pjotr	Pjotr	k1gMnSc1	Pjotr
Bagration	Bagration	k1gInSc4	Bagration
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Rusům	Rus	k1gMnPc3	Rus
anektovat	anektovat	k5eAaBmF	anektovat
celou	celý	k2eAgFnSc4d1	celá
Gruzii	Gruzie	k1gFnSc4	Gruzie
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
přístavů	přístav	k1gInPc2	přístav
Suchumi	Suchumi	k1gNnSc2	Suchumi
a	a	k8xC	a
Batumi	Batumi	k1gNnSc2	Batumi
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
získali	získat	k5eAaPmAgMnP	získat
až	až	k9	až
po	po	k7c6	po
rusko-tureckých	ruskourecký	k2eAgFnPc6d1	rusko-turecká
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vliv	vliv	k1gInSc4	vliv
carství	carství	k1gNnSc2	carství
a	a	k8xC	a
rusifikace	rusifikace	k1gFnSc2	rusifikace
země	zem	k1gFnSc2	zem
natolik	natolik	k6eAd1	natolik
zesílil	zesílit	k5eAaPmAgMnS	zesílit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
začalo	začít	k5eAaPmAgNnS	začít
bouřit	bouřit	k5eAaImF	bouřit
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
růst	růst	k5eAaImF	růst
gruzínský	gruzínský	k2eAgInSc1d1	gruzínský
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
moc	moc	k6eAd1	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rychle	rychle	k6eAd1	rychle
získali	získat	k5eAaPmAgMnP	získat
gruzínští	gruzínský	k2eAgMnPc1d1	gruzínský
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Noa	Noa	k1gMnSc2	Noa
Žordaniji	Žordaniji	k1gMnSc2	Žordaniji
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
výbor	výbor	k1gInSc1	výbor
Zakavkazský	zakavkazský	k2eAgInSc4d1	zakavkazský
komisariát	komisariát	k1gInSc4	komisariát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
společná	společný	k2eAgFnSc1d1	společná
vláda	vláda	k1gFnSc1	vláda
nacionálních	nacionální	k2eAgFnPc2d1	nacionální
stran	strana	k1gFnPc2	strana
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
budoucích	budoucí	k2eAgFnPc2d1	budoucí
kavkazských	kavkazský	k2eAgFnPc2d1	kavkazská
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
Zakavkazské	zakavkazský	k2eAgFnSc2d1	zakavkazská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Existovala	existovat	k5eAaImAgFnS	existovat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
začal	začít	k5eAaPmAgInS	začít
požadovat	požadovat	k5eAaImF	požadovat
politickou	politický	k2eAgFnSc4d1	politická
i	i	k8xC	i
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
orientaci	orientace	k1gFnSc4	orientace
na	na	k7c4	na
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
nemohli	moct	k5eNaImAgMnP	moct
představitelé	představitel	k1gMnPc1	představitel
Arménie	Arménie	k1gFnSc2	Arménie
a	a	k8xC	a
Gruzie	Gruzie	k1gFnSc2	Gruzie
souhlasit	souhlasit	k5eAaImF	souhlasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
požádala	požádat	k5eAaPmAgFnS	požádat
Německo	Německo	k1gNnSc4	Německo
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
také	také	k9	také
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ruští	ruský	k2eAgMnPc1d1	ruský
bolševici	bolševik	k1gMnPc1	bolševik
uznali	uznat	k5eAaPmAgMnP	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Gruzie	Gruzie	k1gFnSc2	Gruzie
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
povolena	povolit	k5eAaPmNgFnS	povolit
bolševická	bolševický	k2eAgFnSc1d1	bolševická
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
bělogvardějcům	bělogvardějec	k1gMnPc3	bělogvardějec
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
bolševické	bolševický	k2eAgNnSc1d1	bolševické
vedení	vedení	k1gNnSc1	vedení
ale	ale	k8xC	ale
porušilo	porušit	k5eAaPmAgNnS	porušit
veškeré	veškerý	k3xTgFnPc4	veškerý
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
země	země	k1gFnSc1	země
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
SSR	SSR	kA	SSR
a	a	k8xC	a
menševická	menševický	k2eAgFnSc1d1	menševická
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
SSR	SSR	kA	SSR
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
Adžarská	Adžarský	k2eAgFnSc1d1	Adžarský
ASSR	ASSR	kA	ASSR
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Abchazská	abchazský	k2eAgFnSc1d1	abchazská
ASSR	ASSR	kA	ASSR
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
Jihoosetská	jihoosetský	k2eAgFnSc1d1	jihoosetská
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
i	i	k9	i
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
integraci	integrace	k1gFnSc6	integrace
s	s	k7c7	s
Arménií	Arménie	k1gFnSc7	Arménie
a	a	k8xC	a
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
v	v	k7c4	v
Zakavkazskou	zakavkazský	k2eAgFnSc4d1	zakavkazská
SFSR	SFSR	kA	SFSR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1936	[number]	k4	1936
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
protisovětské	protisovětský	k2eAgNnSc1d1	protisovětské
srpnové	srpnový	k2eAgNnSc1d1	srpnové
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Gruzie	Gruzie	k1gFnSc2	Gruzie
pocházel	pocházet	k5eAaImAgMnS	pocházet
sovětský	sovětský	k2eAgMnSc1d1	sovětský
diktátor	diktátor	k1gMnSc1	diktátor
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
Sergo	Sergo	k1gNnSc4	Sergo
Ordžonikidze	Ordžonikidze	k1gNnSc2	Ordžonikidze
a	a	k8xC	a
Lavrentij	Lavrentij	k1gMnSc1	Lavrentij
Berija	Berija	k1gMnSc1	Berija
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
neomezeným	omezený	k2eNgMnSc7d1	neomezený
pánem	pán	k1gMnSc7	pán
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
obávaným	obávaný	k2eAgMnSc7d1	obávaný
šéfem	šéf	k1gMnSc7	šéf
sovětské	sovětský	k2eAgFnSc2d1	sovětská
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
pádu	pád	k1gInSc2	pád
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
vláda	vláda	k1gFnSc1	vláda
Gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
SSR	SSR	kA	SSR
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
okupací	okupace	k1gFnSc7	okupace
a	a	k8xC	a
porušením	porušení	k1gNnSc7	porušení
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgInP	schválit
zákony	zákon	k1gInPc1	zákon
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
k	k	k7c3	k
zárukám	záruka	k1gFnPc3	záruka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
úloha	úloha	k1gFnSc1	úloha
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
;	;	kIx,	;
nezávislost	nezávislost	k1gFnSc1	nezávislost
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
se	se	k3xPyFc4	se
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
nových	nový	k2eAgFnPc2d1	nová
republik	republika	k1gFnPc2	republika
nestala	stát	k5eNaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
SNS	SNS	kA	SNS
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
z	z	k7c2	z
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
důvodů	důvod	k1gInPc2	důvod
do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
(	(	kIx(	(
<g/>
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
jako	jako	k8xC	jako
následek	následek	k1gInSc1	následek
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
nezávislých	závislý	k2eNgFnPc6d1	nezávislá
volbách	volba	k1gFnPc6	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
nacionalista	nacionalista	k1gMnSc1	nacionalista
Zviad	Zviad	k1gInSc4	Zviad
Gamsachurdia	Gamsachurdium	k1gNnSc2	Gamsachurdium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zrušit	zrušit	k5eAaPmF	zrušit
autonomní	autonomní	k2eAgNnSc4d1	autonomní
postavení	postavení	k1gNnSc4	postavení
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
a	a	k8xC	a
Abcházie	Abcházie	k1gFnSc2	Abcházie
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sovět	sovět	k1gInSc1	sovět
snažil	snažit	k5eAaImAgInS	snažit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
přičiněním	přičinění	k1gNnSc7	přičinění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
vypukat	vypukat	k5eAaImF	vypukat
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přenesly	přenést	k5eAaPmAgFnP	přenést
i	i	k9	i
do	do	k7c2	do
autonomních	autonomní	k2eAgFnPc2d1	autonomní
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
žádajících	žádající	k2eAgInPc2d1	žádající
také	také	k9	také
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
nebylo	být	k5eNaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgFnPc1d1	autonomní
republiky	republika	k1gFnPc1	republika
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
snaze	snaha	k1gFnSc3	snaha
nového	nový	k2eAgNnSc2d1	nové
gruzínského	gruzínský	k2eAgNnSc2d1	gruzínské
vedení	vedení	k1gNnSc2	vedení
zrušit	zrušit	k5eAaPmF	zrušit
jejich	jejich	k3xOp3gFnSc4	jejich
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
Gruzie	Gruzie	k1gFnSc2	Gruzie
jednotný	jednotný	k2eAgInSc4d1	jednotný
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
a	a	k8xC	a
1992	[number]	k4	1992
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
natolik	natolik	k6eAd1	natolik
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
mezi	mezi	k7c7	mezi
Osety	Oset	k1gMnPc7	Oset
a	a	k8xC	a
Gruzíni	Gruzín	k1gMnPc1	Gruzín
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
střety	střet	k1gInPc4	střet
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgInPc4d1	nesoucí
znaky	znak	k1gInPc4	znak
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uklidnění	uklidnění	k1gNnSc6	uklidnění
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
země	zem	k1gFnSc2	zem
dostal	dostat	k5eAaPmAgMnS	dostat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
SSSR	SSSR	kA	SSSR
Eduard	Eduard	k1gMnSc1	Eduard
Ševardnadze	Ševardnadze	k1gFnSc2	Ševardnadze
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgInS	dostat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Gamsachurdii	Gamsachurdie	k1gFnSc4	Gamsachurdie
probíhaly	probíhat	k5eAaImAgInP	probíhat
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
boje	boj	k1gInPc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vládnout	vládnout	k5eAaImF	vládnout
vydržel	vydržet	k5eAaPmAgMnS	vydržet
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
země	zem	k1gFnPc4	zem
až	až	k9	až
do	do	k7c2	do
růžové	růžový	k2eAgFnSc2d1	růžová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
tendencí	tendence	k1gFnPc2	tendence
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
v	v	k7c6	v
postkomunistických	postkomunistický	k2eAgFnPc6d1	postkomunistická
společnostech	společnost	k1gFnPc6	společnost
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
barevná	barevný	k2eAgFnSc1d1	barevná
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
růžové	růžový	k2eAgFnSc2d1	růžová
<g/>
"	"	kIx"	"
revoluce	revoluce	k1gFnPc1	revoluce
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
prezidentem	prezident	k1gMnSc7	prezident
Michailem	Michail	k1gMnSc7	Michail
Saakašvilim	Saakašvilima	k1gFnPc2	Saakašvilima
proti	proti	k7c3	proti
Ševardnadzeho	Ševardnadze	k1gMnSc4	Ševardnadze
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nevyřešila	vyřešit	k5eNaPmAgFnS	vyřešit
základní	základní	k2eAgInPc4d1	základní
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
problémy	problém	k1gInPc4	problém
–	–	k?	–
chudobu	chudoba	k1gFnSc4	chudoba
a	a	k8xC	a
korupci	korupce	k1gFnSc4	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
Saakašvili	Saakašvili	k1gMnSc1	Saakašvili
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
už	už	k6eAd1	už
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgInS	získat
95	[number]	k4	95
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
zmanipulované	zmanipulovaný	k2eAgFnPc1d1	zmanipulovaná
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
volebním	volební	k2eAgInSc6d1	volební
výsledku	výsledek	k1gInSc6	výsledek
podepsaly	podepsat	k5eAaPmAgInP	podepsat
dary	dar	k1gInPc1	dar
od	od	k7c2	od
nadací	nadace	k1gFnPc2	nadace
amerického	americký	k2eAgMnSc2d1	americký
miliardáře	miliardář	k1gMnSc2	miliardář
George	Georg	k1gMnSc2	Georg
Sorose	Sorosa	k1gFnSc6	Sorosa
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nadací	nadace	k1gFnPc2	nadace
financovaných	financovaný	k2eAgInPc2d1	financovaný
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Saakašvili	Saakašvit	k5eAaPmAgMnP	Saakašvit
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
prosadil	prosadit	k5eAaPmAgMnS	prosadit
úpravy	úprava	k1gFnPc4	úprava
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
"	"	kIx"	"
<g/>
superprezidentský	superprezidentský	k2eAgInSc1d1	superprezidentský
systém	systém	k1gInSc1	systém
<g/>
"	"	kIx"	"
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
veškeré	veškerý	k3xTgFnPc4	veškerý
pravomoci	pravomoc	k1gFnPc4	pravomoc
v	v	k7c6	v
exekutivě	exekutiva	k1gFnSc6	exekutiva
<g/>
.	.	kIx.	.
</s>
<s>
Gruzínským	gruzínský	k2eAgInSc7d1	gruzínský
parlamentem	parlament	k1gInSc7	parlament
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgInP	schválit
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
byla	být	k5eAaImAgFnS	být
snížena	snížit	k5eAaPmNgFnS	snížit
nezávislost	nezávislost	k1gFnSc1	nezávislost
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
či	či	k8xC	či
podle	podle	k7c2	podle
kterých	který	k3yRgMnPc2	který
jsou	být	k5eAaImIp3nP	být
ústřední	ústřední	k2eAgFnPc1d1	ústřední
i	i	k8xC	i
volební	volební	k2eAgFnPc1d1	volební
komise	komise	k1gFnPc1	komise
sestavovány	sestavovat	k5eAaImNgFnP	sestavovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
eliminována	eliminovat	k5eAaBmNgFnS	eliminovat
plná	plný	k2eAgFnSc1d1	plná
svoboda	svoboda	k1gFnSc1	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
konaly	konat	k5eAaImAgFnP	konat
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
lidé	člověk	k1gMnPc1	člověk
žádali	žádat	k5eAaImAgMnP	žádat
odstoupení	odstoupení	k1gNnSc4	odstoupení
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
brutálně	brutálně	k6eAd1	brutálně
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zatýkat	zatýkat	k5eAaImF	zatýkat
představitele	představitel	k1gMnSc4	představitel
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
protesty	protest	k1gInPc1	protest
přerostly	přerůst	k5eAaPmAgInP	přerůst
v	v	k7c4	v
masové	masový	k2eAgFnPc4d1	masová
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
lidé	člověk	k1gMnPc1	člověk
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
dramatický	dramatický	k2eAgInSc4d1	dramatický
pokles	pokles	k1gInSc4	pokles
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
a	a	k8xC	a
korupci	korupce	k1gFnSc4	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
"	"	kIx"	"
<g/>
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zakázal	zakázat	k5eAaPmAgInS	zakázat
nestátní	státní	k2eNgNnPc4d1	nestátní
média	médium	k1gNnPc4	médium
<g/>
,	,	kIx,	,
zakázal	zakázat	k5eAaPmAgInS	zakázat
veřejná	veřejný	k2eAgNnPc4d1	veřejné
shromáždění	shromáždění	k1gNnPc4	shromáždění
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
podnikat	podnikat	k5eAaImF	podnikat
nedemokratické	demokratický	k2eNgInPc4d1	nedemokratický
kroky	krok	k1gInPc4	krok
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
politickým	politický	k2eAgMnPc3d1	politický
oponentům	oponent	k1gMnPc3	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
Saakašvili	Saakašvili	k1gMnPc2	Saakašvili
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
znovu	znovu	k6eAd1	znovu
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
které	který	k3yQgFnPc4	který
opozice	opozice	k1gFnSc1	opozice
označila	označit	k5eAaPmAgFnS	označit
jako	jako	k9	jako
zmanipulované	zmanipulovaný	k2eAgFnPc4d1	zmanipulovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
konaly	konat	k5eAaImAgFnP	konat
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
prezidenta	prezident	k1gMnSc2	prezident
Saakašviliho	Saakašvili	k1gMnSc2	Saakašvili
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
63	[number]	k4	63
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
označila	označit	k5eAaPmAgFnS	označit
tyto	tento	k3xDgFnPc4	tento
volby	volba	k1gFnPc4	volba
za	za	k7c4	za
zfalšované	zfalšovaný	k2eAgMnPc4d1	zfalšovaný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
opoziční	opoziční	k2eAgFnSc1d1	opoziční
koalice	koalice	k1gFnSc1	koalice
Gruzínský	gruzínský	k2eAgInSc1d1	gruzínský
sen	sen	k1gInSc1	sen
miliardáře	miliardář	k1gMnSc2	miliardář
Bidziny	Bidzina	k1gMnSc2	Bidzina
Ivanišviliho	Ivanišvili	k1gMnSc2	Ivanišvili
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Saakašvili	Saakašvili	k1gMnSc1	Saakašvili
uznal	uznat	k5eAaPmAgMnS	uznat
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
začala	začít	k5eAaPmAgFnS	začít
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
mezi	mezi	k7c7	mezi
Gruzií	Gruzie	k1gFnSc7	Gruzie
a	a	k8xC	a
jihoosetinskými	jihoosetinský	k2eAgMnPc7d1	jihoosetinský
separatisty	separatista	k1gMnPc7	separatista
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
nabírat	nabírat	k5eAaImF	nabírat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
přidalo	přidat	k5eAaPmAgNnS	přidat
Rusko	Rusko	k1gNnSc1	Rusko
poté	poté	k6eAd1	poté
co	co	k9	co
podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
OBSE	OBSE	kA	OBSE
gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
armáda	armáda	k1gFnSc1	armáda
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
nerozlišující	rozlišující	k2eNgFnSc7d1	nerozlišující
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1	dělostřelecká
a	a	k8xC	a
raketometnou	raketometný	k2eAgFnSc7d1	raketometná
palbou	palba	k1gFnSc7	palba
na	na	k7c4	na
správní	správní	k2eAgNnSc4d1	správní
středisko	středisko	k1gNnSc4	středisko
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
Cchinvali	Cchinvali	k1gFnSc2	Cchinvali
<g/>
.	.	kIx.	.
</s>
<s>
Gruzíni	Gruzín	k1gMnPc1	Gruzín
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
byli	být	k5eAaImAgMnP	být
donuceni	donucen	k2eAgMnPc1d1	donucen
opustit	opustit	k5eAaPmF	opustit
domovy	domov	k1gInPc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
začala	začít	k5eAaPmAgFnS	začít
ihned	ihned	k6eAd1	ihned
poskytovat	poskytovat	k5eAaImF	poskytovat
materiální	materiální	k2eAgFnSc4d1	materiální
a	a	k8xC	a
psychosociální	psychosociální	k2eAgFnSc4d1	psychosociální
podporu	podpora	k1gFnSc4	podpora
tisícům	tisíc	k4xCgInPc3	tisíc
utečencůJižní	utečencůJižní	k2eAgFnSc1d1	utečencůJižní
Osetie	Osetie	k1gFnSc1	Osetie
obývaná	obývaný	k2eAgFnSc1d1	obývaná
Osety	Oset	k1gMnPc7	Oset
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
ruské	ruský	k2eAgFnSc6d1	ruská
republice	republika	k1gFnSc6	republika
Severní	severní	k2eAgFnSc2d1	severní
Osetie-Alanie	Osetie-Alanie	k1gFnSc2	Osetie-Alanie
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Abcházie	Abcházie	k1gFnSc1	Abcházie
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Gruzii	Gruzie	k1gFnSc6	Gruzie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
Gruzie	Gruzie	k1gFnSc1	Gruzie
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
se	se	k3xPyFc4	se
od	od	k7c2	od
bývalého	bývalý	k2eAgNnSc2d1	bývalé
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
faktickou	faktický	k2eAgFnSc4d1	faktická
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Gruzie	Gruzie	k1gFnSc1	Gruzie
nikdy	nikdy	k6eAd1	nikdy
neuznala	uznat	k5eNaPmAgFnS	uznat
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
Jižní	jižní	k2eAgFnSc1d1	jižní
Osetie	Osetie	k1gFnSc1	Osetie
po	po	k7c6	po
první	první	k4xOgFnSc6	první
gruzínsko-osetinské	gruzínskosetinský	k2eAgFnSc6d1	gruzínsko-osetinský
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
a	a	k8xC	a
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1992.12	[number]	k4	1992.12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
Rusko	Rusko	k1gNnSc1	Rusko
stažení	stažení	k1gNnSc2	stažení
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
z	z	k7c2	z
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
schválil	schválit	k5eAaPmAgInS	schválit
gruzínský	gruzínský	k2eAgInSc1d1	gruzínský
parlament	parlament	k1gInSc1	parlament
kvůli	kvůli	k7c3	kvůli
ruské	ruský	k2eAgFnSc3d1	ruská
agresi	agrese	k1gFnSc3	agrese
vystoupení	vystoupení	k1gNnSc2	vystoupení
ze	z	k7c2	z
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byly	být	k5eAaImAgFnP	být
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
uznány	uznán	k2eAgInPc1d1	uznán
samostatné	samostatný	k2eAgInPc1d1	samostatný
státy	stát	k1gInPc1	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
a	a	k8xC	a
Abcházie	Abcházie	k1gFnSc2	Abcházie
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc4	uznání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
západní	západní	k2eAgMnPc1d1	západní
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
podporovali	podporovat	k5eAaImAgMnP	podporovat
Gruzii	Gruzie	k1gFnSc4	Gruzie
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
EU	EU	kA	EU
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Medveděv	Medveděv	k1gMnSc1	Medveděv
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
z	z	k7c2	z
gruzínského	gruzínský	k2eAgNnSc2d1	gruzínské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Osetii	Osetie	k1gFnSc3	Osetie
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
ponechá	ponechat	k5eAaPmIp3nS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
země	zem	k1gFnPc1	zem
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
potýká	potýkat	k5eAaImIp3nS	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
nestabilní	stabilní	k2eNgFnSc4d1	nestabilní
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
či	či	k8xC	či
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
odliv	odliv	k1gInSc4	odliv
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
vede	vést	k5eAaImIp3nS	vést
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
po	po	k7c6	po
hlavním	hlavní	k2eAgNnSc6d1	hlavní
kavkazském	kavkazský	k2eAgNnSc6d1	kavkazské
rozvodí	rozvodí	k1gNnSc6	rozvodí
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
Gruzie	Gruzie	k1gFnSc2	Gruzie
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
názorů	názor	k1gInPc2	názor
by	by	kYmCp3nS	by
Gruzie	Gruzie	k1gFnSc1	Gruzie
ležela	ležet	k5eAaImAgFnS	ležet
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
její	její	k3xOp3gInPc1	její
severní	severní	k2eAgInPc1d1	severní
výběžky	výběžek	k1gInPc1	výběžek
by	by	kYmCp3nP	by
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zasahovaly	zasahovat	k5eAaImAgInP	zasahovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přijímané	přijímaný	k2eAgFnSc2d1	přijímaná
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Kuma	kum	k1gMnSc2	kum
a	a	k8xC	a
Manyč	Manyč	k1gInSc1	Manyč
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
i	i	k8xC	i
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
části	část	k1gFnSc2	část
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
definice	definice	k1gFnPc1	definice
vedou	vést	k5eAaImIp3nP	vést
evropsko-asijskou	evropskosijský	k2eAgFnSc4d1	evropsko-asijský
hranici	hranice	k1gFnSc4	hranice
také	také	k9	také
přímo	přímo	k6eAd1	přímo
Gruzií	Gruzie	k1gFnSc7	Gruzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
Gruzie	Gruzie	k1gFnSc2	Gruzie
41	[number]	k4	41
až	až	k9	až
43	[number]	k4	43
<g/>
°	°	k?	°
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
např.	např.	kA	např.
šířce	šířka	k1gFnSc6	šířka
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
,	,	kIx,	,
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
severního	severní	k2eAgNnSc2d1	severní
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
pomezí	pomezí	k1gNnSc2	pomezí
Oregonu	Oregon	k1gInSc2	Oregon
a	a	k8xC	a
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
Gruzie	Gruzie	k1gFnSc2	Gruzie
40	[number]	k4	40
až	až	k9	až
47	[number]	k4	47
<g/>
°	°	k?	°
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
např.	např.	kA	např.
délce	délka	k1gFnSc6	délka
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
západního	západní	k2eAgInSc2d1	západní
Jemenu	Jemen	k1gInSc2	Jemen
a	a	k8xC	a
Mosambického	mosambický	k2eAgInSc2d1	mosambický
průlivu	průliv	k1gInSc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
69	[number]	k4	69
700	[number]	k4	700
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Gruzie	Gruzie	k1gFnSc1	Gruzie
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c6	o
1	[number]	k4	1
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
částečně	částečně	k6eAd1	částečně
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznaných	uznaný	k2eAgFnPc2d1	uznaná
separatistických	separatistický	k2eAgFnPc2d1	separatistická
provincií	provincie	k1gFnPc2	provincie
Abcházie	Abcházie	k1gFnSc2	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
činí	činit	k5eAaImIp3nS	činit
rozloha	rozloha	k1gFnSc1	rozloha
Gruzie	Gruzie	k1gFnSc2	Gruzie
jen	jen	k9	jen
57	[number]	k4	57
200	[number]	k4	200
km2	km2	k4	km2
(	(	kIx(	(
<g/>
o	o	k7c4	o
málo	málo	k4c4	málo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
18	[number]	k4	18
%	%	kIx~	%
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Arménií	Arménie	k1gFnSc7	Arménie
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
Kavkazem	Kavkaz	k1gInSc7	Kavkaz
a	a	k8xC	a
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
severní	severní	k2eAgFnPc1d1	severní
hranice	hranice	k1gFnPc1	hranice
tvoří	tvořit	k5eAaImIp3nP	tvořit
Velký	velký	k2eAgInSc4d1	velký
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
Malý	Malý	k1gMnSc1	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
Gruzie	Gruzie	k1gFnSc2	Gruzie
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
spojeny	spojen	k2eAgInPc1d1	spojen
Lišským	Lišský	k2eAgInSc7d1	Lišský
(	(	kIx(	(
<g/>
Suramským	Suramský	k2eAgInSc7d1	Suramský
<g/>
)	)	kIx)	)
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
na	na	k7c4	na
západ	západ	k1gInSc4	západ
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
široká	široký	k2eAgFnSc1d1	široká
kotlina	kotlina	k1gFnSc1	kotlina
řeky	řeka	k1gFnSc2	řeka
Rioni	Rioeň	k1gFnSc3	Rioeň
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
pak	pak	k6eAd1	pak
údolí	údolí	k1gNnSc1	údolí
Kury	kura	k1gFnSc2	kura
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
Elbrus	Elbrus	k1gInSc1	Elbrus
leží	ležet	k5eAaImIp3nS	ležet
kousek	kousek	k1gInSc4	kousek
od	od	k7c2	od
gruzínských	gruzínský	k2eAgFnPc2d1	gruzínská
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hraničního	hraniční	k2eAgInSc2d1	hraniční
hřebene	hřeben	k1gInSc2	hřeben
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
pětitisícovek	pětitisícovka	k1gFnPc2	pětitisícovka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Šchara	Šchara	k1gFnSc1	Šchara
(	(	kIx(	(
<g/>
5201	[number]	k4	5201
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
rozvinuta	rozvinout	k5eAaPmNgFnS	rozvinout
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Nejhustší	hustý	k2eAgFnSc1d3	nejhustší
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Gruzii	Gruzie	k1gFnSc6	Gruzie
a	a	k8xC	a
nejmenší	malý	k2eAgMnSc1d3	nejmenší
na	na	k7c6	na
Jurské	jurský	k2eAgFnSc6d1	jurská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
odtokem	odtok	k1gInSc7	odtok
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
od	od	k7c2	od
80	[number]	k4	80
až	až	k9	až
150	[number]	k4	150
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Gruzii	Gruzie	k1gFnSc6	Gruzie
do	do	k7c2	do
3	[number]	k4	3
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
na	na	k7c6	na
Jorské	Jorský	k2eAgFnSc6d1	Jorský
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Dolnokartlijské	Dolnokartlijský	k2eAgFnSc2d1	Dolnokartlijský
roviny	rovina	k1gFnSc2	rovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
úmořím	úmoří	k1gNnSc7	úmoří
Kaspického	kaspický	k2eAgMnSc2d1	kaspický
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Úmoří	úmoří	k1gNnSc1	úmoří
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tvořeno	tvořit	k5eAaImNgNnS	tvořit
povodím	povodí	k1gNnSc7	povodí
řeky	řeka	k1gFnSc2	řeka
Kury	kura	k1gFnSc2	kura
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
zleva	zleva	k6eAd1	zleva
Velká	velký	k2eAgFnSc1d1	velká
Liachvi	Liachev	k1gFnSc3	Liachev
<g/>
,	,	kIx,	,
Ksani	Ksaeň	k1gFnSc6	Ksaeň
<g/>
,	,	kIx,	,
Aragvi	Aragev	k1gFnSc6	Aragev
<g/>
,	,	kIx,	,
Jori	Jor	k1gFnSc6	Jor
a	a	k8xC	a
Alazani	Alazaň	k1gFnSc6	Alazaň
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgNnPc4d1	poslední
dvě	dva	k4xCgNnPc4	dva
ústí	ústí	k1gNnPc4	ústí
do	do	k7c2	do
Mingačevirské	Mingačevirský	k2eAgFnSc2d1	Mingačevirský
přehradní	přehradní	k2eAgFnSc2d1	přehradní
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
)	)	kIx)	)
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
Paravani	Paravan	k1gMnPc1	Paravan
<g/>
,	,	kIx,	,
Dzama	Dzama	k1gNnSc1	Dzama
<g/>
,	,	kIx,	,
Tana	tanout	k5eAaImSgInS	tanout
<g/>
,	,	kIx,	,
Tedzami	Tedza	k1gFnPc7	Tedza
<g/>
,	,	kIx,	,
Algeti	Alget	k1gMnPc1	Alget
a	a	k8xC	a
Chrami	Chra	k1gFnPc7	Chra
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
černomořského	černomořský	k2eAgNnSc2d1	černomořské
úmoří	úmoří	k1gNnSc2	úmoří
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
)	)	kIx)	)
netvoří	tvořit	k5eNaImIp3nS	tvořit
žádný	žádný	k3yNgInSc4	žádný
systém	systém	k1gInSc4	systém
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
každá	každý	k3xTgFnSc1	každý
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Rioni	Rion	k1gMnPc1	Rion
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
Cchenisckali	Cchenisckali	k1gFnSc2	Cchenisckali
<g/>
,	,	kIx,	,
Techuri	Techur	k1gFnSc2	Techur
a	a	k8xC	a
Kvirila	Kvirila	k1gFnSc1	Kvirila
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Kolchidskou	kolchidský	k2eAgFnSc7d1	Kolchidská
propadlinou	propadlina	k1gFnSc7	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
řeky	řeka	k1gFnPc1	řeka
ústící	ústící	k2eAgFnPc1d1	ústící
do	do	k7c2	do
Černého	Černý	k1gMnSc2	Černý
moře	moře	k1gNnSc2	moře
jsou	být	k5eAaImIp3nP	být
Inguri	Ingur	k1gMnPc1	Ingur
<g/>
,	,	kIx,	,
Kodori	Kodor	k1gMnPc1	Kodor
<g/>
,	,	kIx,	,
Adžarisckali	Adžarisckali	k1gMnPc1	Adžarisckali
<g/>
,	,	kIx,	,
Bzipi	Bzip	k1gMnPc1	Bzip
<g/>
,	,	kIx,	,
Chobi	Chob	k1gMnPc1	Chob
a	a	k8xC	a
Galidzga	Galidzga	k1gFnSc1	Galidzga
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	Řek	k1gMnSc2	Řek
Čoroch	Čoroch	k1gMnSc1	Čoroch
pramenící	pramenící	k2eAgMnSc1d1	pramenící
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
protéká	protékat	k5eAaImIp3nS	protékat
Gruzií	Gruzie	k1gFnPc2	Gruzie
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
(	(	kIx(	(
<g/>
26	[number]	k4	26
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
řek	řeka	k1gFnPc2	řeka
pramenících	pramenící	k2eAgFnPc2d1	pramenící
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
má	mít	k5eAaImIp3nS	mít
maximální	maximální	k2eAgInSc1d1	maximální
průtok	průtok	k1gInSc1	průtok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tání	tání	k1gNnSc2	tání
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
ledovce	ledovec	k1gInPc1	ledovec
(	(	kIx(	(
<g/>
Kodori	Kodor	k1gMnPc1	Kodor
<g/>
,	,	kIx,	,
Inguri	Ingur	k1gMnPc1	Ingur
<g/>
,	,	kIx,	,
Rioni	Rioen	k2eAgMnPc1d1	Rioen
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
průtok	průtok	k1gInSc4	průtok
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
s	s	k7c7	s
výraznými	výrazný	k2eAgMnPc7d1	výrazný
maximy	maxima	k1gFnPc1	maxima
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
minimem	minimum	k1gNnSc7	minimum
před	před	k7c7	před
úsvitem	úsvit	k1gInSc7	úsvit
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
Adžarského	Adžarský	k2eAgNnSc2d1	Adžarský
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
severních	severní	k2eAgInPc2d1	severní
svahů	svah	k1gInPc2	svah
Meschaetského	Meschaetský	k2eAgInSc2d1	Meschaetský
hřbetu	hřbet	k1gInSc2	hřbet
mají	mít	k5eAaImIp3nP	mít
maxima	maximum	k1gNnPc1	maximum
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
časté	častý	k2eAgFnPc1d1	častá
povodně	povodeň	k1gFnPc1	povodeň
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnPc1d1	Horské
řeky	řeka	k1gFnPc1	řeka
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
rychlým	rychlý	k2eAgInSc7d1	rychlý
tokem	tok	k1gInSc7	tok
a	a	k8xC	a
tak	tak	k6eAd1	tak
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
řeky	řeka	k1gFnPc1	řeka
Jihogruzínské	Jihogruzínský	k2eAgFnSc2d1	Jihogruzínský
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tečou	téct	k5eAaImIp3nP	téct
pomalu	pomalu	k6eAd1	pomalu
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
tvořené	tvořený	k2eAgFnPc1d1	tvořená
vápenci	vápenec	k1gInPc7	vápenec
a	a	k8xC	a
vyvřelinami	vyvřelina	k1gFnPc7	vyvřelina
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
silně	silně	k6eAd1	silně
rozvětvenými	rozvětvený	k2eAgInPc7d1	rozvětvený
podzemními	podzemní	k2eAgInPc7d1	podzemní
systémy	systém	k1gInPc7	systém
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
silných	silný	k2eAgNnPc2d1	silné
vřídel	vřídlo	k1gNnPc2	vřídlo
(	(	kIx(	(
<g/>
řeky	řeka	k1gFnPc1	řeka
Čjornaja	Čjornaj	k2eAgFnSc1d1	Čjornaj
<g/>
,	,	kIx,	,
Cačchuri	Cačchuri	k1gNnSc1	Cačchuri
<g/>
,	,	kIx,	,
Rečchi	Rečchi	k1gNnSc1	Rečchi
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Gruzii	Gruzie	k1gFnSc6	Gruzie
a	a	k8xC	a
Nardevani	Nardevaň	k1gFnSc6	Nardevaň
<g/>
,	,	kIx,	,
Tašbaši	Tašbaše	k1gFnSc6	Tašbaše
<g/>
,	,	kIx,	,
Armutlo	Armutlo	k1gFnSc1	Armutlo
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přerozdělují	přerozdělovat	k5eAaImIp3nP	přerozdělovat
a	a	k8xC	a
přirozeně	přirozeně	k6eAd1	přirozeně
regulují	regulovat	k5eAaImIp3nP	regulovat
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
<g/>
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
hydroenergetické	hydroenergetický	k2eAgInPc4d1	hydroenergetický
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
Džavachetie	Džavachetie	k1gFnSc1	Džavachetie
<g/>
,	,	kIx,	,
Kolchidská	kolchidský	k2eAgFnSc1d1	Kolchidská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Kelská	Kelský	k2eAgFnSc1d1	Kelský
planina	planina	k1gFnSc1	planina
<g/>
,	,	kIx,	,
staroledovcová	staroledovcový	k2eAgFnSc1d1	staroledovcový
zóna	zóna	k1gFnSc1	zóna
Abcházie	Abcházie	k1gFnSc2	Abcházie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
skupiny	skupina	k1gFnPc1	skupina
jezer	jezero	k1gNnPc2	jezero
tektonického	tektonický	k2eAgInSc2d1	tektonický
<g/>
,	,	kIx,	,
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
<g/>
,	,	kIx,	,
mořského	mořský	k2eAgInSc2d1	mořský
<g/>
,	,	kIx,	,
říčního	říční	k2eAgMnSc2d1	říční
<g/>
,	,	kIx,	,
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
<g/>
,	,	kIx,	,
závalového	závalový	k2eAgInSc2d1	závalový
<g/>
,	,	kIx,	,
krasového	krasový	k2eAgInSc2d1	krasový
jiného	jiný	k2eAgInSc2d1	jiný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Paravani	Paravan	k1gMnPc1	Paravan
(	(	kIx(	(
<g/>
37	[number]	k4	37
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karcachi	Karcachi	k1gNnSc1	Karcachi
(	(	kIx(	(
<g/>
26,6	[number]	k4	26,6
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paleostomi	Paleosto	k1gFnPc7	Paleosto
(	(	kIx(	(
<g/>
17,3	[number]	k4	17,3
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgNnPc1d3	nejhlubší
jsou	být	k5eAaImIp3nP	být
závalová	závalový	k2eAgNnPc1d1	závalový
jezera	jezero	k1gNnPc1	jezero
Rica	Ric	k2eAgNnPc1d1	Ric
(	(	kIx(	(
<g/>
116	[number]	k4	116
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Amtkel	Amtkel	k1gMnSc1	Amtkel
(	(	kIx(	(
<g/>
72	[number]	k4	72
až	až	k9	až
112	[number]	k4	112
m	m	kA	m
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
hladiny	hladina	k1gFnSc2	hladina
<g/>
)	)	kIx)	)
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
Kelistba	Kelistba	k1gMnSc1	Kelistba
(	(	kIx(	(
<g/>
75	[number]	k4	75
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rybí	rybí	k2eAgInSc1d1	rybí
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
na	na	k7c6	na
jezerech	jezero	k1gNnPc6	jezero
Tabackuri	Tabackur	k1gFnSc2	Tabackur
a	a	k8xC	a
Paleostomi	Paleosto	k1gFnPc7	Paleosto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
složitým	složitý	k2eAgNnSc7d1	složité
geomorfologickým	geomorfologický	k2eAgNnSc7d1	Geomorfologické
členěním	členění	k1gNnSc7	členění
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
je	být	k5eAaImIp3nS	být
teplé	teplý	k2eAgNnSc1d1	teplé
subtropické	subtropický	k2eAgNnSc1d1	subtropické
podnebí	podnebí	k1gNnSc1	podnebí
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
od	od	k7c2	od
1	[number]	k4	1
000	[number]	k4	000
až	až	k6eAd1	až
k	k	k7c3	k
2	[number]	k4	2
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgNnSc1d1	Teplé
podnebí	podnebí	k1gNnSc1	podnebí
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pěstovat	pěstovat	k5eAaImF	pěstovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
plodin	plodina	k1gFnPc2	plodina
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
citrusy	citrus	k1gInPc1	citrus
či	či	k8xC	či
čajovník	čajovník	k1gInSc1	čajovník
<g/>
.	.	kIx.	.
<g/>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
země	zem	k1gFnSc2	zem
klesá	klesat	k5eAaImIp3nS	klesat
podíl	podíl	k1gInSc4	podíl
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
suché	suchý	k2eAgInPc1d1	suchý
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
zde	zde	k6eAd1	zde
převládat	převládat	k5eAaImF	převládat
kontinentální	kontinentální	k2eAgNnSc4d1	kontinentální
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Rioni	Rioň	k1gMnPc7	Rioň
a	a	k8xC	a
Kury	kur	k1gMnPc7	kur
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
mrazivé	mrazivý	k2eAgFnPc1d1	mrazivá
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
teplá	teplý	k2eAgNnPc4d1	teplé
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
různorodosti	různorodost	k1gFnSc3	různorodost
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
malé	malý	k2eAgFnSc6d1	malá
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
obývá	obývat	k5eAaImIp3nS	obývat
Gruzii	Gruzie	k1gFnSc4	Gruzie
na	na	k7c4	na
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
360	[number]	k4	360
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
4	[number]	k4	4
druhy	druh	k1gInPc4	druh
supů	sup	k1gMnPc2	sup
<g/>
,	,	kIx,	,
11	[number]	k4	11
druhů	druh	k1gInPc2	druh
orlů	orel	k1gMnPc2	orel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
160	[number]	k4	160
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
48	[number]	k4	48
druhů	druh	k1gInPc2	druh
plazů	plaz	k1gInPc2	plaz
a	a	k8xC	a
11	[number]	k4	11
druhů	druh	k1gInPc2	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
obývány	obývat	k5eAaImNgInP	obývat
několika	několik	k4yIc7	několik
druhy	druh	k1gInPc4	druh
masožravců	masožravec	k1gMnPc2	masožravec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
rys	rys	k1gMnSc1	rys
<g/>
,	,	kIx,	,
šakal	šakal	k1gMnSc1	šakal
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
hyena	hyena	k1gFnSc1	hyena
žíhaná	žíhaný	k2eAgFnSc1d1	žíhaná
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
a	a	k8xC	a
několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
levharta	levhart	k1gMnSc2	levhart
perského	perský	k2eAgMnSc2d1	perský
(	(	kIx(	(
<g/>
Panthera	Panther	k1gMnSc2	Panther
pardus	pardus	k1gInSc1	pardus
ciscaucasica	ciscaucasica	k6eAd1	ciscaucasica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatní	ostatní	k2eAgFnSc2d1	ostatní
velké	velký	k2eAgFnSc2d1	velká
zvěře	zvěř	k1gFnSc2	zvěř
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
kamzík	kamzík	k1gMnSc1	kamzík
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
,	,	kIx,	,
koza	koza	k1gFnSc1	koza
bezoárová	bezoárový	k2eAgFnSc1d1	bezoárová
<g/>
.	.	kIx.	.
</s>
<s>
Endemicky	endemicky	k6eAd1	endemicky
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
domovem	domov	k1gInSc7	domov
kozorožec	kozorožec	k1gMnSc1	kozorožec
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
<g/>
,	,	kIx,	,
kozorožec	kozorožec	k1gMnSc1	kozorožec
dagestánský	dagestánský	k2eAgMnSc1d1	dagestánský
a	a	k8xC	a
tetřívek	tetřívek	k1gMnSc1	tetřívek
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k1gMnSc1	Tetrao
mlokosieviczi	mlokosievicze	k1gFnSc4	mlokosievicze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dravců	dravec	k1gMnPc2	dravec
jsou	být	k5eAaImIp3nP	být
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
orel	orel	k1gMnSc1	orel
skalní	skalní	k2eAgMnPc1d1	skalní
a	a	k8xC	a
orlosup	orlosup	k1gMnSc1	orlosup
bradatý	bradatý	k2eAgMnSc1d1	bradatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bažant	Bažant	k1gMnSc1	Bažant
obecný	obecný	k2eAgMnSc1d1	obecný
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
původní	původní	k2eAgMnSc1d1	původní
endemický	endemický	k2eAgMnSc1d1	endemický
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
Gruzie	Gruzie	k1gFnSc2	Gruzie
uměle	uměle	k6eAd1	uměle
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rozšířených	rozšířený	k2eAgInPc2d1	rozšířený
bezobratlých	bezobratlý	k2eAgInPc2d1	bezobratlý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
též	též	k9	též
velmi	velmi	k6eAd1	velmi
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
soupis	soupis	k1gInSc1	soupis
představuje	představovat	k5eAaImIp3nS	představovat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
hledání	hledání	k1gNnSc4	hledání
napříč	napříč	k7c7	napříč
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
známí	známý	k2eAgMnPc1d1	známý
pavouci	pavouk	k1gMnPc1	pavouk
čítají	čítat	k5eAaImIp3nP	čítat
501	[number]	k4	501
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flora	Flora	k1gFnSc1	Flora
===	===	k?	===
</s>
</p>
<p>
<s>
44	[number]	k4	44
procent	procent	k1gInSc4	procent
země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
<g/>
%	%	kIx~	%
činí	činit	k5eAaImIp3nS	činit
pralesy	prales	k1gInPc4	prales
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
lesů	les	k1gInPc2	les
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
původní	původní	k2eAgFnSc4d1	původní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
roste	růst	k5eAaImIp3nS	růst
převážně	převážně	k6eAd1	převážně
listnatý	listnatý	k2eAgInSc1d1	listnatý
les	les	k1gInSc1	les
(	(	kIx(	(
<g/>
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
buk	buk	k1gInSc1	buk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
pak	pak	k6eAd1	pak
jehličnatý	jehličnatý	k2eAgInSc4d1	jehličnatý
les	les	k1gInSc4	les
(	(	kIx(	(
<g/>
smrk	smrk	k1gInSc1	smrk
a	a	k8xC	a
jedle	jedle	k1gFnPc1	jedle
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
jedle	jedle	k6eAd1	jedle
kavkazská	kavkazský	k2eAgFnSc1d1	kavkazská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
subalpské	subalpský	k2eAgFnPc1d1	subalpský
a	a	k8xC	a
alpské	alpský	k2eAgFnPc1d1	alpská
louky	louka	k1gFnPc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nížiny	nížina	k1gFnSc2	nížina
a	a	k8xC	a
transkavkazská	transkavkazský	k2eAgFnSc1d1	Transkavkazská
sníženina	sníženina	k1gFnSc1	sníženina
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
tvořena	tvořit	k5eAaImNgFnS	tvořit
stepí	step	k1gFnSc7	step
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kultivovaná	kultivovaný	k2eAgFnSc1d1	kultivovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
převážně	převážně	k6eAd1	převážně
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc4	brambor
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
i	i	k9	i
na	na	k7c4	na
chov	chov	k1gInSc4	chov
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
sektoru	sektor	k1gInSc6	sektor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převažuje	převažovat	k5eAaImIp3nS	převažovat
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
nad	nad	k7c7	nad
strojírenstvím	strojírenství	k1gNnSc7	strojírenství
a	a	k8xC	a
automobilovým	automobilový	k2eAgInSc7d1	automobilový
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tbilisi	Tbilisi	k1gNnSc2	Tbilisi
či	či	k8xC	či
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kutaisi	Kutaise	k1gFnSc4	Kutaise
a	a	k8xC	a
Batumi	Batumi	k1gNnSc4	Batumi
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Gruzie	Gruzie	k1gFnSc1	Gruzie
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
hornickou	hornický	k2eAgFnSc7d1	hornická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
bohaté	bohatý	k2eAgFnPc4d1	bohatá
zásoby	zásoba	k1gFnPc4	zásoba
železných	železný	k2eAgInPc2d1	železný
a	a	k8xC	a
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
exportuje	exportovat	k5eAaBmIp3nS	exportovat
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
manganu	mangan	k1gInSc2	mangan
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Čiatury	Čiatura	k1gFnSc2	Čiatura
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
malá	malý	k2eAgNnPc1d1	malé
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Gruzie	Gruzie	k1gFnSc1	Gruzie
není	být	k5eNaImIp3nS	být
energeticky	energeticky	k6eAd1	energeticky
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
většinu	většina	k1gFnSc4	většina
paliv	palivo	k1gNnPc2	palivo
dovážet	dovážet	k5eAaImF	dovážet
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
<s>
Rafinace	rafinace	k1gFnPc1	rafinace
a	a	k8xC	a
úpravy	úprava	k1gFnPc1	úprava
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
rafinériích	rafinérie	k1gFnPc6	rafinérie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Gruzii	Gruzie	k1gFnSc4	Gruzie
vedou	vést	k5eAaImIp3nP	vést
dva	dva	k4xCgInPc1	dva
význammné	význammný	k2eAgInPc1d1	význammný
ropovody	ropovod	k1gInPc1	ropovod
(	(	kIx(	(
<g/>
BTC	BTC	kA	BTC
a	a	k8xC	a
Baku-Supsa	Baku-Supsa	k1gFnSc1	Baku-Supsa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jihokavkazský	Jihokavkazský	k2eAgInSc1d1	Jihokavkazský
plynovod	plynovod	k1gInSc1	plynovod
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc1d1	spojující
oblast	oblast	k1gFnSc1	oblast
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Urbanizace	urbanizace	k1gFnSc1	urbanizace
<g/>
:	:	kIx,	:
56,5	[number]	k4	56,5
%	%	kIx~	%
</s>
</p>
<p>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
14,37	[number]	k4	14,37
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
3	[number]	k4	3
215	[number]	k4	215
USD	USD	kA	USD
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
(	(	kIx(	(
<g/>
45	[number]	k4	45
%	%	kIx~	%
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
23	[number]	k4	23
%	%	kIx~	%
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
:	:	kIx,	:
16,3	[number]	k4	16,3
%	%	kIx~	%
</s>
</p>
<p>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
<g/>
:	:	kIx,	:
7	[number]	k4	7
%	%	kIx~	%
</s>
</p>
<p>
<s>
Inflace	inflace	k1gFnSc1	inflace
<g/>
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2,8	[number]	k4	2,8
%	%	kIx~	%
</s>
</p>
<p>
<s>
Veřejný	veřejný	k2eAgInSc1d1	veřejný
dluh	dluh	k1gInSc1	dluh
<g/>
:	:	kIx,	:
11,2	[number]	k4	11,2
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
<g/>
:	:	kIx,	:
3,67	[number]	k4	3,67
miliard	miliarda	k4xCgFnPc2	miliarda
USDOd	USDOda	k1gFnPc2	USDOda
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
funguje	fungovat	k5eAaImIp3nS	fungovat
mezi	mezi	k7c7	mezi
Gruzií	Gruzie	k1gFnSc7	Gruzie
a	a	k8xC	a
státy	stát	k1gInPc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
bezvízový	bezvízový	k2eAgInSc1d1	bezvízový
styk	styk	k1gInSc1	styk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
do	do	k7c2	do
Gruzie	Gruzie	k1gFnSc2	Gruzie
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
státům	stát	k1gInPc3	stát
exportujícím	exportující	k2eAgInPc3d1	exportující
do	do	k7c2	do
Gruzie	Gruzie	k1gFnSc2	Gruzie
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pohrozeno	pohrodit	k5eAaPmNgNnS	pohrodit
sankcemi	sankce	k1gFnPc7	sankce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Gruzie	Gruzie	k1gFnSc2	Gruzie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činil	činit	k5eAaImAgMnS	činit
odhadem	odhad	k1gInSc7	odhad
3	[number]	k4	3
729	[number]	k4	729
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Demografická	demografický	k2eAgFnSc1d1	demografická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jiných	jiná	k1gFnPc6	jiná
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sovětských	sovětský	k2eAgFnPc6d1	sovětská
republikách	republika	k1gFnPc6	republika
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
a	a	k8xC	a
Lotyšssku	Lotyšssko	k1gNnSc6	Lotyšssko
<g/>
)	)	kIx)	)
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
dvěma	dva	k4xCgInPc7	dva
hlavními	hlavní	k2eAgInPc7d1	hlavní
trendy	trend	k1gInPc7	trend
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
–	–	k?	–
úbytek	úbytek	k1gInSc1	úbytek
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
"	"	kIx"	"
<g/>
gruzínifikace	gruzínifikace	k1gFnSc2	gruzínifikace
<g/>
"	"	kIx"	"
etnického	etnický	k2eAgNnSc2d1	etnické
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
etnických	etnický	k2eAgMnPc2d1	etnický
Gruzínců	Gruzínec	k1gMnPc2	Gruzínec
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
2002	[number]	k4	2002
o	o	k7c4	o
deset	deset	k4xCc4	deset
procent	procento	k1gNnPc2	procento
–	–	k?	–
z	z	k7c2	z
73,7	[number]	k4	73,7
%	%	kIx~	%
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
83,7	[number]	k4	83,7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Gruzie	Gruzie	k1gFnSc1	Gruzie
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
strmě	strmě	k6eAd1	strmě
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
trend	trend	k1gInSc1	trend
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
4,5	[number]	k4	4,5
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
celkovou	celkový	k2eAgFnSc4d1	celková
populaci	populace	k1gFnSc4	populace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgInPc2	dva
separatistických	separatistický	k2eAgInPc2d1	separatistický
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Abcházie	Abcházie	k1gFnSc1	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Osetie	Osetie	k1gFnSc1	Osetie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
178	[number]	k4	178
000	[number]	k4	000
a	a	k8xC	a
49	[number]	k4	49
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
Abcházie	Abcházie	k1gFnSc2	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
měla	mít	k5eAaImAgFnS	mít
Gruzie	Gruzie	k1gFnSc1	Gruzie
4	[number]	k4	4
321	[number]	k4	321
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
4	[number]	k4	4
382	[number]	k4	382
100	[number]	k4	100
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
přišla	přijít	k5eAaPmAgFnS	přijít
Gruzie	Gruzie	k1gFnSc1	Gruzie
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
kvůli	kvůli	k7c3	kvůli
migraci	migrace	k1gFnSc3	migrace
o	o	k7c4	o
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
emigrací	emigrace	k1gFnSc7	emigrace
lidí	člověk	k1gMnPc2	člověk
hledajících	hledající	k2eAgNnPc2d1	hledající
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
a	a	k8xC	a
snížením	snížení	k1gNnSc7	snížení
porodnosti	porodnost	k1gFnSc2	porodnost
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
přišla	přijít	k5eAaPmAgFnS	přijít
Gruzie	Gruzie	k1gFnSc1	Gruzie
o	o	k7c4	o
14,7	[number]	k4	14,7
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
<g/>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Gruzie	Gruzie	k1gFnSc2	Gruzie
jsou	být	k5eAaImIp3nP	být
Gruzíni	Gruzín	k1gMnPc1	Gruzín
(	(	kIx(	(
<g/>
83,8	[number]	k4	83,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Azerové	Azerové	k2eAgFnSc1d1	Azerové
(	(	kIx(	(
<g/>
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
(	(	kIx(	(
<g/>
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oseti	Oset	k1gMnPc1	Oset
(	(	kIx(	(
<g/>
0,9	[number]	k4	0,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Abchazové	Abchaz	k1gMnPc1	Abchaz
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kartvelské	kartvelský	k2eAgFnSc2d1	kartvelský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
vlastních	vlastní	k2eAgMnPc2d1	vlastní
Gruzínů	Gruzín	k1gMnPc2	Gruzín
také	také	k9	také
Adžarové	Adžar	k1gMnPc1	Adžar
<g/>
,	,	kIx,	,
Lazové	Lazus	k1gMnPc1	Lazus
<g/>
,	,	kIx,	,
Svanové	Svan	k1gMnPc1	Svan
<g/>
,	,	kIx,	,
Mingrelové	Mingrel	k1gMnPc1	Mingrel
<g/>
,	,	kIx,	,
Tušetové	Tušet	k1gMnPc1	Tušet
či	či	k8xC	či
Kachetové	Kachet	k1gMnPc1	Kachet
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
etnické	etnický	k2eAgFnPc4d1	etnická
podskupiny	podskupina	k1gFnPc4	podskupina
Gruzínců	Gruzínec	k1gMnPc2	Gruzínec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
příslušníků	příslušník	k1gMnPc2	příslušník
má	mít	k5eAaImIp3nS	mít
Gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
82,1	[number]	k4	82,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Islám	islám	k1gInSc1	islám
9,9	[number]	k4	9,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
Arménská	arménský	k2eAgFnSc1d1	arménská
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
katolictví	katolictví	k1gNnSc1	katolictví
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
1,4	[number]	k4	1,4
%	%	kIx~	%
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
existuje	existovat	k5eAaImIp3nS	existovat
originální	originální	k2eAgNnSc4d1	originální
místní	místní	k2eAgNnSc4d1	místní
náboženství	náboženství	k1gNnSc4	náboženství
džvarismus	džvarismus	k1gInSc4	džvarismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Básník	básník	k1gMnSc1	básník
Šota	Šotum	k1gNnSc2	Šotum
Rustaveli	Rustavel	k1gInSc6	Rustavel
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
raně	raně	k6eAd1	raně
středověkého	středověký	k2eAgInSc2d1	středověký
národního	národní	k2eAgInSc2d1	národní
eposu	epos	k1gInSc2	epos
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
tygří	tygří	k2eAgFnSc6d1	tygří
kůži	kůže	k1gFnSc6	kůže
(	(	kIx(	(
<g/>
Vepkhist	Vepkhist	k1gInSc1	Vepkhist
<g/>
'	'	kIx"	'
<g/>
q	q	k?	q
<g/>
'	'	kIx"	'
<g/>
aosani	aosan	k1gMnPc1	aosan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
zakladatelskému	zakladatelský	k2eAgNnSc3d1	zakladatelské
dílu	dílo	k1gNnSc3	dílo
se	se	k3xPyFc4	se
vraceli	vracet	k5eAaImAgMnP	vracet
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
obrozenci	obrozenec	k1gMnPc1	obrozenec
jako	jako	k8xC	jako
Ilja	Ilja	k1gMnSc1	Ilja
Čavčavadze	Čavčavadze	k1gFnSc1	Čavčavadze
<g/>
,	,	kIx,	,
Važa	Važ	k2eAgFnSc1d1	Važ
Pšavela	Pšavela	k1gFnSc1	Pšavela
<g/>
,	,	kIx,	,
Akaki	Akaki	k1gNnSc2	Akaki
Cereteli	Ceretel	k1gInSc6	Ceretel
nebo	nebo	k8xC	nebo
Nikoloz	Nikoloz	k1gMnSc1	Nikoloz
Baratašvili	Baratašvili	k1gMnPc1	Baratašvili
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
gruzínskou	gruzínský	k2eAgFnSc4d1	gruzínská
literaturu	literatura	k1gFnSc4	literatura
vzkřísili	vzkřísit	k5eAaPmAgMnP	vzkřísit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc1	základ
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
prózy	próza	k1gFnSc2	próza
Alexander	Alexandra	k1gFnPc2	Alexandra
Kazbegi	Kazbeg	k1gFnSc2	Kazbeg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgMnSc7d1	významný
autorem	autor	k1gMnSc7	autor
básník	básník	k1gMnSc1	básník
Galaktion	Galaktion	k1gInSc4	Galaktion
Tabidze	Tabidze	k1gFnSc2	Tabidze
<g/>
.	.	kIx.	.
</s>
<s>
Obětí	oběť	k1gFnSc7	oběť
Stalinových	Stalinových	k2eAgFnPc2d1	Stalinových
čistek	čistka	k1gFnPc2	čistka
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prozaik	prozaik	k1gMnSc1	prozaik
Micheil	Micheil	k1gMnSc1	Micheil
Džavachišvili	Džavachišvili	k1gMnSc1	Džavachišvili
<g/>
.	.	kIx.	.
</s>
<s>
Bardem	bard	k1gMnSc7	bard
Velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
stal	stát	k5eAaPmAgInS	stát
básník	básník	k1gMnSc1	básník
Grigol	Grigola	k1gFnPc2	Grigola
Abašidze	Abašidze	k1gFnSc2	Abašidze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
Nodar	Nodar	k1gInSc1	Nodar
Dumbadze	Dumbadze	k1gFnSc2	Dumbadze
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Othar	Othar	k1gInSc4	Othar
Čiladze	Čiladze	k1gFnSc1	Čiladze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
gruzínským	gruzínský	k2eAgMnSc7d1	gruzínský
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
je	být	k5eAaImIp3nS	být
Giya	Giya	k1gFnSc1	Giya
Kancheli	Kanchel	k1gInSc3	Kanchel
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Zakaria	Zakarium	k1gNnSc2	Zakarium
Paliašvili	Paliašvili	k1gFnSc2	Paliašvili
je	být	k5eAaImIp3nS	být
autor	autor	k1gMnSc1	autor
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
baletním	baletní	k2eAgMnSc7d1	baletní
choreografem	choreograf	k1gMnSc7	choreograf
byl	být	k5eAaImAgMnS	být
George	Georg	k1gMnSc4	Georg
Balanchine	Balanchin	k1gMnSc5	Balanchin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
věhlas	věhlas	k1gInSc4	věhlas
baletka	baletka	k1gFnSc1	baletka
Nina	Nina	k1gFnSc1	Nina
Ananiašvili	Ananiašvili	k1gFnSc1	Ananiašvili
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sovětském	sovětský	k2eAgInSc6d1	sovětský
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
filmoví	filmový	k2eAgMnPc1d1	filmový
režiséři	režisér	k1gMnPc1	režisér
Tengiz	Tengiza	k1gFnPc2	Tengiza
Abuladze	Abuladze	k1gFnSc2	Abuladze
<g/>
,	,	kIx,	,
Vachtang	Vachtang	k1gMnSc1	Vachtang
Kikabidze	Kikabidze	k1gFnSc2	Kikabidze
a	a	k8xC	a
Georgij	Georgij	k1gFnSc2	Georgij
Danělija	Danělij	k1gInSc2	Danělij
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
působil	působit	k5eAaImAgMnS	působit
režisér	režisér	k1gMnSc1	režisér
Otar	Otar	k1gMnSc1	Otar
Ioseliani	Ioselian	k1gMnPc1	Ioselian
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
gruzínskou	gruzínský	k2eAgFnSc7d1	gruzínská
fotografkou	fotografka	k1gFnSc7	fotografka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
patrně	patrně	k6eAd1	patrně
Salome	Salom	k1gInSc5	Salom
Vatzadze	Vatzadze	k1gFnSc1	Vatzadze
tvořící	tvořící	k2eAgFnPc1d1	tvořící
zejména	zejména	k9	zejména
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
gruzínským	gruzínský	k2eAgMnSc7d1	gruzínský
malířem	malíř	k1gMnSc7	malíř
je	být	k5eAaImIp3nS	být
Niko	nika	k1gFnSc5	nika
Pirosmani	Pirosman	k1gMnPc1	Pirosman
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
naivního	naivní	k2eAgNnSc2d1	naivní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
sochařem	sochař	k1gMnSc7	sochař
pak	pak	k8xC	pak
Zurab	Zurab	k1gInSc1	Zurab
Cereteli	Ceretel	k1gInSc6	Ceretel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
je	být	k5eAaImIp3nS	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Gruzie	Gruzie	k1gFnSc1	Gruzie
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
především	především	k9	především
v	v	k7c6	v
bojových	bojový	k2eAgInPc6d1	bojový
a	a	k8xC	a
silových	silový	k2eAgInPc6d1	silový
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
získal	získat	k5eAaPmAgMnS	získat
Laša	Laša	k1gMnSc1	Laša
Šavdatuašvili	Šavdatuašvili	k1gMnSc1	Šavdatuašvili
<g/>
,	,	kIx,	,
Zurab	Zurab	k1gMnSc1	Zurab
Zviadauri	Zviadaur	k1gFnSc2	Zviadaur
a	a	k8xC	a
Irakli	Irakli	k1gFnSc2	Irakli
Cirekidze	Cirekidze	k1gFnSc2	Cirekidze
<g/>
,	,	kIx,	,
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Vladimer	Vladimer	k1gMnSc1	Vladimer
Chinčegašvili	Chinčegašvili	k1gMnSc1	Chinčegašvili
<g/>
,	,	kIx,	,
Manučar	Manučar	k1gMnSc1	Manučar
Kvirkvelia	Kvirkvelia	k1gFnSc1	Kvirkvelia
a	a	k8xC	a
Revaz	Revaz	k1gInSc1	Revaz
Mindorašvili	Mindorašvili	k1gFnSc2	Mindorašvili
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc4	zlato
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
vzpěrači	vzpěrač	k1gMnPc1	vzpěrač
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Giorgi	Giorgi	k1gNnSc4	Giorgi
Asanidze	Asanidze	k1gFnSc2	Asanidze
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Laša	Laš	k2eAgFnSc1d1	Laš
Talachadze	Talachadze	k1gFnSc1	Talachadze
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
časech	čas	k1gInPc6	čas
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
zlato	zlato	k1gNnSc4	zlato
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
vysokém	vysoký	k2eAgInSc6d1	vysoký
přivezl	přivézt	k5eAaPmAgMnS	přivézt
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
olympiády	olympiáda	k1gFnSc2	olympiáda
Robert	Robert	k1gMnSc1	Robert
Šavlakadze	Šavlakadze	k1gFnSc1	Šavlakadze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
na	na	k7c4	na
OH	OH	kA	OH
není	být	k5eNaImIp3nS	být
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
starou	starý	k2eAgFnSc4d1	stará
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasický	klasický	k2eAgInSc1d1	klasický
řecko-římský	řecko-římský	k2eAgInSc1d1	řecko-římský
zápas	zápas	k1gInSc1	zápas
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
časech	čas	k1gInPc6	čas
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
řadou	řada	k1gFnSc7	řada
gruzínských	gruzínský	k2eAgInPc2d1	gruzínský
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
římští	římský	k2eAgMnPc1d1	římský
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
také	také	k9	také
dosti	dosti	k6eAd1	dosti
populární	populární	k2eAgInSc1d1	populární
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
ve	v	k7c6	v
významných	významný	k2eAgFnPc6d1	významná
ligách	liga	k1gFnPc6	liga
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Tornike	Tornike	k1gFnSc1	Tornike
Šengelia	Šengelia	k1gFnSc1	Šengelia
<g/>
,	,	kIx,	,
Nikoloz	Nikoloz	k1gInSc1	Nikoloz
Ckitišvili	Ckitišvili	k1gFnSc2	Ckitišvili
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
Zaza	Zaza	k1gMnSc1	Zaza
Pačulija	Pačulija	k1gMnSc1	Pačulija
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
NBA	NBA	kA	NBA
s	s	k7c7	s
Golden	Goldno	k1gNnPc2	Goldno
State	status	k1gInSc5	status
Warriors	Warriors	k1gInSc1	Warriors
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gMnPc2	jeho
reprezentantů	reprezentant	k1gMnPc2	reprezentant
v	v	k7c6	v
košíkové	košíková	k1gFnSc6	košíková
Gruzínci	Gruzínec	k1gMnPc1	Gruzínec
-	-	kIx~	-
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
Otar	Otar	k1gMnSc1	Otar
Korkia	Korkia	k1gFnSc1	Korkia
či	či	k8xC	či
olympijští	olympijský	k2eAgMnPc1d1	olympijský
vítězi	vítěz	k1gMnSc6	vítěz
Micheil	Micheil	k1gInSc4	Micheil
Korkia	Korkium	k1gNnSc2	Korkium
a	a	k8xC	a
Zurab	Zuraba	k1gFnPc2	Zuraba
Sakandelidze	Sakandelidze	k1gFnSc2	Sakandelidze
<g/>
.	.	kIx.	.
</s>
<s>
Dinamo	Dinamo	k6eAd1	Dinamo
Tbilisi	Tbilisi	k1gNnSc1	Tbilisi
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
nejprestižnější	prestižní	k2eAgFnSc4d3	nejprestižnější
klubovou	klubový	k2eAgFnSc4d1	klubová
basketbalovou	basketbalový	k2eAgFnSc4d1	basketbalová
soutěž	soutěž	k1gFnSc4	soutěž
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
Pohár	pohár	k1gInSc4	pohár
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Gruzie	Gruzie	k1gFnSc1	Gruzie
zatím	zatím	k6eAd1	zatím
výraznějšího	výrazný	k2eAgInSc2d2	výraznější
úspěchu	úspěch	k1gInSc2	úspěch
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
měla	mít	k5eAaImAgFnS	mít
silnou	silný	k2eAgFnSc4d1	silná
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
-	-	kIx~	-
Dinamo	Dinama	k1gFnSc5	Dinama
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1980	[number]	k4	1980
<g/>
/	/	kIx~	/
<g/>
1981	[number]	k4	1981
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
osobnostem	osobnost	k1gFnPc3	osobnost
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
patřil	patřit	k5eAaImAgMnS	patřit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Čivadze	Čivadze	k1gFnSc2	Čivadze
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
fotbalista	fotbalista	k1gMnSc1	fotbalista
SSSR	SSSR	kA	SSSR
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ramaz	Ramaz	k1gInSc1	Ramaz
Šengelia	Šengelia	k1gFnSc1	Šengelia
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
fotbalista	fotbalista	k1gMnSc1	fotbalista
SSSR	SSSR	kA	SSSR
1978	[number]	k4	1978
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Micheil	Micheil	k1gMnSc1	Micheil
Meschi	Meschi	k1gNnSc2	Meschi
<g/>
,	,	kIx,	,
Slava	Slavo	k1gNnSc2	Slavo
Metreveli	Metrevel	k1gInSc6	Metrevel
a	a	k8xC	a
Givi	Giv	k1gFnPc4	Giv
Čocheli	Čochel	k1gInPc7	Čochel
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
mistři	mistr	k1gMnPc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revaz	Revaz	k1gInSc1	Revaz
Dzodzuašvili	Dzodzuašvili	k1gFnSc1	Dzodzuašvili
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
či	či	k8xC	či
Murtaz	Murtaz	k1gInSc1	Murtaz
Churčilava	Churčilava	k1gFnSc1	Churčilava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
gruzínským	gruzínský	k2eAgMnSc7d1	gruzínský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
posledních	poslední	k2eAgNnPc2d1	poslední
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legendárním	legendární	k2eAgMnSc7d1	legendární
šachistou	šachista	k1gMnSc7	šachista
byl	být	k5eAaImAgMnS	být
Tigran	Tigran	k1gInSc4	Tigran
Petrosjan	Petrosjan	k1gMnSc1	Petrosjan
<g/>
.	.	kIx.	.
</s>
<s>
Finalistou	finalista	k1gMnSc7	finalista
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
byl	být	k5eAaImAgMnS	být
tenista	tenista	k1gMnSc1	tenista
Alex	Alex	k1gMnSc1	Alex
Metreveli	Metrevel	k1gInSc3	Metrevel
<g/>
,	,	kIx,	,
zisk	zisk	k1gInSc4	zisk
titulu	titul	k1gInSc2	titul
mu	on	k3xPp3gMnSc3	on
zhatil	zhatit	k5eAaPmAgMnS	zhatit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
Jan	Jan	k1gMnSc1	Jan
Kodeš	Kodeš	k1gMnSc1	Kodeš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
také	také	k9	také
rugby	rugby	k1gNnSc1	rugby
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc7d1	místní
lidovou	lidový	k2eAgFnSc7d1	lidová
hrou	hra	k1gFnSc7	hra
podobnou	podobný	k2eAgFnSc7d1	podobná
rugby	rugby	k1gNnSc4	rugby
je	být	k5eAaImIp3nS	být
lelo	lelo	k6eAd1	lelo
burti	burt	k5eAaBmF	burt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
gruzínským	gruzínský	k2eAgMnPc3d1	gruzínský
vědcům	vědec	k1gMnPc3	vědec
patří	patřit	k5eAaImIp3nS	patřit
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
lingvista	lingvista	k1gMnSc1	lingvista
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Marr	Marr	k1gMnSc1	Marr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Georgien	Georgina	k1gFnPc2	Georgina
<g/>
#	#	kIx~	#
<g/>
Flora	Flora	k1gFnSc1	Flora
und	und	k?	und
Fauna	fauna	k1gFnSc1	fauna
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Г	Г	k?	Г
с	с	k?	с
с	с	k?	с
р	р	k?	р
<g/>
,	,	kIx,	,
В	В	k?	В
в	в	k?	в
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Gagra	Gagra	k6eAd1	Gagra
</s>
</p>
<p>
<s>
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Gruzie	Gruzie	k1gFnSc2	Gruzie
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
</s>
</p>
<p>
<s>
Abcházie	Abcházie	k1gFnSc1	Abcházie
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Osetie	Osetie	k1gFnSc1	Osetie
</s>
</p>
<p>
<s>
Adžárie	Adžárie	k1gFnSc1	Adžárie
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
revoluce	revoluce	k1gFnSc1	revoluce
</s>
</p>
<p>
<s>
Barevné	barevný	k2eAgFnPc1d1	barevná
revoluce	revoluce	k1gFnPc1	revoluce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Gruzie	Gruzie	k1gFnSc2	Gruzie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Gruzie	Gruzie	k1gFnSc2	Gruzie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gruzie	Gruzie	k1gFnSc2	Gruzie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Gruzie	Gruzie	k1gFnSc2	Gruzie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Gruzie	Gruzie	k1gFnSc2	Gruzie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gNnSc1	plus
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zbavil	zbavit	k5eAaPmAgMnS	zbavit
Evropu	Evropa	k1gFnSc4	Evropa
rovnováhy	rovnováha	k1gFnSc2	rovnováha
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
podrobně	podrobně	k6eAd1	podrobně
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
okolnostmi	okolnost	k1gFnPc7	okolnost
politického	politický	k2eAgInSc2d1	politický
vzestupu	vzestup	k1gInSc2	vzestup
i	i	k9	i
působení	působení	k1gNnSc1	působení
gruzínského	gruzínský	k2eAgMnSc2d1	gruzínský
politika	politik	k1gMnSc2	politik
Eduarda	Eduard	k1gMnSc2	Eduard
Ševardnadzeho	Ševardnadze	k1gMnSc2	Ševardnadze
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Georgia	Georgia	k1gFnSc1	Georgia
TVNews	TVNewsa	k1gFnPc2	TVNewsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Georgia	Georgia	k1gFnSc1	Georgia
–	–	k?	–
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Georgia	Georgia	k1gFnSc1	Georgia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Georgia	Georgius	k1gMnSc2	Georgius
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Georgia	Georgia	k1gFnSc1	Georgia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-01-28	[number]	k4	2011-01-28
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
–	–	k?	–
Georgia	Georgia	k1gFnSc1	Georgia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Gruzie	Gruzie	k1gFnSc1	Gruzie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2009-11-07	[number]	k4	2009-11-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DJIBLADZE	DJIBLADZE	kA	DJIBLADZE
<g/>
,	,	kIx,	,
Mikhail	Mikhail	k1gMnSc1	Mikhail
Leonidovich	Leonidovich	k1gMnSc1	Leonidovich
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Georgia	Georgia	k1gFnSc1	Georgia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
