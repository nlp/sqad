<s>
Philip	Philip	k1gMnSc1	Philip
Kindred	Kindred	k1gMnSc1	Kindred
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gInSc1	Illinois
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Ana	Ana	k1gFnSc1	Ana
<g/>
,	,	kIx,	,
Orange	Orange	k1gFnSc1	Orange
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
stylem	styl	k1gInSc7	styl
psaní	psaní	k1gNnSc2	psaní
upoutal	upoutat	k5eAaPmAgMnS	upoutat
nesčetné	sčetný	k2eNgNnSc1d1	nesčetné
množství	množství	k1gNnSc1	množství
čtenářů	čtenář	k1gMnPc2	čtenář
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgNnP	stát
doslova	doslova	k6eAd1	doslova
kultovními	kultovní	k2eAgFnPc7d1	kultovní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
opakují	opakovat	k5eAaImIp3nP	opakovat
témata	téma	k1gNnPc1	téma
rozdvojené	rozdvojený	k2eAgFnSc2d1	rozdvojená
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
nejednotné	jednotný	k2eNgFnSc2d1	nejednotná
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
postav	postava	k1gFnPc2	postava
závislých	závislý	k2eAgFnPc2d1	závislá
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
lidství	lidství	k1gNnSc4	lidství
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
předčasně	předčasně	k6eAd1	předčasně
o	o	k7c4	o
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
dvojče	dvojče	k1gNnSc1	dvojče
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Jane	Jan	k1gMnSc5	Jan
Charlotte	Charlott	k1gMnSc5	Charlott
<g/>
,	,	kIx,	,
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
zemřela	zemřít	k5eAaPmAgNnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
matka	matka	k1gFnSc1	matka
po	po	k7c6	po
letech	let	k1gInPc6	let
svěřila	svěřit	k5eAaPmAgFnS	svěřit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestra	sestra	k1gFnSc1	sestra
umřela	umřít	k5eAaPmAgFnS	umřít
následkem	následkem	k7c2	následkem
nedostatečné	nedostatečná	k1gFnSc2	nedostatečná
mateřské	mateřský	k2eAgFnSc2d1	mateřská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Dick	Dick	k1gMnSc1	Dick
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
matka	matka	k1gFnSc1	matka
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
to	ten	k3xDgNnSc1	ten
nepravé	pravý	k2eNgNnSc1d1	nepravé
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
San	San	k1gFnSc2	San
Franciska	Francisko	k1gNnSc2	Francisko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
bojovali	bojovat	k5eAaImAgMnP	bojovat
o	o	k7c4	o
svěření	svěření	k1gNnSc4	svěření
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
kvakerskou	kvakerský	k2eAgFnSc4d1	kvakerská
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
základní	základní	k2eAgFnSc4d1	základní
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
vydával	vydávat	k5eAaPmAgInS	vydávat
vlastní	vlastní	k2eAgInSc1d1	vlastní
časopis	časopis	k1gInSc1	časopis
Daily	Daila	k1gFnSc2	Daila
Dick	Dicka	k1gFnPc2	Dicka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náruživým	náruživý	k2eAgMnSc7d1	náruživý
čtenářem	čtenář	k1gMnSc7	čtenář
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
se	se	k3xPyFc4	se
Return	Return	k1gMnSc1	Return
to	ten	k3xDgNnSc1	ten
Liliput	Liliput	k1gInSc1	Liliput
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
první	první	k4xOgFnSc1	první
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Le	Le	k1gMnSc1	Le
Diable	Diable	k1gMnSc1	Diable
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c4	v
Berkeley	Berkele	k2eAgInPc4d1	Berkele
Daily	Dail	k1gInPc4	Dail
Gazette	Gazett	k1gInSc5	Gazett
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
působil	působit	k5eAaImAgMnS	působit
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
brzy	brzy	k6eAd1	brzy
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
projevily	projevit	k5eAaPmAgInP	projevit
psychické	psychický	k2eAgInPc1d1	psychický
problémy	problém	k1gInPc1	problém
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
závratěmi	závrať	k1gFnPc7	závrať
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
střídavým	střídavý	k2eAgInSc7d1	střídavý
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
a	a	k8xC	a
otevřených	otevřený	k2eAgFnPc2d1	otevřená
prostor	prostora	k1gFnPc2	prostora
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
diagnostikované	diagnostikovaný	k2eAgFnSc2d1	diagnostikovaná
jako	jako	k8xS	jako
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
nemoci	nemoc	k1gFnSc3	nemoc
se	se	k3xPyFc4	se
již	již	k6eAd1	již
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
nezbavil	zbavit	k5eNaPmAgMnS	zbavit
a	a	k8xC	a
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
nesčetné	sčetný	k2eNgNnSc1d1	nesčetné
množství	množství	k1gNnSc1	množství
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
také	také	k9	také
režíroval	režírovat	k5eAaImAgMnS	režírovat
pořad	pořad	k1gInSc4	pořad
o	o	k7c6	o
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
pro	pro	k7c4	pro
kalifornské	kalifornský	k2eAgNnSc4d1	kalifornské
rádio	rádio	k1gNnSc4	rádio
KSMO	KSMO	kA	KSMO
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
krevnímu	krevní	k2eAgInSc3d1	krevní
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
místo	místo	k7c2	místo
prodavače	prodavač	k1gMnSc2	prodavač
hudebnin	hudebnina	k1gFnPc2	hudebnina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
práci	práce	k1gFnSc3	práce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
milovníkem	milovník	k1gMnSc7	milovník
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gNnSc3	on
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
přemáhat	přemáhat	k5eAaImF	přemáhat
jeho	jeho	k3xOp3gFnSc4	jeho
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
stačil	stačit	k5eAaBmAgMnS	stačit
pětkrát	pětkrát	k6eAd1	pětkrát
oženit	oženit	k5eAaPmF	oženit
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jeanette	Jeanett	k1gInSc5	Jeanett
Marlin	Marlina	k1gFnPc2	Marlina
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kleo	Kleo	k1gMnSc1	Kleo
Apostolides	Apostolides	k1gMnSc1	Apostolides
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
59	[number]	k4	59
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anne	Anne	k1gNnSc1	Anne
Williams	Williamsa	k1gFnPc2	Williamsa
Rubinstein	Rubinsteina	k1gFnPc2	Rubinsteina
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc1	Nancy
Hackett	Hackett	k1gMnSc1	Hackett
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
a	a	k8xC	a
Leslie	Leslie	k1gFnSc1	Leslie
(	(	kIx(	(
<g/>
Tessa	Tessa	k1gFnSc1	Tessa
<g/>
)	)	kIx)	)
Busby	Busb	k1gInPc1	Busb
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
77	[number]	k4	77
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
pátém	pátý	k4xOgInSc6	pátý
rozvodu	rozvod	k1gInSc6	rozvod
na	na	k7c4	na
svatby	svatba	k1gFnPc4	svatba
zanevřel	zanevřít	k5eAaPmAgMnS	zanevřít
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
byl	být	k5eAaImAgInS	být
otcem	otec	k1gMnSc7	otec
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dcer	dcera	k1gFnPc2	dcera
Laury	Laura	k1gFnSc2	Laura
Archer	Archra	k1gFnPc2	Archra
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
Isoldy	Isolda	k1gMnSc2	Isolda
Frey	Frea	k1gMnSc2	Frea
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Christophera	Christopher	k1gMnSc4	Christopher
Kennetha	Kenneth	k1gMnSc4	Kenneth
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
válku	válka	k1gFnSc4	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
postoje	postoj	k1gInSc2	postoj
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
protestu	protest	k1gInSc6	protest
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
"	"	kIx"	"
<g/>
Writers	Writers	k1gInSc1	Writers
and	and	k?	and
Editors	Editors	k1gInSc1	Editors
War	War	k1gMnSc1	War
Tax	taxa	k1gFnPc2	taxa
Protest	protest	k1gInSc1	protest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
někteří	některý	k3yIgMnPc1	některý
spisovatelé	spisovatel	k1gMnPc1	spisovatel
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
platit	platit	k5eAaImF	platit
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
jako	jako	k8xS	jako
způsob	způsob	k1gInSc4	způsob
protestu	protest	k1gInSc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInSc1d1	finanční
úřad	úřad	k1gInSc1	úřad
IRS	IRS	kA	IRS
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
zabavil	zabavit	k5eAaPmAgMnS	zabavit
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
po	po	k7c6	po
infarktu	infarkt	k1gInSc6	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
,	,	kIx,	,
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
mozková	mozkový	k2eAgFnSc1d1	mozková
mrtvice	mrtvice	k1gFnSc1	mrtvice
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
umírá	umírat	k5eAaImIp3nS	umírat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
54	[number]	k4	54
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Fort	Fort	k?	Fort
Morgan	morgan	k1gInSc4	morgan
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
Colorado	Colorado	k1gNnSc4	Colorado
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
ostatků	ostatek	k1gInPc2	ostatek
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sci-fi	scii	k1gFnSc1	sci-fi
povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
Beyond	Beyond	k1gInSc4	Beyond
Lies	Liesa	k1gFnPc2	Liesa
The	The	k1gMnSc1	The
Wub	Wub	k1gMnSc1	Wub
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Wub	Wub	k1gFnSc1	Wub
<g/>
,	,	kIx,	,
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jeho	jeho	k3xOp3gInSc4	jeho
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
ironický	ironický	k2eAgInSc4d1	ironický
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
humoristický	humoristický	k2eAgInSc4d1	humoristický
podtón	podtón	k1gInSc4	podtón
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
otiskl	otisknout	k5eAaPmAgInS	otisknout
svou	svůj	k3xOyFgFnSc4	svůj
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
povídku	povídka	k1gFnSc4	povídka
Impostor	Impostor	k1gInSc1	Impostor
(	(	kIx(	(
<g/>
Podvodník	podvodník	k1gMnSc1	podvodník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
práci	práce	k1gFnSc4	práce
spisovatele	spisovatel	k1gMnSc2	spisovatel
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
román	román	k1gInSc1	román
Solar	Solar	k1gInSc1	Solar
Lottery	Lotter	k1gInPc1	Lotter
(	(	kIx(	(
<g/>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
loterie	loterie	k1gFnSc1	loterie
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
toužil	toužit	k5eAaImAgInS	toužit
psát	psát	k5eAaImF	psát
hlavně	hlavně	k9	hlavně
realistické	realistický	k2eAgInPc1d1	realistický
romány	román	k1gInPc1	román
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgInS	mít
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydavatelé	vydavatel	k1gMnPc1	vydavatel
mu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
vytrvale	vytrvale	k6eAd1	vytrvale
vraceli	vracet	k5eAaImAgMnP	vracet
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xS	jako
fenomenální	fenomenální	k2eAgMnSc1d1	fenomenální
spisovatel	spisovatel	k1gMnSc1	spisovatel
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Confessions	Confessions	k1gInSc1	Confessions
of	of	k?	of
a	a	k8xC	a
Crap	Crap	k1gMnSc1	Crap
Artist	Artist	k1gMnSc1	Artist
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
úletů	úlet	k1gInPc2	úlet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
klasické	klasický	k2eAgFnSc2d1	klasická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
oddal	oddat	k5eAaPmAgInS	oddat
žánru	žánr	k1gInSc3	žánr
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
za	za	k7c4	za
román	román	k1gInSc4	román
Man	Man	k1gMnSc1	Man
In	In	k1gMnSc1	In
the	the	k?	the
High	High	k1gMnSc1	High
Castle	Castle	k1gFnSc2	Castle
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Vysokého	vysoký	k2eAgInSc2d1	vysoký
zámku	zámek	k1gInSc2	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nezískal	získat	k5eNaPmAgInS	získat
podporu	podpora	k1gFnSc4	podpora
velkých	velký	k2eAgMnPc2d1	velký
nakladatelů	nakladatel	k1gMnPc2	nakladatel
a	a	k8xC	a
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
se	se	k3xPyFc4	se
protlačit	protlačit	k5eAaPmF	protlačit
k	k	k7c3	k
mainstreamovým	mainstreamový	k2eAgMnPc3d1	mainstreamový
čtenářům	čtenář	k1gMnPc3	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
během	během	k7c2	během
většiny	většina	k1gFnSc2	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zažíval	zažívat	k5eAaImAgMnS	zažívat
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
Dickovi	Dicek	k1gMnSc3	Dicek
konečně	konečně	k9	konečně
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
slavným	slavný	k2eAgInSc7d1	slavný
a	a	k8xC	a
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
beletrie	beletrie	k1gFnSc2	beletrie
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
článků	článek	k1gInPc2	článek
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
uskutečňovány	uskutečňován	k2eAgInPc4d1	uskutečňován
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Playboy	playboy	k1gMnSc1	playboy
<g/>
,	,	kIx,	,
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
také	také	k9	také
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
Blade	Blad	k1gInSc5	Blad
Runner	Runnero	k1gNnPc2	Runnero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
pracovní	pracovní	k2eAgFnSc4d1	pracovní
verzi	verze	k1gFnSc4	verze
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
románem	román	k1gInSc7	román
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
The	The	k1gFnSc2	The
Transmigration	Transmigration	k1gInSc1	Transmigration
of	of	k?	of
Timothy	Timotha	k1gFnSc2	Timotha
Archer	Archra	k1gFnPc2	Archra
(	(	kIx(	(
<g/>
Převtělení	převtělení	k1gNnSc1	převtělení
Timothyho	Timothy	k1gMnSc2	Timothy
Archera	Archer	k1gMnSc2	Archer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
napsal	napsat	k5eAaBmAgMnS	napsat
36	[number]	k4	36
románů	román	k1gInPc2	román
a	a	k8xC	a
121	[number]	k4	121
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
román	román	k1gInSc4	román
Flow	Flow	k1gFnSc2	Flow
My	my	k3xPp1nPc1	my
Tears	Tears	k1gInSc1	Tears
<g/>
,	,	kIx,	,
the	the	k?	the
Policeman	Policeman	k1gMnSc1	Policeman
Said	Said	k1gMnSc1	Said
(	(	kIx(	(
<g/>
Kaňte	kanit	k5eAaImRp2nP	kanit
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gFnSc2	můj
slzy	slza	k1gFnSc2	slza
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
policista	policista	k1gMnSc1	policista
<g/>
)	)	kIx)	)
cenu	cena	k1gFnSc4	cena
John	John	k1gMnSc1	John
W.	W.	kA	W.
Campbell	Campbell	k1gMnSc1	Campbell
Memorial	Memorial	k1gMnSc1	Memorial
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
sci-fi	scii	k1gFnPc4	sci-fi
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
román	román	k1gInSc4	román
A	a	k8xC	a
Scanner	scanner	k1gInSc1	scanner
Darkly	Darkl	k1gInPc1	Darkl
(	(	kIx(	(
<g/>
Temný	temný	k2eAgInSc1d1	temný
obraz	obraz	k1gInSc1	obraz
<g/>
)	)	kIx)	)
cenu	cena	k1gFnSc4	cena
britské	britský	k2eAgFnSc2d1	britská
asociace	asociace	k1gFnSc2	asociace
BSFA	BSFA	kA	BSFA
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
sci-fi	scii	k1gFnSc4	sci-fi
román	román	k1gInSc4	román
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Metách	Mety	k1gFnPc6	Mety
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
cenu	cena	k1gFnSc4	cena
Graouilly	Graouilla	k1gFnSc2	Graouilla
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Or	Or	k1gMnSc1	Or
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
udělována	udělován	k2eAgFnSc1d1	udělována
cena	cena	k1gFnSc1	cena
Philipa	Philip	k1gMnSc2	Philip
K.	K.	kA	K.
Dicka	Dicek	k1gMnSc2	Dicek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
životní	životní	k2eAgFnSc6d1	životní
dráze	dráha	k1gFnSc6	dráha
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
Kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
ji	on	k3xPp3gFnSc4	on
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
lidí	člověk	k1gMnPc2	člověk
kouřilo	kouřit	k5eAaImAgNnS	kouřit
a	a	k8xC	a
četlo	číst	k5eAaImAgNnS	číst
The	The	k1gFnSc4	The
Daily	Daila	k1gMnSc2	Daila
Cal	Cal	k1gMnSc2	Cal
a	a	k8xC	a
poslouchalo	poslouchat	k5eAaImAgNnS	poslouchat
mé	můj	k3xOp1gFnPc4	můj
rady	rada	k1gFnPc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Sci-fi	scii	k1gFnSc4	sci-fi
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgMnS	začít
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
když	když	k8xS	když
mi	já	k3xPp1nSc3	já
bylo	být	k5eAaImAgNnS	být
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
koupil	koupit	k5eAaPmAgInS	koupit
výtisk	výtisk	k1gInSc1	výtisk
Stirring	Stirring	k1gInSc4	Stirring
Science	Science	k1gFnSc2	Science
Stories	Storiesa	k1gFnPc2	Storiesa
místo	místo	k7c2	místo
Popular	Populara	k1gFnPc2	Populara
Science	Science	k1gFnSc2	Science
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
pustil	pustit	k5eAaPmAgMnS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
...	...	k?	...
číst	číst	k5eAaImF	číst
Joyce	Joyce	k1gMnSc4	Joyce
<g/>
,	,	kIx,	,
Kafku	Kafka	k1gMnSc4	Kafka
<g/>
,	,	kIx,	,
Steinbecka	Steinbecka	k1gFnSc1	Steinbecka
<g/>
,	,	kIx,	,
Prousta	Prousta	k1gFnSc1	Prousta
<g/>
,	,	kIx,	,
Dos	Dos	k1gFnSc6	Dos
Passose	Passosa	k1gFnSc6	Passosa
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
místnosti	místnost	k1gFnSc6	místnost
bez	bez	k7c2	bez
kuchyně	kuchyně	k1gFnSc2	kuchyně
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
krátké	krátký	k2eAgFnPc4d1	krátká
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jsem	být	k5eAaImIp1nS	být
potkal	potkat	k5eAaPmAgMnS	potkat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsem	být	k5eAaImIp1nS	být
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgMnS	koupit
dům	dům	k1gInSc4	dům
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
prodávat	prodávat	k5eAaImF	prodávat
SF	SF	kA	SF
a	a	k8xC	a
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
jsem	být	k5eAaImIp1nS	být
prodávání	prodávání	k1gNnSc4	prodávání
v	v	k7c6	v
hudebninách	hudebnina	k1gFnPc6	hudebnina
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
poslouchám	poslouchat	k5eAaImIp1nS	poslouchat
Monteverdiho	Monteverdi	k1gMnSc4	Monteverdi
a	a	k8xC	a
Buxtehudeho	Buxtehude	k1gMnSc4	Buxtehude
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
čtu	číst	k5eAaImIp1nS	číst
Ibsena	Ibsen	k1gMnSc4	Ibsen
a	a	k8xC	a
píšu	psát	k5eAaImIp1nS	psát
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nezmizí	zmizet	k5eNaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
přestanete	přestat	k5eAaPmIp2nP	přestat
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
z	z	k7c2	z
románu	román	k1gInSc2	román
VALIS	VALIS	kA	VALIS
<g/>
.	.	kIx.	.
</s>
<s>
Beyond	Beyond	k1gMnSc1	Beyond
Lies	Liesa	k1gFnPc2	Liesa
The	The	k1gMnSc1	The
Wub	Wub	k1gMnSc1	Wub
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Wub	Wub	k1gFnSc1	Wub
<g/>
,	,	kIx,	,
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Little	Little	k1gFnSc1	Little
Movement	Movement	k1gMnSc1	Movement
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
vzpoura	vzpoura	k1gFnSc1	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Impostor	Impostor	k1gInSc1	Impostor
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Podvodník	podvodník	k1gMnSc1	podvodník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paycheck	Paycheck	k1gInSc1	Paycheck
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Výplata	výplata	k1gFnSc1	výplata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
King	King	k1gMnSc1	King
of	of	k?	of
the	the	k?	the
Elves	Elves	k1gInSc1	Elves
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
elfů	elf	k1gMnPc2	elf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Defenders	Defenders	k1gInSc1	Defenders
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Obránci	obránce	k1gMnPc7	obránce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gMnSc1	World
She	She	k1gMnSc1	She
Wanted	Wanted	k1gMnSc1	Wanted
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
si	se	k3xPyFc3	se
vysnila	vysnít	k5eAaPmAgFnS	vysnít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Impossible	Impossible	k1gFnSc1	Impossible
Planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Second	Second	k1gInSc1	Second
Variety	varieta	k1gFnSc2	varieta
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k8xC	jako
Druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
Typ	typ	k1gInSc1	typ
číslo	číslo	k1gNnSc1	číslo
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Eyes	Eyes	k1gInSc1	Eyes
Have	Hav	k1gMnSc2	Hav
It	It	k1gMnSc2	It
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Bije	bít	k5eAaImIp3nS	bít
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Colony	colon	k1gNnPc7	colon
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Kolonie	kolonie	k1gFnSc1	kolonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Golden	Goldna	k1gFnPc2	Goldna
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Adjustment	Adjustment	k1gInSc1	Adjustment
Team	team	k1gInSc1	team
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Úpraváři	úpravář	k1gMnPc7	úpravář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Crystal	Crystal	k1gMnSc1	Crystal
Crypt	Crypt	k1gMnSc1	Crypt
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
krypta	krypta	k1gFnSc1	krypta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Foster	Foster	k1gMnSc1	Foster
<g/>
,	,	kIx,	,
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Dead	Dead	k1gInSc1	Dead
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Fostere	Foster	k1gMnSc5	Foster
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minority	minorita	k1gFnPc1	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Menšinová	menšinový	k2eAgFnSc1d1	menšinová
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
War	War	k?	War
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Válečná	válečný	k2eAgFnSc1d1	válečná
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
Be	Be	k1gMnSc1	Be
A	a	k8xC	a
Blobel	Blobel	k1gMnSc1	Blobel
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Být	být	k5eAaImF	být
tak	tak	k9	tak
Měňavkou	měňavka	k1gFnSc7	měňavka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Život	život	k1gInSc1	život
s	s	k7c7	s
blobem	blob	k1gInSc7	blob
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Little	Little	k1gFnSc1	Little
Black	Black	k1gMnSc1	Black
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
krabička	krabička	k1gFnSc1	krabička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
Can	Can	k1gMnSc1	Can
Remember	Remember	k1gMnSc1	Remember
It	It	k1gMnSc1	It
for	forum	k1gNnPc2	forum
You	You	k1gFnSc6	You
Wholesale	Wholesala	k1gFnSc6	Wholesala
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Zapamatujeme	zapamatovat	k5eAaPmIp1nP	zapamatovat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
za	za	k7c2	za
vás	vy	k3xPp2nPc2	vy
se	se	k3xPyFc4	se
slevou	sleva	k1gFnSc7	sleva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Total	totat	k5eAaImAgMnS	totat
Recall	Recall	k1gMnSc1	Recall
<g/>
.	.	kIx.	.
</s>
<s>
Faith	Faith	k1gInSc1	Faith
of	of	k?	of
Our	Our	k1gFnSc2	Our
Fathers	Fathersa	k1gFnPc2	Fathersa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Víra	víra	k1gFnSc1	víra
našich	náš	k3xOp1gMnPc2	náš
otců	otec	k1gMnPc2	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
bezvýznamného	bezvýznamný	k2eAgMnSc2d1	bezvýznamný
ministerského	ministerský	k2eAgMnSc2d1	ministerský
úředníčka	úředníček	k1gMnSc2	úředníček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
najednou	najednou	k6eAd1	najednou
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světový	světový	k2eAgMnSc1d1	světový
vládce	vládce	k1gMnSc1	vládce
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
navozuje	navozovat	k5eAaImIp3nS	navozovat
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Pre-Persons	Pre-Persons	k1gInSc1	Pre-Persons
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Před-lidé	Předidý	k2eAgNnSc4d1	Před-lidý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frozen	Frozna	k1gFnPc2	Frozna
Journey	Journea	k1gFnSc2	Journea
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Ledová	ledový	k2eAgFnSc1d1	ledová
pouť	pouť	k1gFnSc1	pouť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Handful	Handful	k1gInSc1	Handful
of	of	k?	of
Darkness	Darkness	k1gInSc1	Darkness
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Variable	Variable	k1gMnSc1	Variable
Man	Man	k1gMnSc1	Man
and	and	k?	and
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Preserving	Preserving	k1gInSc1	Preserving
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
vydána	vydán	k2eAgFnSc1d1	vydána
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
he	he	k0	he
Turning	Turning	k1gInSc1	Turning
Wheel	Wheel	k1gMnSc1	Wheel
and	and	k?	and
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
<g/>
!!	!!	k?	!!
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Golden	Goldna	k1gFnPc2	Goldna
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Robots	Robots	k1gInSc1	Robots
<g/>
,	,	kIx,	,
Androids	Androids	k1gInSc1	Androids
<g/>
,	,	kIx,	,
and	and	k?	and
Mechanical	Mechanical	k1gMnSc1	Mechanical
Oddities	Oddities	k1gMnSc1	Oddities
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Hope	Hope	k1gFnSc1	Hope
I	i	k8xC	i
Shall	Shall	k1gMnSc1	Shall
Arrive	Arriev	k1gFnSc2	Arriev
Soon	Soon	k1gMnSc1	Soon
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Collected	Collected	k1gMnSc1	Collected
Stories	Stories	k1gMnSc1	Stories
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětidílný	pětidílný	k2eAgInSc1d1	pětidílný
kompletní	kompletní	k2eAgInSc4d1	kompletní
soubor	soubor	k1gInSc4	soubor
autorových	autorův	k2eAgFnPc2d1	autorova
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vyšly	vyjít	k5eAaPmAgFnP	vyjít
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
díly	díl	k1gInPc4	díl
souboru	soubor	k1gInSc2	soubor
pod	pod	k7c7	pod
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
jmény	jméno	k1gNnPc7	jméno
<g/>
:	:	kIx,	:
Beyond	Beyond	k1gInSc1	Beyond
Lies	Lies	k1gInSc1	Lies
the	the	k?	the
Wub	Wub	k1gFnSc2	Wub
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Wub	Wub	k1gFnSc1	Wub
<g/>
,	,	kIx,	,
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
Second	Second	k1gInSc1	Second
Variety	varieta	k1gFnSc2	varieta
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Father-Thing	Father-Thing	k1gInSc1	Father-Thing
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Otcova	otcův	k2eAgFnSc1d1	otcova
napodobenina	napodobenina	k1gFnSc1	napodobenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Podivný	podivný	k2eAgInSc4d1	podivný
ráj	ráj	k1gInSc4	ráj
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Days	Days	k1gInSc1	Days
of	of	k?	of
Perky	perko	k1gNnPc7	perko
Pat	pata	k1gFnPc2	pata
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Tenkrát	tenkrát	k6eAd1	tenkrát
s	s	k7c7	s
Parádnicí	parádnice	k1gFnSc7	parádnice
Pat	pata	k1gFnPc2	pata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
jako	jako	k8xS	jako
The	The	k1gMnSc1	The
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
Menšinová	menšinový	k2eAgFnSc1d1	menšinová
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k8xS	jako
Minority	minorita	k1gMnPc4	minorita
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Little	Little	k1gFnSc1	Little
Black	Black	k1gMnSc1	Black
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
krabička	krabička	k1gFnSc1	krabička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Solar	Solar	k1gInSc1	Solar
Lottery	Lotter	k1gInPc1	Lotter
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
loterie	loterie	k1gFnSc1	loterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
autorův	autorův	k2eAgInSc4d1	autorův
vydaný	vydaný	k2eAgInSc4d1	vydaný
román	román	k1gInSc4	román
(	(	kIx(	(
<g/>
vyšel	vyjít	k5eAaPmAgInS	vyjít
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
World	World	k1gInSc4	World
of	of	k?	of
Chance	Chanec	k1gInSc2	Chanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Jones	Jones	k1gMnSc1	Jones
Made	Made	k1gInSc1	Made
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stvořil	stvořit	k5eAaPmAgInS	stvořit
Jones	Jones	k1gInSc4	Jones
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
Who	Who	k1gMnSc1	Who
Japed	Japed	k1gMnSc1	Japed
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
roku	rok	k1gInSc2	rok
2114	[number]	k4	2114
v	v	k7c6	v
postapokalyptické	postapokalyptický	k2eAgFnSc6d1	postapokalyptická
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
nukleární	nukleární	k2eAgFnSc6d1	nukleární
válce	válka	k1gFnSc6	válka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
totalitní	totalitní	k2eAgInSc4d1	totalitní
režim	režim	k1gInSc4	režim
zdánlivě	zdánlivě	k6eAd1	zdánlivě
bez	bez	k7c2	bez
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
hladomorů	hladomor	k1gInPc2	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cosmic	Cosmic	k1gMnSc1	Cosmic
Puppets	Puppets	k1gInSc1	Puppets
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přepracováním	přepracování	k1gNnSc7	přepracování
povídky	povídka	k1gFnSc2	povídka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
A	a	k9	a
Glass	Glass	k1gInSc1	Glass
of	of	k?	of
Darkness	Darkness	k1gInSc1	Darkness
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
románu	román	k1gInSc2	román
nepoznává	poznávat	k5eNaImIp3nS	poznávat
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
a	a	k8xC	a
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
novinách	novina	k1gFnPc6	novina
nachází	nacházet	k5eAaImIp3nS	nacházet
dokonce	dokonce	k9	dokonce
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Eye	Eye	k?	Eye
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Oko	oko	k1gNnSc1	oko
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
částicového	částicový	k2eAgInSc2d1	částicový
urychlovače	urychlovač	k1gInSc2	urychlovač
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
naprosto	naprosto	k6eAd1	naprosto
odlišných	odlišný	k2eAgMnPc2d1	odlišný
lidí	člověk	k1gMnPc2	člověk
uvězněno	uvězněn	k2eAgNnSc1d1	uvězněno
v	v	k7c4	v
sérii	série	k1gFnSc4	série
halucinogenních	halucinogenní	k2eAgInPc2d1	halucinogenní
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
stvořených	stvořený	k2eAgNnPc2d1	stvořené
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
jejich	jejich	k3xOp3gMnPc3	jejich
hodnotovým	hodnotový	k2eAgMnPc3d1	hodnotový
systémům	systém	k1gInPc3	systém
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc3	jejich
obavám	obava	k1gFnPc3	obava
a	a	k8xC	a
nadějím	naděje	k1gFnPc3	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Time	Time	k1gFnSc1	Time
Out	Out	k1gFnSc2	Out
of	of	k?	of
Joint	Joint	k1gMnSc1	Joint
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Vykolejený	vykolejený	k2eAgInSc1d1	vykolejený
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
verze	verze	k1gFnSc1	verze
románu	román	k1gInSc2	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
do	do	k7c2	do
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Biography	Biographa	k1gFnSc2	Biographa
in	in	k?	in
Time	Tim	k1gFnSc2	Tim
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
časopise	časopis	k1gInSc6	časopis
New	New	k1gFnSc2	New
Worlds	Worldsa	k1gFnPc2	Worldsa
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
.	.	kIx.	.
</s>
<s>
Vulcan	Vulcan	k1gInSc1	Vulcan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časopisecky	časopisecky	k6eAd1	časopisecky
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgInSc1d1	popisován
svět	svět	k1gInSc1	svět
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
počítači	počítač	k1gMnPc7	počítač
a	a	k8xC	a
vzpoura	vzpoura	k1gFnSc1	vzpoura
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Futurity	Futurit	k1gInPc1	Futurit
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přepracováváním	přepracovávání	k1gNnSc7	přepracovávání
povídky	povídka	k1gFnSc2	povídka
Time	Time	k1gNnSc2	Time
Pawn	Pawna	k1gFnPc2	Pawna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Jim	on	k3xPp3gMnPc3	on
Parsons	Parsons	k1gInSc1	Parsons
je	být	k5eAaImIp3nS	být
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2405	[number]	k4	2405
<g/>
,	,	kIx,	,
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
zachraňovat	zachraňovat	k5eAaImF	zachraňovat
lidské	lidský	k2eAgInPc4d1	lidský
životy	život	k1gInPc4	život
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
přirozených	přirozený	k2eAgInPc2d1	přirozený
porodů	porod	k1gInPc2	porod
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k9	jen
smrt	smrt	k1gFnSc1	smrt
někoho	někdo	k3yInSc4	někdo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc1	vznik
nové	nový	k2eAgFnSc2d1	nová
lidské	lidský	k2eAgFnSc2d1	lidská
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
High	High	k1gInSc1	High
Castle	Castle	k1gFnSc1	Castle
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Vysokého	vysoký	k2eAgInSc2d1	vysoký
zámku	zámek	k1gInSc2	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
román	román	k1gInSc4	román
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
státy	stát	k1gInPc1	stát
Osy	osa	k1gFnSc2	osa
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
cenu	cena	k1gFnSc4	cena
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Game-Players	Game-Players	k1gInSc1	Game-Players
of	of	k?	of
Titan	titan	k1gInSc1	titan
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
Titanu	titan	k1gInSc2	titan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Martian	Martian	k1gInSc4	Martian
Time-Slip	Time-Slip	k1gInSc4	Time-Slip
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Marsovský	marsovský	k2eAgInSc4d1	marsovský
skluz	skluz	k1gInSc4	skluz
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časopisecky	časopisecky	k6eAd1	časopisecky
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
jako	jako	k8xC	jako
All	All	k1gMnSc3	All
We	We	k1gMnSc3	We
Marsmen	Marsmen	k1gInSc4	Marsmen
(	(	kIx(	(
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
jsme	být	k5eAaImIp1nP	být
Marťané	Marťan	k1gMnPc1	Marťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Simulacra	Simulacra	k1gFnSc1	Simulacra
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
Dick	Dick	k1gMnSc1	Dick
poprvé	poprvé	k6eAd1	poprvé
zabývá	zabývat	k5eAaImIp3nS	zabývat
otázkou	otázka	k1gFnSc7	otázka
lidskosti	lidskost	k1gFnSc2	lidskost
androidů	android	k1gInPc2	android
(	(	kIx(	(
<g/>
v	v	k7c6	v
budoucí	budoucí	k2eAgFnSc6d1	budoucí
totalitní	totalitní	k2eAgFnSc6d1	totalitní
společnosti	společnost	k1gFnSc6	společnost
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
krásná	krásný	k2eAgFnSc1d1	krásná
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Clans	Clans	k1gInSc1	Clans
of	of	k?	of
the	the	k?	the
Alphane	Alphan	k1gMnSc5	Alphan
Moon	Moon	k1gMnSc1	Moon
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Klany	klan	k1gInPc4	klan
alfanského	alfanský	k2eAgInSc2d1	alfanský
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
zakložen	zakložit	k5eAaPmNgInS	zakložit
na	na	k7c6	na
autorově	autorův	k2eAgFnSc6d1	autorova
povídce	povídka	k1gFnSc6	povídka
Shell	Shella	k1gFnPc2	Shella
Game	game	k1gInSc4	game
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Penultimate	Penultimat	k1gInSc5	Penultimat
Truth	Truth	k1gMnSc1	Truth
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Předposlední	předposlední	k2eAgFnSc1d1	předposlední
pravda	pravda	k1gFnSc1	pravda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bloodmoney	Bloodmonea	k1gMnSc2	Bloodmonea
<g/>
,	,	kIx,	,
or	or	k?	or
How	How	k1gMnSc1	How
We	We	k1gMnSc1	We
Got	Got	k1gMnSc1	Got
Along	Along	k1gMnSc1	Along
After	Aftra	k1gFnPc2	Aftra
the	the	k?	the
Bomb	bomba	k1gFnPc2	bomba
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krvemsta	Krvemsta	k1gMnSc1	Krvemsta
aneb	aneb	k?	aneb
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
vedlo	vést	k5eAaImAgNnS	vést
po	po	k7c6	po
bombě	bomba	k1gFnSc6	bomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Three	Three	k1gInSc1	Three
Stigmata	stigma	k1gNnPc1	stigma
of	of	k?	of
Palmer	Palmer	k1gMnSc1	Palmer
Eldritch	Eldritch	k1gMnSc1	Eldritch
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Tři	tři	k4xCgInPc1	tři
stigmata	stigma	k1gNnPc1	stigma
Palmera	Palmer	k1gMnSc2	Palmer
Eldritche	Eldritch	k1gMnSc2	Eldritch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
světě	svět	k1gInSc6	svět
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
drogy	droga	k1gFnSc2	droga
vymkly	vymknout	k5eAaPmAgInP	vymknout
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Crack	Crack	k1gMnSc1	Crack
in	in	k?	in
Space	Space	k1gMnSc1	Space
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
rozšířením	rozšíření	k1gNnSc7	rozšíření
popvídky	popvídka	k1gFnSc2	popvídka
Cantata	Cantata	k1gFnSc1	Cantata
140	[number]	k4	140
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
roku	rok	k1gInSc2	rok
2080	[number]	k4	2080
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
vážnými	vážný	k2eAgFnPc7d1	vážná
potížemi	potíž	k1gFnPc7	potíž
plynoucí	plynoucí	k2eAgFnPc4d1	plynoucí
z	z	k7c2	z
přelidnění	přelidnění	k1gNnSc2	přelidnění
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
objeví	objevit	k5eAaPmIp3nP	objevit
portál	portál	k1gInSc4	portál
do	do	k7c2	do
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neobývaného	obývaný	k2eNgInSc2d1	neobývaný
paralelního	paralelní	k2eAgInSc2d1	paralelní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
doufají	doufat	k5eAaImIp3nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
pozemšťané	pozemšťan	k1gMnPc1	pozemšťan
mohli	moct	k5eAaImAgMnP	moct
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
<g/>
.	.	kIx.	.
</s>
<s>
Now	Now	k?	Now
Wait	Waita	k1gFnPc2	Waita
for	forum	k1gNnPc2	forum
Last	Last	k1gMnSc1	Last
Year	Year	k1gMnSc1	Year
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Počkej	počkat	k5eAaPmRp2nS	počkat
si	se	k3xPyFc3	se
na	na	k7c4	na
loňský	loňský	k2eAgInSc4d1	loňský
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
ztracen	ztratit	k5eAaPmNgMnS	ztratit
v	v	k7c6	v
labyrintu	labyrint	k1gInSc6	labyrint
alternativních	alternativní	k2eAgInPc2d1	alternativní
světů	svět	k1gInPc2	svět
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
drogy	droga	k1gFnSc2	droga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
cestovat	cestovat	k5eAaImF	cestovat
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Unteleported	Unteleported	k1gMnSc1	Unteleported
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Neteleportovaný	Neteleportovaný	k2eAgMnSc1d1	Neteleportovaný
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
vyšla	vyjít	k5eAaPmAgFnS	vyjít
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
verze	verze	k1gFnSc1	verze
románu	román	k1gInSc2	román
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lies	Liesa	k1gFnPc2	Liesa
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnPc2	Inc
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Ganymede	Ganymed	k1gMnSc5	Ganymed
Takeover	Takeover	k1gMnSc1	Takeover
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rayem	Ray	k1gMnSc7	Ray
Nelsonem	Nelson	k1gMnSc7	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zbraně	zbraň	k1gFnPc1	zbraň
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
různorodé	různorodý	k2eAgFnPc4d1	různorodá
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
vymknou	vymknout	k5eAaPmIp3nP	vymknout
po	po	k7c6	po
použití	použití	k1gNnSc3	použití
proti	proti	k7c3	proti
dobyvačným	dobyvačný	k2eAgMnPc3d1	dobyvačný
červům	červ	k1gMnPc3	červ
z	z	k7c2	z
Ganymedu	Ganymed	k1gMnSc3	Ganymed
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Zap	Zap	k1gMnSc1	Zap
Gun	Gun	k1gMnSc1	Gun
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časopisecky	časopisecky	k6eAd1	časopisecky
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
jako	jako	k8xC	jako
Project	Project	k1gMnSc1	Project
Plowshare	Plowshar	k1gInSc5	Plowshar
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
závody	závod	k1gInPc1	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c4	v
podvod	podvod	k1gInSc4	podvod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgNnSc3	který
není	být	k5eNaImIp3nS	být
Země	země	k1gFnSc1	země
schopna	schopen	k2eAgFnSc1d1	schopna
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
mimozemské	mimozemský	k2eAgFnSc3d1	mimozemská
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Counter-Clock	Counter-Clock	k1gMnSc1	Counter-Clock
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Když	když	k8xS	když
mrtví	mrtvý	k1gMnPc1	mrtvý
mládnou	mládnout	k5eAaImIp3nP	mládnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
autorovy	autorův	k2eAgFnSc2d1	autorova
povídky	povídka	k1gFnSc2	povídka
Your	Your	k1gMnSc1	Your
Appointment	Appointment	k1gMnSc1	Appointment
Will	Will	k1gMnSc1	Will
Be	Be	k1gFnSc4	Be
Yesterday	Yesterdaa	k1gFnSc2	Yesterdaa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Androids	Androidsa	k1gFnPc2	Androidsa
Dream	Dream	k1gInSc1	Dream
of	of	k?	of
Electric	Electric	k1gMnSc1	Electric
Sheep	Sheep	k1gMnSc1	Sheep
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Sní	snít	k5eAaImIp3nP	snít
androidi	android	k1gMnPc1	android
o	o	k7c6	o
elektrických	elektrický	k2eAgFnPc6d1	elektrická
ovečkách	ovečka	k1gFnPc6	ovečka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Blade	Blad	k1gInSc5	Blad
Runner	Runnero	k1gNnPc2	Runnero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ubik	Ubik	k1gInSc1	Ubik
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
neuznávanějších	uznávaný	k2eNgInPc2d2	uznávaný
autorových	autorův	k2eAgInPc2d1	autorův
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
zneklidňující	zneklidňující	k2eAgInSc1d1	zneklidňující
existenciální	existenciální	k2eAgInSc1d1	existenciální
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
skutečné	skutečný	k2eAgNnSc1d1	skutečné
a	a	k8xC	a
co	co	k9	co
pouhá	pouhý	k2eAgFnSc1d1	pouhá
iluze	iluze	k1gFnSc1	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Galactic	Galactice	k1gFnPc2	Galactice
Pot-Healer	Pot-Healer	k1gMnSc1	Pot-Healer
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
bezvýznamném	bezvýznamný	k2eAgMnSc6d1	bezvýznamný
opraváři	opravář	k1gMnSc6	opravář
keramiky	keramika	k1gFnSc2	keramika
žijícím	žijící	k2eAgFnPc3d1	žijící
v	v	k7c6	v
totalitní	totalitní	k2eAgFnSc6d1	totalitní
pozemské	pozemský	k2eAgFnSc6d1	pozemská
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vyzván	vyzvat	k5eAaPmNgInS	vyzvat
Bohu	bůh	k1gMnSc3	bůh
podobným	podobný	k2eAgMnSc7d1	podobný
cizincem	cizinec	k1gMnSc7	cizinec
známým	známý	k1gMnSc7	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Glimmung	Glimmunga	k1gFnPc2	Glimmunga
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
odborného	odborný	k2eAgInSc2d1	odborný
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
Plowmanově	Plowmanův	k2eAgFnSc6d1	Plowmanův
planetě	planeta	k1gFnSc6	planeta
v	v	k7c6	v
systému	systém	k1gInSc6	systém
hvězdy	hvězda	k1gFnPc1	hvězda
Sirius	Sirius	k1gInSc4	Sirius
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
oceánu	oceán	k1gInSc2	oceán
starobylou	starobylý	k2eAgFnSc4d1	starobylá
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Maze	Maga	k1gFnSc3	Maga
of	of	k?	of
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
čtrnácti	čtrnáct	k4xCc2	čtrnáct
kolonistů	kolonista	k1gMnPc2	kolonista
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Delmak-O	Delmak-O	k1gMnPc2	Delmak-O
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
navzájem	navzájem	k6eAd1	navzájem
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
nebo	nebo	k8xC	nebo
umírají	umírat	k5eAaImIp3nP	umírat
za	za	k7c2	za
podivných	podivný	k2eAgFnPc2d1	podivná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
posádkou	posádka	k1gFnSc7	posádka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Perseus	Perseus	k1gMnSc1	Perseus
9	[number]	k4	9
uvízlé	uvízlý	k2eAgFnSc2d1	uvízlá
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
hvězdy	hvězda	k1gFnSc2	hvězda
s	s	k7c7	s
nemožností	nemožnost	k1gFnSc7	nemožnost
zavolat	zavolat	k5eAaPmF	zavolat
si	se	k3xPyFc3	se
nějakou	nějaký	k3yIgFnSc4	nějaký
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vegetačním	vegetační	k2eAgInSc6d1	vegetační
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
prožívají	prožívat	k5eAaImIp3nP	prožívat
virtuální	virtuální	k2eAgFnSc4d1	virtuální
realitu	realita	k1gFnSc4	realita
generovanou	generovaný	k2eAgFnSc4d1	generovaná
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Our	Our	k?	Our
Friends	Friends	k1gInSc1	Friends
from	from	k1gInSc1	from
Frolix	Frolix	k1gInSc1	Frolix
8	[number]	k4	8
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
románu	román	k1gInSc2	román
nalezne	nalézt	k5eAaBmIp3nS	nalézt
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
nebezpečnému	bezpečný	k2eNgInSc3d1	nebezpečný
tajnému	tajný	k2eAgInSc3d1	tajný
spolku	spolek	k1gInSc3	spolek
<g/>
.	.	kIx.	.
</s>
<s>
We	We	k?	We
Can	Can	k1gMnSc1	Can
Build	Build	k1gMnSc1	Build
You	You	k1gMnSc1	You
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Dokážeme	dokázat	k5eAaPmIp1nP	dokázat
vás	vy	k3xPp2nPc4	vy
stvořit	stvořit	k5eAaPmF	stvořit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Flow	Flow	k?	Flow
My	my	k3xPp1nPc1	my
Tears	Tears	k1gInSc1	Tears
<g/>
,	,	kIx,	,
the	the	k?	the
Policeman	Policeman	k1gMnSc1	Policeman
Said	Said	k1gMnSc1	Said
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Kaňte	kanit	k5eAaImRp2nP	kanit
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gFnSc2	můj
slzy	slza	k1gFnSc2	slza
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
policista	policista	k1gMnSc1	policista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
zskal	zskat	k5eAaBmAgMnS	zskat
Dick	Dick	k1gMnSc1	Dick
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
cenu	cena	k1gFnSc4	cena
John	John	k1gMnSc1	John
W.	W.	kA	W.
Campbell	Campbell	k1gMnSc1	Campbell
Memorial	Memorial	k1gMnSc1	Memorial
<g/>
.	.	kIx.	.
</s>
<s>
Deus	Deus	k1gInSc1	Deus
Irae	Ira	k1gFnSc2	Ira
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
Dickově	Dickův	k2eAgFnSc6d1	Dickova
povídce	povídka	k1gFnSc6	povídka
Velký	velký	k2eAgMnSc1d1	velký
P	P	kA	P
(	(	kIx(	(
<g/>
1063	[number]	k4	1063
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Great	Great	k1gInSc1	Great
C	C	kA	C
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rogerem	Roger	k1gInSc7	Roger
Zelaznym	Zelaznym	k1gInSc1	Zelaznym
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Scanner	scanner	k1gInSc1	scanner
Darkly	Darkl	k1gInPc1	Darkl
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k8xC	jako
Temný	temný	k2eAgInSc1d1	temný
obraz	obraz	k1gInSc1	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
románu	román	k1gInSc6	román
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
mrazivou	mrazivý	k2eAgFnSc7d1	mrazivá
autenticitou	autenticita	k1gFnSc7	autenticita
posána	posána	k1gFnSc1	posána
prohlubující	prohlubující	k2eAgFnSc1d1	prohlubující
se	se	k3xPyFc4	se
paranoická	paranoický	k2eAgFnSc1d1	paranoická
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
VALIS	VALIS	kA	VALIS
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
Dickovy	Dickův	k2eAgInPc4d1	Dickův
gnostické	gnostický	k2eAgInPc4d1	gnostický
"	"	kIx"	"
<g/>
božské	božský	k2eAgFnPc4d1	božská
<g/>
"	"	kIx"	"
trilogie	trilogie	k1gFnPc4	trilogie
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
VALIS	VALIS	kA	VALIS
trilogie	trilogie	k1gFnPc4	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Divine	Divin	k1gMnSc5	Divin
Invasion	Invasion	k1gInSc1	Invasion
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Božská	božský	k2eAgFnSc1d1	božská
invaze	invaze	k1gFnSc1	invaze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
autorovy	autorův	k2eAgFnPc1d1	autorova
"	"	kIx"	"
<g/>
božské	božská	k1gFnPc1	božská
<g/>
"	"	kIx"	"
VALIS	VALIS	kA	VALIS
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Transmigration	Transmigration	k1gInSc1	Transmigration
of	of	k?	of
Timothy	Timotha	k1gFnSc2	Timotha
Archer	Archra	k1gFnPc2	Archra
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Převtělení	převtělení	k1gNnSc4	převtělení
Timothyho	Timothy	k1gMnSc2	Timothy
Archera	Archer	k1gMnSc2	Archer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
třetí	třetí	k4xOgInSc4	třetí
díl	díl	k1gInSc4	díl
autorovy	autorův	k2eAgFnPc1d1	autorova
"	"	kIx"	"
<g/>
božské	božská	k1gFnPc1	božská
<g/>
"	"	kIx"	"
VALIS	VALIS	kA	VALIS
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Lies	Lies	k1gInSc1	Lies
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
verze	verze	k1gFnSc1	verze
románu	román	k1gInSc2	román
Neteleportovaný	Neteleportovaný	k2eAgMnSc1d1	Neteleportovaný
muž	muž	k1gMnSc1	muž
vydaná	vydaný	k2eAgNnPc4d1	vydané
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Radio	radio	k1gNnSc1	radio
Free	Fre	k1gFnSc2	Fre
Albemuth	Albemutha	k1gFnPc2	Albemutha
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Rádio	rádio	k1gNnSc4	rádio
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
Albemut	Albemut	k1gMnSc1	Albemut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
románu	román	k1gInSc2	román
VALIS	VALIS	kA	VALIS
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k1gInSc1	Nick
and	and	k?	and
the	the	k?	the
Glimmung	Glimmung	k1gInSc1	Glimmung
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
napsaný	napsaný	k2eAgInSc1d1	napsaný
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
Plowmanově	Plowmanův	k2eAgFnSc6d1	Plowmanův
planetě	planeta	k1gFnSc6	planeta
jako	jako	k8xC	jako
román	román	k1gInSc4	román
Galactic	Galactice	k1gFnPc2	Galactice
Pot-Healer	Pot-Healer	k1gMnSc1	Pot-Healer
<g/>
.	.	kIx.	.
</s>
<s>
Confessions	Confessions	k1gInSc1	Confessions
of	of	k?	of
a	a	k8xC	a
Crap	Crap	k1gMnSc1	Crap
Artist	Artist	k1gMnSc1	Artist
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Král	Král	k1gMnSc1	Král
úletů	úlet	k1gInPc2	úlet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
nefantastický	fantastický	k2eNgInSc1d1	fantastický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
za	za	k7c2	za
autorova	autorův	k2eAgInSc2d1	autorův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
Whose	Whos	k1gMnSc2	Whos
Teeth	Teeth	k1gMnSc1	Teeth
Were	Wer	k1gMnSc2	Wer
All	All	k1gFnSc2	All
Exactly	Exactly	k1gMnSc2	Exactly
Alike	Alik	k1gMnSc2	Alik
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsán	k2eAgInSc1d1	napsán
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
a	a	k8xC	a
odmítnut	odmítnut	k2eAgInSc1d1	odmítnut
potenciálními	potenciální	k2eAgMnPc7d1	potenciální
vydavateli	vydavatel	k1gMnPc7	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
sporu	spor	k1gInSc6	spor
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
a	a	k8xC	a
výtvarnice	výtvarnice	k1gFnSc2	výtvarnice
a	a	k8xC	a
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
tématy	téma	k1gNnPc7	téma
nenasytnosti	nenasytnost	k1gFnSc2	nenasytnost
<g/>
,	,	kIx,	,
hořkosti	hořkost	k1gFnSc2	hořkost
<g/>
,	,	kIx,	,
pomsty	pomsta	k1gFnSc2	pomsta
a	a	k8xC	a
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Puttering	Puttering	k1gInSc1	Puttering
About	About	k1gInSc1	About
in	in	k?	in
a	a	k8xC	a
Small	Small	k1gMnSc1	Small
Land	Land	k1gMnSc1	Land
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
setkání	setkání	k1gNnSc6	setkání
dvou	dva	k4xCgInPc2	dva
manželských	manželský	k2eAgMnPc2d1	manželský
párů	pár	k1gInPc2	pár
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
počátkem	počátkem	k7c2	počátkem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Milton	Milton	k1gInSc1	Milton
Lumky	lumek	k1gMnPc4	lumek
Territory	Territor	k1gInPc1	Territor
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
and	and	k?	and
the	the	k?	the
Giant	Giant	k1gInSc1	Giant
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
<g/>
,	,	kIx,	,
napsaném	napsaný	k2eAgInSc6d1	napsaný
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1953	[number]	k4	1953
a	a	k8xC	a
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mladá	mladý	k2eAgFnSc1d1	mladá
ustrašená	ustrašený	k2eAgFnSc1d1	ustrašená
dívka	dívka	k1gFnSc1	dívka
snaží	snažit	k5eAaImIp3nS	snažit
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
neuspokojivého	uspokojivý	k2eNgInSc2d1	neuspokojivý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Humpty	Humpt	k1gInPc1	Humpt
Dumpty	Dumpta	k1gFnSc2	Dumpta
in	in	k?	in
Oakland	Oakland	k1gInSc1	Oakland
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Broken	Broken	k2eAgMnSc1d1	Broken
Bubble	Bubble	k1gMnSc1	Bubble
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
napsaný	napsaný	k2eAgInSc1d1	napsaný
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
líčí	líčit	k5eAaImIp3nS	líčit
příběh	příběh	k1gInSc1	příběh
stárnoucího	stárnoucí	k2eAgMnSc2d1	stárnoucí
a	a	k8xC	a
již	již	k6eAd1	již
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
rozvedeného	rozvedený	k2eAgInSc2d1	rozvedený
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znovu	znovu	k6eAd1	znovu
obnoví	obnovit	k5eAaPmIp3nS	obnovit
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
mlqdých	mlqdý	k2eAgMnPc2d1	mlqdý
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
.	.	kIx.	.
</s>
<s>
Gather	Gathra	k1gFnPc2	Gathra
Yourselves	Yourselvesa	k1gFnPc2	Yourselvesa
Together	Togethra	k1gFnPc2	Togethra
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Voices	Voices	k1gMnSc1	Voices
from	from	k1gMnSc1	from
the	the	k?	the
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
napsaném	napsaný	k2eAgInSc6d1	napsaný
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
propadá	propadat	k5eAaPmIp3nS	propadat
jeho	jeho	k3xOp3gMnSc1	jeho
mladý	mladý	k2eAgMnSc1d1	mladý
hrdina	hrdina	k1gMnSc1	hrdina
zoufalství	zoufalství	k1gNnSc2	zoufalství
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gInSc4	on
vedle	vedle	k7c2	vedle
neuspokojivé	uspokojivý	k2eNgFnSc2d1	neuspokojivá
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
manželství	manželství	k1gNnSc2	manželství
zradí	zradit	k5eAaPmIp3nP	zradit
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
ideály	ideál	k1gInPc4	ideál
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Selected	Selected	k1gMnSc1	Selected
Letters	Lettersa	k1gFnPc2	Lettersa
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Selected	Selected	k1gInSc1	Selected
Letters	Letters	k1gInSc1	Letters
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Dark	Dark	k1gInSc1	Dark
Haired	Haired	k1gInSc1	Haired
Girl	girl	k1gFnSc1	girl
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sbírka	sbírka	k1gFnSc1	sbírka
esejů	esej	k1gInPc2	esej
<g/>
,	,	kIx,	,
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Shifting	Shifting	k1gInSc1	Shifting
Realities	Realities	k1gMnSc1	Realities
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
filozofických	filozofický	k2eAgFnPc2d1	filozofická
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Exegesis	Exegesis	k1gFnSc1	Exegesis
of	of	k?	of
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
autorových	autorův	k2eAgInPc2d1	autorův
textů	text	k1gInPc2	text
o	o	k7c6	o
science	scienka	k1gFnSc6	scienka
fiction	fiction	k1gInSc4	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Dickova	Dickův	k2eAgFnSc1d1	Dickova
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
námětem	námět	k1gInSc7	námět
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
volné	volný	k2eAgFnPc4d1	volná
adaptace	adaptace	k1gFnPc4	adaptace
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Blade	Blade	k6eAd1	Blade
Runner	Runner	k1gInSc1	Runner
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
Sní	sníst	k5eAaPmIp3nS	sníst
androidi	android	k1gMnPc1	android
o	o	k7c6	o
elektrických	elektrický	k2eAgFnPc6d1	elektrická
ovečkách	ovečka	k1gFnPc6	ovečka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ridley	Ridlea	k1gFnSc2	Ridlea
Scott	Scotta	k1gFnPc2	Scotta
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Harrison	Harrison	k1gMnSc1	Harrison
Ford	ford	k1gInSc4	ford
Total	totat	k5eAaImAgMnS	totat
Recall	Recall	k1gMnSc1	Recall
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Zapamatujeme	zapamatovat	k5eAaPmIp1nP	zapamatovat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
za	za	k7c2	za
vás	vy	k3xPp2nPc2	vy
se	se	k3xPyFc4	se
slevou	sleva	k1gFnSc7	sleva
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Paul	Paul	k1gMnSc1	Paul
Verhoeven	Verhoeven	k2eAgMnSc1d1	Verhoeven
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
Confessions	Confessionsa	k1gFnPc2	Confessionsa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
<g />
.	.	kIx.	.
</s>
<s>
Barjo	Barjo	k1gNnSc1	Barjo
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Confessions	Confessionsa	k1gFnPc2	Confessionsa
of	of	k?	of
a	a	k8xC	a
Crap	Crap	k1gMnSc1	Crap
Artist	Artist	k1gMnSc1	Artist
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
úletú	úletú	k?	úletú
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jérôme	Jérôm	k1gInSc5	Jérôm
Boivin	Boivina	k1gFnPc2	Boivina
Screamers	Screamers	k1gInSc1	Screamers
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Vřískouni	Vřískoun	k1gMnPc1	Vřískoun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Christian	Christian	k1gMnSc1	Christian
Duguay	Duguaa	k1gFnSc2	Duguaa
<g />
.	.	kIx.	.
</s>
<s>
Impostor	Impostor	k1gInSc1	Impostor
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Podvodník	podvodník	k1gMnSc1	podvodník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Gary	Gara	k1gFnSc2	Gara
Fleder	Fledra	k1gFnPc2	Fledra
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Steven	Stevna	k1gFnPc2	Stevna
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Tom	Tom	k1gMnSc1	Tom
Cruise	Cruise	k1gFnSc1	Cruise
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
von	von	k1gInSc1	von
<g />
.	.	kIx.	.
</s>
<s>
Sydow	Sydow	k?	Sydow
Paycheck	Paycheck	k1gInSc1	Paycheck
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Výplata	výplata	k1gFnSc1	výplata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Woo	Woo	k1gMnSc1	Woo
A	a	k8xC	a
Scanner	scanner	k1gInSc1	scanner
Darkly	Darkl	k1gInPc1	Darkl
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Temný	temný	k2eAgInSc1d1	temný
obraz	obraz	k1gInSc1	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
Linklater	Linklater	k1gMnSc1	Linklater
Next	Next	k1gMnSc1	Next
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Lee	Lea	k1gFnSc3	Lea
Tamahori	Tamahore	k1gFnSc4	Tamahore
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Nicolas	Nicolasa	k1gFnPc2	Nicolasa
Cage	Cage	k1gFnSc1	Cage
Screamers	Screamers	k1gInSc1	Screamers
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Hunting	Hunting	k1gInSc1	Hunting
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Vřískouni	Vřískoun	k1gMnPc1	Vřískoun
<g/>
:	:	kIx,	:
Hon	hon	k1gInSc1	hon
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Sheldon	Sheldon	k1gMnSc1	Sheldon
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
Vřískounů	Vřískoun	k1gInPc2	Vřískoun
využívající	využívající	k2eAgInPc4d1	využívající
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
Dickovy	Dickův	k2eAgFnSc2d1	Dickova
povídky	povídka	k1gFnSc2	povídka
Druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
Radio	radio	k1gNnSc4	radio
Free	Fre	k1gMnSc2	Fre
Albemuth	Albemuth	k1gMnSc1	Albemuth
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Rádio	rádio	k1gNnSc4	rádio
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
Albemut	Albemut	k1gMnSc1	Albemut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Alan	Alan	k1gMnSc1	Alan
Simon	Simon	k1gMnSc1	Simon
The	The	k1gMnSc1	The
Adjustment	Adjustment	k1gMnSc1	Adjustment
Bureau	Bureaa	k1gFnSc4	Bureaa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Správci	správce	k1gMnPc1	správce
osudu	osud	k1gInSc2	osud
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Adjustment	Adjustment	k1gInSc1	Adjustment
Team	team	k1gInSc1	team
(	(	kIx(	(
<g/>
Úpraváři	úpravář	k1gMnPc1	úpravář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
George	Georg	k1gMnSc2	Georg
Nolfi	Nolf	k1gFnSc2	Nolf
Total	totat	k5eAaImAgInS	totat
Recall	Recall	k1gInSc1	Recall
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Zapamatujeme	zapamatovat	k5eAaPmIp1nP	zapamatovat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
za	za	k7c2	za
vás	vy	k3xPp2nPc2	vy
se	se	k3xPyFc4	se
slevou	sleva	k1gFnSc7	sleva
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Len	Lena	k1gFnPc2	Lena
Wiseman	Wiseman	k1gMnSc1	Wiseman
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Colin	Colin	k1gMnSc1	Colin
Farrell	Farrell	k1gMnSc1	Farrell
The	The	k1gMnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Crystal	Crystat	k5eAaImAgMnS	Crystat
Crypt	Crypt	k1gMnSc1	Crypt
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
krypta	krypta	k1gFnSc1	krypta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Shahab	Shahaba	k1gFnPc2	Shahaba
Zargari	Zargar	k1gFnSc2	Zargar
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
desetidílný	desetidílný	k2eAgInSc1d1	desetidílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
High	High	k1gInSc1	High
Castle	Castle	k1gFnSc1	Castle
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Vysokého	vysoký	k2eAgInSc2d1	vysoký
zámku	zámek	k1gInSc2	zámek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
dvacetidílný	dvacetidílný	k2eAgInSc1d1	dvacetidílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
společnosti	společnost	k1gFnSc2	společnost
Amazon	amazona	k1gFnPc2	amazona
Studios	Studios	k?	Studios
Twelve	Twelev	k1gFnSc2	Twelev
Monkeys	Monkeysa	k1gFnPc2	Monkeysa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Dvanáct	dvanáct	k4xCc1	dvanáct
opic	opice	k1gFnPc2	opice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,,	,,	k?	,,
režie	režie	k1gFnSc1	režie
Terry	Terra	k1gFnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
<g/>
.	.	kIx.	.
</s>
<s>
Matrix	Matrix	k1gInSc1	Matrix
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,,	,,	k?	,,
režie	režie	k1gFnSc1	režie
Larry	Larra	k1gFnSc2	Larra
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
Being	Being	k1gMnSc1	Being
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
V	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
Johna	John	k1gMnSc2	John
Malkoviche	Malkovich	k1gMnSc2	Malkovich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Spike	Spike	k1gFnSc1	Spike
Jonze	Jonze	k1gFnSc1	Jonze
<g/>
.	.	kIx.	.
</s>
<s>
Memento	memento	k1gNnSc1	memento
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolana	k1gFnPc2	Nolana
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
režie	režie	k1gFnSc1	režie
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
Eternal	Eternal	k1gMnSc1	Eternal
Sunshine	Sunshin	k1gInSc5	Sunshin
of	of	k?	of
the	the	k?	the
Spotless	Spotless	k1gInSc1	Spotless
Mind	Mind	k1gInSc1	Mind
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Věčný	věčný	k2eAgInSc1d1	věčný
svit	svit	k1gInSc1	svit
neposkvrněné	poskvrněný	k2eNgFnSc2d1	neposkvrněná
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Michel	Michela	k1gFnPc2	Michela
Gondry	Gondr	k1gMnPc4	Gondr
<g/>
.	.	kIx.	.
</s>
<s>
Inception	Inception	k1gInSc1	Inception
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Počátek	počátek	k1gInSc1	počátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolana	k1gFnPc2	Nolana
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
SFK	SFK	kA	SFK
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
samizdatové	samizdatový	k2eAgNnSc1d1	samizdatové
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
fanbook	fanbook	k1gInSc1	fanbook
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
František	František	k1gMnSc1	František
Nešpor	Nešpor	k1gMnSc1	Nešpor
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Jindra	Jindra	k1gMnSc1	Jindra
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Holan	Holan	k1gMnSc1	Holan
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Pilch	Pilch	k1gMnSc1	Pilch
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídky	povídka	k1gFnPc4	povídka
Bije	bít	k5eAaImIp3nS	bít
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
Kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
Válečná	válečný	k2eAgFnSc1d1	válečná
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Wub	Wub	k1gFnSc1	Wub
<g/>
,	,	kIx,	,
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
,	,	kIx,	,
Fostere	Foster	k1gMnSc5	Foster
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
mrtev	mrtev	k2eAgInSc1d1	mrtev
a	a	k8xC	a
Ledová	ledový	k2eAgFnSc1d1	ledová
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Vysokého	vysoký	k2eAgInSc2d1	vysoký
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
SFK	SFK	kA	SFK
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
samizdatové	samizdatový	k2eAgNnSc1d1	samizdatové
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
fanbook	fanbook	k1gInSc1	fanbook
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Pilch	Pilch	k1gMnSc1	Pilch
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
oficiálně	oficiálně	k6eAd1	oficiálně
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Blade	Blade	k6eAd1	Blade
Runner	Runner	k1gInSc1	Runner
<g/>
:	:	kIx,	:
Sní	snít	k5eAaImIp3nP	snít
androidi	android	k1gMnPc1	android
o	o	k7c6	o
elektrických	elektrický	k2eAgFnPc6d1	elektrická
ovečkách	ovečka	k1gFnPc6	ovečka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc4	Winston
Smith	Smitha	k1gFnPc2	Smitha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Linda	Linda	k1gFnSc1	Linda
Bartošková	Bartošková	k1gFnSc1	Bartošková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc4	tři
stigmata	stigma	k1gNnPc4	stigma
Palmera	Palmer	k1gMnSc2	Palmer
Eldritche	Eldritch	k1gMnSc2	Eldritch
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Pilch	Pilch	k1gMnSc1	Pilch
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
<g/>
.	.	kIx.	.
</s>
<s>
Neteleportovaný	Neteleportovaný	k2eAgMnSc1d1	Neteleportovaný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Michael	Michael	k1gMnSc1	Michael
Bronec	Bronec	k1gMnSc1	Bronec
<g/>
.	.	kIx.	.
</s>
<s>
Marsovský	marsovský	k2eAgInSc4d1	marsovský
skluz	skluz	k1gInSc4	skluz
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emil	Emil	k1gMnSc1	Emil
Labaj	Labaj	k1gMnSc1	Labaj
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Temný	temný	k2eAgInSc1d1	temný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
Talpress	Talpress	k1gInSc1	Talpress
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tschorn	Tschorn	k1gMnSc1	Tschorn
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
loterie	loterie	k1gFnSc1	loterie
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Pavel	Pavel	k1gMnSc1	Pavel
Aganov	Aganov	k1gInSc4	Aganov
<g/>
.	.	kIx.	.
</s>
<s>
Ubik	Ubik	k1gInSc1	Ubik
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Richard	Richard	k1gMnSc1	Richard
Podaný	podaný	k2eAgMnSc1d1	podaný
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
..	..	k?	..
Klany	klan	k1gInPc1	klan
alfanského	alfanský	k2eAgInSc2d1	alfanský
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emil	Emil	k1gMnSc1	Emil
Labaj	Labaj	k1gMnSc1	Labaj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Wub	Wub	k1gFnSc1	Wub
<g/>
,	,	kIx,	,
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Petr	Petr	k1gMnSc1	Petr
Holan	Holan	k1gMnSc1	Holan
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
Minority	minorita	k1gMnSc2	minorita
Report	report	k1gInSc1	report
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Helena	Helena	k1gFnSc1	Helena
Heroldová	Heroldová	k1gFnSc1	Heroldová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Andělová	Andělová	k1gFnSc1	Andělová
a	a	k8xC	a
Mirek	Mirek	k1gMnSc1	Mirek
Valina	Valina	k1gMnSc1	Valina
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stvořil	stvořit	k5eAaPmAgInS	stvořit
Jones	Jones	k1gInSc1	Jones
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Hana	Hana	k1gFnSc1	Hana
Volejníková	Volejníková	k1gFnSc1	Volejníková
<g/>
.	.	kIx.	.
</s>
<s>
Výplata	výplata	k1gFnSc1	výplata
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Černík	Černík	k1gMnSc1	Černík
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídky	povídka	k1gFnPc4	povídka
Výplata	výplata	k1gFnSc1	výplata
<g/>
,	,	kIx,	,
Chůva	chůva	k1gFnSc1	chůva
<g/>
,	,	kIx,	,
Jonův	Jonův	k2eAgInSc1d1	Jonův
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Snídaně	snídaně	k1gFnSc1	snídaně
za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
<g/>
,	,	kIx,	,
Městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
Otcova	otcův	k2eAgFnSc1d1	otcova
napodobenina	napodobenina	k1gFnSc1	napodobenina
<g/>
,	,	kIx,	,
Váhavec	váhavec	k1gMnSc1	váhavec
<g/>
,	,	kIx,	,
Autovýr	Autovýr	k1gMnSc1	Autovýr
<g/>
,	,	kIx,	,
Dny	den	k1gInPc1	den
Fešandy	fešanda	k1gFnSc2	fešanda
Pat	pata	k1gFnPc2	pata
<g/>
,	,	kIx,	,
Pohotovost	pohotovost	k1gFnSc1	pohotovost
<g/>
,	,	kIx,	,
Taková	takový	k3xDgFnSc1	takový
maličkost	maličkost	k1gFnSc1	maličkost
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
Tempunauty	Tempunaut	k2eAgInPc1d1	Tempunaut
a	a	k8xC	a
Před-lidé	Předidý	k2eAgInPc1d1	Před-lidý
<g/>
.	.	kIx.	.
</s>
<s>
Vykolejený	vykolejený	k2eAgInSc1d1	vykolejený
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
Milena	Milena	k1gFnSc1	Milena
Poláčkovi	Poláčkovi	k1gRnPc1	Poláčkovi
<g/>
.	.	kIx.	.
</s>
<s>
Počkej	počkat	k5eAaPmRp2nS	počkat
si	se	k3xPyFc3	se
na	na	k7c4	na
loňský	loňský	k2eAgInSc4d1	loňský
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc4	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tschorm	Tschorm	k1gInSc4	Tschorm
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
úletů	úlet	k1gInPc2	úlet
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
<g/>
.	.	kIx.	.
</s>
<s>
Kaňte	kanit	k5eAaImRp2nP	kanit
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gFnSc2	můj
slzy	slza	k1gFnSc2	slza
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
Laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Viktor	Viktor	k1gMnSc1	Viktor
Janiš	Janiš	k1gMnSc1	Janiš
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mrtví	mrtvý	k1gMnPc1	mrtvý
mládnou	mládnout	k5eAaImIp3nP	mládnout
<g/>
,	,	kIx,	,
Argo	Argo	k6eAd1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
Titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Radka	Radka	k1gFnSc1	Radka
Šmahelová	Šmahelová	k1gFnSc1	Šmahelová
<g/>
.	.	kIx.	.
</s>
<s>
VALIS	VALIS	kA	VALIS
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tschorm	Tschorm	k1gInSc4	Tschorm
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Vysokého	vysoký	k2eAgInSc2d1	vysoký
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tschorn	Tschorn	k1gMnSc1	Tschorn
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Převtělení	převtělení	k1gNnSc1	převtělení
Timothyho	Timothy	k1gMnSc2	Timothy
Archera	Archer	k1gMnSc2	Archer
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tschorn	Tschorn	k1gMnSc1	Tschorn
<g/>
.	.	kIx.	.
</s>
<s>
Božská	božský	k2eAgFnSc1d1	božská
invaze	invaze	k1gFnSc1	invaze
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
<g/>
.	.	kIx.	.
</s>
<s>
Rádio	rádio	k1gNnSc1	rádio
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
Albemut	Albemut	k1gMnSc1	Albemut
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Aleš	Aleš	k1gMnSc1	Aleš
Heinz	Heinz	k1gMnSc1	Heinz
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krvemsta	Krvemsta	k1gMnSc1	Krvemsta
aneb	aneb	k?	aneb
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
vedlo	vést	k5eAaImAgNnS	vést
po	po	k7c6	po
bombě	bomba	k1gFnSc6	bomba
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc4	tři
stigmata	stigma	k1gNnPc4	stigma
Palmera	Palmer	k1gMnSc2	Palmer
Eldritche	Eldritch	k1gMnSc2	Eldritch
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emil	Emil	k1gMnSc1	Emil
Labaj	Labaj	k1gMnSc1	Labaj
<g/>
.	.	kIx.	.
</s>
<s>
Deus	Deus	k6eAd1	Deus
Irae	Irae	k1gFnSc1	Irae
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
<g/>
.	.	kIx.	.
</s>
<s>
Podivný	podivný	k2eAgInSc1d1	podivný
ráj	ráj	k1gInSc1	ráj
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
a	a	k8xC	a
Štěpán	Štěpán	k1gMnSc1	Štěpán
Valášek	Valášek	k1gMnSc1	Valášek
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Valášek	Valášek	k1gMnSc1	Valášek
<g/>
.	.	kIx.	.
</s>
<s>
Předposlední	předposlední	k2eAgFnSc1d1	předposlední
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
<g/>
.	.	kIx.	.
</s>
<s>
Minority	minorita	k1gFnPc1	minorita
Report	report	k1gInSc1	report
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Robert	Robert	k1gMnSc1	Robert
Hýsek	Hýsek	k1gMnSc1	Hýsek
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Večerková	Večerková	k1gFnSc1	Večerková
<g/>
,	,	kIx,	,
Veronika	Veronika	k1gFnSc1	Veronika
Austová	Austová	k1gFnSc1	Austová
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
Jirošová	Jirošová	k1gFnSc1	Jirošová
<g/>
,	,	kIx,	,
Taťána	Taťána	k1gFnSc1	Taťána
Ochmanová	Ochmanová	k1gFnSc1	Ochmanová
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
Pala	Pala	k1gMnSc1	Pala
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Pavlisová	Pavlisový	k2eAgFnSc1d1	Pavlisová
<g/>
,	,	kIx,	,
Alexandra	Alexandra	k1gFnSc1	Alexandra
Pokorná	Pokorná	k1gFnSc1	Pokorná
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Roztočil	Roztočil	k1gMnSc1	Roztočil
a	a	k8xC	a
Vladislava	Vladislava	k1gFnSc1	Vladislava
Vaněčková	Vaněčková	k1gFnSc1	Vaněčková
<g/>
.	.	kIx.	.
</s>
<s>
Dokážeme	dokázat	k5eAaPmIp1nP	dokázat
vás	vy	k3xPp2nPc4	vy
stvořit	stvořit	k5eAaPmF	stvořit
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
<g/>
.	.	kIx.	.
</s>
