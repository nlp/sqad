<s>
Gianluigi	Gianluigi	k6eAd1	Gianluigi
Buffon	Buffon	k1gInSc1	Buffon
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Carrara	Carrara	k1gFnSc1	Carrara
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
klubu	klub	k1gInSc6	klub
Juventus	Juventus	k1gInSc1	Juventus
Turín	Turín	k1gInSc1	Turín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
jediným	jediný	k2eAgNnSc7d1	jediné
předchozím	předchozí	k2eAgNnSc7d1	předchozí
působištěm	působiště	k1gNnSc7	působiště
byl	být	k5eAaImAgInS	být
italský	italský	k2eAgInSc1d1	italský
klub	klub	k1gInSc1	klub
Parma	Parma	k1gFnSc1	Parma
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
italského	italský	k2eAgInSc2d1	italský
národního	národní	k2eAgInSc2d1	národní
týmu	tým	k1gInSc2	tým
odehrál	odehrát	k5eAaPmAgInS	odehrát
nejvíce	nejvíce	k6eAd1	nejvíce
utkání	utkání	k1gNnSc4	utkání
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
fotbalistů	fotbalista	k1gMnPc2	fotbalista
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
167	[number]	k4	167
k	k	k7c3	k
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pelé	Pelé	k1gNnSc1	Pelé
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jej	on	k3xPp3gInSc4	on
IFFHS	IFFHS	kA	IFFHS
zvolila	zvolit	k5eAaPmAgFnS	zvolit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1997	[number]	k4	1997
až	až	k6eAd1	až
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
seniorské	seniorský	k2eAgNnSc4d1	seniorské
mužstvo	mužstvo	k1gNnSc4	mužstvo
Parmy	Parma	k1gFnSc2	Parma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Juventusu	Juventus	k1gInSc2	Juventus
Turín	Turín	k1gInSc1	Turín
za	za	k7c4	za
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
nejdražším	drahý	k2eAgMnSc7d3	nejdražší
brankářem	brankář	k1gMnSc7	brankář
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
neopustil	opustit	k5eNaPmAgInS	opustit
ani	ani	k8xC	ani
po	po	k7c6	po
korupční	korupční	k2eAgFnSc6d1	korupční
aféře	aféra	k1gFnSc6	aféra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yQgFnSc3	který
Juventus	Juventus	k1gInSc1	Juventus
sestoupil	sestoupit	k5eAaPmAgInS	sestoupit
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B.	B.	kA	B.
S	s	k7c7	s
Juventusem	Juventus	k1gInSc7	Juventus
získal	získat	k5eAaPmAgMnS	získat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
trofejí	trofej	k1gFnPc2	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgNnPc7d1	jiné
7	[number]	k4	7
ligových	ligový	k2eAgInPc2d1	ligový
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
titul	titul	k1gInSc1	titul
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
italského	italský	k2eAgInSc2d1	italský
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
5	[number]	k4	5
<g/>
x	x	k?	x
italský	italský	k2eAgInSc4d1	italský
superpohár	superpohár	k1gInSc4	superpohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
dokonce	dokonce	k9	dokonce
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bohužel	bohužel	k6eAd1	bohužel
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnPc4	vítěz
nejprestižnější	prestižní	k2eAgFnSc2d3	nejprestižnější
evropské	evropský	k2eAgFnSc2d1	Evropská
soutěže	soutěž	k1gFnSc2	soutěž
nezískal	získat	k5eNaPmAgMnS	získat
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
za	za	k7c2	za
italské	italský	k2eAgFnSc2d1	italská
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
reprezentace	reprezentace	k1gFnSc2	reprezentace
od	od	k7c2	od
kategorie	kategorie	k1gFnSc2	kategorie
U	u	k7c2	u
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Itálie	Itálie	k1gFnSc2	Itálie
debutoval	debutovat	k5eAaBmAgInS	debutovat
29	[number]	k4	29
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgNnSc6d1	kvalifikační
utkání	utkání	k1gNnSc6	utkání
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
proti	proti	k7c3	proti
reprezentaci	reprezentace	k1gFnSc3	reprezentace
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
MS	MS	kA	MS
1998	[number]	k4	1998
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2002	[number]	k4	2002
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Koreji	Korea	k1gFnSc3	Korea
<g/>
,	,	kIx,	,
EURA	euro	k1gNnSc2	euro
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
EURA	euro	k1gNnSc2	euro
2008	[number]	k4	2008
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
EURA	euro	k1gNnSc2	euro
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
EURA	euro	k1gNnSc2	euro
2016	[number]	k4	2016
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
2006	[number]	k4	2006
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Gianluigi	Gianluig	k1gFnSc2	Gianluig
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Itálie	Itálie	k1gFnSc2	Itálie
zářil	zářit	k5eAaImAgMnS	zářit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
turnaj	turnaj	k1gInSc4	turnaj
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pokutového	pokutový	k2eAgNnSc2d1	pokutové
koupu	koupat	k5eAaImIp1nS	koupat
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
jako	jako	k9	jako
vlastní	vlastní	k2eAgInSc1d1	vlastní
gól	gól	k1gInSc1	gól
od	od	k7c2	od
spoluhráče	spoluhráč	k1gMnSc2	spoluhráč
Cristiana	Cristian	k1gMnSc2	Cristian
Zaccarda	Zaccard	k1gMnSc2	Zaccard
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
týmy	tým	k1gInPc4	tým
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Ghany	Ghana	k1gFnPc1	Ghana
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
týmy	tým	k1gInPc1	tým
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
v	v	k7c4	v
play-off	playff	k1gInSc4	play-off
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
Italové	Ital	k1gMnPc1	Ital
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
vyrovnaných	vyrovnaný	k2eAgInPc6d1	vyrovnaný
dvou	dva	k4xCgInPc6	dva
poločasech	poločas	k1gInPc6	poločas
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
muselo	muset	k5eAaImAgNnS	muset
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
byli	být	k5eAaImAgMnP	být
Italové	Ital	k1gMnPc1	Ital
velmi	velmi	k6eAd1	velmi
unavení	unavený	k2eAgMnPc1d1	unavený
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
Francouzi	Francouz	k1gMnPc1	Francouz
často	často	k6eAd1	často
využívali	využívat	k5eAaImAgMnP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Gigi	Gigi	k6eAd1	Gigi
ovšem	ovšem	k9	ovšem
nádherným	nádherný	k2eAgInSc7d1	nádherný
zákrokem	zákrok	k1gInSc7	zákrok
proti	proti	k7c3	proti
hlavičce	hlavička	k1gFnSc3	hlavička
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hvězdy	hvězda	k1gFnSc2	hvězda
Zinedine	Zinedin	k1gInSc5	Zinedin
Zidana	Zidana	k1gFnSc1	Zidana
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
podržel	podržet	k5eAaPmAgMnS	podržet
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
až	až	k6eAd1	až
do	do	k7c2	do
penaltového	penaltový	k2eAgInSc2d1	penaltový
rozstřelu	rozstřel	k1gInSc2	rozstřel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
nakonec	nakonec	k6eAd1	nakonec
Italové	Ital	k1gMnPc1	Ital
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získali	získat	k5eAaPmAgMnP	získat
počtvrté	počtvrté	k4xO	počtvrté
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Buffon	Buffon	k1gInSc1	Buffon
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
EURA	euro	k1gNnPc4	euro
2012	[number]	k4	2012
a	a	k8xC	a
odchytal	odchytat	k5eAaPmAgInS	odchytat
všech	všecek	k3xTgNnPc2	všecek
šest	šest	k4xCc1	šest
utkání	utkání	k1gNnPc2	utkání
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
zápasy	zápas	k1gInPc1	zápas
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Irskem	Irsko	k1gNnSc7	Irsko
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
Itálie	Itálie	k1gFnSc2	Itálie
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
odchytal	odchytat	k5eAaPmAgInS	odchytat
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
až	až	k9	až
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
pro	pro	k7c4	pro
Italy	Ital	k1gMnPc4	Ital
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
gól	gól	k1gInSc4	gól
až	až	k9	až
v	v	k7c6	v
nastaveném	nastavený	k2eAgInSc6d1	nastavený
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
92	[number]	k4	92
<g/>
.	.	kIx.	.
minuta	minuta	k1gFnSc1	minuta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
porazila	porazit	k5eAaPmAgFnS	porazit
mírně	mírně	k6eAd1	mírně
favorizované	favorizovaný	k2eAgNnSc4d1	favorizované
Německo	Německo	k1gNnSc4	Německo
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
týmu	tým	k1gInSc6	tým
turnaje	turnaj	k1gInSc2	turnaj
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
hbití	hbitý	k2eAgMnPc1d1	hbitý
Španělé	Španěl	k1gMnPc1	Španěl
jej	on	k3xPp3gMnSc4	on
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
překonali	překonat	k5eAaPmAgMnP	překonat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
sami	sám	k3xTgMnPc1	sám
inkasovali	inkasovat	k5eAaBmAgMnP	inkasovat
<g/>
.	.	kIx.	.
</s>
<s>
Gianluigi	Gianluigi	k6eAd1	Gianluigi
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
týmem	tým	k1gInSc7	tým
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Carrara	Carrara	k1gFnSc1	Carrara
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
sportovně	sportovně	k6eAd1	sportovně
založená	založený	k2eAgFnSc1d1	založená
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Maria	Maria	k1gFnSc1	Maria
Stella	Stella	k1gFnSc1	Stella
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
disciplíně	disciplína	k1gFnSc6	disciplína
hod	hod	k1gInSc4	hod
s	s	k7c7	s
diskem	disk	k1gInSc7	disk
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Adriano	Adriana	k1gFnSc5	Adriana
byl	být	k5eAaImAgInS	být
vzpěrač	vzpěrač	k1gMnSc1	vzpěrač
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
Veronica	Veronic	k2eAgFnSc1d1	Veronica
a	a	k8xC	a
Guendalina	Guendalina	k1gFnSc1	Guendalina
hrály	hrát	k5eAaImAgFnP	hrát
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Brankářská	brankářský	k2eAgFnSc1d1	brankářská
legenda	legenda	k1gFnSc1	legenda
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Buffon	Buffon	k1gInSc4	Buffon
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
spřízněna	spříznit	k5eAaPmNgFnS	spříznit
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
Lorenzo	Lorenza	k1gFnSc5	Lorenza
byl	být	k5eAaImAgInS	být
bratrancem	bratranec	k1gMnSc7	bratranec
jeho	on	k3xPp3gMnSc2	on
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
českou	český	k2eAgFnSc7d1	Česká
modelkou	modelka	k1gFnSc7	modelka
Alenou	Alena	k1gFnSc7	Alena
Šeredovou	šeredův	k2eAgFnSc7d1	šeredův
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Luise	Luisa	k1gFnSc6	Luisa
Thomase	Thomas	k1gMnSc4	Thomas
a	a	k8xC	a
Davida	David	k1gMnSc4	David
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
ve	v	k7c6	v
vyšehradské	vyšehradský	k2eAgFnSc6d1	Vyšehradská
Bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
již	již	k6eAd1	již
rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
<g/>
.	.	kIx.	.
</s>
<s>
Parma	Parma	k1gFnSc1	Parma
FC	FC	kA	FC
Pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
Coppa	Coppa	k1gFnSc1	Coppa
Italia	Italia	k1gFnSc1	Italia
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
Supercoppa	Supercoppa	k1gFnSc1	Supercoppa
italiana	italiana	k1gFnSc1	italiana
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1999	[number]	k4	1999
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
Serie	serie	k1gFnSc2	serie
A	A	kA	A
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
16	[number]	k4	16
Serie	serie	k1gFnSc1	serie
B	B	kA	B
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
Coppa	Copp	k1gMnSc2	Copp
Italia	Italius	k1gMnSc2	Italius
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
Supercoppa	Supercoppa	k1gFnSc1	Supercoppa
italiana	italiana	k1gFnSc1	italiana
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2002	[number]	k4	2002
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
Itálie	Itálie	k1gFnSc1	Itálie
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1996	[number]	k4	1996
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2006	[number]	k4	2006
Mistrovství	mistrovství	k1gNnPc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
:	:	kIx,	:
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
brankář	brankář	k1gMnSc1	brankář
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
IFFHS	IFFHS	kA	IFFHS
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
člen	člen	k1gInSc1	člen
FIFA	FIFA	kA	FIFA
100	[number]	k4	100
UEFA	UEFA	kA	UEFA
Club	club	k1gInSc1	club
Footballer	Footballer	k1gInSc1	Footballer
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
All-Stars	All-Stars	k1gInSc1	All-Stars
tým	tým	k1gInSc1	tým
MS	MS	kA	MS
2006	[number]	k4	2006
</s>
