<s>
Sesuvové	Sesuvový	k2eAgNnSc1d1	Sesuvový
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sesuvu	sesuv	k1gInSc2	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kamení	kamení	k1gNnSc1	kamení
popř.	popř.	kA	popř.
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
