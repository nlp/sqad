<p>
<s>
Sesuvové	Sesuvový	k2eAgNnSc1d1	Sesuvový
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sesuvu	sesuv	k1gInSc2	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kamení	kamení	k1gNnSc1	kamení
popř.	popř.	kA	popř.
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
sesuvu	sesuv	k1gInSc3	sesuv
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
například	například	k6eAd1	například
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
velkých	velký	k2eAgFnPc2d1	velká
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
nebo	nebo	k8xC	nebo
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Sesutý	sesutý	k2eAgInSc1d1	sesutý
materiál	materiál	k1gInSc1	materiál
zahradí	zahradit	k5eAaPmIp3nS	zahradit
dolinu	dolina	k1gFnSc4	dolina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
nebo	nebo	k8xC	nebo
potok	potok	k1gInSc1	potok
a	a	k8xC	a
za	za	k7c7	za
takto	takto	k6eAd1	takto
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
hrází	hráz	k1gFnSc7	hráz
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
–	–	k?	–
Modrac	Modrac	k1gFnSc1	Modrac
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Odlezelské	Odlezelský	k2eAgNnSc1d1	Odlezelské
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
–	–	k?	–
Amtkel	Amtkela	k1gFnPc2	Amtkela
</s>
</p>
<p>
<s>
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
–	–	k?	–
Duszatyńskie	Duszatyńskie	k1gFnSc2	Duszatyńskie
</s>
</p>
<p>
<s>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
v	v	k7c6	v
Levočských	levočský	k2eAgInPc6d1	levočský
vrších	vrch	k1gInPc6	vrch
–	–	k?	–
Baňur	Baňura	k1gFnPc2	Baňura
</s>
</p>
<p>
<s>
ve	v	k7c6	v
Slanských	Slanský	k2eAgInPc6d1	Slanský
vrších	vrch	k1gInPc6	vrch
–	–	k?	–
Izra	Izr	k1gInSc2	Izr
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Izra	Izra	k1gFnSc1	Izra
</s>
</p>
<p>
<s>
ve	v	k7c6	v
Spišské	spišský	k2eAgFnSc6d1	Spišská
Maguře	Magura	k1gFnSc6	Magura
–	–	k?	–
Jezerské	Jezerský	k2eAgNnSc1d1	Jezerský
jazero	jazero	k1gNnSc1	jazero
<g/>
,	,	kIx,	,
Ksenino	Ksenin	k2eAgNnSc1d1	Ksenin
jazierko	jazierka	k1gFnSc5	jazierka
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc4d1	Malé
jazero	jazero	k1gNnSc4	jazero
<g/>
,	,	kIx,	,
Osturnianske	Osturnianske	k1gNnSc4	Osturnianske
jazero	jazero	k1gNnSc1	jazero
<g/>
,	,	kIx,	,
Veľké	Veľký	k2eAgNnSc1d1	Veľké
jazero	jazero	k1gNnSc1	jazero
</s>
</p>
<p>
<s>
ve	v	k7c6	v
Vihorlatských	vihorlatský	k2eAgFnPc6d1	vihorlatský
vrších	vrš	k1gFnPc6	vrš
–	–	k?	–
Kotlík	kotlík	k1gInSc1	kotlík
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc1d1	Malé
Morské	Morský	k2eAgNnSc1d1	Morský
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
Morské	Morský	k2eAgNnSc1d1	Morský
oko	oko	k1gNnSc1	oko
</s>
</p>
<p>
<s>
v	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Tatrách	Tatra	k1gFnPc6	Tatra
–	–	k?	–
Babkové	Babková	k1gFnSc2	Babková
pliesko	pliesko	k6eAd1	pliesko
</s>
</p>
<p>
<s>
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
–	–	k?	–
Jašilkul	Jašilkul	k1gInSc1	Jašilkul
<g/>
,	,	kIx,	,
Sarezské	Sarezský	k2eAgNnSc1d1	Sarezské
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
–	–	k?	–
Ozirce	Ozirce	k1gMnSc4	Ozirce
<g/>
,	,	kIx,	,
Synevyr	Synevyr	k1gMnSc1	Synevyr
</s>
</p>
