<s desamb="1">
Stoupání	stoupání	k1gNnSc1
mu	on	k3xPp3gMnSc3
ale	ale	k8xC
činí	činit	k5eAaImIp3nS
obtíže	obtíž	k1gFnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
není	být	k5eNaImIp3nS
často	často	k6eAd1
vidět	vidět	k5eAaImF
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
velbloudi	velbloud	k1gMnPc1
dvouhrbí	dvouhrbý	k2eAgMnPc1d1
někdy	někdy	k6eAd1
do	do	k7c2
hor	hora	k1gFnPc2
pronikají	pronikat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>