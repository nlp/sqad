<s>
Velbloud	velbloud	k1gMnSc1
</s>
<s>
Velbloud	velbloud	k1gMnSc1
velbloud	velbloud	k1gMnSc1
jednohrbý	jednohrbý	k2eAgMnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
sudokopytníci	sudokopytník	k1gMnPc1
(	(	kIx(
<g/>
Artiodactyla	Artiodactyla	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
velbloudovití	velbloudovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Camelidae	Camelidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
velbloud	velbloud	k1gMnSc1
(	(	kIx(
<g/>
Camelus	Camelus	k1gInSc1
<g/>
)	)	kIx)
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
</s>
<s>
Původ	původ	k1gInSc1
<g/>
,	,	kIx,
migrace	migrace	k1gFnSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Původ	původ	k1gInSc1
<g/>
,	,	kIx,
migrace	migrace	k1gFnSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velbloud	velbloud	k1gMnSc1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
velbloudovitých	velbloudovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Camelidae	Camelidae	k1gFnPc2
<g/>
)	)	kIx)
zahrnující	zahrnující	k2eAgInSc4d1
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
sudokopytníků	sudokopytník	k1gMnPc2
<g/>
:	:	kIx,
velblouda	velbloud	k1gMnSc2
dvouhrbého	dvouhrbý	k2eAgNnSc2d1
divokého	divoký	k2eAgNnSc2d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
ferus	ferus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velblouda	velbloud	k1gMnSc2
dvouhrbého	dvouhrbý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gInSc1
bactrianus	bactrianus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvaného	zvaný	k2eAgInSc2d1
též	též	k9
drabař	drabař	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
velblouda	velbloud	k1gMnSc2
jednohrbého	jednohrbý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Camelus	Camelus	k1gInSc1
dromedarius	dromedarius	k1gInSc1
<g/>
)	)	kIx)
neboli	neboli	k8xC
dromedára	dromedár	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
druh	druh	k1gInSc4
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
<g/>
,	,	kIx,
zbylé	zbylý	k2eAgInPc1d1
dva	dva	k4xCgInPc1
jsou	být	k5eAaImIp3nP
chovány	chovat	k5eAaImNgInP
člověkem	člověk	k1gMnSc7
s	s	k7c7
různou	různý	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
volnosti	volnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
velbloud	velbloud	k1gMnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
zajímavou	zajímavý	k2eAgFnSc4d1
etymologii	etymologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
jej	on	k3xPp3gMnSc4
často	často	k6eAd1
spojují	spojovat	k5eAaImIp3nP
s	s	k7c7
adjektivem	adjektivum	k1gNnSc7
velký	velký	k2eAgInSc1d1
a	a	k8xC
slovy	slovo	k1gNnPc7
bloud	bloud	k1gMnSc1
<g/>
,	,	kIx,
bloudit	bloudit	k5eAaImF
či	či	k8xC
obluda	obluda	k1gFnSc1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
zcela	zcela	k6eAd1
mylnou	mylný	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
etymologii	etymologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
do	do	k7c2
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
dostalo	dostat	k5eAaPmAgNnS
v	v	k7c6
období	období	k1gNnSc6
Stěhování	stěhování	k1gNnPc2
národů	národ	k1gInPc2
z	z	k7c2
gótštiny	gótština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
gótské	gótský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
ulbandus	ulbandus	k1gInSc1
<g/>
,	,	kIx,
odvozené	odvozený	k2eAgInPc1d1
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
názvu	název	k1gInSc2
slona	slon	k1gMnSc2
elefas	elefasa	k1gFnPc2
(	(	kIx(
<g/>
genitiv	genitiv	k1gInSc1
-antos	-antos	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gótové	Gót	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Černomoří	Černomoří	k1gNnSc2
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
setkávali	setkávat	k5eAaImAgMnP
s	s	k7c7
východními	východní	k2eAgMnPc7d1
kočovníky	kočovník	k1gMnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
Alany	Alan	k1gMnPc4
a	a	k8xC
Huny	Hun	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
chovali	chovat	k5eAaImAgMnP
velbloudy	velbloud	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gótové	Gót	k1gMnPc1
pro	pro	k7c4
toto	tento	k3xDgNnSc4
zvíře	zvíře	k1gNnSc4
neměli	mít	k5eNaImAgMnP
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
prostřednictvím	prostřednictvím	k7c2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
znali	znát	k5eAaImAgMnP
slony	slon	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenesení	přenesení	k1gNnSc1
názvu	název	k1gInSc2
slona	slon	k1gMnSc2
na	na	k7c6
velblouda	velbloud	k1gMnSc2
bylo	být	k5eAaImAgNnS
zřejmě	zřejmě	k6eAd1
motivováno	motivovat	k5eAaBmNgNnS
setkáním	setkání	k1gNnSc7
s	s	k7c7
velkými	velký	k2eAgFnPc7d1
neznámými	známý	k2eNgFnPc7d1
dopravními	dopravní	k2eAgFnPc7d1
zvířaty	zvíře	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Slovenské	slovenský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
ťava	ťav	k1gInSc2
má	mít	k5eAaImIp3nS
turkický	turkický	k2eAgInSc4d1
původ	původ	k1gInSc4
a	a	k8xC
do	do	k7c2
slovenštiny	slovenština	k1gFnSc2
proniklo	proniknout	k5eAaPmAgNnS
přes	přes	k7c4
maďarštinu	maďarština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
Camelus	Camelus	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
semitských	semitský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
–	–	k?
např.	např.	kA
v	v	k7c6
hebrejštině	hebrejština	k1gFnSc6
a	a	k8xC
aramejštině	aramejština	k1gFnSc6
gamal	gamal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
dromedár	dromedár	k1gMnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Velbloudi	velbloud	k1gMnPc1
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
vysocí	vysoký	k2eAgMnPc1d1
<g/>
,	,	kIx,
statní	statný	k2eAgMnPc1d1
přežvýkavci	přežvýkavec	k1gMnPc1
s	s	k7c7
malou	malý	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
na	na	k7c6
mohutném	mohutný	k2eAgInSc6d1
krku	krk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosahují	dosahovat	k5eAaImIp3nP
výšky	výška	k1gFnPc1
do	do	k7c2
dvou	dva	k4xCgNnPc2
a	a	k8xC
půl	půl	k1xP
metru	metr	k1gInSc2
<g/>
,	,	kIx,
délky	délka	k1gFnSc2
do	do	k7c2
tří	tři	k4xCgNnPc2
a	a	k8xC
půl	půl	k6eAd1
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
krku	krk	k1gInSc6
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
hřívy	hříva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloudi	velbloud	k1gMnPc1
jsou	být	k5eAaImIp3nP
zásadně	zásadně	k6eAd1
býložraví	býložravý	k2eAgMnPc1d1
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
vybíraví	vybíravý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Divoký	divoký	k2eAgMnSc1d1
velbloud	velbloud	k1gMnSc1
(	(	kIx(
<g/>
Camelus	Camelus	k1gMnSc1
ferus	ferus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Velbloudi	velbloud	k1gMnPc1
pochází	pocházet	k5eAaImIp3nP
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
lama	lama	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
asi	asi	k9
15	#num#	k4
milionů	milion	k4xCgInPc2
domácích	domácí	k2eAgMnPc2d1
velbloudů	velbloud	k1gMnPc2
jednohrbých	jednohrbý	k2eAgInPc2d1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
západní	západní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
najdeme	najít	k5eAaPmIp1nP
asi	asi	k9
25	#num#	k4
tisíc	tisíc	k4xCgInPc2
zdivočelých	zdivočelý	k2eAgMnPc2d1
dromedárů	dromedár	k1gMnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
až	až	k9
700	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
potomků	potomek	k1gMnPc2
velbloudů	velbloud	k1gMnPc2
dovezených	dovezený	k2eAgMnPc2d1
z	z	k7c2
Arábie	Arábie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Velbloudů	velbloud	k1gMnPc2
dvouhrbých	dvouhrbý	k2eAgMnPc2d1
se	se	k3xPyFc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
chová	chovat	k5eAaImIp3nS
asi	asi	k9
1,5	1,5	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mongolských	mongolský	k2eAgFnPc6d1
pouštích	poušť	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
asi	asi	k9
1000	#num#	k4
jedinců	jedinec	k1gMnPc2
divokých	divoký	k2eAgInPc2d1
velbloudů	velbloud	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářské	hospodářský	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Velbloudi	velbloud	k1gMnPc1
jsou	být	k5eAaImIp3nP
pověstní	pověstný	k2eAgMnPc1d1
svou	svůj	k3xOyFgFnSc7
schopností	schopnost	k1gFnSc7
přetrvávat	přetrvávat	k5eAaImF
dlouho	dlouho	k6eAd1
bez	bez	k7c2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloud	velbloud	k1gMnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
především	především	k9
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
břemen	břemeno	k1gNnPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
také	také	k9
pro	pro	k7c4
maso	maso	k1gNnSc4
a	a	k8xC
mléko	mléko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
ale	ale	k9
i	i	k9
k	k	k7c3
jízdě	jízda	k1gFnSc3
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloudí	velbloudí	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
značné	značný	k2eAgFnPc4d1
kvality	kvalita	k1gFnPc4
zejména	zejména	k9
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užitečný	užitečný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
i	i	k9
velbloudí	velbloudí	k2eAgInSc1d1
trus	trus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
jako	jako	k8xC,k8xS
palivo	palivo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
domestifikovali	domestifikovat	k5eAaImAgMnP,k5eAaBmAgMnP,k5eAaPmAgMnP
velbloudy	velbloud	k1gMnPc4
před	před	k7c7
mnoha	mnoho	k4c7
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jejich	jejich	k3xOp3gMnPc1
divocí	divoký	k2eAgMnPc1d1
předkové	předek	k1gMnPc1
nejsou	být	k5eNaImIp3nP
známi	znám	k2eAgMnPc1d1
(	(	kIx(
<g/>
určitě	určitě	k6eAd1
se	se	k3xPyFc4
však	však	k9
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
recentní	recentní	k2eAgInSc4d1
Camelus	Camelus	k1gInSc4
ferus	ferus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvouhrbý	dvouhrbý	k2eAgInSc1d1
velbloud	velbloud	k1gMnSc1
byl	být	k5eAaImAgInS
v	v	k7c4
domestikován	domestikován	k2eAgInSc4d1
před	před	k7c4
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
dromedár	dromedár	k1gMnSc1
až	až	k9
před	před	k7c7
3	#num#	k4
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
Brehm	Brehm	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
starověkým	starověký	k2eAgMnPc3d1
Egypťanům	Egypťan	k1gMnPc3
sloužil	sloužit	k5eAaImAgInS
již	již	k6eAd1
od	od	k7c2
dob	doba	k1gFnPc2
Nové	Nové	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
je	on	k3xPp3gNnSc4
zřejmě	zřejmě	k6eAd1
od	od	k7c2
semitských	semitský	k2eAgMnPc2d1
kočovníků	kočovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypťané	Egypťan	k1gMnPc1
prý	prý	k9
také	také	k9
naučili	naučit	k5eAaPmAgMnP
velblouda	velbloud	k1gMnSc4
i	i	k8xC
jednoduchému	jednoduchý	k2eAgInSc3d1
tanci	tanec	k1gInSc3
zvanému	zvaný	k2eAgInSc3d1
kenken	kenken	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bibli	bible	k1gFnSc6
je	být	k5eAaImIp3nS
velbloud	velbloud	k1gMnSc1
často	často	k6eAd1
zmiňován	zmiňován	k2eAgMnSc1d1
<g/>
,	,	kIx,
přestože	přestože	k8xS
jej	on	k3xPp3gMnSc4
Izraelité	izraelita	k1gMnPc1
pokládali	pokládat	k5eAaImAgMnP
za	za	k7c4
nečisté	čistý	k2eNgNnSc4d1
zvíře	zvíře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
Arabové	Arab	k1gMnPc1
velblouda	velbloud	k1gMnSc2
za	za	k7c4
nečistého	nečistý	k1gMnSc4
nepokládají	pokládat	k5eNaImIp3nP
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
maso	maso	k1gNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
muslimy	muslim	k1gMnPc4
halal	halal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zakoupila	zakoupit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1856	#num#	k4
ve	v	k7c6
Smyrně	Smyrna	k1gFnSc6
75	#num#	k4
velbloudů	velbloud	k1gMnPc2
se	s	k7c7
záměrem	záměr	k1gInSc7
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
vojenské	vojenský	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
americké	americký	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
sloužili	sloužit	k5eAaImAgMnP
na	na	k7c6
straně	strana	k1gFnSc6
Konfederace	konfederace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
zájem	zájem	k1gInSc1
armády	armáda	k1gFnSc2
opadl	opadnout	k5eAaPmAgInS
a	a	k8xC
dromedáři	dromedár	k1gMnPc1
byli	být	k5eAaImAgMnP
dílem	dílem	k6eAd1
prodáni	prodán	k2eAgMnPc1d1
<g/>
,	,	kIx,
dílem	dílem	k6eAd1
zdivočeli	zdivočet	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
přežili	přežít	k5eAaPmAgMnP
asi	asi	k9
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloudi	velbloud	k1gMnPc1
jednohrbí	jednohrbý	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
také	také	k9
<g/>
,	,	kIx,
s	s	k7c7
mnoha	mnoho	k4c7
jinými	jiný	k2eAgMnPc7d1
domácími	domácí	k1gMnPc7
zvířaty	zvíře	k1gNnPc7
(	(	kIx(
<g/>
králík	králík	k1gMnSc1
<g/>
,	,	kIx,
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
kočka	kočka	k1gFnSc1
<g/>
,	,	kIx,
kůň	kůň	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
introdukováni	introdukován	k2eAgMnPc1d1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Velbloud	velbloud	k1gMnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
rychlý	rychlý	k2eAgInSc1d1
a	a	k8xC
vytrvalý	vytrvalý	k2eAgMnSc1d1
běžec	běžec	k1gMnSc1
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
uběhnout	uběhnout	k5eAaPmF
až	až	k9
175	#num#	k4
km	km	kA
za	za	k7c4
12	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoupání	stoupání	k1gNnSc1
mu	on	k3xPp3gMnSc3
ale	ale	k8xC
činí	činit	k5eAaImIp3nS
obtíže	obtíž	k1gFnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
není	být	k5eNaImIp3nS
často	často	k6eAd1
vidět	vidět	k5eAaImF
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
velbloudi	velbloud	k1gMnPc1
dvouhrbí	dvouhrbý	k2eAgMnPc1d1
někdy	někdy	k6eAd1
do	do	k7c2
hor	hora	k1gFnPc2
pronikají	pronikat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloudi	velbloud	k1gMnPc1
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
používali	používat	k5eAaImAgMnP
hlavně	hlavně	k9
k	k	k7c3
dopravě	doprava	k1gFnSc3
nákladů	náklad	k1gInPc2
a	a	k8xC
k	k	k7c3
jízdě	jízda	k1gFnSc3
<g/>
,	,	kIx,
méně	málo	k6eAd2
k	k	k7c3
tahu	tah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
pouštních	pouštní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Afriky	Afrika	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
šíří	šířit	k5eAaImIp3nS
užívání	užívání	k1gNnSc1
terénních	terénní	k2eAgNnPc2d1
a	a	k8xC
nákladních	nákladní	k2eAgNnPc2d1
aut	auto	k1gNnPc2
<g/>
,	,	kIx,
však	však	k9
význam	význam	k1gInSc1
velbloudů	velbloud	k1gMnPc2
v	v	k7c6
dopravě	doprava	k1gFnSc6
pomalu	pomalu	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Maso	maso	k1gNnSc1
mladých	mladý	k2eAgMnPc2d1
velbloudů	velbloud	k1gMnPc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
největší	veliký	k2eAgFnSc4d3
lahůdku	lahůdka	k1gFnSc4
zvláště	zvláště	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
a	a	k8xC
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
pro	pro	k7c4
maso	maso	k1gNnSc4
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
spíše	spíše	k9
velbloudi	velbloud	k1gMnPc1
jednohrbí	jednohrbý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
velbloudi	velbloud	k1gMnPc1
dvouhrbí	dvouhrbý	k2eAgMnPc1d1
<g/>
,	,	kIx,
velbloudí	velbloudí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
obvykle	obvykle	k6eAd1
není	být	k5eNaImIp3nS
konzumováno	konzumován	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
velbloudi	velbloud	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
pojmenováni	pojmenován	k2eAgMnPc1d1
a	a	k8xC
majitelé	majitel	k1gMnPc1
k	k	k7c3
nim	on	k3xPp3gFnPc3
mají	mít	k5eAaImIp3nP
silný	silný	k2eAgInSc4d1
citový	citový	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ovšem	ovšem	k9
nevylučuje	vylučovat	k5eNaImIp3nS
rituální	rituální	k2eAgInPc4d1
důvody	důvod	k1gInPc4
a	a	k8xC
pojídání	pojídání	k1gNnSc4
velbloudů	velbloud	k1gMnPc2
ukradených	ukradený	k2eAgFnPc2d1
jiným	jiný	k2eAgMnPc3d1
kmenům	kmen	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maso	maso	k1gNnSc4
z	z	k7c2
introdukovaných	introdukovaný	k2eAgMnPc2d1
velbloudů	velbloud	k1gMnPc2
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
také	také	k9
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jateční	jateční	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
velblouda	velbloud	k1gMnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
450	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
jateční	jateční	k2eAgFnSc1d1
výtěžnost	výtěžnost	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Velbloudí	velbloudí	k2eAgNnSc1d1
mléko	mléko	k1gNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
tučné	tučný	k2eAgNnSc4d1
a	a	k8xC
husté	hustý	k2eAgNnSc4d1
<g/>
,	,	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
také	také	k6eAd1
máslo	máslo	k1gNnSc1
<g/>
,	,	kIx,
jogurty	jogurt	k1gInPc1
a	a	k8xC
sýry	sýr	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
produkci	produkce	k1gFnSc6
mléka	mléko	k1gNnSc2
převažují	převažovat	k5eAaImIp3nP
velbloudi	velbloud	k1gMnPc1
jednohrbí	jednohrbý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
hospodářském	hospodářský	k2eAgNnSc6d1
využití	využití	k1gNnSc6
mají	mít	k5eAaImIp3nP
nemalý	malý	k2eNgInSc4d1
význam	význam	k1gInSc4
kříženci	kříženec	k1gMnPc1
velbloudů	velbloud	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1
krvinky	krvinka	k1gFnPc1
</s>
<s>
Velbloud	velbloud	k1gMnSc1
dvouhrbý	dvouhrbý	k2eAgMnSc1d1
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
</s>
<s>
Červené	Červené	k2eAgFnPc1d1
krvinky	krvinka	k1gFnPc1
velbloudů	velbloud	k1gMnPc2
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
velké	velký	k2eAgFnPc1d1
a	a	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgMnPc2d1
savců	savec	k1gMnPc2
nemají	mít	k5eNaImIp3nP
tvar	tvar	k1gInSc4
piškotu	piškot	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
kulaté	kulatý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
tak	tak	k9
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
buňka	buňka	k1gFnSc1
mohla	moct	k5eAaImAgFnS
řídit	řídit	k5eAaImF
poměrně	poměrně	k6eAd1
složitý	složitý	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
díky	díky	k7c3
němuž	jenž	k3xRgMnSc3
může	moct	k5eAaImIp3nS
velbloud	velbloud	k1gMnSc1
dlouho	dlouho	k6eAd1
žít	žít	k5eAaImF
bez	bez	k7c2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
principem	princip	k1gInSc7
je	být	k5eAaImIp3nS
nabobtnávání	nabobtnávání	k1gNnSc1
červených	červený	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
koncentraci	koncentrace	k1gFnSc6
minerálů	minerál	k1gInPc2
v	v	k7c6
krvi	krev	k1gFnSc6
(	(	kIx(
<g/>
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
více	hodně	k6eAd2
přijal	přijmout	k5eAaPmAgMnS
velbloud	velbloud	k1gMnSc1
tekutin	tekutina	k1gFnPc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
menší	malý	k2eAgFnSc1d2
bude	být	k5eAaImBp3nS
koncentrace	koncentrace	k1gFnSc1
minerálů	minerál	k1gInPc2
v	v	k7c6
krvi	krev	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
nabobtnané	nabobtnaný	k2eAgFnSc2d1
krvinky	krvinka	k1gFnSc2
vodu	voda	k1gFnSc4
v	v	k7c6
nich	on	k3xPp3gFnPc6
obsaženou	obsažený	k2eAgFnSc4d1
udrží	udržet	k5eAaPmIp3nP
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
koncentrace	koncentrace	k1gFnSc1
minerálů	minerál	k1gInPc2
v	v	k7c6
krvi	krev	k1gFnSc6
nezvedne	zvednout	k5eNaPmIp3nS
nad	nad	k7c4
normální	normální	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
velbloud	velbloud	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vystupoval	vystupovat	k5eAaImAgMnS
na	na	k7c6
pouštním	pouštní	k2eAgInSc6d1
festivalu	festival	k1gInSc6
jaisalmer	jaisalmer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.osel.cz/index.php?clanek=6762	http://www.osel.cz/index.php?clanek=6762	k4
-	-	kIx~
Fosilní	fosilní	k2eAgMnPc1d1
velbloudi	velbloud	k1gMnPc1
z	z	k7c2
kanadské	kanadský	k2eAgFnSc2d1
Arktidy	Arktida	k1gFnSc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Nullarbor	Nullarbor	k1gMnSc1
Plain	Plain	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
Encarta	Encarta	k1gFnSc1
2007	#num#	k4
[	[	kIx(
<g/>
DVD	DVD	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Microsoft	Microsoft	kA
Encarta	Encarta	k1gFnSc1
2007	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
velbloud	velbloud	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
velbloud	velbloud	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Taxon	taxon	k1gInSc1
camelus	camelus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Brožura	brožura	k1gFnSc1
OSN	OSN	kA
o	o	k7c6
velbloudech	velbloud	k1gMnPc6
a	a	k8xC
velbloudím	velbloudí	k2eAgNnSc6d1
mléce	mléko	k1gNnSc6
<g/>
:	:	kIx,
http://www.fao.org/DOCREP/003/X6528E/X6528E00.htm#TOC	http://www.fao.org/DOCREP/003/X6528E/X6528E00.htm#TOC	k4
</s>
<s>
http://www.ingema.net/in2001/clanek.php?id=344	http://www.ingema.net/in2001/clanek.php?id=344	k4
</s>
