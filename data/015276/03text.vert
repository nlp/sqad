<s>
Kaliningrad	Kaliningrad	k1gInSc1
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
К	К	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
54	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
~	~	kIx~
<g/>
5	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
5	#num#	k4
rajónů	rajón	k1gInPc2
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Ruska	Rusko	k1gNnSc2
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
,	,	kIx,
Kaliningradská	kaliningradský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
224,7	224,7	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
475	#num#	k4
056	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
2	#num#	k4
114,2	114,2	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Etnické	etnický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
</s>
<s>
Rusové	Rus	k1gMnPc1
(	(	kIx(
<g/>
87	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ukrajinci	Ukrajinec	k1gMnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bělorusové	Bělorus	k1gMnPc1
(	(	kIx(
<g/>
3,7	3,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arméni	Armén	k1gMnPc1
(	(	kIx(
<g/>
0,8	0,8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnPc1d1
(	(	kIx(
<g/>
2,5	2,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
Náboženské	náboženský	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Pravoslavné	pravoslavný	k2eAgNnSc1d1
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
ateismus	ateismus	k1gInSc1
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Jarošuk	Jarošuka	k1gFnPc2
Vznik	vznik	k1gInSc1
</s>
<s>
1255	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.klgd.ru	www.klgd.ra	k1gFnSc4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+7	+7	k4
4012	#num#	k4
PSČ	PSČ	kA
</s>
<s>
236010	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1946	#num#	k4
Königsberg	Königsberg	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
;	;	kIx,
rusky	rusky	k6eAd1
К	К	k?
nebo	nebo	k8xC
К	К	k?
<g/>
,	,	kIx,
česky	česky	k6eAd1
Královec	Královec	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Królewiec	Królewiec	k1gInSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Regiomontium	Regiomontium	k1gNnSc1
<g/>
,	,	kIx,
litevsky	litevsky	k6eAd1
Karaliaučius	Karaliaučius	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Kaliningradské	kaliningradský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
exklávy	exkláva	k1gFnSc2
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
při	při	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Pregola	Pregola	k1gFnSc1
do	do	k7c2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
475	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalo	bývat	k5eAaImAgNnS
metropolí	metropol	k1gFnSc7
Východního	východní	k2eAgNnSc2d1
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Sambové	Sambové	k2eAgFnSc1d1
</s>
<s>
Osada	osada	k1gFnSc1
Pregnora	Pregnora	k1gFnSc1
(	(	kIx(
<g/>
Twangste	Twangst	k1gMnSc5
<g/>
)	)	kIx)
na	na	k7c6
místě	místo	k1gNnSc6
pozdějšího	pozdní	k2eAgNnSc2d2
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
již	již	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
při	při	k7c6
dobývání	dobývání	k1gNnSc6
Pruska	Prusko	k1gNnSc2
Řádem	řád	k1gInSc7
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
(	(	kIx(
<g/>
1255	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
obývána	obýván	k2eAgFnSc1d1
kmenem	kmen	k1gInSc7
Sambů	Samb	k1gMnPc2
z	z	k7c2
baltského	baltský	k2eAgNnSc2d1
etnika	etnikum	k1gNnSc2
starých	starý	k2eAgMnPc2d1
Prusů	Prus	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
podlehlo	podlehnout	k5eAaPmAgNnS
germanizaci	germanizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Královec	Královec	k1gInSc1
</s>
<s>
Křižáci	křižák	k1gMnPc1
město	město	k1gNnSc4
založili	založit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1255	#num#	k4
na	na	k7c4
počest	počest	k1gFnSc4
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Přemysla	Přemysl	k1gMnSc2
Otakara	Otakar	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
proti	proti	k7c3
pohanským	pohanský	k2eAgMnPc3d1
Prusům	Prus	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
zde	zde	k6eAd1
postavili	postavit	k5eAaPmAgMnP
hrad	hrad	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1256	#num#	k4
pojmenovali	pojmenovat	k5eAaPmAgMnP
Královská	královský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
v	v	k7c6
Sambii	Sambie	k1gFnSc6
(	(	kIx(
<g/>
castrum	castrum	k1gNnSc1
de	de	k?
Coningsberg	Coningsberg	k1gMnSc1
in	in	k?
Sambia	Sambia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Mons	Mons	k1gInSc1
Regius	Regius	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Regiomontium	Regiomontium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1290	#num#	k4
získal	získat	k5eAaPmAgInS
Královec	Královec	k1gInSc4
městská	městský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvíjel	rozvíjet	k5eAaImAgMnS
se	se	k3xPyFc4
jako	jako	k9
přístav	přístav	k1gInSc1
a	a	k8xC
člen	člen	k1gInSc1
Hanzy	hanza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1300	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Pregola	Pregola	k1gFnSc1
osada	osada	k1gFnSc1
Löbenicht	Löbenichta	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
1327	#num#	k4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Pergole	pergola	k1gFnSc3
osada	osada	k1gFnSc1
Kneiphof	Kneiphof	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
části	část	k1gFnPc1
spojeny	spojit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1724	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1312	#num#	k4
se	se	k3xPyFc4
hrad	hrad	k1gInSc1
stal	stát	k5eAaPmAgInS
sídlem	sídlo	k1gNnSc7
velmistra	velmistr	k1gMnSc2
řádů	řád	k1gInPc2
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
;	;	kIx,
nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
začala	začít	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
zasvěcené	zasvěcený	k2eAgInPc4d1
sv.	sv.	kA
Vojtěchovi	Vojtěch	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1345	#num#	k4
město	město	k1gNnSc1
postihl	postihnout	k5eAaPmAgInS
ničivý	ničivý	k2eAgInSc1d1
požár	požár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1701	#num#	k4
připadl	připadnout	k5eAaPmAgInS
Královec	Královec	k1gInSc1
Prusku	Prusko	k1gNnSc3
a	a	k8xC
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
korunován	korunovat	k5eAaBmNgMnS
pruským	pruský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byla	být	k5eAaImAgNnP
připojena	připojen	k2eAgNnPc1d1
další	další	k2eAgNnPc1d1
předměstí	předměstí	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
připadlo	připadnout	k5eAaPmAgNnS
město	město	k1gNnSc1
nakrátko	nakrátko	k6eAd1
Rusku	Ruska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1871	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
Německého	německý	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kalingradská	Kalingradský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
pochází	pocházet	k5eAaImIp3nS
patrně	patrně	k6eAd1
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mj.	mj.	kA
je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
pohřben	pohřbít	k5eAaPmNgMnS
německý	německý	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
Immanuel	Immanuel	k1gMnSc1
Kant	Kant	k1gMnSc1
</s>
<s>
Centrum	centrum	k1gNnSc1
Kaliningradu	Kaliningrad	k1gInSc2
</s>
<s>
Vítězné	vítězný	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
jako	jako	k8xS,k8xC
důležitý	důležitý	k2eAgInSc1d1
přístav	přístav	k1gInSc1
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
těžce	těžce	k6eAd1
bombardováno	bombardovat	k5eAaImNgNnS
britským	britský	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
jej	on	k3xPp3gInSc4
opustila	opustit	k5eAaPmAgFnS
ještě	ještě	k6eAd1
před	před	k7c7
příchodem	příchod	k1gInSc7
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gNnSc4
obsadila	obsadit	k5eAaPmAgFnS
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
po	po	k7c6
čtyřdenní	čtyřdenní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejím	její	k3xOp3gInSc6
příchodu	příchod	k1gInSc6
bylo	být	k5eAaImAgNnS
z	z	k7c2
města	město	k1gNnSc2
vysídleno	vysídlen	k2eAgNnSc1d1
zbylých	zbylý	k2eAgNnPc2d1
200	#num#	k4
000	#num#	k4
Němců	Němec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstalo	zůstat	k5eAaPmAgNnS
asi	asi	k9
20	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
316	#num#	k4
000	#num#	k4
před	před	k7c7
válkou	válka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
postupimské	postupimský	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
město	město	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
spolu	spolu	k6eAd1
se	s	k7c7
severní	severní	k2eAgFnSc7d1
částí	část	k1gFnSc7
Východního	východní	k2eAgNnSc2d1
Pruska	Prusko	k1gNnSc2
Sovětskému	sovětský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
bylo	být	k5eAaImAgNnS
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
Kaliningrad	Kaliningrad	k1gInSc4
podle	podle	k7c2
sovětského	sovětský	k2eAgMnSc2d1
politika	politik	k1gMnSc2
Michaila	Michail	k1gMnSc2
Ivanoviče	Ivanovič	k1gMnSc2
Kalinina	Kalinin	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
</s>
<s>
Jako	jako	k8xC,k8xS
nejzápadnější	západní	k2eAgFnSc1d3
výspa	výspa	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
nabyla	nabýt	k5eAaPmAgFnS
Kaliningradská	kaliningradský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
velkého	velký	k2eAgInSc2d1
významu	význam	k1gInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaliningrad	Kaliningrad	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sídlem	sídlo	k1gNnSc7
sovětské	sovětský	k2eAgFnSc2d1
Baltské	baltský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
svého	svůj	k3xOyFgInSc2
strategického	strategický	k2eAgInSc2d1
významu	význam	k1gInSc2
byl	být	k5eAaImAgInS
Kaliningrad	Kaliningrad	k1gInSc1
uzavřen	uzavřít	k5eAaPmNgInS
pro	pro	k7c4
cizince	cizinec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
Regionální	regionální	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
historie	historie	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
Kaliningradské	kaliningradský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
Baltská	baltský	k2eAgFnSc1d1
federální	federální	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Immanuela	Immanuel	k1gMnSc2
Kanta	Kant	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
se	se	k3xPyFc4
otevřelo	otevřít	k5eAaPmAgNnS
Jantarové	jantarový	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
;	;	kIx,
mezi	mezi	k7c7
exponáty	exponát	k1gInPc7
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
největší	veliký	k2eAgInSc1d3
valoun	valoun	k1gInSc1
jantaru	jantar	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
sbírka	sbírka	k1gFnSc1
více	hodně	k6eAd2
než	než	k8xS
3000	#num#	k4
jantarových	jantarový	k2eAgFnPc2d1
inkluzí	inkluze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
město	město	k1gNnSc1
společně	společně	k6eAd1
s	s	k7c7
Kaliningradskou	kaliningradský	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
exklávou	exkláva	k1gFnSc7
Ruska	Rusko	k1gNnSc2
a	a	k8xC
otevřelo	otevřít	k5eAaPmAgNnS
se	s	k7c7
zahraničním	zahraniční	k2eAgMnSc7d1
návštěvníkům	návštěvník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nedalekém	daleký	k2eNgNnSc6d1
městě	město	k1gNnSc6
Baltijsku	Baltijsek	k1gInSc2
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
ruský	ruský	k2eAgInSc1d1
přístav	přístav	k1gInSc1
v	v	k7c6
Baltském	baltský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
během	během	k7c2
roku	rok	k1gInSc2
nezamrzá	zamrzat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1
hostil	hostit	k5eAaImAgInS
některé	některý	k3yIgInPc4
zápasy	zápas	k1gInPc4
během	během	k7c2
ruského	ruský	k2eAgNnSc2d1
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Německá	německý	k2eAgNnPc1d1
sportovní	sportovní	k2eAgNnPc1d1
sdružení	sdružení	k1gNnPc1
</s>
<s>
Všechny	všechen	k3xTgInPc1
německé	německý	k2eAgInPc1d1
kluby	klub	k1gInPc1
zanikly	zaniknout	k5eAaPmAgInP
se	s	k7c7
sovětskou	sovětský	k2eAgFnSc7d1
anexí	anexe	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VfB	VfB	k?
Königsberg	Königsberg	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
nejúspěšnější	úspěšný	k2eAgFnSc1d3
fotbalové	fotbalový	k2eAgNnSc4d1
mužstvo	mužstvo	k1gNnSc4
Pruska	Prusko	k1gNnSc2
(	(	kIx(
<g/>
mj.	mj.	kA
jedenáctinásobný	jedenáctinásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
Baltu	Balt	k1gInSc2
a	a	k8xC
pětinásobný	pětinásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
Východního	východní	k2eAgNnSc2d1
Pruska	Prusko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
SV	sv	kA
Prussia-Samland	Prussia-Samland	k1gInSc1
Königsberg	Königsberg	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
<g/>
,	,	kIx,
pětinásobný	pětinásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
Baltu	Balt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
SpVgg	SpVgg	k1gInSc1
ASCO	ASCO	kA
Königsberg	Königsberg	k1gInSc1
–	–	k?
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojnásobný	trojnásobný	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
regionální	regionální	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Concordia	Concordium	k1gNnPc4
Königsberg	Königsberg	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojnásobný	dvojnásobný	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
regionální	regionální	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
SV	sv	kA
Rasensport-Preußen	Rasensport-Preußen	k2eAgInSc1d1
Königsberg	Königsberg	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedminásobný	sedminásobný	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
regionální	regionální	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Königsberger	Königsberger	k1gMnSc1
STV	STV	kA
–	–	k?
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
,	,	kIx,
založený	založený	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pětinásobný	pětinásobný	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
regionální	regionální	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reichsbahn	Reichsbahn	k1gInSc1
SG	SG	kA
Königsberg	Königsberg	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pětinásobný	pětinásobný	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
nejvyšší	vysoký	k2eAgFnSc2d3
regionální	regionální	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
sdružení	sdružení	k1gNnSc1
</s>
<s>
FK	FK	kA
Baltika	Baltika	k1gFnSc1
Kaliningrad	Kaliningrad	k1gInSc1
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
FNL	FNL	kA
(	(	kIx(
<g/>
Ruská	ruský	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
hrál	hrát	k5eAaImAgInS
klub	klub	k1gInSc1
i	i	k8xC
některé	některý	k3yIgFnSc2
sezóny	sezóna	k1gFnSc2
v	v	k7c6
Ruské	ruský	k2eAgFnSc6d1
Premier	Premier	k1gInSc1
Lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Turistické	turistický	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
Sackheimská	Sackheimský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
a	a	k8xC
Braniborská	braniborský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
<s>
Jantarové	jantarový	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Krista	Kristus	k1gMnSc2
Spasitele	spasitel	k1gMnSc2
</s>
<s>
Náměstí	náměstí	k1gNnSc1
Vítězství	vítězství	k1gNnSc1
(	(	kIx(
<g/>
centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Dům	dům	k1gInSc1
sovětů	sovět	k1gInPc2
</s>
<s>
Kalinigradská	Kalinigradský	k2eAgFnSc1d1
Zoo	zoo	k1gFnSc1
</s>
<s>
Kantova	Kantův	k2eAgFnSc1d1
Univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
<g/>
:	:	kIx,
Královecká	Královecký	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
staré	starý	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
<s>
Jantarové	jantarový	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Krista	Kristus	k1gMnSc2
Spasitele	spasitel	k1gMnSc2
</s>
<s>
Změna	změna	k1gFnSc1
jména	jméno	k1gNnSc2
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
debatám	debata	k1gFnPc3
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
změnit	změnit	k5eAaPmF
název	název	k1gInSc4
města	město	k1gNnSc2
zpět	zpět	k6eAd1
na	na	k7c4
Königsberg	Königsberg	k1gInSc4
(	(	kIx(
<g/>
Královec	Královec	k1gInSc4
<g/>
)	)	kIx)
stejným	stejný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
se	se	k3xPyFc4
vrátilo	vrátit	k5eAaPmAgNnS
několik	několik	k4yIc1
dalších	další	k2eAgNnPc2d1
ruských	ruský	k2eAgNnPc2d1
měst	město	k1gNnPc2
ke	k	k7c3
svým	svůj	k3xOyFgNnPc3
předrevolučním	předrevoluční	k2eAgNnPc3d1
jménům	jméno	k1gNnPc3
(	(	kIx(
<g/>
např.	např.	kA
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Jekatěrinburg	Jekatěrinburg	k1gInSc1
<g/>
,	,	kIx,
Samara	Samara	k1gFnSc1
<g/>
,	,	kIx,
Tver	Tver	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
navrhovali	navrhovat	k5eAaImAgMnP
změnu	změna	k1gFnSc4
jména	jméno	k1gNnSc2
na	na	k7c4
Kalinograd	Kalinograd	k1gInSc4
(	(	kIx(
<g/>
kalina	kalina	k1gFnSc1
je	být	k5eAaImIp3nS
keř	keř	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
posvátné	posvátný	k2eAgFnPc4d1
konotace	konotace	k1gFnPc4
ve	v	k7c6
slovanském	slovanský	k2eAgInSc6d1
folklóru	folklór	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
motiv	motiv	k1gInSc1
v	v	k7c6
desítkách	desítka	k1gFnPc6
lidových	lidový	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
spojených	spojený	k2eAgMnPc2d1
hlavně	hlavně	k6eAd1
s	s	k7c7
mládím	mládí	k1gNnSc7
<g/>
,	,	kIx,
svatbami	svatba	k1gFnPc7
či	či	k8xC
pohřby	pohřeb	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Kantgrad	Kantgrada	k1gFnPc2
je	být	k5eAaImIp3nS
další	další	k2eAgNnSc1d1
navrhované	navrhovaný	k2eAgNnSc1d1
alternativní	alternativní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgNnSc1d1
velkého	velký	k2eAgMnSc2d1
filozofa	filozof	k1gMnSc2
Immanuela	Immanuel	k1gMnSc2
Kanta	Kant	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ve	v	k7c6
městě	město	k1gNnSc6
prožil	prožít	k5eAaPmAgMnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
Socha	socha	k1gFnSc1
Immanuela	Immanuel	k1gMnSc2
Kanta	Kant	k1gMnSc2
</s>
<s>
Martynas	Martynas	k1gMnSc1
Mažvydas	Mažvydas	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1510	#num#	k4
–	–	k?
asi	asi	k9
1563	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
první	první	k4xOgFnSc2
tištěné	tištěný	k2eAgFnSc2d1
litevské	litevský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
</s>
<s>
Christian	Christian	k1gMnSc1
Goldbach	Goldbach	k1gMnSc1
(	(	kIx(
<g/>
1690	#num#	k4
–	–	k?
1764	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
</s>
<s>
Immanuel	Immanuel	k1gMnSc1
Kant	Kant	k1gMnSc1
(	(	kIx(
<g/>
1724	#num#	k4
–	–	k?
1804	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
</s>
<s>
Ernst	Ernst	k1gMnSc1
Theodor	Theodor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
Hoffmann	Hoffmann	k1gMnSc1
(	(	kIx(
<g/>
1776	#num#	k4
–	–	k?
1822	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Gotthilf	Gotthilf	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Hagen	Hagen	k2eAgMnSc1d1
(	(	kIx(
<g/>
1797	#num#	k4
–	–	k?
1884	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
</s>
<s>
Fanny	Fann	k1gInPc1
Lewaldová	Lewaldová	k1gFnSc1
(	(	kIx(
<g/>
1811	#num#	k4
–	–	k?
1889	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
feministka	feministka	k1gFnSc1
a	a	k8xC
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
(	(	kIx(
<g/>
1813	#num#	k4
–	–	k?
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Gustav	Gustav	k1gMnSc1
Robert	Robert	k1gMnSc1
Kirchhoff	Kirchhoff	k1gMnSc1
(	(	kIx(
<g/>
1824	#num#	k4
–	–	k?
1887	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
</s>
<s>
Karl	Karl	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
König	König	k1gMnSc1
(	(	kIx(
<g/>
1832	#num#	k4
–	–	k?
1901	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
Wallach	Wallach	k1gMnSc1
(	(	kIx(
<g/>
1847	#num#	k4
–	–	k?
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemik	chemik	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Pabst	Pabst	k1gMnSc1
(	(	kIx(
<g/>
1854	#num#	k4
–	–	k?
1897	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
klavírista	klavírista	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
</s>
<s>
David	David	k1gMnSc1
Hilbert	Hilbert	k1gMnSc1
(	(	kIx(
<g/>
1862	#num#	k4
–	–	k?
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
</s>
<s>
Erich	Erich	k1gMnSc1
von	von	k1gInSc4
Drygalski	Drygalsk	k1gFnSc2
(	(	kIx(
<g/>
1865	#num#	k4
–	–	k?
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
</s>
<s>
Käthe	Käthe	k1gFnSc1
Kollwitzová	Kollwitzová	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
–	–	k?
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malířka	malířka	k1gFnSc1
a	a	k8xC
sochařka	sochařka	k1gFnSc1
</s>
<s>
Eugen	Eugen	k2eAgMnSc1d1
Sandow	Sandow	k1gMnSc1
(	(	kIx(
<g/>
1867	#num#	k4
–	–	k?
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
profesionálních	profesionální	k2eAgMnPc2d1
kulturistů	kulturista	k1gMnPc2
</s>
<s>
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
(	(	kIx(
<g/>
1868	#num#	k4
–	–	k?
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
</s>
<s>
Agnes	Agnes	k1gInSc1
Miegelová	Miegelová	k1gFnSc1
(	(	kIx(
<g/>
1879	#num#	k4
–	–	k?
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Werner	Werner	k1gMnSc1
Kempf	Kempf	k1gMnSc1
(	(	kIx(
<g/>
1886	#num#	k4
–	–	k?
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
generál	generál	k1gMnSc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Paneth	Paneth	k1gMnSc1
(	(	kIx(
<g/>
1887	#num#	k4
–	–	k?
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemik	chemik	k1gMnSc1
</s>
<s>
Julius	Julius	k1gMnSc1
Abronsius	Abronsius	k1gMnSc1
(	(	kIx(
<g/>
1901	#num#	k4
-	-	kIx~
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
univerzitní	univerzitní	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
</s>
<s>
Hannah	Hannah	k1gInSc1
Arendtová	Arendtová	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
–	–	k?
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politoložka	politoložka	k1gFnSc1
</s>
<s>
Lea	Lea	k1gFnSc1
Rabinová	Rabinová	k1gFnSc1
(	(	kIx(
<g/>
1928	#num#	k4
–	–	k?
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
a	a	k8xC
manželka	manželka	k1gFnSc1
Jicchaka	Jicchak	k1gMnSc2
Rabina	Rabin	k1gMnSc2
</s>
<s>
Viktor	Viktor	k1gMnSc1
Pacajev	Pacajev	k1gMnSc1
(	(	kIx(
<g/>
1933	#num#	k4
–	–	k?
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kosmonaut	kosmonaut	k1gMnSc1
</s>
<s>
Alexej	Alexej	k1gMnSc1
Leonov	Leonov	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kosmonaut	kosmonaut	k1gMnSc1
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
uskutečnil	uskutečnit	k5eAaPmAgInS
vesmírnou	vesmírný	k2eAgFnSc4d1
procházku	procházka	k1gFnSc4
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
August	August	k1gMnSc1
Winkler	Winkler	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Romaněnko	Romaněnka	k1gFnSc5
(	(	kIx(
<g/>
*	*	kIx~
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kosmonaut	kosmonaut	k1gMnSc1
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Putinová	Putinová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bývalá	bývalý	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
Vladimira	Vladimir	k1gInSc2
Putina	putin	k2eAgFnSc1d1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Aalborg	Aalborg	k1gInSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Białystok	Białystok	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Cork	Cork	k1gInSc1
<g/>
,	,	kIx,
Irsko	Irsko	k1gNnSc1
</s>
<s>
Elbląg	Elbląg	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Gdaňsk	Gdaňsk	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Gdyně	Gdyně	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Groningen	Groningen	k1gInSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Kiel	Kiel	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Kalmar	Kalmar	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Klaipė	Klaipė	k1gFnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
</s>
<s>
Lodž	Lodž	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Malmö	Malmö	k?
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Mexico	Mexico	k6eAd1
City	city	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Murmansk	Murmansk	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Norfolk	Norfolk	k1gInSc1
<g/>
,	,	kIx,
Virginie	Virginie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Olsztyn	Olsztyn	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Omsk	Omsk	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Šiauliai	Šiauliai	k1gNnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
</s>
<s>
Toruň	Toruň	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Zabrze	Zabrze	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Zwolle	Zwolle	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ruský	ruský	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
zákon	zákon	k1gInSc1
248-Ф	248-Ф	k?
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2014-07-21	2014-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
26	#num#	k4
<g/>
.	.	kIx.
Ч	Ч	k?
п	п	k?
н	н	k?
Р	Р	k?
Ф	Ф	k?
п	п	k?
м	м	k?
о	о	k?
н	н	k?
1	#num#	k4
я	я	k?
2018	#num#	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
The	The	k1gFnSc1
songs	songs	k1gInSc1
of	of	k?
the	the	k?
Russian	Russiany	k1gInPc2
people	people	k6eAd1
<g/>
,	,	kIx,
as	as	k9
illustrative	illustrativ	k1gInSc5
of	of	k?
Slavonic	Slavonice	k1gFnPc2
mythology	mytholog	k1gMnPc4
and	and	k?
Russian	Russian	k1gInSc1
social	social	k1gMnSc1
life	life	k1gInSc1
:	:	kIx,
Ralston	Ralston	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Ralston	Ralston	k1gInSc1
Shedden	Sheddna	k1gFnPc2
<g/>
,	,	kIx,
1828-1889	1828-1889	k4
:	:	kIx,
Free	Fre	k1gInSc2
Download	Download	k1gInSc1
&	&	k?
Streaming	Streaming	k1gInSc1
:	:	kIx,
Internet	Internet	k1gInSc1
Archive	archiv	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archive	archiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sedm	sedm	k4xCc4
mostů	most	k1gInPc2
města	město	k1gNnSc2
Královce	Královec	k1gInSc2
</s>
<s>
Královec	Královec	k1gInSc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kaliningrad	Kaliningrad	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Kaliningrad	Kaliningrad	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
oblasti	oblast	k1gFnSc2
od	od	k7c2
1815	#num#	k4
do	do	k7c2
1945	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rusko	Rusko	k1gNnSc1
–	–	k?
Р	Р	k?
–	–	k?
(	(	kIx(
<g/>
RUS	Rus	k1gMnSc1
<g/>
)	)	kIx)
Subjekty	subjekt	k1gInPc1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Republiky	republika	k1gFnSc2
</s>
<s>
Adygejsko	Adygejsko	k1gNnSc1
(	(	kIx(
<g/>
Majkop	Majkop	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Altajsko	Altajsko	k1gNnSc1
(	(	kIx(
<g/>
Gorno-Altajsk	Gorno-Altajsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Baškortostán	Baškortostán	k1gInSc1
(	(	kIx(
<g/>
Ufa	Ufa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Burjatsko	Burjatsko	k1gNnSc1
(	(	kIx(
<g/>
Ulan-Ude	Ulan-Ud	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Čečensko	Čečensko	k1gNnSc1
(	(	kIx(
<g/>
Grozný	Grozný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Čuvašsko	Čuvašsko	k1gNnSc1
(	(	kIx(
<g/>
Čeboksary	Čeboksara	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Dagestán	Dagestán	k1gInSc1
(	(	kIx(
<g/>
Machačkala	Machačkala	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Chakasie	Chakasie	k1gFnSc1
(	(	kIx(
<g/>
Abakan	Abakan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ingušsko	Ingušsko	k1gNnSc1
(	(	kIx(
<g/>
Magas	Magas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kabardsko-Balkarsko	Kabardsko-Balkarsko	k1gNnSc1
(	(	kIx(
<g/>
Nalčik	Nalčik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalmycko	Kalmycko	k1gNnSc1
(	(	kIx(
<g/>
Elista	Elista	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Karačajevsko-Čerkesko	Karačajevsko-Čerkesko	k1gNnSc1
(	(	kIx(
<g/>
Čerkesk	Čerkesk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Karélie	Karélie	k1gFnSc1
(	(	kIx(
<g/>
Petrozavodsk	Petrozavodsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Komi	Komi	k1gNnSc1
(	(	kIx(
<g/>
Syktyvkar	Syktyvkar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krym	Krym	k1gInSc1
(	(	kIx(
<g/>
Simferopol	Simferopol	k1gInSc1
<g/>
)	)	kIx)
<g/>
¹	¹	k?
</s>
<s>
Marijsko	Marijsko	k1gNnSc1
(	(	kIx(
<g/>
Joškar-Ola	Joškar-Ola	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Mordvinsko	Mordvinsko	k1gNnSc1
(	(	kIx(
<g/>
Saransk	Saransk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sacha	Sacha	k1gFnSc1
(	(	kIx(
<g/>
Jakutsk	Jakutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Osetie-Alanie	Osetie-Alanie	k1gFnSc1
(	(	kIx(
<g/>
Vladikavkaz	Vladikavkaz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tatarstán	Tatarstán	k1gInSc1
(	(	kIx(
<g/>
Kazaň	Kazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tuva	Tuva	k1gFnSc1
(	(	kIx(
<g/>
Kyzyl	Kyzyl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Udmurtsko	Udmurtsko	k1gNnSc1
(	(	kIx(
<g/>
Iževsk	Iževsk	k1gInSc1
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
</s>
<s>
Altajský	altajský	k2eAgInSc1d1
(	(	kIx(
<g/>
Barnaul	Barnaul	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chabarovsk	Chabarovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kamčatský	kamčatský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnodarský	krasnodarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnodar	Krasnodar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnojarský	Krasnojarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnojarsk	Krasnojarsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Permský	permský	k2eAgInSc1d1
(	(	kIx(
<g/>
Perm	perm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přímořský	přímořský	k2eAgInSc4d1
(	(	kIx(
<g/>
Vladivostok	Vladivostok	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Stavropolský	stavropolský	k2eAgInSc1d1
(	(	kIx(
<g/>
Stavropol	Stavropol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zabajkalský	zabajkalský	k2eAgInSc1d1
(	(	kIx(
<g/>
Čita	čit	k2eAgFnSc1d1
<g/>
)	)	kIx)
Oblasti	oblast	k1gFnPc1
</s>
<s>
Amurská	amurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Blagověščensk	Blagověščensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Archangelská	archangelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Archangelsk	Archangelsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Astrachaňská	astrachaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Astrachaň	Astrachaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bělgorodská	Bělgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bělgorod	Bělgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Brjanská	Brjanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Brjansk	Brjansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čeljabinská	čeljabinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Čeljabinsk	Čeljabinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Irkutská	irkutský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Irkutsk	Irkutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ivanovská	Ivanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ivanovo	Ivanův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Jaroslavská	Jaroslavský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jaroslavl	Jaroslavl	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalužská	Kalužský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaluga	Kaluga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kemerovská	Kemerovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kemerovo	Kemerův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kirovská	Kirovská	k1gFnSc1
(	(	kIx(
<g/>
Kirov	Kirov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostromská	Kostromský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kostroma	Kostroma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kurganská	Kurganský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kurská	kurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kursk	Kursk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Leningradská	leningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lipecká	Lipecká	k1gFnSc1
(	(	kIx(
<g/>
Lipeck	Lipeck	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Magadanská	Magadanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Magadan	Magadan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Moskevská	moskevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Murmanská	murmanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Murmansk	Murmansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nižněnovgorodská	Nižněnovgorodský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Nižnij	Nižnij	k1gFnSc1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Veliký	veliký	k2eAgInSc1d1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novosibirská	novosibirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Novosibirsk	Novosibirsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Omská	omský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Omsk	Omsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Orelská	orelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orel	Orel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Orenburská	orenburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orenburg	Orenburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Penzenská	Penzenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Penza	Penza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pskovská	Pskovská	k1gFnSc1
(	(	kIx(
<g/>
Pskov	Pskov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rjazaňská	rjazaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rjazaň	Rjazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rostovská	Rostovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rostov	Rostov	k1gInSc1
na	na	k7c4
Donu	dona	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Samarská	samarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Samara	Samara	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Saratovská	Saratovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Saratov	Saratovo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Smolenská	Smolenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Smolensk	Smolensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sverdlovská	sverdlovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jekatěrinburg	Jekatěrinburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tambovská	Tambovská	k1gFnSc1
(	(	kIx(
<g/>
Tambov	Tambov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tomská	Tomská	k1gFnSc1
(	(	kIx(
<g/>
Tomsk	Tomsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tverská	Tverský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tver	Tver	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tulská	tulský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tula	Tula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ťumeňská	Ťumeňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ťumeň	Ťumeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Uljanovská	Uljanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Uljanovsk	Uljanovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vladimirská	Vladimirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vladimir	Vladimir	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Volgogradská	volgogradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Volgograd	Volgograd	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vologdská	Vologdský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vologda	Vologda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Voroněžská	voroněžský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Voroněž	Voroněž	k1gFnSc1
<g/>
)	)	kIx)
Federální	federální	k2eAgNnSc1d1
města	město	k1gNnSc2
</s>
<s>
Moskva	Moskva	k1gFnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Sevastopol¹	Sevastopol¹	k?
Autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Židovská	židovská	k1gFnSc1
(	(	kIx(
<g/>
Birobidžan	Birobidžan	k1gInSc1
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
</s>
<s>
Čukotský	čukotský	k2eAgInSc1d1
(	(	kIx(
<g/>
Anadyr	Anadyr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chantymansijský	Chantymansijský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Jamalo-něnecký	Jamalo-něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Salechard	Salechard	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Něnecký	Něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Narjan-Mar	Narjan-Mar	k1gInSc1
<g/>
)	)	kIx)
<g/>
³	³	k?
</s>
<s>
¹	¹	k?
Nárokováno	nárokován	k2eAgNnSc1d1
Ukrajinou	Ukrajina	k1gFnSc7
<g/>
,	,	kIx,
mezinárodně	mezinárodně	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
²	²	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Ťumeňské	Ťumeňský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
³	³	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Archangelské	archangelský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Města	město	k1gNnPc1
v	v	k7c6
Kaliningradské	kaliningradský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
Kaliningrad	Kaliningrad	k1gInSc1
</s>
<s>
Bagrationovsk	Bagrationovsk	k1gInSc1
•	•	k?
Baltijsk	Baltijsk	k1gInSc1
•	•	k?
Čerňachovsk	Čerňachovsk	k1gInSc1
•	•	k?
Gurjevsk	Gurjevsk	k1gInSc1
•	•	k?
Gusev	Gusev	k1gFnSc1
•	•	k?
Gvardějsk	Gvardějsk	k1gInSc1
•	•	k?
Krasnoznamensk	Krasnoznamensk	k1gInSc1
•	•	k?
Laduškin	Laduškin	k1gMnSc1
•	•	k?
Mamonovo	Mamonův	k2eAgNnSc1d1
•	•	k?
Něman	Něman	k1gInSc1
•	•	k?
Nestěrov	Nestěrov	k1gInSc1
•	•	k?
Ozjorsk	Ozjorsk	k1gInSc1
•	•	k?
Pioněrskij	Pioněrskij	k1gFnSc2
•	•	k?
Polessk	Polessk	k1gInSc1
•	•	k?
Pravdinsk	Pravdinsk	k1gInSc1
•	•	k?
Primorsk	Primorsk	k1gInSc1
•	•	k?
Slavsk	Slavsk	k1gInSc1
•	•	k?
Sovětsk	Sovětsk	k1gInSc1
•	•	k?
Svetlij	Svetlij	k1gFnSc2
•	•	k?
Svetlogorsk	Svetlogorsk	k1gInSc1
•	•	k?
Zelenogradsk	Zelenogradsk	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
151717	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4031541-1	4031541-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81086433	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
145427148	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81086433	#num#	k4
</s>
