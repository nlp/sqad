<s>
Kaliningrad	Kaliningrad	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1946	#num#	k4
Königsberg	Königsberg	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
;	;	kIx,
rusky	rusky	k6eAd1
К	К	k?
nebo	nebo	k8xC
К	К	k?
<g/>
,	,	kIx,
česky	česky	k6eAd1
Královec	Královec	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Królewiec	Królewiec	k1gInSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Regiomontium	Regiomontium	k1gNnSc1
<g/>
,	,	kIx,
litevsky	litevsky	k6eAd1
Karaliaučius	Karaliaučius	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Kaliningradské	kaliningradský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
exklávy	exkláva	k1gFnSc2
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>