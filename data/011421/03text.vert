<p>
<s>
Máz	máz	k1gInSc1	máz
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
objemová	objemový	k2eAgFnSc1d1	objemová
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Mass	Mass	k1gInSc1	Mass
používaného	používaný	k2eAgNnSc2d1	používané
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
1,069	[number]	k4	1,069
litru	litr	k1gInSc2	litr
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přepočet	přepočet	k1gInSc4	přepočet
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
míra	míra	k1gFnSc1	míra
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
Pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
)	)	kIx)	)
–	–	k?	–
3,35	[number]	k4	3,35
litru	litr	k1gInSc2	litr
</s>
</p>
<p>
<s>
český	český	k2eAgInSc1d1	český
respektive	respektive	k9	respektive
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
máz	máz	k1gInSc1	máz
<g/>
,	,	kIx,	,
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
1,415	[number]	k4	1,415
litru	litr	k1gInSc2	litr
</s>
</p>
<p>
<s>
moravský	moravský	k2eAgInSc1d1	moravský
máz	máz	k1gInSc1	máz
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
1,07	[number]	k4	1,07
litru	litr	k1gInSc2	litr
</s>
</p>
<p>
<s>
máz	máz	k1gInSc1	máz
se	se	k3xPyFc4	se
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
2	[number]	k4	2
holby	holba	k1gFnSc2	holba
a	a	k8xC	a
4	[number]	k4	4
žejdlíky	žejdlík	k1gInPc7	žejdlík
</s>
</p>
<p>
<s>
==	==	k?	==
poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
máz	máz	k1gInSc4	máz
také	také	k9	také
označovala	označovat	k5eAaImAgFnS	označovat
jednotka	jednotka	k1gFnSc1	jednotka
pinta	pinta	k1gFnSc1	pinta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
činila	činit	k5eAaImAgFnS	činit
1,938	[number]	k4	1,938
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Malý	malý	k2eAgInSc1d1	malý
slovník	slovník	k1gInSc1	slovník
jednotek	jednotka	k1gFnPc2	jednotka
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
katalogové	katalogový	k2eAgNnSc1d1	Katalogové
číslo	číslo	k1gNnSc1	číslo
23-065-82	[number]	k4	23-065-82
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
máz	máz	k1gInSc1	máz
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
