<s>
Galileo	Galilea	k1gFnSc5	Galilea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
také	také	k9	také
planetu	planeta	k1gFnSc4	planeta
Neptun	Neptun	k1gInSc4	Neptun
<g/>
,	,	kIx,	,
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
žádnou	žádný	k3yNgFnSc4	žádný
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
poznámkách	poznámka	k1gFnPc6	poznámka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
jen	jen	k9	jen
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
nezajímavých	zajímavý	k2eNgFnPc2d1	nezajímavá
nejasných	jasný	k2eNgFnPc2d1	nejasná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
