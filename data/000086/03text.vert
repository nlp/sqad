<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
sestává	sestávat	k5eAaImIp3nS	sestávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
z	z	k7c2	z
18	[number]	k4	18
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
101	[number]	k4	101
departementů	departement	k1gInPc2	departement
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
reorganizace	reorganizace	k1gFnSc2	reorganizace
administrativně-správního	administrativněprávní	k2eAgNnSc2d1	administrativně-správní
dělení	dělení	k1gNnSc2	dělení
francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
existovalo	existovat	k5eAaImAgNnS	existovat
27	[number]	k4	27
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
správního	správní	k2eAgNnSc2d1	správní
dělení	dělení	k1gNnSc2	dělení
do	do	k7c2	do
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
18	[number]	k4	18
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
části	část	k1gFnSc6	část
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
Francie	Francie	k1gFnSc2	Francie
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korsika	Korsika	k1gFnSc1	Korsika
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Corse	Corse	k1gFnSc2	Corse
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
statut	statut	k1gInSc1	statut
(	(	kIx(	(
<g/>
collectivité	collectivitý	k2eAgFnSc6d1	collectivitý
territoriale	territoriala	k1gFnSc6	territoriala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
17	[number]	k4	17
regionů	region	k1gInPc2	region
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Est	Est	k1gMnSc1	Est
Akvitánie-Limousin-Poitou-Charentes	Akvitánie-Limousin-Poitou-Charentes	k1gMnSc1	Akvitánie-Limousin-Poitou-Charentes
Auvergne-Rhône-Alpes	Auvergne-Rhône-Alpes	k1gMnSc1	Auvergne-Rhône-Alpes
Burgundsko-Franche-Comté	Burgundsko-Franche-Comtý	k2eAgFnSc2d1	Burgundsko-Franche-Comtý
Bretaň	Bretaň	k1gFnSc1	Bretaň
Centre-Val	Centre-Val	k1gInSc1	Centre-Val
de	de	k?	de
Loire	Loir	k1gInSc5	Loir
Korsika	Korsika	k1gFnSc1	Korsika
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
Languedoc-Roussillon-Midi-Pyrénées	Languedoc-Roussillon-Midi-Pyrénéesa	k1gFnPc2	Languedoc-Roussillon-Midi-Pyrénéesa
Hauts-de-France	Hautse-France	k1gFnSc2	Hauts-de-France
Normandie	Normandie	k1gFnSc2	Normandie
Pays	Paysa	k1gFnPc2	Paysa
de	de	k?	de
la	la	k1gNnSc4	la
Loire	Loir	k1gInSc5	Loir
Provence-Alpes-Côte	Provence-Alpes-Côt	k1gInSc5	Provence-Alpes-Côt
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Azur	azur	k1gInSc1	azur
Guadeloupe	Guadeloupe	k1gMnSc2	Guadeloupe
Guyana	Guyana	k1gFnSc1	Guyana
Martinik	Martinik	k1gMnSc1	Martinik
Réunion	Réunion	k1gInSc1	Réunion
Mayotte	Mayott	k1gInSc5	Mayott
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Francie	Francie	k1gFnSc1	Francie
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
27	[number]	k4	27
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
22	[number]	k4	22
v	v	k7c6	v
Metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
5	[number]	k4	5
v	v	k7c6	v
Zámořské	zámořský	k2eAgFnSc6d1	zámořská
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
