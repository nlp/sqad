<s>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
kultovní	kultovní	k2eAgInSc1d1	kultovní
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Quentin	Quentin	k1gMnSc1	Quentin
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
<g/>
.	.	kIx.	.
</s>
