<s>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
kultovní	kultovní	k2eAgInSc1d1	kultovní
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Quentin	Quentin	k1gMnSc1	Quentin
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obdržel	obdržet	k5eAaPmAgInS	obdržet
sedm	sedm	k4xCc4	sedm
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
proměnil	proměnit	k5eAaPmAgInS	proměnit
ale	ale	k9	ale
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rogerem	Roger	k1gInSc7	Roger
Avarym	Avarym	k1gInSc1	Avarym
získali	získat	k5eAaPmAgMnP	získat
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
původní	původní	k2eAgInSc4d1	původní
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
palma	palma	k1gFnSc1	palma
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc4	Fiction
tvoří	tvořit	k5eAaImIp3nS	tvořit
několik	několik	k4yIc4	několik
příběhů	příběh	k1gInPc2	příběh
z	z	k7c2	z
losangeleského	losangeleský	k2eAgNnSc2d1	losangeleské
gangsterského	gangsterský	k2eAgNnSc2d1	gangsterské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
scénách	scéna	k1gFnPc6	scéna
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
prolínají	prolínat	k5eAaImIp3nP	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
společným	společný	k2eAgInSc7d1	společný
prvkem	prvek	k1gInSc7	prvek
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc1	postava
Marsella	Marsella	k1gFnSc1	Marsella
Wallace	Wallace	k1gFnSc1	Wallace
(	(	kIx(	(
<g/>
Ving	Ving	k1gMnSc1	Ving
Rhames	Rhames	k1gMnSc1	Rhames
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vincenta	Vincent	k1gMnSc2	Vincent
Vegy	Vega	k1gMnSc2	Vega
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Miy	Miy	k1gFnSc1	Miy
Wallacové	Wallacové	k2eAgFnSc1d1	Wallacové
(	(	kIx(	(
<g/>
Uma	uma	k1gFnSc1	uma
Thurman	Thurman	k1gMnSc1	Thurman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgFnPc1d1	jediná
tři	tři	k4xCgFnPc1	tři
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
povídkách	povídka	k1gFnPc6	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
připojena	připojen	k2eAgFnSc1d1	připojena
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dodatků	dodatek	k1gInPc2	dodatek
<g/>
,	,	kIx,	,
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
epizod	epizoda	k1gFnPc2	epizoda
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
mozaice	mozaika	k1gFnSc6	mozaika
je	být	k5eAaImIp3nS	být
také	také	k9	také
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
společných	společný	k2eAgInPc2d1	společný
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
povídky	povídka	k1gFnPc1	povídka
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
bizarních	bizarní	k2eAgInPc2d1	bizarní
dějových	dějový	k2eAgInPc2d1	dějový
zvratů	zvrat	k1gInPc2	zvrat
<g/>
,	,	kIx,	,
narážek	narážka	k1gFnPc2	narážka
na	na	k7c4	na
klasické	klasický	k2eAgInPc4d1	klasický
béčkové	béčkový	k2eAgInPc4d1	béčkový
filmy	film	k1gInPc4	film
i	i	k8xC	i
postmoderních	postmoderní	k2eAgInPc2d1	postmoderní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
<g/>
,	,	kIx,	,
chronologicky	chronologicky	k6eAd1	chronologicky
první	první	k4xOgMnSc1	první
a	a	k8xC	a
nejrozvětvenější	rozvětvený	k2eAgFnSc6d3	nejrozvětvenější
povídce	povídka	k1gFnSc6	povídka
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Situace	situace	k1gFnSc1	situace
kolem	kolem	k7c2	kolem
Bonnie	Bonnie	k1gFnSc2	Bonnie
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Bonnie	Bonnie	k1gFnSc1	Bonnie
Situation	Situation	k1gInSc1	Situation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vyústění	vyústění	k1gNnSc1	vyústění
a	a	k8xC	a
následně	následně	k6eAd1	následně
začátek	začátek	k1gInSc1	začátek
tvoří	tvořit	k5eAaImIp3nS	tvořit
prolog	prolog	k1gInSc4	prolog
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
až	až	k9	až
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
druhořadí	druhořadý	k2eAgMnPc1d1	druhořadý
zabijáci	zabiják	k1gMnPc1	zabiják
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Jules	Jules	k1gMnSc1	Jules
(	(	kIx(	(
<g/>
Samuel	Samuel	k1gMnSc1	Samuel
L.	L.	kA	L.
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vincent	Vincent	k1gMnSc1	Vincent
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
<g/>
,	,	kIx,	,
neaktivním	aktivní	k2eNgMnSc7d1	neaktivní
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Marvinem	Marvin	k1gMnSc7	Marvin
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
mafiánského	mafiánský	k2eAgMnSc4d1	mafiánský
bosse	boss	k1gMnSc4	boss
Marselluse	Marselluse	k1gFnSc2	Marselluse
Wallace	Wallace	k1gFnSc2	Wallace
přepadnou	přepadnout	k5eAaPmIp3nP	přepadnout
a	a	k8xC	a
zabijí	zabít	k5eAaPmIp3nP	zabít
skupinu	skupina	k1gFnSc4	skupina
bývalých	bývalý	k2eAgMnPc2d1	bývalý
Marsellusových	Marsellusový	k2eAgMnPc2d1	Marsellusový
obchodních	obchodní	k2eAgMnPc2d1	obchodní
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
a	a	k8xC	a
seberou	sebrat	k5eAaPmIp3nP	sebrat
jim	on	k3xPp3gMnPc3	on
kufřík	kufřík	k1gInSc4	kufřík
s	s	k7c7	s
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
lesklým	lesklý	k2eAgInSc7d1	lesklý
předmětem	předmět	k1gInSc7	předmět
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
není	být	k5eNaImIp3nS	být
nikde	nikde	k6eAd1	nikde
ve	v	k7c6	v
filmu	film	k1gInSc2	film
prozrazen	prozradit	k5eAaPmNgMnS	prozradit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
předmětem	předmět	k1gInSc7	předmět
<g/>
,	,	kIx,	,
a	a	k8xC	a
fanoušci	fanoušek	k1gMnPc1	fanoušek
filmu	film	k1gInSc2	film
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
kufříku	kufřík	k1gInSc6	kufřík
vlastně	vlastně	k9	vlastně
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
ale	ale	k8xC	ale
z	z	k7c2	z
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
místnosti	místnost	k1gFnSc2	místnost
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
před	před	k7c7	před
zabijáky	zabiják	k1gMnPc7	zabiják
skrýval	skrývat	k5eAaImAgMnS	skrývat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vystřílí	vystřílet	k5eAaPmIp3nS	vystřílet
na	na	k7c4	na
Julese	Julese	k1gFnPc4	Julese
a	a	k8xC	a
Vincenta	Vincent	k1gMnSc2	Vincent
celý	celý	k2eAgInSc1d1	celý
zásobník	zásobník	k1gInSc1	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
kulkou	kulka	k1gFnSc7	kulka
se	se	k3xPyFc4	se
ale	ale	k9	ale
netrefí	trefit	k5eNaPmIp3nS	trefit
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Jules	Jules	k1gInSc1	Jules
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
zázrak	zázrak	k1gInSc4	zázrak
a	a	k8xC	a
Boží	boží	k2eAgNnSc4d1	boží
znamení	znamení	k1gNnSc4	znamení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změnili	změnit	k5eAaPmAgMnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozdější	pozdní	k2eAgFnSc6d2	pozdější
poněkud	poněkud	k6eAd1	poněkud
ohnivé	ohnivý	k2eAgFnSc3d1	ohnivá
diskusi	diskuse	k1gFnSc3	diskuse
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
Marvina	Marvina	k1gFnSc1	Marvina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
zadním	zadní	k2eAgNnSc6d1	zadní
sedadle	sedadlo	k1gNnSc6	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
situace	situace	k1gFnSc2	situace
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
uprostřed	uprostřed	k6eAd1	uprostřed
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
vracejí	vracet	k5eAaImIp3nP	vracet
se	se	k3xPyFc4	se
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
autě	aut	k1gInSc6	aut
mrtvolu	mrtvola	k1gFnSc4	mrtvola
omylem	omyl	k1gInSc7	omyl
zabitého	zabitý	k2eAgMnSc2d1	zabitý
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Schovají	schovat	k5eAaPmIp3nP	schovat
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Julesova	Julesův	k2eAgMnSc2d1	Julesův
přítele	přítel	k1gMnSc2	přítel
Jimmyho	Jimmy	k1gMnSc2	Jimmy
(	(	kIx(	(
<g/>
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
zůstat	zůstat	k5eAaPmF	zůstat
nanejvýš	nanejvýš	k6eAd1	nanejvýš
hodinu	hodina	k1gFnSc4	hodina
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ze	z	k7c2	z
směny	směna	k1gFnSc2	směna
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Jimmyho	Jimmy	k1gMnSc2	Jimmy
manželka	manželka	k1gFnSc1	manželka
Bonnie	Bonnie	k1gFnSc1	Bonnie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
kontakty	kontakt	k1gInPc4	kontakt
na	na	k7c4	na
podsvětí	podsvětí	k1gNnSc4	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
a	a	k8xC	a
Jules	Jules	k1gMnSc1	Jules
mu	on	k3xPp3gMnSc3	on
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
hodiny	hodina	k1gFnSc2	hodina
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
jim	on	k3xPp3gMnPc3	on
Marsellus	Marsellus	k1gMnSc1	Marsellus
pošle	poslat	k5eAaPmIp3nS	poslat
mimořádně	mimořádně	k6eAd1	mimořádně
schopného	schopný	k2eAgMnSc4d1	schopný
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Winston	Winston	k1gInSc1	Winston
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
Harvey	Harvea	k1gMnSc2	Harvea
Keitel	Keitel	k1gMnSc1	Keitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jim	on	k3xPp3gMnPc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
uklidit	uklidit	k5eAaPmF	uklidit
auto	auto	k1gNnSc4	auto
a	a	k8xC	a
včas	včas	k6eAd1	včas
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
a	a	k8xC	a
Vincent	Vincent	k1gMnSc1	Vincent
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
diskusi	diskuse	k1gFnSc6	diskuse
v	v	k7c6	v
bistru	bistr	k1gInSc6	bistr
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
si	se	k3xPyFc3	se
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Boží	boží	k2eAgInSc1d1	boží
zásah	zásah	k1gInSc1	zásah
a	a	k8xC	a
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
to	ten	k3xDgNnSc4	ten
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
pouhou	pouhý	k2eAgFnSc4d1	pouhá
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Vincent	Vincent	k1gMnSc1	Vincent
odejde	odejít	k5eAaPmIp3nS	odejít
na	na	k7c4	na
záchod	záchod	k1gInSc4	záchod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bistro	bistro	k1gNnSc1	bistro
pokusí	pokusit	k5eAaPmIp3nS	pokusit
vykrást	vykrást	k5eAaPmF	vykrást
milenecký	milenecký	k2eAgInSc4d1	milenecký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
lupičský	lupičský	k2eAgInSc4d1	lupičský
pár	pár	k4xCyI	pár
Pumpkin	Pumpkin	k1gInSc4	Pumpkin
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
britský	britský	k2eAgInSc4d1	britský
akcent	akcent	k1gInSc4	akcent
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
oslovován	oslovován	k2eAgMnSc1d1	oslovován
jako	jako	k8xC	jako
Ringo	Ringo	k1gMnSc1	Ringo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Honey	Hone	k2eAgFnPc4d1	Hone
Bunny	Bunna	k1gFnPc4	Bunna
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrožují	vyhrožovat	k5eAaImIp3nP	vyhrožovat
zastřelením	zastřelení	k1gNnSc7	zastřelení
personálu	personál	k1gInSc2	personál
i	i	k8xC	i
hostům	host	k1gMnPc3	host
<g/>
,	,	kIx,	,
vybírají	vybírat	k5eAaImIp3nP	vybírat
ode	ode	k7c2	ode
všech	všecek	k3xTgMnPc2	všecek
peněženky	peněženka	k1gFnSc2	peněženka
a	a	k8xC	a
cennosti	cennost	k1gFnSc2	cennost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Jules	Jules	k1gMnSc1	Jules
jim	on	k3xPp3gFnPc3	on
dá	dát	k5eAaPmIp3nS	dát
svoji	svůj	k3xOyFgFnSc4	svůj
peněženku	peněženka	k1gFnSc4	peněženka
<g/>
.	.	kIx.	.
</s>
<s>
Kufřík	kufřík	k1gInSc1	kufřík
jim	on	k3xPp3gMnPc3	on
ale	ale	k8xC	ale
dát	dát	k5eAaPmF	dát
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Ringo	Ringo	k6eAd1	Ringo
oslněn	oslnit	k5eAaPmNgInS	oslnit
záhadným	záhadný	k2eAgInSc7d1	záhadný
předmětem	předmět	k1gInSc7	předmět
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
Jules	Jules	k1gInSc4	Jules
odzbrojí	odzbrojit	k5eAaPmIp3nP	odzbrojit
<g/>
.	.	kIx.	.
</s>
<s>
Vyloží	vyložit	k5eAaPmIp3nP	vyložit
Pumpkinovi	Pumpkinův	k2eAgMnPc1d1	Pumpkinův
svoje	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
je	on	k3xPp3gNnSc4	on
potkal	potkat	k5eAaPmAgInS	potkat
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Zarecituje	zarecitovat	k5eAaPmIp3nS	zarecitovat
jim	on	k3xPp3gMnPc3	on
smyšlený	smyšlený	k2eAgInSc1d1	smyšlený
citát	citát	k1gInSc1	citát
z	z	k7c2	z
bible	bible	k1gFnSc2	bible
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
silný	silný	k2eAgMnSc1d1	silný
chránil	chránit	k5eAaImAgMnS	chránit
slabé	slabý	k2eAgFnSc3d1	slabá
před	před	k7c7	před
"	"	kIx"	"
<g/>
tyranií	tyranie	k1gFnSc7	tyranie
lidské	lidský	k2eAgFnSc2d1	lidská
zloby	zloba	k1gFnSc2	zloba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
citát	citát	k1gInSc1	citát
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
sice	sice	k8xC	sice
skutečným	skutečný	k2eAgInSc7d1	skutečný
biblickým	biblický	k2eAgInSc7d1	biblický
úryvkem	úryvek	k1gInSc7	úryvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silně	silně	k6eAd1	silně
obměněný	obměněný	k2eAgInSc1d1	obměněný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Ezechiel	Ezechiel	k1gMnSc1	Ezechiel
25,17	[number]	k4	25,17
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
filmovým	filmový	k2eAgFnPc3d1	filmová
hláškám	hláška	k1gFnPc3	hláška
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
citátu	citát	k1gInSc2	citát
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
spravedlivého	spravedlivý	k2eAgNnSc2d1	spravedlivé
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
lemována	lemován	k2eAgFnSc1d1	lemována
jest	být	k5eAaImIp3nS	být
nespravedlností	nespravedlnost	k1gFnSc7	nespravedlnost
<g/>
,	,	kIx,	,
sobectvím	sobectví	k1gNnSc7	sobectví
a	a	k8xC	a
tyranií	tyranie	k1gFnSc7	tyranie
lidské	lidský	k2eAgFnSc2d1	lidská
zloby	zloba	k1gFnSc2	zloba
<g/>
.	.	kIx.	.
</s>
<s>
Požehnán	požehnán	k2eAgMnSc1d1	požehnán
buď	buď	k8xC	buď
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
vyvede	vyvést	k5eAaPmIp3nS	vyvést
slabé	slabý	k2eAgNnSc1d1	slabé
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
temnoty	temnota	k1gFnSc2	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Neb	neb	k8xC	neb
ten	ten	k3xDgMnSc1	ten
jest	být	k5eAaImIp3nS	být
skutečným	skutečný	k2eAgMnSc7d1	skutečný
pastýřem	pastýř	k1gMnSc7	pastýř
a	a	k8xC	a
spasitelem	spasitel	k1gMnSc7	spasitel
zbloudilých	zbloudilý	k2eAgFnPc2d1	zbloudilá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
srazím	srazit	k5eAaPmIp1nS	srazit
k	k	k7c3	k
zemi	zem	k1gFnSc6	zem
mocným	mocný	k2eAgInSc7d1	mocný
trestem	trest	k1gInSc7	trest
a	a	k8xC	a
divokým	divoký	k2eAgInSc7d1	divoký
hněvem	hněv	k1gInSc7	hněv
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
otrávit	otrávit	k5eAaPmF	otrávit
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
mé	můj	k3xOp1gMnPc4	můj
bratry	bratr	k1gMnPc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
uvalím	uvalit	k5eAaPmIp1nS	uvalit
svou	svůj	k3xOyFgFnSc4	svůj
mstu	msta	k1gFnSc4	msta
na	na	k7c4	na
Tebe	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
seznáš	seznat	k5eAaPmIp2nS	seznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
mé	můj	k3xOp1gFnPc4	můj
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlí	vysvětlit	k5eAaPmIp3nP	vysvětlit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
nechce	chtít	k5eNaImIp3nS	chtít
být	být	k5eAaImF	být
tím	ten	k3xDgMnSc7	ten
tyranem	tyran	k1gMnSc7	tyran
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgInSc7	ten
silným	silný	k2eAgMnSc7d1	silný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
slabé	slabý	k2eAgInPc4d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
peněženku	peněženka	k1gFnSc4	peněženka
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
jim	on	k3xPp3gMnPc3	on
1	[number]	k4	1
500	[number]	k4	500
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
je	on	k3xPp3gInPc4	on
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
celý	celý	k2eAgInSc4d1	celý
film	film	k1gInSc4	film
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
Vega	Vega	k1gMnSc1	Vega
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
Marselluse	Marselluse	k1gFnSc1	Marselluse
Wallace	Wallace	k1gFnSc1	Wallace
(	(	kIx(	(
<g/>
Vincent	Vincent	k1gMnSc1	Vincent
Vega	Vega	k1gMnSc1	Vega
and	and	k?	and
Marsellus	Marsellus	k1gMnSc1	Marsellus
Wallace	Wallace	k1gFnSc2	Wallace
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wife	Wife	k1gFnSc7	Wife
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chronologicky	chronologicky	k6eAd1	chronologicky
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Vincent	Vincent	k1gMnSc1	Vincent
Vega	Vega	k1gMnSc1	Vega
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
první	první	k4xOgFnSc2	první
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
nakoupí	nakoupit	k5eAaPmIp3nS	nakoupit
u	u	k7c2	u
dealera	dealer	k1gMnSc2	dealer
Lance	lance	k1gNnSc2	lance
(	(	kIx(	(
<g/>
Eric	Eric	k1gFnSc1	Eric
Stoltz	Stoltz	k1gMnSc1	Stoltz
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
drogu	droga	k1gFnSc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
jde	jít	k5eAaImIp3nS	jít
na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
s	s	k7c7	s
Miou	Mioa	k1gFnSc4	Mioa
Wallaceovou	Wallaceův	k2eAgFnSc7d1	Wallaceova
<g/>
,	,	kIx,	,
ženou	žena	k1gFnSc7	žena
Marselluse	Marselluse	k1gFnSc2	Marselluse
Wallace	Wallace	k1gFnSc2	Wallace
(	(	kIx(	(
<g/>
Uma	uma	k1gFnSc1	uma
Thurman	Thurman	k1gMnSc1	Thurman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
baru	bar	k1gInSc2	bar
u	u	k7c2	u
Mazaného	mazaný	k2eAgMnSc2d1	mazaný
králíčka	králíček	k1gMnSc2	králíček
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
žádné	žádný	k3yNgNnSc4	žádný
rande	rande	k1gNnSc4	rande
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Vincent	Vincent	k1gMnSc1	Vincent
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
o	o	k7c4	o
přátelskou	přátelský	k2eAgFnSc4d1	přátelská
večeři	večeře	k1gFnSc4	večeře
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
má	mít	k5eAaImIp3nS	mít
Vincent	Vincent	k1gMnSc1	Vincent
nějak	nějak	k6eAd1	nějak
zabavit	zabavit	k5eAaPmF	zabavit
ženu	žena	k1gFnSc4	žena
svého	svůj	k3xOyFgMnSc2	svůj
bosse	boss	k1gMnSc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
Přátelsky	přátelsky	k6eAd1	přátelsky
si	se	k3xPyFc3	se
povídají	povídat	k5eAaImIp3nP	povídat
<g/>
,	,	kIx,	,
tančí	tančit	k5eAaImIp3nP	tančit
v	v	k7c6	v
taneční	taneční	k2eAgFnSc6d1	taneční
soutěži	soutěž	k1gFnSc6	soutěž
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
restaurací	restaurace	k1gFnSc7	restaurace
a	a	k8xC	a
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
pak	pak	k6eAd1	pak
vezme	vzít	k5eAaPmIp3nS	vzít
Miu	Miu	k1gFnSc7	Miu
Wallaceovou	Wallaceův	k2eAgFnSc7d1	Wallaceova
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Bojí	bát	k5eAaImIp3nS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
Mia	Mia	k1gFnSc1	Mia
bude	být	k5eAaImBp3nS	být
svádět	svádět	k5eAaImF	svádět
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zkoušku	zkouška	k1gFnSc4	zkouška
jeho	jeho	k3xOp3gFnSc2	jeho
loajality	loajalita	k1gFnSc2	loajalita
vůči	vůči	k7c3	vůči
Marsellusovi	Marsellus	k1gMnSc3	Marsellus
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
proto	proto	k8xC	proto
co	co	k3yInSc1	co
nejrychleji	rychle	k6eAd3	rychle
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
jakýmkoliv	jakýkoliv	k3yIgNnPc3	jakýkoliv
rizikům	riziko	k1gNnPc3	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
doma	doma	k6eAd1	doma
na	na	k7c6	na
záchodě	záchod	k1gInSc6	záchod
<g/>
,	,	kIx,	,
Mia	Mia	k1gFnSc1	Mia
se	se	k3xPyFc4	se
přehrabuje	přehrabovat	k5eAaImIp3nS	přehrabovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
věcech	věc	k1gFnPc6	věc
a	a	k8xC	a
předávkuje	předávkovat	k5eAaPmIp3nS	předávkovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
zpanikaří	zpanikařit	k5eAaPmIp3nS	zpanikařit
<g/>
,	,	kIx,	,
jede	jet	k5eAaImIp3nS	jet
k	k	k7c3	k
Lanceovi	Lanceus	k1gMnSc3	Lanceus
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
povede	povést	k5eAaPmIp3nS	povést
zachránit	zachránit	k5eAaPmF	zachránit
Mii	Mii	k1gMnPc3	Mii
život	život	k1gInSc4	život
injekcí	injekce	k1gFnPc2	injekce
adrenalinu	adrenalin	k1gInSc2	adrenalin
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Zlaté	zlatý	k2eAgFnPc4d1	zlatá
hodinky	hodinka	k1gFnPc4	hodinka
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Gold	Gold	k1gMnSc1	Gold
Watch	Watch	k1gMnSc1	Watch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chronologicky	chronologicky	k6eAd1	chronologicky
třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
boxer	boxer	k1gMnSc1	boxer
Butch	Butch	k1gMnSc1	Butch
Coolidge	Coolidge	k1gFnSc1	Coolidge
(	(	kIx(	(
<g/>
Bruce	Bruce	k1gFnSc1	Bruce
Willis	Willis	k1gFnSc2	Willis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
od	od	k7c2	od
Marselluse	Marselluse	k1gFnSc2	Marselluse
uplatit	uplatit	k5eAaPmF	uplatit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prohrál	prohrát	k5eAaPmAgMnS	prohrát
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
neprohraje	prohrát	k5eNaPmIp3nS	prohrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabije	zabít	k5eAaPmIp3nS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Utíká	utíkat	k5eAaImIp3nS	utíkat
z	z	k7c2	z
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
před	před	k7c7	před
Marsellusem	Marsellus	k1gInSc7	Marsellus
<g/>
,	,	kIx,	,
při	při	k7c6	při
vystěhovávání	vystěhovávání	k1gNnSc6	vystěhovávání
svého	své	k1gNnSc2	své
bytu	byt	k1gInSc2	byt
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Fabienne	Fabienn	k1gInSc5	Fabienn
(	(	kIx(	(
<g/>
Maria	Maria	k1gFnSc1	Maria
de	de	k?	de
Medeiros	Medeirosa	k1gFnPc2	Medeirosa
<g/>
)	)	kIx)	)
zapomene	zapomnět	k5eAaImIp3nS	zapomnět
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
nočním	noční	k2eAgInSc6d1	noční
stolku	stolek	k1gInSc6	stolek
zlaté	zlatý	k2eAgFnPc4d1	zlatá
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Butch	Butch	k1gInSc1	Butch
zdědil	zdědit	k5eAaPmAgInS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
padlém	padlý	k2eAgInSc6d1	padlý
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
cení	cenit	k5eAaImIp3nS	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Butch	Butch	k1gMnSc1	Butch
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hodinky	hodinka	k1gFnPc4	hodinka
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
někdo	někdo	k3yInSc1	někdo
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
a	a	k8xC	a
opravdu	opravdu	k6eAd1	opravdu
čeká	čekat	k5eAaImIp3nS	čekat
-	-	kIx~	-
Vincent	Vincent	k1gMnSc1	Vincent
Vega	Vega	k1gMnSc1	Vega
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
na	na	k7c6	na
záchodě	záchod	k1gInSc6	záchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Marsellus	Marsellus	k1gMnSc1	Marsellus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
momentálně	momentálně	k6eAd1	momentálně
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
šel	jít	k5eAaImAgMnS	jít
nakoupit	nakoupit	k5eAaPmF	nakoupit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
svou	svůj	k3xOyFgFnSc4	svůj
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
Butch	Butch	k1gInSc4	Butch
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
Vincenta	Vincent	k1gMnSc4	Vincent
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jede	jet	k5eAaImIp3nS	jet
autem	aut	k1gInSc7	aut
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
potká	potkat	k5eAaPmIp3nS	potkat
Marselluse	Marselluse	k1gFnSc1	Marselluse
vracejícího	vracející	k2eAgNnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
krámu	krám	k1gInSc2	krám
<g/>
.	.	kIx.	.
</s>
<s>
Pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
přejet	přejet	k5eAaPmF	přejet
<g/>
,	,	kIx,	,
Marsellus	Marsellus	k1gMnSc1	Marsellus
ale	ale	k8xC	ale
přežije	přežít	k5eAaPmIp3nS	přežít
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
pistolí	pistol	k1gFnSc7	pistol
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
skončí	skončit	k5eAaPmIp3nS	skončit
rvačkou	rvačka	k1gFnSc7	rvačka
v	v	k7c6	v
krámku	krámek	k1gInSc6	krámek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Butch	Butch	k1gInSc1	Butch
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
<g/>
.	.	kIx.	.
</s>
<s>
Rvačku	rvačka	k1gFnSc4	rvačka
přeruší	přerušit	k5eAaPmIp3nS	přerušit
prodavač	prodavač	k1gMnSc1	prodavač
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gNnPc4	on
namíří	namířit	k5eAaPmIp3nP	namířit
brokovnici	brokovnice	k1gFnSc3	brokovnice
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
krámě	krám	k1gInSc6	krám
nikdo	nikdo	k3yNnSc1	nikdo
zabíjet	zabíjet	k5eAaImF	zabíjet
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Zed	Zed	k1gFnSc6	Zed
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zavolá	zavolat	k5eAaPmIp3nS	zavolat
Zeda	Zeda	k1gMnSc1	Zeda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
přijede	přijet	k5eAaPmIp3nS	přijet
<g/>
.	.	kIx.	.
</s>
<s>
Spoutají	spoutat	k5eAaPmIp3nP	spoutat
Butche	Butch	k1gFnPc4	Butch
i	i	k8xC	i
Marselluse	Marselluse	k1gFnPc4	Marselluse
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
znásilnit	znásilnit	k5eAaPmF	znásilnit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Zed	Zed	k1gFnSc1	Zed
znásilňuje	znásilňovat	k5eAaImIp3nS	znásilňovat
Marselluse	Marselluse	k1gFnSc1	Marselluse
a	a	k8xC	a
prodavač	prodavač	k1gMnSc1	prodavač
Maynard	Maynard	k1gMnSc1	Maynard
tomu	ten	k3xDgNnSc3	ten
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
<g/>
,	,	kIx,	,
Butchovi	Butch	k1gMnSc6	Butch
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
uvolnit	uvolnit	k5eAaPmF	uvolnit
z	z	k7c2	z
pout	pouto	k1gNnPc2	pouto
<g/>
.	.	kIx.	.
</s>
<s>
Najde	najít	k5eAaPmIp3nS	najít
v	v	k7c6	v
krámku	krámek	k1gInSc6	krámek
katanu	katan	k1gMnSc3	katan
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
zabije	zabít	k5eAaPmIp3nS	zabít
prodavače	prodavač	k1gMnSc4	prodavač
a	a	k8xC	a
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
i	i	k9	i
Zeda	Zed	k2eAgFnSc1d1	Zed
<g/>
.	.	kIx.	.
</s>
<s>
Marsellus	Marsellus	k1gInSc1	Marsellus
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
odtáhne	odtáhnout	k5eAaPmIp3nS	odtáhnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Zedem	Zedem	k1gInSc1	Zedem
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
vyřídí	vyřídit	k5eAaPmIp3nS	vyřídit
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Střelí	střelit	k5eAaPmIp3nS	střelit
ho	on	k3xPp3gMnSc4	on
brokovnicí	brokovnice	k1gFnSc7	brokovnice
do	do	k7c2	do
koulí	koule	k1gFnPc2	koule
a	a	k8xC	a
slíbí	slíbit	k5eAaPmIp3nS	slíbit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
prdel	prdel	k1gFnSc1	prdel
pozná	poznat	k5eAaPmIp3nS	poznat
středověk	středověk	k1gInSc4	středověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Butchem	Butch	k1gInSc7	Butch
je	být	k5eAaImIp3nS	být
Marsellus	Marsellus	k1gInSc1	Marsellus
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
zachránil	zachránit	k5eAaPmAgInS	zachránit
mu	on	k3xPp3gMnSc3	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
povolí	povolit	k5eAaPmIp3nS	povolit
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
odjet	odjet	k5eAaPmF	odjet
z	z	k7c2	z
města	město	k1gNnSc2	město
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
odplatě	odplata	k1gFnSc6	odplata
nikdy	nikdy	k6eAd1	nikdy
nikomu	nikdo	k3yNnSc3	nikdo
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
.	.	kIx.	.
</s>
