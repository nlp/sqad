<s>
Semenáč	semenáč	k1gInSc1	semenáč
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
generativním	generativní	k2eAgInSc7d1	generativní
způsobem	způsob	k1gInSc7	způsob
<g/>
;	;	kIx,	;
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
roubovanými	roubovaný	k2eAgFnPc7d1	roubovaná
nebo	nebo	k8xC	nebo
řízkovanými	řízkovaný	k2eAgFnPc7d1	řízkovaný
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
klony	klon	k1gInPc1	klon
matečné	matečný	k2eAgFnSc2d1	matečná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
