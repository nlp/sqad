<p>
<s>
Semenáč	semenáč	k1gInSc1	semenáč
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
generativním	generativní	k2eAgInSc7d1	generativní
způsobem	způsob	k1gInSc7	způsob
<g/>
;	;	kIx,	;
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
roubovanými	roubovaný	k2eAgFnPc7d1	roubovaná
nebo	nebo	k8xC	nebo
řízkovanými	řízkovaný	k2eAgFnPc7d1	řízkovaný
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
klony	klon	k1gInPc1	klon
matečné	matečný	k2eAgFnSc2d1	matečná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Generativně	generativně	k6eAd1	generativně
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
semenáčů	semenáč	k1gInPc2	semenáč
v	v	k7c6	v
ovocnictví	ovocnictví	k1gNnSc6	ovocnictví
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
jabloní	jabloň	k1gFnPc2	jabloň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
semenáče	semenáč	k1gInPc1	semenáč
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
také	také	k9	také
pláňky	pláňka	k1gFnSc2	pláňka
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
podnože	podnož	k1gFnPc1	podnož
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
jsou	být	k5eAaImIp3nP	být
roubovány	roubován	k2eAgFnPc1d1	roubována
či	či	k8xC	či
očkovány	očkován	k2eAgFnPc1d1	očkována
různé	různý	k2eAgFnPc1d1	různá
ušlechtilé	ušlechtilý	k2eAgFnPc1d1	ušlechtilá
odrůdy	odrůda	k1gFnPc1	odrůda
téhož	týž	k3xTgInSc2	týž
nebo	nebo	k8xC	nebo
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
druhu	druh	k1gInSc2	druh
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Semenáček	semenáček	k1gInSc4	semenáček
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrobnělé	zdrobnělý	k2eAgNnSc1d1	zdrobnělé
slovo	slovo	k1gNnSc1	slovo
semenáček	semenáček	k1gInSc1	semenáček
je	být	k5eAaImIp3nS	být
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
klíční	klíční	k2eAgFnSc4d1	klíční
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
užší	úzký	k2eAgNnSc4d2	užší
vymezení	vymezení	k1gNnSc4	vymezení
slova	slovo	k1gNnSc2	slovo
semenáč	semenáč	k1gInSc4	semenáč
na	na	k7c4	na
juvenilní	juvenilní	k2eAgNnPc4d1	juvenilní
stádia	stádium	k1gNnPc4	stádium
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
tak	tak	k9	tak
označují	označovat	k5eAaImIp3nP	označovat
pouze	pouze	k6eAd1	pouze
rostliny	rostlina	k1gFnPc1	rostlina
od	od	k7c2	od
vyklíčení	vyklíčení	k1gNnSc2	vyklíčení
do	do	k7c2	do
opadu	opad	k1gInSc2	opad
typických	typický	k2eAgInPc2d1	typický
děložních	děložní	k2eAgInPc2d1	děložní
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Klíční	klíční	k2eAgFnSc1d1	klíční
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
díky	díky	k7c3	díky
odlišným	odlišný	k2eAgInPc3d1	odlišný
děložním	děložní	k2eAgInPc3d1	děložní
a	a	k8xC	a
primárním	primární	k2eAgInPc3d1	primární
listům	list	k1gInPc3	list
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
obtížně	obtížně	k6eAd1	obtížně
určitelná	určitelný	k2eAgFnSc1d1	určitelná
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
odlišnost	odlišnost	k1gFnSc1	odlišnost
děložních	děložní	k2eAgMnPc2d1	děložní
a	a	k8xC	a
"	"	kIx"	"
<g/>
normálních	normální	k2eAgInPc2d1	normální
<g/>
"	"	kIx"	"
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
podoby	podoba	k1gFnSc2	podoba
semenáčků	semenáček	k1gMnPc2	semenáček
plevelů	plevel	k1gInPc2	plevel
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
rostlin	rostlina	k1gFnPc2	rostlina
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
zemědělce	zemědělec	k1gMnPc4	zemědělec
<g/>
,	,	kIx,	,
zahradníky	zahradník	k1gMnPc4	zahradník
nebo	nebo	k8xC	nebo
lesníky	lesník	k1gMnPc4	lesník
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
správná	správný	k2eAgFnSc1d1	správná
determinace	determinace	k1gFnSc1	determinace
druhu	druh	k1gInSc2	druh
již	již	k6eAd1	již
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slova	slovo	k1gNnPc4	slovo
semenáček	semenáček	k1gInSc1	semenáček
nebo	nebo	k8xC	nebo
semenáč	semenáč	k1gInSc1	semenáč
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
nesprávně	správně	k6eNd1	správně
používána	používán	k2eAgNnPc1d1	používáno
jako	jako	k8xC	jako
synonyma	synonymum	k1gNnPc1	synonymum
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
sazenice	sazenice	k1gFnSc2	sazenice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školkařských	školkařský	k2eAgInPc6d1	školkařský
provozech	provoz	k1gInPc6	provoz
(	(	kIx(	(
<g/>
lesních	lesní	k2eAgFnPc6d1	lesní
a	a	k8xC	a
okrasných	okrasný	k2eAgFnPc6d1	okrasná
školkách	školka	k1gFnPc6	školka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xC	jako
semenáčky	semenáček	k1gInPc1	semenáček
označovány	označovat	k5eAaImNgInP	označovat
zpravidla	zpravidla	k6eAd1	zpravidla
maximálně	maximálně	k6eAd1	maximálně
jednoleté	jednoletý	k2eAgFnPc4d1	jednoletá
rostlinky	rostlinka	k1gFnPc4	rostlinka
vzešlé	vzešlý	k2eAgFnPc4d1	vzešlá
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
před	před	k7c7	před
školkováním	školkování	k1gNnSc7	školkování
nebo	nebo	k8xC	nebo
podřezáváním	podřezávání	k1gNnSc7	podřezávání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
operacích	operace	k1gFnPc6	operace
bývají	bývat	k5eAaImIp3nP	bývat
školkováním	školkování	k1gNnSc7	školkování
nebo	nebo	k8xC	nebo
podřezáváním	podřezávání	k1gNnSc7	podřezávání
zesílené	zesílený	k2eAgFnSc2d1	zesílená
rostliny	rostlina	k1gFnSc2	rostlina
označované	označovaný	k2eAgFnPc1d1	označovaná
již	již	k6eAd1	již
jako	jako	k9	jako
sazenice	sazenice	k1gFnSc1	sazenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
