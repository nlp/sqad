<s>
Skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalové	metalový	k2eAgFnPc4d1	metalová
a	a	k8xC	a
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgFnSc6d1	rocková
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
paradoxně	paradoxně	k6eAd1	paradoxně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
glam	glam	k6eAd1	glam
metalovými	metalový	k2eAgFnPc7d1	metalová
kapelami	kapela	k1gFnPc7	kapela
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
žánrovou	žánrový	k2eAgFnSc7d1	žánrová
stylovostí	stylovost	k1gFnSc7	stylovost
<g/>
.	.	kIx.	.
</s>
