<s>
Králové	Král	k1gMnPc1
zlodějů	zloděj	k1gMnPc2
</s>
<s>
Králové	Králové	k2eAgMnPc2d1
zlodějů	zloděj	k1gMnPc2
Původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
King	King	k1gMnSc1
of	of	k?
Thieves	Thieves	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
108	#num#	k4
min	mina	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
kriminální	kriminální	k2eAgNnSc1d1
filmfilmové	filmfilmový	k2eAgNnSc1d1
drama	drama	k1gNnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Joe	Joe	k?
Penhall	Penhall	k1gInSc1
Režie	režie	k1gFnSc1
</s>
<s>
James	James	k1gInSc1
Marsh	Marsha	k1gFnPc2
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
CaineMichael	CaineMichael	k1gMnSc1
GambonCharlie	GambonCharlie	k1gFnSc2
CoxRay	CoxRaa	k1gFnSc2
WinstoneJim	WinstoneJim	k1gMnSc1
Broadbent	Broadbent	k1gMnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Produkce	produkce	k1gFnSc1
</s>
<s>
Tim	Tim	k?
BevanEric	BevanEric	k1gMnSc1
Fellner	Fellner	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Wallfisch	Wallfisch	k1gMnSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Danny	Dann	k1gInPc1
Cohen	Cohen	k2eAgInSc1d1
Střih	střih	k1gInSc4
</s>
<s>
Jinx	Jinx	k1gInSc1
Godfrey	Godfrea	k1gMnSc2
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Working	Working	k1gInSc1
Title	titla	k1gFnSc6
Films	Filmsa	k1gFnPc2
Distribuce	distribuce	k1gFnSc1
</s>
<s>
StudioCanal	StudioCanat	k5eAaImAgMnS,k5eAaPmAgMnS
Králové	Králová	k1gFnPc4
zlodějů	zloděj	k1gMnPc2
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Králové	Král	k1gMnPc1
zlodějů	zloděj	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
King	King	k1gMnSc1
of	of	k?
Thieves	Thieves	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgInSc1d1
kriminální	kriminální	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režíroval	režírovat	k5eAaImAgMnS
jej	on	k3xPp3gNnSc4
James	James	k1gMnSc1
Marsh	Marsh	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
natáčení	natáčení	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
ještě	ještě	k9
před	před	k7c7
uvedením	uvedení	k1gNnSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
předešlého	předešlý	k2eAgInSc2d1
snímku	snímek	k1gInSc2
The	The	k1gMnSc2
Mercy	Merca	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočen	natočen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
podle	podle	k7c2
skutečných	skutečný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
proběhly	proběhnout	k5eAaPmAgFnP
roku	rok	k1gInSc2
2015	#num#	k4
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Hatton	Hatton	k1gInSc1
Garden	Gardna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
důchodců	důchodce	k1gMnPc2
zde	zde	k6eAd1
tehdy	tehdy	k6eAd1
provedla	provést	k5eAaPmAgFnS
loupež	loupež	k1gFnSc4
šperků	šperk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
snímku	snímek	k1gInSc6
hrají	hrát	k5eAaImIp3nP
Michael	Michael	k1gMnSc1
Caine	Cain	k1gInSc5
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Gambon	Gambon	k1gMnSc1
<g/>
,	,	kIx,
Ray	Ray	k1gMnSc1
Winstone	Winston	k1gInSc5
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
Courtenay	Courtenaa	k1gFnSc2
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Broadbent	Broadbent	k1gMnSc1
<g/>
,	,	kIx,
Charlie	Charlie	k1gMnSc1
Cox	Cox	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudbu	hudba	k1gFnSc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
složil	složit	k5eAaPmAgMnS
Benjamin	Benjamin	k1gMnSc1
Wallfisch	Wallfisch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
premiéra	premiéra	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
14	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Recenze	recenze	k1gFnSc1
</s>
<s>
Mirka	Mirka	k1gFnSc1
Spáčilová	Spáčilová	k1gFnSc1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
FLEMING	FLEMING	kA
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gMnSc1
Marsh	Marsh	k1gMnSc1
&	&	k?
Joe	Joe	k1gMnSc1
Penhall	Penhall	k1gMnSc1
Conspire	Conspir	k1gInSc5
On	on	k3xPp3gMnSc1
Working	Working	k1gInSc1
Title	titla	k1gFnSc3
<g/>
’	’	k?
<g/>
s	s	k7c7
Hatton	Hatton	k1gInSc4
Garden	Gardna	k1gFnPc2
Jewel	Jewel	k1gMnSc1
Heist	Heist	k1gMnSc1
Caper	Caper	k1gMnSc1
Pic	pic	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deadline	Deadlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2016-06-07	2016-06-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Králové	Král	k1gMnPc1
zlodějů	zloděj	k1gMnPc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Králové	Králová	k1gFnPc1
zlodějů	zloděj	k1gMnPc2
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Filmy	film	k1gInPc1
režírované	režírovaný	k2eAgInPc1d1
Jamesem	James	k1gMnSc7
Marshem	Marsh	k1gInSc7
</s>
<s>
The	The	k?
Animator	Animator	k1gInSc1
of	of	k?
Prague	Prague	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Cale	Cal	k1gMnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wisconsinský	wisconsinský	k2eAgInSc4d1
výlet	výlet	k1gInSc4
za	za	k7c7
smrtí	smrt	k1gFnSc7
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Team	team	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc1
King	King	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Muž	muž	k1gMnSc1
na	na	k7c6
laně	lano	k1gNnSc6
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Vraždy	vražda	k1gFnSc2
v	v	k7c6
Yorkshiru	Yorkshiro	k1gNnSc6
<g/>
:	:	kIx,
1980	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Project	Projecta	k1gFnPc2
Nim	on	k3xPp3gMnPc3
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Shadow	Shadow	k1gMnSc1
Dancer	Dancer	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teorie	teorie	k1gFnSc1
všeho	všecek	k3xTgNnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc2
Mercy	Merca	k1gMnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Králové	Králové	k2eAgMnPc2d1
zlodějů	zloděj	k1gMnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
