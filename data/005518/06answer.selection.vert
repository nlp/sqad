<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
piva	pivo	k1gNnSc2	pivo
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
výrobu	výroba	k1gFnSc4	výroba
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
na	na	k7c4	na
1	[number]	k4	1
litr	litr	k1gInSc4	litr
vystaveného	vystavený	k2eAgNnSc2d1	vystavené
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
spotřebováno	spotřebován	k2eAgNnSc1d1	spotřebováno
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
až	až	k9	až
osmkrát	osmkrát	k6eAd1	osmkrát
více	hodně	k6eAd2	hodně
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
