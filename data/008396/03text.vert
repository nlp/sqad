<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgFnPc2d1	národní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stěžejní	stěžejní	k2eAgNnSc1d1	stěžejní
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
původní	původní	k2eAgFnSc1d1	původní
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kdy	kdy	k6eAd1	kdy
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sbírku	sbírka	k1gFnSc4	sbírka
13	[number]	k4	13
balad	balada	k1gFnPc2	balada
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
lidovou	lidový	k2eAgFnSc7d1	lidová
slovesností	slovesnost	k1gFnSc7	slovesnost
poukazující	poukazující	k2eAgFnSc7d1	poukazující
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
Kytice	kytice	k1gFnSc1	kytice
==	==	k?	==
</s>
</p>
<p>
<s>
Polovina	polovina	k1gFnSc1	polovina
básní	báseň	k1gFnPc2	báseň
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
a	a	k8xC	a
otištěna	otištěn	k2eAgFnSc1d1	otištěna
před	před	k7c7	před
souhrnným	souhrnný	k2eAgNnSc7d1	souhrnné
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
Polednice	polednice	k1gFnSc1	polednice
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
tiskem	tisk	k1gInSc7	tisk
vydána	vydán	k2eAgFnSc1d1	vydána
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
a	a	k8xC	a
Poklad	poklad	k1gInSc4	poklad
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
tiskem	tisek	k1gMnSc7	tisek
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
je	být	k5eAaImIp3nS	být
též	též	k9	též
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kolovrat	kolovrat	k1gInSc4	kolovrat
<g/>
,	,	kIx,	,
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
a	a	k8xC	a
Holoubek	Holoubek	k1gMnSc1	Holoubek
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
balady	balada	k1gFnPc1	balada
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Raná	raný	k2eAgNnPc1d1	rané
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
Kytice	kytice	k1gFnSc2	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgMnPc2d1	národní
vydal	vydat	k5eAaPmAgMnS	vydat
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
pražský	pražský	k2eAgMnSc1d1	pražský
nakladatel	nakladatel	k1gMnSc1	nakladatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
12	[number]	k4	12
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
ilustrována	ilustrován	k2eAgFnSc1d1	ilustrována
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
nakladatel	nakladatel	k1gMnSc1	nakladatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c4	o
baladu	balada	k1gFnSc4	balada
Lilie	lilie	k1gFnSc2	lilie
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
oddíl	oddíl	k1gInSc1	oddíl
Písně	píseň	k1gFnSc2	píseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgNnPc4	dva
Pospíšilova	Pospíšilův	k2eAgNnPc4d1	Pospíšilovo
vydání	vydání	k1gNnPc4	vydání
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
a	a	k8xC	a
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
dřevorytec	dřevorytec	k1gMnSc1	dřevorytec
František	František	k1gMnSc1	František
Richter	Richter	k1gMnSc1	Richter
st.	st.	kA	st.
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgNnSc1	třetí
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
sice	sice	k8xC	sice
až	až	k6eAd1	až
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jím	on	k3xPp3gNnSc7	on
ještě	ještě	k6eAd1	ještě
připraveno	připravit	k5eAaPmNgNnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
oddíl	oddíl	k1gInSc1	oddíl
Písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
charakteristika	charakteristika	k1gFnSc1	charakteristika
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
tvoří	tvořit	k5eAaImIp3nP	tvořit
balady	balada	k1gFnPc1	balada
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
sám	sám	k3xTgMnSc1	sám
Erben	Erben	k1gMnSc1	Erben
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
nikdy	nikdy	k6eAd1	nikdy
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
básník	básník	k1gMnSc1	básník
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
(	(	kIx(	(
<g/>
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
<g/>
)	)	kIx)	)
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
Kytice	kytice	k1gFnSc1	kytice
sbírkou	sbírka	k1gFnSc7	sbírka
ohlasovou	ohlasový	k2eAgFnSc7d1	ohlasová
a	a	k8xC	a
projevem	projev	k1gInSc7	projev
folkloristicky	folkloristicky	k6eAd1	folkloristicky
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenková	myšlenkový	k2eAgFnSc1d1	myšlenková
báze	báze	k1gFnSc1	báze
a	a	k8xC	a
etický	etický	k2eAgInSc1d1	etický
princip	princip	k1gInSc1	princip
básní	báseň	k1gFnPc2	báseň
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
klasicistní	klasicistní	k2eAgInPc4d1	klasicistní
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
básním	báseň	k1gFnPc3	báseň
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
poznámky	poznámka	k1gFnPc1	poznámka
(	(	kIx(	(
<g/>
Poznamenání	poznamenání	k1gNnSc1	poznamenání
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgInPc1d1	obsahující
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
původu	původ	k1gInSc6	původ
či	či	k8xC	či
o	o	k7c6	o
jinoslovanských	jinoslovanský	k2eAgFnPc6d1	jinoslovanský
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
pro	pro	k7c4	pro
Erbena	Erben	k1gMnSc4	Erben
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
otázkou	otázka	k1gFnSc7	otázka
lidských	lidský	k2eAgInPc2d1	lidský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
ho	on	k3xPp3gMnSc4	on
především	především	k9	především
zajímá	zajímat	k5eAaImIp3nS	zajímat
problém	problém	k1gInSc1	problém
viny	vina	k1gFnSc2	vina
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Erbena	Erben	k1gMnSc2	Erben
je	být	k5eAaImIp3nS	být
nejzákladnějším	základní	k2eAgInSc7d3	nejzákladnější
lidským	lidský	k2eAgInSc7d1	lidský
vztahem	vztah	k1gInSc7	vztah
pouto	pouto	k1gNnSc1	pouto
mezi	mezi	k7c7	mezi
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Konflikty	konflikt	k1gInPc1	konflikt
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
životě	život	k1gInSc6	život
vznikají	vznikat	k5eAaImIp3nP	vznikat
především	především	k6eAd1	především
narušením	narušení	k1gNnSc7	narušení
základních	základní	k2eAgInPc2d1	základní
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
vinu	vina	k1gFnSc4	vina
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepřiměřený	přiměřený	k2eNgInSc1d1	nepřiměřený
<g/>
,	,	kIx,	,
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
pojetí	pojetí	k1gNnSc6	pojetí
bezmocný	bezmocný	k2eAgInSc4d1	bezmocný
proti	proti	k7c3	proti
přírodním	přírodní	k2eAgFnPc3d1	přírodní
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
ho	on	k3xPp3gMnSc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Vinu	vina	k1gFnSc4	vina
vždy	vždy	k6eAd1	vždy
nese	nést	k5eAaImIp3nS	nést
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
neromantickým	romantický	k2eNgInSc7d1	neromantický
znakem	znak	k1gInSc7	znak
balad	balada	k1gFnPc2	balada
je	být	k5eAaImIp3nS	být
naprostá	naprostý	k2eAgFnSc1d1	naprostá
nevzdorovitost	nevzdorovitost	k1gFnSc1	nevzdorovitost
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zcela	zcela	k6eAd1	zcela
bázlivě	bázlivě	k6eAd1	bázlivě
a	a	k8xC	a
s	s	k7c7	s
pokorou	pokora	k1gFnSc7	pokora
přijímají	přijímat	k5eAaImIp3nP	přijímat
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
individualistické	individualistický	k2eAgFnPc1d1	individualistická
tendence	tendence	k1gFnPc1	tendence
ani	ani	k8xC	ani
typická	typický	k2eAgFnSc1d1	typická
rozpolcenost	rozpolcenost	k1gFnSc1	rozpolcenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časoprostor	časoprostor	k1gInSc1	časoprostor
básní	báseň	k1gFnPc2	báseň
je	být	k5eAaImIp3nS	být
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
,	,	kIx,	,
symbolický	symbolický	k2eAgInSc1d1	symbolický
(	(	kIx(	(
<g/>
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
chalupa	chalupa	k1gFnSc1	chalupa
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Erben	Erben	k1gMnSc1	Erben
nemluví	mluvit	k5eNaImIp3nS	mluvit
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
ani	ani	k8xC	ani
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
jakoby	jakoby	k8xS	jakoby
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
prvek	prvek	k1gInSc1	prvek
determinismu	determinismus	k1gInSc2	determinismus
(	(	kIx(	(
<g/>
osudovosti	osudovost	k1gFnSc2	osudovost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
Štědrém	štědrý	k2eAgInSc6d1	štědrý
dnu	den	k1gInSc6	den
a	a	k8xC	a
Věštkyni	věštkyně	k1gFnSc6	věštkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgInPc4d1	jazykový
prostředky	prostředek	k1gInPc4	prostředek
==	==	k?	==
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c4	na
popisy	popis	k1gInPc4	popis
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
romantických	romantický	k2eAgFnPc2d1	romantická
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
básně	báseň	k1gFnPc1	báseň
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
,	,	kIx,	,
soustředí	soustředit	k5eAaPmIp3nP	soustředit
na	na	k7c4	na
syžet	syžet	k1gInSc4	syžet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
dialogů	dialog	k1gInPc2	dialog
<g/>
,	,	kIx,	,
úsporného	úsporný	k2eAgInSc2d1	úsporný
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
baladické	baladický	k2eAgFnSc2d1	baladická
zkratky	zkratka	k1gFnSc2	zkratka
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
rychlý	rychlý	k2eAgInSc4d1	rychlý
dějový	dějový	k2eAgInSc4d1	dějový
spád	spád	k1gInSc4	spád
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velké	velký	k2eAgFnSc3d1	velká
dramatičnosti	dramatičnost	k1gFnSc3	dramatičnost
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
gnomickým	gnomický	k2eAgInSc7d1	gnomický
veršem	verš	k1gInSc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP	vytvářet
výstižné	výstižný	k2eAgFnSc2d1	výstižná
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zredukována	zredukovat	k5eAaPmNgFnS	zredukovat
o	o	k7c4	o
zbytečná	zbytečný	k2eAgNnPc4d1	zbytečné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
hodně	hodně	k6eAd1	hodně
onomatopoeií	onomatopoeie	k1gFnPc2	onomatopoeie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Vodníkovi	vodník	k1gMnSc6	vodník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eufonii	eufonie	k1gFnSc4	eufonie
<g/>
,	,	kIx,	,
přirovnání	přirovnání	k1gNnPc4	přirovnání
<g/>
,	,	kIx,	,
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
epitetonů	epiteton	k1gInPc2	epiteton
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
figur	figura	k1gFnPc2	figura
převažují	převažovat	k5eAaImIp3nP	převažovat
anafora	anafora	k1gFnSc1	anafora
<g/>
,	,	kIx,	,
apostrofa	apostrofa	k1gFnSc1	apostrofa
<g/>
,	,	kIx,	,
epifora	epifora	k1gFnSc1	epifora
a	a	k8xC	a
epizeuxis	epizeuxis	k1gFnSc1	epizeuxis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kompozice	kompozice	k1gFnSc2	kompozice
==	==	k?	==
</s>
</p>
<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13	[number]	k4	13
básní	báseň	k1gFnPc2	báseň
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
žánrově	žánrově	k6eAd1	žánrově
i	i	k9	i
obsahově	obsahově	k6eAd1	obsahově
ostatním	ostatní	k2eAgMnPc3d1	ostatní
vymykají	vymykat	k5eAaImIp3nP	vymykat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgFnSc1d1	svatební
košile	košile	k1gFnSc1	košile
</s>
</p>
<p>
<s>
Polednice	polednice	k1gFnSc1	polednice
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
</s>
</p>
<p>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Holoubek	Holoubek	k1gMnSc1	Holoubek
</s>
</p>
<p>
<s>
Záhořovo	Záhořův	k2eAgNnSc1d1	Záhořovo
lože	lože	k1gNnSc1	lože
</s>
</p>
<p>
<s>
Vodník	vodník	k1gMnSc1	vodník
</s>
</p>
<p>
<s>
Vrba	Vrba	k1gMnSc1	Vrba
</s>
</p>
<p>
<s>
Lilie	lilie	k1gFnSc1	lilie
</s>
</p>
<p>
<s>
Dceřina	dceřin	k2eAgFnSc1d1	dceřina
kletba	kletba	k1gFnSc1	kletba
</s>
</p>
<p>
<s>
VěštkyněV	VěštkyněV	k?	VěštkyněV
původním	původní	k2eAgNnSc6d1	původní
vydání	vydání	k1gNnSc6	vydání
byly	být	k5eAaImAgFnP	být
básně	báseň	k1gFnPc1	báseň
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
básně	báseň	k1gFnPc1	báseň
stejně	stejně	k6eAd1	stejně
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
od	od	k7c2	od
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
tematicky	tematicky	k6eAd1	tematicky
blízké	blízký	k2eAgInPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
-	-	kIx~	-
Věštkyně	věštkyně	k1gFnPc1	věštkyně
<g/>
:	:	kIx,	:
Vlastenecké	vlastenecký	k2eAgFnPc1d1	vlastenecká
básně	báseň	k1gFnPc1	báseň
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
prvkem	prvek	k1gInSc7	prvek
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
rámec	rámec	k1gInSc4	rámec
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
-	-	kIx~	-
Dceřina	dceřin	k2eAgFnSc1d1	dceřina
kletba	kletba	k1gFnSc1	kletba
<g/>
:	:	kIx,	:
o	o	k7c6	o
porušení	porušení	k1gNnSc6	porušení
vztahu	vztah	k1gInSc2	vztah
matka	matka	k1gFnSc1	matka
-	-	kIx~	-
dítě	dítě	k1gNnSc1	dítě
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgFnSc1d1	svatební
košile	košile	k1gFnSc1	košile
-	-	kIx~	-
Vrba	vrba	k1gFnSc1	vrba
<g/>
:	:	kIx,	:
proměna	proměna	k1gFnSc1	proměna
člověka	člověk	k1gMnSc2	člověk
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
a	a	k8xC	a
vrbu	vrba	k1gFnSc4	vrba
</s>
</p>
<p>
<s>
Polednice	polednice	k1gFnSc1	polednice
-	-	kIx~	-
Vodník	vodník	k1gMnSc1	vodník
<g/>
:	:	kIx,	:
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
nadpřirozené	nadpřirozený	k2eAgFnPc1d1	nadpřirozená
bytosti	bytost	k1gFnPc1	bytost
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
-	-	kIx~	-
Záhořovo	Záhořův	k2eAgNnSc1d1	Záhořovo
lože	lože	k1gNnSc1	lože
<g/>
:	:	kIx,	:
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
námět	námět	k1gInSc4	námět
<g/>
;	;	kIx,	;
motiv	motiv	k1gInSc4	motiv
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
vykoupení	vykoupení	k1gNnSc2	vykoupení
a	a	k8xC	a
pokání	pokání	k1gNnSc2	pokání
</s>
</p>
<p>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
-	-	kIx~	-
Holoubek	Holoubek	k1gMnSc1	Holoubek
<g/>
:	:	kIx,	:
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
;	;	kIx,	;
neméně	málo	k6eNd2	málo
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
svědomíKarel	svědomíKarel	k1gMnSc1	svědomíKarel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k6eAd1	ještě
balady	balada	k1gFnSc2	balada
Smolný	smolný	k2eAgInSc1d1	smolný
var	var	k1gInSc1	var
a	a	k8xC	a
Cizí	cizí	k2eAgMnSc1d1	cizí
host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
vydání	vydání	k1gNnSc6	vydání
nejsou	být	k5eNaImIp3nP	být
zakomponovány	zakomponován	k2eAgFnPc1d1	zakomponována
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
vyřčena	vyřčen	k2eAgFnSc1d1	vyřčena
příčina	příčina	k1gFnSc1	příčina
lidské	lidský	k2eAgFnSc2d1	lidská
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
(	(	kIx(	(
<g/>
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
sloky	sloka	k1gFnSc2	sloka
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Snad	snad	k9	snad
že	že	k8xS	že
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
dcera	dcera	k1gFnSc1	dcera
mateřina	mateřina	k1gFnSc1	mateřina
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jíž	jenž	k3xRgFnSc3	jenž
mile	mile	k6eAd1	mile
dech	dech	k1gInSc1	dech
tvůj	tvůj	k3xOp2gInSc1	tvůj
zavoní	zavonět	k5eAaPmIp3nS	zavonět
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
snad	snad	k9	snad
že	že	k8xS	že
i	i	k9	i
najdeš	najít	k5eAaPmIp2nS	najít
některého	některý	k3yIgMnSc4	některý
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jenž	jenž	k3xRgMnSc1	jenž
k	k	k7c3	k
tobě	ty	k3xPp2nSc3	ty
srdce	srdce	k1gNnPc4	srdce
nakloní	naklonit	k5eAaPmIp3nP	naklonit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Kytice	kytice	k1gFnSc1	kytice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
když	když	k8xS	když
večer	večer	k6eAd1	večer
pohromadě	pohromadě	k6eAd1	pohromadě
</s>
</p>
<p>
<s>
mládež	mládež	k1gFnSc1	mládež
za	za	k7c2	za
mrazu	mráz	k1gInSc2	mráz
sedává	sedávat	k5eAaImIp3nS	sedávat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rád	rád	k2eAgMnSc1d1	rád
stařeček	stařeček	k1gMnSc1	stařeček
povídává	povídávat	k5eAaImIp3nS	povídávat
</s>
</p>
<p>
<s>
o	o	k7c6	o
vdově	vdova	k1gFnSc6	vdova
a	a	k8xC	a
o	o	k7c6	o
pokladě	poklad	k1gInSc6	poklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Poklad	poklad	k1gInSc4	poklad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
ses	ses	k?	ses
<g/>
,	,	kIx,	,
panno	panna	k1gFnSc5	panna
<g/>
,	,	kIx,	,
radila	radil	k1gMnSc4	radil
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
na	na	k7c4	na
boha	bůh	k1gMnSc4	bůh
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
myslila	myslit	k5eAaImAgFnS	myslit
</s>
</p>
<p>
<s>
a	a	k8xC	a
druha	druh	k1gMnSc2	druh
zlého	zlý	k1gMnSc2	zlý
odbyla	odbýt	k5eAaPmAgFnS	odbýt
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Bys	by	kYmCp2nS	by
byla	být	k5eAaImAgFnS	být
jinak	jinak	k6eAd1	jinak
jednala	jednat	k5eAaImAgFnS	jednat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zle	zle	k6eAd1	zle
bysi	bysi	k6eAd1	bysi
byla	být	k5eAaImAgFnS	být
skonala	skonat	k5eAaPmAgFnS	skonat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tvé	tvůj	k3xOp2gNnSc1	tvůj
tělo	tělo	k1gNnSc1	tělo
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
spanilé	spanilý	k2eAgNnSc1d1	spanilé
</s>
</p>
<p>
<s>
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
co	co	k9	co
ty	ten	k3xDgFnPc4	ten
košile	košile	k1gFnPc4	košile
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Svatební	svatební	k2eAgFnPc4d1	svatební
košile	košile	k1gFnPc4	košile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
mdlobách	mdloba	k1gFnPc6	mdloba
tu	tu	k6eAd1	tu
matka	matka	k1gFnSc1	matka
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
k	k	k7c3	k
ňadrám	ňadra	k1gFnPc3	ňadra
dítě	dítě	k1gNnSc1	dítě
přimknuté	přimknutý	k2eAgFnPc1d1	přimknutá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
matku	matka	k1gFnSc4	matka
vzkřísil	vzkřísit	k5eAaPmAgInS	vzkřísit
ještě	ještě	k6eAd1	ještě
stěží	stěží	k6eAd1	stěží
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
avšak	avšak	k8xC	avšak
dítě	dítě	k1gNnSc1	dítě
–	–	k?	–
zalknuté	zalknutý	k2eAgFnSc2d1	zalknutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Polednice	polednice	k1gFnSc2	polednice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
což	což	k9	což
ten	ten	k3xDgInSc4	ten
zlatý	zlatý	k2eAgInSc4d1	zlatý
kolovrat	kolovrat	k1gInSc4	kolovrat
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Jakou	jaký	k3yRgFnSc4	jaký
teď	teď	k6eAd1	teď
píseň	píseň	k1gFnSc4	píseň
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
do	do	k7c2	do
třetice	třetice	k1gFnSc2	třetice
zahrat	zahrat	k1gInSc1	zahrat
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
nikdo	nikdo	k3yNnSc1	nikdo
neuslyšel	uslyšet	k5eNaPmAgMnS	uslyšet
</s>
</p>
<p>
<s>
ani	ani	k8xC	ani
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Však	však	k9	však
lépe	dobře	k6eAd2	dobře
v	v	k7c6	v
mylné	mylný	k2eAgFnSc6d1	mylná
naději	naděje	k1gFnSc6	naděje
sníti	snít	k5eAaPmF	snít
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
před	před	k7c7	před
sebou	se	k3xPyFc7	se
čirou	čirý	k2eAgFnSc4d1	čirá
temnotu	temnota	k1gFnSc4	temnota
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nežli	nežli	k8xS	nežli
budoucnost	budoucnost	k1gFnSc4	budoucnost
odhaliti	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
strašlivou	strašlivý	k2eAgFnSc4d1	strašlivá
poznati	poznat	k5eAaPmF	poznat
jistotu	jistota	k1gFnSc4	jistota
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Však	však	k9	však
nelze	lze	k6eNd1	lze
kamenu	kámen	k1gInSc2	kámen
</s>
</p>
<p>
<s>
tak	tak	k6eAd1	tak
těžko	těžko	k6eAd1	těžko
ležeti	ležet	k5eAaImF	ležet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jako	jako	k9	jako
jí	on	k3xPp3gFnSc3	on
na	na	k7c6	na
jmenu	jmen	k1gInSc6	jmen
</s>
</p>
<p>
<s>
spočívá	spočívat	k5eAaImIp3nS	spočívat
prokletí	prokletí	k1gNnSc1	prokletí
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Holoubek	Holoubek	k1gMnSc1	Holoubek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leč	leč	k8xS	leč
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
jeho	jeho	k3xOp3gFnSc2	jeho
té	ten	k3xDgFnSc2	ten
samé	samý	k3xTgFnSc2	samý
chvíle	chvíle	k1gFnSc2	chvíle
</s>
</p>
<p>
<s>
vznášejí	vznášet	k5eAaImIp3nP	vznášet
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
holubice	holubice	k1gFnPc1	holubice
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
v	v	k7c6	v
radostném	radostný	k2eAgInSc6d1	radostný
plesu	ples	k1gInSc6	ples
vznášejí	vznášet	k5eAaImIp3nP	vznášet
se	se	k3xPyFc4	se
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
až	až	k9	až
i	i	k9	i
se	se	k3xPyFc4	se
vznesly	vznést	k5eAaPmAgFnP	vznést
k	k	k7c3	k
andělskému	andělský	k2eAgInSc3d1	andělský
kůru	kůr	k1gInSc3	kůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Záhořovo	Záhořův	k2eAgNnSc1d1	Záhořovo
lože	lože	k1gNnSc1	lože
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Něco	něco	k3yInSc1	něco
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
.	.	kIx.	.
–	–	k?	–
Pode	pod	k7c7	pod
dveřmi	dveře	k1gFnPc7	dveře
</s>
</p>
<p>
<s>
mok	mok	k1gInSc1	mok
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
–	–	k?	–
krvavý	krvavý	k2eAgInSc4d1	krvavý
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
a	a	k8xC	a
když	když	k8xS	když
stará	starat	k5eAaImIp3nS	starat
otevřela	otevřít	k5eAaPmAgFnS	otevřít
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kdo	kdo	k3yQnSc1	kdo
leknutí	leknutí	k1gNnSc4	leknutí
vypraví	vypravit	k5eAaPmIp3nS	vypravit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
věci	věc	k1gFnPc1	věc
tu	tu	k6eAd1	tu
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
leží	ležet	k5eAaImIp3nS	ležet
–	–	k?	–
</s>
</p>
<p>
<s>
mráz	mráz	k1gInSc1	mráz
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
hrůzou	hrůza	k1gFnSc7	hrůza
běží	běžet	k5eAaImIp3nS	běžet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dětská	dětský	k2eAgFnSc1d1	dětská
hlava	hlava	k1gFnSc1	hlava
bez	bez	k7c2	bez
tělíčka	tělíčko	k1gNnSc2	tělíčko
</s>
</p>
<p>
<s>
a	a	k8xC	a
tělíčko	tělíčko	k1gNnSc1	tělíčko
bez	bez	k7c2	bez
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
na	na	k7c4	na
píšťalku	píšťalka	k1gFnSc4	píšťalka
bude	být	k5eAaImBp3nS	být
pěti	pět	k4xCc3	pět
–	–	k?	–
</s>
</p>
<p>
<s>
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
rozprávěti	rozprávět	k5eAaImF	rozprávět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kletbu	kletba	k1gFnSc4	kletba
zůstavuji	zůstavovat	k5eAaImIp1nS	zůstavovat
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
matko	matka	k1gFnSc5	matka
má	mít	k5eAaImIp3nS	mít
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
kletbu	kletba	k1gFnSc4	kletba
zůstavuji	zůstavovat	k5eAaImIp1nS	zůstavovat
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bys	by	kYmCp2nS	by
nenašla	najít	k5eNaPmAgFnS	najít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hrobě	hrob	k1gInSc6	hrob
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
žes	žes	kA	žes
mi	já	k3xPp1nSc3	já
zvůli	zvůle	k1gFnSc3	zvůle
dávala	dávat	k5eAaImAgFnS	dávat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Dceřina	dceřin	k2eAgFnSc1d1	dceřina
kletba	kletba	k1gFnSc1	kletba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slyšte	slyšet	k5eAaImRp2nP	slyšet
a	a	k8xC	a
pilně	pilně	k6eAd1	pilně
važte	vázat	k5eAaImRp2nP	vázat
moje	můj	k3xOp1gMnPc4	můj
slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
nic	nic	k6eAd1	nic
se	se	k3xPyFc4	se
nenoste	nosit	k5eNaImRp2nP	nosit
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
leč	leč	k8xC	leč
nad	nad	k7c7	nad
tím	ten	k3xDgInSc7	ten
břichem	břich	k1gInSc7	břich
vřelé	vřelý	k2eAgNnSc1d1	vřelé
srdce	srdce	k1gNnSc1	srdce
znova	znova	k6eAd1	znova
</s>
</p>
<p>
<s>
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
hlava	hlava	k1gFnSc1	hlava
naroste	narůst	k5eAaPmIp3nS	narůst
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Věštkyně	věštkyně	k1gFnSc2	věštkyně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgNnSc1d1	hudební
zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc4	některý
básně	báseň	k1gFnPc4	báseň
také	také	k6eAd1	také
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
skladatel	skladatel	k1gMnSc1	skladatel
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
jako	jako	k8xS	jako
kantátu	kantáta	k1gFnSc4	kantáta
(	(	kIx(	(
<g/>
Svatební	svatební	k2eAgFnPc4d1	svatební
košile	košile	k1gFnPc4	košile
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
jako	jako	k8xC	jako
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgFnSc1d1	svatební
košile	košile	k1gFnSc1	košile
(	(	kIx(	(
<g/>
opus	opus	k1gInSc1	opus
69	[number]	k4	69
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodník	vodník	k1gMnSc1	vodník
(	(	kIx(	(
<g/>
opus	opus	k1gInSc1	opus
107	[number]	k4	107
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polednice	polednice	k1gFnSc1	polednice
(	(	kIx(	(
<g/>
opus	opus	k1gInSc1	opus
108	[number]	k4	108
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kolovrat	kolovrat	k1gInSc1	kolovrat
(	(	kIx(	(
<g/>
opus	opus	k1gInSc1	opus
109	[number]	k4	109
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Holoubek	Holoubek	k1gMnSc1	Holoubek
(	(	kIx(	(
<g/>
opus	opus	k1gInSc1	opus
110	[number]	k4	110
<g/>
)	)	kIx)	)
<g/>
Některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
ztvárněny	ztvárnit	k5eAaPmNgFnP	ztvárnit
také	také	k9	také
jako	jako	k8xS	jako
melodramata	melodramata	k?	melodramata
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
od	od	k7c2	od
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Fibicha	Fibich	k1gMnSc2	Fibich
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
zpracování	zpracování	k1gNnSc4	zpracování
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
3	[number]	k4	3
Vltava	Vltava	k1gFnSc1	Vltava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
režií	režie	k1gFnSc7	režie
Markéty	Markéta	k1gFnSc2	Markéta
Jahodové	Jahodová	k1gFnSc2	Jahodová
nahrány	nahrát	k5eAaPmNgFnP	nahrát
4	[number]	k4	4
balady	balada	k1gFnPc4	balada
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Balady	balada	k1gFnPc1	balada
Kytice	kytice	k1gFnSc2	kytice
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kolovrat	kolovrat	k1gInSc4	kolovrat
namluvil	namluvit	k5eAaPmAgMnS	namluvit
Pavel	Pavel	k1gMnSc1	Pavel
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
a	a	k8xC	a
Poklad	poklad	k1gInSc1	poklad
Barbora	Barbora	k1gFnSc1	Barbora
Munzarová	Munzarová	k1gFnSc1	Munzarová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
Kytice	kytice	k1gFnSc2	kytice
natočil	natočit	k5eAaBmAgInS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
F.	F.	kA	F.
A.	A.	kA	A.
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
ceny	cena	k1gFnSc2	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
2001	[number]	k4	2001
za	za	k7c4	za
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komiksové	komiksový	k2eAgNnSc4d1	komiksové
zpracování	zpracování	k1gNnSc4	zpracování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
Transmedialist	Transmedialist	k1gInSc4	Transmedialist
zpracovalo	zpracovat	k5eAaPmAgNnS	zpracovat
a	a	k8xC	a
vydalo	vydat	k5eAaPmAgNnS	vydat
publikaci	publikace	k1gFnSc4	publikace
Komiksová	komiksový	k2eAgFnSc1d1	komiksová
Kytice	kytice	k1gFnSc1	kytice
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
původní	původní	k2eAgFnSc1d1	původní
koncepce	koncepce	k1gFnSc1	koncepce
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
baladu	balada	k1gFnSc4	balada
Svatojanská	svatojanský	k2eAgFnSc1d1	Svatojanská
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
domova	domov	k1gInSc2	domov
a	a	k8xC	a
Myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Novina	novina	k1gFnSc1	novina
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
Kytice	kytice	k1gFnSc1	kytice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s.	s.	k?	s.
287	[number]	k4	287
<g/>
–	–	k?	–
<g/>
293	[number]	k4	293
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠUJAN	ŠUJAN	kA	ŠUJAN
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Erbenova	Erbenův	k2eAgFnSc1d1	Erbenova
Kytice	kytice	k1gFnSc1	kytice
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
aesthetické	aesthetický	k2eAgFnSc6d1	aesthetická
s	s	k7c7	s
rozborem	rozbor	k1gInSc7	rozbor
Pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Šujan	Šujan	k1gMnSc1	Šujan
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
34	[number]	k4	34
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ERBEN	Erben	k1gMnSc1	Erben
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgFnPc2d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Tisk	tisk	k1gInSc1	tisk
a	a	k8xC	a
náklad	náklad	k1gInSc1	náklad
Jarosl	Jarosl	k1gInSc1	Jarosl
<g/>
.	.	kIx.	.
</s>
<s>
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
149	[number]	k4	149
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Kytice	kytice	k1gFnSc2	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgFnPc2d1	národní
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Rozbor	rozbor	k1gInSc1	rozbor
Kytice	kytice	k1gFnSc1	kytice
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
Josef	Josef	k1gMnSc1	Josef
Vojvodík	Vojvodík	k1gMnSc1	Vojvodík
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
zpracování	zpracování	k1gNnSc1	zpracování
básní	báseň	k1gFnPc2	báseň
Kytice	kytice	k1gFnSc2	kytice
<g/>
,	,	kIx,	,
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kolovrat	kolovrat	k1gInSc4	kolovrat
a	a	k8xC	a
Poklad	poklad	k1gInSc4	poklad
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
katalogu	katalog	k1gInSc2	katalog
Vědecké	vědecký	k2eAgFnSc2d1	vědecká
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
digitalizováno	digitalizovat	k5eAaImNgNnS	digitalizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
eBooks	eBooks	k6eAd1	eBooks
on	on	k3xPp3gInSc1	on
Demand	Demand	k1gInSc1	Demand
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
Kytice	kytice	k1gFnSc2	kytice
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Krameria	Kramerius	k1gMnSc2	Kramerius
</s>
</p>
<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.Pohadkozem.cz	www.Pohadkozem.cza	k1gFnPc2	www.Pohadkozem.cza
</s>
</p>
