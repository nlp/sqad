<s>
Boston	Boston	k1gInSc1	Boston
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
významný	významný	k2eAgInSc4d1	významný
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
