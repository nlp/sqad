<s>
Boston	Boston	k1gInSc1	Boston
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
významný	významný	k2eAgInSc4d1	významný
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
téměř	téměř	k6eAd1	téměř
7,5	[number]	k4	7,5
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pátou	pátý	k4xOgFnSc4	pátý
největší	veliký	k2eAgFnSc4d3	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
měst	město	k1gNnPc2	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
středisko	středisko	k1gNnSc1	středisko
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
atraktivních	atraktivní	k2eAgNnPc2d1	atraktivní
sportovních	sportovní	k2eAgNnPc2d1	sportovní
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1630	[number]	k4	1630
puritánskými	puritánský	k2eAgMnPc7d1	puritánský
kolonisty	kolonista	k1gMnPc7	kolonista
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Bostonské	bostonský	k2eAgNnSc4d1	Bostonské
pití	pití	k1gNnSc4	pití
čaje	čaj	k1gInSc2	čaj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
a	a	k8xC	a
první	první	k4xOgFnPc4	první
bitvy	bitva	k1gFnPc4	bitva
americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
byl	být	k5eAaImAgInS	být
přístavní	přístavní	k2eAgMnSc1d1	přístavní
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc1d1	obchodní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
středisek	středisko	k1gNnPc2	středisko
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
textilního	textilní	k2eAgInSc2d1	textilní
<g/>
,	,	kIx,	,
sklářského	sklářský	k2eAgInSc2d1	sklářský
a	a	k8xC	a
kožedělného	kožedělný	k2eAgInSc2d1	kožedělný
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
malá	malý	k2eAgFnSc1d1	malá
plocha	plocha	k1gFnSc1	plocha
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stálo	stát	k5eAaImAgNnS	stát
historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
ztrojnásobila	ztrojnásobit	k5eAaPmAgFnS	ztrojnásobit
zavážením	zavážení	k1gNnSc7	zavážení
močálů	močál	k1gInPc2	močál
a	a	k8xC	a
mělčin	mělčina	k1gFnPc2	mělčina
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
i	i	k9	i
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
splynulo	splynout	k5eAaPmAgNnS	splynout
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Massachusettského	massachusettský	k2eAgInSc2d1	massachusettský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
MIT	MIT	kA	MIT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
obcemi	obec	k1gFnPc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
anglické	anglický	k2eAgNnSc1d1	anglické
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
města	město	k1gNnSc2	město
doplnili	doplnit	k5eAaPmAgMnP	doplnit
nejprve	nejprve	k6eAd1	nejprve
Irové	Ir	k1gMnPc1	Ir
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
velkým	velký	k2eAgInSc7d1	velký
uzlem	uzel	k1gInSc7	uzel
husté	hustý	k2eAgFnSc2d1	hustá
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zde	zde	k6eAd1	zde
první	první	k4xOgInPc4	první
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
zde	zde	k6eAd1	zde
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
provoz	provoz	k1gInSc4	provoz
první	první	k4xOgNnSc1	první
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ale	ale	k9	ale
průmysl	průmysl	k1gInSc1	průmysl
začal	začít	k5eAaPmAgInS	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
za	za	k7c7	za
levnější	levný	k2eAgFnSc7d2	levnější
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
službami	služba	k1gFnPc7	služba
a	a	k8xC	a
státní	státní	k2eAgFnSc7d1	státní
správou	správa	k1gFnSc7	správa
tvoří	tvořit	k5eAaImIp3nS	tvořit
dnes	dnes	k6eAd1	dnes
přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
ekonomiky	ekonomika	k1gFnPc1	ekonomika
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
složku	složka	k1gFnSc4	složka
tvoří	tvořit	k5eAaImIp3nP	tvořit
také	také	k9	také
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
slavná	slavný	k2eAgNnPc4d1	slavné
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc4	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnPc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
během	během	k7c2	během
tradičního	tradiční	k2eAgInSc2d1	tradiční
Bostonského	bostonský	k2eAgInSc2d1	bostonský
maratonu	maraton	k1gInSc2	maraton
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
dalších	další	k1gNnPc2	další
byly	být	k5eAaImAgFnP	být
zraněny	zranit	k5eAaPmNgFnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
ploché	plochý	k2eAgFnSc6d1	plochá
krajině	krajina	k1gFnSc6	krajina
při	při	k7c6	při
členitém	členitý	k2eAgNnSc6d1	členité
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Boston	Boston	k1gInSc1	Boston
od	od	k7c2	od
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc7d1	východní
hranicí	hranice	k1gFnSc7	hranice
je	být	k5eAaImIp3nS	být
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
řada	řada	k1gFnSc1	řada
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
rozlohy	rozloha	k1gFnSc2	rozloha
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
(	(	kIx(	(
<g/>
106	[number]	k4	106
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
Logan	Logany	k1gInPc2	Logany
airport	airport	k1gInSc4	airport
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
pahorky	pahorek	k1gInPc1	pahorek
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
odbagrovány	odbagrován	k2eAgFnPc1d1	odbagrován
na	na	k7c4	na
zaplnění	zaplnění	k1gNnSc4	zaplnění
močálů	močál	k1gInPc2	močál
a	a	k8xC	a
mělčin	mělčina	k1gFnPc2	mělčina
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Beacon	Beacon	k1gMnSc1	Beacon
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
State	status	k1gInSc5	status
house	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
jako	jako	k8xS	jako
částečná	částečný	k2eAgFnSc1d1	částečná
historická	historický	k2eAgFnSc1d1	historická
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
leží	ležet	k5eAaImIp3nS	ležet
poměrně	poměrně	k6eAd1	poměrně
jižně	jižně	k6eAd1	jižně
(	(	kIx(	(
<g/>
asi	asi	k9	asi
jako	jako	k9	jako
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Boston	Boston	k1gInSc1	Boston
dosti	dosti	k6eAd1	dosti
drsné	drsný	k2eAgNnSc4d1	drsné
podnebí	podnebí	k1gNnSc4	podnebí
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
s	s	k7c7	s
horkými	horký	k2eAgInPc7d1	horký
letními	letní	k2eAgInPc7d1	letní
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgNnSc1d1	mořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
zastavěno	zastavět	k5eAaPmNgNnS	zastavět
přístavními	přístavní	k2eAgInPc7d1	přístavní
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
pěkné	pěkný	k2eAgFnPc1d1	pěkná
pláže	pláž	k1gFnPc1	pláž
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ve	v	k7c4	v
Wonderland	Wonderland	k1gInSc4	Wonderland
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Cape	capat	k5eAaImIp3nS	capat
Cod	coda	k1gFnPc2	coda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
budovy	budova	k1gFnPc1	budova
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
Prudential	Prudential	k1gMnSc1	Prudential
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
Federal	Federal	k1gMnSc1	Federal
Reserve	Reserev	k1gFnSc2	Reserev
Bank	bank	k1gInSc1	bank
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
One	One	k1gFnSc1	One
Boston	Boston	k1gInSc1	Boston
Place	plac	k1gInSc6	plac
a	a	k8xC	a
One	One	k1gFnSc6	One
International	International	k1gFnSc6	International
Place	plac	k1gInSc6	plac
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
617	[number]	k4	617
594	[number]	k4	594
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
53,9	[number]	k4	53,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
24,4	[number]	k4	24,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
8,9	[number]	k4	8,9
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
8,4	[number]	k4	8,4
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
17,5	[number]	k4	17,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
lidé	člověk	k1gMnPc1	člověk
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
15,8	[number]	k4	15,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hispánci	hispánek	k1gMnPc1	hispánek
čili	čili	k8xC	čili
latinos	latinos	k1gInSc1	latinos
15,4	[number]	k4	15,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
8,3	[number]	k4	8,3
%	%	kIx~	%
a	a	k8xC	a
6,4	[number]	k4	6,4
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bostonu	Boston	k1gInSc2	Boston
činil	činit	k5eAaImAgMnS	činit
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
asi	asi	k9	asi
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prudce	prudko	k6eAd1	prudko
rostl	růst	k5eAaImAgInS	růst
až	až	k9	až
na	na	k7c4	na
670	[number]	k4	670
tisíc	tisíc	k4xCgInSc4	tisíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
maxima	maximum	k1gNnSc2	maximum
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
klesal	klesat	k5eAaImAgInS	klesat
až	až	k9	až
na	na	k7c4	na
560	[number]	k4	560
tisíc	tisíc	k4xCgInSc4	tisíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
opět	opět	k6eAd1	opět
mírně	mírně	k6eAd1	mírně
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
přes	přes	k7c4	přes
23	[number]	k4	23
tisíc	tisíc	k4xCgInSc4	tisíc
USD	USD	kA	USD
<g/>
,	,	kIx,	,
na	na	k7c4	na
domácnost	domácnost	k1gFnSc4	domácnost
skoro	skoro	k6eAd1	skoro
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
25	[number]	k4	25
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
pod	pod	k7c7	pod
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
nad	nad	k7c7	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
žilo	žít	k5eAaImAgNnS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Logan	Logana	k1gFnPc2	Logana
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
27	[number]	k4	27
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
cestujících	cestující	k1gMnPc2	cestující
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
terminály	terminál	k1gInPc7	terminál
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInPc1d1	ležící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
s	s	k7c7	s
dobrým	dobrý	k2eAgNnSc7d1	dobré
spojením	spojení	k1gNnSc7	spojení
městskou	městský	k2eAgFnSc7d1	městská
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Páteří	páteř	k1gFnSc7	páteř
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
a	a	k8xC	a
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mimo	mimo	k7c4	mimo
centrum	centrum	k1gNnSc4	centrum
jede	jet	k5eAaImIp3nS	jet
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
oranžová	oranžový	k2eAgFnSc1d1	oranžová
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
linka	linka	k1gFnSc1	linka
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
desítkami	desítka	k1gFnPc7	desítka
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
tvoří	tvořit	k5eAaImIp3nS	tvořit
systém	systém	k1gInSc4	systém
MBTA	MBTA	kA	MBTA
s	s	k7c7	s
jednotnými	jednotný	k2eAgFnPc7d1	jednotná
jízdenkami	jízdenka	k1gFnPc7	jízdenka
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
přestupu	přestup	k1gInSc2	přestup
<g/>
,	,	kIx,	,
týdenní	týdenní	k2eAgFnSc1d1	týdenní
jízdenka	jízdenka	k1gFnSc1	jízdenka
s	s	k7c7	s
neomezeným	omezený	k2eNgInSc7d1	neomezený
počtem	počet	k1gInSc7	počet
jízd	jízda	k1gFnPc2	jízda
stojí	stát	k5eAaImIp3nS	stát
15	[number]	k4	15
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přístavu	přístav	k1gInSc2	přístav
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc1	několik
lodních	lodní	k2eAgFnPc2d1	lodní
linek	linka	k1gFnPc2	linka
MBTA	MBTA	kA	MBTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
navazuje	navazovat	k5eAaImIp3nS	navazovat
asi	asi	k9	asi
tucet	tucet	k1gInSc1	tucet
příměstských	příměstský	k2eAgFnPc2d1	příměstská
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
slabým	slabý	k2eAgInSc7d1	slabý
provozem	provoz	k1gInSc7	provoz
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hustší	hustý	k2eAgFnSc1d2	hustší
autobusová	autobusový	k2eAgFnSc1d1	autobusová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
jezdí	jezdit	k5eAaImIp3nP	jezdit
také	také	k9	také
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
i	i	k9	i
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
dopravu	doprava	k1gFnSc4	doprava
např.	např.	kA	např.
do	do	k7c2	do
Belmontu	Belmont	k1gInSc2	Belmont
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
linky	linka	k1gFnPc1	linka
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
hlavních	hlavní	k2eAgNnPc2d1	hlavní
nádraží	nádraží	k1gNnPc2	nádraží
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Station	station	k1gInSc1	station
a	a	k8xC	a
North	North	k1gInSc1	North
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
leží	ležet	k5eAaImIp3nS	ležet
vlastní	vlastní	k2eAgNnSc1d1	vlastní
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
jedou	jet	k5eAaImIp3nP	jet
také	také	k9	také
dálkové	dálkový	k2eAgInPc4d1	dálkový
železniční	železniční	k2eAgInPc4d1	železniční
i	i	k8xC	i
autobusové	autobusový	k2eAgInPc4d1	autobusový
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
nádraží	nádraží	k1gNnPc4	nádraží
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
South	South	k1gInSc1	South
station	station	k1gInSc1	station
má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc4d1	přímé
autobusové	autobusový	k2eAgNnSc4d1	autobusové
spojení	spojení	k1gNnSc4	spojení
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgNnSc7d1	tradiční
střediskem	středisko	k1gNnSc7	středisko
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
zde	zde	k6eAd1	zde
studuje	studovat	k5eAaImIp3nS	studovat
přes	přes	k7c4	přes
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
samém	samý	k3xTgMnSc6	samý
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
Boston	Boston	k1gInSc1	Boston
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Northeastern	Northeastern	k1gNnSc1	Northeastern
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
College	College	k1gFnSc1	College
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
hudebních	hudební	k2eAgFnPc2d1	hudební
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
zasahuji	zasahovat	k5eAaImIp1nS	zasahovat
prestižní	prestižní	k2eAgFnSc1d1	prestižní
univerzity	univerzita	k1gFnPc1	univerzita
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Cambridge	Cambridge	k1gFnSc1	Cambridge
(	(	kIx(	(
<g/>
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
a	a	k8xC	a
Massachusettský	massachusettský	k2eAgInSc1d1	massachusettský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bostonu	Boston	k1gInSc2	Boston
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
(	(	kIx(	(
<g/>
Tufts	Tuftsa	k1gFnPc2	Tuftsa
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Brandeis	Brandeis	k1gFnSc2	Brandeis
University	universita	k1gFnSc2	universita
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
slavných	slavný	k2eAgNnPc2d1	slavné
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
Boston	Boston	k1gInSc1	Boston
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
jsou	být	k5eAaImIp3nP	být
galerie	galerie	k1gFnPc1	galerie
a	a	k8xC	a
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc4	Arts
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
návštěvní	návštěvní	k2eAgFnPc1d1	návštěvní
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
vstupné	vstupné	k1gNnSc1	vstupné
platí	platit	k5eAaImIp3nS	platit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgNnSc1d1	Evropské
umění	umění	k1gNnSc1	umění
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
impresionisté	impresionista	k1gMnPc1	impresionista
<g/>
;	;	kIx,	;
mimoevropské	mimoevropský	k2eAgNnSc4d1	mimoevropské
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
)	)	kIx)	)
465	[number]	k4	465
Huntington	Huntington	k1gInSc1	Huntington
Ave	ave	k1gNnSc2	ave
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Mus	Musa	k1gFnPc2	Musa
<g/>
.	.	kIx.	.
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
Green	Green	k1gInSc1	Green
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
17	[number]	k4	17
USD	USD	kA	USD
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
planetárium	planetárium	k1gNnSc1	planetárium
<g/>
,	,	kIx,	,
obří	obří	k2eAgFnPc1d1	obří
projekce	projekce	k1gFnPc1	projekce
<g/>
,	,	kIx,	,
simulátory	simulátor	k1gInPc1	simulátor
<g/>
,	,	kIx,	,
laserová	laserový	k2eAgFnSc1d1	laserová
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
výstavy	výstava	k1gFnSc2	výstava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
Science	Scienec	k1gInSc2	Scienec
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Science	Science	k1gFnSc2	Science
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
denně	denně	k6eAd1	denně
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
USD	USD	kA	USD
+	+	kIx~	+
výstavy	výstava	k1gFnSc2	výstava
Boston	Boston	k1gInSc1	Boston
Childern	Childern	k1gInSc1	Childern
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc1	umění
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
South	Southa	k1gFnPc2	Southa
Station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
Red	Red	k1gFnSc1	Red
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
10	[number]	k4	10
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
10	[number]	k4	10
USD	USD	kA	USD
Museum	museum	k1gNnSc1	museum
of	of	k?	of
African	African	k1gMnSc1	African
American	American	k1gMnSc1	American
History	Histor	k1gInPc4	Histor
<g/>
,	,	kIx,	,
46	[number]	k4	46
Joy	Joy	k1gFnPc2	Joy
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Beacon	Beacon	k1gMnSc1	Beacon
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Park	park	k1gInSc1	park
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
10-16	[number]	k4	10-16
kromě	kromě	k7c2	kromě
Ne	Ne	kA	Ne
<g/>
,	,	kIx,	,
5	[number]	k4	5
USD	USD	kA	USD
Gibson	Gibson	k1gInSc1	Gibson
House	house	k1gNnSc1	house
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
rodinný	rodinný	k2eAgInSc4d1	rodinný
dům	dům	k1gInSc4	dům
a	a	k8xC	a
sbírky	sbírka	k1gFnPc4	sbírka
od	od	k7c2	od
1800	[number]	k4	1800
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
137	[number]	k4	137
Beacon	Beacon	k1gInSc1	Beacon
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Arlington	Arlington	k1gInSc1	Arlington
Str	str	kA	str
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Green	Green	k1gInSc1	Green
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
St	St	kA	St
až	až	k9	až
Ne	ne	k9	ne
ve	v	k7c4	v
13	[number]	k4	13
<g/>
,	,	kIx,	,
14	[number]	k4	14
a	a	k8xC	a
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
7	[number]	k4	7
USD	USD	kA	USD
Otis	Otis	k1gInSc1	Otis
House	house	k1gNnSc1	house
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
rodinný	rodinný	k2eAgInSc4d1	rodinný
dům	dům	k1gInSc4	dům
a	a	k8xC	a
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
141	[number]	k4	141
Cambridge	Cambridge	k1gFnSc2	Cambridge
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
Lynde	Lynd	k1gMnSc5	Lynd
Str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Charles	Charles	k1gMnSc1	Charles
<g/>
/	/	kIx~	/
<g/>
MGH	MGH	kA	MGH
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
St	St	kA	St
-	-	kIx~	-
Ne	ne	k9	ne
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
8	[number]	k4	8
USD	USD	kA	USD
Isabella	Isabella	k1gMnSc1	Isabella
Stewart	Stewart	k1gMnSc1	Stewart
Gardner	Gardner	k1gInSc4	Gardner
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
sbírka	sbírka	k1gFnSc1	sbírka
umění	umění	k1gNnSc2	umění
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
280	[number]	k4	280
Fenway	Fenway	k1gInPc1	Fenway
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
Mus	Musa	k1gFnPc2	Musa
<g/>
.	.	kIx.	.
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
Green	Green	k1gInSc1	Green
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
11-17	[number]	k4	11-17
mimo	mimo	k7c4	mimo
Po	Po	kA	Po
<g/>
,	,	kIx,	,
12	[number]	k4	12
USD	USD	kA	USD
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc1	všechen
Isabelly	Isabella	k1gFnPc1	Isabella
mají	mít	k5eAaImIp3nP	mít
vstup	vstup	k1gInSc4	vstup
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
J.	J.	kA	J.
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Presidential	Presidential	k1gMnSc1	Presidential
Library	Librara	k1gFnSc2	Librara
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc1	sbírka
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
bus	bus	k1gInSc1	bus
zdarma	zdarma	k6eAd1	zdarma
od	od	k7c2	od
metra	metro	k1gNnSc2	metro
JFK	JFK	kA	JFK
<g/>
/	/	kIx~	/
<g/>
UMass	UMass	k1gInSc1	UMass
(	(	kIx(	(
<g/>
Red	Red	k1gFnSc1	Red
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
12	[number]	k4	12
USD	USD	kA	USD
</s>
<s>
Boston	Boston	k1gInSc1	Boston
je	být	k5eAaImIp3nS	být
především	především	k9	především
baseballové	baseballový	k2eAgNnSc1d1	baseballové
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Boston	Boston	k1gInSc1	Boston
Red	Red	k1gMnSc2	Red
Sox	Sox	k1gMnSc2	Sox
patří	patřit	k5eAaImIp3nS	patřit
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
k	k	k7c3	k
favoritům	favorit	k1gMnPc3	favorit
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
hrají	hrát	k5eAaImIp3nP	hrát
ve	v	k7c4	v
Fenway	Fenwaa	k1gFnPc4	Fenwaa
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
Kenmore	Kenmor	k1gInSc5	Kenmor
Square	square	k1gInSc4	square
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gNnSc1	stadion
Fenway	Fenwaa	k1gFnSc2	Fenwaa
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
vůbec	vůbec	k9	vůbec
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
sportovním	sportovní	k2eAgInPc3d1	sportovní
stadionům	stadion	k1gInPc3	stadion
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
sportem	sport	k1gInSc7	sport
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
klub	klub	k1gInSc1	klub
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
a	a	k8xC	a
nejstarší	starý	k2eAgInPc4d3	nejstarší
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Šestkrát	šestkrát	k6eAd1	šestkrát
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Stanley	Stanle	k1gMnPc4	Stanle
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k1gMnPc1	domácí
utkání	utkání	k1gNnSc2	utkání
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
TD	TD	kA	TD
Banknorth	Banknortha	k1gFnPc2	Banknortha
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Bruins	Bruins	k6eAd1	Bruins
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
Original	Original	k1gMnSc7	Original
Six	Six	k1gMnSc7	Six
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
Celtics	Celticsa	k1gFnPc2	Celticsa
hrají	hrát	k5eAaImIp3nP	hrát
NBA	NBA	kA	NBA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
a	a	k8xC	a
soutěž	soutěž	k1gFnSc1	soutěž
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
trošku	trošku	k6eAd1	trošku
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tým	tým	k1gInSc1	tým
nesídlí	sídlet	k5eNaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
Foxboro	Foxbora	k1gFnSc5	Foxbora
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
tým	tým	k1gInSc1	tým
New	New	k1gFnSc2	New
England	Englanda	k1gFnPc2	Englanda
Patriot	patriot	k1gMnSc1	patriot
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
krát	krát	k6eAd1	krát
NFL	NFL	kA	NFL
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
také	také	k9	také
Bostonský	bostonský	k2eAgInSc1d1	bostonský
maraton	maraton	k1gInSc1	maraton
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
podzimní	podzimní	k2eAgFnSc1d1	podzimní
regata	regata	k1gFnSc1	regata
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
Bostonu	Boston	k1gInSc2	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Chang-čou	Chang-čou	k1gNnSc2	Chang-čou
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Kjóto	Kjóto	k1gNnSc1	Kjóto
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Padova	Padova	k1gFnSc1	Padova
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Sekondi-Takoradi	Sekondi-Takorad	k1gMnPc1	Sekondi-Takorad
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Štrasburg	Štrasburg	k1gInSc1	Štrasburg
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
Tchaj-pej	Tchajej	k1gMnPc2	Tchaj-pej
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
