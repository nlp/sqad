<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Zlatého	zlatý	k2eAgInSc2d1	zlatý
potoka	potok	k1gInSc2	potok
(	(	kIx(	(
<g/>
Dědiny	dědina	k1gFnSc2	dědina
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
287	[number]	k4	287
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
na	na	k7c6	na
katastrální	katastrální	k2eAgFnSc6d1	katastrální
ploše	plocha	k1gFnSc6	plocha
3	[number]	k4	3
466	[number]	k4	466
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
