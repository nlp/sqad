<s>
Dobruška	Dobruška	k1gFnSc1	Dobruška
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Gutenfeld	Gutenfeld	k1gInSc1	Gutenfeld
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Zlatého	zlatý	k2eAgInSc2d1	zlatý
potoka	potok	k1gInSc2	potok
(	(	kIx(	(
<g/>
Dědiny	dědina	k1gFnSc2	dědina
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
287	[number]	k4	287
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
na	na	k7c6	na
katastrální	katastrální	k2eAgFnSc6d1	katastrální
ploše	plocha	k1gFnSc6	plocha
3	[number]	k4	3
466	[number]	k4	466
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
spravuje	spravovat	k5eAaImIp3nS	spravovat
části	část	k1gFnSc3	část
obce	obec	k1gFnSc2	obec
Běstviny	Běstvina	k1gFnSc2	Běstvina
<g/>
,	,	kIx,	,
Domašín	Domašína	k1gFnPc2	Domašína
<g/>
,	,	kIx,	,
Chábory	Chábora	k1gFnSc2	Chábora
<g/>
,	,	kIx,	,
Spáleniště	spáleniště	k1gNnSc2	spáleniště
<g/>
,	,	kIx,	,
Pulice	Pulic	k1gMnPc4	Pulic
<g/>
,	,	kIx,	,
Mělčany	Mělčan	k1gMnPc4	Mělčan
a	a	k8xC	a
Křovice	Křovice	k1gFnPc4	Křovice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Dobrušce	Dobruška	k1gFnSc6	Dobruška
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
postihl	postihnout	k5eAaPmAgInS	postihnout
Dobrušku	Dobruška	k1gFnSc4	Dobruška
největší	veliký	k2eAgInSc1d3	veliký
požár	požár	k1gInSc1	požár
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1926	[number]	k4	1926
navštívil	navštívit	k5eAaPmAgMnS	navštívit
město	město	k1gNnSc4	město
oficiálně	oficiálně	k6eAd1	oficiálně
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
T.G.	T.G.	k1gMnSc1	T.G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
zapsal	zapsat	k5eAaPmAgMnS	zapsat
se	se	k3xPyFc4	se
do	do	k7c2	do
pamětní	pamětní	k2eAgFnSc2d1	pamětní
knihy	kniha	k1gFnSc2	kniha
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
Dobrušky	Dobruška	k1gFnSc2	Dobruška
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Dobrušce	Dobruška	k1gFnSc6	Dobruška
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
románská	románský	k2eAgFnSc1d1	románská
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
hmotnou	hmotný	k2eAgFnSc7d1	hmotná
památkou	památka	k1gFnSc7	památka
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
raná	raný	k2eAgNnPc1d1	rané
díla	dílo	k1gNnPc1	dílo
malíře	malíř	k1gMnSc2	malíř
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
<g/>
.	.	kIx.	.
</s>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
zbarokizovaný	zbarokizovaný	k2eAgInSc1d1	zbarokizovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1709	[number]	k4	1709
až	až	k9	až
1724	[number]	k4	1724
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
za	za	k7c7	za
městem	město	k1gNnSc7	město
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1736	[number]	k4	1736
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Městské	městský	k2eAgNnSc4d1	Městské
lapidárium	lapidárium	k1gNnSc4	lapidárium
Rodný	rodný	k2eAgInSc1d1	rodný
domek	domek	k1gInSc1	domek
Františka	František	k1gMnSc2	František
Vladislava	Vladislav	k1gMnSc2	Vladislav
Heka	Hekus	k1gMnSc2	Hekus
Židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
Jediná	jediný	k2eAgFnSc1d1	jediná
zpřístupněná	zpřístupněný	k2eAgFnSc1d1	zpřístupněná
mikve	mikev	k1gFnPc1	mikev
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
rabínském	rabínský	k2eAgInSc6d1	rabínský
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
městského	městský	k2eAgInSc2d1	městský
<g />
.	.	kIx.	.
</s>
<s>
muzea	muzeum	k1gNnPc1	muzeum
Hrobka	hrobka	k1gFnSc1	hrobka
rodiny	rodina	k1gFnSc2	rodina
Laichterovy	Laichterův	k2eAgFnSc2d1	Laichterova
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Městský	městský	k2eAgInSc1d1	městský
dům	dům	k1gInSc1	dům
-	-	kIx~	-
hotel	hotel	k1gInSc1	hotel
u	u	k7c2	u
Rýdlů	Rýdl	k1gMnPc2	Rýdl
Vila	vít	k5eAaImAgFnS	vít
U	u	k7c2	u
Rýdlů	Rýdl	k1gMnPc2	Rýdl
Městský	městský	k2eAgInSc4d1	městský
dům	dům	k1gInSc4	dům
-	-	kIx~	-
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
Josefa	Josef	k1gMnSc2	Josef
Laichtera	Laichter	k1gMnSc2	Laichter
Kovárna	kovárna	k1gFnSc1	kovárna
Synagoga	synagoga	k1gFnSc1	synagoga
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
ve	v	k7c6	v
Studánce	studánka	k1gFnSc6	studánka
Dobruška	Dobruška	k1gFnSc1	Dobruška
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
lokální	lokální	k2eAgFnSc2d1	lokální
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
Opočno	Opočno	k1gNnSc4	Opočno
pod	pod	k7c7	pod
Orlickými	orlický	k2eAgFnPc7d1	Orlická
horami	hora	k1gFnPc7	hora
-	-	kIx~	-
Dobruška	Dobruška	k1gFnSc1	Dobruška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
využívána	využívat	k5eAaImNgFnS	využívat
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
spojnicí	spojnice	k1gFnSc7	spojnice
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
(	(	kIx(	(
<g/>
Náchod	Náchod	k1gInSc1	Náchod
-	-	kIx~	-
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobruška	Dobruška	k1gFnSc1	Dobruška
Běstviny	Běstvina	k1gFnSc2	Běstvina
Domašín	Domašína	k1gFnPc2	Domašína
Chábory	Chábora	k1gFnSc2	Chábora
Křovice	Křovice	k1gFnSc2	Křovice
Mělčany	Mělčan	k1gMnPc4	Mělčan
Pulice	Pulice	k1gFnSc2	Pulice
Spáleniště	spáleniště	k1gNnSc2	spáleniště
Augustin	Augustin	k1gMnSc1	Augustin
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
(	(	kIx(	(
<g/>
1736	[number]	k4	1736
<g/>
-	-	kIx~	-
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
gambista	gambista	k1gMnSc1	gambista
a	a	k8xC	a
houslista	houslista	k1gMnSc1	houslista
František	František	k1gMnSc1	František
Vladislav	Vladislav	k1gMnSc1	Vladislav
Hek	hek	k1gInSc1	hek
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
proslavený	proslavený	k2eAgMnSc1d1	proslavený
díky	díky	k7c3	díky
románu	román	k1gInSc3	román
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc4	věk
a	a	k8xC	a
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
televiznímu	televizní	k2eAgInSc3d1	televizní
seriálu	seriál	k1gInSc3	seriál
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc4	věk
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
režiséra	režisér	k1gMnSc2	režisér
Františka	František	k1gMnSc2	František
Filipa	Filip	k1gMnSc2	Filip
Alois	Alois	k1gMnSc1	Alois
Beer	Beer	k1gMnSc1	Beer
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písmák	písmák	k1gMnSc1	písmák
a	a	k8xC	a
naivní	naivní	k2eAgMnSc1d1	naivní
malíř	malíř	k1gMnSc1	malíř
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
mládí	mládí	k1gNnSc4	mládí
František	František	k1gMnSc1	František
Adolf	Adolf	k1gMnSc1	Adolf
Šubert	Šubert	k1gMnSc1	Šubert
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Karel	Karel	k1gMnSc1	Karel
Lier	Lier	k1gMnSc1	Lier
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakladatel	nakladatel	k1gMnSc1	nakladatel
a	a	k8xC	a
knihkupec	knihkupec	k1gMnSc1	knihkupec
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
spisů	spis	k1gInPc2	spis
T.G.	T.G.	k1gMnSc1	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Radim	Radim	k1gMnSc1	Radim
<g />
.	.	kIx.	.
</s>
<s>
Drejsl	Drejsl	k1gInSc1	Drejsl
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Jan	Jan	k1gMnSc1	Jan
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
sklářské	sklářský	k2eAgFnSc2d1	sklářská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Kamenickém	kamenický	k2eAgInSc6d1	kamenický
Šenově	Šenov	k1gInSc6	Šenov
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
<g />
.	.	kIx.	.
</s>
<s>
školy	škola	k1gFnPc1	škola
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
V	v	k7c6	v
Dobrušce	Dobruška	k1gFnSc6	Dobruška
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
pořádány	pořádán	k2eAgFnPc1d1	pořádána
různé	různý	k2eAgFnPc1d1	různá
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Dobrušské	Dobrušský	k2eAgInPc4d1	Dobrušský
letní	letní	k2eAgInPc4d1	letní
muzicírování	muzicírování	k?	muzicírování
(	(	kIx(	(
<g/>
živé	živý	k2eAgInPc1d1	živý
koncerty	koncert	k1gInPc1	koncert
každou	každý	k3xTgFnSc4	každý
červencovou	červencový	k2eAgFnSc4d1	červencová
a	a	k8xC	a
srpnovou	srpnový	k2eAgFnSc4d1	srpnová
neděli	neděle	k1gFnSc4	neděle
od	od	k7c2	od
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nově	nově	k6eAd1	nově
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
F.	F.	kA	F.
L.	L.	kA	L.
Věka	Věka	k1gMnSc1	Věka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Ábrahámhégy	Ábrahámhég	k1gInPc1	Ábrahámhég
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
oblast	oblast	k1gFnSc4	oblast
Vale	val	k1gInSc6	val
Royal	Royal	k1gInSc1	Royal
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Miejska	Miejsek	k1gMnSc2	Miejsek
Górka	Górek	k1gMnSc2	Górek
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Radków	Radków	k1gFnSc2	Radków
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Pilawa	Pilaw	k1gInSc2	Pilaw
Górna	Górna	k1gFnSc1	Górna
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Velký	velký	k2eAgMnSc1d1	velký
Meder	Meder	k1gMnSc1	Meder
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Hnúšťa	Hnúšť	k1gInSc2	Hnúšť
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Galerie	galerie	k1gFnSc2	galerie
Dobruška	Dobruška	k1gFnSc1	Dobruška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dobruška	Dobruška	k1gFnSc1	Dobruška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
virtuální	virtuální	k2eAgFnSc1d1	virtuální
show	show	k1gFnSc1	show
</s>
