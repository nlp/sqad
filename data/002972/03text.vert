<s>
Gejša	gejša	k1gFnSc1	gejša
(	(	kIx(	(
<g/>
芸	芸	k?	芸
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
označujícící	označujícící	k2eAgFnSc4d1	označujícící
profesionální	profesionální	k2eAgFnSc4d1	profesionální
společnici	společnice	k1gFnSc4	společnice
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Gejša	gejša	k1gFnSc1	gejša
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
gej	gej	k?	gej
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
sha	sha	k?	sha
<g/>
"	"	kIx"	"
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
bytí	bytí	k1gNnSc2	bytí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Gejša	gejša	k1gFnSc1	gejša
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
přítomnost	přítomnost	k1gFnSc1	přítomnost
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
doprovod	doprovod	k1gInSc1	doprovod
umělkyně	umělkyně	k1gFnSc2	umělkyně
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedické	encyklopedický	k2eAgFnPc1d1	encyklopedická
definice	definice	k1gFnPc1	definice
často	často	k6eAd1	často
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
krásnou	krásný	k2eAgFnSc4d1	krásná
a	a	k8xC	a
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
hlavním	hlavní	k2eAgNnSc7d1	hlavní
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
bavit	bavit	k5eAaImF	bavit
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Gejša	gejša	k1gFnSc1	gejša
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
muže	muž	k1gMnSc4	muž
bavit	bavit	k5eAaImF	bavit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
tancem	tanec	k1gInSc7	tanec
<g/>
,	,	kIx,	,
zpěvem	zpěv	k1gInSc7	zpěv
či	či	k8xC	či
jiným	jiný	k2eAgNnSc7d1	jiné
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
typické	typický	k2eAgInPc4d1	typický
a	a	k8xC	a
neodmyslitelné	odmyslitelný	k2eNgInPc4d1	neodmyslitelný
symboly	symbol	k1gInPc4	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxem	paradox	k1gInSc7	paradox
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc2	první
gejši	gejša	k1gFnSc2	gejša
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
gejši-ženy	gejši-žen	k2eAgFnPc1d1	gejši-žen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
bílým	bílý	k2eAgInSc7d1	bílý
make-upem	makep	k1gInSc7	make-up
a	a	k8xC	a
tradičním	tradiční	k2eAgInSc7d1	tradiční
japonským	japonský	k2eAgInSc7d1	japonský
šatem	šat	k1gInSc7	šat
kimono	kimono	k1gNnSc4	kimono
okouzlily	okouzlit	k5eAaPmAgFnP	okouzlit
nejednoho	nejeden	k4xCyIgMnSc2	nejeden
Evropana	Evropan	k1gMnSc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
gejša	gejša	k1gFnSc1	gejša
ženského	ženský	k2eAgNnSc2d1	ženské
pohlaví	pohlaví	k1gNnSc2	pohlaví
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
1751	[number]	k4	1751
v	v	k7c6	v
nevěstinci	nevěstinec	k1gInSc6	nevěstinec
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
gejšami	gejša	k1gFnPc7	gejša
výhradně	výhradně	k6eAd1	výhradně
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc4	obličej
a	a	k8xC	a
šíji	šíje	k1gFnSc4	šíje
si	se	k3xPyFc3	se
gejši	gejša	k1gFnPc1	gejša
natíraly	natírat	k5eAaImAgFnP	natírat
bílým	bílý	k2eAgInSc7d1	bílý
make-upem	makep	k1gInSc7	make-up
vždy	vždy	k6eAd1	vždy
však	však	k9	však
nechávaly	nechávat	k5eAaImAgInP	nechávat
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
(	(	kIx(	(
<g/>
u	u	k7c2	u
vlasů	vlas	k1gInPc2	vlas
<g/>
)	)	kIx)	)
nepatrný	patrný	k2eNgInSc4d1	patrný
kus	kus	k1gInSc4	kus
nenabarvené	nabarvený	k2eNgFnSc2d1	nenabarvená
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
daly	dát	k5eAaPmAgFnP	dát
najevo	najevo	k6eAd1	najevo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
jen	jen	k9	jen
"	"	kIx"	"
<g/>
maska	maska	k1gFnSc1	maska
<g/>
"	"	kIx"	"
a	a	k8xC	a
muže	muž	k1gMnSc4	muž
vzrušovalo	vzrušovat	k5eAaImAgNnS	vzrušovat
vidět	vidět	k5eAaImF	vidět
pravou	pravý	k2eAgFnSc4d1	pravá
tvář	tvář	k1gFnSc4	tvář
gejši	gejša	k1gFnSc2	gejša
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
u	u	k7c2	u
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
malovaly	malovat	k5eAaImAgFnP	malovat
dvě	dva	k4xCgFnPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgFnPc1	tři
čáry	čára	k1gFnPc1	čára
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
klínů	klín	k1gInPc2	klín
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
působit	působit	k5eAaImF	působit
dramaticky	dramaticky	k6eAd1	dramaticky
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
eroticky	eroticky	k6eAd1	eroticky
<g/>
.	.	kIx.	.
</s>
<s>
Obočí	obočit	k5eAaPmIp3nP	obočit
a	a	k8xC	a
oční	oční	k2eAgFnPc1d1	oční
linky	linka	k1gFnPc1	linka
si	se	k3xPyFc3	se
zvýrazňovaly	zvýrazňovat	k5eAaImAgFnP	zvýrazňovat
uhlíkem	uhlík	k1gInSc7	uhlík
z	z	k7c2	z
větvičky	větvička	k1gFnSc2	větvička
paulovnie	paulovnie	k1gFnSc2	paulovnie
plstnaté	plstnatý	k2eAgFnSc2d1	plstnatá
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
rtů	ret	k1gInPc2	ret
si	se	k3xPyFc3	se
natíraly	natírat	k5eAaImAgFnP	natírat
pigmentovou	pigmentový	k2eAgFnSc7d1	pigmentová
směsí	směs	k1gFnSc7	směs
<g/>
,	,	kIx,	,
tak	tak	k9	tak
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc4	jejich
ústa	ústa	k1gNnPc4	ústa
vypadala	vypadat	k5eAaPmAgFnS	vypadat
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
skutečná	skutečný	k2eAgFnSc1d1	skutečná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
ideál	ideál	k1gInSc4	ideál
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kadeřníkovi	kadeřník	k1gMnSc3	kadeřník
se	se	k3xPyFc4	se
chodilo	chodit	k5eAaImAgNnS	chodit
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
gejšin	gejšin	k2eAgInSc1d1	gejšin
účes	účes	k1gInSc1	účes
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
pracný	pracný	k2eAgInSc1d1	pracný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasů	vlas	k1gInPc2	vlas
vtíral	vtírat	k5eAaImAgInS	vtírat
horký	horký	k2eAgInSc1d1	horký
vosk	vosk	k1gInSc1	vosk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
účes	účes	k1gInSc1	účes
držel	držet	k5eAaImAgInS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Účesů	účes	k1gInPc2	účes
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
a	a	k8xC	a
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
gejši	gejša	k1gFnSc2	gejša
a	a	k8xC	a
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgInP	měnit
účesy	účes	k1gInPc1	účes
podle	podle	k7c2	podle
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Momovare	Momovar	k1gInSc5	Momovar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nosily	nosit	k5eAaImAgFnP	nosit
nejmladší	mladý	k2eAgFnPc4d3	nejmladší
gejši-učednice	gejšičednice	k1gFnPc4	gejši-učednice
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
"	"	kIx"	"
<g/>
Warešinobu	Warešinoba	k1gFnSc4	Warešinoba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ofuku	Ofuka	k1gFnSc4	Ofuka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
verze	verze	k1gFnSc1	verze
Warešinobu	Warešinoba	k1gFnSc4	Warešinoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kacujama	Kacujama	k1gFnSc1	Kacujama
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Sakko	Sakko	k1gNnSc1	Sakko
<g/>
.	.	kIx.	.
</s>
<s>
Gejši-učednice	Gejšičednice	k1gFnSc1	Gejši-učednice
"	"	kIx"	"
<g/>
majko	majka	k1gFnSc5	majka
<g/>
"	"	kIx"	"
nosí	nosit	k5eAaImIp3nP	nosit
kimono	kimono	k1gNnSc4	kimono
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
kapsovitými	kapsovitý	k2eAgInPc7d1	kapsovitý
rukávy	rukáv	k1gInPc7	rukáv
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
výrazným	výrazný	k2eAgNnSc7d1	výrazné
obi	obi	k?	obi
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pás	pás	k1gInSc4	pás
kimona	kimono	k1gNnSc2	kimono
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
hrudní	hrudní	k2eAgFnSc2d1	hrudní
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
pupíku	pupík	k1gInSc2	pupík
<g/>
.	.	kIx.	.
</s>
<s>
Obi	Obi	k?	Obi
vlastně	vlastně	k9	vlastně
kimono	kimono	k1gNnSc1	kimono
nedrží	držet	k5eNaImIp3nS	držet
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
provázky	provázek	k1gInPc1	provázek
a	a	k8xC	a
sponky	sponka	k1gFnPc1	sponka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
pod	pod	k7c7	pod
obi	obi	k?	obi
skryjí	skrýt	k5eAaPmIp3nP	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
uzel	uzel	k1gInSc1	uzel
obi	obi	k?	obi
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
mnoho	mnoho	k4c1	mnoho
vycpávek	vycpávka	k1gFnPc2	vycpávka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vypadal	vypadat	k5eAaPmAgInS	vypadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Gejša-učednice	Gejšačednice	k1gFnSc1	Gejša-učednice
"	"	kIx"	"
<g/>
majko	majka	k1gFnSc5	majka
<g/>
"	"	kIx"	"
nosí	nosit	k5eAaImIp3nS	nosit
velké	velký	k2eAgNnSc4d1	velké
široké	široký	k2eAgNnSc4d1	široké
obi	obi	k?	obi
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakoby	jakoby	k8xS	jakoby
baťůžek	baťůžek	k1gInSc4	baťůžek
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
visící	visící	k2eAgInPc1d1	visící
kusy	kus	k1gInPc1	kus
obi	obi	k?	obi
až	až	k6eAd1	až
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oblečení	oblečení	k1gNnSc1	oblečení
dospělé	dospělý	k2eAgFnSc2d1	dospělá
gejši	gejša	k1gFnSc2	gejša
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jednodušeji	jednoduše	k6eAd2	jednoduše
a	a	k8xC	a
více	hodně	k6eAd2	hodně
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
ženskou	ženský	k2eAgFnSc4d1	ženská
postavu	postava	k1gFnSc4	postava
a	a	k8xC	a
rukávy	rukáv	k1gInPc4	rukáv
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
tak	tak	k6eAd1	tak
kapsovité	kapsovitý	k2eAgFnPc1d1	kapsovitá
jako	jako	k8xC	jako
u	u	k7c2	u
gejši-učednice	gejšičednice	k1gFnSc2	gejši-učednice
<g/>
.	.	kIx.	.
</s>
<s>
Obi	Obi	k?	Obi
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc4d1	malý
uzlík	uzlík	k1gInSc4	uzlík
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
krabičku	krabička	k1gFnSc4	krabička
<g/>
.	.	kIx.	.
</s>
<s>
Výchova	výchova	k1gFnSc1	výchova
budoucí	budoucí	k2eAgFnSc2d1	budoucí
gejši	gejša	k1gFnSc2	gejša
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
tradičně	tradičně	k6eAd1	tradičně
už	už	k6eAd1	už
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
většinou	většinou	k6eAd1	většinou
dcery	dcera	k1gFnSc2	dcera
bývalých	bývalý	k2eAgFnPc2d1	bývalá
gejš	gejša	k1gFnPc2	gejša
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
děvčátka	děvčátko	k1gNnPc1	děvčátko
začínají	začínat	k5eAaImIp3nP	začínat
učit	učit	k5eAaImF	učit
tančit	tančit	k5eAaImF	tančit
<g/>
,	,	kIx,	,
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
příjemně	příjemně	k6eAd1	příjemně
hovořit	hovořit	k5eAaImF	hovořit
a	a	k8xC	a
studují	studovat	k5eAaImIp3nP	studovat
i	i	k9	i
umění	umění	k1gNnSc4	umění
čajového	čajový	k2eAgInSc2d1	čajový
obřadu	obřad	k1gInSc2	obřad
<g/>
,	,	kIx,	,
kaligrafie	kaligrafie	k1gFnSc2	kaligrafie
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
šamisen	šamisen	k1gInSc4	šamisen
(	(	kIx(	(
<g/>
japonská	japonský	k2eAgFnSc1d1	japonská
kytara	kytara	k1gFnSc1	kytara
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
strunami	struna	k1gFnPc7	struna
potažena	potažen	k2eAgFnSc1d1	potažena
kůží	kůže	k1gFnSc7	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
bubínky	bubínek	k1gInPc4	bubínek
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
tradičních	tradiční	k2eAgNnPc2d1	tradiční
japonských	japonský	k2eAgNnPc2d1	Japonské
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
14	[number]	k4	14
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
stává	stávat	k5eAaImIp3nS	stávat
gejša-učednice	gejšačednice	k1gFnSc1	gejša-učednice
"	"	kIx"	"
<g/>
majko	majka	k1gFnSc5	majka
<g/>
"	"	kIx"	"
a	a	k8xC	a
ujímá	ujímat	k5eAaImIp3nS	ujímat
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
starší	starý	k2eAgFnSc1d2	starší
gejša	gejša	k1gFnSc1	gejša
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
provází	provázet	k5eAaImIp3nS	provázet
na	na	k7c4	na
večírky	večírek	k1gInPc4	večírek
a	a	k8xC	a
která	který	k3yQgNnPc4	který
je	být	k5eAaImIp3nS	být
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
zákazníky	zákazník	k1gMnPc7	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
pravými	pravý	k2eAgFnPc7d1	pravá
gejšami	gejša	k1gFnPc7	gejša
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
gejši	gejša	k1gFnSc2	gejša
nadále	nadále	k6eAd1	nadále
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
slovo	slovo	k1gNnSc1	slovo
gejša	gejša	k1gFnSc1	gejša
synonymem	synonymum	k1gNnSc7	synonymum
japonské	japonský	k2eAgFnSc2d1	japonská
prostitutky	prostitutka	k1gFnSc2	prostitutka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
gejša	gejša	k1gFnSc1	gejša
je	on	k3xPp3gFnPc4	on
společnice	společnice	k1gFnPc4	společnice
pro	pro	k7c4	pro
společenská	společenský	k2eAgNnPc4d1	společenské
setkání	setkání	k1gNnPc4	setkání
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
kulturní	kulturní	k2eAgNnSc4d1	kulturní
zabavení	zabavení	k1gNnSc4	zabavení
hostů	host	k1gMnPc2	host
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
,	,	kIx,	,
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
básněmi	báseň	k1gFnPc7	báseň
<g/>
,	,	kIx,	,
hrami	hra	k1gFnPc7	hra
a	a	k8xC	a
učenou	učený	k2eAgFnSc7d1	učená
filosofickou	filosofický	k2eAgFnSc7d1	filosofická
konverzací	konverzace	k1gFnSc7	konverzace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
gejša	gejša	k1gFnSc1	gejša
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
stávalo	stávat	k5eAaImAgNnS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
vlivný	vlivný	k2eAgMnSc1d1	vlivný
muž	muž	k1gMnSc1	muž
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
oficiální	oficiální	k2eAgInSc4d1	oficiální
milenecký	milenecký	k2eAgInSc4d1	milenecký
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
veřejný	veřejný	k2eAgMnSc1d1	veřejný
a	a	k8xC	a
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
veden	vést	k5eAaImNgMnS	vést
jako	jako	k8xS	jako
její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
danna	danna	k1gFnSc1	danna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
patron	patron	k1gInSc1	patron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platil	platit	k5eAaImAgMnS	platit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
většinou	většinou	k6eAd1	většinou
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
až	až	k9	až
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
většinu	většina	k1gFnSc4	většina
jejích	její	k3xOp3gInPc2	její
značných	značný	k2eAgInPc2d1	značný
výdajů	výdaj	k1gInPc2	výdaj
za	za	k7c4	za
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
klasický	klasický	k2eAgInSc4d1	klasický
hodinový	hodinový	k2eAgInSc4d1	hodinový
honorář	honorář	k1gInSc4	honorář
<g/>
,	,	kIx,	,
za	za	k7c4	za
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
strávil	strávit	k5eAaPmAgMnS	strávit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
měl	mít	k5eAaImAgInS	mít
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
výsady	výsada	k1gFnPc4	výsada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
to	ten	k3xDgNnSc1	ten
značně	značně	k6eAd1	značně
připomínalo	připomínat	k5eAaImAgNnS	připomínat
klasické	klasický	k2eAgNnSc4d1	klasické
milenecké	milenecký	k2eAgNnSc4d1	milenecké
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
gejši	gejša	k1gFnPc1	gejša
vždy	vždy	k6eAd1	vždy
samy	sám	k3xTgFnPc4	sám
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
s	s	k7c7	s
mužem	muž	k1gMnSc7	muž
naváží	navážet	k5eAaImIp3nS	navážet
i	i	k9	i
fyzický	fyzický	k2eAgInSc4d1	fyzický
poměr	poměr	k1gInSc4	poměr
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgFnP	být
kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
výjimky	výjimka	k1gFnSc2	výjimka
smluvně	smluvně	k6eAd1	smluvně
povinné	povinný	k2eAgNnSc1d1	povinné
jim	on	k3xPp3gInPc3	on
věnovat	věnovat	k5eAaPmF	věnovat
třeba	třeba	k6eAd1	třeba
i	i	k9	i
veškerý	veškerý	k3xTgInSc4	veškerý
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
co	co	k9	co
měly	mít	k5eAaImAgFnP	mít
a	a	k8xC	a
starat	starat	k5eAaImF	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
kulturní	kulturní	k2eAgNnSc4d1	kulturní
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
zábavu	zábava	k1gFnSc4	zábava
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
tradiční	tradiční	k2eAgFnSc1d1	tradiční
japonská	japonský	k2eAgFnSc1d1	japonská
prostitutka	prostitutka	k1gFnSc1	prostitutka
nosila	nosit	k5eAaImAgFnS	nosit
mnohdy	mnohdy	k6eAd1	mnohdy
stejně	stejně	k6eAd1	stejně
skvostná	skvostný	k2eAgNnPc4d1	skvostné
kimona	kimono	k1gNnPc4	kimono
i	i	k8xC	i
účesy	účes	k1gInPc4	účes
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
vzory	vzor	k1gInPc4	vzor
na	na	k7c6	na
oděvu	oděv	k1gInSc6	oděv
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
významových	významový	k2eAgFnPc6d1	významová
kombinacích	kombinace	k1gFnPc6	kombinace
postrádaly	postrádat	k5eAaImAgFnP	postrádat
vybraná	vybraná	k1gFnSc1	vybraná
a	a	k8xC	a
učená	učená	k1gFnSc1	učená
poselství	poselství	k1gNnSc2	poselství
a	a	k8xC	a
spíše	spíše	k9	spíše
významem	význam	k1gInSc7	význam
oznamovaly	oznamovat	k5eAaImAgInP	oznamovat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
světskost	světskost	k1gFnSc4	světskost
a	a	k8xC	a
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
nevkusnou	vkusný	k2eNgFnSc4d1	nevkusná
<g/>
)	)	kIx)	)
marnotratnost	marnotratnost	k1gFnSc4	marnotratnost
nositelky	nositelka	k1gFnSc2	nositelka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
cizí	cizí	k2eAgMnPc4d1	cizí
cestovatele	cestovatel	k1gMnPc4	cestovatel
neznalé	znalý	k2eNgMnPc4d1	neznalý
požadavku	požadavek	k1gInSc3	požadavek
skromnosti	skromnost	k1gFnSc2	skromnost
a	a	k8xC	a
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
významů	význam	k1gInPc2	význam
velice	velice	k6eAd1	velice
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
gejšou	gejša	k1gFnSc7	gejša
splést	splést	k5eAaPmF	splést
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obi	obi	k?	obi
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prostitutky	prostitutka	k1gFnPc1	prostitutka
zavazovaly	zavazovat	k5eAaImAgFnP	zavazovat
zepředu	zepředu	k6eAd1	zepředu
a	a	k8xC	a
v	v	k7c6	v
líčení	líčení	k1gNnSc6	líčení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
prostitutek	prostitutka	k1gFnPc2	prostitutka
křiklavé	křiklavý	k2eAgFnSc2d1	křiklavá
až	až	k8xS	až
vulgární	vulgární	k2eAgFnSc2d1	vulgární
(	(	kIx(	(
<g/>
prostitutky	prostitutka	k1gFnPc1	prostitutka
také	také	k9	také
nosily	nosit	k5eAaImAgFnP	nosit
lehce	lehko	k6eAd1	lehko
odlišné	odlišný	k2eAgInPc1d1	odlišný
účesy	účes	k1gInPc1	účes
a	a	k8xC	a
užívaly	užívat	k5eAaImAgInP	užívat
levnější	levný	k2eAgInPc1d2	levnější
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neměly	mít	k5eNaImAgFnP	mít
finanční	finanční	k2eAgFnPc4d1	finanční
možnosti	možnost	k1gFnPc4	možnost
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc4d1	společenská
prestiž	prestiž	k1gFnSc4	prestiž
gejš	gejša	k1gFnPc2	gejša
nutnou	nutný	k2eAgFnSc4d1	nutná
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
výběrových	výběrový	k2eAgFnPc2d1	výběrová
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rozmohlo	rozmoct	k5eAaPmAgNnS	rozmoct
cestování	cestování	k1gNnSc1	cestování
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
převážně	převážně	k6eAd1	převážně
amerických	americký	k2eAgMnPc2d1	americký
okupačních	okupační	k2eAgMnPc2d1	okupační
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mnohdy	mnohdy	k6eAd1	mnohdy
nepoznávali	poznávat	k5eNaImAgMnP	poznávat
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
gejšou	gejša	k1gFnSc7	gejša
a	a	k8xC	a
prostitutkou	prostitutka	k1gFnSc7	prostitutka
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
od	od	k7c2	od
obojích	oboj	k1gFnPc6	oboj
stejné	stejný	k2eAgFnPc1d1	stejná
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Japonsko	Japonsko	k1gNnSc1	Japonsko
vzpamatovávalo	vzpamatovávat	k5eAaImAgNnS	vzpamatovávat
z	z	k7c2	z
porážky	porážka	k1gFnSc2	porážka
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
běžní	běžný	k2eAgMnPc1d1	běžný
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
zoufalství	zoufalství	k1gNnSc2	zoufalství
i	i	k8xC	i
svých	svůj	k3xOyFgFnPc2	svůj
finančních	finanční	k2eAgFnPc2d1	finanční
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
válka	válka	k1gFnSc1	válka
velice	velice	k6eAd1	velice
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mnoho	mnoho	k4c1	mnoho
gejš	gejša	k1gFnPc2	gejša
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgMnPc3	svůj
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
zákazníkům	zákazník	k1gMnPc3	zákazník
dopřejí	dopřát	k5eAaPmIp3nP	dopřát
různá	různý	k2eAgNnPc4d1	různé
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
samozřejmost	samozřejmost	k1gFnSc4	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
také	také	k9	také
nebylo	být	k5eNaImAgNnS	být
povolání	povolání	k1gNnSc1	povolání
gejši	gejša	k1gFnSc2	gejša
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
prostituce	prostituce	k1gFnSc1	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
učení	učení	k1gNnSc2	učení
mladých	mladý	k2eAgFnPc2d1	mladá
gejš	gejša	k1gFnPc2	gejša
nemá	mít	k5eNaImIp3nS	mít
takovou	takový	k3xDgFnSc4	takový
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Kjótu	Kjót	k1gMnSc6	Kjót
<g/>
,	,	kIx,	,
tradičním	tradiční	k2eAgMnSc6d1	tradiční
městu	město	k1gNnSc3	město
gejš	gejša	k1gFnPc2	gejša
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
umělkyň	umělkyně	k1gFnPc2	umělkyně
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
živí	živit	k5eAaImIp3nP	živit
jako	jako	k9	jako
hostesky	hosteska	k1gFnPc1	hosteska
na	na	k7c6	na
oficiálních	oficiální	k2eAgInPc6d1	oficiální
banketech	banket	k1gInPc6	banket
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
předváděním	předvádění	k1gNnSc7	předvádění
tradičních	tradiční	k2eAgNnPc2d1	tradiční
umění	umění	k1gNnPc2	umění
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Soukromé	soukromý	k2eAgInPc1d1	soukromý
večírky	večírek	k1gInPc1	večírek
s	s	k7c7	s
gejšami	gejša	k1gFnPc7	gejša
už	už	k9	už
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
málo	málo	k6eAd1	málo
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Spatřit	spatřit	k5eAaPmF	spatřit
gejšu	gejša	k1gFnSc4	gejša
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
japonského	japonský	k2eAgNnSc2d1	Japonské
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
velkých	velký	k2eAgInPc2d1	velký
festivalů	festival	k1gInPc2	festival
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
převlečenou	převlečený	k2eAgFnSc4d1	převlečená
japonskou	japonský	k2eAgFnSc4d1	japonská
školačku	školačka	k1gFnSc4	školačka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
fotografovat	fotografovat	k5eAaImF	fotografovat
davy	dav	k1gInPc4	dav
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
gejši	gejša	k1gFnPc1	gejša
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
zaměstnané	zaměstnaný	k2eAgFnPc1d1	zaměstnaná
v	v	k7c6	v
čajovnách	čajovna	k1gFnPc6	čajovna
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
měly	mít	k5eAaImAgInP	mít
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
procházet	procházet	k5eAaImF	procházet
jen	jen	k9	jen
tak	tak	k6eAd1	tak
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
50	[number]	k4	50
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
gejšiným	gejšin	k2eAgInSc7d1	gejšin
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
dospělosti	dospělost	k1gFnSc3	dospělost
Mizuage	Mizuag	k1gInSc2	Mizuag
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
zákonu	zákon	k1gInSc3	zákon
o	o	k7c4	o
prostituci	prostituce	k1gFnSc4	prostituce
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
gejša	gejša	k1gFnSc1	gejša
-	-	kIx~	-
učednice	učednice	k1gFnSc1	učednice
dostatečně	dostatečně	k6eAd1	dostatečně
známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
mistrové	mistrová	k1gFnSc2	mistrová
nabídnout	nabídnout	k5eAaPmF	nabídnout
k	k	k7c3	k
prodeji	prodej	k1gFnSc3	prodej
své	svůj	k3xOyFgNnSc4	svůj
panenství	panenství	k1gNnSc4	panenství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgInSc4d1	významný
rituální	rituální	k2eAgInSc4d1	rituální
milník	milník	k1gInSc4	milník
dospělosti	dospělost	k1gFnSc2	dospělost
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
získanou	získaný	k2eAgFnSc7d1	získaná
částkou	částka	k1gFnSc7	částka
měla	mít	k5eAaImAgFnS	mít
splatit	splatit	k5eAaPmF	splatit
dluh	dluh	k1gInSc4	dluh
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
drahá	drahý	k2eAgNnPc4d1	drahé
kimona	kimono	k1gNnPc4	kimono
<g/>
,	,	kIx,	,
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
uměních	umění	k1gNnPc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Majitelka	majitelka	k1gFnSc1	majitelka
čajovny	čajovna	k1gFnSc2	čajovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
gejša-učednice	gejšačednice	k1gFnSc1	gejša-učednice
zaregistrovaná	zaregistrovaný	k2eAgFnSc1d1	zaregistrovaná
(	(	kIx(	(
<g/>
a	a	k8xC	a
požívala	požívat	k5eAaImAgFnS	požívat
tedy	tedy	k9	tedy
patronát	patronát	k1gInSc4	patronát
paní	paní	k1gFnSc2	paní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
pod	pod	k7c7	pod
její	její	k3xOp3gFnSc7	její
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
dostávalo	dostávat	k5eAaImAgNnS	dostávat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
tam	tam	k6eAd1	tam
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgFnSc2	veškerý
výuky	výuka	k1gFnSc2	výuka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
mizuage	mizuage	k1gFnSc1	mizuage
nabízet	nabízet	k5eAaImF	nabízet
nejbohatším	bohatý	k2eAgMnPc3d3	nejbohatší
zákazníkům	zákazník	k1gMnPc3	zákazník
domu	dům	k1gInSc2	dům
nebo	nebo	k8xC	nebo
nejbohatším	bohatý	k2eAgMnPc3d3	nejbohatší
mužům	muž	k1gMnPc3	muž
města	město	k1gNnSc2	město
a	a	k8xC	a
řídila	řídit	k5eAaImAgFnS	řídit
následnou	následný	k2eAgFnSc7d1	následná
<g/>
,	,	kIx,	,
společensky	společensky	k6eAd1	společensky
prestižní	prestižní	k2eAgFnSc4d1	prestižní
<g/>
,	,	kIx,	,
dražbu	dražba	k1gFnSc4	dražba
o	o	k7c4	o
mizuage	mizuage	k1gFnSc4	mizuage
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
dobrým	dobrý	k2eAgNnSc7d1	dobré
společenským	společenský	k2eAgNnSc7d1	společenské
postavením	postavení	k1gNnSc7	postavení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
největší	veliký	k2eAgFnSc4d3	veliký
částku	částka	k1gFnSc4	částka
a	a	k8xC	a
případné	případný	k2eAgFnPc4d1	případná
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
to	ten	k3xDgNnSc4	ten
privilegium	privilegium	k1gNnSc4	privilegium
zbavit	zbavit	k5eAaPmF	zbavit
gejšu-učednici	gejšučednice	k1gFnSc4	gejšu-učednice
panenství	panenství	k1gNnSc2	panenství
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
gejša	gejša	k1gFnSc1	gejša
už	už	k6eAd1	už
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
zákazníkem	zákazník	k1gMnSc7	zákazník
nestýkala	stýkat	k5eNaImAgFnS	stýkat
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
toto	tento	k3xDgNnSc1	tento
privilegium	privilegium	k1gNnSc1	privilegium
s	s	k7c7	s
gejšou	gejša	k1gFnSc7	gejša
spát	spát	k5eAaImF	spát
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
gejši	gejša	k1gFnPc1	gejša
sice	sice	k8xC	sice
byly	být	k5eAaImAgFnP	být
profesionální	profesionální	k2eAgFnPc1d1	profesionální
společnice	společnice	k1gFnPc1	společnice
a	a	k8xC	a
umělkyně	umělkyně	k1gFnPc1	umělkyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgInPc4	svůj
milenecké	milenecký	k2eAgInPc4d1	milenecký
vztahy	vztah	k1gInPc4	vztah
si	se	k3xPyFc3	se
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
samy	sám	k3xTgFnPc1	sám
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
prací	práce	k1gFnSc7	práce
nijak	nijak	k6eAd1	nijak
nesouvisely	souviset	k5eNaImAgInP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
zákazníky	zákazník	k1gMnPc7	zákazník
gejš	gejša	k1gFnPc2	gejša
převážně	převážně	k6eAd1	převážně
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
šéfové	šéf	k1gMnPc1	šéf
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
turisté	turist	k1gMnPc1	turist
(	(	kIx(	(
<g/>
hotely	hotel	k1gInPc1	hotel
a	a	k8xC	a
turistické	turistický	k2eAgFnSc2d1	turistická
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
najmout	najmout	k5eAaPmF	najmout
gejšu	gejša	k1gFnSc4	gejša
osobně	osobně	k6eAd1	osobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gejši	gejša	k1gFnPc1	gejša
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
konverzovat	konverzovat	k5eAaImF	konverzovat
s	s	k7c7	s
hosty	host	k1gMnPc7	host
<g/>
,	,	kIx,	,
tančit	tančit	k5eAaImF	tančit
<g/>
,	,	kIx,	,
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
lichotit	lichotit	k5eAaImF	lichotit
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
skládat	skládat	k5eAaImF	skládat
origami	origa	k1gFnPc7	origa
<g/>
,	,	kIx,	,
přednášet	přednášet	k5eAaImF	přednášet
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Účet	účet	k1gInSc1	účet
přichází	přicházet	k5eAaImIp3nS	přicházet
poštou	pošta	k1gFnSc7	pošta
za	za	k7c4	za
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
a	a	k8xC	a
při	při	k7c6	při
pěti	pět	k4xCc6	pět
hostech	host	k1gMnPc6	host
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
kolem	kolem	k6eAd1	kolem
milionu	milion	k4xCgInSc2	milion
jenů	jen	k1gInPc2	jen
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gejši	gejša	k1gFnPc1	gejša
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
najímány	najímán	k2eAgMnPc4d1	najímán
na	na	k7c4	na
důležitá	důležitý	k2eAgNnPc4d1	důležité
obchodní	obchodní	k2eAgNnPc4d1	obchodní
jednání	jednání	k1gNnPc4	jednání
a	a	k8xC	a
schůze	schůze	k1gFnPc4	schůze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
dodaly	dodat	k5eAaPmAgFnP	dodat
lesku	lesk	k1gInSc2	lesk
a	a	k8xC	a
účastníkům	účastník	k1gMnPc3	účastník
pocit	pocit	k1gInSc1	pocit
luxusu	luxus	k1gInSc6	luxus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samotné	samotný	k2eAgMnPc4d1	samotný
muže	muž	k1gMnPc4	muž
představuje	představovat	k5eAaImIp3nS	představovat
gejša	gejša	k1gFnSc1	gejša
jejich	jejich	k3xOp3gInSc4	jejich
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Gejša	gejša	k1gFnSc1	gejša
je	být	k5eAaImIp3nS	být
mužovou	mužův	k2eAgFnSc7d1	mužova
důvěrnicí	důvěrnice	k1gFnSc7	důvěrnice
i	i	k8xC	i
komplicem	komplic	k1gMnSc7	komplic
<g/>
.	.	kIx.	.
</s>
<s>
Oiran	Oiran	k1gInSc1	Oiran
Kimono	kimono	k1gNnSc4	kimono
GOLDEN	GOLDEN	kA	GOLDEN
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
.	.	kIx.	.
</s>
<s>
Gejša	gejša	k1gFnSc1	gejša
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
644	[number]	k4	644
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gejša	gejša	k1gFnSc1	gejša
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
芸	芸	k?	芸
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
