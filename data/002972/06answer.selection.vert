<s>
Paradoxem	paradox	k1gInSc7	paradox
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc2	první
gejši	gejša	k1gFnSc2	gejša
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
gejši-ženy	gejši-žen	k2eAgFnPc1d1	gejši-žen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
bílým	bílý	k2eAgInSc7d1	bílý
make-upem	makep	k1gInSc7	make-up
a	a	k8xC	a
tradičním	tradiční	k2eAgInSc7d1	tradiční
japonským	japonský	k2eAgInSc7d1	japonský
šatem	šat	k1gInSc7	šat
kimono	kimono	k1gNnSc4	kimono
okouzlily	okouzlit	k5eAaPmAgFnP	okouzlit
nejednoho	nejeden	k4xCyIgMnSc2	nejeden
Evropana	Evropan	k1gMnSc2	Evropan
<g/>
.	.	kIx.	.
</s>
