<p>
<s>
Trithuria	Trithurium	k1gNnSc2	Trithurium
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
rod	rod	k1gInSc1	rod
čeledi	čeleď	k1gFnSc2	čeleď
Hydatellaceae	Hydatellacea	k1gInSc2	Hydatellacea
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
s	s	k7c7	s
lekníny	leknín	k1gInPc7	leknín
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgNnSc1d1	náležející
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
leknínotvaré	leknínotvarý	k2eAgMnPc4d1	leknínotvarý
(	(	kIx(	(
<g/>
Nymphaeales	Nymphaeales	k1gInSc1	Nymphaeales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nenápadné	nápadný	k2eNgFnPc1d1	nenápadná
drobné	drobný	k2eAgFnPc1d1	drobná
byliny	bylina	k1gFnPc1	bylina
s	s	k7c7	s
bezobalnými	bezobalný	k2eAgInPc7d1	bezobalný
květy	květ	k1gInPc7	květ
a	a	k8xC	a
šídlovitými	šídlovitý	k2eAgInPc7d1	šídlovitý
listy	list	k1gInPc7	list
v	v	k7c6	v
růžici	růžice	k1gFnSc6	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
asi	asi	k9	asi
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c4	mezi
jednoděložné	jednoděložný	k2eAgFnPc4d1	jednoděložná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Trithuria	Trithurium	k1gNnSc2	Trithurium
jsou	být	k5eAaImIp3nP	být
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
nebo	nebo	k8xC	nebo
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
drobné	drobný	k2eAgFnPc1d1	drobná
vodní	vodní	k2eAgFnPc1d1	vodní
byliny	bylina	k1gFnPc1	bylina
s	s	k7c7	s
úzkými	úzký	k2eAgInPc7d1	úzký
trávovitými	trávovitý	k2eAgInPc7d1	trávovitý
listy	list	k1gInPc7	list
nahloučenými	nahloučený	k2eAgInPc7d1	nahloučený
do	do	k7c2	do
růžice	růžice	k1gFnSc2	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
vynořené	vynořený	k2eAgInPc1d1	vynořený
<g/>
,	,	kIx,	,
jednožilné	jednožilný	k2eAgInPc1d1	jednožilný
<g/>
.	.	kIx.	.
</s>
<s>
Květy	Květa	k1gFnPc1	Květa
jsou	být	k5eAaImIp3nP	být
bezobalné	bezobalný	k2eAgFnPc1d1	bezobalný
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
nahloučené	nahloučený	k2eAgFnPc1d1	nahloučená
v	v	k7c6	v
hlávkách	hlávka	k1gFnPc6	hlávka
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
1	[number]	k4	1
tyčinkou	tyčinka	k1gFnSc7	tyčinka
a	a	k8xC	a
1	[number]	k4	1
svrchním	svrchní	k2eAgInSc7d1	svrchní
pestíkem	pestík	k1gInSc7	pestík
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
nepukavé	pukavý	k2eNgInPc1d1	nepukavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Trithuria	Trithurium	k1gNnSc2	Trithurium
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Hydatellaceae	Hydatellacea	k1gFnSc2	Hydatellacea
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
omylem	omylem	k6eAd1	omylem
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
jednoděložné	jednoděložný	k2eAgFnPc4d1	jednoděložná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
zástupcům	zástupce	k1gMnPc3	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Centrolepidaceae	Centrolepidacea	k1gFnSc2	Centrolepidacea
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cronquistově	Cronquistův	k2eAgMnSc6d1	Cronquistův
<g/>
,	,	kIx,	,
Dahlgrenově	Dahlgrenův	k2eAgMnSc6d1	Dahlgrenův
i	i	k8xC	i
Tachtadžjanově	Tachtadžjanův	k2eAgInSc6d1	Tachtadžjanův
systému	systém	k1gInSc6	systém
byla	být	k5eAaImAgFnS	být
čeleď	čeleď	k1gFnSc1	čeleď
Hydatellaceae	Hydatellacea	k1gInSc2	Hydatellacea
řazena	řadit	k5eAaImNgFnS	řadit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoděložných	jednoděložná	k1gFnPc2	jednoděložná
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
řádu	řád	k1gInSc2	řád
Hydatellales	Hydatellalesa	k1gFnPc2	Hydatellalesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
APG	APG	kA	APG
byla	být	k5eAaImAgFnS	být
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
lipnicotvaré	lipnicotvarý	k2eAgMnPc4d1	lipnicotvarý
(	(	kIx(	(
<g/>
Poales	Poales	k1gInSc1	Poales
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
systému	systém	k1gInSc6	systém
APG	APG	kA	APG
III	III	kA	III
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
ohledně	ohledně	k7c2	ohledně
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
morfologie	morfologie	k1gFnSc1	morfologie
embrya	embryo	k1gNnSc2	embryo
přeřazena	přeřadit	k5eAaPmNgFnS	přeřadit
mezi	mezi	k7c4	mezi
nižší	nízký	k2eAgFnPc4d2	nižší
dvouděložné	dvouděložná	k1gFnPc4	dvouděložná
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
leknínotvaré	leknínotvarý	k2eAgMnPc4d1	leknínotvarý
(	(	kIx(	(
<g/>
Nymphaeales	Nymphaeales	k1gInSc1	Nymphaeales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
rozlišovány	rozlišovat	k5eAaImNgInP	rozlišovat
2	[number]	k4	2
rody	rod	k1gInPc1	rod
<g/>
:	:	kIx,	:
Trithuria	Trithurium	k1gNnPc1	Trithurium
a	a	k8xC	a
Hydatella	Hydatello	k1gNnPc1	Hydatello
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Hydatella	Hydatello	k1gNnSc2	Hydatello
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
Trithuria	Trithurium	k1gNnSc2	Trithurium
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
sloučeny	sloučen	k2eAgInPc1d1	sloučen
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Trithuria	Trithurium	k1gNnSc2	Trithurium
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
čeledi	čeleď	k1gFnSc2	čeleď
Hydatellaceae	Hydatellacea	k1gFnSc2	Hydatellacea
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
nomina	nomin	k2eAgFnSc1d1	nomina
conservanda	conservanda	k1gFnSc1	conservanda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k9	již
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
taxonomie	taxonomie	k1gFnSc2	taxonomie
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trithuria	Trithurium	k1gNnSc2	Trithurium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
The	The	k1gFnSc1	The
Families	Families	k1gInSc1	Families
of	of	k?	of
Flowering	Flowering	k1gInSc1	Flowering
Plants	Plants	k1gInSc1	Plants
<g/>
:	:	kIx,	:
Hydatellaceae	Hydatellaceae	k1gInSc1	Hydatellaceae
</s>
</p>
<p>
<s>
Angiosperm	Angiosperm	k1gInSc1	Angiosperm
Phylogeny	Phylogen	k1gInPc1	Phylogen
</s>
</p>
