<s>
Trithuria	Trithurium	k1gNnSc2	Trithurium
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
rod	rod	k1gInSc1	rod
čeledi	čeleď	k1gFnSc2	čeleď
Hydatellaceae	Hydatellacea	k1gInSc2	Hydatellacea
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
s	s	k7c7	s
lekníny	leknín	k1gInPc7	leknín
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgNnSc1d1	náležející
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
leknínotvaré	leknínotvarý	k2eAgMnPc4d1	leknínotvarý
(	(	kIx(	(
<g/>
Nymphaeales	Nymphaeales	k1gInSc1	Nymphaeales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
