<s>
Křemen	křemen	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Křemen	křemen	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Křemen	křemen	k1gInSc1
</s>
<s>
Drúza	drúza	k1gFnSc1
čirých	čirý	k2eAgInPc2d1
krystalů	krystal	k1gInPc2
křemene	křemen	k1gInSc2
–	–	k?
křišťál	křišťál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Minerál	minerál	k1gInSc1
</s>
<s>
Chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
SiO	SiO	k?
<g/>
2	#num#	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
rozmanitá	rozmanitý	k2eAgFnSc1d1
</s>
<s>
Vzhled	vzhled	k1gInSc1
krystalu	krystal	k1gInSc2
</s>
<s>
krystalovaný	krystalovaný	k2eAgInSc1d1
</s>
<s>
Soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gInSc1
klencová	klencový	k2eAgFnSc1d1
<g/>
,	,	kIx,
β	β	k1gInSc1
šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Lesk	lesk	k1gInSc1
</s>
<s>
skelný	skelný	k2eAgInSc1d1
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1
</s>
<s>
neštěpný	štěpný	k2eNgInSc1d1
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
1,544	1,544	k4
(	(	kIx(
<g/>
o	o	k7c4
<g/>
)	)	kIx)
/	/	kIx~
1,553	1,553	k4
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dvojlom	dvojlom	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vryp	vryp	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc4d1
<g/>
,	,	kIx,
šedý	šedý	k2eAgInSc4d1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,6	2,6	k4
g	g	kA
⋅	⋅	k?
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
</s>
<s>
kyselina	kyselina	k1gFnSc1
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
lom	lom	k1gInSc1
<g/>
:	:	kIx,
nerovný	rovný	k2eNgInSc4d1
<g/>
,	,	kIx,
tříšťnatý	tříšťnatý	k2eAgInSc4d1
nebo	nebo	k8xC
lasturnatý	lasturnatý	k2eAgInSc4d1
<g/>
,	,	kIx,
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
tvoří	tvořit	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
</s>
<s>
Model	model	k1gInSc1
krystalu	krystal	k1gInSc2
křemene	křemen	k1gInSc2
</s>
<s>
Křemen	křemen	k1gInSc1
je	být	k5eAaImIp3nS
minerál	minerál	k1gInSc4
s	s	k7c7
chemickým	chemický	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
SiO	SiO	kA
<g/>
2	#num#	k4
(	(	kIx(
<g/>
oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hojně	hojně	k6eAd1
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgMnSc1d1
v	v	k7c6
litosféře	litosféra	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Křemen	křemen	k1gInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
patří	patřit	k5eAaImIp3nS
hvězdovec	hvězdovec	k1gInSc1
<g/>
,	,	kIx,
čirý	čirý	k2eAgInSc1d1
křišťál	křišťál	k1gInSc1
<g/>
,	,	kIx,
růžový	růžový	k2eAgInSc1d1
růženín	růženín	k1gInSc1
<g/>
,	,	kIx,
hnědá	hnědý	k2eAgFnSc1d1
či	či	k8xC
kouřová	kouřový	k2eAgFnSc1d1
záhněda	záhněda	k1gFnSc1
<g/>
,	,	kIx,
kryptokrystalický	kryptokrystalický	k2eAgInSc1d1
chalcedon	chalcedon	k1gInSc1
<g/>
,	,	kIx,
pruhovaný	pruhovaný	k2eAgInSc1d1
achát	achát	k1gInSc1
<g/>
,	,	kIx,
onyx	onyx	k1gInSc1
<g/>
,	,	kIx,
fialový	fialový	k2eAgInSc1d1
ametyst	ametyst	k1gInSc1
<g/>
,	,	kIx,
tygří	tygří	k2eAgNnSc1d1
oko	oko	k1gNnSc1
<g/>
,	,	kIx,
železitý	železitý	k2eAgInSc1d1
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
jaspis	jaspis	k1gInSc1
krystalizující	krystalizující	k2eAgInSc1d1
v	v	k7c6
trojklonné	trojklonný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
žlutý	žlutý	k2eAgInSc1d1
citrín	citrín	k?
<g/>
,	,	kIx,
černý	černý	k2eAgInSc1d1
morion	morion	k1gInSc1
či	či	k8xC
zelený	zelený	k2eAgInSc1d1
prasem	prasem	k1gInSc1
nebo	nebo	k8xC
pazourek	pazourek	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křemen	křemen	k1gInSc1
je	být	k5eAaImIp3nS
běžnou	běžný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
žuly	žula	k1gFnSc2
<g/>
,	,	kIx,
pískovce	pískovec	k1gInSc2
a	a	k8xC
mnoha	mnoho	k4c2
dalších	další	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Označení	označení	k1gNnSc4
„	„	k?
<g/>
křemen	křemen	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
také	také	k9
„	„	k?
<g/>
skřemen	skřemen	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
z	z	k7c2
„	„	k?
<g/>
ker	kra	k1gFnPc2
<g/>
“	“	k?
,	,	kIx,
z	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
vychází	vycházet	k5eAaImIp3nS
i	i	k9
křesat	křesat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglické	anglický	k2eAgFnPc4d1
označení	označení	k1gNnSc3
„	„	k?
<g/>
quartz	quartz	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
německého	německý	k2eAgNnSc2d1
„	„	k?
<g/>
quarz	quarz	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
či	či	k8xC
„	„	k?
<g/>
twarc	twarc	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ze	z	k7c2
slovanského	slovanský	k2eAgNnSc2d1
„	„	k?
<g/>
twardy	twardy	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
znamenající	znamenající	k2eAgInSc1d1
„	„	k?
<g/>
tvrdý	tvrdý	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Křemen	křemen	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k9
poslední	poslední	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Bowenova	Bowenův	k2eAgNnSc2d1
reakčního	reakční	k2eAgNnSc2d1
schématu	schéma	k1gNnSc2
z	z	k7c2
magmatu	magma	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Struktura	struktura	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
propojenými	propojený	k2eAgInPc7d1
tetraedry	tetraedr	k1gInPc7
SiO	SiO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
základních	základní	k2eAgInPc2d1
stavebních	stavební	k2eAgInPc2d1
prvků	prvek	k1gInPc2
křemičitanů	křemičitan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
pegmatitech	pegmatit	k1gInPc6
<g/>
,	,	kIx,
žulách	žula	k1gFnPc6
<g/>
,	,	kIx,
ryolitech	ryolit	k1gInPc6
či	či	k8xC
jako	jako	k9
výplň	výplň	k1gFnSc4
žilných	žilný	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
a	a	k8xC
různých	různý	k2eAgFnPc2d1
dutin	dutina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystaluje	krystalovat	k5eAaImIp3nS
v	v	k7c6
klencové	klencový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
a	a	k8xC
na	na	k7c6
Mohsově	Mohsův	k2eAgFnSc6d1
stupnici	stupnice	k1gFnSc6
tvrdosti	tvrdost	k1gFnSc2
má	mít	k5eAaImIp3nS
tvrdost	tvrdost	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
průhledný	průhledný	k2eAgInSc1d1
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typický	typický	k2eAgInSc1d1
tvar	tvar	k1gInSc1
krystalu	krystal	k1gInSc2
je	být	k5eAaImIp3nS
šestiboký	šestiboký	k2eAgInSc1d1
hranol	hranol	k1gInSc1
s	s	k7c7
dvěma	dva	k4xCgInPc7
klenci	klenec	k1gInPc7
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yQgMnSc2,k3yIgMnSc2,k3yRgMnSc2
bývají	bývat	k5eAaImIp3nP
plochy	plocha	k1gFnPc1
hranolu	hranol	k1gInSc2
vodorovně	vodorovně	k6eAd1
rýhovány	rýhován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
však	však	k9
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
dvojčatění	dvojčatění	k1gNnSc3
nebo	nebo	k8xC
růstu	růst	k1gInSc3
polykrystalů	polykrystal	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
setkáváme	setkávat	k5eAaImIp1nP
se	se	k3xPyFc4
také	také	k9
s	s	k7c7
monokrystaly	monokrystal	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpouští	rozpouštět	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
kyselině	kyselina	k1gFnSc6
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Kryptokrystalickou	Kryptokrystalický	k2eAgFnSc7d1
formou	forma	k1gFnSc7
křemene	křemen	k1gInSc2
je	být	k5eAaImIp3nS
chalcedon	chalcedon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vzniká	vznikat	k5eAaImIp3nS
buď	buď	k8xC
přímým	přímý	k2eAgNnSc7d1
srážením	srážení	k1gNnSc7
z	z	k7c2
roztoků	roztok	k1gInPc2
či	či	k8xC
rekrystalizací	rekrystalizace	k1gFnPc2
amorfních	amorfní	k2eAgInPc2d1
opálů	opál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
odrůdy	odrůda	k1gFnPc4
chalcedonu	chalcedon	k1gInSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
jaspis	jaspis	k1gInSc1
<g/>
,	,	kIx,
chryzopras	chryzopras	k1gInSc1
<g/>
,	,	kIx,
karneol	karneol	k1gInSc1
či	či	k8xC
onyx	onyx	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střídající	střídající	k2eAgFnSc1d1
se	s	k7c7
proužky	proužek	k1gInPc7
křemene	křemen	k1gInSc2
a	a	k8xC
chalcedonu	chalcedon	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
achát	achát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Křemen	křemen	k1gInSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
odrůdách	odrůda	k1gFnPc6
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
i	i	k9
drahokamy	drahokam	k1gInPc1
jako	jako	k8xC,k8xS
například	například	k6eAd1
<g/>
:	:	kIx,
křišťál	křišťál	k1gInSc1
(	(	kIx(
<g/>
čirý	čirý	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ametyst	ametyst	k1gInSc1
(	(	kIx(
<g/>
fialový	fialový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
záhněda	záhněda	k1gFnSc1
(	(	kIx(
<g/>
hnědá	hnědý	k2eAgFnSc1d1
či	či	k8xC
kouřová	kouřový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
citrín	citrín	k?
(	(	kIx(
<g/>
žlutý	žlutý	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
růženín	růženín	k1gInSc1
(	(	kIx(
<g/>
růžový	růžový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
morion	morion	k1gInSc1
(	(	kIx(
<g/>
černý	černý	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
prasem	prasem	k1gInSc1
(	(	kIx(
<g/>
zelený	zelený	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozpoznáváme	rozpoznávat	k5eAaImIp1nP
vyšší	vysoký	k2eAgInSc4d2
a	a	k8xC
nižší	nízký	k2eAgInSc4d2
křemen	křemen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nižší	nízký	k2eAgInSc1d2
křemen	křemen	k1gInSc1
(	(	kIx(
<g/>
také	také	k6eAd1
alfa	alfa	k1gNnSc1
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgFnSc1d1
do	do	k7c2
teploty	teplota	k1gFnSc2
573	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
po	po	k7c6
překročení	překročení	k1gNnSc6
mezní	mezní	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
modifikaci	modifikace	k1gFnSc3
do	do	k7c2
hexagonální	hexagonální	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
vyššího	vysoký	k2eAgInSc2d2
křemene	křemen	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
beta	beta	k1gNnSc1
křemen	křemen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
současně	současně	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
tlaku	tlak	k1gInSc2
<g/>
,	,	kIx,
přeměňuje	přeměňovat	k5eAaImIp3nS
se	se	k3xPyFc4
křemen	křemen	k1gInSc1
ve	v	k7c4
stabilní	stabilní	k2eAgInSc4d1
coesit	coesit	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
ve	v	k7c6
stišovit	stišovit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Křemen	křemen	k1gInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
odolný	odolný	k2eAgMnSc1d1
proti	proti	k7c3
zvětrávání	zvětrávání	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
z	z	k7c2
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
hromadí	hromadit	k5eAaImIp3nP
v	v	k7c6
náplavech	náplav	k1gInPc6
a	a	k8xC
sedimentech	sediment	k1gInPc6
ve	v	k7c6
formě	forma	k1gFnSc6
zrnek	zrnko	k1gNnPc2
<g/>
,	,	kIx,
valounků	valounek	k1gInPc2
<g/>
,	,	kIx,
valounů	valoun	k1gInPc2
(	(	kIx(
<g/>
tvoří	tvořit	k5eAaImIp3nP
písky	písek	k1gInPc1
<g/>
,	,	kIx,
štěrky	štěrk	k1gInPc1
atd	atd	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
ε	ε	k1gFnPc2
krystalu	krystal	k1gInSc2
křemene	křemen	k1gInSc2
je	být	k5eAaImIp3nS
4,3	4,3	k4
až	až	k9
4,7	4,7	k4
<g/>
.	.	kIx.
</s>
<s>
Získávání	získávání	k1gNnSc1
</s>
<s>
Křišťál	křišťál	k1gInSc1
–	–	k?
krystalický	krystalický	k2eAgInSc1d1
oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
</s>
<s>
Křemen	křemen	k1gInSc1
se	se	k3xPyFc4
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
těží	těžet	k5eAaImIp3nS
jako	jako	k9
součást	součást	k1gFnSc4
písků	písek	k1gInPc2
a	a	k8xC
štěrků	štěrk	k1gInPc2
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
na	na	k7c4
speciální	speciální	k2eAgInPc4d1
slévárenské	slévárenský	k2eAgInPc4d1
anebo	anebo	k8xC
sklářské	sklářský	k2eAgInPc4d1
písky	písek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
těžit	těžit	k5eAaImF
kvarcity	kvarcit	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
horniny	hornina	k1gFnPc4
složené	složený	k2eAgFnPc1d1
převážně	převážně	k6eAd1
z	z	k7c2
křemene	křemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
čistý	čistý	k2eAgInSc4d1
křemen	křemen	k1gInSc4
bez	bez	k7c2
příměsí	příměs	k1gFnPc2
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
z	z	k7c2
některých	některý	k3yIgFnPc2
křemenných	křemenný	k2eAgFnPc2d1
žil	žíla	k1gFnPc2
a	a	k8xC
křemenných	křemenný	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
pegmatitů	pegmatit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sběratelské	sběratelský	k2eAgInPc4d1
a	a	k8xC
šperkařské	šperkařský	k2eAgInPc4d1
účely	účel	k1gInPc4
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
drahokamové	drahokamový	k2eAgFnPc4d1
odrůdy	odrůda	k1gFnPc4
křemene	křemen	k1gInSc2
a	a	k8xC
sbírkové	sbírkový	k2eAgFnSc2d1
ukázky	ukázka	k1gFnSc2
křemene	křemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
šperkařství	šperkařství	k1gNnSc4
se	se	k3xPyFc4
získávají	získávat	k5eAaImIp3nP
zejména	zejména	k9
mikrokrystalické	mikrokrystalický	k2eAgInPc1d1
křemeny	křemen	k1gInPc1
(	(	kIx(
<g/>
jaspis	jaspis	k1gInSc1
<g/>
,	,	kIx,
achát	achát	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
vysoce	vysoce	k6eAd1
kvalitní	kvalitní	k2eAgFnPc1d1
čiré	čirý	k2eAgFnPc1d1
a	a	k8xC
nepopraskané	popraskaný	k2eNgFnPc1d1
záhnědy	záhněda	k1gFnPc1
<g/>
,	,	kIx,
křišťály	křišťál	k1gInPc1
<g/>
,	,	kIx,
citríny	citríny	k?
a	a	k8xC
ametysty	ametyst	k1gInPc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
brusů	brus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
piezoelektrické	piezoelektrický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
je	být	k5eAaImIp3nS
uměle	uměle	k6eAd1
vyráběný	vyráběný	k2eAgInSc1d1
křemen	křemen	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
krystal	krystal	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
hojně	hojně	k6eAd1
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
jako	jako	k8xC,k8xS
oscilátor	oscilátor	k1gInSc1
v	v	k7c6
radiotechnice	radiotechnika	k1gFnSc6
a	a	k8xC
dalších	další	k2eAgNnPc6d1
elektronických	elektronický	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
hodinách	hodina	k1gFnPc6
a	a	k8xC
dalších	další	k2eAgInPc6d1
přístrojích	přístroj	k1gInPc6
(	(	kIx(
<g/>
PC	PC	kA
atd.	atd.	kA
<g/>
)	)	kIx)
Jeho	jeho	k3xOp3gFnSc7
předností	přednost	k1gFnSc7
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
malá	malý	k2eAgFnSc1d1
závislost	závislost	k1gFnSc1
piezoelektrického	piezoelektrický	k2eAgInSc2d1
koeficientu	koeficient	k1gInSc2
na	na	k7c6
teplotě	teplota	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křemenné	křemenný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
křemene	křemen	k1gInSc2
amorfní	amorfní	k2eAgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
laboratorní	laboratorní	k2eAgNnSc4d1
a	a	k8xC
další	další	k2eAgNnSc4d1
využití	využití	k1gNnSc4
ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křemenné	křemenný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
je	být	k5eAaImIp3nS
také	také	k9
propustné	propustné	k1gNnSc1
pro	pro	k7c4
UV	UV	kA
záření	záření	k1gNnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
pro	pro	k7c4
měření	měření	k1gNnSc4
v	v	k7c6
UV	UV	kA
oblasti	oblast	k1gFnPc1
používají	používat	k5eAaImIp3nP
právě	právě	k9
křemenné	křemenný	k2eAgFnPc1d1
kyvety	kyveta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
odrůdy	odrůda	k1gFnPc4
se	se	k3xPyFc4
cení	cenit	k5eAaImIp3nP
jako	jako	k9
polodrahokamy	polodrahokam	k1gInPc1
a	a	k8xC
ozdobné	ozdobný	k2eAgInPc1d1
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
používány	používat	k5eAaImNgFnP
ve	v	k7c4
šperkařství	šperkařství	k1gNnSc4
a	a	k8xC
jako	jako	k9
dekorace	dekorace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Naleziště	naleziště	k1gNnSc1
</s>
<s>
běžný	běžný	k2eAgInSc1d1
minerál	minerál	k1gInSc1
<g/>
,	,	kIx,
všeobecně	všeobecně	k6eAd1
rozšířený	rozšířený	k2eAgInSc1d1
</s>
<s>
Nový	nový	k2eAgMnSc1d1
Kýz	Kýz	k1gMnSc1
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
(	(	kIx(
<g/>
sklářská	sklářský	k2eAgFnSc1d1
surovina	surovina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Podkrkonoší	Podkrkonoší	k1gNnSc1
(	(	kIx(
<g/>
ametyst	ametyst	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Písecko	Písecko	k1gNnSc1
(	(	kIx(
<g/>
růženín	růženín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgInPc1d1
Bory	bor	k1gInPc1
u	u	k7c2
Velkého	velký	k2eAgNnSc2d1
Meziříčí	Meziříčí	k1gNnSc2
(	(	kIx(
<g/>
záhněda	záhněda	k1gFnSc1
<g/>
,	,	kIx,
růženín	růženín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Bochovice	Bochovice	k1gFnSc1
a	a	k8xC
Hostákov	Hostákov	k1gInSc1
(	(	kIx(
<g/>
mohutné	mohutný	k2eAgFnSc2d1
žíly	žíla	k1gFnSc2
s	s	k7c7
ametystem	ametyst	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Brdy	Brdy	k1gInPc1
(	(	kIx(
<g/>
barevné	barevný	k2eAgInPc4d1
železité	železitý	k2eAgInPc4d1
křemeny	křemen	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Strážník	strážník	k1gMnSc1
u	u	k7c2
Peřimova	Peřimův	k2eAgInSc2d1
(	(	kIx(
<g/>
hvězdovce	hvězdovka	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
Mez	mez	k1gFnSc1
<g/>
,	,	kIx,
Bohdalov	Bohdalov	k1gInSc1
a	a	k8xC
Nové	Nové	k2eAgNnSc1d1
Veselí	veselí	k1gNnSc1
(	(	kIx(
<g/>
žezlové	žezlový	k2eAgInPc1d1
ametysty	ametyst	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Laštovičky	laštovička	k1gFnPc1
<g/>
,	,	kIx,
Bobrůvka	Bobrůvka	k1gFnSc1
<g/>
,	,	kIx,
Rousměrov	Rousměrov	k1gInSc1
<g/>
,	,	kIx,
Suky	suk	k1gInPc1
(	(	kIx(
<g/>
záhněda	záhněda	k1gFnSc1
<g/>
,	,	kIx,
citrín	citrín	k?
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
pazourek	pazourek	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
křemen	křemen	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
křemen	křemen	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
popis	popis	k1gInSc1
křemene	křemen	k1gInSc2
v	v	k7c6
učebních	učební	k2eAgInPc6d1
textech	text	k1gInPc6
mineralogie	mineralogie	k1gFnSc2
na	na	k7c6
PřF	PřF	k1gFnSc6
MU	MU	kA
</s>
<s>
podrobný	podrobný	k2eAgInSc4d1
přehled	přehled	k1gInSc4
vlastností	vlastnost	k1gFnPc2
a	a	k8xC
odrůd	odrůda	k1gFnPc2
křemene	křemen	k1gInSc2
na	na	k7c4
Rockhound	Rockhound	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
učební	učební	k2eAgInSc1d1
text	text	k1gInSc1
o	o	k7c6
křemeni	křemen	k1gInSc6
od	od	k7c2
Dalibora	Dalibor	k1gMnSc2
Velebila	Velebil	k1gMnSc2
</s>
<s>
;	;	kIx,
navštíeno	navštíet	k5eAaImNgNnS,k5eAaBmNgNnS,k5eAaPmNgNnS
2018-08-08	2018-08-08	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Křemen	křemen	k1gInSc1
Fanerokrystalické	Fanerokrystalický	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
</s>
<s>
křišťál	křišťál	k1gInSc1
•	•	k?
ametyst	ametyst	k1gInSc1
•	•	k?
záhněda	záhněda	k1gFnSc1
•	•	k?
růženín	růženín	k1gInSc1
•	•	k?
morion	morion	k1gInSc1
•	•	k?
citrín	citrín	k?
•	•	k?
modrý	modrý	k2eAgInSc1d1
křemen	křemen	k1gInSc1
•	•	k?
tygří	tygří	k2eAgNnSc1d1
oko	oko	k1gNnSc1
•	•	k?
sokolí	sokolí	k2eAgNnSc1d1
oko	oko	k1gNnSc1
•	•	k?
kočičí	kočičí	k2eAgNnSc1d1
oko	oko	k1gNnSc1
•	•	k?
aventurin	aventurin	k1gInSc4
Kryptokrystalické	Kryptokrystalický	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
</s>
<s>
chalcedon	chalcedon	k1gInSc1
•	•	k?
achát	achát	k1gInSc1
•	•	k?
mechový	mechový	k2eAgInSc1d1
achát	achát	k1gInSc1
•	•	k?
onyx	onyx	k1gInSc1
•	•	k?
plazma	plazma	k1gFnSc1
•	•	k?
prasem	prasem	k1gInSc1
•	•	k?
karneol	karneol	k1gInSc1
•	•	k?
sardonyx	sardonyx	k1gInSc1
•	•	k?
chryzopras	chryzopras	k1gInSc1
•	•	k?
heliotrop	heliotrop	k1gInSc1
•	•	k?
jaspis	jaspis	k1gInSc1
•	•	k?
enhydros	enhydrosa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4048011-2	4048011-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
4717	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85109732	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85109732	#num#	k4
</s>
