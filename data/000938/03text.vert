<s>
Toronto	Toronto	k1gNnSc1	Toronto
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
2,6	[number]	k4	2,6
miliony	milion	k4xCgInPc7	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
břehu	břeh	k1gInSc6	břeh
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
šest	šest	k4xCc1	šest
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
East	East	k2eAgInSc1d1	East
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Etobicoke	Etobicoke	k1gInSc1	Etobicoke
<g/>
,	,	kIx,	,
North	North	k1gInSc1	North
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Scarborough	Scarborough	k1gInSc1	Scarborough
a	a	k8xC	a
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
sídelní	sídelní	k2eAgFnSc1d1	sídelní
aglomerace	aglomerace	k1gFnSc1	aglomerace
(	(	kIx(	(
<g/>
GTA	GTA	kA	GTA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k9	již
Toronto	Toronto	k1gNnSc4	Toronto
formuje	formovat	k5eAaImIp3nS	formovat
mj.	mj.	kA	mj.
společně	společně	k6eAd1	společně
s	s	k7c7	s
městy	město	k1gNnPc7	město
Mississauga	Mississaug	k1gMnSc2	Mississaug
<g/>
,	,	kIx,	,
Brampton	Brampton	k1gInSc1	Brampton
<g/>
,	,	kIx,	,
Vaughan	Vaughan	k1gInSc1	Vaughan
<g/>
,	,	kIx,	,
Pickerin	Pickerin	k1gInSc1	Pickerin
a	a	k8xC	a
Markham	Markham	k1gInSc1	Markham
<g/>
,	,	kIx,	,
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
6,3	[number]	k4	6,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
Toronto	Toronto	k1gNnSc1	Toronto
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
srdci	srdce	k1gNnSc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ontarijské	ontarijský	k2eAgFnSc2d1	Ontarijská
aglomerace	aglomerace	k1gFnSc2	aglomerace
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
podkova	podkova	k1gFnSc1	podkova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
8,7	[number]	k4	8,7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
kanadského	kanadský	k2eAgNnSc2d1	kanadské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nejsilnější	silný	k2eAgInPc1d3	nejsilnější
z	z	k7c2	z
kanadských	kanadský	k2eAgNnPc2d1	kanadské
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
Toronto	Toronto	k1gNnSc1	Toronto
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
středisko	středisko	k1gNnSc4	středisko
globálního	globální	k2eAgInSc2d1	globální
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Torontská	torontský	k2eAgFnSc1d1	Torontská
burza	burza	k1gFnSc1	burza
je	být	k5eAaImIp3nS	být
sedmou	sedmý	k4xOgFnSc7	sedmý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
burzou	burza	k1gFnSc7	burza
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
finančnictví	finančnictví	k1gNnSc2	finančnictví
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
pozici	pozice	k1gFnSc4	pozice
rovněž	rovněž	k9	rovněž
např.	např.	kA	např.
filmová	filmový	k2eAgFnSc1d1	filmová
či	či	k8xC	či
softwarová	softwarový	k2eAgFnSc1d1	softwarová
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ústředí	ústředí	k1gNnSc1	ústředí
většiny	většina	k1gFnSc2	většina
kanadských	kanadský	k2eAgFnPc2d1	kanadská
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkosmopolitnějších	kosmopolitní	k2eAgNnPc2d3	nejkosmopolitnější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
sem	sem	k6eAd1	sem
směřuje	směřovat	k5eAaImIp3nS	směřovat
většina	většina	k1gFnSc1	většina
imigrantů	imigrant	k1gMnPc2	imigrant
přijíždějících	přijíždějící	k2eAgMnPc2d1	přijíždějící
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
49	[number]	k4	49
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Toronta	Toronto	k1gNnSc2	Toronto
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
různorodých	různorodý	k2eAgFnPc2d1	různorodá
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
osobitými	osobitý	k2eAgInPc7d1	osobitý
zvyky	zvyk	k1gInPc7	zvyk
<g/>
,	,	kIx,	,
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
kuchyní	kuchyně	k1gFnSc7	kuchyně
se	se	k3xPyFc4	se
neopominutelně	opominutelně	k6eNd1	opominutelně
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
na	na	k7c6	na
městském	městský	k2eAgInSc6d1	městský
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nízké	nízký	k2eAgFnSc3d1	nízká
kriminalitě	kriminalita	k1gFnSc3	kriminalita
<g/>
,	,	kIx,	,
čistému	čistý	k2eAgNnSc3d1	čisté
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
životní	životní	k2eAgFnSc3d1	životní
úrovni	úroveň	k1gFnSc3	úroveň
a	a	k8xC	a
otevřenosti	otevřenost	k1gFnSc2	otevřenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
jiným	jiný	k2eAgFnPc3d1	jiná
kulturám	kultura	k1gFnPc3	kultura
je	být	k5eAaImIp3nS	být
Toronto	Toronto	k1gNnSc1	Toronto
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
podmínkami	podmínka	k1gFnPc7	podmínka
k	k	k7c3	k
životu	život	k1gInSc3	život
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Economist	Economist	k1gMnSc1	Economist
Intelligence	Intelligenec	k1gMnSc2	Intelligenec
Unit	Unit	k1gMnSc1	Unit
a	a	k8xC	a
průzkumu	průzkum	k1gInSc3	průzkum
Mercer	Mercra	k1gFnPc2	Mercra
Quality	Qualita	k1gFnSc2	Qualita
of	of	k?	of
Living	Living	k1gInSc1	Living
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
zástavbou	zástavba	k1gFnSc7	zástavba
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
překvapivě	překvapivě	k6eAd1	překvapivě
mnoho	mnoho	k4c1	mnoho
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
ostatně	ostatně	k6eAd1	ostatně
svědčí	svědčit	k5eAaImIp3nS	svědčit
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
rozšířených	rozšířený	k2eAgNnPc2d1	rozšířené
přízvisek	přízvisko	k1gNnPc2	přízvisko
Toronta	Toronto	k1gNnSc2	Toronto
"	"	kIx"	"
<g/>
město	město	k1gNnSc1	město
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
High	High	k1gInSc1	High
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
160	[number]	k4	160
ha	ha	kA	ha
a	a	k8xC	a
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
malá	malý	k2eAgFnSc1d1	malá
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
torontských	torontský	k2eAgInPc6d1	torontský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgInPc2d1	ležící
poblíž	poblíž	k7c2	poblíž
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
turistické	turistický	k2eAgFnPc4d1	turistická
stezky	stezka	k1gFnPc4	stezka
<g/>
,	,	kIx,	,
rybníky	rybník	k1gInPc4	rybník
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc4d2	menší
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
pláže	pláž	k1gFnPc4	pláž
<g/>
,	,	kIx,	,
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
Centreville	Centreville	k1gFnPc4	Centreville
a	a	k8xC	a
místa	místo	k1gNnPc4	místo
vhodná	vhodný	k2eAgNnPc4d1	vhodné
k	k	k7c3	k
piknikování	piknikování	k1gNnSc3	piknikování
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgMnPc3	všecek
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
pozitivům	pozitiv	k1gMnPc3	pozitiv
bylo	být	k5eAaImAgNnS	být
Toronto	Toronto	k1gNnSc4	Toronto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ohodnoceno	ohodnotit	k5eAaPmNgNnS	ohodnotit
jako	jako	k9	jako
nejdražší	drahý	k2eAgNnSc4d3	nejdražší
kanadské	kanadský	k2eAgNnSc4d1	kanadské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
někdejší	někdejší	k2eAgFnSc1d1	někdejší
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
samostatně	samostatně	k6eAd1	samostatně
stojící	stojící	k2eAgFnSc1d1	stojící
stavba	stavba	k1gFnSc1	stavba
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
CN	CN	kA	CN
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
553	[number]	k4	553
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
stavbou	stavba	k1gFnSc7	stavba
Burj	Burj	k1gInSc4	Burj
Khalifa	Khalif	k1gMnSc4	Khalif
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
turistických	turistický	k2eAgFnPc2d1	turistická
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Royal	Royal	k1gMnSc1	Royal
Ontario	Ontario	k1gNnSc4	Ontario
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Ontario	Ontario	k1gNnSc4	Ontario
<g/>
,	,	kIx,	,
Baťa	Baťa	k1gMnSc1	Baťa
Shoe	Sho	k1gMnSc2	Sho
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
Gardiner	Gardiner	k1gInSc4	Gardiner
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
Casa	Casa	k1gMnSc1	Casa
Loma	Loma	k1gMnSc1	Loma
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Ballet	Ballet	k1gInSc1	Ballet
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
Science	Science	k1gFnSc2	Science
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
Hockey	Hockea	k1gFnPc4	Hockea
Hall	Halla	k1gFnPc2	Halla
of	of	k?	of
Fame	Fame	k1gNnSc4	Fame
(	(	kIx(	(
<g/>
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Creek	Creek	k1gMnSc1	Creek
Pioneer	Pioneer	kA	Pioneer
Village	Village	k1gNnSc2	Village
<g/>
,	,	kIx,	,
Riverdale	Riverdala	k1gFnSc3	Riverdala
Farm	Farma	k1gFnPc2	Farma
a	a	k8xC	a
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Innuit	Innuit	k1gMnSc1	Innuit
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Torontské	torontský	k2eAgNnSc1d1	Torontské
zoo	zoo	k1gNnSc1	zoo
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
tisíci	tisíc	k4xCgInPc7	tisíc
zvířat	zvíře	k1gNnPc2	zvíře
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
Toronto	Toronto	k1gNnSc1	Toronto
Islands	Islandsa	k1gFnPc2	Islandsa
(	(	kIx(	(
<g/>
Torontské	torontský	k2eAgInPc1d1	torontský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Distillery	Distiller	k1gInPc1	Distiller
District	Districta	k1gFnPc2	Districta
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Lawrence	Lawrence	k1gFnSc1	Lawrence
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Beaches	Beaches	k1gMnSc1	Beaches
<g/>
,	,	kIx,	,
Harbourfront	Harbourfront	k1gMnSc1	Harbourfront
<g/>
,	,	kIx,	,
Scarborough	Scarborough	k1gMnSc1	Scarborough
Bluffs	Bluffsa	k1gFnPc2	Bluffsa
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Young	Young	k1gInSc1	Young
-	-	kIx~	-
Dundas	Dundas	k1gInSc1	Dundas
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Financial	Financial	k1gMnSc1	Financial
District	District	k1gMnSc1	District
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
mrakodrapy	mrakodrap	k1gInPc7	mrakodrap
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
A.T.H.	A.T.H.	k1gFnSc1	A.T.H.
(	(	kIx(	(
<g/>
největší	veliký	k2eAgNnSc1d3	veliký
podzemní	podzemní	k2eAgNnSc1d1	podzemní
obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
27	[number]	k4	27
km	km	kA	km
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1200	[number]	k4	1200
obchody	obchod	k1gInPc7	obchod
<g/>
)	)	kIx)	)
a	a	k8xC	a
Exhibition	Exhibition	k1gInSc1	Exhibition
Place	plac	k1gInSc6	plac
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každoroční	každoroční	k2eAgFnSc2d1	každoroční
výstavy	výstava	k1gFnSc2	výstava
na	na	k7c4	na
CNE	CNE	kA	CNE
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Ex	ex	k6eAd1	ex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Canada	Canada	k1gFnSc1	Canada
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wonderland	Wonderlanda	k1gFnPc2	Wonderlanda
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
km2	km2	k4	km2
<g/>
)	)	kIx)	)
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letní	letní	k2eAgFnSc2d1	letní
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
květen-říjen	květen-říjen	k2eAgMnSc1d1	květen-říjen
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Toronta	Toronto	k1gNnSc2	Toronto
pochází	pocházet	k5eAaImIp3nS	pocházet
řada	řada	k1gFnSc1	řada
věhlasných	věhlasný	k2eAgInPc2d1	věhlasný
sportovních	sportovní	k2eAgInPc2d1	sportovní
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
Toronto	Toronto	k1gNnSc4	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
Blue	Blue	k1gFnPc2	Blue
Jays	Jaysa	k1gFnPc2	Jaysa
(	(	kIx(	(
<g/>
MLB	MLB	kA	MLB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Argonauts	Argonauts	k1gInSc1	Argonauts
(	(	kIx(	(
<g/>
CFL	CFL	kA	CFL
<g/>
)	)	kIx)	)
a	a	k8xC	a
Toronto	Toronto	k1gNnSc1	Toronto
Raptors	Raptorsa	k1gFnPc2	Raptorsa
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gInSc1	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Čchung-čching	Čchung-čching	k1gInSc1	Čchung-čching
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Miláno	Milán	k2eAgNnSc1d1	Miláno
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c6	na
Donu	Don	k1gInSc6	Don
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Eilat	Eilat	k1gInSc1	Eilat
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Teherán	Teherán	k1gInSc1	Teherán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
Sao	Sao	k1gFnSc2	Sao
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc5	Brazílie
Ho	on	k3xPp3gNnSc2	on
Či	či	k8xC	či
Minovo	Minův	k2eAgNnSc1d1	Minovo
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Volgograd	Volgograd	k1gInSc1	Volgograd
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Quito	Quit	k2eAgNnSc1d1	Quito
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Sagamihara	Sagamihara	k1gFnSc1	Sagamihara
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Toronto	Toronto	k1gNnSc1	Toronto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
