<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
globální	globální	k2eAgInSc1d1	globální
vojenský	vojenský	k2eAgInSc1d1	vojenský
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
miliony	milion	k4xCgInPc4	milion
obětí	oběť	k1gFnPc2	oběť
dosud	dosud	k6eAd1	dosud
největším	veliký	k2eAgMnPc3d3	veliký
a	a	k8xC	a
nejvíc	nejvíc	k6eAd1	nejvíc
zničujícím	zničující	k2eAgNnSc7d1	zničující
válečným	válečný	k2eAgNnSc7d1	válečné
střetnutím	střetnutí	k1gNnSc7	střetnutí
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
když	když	k8xS	když
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
Francie	Francie	k1gFnPc1	Francie
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
státy	stát	k1gInPc1	stát
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
Německu	Německo	k1gNnSc6	Německo
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
považováno	považován	k2eAgNnSc4d1	považováno
přepadení	přepadení	k1gNnSc4	přepadení
Číny	Čína	k1gFnSc2	Čína
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
již	již	k6eAd1	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nastal	nastat	k5eAaPmAgInS	nastat
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
kapitulací	kapitulace	k1gFnSc7	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
americkém	americký	k2eAgNnSc6d1	americké
svržení	svržení	k1gNnSc6	svržení
atomových	atomový	k2eAgFnPc2d1	atomová
bomb	bomba	k1gFnPc2	bomba
na	na	k7c6	na
města	město	k1gNnSc2	město
Hirošima	Hirošima	k1gFnSc1	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
Japonsko	Japonsko	k1gNnSc1	Japonsko
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
války	válka	k1gFnSc2	válka
bývají	bývat	k5eAaImIp3nP	bývat
hledány	hledat	k5eAaImNgFnP	hledat
v	v	k7c6	v
důsledcích	důsledek	k1gInPc6	důsledek
ideologií	ideologie	k1gFnPc2	ideologie
a	a	k8xC	a
politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
imperialismus	imperialismus	k1gInSc4	imperialismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
v	v	k7c6	v
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
s	s	k7c7	s
Versailleskou	versailleský	k2eAgFnSc7d1	Versailleská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
prohloubit	prohloubit	k5eAaPmF	prohloubit
pocit	pocit	k1gInSc4	pocit
ponížení	ponížení	k1gNnSc2	ponížení
v	v	k7c6	v
poražených	poražený	k2eAgInPc6d1	poražený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dopadech	dopad	k1gInPc6	dopad
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
vlivy	vliv	k1gInPc4	vliv
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
oslabily	oslabit	k5eAaPmAgInP	oslabit
mnoho	mnoho	k4c4	mnoho
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vzestup	vzestup	k1gInSc4	vzestup
nacismu	nacismus	k1gInSc2	nacismus
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
např.	např.	kA	např.
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fašismu	fašismus	k1gInSc2	fašismus
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
např.	např.	kA	např.
Benita	Benit	k1gMnSc2	Benit
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
)	)	kIx)	)
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
např.	např.	kA	např.
Josifa	Josif	k1gMnSc2	Josif
Vissarionoviče	Vissarionovič	k1gMnSc2	Vissarionovič
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
totalitních	totalitní	k2eAgInPc2d1	totalitní
států	stát	k1gInPc2	stát
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnSc3d1	německá
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
předcházela	předcházet	k5eAaImAgFnS	předcházet
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
<g/>
,	,	kIx,	,
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
datovaná	datovaný	k2eAgFnSc1d1	datovaná
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
paktem	pakt	k1gInSc7	pakt
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
–	–	k?	–
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgNnPc2d1	jiné
ustanovení	ustanovení	k1gNnPc2	ustanovení
–	–	k?	–
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
expanzi	expanze	k1gFnSc4	expanze
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
šestnáct	šestnáct	k4xCc4	šestnáct
dní	den	k1gInPc2	den
po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
také	také	k9	také
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
událostí	událost	k1gFnPc2	událost
byl	být	k5eAaImAgInS	být
však	však	k9	však
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
sám	sám	k3xTgMnSc1	sám
přepaden	přepadnout	k5eAaPmNgMnS	přepadnout
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spojenci	spojenec	k1gMnPc7	spojenec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
některým	některý	k3yIgMnPc3	některý
Spojencům	spojenec	k1gMnPc3	spojenec
v	v	k7c6	v
počínající	počínající	k2eAgFnSc6d1	počínající
válce	válka	k1gFnSc6	válka
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
války	válka	k1gFnSc2	válka
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Japonsko	Japonsko	k1gNnSc1	Japonsko
udeřilo	udeřit	k5eAaPmAgNnS	udeřit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
námořní	námořní	k2eAgFnSc4d1	námořní
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Pearl	Pearla	k1gFnPc2	Pearla
Harboru	Harbor	k1gInSc2	Harbor
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
provázely	provázet	k5eAaImAgFnP	provázet
v	v	k7c4	v
dosud	dosud	k6eAd1	dosud
nevídané	vídaný	k2eNgFnSc6d1	nevídaná
míře	míra	k1gFnSc6	míra
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgInPc1d1	válečný
zločiny	zločin	k1gInPc1	zločin
a	a	k8xC	a
nehumánní	humánní	k2eNgNnPc1d1	nehumánní
zacházení	zacházení	k1gNnPc1	zacházení
s	s	k7c7	s
válečnými	válečný	k2eAgMnPc7d1	válečný
zajatci	zajatec	k1gMnPc7	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
všem	všecek	k3xTgInPc3	všecek
dosavadním	dosavadní	k2eAgInPc3d1	dosavadní
konfliktům	konflikt	k1gInPc3	konflikt
bylo	být	k5eAaImAgNnS	být
průběhem	průběh	k1gInSc7	průběh
bojů	boj	k1gInPc2	boj
podstatně	podstatně	k6eAd1	podstatně
zasaženo	zasažen	k2eAgNnSc1d1	zasaženo
rovněž	rovněž	k9	rovněž
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
genocidy	genocida	k1gFnSc2	genocida
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nacistické	nacistický	k2eAgFnSc2d1	nacistická
rasové	rasový	k2eAgFnSc2d1	rasová
ideologie	ideologie	k1gFnSc2	ideologie
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
šest	šest	k4xCc1	šest
milionů	milion	k4xCgInPc2	milion
evropských	evropský	k2eAgMnPc2d1	evropský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
milionové	milionový	k2eAgFnPc1d1	milionová
oběti	oběť	k1gFnPc1	oběť
byly	být	k5eAaImAgFnP	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
územích	území	k1gNnPc6	území
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Civilní	civilní	k2eAgFnPc1d1	civilní
oběti	oběť	k1gFnPc1	oběť
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
hladu	hlad	k1gInSc3	hlad
vyvolanými	vyvolaný	k2eAgFnPc7d1	vyvolaná
válečnými	válečný	k2eAgFnPc7d1	válečná
operacemi	operace	k1gFnPc7	operace
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
masakrům	masakr	k1gInPc3	masakr
nebo	nebo	k8xC	nebo
genocidě	genocida	k1gFnSc3	genocida
páchanými	páchaný	k2eAgInPc7d1	páchaný
německými	německý	k2eAgInPc7d1	německý
a	a	k8xC	a
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
komandy	komando	k1gNnPc7	komando
na	na	k7c6	na
obsazených	obsazený	k2eAgNnPc6d1	obsazené
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Milionům	milion	k4xCgInPc3	milion
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
předejít	předejít	k5eAaPmF	předejít
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
dodávkami	dodávka	k1gFnPc7	dodávka
nejen	nejen	k6eAd1	nejen
potravin	potravina	k1gFnPc2	potravina
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc6	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgNnSc1d1	válečné
úsilí	úsilí	k1gNnSc1	úsilí
pohlcovalo	pohlcovat	k5eAaImAgNnS	pohlcovat
veškerý	veškerý	k3xTgInSc4	veškerý
lidský	lidský	k2eAgInSc4d1	lidský
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
<g/>
,	,	kIx,	,
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
potenciál	potenciál	k1gInSc4	potenciál
všech	všecek	k3xTgInPc2	všecek
zúčastněných	zúčastněný	k2eAgInPc2d1	zúčastněný
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
totální	totální	k2eAgFnSc2d1	totální
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
"	"	kIx"	"
<g/>
civilizované	civilizovaný	k2eAgFnPc4d1	civilizovaná
metody	metoda	k1gFnPc4	metoda
válčení	válčení	k1gNnSc2	válčení
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
výrazně	výrazně	k6eAd1	výrazně
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
demokratické	demokratický	k2eAgInPc1d1	demokratický
státy	stát	k1gInPc1	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
USA	USA	kA	USA
odmítaly	odmítat	k5eAaImAgFnP	odmítat
plošné	plošný	k2eAgInPc4d1	plošný
nálety	nálet	k1gInPc4	nálet
na	na	k7c4	na
nepřátelská	přátelský	k2eNgNnPc4d1	nepřátelské
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
samy	sám	k3xTgFnPc1	sám
uchýlily	uchýlit	k5eAaPmAgInP	uchýlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
ústředním	ústřední	k2eAgInSc7d1	ústřední
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
prevence	prevence	k1gFnSc1	prevence
vzniku	vznik	k1gInSc2	vznik
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
dalšího	další	k2eAgInSc2d1	další
podobného	podobný	k2eAgInSc2d1	podobný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgFnP	zrodit
dvě	dva	k4xCgFnPc1	dva
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
světové	světový	k2eAgFnPc1d1	světová
supervelmoci	supervelmoc	k1gFnPc1	supervelmoc
<g/>
:	:	kIx,	:
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
antagonismus	antagonismus	k1gInSc1	antagonismus
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
bipolárnímu	bipolární	k2eAgNnSc3d1	bipolární
rozdělení	rozdělení	k1gNnSc3	rozdělení
světa	svět	k1gInSc2	svět
a	a	k8xC	a
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
spustil	spustit	k5eAaPmAgMnS	spustit
napříč	napříč	k7c7	napříč
evropským	evropský	k2eAgInSc7d1	evropský
kontinentem	kontinent	k1gInSc7	kontinent
železnou	železný	k2eAgFnSc4d1	železná
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělila	oddělit	k5eAaPmAgFnS	oddělit
svobodný	svobodný	k2eAgInSc4d1	svobodný
západní	západní	k2eAgInSc4d1	západní
svět	svět	k1gInSc4	svět
a	a	k8xC	a
komunistické	komunistický	k2eAgInPc4d1	komunistický
státy	stát	k1gInPc4	stát
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
poddané	poddaná	k1gFnSc2	poddaná
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
efektem	efekt	k1gInSc7	efekt
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
také	také	k9	také
vzrůst	vzrůst	k1gInSc1	vzrůst
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
ovládanými	ovládaný	k2eAgFnPc7d1	ovládaná
koloniálními	koloniální	k2eAgFnPc7d1	koloniální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
akceleraci	akcelerace	k1gFnSc3	akcelerace
dekolonizačních	dekolonizační	k2eAgNnPc2d1	dekolonizační
hnutí	hnutí	k1gNnPc2	hnutí
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Příčiny	příčina	k1gFnSc2	příčina
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc4	příčina
nového	nový	k2eAgInSc2d1	nový
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
konfliktu	konflikt	k1gInSc2	konflikt
tkvěly	tkvět	k5eAaImAgFnP	tkvět
v	v	k7c6	v
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
s	s	k7c7	s
výsledky	výsledek	k1gInPc4	výsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
menších	malý	k2eAgFnPc2d2	menší
zemí	zem	k1gFnPc2	zem
zahnízdila	zahnízdit	k5eAaPmAgFnS	zahnízdit
především	především	k6eAd1	především
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
velkých	velký	k2eAgFnPc6d1	velká
mocnostech	mocnost	k1gFnPc6	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Poražené	poražený	k2eAgNnSc1d1	poražené
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
přinuceno	přinutit	k5eAaPmNgNnS	přinutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
významné	významný	k2eAgFnSc2d1	významná
části	část	k1gFnSc2	část
svého	svůj	k3xOyFgNnSc2	svůj
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
území	území	k1gNnSc2	území
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vítězné	vítězný	k2eAgFnSc2d1	vítězná
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Smělo	smět	k5eAaImAgNnS	smět
nadále	nadále	k6eAd1	nadále
udržovat	udržovat	k5eAaImF	udržovat
jenom	jenom	k9	jenom
omezené	omezený	k2eAgFnPc4d1	omezená
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
zavázat	zavázat	k5eAaPmF	zavázat
k	k	k7c3	k
platbám	platba	k1gFnPc3	platba
vysokých	vysoký	k2eAgFnPc2d1	vysoká
válečných	válečná	k1gFnPc2	válečná
reparací	reparace	k1gFnPc2	reparace
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
států	stát	k1gInPc2	stát
stojících	stojící	k2eAgInPc2d1	stojící
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vítězů	vítěz	k1gMnPc2	vítěz
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jenom	jenom	k9	jenom
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
území	území	k1gNnSc4	území
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
předchozím	předchozí	k2eAgFnPc3d1	předchozí
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
a	a	k8xC	a
ztrátám	ztráta	k1gFnPc3	ztráta
materiálním	materiální	k2eAgFnPc3d1	materiální
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
neuspokojena	uspokojen	k2eNgFnSc1d1	neuspokojena
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
velmocenských	velmocenský	k2eAgFnPc6d1	velmocenská
ambicích	ambice	k1gFnPc6	ambice
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
vítězné	vítězný	k2eAgNnSc1d1	vítězné
Japonsko	Japonsko	k1gNnSc1	Japonsko
neuspělo	uspět	k5eNaPmAgNnS	uspět
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
posílit	posílit	k5eAaPmF	posílit
svoji	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kontrolu	kontrola	k1gFnSc4	kontrola
zdrojů	zdroj	k1gInPc2	zdroj
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nemohlo	moct	k5eNaImAgNnS	moct
získávat	získávat	k5eAaImF	získávat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
prostorově	prostorově	k6eAd1	prostorově
omezeném	omezený	k2eAgNnSc6d1	omezené
ostrovním	ostrovní	k2eAgNnSc6d1	ostrovní
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
ropu	ropa	k1gFnSc4	ropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
demokratické	demokratický	k2eAgFnPc1d1	demokratická
mocnosti	mocnost	k1gFnPc1	mocnost
pokusily	pokusit	k5eAaPmAgFnP	pokusit
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
trvalost	trvalost	k1gFnSc4	trvalost
versailleského	versailleský	k2eAgNnSc2d1	versailleské
uspořádání	uspořádání	k1gNnSc2	uspořádání
Evropy	Evropa	k1gFnSc2	Evropa
diplomatickými	diplomatický	k2eAgInPc7d1	diplomatický
kroky	krok	k1gInPc7	krok
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
stabilního	stabilní	k2eAgInSc2d1	stabilní
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
(	(	kIx(	(
<g/>
ve	v	k7c6	v
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnSc6d1	mluvící
části	část	k1gFnSc6	část
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
by	by	kYmCp3nP	by
národy	národ	k1gInPc4	národ
mírovým	mírový	k2eAgInSc7d1	mírový
způsobem	způsob	k1gInSc7	způsob
urovnávaly	urovnávat	k5eAaImAgInP	urovnávat
svoje	svůj	k3xOyFgFnPc4	svůj
spory	spora	k1gFnPc4	spora
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rovněž	rovněž	k6eAd1	rovněž
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Locarnu	Locarno	k1gNnSc6	Locarno
sjednány	sjednán	k2eAgFnPc1d1	sjednána
dohody	dohoda	k1gFnPc1	dohoda
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byla	být	k5eAaImAgFnS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
záruka	záruka	k1gFnSc1	záruka
trvalosti	trvalost	k1gFnSc2	trvalost
německo-francouzské	německorancouzský	k2eAgFnSc2d1	německo-francouzská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
případné	případný	k2eAgInPc1d1	případný
územní	územní	k2eAgInPc1d1	územní
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
východními	východní	k2eAgMnPc7d1	východní
sousedy	soused	k1gMnPc7	soused
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
řešeny	řešit	k5eAaImNgInP	řešit
rozhodčím	rozhodčí	k1gMnPc3	rozhodčí
řízením	řízení	k1gNnSc7	řízení
(	(	kIx(	(
<g/>
arbitráží	arbitráž	k1gFnPc2	arbitráž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgInS	být
nadto	nadto	k6eAd1	nadto
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
Briand-Kelloggův	Briand-Kelloggův	k2eAgInSc1d1	Briand-Kelloggův
pakt	pakt	k1gInSc1	pakt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
všem	všecek	k3xTgInPc3	všecek
účastenským	účastenský	k2eAgInPc3d1	účastenský
státům	stát	k1gInPc3	stát
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
vedení	vedení	k1gNnSc4	vedení
útočné	útočný	k2eAgFnSc2d1	útočná
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mírové	mírový	k2eAgFnPc1d1	mírová
snahy	snaha	k1gFnPc1	snaha
však	však	k9	však
dostaly	dostat	k5eAaPmAgFnP	dostat
povážlivé	povážlivý	k2eAgFnPc1d1	povážlivá
trhliny	trhlina	k1gFnPc1	trhlina
jednak	jednak	k8xC	jednak
obnoveným	obnovený	k2eAgInSc7d1	obnovený
izolacionismem	izolacionismus	k1gInSc7	izolacionismus
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
rozmachem	rozmach	k1gInSc7	rozmach
různých	různý	k2eAgInPc2d1	různý
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
výraznou	výrazný	k2eAgFnSc7d1	výrazná
měrou	míra	k1gFnSc7wR	míra
přispěla	přispět	k5eAaPmAgFnS	přispět
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
devastující	devastující	k2eAgInSc4d1	devastující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
světové	světový	k2eAgNnSc4d1	světové
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vedla	vést	k5eAaImAgFnS	vést
Ruská	ruský	k2eAgFnSc1d1	ruská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
komunistického	komunistický	k2eAgInSc2d1	komunistický
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
Leninově	Leninův	k2eAgFnSc6d1	Leninova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pochodu	pochod	k1gInSc6	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
převzal	převzít	k5eAaPmAgMnS	převzít
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgFnSc4	první
fašistickou	fašistický	k2eAgFnSc4d1	fašistická
diktaturu	diktatura	k1gFnSc4	diktatura
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
vliv	vliv	k1gInSc1	vliv
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
císařskou	císařský	k2eAgFnSc4d1	císařská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
výbojného	výbojný	k2eAgInSc2d1	výbojný
militaristického	militaristický	k2eAgInSc2d1	militaristický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mukdenském	mukdenský	k2eAgInSc6d1	mukdenský
incidentu	incident	k1gInSc6	incident
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Japonsko	Japonsko	k1gNnSc1	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přeměnilo	přeměnit	k5eAaPmAgNnS	přeměnit
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
loutkový	loutkový	k2eAgInSc4d1	loutkový
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvrat	rozvrat	k1gInSc1	rozvrat
země	zem	k1gFnSc2	zem
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
projevující	projevující	k2eAgFnPc1d1	projevující
expanzionistické	expanzionistický	k2eAgFnPc1d1	expanzionistická
tendence	tendence	k1gFnPc1	tendence
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vzestup	vzestup	k1gInSc4	vzestup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc2	vůdce
nacionálních	nacionální	k2eAgMnPc2d1	nacionální
socialistů	socialist	k1gMnPc2	socialist
(	(	kIx(	(
<g/>
NSDAP	NSDAP	kA	NSDAP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sliboval	slibovat	k5eAaImAgMnS	slibovat
německému	německý	k2eAgInSc3d1	německý
národu	národ	k1gInSc3	národ
zrušení	zrušení	k1gNnSc2	zrušení
versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
zajištění	zajištění	k1gNnSc2	zajištění
"	"	kIx"	"
<g/>
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
"	"	kIx"	"
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
Lebensraum	Lebensraum	k1gNnSc1	Lebensraum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k4c4	málo
měsíců	měsíc	k1gInPc2	měsíc
realizoval	realizovat	k5eAaBmAgMnS	realizovat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
obnovena	obnoven	k2eAgFnSc1d1	obnovena
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
urychleno	urychlen	k2eAgNnSc4d1	urychleno
obnovení	obnovení	k1gNnSc4	obnovení
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
bylo	být	k5eAaImAgNnS	být
připojeno	připojen	k2eAgNnSc1d1	připojeno
Sársko	Sársko	k1gNnSc1	Sársko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
napadla	napadnout	k5eAaPmAgFnS	napadnout
Itálie	Itálie	k1gFnSc1	Itálie
za	za	k7c2	za
blahovolného	blahovolný	k2eAgNnSc2d1	blahovolné
přihlížení	přihlížení	k1gNnSc2	přihlížení
Společnosti	společnost	k1gFnPc1	společnost
národů	národ	k1gInPc2	národ
Habeš	Habeš	k1gFnSc1	Habeš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
smlouvami	smlouva	k1gFnPc7	smlouva
z	z	k7c2	z
Versailles	Versailles	k1gFnSc2	Versailles
a	a	k8xC	a
Locarna	Locarno	k1gNnSc2	Locarno
remilitarizoval	remilitarizovat	k5eAaBmAgMnS	remilitarizovat
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1936	[number]	k4	1936
Porýní	Porýní	k1gNnPc2	Porýní
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
však	však	k9	však
zdržely	zdržet	k5eAaPmAgFnP	zdržet
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
protiakcí	protiakce	k1gFnPc2	protiakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistické	nacionalistický	k2eAgFnPc1d1	nacionalistická
síly	síla	k1gFnPc1	síla
fašistického	fašistický	k2eAgMnSc2d1	fašistický
generála	generál	k1gMnSc2	generál
Francisca	Franciscus	k1gMnSc2	Franciscus
Franca	Franca	k?	Franca
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgInP	postavit
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
vládě	vláda	k1gFnSc3	vláda
vedené	vedený	k2eAgFnSc2d1	vedená
levicovými	levicový	k2eAgMnPc7d1	levicový
republikány	republikán	k1gMnPc7	republikán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
brigádami	brigáda	k1gFnPc7	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Franco	Franco	k6eAd1	Franco
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
krvavém	krvavý	k2eAgInSc6d1	krvavý
střetu	střet	k1gInSc6	střet
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
válčení	válčení	k1gNnSc2	válčení
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1936	[number]	k4	1936
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
tzv.	tzv.	kA	tzv.
Osu	osa	k1gFnSc4	osa
Berlín-Řím	Berlín-Ří	k1gMnPc3	Berlín-Ří
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
sjednaly	sjednat	k5eAaPmAgFnP	sjednat
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
protisovětské	protisovětský	k2eAgNnSc4d1	protisovětské
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
pakt	pakt	k1gInSc4	pakt
proti	proti	k7c3	proti
Kominterně	Kominterna	k1gFnSc3	Kominterna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Osa	osa	k1gFnSc1	osa
Berlín-Řím-Tokio	Berlín-Řím-Tokio	k6eAd1	Berlín-Řím-Tokio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1937	[number]	k4	1937
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Japonsko	Japonsko	k1gNnSc1	Japonsko
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
započala	započnout	k5eAaPmAgFnS	započnout
druhá	druhý	k4xOgFnSc1	druhý
čínsko-japonská	čínskoaponský	k2eAgFnSc1d1	čínsko-japonská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
válčící	válčící	k2eAgFnSc2d1	válčící
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společně	společně	k6eAd1	společně
čelili	čelit	k5eAaImAgMnP	čelit
cizím	cizí	k2eAgMnPc3d1	cizí
útočníkům	útočník	k1gMnPc3	útočník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
Japonci	Japonec	k1gMnPc1	Japonec
brzy	brzy	k6eAd1	brzy
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Šanghaj	Šanghaj	k1gFnSc4	Šanghaj
<g/>
,	,	kIx,	,
Nanking	Nanking	k1gInSc4	Nanking
a	a	k8xC	a
nejprůmyslovější	průmyslový	k2eAgFnPc4d3	nejprůmyslovější
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerův	Hitlerův	k2eAgInSc1d1	Hitlerův
expanzionizmus	expanzionizmus	k1gInSc1	expanzionizmus
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vedl	vést	k5eAaImAgInS	vést
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
anšlusu	anšlus	k1gInSc3	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
připojení	připojení	k1gNnSc1	připojení
tohoto	tento	k3xDgInSc2	tento
malého	malý	k2eAgInSc2d1	malý
státu	stát	k1gInSc2	stát
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yQnSc3	což
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
své	svůj	k3xOyFgMnPc4	svůj
znepokojení	znepokojení	k1gNnSc1	znepokojení
německým	německý	k2eAgInSc7d1	německý
postupem	postup	k1gInSc7	postup
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	s	k7c7	s
stupňujícím	stupňující	k2eAgNnSc7d1	stupňující
zbrojením	zbrojení	k1gNnSc7	zbrojení
<g/>
,	,	kIx,	,
nijak	nijak	k6eAd1	nijak
nezasáhly	zasáhnout	k5eNaPmAgFnP	zasáhnout
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
Hitler	Hitler	k1gMnSc1	Hitler
Československu	Československo	k1gNnSc6	Československo
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
Německu	Německo	k1gNnSc3	Německo
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
československé	československý	k2eAgNnSc1d1	Československé
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
území	území	k1gNnSc1	území
obývané	obývaný	k2eAgFnSc2d1	obývaná
sudetskými	sudetský	k2eAgMnPc7d1	sudetský
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pohraničí	pohraničí	k1gNnSc6	pohraničí
puč	puč	k1gInSc1	puč
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
Sudetoněmeckou	sudetoněmecký	k2eAgFnSc7d1	Sudetoněmecká
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
sice	sice	k8xC	sice
tento	tento	k3xDgInSc4	tento
puč	puč	k1gInSc4	puč
potlačila	potlačit	k5eAaPmAgFnS	potlačit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
začaly	začít	k5eAaPmAgFnP	začít
proti	proti	k7c3	proti
Československu	Československo	k1gNnSc3	Československo
útočit	útočit	k5eAaImF	útočit
jednotky	jednotka	k1gFnPc1	jednotka
Sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
Freikorpsu	Freikorps	k1gInSc2	Freikorps
vyzbrojovaného	vyzbrojovaný	k2eAgInSc2d1	vyzbrojovaný
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc4	datum
začátku	začátek	k1gInSc2	začátek
této	tento	k3xDgFnSc2	tento
teroristické	teroristický	k2eAgFnSc2d1	teroristická
kampaně	kampaň	k1gFnSc2	kampaň
bylo	být	k5eAaImAgNnS	být
pozdější	pozdní	k2eAgNnSc1d2	pozdější
československou	československý	k2eAgFnSc7d1	Československá
exilovou	exilový	k2eAgFnSc7d1	exilová
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
určeno	určen	k2eAgNnSc4d1	určeno
jako	jako	k9	jako
datum	datum	k1gNnSc4	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Československo	Československo	k1gNnSc1	Československo
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Neville	Neville	k1gFnSc2	Neville
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
mezitím	mezitím	k6eAd1	mezitím
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
přijetím	přijetí	k1gNnSc7	přijetí
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
donuceno	donucen	k2eAgNnSc1d1	donuceno
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
Sudet	Sudety	k1gFnPc2	Sudety
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
příslib	příslib	k1gInSc4	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezabere	zabrat	k5eNaPmIp3nS	zabrat
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
československé	československý	k2eAgNnSc4d1	Československé
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
byl	být	k5eAaImAgInS	být
nucený	nucený	k2eAgInSc1d1	nucený
odchod	odchod	k1gInSc1	odchod
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
odpůrců	odpůrce	k1gMnPc2	odpůrce
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
do	do	k7c2	do
okleštěného	okleštěný	k2eAgNnSc2d1	okleštěné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
"	"	kIx"	"
<g/>
míru	míra	k1gFnSc4	míra
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
dobu	doba	k1gFnSc4	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
neuplynulo	uplynout	k5eNaPmAgNnS	uplynout
ani	ani	k8xC	ani
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
když	když	k8xS	když
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
obsadil	obsadit	k5eAaPmAgInS	obsadit
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
zbytek	zbytek	k1gInSc1	zbytek
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
v	v	k7c4	v
Protektorát	protektorát	k1gInSc4	protektorát
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
svoji	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
slovenský	slovenský	k2eAgMnSc1d1	slovenský
štát	štát	k1gMnSc1	štát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jozefem	Jozef	k1gMnSc7	Jozef
Tisem	tis	k1gInSc7	tis
německým	německý	k2eAgInSc7d1	německý
satelitem	satelit	k1gInSc7	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Pobouřeni	pobouřit	k5eAaPmNgMnP	pobouřit
touto	tento	k3xDgFnSc7	tento
novou	nový	k2eAgFnSc7d1	nová
agresí	agrese	k1gFnSc7	agrese
a	a	k8xC	a
Hitlerovými	Hitlerův	k2eAgFnPc7d1	Hitlerova
výhrůžkami	výhrůžka	k1gFnPc7	výhrůžka
vůči	vůči	k7c3	vůči
Polsku	Polsko	k1gNnSc3	Polsko
kvůli	kvůli	k7c3	kvůli
Gdaňsku	Gdaňsk	k1gInSc3	Gdaňsk
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
zaručili	zaručit	k5eAaPmAgMnP	zaručit
za	za	k7c4	za
polskou	polský	k2eAgFnSc4d1	polská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
mezi	mezi	k7c7	mezi
Stalinem	Stalin	k1gMnSc7	Stalin
a	a	k8xC	a
západními	západní	k2eAgFnPc7d1	západní
demokraciemi	demokracie	k1gFnPc7	demokracie
nakonec	nakonec	k6eAd1	nakonec
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
sjednání	sjednání	k1gNnSc3	sjednání
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
k	k	k7c3	k
oboustranné	oboustranný	k2eAgFnSc3d1	oboustranná
neutralitě	neutralita	k1gFnSc3	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajném	tajný	k2eAgInSc6d1	tajný
dodatku	dodatek	k1gInSc6	dodatek
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
smlouvě	smlouva	k1gFnSc3	smlouva
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
velmoci	velmoc	k1gFnPc4	velmoc
navíc	navíc	k6eAd1	navíc
-	-	kIx~	-
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
-	-	kIx~	-
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
střetu	střet	k1gInSc3	střet
na	na	k7c4	na
území	území	k1gNnSc4	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Jablunkovský	jablunkovský	k2eAgInSc4d1	jablunkovský
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
německo-polská	německoolský	k2eAgFnSc1d1	německo-polská
přestřelka	přestřelka	k1gFnSc1	přestřelka
v	v	k7c6	v
Mostech	most	k1gInPc6	most
u	u	k7c2	u
Jablunkova	Jablunkův	k2eAgInSc2d1	Jablunkův
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chronologie	chronologie	k1gFnSc2	chronologie
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1939	[number]	k4	1939
německým	německý	k2eAgInSc7d1	německý
a	a	k8xC	a
slovenským	slovenský	k2eAgInSc7d1	slovenský
útokem	útok	k1gInSc7	útok
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
dnech	dno	k1gNnPc6	dno
provedli	provést	k5eAaPmAgMnP	provést
Němci	Němec	k1gMnPc1	Němec
několik	několik	k4yIc4	několik
fingovaných	fingovaný	k2eAgFnPc2d1	fingovaná
provokací	provokace	k1gFnPc2	provokace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
přepadení	přepadení	k1gNnSc4	přepadení
vysílačky	vysílačka	k1gFnSc2	vysílačka
v	v	k7c6	v
Gliwicích	Gliwicí	k2eAgFnPc6d1	Gliwicí
příslušníky	příslušník	k1gMnPc7	příslušník
Abwehru	Abwehra	k1gFnSc4	Abwehra
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
uniformách	uniforma	k1gFnPc6	uniforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
jejich	jejich	k3xOp3gFnSc4	jejich
invazi	invaze	k1gFnSc4	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
nebyla	být	k5eNaImAgFnS	být
početně	početně	k6eAd1	početně
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
slabší	slabý	k2eAgNnSc4d2	slabší
než	než	k8xS	než
Wehrmacht	wehrmacht	k1gFnSc2	wehrmacht
<g/>
,	,	kIx,	,
svojí	svůj	k3xOyFgFnSc7	svůj
úrovní	úroveň	k1gFnSc7	úroveň
výzbroje	výzbroj	k1gFnSc2	výzbroj
a	a	k8xC	a
výstroje	výstroj	k1gFnSc2	výstroj
nepředstavovala	představovat	k5eNaImAgFnS	představovat
pro	pro	k7c4	pro
útočníka	útočník	k1gMnSc4	útočník
rovnocenného	rovnocenný	k2eAgMnSc4d1	rovnocenný
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dřívějšími	dřívější	k2eAgFnPc7d1	dřívější
zárukami	záruka	k1gFnPc7	záruka
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
po	po	k7c6	po
marném	marný	k2eAgNnSc6d1	marné
vypršení	vypršení	k1gNnSc6	vypršení
ultimáta	ultimátum	k1gNnSc2	ultimátum
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc3	září
Německu	Německo	k1gNnSc3	Německo
válku	válek	k1gInSc2	válek
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
vojenský	vojenský	k2eAgInSc1d1	vojenský
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
bombardováním	bombardování	k1gNnSc7	bombardování
města	město	k1gNnSc2	město
Wieluń	Wieluń	k1gFnSc2	Wieluń
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
proti	proti	k7c3	proti
polským	polský	k2eAgInPc3d1	polský
vojenským	vojenský	k2eAgInPc3d1	vojenský
a	a	k8xC	a
komunikačním	komunikační	k2eAgInPc3d1	komunikační
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
dnech	den	k1gInPc6	den
bojů	boj	k1gInPc2	boj
prolomila	prolomit	k5eAaPmAgFnS	prolomit
dvě	dva	k4xCgNnPc4	dva
velká	velký	k2eAgNnPc4d1	velké
německá	německý	k2eAgNnPc4d1	německé
vojenská	vojenský	k2eAgNnPc4d1	vojenské
uskupení	uskupení	k1gNnPc4	uskupení
<g/>
,	,	kIx,	,
útočící	útočící	k2eAgFnPc4d1	útočící
z	z	k7c2	z
Pomořanska	Pomořansko	k1gNnSc2	Pomořansko
a	a	k8xC	a
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
polskou	polský	k2eAgFnSc4d1	polská
obrannou	obranný	k2eAgFnSc4d1	obranná
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnSc2d1	pohybující
německé	německý	k2eAgFnSc2d1	německá
pancéřové	pancéřový	k2eAgFnSc2d1	pancéřová
a	a	k8xC	a
motorizované	motorizovaný	k2eAgFnSc2d1	motorizovaná
divize	divize	k1gFnSc2	divize
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
vstříc	vstříc	k7c3	vstříc
Varšavě	Varšava	k1gFnSc3	Varšava
a	a	k8xC	a
Brestu	Brest	k1gInSc2	Brest
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
jim	on	k3xPp3gMnPc3	on
těžkopádná	těžkopádný	k2eAgFnSc1d1	těžkopádná
polská	polský	k2eAgFnSc1d1	polská
pěchota	pěchota	k1gFnSc1	pěchota
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
novináři	novinář	k1gMnPc1	novinář
použili	použít	k5eAaPmAgMnP	použít
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
tohoto	tento	k3xDgInSc2	tento
nového	nový	k2eAgInSc2d1	nový
způsobu	způsob	k1gInSc2	způsob
vedení	vedení	k1gNnSc2	vedení
boje	boj	k1gInSc2	boj
termínu	termín	k1gInSc2	termín
Blitzkrieg	Blitzkriega	k1gFnPc2	Blitzkriega
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
blesková	bleskový	k2eAgFnSc1d1	blesková
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
8	[number]	k4	8
<g/>
.	.	kIx.	.
až	až	k9	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
Varšavě	Varšava	k1gFnSc3	Varšava
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
polské	polský	k2eAgFnSc2d1	polská
divize	divize	k1gFnSc2	divize
sevřené	sevřený	k2eAgFnSc2d1	sevřená
v	v	k7c6	v
Poznani	Poznaň	k1gFnSc6	Poznaň
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
podnikly	podniknout	k5eAaPmAgFnP	podniknout
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
překročila	překročit	k5eAaPmAgFnS	překročit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
polské	polský	k2eAgFnSc2d1	polská
východní	východní	k2eAgFnSc2d1	východní
hranice	hranice	k1gFnSc2	hranice
a	a	k8xC	a
v	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
Varšava	Varšava	k1gFnSc1	Varšava
zcela	zcela	k6eAd1	zcela
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
jedenácti	jedenáct	k4xCc6	jedenáct
dnech	den	k1gInPc6	den
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
dělostřeleckého	dělostřelecký	k2eAgNnSc2d1	dělostřelecké
a	a	k8xC	a
leteckého	letecký	k2eAgNnSc2d1	letecké
bombardování	bombardování	k1gNnSc2	bombardování
obležené	obležený	k2eAgNnSc4d1	obležené
polské	polský	k2eAgNnSc4d1	polské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
kapsy	kapsa	k1gFnSc2	kapsa
vytrvávaly	vytrvávat	k5eAaImAgFnP	vytrvávat
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
skončil	skončit	k5eAaPmAgInS	skončit
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
završení	završení	k1gNnSc6	završení
bojů	boj	k1gInPc2	boj
si	se	k3xPyFc3	se
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Sověti	Sovět	k1gMnPc1	Sovět
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
svoji	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
<g/>
:	:	kIx,	:
západní	západní	k2eAgNnPc4d1	západní
území	území	k1gNnPc4	území
Polska	Polsko	k1gNnSc2	Polsko
byla	být	k5eAaImAgNnP	být
začleněna	začlenit	k5eAaPmNgNnP	začlenit
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
východní	východní	k2eAgNnSc1d1	východní
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
Polsko	Polsko	k1gNnSc1	Polsko
bylo	být	k5eAaImAgNnS	být
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
v	v	k7c4	v
Generální	generální	k2eAgNnSc4d1	generální
gouvernement	gouvernement	k1gNnSc4	gouvernement
okupovaný	okupovaný	k2eAgMnSc1d1	okupovaný
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
represálie	represálie	k1gFnPc1	represálie
proti	proti	k7c3	proti
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Probíhaly	probíhat	k5eAaImAgFnP	probíhat
deportace	deportace	k1gFnPc1	deportace
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
popravy	poprava	k1gFnPc4	poprava
polské	polský	k2eAgFnSc2d1	polská
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
teroru	teror	k1gInSc6	teror
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
především	především	k6eAd1	především
Himmlerovy	Himmlerův	k2eAgInPc1d1	Himmlerův
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
SS-Totenkopfverbände	SS-Totenkopfverbänd	k1gInSc5	SS-Totenkopfverbänd
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
nemilosrdné	milosrdný	k2eNgFnPc4d1	nemilosrdná
metody	metoda	k1gFnPc4	metoda
uplatňovali	uplatňovat	k5eAaImAgMnP	uplatňovat
Sověti	Sovět	k1gMnPc1	Sovět
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
22	[number]	k4	22
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
důstojníků	důstojník	k1gMnPc2	důstojník
zajatých	zajatá	k1gFnPc2	zajatá
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
příkaz	příkaz	k1gInSc4	příkaz
popraveno	popraven	k2eAgNnSc1d1	popraveno
NKVD	NKVD	kA	NKVD
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
katyňském	katyňský	k2eAgInSc6d1	katyňský
masakru	masakr	k1gInSc6	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zimní	zimní	k2eAgFnSc1d1	zimní
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
nátlaku	nátlak	k1gInSc2	nátlak
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
Finsko	Finsko	k1gNnSc1	Finsko
sovětské	sovětský	k2eAgInPc1d1	sovětský
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
odstoupení	odstoupení	k1gNnSc4	odstoupení
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
napadeno	napaden	k2eAgNnSc1d1	napadeno
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
drtivé	drtivý	k2eAgFnSc3d1	drtivá
sovětské	sovětský	k2eAgFnSc3d1	sovětská
převaze	převaha	k1gFnSc3	převaha
očekával	očekávat	k5eAaImAgMnS	očekávat
Stalin	Stalin	k1gMnSc1	Stalin
rychlé	rychlý	k2eAgInPc4d1	rychlý
a	a	k8xC	a
snadné	snadný	k2eAgNnSc4d1	snadné
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Sověti	Sovět	k1gMnPc1	Sovět
Finy	Fina	k1gFnSc2	Fina
hrubě	hrubě	k6eAd1	hrubě
podcenili	podcenit	k5eAaPmAgMnP	podcenit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obratné	obratný	k2eAgFnSc3d1	obratná
taktice	taktika	k1gFnSc3	taktika
dokázala	dokázat	k5eAaPmAgFnS	dokázat
finská	finský	k2eAgFnSc1d1	finská
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
maršálem	maršál	k1gMnSc7	maršál
Mannerheimem	Mannerheim	k1gMnSc7	Mannerheim
zadržovat	zadržovat	k5eAaImF	zadržovat
Sověty	Sovět	k1gMnPc7	Sovět
až	až	k6eAd1	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
velení	velení	k1gNnSc1	velení
bylo	být	k5eAaImAgNnS	být
zdecimováno	zdecimovat	k5eAaPmNgNnS	zdecimovat
stalinskými	stalinský	k2eAgFnPc7d1	stalinská
čistkami	čistka	k1gFnPc7	čistka
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
200	[number]	k4	200
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
finské	finský	k2eAgFnSc2d1	finská
ztráty	ztráta	k1gFnSc2	ztráta
činily	činit	k5eAaImAgInP	činit
25	[number]	k4	25
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
operaci	operace	k1gFnSc4	operace
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
podepsána	podepsán	k2eAgFnSc1d1	podepsána
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
muselo	muset	k5eAaImAgNnS	muset
Finsko	Finsko	k1gNnSc4	Finsko
odstoupit	odstoupit	k5eAaPmF	odstoupit
Sovětům	Sovět	k1gMnPc3	Sovět
část	část	k1gFnSc4	část
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
poznatky	poznatek	k1gInPc1	poznatek
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
přivedly	přivést	k5eAaPmAgFnP	přivést
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
vážnou	vážný	k2eAgFnSc7d1	vážná
překážkou	překážka	k1gFnSc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
sovětského	sovětský	k2eAgInSc2d1	sovětský
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
Finové	Fin	k1gMnPc1	Fin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
pokračovací	pokračovací	k2eAgFnSc6d1	pokračovací
válce	válka	k1gFnSc6	válka
pokusili	pokusit	k5eAaPmAgMnP	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
ztracená	ztracený	k2eAgNnPc1d1	ztracené
území	území	k1gNnPc1	území
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
Finové	Fin	k1gMnPc1	Fin
spojenci	spojenec	k1gMnPc7	spojenec
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
zachránili	zachránit	k5eAaPmAgMnP	zachránit
si	se	k3xPyFc3	se
svoje	svůj	k3xOyFgMnPc4	svůj
židovské	židovský	k2eAgMnPc4d1	židovský
spoluobčany	spoluobčan	k1gMnPc4	spoluobčan
před	před	k7c7	před
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
otázce	otázka	k1gFnSc6	otázka
se	se	k3xPyFc4	se
finská	finský	k2eAgFnSc1d1	finská
vláda	vláda	k1gFnSc1	vláda
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
vůbec	vůbec	k9	vůbec
bavit	bavit	k5eAaImF	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Operace	operace	k1gFnSc1	operace
Weserübung	Weserübung	k1gInSc1	Weserübung
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
německého	německý	k2eAgNnSc2d1	německé
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
Kriegsmarine	Kriegsmarin	k1gInSc5	Kriegsmarin
<g/>
)	)	kIx)	)
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1939	[number]	k4	1939
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
okupaci	okupace	k1gFnSc3	okupace
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
přístavy	přístav	k1gInPc1	přístav
představovaly	představovat	k5eAaImAgInP	představovat
optimální	optimální	k2eAgFnSc4d1	optimální
základny	základna	k1gFnPc4	základna
pro	pro	k7c4	pro
německé	německý	k2eAgFnPc4d1	německá
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
Hitler	Hitler	k1gMnSc1	Hitler
projevil	projevit	k5eAaPmAgMnS	projevit
souhlas	souhlas	k1gInSc4	souhlas
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
plán	plán	k1gInSc1	plán
Unternehmen	Unternehmen	k2eAgInSc4d1	Unternehmen
Weserübung	Weserübung	k1gInSc4	Weserübung
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
Norsko	Norsko	k1gNnSc1	Norsko
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
po	po	k7c6	po
souběžném	souběžný	k2eAgNnSc6d1	souběžné
vylodění	vylodění	k1gNnSc6	vylodění
německých	německý	k2eAgFnPc2d1	německá
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
osmi	osm	k4xCc6	osm
největších	veliký	k2eAgInPc6d3	veliký
přístavech	přístav	k1gInPc6	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
také	také	k9	také
okupaci	okupace	k1gFnSc4	okupace
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
letecké	letecký	k2eAgFnPc4d1	letecká
základny	základna	k1gFnPc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
Hitlerově	Hitlerův	k2eAgNnSc6d1	Hitlerovo
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
sehrála	sehrát	k5eAaPmAgFnS	sehrát
rovněž	rovněž	k9	rovněž
důležitost	důležitost	k1gFnSc4	důležitost
norského	norský	k2eAgInSc2d1	norský
přístavu	přístav	k1gInSc2	přístav
Narvik	Narvik	k1gMnSc1	Narvik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
překladiště	překladiště	k1gNnSc4	překladiště
při	při	k7c6	při
dovozu	dovoz	k1gInSc6	dovoz
švédské	švédský	k2eAgFnSc2d1	švédská
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
dobře	dobře	k6eAd1	dobře
vědomi	vědom	k2eAgMnPc1d1	vědom
i	i	k9	i
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chystali	chystat	k5eAaImAgMnP	chystat
vlastní	vlastní	k2eAgInSc4d1	vlastní
výsadek	výsadek	k1gInSc4	výsadek
v	v	k7c6	v
Narviku	Narvik	k1gInSc6	Narvik
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
hodlali	hodlat	k5eAaImAgMnP	hodlat
podpořit	podpořit	k5eAaPmF	podpořit
Finy	Fina	k1gFnPc4	Fina
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
upuštěno	upustit	k5eAaPmNgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Britové	Brit	k1gMnPc1	Brit
zaminovávání	zaminovávání	k1gNnSc2	zaminovávání
norských	norský	k2eAgFnPc2d1	norská
výsostných	výsostný	k2eAgFnPc2d1	výsostná
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
čímž	což	k3yQnSc7	což
však	však	k9	však
podnítili	podnítit	k5eAaPmAgMnP	podnítit
německý	německý	k2eAgInSc4d1	německý
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
odporu	odpor	k1gInSc6	odpor
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dánská	dánský	k2eAgFnSc1d1	dánská
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
si	se	k3xPyFc3	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
relativní	relativní	k2eAgFnSc4d1	relativní
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
kolaborace	kolaborace	k1gFnSc2	kolaborace
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Safari	safari	k1gNnSc4	safari
Němci	Němec	k1gMnPc1	Němec
převzali	převzít	k5eAaPmAgMnP	převzít
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
bylo	být	k5eAaImAgNnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
okupovanou	okupovaný	k2eAgFnSc7d1	okupovaná
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
zachránila	zachránit	k5eAaPmAgFnS	zachránit
před	před	k7c7	před
nacistickým	nacistický	k2eAgInSc7d1	nacistický
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
asi	asi	k9	asi
sedmi	sedm	k4xCc2	sedm
tisíc	tisíc	k4xCgInSc4	tisíc
židů	žid	k1gMnPc2	žid
přešla	přejít	k5eAaPmAgFnS	přejít
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
německám	německat	k5eAaPmIp1nS	německat
zátahem	zátah	k1gInSc7	zátah
na	na	k7c4	na
dánské	dánský	k2eAgMnPc4d1	dánský
židy	žid	k1gMnPc4	žid
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
do	do	k7c2	do
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Norsku	Norsko	k1gNnSc6	Norsko
narazili	narazit	k5eAaPmAgMnP	narazit
Němci	Němec	k1gMnPc1	Němec
na	na	k7c4	na
houževnatý	houževnatý	k2eAgInSc4d1	houževnatý
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vylodění	vylodění	k1gNnSc2	vylodění
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Osla	Oslo	k1gNnSc2	Oslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
teprve	teprve	k6eAd1	teprve
německými	německý	k2eAgMnPc7d1	německý
výsadkáři	výsadkář	k1gMnPc7	výsadkář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Narviku	Narvikum	k1gNnSc6	Narvikum
se	se	k3xPyFc4	se
Němcům	Němec	k1gMnPc3	Němec
podařilo	podařit	k5eAaPmAgNnS	podařit
obsadit	obsadit	k5eAaPmF	obsadit
město	město	k1gNnSc1	město
i	i	k8xC	i
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
však	však	k9	však
byli	být	k5eAaImAgMnP	být
obklíčeni	obklíčit	k5eAaPmNgMnP	obklíčit
takřka	takřka	k6eAd1	takřka
pětinásobnou	pětinásobný	k2eAgFnSc7d1	pětinásobná
přesilou	přesila	k1gFnSc7	přesila
Britů	Brit	k1gMnPc2	Brit
<g/>
,	,	kIx,	,
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
Norů	Nor	k1gMnPc2	Nor
<g/>
,	,	kIx,	,
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
Královským	královský	k2eAgNnSc7d1	královské
námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Navy	Navy	k?	Navy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Narvik	Narvik	k1gInSc4	Narvik
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
nuceni	nutit	k5eAaImNgMnP	nutit
město	město	k1gNnSc4	město
vyklidit	vyklidit	k5eAaPmF	vyklidit
a	a	k8xC	a
ustoupit	ustoupit	k5eAaPmF	ustoupit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vývoj	vývoj	k1gInSc1	vývoj
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
přiměl	přimět	k5eAaPmAgMnS	přimět
Spojence	spojenec	k1gMnSc2	spojenec
počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
jejich	jejich	k3xOp3gInPc2	jejich
oddílů	oddíl	k1gInPc2	oddíl
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zde	zde	k6eAd1	zde
později	pozdě	k6eAd2	pozdě
instalovali	instalovat	k5eAaBmAgMnP	instalovat
loutkovou	loutkový	k2eAgFnSc4d1	loutková
vládu	vláda	k1gFnSc4	vláda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
norského	norský	k2eAgMnSc2d1	norský
zrádce	zrádce	k1gMnSc2	zrádce
Vidkuna	Vidkuna	k1gFnSc1	Vidkuna
Quislinga	quisling	k1gMnSc2	quisling
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
války	válka	k1gFnSc2	válka
Německu	Německo	k1gNnSc3	Německo
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Francouzi	Francouz	k1gMnPc1	Francouz
omezenou	omezený	k2eAgFnSc4d1	omezená
a	a	k8xC	a
spíše	spíše	k9	spíše
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
do	do	k7c2	do
Sárska	Sársko	k1gNnSc2	Sársko
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
klidné	klidný	k2eAgNnSc1d1	klidné
období	období	k1gNnSc1	období
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
Sitzkrieg	Sitzkrieg	k1gInSc1	Sitzkrieg
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
vsedě	vsedě	k6eAd1	vsedě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
podivná	podivný	k2eAgFnSc1d1	podivná
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kvůli	kvůli	k7c3	kvůli
nepříznivému	příznivý	k2eNgNnSc3d1	nepříznivé
podzimnímu	podzimní	k2eAgNnSc3d1	podzimní
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
neočekávaně	očekávaně	k6eNd1	očekávaně
vysokým	vysoký	k2eAgFnPc3d1	vysoká
německým	německý	k2eAgFnPc3d1	německá
ztrátám	ztráta	k1gFnPc3	ztráta
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
francouzské	francouzský	k2eAgFnSc2d1	francouzská
pasivity	pasivita	k1gFnSc2	pasivita
spočívaly	spočívat	k5eAaImAgFnP	spočívat
v	v	k7c6	v
defenzivní	defenzivní	k2eAgFnSc6d1	defenzivní
mentalitě	mentalita	k1gFnSc6	mentalita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
duchu	duch	k1gMnSc6	duch
se	se	k3xPyFc4	se
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
silně	silně	k6eAd1	silně
opevněnou	opevněný	k2eAgFnSc4d1	opevněná
pohraniční	pohraniční	k2eAgFnSc4d1	pohraniční
Maginotovu	Maginotův	k2eAgFnSc4d1	Maginotova
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
podél	podél	k7c2	podél
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
října	říjen	k1gInSc2	říjen
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Hitler	Hitler	k1gMnSc1	Hitler
Vrchní	vrchní	k1gFnSc2	vrchní
velení	velení	k1gNnSc2	velení
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Oberkommando	Oberkommanda	k1gFnSc5	Oberkommanda
des	des	k1gNnPc6	des
Heeres	Heeres	k1gInSc4	Heeres
<g/>
)	)	kIx)	)
vypracováním	vypracování	k1gNnSc7	vypracování
plánu	plán	k1gInSc2	plán
pro	pro	k7c4	pro
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Fall	Fallum	k1gNnPc2	Fallum
Gelb	Gelba	k1gFnPc2	Gelba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
plán	plán	k1gInSc1	plán
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
státům	stát	k1gInPc3	stát
Beneluxu	Benelux	k1gInSc2	Benelux
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
záměru	záměra	k1gFnSc4	záměra
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
invaze	invaze	k1gFnSc1	invaze
na	na	k7c4	na
západ	západ	k1gInSc4	západ
provedena	provést	k5eAaPmNgFnS	provést
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
generál	generál	k1gMnSc1	generál
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc4	von
Manstein	Manstein	k2eAgInSc4d1	Manstein
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jádro	jádro	k1gNnSc1	jádro
obrněných	obrněný	k2eAgFnPc2d1	obrněná
sil	síla	k1gFnPc2	síla
soustředil	soustředit	k5eAaPmAgInS	soustředit
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
postupu	postup	k1gInSc3	postup
přes	přes	k7c4	přes
Ardenský	ardenský	k2eAgInSc4d1	ardenský
les	les	k1gInSc4	les
a	a	k8xC	a
řeku	řeka	k1gFnSc4	řeka
Mázu	máz	k1gInSc2	máz
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
následný	následný	k2eAgInSc1d1	následný
průlom	průlom	k1gInSc1	průlom
měl	mít	k5eAaImAgInS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
armád	armáda	k1gFnPc2	armáda
ve	v	k7c6	v
dví	dví	k1xP	dví
<g/>
.	.	kIx.	.
</s>
<s>
Ardeny	Ardeny	k1gFnPc1	Ardeny
představovaly	představovat	k5eAaImAgFnP	představovat
kopcovitý	kopcovitý	k2eAgInSc4d1	kopcovitý
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
zalesněný	zalesněný	k2eAgInSc1d1	zalesněný
terén	terén	k1gInSc1	terén
naprosto	naprosto	k6eAd1	naprosto
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Manstein	Manstein	k1gMnSc1	Manstein
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřítel	nepřítel	k1gMnSc1	nepřítel
zde	zde	k6eAd1	zde
nebude	být	k5eNaImBp3nS	být
očekávat	očekávat	k5eAaImF	očekávat
masivní	masivní	k2eAgInSc4d1	masivní
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bude	být	k5eAaImBp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
momentu	moment	k1gInSc2	moment
překvapení	překvapení	k1gNnPc2	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
válčící	válčící	k2eAgFnPc1d1	válčící
strany	strana	k1gFnPc1	strana
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
stejně	stejně	k6eAd1	stejně
početnými	početný	k2eAgFnPc7d1	početná
armádami	armáda	k1gFnPc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
měli	mít	k5eAaImAgMnP	mít
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
německá	německý	k2eAgFnSc1d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
měla	mít	k5eAaImAgFnS	mít
nad	nad	k7c7	nad
svými	svůj	k3xOyFgMnPc7	svůj
protivníky	protivník	k1gMnPc7	protivník
navrch	navrch	k6eAd1	navrch
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pružnější	pružný	k2eAgFnSc7d2	pružnější
organizací	organizace	k1gFnSc7	organizace
německých	německý	k2eAgFnPc2d1	německá
pancéřových	pancéřový	k2eAgFnPc2d1	pancéřová
sil	síla	k1gFnPc2	síla
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
faktor	faktor	k1gInSc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
útok	útok	k1gInSc1	útok
započal	započnout	k5eAaPmAgInS	započnout
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wehrmacht	wehrmacht	k1gFnSc2	wehrmacht
překročil	překročit	k5eAaPmAgMnS	překročit
hranice	hranice	k1gFnPc4	hranice
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Holandska	Holandsko	k1gNnSc2	Holandsko
a	a	k8xC	a
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
výsadkáři	výsadkář	k1gMnPc1	výsadkář
obsadili	obsadit	k5eAaPmAgMnP	obsadit
důležitá	důležitý	k2eAgNnPc4d1	důležité
letiště	letiště	k1gNnPc4	letiště
<g/>
,	,	kIx,	,
mosty	most	k1gInPc4	most
a	a	k8xC	a
strategicky	strategicky	k6eAd1	strategicky
významnou	významný	k2eAgFnSc4d1	významná
belgickou	belgický	k2eAgFnSc4d1	belgická
pevnost	pevnost	k1gFnSc4	pevnost
Eben-Emael	Eben-Emaela	k1gFnPc2	Eben-Emaela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
Beneluxu	Benelux	k1gInSc2	Benelux
postoupil	postoupit	k5eAaPmAgInS	postoupit
britský	britský	k2eAgInSc1d1	britský
expediční	expediční	k2eAgInSc1d1	expediční
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
část	část	k1gFnSc1	část
francouzských	francouzský	k2eAgFnPc2d1	francouzská
armád	armáda	k1gFnPc2	armáda
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
řeky	řeka	k1gFnSc2	řeka
Dyle	Dyl	k1gFnSc2	Dyl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hodlaly	hodlat	k5eAaImAgFnP	hodlat
zastavit	zastavit	k5eAaPmF	zastavit
německý	německý	k2eAgInSc4d1	německý
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
šly	jít	k5eAaImAgInP	jít
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
Mansteinovu	Mansteinův	k2eAgInSc3d1	Mansteinův
plánu	plán	k1gInSc3	plán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sedm	sedm	k4xCc1	sedm
z	z	k7c2	z
deseti	deset	k4xCc2	deset
německých	německý	k2eAgFnPc2d1	německá
pancéřových	pancéřový	k2eAgFnPc2d1	pancéřová
divizí	divize	k1gFnPc2	divize
valilo	valit	k5eAaImAgNnS	valit
nikým	nikdo	k3yNnSc7	nikdo
nezpozorováno	zpozorovat	k5eNaPmNgNnS	zpozorovat
ardenskými	ardenský	k2eAgInPc7d1	ardenský
průsmyky	průsmyk	k1gInPc7	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
jen	jen	k9	jen
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
dorazily	dorazit	k5eAaPmAgInP	dorazit
německé	německý	k2eAgInPc1d1	německý
tanky	tank	k1gInPc1	tank
vedené	vedený	k2eAgFnSc2d1	vedená
generálem	generál	k1gMnSc7	generál
Guderianem	Guderian	k1gMnSc7	Guderian
u	u	k7c2	u
Sedanu	sedan	k1gInSc2	sedan
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Máze	máz	k1gInSc6	máz
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
po	po	k7c6	po
urputném	urputný	k2eAgInSc6d1	urputný
boji	boj	k1gInSc6	boj
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
podnikla	podniknout	k5eAaPmAgFnS	podniknout
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
ničivý	ničivý	k2eAgMnSc1d1	ničivý
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Rotterdam	Rotterdam	k1gInSc4	Rotterdam
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Holandsko	Holandsko	k1gNnSc1	Holandsko
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
padl	padnout	k5eAaImAgInS	padnout
také	také	k9	také
Brusel	Brusel	k1gInSc1	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
Mázy	máz	k1gInPc4	máz
se	se	k3xPyFc4	se
německé	německý	k2eAgFnPc1d1	německá
tankové	tankový	k2eAgFnPc1d1	tanková
divize	divize	k1gFnPc1	divize
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
ústí	ústí	k1gNnSc4	ústí
řeky	řeka	k1gFnSc2	řeka
Sommy	Somma	k1gFnSc2	Somma
do	do	k7c2	do
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
završen	završen	k2eAgMnSc1d1	završen
Mansteinův	Mansteinův	k2eAgMnSc1d1	Mansteinův
"	"	kIx"	"
<g/>
sek	sek	k1gInSc1	sek
srpem	srp	k1gInSc7	srp
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sichelschnitt	Sichelschnitt	k1gInSc1	Sichelschnitt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
bok	bok	k1gInSc4	bok
německých	německý	k2eAgFnPc2d1	německá
divizí	divize	k1gFnPc2	divize
u	u	k7c2	u
Arrasu	Arras	k1gInSc2	Arras
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byli	být	k5eAaImAgMnP	být
odraženi	odrazit	k5eAaPmNgMnP	odrazit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
obklíčení	obklíčení	k1gNnSc6	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vedl	vést	k5eAaImAgInS	vést
neustálý	neustálý	k2eAgInSc1d1	neustálý
německý	německý	k2eAgInSc1d1	německý
postup	postup	k1gInSc1	postup
k	k	k7c3	k
sevření	sevření	k1gNnSc3	sevření
britských	britský	k2eAgFnPc2d1	britská
a	a	k8xC	a
francouzských	francouzský	k2eAgFnPc2d1	francouzská
jednotek	jednotka	k1gFnPc2	jednotka
u	u	k7c2	u
Dunkerque	Dunkerqu	k1gFnSc2	Dunkerqu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
obklíčených	obklíčený	k2eAgNnPc2d1	obklíčené
vojsk	vojsko	k1gNnPc2	vojsko
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
zahájena	zahájen	k2eAgFnSc1d1	zahájena
operace	operace	k1gFnSc1	operace
Dynamo	dynamo	k1gNnSc1	dynamo
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
nepřetržité	přetržitý	k2eNgInPc4d1	nepřetržitý
německé	německý	k2eAgInPc4d1	německý
nálety	nálet	k1gInPc4	nálet
britským	britský	k2eAgInPc3d1	britský
vojenským	vojenský	k2eAgInPc3d1	vojenský
i	i	k8xC	i
civilním	civilní	k2eAgNnPc3d1	civilní
plavidlům	plavidlo	k1gNnPc3	plavidlo
podařilo	podařit	k5eAaPmAgNnS	podařit
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
evakuovat	evakuovat	k5eAaBmF	evakuovat
z	z	k7c2	z
Dunkerque	Dunkerqu	k1gInSc2	Dunkerqu
takřka	takřka	k6eAd1	takřka
340	[number]	k4	340
000	[number]	k4	000
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Evakuován	evakuován	k2eAgInSc1d1	evakuován
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
expediční	expediční	k2eAgInSc1d1	expediční
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
110	[number]	k4	110
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
záchraně	záchrana	k1gFnSc3	záchrana
Spojenců	spojenec	k1gMnPc2	spojenec
přispěl	přispět	k5eAaPmAgMnS	přispět
i	i	k9	i
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
rozkaz	rozkaz	k1gInSc4	rozkaz
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
ze	z	k7c2	z
zcela	zcela	k6eAd1	zcela
nepochopitelných	pochopitelný	k2eNgInPc2d1	nepochopitelný
důvodů	důvod	k1gInPc2	důvod
zakázal	zakázat	k5eAaPmAgInS	zakázat
svým	svůj	k3xOyFgInPc3	svůj
tankům	tank	k1gInPc3	tank
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
obnovili	obnovit	k5eAaPmAgMnP	obnovit
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
ofenzívou	ofenzíva	k1gFnSc7	ofenzíva
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Sommě	Somma	k1gFnSc6	Somma
a	a	k8xC	a
Aisně	Aisna	k1gFnSc6	Aisna
útok	útok	k1gInSc1	útok
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
již	již	k6eAd1	již
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
dnech	den	k1gInPc6	den
překročili	překročit	k5eAaPmAgMnP	překročit
řeku	řeka	k1gFnSc4	řeka
Seinu	Seina	k1gFnSc4	Seina
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Německa	Německo	k1gNnSc2	Německo
i	i	k8xC	i
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
Italů	Ital	k1gMnPc2	Ital
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
skončil	skončit	k5eAaPmAgInS	skončit
ale	ale	k9	ale
navzdory	navzdory	k7c3	navzdory
italské	italský	k2eAgFnSc3d1	italská
převaze	převaha	k1gFnSc3	převaha
pohromou	pohroma	k1gFnSc7	pohroma
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
triumfující	triumfující	k2eAgFnSc4d1	triumfující
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
prolomena	prolomen	k2eAgFnSc1d1	prolomena
Maginotova	Maginotův	k2eAgFnSc1d1	Maginotova
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
kritických	kritický	k2eAgFnPc2d1	kritická
okolností	okolnost	k1gFnPc2	okolnost
byl	být	k5eAaImAgInS	být
francouzským	francouzský	k2eAgMnSc7d1	francouzský
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
Philippe	Philipp	k1gInSc5	Philipp
Pétain	Pétain	k1gMnSc1	Pétain
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zahájil	zahájit	k5eAaPmAgMnS	zahájit
okamžitě	okamžitě	k6eAd1	okamžitě
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
železničním	železniční	k2eAgInSc6d1	železniční
vagóně	vagón	k1gInSc6	vagón
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
Němci	Němec	k1gMnPc1	Němec
podepsali	podepsat	k5eAaPmAgMnP	podepsat
příměří	příměří	k1gNnSc4	příměří
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
poblahopřál	poblahopřát	k5eAaPmAgInS	poblahopřát
Německu	Německo	k1gNnSc3	Německo
k	k	k7c3	k
brilantnímu	brilantní	k2eAgNnSc3d1	brilantní
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Blitzkrieg	Blitzkrieg	k1gInSc1	Blitzkrieg
na	na	k7c6	na
západě	západ	k1gInSc6	západ
tak	tak	k6eAd1	tak
skončil	skončit	k5eAaPmAgMnS	skončit
po	po	k7c6	po
pouhých	pouhý	k2eAgInPc6d1	pouhý
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
příměří	příměří	k1gNnPc2	příměří
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
a	a	k8xC	a
západ	západ	k1gInSc1	západ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
důležité	důležitý	k2eAgFnPc1d1	důležitá
letecké	letecký	k2eAgFnPc1d1	letecká
základny	základna	k1gFnPc1	základna
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
a	a	k8xC	a
přístavy	přístav	k1gInPc1	přístav
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
okupovány	okupovat	k5eAaBmNgInP	okupovat
Wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Francie	Francie	k1gFnSc1	Francie
setrvala	setrvat	k5eAaPmAgFnS	setrvat
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Pétainovy	Pétainův	k2eAgFnSc2d1	Pétainova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Vichy	Vicha	k1gFnSc2	Vicha
<g/>
.	.	kIx.	.
</s>
<s>
Pétain	Pétain	k1gInSc1	Pétain
zde	zde	k6eAd1	zde
nastolil	nastolit	k5eAaPmAgInS	nastolit
kolaborantský	kolaborantský	k2eAgInSc1d1	kolaborantský
<g/>
,	,	kIx,	,
loutkový	loutkový	k2eAgInSc1d1	loutkový
vichistický	vichistický	k2eAgInSc1d1	vichistický
režim	režim	k1gInSc1	režim
zcela	zcela	k6eAd1	zcela
loajální	loajální	k2eAgMnSc1d1	loajální
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
exilového	exilový	k2eAgNnSc2d1	exilové
hnutí	hnutí	k1gNnSc2	hnutí
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
francouzských	francouzský	k2eAgFnPc2d1	francouzská
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
Forces	Forces	k1gMnSc1	Forces
françaises	françaises	k1gMnSc1	françaises
libres	libres	k1gMnSc1	libres
<g/>
)	)	kIx)	)
postavil	postavit	k5eAaPmAgMnS	postavit
služebně	služebně	k6eAd1	služebně
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
generál	generál	k1gMnSc1	generál
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
flota	flota	k1gFnSc1	flota
zakotvená	zakotvený	k2eAgFnSc1d1	zakotvená
u	u	k7c2	u
alžírského	alžírský	k2eAgInSc2d1	alžírský
Oranu	Oran	k1gInSc2	Oran
byla	být	k5eAaImAgFnS	být
však	však	k9	však
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
napadena	napadnout	k5eAaPmNgFnS	napadnout
britským	britský	k2eAgNnSc7d1	Britské
námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepadla	padnout	k5eNaPmAgFnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
ovládal	ovládat	k5eAaImAgMnS	ovládat
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1940	[number]	k4	1940
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
spojenců	spojenec	k1gMnPc2	spojenec
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
zbývajícím	zbývající	k2eAgMnSc7d1	zbývající
nepřítelem	nepřítel	k1gMnSc7	nepřítel
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
osamocená	osamocený	k2eAgFnSc1d1	osamocená
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
nový	nový	k2eAgMnSc1d1	nový
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Winston	Winston	k1gInSc4	Winston
Churchill	Churchill	k1gMnSc1	Churchill
sliboval	slibovat	k5eAaImAgMnS	slibovat
Britům	Brit	k1gMnPc3	Brit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
burcujících	burcující	k2eAgInPc6d1	burcující
projevech	projev	k1gInPc6	projev
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
pot	pot	k1gInSc1	pot
a	a	k8xC	a
slzy	slza	k1gFnPc1	slza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
britská	britský	k2eAgFnSc1d1	britská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
překážkou	překážka	k1gFnSc7	překážka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bránila	bránit	k5eAaImAgFnS	bránit
Hitlerovým	Hitlerův	k2eAgFnPc3d1	Hitlerova
armádám	armáda	k1gFnPc3	armáda
v	v	k7c6	v
podrobení	podrobení	k1gNnSc6	podrobení
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
jež	jenž	k3xRgFnSc1	jenž
mohla	moct	k5eAaImAgFnS	moct
zmařit	zmařit	k5eAaPmF	zmařit
chystanou	chystaný	k2eAgFnSc4d1	chystaná
invazi	invaze	k1gFnSc4	invaze
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Seelöwe	Seelöw	k1gFnSc2	Seelöw
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nyní	nyní	k6eAd1	nyní
britské	britský	k2eAgNnSc1d1	Britské
letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
leteckým	letecký	k2eAgMnSc7d1	letecký
maršálem	maršál	k1gMnSc7	maršál
Hughem	Hugh	k1gInSc7	Hugh
Dowdingem	Dowding	k1gInSc7	Dowding
<g/>
.	.	kIx.	.
</s>
<s>
Zlomit	zlomit	k5eAaPmF	zlomit
sílu	síla	k1gFnSc4	síla
RAF	raf	k0	raf
se	se	k3xPyFc4	se
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
bitvě	bitva	k1gFnSc6	bitva
pokusila	pokusit	k5eAaPmAgFnS	pokusit
německá	německý	k2eAgFnSc1d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc4	Göring
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
rovněž	rovněž	k9	rovněž
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Británii	Británie	k1gFnSc4	Británie
izolují	izolovat	k5eAaBmIp3nP	izolovat
a	a	k8xC	a
vyhladoví	vyhladovit	k5eAaPmIp3nP	vyhladovit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svých	svůj	k3xOyFgFnPc2	svůj
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
operujících	operující	k2eAgFnPc2d1	operující
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
letecká	letecký	k2eAgFnSc1d1	letecká
bitva	bitva	k1gFnSc1	bitva
začala	začít	k5eAaPmAgFnS	začít
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
britské	britský	k2eAgInPc4d1	britský
konvoje	konvoj	k1gInPc4	konvoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
kanál	kanál	k1gInSc4	kanál
(	(	kIx(	(
<g/>
Kanalkampf	Kanalkampf	k1gInSc1	Kanalkampf
<g/>
)	)	kIx)	)
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
z	z	k7c2	z
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
orla	orel	k1gMnSc2	orel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Adlertag	Adlertag	k1gInSc1	Adlertag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
nejprve	nejprve	k6eAd1	nejprve
částečně	částečně	k6eAd1	částečně
neutralizovali	neutralizovat	k5eAaBmAgMnP	neutralizovat
britský	britský	k2eAgInSc4d1	britský
radarový	radarový	k2eAgInSc4d1	radarový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
proti	proti	k7c3	proti
přístavům	přístav	k1gInPc3	přístav
a	a	k8xC	a
vojenským	vojenský	k2eAgFnPc3d1	vojenská
a	a	k8xC	a
leteckým	letecký	k2eAgNnPc3d1	letecké
zařízením	zařízení	k1gNnPc3	zařízení
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
boje	boj	k1gInSc2	boj
utrpěly	utrpět	k5eAaPmAgFnP	utrpět
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ztráty	ztráta	k1gFnSc2	ztráta
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
proti	proti	k7c3	proti
letištím	letiště	k1gNnPc3	letiště
RAF	raf	k0	raf
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
a	a	k8xC	a
leteckým	letecký	k2eAgFnPc3d1	letecká
továrnám	továrna	k1gFnPc3	továrna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
etapě	etapa	k1gFnSc6	etapa
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Němci	Němec	k1gMnPc1	Němec
denní	denní	k2eAgInSc4d1	denní
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
velká	velký	k2eAgNnPc4d1	velké
britská	britský	k2eAgNnPc4d1	Britské
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
Coventry	Coventr	k1gInPc4	Coventr
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zlomit	zlomit	k5eAaPmF	zlomit
morálku	morálka	k1gFnSc4	morálka
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
Britové	Brit	k1gMnPc1	Brit
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
odrazili	odrazit	k5eAaPmAgMnP	odrazit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
německý	německý	k2eAgInSc4d1	německý
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Hitler	Hitler	k1gMnSc1	Hitler
nařídil	nařídit	k5eAaPmAgMnS	nařídit
odložit	odložit	k5eAaPmF	odložit
vylodění	vylodění	k1gNnSc4	vylodění
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
Luftwaffe	Luftwaff	k1gMnSc5	Luftwaff
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c4	v
bombardování	bombardování	k1gNnSc4	bombardování
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Blitz	Blitz	k1gInSc1	Blitz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
zničených	zničený	k2eAgNnPc2d1	zničené
letadel	letadlo	k1gNnPc2	letadlo
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
omezovala	omezovat	k5eAaImAgFnS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
noční	noční	k2eAgInPc4d1	noční
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
i	i	k9	i
přesto	přesto	k6eAd1	přesto
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
přes	přes	k7c4	přes
40	[number]	k4	40
000	[number]	k4	000
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
usilovali	usilovat	k5eAaImAgMnP	usilovat
Němci	Němec	k1gMnPc1	Němec
o	o	k7c6	o
zničení	zničení	k1gNnSc6	zničení
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
stíhaček	stíhačka	k1gFnPc2	stíhačka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podcenili	podcenit	k5eAaPmAgMnP	podcenit
význam	význam	k1gInSc4	význam
radaru	radar	k1gInSc2	radar
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
britský	britský	k2eAgInSc1d1	britský
průmysl	průmysl	k1gInSc1	průmysl
dokázal	dokázat	k5eAaPmAgInS	dokázat
vždy	vždy	k6eAd1	vždy
plně	plně	k6eAd1	plně
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
ztráty	ztráta	k1gFnPc4	ztráta
vlastních	vlastní	k2eAgInPc2d1	vlastní
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
"	"	kIx"	"
<g/>
nemnohých	mnohý	k2eNgMnPc2d1	nemnohý
<g/>
"	"	kIx"	"
náležel	náležet	k5eAaImAgInS	náležet
kromě	kromě	k7c2	kromě
britských	britský	k2eAgMnPc2d1	britský
pilotů	pilot	k1gMnPc2	pilot
také	také	k9	také
Polákům	Polák	k1gMnPc3	Polák
<g/>
,	,	kIx,	,
Kanaďanům	Kanaďan	k1gMnPc3	Kanaďan
<g/>
,	,	kIx,	,
Novozélanďanům	Novozélanďan	k1gMnPc3	Novozélanďan
<g/>
,	,	kIx,	,
Čechoslovákům	Čechoslovák	k1gMnPc3	Čechoslovák
<g/>
,	,	kIx,	,
Belgičanům	Belgičan	k1gMnPc3	Belgičan
a	a	k8xC	a
zástupcům	zástupce	k1gMnPc3	zástupce
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Balkánské	balkánský	k2eAgNnSc1d1	balkánské
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
směřoval	směřovat	k5eAaImAgInS	směřovat
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
své	svůj	k3xOyFgFnPc4	svůj
velmocenské	velmocenský	k2eAgFnPc4d1	velmocenská
ambice	ambice	k1gFnPc4	ambice
na	na	k7c4	na
Balkánský	balkánský	k2eAgInSc4d1	balkánský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1939	[number]	k4	1939
okupovala	okupovat	k5eAaBmAgFnS	okupovat
italská	italský	k2eAgFnSc1d1	italská
vojska	vojsko	k1gNnSc2	vojsko
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	nízce	k6eAd2	nízce
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
zahájila	zahájit	k5eAaPmAgFnS	zahájit
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
Řecku	Řecko	k1gNnSc3	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
špatně	špatně	k6eAd1	špatně
vybavených	vybavený	k2eAgNnPc2d1	vybavené
a	a	k8xC	a
nedostatečně	dostatečně	k6eNd1	dostatečně
motivovaných	motivovaný	k2eAgMnPc2d1	motivovaný
italských	italský	k2eAgMnPc2d1	italský
vojáků	voják	k1gMnPc2	voják
selhala	selhat	k5eAaPmAgFnS	selhat
sotva	sotva	k6eAd1	sotva
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
přiměli	přimět	k5eAaPmAgMnP	přimět
italské	italský	k2eAgMnPc4d1	italský
agresory	agresor	k1gMnPc4	agresor
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
do	do	k7c2	do
defenzívy	defenzíva	k1gFnSc2	defenzíva
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
je	on	k3xPp3gMnPc4	on
zatlačili	zatlačit	k5eAaPmAgMnP	zatlačit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
porážky	porážka	k1gFnSc2	porážka
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnSc4	spojenec
a	a	k8xC	a
z	z	k7c2	z
obav	obava	k1gFnPc2	obava
z	z	k7c2	z
britského	britský	k2eAgInSc2d1	britský
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
německou	německý	k2eAgFnSc4d1	německá
kontrolu	kontrola	k1gFnSc4	kontrola
rumunských	rumunský	k2eAgFnPc2d1	rumunská
ropných	ropný	k2eAgFnPc2d1	ropná
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
Hitler	Hitler	k1gMnSc1	Hitler
pokyn	pokyn	k1gInSc1	pokyn
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
plánů	plán	k1gInPc2	plán
tažení	tažení	k1gNnSc1	tažení
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Marita	Marita	k1gFnSc1	Marita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
mocností	mocnost	k1gFnPc2	mocnost
Osy	osa	k1gFnSc2	osa
přidaly	přidat	k5eAaPmAgInP	přidat
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
k	k	k7c3	k
Ose	osa	k1gFnSc3	osa
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
protiněmecké	protiněmecký	k2eAgFnPc1d1	protiněmecká
demonstrace	demonstrace	k1gFnPc1	demonstrace
a	a	k8xC	a
puč	puč	k1gInSc1	puč
jugoslávských	jugoslávský	k2eAgMnPc2d1	jugoslávský
leteckých	letecký	k2eAgMnPc2d1	letecký
důstojníků	důstojník	k1gMnPc2	důstojník
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vystoupení	vystoupení	k1gNnSc4	vystoupení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
z	z	k7c2	z
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
překročily	překročit	k5eAaPmAgFnP	překročit
německé	německý	k2eAgFnPc1d1	německá
divize	divize	k1gFnPc1	divize
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
podnikla	podniknout	k5eAaPmAgFnS	podniknout
ničivý	ničivý	k2eAgInSc4d1	ničivý
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
obrana	obrana	k1gFnSc1	obrana
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
prolomena	prolomen	k2eAgFnSc1d1	prolomena
a	a	k8xC	a
už	už	k6eAd1	už
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
stanuly	stanout	k5eAaPmAgFnP	stanout
jednotky	jednotka	k1gFnPc1	jednotka
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
v	v	k7c6	v
chorvatském	chorvatský	k2eAgInSc6d1	chorvatský
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
uvítány	uvítán	k2eAgInPc1d1	uvítán
jásajícími	jásající	k2eAgInPc7d1	jásající
davy	dav	k1gInPc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
dobyly	dobýt	k5eAaPmAgFnP	dobýt
německé	německý	k2eAgFnPc1d1	německá
pancéřové	pancéřový	k2eAgFnPc1d1	pancéřová
divize	divize	k1gFnSc2	divize
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
nato	nato	k6eAd1	nato
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
přijalo	přijmout	k5eAaPmAgNnS	přijmout
jugoslávské	jugoslávský	k2eAgNnSc1d1	jugoslávské
velení	velení	k1gNnSc1	velení
svou	svůj	k3xOyFgFnSc4	svůj
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Poražená	poražený	k2eAgFnSc1d1	poražená
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
vítězné	vítězný	k2eAgInPc4d1	vítězný
státy	stát	k1gInPc4	stát
a	a	k8xC	a
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
troskách	troska	k1gFnPc6	troska
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
tzv.	tzv.	kA	tzv.
Nezávislý	závislý	k2eNgInSc4d1	nezávislý
stát	stát	k1gInSc4	stát
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ustašovců	ustašovec	k1gMnPc2	ustašovec
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
posílení	posílení	k1gNnSc1	posílení
britskými	britský	k2eAgFnPc7d1	britská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
přesunutými	přesunutý	k2eAgInPc7d1	přesunutý
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kladli	klást	k5eAaImAgMnP	klást
Němcům	Němec	k1gMnPc3	Němec
houževnatější	houževnatý	k2eAgInSc4d2	houževnatější
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
postup	postup	k1gInSc1	postup
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
ztěžoval	ztěžovat	k5eAaImAgMnS	ztěžovat
také	také	k9	také
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
hornatý	hornatý	k2eAgInSc4d1	hornatý
terén	terén	k1gInSc4	terén
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
Metaxasovy	Metaxasův	k2eAgFnSc2d1	Metaxasova
linie	linie	k1gFnSc2	linie
padlo	padnout	k5eAaImAgNnS	padnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Němců	Němec	k1gMnPc2	Němec
město	město	k1gNnSc1	město
Soluň	Soluň	k1gFnSc1	Soluň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
řecké	řecký	k2eAgFnSc2d1	řecká
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
byli	být	k5eAaImAgMnP	být
Řekové	Řek	k1gMnPc1	Řek
obklíčeni	obklíčit	k5eAaPmNgMnP	obklíčit
rovněž	rovněž	k6eAd1	rovněž
v	v	k7c6	v
Epiru	Epir	k1gInSc6	Epir
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zadržet	zadržet	k5eAaPmF	zadržet
Němce	Němec	k1gMnPc4	Němec
u	u	k7c2	u
Thermopyl	Thermopyly	k1gFnPc2	Thermopyly
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
obrana	obrana	k1gFnSc1	obrana
zdolána	zdolán	k2eAgFnSc1d1	zdolána
<g/>
,	,	kIx,	,
zahájili	zahájit	k5eAaPmAgMnP	zahájit
obojživelnou	obojživelný	k2eAgFnSc4d1	obojživelná
evakuační	evakuační	k2eAgFnSc4d1	evakuační
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
oddíly	oddíl	k1gInPc1	oddíl
Wehrmachtu	wehrmacht	k1gInSc3	wehrmacht
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
na	na	k7c4	na
Peloponés	Peloponés	k1gInSc4	Peloponés
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
balkánské	balkánský	k2eAgNnSc1d1	balkánské
tažení	tažení	k1gNnSc1	tažení
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
provedly	provést	k5eAaPmAgFnP	provést
německé	německý	k2eAgFnPc1d1	německá
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
leteckou	letecký	k2eAgFnSc4d1	letecká
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Krétu	Kréta	k1gFnSc4	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
útoku	útok	k1gInSc6	útok
však	však	k9	však
němečtí	německý	k2eAgMnPc1d1	německý
výsadkáři	výsadkář	k1gMnPc1	výsadkář
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
také	také	k9	také
zprvu	zprvu	k6eAd1	zprvu
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
dobýt	dobýt	k5eAaPmF	dobýt
žádné	žádný	k3yNgNnSc1	žádný
letiště	letiště	k1gNnSc1	letiště
potřebné	potřebný	k2eAgNnSc1d1	potřebné
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
příchodu	příchod	k1gInSc2	příchod
posil	posila	k1gFnPc2	posila
a	a	k8xC	a
k	k	k7c3	k
plynulému	plynulý	k2eAgNnSc3d1	plynulé
zásobování	zásobování	k1gNnSc3	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
koordinaci	koordinace	k1gFnSc3	koordinace
a	a	k8xC	a
komunikaci	komunikace	k1gFnSc3	komunikace
mezi	mezi	k7c7	mezi
jednotkami	jednotka	k1gFnPc7	jednotka
britského	britský	k2eAgInSc2d1	britský
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
intenzivním	intenzivní	k2eAgInPc3d1	intenzivní
útokům	útok	k1gInPc3	útok
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
se	se	k3xPyFc4	se
však	však	k9	však
Němcům	Němec	k1gMnPc3	Němec
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
svoje	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
vytlačit	vytlačit	k5eAaPmF	vytlačit
Spojence	spojenec	k1gMnSc4	spojenec
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
zakázal	zakázat	k5eAaPmAgInS	zakázat
provádění	provádění	k1gNnPc4	provádění
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
dalších	další	k2eAgFnPc2d1	další
výsadkových	výsadkový	k2eAgFnPc2d1	výsadková
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Severoafrické	severoafrický	k2eAgNnSc1d1	severoafrické
tažení	tažení	k1gNnSc1	tažení
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
160	[number]	k4	160
000	[number]	k4	000
italských	italský	k2eAgMnPc2d1	italský
vojáků	voják	k1gMnPc2	voják
proniklo	proniknout	k5eAaPmAgNnS	proniknout
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
protektorátem	protektorát	k1gInSc7	protektorát
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgInSc1d1	italský
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
však	však	k9	však
zastavil	zastavit	k5eAaPmAgInS	zastavit
po	po	k7c6	po
pouhých	pouhý	k2eAgNnPc6d1	pouhé
sto	sto	k4xCgNnSc4	sto
kilometrech	kilometr	k1gInPc6	kilometr
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
36	[number]	k4	36
000	[number]	k4	000
britských	britský	k2eAgMnPc2d1	britský
<g/>
,	,	kIx,	,
australských	australský	k2eAgInPc2d1	australský
<g/>
,	,	kIx,	,
indických	indický	k2eAgInPc2d1	indický
<g/>
,	,	kIx,	,
novozélandských	novozélandský	k2eAgInPc2d1	novozélandský
a	a	k8xC	a
jihoafrických	jihoafrický	k2eAgMnPc2d1	jihoafrický
vojáků	voják	k1gMnPc2	voják
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byli	být	k5eAaImAgMnP	být
Italové	Ital	k1gMnPc1	Ital
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokračujících	pokračující	k2eAgInPc6d1	pokračující
bojích	boj	k1gInPc6	boj
byli	být	k5eAaImAgMnP	být
Italové	Ital	k1gMnPc1	Ital
nuceni	nutit	k5eAaImNgMnP	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
na	na	k7c4	na
Kyrenaiku	Kyrenaika	k1gFnSc4	Kyrenaika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
posléze	posléze	k6eAd1	posléze
polapena	polapit	k5eAaPmNgFnS	polapit
a	a	k8xC	a
zničena	zničit	k5eAaPmNgFnS	zničit
britským	britský	k2eAgInSc7d1	britský
útokem	útok	k1gInSc7	útok
napříč	napříč	k7c7	napříč
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
katastrofálním	katastrofální	k2eAgInSc6d1	katastrofální
vývoji	vývoj	k1gInSc6	vývoj
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
Mussolinimu	Mussolinima	k1gFnSc4	Mussolinima
než	než	k8xS	než
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
dvě	dva	k4xCgFnPc4	dva
divize	divize	k1gFnPc4	divize
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
vrhl	vrhnout	k5eAaImAgMnS	vrhnout
do	do	k7c2	do
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
britské	britský	k2eAgFnSc2d1	britská
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
oslabené	oslabený	k2eAgNnSc1d1	oslabené
stažením	stažení	k1gNnSc7	stažení
části	část	k1gFnSc2	část
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
jeho	jeho	k3xOp3gInSc1	jeho
Afrikakorps	Afrikakorps	k1gInSc1	Afrikakorps
zpět	zpět	k6eAd1	zpět
Kyrenaiku	Kyrenaika	k1gFnSc4	Kyrenaika
a	a	k8xC	a
přístav	přístav	k1gInSc4	přístav
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
dorazily	dorazit	k5eAaPmAgInP	dorazit
Rommelovy	Rommelův	k2eAgInPc1d1	Rommelův
tanky	tank	k1gInPc1	tank
k	k	k7c3	k
libyjskému	libyjský	k2eAgInSc3d1	libyjský
přístavu	přístav	k1gInSc3	přístav
a	a	k8xC	a
pevnosti	pevnost	k1gFnSc3	pevnost
Tobrúk	Tobrúko	k1gNnPc2	Tobrúko
<g/>
,	,	kIx,	,
bráněnému	bráněný	k2eAgInSc3d1	bráněný
australskými	australský	k2eAgMnPc7d1	australský
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Tobrúk	Tobrúk	k1gMnSc1	Tobrúk
představoval	představovat	k5eAaImAgMnS	představovat
překážku	překážka	k1gFnSc4	překážka
postupu	postup	k1gInSc2	postup
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
Němci	Němec	k1gMnSc3	Němec
třikrát	třikrát	k6eAd1	třikrát
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gNnSc1	jejich
úsilí	úsilí	k1gNnSc1	úsilí
skončilo	skončit	k5eAaPmAgNnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
také	také	k9	také
britské	britský	k2eAgFnPc1d1	britská
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
vyproštění	vyproštění	k1gNnSc6	vyproštění
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
k	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
přemohlo	přemoct	k5eAaPmAgNnS	přemoct
vojsko	vojsko	k1gNnSc1	vojsko
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
proněmeckou	proněmecký	k2eAgFnSc4d1	proněmecká
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
německými	německý	k2eAgFnPc7d1	německá
vzdušnými	vzdušný	k2eAgFnPc7d1	vzdušná
silami	síla	k1gFnPc7	síla
z	z	k7c2	z
letišť	letiště	k1gNnPc2	letiště
ve	v	k7c4	v
vichistickou	vichistický	k2eAgFnSc4d1	vichistická
vládou	vláda	k1gFnSc7	vláda
kontrolované	kontrolovaný	k2eAgFnSc3d1	kontrolovaná
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
proto	proto	k8xC	proto
Britové	Brit	k1gMnPc1	Brit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Svobodnými	svobodný	k2eAgMnPc7d1	svobodný
Francouzi	Francouz	k1gMnPc7	Francouz
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Sýrii	Sýrie	k1gFnSc4	Sýrie
a	a	k8xC	a
Libanon	Libanon	k1gInSc4	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pronikly	proniknout	k5eAaPmAgInP	proniknout
smíšené	smíšený	k2eAgInPc1d1	smíšený
britské	britský	k2eAgInPc1d1	britský
a	a	k8xC	a
koloniální	koloniální	k2eAgInPc1d1	koloniální
oddíly	oddíl	k1gInPc1	oddíl
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
porazily	porazit	k5eAaPmAgFnP	porazit
zde	zde	k6eAd1	zde
umístěné	umístěný	k2eAgNnSc4d1	umístěné
italské	italský	k2eAgNnSc4d1	italské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Italská	italský	k2eAgFnSc1d1	italská
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
vzala	vzít	k5eAaPmAgFnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
a	a	k8xC	a
Východní	východní	k2eAgFnSc1d1	východní
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
bolševismu	bolševismus	k1gInSc3	bolševismus
náležel	náležet	k5eAaImAgInS	náležet
společně	společně	k6eAd1	společně
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
"	"	kIx"	"
<g/>
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
<g/>
"	"	kIx"	"
na	na	k7c6	na
východě	východ	k1gInSc6	východ
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
motivy	motiv	k1gInPc4	motiv
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc4d1	vlastní
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
nacistické	nacistický	k2eAgFnSc2d1	nacistická
rasové	rasový	k2eAgFnSc2d1	rasová
ideologie	ideologie	k1gFnSc2	ideologie
vedeno	veden	k2eAgNnSc4d1	vedeno
jako	jako	k8xS	jako
vyhlazovací	vyhlazovací	k2eAgFnSc1d1	vyhlazovací
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
připravováno	připravovat	k5eAaImNgNnS	připravovat
již	již	k9	již
od	od	k7c2	od
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
byl	být	k5eAaImAgInS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
plán	plán	k1gInSc1	plán
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
,	,	kIx,	,
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
rozdělení	rozdělení	k1gNnSc4	rozdělení
německých	německý	k2eAgFnPc2d1	německá
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
obsazeny	obsadit	k5eAaPmNgInP	obsadit
pobaltské	pobaltský	k2eAgInPc1d1	pobaltský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Leningrad	Leningrad	k1gInSc1	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
spočívala	spočívat	k5eAaImAgFnS	spočívat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
tíže	tíž	k1gFnPc4	tíž
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
postoupit	postoupit	k5eAaPmF	postoupit
přes	přes	k7c4	přes
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
a	a	k8xC	a
dobýt	dobýt	k5eAaPmF	dobýt
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
uskupení	uskupení	k1gNnSc1	uskupení
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rumuny	Rumun	k1gMnPc7	Rumun
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc7	Ital
a	a	k8xC	a
Maďary	Maďar	k1gMnPc7	Maďar
mělo	mít	k5eAaImAgNnS	mít
okupovat	okupovat	k5eAaBmF	okupovat
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
operační	operační	k2eAgInSc4d1	operační
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
linie	linie	k1gFnSc1	linie
spojující	spojující	k2eAgFnSc2d1	spojující
města	město	k1gNnSc2	město
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
a	a	k8xC	a
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
napaden	napadnout	k5eAaPmNgInS	napadnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
s	s	k7c7	s
3600	[number]	k4	3600
tanky	tank	k1gInPc7	tank
překročily	překročit	k5eAaPmAgInP	překročit
sovětské	sovětský	k2eAgFnPc4d1	sovětská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
přesuny	přesun	k1gInPc1	přesun
německých	německý	k2eAgFnPc2d1	německá
armád	armáda	k1gFnPc2	armáda
byly	být	k5eAaImAgInP	být
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgInPc1d1	patrný
celé	celý	k2eAgInPc1d1	celý
měsíce	měsíc	k1gInPc1	měsíc
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c4	v
sovětské	sovětský	k2eAgFnPc4d1	sovětská
vojenské	vojenský	k2eAgFnPc4d1	vojenská
kapacity	kapacita	k1gFnPc4	kapacita
byla	být	k5eAaImAgFnS	být
otřesena	otřást	k5eAaPmNgFnS	otřást
zimní	zimní	k2eAgFnSc7d1	zimní
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgInS	odmítat
vykonat	vykonat	k5eAaPmF	vykonat
jakákoli	jakýkoli	k3yIgNnPc4	jakýkoli
protiopatření	protiopatření	k1gNnPc4	protiopatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgNnP	moct
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
Němce	Němec	k1gMnPc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
tudíž	tudíž	k8xC	tudíž
útokem	útok	k1gInSc7	útok
naprosto	naprosto	k6eAd1	naprosto
zaskočena	zaskočit	k5eAaPmNgFnS	zaskočit
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
měla	mít	k5eAaImAgFnS	mít
nad	nad	k7c7	nad
Němci	Němec	k1gMnPc7	Němec
převahu	převah	k1gInSc2	převah
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
i	i	k8xC	i
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
sovětské	sovětský	k2eAgNnSc1d1	sovětské
letectvo	letectvo	k1gNnSc1	letectvo
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
poničeno	poničit	k5eAaPmNgNnS	poničit
německými	německý	k2eAgInPc7d1	německý
nálety	nálet	k1gInPc7	nálet
první	první	k4xOgFnSc6	první
den	den	k1gInSc1	den
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
pancéřové	pancéřový	k2eAgFnPc1d1	pancéřová
kleště	kleště	k1gFnPc1	kleště
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
pěchotou	pěchota	k1gFnSc7	pěchota
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
obklíčily	obklíčit	k5eAaPmAgFnP	obklíčit
a	a	k8xC	a
zničily	zničit	k5eAaPmAgFnP	zničit
ohromné	ohromný	k2eAgNnSc4d1	ohromné
seskupení	seskupení	k1gNnSc4	seskupení
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Białystok-Minsk	Białystok-Minsk	k1gInSc1	Białystok-Minsk
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
urazily	urazit	k5eAaPmAgFnP	urazit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
července	červenec	k1gInSc2	červenec
překročila	překročit	k5eAaPmAgFnS	překročit
skupina	skupina	k1gFnSc1	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
Dněpr	Dněpr	k1gInSc1	Dněpr
a	a	k8xC	a
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
se	se	k3xPyFc4	se
ke	k	k7c3	k
Smolensku	Smolensko	k1gNnSc3	Smolensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
rozhořčená	rozhořčený	k2eAgFnSc1d1	rozhořčená
obkličovací	obkličovací	k2eAgFnSc1d1	obkličovací
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
skončena	skončit	k5eAaPmNgFnS	skončit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
těchto	tento	k3xDgNnPc6	tento
střetnutích	střetnutí	k1gNnPc6	střetnutí
padly	padnout	k5eAaImAgFnP	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
stovky	stovka	k1gFnSc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
skupina	skupina	k1gFnSc1	skupina
armád	armáda	k1gFnPc2	armáda
Jih	jih	k1gInSc4	jih
obsadila	obsadit	k5eAaPmAgFnS	obsadit
západní	západní	k2eAgFnSc4d1	západní
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Rumuni	Rumun	k1gMnPc1	Rumun
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
Oděsu	Oděsa	k1gFnSc4	Oděsa
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sektoru	sektor	k1gInSc6	sektor
fronty	fronta	k1gFnSc2	fronta
se	se	k3xPyFc4	se
však	však	k9	však
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Němci	Němec	k1gMnPc1	Němec
rychle	rychle	k6eAd1	rychle
překonali	překonat	k5eAaPmAgMnP	překonat
řeku	řeka	k1gFnSc4	řeka
Dvinu	Dvin	k1gInSc2	Dvin
a	a	k8xC	a
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
Estonska	Estonsko	k1gNnSc2	Estonsko
směřovali	směřovat	k5eAaImAgMnP	směřovat
k	k	k7c3	k
Leningradu	Leningrad	k1gInSc3	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
postupovaly	postupovat	k5eAaImAgFnP	postupovat
za	za	k7c4	za
jednotkami	jednotka	k1gFnPc7	jednotka
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
SS	SS	kA	SS
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemilosrdně	milosrdně	k6eNd1	milosrdně
likvidovaly	likvidovat	k5eAaBmAgInP	likvidovat
veškeré	veškerý	k3xTgNnSc4	veškerý
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
politické	politický	k2eAgMnPc4d1	politický
komisaře	komisař	k1gMnPc4	komisař
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
zvěrstvech	zvěrstvo	k1gNnPc6	zvěrstvo
se	se	k3xPyFc4	se
leckdy	leckdy	k6eAd1	leckdy
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
krutostí	krutost	k1gFnSc7	krutost
a	a	k8xC	a
brutalitou	brutalita	k1gFnSc7	brutalita
bylo	být	k5eAaImAgNnS	být
nakládáno	nakládat	k5eAaImNgNnS	nakládat
rovněž	rovněž	k9	rovněž
se	s	k7c7	s
zajatými	zajatý	k2eAgMnPc7d1	zajatý
rudoarmějci	rudoarmějec	k1gMnPc7	rudoarmějec
všech	všecek	k3xTgFnPc2	všecek
hodností	hodnost	k1gFnPc2	hodnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
a	a	k8xC	a
také	také	k6eAd1	také
s	s	k7c7	s
civilisty	civilista	k1gMnPc7	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Smolenska	Smolensko	k1gNnSc2	Smolensko
se	se	k3xPyFc4	se
němečtí	německý	k2eAgMnPc1d1	německý
generálové	generál	k1gMnPc1	generál
zasazovali	zasazovat	k5eAaImAgMnP	zasazovat
za	za	k7c4	za
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnPc4	jejich
vojska	vojsko	k1gNnPc4	vojsko
nyní	nyní	k6eAd1	nyní
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
jen	jen	k9	jen
400	[number]	k4	400
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Hitler	Hitler	k1gMnSc1	Hitler
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
ovládnout	ovládnout	k5eAaPmF	ovládnout
ukrajinské	ukrajinský	k2eAgInPc4d1	ukrajinský
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
tažení	tažení	k1gNnSc2	tažení
nařídilo	nařídit	k5eAaPmAgNnS	nařídit
sovětské	sovětský	k2eAgNnSc1d1	sovětské
vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
(	(	kIx(	(
<g/>
Stavka	Stavka	k1gFnSc1	Stavka
<g/>
)	)	kIx)	)
evakuaci	evakuace	k1gFnSc4	evakuace
sovětského	sovětský	k2eAgInSc2d1	sovětský
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
především	především	k9	především
demontování	demontování	k1gNnSc1	demontování
veškerého	veškerý	k3xTgInSc2	veškerý
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
evakuaci	evakuace	k1gFnSc4	evakuace
ze	z	k7c2	z
západních	západní	k2eAgInPc2d1	západní
regionů	region	k1gInPc2	region
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
za	za	k7c4	za
Ural	Ural	k1gInSc4	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
proto	proto	k8xC	proto
skupině	skupina	k1gFnSc6	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
nařídil	nařídit	k5eAaPmAgInS	nařídit
zastavit	zastavit	k5eAaPmF	zastavit
postup	postup	k1gInSc4	postup
na	na	k7c4	na
sovětské	sovětský	k2eAgNnSc4d1	sovětské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
obrátit	obrátit	k5eAaPmF	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
Němci	Němec	k1gMnPc7	Němec
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Finy	Fin	k1gMnPc7	Fin
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
Leningrad	Leningrad	k1gInSc4	Leningrad
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
postupně	postupně	k6eAd1	postupně
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
odřízli	odříznout	k5eAaPmAgMnP	odříznout
od	od	k7c2	od
veškerého	veškerý	k3xTgNnSc2	veškerý
nutného	nutný	k2eAgNnSc2d1	nutné
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Obležení	obležení	k1gNnSc1	obležení
Leningradu	Leningrad	k1gInSc2	Leningrad
trvalo	trvat	k5eAaImAgNnS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
si	se	k3xPyFc3	se
životy	život	k1gInPc1	život
až	až	k9	až
jednoho	jeden	k4xCgMnSc4	jeden
milionu	milion	k4xCgInSc2	milion
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
gigantické	gigantický	k2eAgFnSc6d1	gigantická
obkličovací	obkličovací	k2eAgFnSc6d1	obkličovací
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kyjeva	Kyjev	k1gInSc2	Kyjev
zajato	zajat	k2eAgNnSc1d1	zajato
dalších	další	k2eAgInPc6d1	další
650	[number]	k4	650
000	[number]	k4	000
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šestitýdenní	šestitýdenní	k2eAgFnSc6d1	šestitýdenní
pauze	pauza	k1gFnSc6	pauza
obnovila	obnovit	k5eAaPmAgFnS	obnovit
skupina	skupina	k1gFnSc1	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
svůj	svůj	k3xOyFgInSc4	svůj
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Moskvu	Moskva	k1gFnSc4	Moskva
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
operace	operace	k1gFnSc1	operace
Tajfun	tajfun	k1gInSc1	tajfun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
postup	postup	k1gInSc1	postup
však	však	k9	však
záhy	záhy	k6eAd1	záhy
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
podzimní	podzimní	k2eAgInPc4d1	podzimní
deště	dešť	k1gInPc4	dešť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
proměnily	proměnit	k5eAaPmAgFnP	proměnit
ruské	ruský	k2eAgFnPc4d1	ruská
cesty	cesta	k1gFnPc4	cesta
v	v	k7c4	v
bláto	bláto	k1gNnSc4	bláto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
udeřily	udeřit	k5eAaPmAgInP	udeřit
první	první	k4xOgInPc1	první
mrazy	mráz	k1gInPc1	mráz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
pronikli	proniknout	k5eAaPmAgMnP	proniknout
Němci	Němec	k1gMnPc1	Němec
do	do	k7c2	do
Rostova	Rostův	k2eAgInSc2d1	Rostův
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
a	a	k8xC	a
na	na	k7c4	na
Krym	Krym	k1gInSc4	Krym
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
zahájili	zahájit	k5eAaPmAgMnP	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
finální	finální	k2eAgFnSc4d1	finální
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
proti	proti	k7c3	proti
Moskvě	Moskva	k1gFnSc3	Moskva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
obrovitým	obrovitý	k2eAgInSc7d1	obrovitý
obchvatem	obchvat	k1gInSc7	obchvat
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
měsíce	měsíc	k1gInSc2	měsíc
stály	stát	k5eAaImAgFnP	stát
předsunuté	předsunutý	k2eAgFnPc1d1	předsunutá
jednotky	jednotka	k1gFnPc1	jednotka
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
sotva	sotva	k6eAd1	sotva
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
,	,	kIx,	,
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
německý	německý	k2eAgInSc1d1	německý
postup	postup	k1gInSc1	postup
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
zahájily	zahájit	k5eAaPmAgFnP	zahájit
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
sibiřské	sibiřský	k2eAgFnPc1d1	sibiřská
divize	divize	k1gFnPc1	divize
maršála	maršál	k1gMnSc4	maršál
Žukova	Žukov	k1gInSc2	Žukov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
moderními	moderní	k2eAgInPc7d1	moderní
tanky	tank	k1gInPc1	tank
T-	T-	k1gFnSc1	T-
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc4d1	sovětský
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
zatlačil	zatlačit	k5eAaPmAgInS	zatlačit
příliš	příliš	k6eAd1	příliš
roztažené	roztažený	k2eAgFnPc1d1	roztažená
německé	německý	k2eAgFnPc1d1	německá
linie	linie	k1gFnPc1	linie
o	o	k7c4	o
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
blesková	bleskový	k2eAgFnSc1d1	blesková
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
prakticky	prakticky	k6eAd1	prakticky
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Útok	útok	k1gInSc1	útok
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbor	k1gMnSc1	Harbor
a	a	k8xC	a
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
agrese	agrese	k1gFnSc1	agrese
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ochlazení	ochlazení	k1gNnSc1	ochlazení
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
Francie	Francie	k1gFnSc2	Francie
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
japonské	japonský	k2eAgFnPc1d1	japonská
jednotky	jednotka	k1gFnPc1	jednotka
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1940	[number]	k4	1940
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Indočíny	Indočína	k1gFnSc2	Indočína
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
Japonsko	Japonsko	k1gNnSc4	Japonsko
formálně	formálně	k6eAd1	formálně
stvrdilo	stvrdit	k5eAaPmAgNnS	stvrdit
své	svůj	k3xOyFgNnSc1	svůj
spojenectví	spojenectví	k1gNnSc1	spojenectví
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
a	a	k8xC	a
stupňující	stupňující	k2eAgMnSc1d1	stupňující
se	se	k3xPyFc4	se
japonské	japonský	k2eAgInPc1d1	japonský
požadavky	požadavek	k1gInPc1	požadavek
vůči	vůči	k7c3	vůči
Holandské	holandský	k2eAgFnSc3d1	holandská
východní	východní	k2eAgFnSc3d1	východní
Indii	Indie	k1gFnSc3	Indie
přiměly	přimět	k5eAaPmAgFnP	přimět
USA	USA	kA	USA
k	k	k7c3	k
uvalení	uvalení	k1gNnSc3	uvalení
ropného	ropný	k2eAgNnSc2d1	ropné
embarga	embargo	k1gNnSc2	embargo
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
vážně	vážně	k6eAd1	vážně
poškozeno	poškozen	k2eAgNnSc1d1	poškozeno
jeho	jeho	k3xOp3gNnSc4	jeho
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
militantního	militantní	k2eAgNnSc2d1	militantní
křídla	křídlo	k1gNnSc2	křídlo
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
Hideki	Hideki	k1gNnSc4	Hideki
Tódžó	Tódžó	k1gFnSc2	Tódžó
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Japonsko	Japonsko	k1gNnSc1	Japonsko
muselo	muset	k5eAaImAgNnS	muset
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
svých	svůj	k3xOyFgInPc2	svůj
výbojů	výboj	k1gInPc2	výboj
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
zajistit	zajistit	k5eAaPmF	zajistit
dodávky	dodávka	k1gFnPc4	dodávka
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
surovin	surovina	k1gFnPc2	surovina
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Císařský	císařský	k2eAgInSc1d1	císařský
generální	generální	k2eAgInSc1d1	generální
štáb	štáb	k1gInSc1	štáb
proto	proto	k8xC	proto
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
plán	plán	k1gInSc1	plán
obranného	obranný	k2eAgInSc2d1	obranný
perimetru	perimetr	k1gInSc2	perimetr
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
Japonsku	Japonsko	k1gNnSc6	Japonsko
nerušené	rušený	k2eNgNnSc4d1	nerušené
využívání	využívání	k1gNnSc4	využívání
tamních	tamní	k2eAgInPc2d1	tamní
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
k	k	k7c3	k
vedení	vedení	k1gNnSc3	vedení
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
úspěchu	úspěch	k1gInSc2	úspěch
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
byla	být	k5eAaImAgFnS	být
neutralizace	neutralizace	k1gFnSc1	neutralizace
americké	americký	k2eAgFnSc2d1	americká
tichomořské	tichomořský	k2eAgFnSc2d1	tichomořská
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
se	se	k3xPyFc4	se
z	z	k7c2	z
japonských	japonský	k2eAgFnPc2d1	japonská
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
připluly	připlout	k5eAaPmAgFnP	připlout
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
vznesly	vznést	k5eAaPmAgFnP	vznést
stovky	stovka	k1gFnPc1	stovka
japonských	japonský	k2eAgNnPc2d1	Japonské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
osmou	osma	k1gFnSc7	osma
hodinou	hodina	k1gFnSc7	hodina
podnikly	podniknout	k5eAaPmAgFnP	podniknout
ničivý	ničivý	k2eAgInSc4d1	ničivý
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
námořní	námořní	k2eAgFnSc4d1	námořní
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Pearl	Pearla	k1gFnPc2	Pearla
Harboru	Harbor	k1gInSc2	Harbor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvouhodinového	dvouhodinový	k2eAgInSc2d1	dvouhodinový
útoku	útok	k1gInSc2	útok
byla	být	k5eAaImAgFnS	být
potopena	potopen	k2eAgFnSc1d1	potopena
nebo	nebo	k8xC	nebo
vážně	vážně	k6eAd1	vážně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
řada	řada	k1gFnSc1	řada
amerických	americký	k2eAgFnPc2d1	americká
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
zákeřném	zákeřný	k2eAgNnSc6d1	zákeřné
napadení	napadení	k1gNnSc6	napadení
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
USA	USA	kA	USA
Japonsku	Japonsko	k1gNnSc6	Japonsko
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
přidaly	přidat	k5eAaPmAgInP	přidat
také	také	k9	také
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
USA	USA	kA	USA
pozvolna	pozvolna	k6eAd1	pozvolna
opouštěly	opouštět	k5eAaImAgFnP	opouštět
svoji	svůj	k3xOyFgFnSc4	svůj
izolacionistickou	izolacionistický	k2eAgFnSc4d1	izolacionistická
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
zastávaly	zastávat	k5eAaImAgInP	zastávat
jen	jen	k9	jen
formálně	formálně	k6eAd1	formálně
neutrální	neutrální	k2eAgInSc4d1	neutrální
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
válčícím	válčící	k2eAgFnPc3d1	válčící
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
prezident	prezident	k1gMnSc1	prezident
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
když	když	k8xS	když
prosadil	prosadit	k5eAaPmAgMnS	prosadit
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc6	pronájem
umožňující	umožňující	k2eAgFnSc1d1	umožňující
Britům	Brit	k1gMnPc3	Brit
<g/>
,	,	kIx,	,
Číňanům	Číňan	k1gMnPc3	Číňan
a	a	k8xC	a
Sovětům	Sovět	k1gMnPc3	Sovět
odebírat	odebírat	k5eAaImF	odebírat
americké	americký	k2eAgFnPc4d1	americká
zbrojní	zbrojní	k2eAgFnPc4d1	zbrojní
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
dodávky	dodávka	k1gFnPc4	dodávka
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
splacení	splacení	k1gNnSc2	splacení
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
japonském	japonský	k2eAgNnSc6d1	Japonské
přepadení	přepadení	k1gNnSc6	přepadení
Pearl	Pearl	k1gInSc4	Pearl
Harboru	Harbor	k1gInSc2	Harbor
se	se	k3xPyFc4	se
ohromné	ohromný	k2eAgFnPc1d1	ohromná
americké	americký	k2eAgFnPc1d1	americká
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
kapacity	kapacita	k1gFnPc1	kapacita
plně	plně	k6eAd1	plně
zapojily	zapojit	k5eAaPmAgFnP	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Ose	osa	k1gFnSc3	osa
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
válku	válka	k1gFnSc4	válka
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
americkým	americký	k2eAgInPc3d1	americký
a	a	k8xC	a
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
osobně	osobně	k6eAd1	osobně
schválil	schválit	k5eAaPmAgMnS	schválit
přípravu	příprava	k1gFnSc4	příprava
na	na	k7c6	na
provedení	provedení	k1gNnSc6	provedení
sabotážních	sabotážní	k2eAgInPc2d1	sabotážní
a	a	k8xC	a
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Operace	operace	k1gFnSc1	operace
Pastorius	Pastorius	k1gInSc1	Pastorius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ochromení	ochromení	k1gNnSc1	ochromení
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
floty	flota	k1gFnSc2	flota
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
Japoncům	Japonec	k1gMnPc3	Japonec
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
prosince	prosinec	k1gInSc2	prosinec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Japonci	Japonec	k1gMnPc1	Japonec
britský	britský	k2eAgInSc4d1	britský
Hongkong	Hongkong	k1gInSc4	Hongkong
a	a	k8xC	a
Gilbertovy	Gilbertův	k2eAgInPc4d1	Gilbertův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Guam	Guam	k1gInSc4	Guam
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
Wake	Wake	k1gFnPc2	Wake
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
britského	britský	k2eAgNnSc2d1	Britské
Malajska	Malajsko	k1gNnSc2	Malajsko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
Siamu	Siam	k1gInSc2	Siam
napadli	napadnout	k5eAaPmAgMnP	napadnout
Barmu	Barma	k1gFnSc4	Barma
a	a	k8xC	a
vylodili	vylodit	k5eAaPmAgMnP	vylodit
se	se	k3xPyFc4	se
na	na	k7c4	na
Američanech	Američan	k1gMnPc6	Američan
kontrolovaných	kontrolovaný	k2eAgFnPc6d1	kontrolovaná
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
nedobytnou	dobytný	k2eNgFnSc4d1	nedobytná
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
padl	padnout	k5eAaPmAgInS	padnout
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
130	[number]	k4	130
000	[number]	k4	000
Indů	Ind	k1gMnPc2	Ind
<g/>
,	,	kIx,	,
Australanů	Australan	k1gMnPc2	Australan
a	a	k8xC	a
Britů	Brit	k1gMnPc2	Brit
bylo	být	k5eAaImAgNnS	být
vzato	vzít	k5eAaPmNgNnS	vzít
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
Singapuru	Singapur	k1gInSc2	Singapur
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tudíž	tudíž	k8xC	tudíž
nejvíce	hodně	k6eAd3	hodně
pokořující	pokořující	k2eAgFnSc7d1	pokořující
kapitulací	kapitulace	k1gFnSc7	kapitulace
britských	britský	k2eAgFnPc2d1	britská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Jávském	jávský	k2eAgNnSc6d1	jávské
moři	moře	k1gNnSc6	moře
okupovali	okupovat	k5eAaBmAgMnP	okupovat
Japonci	Japonec	k1gMnPc1	Japonec
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
Holandskou	holandský	k2eAgFnSc4d1	holandská
východní	východní	k2eAgFnSc4d1	východní
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
Guinei	Guine	k1gFnSc6	Guine
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
filipínské	filipínský	k2eAgInPc1d1	filipínský
oddíly	oddíl	k1gInPc1	oddíl
kapitulovaly	kapitulovat	k5eAaBmAgInP	kapitulovat
na	na	k7c6	na
filipínském	filipínský	k2eAgInSc6d1	filipínský
Luzonu	Luzon	k1gInSc6	Luzon
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
obránci	obránce	k1gMnPc1	obránce
Corregidoru	Corregidor	k1gInSc2	Corregidor
vydrželi	vydržet	k5eAaPmAgMnP	vydržet
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Japonci	Japonec	k1gMnPc1	Japonec
obojživelnou	obojživelný	k2eAgFnSc4d1	obojživelná
operaci	operace	k1gFnSc4	operace
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
Port	porta	k1gFnPc2	porta
Moresby	Moresba	k1gFnSc2	Moresba
na	na	k7c4	na
Nové	Nové	k2eAgNnSc4d1	Nové
Guinei	Guinei	k1gNnSc4	Guinei
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
narušili	narušit	k5eAaPmAgMnP	narušit
komunikační	komunikační	k2eAgInPc4d1	komunikační
spoje	spoj	k1gInPc4	spoj
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Korálovém	korálový	k2eAgNnSc6d1	korálové
moři	moře	k1gNnSc6	moře
Spojenci	spojenec	k1gMnPc1	spojenec
odrazili	odrazit	k5eAaPmAgMnP	odrazit
japonské	japonský	k2eAgFnPc4d1	japonská
invazní	invazní	k2eAgFnPc4d1	invazní
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
symbolickém	symbolický	k2eAgInSc6d1	symbolický
Doolittlově	Doolittlův	k2eAgInSc6d1	Doolittlův
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
Tokio	Tokio	k1gNnSc4	Tokio
japonské	japonský	k2eAgNnSc1d1	Japonské
velení	velení	k1gNnSc1	velení
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
obsazení	obsazení	k1gNnSc6	obsazení
atolu	atol	k1gInSc2	atol
Midway	Midwaa	k1gFnSc2	Midwaa
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
dobyty	dobyt	k2eAgInPc4d1	dobyt
Aleutské	aleutský	k2eAgInPc4d1	aleutský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
vyplula	vyplout	k5eAaPmAgFnS	vyplout
japonská	japonský	k2eAgFnSc1d1	japonská
flota	flota	k1gFnSc1	flota
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
admirála	admirál	k1gMnSc2	admirál
Jamamota	Jamamot	k1gMnSc2	Jamamot
vstříc	vstříc	k6eAd1	vstříc
Midwayi	Midwayi	k1gNnSc4	Midwayi
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Američané	Američan	k1gMnPc1	Američan
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
prolomili	prolomit	k5eAaPmAgMnP	prolomit
japonský	japonský	k2eAgInSc4d1	japonský
kód	kód	k1gInSc4	kód
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
tak	tak	k6eAd1	tak
zpraveni	zpravit	k5eAaPmNgMnP	zpravit
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
úmyslu	úmysl	k1gInSc6	úmysl
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Američanům	Američan	k1gMnPc3	Američan
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Midway	Midwaa	k1gFnSc2	Midwaa
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
japonské	japonský	k2eAgFnPc4d1	japonská
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
tvořily	tvořit	k5eAaImAgFnP	tvořit
jádro	jádro	k1gNnSc4	jádro
japonských	japonský	k2eAgFnPc2d1	japonská
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Jamamoto	Jamamota	k1gFnSc5	Jamamota
nařídil	nařídit	k5eAaPmAgMnS	nařídit
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pouhých	pouhý	k2eAgInPc6d1	pouhý
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc3	období
japonských	japonský	k2eAgMnPc2d1	japonský
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
císařské	císařský	k2eAgNnSc1d1	císařské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
připravila	připravit	k5eAaPmAgFnS	připravit
o	o	k7c4	o
klíčové	klíčový	k2eAgFnPc4d1	klíčová
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nevzpamatovalo	vzpamatovat	k5eNaPmAgNnS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1941	[number]	k4	1941
zahájila	zahájit	k5eAaPmAgFnS	zahájit
posílená	posílený	k2eAgFnSc1d1	posílená
britská	britský	k2eAgFnSc1d1	britská
8	[number]	k4	8
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
operaci	operace	k1gFnSc4	operace
Crusader	Crusadero	k1gNnPc2	Crusadero
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Tobrúk	Tobrúk	k1gInSc4	Tobrúk
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
německé	německý	k2eAgFnPc1d1	německá
námořní	námořní	k2eAgFnPc1d1	námořní
a	a	k8xC	a
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
linie	linie	k1gFnPc1	linie
byly	být	k5eAaImAgFnP	být
narušovány	narušován	k2eAgInPc4d1	narušován
útoky	útok	k1gInPc4	útok
britských	britský	k2eAgFnPc2d1	britská
námořních	námořní	k2eAgFnPc2d1	námořní
a	a	k8xC	a
leteckých	letecký	k2eAgFnPc2d1	letecká
sil	síla	k1gFnPc2	síla
operujících	operující	k2eAgFnPc2d1	operující
z	z	k7c2	z
Malty	Malta	k1gFnSc2	Malta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Rommel	Rommel	k1gMnSc1	Rommel
přinucen	přinutit	k5eAaPmNgMnS	přinutit
k	k	k7c3	k
ústupu	ústup	k1gInSc2	ústup
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
výchozí	výchozí	k2eAgFnSc4d1	výchozí
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
britský	britský	k2eAgInSc1d1	britský
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
nepřátelské	přátelský	k2eNgFnPc4d1	nepřátelská
komunikační	komunikační	k2eAgFnPc4d1	komunikační
linie	linie	k1gFnPc4	linie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
polevil	polevit	k5eAaPmAgInS	polevit
<g/>
,	,	kIx,	,
síly	síla	k1gFnPc1	síla
Osy	osa	k1gFnSc2	osa
byly	být	k5eAaImAgFnP	být
rychle	rychle	k6eAd1	rychle
doplněny	doplněn	k2eAgFnPc1d1	doplněna
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
obnovily	obnovit	k5eAaPmAgFnP	obnovit
svoji	svůj	k3xOyFgFnSc4	svůj
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
poté	poté	k6eAd1	poté
snadno	snadno	k6eAd1	snadno
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
Brity	Brit	k1gMnPc4	Brit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Kyrenaiku	Kyrenaika	k1gFnSc4	Kyrenaika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
přemohl	přemoct	k5eAaPmAgInS	přemoct
jejich	jejich	k3xOp3gFnSc4	jejich
obranu	obrana	k1gFnSc4	obrana
u	u	k7c2	u
Gazaly	Gazala	k1gFnSc2	Gazala
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
Afrikakorps	Afrikakorps	k1gInSc1	Afrikakorps
zdolal	zdolat	k5eAaPmAgInS	zdolat
veškeré	veškerý	k3xTgFnPc4	veškerý
britské	britský	k2eAgFnPc4d1	britská
obranné	obranný	k2eAgFnPc4d1	obranná
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
po	po	k7c6	po
dvoudenním	dvoudenní	k2eAgInSc6d1	dvoudenní
boji	boj	k1gInSc6	boj
obsadil	obsadit	k5eAaPmAgMnS	obsadit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Tobrúk	Tobrúka	k1gFnPc2	Tobrúka
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Osy	osa	k1gFnSc2	osa
bylo	být	k5eAaImAgNnS	být
korunováno	korunován	k2eAgNnSc1d1	korunováno
zajetím	zajetí	k1gNnSc7	zajetí
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
000	[number]	k4	000
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
pak	pak	k6eAd1	pak
navzdory	navzdory	k7c3	navzdory
rozkazům	rozkaz	k1gInPc3	rozkaz
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
poraženou	poražený	k2eAgFnSc4d1	poražená
8	[number]	k4	8
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
střetl	střetnout	k5eAaPmAgMnS	střetnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alamejnu	Alamejn	k1gInSc2	Alamejn
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Rommelův	Rommelův	k2eAgInSc1d1	Rommelův
pokus	pokus	k1gInSc1	pokus
obsadit	obsadit	k5eAaPmF	obsadit
Alexandrii	Alexandrie	k1gFnSc4	Alexandrie
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
pouhých	pouhý	k2eAgFnPc2d1	pouhá
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
červenec	červenec	k1gInSc4	červenec
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
příliš	příliš	k6eAd1	příliš
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Atlantik	Atlantik	k1gInSc4	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
ponorkové	ponorkový	k2eAgNnSc1d1	ponorkové
loďstvo	loďstvo	k1gNnSc1	loďstvo
mělo	mít	k5eAaImAgNnS	mít
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
a	a	k8xC	a
izolovat	izolovat	k5eAaBmF	izolovat
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
ničením	ničení	k1gNnSc7	ničení
obchodních	obchodní	k2eAgFnPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vlastní	vlastní	k2eAgInSc4d1	vlastní
počátek	počátek	k1gInSc4	počátek
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Atlantik	Atlantik	k1gInSc4	Atlantik
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
14	[number]	k4	14
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
německá	německý	k2eAgFnSc1d1	německá
ponorka	ponorka	k1gFnSc1	ponorka
vnikla	vniknout	k5eAaPmAgFnS	vniknout
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Scapa	Scap	k1gMnSc2	Scap
Flow	Flow	k1gMnSc2	Flow
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgInSc2d1	hlavní
opěrného	opěrný	k2eAgInSc2d1	opěrný
bodu	bod	k1gInSc2	bod
britské	britský	k2eAgFnSc2d1	britská
domácí	domácí	k2eAgFnSc2d1	domácí
floty	flota	k1gFnSc2	flota
(	(	kIx(	(
<g/>
Home	Home	k1gFnSc1	Home
Fleet	Fleet	k1gMnSc1	Fleet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
potopila	potopit	k5eAaPmAgFnS	potopit
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
zde	zde	k6eAd1	zde
zakotvených	zakotvený	k2eAgFnPc2d1	zakotvená
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
německé	německý	k2eAgInPc4d1	německý
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
omezovaly	omezovat	k5eAaImAgInP	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
ojedinělé	ojedinělý	k2eAgFnPc4d1	ojedinělá
akce	akce	k1gFnPc4	akce
osamocených	osamocený	k2eAgFnPc2d1	osamocená
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zřízení	zřízení	k1gNnSc4	zřízení
ponorkových	ponorkový	k2eAgFnPc2d1	ponorková
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
přístavech	přístav	k1gInPc6	přístav
v	v	k7c6	v
Brestu	Brest	k1gInSc6	Brest
<g/>
,	,	kIx,	,
Lorientu	Lorient	k1gInSc6	Lorient
<g/>
,	,	kIx,	,
Saint-Nazaire	Saint-Nazair	k1gInSc5	Saint-Nazair
a	a	k8xC	a
La	la	k1gNnSc3	la
Rochelle	Rochelle	k1gNnSc2	Rochelle
<g/>
,	,	kIx,	,
zajišťujících	zajišťující	k2eAgInPc2d1	zajišťující
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
dosažení	dosažení	k1gNnSc4	dosažení
operačních	operační	k2eAgInPc2d1	operační
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zefektivnění	zefektivnění	k1gNnSc3	zefektivnění
německých	německý	k2eAgFnPc2d1	německá
ponorkových	ponorkový	k2eAgFnPc2d1	ponorková
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
ponorky	ponorka	k1gFnPc1	ponorka
plně	plně	k6eAd1	plně
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
taktiku	taktika	k1gFnSc4	taktika
vlčích	vlčí	k2eAgFnPc2d1	vlčí
smeček	smečka	k1gFnPc2	smečka
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgFnPc3	jenž
byly	být	k5eAaImAgInP	být
spojenecké	spojenecký	k2eAgInPc1d1	spojenecký
konvoje	konvoj	k1gInPc1	konvoj
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
chráněné	chráněný	k2eAgInPc4d1	chráněný
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
torpédoborci	torpédoborec	k1gInPc7	torpédoborec
<g/>
,	,	kIx,	,
bezmocné	bezmocný	k2eAgInPc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
přišli	přijít	k5eAaPmAgMnP	přijít
Spojenci	spojenec	k1gMnPc1	spojenec
o	o	k7c4	o
4,5	[number]	k4	4,5
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
lodního	lodní	k2eAgInSc2d1	lodní
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
stejné	stejný	k2eAgNnSc4d1	stejné
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
obratně	obratně	k6eAd1	obratně
využívala	využívat	k5eAaPmAgFnS	využívat
nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
mnohé	mnohé	k1gNnSc4	mnohé
z	z	k7c2	z
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
kapitánů	kapitán	k1gMnPc2	kapitán
stylizovala	stylizovat	k5eAaImAgFnS	stylizovat
do	do	k7c2	do
role	role	k1gFnSc2	role
národních	národní	k2eAgInPc2d1	národní
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
vedení	vedení	k1gNnSc2	vedení
ponorkové	ponorkový	k2eAgFnSc2d1	ponorková
války	válka	k1gFnSc2	válka
vyplula	vyplout	k5eAaPmAgFnS	vyplout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
z	z	k7c2	z
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
německá	německý	k2eAgFnSc1d1	německá
eskadra	eskadra	k1gFnSc1	eskadra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
právě	právě	k6eAd1	právě
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
uvedené	uvedený	k2eAgFnSc2d1	uvedená
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodě	loď	k1gFnSc2	loď
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
,	,	kIx,	,
těžkého	těžký	k2eAgInSc2d1	těžký
křižníku	křižník	k1gInSc2	křižník
Prinz	Prinz	k1gMnSc1	Prinz
Eugen	Eugno	k1gNnPc2	Eugno
a	a	k8xC	a
několika	několik	k4yIc2	několik
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
plavby	plavba	k1gFnSc2	plavba
v	v	k7c6	v
Dánském	dánský	k2eAgInSc6d1	dánský
průlivu	průliv	k1gInSc6	průliv
mezi	mezi	k7c7	mezi
Grónskem	Grónsko	k1gNnSc7	Grónsko
a	a	k8xC	a
Islandem	Island	k1gInSc7	Island
poslal	poslat	k5eAaPmAgMnS	poslat
Bismarck	Bismarck	k1gInSc4	Bismarck
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
britský	britský	k2eAgInSc4d1	britský
bitevní	bitevní	k2eAgInSc4d1	bitevní
křižník	křižník	k1gInSc4	křižník
HMS	HMS	kA	HMS
Hood	Hood	k1gInSc1	Hood
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
doplul	doplout	k5eAaPmAgMnS	doplout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dostižen	dostižen	k2eAgInSc1d1	dostižen
a	a	k8xC	a
zničen	zničen	k2eAgInSc1d1	zničen
britským	britský	k2eAgNnSc7d1	Britské
loďstvem	loďstvo	k1gNnSc7	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
do	do	k7c2	do
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
německých	německý	k2eAgFnPc2d1	německá
akcí	akce	k1gFnPc2	akce
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
americkému	americký	k2eAgNnSc3d1	americké
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
špatně	špatně	k6eAd1	špatně
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
americká	americký	k2eAgFnSc1d1	americká
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
obrana	obrana	k1gFnSc1	obrana
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
zamezit	zamezit	k5eAaPmF	zamezit
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
obchodní	obchodní	k2eAgFnSc4d1	obchodní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgNnSc1d1	operační
pásmo	pásmo	k1gNnSc1	pásmo
ponorek	ponorka	k1gFnPc2	ponorka
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
intenzivnější	intenzivní	k2eAgFnSc2d2	intenzivnější
obrany	obrana	k1gFnSc2	obrana
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
až	až	k6eAd1	až
do	do	k7c2	do
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
jižního	jižní	k2eAgInSc2d1	jižní
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
počet	počet	k1gInSc1	počet
asi	asi	k9	asi
50	[number]	k4	50
ponorek	ponorka	k1gFnPc2	ponorka
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
rokem	rok	k1gInSc7	rok
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zničily	zničit	k5eAaPmAgFnP	zničit
přes	přes	k7c4	přes
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
lodního	lodní	k2eAgInSc2d1	lodní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
přiblížily	přiblížit	k5eAaPmAgInP	přiblížit
kritické	kritický	k2eAgInPc1d1	kritický
hranici	hranice	k1gFnSc4	hranice
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
britské	britský	k2eAgNnSc4d1	Britské
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1942	[number]	k4	1942
také	také	k9	také
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
Operace	operace	k1gFnSc1	operace
Pastorius	Pastorius	k1gInSc1	Pastorius
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
vylodění	vylodění	k1gNnSc4	vylodění
německých	německý	k2eAgMnPc2d1	německý
sabotérů	sabotér	k1gMnPc2	sabotér
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
poškodit	poškodit	k5eAaPmF	poškodit
válečný	válečný	k2eAgInSc4d1	válečný
průmysl	průmysl	k1gInSc4	průmysl
USA	USA	kA	USA
a	a	k8xC	a
vyvolat	vyvolat	k5eAaPmF	vyvolat
bombovými	bombový	k2eAgInPc7d1	bombový
útoky	útok	k1gInPc7	útok
paniku	panika	k1gFnSc4	panika
mezi	mezi	k7c7	mezi
civilním	civilní	k2eAgNnSc7d1	civilní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
však	však	k9	však
Spojenci	spojenec	k1gMnPc1	spojenec
prolomili	prolomit	k5eAaPmAgMnP	prolomit
německé	německý	k2eAgNnSc4d1	německé
šifrovací	šifrovací	k2eAgNnSc4d1	šifrovací
zařízení	zařízení	k1gNnSc4	zařízení
Enigma	enigma	k1gNnSc1	enigma
<g/>
,	,	kIx,	,
používané	používaný	k2eAgNnSc1d1	používané
ponorkami	ponorka	k1gFnPc7	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
produkci	produkce	k1gFnSc3	produkce
modernějších	moderní	k2eAgNnPc2d2	modernější
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1943	[number]	k4	1943
potopeno	potopit	k5eAaPmNgNnS	potopit
43	[number]	k4	43
německých	německý	k2eAgFnPc2d1	německá
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
dočasné	dočasný	k2eAgNnSc1d1	dočasné
přerušení	přerušení	k1gNnSc1	přerušení
ponorkových	ponorkový	k2eAgFnPc2d1	ponorková
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
zvratu	zvrat	k1gInSc6	zvrat
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
poklesem	pokles	k1gInSc7	pokles
potopené	potopený	k2eAgFnSc2d1	potopená
tonáže	tonáž	k1gFnSc2	tonáž
na	na	k7c4	na
3,5	[number]	k4	3,5
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
nezměnil	změnit	k5eNaPmAgInS	změnit
ani	ani	k8xC	ani
vynález	vynález	k1gInSc1	vynález
šnorchlu	šnorchl	k1gInSc2	šnorchl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1944	[number]	k4	1944
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
opuštěny	opuštěn	k2eAgFnPc4d1	opuštěna
ponorkové	ponorkový	k2eAgFnPc4d1	ponorková
základny	základna	k1gFnPc4	základna
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
zatlačeny	zatlačit	k5eAaPmNgFnP	zatlačit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
kapitulaci	kapitulace	k1gFnSc6	kapitulace
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
byly	být	k5eAaImAgFnP	být
ponorky	ponorka	k1gFnPc1	ponorka
buď	buď	k8xC	buď
poslány	poslán	k2eAgFnPc1d1	poslána
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
posádky	posádka	k1gFnPc1	posádka
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
ve	v	k7c6	v
spojeneckých	spojenecký	k2eAgInPc6d1	spojenecký
přístavech	přístav	k1gInPc6	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgInP	být
sovětské	sovětský	k2eAgInPc1d1	sovětský
průlomy	průlom	k1gInPc1	průlom
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
eliminovány	eliminovat	k5eAaBmNgFnP	eliminovat
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1942	[number]	k4	1942
opět	opět	k6eAd1	opět
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
(	(	kIx(	(
<g/>
Fall	Fall	k1gInSc1	Fall
Blau	Blaus	k1gInSc2	Blaus
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dobytí	dobytí	k1gNnSc2	dobytí
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
Volhy	Volha	k1gFnSc2	Volha
proniknutí	proniknutí	k1gNnSc2	proniknutí
za	za	k7c4	za
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
obsazeno	obsazen	k2eAgNnSc4d1	obsazeno
Baku	Baku	k1gNnSc4	Baku
a	a	k8xC	a
místní	místní	k2eAgFnPc4d1	místní
ropná	ropný	k2eAgNnPc4d1	ropné
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
půda	půda	k1gFnSc1	půda
po	po	k7c6	po
jarním	jarní	k2eAgNnSc6d1	jarní
tání	tání	k1gNnSc6	tání
dostatečně	dostatečně	k6eAd1	dostatečně
ztvrdla	ztvrdnout	k5eAaPmAgFnS	ztvrdnout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
zahájen	zahájit	k5eAaPmNgInS	zahájit
útok	útok	k1gInSc1	útok
proti	proti	k7c3	proti
Sovětům	Sověty	k1gInPc3	Sověty
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
dobytím	dobytí	k1gNnSc7	dobytí
obleženého	obležený	k2eAgInSc2d1	obležený
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
Němci	Němec	k1gMnPc1	Němec
východně	východně	k6eAd1	východně
od	od	k7c2	od
Charkova	Charkov	k1gInSc2	Charkov
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Voroněž	Voroněž	k1gFnSc4	Voroněž
a	a	k8xC	a
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Donu	Don	k1gInSc2	Don
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
rychlosti	rychlost	k1gFnSc3	rychlost
útoku	útok	k1gInSc2	útok
byly	být	k5eAaImAgInP	být
počty	počet	k1gInPc1	počet
zajatých	zajatý	k2eAgMnPc2d1	zajatý
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
generálové	generál	k1gMnPc1	generál
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němci	Němec	k1gMnPc1	Němec
chystají	chystat	k5eAaImIp3nP	chystat
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
mocnější	mocný	k2eAgInSc4d2	mocnější
úder	úder	k1gInSc4	úder
na	na	k7c4	na
Moskvu	Moskva	k1gFnSc4	Moskva
a	a	k8xC	a
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
zde	zde	k6eAd1	zde
veškeré	veškerý	k3xTgFnPc4	veškerý
dostupné	dostupný	k2eAgFnPc4d1	dostupná
zálohy	záloha	k1gFnPc4	záloha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
měla	mít	k5eAaImAgFnS	mít
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
obklíčení	obklíčení	k1gNnSc4	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
vlivem	vlivem	k7c2	vlivem
relativní	relativní	k2eAgFnSc2d1	relativní
snadnosti	snadnost	k1gFnSc2	snadnost
německého	německý	k2eAgInSc2d1	německý
postupu	postup	k1gInSc2	postup
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pozměnit	pozměnit	k5eAaPmF	pozměnit
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
polovině	polovina	k1gFnSc6	polovina
armád	armáda	k1gFnPc2	armáda
blížících	blížící	k2eAgFnPc2d1	blížící
se	se	k3xPyFc4	se
ke	k	k7c3	k
Stalingradu	Stalingrad	k1gInSc3	Stalingrad
zamířit	zamířit	k5eAaPmF	zamířit
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
dobyt	dobyt	k2eAgInSc4d1	dobyt
Rostov	Rostov	k1gInSc4	Rostov
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
německé	německý	k2eAgInPc1d1	německý
tanky	tank	k1gInPc1	tank
přehnaly	přehnat	k5eAaPmAgInP	přehnat
přes	přes	k7c4	přes
Kubáň	Kubáň	k1gFnSc4	Kubáň
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
dorazily	dorazit	k5eAaPmAgFnP	dorazit
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
Kavkazu	Kavkaz	k1gInSc3	Kavkaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
spatřeny	spatřen	k2eAgInPc1d1	spatřen
první	první	k4xOgFnPc1	první
vrtné	vrtný	k2eAgFnPc1d1	vrtná
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
bavorští	bavorský	k2eAgMnPc1d1	bavorský
horští	horský	k2eAgMnPc1d1	horský
myslivci	myslivec	k1gMnPc1	myslivec
vlajku	vlajka	k1gFnSc4	vlajka
se	s	k7c7	s
svastikou	svastika	k1gFnSc7	svastika
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Elbrus	Elbrus	k1gInSc4	Elbrus
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkým	velký	k2eAgFnPc3d1	velká
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
a	a	k8xC	a
nedostačující	dostačující	k2eNgFnSc6d1	nedostačující
železniční	železniční	k2eAgFnSc6d1	železniční
síti	síť	k1gFnSc6	síť
se	se	k3xPyFc4	se
však	však	k9	však
vážně	vážně	k6eAd1	vážně
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
zásobování	zásobování	k1gNnSc1	zásobování
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zpomalilo	zpomalit	k5eAaPmAgNnS	zpomalit
další	další	k2eAgInPc4d1	další
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
ofenzívou	ofenzíva	k1gFnSc7	ofenzíva
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
rovněž	rovněž	k9	rovněž
postup	postup	k1gInSc4	postup
německé	německý	k2eAgFnSc2d1	německá
6	[number]	k4	6
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
vstříc	vstříc	k7c3	vstříc
Stalingradu	Stalingrad	k1gInSc3	Stalingrad
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
začala	začít	k5eAaPmAgFnS	začít
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k9	co
asi	asi	k9	asi
1000	[number]	k4	1000
letadel	letadlo	k1gNnPc2	letadlo
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
zasypalo	zasypat	k5eAaPmAgNnS	zasypat
Stalingrad	Stalingrad	k1gInSc1	Stalingrad
zápalnými	zápalný	k2eAgFnPc7d1	zápalná
bombami	bomba	k1gFnPc7	bomba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
proměnily	proměnit	k5eAaPmAgFnP	proměnit
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
v	v	k7c4	v
hromadu	hromada	k1gFnSc4	hromada
sutin	sutina	k1gFnPc2	sutina
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
Volhy	Volha	k1gFnSc2	Volha
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Stalingradu	Stalingrad	k1gInSc3	Stalingrad
Stavka	Stavka	k1gFnSc1	Stavka
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
improvizovanou	improvizovaný	k2eAgFnSc4d1	improvizovaná
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc7	Němec
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
rozhořčeném	rozhořčený	k2eAgInSc6d1	rozhořčený
boji	boj	k1gInSc6	boj
o	o	k7c4	o
každý	každý	k3xTgInSc4	každý
dům	dům	k1gInSc4	dům
a	a	k8xC	a
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zapletli	zaplést	k5eAaPmAgMnP	zaplést
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
náročné	náročný	k2eAgFnSc2d1	náročná
a	a	k8xC	a
vyčerpávající	vyčerpávající	k2eAgFnSc2d1	vyčerpávající
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
rozvalinách	rozvalina	k1gFnPc6	rozvalina
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
,	,	kIx,	,
pokračující	pokračující	k2eAgInPc4d1	pokračující
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
6	[number]	k4	6
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
svých	svůj	k3xOyFgInPc2	svůj
bojových	bojový	k2eAgInPc2d1	bojový
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sovětské	sovětský	k2eAgInPc1d1	sovětský
62	[number]	k4	62
<g/>
.	.	kIx.	.
armádě	armáda	k1gFnSc6	armáda
neustále	neustále	k6eAd1	neustále
proudily	proudit	k5eAaPmAgInP	proudit
přes	přes	k7c4	přes
Volhu	Volha	k1gFnSc4	Volha
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
posily	posila	k1gFnSc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
roztažení	roztažení	k1gNnSc3	roztažení
fronty	fronta	k1gFnSc2	fronta
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
křídla	křídlo	k1gNnSc2	křídlo
tvořena	tvořit	k5eAaImNgFnS	tvořit
méně	málo	k6eAd2	málo
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
rumunskými	rumunský	k2eAgFnPc7d1	rumunská
<g/>
,	,	kIx,	,
maďarskými	maďarský	k2eAgFnPc7d1	maďarská
a	a	k8xC	a
italskými	italský	k2eAgFnPc7d1	italská
divizemi	divize	k1gFnPc7	divize
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
slabosti	slabost	k1gFnSc3	slabost
využili	využít	k5eAaPmAgMnP	využít
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Uran	Uran	k1gMnSc1	Uran
provedli	provést	k5eAaPmAgMnP	provést
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
dva	dva	k4xCgInPc1	dva
mohutné	mohutný	k2eAgInPc1d1	mohutný
údery	úder	k1gInPc1	úder
severozápadně	severozápadně	k6eAd1	severozápadně
a	a	k8xC	a
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
útočící	útočící	k2eAgFnPc1d1	útočící
sovětské	sovětský	k2eAgFnPc1d1	sovětská
fronty	fronta	k1gFnPc1	fronta
setkaly	setkat	k5eAaPmAgFnP	setkat
západně	západně	k6eAd1	západně
od	od	k7c2	od
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
uvržena	uvržen	k2eAgFnSc1d1	uvržena
do	do	k7c2	do
obklíčení	obklíčení	k1gNnSc2	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Ujištěn	ujištěn	k2eAgInSc1d1	ujištěn
Göringovým	Göringový	k2eAgNnSc7d1	Göringový
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zásobit	zásobit	k5eAaPmF	zásobit
obleženou	obležený	k2eAgFnSc4d1	obležená
6	[number]	k4	6
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
letecky	letecky	k6eAd1	letecky
<g/>
,	,	kIx,	,
rozkázal	rozkázat	k5eAaPmAgMnS	rozkázat
Hitler	Hitler	k1gMnSc1	Hitler
jejímu	její	k3xOp3gMnSc3	její
veliteli	velitel	k1gMnSc3	velitel
<g/>
,	,	kIx,	,
generálu	generál	k1gMnSc3	generál
Paulusovi	Paulus	k1gMnSc3	Paulus
<g/>
,	,	kIx,	,
vytrvat	vytrvat	k5eAaPmF	vytrvat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vyproštění	vyproštění	k1gNnSc4	vyproštění
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
trvající	trvající	k2eAgInSc4d1	trvající
sovětský	sovětský	k2eAgInSc4d1	sovětský
nápor	nápor	k1gInSc4	nápor
a	a	k8xC	a
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
kategorický	kategorický	k2eAgInSc4d1	kategorický
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
vyklizením	vyklizení	k1gNnSc7	vyklizení
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
pasti	past	k1gFnSc6	past
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
operace	operace	k1gFnSc1	operace
Saturn	Saturn	k1gInSc1	Saturn
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
Němce	Němec	k1gMnSc4	Němec
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
ve	v	k7c6	v
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
Paulus	Paulus	k1gMnSc1	Paulus
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
výzvu	výzva	k1gFnSc4	výzva
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
Sověti	Sovět	k1gMnPc1	Sovět
obnovili	obnovit	k5eAaPmAgMnP	obnovit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
než	než	k8xS	než
konečně	konečně	k6eAd1	konečně
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Paulus	Paulus	k1gInSc1	Paulus
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
vyhladovělých	vyhladovělý	k2eAgMnPc2d1	vyhladovělý
vojáků	voják	k1gMnPc2	voják
6	[number]	k4	6
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
beznadějné	beznadějný	k2eAgFnSc6d1	beznadějná
situaci	situace	k1gFnSc6	situace
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
se	se	k3xPyFc4	se
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
omezené	omezený	k2eAgInPc1d1	omezený
střety	střet	k1gInPc1	střet
především	především	k9	především
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
stokách	stoka	k1gFnPc6	stoka
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
6	[number]	k4	6
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
padlo	padnout	k5eAaPmAgNnS	padnout
do	do	k7c2	do
sovětského	sovětský	k2eAgNnSc2d1	sovětské
zajetí	zajetí	k1gNnSc2	zajetí
90	[number]	k4	90
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
pouze	pouze	k6eAd1	pouze
6000	[number]	k4	6000
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
vítězství	vítězství	k1gNnSc1	vítězství
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
definitivně	definitivně	k6eAd1	definitivně
zlomilo	zlomit	k5eAaPmAgNnS	zlomit
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
neporazitelnosti	neporazitelnost	k1gFnSc6	neporazitelnost
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
obratu	obrat	k1gInSc2	obrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
.	.	kIx.	.
</s>
<s>
Armády	armáda	k1gFnPc1	armáda
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
Osy	osa	k1gFnSc2	osa
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1942	[number]	k4	1942
zotavovaly	zotavovat	k5eAaImAgFnP	zotavovat
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
náročných	náročný	k2eAgInPc2d1	náročný
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
navštívil	navštívit	k5eAaPmAgInS	navštívit
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
velitelem	velitel	k1gMnSc7	velitel
8	[number]	k4	8
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
jmenován	jmenován	k2eAgMnSc1d1	jmenován
generál	generál	k1gMnSc1	generál
Montgomery	Montgomera	k1gFnSc2	Montgomera
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dodal	dodat	k5eAaPmAgMnS	dodat
britským	britský	k2eAgFnPc3d1	britská
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
novou	nový	k2eAgFnSc4d1	nová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
pokusil	pokusit	k5eAaPmAgMnS	pokusit
znovu	znovu	k6eAd1	znovu
prorazit	prorazit	k5eAaPmF	prorazit
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
odražen	odrazit	k5eAaPmNgInS	odrazit
rozhodnou	rozhodný	k2eAgFnSc7d1	rozhodná
britskou	britský	k2eAgFnSc7d1	britská
obranou	obrana	k1gFnSc7	obrana
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Alam	Alama	k1gFnPc2	Alama
Halfy	halfa	k1gFnSc2	halfa
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgInS	vydat
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
Britové	Brit	k1gMnPc1	Brit
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Montgomery	Montgomera	k1gFnPc4	Montgomera
druhou	druhý	k4xOgFnSc4	druhý
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
po	po	k7c6	po
dvanácti	dvanáct	k4xCc6	dvanáct
dnech	den	k1gInPc6	den
intenzivních	intenzivní	k2eAgInPc2d1	intenzivní
bojů	boj	k1gInPc2	boj
síly	síla	k1gFnSc2	síla
Commonwealthu	Commonwealth	k1gInSc6	Commonwealth
přinutily	přinutit	k5eAaPmAgInP	přinutit
Afrikakorps	Afrikakorps	k1gInSc4	Afrikakorps
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
jednotky	jednotka	k1gFnSc2	jednotka
vylodily	vylodit	k5eAaPmAgFnP	vylodit
v	v	k7c6	v
Casablance	Casablanca	k1gFnSc6	Casablanca
<g/>
,	,	kIx,	,
Oranu	Oran	k1gInSc6	Oran
a	a	k8xC	a
Alžíru	Alžír	k1gInSc6	Alžír
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Torch	Torch	k1gMnSc1	Torch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
nečekaně	nečekaně	k6eAd1	nečekaně
rychle	rychle	k6eAd1	rychle
zareagovali	zareagovat	k5eAaPmAgMnP	zareagovat
přesunutím	přesunutí	k1gNnSc7	přesunutí
vojska	vojsko	k1gNnSc2	vojsko
do	do	k7c2	do
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ustupoval	ustupovat	k5eAaImAgInS	ustupovat
rovněž	rovněž	k9	rovněž
Rommelův	Rommelův	k2eAgInSc1d1	Rommelův
Afrikakorps	Afrikakorps	k1gInSc1	Afrikakorps
<g/>
,	,	kIx,	,
obezřetně	obezřetně	k6eAd1	obezřetně
stíhaný	stíhaný	k2eAgMnSc1d1	stíhaný
Montgomeryho	Montgomery	k1gMnSc2	Montgomery
8	[number]	k4	8
<g/>
.	.	kIx.	.
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
Libye	Libye	k1gFnSc2	Libye
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
opevnil	opevnit	k5eAaPmAgMnS	opevnit
na	na	k7c6	na
tuniských	tuniský	k2eAgFnPc6d1	Tuniská
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
sevření	sevření	k1gNnSc2	sevření
a	a	k8xC	a
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
západ	západ	k1gInSc4	západ
proti	proti	k7c3	proti
americkým	americký	k2eAgFnPc3d1	americká
divizím	divize	k1gFnPc3	divize
v	v	k7c6	v
průsmyku	průsmyk	k1gInSc6	průsmyk
Kaserín	Kaserín	k1gInSc1	Kaserín
<g/>
.	.	kIx.	.
</s>
<s>
Ostřílení	ostřílený	k2eAgMnPc1d1	ostřílený
němečtí	německý	k2eAgMnPc1d1	německý
veteráni	veterán	k1gMnPc1	veterán
zde	zde	k6eAd1	zde
přivodili	přivodit	k5eAaPmAgMnP	přivodit
Američanům	Američan	k1gMnPc3	Američan
vážnou	vážný	k2eAgFnSc4d1	vážná
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nezdaru	nezdar	k1gInSc2	nezdar
rychle	rychle	k6eAd1	rychle
poučili	poučit	k5eAaPmAgMnP	poučit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
Britům	Brit	k1gMnPc3	Brit
podařilo	podařit	k5eAaPmAgNnS	podařit
vymanévrovat	vymanévrovat	k5eAaPmF	vymanévrovat
Rommela	Rommel	k1gMnSc4	Rommel
u	u	k7c2	u
Mareth	Maretha	k1gFnPc2	Maretha
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc1	postavení
Osy	osa	k1gFnSc2	osa
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
začalo	začít	k5eAaPmAgNnS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Spojenci	spojenec	k1gMnPc1	spojenec
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
Osy	osa	k1gFnSc2	osa
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
kapitulovaly	kapitulovat	k5eAaBmAgFnP	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
padlo	padnout	k5eAaPmAgNnS	padnout
275	[number]	k4	275
000	[number]	k4	000
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
evropských	evropský	k2eAgInPc6d1	evropský
státech	stát	k1gInPc6	stát
zavedeny	zavést	k5eAaPmNgFnP	zavést
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
metody	metoda	k1gFnPc1	metoda
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
podrobeným	podrobený	k2eAgNnSc7d1	podrobené
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
nacistických	nacistický	k2eAgFnPc2d1	nacistická
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
rasových	rasový	k2eAgFnPc2d1	rasová
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
idejí	idea	k1gFnPc2	idea
a	a	k8xC	a
záměrů	záměr	k1gInPc2	záměr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
naplnění	naplnění	k1gNnSc3	naplnění
využívali	využívat	k5eAaPmAgMnP	využívat
okupanti	okupant	k1gMnPc1	okupant
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
prostředků	prostředek	k1gInPc2	prostředek
represe	represe	k1gFnSc2	represe
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
obnášely	obnášet	k5eAaImAgFnP	obnášet
především	především	k6eAd1	především
vojenský	vojenský	k2eAgInSc4d1	vojenský
a	a	k8xC	a
policejní	policejní	k2eAgInSc4d1	policejní
útlak	útlak	k1gInSc4	útlak
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc4d1	reprezentovaný
systémem	systém	k1gInSc7	systém
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
a	a	k8xC	a
hrozbou	hrozba	k1gFnSc7	hrozba
odvlečení	odvlečení	k1gNnSc2	odvlečení
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
byly	být	k5eAaImAgFnP	být
postiženy	postihnout	k5eAaPmNgFnP	postihnout
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Represe	represe	k1gFnSc1	represe
a	a	k8xC	a
vykořisťování	vykořisťování	k1gNnSc1	vykořisťování
však	však	k9	však
brzy	brzy	k6eAd1	brzy
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
poražených	poražený	k2eAgInPc2d1	poražený
a	a	k8xC	a
okupovaných	okupovaný	k2eAgInPc2d1	okupovaný
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
stávka	stávka	k1gFnSc1	stávka
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
železničářů	železničář	k1gMnPc2	železničář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
útokům	útok	k1gInPc3	útok
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
odbojové	odbojový	k2eAgFnPc1d1	odbojová
skupiny	skupina	k1gFnPc1	skupina
Résistance	Résistance	k1gFnSc2	Résistance
a	a	k8xC	a
Maquis	maquis	k1gNnSc2	maquis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
působila	působit	k5eAaImAgFnS	působit
podzemní	podzemní	k2eAgFnSc1d1	podzemní
Zemská	zemský	k2eAgFnSc1d1	zemská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
Armija	Armij	k2eAgFnSc1d1	Armija
Krajowa	Krajowa	k1gFnSc1	Krajowa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1944	[number]	k4	1944
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
varšavské	varšavský	k2eAgNnSc4d1	Varšavské
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýše	vysoce	k6eAd3	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
nacistů	nacista	k1gMnPc2	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInSc4d3	nejsilnější
odpor	odpor	k1gInSc4	odpor
převládal	převládat	k5eAaImAgInS	převládat
v	v	k7c6	v
balkánských	balkánský	k2eAgInPc6d1	balkánský
státech	stát	k1gInPc6	stát
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávští	jugoslávský	k2eAgMnPc1d1	jugoslávský
partyzáni	partyzán	k1gMnPc1	partyzán
vedení	vedení	k1gNnSc2	vedení
Titem	Tit	k1gMnSc7	Tit
dokázali	dokázat	k5eAaPmAgMnP	dokázat
osvobodit	osvobodit	k5eAaPmF	osvobodit
rozlehlá	rozlehlý	k2eAgNnPc1d1	rozlehlé
území	území	k1gNnPc1	území
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vázali	vázat	k5eAaImAgMnP	vázat
několik	několik	k4yIc4	několik
divizí	divize	k1gFnPc2	divize
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
řečtí	řecký	k2eAgMnPc1d1	řecký
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
(	(	kIx(	(
<g/>
ELAS	ELAS	kA	ELAS
<g/>
)	)	kIx)	)
a	a	k8xC	a
monarchističtí	monarchistický	k2eAgMnPc1d1	monarchistický
partyzáni	partyzán	k1gMnPc1	partyzán
(	(	kIx(	(
<g/>
EDES	EDES	kA	EDES
<g/>
)	)	kIx)	)
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
hornaté	hornatý	k2eAgNnSc4d1	hornaté
řecké	řecký	k2eAgNnSc4d1	řecké
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
operovaly	operovat	k5eAaImAgFnP	operovat
četné	četný	k2eAgFnPc1d1	četná
odbojové	odbojový	k2eAgFnPc1d1	odbojová
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
prováděly	provádět	k5eAaImAgFnP	provádět
sabotáže	sabotáž	k1gFnPc4	sabotáž
na	na	k7c6	na
komunikačních	komunikační	k2eAgInPc6d1	komunikační
uzlech	uzel	k1gInPc6	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Partyzánský	partyzánský	k2eAgInSc1d1	partyzánský
způsob	způsob	k1gInSc1	způsob
boje	boj	k1gInSc2	boj
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
už	už	k9	už
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
západních	západní	k2eAgFnPc2d1	západní
částí	část	k1gFnPc2	část
země	zem	k1gFnPc1	zem
zahájily	zahájit	k5eAaPmAgFnP	zahájit
odříznuté	odříznutý	k2eAgFnPc4d1	odříznutá
jednotky	jednotka	k1gFnPc4	jednotka
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
okupačním	okupační	k2eAgFnPc3d1	okupační
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
vedli	vést	k5eAaImAgMnP	vést
Němci	Němec	k1gMnPc1	Němec
odlišně	odlišně	k6eAd1	odlišně
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nebo	nebo	k8xC	nebo
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
silné	silný	k2eAgNnSc1d1	silné
komunistické	komunistický	k2eAgNnSc1d1	komunistické
hnutí	hnutí	k1gNnSc1	hnutí
odporu	odpor	k1gInSc2	odpor
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
země	zem	k1gFnSc2	zem
Němci	Němec	k1gMnSc3	Němec
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
spíše	spíše	k9	spíše
ojedinělým	ojedinělý	k2eAgInPc3d1	ojedinělý
případům	případ	k1gInPc3	případ
masakrů	masakr	k1gInPc2	masakr
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
Albánii	Albánie	k1gFnSc6	Albánie
nebo	nebo	k8xC	nebo
SSSR	SSSR	kA	SSSR
nabývala	nabývat	k5eAaImAgFnS	nabývat
podoby	podoba	k1gFnSc2	podoba
nemilosrdného	milosrdný	k2eNgInSc2d1	nemilosrdný
a	a	k8xC	a
systematického	systematický	k2eAgInSc2d1	systematický
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jenom	jenom	k9	jenom
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
takřka	takřka	k6eAd1	takřka
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
veškeré	veškerý	k3xTgFnSc2	veškerý
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Asijské	asijský	k2eAgInPc1d1	asijský
národy	národ	k1gInPc1	národ
okupované	okupovaný	k2eAgInPc1d1	okupovaný
císařskou	císařský	k2eAgFnSc7d1	císařská
armádou	armáda	k1gFnSc7	armáda
byly	být	k5eAaImAgFnP	být
Japonci	Japonec	k1gMnPc1	Japonec
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
východoasijské	východoasijský	k2eAgFnSc2d1	východoasijská
sféry	sféra	k1gFnSc2	sféra
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
vítáni	vítat	k5eAaImNgMnP	vítat
jako	jako	k9	jako
osvoboditelé	osvoboditel	k1gMnPc1	osvoboditel
z	z	k7c2	z
nadvlády	nadvláda	k1gFnSc2	nadvláda
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
brutalita	brutalita	k1gFnSc1	brutalita
vůči	vůči	k7c3	vůči
Asiatům	Asiat	k1gMnPc3	Asiat
zvedla	zvednout	k5eAaPmAgFnS	zvednout
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
brzy	brzy	k6eAd1	brzy
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Italské	italský	k2eAgNnSc1d1	italské
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1943	[number]	k4	1943
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
Casablanská	casablanský	k2eAgFnSc1d1	casablanský
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
Churchill	Churchill	k1gMnSc1	Churchill
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
k	k	k7c3	k
upřednostnění	upřednostnění	k1gNnSc3	upřednostnění
invaze	invaze	k1gFnSc2	invaze
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
před	před	k7c7	před
přímým	přímý	k2eAgInSc7d1	přímý
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Realizace	realizace	k1gFnSc1	realizace
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
započala	započnout	k5eAaPmAgFnS	započnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
britsko-kanadsko-americké	britskoanadskomerický	k2eAgFnPc1d1	britsko-kanadsko-americký
síly	síla	k1gFnPc1	síla
zachytily	zachytit	k5eAaPmAgFnP	zachytit
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
houževnatému	houževnatý	k2eAgInSc3d1	houževnatý
odporu	odpor	k1gInSc3	odpor
německých	německý	k2eAgFnPc2d1	německá
divizí	divize	k1gFnPc2	divize
nebyla	být	k5eNaImAgFnS	být
vojska	vojsko	k1gNnSc2	vojsko
Osy	osa	k1gFnSc2	osa
schopná	schopný	k2eAgFnSc1d1	schopná
zabránit	zabránit	k5eAaPmF	zabránit
Spojencům	spojenec	k1gMnPc3	spojenec
v	v	k7c6	v
dobytí	dobytí	k1gNnSc6	dobytí
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ještě	ještě	k9	ještě
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
Sicílie	Sicílie	k1gFnSc2	Sicílie
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
stihla	stihnout	k5eAaPmAgFnS	stihnout
Osa	osa	k1gFnSc1	osa
evakuovat	evakuovat	k5eAaBmF	evakuovat
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zbaven	zbaven	k2eAgMnSc1d1	zbaven
moci	moct	k5eAaImF	moct
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
však	však	k9	však
osvobozen	osvobozen	k2eAgInSc1d1	osvobozen
německým	německý	k2eAgNnSc7d1	německé
komandem	komando	k1gNnSc7	komando
a	a	k8xC	a
na	na	k7c6	na
Němci	Němec	k1gMnPc7	Němec
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
území	území	k1gNnSc6	území
později	pozdě	k6eAd2	pozdě
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
loutkovou	loutkový	k2eAgFnSc4d1	loutková
Italskou	italský	k2eAgFnSc4d1	italská
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
se	s	k7c7	s
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
podepsání	podepsání	k1gNnSc6	podepsání
kapitulace	kapitulace	k1gFnSc2	kapitulace
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
překročili	překročit	k5eAaPmAgMnP	překročit
Britové	Brit	k1gMnPc1	Brit
Messinský	messinský	k2eAgInSc4d1	messinský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
italské	italský	k2eAgFnSc2d1	italská
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
vylodili	vylodit	k5eAaPmAgMnP	vylodit
u	u	k7c2	u
Salerna	Salerna	k1gFnSc1	Salerna
nedaleko	nedaleko	k7c2	nedaleko
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ale	ale	k9	ale
museli	muset	k5eAaImAgMnP	muset
čelit	čelit	k5eAaImF	čelit
prudkému	prudký	k2eAgInSc3d1	prudký
německému	německý	k2eAgInSc3d1	německý
protiútoku	protiútok	k1gInSc3	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ještě	ještě	k9	ještě
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
zimy	zima	k1gFnSc2	zima
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
na	na	k7c4	na
Gustavovu	Gustavův	k2eAgFnSc4d1	Gustavova
linii	linie	k1gFnSc4	linie
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hodlali	hodlat	k5eAaImAgMnP	hodlat
využít	využít	k5eAaPmF	využít
přirozeného	přirozený	k2eAgInSc2d1	přirozený
defenzivního	defenzivní	k2eAgInSc2d1	defenzivní
terénu	terén	k1gInSc2	terén
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
k	k	k7c3	k
co	co	k9	co
největšímu	veliký	k2eAgNnSc3d3	veliký
zpomalení	zpomalení	k1gNnSc3	zpomalení
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
postupu	postup	k1gInSc2	postup
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1944	[number]	k4	1944
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusili	pokusit	k5eAaPmAgMnP	pokusit
prolomit	prolomit	k5eAaPmF	prolomit
německou	německý	k2eAgFnSc4d1	německá
obranu	obrana	k1gFnSc4	obrana
vyloděním	vylodění	k1gNnSc7	vylodění
u	u	k7c2	u
Anzia	Anzium	k1gNnSc2	Anzium
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
Gustavovy	Gustavův	k2eAgFnSc2d1	Gustavova
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
začala	začít	k5eAaPmAgFnS	začít
namáhavá	namáhavý	k2eAgFnSc1d1	namáhavá
a	a	k8xC	a
zdlouhavá	zdlouhavý	k2eAgFnSc1d1	zdlouhavá
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Monte	Mont	k1gInSc5	Mont
Cassino	Cassin	k2eAgNnSc5d1	Cassino
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
podnikli	podniknout	k5eAaPmAgMnP	podniknout
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc4	čtyři
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
se	se	k3xPyFc4	se
Polákům	Polák	k1gMnPc3	Polák
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
dobýt	dobýt	k5eAaPmF	dobýt
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
spojenečtí	spojenecký	k2eAgMnPc1d1	spojenecký
vojáci	voják	k1gMnPc1	voják
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
na	na	k7c4	na
Gótskou	gótský	k2eAgFnSc4d1	gótská
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
protínající	protínající	k2eAgInSc4d1	protínající
italský	italský	k2eAgInSc4d1	italský
poloostrov	poloostrov	k1gInSc4	poloostrov
mezi	mezi	k7c7	mezi
Pisou	Pisa	k1gFnSc7	Pisa
a	a	k8xC	a
Rimini	Rimin	k1gMnPc1	Rimin
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlý	rychlý	k2eAgInSc1d1	rychlý
postup	postup	k1gInSc1	postup
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
Itálii	Itálie	k1gFnSc4	Itálie
by	by	kYmCp3nP	by
Spojencům	spojenec	k1gMnPc3	spojenec
umožnil	umožnit	k5eAaPmAgInS	umožnit
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
zimy	zima	k1gFnSc2	zima
1944	[number]	k4	1944
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
armády	armáda	k1gFnSc2	armáda
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
zdolat	zdolat	k5eAaPmF	zdolat
německý	německý	k2eAgInSc4d1	německý
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Kurském	kurský	k2eAgInSc6d1	kurský
oblouku	oblouk	k1gInSc6	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
operace	operace	k1gFnSc2	operace
Saturn	Saturn	k1gMnSc1	Saturn
přišel	přijít	k5eAaPmAgMnS	přijít
Wehrmacht	wehrmacht	k1gInSc4	wehrmacht
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Rostovem	Rostov	k1gInSc7	Rostov
a	a	k8xC	a
Charkovem	Charkov	k1gInSc7	Charkov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
německém	německý	k2eAgInSc6d1	německý
protiútoku	protiútok	k1gInSc6	protiútok
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
čelili	čelit	k5eAaImAgMnP	čelit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sokolova	Sokolov	k1gInSc2	Sokolov
i	i	k8xC	i
vojáci	voják	k1gMnPc1	voják
1	[number]	k4	1
<g/>
.	.	kIx.	.
československého	československý	k2eAgInSc2d1	československý
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
však	však	k9	však
Němci	Němec	k1gMnPc1	Němec
opět	opět	k6eAd1	opět
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Charkov	Charkov	k1gInSc4	Charkov
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
výduť	výduť	k1gFnSc4	výduť
u	u	k7c2	u
města	město	k1gNnSc2	město
Kursk	Kursk	k1gInSc1	Kursk
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
jarní	jarní	k2eAgInPc1d1	jarní
deště	dešť	k1gInPc1	dešť
přerušily	přerušit	k5eAaPmAgInP	přerušit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1943	[number]	k4	1943
boje	boj	k1gInSc2	boj
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
do	do	k7c2	do
plánování	plánování	k1gNnSc2	plánování
nové	nový	k2eAgFnSc2d1	nová
letní	letní	k2eAgFnSc2d1	letní
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
obklíčení	obklíčení	k1gNnSc4	obklíčení
a	a	k8xC	a
zničení	zničení	k1gNnSc4	zničení
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
výběžku	výběžek	k1gInSc6	výběžek
u	u	k7c2	u
města	město	k1gNnSc2	město
Kursk	Kursk	k1gInSc1	Kursk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
dřívějšími	dřívější	k2eAgFnPc7d1	dřívější
letními	letní	k2eAgFnPc7d1	letní
kampaněmi	kampaň	k1gFnPc7	kampaň
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mnohem	mnohem	k6eAd1	mnohem
skromnější	skromný	k2eAgInSc4d2	skromnější
operační	operační	k2eAgInSc4d1	operační
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Stavka	Stavka	k1gFnSc1	Stavka
byla	být	k5eAaImAgFnS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sovětské	sovětský	k2eAgFnSc2d1	sovětská
rozvědky	rozvědka	k1gFnSc2	rozvědka
dostatečně	dostatečně	k6eAd1	dostatečně
informována	informován	k2eAgFnSc1d1	informována
o	o	k7c6	o
německých	německý	k2eAgInPc6d1	německý
úmyslech	úmysl	k1gInPc6	úmysl
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
proto	proto	k8xC	proto
kurský	kurský	k2eAgInSc1d1	kurský
oblouk	oblouk	k1gInSc1	oblouk
značně	značně	k6eAd1	značně
opevnit	opevnit	k5eAaPmF	opevnit
vytvořením	vytvoření	k1gNnSc7	vytvoření
hustých	hustý	k2eAgInPc2d1	hustý
minových	minový	k2eAgInPc6d1	minový
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
protitankových	protitankový	k2eAgFnPc2d1	protitanková
pastí	past	k1gFnPc2	past
<g/>
,	,	kIx,	,
rozmístěním	rozmístění	k1gNnSc7	rozmístění
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
protitankových	protitankový	k2eAgNnPc2d1	protitankové
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
zřízením	zřízení	k1gNnSc7	zřízení
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
vedených	vedený	k2eAgFnPc2d1	vedená
linií	linie	k1gFnPc2	linie
zákopů	zákop	k1gInPc2	zákop
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kurska	Kursk	k1gInSc2	Kursk
začala	začít	k5eAaPmAgFnS	začít
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
německým	německý	k2eAgInSc7d1	německý
úderem	úder	k1gInSc7	úder
severně	severně	k6eAd1	severně
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jádro	jádro	k1gNnSc1	jádro
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
představovaly	představovat	k5eAaImAgFnP	představovat
pancéřové	pancéřový	k2eAgFnPc1d1	pancéřová
divize	divize	k1gFnPc1	divize
tvořené	tvořený	k2eAgFnPc1d1	tvořená
také	také	k9	také
novými	nový	k2eAgInPc7d1	nový
typy	typ	k1gInPc7	typ
tanků	tank	k1gInPc2	tank
včetně	včetně	k7c2	včetně
Tigrů	Tigr	k1gInPc2	Tigr
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
střetnutí	střetnutí	k1gNnSc1	střetnutí
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
tankovou	tankový	k2eAgFnSc7d1	tanková
bitvou	bitva	k1gFnSc7	bitva
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
ohromné	ohromný	k2eAgFnSc3d1	ohromná
koncentraci	koncentrace	k1gFnSc3	koncentrace
tankových	tankový	k2eAgFnPc2d1	tanková
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
německým	německý	k2eAgFnPc3d1	německá
pancéřovým	pancéřový	k2eAgFnPc3d1	pancéřová
kleštím	kleště	k1gFnPc3	kleště
nepodařilo	podařit	k5eNaPmAgNnS	podařit
spojit	spojit	k5eAaPmF	spojit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Hitler	Hitler	k1gMnSc1	Hitler
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zastavení	zastavení	k1gNnSc1	zastavení
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
marném	marný	k2eAgInSc6d1	marný
boji	boj	k1gInSc6	boj
byly	být	k5eAaImAgFnP	být
promrhány	promrhán	k2eAgFnPc1d1	promrhána
německé	německý	k2eAgFnPc1d1	německá
tankové	tankový	k2eAgFnPc1d1	tanková
zálohy	záloha	k1gFnPc1	záloha
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
iniciativa	iniciativa	k1gFnSc1	iniciativa
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
přešla	přejít	k5eAaPmAgFnS	přejít
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
zahájili	zahájit	k5eAaPmAgMnP	zahájit
vlastní	vlastní	k2eAgInSc4d1	vlastní
protiútok	protiútok	k1gInSc4	protiútok
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
dobyli	dobýt	k5eAaPmAgMnP	dobýt
těžce	těžce	k6eAd1	těžce
zkoušený	zkoušený	k2eAgInSc4d1	zkoušený
Charkov	Charkov	k1gInSc4	Charkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Smolensk	Smolensk	k1gInSc4	Smolensk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
Sověty	Sověty	k1gInPc4	Sověty
osvobozen	osvobozen	k2eAgInSc4d1	osvobozen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zadržet	zadržet	k5eAaPmF	zadržet
stupňující	stupňující	k2eAgInSc4d1	stupňující
se	se	k3xPyFc4	se
nápor	nápor	k1gInSc4	nápor
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
vybudováním	vybudování	k1gNnSc7	vybudování
Východního	východní	k2eAgInSc2d1	východní
valu	val	k1gInSc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
obranná	obranný	k2eAgFnSc1d1	obranná
linie	linie	k1gFnSc1	linie
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
záhy	záhy	k6eAd1	záhy
proražena	prorazit	k5eAaPmNgFnS	prorazit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Dněpr	Dněpr	k1gInSc4	Dněpr
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Sověti	Sovět	k1gMnPc1	Sovět
řadu	řad	k1gInSc2	řad
předmostí	předmostí	k1gNnSc2	předmostí
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
tohoto	tento	k3xDgInSc2	tento
toku	tok	k1gInSc2	tok
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
dobyli	dobýt	k5eAaPmAgMnP	dobýt
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
československých	československý	k2eAgMnPc2d1	československý
vojáků	voják	k1gMnPc2	voják
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
neustával	ustávat	k5eNaImAgInS	ustávat
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
leningradsko-novgorodské	leningradskoovgorodský	k2eAgFnSc2d1	leningradsko-novgorodský
operace	operace	k1gFnSc2	operace
prolomena	prolomen	k2eAgFnSc1d1	prolomena
blokáda	blokáda	k1gFnSc1	blokáda
Leningradu	Leningrad	k1gInSc2	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zatím	zatím	k6eAd1	zatím
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
osvobozování	osvobozování	k1gNnSc1	osvobozování
Pravobřežní	pravobřežní	k2eAgFnSc2d1	pravobřežní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
žytomyrsko-berdyčevské	žytomyrskoerdyčevský	k2eAgFnSc2d1	žytomyrsko-berdyčevský
a	a	k8xC	a
korsuň-ševčenkovské	korsuň-ševčenkovský	k2eAgFnSc2d1	korsuň-ševčenkovský
operace	operace	k1gFnSc2	operace
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
příslušníci	příslušník	k1gMnPc1	příslušník
1	[number]	k4	1
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
brigády	brigáda	k1gFnSc2	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
části	část	k1gFnSc2	část
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
podařilo	podařit	k5eAaPmAgNnS	podařit
vyváznout	vyváznout	k5eAaPmF	vyváznout
z	z	k7c2	z
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
obklíčení	obklíčení	k1gNnSc2	obklíčení
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kamence	Kamenec	k1gInSc2	Kamenec
Podolského	podolský	k2eAgInSc2d1	podolský
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgFnPc1d1	pokračující
sovětské	sovětský	k2eAgFnPc1d1	sovětská
ofenzívy	ofenzíva	k1gFnPc1	ofenzíva
zatlačily	zatlačit	k5eAaPmAgFnP	zatlačit
Němce	Němec	k1gMnPc4	Němec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Prut	prut	k1gInSc4	prut
na	na	k7c6	na
rumunských	rumunský	k2eAgFnPc6d1	rumunská
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
tanky	tank	k1gInPc1	tank
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
probily	probít	k5eAaPmAgFnP	probít
až	až	k9	až
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Lvova	Lvov	k1gInSc2	Lvov
<g/>
,	,	kIx,	,
když	když	k8xS	když
předtím	předtím	k6eAd1	předtím
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
sovětsko-polské	sovětskoolský	k2eAgFnPc4d1	sovětsko-polská
hranice	hranice	k1gFnPc4	hranice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
německých	německý	k2eAgFnPc2d1	německá
a	a	k8xC	a
rumunských	rumunský	k2eAgFnPc2d1	rumunská
divizí	divize	k1gFnPc2	divize
byla	být	k5eAaImAgFnS	být
odříznuta	odříznout	k5eAaPmNgFnS	odříznout
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
dílem	dílem	k6eAd1	dílem
zlikvidována	zlikvidovat	k5eAaPmNgFnS	zlikvidovat
při	při	k7c6	při
krymské	krymský	k2eAgFnSc6d1	Krymská
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
dílem	dílem	k6eAd1	dílem
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
evakuovat	evakuovat	k5eAaBmF	evakuovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Guadalcanal	Guadalcanal	k1gFnSc6	Guadalcanal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
japonské	japonský	k2eAgFnSc2d1	japonská
expanze	expanze	k1gFnSc2	expanze
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
americké	americký	k2eAgNnSc1d1	americké
vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1942	[number]	k4	1942
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
námořních	námořní	k2eAgFnPc2d1	námořní
a	a	k8xC	a
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
Šalamounovy	Šalamounův	k2eAgInPc4d1	Šalamounův
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
Novou	nový	k2eAgFnSc4d1	nová
Guineu	Guinea	k1gFnSc4	Guinea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
eliminována	eliminovat	k5eAaBmNgFnS	eliminovat
klíčová	klíčový	k2eAgFnSc1d1	klíčová
japonská	japonský	k2eAgFnSc1d1	japonská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
základna	základna	k1gFnSc1	základna
v	v	k7c6	v
Rabaulu	Rabaulum	k1gNnSc6	Rabaulum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
obsazení	obsazení	k1gNnSc3	obsazení
představovalo	představovat	k5eAaImAgNnS	představovat
dobytí	dobytí	k1gNnSc1	dobytí
ostrova	ostrov	k1gInSc2	ostrov
Guadalcanal	Guadalcanal	k1gFnSc2	Guadalcanal
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
měsíci	měsíc	k1gInSc6	měsíc
zahájen	zahájit	k5eAaPmNgInS	zahájit
výsadek	výsadek	k1gInSc1	výsadek
americké	americký	k2eAgFnSc2d1	americká
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1	počáteční
snadné	snadný	k2eAgNnSc1d1	snadné
přistání	přistání	k1gNnSc1	přistání
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
zkomplikoval	zkomplikovat	k5eAaPmAgInS	zkomplikovat
příchod	příchod	k1gInSc1	příchod
japonských	japonský	k2eAgFnPc2d1	japonská
posil	posila	k1gFnPc2	posila
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tokijský	tokijský	k2eAgInSc1d1	tokijský
expres	expres	k1gInSc1	expres
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
svedly	svést	k5eAaPmAgFnP	svést
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
šest	šest	k4xCc4	šest
námořních	námořní	k2eAgFnPc2d1	námořní
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
zuřila	zuřit	k5eAaImAgFnS	zuřit
vyčerpávající	vyčerpávající	k2eAgFnSc1d1	vyčerpávající
materiálová	materiálový	k2eAgFnSc1d1	materiálová
bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Enormní	enormní	k2eAgFnPc1d1	enormní
ztráty	ztráta	k1gFnPc1	ztráta
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
letadel	letadlo	k1gNnPc2	letadlo
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Japonce	Japonec	k1gMnPc4	Japonec
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
loďstva	loďstvo	k1gNnSc2	loďstvo
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Guadalcanal	Guadalcanal	k1gFnSc6	Guadalcanal
skončila	skončit	k5eAaPmAgFnS	skončit
pak	pak	k6eAd1	pak
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
americkým	americký	k2eAgNnSc7d1	americké
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
pokusili	pokusit	k5eAaPmAgMnP	pokusit
proniknout	proniknout	k5eAaPmF	proniknout
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
barmskou	barmský	k2eAgFnSc7d1	barmská
džunglí	džungle	k1gFnSc7	džungle
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Iravádí	Iravádí	k1gNnSc2	Iravádí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
skončila	skončit	k5eAaPmAgFnS	skončit
katastrofálním	katastrofální	k2eAgInSc7d1	katastrofální
ústupem	ústup	k1gInSc7	ústup
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
získali	získat	k5eAaPmAgMnP	získat
Američané	Američan	k1gMnPc1	Američan
zpět	zpět	k6eAd1	zpět
Aleutské	aleutský	k2eAgInPc1d1	aleutský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
hlavní	hlavní	k2eAgFnPc1d1	hlavní
operace	operace	k1gFnPc1	operace
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
vypudili	vypudit	k5eAaPmAgMnP	vypudit
Australané	Australan	k1gMnPc1	Australan
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
MacArthura	MacArthur	k1gMnSc2	MacArthur
Japonce	Japonec	k1gMnSc2	Japonec
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Nové	Nové	k2eAgFnSc2d1	Nové
Guineie	Guineie	k1gFnSc2	Guineie
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
a	a	k8xC	a
novozélandský	novozélandský	k2eAgInSc1d1	novozélandský
postup	postup	k1gInSc1	postup
na	na	k7c6	na
Šalamounových	Šalamounových	k2eAgInPc6d1	Šalamounových
ostrovech	ostrov	k1gInPc6	ostrov
byl	být	k5eAaImAgInS	být
fakticky	fakticky	k6eAd1	fakticky
završen	završit	k5eAaPmNgInS	završit
vyloděním	vylodění	k1gNnSc7	vylodění
na	na	k7c4	na
Bougainville	Bougainville	k1gInSc4	Bougainville
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Američané	Američan	k1gMnPc1	Američan
na	na	k7c4	na
Gilbertovy	Gilbertův	k2eAgInPc4d1	Gilbertův
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
silně	silně	k6eAd1	silně
opevněný	opevněný	k2eAgInSc4d1	opevněný
atol	atol	k1gInSc4	atol
Tarawa	Taraw	k1gInSc2	Taraw
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1944	[number]	k4	1944
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
Američané	Američan	k1gMnPc1	Američan
na	na	k7c4	na
Marshallovy	Marshallův	k2eAgInPc4d1	Marshallův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
tuhým	tuhý	k2eAgInSc7d1	tuhý
odporem	odpor	k1gInSc7	odpor
na	na	k7c6	na
Kwajaleinu	Kwajalein	k1gInSc6	Kwajalein
a	a	k8xC	a
Eniwetoku	Eniwetok	k1gInSc6	Eniwetok
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
japonských	japonský	k2eAgInPc6d1	japonský
nezdarech	nezdar	k1gInPc6	nezdar
náležel	náležet	k5eAaImAgInS	náležet
americkým	americký	k2eAgFnPc3d1	americká
ponorkám	ponorka	k1gFnPc3	ponorka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potápěly	potápět	k5eAaImAgFnP	potápět
japonské	japonský	k2eAgFnPc1d1	japonská
obchodní	obchodní	k2eAgFnPc1d1	obchodní
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
následnému	následný	k2eAgInSc3d1	následný
nedostatku	nedostatek	k1gInSc2	nedostatek
surovin	surovina	k1gFnPc2	surovina
kolabovala	kolabovat	k5eAaImAgFnS	kolabovat
japonská	japonský	k2eAgFnSc1d1	japonská
válečná	válečný	k2eAgFnSc1d1	válečná
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Zásobování	zásobování	k1gNnSc1	zásobování
odloučených	odloučený	k2eAgInPc2d1	odloučený
postů	post	k1gInPc2	post
se	se	k3xPyFc4	se
hroutilo	hroutit	k5eAaImAgNnS	hroutit
<g/>
,	,	kIx,	,
evakuace	evakuace	k1gFnSc1	evakuace
a	a	k8xC	a
přesuny	přesun	k1gInPc1	přesun
jednotek	jednotka	k1gFnPc2	jednotka
musely	muset	k5eAaImAgFnP	muset
provádět	provádět	k5eAaImF	provádět
válečné	válečný	k2eAgFnPc1d1	válečná
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
okolních	okolní	k2eAgInPc2d1	okolní
ostrovů	ostrov	k1gInPc2	ostrov
Spojenci	spojenec	k1gMnSc3	spojenec
izolováno	izolovat	k5eAaBmNgNnS	izolovat
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
Rabaulu	Rabaulum	k1gNnSc6	Rabaulum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
operace	operace	k1gFnSc1	operace
U-Go	U-Go	k6eAd1	U-Go
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
indického	indický	k2eAgInSc2d1	indický
Ásámu	Ásám	k1gInSc2	Ásám
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
dobytím	dobytí	k1gNnSc7	dobytí
by	by	kYmCp3nP	by
Japonci	Japonec	k1gMnPc1	Japonec
znemožnili	znemožnit	k5eAaPmAgMnP	znemožnit
přísun	přísun	k1gInSc4	přísun
zásob	zásoba	k1gFnPc2	zásoba
Čankajškovým	Čankajškův	k2eAgMnPc3d1	Čankajškův
čínským	čínský	k2eAgMnPc3d1	čínský
nacionalistům	nacionalista	k1gMnPc3	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
záměry	záměr	k1gInPc1	záměr
byly	být	k5eAaImAgInP	být
však	však	k9	však
zmařeny	zmařen	k2eAgInPc1d1	zmařen
odhodlanou	odhodlaný	k2eAgFnSc7d1	odhodlaná
obranou	obrana	k1gFnSc7	obrana
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
indických	indický	k2eAgNnPc2d1	indické
měst	město	k1gNnPc2	město
Kohimy	Kohima	k1gFnSc2	Kohima
a	a	k8xC	a
Imphálu	Imphál	k1gInSc2	Imphál
silami	síla	k1gFnPc7	síla
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
současně	současně	k6eAd1	současně
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
japonská	japonský	k2eAgFnSc1d1	japonská
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Iči-Go	Iči-Go	k6eAd1	Iči-Go
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
téměř	téměř	k6eAd1	téměř
úplný	úplný	k2eAgInSc4d1	úplný
kolaps	kolaps	k1gInSc4	kolaps
čínských	čínský	k2eAgNnPc2d1	čínské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
Marianech	Marian	k1gMnPc6	Marian
a	a	k8xC	a
utkali	utkat	k5eAaPmAgMnP	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Japonci	Japonec	k1gMnPc7	Japonec
v	v	k7c6	v
namáhavé	namáhavý	k2eAgFnSc6d1	namáhavá
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Saipan	Saipan	k1gInSc4	Saipan
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
zvrat	zvrat	k1gInSc4	zvrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gNnSc1	jejich
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
a	a	k8xC	a
letectvo	letectvo	k1gNnSc1	letectvo
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
poraženy	porazit	k5eAaPmNgInP	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
ve	v	k7c6	v
Filipínském	filipínský	k2eAgNnSc6d1	filipínské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Marian	Mariana	k1gFnPc2	Mariana
mohly	moct	k5eAaImAgInP	moct
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
vzlétat	vzlétat	k5eAaImF	vzlétat
americké	americký	k2eAgInPc4d1	americký
bombardéry	bombardér	k1gInPc4	bombardér
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
destrukce	destrukce	k1gFnSc1	destrukce
japonských	japonský	k2eAgNnPc2d1	Japonské
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
americký	americký	k2eAgInSc1d1	americký
výsadek	výsadek	k1gInSc1	výsadek
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
vyplulo	vyplout	k5eAaPmAgNnS	vyplout
japonské	japonský	k2eAgNnSc1d1	Japonské
císařské	císařský	k2eAgNnSc1d1	císařské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
posledního	poslední	k2eAgNnSc2d1	poslední
velkého	velký	k2eAgNnSc2d1	velké
střetnutí	střetnutí	k1gNnSc2	střetnutí
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
trvající	trvající	k2eAgFnSc2d1	trvající
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Leyte	Leyt	k1gInSc5	Leyt
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
japonská	japonský	k2eAgFnSc1d1	japonská
flota	flota	k1gFnSc1	flota
navzdory	navzdory	k7c3	navzdory
sebevražedným	sebevražedný	k2eAgInPc3d1	sebevražedný
útokům	útok	k1gInPc3	útok
kamikaze	kamikaze	k1gMnSc2	kamikaze
zcela	zcela	k6eAd1	zcela
rozdrcena	rozdrcen	k2eAgFnSc1d1	rozdrcena
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Strategické	strategický	k2eAgNnSc1d1	strategické
bombardování	bombardování	k1gNnSc1	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
koncepce	koncepce	k1gFnSc2	koncepce
strategického	strategický	k2eAgNnSc2d1	strategické
bombardování	bombardování	k1gNnSc2	bombardování
lze	lze	k6eAd1	lze
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
leteckého	letecký	k2eAgNnSc2d1	letecké
bombardování	bombardování	k1gNnSc2	bombardování
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
týlu	týl	k1gInSc2	týl
rozvíjena	rozvíjet	k5eAaImNgFnS	rozvíjet
především	především	k6eAd1	především
britskými	britský	k2eAgMnPc7d1	britský
a	a	k8xC	a
americkými	americký	k2eAgMnPc7d1	americký
leteckými	letecký	k2eAgMnPc7d1	letecký
teoretiky	teoretik	k1gMnPc7	teoretik
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
tuto	tento	k3xDgFnSc4	tento
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
Legie	legie	k1gFnSc1	legie
Condor	Condora	k1gFnPc2	Condora
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
Guerniku	Guernik	k1gInSc3	Guernik
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Japonci	Japonec	k1gMnPc1	Japonec
napadali	napadat	k5eAaPmAgMnP	napadat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Wu-chan	Wuhan	k1gInSc1	Wu-chan
<g/>
,	,	kIx,	,
Nanking	Nanking	k1gInSc1	Nanking
nebo	nebo	k8xC	nebo
Kanton	Kanton	k1gInSc1	Kanton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
ničivé	ničivý	k2eAgInPc4d1	ničivý
nálety	nálet	k1gInPc4	nálet
proti	proti	k7c3	proti
civilním	civilní	k2eAgInPc3d1	civilní
i	i	k8xC	i
vojenským	vojenský	k2eAgInPc3d1	vojenský
cílům	cíl	k1gInPc3	cíl
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
těžce	těžce	k6eAd1	těžce
byla	být	k5eAaImAgFnS	být
poničena	poničen	k2eAgFnSc1d1	poničena
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
také	také	k9	také
centrum	centrum	k1gNnSc1	centrum
Rotterdamu	Rotterdam	k1gInSc2	Rotterdam
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Bombardování	bombardování	k1gNnSc1	bombardování
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
začalo	začít	k5eAaPmAgNnS	začít
už	už	k6eAd1	už
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
neúspěchu	neúspěch	k1gInSc6	neúspěch
přešla	přejít	k5eAaPmAgFnS	přejít
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1940	[number]	k4	1940
k	k	k7c3	k
nočním	noční	k2eAgInPc3d1	noční
náletům	nálet	k1gInPc3	nálet
na	na	k7c4	na
britská	britský	k2eAgNnPc4d1	Britské
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
Blitz	Blitz	k1gInSc4	Blitz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vybombardovali	vybombardovat	k5eAaPmAgMnP	vybombardovat
Němci	Němec	k1gMnPc1	Němec
Coventry	Coventr	k1gMnPc7	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Bombardovací	bombardovací	k2eAgNnSc4d1	bombardovací
velitelství	velitelství	k1gNnSc4	velitelství
RAF	raf	k0	raf
zintenzívnilo	zintenzívnit	k5eAaPmAgNnS	zintenzívnit
odvetné	odvetný	k2eAgInPc4d1	odvetný
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
průmyslovým	průmyslový	k2eAgInPc3d1	průmyslový
cílům	cíl	k1gInPc3	cíl
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
původně	původně	k6eAd1	původně
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bombardování	bombardování	k1gNnSc1	bombardování
budou	být	k5eAaImBp3nP	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
pouze	pouze	k6eAd1	pouze
letiště	letiště	k1gNnPc4	letiště
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
vojenské	vojenský	k2eAgInPc4d1	vojenský
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgNnPc2	tento
omezení	omezení	k1gNnPc2	omezení
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
upuštěno	upuštěn	k2eAgNnSc1d1	upuštěno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blížícímu	blížící	k2eAgMnSc3d1	blížící
se	se	k3xPyFc4	se
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
omezila	omezit	k5eAaPmAgFnS	omezit
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
další	další	k1gNnSc4	další
bombardování	bombardování	k1gNnSc2	bombardování
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
srovnán	srovnán	k2eAgInSc1d1	srovnán
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Luftwaffe	Luftwaff	k1gMnSc5	Luftwaff
sehrála	sehrát	k5eAaPmAgFnS	sehrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
taktéž	taktéž	k?	taktéž
při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ani	ani	k8xC	ani
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
ani	ani	k8xC	ani
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
nepřivodila	přivodit	k5eNaPmAgFnS	přivodit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
německá	německý	k2eAgFnSc1d1	německá
snaha	snaha	k1gFnSc1	snaha
zbrzdit	zbrzdit	k5eAaPmF	zbrzdit
postup	postup	k1gInSc4	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
strategickým	strategický	k2eAgInPc3d1	strategický
bombardováním	bombardování	k1gNnSc7	bombardování
sovětských	sovětský	k2eAgFnPc2d1	sovětská
továren	továrna	k1gFnPc2	továrna
byla	být	k5eAaImAgFnS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
neexistencí	neexistence	k1gFnSc7	neexistence
vhodného	vhodný	k2eAgInSc2d1	vhodný
typu	typ	k1gInSc2	typ
letounu	letoun	k1gInSc2	letoun
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
doletu	dolet	k1gInSc2	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
USAAF	USAAF	kA	USAAF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
účinnost	účinnost	k1gFnSc4	účinnost
bombardování	bombardování	k1gNnSc2	bombardování
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvyšování	zvyšování	k1gNnSc2	zvyšování
počtu	počet	k1gInSc2	počet
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
,	,	kIx,	,
vylepšování	vylepšování	k1gNnSc3	vylepšování
technických	technický	k2eAgInPc2d1	technický
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
posilování	posilování	k1gNnPc2	posilování
eskort	eskorta	k1gFnPc2	eskorta
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
stíhaček	stíhačka	k1gFnPc2	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
kriticky	kriticky	k6eAd1	kriticky
nízké	nízký	k2eAgFnPc1d1	nízká
efektivity	efektivita	k1gFnPc1	efektivita
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
továrny	továrna	k1gFnPc4	továrna
přešli	přejít	k5eAaPmAgMnP	přejít
Britové	Brit	k1gMnPc1	Brit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
k	k	k7c3	k
plošným	plošný	k2eAgInPc3d1	plošný
náletům	nálet	k1gInPc3	nálet
na	na	k7c4	na
německá	německý	k2eAgNnPc4d1	německé
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
bombardovalo	bombardovat	k5eAaImAgNnS	bombardovat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
–	–	k?	–
po	po	k7c4	po
oslabení	oslabení	k1gNnSc4	oslabení
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
–	–	k?	–
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
k	k	k7c3	k
bombardování	bombardování	k1gNnSc3	bombardování
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
provedla	provést	k5eAaPmAgFnS	provést
RAF	raf	k0	raf
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Lübeck	Lübeck	k1gInSc4	Lübeck
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
německým	německý	k2eAgMnSc7d1	německý
městem	město	k1gNnSc7	město
obráceným	obrácený	k2eAgNnSc7d1	obrácené
v	v	k7c4	v
trosky	troska	k1gFnPc4	troska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
podniklo	podniknout	k5eAaPmAgNnS	podniknout
tisíc	tisíc	k4xCgInSc4	tisíc
britských	britský	k2eAgInPc2d1	britský
bombardérů	bombardér	k1gInPc2	bombardér
nálety	nálet	k1gInPc4	nálet
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
Kolín	Kolín	k1gInSc4	Kolín
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Essen	Essen	k1gInSc4	Essen
a	a	k8xC	a
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
brzy	brzy	k6eAd1	brzy
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
než	než	k8xS	než
ničit	ničit	k5eAaImF	ničit
města	město	k1gNnPc4	město
výbušninami	výbušnina	k1gFnPc7	výbušnina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
je	být	k5eAaImIp3nS	být
zapálit	zapálit	k5eAaPmF	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
svrhávali	svrhávat	k5eAaImAgMnP	svrhávat
na	na	k7c6	na
veliké	veliký	k2eAgFnSc6d1	veliká
ploše	plocha	k1gFnSc6	plocha
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
lehkých	lehký	k2eAgFnPc2d1	lehká
zápalných	zápalný	k2eAgFnPc2d1	zápalná
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
město	město	k1gNnSc4	město
osvětlil	osvětlit	k5eAaPmAgInS	osvětlit
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
bombardérů	bombardér	k1gInPc2	bombardér
měla	mít	k5eAaImAgFnS	mít
těžké	těžký	k2eAgFnPc4d1	těžká
bomby	bomba	k1gFnPc4	bomba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
svrhávala	svrhávat	k5eAaImAgFnS	svrhávat
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Přerušila	přerušit	k5eAaPmAgFnS	přerušit
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
hasiči	hasič	k1gMnPc1	hasič
nedostali	dostat	k5eNaPmAgMnP	dostat
k	k	k7c3	k
požárům	požár	k1gInPc3	požár
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
tím	ten	k3xDgNnSc7	ten
přerušila	přerušit	k5eAaPmAgFnS	přerušit
rozvod	rozvod	k1gInSc4	rozvod
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
prováděné	prováděný	k2eAgFnSc2d1	prováděná
statistiky	statistika	k1gFnSc2	statistika
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sklepní	sklepní	k2eAgInPc1d1	sklepní
kryty	kryt	k1gInPc1	kryt
chránily	chránit	k5eAaImAgInP	chránit
dosti	dosti	k6eAd1	dosti
–	–	k?	–
přímým	přímý	k2eAgInSc7d1	přímý
zásahem	zásah	k1gInSc7	zásah
bomby	bomba	k1gFnSc2	bomba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
jen	jen	k9	jen
5	[number]	k4	5
<g/>
%	%	kIx~	%
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cca	cca	kA	cca
80	[number]	k4	80
<g/>
%	%	kIx~	%
obětí	oběť	k1gFnPc2	oběť
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
udušení	udušení	k1gNnSc2	udušení
kouřem	kouř	k1gInSc7	kouř
z	z	k7c2	z
požárů-	požárů-	k?	požárů-
oproti	oproti	k7c3	oproti
bunkru	bunkr	k1gInSc3	bunkr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
klimatizaci	klimatizace	k1gFnSc4	klimatizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
spálena	spálit	k5eAaPmNgFnS	spálit
na	na	k7c4	na
prach	prach	k1gInSc4	prach
města	město	k1gNnSc2	město
v	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
.	.	kIx.	.
</s>
<s>
Spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
bombardování	bombardování	k1gNnSc1	bombardování
Hamburku	Hamburk	k1gInSc2	Hamburk
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
ohňovou	ohňový	k2eAgFnSc4d1	ohňová
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
padlo	padnout	k5eAaPmAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hamburk	Hamburk	k1gInSc1	Hamburk
hořel	hořet	k5eAaImAgInS	hořet
14	[number]	k4	14
dní	den	k1gInPc2	den
a	a	k8xC	a
nocí	noc	k1gFnPc2	noc
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
zář	zář	k1gFnSc1	zář
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vidět	vidět	k5eAaImF	vidět
na	na	k7c4	na
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
náletům	nálet	k1gInPc3	nálet
vystaven	vystavit	k5eAaPmNgInS	vystavit
také	také	k9	také
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
spojenecké	spojenecký	k2eAgInPc1d1	spojenecký
útoky	útok	k1gInPc1	útok
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
více	hodně	k6eAd2	hodně
na	na	k7c4	na
komunikační	komunikační	k2eAgInPc4d1	komunikační
uzly	uzel	k1gInPc4	uzel
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
německá	německý	k2eAgNnPc1d1	německé
města	město	k1gNnPc1	město
byla	být	k5eAaImAgNnP	být
vytrvale	vytrvale	k6eAd1	vytrvale
bombardována	bombardovat	k5eAaImNgNnP	bombardovat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
zkázu	zkáza	k1gFnSc4	zkáza
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
pokusili	pokusit	k5eAaPmAgMnP	pokusit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1944	[number]	k4	1944
odpovědět	odpovědět	k5eAaPmF	odpovědět
tajnými	tajný	k2eAgFnPc7d1	tajná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
raketami	raketa	k1gFnPc7	raketa
V-1	V-1	k1gMnPc2	V-1
a	a	k8xC	a
V-	V-	k1gFnPc2	V-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
vojenské	vojenský	k2eAgInPc4d1	vojenský
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byly	být	k5eAaImAgFnP	být
uplatňovány	uplatňovat	k5eAaImNgFnP	uplatňovat
proti	proti	k7c3	proti
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
Antverp	Antverpy	k1gFnPc2	Antverpy
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
západoevropských	západoevropský	k2eAgNnPc2d1	západoevropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
provedli	provést	k5eAaPmAgMnP	provést
Spojenci	spojenec	k1gMnPc1	spojenec
mezi	mezi	k7c7	mezi
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1945	[number]	k4	1945
bombardování	bombardování	k1gNnPc2	bombardování
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
mezi	mezi	k7c7	mezi
25	[number]	k4	25
000	[number]	k4	000
až	až	k9	až
40	[number]	k4	40
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
útoků	útok	k1gInPc2	útok
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
kolem	kolem	k7c2	kolem
600	[number]	k4	600
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ochromení	ochromení	k1gNnSc1	ochromení
německé	německý	k2eAgFnSc2d1	německá
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
produkce	produkce	k1gFnSc2	produkce
následkem	následkem	k7c2	následkem
rozvratu	rozvrat	k1gInSc2	rozvrat
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
strategického	strategický	k2eAgNnSc2d1	strategické
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
,	,	kIx,	,
nenastalo	nastat	k5eNaPmAgNnS	nastat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
Američanů	Američan	k1gMnPc2	Američan
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
bombardování	bombardování	k1gNnSc1	bombardování
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
zapojovaly	zapojovat	k5eAaImAgInP	zapojovat
rovněž	rovněž	k9	rovněž
americké	americký	k2eAgInPc1d1	americký
letouny	letoun	k1gInPc1	letoun
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
sedmi	sedm	k4xCc6	sedm
měsících	měsíc	k1gInPc6	měsíc
vyústila	vyústit	k5eAaPmAgFnS	vyústit
letecká	letecký	k2eAgFnSc1d1	letecká
kampaň	kampaň	k1gFnSc1	kampaň
proti	proti	k7c3	proti
Japonsku	Japonsko	k1gNnSc3	Japonsko
ve	v	k7c6	v
zničení	zničení	k1gNnSc3	zničení
takřka	takřka	k6eAd1	takřka
šedesáti	šedesát	k4xCc2	šedesát
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejničivěji	ničivě	k6eAd3	ničivě
byly	být	k5eAaImAgFnP	být
zasaženy	zasažen	k2eAgFnPc1d1	zasažena
velké	velký	k2eAgFnPc1d1	velká
konurbace	konurbace	k1gFnPc1	konurbace
jako	jako	k8xC	jako
Nagoja	Nagoja	k1gFnSc1	Nagoja
<g/>
,	,	kIx,	,
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Kóbe	Kóbe	k1gNnSc1	Kóbe
a	a	k8xC	a
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náletech	nálet	k1gInPc6	nálet
na	na	k7c4	na
Tokio	Tokio	k1gNnSc4	Tokio
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
100	[number]	k4	100
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
sutiny	sutina	k1gFnSc2	sutina
proměnily	proměnit	k5eAaPmAgFnP	proměnit
celé	celý	k2eAgFnPc1d1	celá
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Churchill	Churchill	k1gMnSc1	Churchill
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
setkali	setkat	k5eAaPmAgMnP	setkat
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
projednali	projednat	k5eAaPmAgMnP	projednat
společný	společný	k2eAgInSc4d1	společný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
bude	být	k5eAaImBp3nS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
v	v	k7c6	v
nadcházejícím	nadcházející	k2eAgInSc6d1	nadcházející
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
hodlal	hodlat	k5eAaImAgMnS	hodlat
tomuto	tento	k3xDgMnSc3	tento
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
předejít	předejít	k5eAaPmF	předejít
budováním	budování	k1gNnSc7	budování
silně	silně	k6eAd1	silně
opevněného	opevněný	k2eAgInSc2d1	opevněný
systému	systém	k1gInSc2	systém
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
pevností	pevnost	k1gFnPc2	pevnost
zvaných	zvaný	k2eAgInPc2d1	zvaný
Atlantický	atlantický	k2eAgInSc4d1	atlantický
val	val	k1gInSc4	val
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Overlord	Overlorda	k1gFnPc2	Overlorda
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zněl	znět	k5eAaImAgInS	znět
krycí	krycí	k2eAgInSc1d1	krycí
název	název	k1gInSc1	název
vylodění	vylodění	k1gNnSc2	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
den	den	k1gInSc4	den
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zhruba	zhruba	k6eAd1	zhruba
6500	[number]	k4	6500
spojeneckých	spojenecký	k2eAgNnPc2d1	spojenecké
plavidel	plavidlo	k1gNnPc2	plavidlo
vysadilo	vysadit	k5eAaPmAgNnS	vysadit
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
Normandie	Normandie	k1gFnSc2	Normandie
dvě	dva	k4xCgFnPc1	dva
americké	americký	k2eAgFnPc1d1	americká
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
britské	britský	k2eAgFnPc1d1	britská
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
kanadskou	kanadský	k2eAgFnSc4d1	kanadská
divizi	divize	k1gFnSc4	divize
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
třemi	tři	k4xCgFnPc7	tři
výsadkovými	výsadkový	k2eAgFnPc7d1	výsadková
divizemi	divize	k1gFnPc7	divize
<g/>
,	,	kIx,	,
zajišťujícími	zajišťující	k2eAgInPc7d1	zajišťující
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
týlu	týl	k1gInSc6	týl
křídla	křídlo	k1gNnSc2	křídlo
vyloďujících	vyloďující	k2eAgFnPc2d1	vyloďující
se	se	k3xPyFc4	se
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
12	[number]	k4	12
000	[number]	k4	000
letadly	letadlo	k1gNnPc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
houževnaté	houževnatý	k2eAgFnSc3d1	houževnatá
obraně	obrana	k1gFnSc3	obrana
se	se	k3xPyFc4	se
Němcům	Němec	k1gMnPc3	Němec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zatlačit	zatlačit	k5eAaPmF	zatlačit
útočníky	útočník	k1gMnPc4	útočník
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Spojenci	spojenec	k1gMnSc3	spojenec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
,	,	kIx,	,
dokázali	dokázat	k5eAaPmAgMnP	dokázat
postupně	postupně	k6eAd1	postupně
rozšířit	rozšířit	k5eAaPmF	rozšířit
svoje	svůj	k3xOyFgNnSc4	svůj
předmostí	předmostí	k1gNnSc4	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
dopravili	dopravit	k5eAaPmAgMnP	dopravit
Spojenci	spojenec	k1gMnPc1	spojenec
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
kolem	kolem	k7c2	kolem
850	[number]	k4	850
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
vyčistili	vyčistit	k5eAaPmAgMnP	vyčistit
poloostrov	poloostrov	k1gInSc4	poloostrov
Cotentin	Cotentin	k2eAgInSc4d1	Cotentin
s	s	k7c7	s
přístavem	přístav	k1gInSc7	přístav
Cherbourg	Cherbourg	k1gMnSc1	Cherbourg
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
podnikali	podnikat	k5eAaImAgMnP	podnikat
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Caen	Caen	k1gNnSc4	Caen
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
tuhých	tuhý	k2eAgInPc6d1	tuhý
bojích	boj	k1gInPc6	boj
dobyto	dobyt	k2eAgNnSc1d1	dobyto
Brity	Brit	k1gMnPc4	Brit
a	a	k8xC	a
Kanaďany	Kanaďan	k1gMnPc4	Kanaďan
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Američané	Američan	k1gMnPc1	Američan
operaci	operace	k1gFnSc3	operace
Cobra	Cobr	k1gInSc2	Cobr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
prolomení	prolomení	k1gNnSc3	prolomení
německé	německý	k2eAgFnSc2d1	německá
linie	linie	k1gFnSc2	linie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
provést	provést	k5eAaPmF	provést
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
by	by	kYmCp3nS	by
odřízl	odříznout	k5eAaPmAgInS	odříznout
divize	divize	k1gFnSc2	divize
generála	generál	k1gMnSc2	generál
Pattona	Patton	k1gMnSc2	Patton
pronikající	pronikající	k2eAgFnSc7d1	pronikající
do	do	k7c2	do
Bretaně	Bretaň	k1gFnSc2	Bretaň
a	a	k8xC	a
k	k	k7c3	k
Loiře	Loira	k1gFnSc3	Loira
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
tanky	tank	k1gInPc1	tank
však	však	k9	však
byly	být	k5eAaImAgInP	být
snadno	snadno	k6eAd1	snadno
odraženy	odražen	k2eAgInPc1d1	odražen
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
německých	německý	k2eAgFnPc2d1	německá
pancéřových	pancéřový	k2eAgFnPc2d1	pancéřová
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začala	začít	k5eAaPmAgFnS	začít
u	u	k7c2	u
Falaise	Falaise	k1gFnSc2	Falaise
nebezpečně	bezpečně	k6eNd1	bezpečně
stahovat	stahovat	k5eAaImF	stahovat
smyčka	smyčka	k1gFnSc1	smyčka
hrozící	hrozící	k2eAgFnSc1d1	hrozící
jejich	jejich	k3xOp3gNnSc7	jejich
obklíčením	obklíčení	k1gNnSc7	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Westheer	Westheer	k1gInSc1	Westheer
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pohromy	pohroma	k1gFnSc2	pohroma
a	a	k8xC	a
v	v	k7c6	v
značně	značně	k6eAd1	značně
pošramoceném	pošramocený	k2eAgInSc6d1	pošramocený
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
za	za	k7c4	za
Seinu	Seina	k1gFnSc4	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Vítězní	vítězný	k2eAgMnPc1d1	vítězný
Spojenci	spojenec	k1gMnPc1	spojenec
poté	poté	k6eAd1	poté
za	za	k7c2	za
úzké	úzký	k2eAgFnSc2d1	úzká
součinnosti	součinnost	k1gFnSc2	součinnost
s	s	k7c7	s
francouzským	francouzský	k2eAgNnSc7d1	francouzské
hnutím	hnutí	k1gNnSc7	hnutí
odporu	odpor	k1gInSc2	odpor
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
o	o	k7c4	o
deset	deset	k4xCc4	deset
dnů	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
Američané	Američan	k1gMnPc1	Američan
a	a	k8xC	a
svobodní	svobodný	k2eAgMnPc1d1	svobodný
Francouzi	Francouz	k1gMnPc1	Francouz
operaci	operace	k1gFnSc4	operace
Dragoon	Dragoon	k1gInSc4	Dragoon
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
rychle	rychle	k6eAd1	rychle
postoupili	postoupit	k5eAaPmAgMnP	postoupit
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Rhôny	Rhôna	k1gFnSc2	Rhôna
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
u	u	k7c2	u
Dijonu	Dijon	k1gInSc2	Dijon
se	s	k7c7	s
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
jednotkami	jednotka	k1gFnPc7	jednotka
razícími	razící	k2eAgFnPc7d1	razící
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
německých	německý	k2eAgMnPc2d1	německý
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
vědoma	vědom	k2eAgFnSc1d1	vědoma
si	se	k3xPyFc3	se
bezvýchodnosti	bezvýchodnost	k1gFnPc4	bezvýchodnost
německého	německý	k2eAgNnSc2d1	německé
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
provedla	provést	k5eAaPmAgFnS	provést
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byli	být	k5eAaImAgMnP	být
spiklenci	spiklenec	k1gMnPc1	spiklenec
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
Seiny	Seina	k1gFnSc2	Seina
pronikli	proniknout	k5eAaPmAgMnP	proniknout
Spojenci	spojenec	k1gMnPc1	spojenec
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
k	k	k7c3	k
německým	německý	k2eAgFnPc3d1	německá
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zářijovém	zářijový	k2eAgInSc6d1	zářijový
týdnu	týden	k1gInSc6	týden
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
a	a	k8xC	a
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
také	také	k9	také
Antverpy	Antverpy	k1gFnPc4	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
Šeldy	Šelda	k1gFnSc2	Šelda
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
zdejší	zdejší	k2eAgInSc1d1	zdejší
přístav	přístav	k1gInSc4	přístav
bezcenný	bezcenný	k2eAgInSc4d1	bezcenný
<g/>
,	,	kIx,	,
setrvalo	setrvat	k5eAaPmAgNnS	setrvat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Němců	Němec	k1gMnPc2	Němec
až	až	k6eAd1	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgInPc2d1	jiný
přístavů	přístav	k1gInPc2	přístav
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
a	a	k8xC	a
Atlantiku	Atlantik	k1gInSc6	Atlantik
držely	držet	k5eAaImAgFnP	držet
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Hitlerovými	Hitlerův	k2eAgInPc7d1	Hitlerův
rozkazy	rozkaz	k1gInPc7	rozkaz
německé	německý	k2eAgFnSc2d1	německá
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tím	ten	k3xDgNnSc7	ten
výrazně	výrazně	k6eAd1	výrazně
narušily	narušit	k5eAaPmAgInP	narušit
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
zásobování	zásobování	k1gNnSc1	zásobování
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
přístavy	přístav	k1gInPc4	přístav
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Dunkerque	Dunkerque	k1gInSc1	Dunkerque
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
obléhání	obléhání	k1gNnSc6	obléhání
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
Československá	československý	k2eAgFnSc1d1	Československá
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obrněná	obrněný	k2eAgFnSc1d1	obrněná
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
mezitím	mezitím	k6eAd1	mezitím
konsolidovali	konsolidovat	k5eAaBmAgMnP	konsolidovat
svoje	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
valu	val	k1gInSc6	val
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
Montgomery	Montgomera	k1gFnSc2	Montgomera
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
Market	market	k1gInSc1	market
Garden	Gardno	k1gNnPc2	Gardno
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	nízce	k6eAd2	nízce
měli	mít	k5eAaImAgMnP	mít
Spojenci	spojenec	k1gMnPc1	spojenec
rychle	rychle	k6eAd1	rychle
překonat	překonat	k5eAaPmF	překonat
Rýn	Rýn	k1gInSc4	Rýn
a	a	k8xC	a
vpadnout	vpadnout	k5eAaPmF	vpadnout
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byli	být	k5eAaImAgMnP	být
britští	britský	k2eAgMnPc1d1	britský
a	a	k8xC	a
američtí	americký	k2eAgMnPc1d1	americký
parašutisté	parašutista	k1gMnPc1	parašutista
vysazeni	vysadit	k5eAaPmNgMnP	vysadit
u	u	k7c2	u
mostů	most	k1gInPc2	most
přes	přes	k7c4	přes
Rýn	Rýn	k1gInSc4	Rýn
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
britské	britský	k2eAgInPc1d1	britský
tanky	tank	k1gInPc1	tank
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
výsadkáře	výsadkář	k1gMnSc4	výsadkář
vyprostit	vyprostit	k5eAaPmF	vyprostit
<g/>
.	.	kIx.	.
</s>
<s>
Posledního	poslední	k2eAgInSc2d1	poslední
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Arnhemu	Arnhem	k1gInSc6	Arnhem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
Spojenci	spojenec	k1gMnPc1	spojenec
zmocnit	zmocnit	k5eAaPmF	zmocnit
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
obranou	obrana	k1gFnSc7	obrana
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
si	se	k3xPyFc3	se
Němci	Němec	k1gMnPc1	Němec
zajistili	zajistit	k5eAaPmAgMnP	zajistit
čas	čas	k1gInSc4	čas
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
svých	svůj	k3xOyFgFnPc2	svůj
poničených	poničený	k2eAgFnPc2d1	poničená
divizí	divize	k1gFnPc2	divize
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgFnPc3d1	přetrvávající
zásobovacím	zásobovací	k2eAgFnPc3d1	zásobovací
obtížím	obtíž	k1gFnPc3	obtíž
Spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1944	[number]	k4	1944
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Operace	operace	k1gFnSc1	operace
Bagration	Bagration	k1gInSc1	Bagration
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
třetího	třetí	k4xOgNnSc2	třetí
výročí	výročí	k1gNnPc2	výročí
napadení	napadení	k1gNnSc2	napadení
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
zahájily	zahájit	k5eAaPmAgInP	zahájit
Žukovovy	Žukovův	k2eAgInPc1d1	Žukovův
a	a	k8xC	a
Vasilevského	Vasilevský	k2eAgInSc2d1	Vasilevský
fronty	fronta	k1gFnPc1	fronta
operaci	operace	k1gFnSc3	operace
Bagration	Bagration	k1gInSc1	Bagration
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
směřovala	směřovat	k5eAaImAgFnS	směřovat
proti	proti	k7c3	proti
skupině	skupina	k1gFnSc3	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
generálové	generál	k1gMnPc1	generál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
soustředili	soustředit	k5eAaPmAgMnP	soustředit
hlavní	hlavní	k2eAgFnPc4d1	hlavní
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
nepočítali	počítat	k5eNaImAgMnP	počítat
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
vesměs	vesměs	k6eAd1	vesměs
bažinatém	bažinatý	k2eAgInSc6d1	bažinatý
terénu	terén	k1gInSc6	terén
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
bitvě	bitva	k1gFnSc6	bitva
si	se	k3xPyFc3	se
Sověti	Sovět	k1gMnPc1	Sovět
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
místní	místní	k2eAgFnSc4d1	místní
trojnásobnou	trojnásobný	k2eAgFnSc4d1	trojnásobná
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
pěchotě	pěchota	k1gFnSc6	pěchota
a	a	k8xC	a
disponovali	disponovat	k5eAaBmAgMnP	disponovat
rovněž	rovněž	k9	rovněž
absolutní	absolutní	k2eAgFnSc7d1	absolutní
převahou	převaha	k1gFnSc7	převaha
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Hitler	Hitler	k1gMnSc1	Hitler
zakázal	zakázat	k5eAaPmAgMnS	zakázat
ústup	ústup	k1gInSc4	ústup
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
linie	linie	k1gFnSc1	linie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
útoku	útok	k1gInSc2	útok
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
útočící	útočící	k2eAgInPc1d1	útočící
sovětské	sovětský	k2eAgInPc1d1	sovětský
tanky	tank	k1gInPc1	tank
setkaly	setkat	k5eAaPmAgInP	setkat
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
obklíčily	obklíčit	k5eAaPmAgFnP	obklíčit
a	a	k8xC	a
zničily	zničit	k5eAaPmAgFnP	zničit
jádro	jádro	k1gNnSc4	jádro
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Střed	středa	k1gFnPc2	středa
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
nechal	nechat	k5eAaPmAgMnS	nechat
přemístit	přemístit	k5eAaPmF	přemístit
do	do	k7c2	do
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
strategickým	strategický	k2eAgInSc7d1	strategický
záměrem	záměr	k1gInSc7	záměr
sovětského	sovětský	k2eAgNnSc2d1	sovětské
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Lvovsko-sandoměřskou	Lvovskoandoměřský	k2eAgFnSc4d1	Lvovsko-sandoměřská
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
čelil	čelit	k5eAaImAgInS	čelit
nyní	nyní	k6eAd1	nyní
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
průlomu	průlom	k1gInSc2	průlom
katastrofálních	katastrofální	k2eAgInPc2d1	katastrofální
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Sověti	Sovět	k1gMnPc1	Sovět
Lvov	Lvov	k1gInSc4	Lvov
<g/>
,	,	kIx,	,
východní	východní	k2eAgNnSc1d1	východní
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Vilno	Vilno	k1gNnSc1	Vilno
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ohrozili	ohrozit	k5eAaPmAgMnP	ohrozit
německé	německý	k2eAgFnPc4d1	německá
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
a	a	k8xC	a
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
se	se	k3xPyFc4	se
k	k	k7c3	k
Východnímu	východní	k2eAgNnSc3d1	východní
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
údery	úder	k1gInPc1	úder
uvrhly	uvrhnout	k5eAaPmAgInP	uvrhnout
Ostheer	Ostheer	k1gInSc4	Ostheer
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
kolapsu	kolaps	k1gInSc2	kolaps
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
zastavila	zastavit	k5eAaPmAgFnS	zastavit
na	na	k7c6	na
Visle	Visla	k1gFnSc6	Visla
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
varšavské	varšavský	k2eAgNnSc1d1	Varšavské
povstání	povstání	k1gNnSc1	povstání
vedené	vedený	k2eAgNnSc1d1	vedené
Zemskou	zemský	k2eAgFnSc7d1	zemská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
však	však	k9	však
nacisté	nacista	k1gMnPc1	nacista
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
bojů	boj	k1gInPc2	boj
brutálně	brutálně	k6eAd1	brutálně
potlačili	potlačit	k5eAaPmAgMnP	potlačit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
200	[number]	k4	200
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
spustila	spustit	k5eAaPmAgFnS	spustit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
jasko-kišiněvskou	jaskoišiněvský	k2eAgFnSc4d1	jasko-kišiněvský
operaci	operace	k1gFnSc4	operace
proti	proti	k7c3	proti
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
proněmeckého	proněmecký	k2eAgMnSc2d1	proněmecký
diktátora	diktátor	k1gMnSc2	diktátor
Antonesca	Antonescus	k1gMnSc2	Antonescus
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
rumunského	rumunský	k2eAgNnSc2d1	rumunské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Rumuni	Rumun	k1gMnPc1	Rumun
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
příměří	příměří	k1gNnSc2	příměří
a	a	k8xC	a
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
válku	válka	k1gFnSc4	válka
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozhořelo	rozhořet	k5eAaPmAgNnS	rozhořet
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Sověti	Sovět	k1gMnPc1	Sovět
společně	společně	k6eAd1	společně
s	s	k7c7	s
československými	československý	k2eAgFnPc7d1	Československá
jednotkami	jednotka	k1gFnPc7	jednotka
proto	proto	k8xC	proto
v	v	k7c6	v
září	září	k1gNnSc6	září
zahájili	zahájit	k5eAaPmAgMnP	zahájit
karpatsko-dukelskou	karpatskoukelský	k2eAgFnSc4d1	karpatsko-dukelská
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
navázáno	navázat	k5eAaPmNgNnS	navázat
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
vzpouru	vzpoura	k1gFnSc4	vzpoura
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
během	během	k7c2	během
baltické	baltický	k2eAgFnSc2d1	baltická
operace	operace	k1gFnSc2	operace
zlomili	zlomit	k5eAaPmAgMnP	zlomit
Sověti	Sovět	k1gMnPc1	Sovět
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
odpor	odpor	k1gInSc4	odpor
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
místních	místní	k2eAgMnPc2d1	místní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc4	zbytek
několika	několik	k4yIc2	několik
německých	německý	k2eAgFnPc2d1	německá
divizí	divize	k1gFnPc2	divize
byly	být	k5eAaImAgInP	být
sevřeny	sevřít	k5eAaPmNgInP	sevřít
v	v	k7c6	v
kuronské	kuronský	k2eAgFnSc6d1	kuronský
kapse	kapsa	k1gFnSc6	kapsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
pronikla	proniknout	k5eAaPmAgFnS	proniknout
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
do	do	k7c2	do
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
poté	poté	k6eAd1	poté
změnilo	změnit	k5eAaPmAgNnS	změnit
strany	strana	k1gFnSc2	strana
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
Sověti	Sovět	k1gMnPc1	Sovět
a	a	k8xC	a
jugoslávští	jugoslávský	k2eAgMnPc1d1	jugoslávský
partyzáni	partyzán	k1gMnPc1	partyzán
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
německému	německý	k2eAgNnSc3d1	německé
vyklizení	vyklizení	k1gNnSc3	vyklizení
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
maďarský	maďarský	k2eAgMnSc1d1	maďarský
regent	regent	k1gMnSc1	regent
Miklós	Miklós	k1gInSc4	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sjednat	sjednat	k5eAaPmF	sjednat
příměří	příměří	k1gNnSc4	příměří
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
maďarskými	maďarský	k2eAgMnPc7d1	maďarský
fašisty	fašista	k1gMnPc4	fašista
podporovanými	podporovaný	k2eAgMnPc7d1	podporovaný
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
okupovali	okupovat	k5eAaBmAgMnP	okupovat
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
přes	přes	k7c4	přes
úpornou	úporný	k2eAgFnSc4d1	úporná
obranu	obrana	k1gFnSc4	obrana
německých	německý	k2eAgFnPc2d1	německá
a	a	k8xC	a
maďarských	maďarský	k2eAgFnPc2d1	maďarská
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
podařilo	podařit	k5eAaPmAgNnS	podařit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
dokončit	dokončit	k5eAaPmF	dokončit
obklíčení	obklíčení	k1gNnSc2	obklíčení
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
<g/>
.	.	kIx.	.
</s>
<s>
Podzimního	podzimní	k2eAgNnSc2d1	podzimní
zklidnění	zklidnění	k1gNnSc3	zklidnění
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
využil	využít	k5eAaPmAgMnS	využít
Hitler	Hitler	k1gMnSc1	Hitler
ke	k	k7c3	k
shromáždění	shromáždění	k1gNnSc3	shromáždění
nových	nový	k2eAgFnPc2d1	nová
tankových	tankový	k2eAgFnPc2d1	tanková
záloh	záloha	k1gFnPc2	záloha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
hodlal	hodlat	k5eAaImAgMnS	hodlat
udeřit	udeřit	k5eAaPmF	udeřit
proti	proti	k7c3	proti
americkým	americký	k2eAgFnPc3d1	americká
divizím	divize	k1gFnPc3	divize
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
belgických	belgický	k2eAgFnPc2d1	belgická
nížin	nížina	k1gFnPc2	nížina
a	a	k8xC	a
k	k	k7c3	k
Antverpám	Antverpy	k1gFnPc3	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Dobytím	dobytí	k1gNnSc7	dobytí
tohoto	tento	k3xDgInSc2	tento
přístavu	přístav	k1gInSc2	přístav
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
vrazil	vrazit	k5eAaPmAgMnS	vrazit
klín	klín	k1gInSc4	klín
mezi	mezi	k7c4	mezi
americké	americký	k2eAgFnPc4d1	americká
a	a	k8xC	a
britské	britský	k2eAgFnPc4d1	britská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
Hitlerův	Hitlerův	k2eAgInSc1d1	Hitlerův
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zvrat	zvrat	k1gInSc4	zvrat
vývoje	vývoj	k1gInSc2	vývoj
války	válka	k1gFnSc2	válka
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tři	tři	k4xCgFnPc1	tři
německé	německý	k2eAgFnPc1d1	německá
armády	armáda	k1gFnPc1	armáda
zcela	zcela	k6eAd1	zcela
zaskočily	zaskočit	k5eAaPmAgFnP	zaskočit
své	svůj	k3xOyFgMnPc4	svůj
protivníky	protivník	k1gMnPc4	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Špatné	špatný	k2eAgFnPc1d1	špatná
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
zpočátku	zpočátku	k6eAd1	zpočátku
znemožnily	znemožnit	k5eAaPmAgFnP	znemožnit
Spojencům	spojenec	k1gMnPc3	spojenec
uplatnit	uplatnit	k5eAaPmF	uplatnit
jejich	jejich	k3xOp3gFnSc4	jejich
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
počátečních	počáteční	k2eAgInPc6d1	počáteční
německých	německý	k2eAgInPc6d1	německý
úspěších	úspěch	k1gInPc6	úspěch
však	však	k9	však
energická	energický	k2eAgNnPc4d1	energické
protiopatření	protiopatření	k1gNnPc4	protiopatření
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Němce	Němec	k1gMnPc4	Němec
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
stihli	stihnout	k5eAaPmAgMnP	stihnout
překonat	překonat	k5eAaPmF	překonat
řeku	řeka	k1gFnSc4	řeka
Mázu	máz	k1gInSc2	máz
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
nastalo	nastat	k5eAaPmAgNnS	nastat
zlepšení	zlepšení	k1gNnSc4	zlepšení
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
letectvo	letectvo	k1gNnSc1	letectvo
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
pancéřové	pancéřový	k2eAgFnPc4d1	pancéřová
divize	divize	k1gFnPc4	divize
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tudíž	tudíž	k8xC	tudíž
přešly	přejít	k5eAaPmAgFnP	přejít
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
výběžek	výběžek	k1gInSc4	výběžek
Spojenci	spojenec	k1gMnPc1	spojenec
eliminovali	eliminovat	k5eAaBmAgMnP	eliminovat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
když	když	k8xS	když
zatlačili	zatlačit	k5eAaPmAgMnP	zatlačit
Wehrmacht	wehrmacht	k1gFnPc4	wehrmacht
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
původních	původní	k2eAgFnPc2d1	původní
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ještě	ještě	k9	ještě
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Němci	Němec	k1gMnPc1	Němec
menší	malý	k2eAgFnSc4d2	menší
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
v	v	k7c6	v
Alsasku	Alsasko	k1gNnSc6	Alsasko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
února	únor	k1gInSc2	únor
Spojenci	spojenec	k1gMnPc1	spojenec
vyčistili	vyčistit	k5eAaPmAgMnP	vyčistit
Porýní	Porýní	k1gNnSc4	Porýní
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
překročili	překročit	k5eAaPmAgMnP	překročit
na	na	k7c6	na
široké	široký	k2eAgFnSc6d1	široká
frontě	fronta	k1gFnSc6	fronta
samotný	samotný	k2eAgInSc1d1	samotný
Rýn	Rýn	k1gInSc1	Rýn
a	a	k8xC	a
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Porúří	Porúří	k1gNnSc6	Porúří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
obklíčeno	obklíčit	k5eAaPmNgNnS	obklíčit
a	a	k8xC	a
zajato	zajmout	k5eAaPmNgNnS	zajmout
přes	přes	k7c4	přes
300	[number]	k4	300
000	[number]	k4	000
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Britové	Brit	k1gMnPc1	Brit
překonali	překonat	k5eAaPmAgMnP	překonat
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
řeku	řeka	k1gFnSc4	řeka
Veseru	Vesera	k1gFnSc4	Vesera
a	a	k8xC	a
o	o	k7c4	o
šest	šest	k4xCc4	šest
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Američané	Američan	k1gMnPc1	Američan
Labe	Labe	k1gNnSc2	Labe
u	u	k7c2	u
Magdeburku	Magdeburk	k1gInSc2	Magdeburk
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
pouhých	pouhý	k2eAgFnPc2d1	pouhá
120	[number]	k4	120
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
přenechat	přenechat	k5eAaPmF	přenechat
dobytí	dobytí	k1gNnSc4	dobytí
německého	německý	k2eAgNnSc2d1	německé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
setkali	setkat	k5eAaPmAgMnP	setkat
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
v	v	k7c6	v
Torgau	Torgaus	k1gInSc6	Torgaus
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
3	[number]	k4	3
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
překročila	překročit	k5eAaPmAgFnS	překročit
západní	západní	k2eAgFnSc1d1	západní
hranice	hranice	k1gFnSc1	hranice
prvorepublikového	prvorepublikový	k2eAgNnSc2d1	prvorepublikové
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
Baltu	Balt	k1gInSc3	Balt
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
pokusili	pokusit	k5eAaPmAgMnP	pokusit
vyprostit	vyprostit	k5eAaPmF	vyprostit
obleženou	obležený	k2eAgFnSc4d1	obležená
Budapešť	Budapešť	k1gFnSc4	Budapešť
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gInPc1	jejich
protiútoky	protiútok	k1gInPc1	protiútok
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
jen	jen	k9	jen
omezených	omezený	k2eAgInPc2d1	omezený
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnPc1d1	zbývající
němečtí	německý	k2eAgMnPc1d1	německý
a	a	k8xC	a
maďarští	maďarský	k2eAgMnPc1d1	maďarský
obránci	obránce	k1gMnPc1	obránce
města	město	k1gNnSc2	město
kapitulovali	kapitulovat	k5eAaBmAgMnP	kapitulovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
spustili	spustit	k5eAaPmAgMnP	spustit
Sověti	Sovět	k1gMnPc1	Sovět
viselsko-oderskou	viselskoderský	k2eAgFnSc4d1	viselsko-oderský
operaci	operace	k1gFnSc4	operace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vedla	vést	k5eAaImAgFnS	vést
už	už	k6eAd1	už
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
k	k	k7c3	k
prolomení	prolomení	k1gNnSc3	prolomení
německé	německý	k2eAgFnSc2d1	německá
obranné	obranný	k2eAgFnSc2d1	obranná
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
následně	následně	k6eAd1	následně
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
zcela	zcela	k6eAd1	zcela
zničenou	zničený	k2eAgFnSc4d1	zničená
Varšavu	Varšava	k1gFnSc4	Varšava
a	a	k8xC	a
města	město	k1gNnPc4	město
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
a	a	k8xC	a
jižním	jižní	k2eAgNnSc6d1	jižní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
února	únor	k1gInSc2	únor
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
pouhých	pouhý	k2eAgInPc2d1	pouhý
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
postupu	postup	k1gInSc6	postup
Sověti	Sovět	k1gMnPc1	Sovět
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
objevili	objevit	k5eAaPmAgMnP	objevit
nacistický	nacistický	k2eAgInSc4d1	nacistický
vyhlazovací	vyhlazovací	k2eAgInSc4d1	vyhlazovací
tábor	tábor	k1gInSc4	tábor
Osvětim	Osvětim	k1gFnSc1	Osvětim
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
kolem	kolem	k7c2	kolem
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
východoevropských	východoevropský	k2eAgInPc2d1	východoevropský
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
na	na	k7c6	na
jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
představitelé	představitel	k1gMnPc1	představitel
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Churchill	Churchill	k1gMnSc1	Churchill
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
poválečném	poválečný	k2eAgNnSc6d1	poválečné
uspořádání	uspořádání	k1gNnSc6	uspořádání
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zavázal	zavázat	k5eAaPmAgInS	zavázat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
proti	proti	k7c3	proti
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
vedla	vést	k5eAaImAgFnS	vést
pěchota	pěchota	k1gFnSc1	pěchota
1	[number]	k4	1
<g/>
.	.	kIx.	.
československého	československý	k2eAgInSc2d1	československý
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
tuhé	tuhý	k2eAgInPc1d1	tuhý
dvouměsíční	dvouměsíční	k2eAgInPc1d1	dvouměsíční
boje	boj	k1gInPc1	boj
o	o	k7c4	o
Liptovský	liptovský	k2eAgInSc4d1	liptovský
Mikuláš	mikuláš	k1gInSc4	mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
pobřeží	pobřeží	k1gNnSc2	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
Pomořansku	Pomořansko	k1gNnSc6	Pomořansko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
i	i	k8xC	i
ostravsko-opavská	ostravskopavský	k2eAgFnSc1d1	ostravsko-opavská
a	a	k8xC	a
bratislavsko-brněnská	bratislavskorněnský	k2eAgFnSc1d1	bratislavsko-brněnský
operace	operace	k1gFnSc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Prusku	Prusko	k1gNnSc6	Prusko
odříznuté	odříznutý	k2eAgFnPc1d1	odříznutá
německé	německý	k2eAgFnPc1d1	německá
síly	síla	k1gFnPc1	síla
byly	být	k5eAaImAgFnP	být
neutralizovány	neutralizovat	k5eAaBmNgFnP	neutralizovat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
východopruské	východopruský	k2eAgFnSc2d1	Východopruská
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Sověti	Sovět	k1gMnPc1	Sovět
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
k	k	k7c3	k
závěrečnému	závěrečný	k2eAgNnSc3d1	závěrečné
střetnutí	střetnutí	k1gNnSc3	střetnutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Spojenci	spojenec	k1gMnPc1	spojenec
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
překročili	překročit	k5eAaPmAgMnP	překročit
řeku	řeka	k1gFnSc4	řeka
Pád	Pád	k1gInSc4	Pád
a	a	k8xC	a
dobyli	dobýt	k5eAaPmAgMnP	dobýt
severoitalská	severoitalský	k2eAgNnPc4d1	severoitalské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
dopadli	dopadnout	k5eAaPmAgMnP	dopadnout
italští	italský	k2eAgMnPc1d1	italský
partyzáni	partyzán	k1gMnPc1	partyzán
prchajícího	prchající	k2eAgMnSc2d1	prchající
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
válka	válka	k1gFnSc1	válka
oficiálně	oficiálně	k6eAd1	oficiálně
skončila	skončit	k5eAaPmAgFnS	skončit
kapitulací	kapitulace	k1gFnSc7	kapitulace
zdejších	zdejší	k2eAgFnPc2d1	zdejší
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Berlín	Berlín	k1gInSc4	Berlín
začala	začít	k5eAaPmAgFnS	začít
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
útokem	útok	k1gInSc7	útok
Žukovových	Žukovových	k2eAgFnSc2d1	Žukovových
a	a	k8xC	a
Koněvových	Koněvův	k2eAgFnPc2d1	Koněvova
divizí	divize	k1gFnPc2	divize
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
a	a	k8xC	a
Nise	Nisa	k1gFnSc6	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
týdnu	týden	k1gInSc6	týden
rozhořčených	rozhořčený	k2eAgInPc2d1	rozhořčený
bojů	boj	k1gInPc2	boj
Sověti	Sovět	k1gMnPc1	Sovět
dokončili	dokončit	k5eAaPmAgMnP	dokončit
obklíčení	obklíčení	k1gNnSc4	obklíčení
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
boj	boj	k1gInSc1	boj
o	o	k7c4	o
město	město	k1gNnSc4	město
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
konečné	konečný	k2eAgFnSc2d1	konečná
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pronikli	proniknout	k5eAaPmAgMnP	proniknout
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
spáchal	spáchat	k5eAaPmAgMnS	spáchat
v	v	k7c6	v
podzemním	podzemní	k2eAgNnSc6d1	podzemní
bunkru	bunkr	k1gInSc6	bunkr
pod	pod	k7c7	pod
Říšským	říšský	k2eAgNnSc7d1	říšské
kancléřstvím	kancléřství	k1gNnSc7	kancléřství
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
obránci	obránce	k1gMnPc1	obránce
Berlína	Berlín	k1gInSc2	Berlín
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vzdali	vzdát	k5eAaPmAgMnP	vzdát
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Američané	Američan	k1gMnPc1	Američan
postup	postup	k1gInSc4	postup
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
osu	osa	k1gFnSc4	osa
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osvobození	osvobození	k1gNnSc2	osvobození
západních	západní	k2eAgFnPc2d1	západní
a	a	k8xC	a
jihozápadních	jihozápadní	k2eAgFnPc2d1	jihozápadní
Čech	Čechy	k1gFnPc2	Čechy
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
květnové	květnový	k2eAgNnSc1d1	květnové
povstání	povstání	k1gNnSc1	povstání
českého	český	k2eAgInSc2d1	český
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
Přerova	Přerov	k1gInSc2	Přerov
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
i	i	k9	i
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
začalo	začít	k5eAaPmAgNnS	začít
pražské	pražský	k2eAgNnSc4d1	Pražské
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
poté	poté	k6eAd1	poté
skončilo	skončit	k5eAaPmAgNnS	skončit
podpisem	podpis	k1gInSc7	podpis
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
ústupem	ústup	k1gInSc7	ústup
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Pražské	pražský	k2eAgFnSc2d1	Pražská
operace	operace	k1gFnSc2	operace
sovětské	sovětský	k2eAgFnSc2d1	sovětská
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
sváděly	svádět	k5eAaImAgFnP	svádět
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
boje	boj	k1gInSc2	boj
až	až	k8xS	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
bojem	boj	k1gInSc7	boj
československých	československý	k2eAgFnPc2d1	Československá
jednotek	jednotka	k1gFnPc2	jednotka
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Břest	Břest	k?	Břest
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
skončila	skončit	k5eAaPmAgFnS	skončit
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
bezpodmínečná	bezpodmínečný	k2eAgFnSc1d1	bezpodmínečná
kapitulace	kapitulace	k1gFnSc1	kapitulace
německých	německý	k2eAgFnPc2d1	německá
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
námořní	námořní	k2eAgFnPc1d1	námořní
a	a	k8xC	a
pozemní	pozemní	k2eAgFnPc1d1	pozemní
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
vylodily	vylodit	k5eAaPmAgInP	vylodit
na	na	k7c6	na
klíčovém	klíčový	k2eAgInSc6d1	klíčový
filipínském	filipínský	k2eAgInSc6d1	filipínský
ostrově	ostrov	k1gInSc6	ostrov
Luzon	Luzon	k1gInSc1	Luzon
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
následoval	následovat	k5eAaImAgInS	následovat
výsadek	výsadek	k1gInSc1	výsadek
na	na	k7c4	na
Mindanao	Mindanao	k1gNnSc4	Mindanao
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
čínských	čínský	k2eAgMnPc2d1	čínský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
zahájili	zahájit	k5eAaPmAgMnP	zahájit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1944	[number]	k4	1944
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
proti	proti	k7c3	proti
Japoncům	Japonec	k1gMnPc3	Japonec
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
završili	završit	k5eAaPmAgMnP	završit
obsazením	obsazení	k1gNnSc7	obsazení
Rangúnu	Rangún	k1gInSc2	Rangún
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
dalšího	další	k2eAgInSc2d1	další
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
zatím	zatím	k6eAd1	zatím
Američané	Američan	k1gMnPc1	Američan
svedli	svést	k5eAaPmAgMnP	svést
náročnou	náročný	k2eAgFnSc4d1	náročná
bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
Iwodžimu	Iwodžima	k1gFnSc4	Iwodžima
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
těžkými	těžký	k2eAgFnPc7d1	těžká
ztrátami	ztráta	k1gFnPc7	ztráta
dobyta	dobýt	k5eAaPmNgFnS	dobýt
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
letiště	letiště	k1gNnSc1	letiště
posléze	posléze	k6eAd1	posléze
využilo	využít	k5eAaPmAgNnS	využít
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
k	k	k7c3	k
zintenzivnění	zintenzivnění	k1gNnSc3	zintenzivnění
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
ničivých	ničivý	k2eAgInPc2d1	ničivý
náletů	nálet	k1gInPc2	nálet
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
začala	začít	k5eAaPmAgFnS	začít
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Okinawu	Okinawa	k1gFnSc4	Okinawa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
jen	jen	k6eAd1	jen
500	[number]	k4	500
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Kjúšú	Kjúšú	k1gFnSc2	Kjúšú
<g/>
,	,	kIx,	,
nejjižnějšího	jižní	k2eAgMnSc2d3	nejjižnější
z	z	k7c2	z
japonských	japonský	k2eAgInPc2d1	japonský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
opevněni	opevněn	k2eAgMnPc1d1	opevněn
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Okinawy	Okinawa	k1gFnSc2	Okinawa
vedli	vést	k5eAaImAgMnP	vést
Japonci	Japonec	k1gMnPc1	Japonec
tvrdošíjný	tvrdošíjný	k2eAgInSc4d1	tvrdošíjný
a	a	k8xC	a
fanatický	fanatický	k2eAgInSc4d1	fanatický
odpor	odpor	k1gInSc4	odpor
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
padla	padnout	k5eAaImAgFnS	padnout
nebo	nebo	k8xC	nebo
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
nesmírné	smírný	k2eNgFnPc1d1	nesmírná
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
vůdčí	vůdčí	k2eAgMnPc1d1	vůdčí
představitelé	představitel	k1gMnPc1	představitel
Spojenců	spojenec	k1gMnPc2	spojenec
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
předchozí	předchozí	k2eAgNnSc4d1	předchozí
ujednání	ujednání	k1gNnSc4	ujednání
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
Japonsku	Japonsko	k1gNnSc3	Japonsko
adresována	adresován	k2eAgFnSc1d1	adresována
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
bezpodmínečné	bezpodmínečný	k2eAgFnSc3d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
japonskou	japonský	k2eAgFnSc7d1	japonská
vládou	vláda	k1gFnSc7	vláda
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Truman	Truman	k1gMnSc1	Truman
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
svrhlo	svrhnout	k5eAaPmAgNnS	svrhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bombu	bomba	k1gFnSc4	bomba
na	na	k7c4	na
japonské	japonský	k2eAgNnSc4d1	Japonské
město	město	k1gNnSc4	město
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
další	další	k2eAgInPc1d1	další
na	na	k7c4	na
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
dosud	dosud	k6eAd1	dosud
jediném	jediný	k2eAgInSc6d1	jediný
atomovém	atomový	k2eAgInSc6d1	atomový
úderu	úder	k1gInSc6	úder
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Japonsku	Japonsko	k1gNnSc3	Japonsko
i	i	k8xC	i
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
zahájil	zahájit	k5eAaPmAgInS	zahájit
operaci	operace	k1gFnSc4	operace
Srpnová	srpnový	k2eAgFnSc1d1	srpnová
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
přemohl	přemoct	k5eAaPmAgInS	přemoct
kwantungskou	kwantungský	k2eAgFnSc4d1	kwantungský
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
Japonci	Japonec	k1gMnPc1	Japonec
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
Mandžusku	Mandžusko	k1gNnSc6	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
katastrof	katastrofa	k1gFnPc2	katastrofa
japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
Hirohito	Hirohit	k2eAgNnSc4d1	Hirohito
oznámil	oznámit	k5eAaPmAgInS	oznámit
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
kapitulaci	kapitulace	k1gFnSc4	kapitulace
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
podepsána	podepsán	k2eAgFnSc1d1	podepsána
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
kloní	klonit	k5eAaImIp3nS	klonit
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
ztrát	ztráta	k1gFnPc2	ztráta
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ovšem	ovšem	k9	ovšem
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvážnější	vážní	k2eAgFnPc4d3	nejvážnější
ztráty	ztráta	k1gFnPc4	ztráta
utrpěl	utrpět	k5eAaPmAgInS	utrpět
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Procentuálně	procentuálně	k6eAd1	procentuálně
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
ztrátami	ztráta	k1gFnPc7	ztráta
bylo	být	k5eAaImAgNnS	být
stiženo	stižen	k2eAgNnSc1d1	stiženo
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
pozbylo	pozbýt	k5eAaPmAgNnS	pozbýt
takřka	takřka	k6eAd1	takřka
pětinu	pětina	k1gFnSc4	pětina
své	svůj	k3xOyFgFnSc2	svůj
předválečné	předválečný	k2eAgFnSc2d1	předválečná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
jeho	jeho	k3xOp3gMnPc2	jeho
mrtvých	mrtvý	k1gMnPc2	mrtvý
představovali	představovat	k5eAaImAgMnP	představovat
polští	polský	k2eAgMnPc1d1	polský
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
souhrnného	souhrnný	k2eAgInSc2d1	souhrnný
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
tvořili	tvořit	k5eAaImAgMnP	tvořit
přibližně	přibližně	k6eAd1	přibližně
83	[number]	k4	83
%	%	kIx~	%
příslušníci	příslušník	k1gMnPc1	příslušník
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
států	stát	k1gInPc2	stát
a	a	k8xC	a
17	[number]	k4	17
%	%	kIx~	%
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zemí	zem	k1gFnPc2	zem
účastnících	účastnící	k2eAgFnPc2d1	účastnící
se	se	k3xPyFc4	se
konfliktu	konflikt	k1gInSc3	konflikt
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
,	,	kIx,	,
hladu	hlad	k1gMnSc3	hlad
<g/>
,	,	kIx,	,
masakrům	masakr	k1gInPc3	masakr
nebo	nebo	k8xC	nebo
genocidě	genocida	k1gFnSc6	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nacistických	nacistický	k2eAgInPc6d1	nacistický
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombardování	bombardování	k1gNnSc2	bombardování
a	a	k8xC	a
kolem	kolem	k7c2	kolem
14,5	[number]	k4	14,5
milionů	milion	k4xCgInPc2	milion
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
byla	být	k5eAaImAgFnS	být
důsledkem	důsledek	k1gInSc7	důsledek
genocidy	genocida	k1gFnSc2	genocida
a	a	k8xC	a
válečných	válečný	k2eAgMnPc2d1	válečný
zločinů	zločin	k1gMnPc2	zločin
prováděných	prováděný	k2eAgFnPc2d1	prováděná
německými	německý	k2eAgFnPc7d1	německá
a	a	k8xC	a
japonskými	japonský	k2eAgFnPc7d1	japonská
silami	síla	k1gFnPc7	síla
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Nejodpornější	odporný	k2eAgInSc1d3	nejodpornější
německý	německý	k2eAgInSc1d1	německý
zločin	zločin	k1gInSc1	zločin
představuje	představovat	k5eAaImIp3nS	představovat
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
systematické	systematický	k2eAgNnSc1d1	systematické
a	a	k8xC	a
promyšlené	promyšlený	k2eAgNnSc1d1	promyšlené
vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
teritoriu	teritorium	k1gNnSc6	teritorium
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
životy	život	k1gInPc4	život
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
brutalita	brutalita	k1gFnSc1	brutalita
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
i	i	k9	i
proti	proti	k7c3	proti
dalším	další	k2eAgFnPc3d1	další
skupinám	skupina	k1gFnPc3	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
byly	být	k5eAaImAgFnP	být
vyvražděny	vyvražděn	k2eAgFnPc1d1	vyvražděna
stovky	stovka	k1gFnPc1	stovka
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
za	za	k7c4	za
útoky	útok	k1gInPc4	útok
partyzánů	partyzán	k1gMnPc2	partyzán
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
zahynula	zahynout	k5eAaPmAgFnS	zahynout
asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejotřesnějším	otřesný	k2eAgInSc7d3	nejotřesnější
japonským	japonský	k2eAgInSc7d1	japonský
zločinem	zločin	k1gInSc7	zločin
byl	být	k5eAaImAgMnS	být
nankingský	nankingský	k2eAgInSc4d1	nankingský
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgInP	být
zavražděny	zavraždit	k5eAaPmNgInP	zavraždit
či	či	k8xC	či
znásilněny	znásilněn	k2eAgFnPc4d1	znásilněna
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
čínských	čínský	k2eAgMnPc2d1	čínský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
konfliktu	konflikt	k1gInSc2	konflikt
pak	pak	k6eAd1	pak
japonská	japonský	k2eAgFnSc1d1	japonská
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
přivodila	přivodit	k5eAaPmAgFnS	přivodit
smrt	smrt	k1gFnSc4	smrt
3	[number]	k4	3
až	až	k9	až
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
Německa	Německo	k1gNnSc2	Německo
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Zločiny	zločin	k1gInPc1	zločin
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
se	se	k3xPyFc4	se
nacistické	nacistický	k2eAgFnSc2d1	nacistická
špičky	špička	k1gFnSc2	špička
sešly	sejít	k5eAaPmAgInP	sejít
na	na	k7c4	na
konferenci	konference	k1gFnSc4	konference
ve	v	k7c6	v
Wannsee	Wannsee	k1gFnSc6	Wannsee
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
o	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
řešení	řešení	k1gNnSc6	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
(	(	kIx(	(
<g/>
holokaust	holokaust	k1gInSc1	holokaust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
tohoto	tento	k3xDgInSc2	tento
zvráceného	zvrácený	k2eAgInSc2d1	zvrácený
záměru	záměr	k1gInSc2	záměr
posloužil	posloužit	k5eAaPmAgInS	posloužit
nacistům	nacista	k1gMnPc3	nacista
systém	systém	k1gInSc4	systém
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
a	a	k8xC	a
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
stihl	stihnout	k5eAaPmAgInS	stihnout
také	také	k9	také
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
asi	asi	k9	asi
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
"	"	kIx"	"
<g/>
podlidí	podčlověk	k1gMnPc2	podčlověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Untermenschen	Untermenschen	k1gInSc1	Untermenschen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
,	,	kIx,	,
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgInPc2d1	Jehovův
,	,	kIx,	,
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
úmyslného	úmyslný	k2eAgNnSc2d1	úmyslné
vyhlazování	vyhlazování	k1gNnSc2	vyhlazování
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
a	a	k8xC	a
realizován	realizovat	k5eAaBmNgInS	realizovat
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
provedena	proveden	k2eAgFnSc1d1	provedena
likvidace	likvidace	k1gFnSc1	likvidace
a	a	k8xC	a
etnická	etnický	k2eAgFnSc1d1	etnická
čistka	čistka	k1gFnSc1	čistka
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
německý	německý	k2eAgInSc4d1	německý
národ	národ	k1gInSc4	národ
zajištěn	zajištěn	k2eAgInSc4d1	zajištěn
Lebensraum	Lebensraum	k1gInSc4	Lebensraum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
životní	životní	k2eAgInSc4d1	životní
prostor	prostor	k1gInSc4	prostor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Miliony	milion	k4xCgInPc1	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
totálně	totálně	k6eAd1	totálně
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
válečné	válečný	k2eAgFnSc6d1	válečná
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
nacistických	nacistický	k2eAgInPc6d1	nacistický
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nesčíslné	sčíslný	k2eNgNnSc1d1	nesčíslné
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
v	v	k7c6	v
sovětských	sovětský	k2eAgInPc6d1	sovětský
gulazích	gulag	k1gInPc6	gulag
a	a	k8xC	a
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
občané	občan	k1gMnPc1	občan
okupovaných	okupovaný	k2eAgFnPc2d1	okupovaná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
němečtí	německý	k2eAgMnPc1d1	německý
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
a	a	k8xC	a
sovětští	sovětský	k2eAgMnPc1d1	sovětský
občané	občan	k1gMnPc1	občan
podezřelí	podezřelý	k2eAgMnPc1d1	podezřelý
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
5,7	[number]	k4	5,7
milionů	milion	k4xCgInPc2	milion
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
upadli	upadnout	k5eAaPmAgMnP	upadnout
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
války	válka	k1gFnSc2	válka
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
nebo	nebo	k8xC	nebo
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
hlady	hlady	k6eAd1	hlady
a	a	k8xC	a
na	na	k7c6	na
nemoci	nemoc	k1gFnSc6	nemoc
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
byli	být	k5eAaImAgMnP	být
navíc	navíc	k6eAd1	navíc
pokládáni	pokládán	k2eAgMnPc1d1	pokládán
za	za	k7c4	za
zrádce	zrádce	k1gMnPc4	zrádce
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
byli	být	k5eAaImAgMnP	být
mnozí	mnohý	k2eAgMnPc1d1	mnohý
posláni	poslán	k2eAgMnPc1d1	poslán
do	do	k7c2	do
gulagů	gulag	k1gInPc2	gulag
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
také	také	k9	také
japonské	japonský	k2eAgInPc4d1	japonský
zajatecké	zajatecký	k2eAgInPc4d1	zajatecký
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
pracovní	pracovní	k2eAgInPc4d1	pracovní
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
armáda	armáda	k1gFnSc1	armáda
dále	daleko	k6eAd2	daleko
odvedla	odvést	k5eAaPmAgFnS	odvést
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
čínských	čínský	k2eAgMnPc2d1	čínský
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
pak	pak	k6eAd1	pak
využívala	využívat	k5eAaImAgFnS	využívat
k	k	k7c3	k
otrockým	otrocký	k2eAgFnPc3d1	otrocká
pracím	práce	k1gFnPc3	práce
v	v	k7c6	v
Mandžukuu	Mandžukuum	k1gNnSc6	Mandžukuum
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
omezené	omezený	k2eAgNnSc1d1	omezené
použití	použití	k1gNnSc1	použití
biologických	biologický	k2eAgFnPc2d1	biologická
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
státy	stát	k1gInPc1	stát
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
nasadili	nasadit	k5eAaPmAgMnP	nasadit
yperit	yperit	k1gInSc4	yperit
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
těchto	tento	k3xDgInPc2	tento
smrtících	smrtící	k2eAgInPc2d1	smrtící
prostředků	prostředek	k1gInPc2	prostředek
při	při	k7c6	při
invazi	invaze	k1gFnSc6	invaze
a	a	k8xC	a
okupaci	okupace	k1gFnSc6	okupace
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
i	i	k8xC	i
Japonci	Japonec	k1gMnPc1	Japonec
testovali	testovat	k5eAaImAgMnP	testovat
účinky	účinek	k1gInPc1	účinek
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc3	ničení
na	na	k7c6	na
civilistech	civilista	k1gMnPc6	civilista
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
na	na	k7c6	na
zajatcích	zajatec	k1gMnPc6	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otrocké	otrocký	k2eAgFnSc3d1	otrocká
práci	práce	k1gFnSc3	práce
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
i	i	k9	i
zajatí	zajatý	k1gMnPc1	zajatý
či	či	k8xC	či
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
internovaní	internovaný	k2eAgMnPc1d1	internovaný
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
miliony	milion	k4xCgInPc7	milion
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
takto	takto	k6eAd1	takto
nakládáno	nakládat	k5eAaImNgNnS	nakládat
především	především	k9	především
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
obdobnému	obdobný	k2eAgNnSc3d1	obdobné
zacházení	zacházení	k1gNnSc3	zacházení
byli	být	k5eAaImAgMnP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
i	i	k9	i
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byly	být	k5eAaImAgInP	být
internovány	internovat	k5eAaBmNgInP	internovat
tisíce	tisíc	k4xCgInPc1	tisíc
japonských	japonský	k2eAgMnPc2d1	japonský
<g/>
,	,	kIx,	,
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stejnou	stejný	k2eAgFnSc4d1	stejná
praxi	praxe	k1gFnSc4	praxe
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
i	i	k9	i
kanadská	kanadský	k2eAgFnSc1d1	kanadská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
německé	německý	k2eAgInPc1d1	německý
zločiny	zločin	k1gInPc1	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc4	vedení
útočné	útočný	k2eAgFnSc2d1	útočná
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
míru	mír	k1gInSc3	mír
a	a	k8xC	a
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
potrestány	potrestán	k2eAgInPc4d1	potrestán
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
norimberském	norimberský	k2eAgInSc6d1	norimberský
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
byli	být	k5eAaImAgMnP	být
odsouzeni	odsouzen	k2eAgMnPc1d1	odsouzen
čelní	čelní	k2eAgMnPc1d1	čelní
představitelé	představitel	k1gMnPc1	představitel
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byli	být	k5eAaImAgMnP	být
souzeni	souzen	k2eAgMnPc1d1	souzen
strůjci	strůjce	k1gMnPc1	strůjce
japonských	japonský	k2eAgInPc2d1	japonský
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
v	v	k7c6	v
tokijském	tokijský	k2eAgInSc6d1	tokijský
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
obou	dva	k4xCgMnPc2	dva
těchto	tento	k3xDgInPc2	tento
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
soudních	soudní	k2eAgInPc2d1	soudní
tribunálů	tribunál	k1gInPc2	tribunál
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
základy	základ	k1gInPc7	základ
moderního	moderní	k2eAgNnSc2d1	moderní
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
norimberského	norimberský	k2eAgInSc2d1	norimberský
tribunálu	tribunál	k1gInSc2	tribunál
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vykoná	vykonat	k5eAaPmIp3nS	vykonat
zločinný	zločinný	k2eAgInSc1d1	zločinný
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vinen	vinen	k2eAgMnSc1d1	vinen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
vymlouvat	vymlouvat	k5eAaImF	vymlouvat
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
jenom	jenom	k9	jenom
splnil	splnit	k5eAaPmAgMnS	splnit
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
německá	německý	k2eAgFnSc1d1	německá
ponorka	ponorka	k1gFnSc1	ponorka
U	u	k7c2	u
852	[number]	k4	852
postřílela	postřílet	k5eAaPmAgFnS	postřílet
bezbranné	bezbranný	k2eAgInPc4d1	bezbranný
trosečníky	trosečník	k1gMnPc4	trosečník
kulometem	kulomet	k1gInSc7	kulomet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byli	být	k5eAaImAgMnP	být
aktéři	aktér	k1gMnPc1	aktér
nalezeni	nalézt	k5eAaBmNgMnP	nalézt
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
kapitán	kapitán	k1gMnSc1	kapitán
za	za	k7c2	za
vydání	vydání	k1gNnSc2	vydání
a	a	k8xC	a
voják-	voják-	k?	voják-
kulometčík-	kulometčík-	k?	kulometčík-
za	za	k7c4	za
splnění	splnění	k1gNnSc4	splnění
zločinného	zločinný	k2eAgInSc2d1	zločinný
rozkazu	rozkaz	k1gInSc2	rozkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
německým	německý	k2eAgMnPc3d1	německý
vojákům	voják	k1gMnPc3	voják
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
zdůrazňováno	zdůrazňovat	k5eAaImNgNnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgInSc1d1	daný
rozkaz	rozkaz	k1gInSc1	rozkaz
musí	muset	k5eAaImIp3nS	muset
okamžitě	okamžitě	k6eAd1	okamžitě
splnit	splnit	k5eAaPmF	splnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
za	za	k7c4	za
ten	ten	k3xDgInSc4	ten
rozkaz	rozkaz	k1gInSc4	rozkaz
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
nadřízený	nadřízený	k1gMnSc1	nadřízený
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jim	on	k3xPp3gMnPc3	on
ho	on	k3xPp3gNnSc4	on
dal	dát	k5eAaPmAgInS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
však	však	k9	však
pravidla	pravidlo	k1gNnPc1	pravidlo
určují	určovat	k5eAaImIp3nP	určovat
vítězové	vítěz	k1gMnPc1	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
zločinům	zločin	k1gInPc3	zločin
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
dopustili	dopustit	k5eAaPmAgMnP	dopustit
příslušníci	příslušník	k1gMnPc1	příslušník
států	stát	k1gInPc2	stát
Osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
činy	čina	k1gFnSc2	čina
způsobené	způsobený	k2eAgFnSc2d1	způsobená
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Spojenců	spojenec	k1gMnPc2	spojenec
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
souzeny	souzen	k2eAgFnPc1d1	souzena
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
přesuny	přesun	k1gInPc4	přesun
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
internace	internace	k1gFnSc1	internace
občanů	občan	k1gMnPc2	občan
japonského	japonský	k2eAgInSc2d1	japonský
<g/>
,	,	kIx,	,
německého	německý	k2eAgInSc2d1	německý
a	a	k8xC	a
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc4d1	sovětský
masakr	masakr	k1gInSc4	masakr
polských	polský	k2eAgMnPc2d1	polský
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
Katyni	Katyně	k1gFnSc6	Katyně
a	a	k8xC	a
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
plošné	plošný	k2eAgInPc1d1	plošný
nálety	nálet	k1gInPc1	nálet
na	na	k7c4	na
civilní	civilní	k2eAgInPc4d1	civilní
cíle	cíl	k1gInPc4	cíl
na	na	k7c6	na
nepřátelském	přátelský	k2eNgNnSc6d1	nepřátelské
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
především	především	k9	především
bombardování	bombardování	k1gNnSc1	bombardování
Drážďan	Drážďany	k1gInPc2	Drážďany
a	a	k8xC	a
atomové	atomový	k2eAgInPc4d1	atomový
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
typy	typ	k1gInPc1	typ
letadel	letadlo	k1gNnPc2	letadlo
plnily	plnit	k5eAaImAgInP	plnit
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgInPc1d1	stíhací
a	a	k8xC	a
bombardovací	bombardovací	k2eAgInPc1d1	bombardovací
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
podporovaly	podporovat	k5eAaImAgFnP	podporovat
pozemní	pozemní	k2eAgFnPc4d1	pozemní
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
začala	začít	k5eAaPmAgFnS	začít
letadla	letadlo	k1gNnPc1	letadlo
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
vzdušné	vzdušný	k2eAgFnSc3d1	vzdušná
přepravě	přeprava	k1gFnSc3	přeprava
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc4	vybavení
či	či	k8xC	či
posil	posít	k5eAaPmAgMnS	posít
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jen	jen	k9	jen
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
strategického	strategický	k2eAgNnSc2d1	strategické
bombardování	bombardování	k1gNnSc2	bombardování
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
zalidněným	zalidněný	k2eAgFnPc3d1	zalidněná
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
narušení	narušení	k1gNnSc1	narušení
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
morálky	morálka	k1gFnSc2	morálka
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
pokrok	pokrok	k1gInSc4	pokrok
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
systém	systém	k1gInSc1	systém
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
využíval	využívat	k5eAaPmAgMnS	využívat
radaru	radar	k1gInSc2	radar
a	a	k8xC	a
vylepšeného	vylepšený	k2eAgNnSc2d1	vylepšené
protiletadlového	protiletadlový	k2eAgNnSc2d1	protiletadlové
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
prováděné	prováděný	k2eAgFnSc2d1	prováděná
statistiky	statistika	k1gFnSc2	statistika
však	však	k9	však
překvapivě	překvapivě	k6eAd1	překvapivě
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
vzalo	vzít	k5eAaPmAgNnS	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
množství	množství	k1gNnSc2	množství
nasazené	nasazený	k2eAgFnSc2d1	nasazená
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
efekt	efekt	k1gInSc1	efekt
sestřelených	sestřelený	k2eAgNnPc2d1	sestřelené
letadel	letadlo	k1gNnPc2	letadlo
velmi	velmi	k6eAd1	velmi
nízký-	nízký-	k?	nízký-
nejvíce	hodně	k6eAd3	hodně
letadel	letadlo	k1gNnPc2	letadlo
sestřelily	sestřelit	k5eAaPmAgFnP	sestřelit
stíhačky	stíhačka	k1gFnPc1	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
morální	morální	k2eAgInSc4d1	morální
efekt	efekt	k1gInSc4	efekt
<g/>
:	:	kIx,	:
lidé	člověk	k1gMnPc1	člověk
běželi	běžet	k5eAaImAgMnP	běžet
do	do	k7c2	do
krytu	kryt	k1gInSc2	kryt
a	a	k8xC	a
dunění	dunění	k1gNnPc2	dunění
děl	dělo	k1gNnPc2	dělo
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
letadly	letadlo	k1gNnPc7	letadlo
bojuje	bojovat	k5eAaImIp3nS	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
efekt	efekt	k1gInSc1	efekt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřátelská	přátelský	k2eNgNnPc1d1	nepřátelské
letadla	letadlo	k1gNnPc1	letadlo
byla	být	k5eAaImAgNnP	být
přinucena	přinutit	k5eAaPmNgNnP	přinutit
létat	létat	k5eAaImF	létat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
měla	mít	k5eAaImAgFnS	mít
ztížené	ztížený	k2eAgNnSc4d1	ztížené
přesné	přesný	k2eAgNnSc4d1	přesné
zaměřování	zaměřování	k1gNnSc4	zaměřování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
prvního	první	k4xOgNnSc2	první
omezeného	omezený	k2eAgNnSc2d1	omezené
nasazení	nasazení	k1gNnSc2	nasazení
se	se	k3xPyFc4	se
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dočkala	dočkat	k5eAaPmAgNnP	dočkat
proudová	proudový	k2eAgNnPc1d1	proudové
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
pozdnímu	pozdní	k2eAgNnSc3d1	pozdní
zavedení	zavedení	k1gNnSc3	zavedení
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
sice	sice	k8xC	sice
neměla	mít	k5eNaImAgFnS	mít
vážnější	vážní	k2eAgInSc4d2	vážnější
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
poválečné	poválečný	k2eAgNnSc4d1	poválečné
masové	masový	k2eAgNnSc4d1	masové
rozšíření	rozšíření	k1gNnSc4	rozšíření
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
námořním	námořní	k2eAgNnSc6d1	námořní
válečnictví	válečnictví	k1gNnSc6	válečnictví
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
všestranného	všestranný	k2eAgInSc2d1	všestranný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
rozmach	rozmach	k1gInSc4	rozmach
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
námořní	námořní	k2eAgNnSc1d1	námořní
letectvo	letectvo	k1gNnSc1	letectvo
mohlo	moct	k5eAaImAgNnS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
jen	jen	k9	jen
skromnými	skromný	k2eAgInPc7d1	skromný
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
,	,	kIx,	,
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
Tarent	Tarent	k1gInSc4	Tarent
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbora	k1gFnPc2	Harbora
a	a	k8xC	a
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
Korálovém	korálový	k2eAgNnSc6d1	korálové
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
u	u	k7c2	u
Midway	Midwaa	k1gFnSc2	Midwaa
brzy	brzy	k6eAd1	brzy
povýšily	povýšit	k5eAaPmAgFnP	povýšit
letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
na	na	k7c4	na
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
námořní	námořní	k2eAgFnSc4d1	námořní
zbraň	zbraň	k1gFnSc4	zbraň
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Atlantik	Atlantik	k1gInSc4	Atlantik
se	se	k3xPyFc4	se
doprovodné	doprovodný	k2eAgFnPc1d1	doprovodná
lodě	loď	k1gFnPc1	loď
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
životně	životně	k6eAd1	životně
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
konvojů	konvoj	k1gInPc2	konvoj
výrazně	výrazně	k6eAd1	výrazně
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
jejich	jejich	k3xOp3gFnSc1	jejich
obranyschopnost	obranyschopnost	k1gFnSc1	obranyschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vyšší	vysoký	k2eAgFnSc2d2	vyšší
efektivity	efektivita	k1gFnSc2	efektivita
byly	být	k5eAaImAgFnP	být
letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
ekonomicky	ekonomicky	k6eAd1	ekonomicky
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgFnPc1d1	náročná
než	než	k8xS	než
bitevní	bitevní	k2eAgFnPc1d1	bitevní
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nemusely	muset	k5eNaImAgFnP	muset
být	být	k5eAaImF	být
podobně	podobně	k6eAd1	podobně
silně	silně	k6eAd1	silně
pancéřovány	pancéřován	k2eAgFnPc1d1	pancéřována
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
svoji	svůj	k3xOyFgFnSc4	svůj
ničivou	ničivý	k2eAgFnSc4d1	ničivá
účinnost	účinnost	k1gFnSc4	účinnost
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nasazovány	nasazován	k2eAgInPc1d1	nasazován
všemi	všecek	k3xTgFnPc7	všecek
válčícími	válčící	k2eAgFnPc7d1	válčící
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
protiponorkových	protiponorkový	k2eAgFnPc2d1	protiponorková
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
taktiky	taktika	k1gFnSc2	taktika
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
sonar	sonar	k1gInSc1	sonar
<g/>
,	,	kIx,	,
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
pumy	puma	k1gFnSc2	puma
a	a	k8xC	a
konvoje	konvoj	k1gInSc2	konvoj
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Němci	Němec	k1gMnPc1	Němec
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
svých	svůj	k3xOyFgFnPc2	svůj
útočných	útočný	k2eAgFnPc2d1	útočná
kapacit	kapacita	k1gFnPc2	kapacita
vynalezením	vynalezení	k1gNnSc7	vynalezení
schnorchelu	schnorchel	k1gInSc2	schnorchel
a	a	k8xC	a
uplatňování	uplatňování	k1gNnSc1	uplatňování
taktiky	taktika	k1gFnSc2	taktika
vlčích	vlčí	k2eAgFnPc2d1	vlčí
smeček	smečka	k1gFnPc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgNnSc1d1	pozemní
vojenství	vojenství	k1gNnSc1	vojenství
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
od	od	k7c2	od
statických	statický	k2eAgFnPc2d1	statická
frontových	frontový	k2eAgFnPc2d1	frontová
linií	linie	k1gFnPc2	linie
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
k	k	k7c3	k
dynamičtějšímu	dynamický	k2eAgNnSc3d2	dynamičtější
a	a	k8xC	a
mobilnějšímu	mobilní	k2eAgNnSc3d2	mobilnější
vedení	vedení	k1gNnSc3	vedení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
inovací	inovace	k1gFnSc7	inovace
byl	být	k5eAaImAgInS	být
koncept	koncept	k1gInSc1	koncept
koordinované	koordinovaný	k2eAgFnSc2d1	koordinovaná
podpory	podpora	k1gFnSc2	podpora
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
uplatňován	uplatňován	k2eAgMnSc1d1	uplatňován
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c4	v
primární	primární	k2eAgFnSc4d1	primární
pozemní	pozemní	k2eAgFnSc4d1	pozemní
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
sestrojeny	sestrojen	k2eAgInPc1d1	sestrojen
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
pokročilejší	pokročilý	k2eAgInPc4d2	pokročilejší
typy	typ	k1gInPc4	typ
tankových	tankový	k2eAgFnPc2d1	tanková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
zvyšováním	zvyšování	k1gNnSc7	zvyšování
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
zesílením	zesílení	k1gNnSc7	zesílení
pancíře	pancíř	k1gInSc2	pancíř
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
palebnou	palebný	k2eAgFnSc7d1	palebná
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nejprogresivnější	progresivní	k2eAgFnSc4d3	nejprogresivnější
doktrínu	doktrína	k1gFnSc4	doktrína
použití	použití	k1gNnSc2	použití
tanků	tank	k1gInPc2	tank
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
Němci	Němec	k1gMnSc3	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
přímému	přímý	k2eAgInSc3d1	přímý
střetu	střet	k1gInSc3	střet
vlastních	vlastní	k2eAgInPc2d1	vlastní
tanků	tank	k1gInPc2	tank
s	s	k7c7	s
nepřátelskými	přátelský	k2eNgMnPc7d1	nepřátelský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
ostatních	ostatní	k2eAgFnPc2d1	ostatní
složek	složka	k1gFnPc2	složka
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
především	především	k9	především
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
osvědčilo	osvědčit	k5eAaPmAgNnS	osvědčit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
taktiky	taktika	k1gFnSc2	taktika
Blitzkriegu	Blitzkrieg	k1gInSc2	Blitzkrieg
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
postupně	postupně	k6eAd1	postupně
nabývaly	nabývat	k5eAaImAgFnP	nabývat
různé	různý	k2eAgFnPc1d1	různá
protitankové	protitankový	k2eAgFnPc1d1	protitanková
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
stíhače	stíhač	k1gMnPc4	stíhač
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
střelba	střelba	k1gFnSc1	střelba
<g/>
,	,	kIx,	,
protitanková	protitankový	k2eAgNnPc1d1	protitankové
děla	dělo	k1gNnPc1	dělo
<g/>
,	,	kIx,	,
protitankové	protitankový	k2eAgFnPc1d1	protitanková
miny	mina	k1gFnPc1	mina
a	a	k8xC	a
pěchotní	pěchotní	k2eAgFnPc1d1	pěchotní
protitankové	protitankový	k2eAgFnPc1d1	protitanková
zbraně	zbraň	k1gFnPc1	zbraň
krátkého	krátký	k2eAgInSc2d1	krátký
dostřelu	dostřel	k1gInSc2	dostřel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zvyšující	zvyšující	k2eAgNnSc4d1	zvyšující
se	se	k3xPyFc4	se
úroveň	úroveň	k1gFnSc1	úroveň
mechanizace	mechanizace	k1gFnSc2	mechanizace
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
méně	málo	k6eAd2	málo
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
pěchota	pěchota	k1gFnSc1	pěchota
jádrem	jádro	k1gNnSc7	jádro
každého	každý	k3xTgNnSc2	každý
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pěchotních	pěchotní	k2eAgFnPc2d1	pěchotní
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
příliš	příliš	k6eAd1	příliš
nelišila	lišit	k5eNaImAgFnS	lišit
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Největších	veliký	k2eAgInPc2d3	veliký
pokroků	pokrok	k1gInPc2	pokrok
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
rozšířením	rozšíření	k1gNnSc7	rozšíření
rychle	rychle	k6eAd1	rychle
přenosných	přenosný	k2eAgMnPc2d1	přenosný
kulometů	kulomet	k1gInPc2	kulomet
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
samopalů	samopal	k1gInPc2	samopal
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
obzvláště	obzvláště	k6eAd1	obzvláště
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
zblízka	zblízka	k6eAd1	zblízka
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
zástavbě	zástavba	k1gFnSc6	zástavba
anebo	anebo	k8xC	anebo
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Přednosti	přednost	k1gFnPc1	přednost
pušky	puška	k1gFnSc2	puška
a	a	k8xC	a
samopalu	samopal	k1gInSc2	samopal
v	v	k7c6	v
sobě	se	k3xPyFc3	se
spojovala	spojovat	k5eAaImAgFnS	spojovat
útočná	útočný	k2eAgFnSc1d1	útočná
puška	puška	k1gFnSc1	puška
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
stala	stát	k5eAaPmAgFnS	stát
standardní	standardní	k2eAgFnSc7d1	standardní
pěchotní	pěchotní	k2eAgFnSc7d1	pěchotní
zbraní	zbraň	k1gFnSc7	zbraň
takřka	takřka	k6eAd1	takřka
všech	všecek	k3xTgFnPc2	všecek
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
komunikace	komunikace	k1gFnSc2	komunikace
se	se	k3xPyFc4	se
válčící	válčící	k2eAgFnPc1d1	válčící
strany	strana	k1gFnPc1	strana
pokusily	pokusit	k5eAaPmAgFnP	pokusit
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
používáním	používání	k1gNnSc7	používání
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
seznamů	seznam	k1gInPc2	seznam
kódů	kód	k1gInPc2	kód
pro	pro	k7c4	pro
šifrování	šifrování	k1gNnSc4	šifrování
využívaných	využívaný	k2eAgFnPc2d1	využívaná
různými	různý	k2eAgInPc7d1	různý
šifrovacími	šifrovací	k2eAgInPc7d1	šifrovací
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
Enigma	enigma	k1gFnSc1	enigma
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
působily	působit	k5eAaImAgFnP	působit
systémy	systém	k1gInPc4	systém
rozluštění	rozluštění	k1gNnSc4	rozluštění
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
náležela	náležet	k5eAaImAgFnS	náležet
například	například	k6eAd1	například
britská	britský	k2eAgFnSc1d1	britská
Ultra	ultra	k2eAgFnSc1d1	ultra
či	či	k8xC	či
americký	americký	k2eAgInSc1d1	americký
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokázal	dokázat	k5eAaPmAgMnS	dokázat
prolomit	prolomit	k5eAaPmF	prolomit
japonský	japonský	k2eAgInSc4d1	japonský
námořní	námořní	k2eAgInSc4d1	námořní
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
rozvědky	rozvědka	k1gFnSc2	rozvědka
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zaměřovaly	zaměřovat	k5eAaImAgFnP	zaměřovat
na	na	k7c4	na
provádění	provádění	k1gNnSc4	provádění
mystifikačních	mystifikační	k2eAgFnPc2d1	mystifikační
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
několikrát	několikrát	k6eAd1	několikrát
použili	použít	k5eAaPmAgMnP	použít
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odvrátili	odvrátit	k5eAaPmAgMnP	odvrátit
pozornost	pozornost	k1gFnSc4	pozornost
německého	německý	k2eAgNnSc2d1	německé
velení	velení	k1gNnSc2	velení
od	od	k7c2	od
příprav	příprava	k1gFnPc2	příprava
invazí	invaze	k1gFnPc2	invaze
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
či	či	k8xC	či
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jiné	jiný	k2eAgInPc4d1	jiný
významné	významný	k2eAgInPc4d1	významný
technologické	technologický	k2eAgInPc4d1	technologický
a	a	k8xC	a
technické	technický	k2eAgInPc4d1	technický
počiny	počin	k1gInPc4	počin
dosažené	dosažený	k2eAgInPc4d1	dosažený
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
patří	patřit	k5eAaImIp3nP	patřit
první	první	k4xOgInPc1	první
programovatelné	programovatelný	k2eAgInPc1d1	programovatelný
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
řízené	řízený	k2eAgInPc1d1	řízený
střely	střel	k1gInPc1	střel
a	a	k8xC	a
balistické	balistický	k2eAgFnPc1d1	balistická
rakety	raketa	k1gFnPc1	raketa
(	(	kIx(	(
<g/>
V-	V-	k1gFnSc1	V-
<g/>
1	[number]	k4	1
a	a	k8xC	a
V-	V-	k1gMnSc3	V-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
skončily	skončit	k5eAaPmAgFnP	skončit
mnohaleté	mnohaletý	k2eAgFnPc1d1	mnohaletá
diskuze	diskuze	k1gFnPc1	diskuze
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
nýtování	nýtování	k1gNnSc1	nýtování
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
svařování	svařování	k1gNnSc1	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Svařování	svařování	k1gNnSc1	svařování
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
<g/>
,	,	kIx,	,
úspornější	úsporný	k2eAgNnSc1d2	úspornější
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
<g/>
.	.	kIx.	.
</s>
<s>
Svařené	svařený	k2eAgInPc1d1	svařený
díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
lehčí	lehký	k2eAgInPc4d2	lehčí
(	(	kIx(	(
<g/>
nýtované	nýtovaný	k2eAgInPc4d1	nýtovaný
spoje	spoj	k1gInPc4	spoj
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
přeplátované	přeplátovaný	k2eAgFnPc1d1	přeplátovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
ruské	ruský	k2eAgInPc1d1	ruský
tanky	tank	k1gInPc1	tank
lehčí	lehčit	k5eAaImIp3nP	lehčit
a	a	k8xC	a
materiálově	materiálově	k6eAd1	materiálově
úspornější	úsporný	k2eAgFnSc1d2	úspornější
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
nýtování	nýtování	k1gNnSc1	nýtování
fyzicky	fyzicky	k6eAd1	fyzicky
velmi	velmi	k6eAd1	velmi
namáhavé	namáhavý	k2eAgNnSc1d1	namáhavé
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
jejich	jejich	k3xOp3gFnSc2	jejich
fyzické	fyzický	k2eAgFnSc2d1	fyzická
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
je	on	k3xPp3gInPc4	on
vykonávat	vykonávat	k5eAaImF	vykonávat
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kdežto	kdežto	k8xS	kdežto
sváření	sváření	k1gNnSc1	sváření
zvládly	zvládnout	k5eAaPmAgInP	zvládnout
ve	v	k7c6	v
zbrojovkách	zbrojovka	k1gFnPc6	zbrojovka
i	i	k8xC	i
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
mohli	moct	k5eAaImAgMnP	moct
jít	jít	k5eAaImF	jít
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
spojenci	spojenec	k1gMnPc1	spojenec
zrušili	zrušit	k5eAaPmAgMnP	zrušit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
USA	USA	kA	USA
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poražené	poražený	k2eAgNnSc4d1	poražené
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
historické	historický	k2eAgFnPc1d1	historická
německé	německý	k2eAgFnPc1d1	německá
země	zem	k1gFnPc1	zem
na	na	k7c6	na
východě	východ	k1gInSc6	východ
byly	být	k5eAaImAgFnP	být
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
polské	polský	k2eAgFnPc1d1	polská
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc6d1	sovětská
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
vysídlení	vysídlení	k1gNnSc1	vysídlení
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
stvrzeno	stvrdit	k5eAaPmNgNnS	stvrdit
na	na	k7c6	na
postupimské	postupimský	k2eAgFnSc6d1	Postupimská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
opustit	opustit	k5eAaPmF	opustit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
získalo	získat	k5eAaPmAgNnS	získat
nové	nový	k2eAgFnPc4d1	nová
západní	západní	k2eAgFnPc4d1	západní
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
kompenzována	kompenzován	k2eAgFnSc1d1	kompenzována
ztráta	ztráta	k1gFnSc1	ztráta
Sověty	Sovět	k1gMnPc7	Sovět
zabraného	zabraný	k2eAgNnSc2d1	zabrané
polského	polský	k2eAgNnSc2d1	polské
východního	východní	k2eAgNnSc2d1	východní
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
kolem	kolem	k6eAd1	kolem
3,5	[number]	k4	3,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
si	se	k3xPyFc3	se
vedle	vedle	k7c2	vedle
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
podržel	podržet	k5eAaPmAgInS	podržet
přímou	přímý	k2eAgFnSc4d1	přímá
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
pobaltskými	pobaltský	k2eAgInPc7d1	pobaltský
státy	stát	k1gInPc7	stát
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
a	a	k8xC	a
Estonskem	Estonsko	k1gNnSc7	Estonsko
a	a	k8xC	a
nad	nad	k7c7	nad
dříve	dříve	k6eAd2	dříve
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
Besarábií	Besarábie	k1gFnSc7	Besarábie
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
získal	získat	k5eAaPmAgInS	získat
československou	československý	k2eAgFnSc4d1	Československá
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
okupovaly	okupovat	k5eAaBmAgInP	okupovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
své	svůj	k3xOyFgFnSc3	svůj
správě	správa	k1gFnSc3	správa
vystavily	vystavit	k5eAaPmAgInP	vystavit
někdejší	někdejší	k2eAgInPc1d1	někdejší
japonské	japonský	k2eAgInPc1d1	japonský
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Sověti	Sovět	k1gMnPc1	Sovět
anektovali	anektovat	k5eAaBmAgMnP	anektovat
jižní	jižní	k2eAgInSc4d1	jižní
Sachalin	Sachalin	k1gInSc4	Sachalin
a	a	k8xC	a
Kurilské	kurilský	k2eAgInPc4d1	kurilský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
Japonci	Japonec	k1gMnPc7	Japonec
kontrolovaná	kontrolovaný	k2eAgFnSc1d1	kontrolovaná
Korea	Korea	k1gFnSc1	Korea
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
velmoci	velmoc	k1gFnPc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
míru	mír	k1gInSc2	mír
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Spojenci	spojenec	k1gMnPc1	spojenec
Organizaci	organizace	k1gFnSc4	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgInSc3	tento
počinu	počin	k1gInSc3	počin
se	se	k3xPyFc4	se
v	v	k7c6	v
alianci	aliance	k1gFnSc6	aliance
mezi	mezi	k7c7	mezi
Západem	západ	k1gInSc7	západ
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
začaly	začít	k5eAaPmAgFnP	začít
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konfliktu	konflikt	k1gInSc2	konflikt
objevovat	objevovat	k5eAaImF	objevovat
vážné	vážný	k2eAgFnPc4d1	vážná
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
tzv.	tzv.	kA	tzv.
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
od	od	k7c2	od
Štětína	Štětín	k1gInSc2	Štětín
na	na	k7c6	na
Baltu	Balt	k1gInSc6	Balt
k	k	k7c3	k
Terstu	Terst	k1gInSc3	Terst
na	na	k7c6	na
Jadranu	Jadran	k1gInSc6	Jadran
a	a	k8xC	a
jež	jenž	k3xRgFnPc1	jenž
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělovala	oddělovat	k5eAaImAgFnS	oddělovat
demokratické	demokratický	k2eAgInPc4d1	demokratický
západní	západní	k2eAgInPc4d1	západní
státy	stát	k1gInPc4	stát
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
země	zem	k1gFnSc2	zem
řízené	řízený	k2eAgInPc4d1	řízený
Sověty	Sověty	k1gInPc4	Sověty
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstající	vzrůstající	k2eAgNnSc1d1	vzrůstající
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
přerostlo	přerůst	k5eAaPmAgNnS	přerůst
ve	v	k7c4	v
studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Američany	Američan	k1gMnPc4	Američan
vedené	vedený	k2eAgFnSc2d1	vedená
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc2d1	sovětská
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
nové	nový	k2eAgInPc1d1	nový
válečné	válečný	k2eAgInPc1d1	válečný
konflikty	konflikt	k1gInPc1	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
nacionalisté	nacionalista	k1gMnPc1	nacionalista
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
utkali	utkat	k5eAaPmAgMnP	utkat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgFnPc1d1	komunistická
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
podporou	podpora	k1gFnSc7	podpora
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1949	[number]	k4	1949
Mao	Mao	k1gFnSc3	Mao
Ce-tungem	Ceung	k1gInSc7	Ce-tung
založena	založit	k5eAaPmNgFnS	založit
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poražení	poražený	k2eAgMnPc1d1	poražený
nacionalisté	nacionalista	k1gMnPc1	nacionalista
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
královským	královský	k2eAgNnSc7d1	královské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgMnSc7	jenž
stáli	stát	k5eAaImAgMnP	stát
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
<g/>
</s>
<s>
,	,	kIx,	,
a	a	k8xC	a
místními	místní	k2eAgMnPc7d1	místní
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
nakonec	nakonec	k6eAd1	nakonec
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Nejvážnější	vážní	k2eAgInSc1d3	nejvážnější
konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
na	na	k7c6	na
Korejském	korejský	k2eAgInSc6d1	korejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
komunistická	komunistický	k2eAgFnSc1d1	komunistická
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
Sověty	Sověty	k1gInPc7	Sověty
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
napadla	napadnout	k5eAaPmAgFnS	napadnout
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
materiální	materiální	k2eAgFnSc4d1	materiální
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
západních	západní	k2eAgFnPc2d1	západní
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
příměřím	příměří	k1gNnPc3	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nastalo	nastat	k5eAaPmAgNnS	nastat
v	v	k7c6	v
zámořských	zámořský	k2eAgFnPc6d1	zámořská
državách	država	k1gFnPc6	država
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
období	období	k1gNnSc2	období
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
tkvěly	tkvět	k5eAaImAgFnP	tkvět
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
nahlížení	nahlížení	k1gNnPc2	nahlížení
Evropanů	Evropan	k1gMnPc2	Evropan
na	na	k7c4	na
kolonialismus	kolonialismus	k1gInSc4	kolonialismus
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
v	v	k7c6	v
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
evropských	evropský	k2eAgFnPc2d1	Evropská
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
mocností	mocnost	k1gFnPc2	mocnost
způsobeném	způsobený	k2eAgInSc6d1	způsobený
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
v	v	k7c6	v
rostoucích	rostoucí	k2eAgInPc6d1	rostoucí
požadavcích	požadavek	k1gInPc6	požadavek
ovládaných	ovládaný	k2eAgInPc2d1	ovládaný
národů	národ	k1gInPc2	národ
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Dekolonizační	dekolonizační	k2eAgInSc1d1	dekolonizační
proces	proces	k1gInSc1	proces
probíhal	probíhat	k5eAaImAgInS	probíhat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
četné	četný	k2eAgFnPc1d1	četná
výjimky	výjimka	k1gFnPc1	výjimka
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
Indočína	Indočína	k1gFnSc1	Indočína
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
a	a	k8xC	a
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zemí	zem	k1gFnPc2	zem
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Evropanů	Evropan	k1gMnPc2	Evropan
rozdělena	rozdělen	k2eAgNnPc4d1	rozděleno
podle	podle	k7c2	podle
etnického	etnický	k2eAgNnSc2d1	etnické
nebo	nebo	k8xC	nebo
náboženského	náboženský	k2eAgNnSc2d1	náboženské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rozdělení	rozdělení	k1gNnSc4	rozdělení
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
na	na	k7c4	na
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc2	Pákistán
na	na	k7c6	na
území	území	k1gNnSc6	území
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Britského	britský	k2eAgInSc2d1	britský
Rádže	rádža	k1gMnPc4	rádža
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
poválečné	poválečný	k2eAgFnSc2d1	poválečná
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
obnovy	obnova	k1gFnSc2	obnova
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
všeobecně	všeobecně	k6eAd1	všeobecně
probíhala	probíhat	k5eAaImAgFnS	probíhat
vcelku	vcelku	k6eAd1	vcelku
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
díky	díky	k7c3	díky
Marshallovu	Marshallův	k2eAgInSc3d1	Marshallův
plánu	plán	k1gInSc3	plán
snadno	snadno	k6eAd1	snadno
zotavilo	zotavit	k5eAaPmAgNnS	zotavit
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
dvojnásobné	dvojnásobný	k2eAgFnSc3d1	dvojnásobná
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
výkonnosti	výkonnost	k1gFnSc3	výkonnost
oproti	oproti	k7c3	oproti
předválečnému	předválečný	k2eAgInSc3d1	předválečný
stavu	stav	k1gInSc3	stav
(	(	kIx(	(
<g/>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
objevily	objevit	k5eAaPmAgInP	objevit
plakáty	plakát	k1gInPc1	plakát
"	"	kIx"	"
<g/>
Wir	Wir	k1gMnSc1	Wir
sind	sind	k1gMnSc1	sind
wieder	wieder	k1gMnSc1	wieder
Wer	Wer	k1gMnSc1	Wer
<g/>
"	"	kIx"	"
–	–	k?	–
My	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
zase	zase	k9	zase
Někdo	někdo	k3yInSc1	někdo
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
uvítali	uvítat	k5eAaPmAgMnP	uvítat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
znáborovaných	znáborovaný	k2eAgMnPc2d1	znáborovaný
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
prožívalo	prožívat	k5eAaImAgNnS	prožívat
obrovský	obrovský	k2eAgInSc4d1	obrovský
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozmach	rozmach	k1gInSc4	rozmach
a	a	k8xC	a
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
miliony	milion	k4xCgInPc1	milion
dalších	další	k2eAgMnPc2d1	další
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
vzešla	vzejít	k5eAaPmAgFnS	vzejít
z	z	k7c2	z
války	válka	k1gFnSc2	válka
zcela	zcela	k6eAd1	zcela
zruinována	zruinován	k2eAgFnSc1d1	zruinována
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
během	během	k7c2	během
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpané	vyčerpaný	k2eAgNnSc1d1	vyčerpané
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gInSc1	jeho
relativní	relativní	k2eAgInSc1d1	relativní
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
pokles	pokles	k1gInSc1	pokles
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
navzdory	navzdory	k7c3	navzdory
počátečním	počáteční	k2eAgFnPc3d1	počáteční
obtížím	obtíž	k1gFnPc3	obtíž
brzy	brzy	k6eAd1	brzy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
napomohl	napomoct	k5eAaPmAgMnS	napomoct
rychlé	rychlý	k2eAgFnSc3d1	rychlá
modernizaci	modernizace	k1gFnSc3	modernizace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
poměrně	poměrně	k6eAd1	poměrně
pružně	pružně	k6eAd1	pružně
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
s	s	k7c7	s
utrpěnými	utrpěný	k2eAgFnPc7d1	utrpěná
škodami	škoda	k1gFnPc7	škoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
postihly	postihnout	k5eAaPmAgFnP	postihnout
převážně	převážně	k6eAd1	převážně
západní	západní	k2eAgNnSc4d1	západní
území	území	k1gNnSc4	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Japonsko	Japonsko	k1gNnSc1	Japonsko
rapidního	rapidní	k2eAgInSc2d1	rapidní
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
vzestupu	vzestup	k1gInSc2	vzestup
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
světových	světový	k2eAgFnPc2d1	světová
ekonomik	ekonomika	k1gFnPc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Obstojně	obstojně	k6eAd1	obstojně
se	se	k3xPyFc4	se
vyvíjející	vyvíjející	k2eAgNnSc1d1	vyvíjející
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
úrovně	úroveň	k1gFnSc2	úroveň
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
tvrdě	tvrdě	k6eAd1	tvrdě
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
Velkým	velký	k2eAgInSc7d1	velký
skokem	skok	k1gInSc7	skok
<g/>
,	,	kIx,	,
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
experimentem	experiment	k1gInSc7	experiment
s	s	k7c7	s
katastrofálními	katastrofální	k2eAgInPc7d1	katastrofální
důsledky	důsledek	k1gInPc7	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
obstarávaly	obstarávat	k5eAaImAgInP	obstarávat
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
veškeré	veškerý	k3xTgFnSc2	veškerý
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
nezměnilo	změnit	k5eNaPmAgNnS	změnit
až	až	k9	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
