<s>
Colorado	Colorado	k1gNnSc1
(	(	kIx(
<g/>
přítok	přítok	k1gInSc1
Kalifornského	kalifornský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Colorado	Colorado	k1gNnSc1
Řeka	Řek	k1gMnSc2
v	v	k7c4
Bullhead	Bullhead	k1gInSc4
CityZákladní	CityZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
2740	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
635	#num#	k4
000	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
508	#num#	k4
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Skalnaté	skalnatý	k2eAgFnPc1d1
hory	hora	k1gFnPc1
40	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
19,94	19,94	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
105	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
34,04	34,04	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Kalifornský	kalifornský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
31	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
55,58	55,58	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
114	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
14,54	14,54	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
USA	USA	kA
USA	USA	kA
(	(	kIx(
<g/>
Colorado	Colorado	k1gNnSc1
<g/>
,	,	kIx,
Utah	Utah	k1gInSc1
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
<g/>
,	,	kIx,
Nevada	Nevada	k1gFnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
Baja	Baj	k2eAgNnPc1d1
California	Californium	k1gNnPc1
<g/>
,	,	kIx,
Sonora	sonora	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Kalifornský	kalifornský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
povodí	povodí	k1gNnSc2
řeky	řeka	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Colorado	Colorado	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Colorado	Colorado	k1gNnSc1
River	Rivra	k1gFnPc2
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
Río	Río	k1gFnSc2
Colorado	Colorado	k1gNnSc1
znamená	znamenat	k5eAaImIp3nS
Barevná	barevný	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
převážně	převážně	k6eAd1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
protéká	protékat	k5eAaImIp3nS
státy	stát	k1gInPc1
Colorado	Colorado	k1gNnSc1
<g/>
,	,	kIx,
Utah	Utah	k1gInSc1
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
<g/>
,	,	kIx,
Nevada	Nevada	k1gFnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
Nevadou	Nevada	k1gFnSc7
a	a	k8xC
Arizonou	Arizona	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
mezi	mezi	k7c7
Arizonou	Arizona	k1gFnSc7
a	a	k8xC
Kalifornií	Kalifornie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolním	dolní	k2eAgInSc7d1
tokem	tok	k1gInSc7
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
hranici	hranice	k1gFnSc4
států	stát	k1gInPc2
Baja	Bajum	k1gNnSc2
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Sonora	sonora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
2740	#num#	k4
km	km	kA
a	a	k8xC
včetně	včetně	k7c2
pravého	pravý	k2eAgInSc2d1
přítoku	přítok	k1gInSc2
Green	Green	k2eAgInSc4d1
River	River	k1gInSc4
3200	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
635	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP
ve	v	k7c6
Skalnatých	skalnatý	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protéká	protékat	k5eAaImIp3nS
převážně	převážně	k6eAd1
polopouštními	polopouštní	k2eAgFnPc7d1
a	a	k8xC
pouštními	pouštní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
států	stát	k1gInPc2
Utah	Utah	k1gInSc1
a	a	k8xC
Arizona	Arizona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Koloradské	Koloradský	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
hluboké	hluboký	k2eAgInPc4d1
kaňony	kaňon	k1gInPc4
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
800	#num#	k4
km	km	kA
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
je	být	k5eAaImIp3nS
i	i	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
na	na	k7c6
světě	svět	k1gInSc6
Grand	grand	k1gMnSc1
Canyon	Canyon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnPc1
do	do	k7c2
Kalifornského	kalifornský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
deltu	delta	k1gFnSc4
o	o	k7c6
rozloze	rozloha	k1gFnSc6
8600	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
řeka	řeka	k1gFnSc1
změnila	změnit	k5eAaPmAgFnS
tok	tok	k1gInSc4
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
tak	tak	k6eAd1
slané	slaný	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
zvané	zvaný	k2eAgNnSc4d1
Saltonské	Saltonský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
téměř	téměř	k6eAd1
zaplavilo	zaplavit	k5eAaPmAgNnS
údolí	údolí	k1gNnSc1
Imperial	Imperial	k1gInSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Větší	veliký	k2eAgInPc1d2
přítoky	přítok	k1gInPc1
</s>
<s>
zleva	zleva	k6eAd1
–	–	k?
San	San	k1gMnSc1
Juan	Juan	k1gMnSc1
<g/>
,	,	kIx,
Malé	Malé	k2eAgNnSc1d1
Colorado	Colorado	k1gNnSc1
<g/>
,	,	kIx,
Gila	Gila	k1gFnSc1
</s>
<s>
zprava	zprava	k6eAd1
–	–	k?
Green	Green	k1gInSc1
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdrojem	zdroj	k1gInSc7
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
tající	tající	k2eAgInSc1d1
sníh	sníh	k1gInSc1
ve	v	k7c6
Skalnatých	skalnatý	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvodnější	Nejvodný	k2eAgInSc1d2
je	on	k3xPp3gNnPc4
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
a	a	k8xC
létě	léto	k1gNnSc6
je	být	k5eAaImIp3nS
vody	voda	k1gFnPc1
v	v	k7c6
řece	řeka	k1gFnSc6
málo	málo	k4c1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
u	u	k7c2
Lees	Leesa	k1gFnPc2
Ferry	Ferro	k1gNnPc7
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
činí	činit	k5eAaImIp3nS
508	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
K	k	k7c3
ústí	ústí	k1gNnSc3
většina	většina	k1gFnSc1
vody	voda	k1gFnPc1
nedotéká	dotékat	k5eNaImIp3nS
v	v	k7c6
důsledku	důsledek	k1gInSc6
zavlažování	zavlažování	k1gNnSc2
a	a	k8xC
zásobování	zásobování	k1gNnSc2
vodou	voda	k1gFnSc7
a	a	k8xC
průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
tam	tam	k6eAd1
činí	činit	k5eAaImIp3nS
pouhých	pouhý	k2eAgInPc2d1
5	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Řeka	Řek	k1gMnSc2
unáší	unášet	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
pevných	pevný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
,	,	kIx,
ročně	ročně	k6eAd1
v	v	k7c6
průměru	průměr	k1gInSc6
160	#num#	k4
Mt	Mt	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
všechny	všechen	k3xTgFnPc1
usazují	usazovat	k5eAaImIp3nP
v	v	k7c6
jezerech	jezero	k1gNnPc6
Powell	Powella	k1gFnPc2
a	a	k8xC
Mead	Meada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vybudováním	vybudování	k1gNnSc7
přehrad	přehrada	k1gFnPc2
řeka	řeka	k1gFnSc1
často	často	k6eAd1
ukazovala	ukazovat	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
divoký	divoký	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
tání	tání	k1gNnSc2
sněhu	sníh	k1gInSc2
v	v	k7c6
horách	hora	k1gFnPc6
zatápěla	zatápět	k5eAaImAgFnS
farmářské	farmářský	k2eAgFnPc4d1
usedlosti	usedlost	k1gFnPc4
<g/>
,	,	kIx,
ležící	ležící	k2eAgInPc4d1
níže	nízce	k6eAd2
po	po	k7c6
proudu	proud	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
odváděna	odvádět	k5eAaImNgFnS
zavlažovacími	zavlažovací	k2eAgInPc7d1
kanály	kanál	k1gInPc7
a	a	k8xC
vodovody	vodovod	k1gInPc7
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
zásobování	zásobování	k1gNnSc3
pro	pro	k7c4
města	město	k1gNnPc4
na	na	k7c6
Kalifornském	kalifornský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
včetně	včetně	k7c2
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
byly	být	k5eAaImAgInP
na	na	k7c6
řece	řeka	k1gFnSc6
postaveny	postaven	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
(	(	kIx(
<g/>
Glen	Glen	k1gMnSc1
Canyon	Canyon	k1gMnSc1
<g/>
,	,	kIx,
Hoover	Hoover	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c7
kterými	který	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
se	se	k3xPyFc4
vytvořila	vytvořit	k5eAaPmAgNnP
velká	velký	k2eAgNnPc1d1
přehradní	přehradní	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
(	(	kIx(
<g/>
Powell	Powell	k1gInSc1
<g/>
,	,	kIx,
Mead	Mead	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každé	každý	k3xTgFnSc6
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
přibližně	přibližně	k6eAd1
650	#num#	k4
km²	km²	k?
a	a	k8xC
objem	objem	k1gInSc1
34	#num#	k4
km³	km³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc4d2
přehradní	přehradní	k2eAgNnPc4d1
jezera	jezero	k1gNnPc4
(	(	kIx(
<g/>
Mohave	Mohav	k1gInSc5
<g/>
,	,	kIx,
Havasu	Havas	k1gInSc2
<g/>
)	)	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
za	za	k7c7
hrázemi	hráz	k1gFnPc7
Davis	Davis	k1gFnPc2
<g/>
,	,	kIx,
Parker	Parkra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
těchto	tento	k3xDgFnPc2
přehrad	přehrada	k1gFnPc2
jsou	být	k5eAaImIp3nP
vodní	vodní	k2eAgFnPc1d1
elektrárny	elektrárna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zavlažování	zavlažování	k1gNnSc3
slouží	sloužit	k5eAaImIp3nP
přehrady	přehrada	k1gFnPc1
Palo	Pala	k1gMnSc5
Verde	Verd	k1gMnSc5
<g/>
,	,	kIx,
Imperial	Imperial	k1gMnSc1
a	a	k8xC
Morelos	Morelos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
(	(	kIx(
<g/>
Roosevelt	Roosevelt	k1gMnSc1
<g/>
,	,	kIx,
Santa	Santa	k1gMnSc1
Clara	Clara	k1gFnSc1
<g/>
,	,	kIx,
Horshu	Horsha	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
zavlažovací	zavlažovací	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
byly	být	k5eAaImAgInP
vybudovány	vybudovat	k5eAaPmNgInP
také	také	k9
v	v	k7c6
povodí	povodí	k1gNnSc6
přítoku	přítok	k1gInSc2
Gila	Gil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
je	být	k5eAaImIp3nS
možná	možná	k9
vodní	vodní	k2eAgInPc1d1
doprava	doprava	k6eAd1
říčními	říční	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
zanedbatelný	zanedbatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
u	u	k7c2
Lees	Leesa	k1gFnPc2
Ferry	Ferro	k1gNnPc7
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
К	К	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FREEMAN	FREEMAN	kA
<g/>
,	,	kIx,
L.	L.	kA
R.	R.	kA
The	The	k1gMnSc1
Colorado	Colorado	k1gNnSc1
River	River	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
POWELL	POWELL	kA
<g/>
,	,	kIx,
J.	J.	kA
W.	W.	kA
Exploration	Exploration	k1gInSc1
of	of	k?
the	the	k?
Colorado	Colorado	k1gNnSc1
River	River	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Colorado	Colorado	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
342293	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4259063-2	4259063-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85028687	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
42148570477424310488	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85028687	#num#	k4
</s>
