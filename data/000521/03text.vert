<s>
Adolf	Adolf	k1gMnSc1	Adolf
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1855	[number]	k4	1855
Habry	habr	k1gInPc4	habr
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc4	Rakouska-Uhersek
poslanec	poslanec	k1gMnSc1	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
předák	předák	k1gMnSc1	předák
moravské	moravský	k2eAgFnSc2d1	Moravská
odnože	odnož	k1gFnSc2	odnož
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1855	[number]	k4	1855
v	v	k7c6	v
Habrech	habr	k1gInPc6	habr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
absolvování	absolvování	k1gNnSc6	absolvování
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1	Karlo-Ferdinandova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
působení	působení	k1gNnSc6	působení
u	u	k7c2	u
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
u	u	k7c2	u
obchodního	obchodní	k2eAgInSc2d1	obchodní
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
odešel	odejít	k5eAaPmAgMnS	odejít
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
otevřel	otevřít	k5eAaPmAgInS	otevřít
advokátní	advokátní	k2eAgFnSc4d1	advokátní
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
Politiky	politika	k1gFnSc2	politika
i	i	k8xC	i
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
redigoval	redigovat	k5eAaImAgInS	redigovat
opoziční	opoziční	k2eAgInSc1d1	opoziční
list	list	k1gInSc1	list
Tribüne	Tribün	k1gInSc5	Tribün
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vydával	vydávat	k5eAaPmAgInS	vydávat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Jan	Jan	k1gMnSc1	Jan
Stanislav	Stanislav	k1gMnSc1	Stanislav
Skrejšovský	Skrejšovský	k1gMnSc1	Skrejšovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
novinář	novinář	k1gMnSc1	novinář
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
vydavatelem	vydavatel	k1gMnSc7	vydavatel
Moravských	moravský	k2eAgInPc2d1	moravský
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
vydáváním	vydávání	k1gNnSc7	vydávání
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamenaly	znamenat	k5eAaImAgFnP	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
zvrat	zvrat	k1gInSc4	zvrat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
žurnalistice	žurnalistika	k1gFnSc6	žurnalistika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
dekádách	dekáda	k1gFnPc6	dekáda
vynikaly	vynikat	k5eAaImAgFnP	vynikat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
osobnosti	osobnost	k1gFnPc4	osobnost
jako	jako	k8xC	jako
bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapková	k1gFnSc2	Čapková
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahen	k2eAgMnSc1d1	Mahen
nebo	nebo	k8xC	nebo
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
advokát	advokát	k1gMnSc1	advokát
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
coby	coby	k?	coby
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
s	s	k7c7	s
Omladinou	Omladina	k1gFnSc7	Omladina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
politických	politický	k2eAgFnPc6d1	politická
kauzách	kauza	k1gFnPc6	kauza
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
přímo	přímo	k6eAd1	přímo
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ideově	ideově	k6eAd1	ideově
byl	být	k5eAaImAgInS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
mladočesky	mladočesky	k6eAd1	mladočesky
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
rozštěpení	rozštěpení	k1gNnSc1	rozštěpení
na	na	k7c4	na
staro-	staro-	k?	staro-
a	a	k8xC	a
mladočechy	mladočech	k1gMnPc4	mladočech
jako	jako	k9	jako
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
tak	tak	k9	tak
do	do	k7c2	do
zatím	zatím	k6eAd1	zatím
ještě	ještě	k9	ještě
jednotné	jednotný	k2eAgFnSc2d1	jednotná
moravské	moravský	k2eAgFnSc2d1	Moravská
staročeské	staročeský	k2eAgFnSc2d1	staročeská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ovšem	ovšem	k9	ovšem
již	již	k9	již
brzy	brzy	k6eAd1	brzy
vedl	vést	k5eAaImAgMnS	vést
liberální	liberální	k2eAgNnSc4d1	liberální
<g/>
,	,	kIx,	,
mladočeské	mladočeský	k2eAgNnSc4d1	mladočeské
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
coby	coby	k?	coby
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
obdobu	obdoba	k1gFnSc4	obdoba
mladočechů	mladočech	k1gMnPc2	mladočech
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
politické	politický	k2eAgFnSc6d1	politická
formaci	formace	k1gFnSc6	formace
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
post	posta	k1gFnPc2	posta
místopředsedy	místopředseda	k1gMnSc2	místopředseda
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Nymburská	nymburský	k2eAgFnSc1d1	Nymburská
rezoluce	rezoluce	k1gFnSc1	rezoluce
mladočechů	mladočech	k1gMnPc2	mladočech
coby	coby	k?	coby
zásadní	zásadní	k2eAgInSc4d1	zásadní
programový	programový	k2eAgInSc4d1	programový
dokument	dokument	k1gInSc4	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
označovala	označovat	k5eAaImAgFnS	označovat
Lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
explicitně	explicitně	k6eAd1	explicitně
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
zemskou	zemský	k2eAgFnSc7d1	zemská
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Stránský	Stránský	k1gMnSc1	Stránský
pravidelně	pravidelně	k6eAd1	pravidelně
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
s	s	k7c7	s
protějšky	protějšek	k1gInPc7	protějšek
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
důležitých	důležitý	k2eAgFnPc2d1	důležitá
stranických	stranický	k2eAgFnPc2d1	stranická
porad	porada	k1gFnPc2	porada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Čech	Čechy	k1gFnPc2	Čechy
ale	ale	k8xC	ale
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachován	k2eAgFnSc1d1	zachována
silnější	silný	k2eAgFnSc1d2	silnější
pozice	pozice	k1gFnSc1	pozice
staročechů	staročech	k1gMnPc2	staročech
a	a	k8xC	a
tak	tak	k9	tak
Stránský	Stránský	k1gMnSc1	Stránský
s	s	k7c7	s
moravskými	moravský	k2eAgMnPc7d1	moravský
staročechy	staročech	k1gMnPc7	staročech
udržoval	udržovat	k5eAaImAgMnS	udržovat
politiku	politika	k1gFnSc4	politika
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
mladočeské	mladočeský	k2eAgNnSc1d1	mladočeské
křídlo	křídlo	k1gNnSc1	křídlo
ovšem	ovšem	k9	ovšem
i	i	k9	i
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
moravských	moravský	k2eAgMnPc2d1	moravský
mladočechů	mladočech	k1gMnPc2	mladočech
s	s	k7c7	s
protějšky	protějšek	k1gInPc7	protějšek
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
otřes	otřes	k1gInSc4	otřes
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
vůdce	vůdce	k1gMnSc1	vůdce
mladočechů	mladočech	k1gMnPc2	mladočech
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
tehdy	tehdy	k6eAd1	tehdy
totiž	totiž	k9	totiž
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
politiku	politika	k1gFnSc4	politika
provládní	provládní	k2eAgFnSc2d1	provládní
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k9	i
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
mladočeského	mladočeský	k2eAgMnSc2d1	mladočeský
nominanta	nominant	k1gMnSc2	nominant
do	do	k7c2	do
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
mladočeši	mladočech	k1gMnPc1	mladočech
utvořili	utvořit	k5eAaPmAgMnP	utvořit
společný	společný	k2eAgInSc4d1	společný
klub	klub	k1gInSc4	klub
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
smýšlejícími	smýšlející	k2eAgMnPc7d1	smýšlející
agrárníky	agrárník	k1gMnPc7	agrárník
a	a	k8xC	a
klerikály	klerikál	k1gMnPc7	klerikál
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Stránský	Stránský	k1gMnSc1	Stránský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
moravští	moravský	k2eAgMnPc1d1	moravský
mladočeši	mladočech	k1gMnPc1	mladočech
zůstali	zůstat	k5eAaPmAgMnP	zůstat
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
českými	český	k2eAgFnPc7d1	Česká
nesocialistickými	socialistický	k2eNgFnPc7d1	nesocialistická
stranami	strana	k1gFnPc7	strana
mimo	mimo	k7c4	mimo
tuto	tento	k3xDgFnSc4	tento
parlamentu	parlament	k1gInSc6	parlament
frakci	frakce	k1gFnSc4	frakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
ustavení	ustavení	k1gNnSc6	ustavení
Lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
předsedů	předseda	k1gMnPc2	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
sem	sem	k6eAd1	sem
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
za	za	k7c4	za
městskou	městský	k2eAgFnSc4d1	městská
kurii	kurie	k1gFnSc4	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc4	obvod
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
atd.	atd.	kA	atd.
Zvolen	Zvolen	k1gInSc1	Zvolen
byl	být	k5eAaImAgInS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
týž	týž	k3xTgInSc4	týž
obvod	obvod	k1gInSc4	obvod
uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
a	a	k8xC	a
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
,	,	kIx,	,
za	za	k7c4	za
českou	český	k2eAgFnSc4d1	Česká
městskou	městský	k2eAgFnSc4d1	městská
kurii	kurie	k1gFnSc4	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc4	obvod
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Žďár	Žďár	k1gInSc1	Žďár
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
atd.	atd.	kA	atd.
a	a	k8xC	a
křeslo	křeslo	k1gNnSc4	křeslo
poslance	poslanec	k1gMnSc2	poslanec
tu	tu	k6eAd1	tu
obhájil	obhájit	k5eAaPmAgMnS	obhájit
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
byl	být	k5eAaImAgInS	být
i	i	k9	i
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Říšskou	říšský	k2eAgFnSc4d1	říšská
radu	rada	k1gFnSc4	rada
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
místo	místo	k7c2	místo
Josefa	Josef	k1gMnSc2	Josef
Fanderlíka	Fanderlík	k1gMnSc2	Fanderlík
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
městskou	městský	k2eAgFnSc4d1	městská
kurii	kurie	k1gFnSc4	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc4	obvod
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Jaroměřice	Jaroměřice	k1gFnPc1	Jaroměřice
atd.	atd.	kA	atd.
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
za	za	k7c4	za
stejný	stejný	k2eAgInSc4d1	stejný
obvod	obvod	k1gInSc4	obvod
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
konaných	konaný	k2eAgFnPc2d1	konaná
již	již	k6eAd1	již
v	v	k7c6	v
systému	systém	k1gInSc6	systém
rovného	rovný	k2eAgNnSc2d1	rovné
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kurií	kurie	k1gFnPc2	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Zvolen	Zvolen	k1gInSc1	Zvolen
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Morava	Morava	k1gFnSc1	Morava
09	[number]	k4	09
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
hospitant	hospitant	k1gMnSc1	hospitant
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
střechové	střechový	k2eAgFnSc2d1	střechová
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
frakce	frakce	k1gFnSc2	frakce
českých	český	k2eAgMnPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Opětovně	opětovně	k6eAd1	opětovně
získal	získat	k5eAaPmAgMnS	získat
mandát	mandát	k1gInSc4	mandát
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc2	jenž
usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
Jednota	jednota	k1gFnSc1	jednota
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
pokrokových	pokrokový	k2eAgMnPc2d1	pokrokový
poslanců	poslanec	k1gMnPc2	poslanec
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
parlamentní	parlamentní	k2eAgMnSc1d1	parlamentní
politik	politik	k1gMnSc1	politik
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
radikálně	radikálně	k6eAd1	radikálně
opoziční	opoziční	k2eAgMnPc4d1	opoziční
české	český	k2eAgMnPc4d1	český
řečníky	řečník	k1gMnPc4	řečník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
projednávala	projednávat	k5eAaImAgFnS	projednávat
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Badeniho	Badeni	k1gMnSc2	Badeni
volební	volební	k2eAgFnSc1d1	volební
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
Stránský	Stránský	k1gMnSc1	Stránský
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
radikálně	radikálně	k6eAd1	radikálně
opozičnímu	opoziční	k2eAgNnSc3d1	opoziční
křídlu	křídlo	k1gNnSc3	křídlo
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
mladočeši	mladočech	k1gMnPc1	mladočech
totiž	totiž	k9	totiž
považovali	považovat	k5eAaImAgMnP	považovat
reformu	reforma	k1gFnSc4	reforma
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
mírnou	mírný	k2eAgFnSc4d1	mírná
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
<g/>
,	,	kIx,	,
radikálnější	radikální	k2eAgInSc4d2	radikálnější
návrh	návrh	k1gInSc4	návrh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgMnPc1d1	další
předáci	předák	k1gMnPc1	předák
jako	jako	k8xS	jako
Josef	Josef	k1gMnSc1	Josef
Kaizl	Kaizl	k1gMnSc1	Kaizl
nebo	nebo	k8xC	nebo
Gustav	Gustav	k1gMnSc1	Gustav
Eim	Eim	k1gMnSc1	Eim
vládní	vládní	k2eAgFnSc4d1	vládní
předlohu	předloha	k1gFnSc4	předloha
podpořili	podpořit	k5eAaPmAgMnP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pro	pro	k7c4	pro
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
českého	český	k2eAgNnSc2d1	české
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
několika	několik	k4yIc2	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
česko-německé	českoěmecký	k2eAgFnPc4d1	česko-německá
konference	konference	k1gFnPc4	konference
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
národnostního	národnostní	k2eAgInSc2d1	národnostní
smíru	smír	k1gInSc2	smír
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
moravští	moravský	k2eAgMnPc1d1	moravský
mladočeši	mladočech	k1gMnPc1	mladočech
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
protiklerikální	protiklerikální	k2eAgFnSc4d1	protiklerikální
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
agrárníky	agrárník	k1gMnPc7	agrárník
i	i	k8xC	i
sociálními	sociální	k2eAgMnPc7d1	sociální
demokraty	demokrat	k1gMnPc7	demokrat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
eliminovat	eliminovat	k5eAaBmF	eliminovat
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
katolické	katolický	k2eAgFnSc2d1	katolická
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
politiky	politika	k1gFnSc2	politika
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
i	i	k9	i
ve	v	k7c6	v
veřejném	veřejný	k2eAgMnSc6d1	veřejný
<g/>
,	,	kIx,	,
spolkovém	spolkový	k2eAgMnSc6d1	spolkový
a	a	k8xC	a
hospodářském	hospodářský	k2eAgInSc6d1	hospodářský
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
postu	post	k1gInSc6	post
ředitele	ředitel	k1gMnSc4	ředitel
Hypoteční	hypoteční	k2eAgFnSc2d1	hypoteční
banky	banka	k1gFnSc2	banka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Moravského	moravský	k2eAgInSc2d1	moravský
pojišťovacího	pojišťovací	k2eAgInSc2d1	pojišťovací
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
odboji	odboj	k1gInSc6	odboj
a	a	k8xC	a
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
sloučil	sloučit	k5eAaPmAgInS	sloučit
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
mladočechy	mladočech	k1gMnPc7	mladočech
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
liberálními	liberální	k2eAgFnPc7d1	liberální
skupinami	skupina	k1gFnPc7	skupina
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
státoprávní	státoprávní	k2eAgFnSc2d1	státoprávní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
utvořené	utvořený	k2eAgFnSc2d1	utvořená
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
existence	existence	k1gFnSc2	existence
monarchie	monarchie	k1gFnSc2	monarchie
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
politika	politika	k1gFnSc1	politika
v	v	k7c6	v
státoprávních	státoprávní	k2eAgFnPc6d1	státoprávní
otázkách	otázka	k1gFnPc6	otázka
radikalizovala	radikalizovat	k5eAaBmAgFnS	radikalizovat
a	a	k8xC	a
Stránský	Stránský	k1gMnSc1	Stránský
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1918	[number]	k4	1918
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Češi	Čech	k1gMnPc1	Čech
zničí	zničit	k5eAaPmIp3nP	zničit
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
protože	protože	k8xS	protože
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
staletým	staletý	k2eAgInSc7d1	staletý
zločinem	zločin	k1gInSc7	zločin
proti	proti	k7c3	proti
svobodě	svoboda	k1gFnSc3	svoboda
lidského	lidský	k2eAgNnSc2d1	lidské
pokolení	pokolení	k1gNnSc2	pokolení
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
národní	národní	k2eAgFnSc7d1	národní
povinností	povinnost	k1gFnSc7	povinnost
Čechů	Čech	k1gMnPc2	Čech
je	být	k5eAaImIp3nS	být
škodit	škodit	k5eAaImF	škodit
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
předchozí	předchozí	k2eAgInPc4d1	předchozí
kroky	krok	k1gInPc4	krok
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1918	[number]	k4	1918
úřední	úřední	k2eAgFnSc7d1	úřední
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zavádět	zavádět	k5eAaImF	zavádět
krajské	krajský	k2eAgNnSc4d1	krajské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
naplňovala	naplňovat	k5eAaImAgFnS	naplňovat
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
plány	plán	k1gInPc4	plán
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
na	na	k7c6	na
utvoření	utvoření	k1gNnSc6	utvoření
etnicky	etnicky	k6eAd1	etnicky
německých	německý	k2eAgFnPc2d1	německá
entit	entita	k1gFnPc2	entita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Stránský	Stránský	k1gMnSc1	Stránský
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
po	po	k7c6	po
říjnu	říjen	k1gInSc6	říjen
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
první	první	k4xOgFnSc6	první
československé	československý	k2eAgFnSc6d1	Československá
vládě	vláda	k1gFnSc6	vláda
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
Karla	Karel	k1gMnSc2	Karel
Kramáře	kramář	k1gMnSc2	kramář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
železářský	železářský	k2eAgInSc4d1	železářský
průmysl	průmysl	k1gInSc4	průmysl
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
získal	získat	k5eAaPmAgMnS	získat
senátorské	senátorský	k2eAgNnSc4d1	senátorské
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
republiky	republika	k1gFnSc2	republika
Československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátu	senát	k1gInSc6	senát
zasedal	zasedat	k5eAaImAgInS	zasedat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
tvořil	tvořit	k5eAaImAgInS	tvořit
liberální	liberální	k2eAgInSc1d1	liberální
<g/>
,	,	kIx,	,
brněnské	brněnský	k2eAgNnSc4d1	brněnské
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vstřícnější	vstřícný	k2eAgNnSc1d2	vstřícnější
k	k	k7c3	k
politice	politika	k1gFnSc3	politika
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
radikalizovala	radikalizovat	k5eAaBmAgFnS	radikalizovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
masový	masový	k2eAgInSc4d1	masový
odchod	odchod	k1gInSc4	odchod
členstva	členstvo	k1gNnSc2	členstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
novou	nový	k2eAgFnSc7d1	nová
umírněnou	umírněný	k2eAgFnSc7d1	umírněná
formací	formace	k1gFnSc7	formace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
neúspěchu	neúspěch	k1gInSc6	neúspěch
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
