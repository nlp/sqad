<s>
Borovice	borovice	k1gFnSc1	borovice
pinie	pinie	k1gFnSc1	pinie
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
pinea	pinea	k1gMnSc1	pinea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
pouze	pouze	k6eAd1	pouze
pinie	pinie	k1gFnSc1	pinie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
strom	strom	k1gInSc1	strom
typický	typický	k2eAgInSc1d1	typický
svým	svůj	k3xOyFgInSc7	svůj
deštníkovitým	deštníkovitý	k2eAgInSc7d1	deštníkovitý
habitem	habitus	k1gInSc7	habitus
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
šišek	šiška	k1gFnPc2	šiška
s	s	k7c7	s
masivními	masivní	k2eAgFnPc7d1	masivní
apofýzami	apofýza	k1gFnPc7	apofýza
<g/>
.	.	kIx.	.
</s>
