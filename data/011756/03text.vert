<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
pinie	pinie	k1gFnSc1	pinie
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
pinea	pinea	k1gMnSc1	pinea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
pouze	pouze	k6eAd1	pouze
pinie	pinie	k1gFnSc1	pinie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
strom	strom	k1gInSc1	strom
typický	typický	k2eAgInSc1d1	typický
svým	svůj	k3xOyFgInSc7	svůj
deštníkovitým	deštníkovitý	k2eAgInSc7d1	deštníkovitý
habitem	habitus	k1gInSc7	habitus
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
šišek	šiška	k1gFnPc2	šiška
s	s	k7c7	s
masivními	masivní	k2eAgFnPc7d1	masivní
apofýzami	apofýza	k1gFnPc7	apofýza
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
výšky	výška	k1gFnSc2	výška
až	až	k9	až
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
stromem	strom	k1gInSc7	strom
Středozemí	středozemí	k1gNnSc2	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
prohřáté	prohřátý	k2eAgNnSc1d1	prohřáté
<g/>
,	,	kIx,	,
suché	suchý	k2eAgFnPc1d1	suchá
a	a	k8xC	a
kypré	kyprý	k2eAgFnPc1d1	kyprá
půdy	půda	k1gFnPc1	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
pinie	pinie	k1gFnSc2	pinie
je	být	k5eAaImIp3nS	být
jehličnatý	jehličnatý	k2eAgInSc1d1	jehličnatý
neopadavý	opadavý	k2eNgInSc1d1	neopadavý
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
své	svůj	k3xOyFgFnSc2	svůj
dospělosti	dospělost	k1gFnSc2	dospělost
asi	asi	k9	asi
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
deštníkovitého	deštníkovitý	k2eAgInSc2d1	deštníkovitý
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
v	v	k7c6	v
obryse	obrys	k1gInSc6	obrys
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
eliptická	eliptický	k2eAgFnSc1d1	eliptická
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
polokulovitá	polokulovitý	k2eAgFnSc1d1	polokulovitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
bývá	bývat	k5eAaImIp3nS	bývat
mnohdy	mnohdy	k6eAd1	mnohdy
pokřivený	pokřivený	k2eAgInSc1d1	pokřivený
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
v	v	k7c4	v
mohutné	mohutný	k2eAgFnPc4d1	mohutná
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
stromy	strom	k1gInPc1	strom
mají	mít	k5eAaImIp3nP	mít
borku	borka	k1gFnSc4	borka
šedou	šedý	k2eAgFnSc4d1	šedá
nebo	nebo	k8xC	nebo
světle	světlo	k1gNnSc6	světlo
šedou	šedý	k2eAgFnSc4d1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
exempláře	exemplář	k1gInPc1	exemplář
ji	on	k3xPp3gFnSc4	on
mají	mít	k5eAaImIp3nP	mít
silně	silně	k6eAd1	silně
brázditou	brázditý	k2eAgFnSc4d1	brázditá
<g/>
,	,	kIx,	,
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
se	se	k3xPyFc4	se
do	do	k7c2	do
plošek	ploška	k1gFnPc2	ploška
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
plošky	ploška	k1gFnPc1	ploška
připomínají	připomínat	k5eAaImIp3nP	připomínat
šupiny	šupina	k1gFnPc4	šupina
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
větévky	větévka	k1gFnPc1	větévka
jsou	být	k5eAaImIp3nP	být
šedozelené	šedozelený	k2eAgFnPc1d1	šedozelená
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
s	s	k7c7	s
pupeny	pupen	k1gInPc7	pupen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
světlehnědé	světlehnědý	k2eAgFnSc2d1	světlehnědá
šupiny	šupina	k1gFnSc2	šupina
s	s	k7c7	s
nepravidelně	pravidelně	k6eNd1	pravidelně
rozštěpenými	rozštěpený	k2eAgInPc7d1	rozštěpený
okraji	okraj	k1gInPc7	okraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jehlice	jehlice	k1gFnPc1	jehlice
jsou	být	k5eAaImIp3nP	být
tmavohnědé	tmavohnědý	k2eAgInPc4d1	tmavohnědý
až	až	k6eAd1	až
šedozelené	šedozelený	k2eAgInPc4d1	šedozelený
s	s	k7c7	s
velice	velice	k6eAd1	velice
tenkými	tenký	k2eAgInPc7d1	tenký
podélnými	podélný	k2eAgInPc7d1	podélný
proužky	proužek	k1gInPc7	proužek
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgInPc1d1	špičatý
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
hodně	hodně	k6eAd1	hodně
volné	volný	k2eAgNnSc1d1	volné
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
mm	mm	kA	mm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brachyblastech	brachyblast	k1gInPc6	brachyblast
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
jehlicích	jehlice	k1gFnPc6	jehlice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válcovité	válcovitý	k2eAgFnPc1d1	válcovitá
samčí	samčí	k2eAgFnPc1d1	samčí
šištice	šištice	k1gFnPc1	šištice
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
kolem	kolem	k7c2	kolem
1	[number]	k4	1
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
počtech	počet	k1gInPc6	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
své	svůj	k3xOyFgFnSc2	svůj
zralosti	zralost	k1gFnSc2	zralost
jsou	být	k5eAaImIp3nP	být
samičí	samičí	k2eAgFnPc1d1	samičí
šištice	šištice	k1gFnPc1	šištice
leskle	leskle	k6eAd1	leskle
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
hnědočervené	hnědočervený	k2eAgFnPc1d1	hnědočervená
<g/>
,	,	kIx,	,
asi	asi	k9	asi
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
cm	cm	kA	cm
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
vejcovité	vejcovitý	k2eAgFnPc1d1	vejcovitá
až	až	k6eAd1	až
oválné	oválný	k2eAgFnPc1d1	oválná
<g/>
.	.	kIx.	.
</s>
<s>
Semínka	semínko	k1gNnPc1	semínko
jsou	být	k5eAaImIp3nP	být
jedlá	jedlý	k2eAgNnPc1d1	jedlé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
až	až	k9	až
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
opylení	opylení	k1gNnSc6	opylení
(	(	kIx(	(
<g/>
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
až	až	k9	až
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
jehličnanem	jehličnan	k1gInSc7	jehličnan
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zde	zde	k6eAd1	zde
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
nedaleko	nedaleko	k7c2	nedaleko
Mediteránu	mediterán	k1gInSc2	mediterán
<g/>
,	,	kIx,	,
u	u	k7c2	u
jeho	jeho	k3xOp3gNnSc2	jeho
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
borovice	borovice	k1gFnSc1	borovice
hojně	hojně	k6eAd1	hojně
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nároky	nárok	k1gInPc1	nárok
==	==	k?	==
</s>
</p>
<p>
<s>
Pinie	pinie	k1gFnPc1	pinie
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
prohřáté	prohřátý	k2eAgFnSc2d1	prohřátá
<g/>
,	,	kIx,	,
suché	suchý	k2eAgFnSc2d1	suchá
a	a	k8xC	a
kypré	kyprý	k2eAgFnSc2d1	kyprá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
přežít	přežít	k5eAaPmF	přežít
ani	ani	k8xC	ani
mírnou	mírný	k2eAgFnSc4d1	mírná
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesnesou	snést	k5eNaPmIp3nP	snést
mráz	mráz	k1gInSc4	mráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Háje	háj	k1gInPc1	háj
a	a	k8xC	a
stromořadí	stromořadí	k1gNnPc1	stromořadí
pinie	pinie	k1gFnSc2	pinie
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
krásné	krásný	k2eAgFnPc1d1	krásná
a	a	k8xC	a
malebné	malebný	k2eAgFnPc1d1	malebná
<g/>
,	,	kIx,	,
dotvářejí	dotvářet	k5eAaImIp3nP	dotvářet
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Pinie	pinie	k1gFnPc1	pinie
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
kromě	kromě	k7c2	kromě
estetického	estetický	k2eAgInSc2d1	estetický
i	i	k8xC	i
svůj	svůj	k3xOyFgInSc4	svůj
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgNnSc1d1	odolné
vůči	vůči	k7c3	vůči
silnému	silný	k2eAgInSc3d1	silný
větru	vítr	k1gInSc3	vítr
a	a	k8xC	a
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
krajině	krajina	k1gFnSc6	krajina
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
větrolamy	větrolam	k1gInPc1	větrolam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Semena	semeno	k1gNnPc1	semeno
borovice	borovice	k1gFnSc2	borovice
pinie	pinie	k1gFnSc2	pinie
jsou	být	k5eAaImIp3nP	být
jedlá	jedlý	k2eAgNnPc1d1	jedlé
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
piniové	piniový	k2eAgMnPc4d1	piniový
oříšky	oříšek	k1gMnPc4	oříšek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
pinocchi	pinocchi	k6eAd1	pinocchi
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
piňolky	piňolek	k1gInPc1	piňolek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
konzumací	konzumace	k1gFnSc7	konzumace
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
je	být	k5eAaImIp3nS	být
zbavit	zbavit	k5eAaPmF	zbavit
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
osemení	osemení	k1gNnSc2	osemení
<g/>
,	,	kIx,	,
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
pražit	pražit	k5eAaImF	pražit
a	a	k8xC	a
solit	solit	k5eAaImF	solit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
delikatesu	delikatesa	k1gFnSc4	delikatesa
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
se	se	k3xPyFc4	se
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
různých	různý	k2eAgInPc2d1	různý
pokrmů	pokrm	k1gInPc2	pokrm
převážně	převážně	k6eAd1	převážně
italské	italský	k2eAgFnSc2d1	italská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
směsi	směs	k1gFnPc1	směs
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
nejedlých	jedlý	k2eNgNnPc2d1	nejedlé
semínek	semínko	k1gNnPc2	semínko
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
borovic	borovice	k1gFnPc2	borovice
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
borovice	borovice	k1gFnPc1	borovice
Armandovy	Armandův	k2eAgFnPc1d1	Armandova
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c4	na
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
i	i	k8xC	i
týdnů	týden	k1gInPc2	týden
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vnímání	vnímání	k1gNnSc4	vnímání
chuti	chuť	k1gFnSc2	chuť
–	–	k?	–
veškeré	veškerý	k3xTgFnPc1	veškerý
chutě	chuť	k1gFnPc1	chuť
jsou	být	k5eAaImIp3nP	být
vnímané	vnímaný	k2eAgInPc1d1	vnímaný
jako	jako	k8xC	jako
nepříjemně	příjemně	k6eNd1	příjemně
hořké	hořký	k2eAgNnSc1d1	hořké
<g/>
.	.	kIx.	.
<g/>
Dřevo	dřevo	k1gNnSc1	dřevo
pinie	pinie	k1gFnSc2	pinie
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využíváno	využívat	k5eAaPmNgNnS	využívat
především	především	k6eAd1	především
jako	jako	k8xS	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
(	(	kIx(	(
<g/>
okna	okno	k1gNnPc4	okno
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
schody	schod	k1gInPc4	schod
<g/>
,	,	kIx,	,
kuchyňský	kuchyňský	k2eAgInSc4d1	kuchyňský
nábytek	nábytek	k1gInSc4	nábytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
druhům	druh	k1gMnPc3	druh
borovic	borovice	k1gFnPc2	borovice
<g/>
,	,	kIx,	,
minimum	minimum	k1gNnSc4	minimum
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
hl.	hl.	k?	hl.
Andreas	Andreas	k1gInSc1	Andreas
Bärtels	Bärtels	k1gInSc1	Bärtels
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
přeloženo	přeložen	k2eAgNnSc4d1	přeloženo
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
originálu	originál	k1gInSc2	originál
Bäume	Bäum	k1gInSc5	Bäum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
a	a	k8xC	a
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85830	[number]	k4	85830
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
hl.	hl.	k?	hl.
Gregor	Gregor	k1gMnSc1	Gregor
Aas	Aas	k1gMnSc1	Aas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
-	-	kIx~	-
STROMY	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
borovice	borovice	k1gFnSc2	borovice
pinie	pinie	k1gFnSc2	pinie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pinus	Pinus	k1gMnSc1	Pinus
pinea	pinea	k1gMnSc1	pinea
i	i	k9	i
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
</s>
</p>
<p>
<s>
Piniové	piniový	k2eAgInPc1d1	piniový
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
účinné	účinný	k2eAgFnPc1d1	účinná
látky	látka	k1gFnPc1	látka
</s>
</p>
