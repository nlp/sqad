<s>
Pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Útok	útok	k1gInSc1
fríských	fríský	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
na	na	k7c4
Damiettu	Damietta	k1gFnSc4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Ajjúbovský	Ajjúbovský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
v	v	k7c6
Egyptě	Egypt	k1gInSc6
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Vítězství	vítězství	k1gNnSc1
egyptských	egyptský	k2eAgInPc2d1
Ajjúbovců	Ajjúbovec	k1gInPc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Tyrolsko	Tyrolsko	k1gNnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
</s>
<s>
Durynsko	Durynsko	k1gNnSc1
</s>
<s>
Meránie	Meránie	k1gFnSc1
</s>
<s>
Bádensko	Bádensko	k1gNnSc1
</s>
<s>
Holandsko	Holandsko	k1gNnSc1
</s>
<s>
Brabantsko	Brabantsko	k6eAd1
</s>
<s>
Frísko	Frísko	k1gNnSc1
</s>
<s>
Kolínsko	Kolínsko	k1gNnSc1
</s>
<s>
Brixenské	brixenský	k2eAgNnSc1d1
knížecí	knížecí	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
</s>
<s>
Pasovské	Pasovský	k2eAgNnSc1d1
knížecí	knížecí	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
</s>
<s>
Sicilské	sicilský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Halič	Halič	k1gFnSc1
</s>
<s>
Chorvatsko-dalmátské	Chorvatsko-dalmátský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Flandry	Flandry	k1gInPc1
</s>
<s>
Burgundsko	Burgundsko	k1gNnSc1
</s>
<s>
Rodez	Rodez	k1gMnSc1
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Janovská	Janovská	k1gFnSc1
republika	republika	k1gFnSc1
</s>
<s>
muslimští	muslimský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Rúmský	Rúmský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
</s>
<s>
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Antiochie-Tripolis	Antiochie-Tripolis	k1gFnSc1
</s>
<s>
rytířsko-vojenské	rytířsko-vojenský	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
</s>
<s>
Ajjúbovský	Ajjúbovský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Ajjúbovský	Ajjúbovský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
Damašský	damašský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Homský	Homský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Chamátský	Chamátský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
(	(	kIx(
<g/>
Hamá	hamat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Aleppský	Aleppský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Baalbecký	Baalbecký	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gInSc5
Bohemund	Bohemund	k1gInSc4
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hugo	Hugo	k1gMnSc1
Kyperský	kyperský	k2eAgInSc1d1
Kajkávús	Kajkávús	k1gInSc1
I.	I.	kA
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufský	Štaufský	k2eAgInSc1d1
Leopold	Leopolda	k1gFnPc2
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Albert	Alberta	k1gFnPc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolský	tyrolský	k2eAgInSc1d1
Ludvík	Ludvík	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
Ludvík	Ludvík	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Durynský	durynský	k2eAgMnSc1d1
†	†	k?
Vilém	Vilém	k1gMnSc1
Holandský	holandský	k2eAgMnSc1d1
Jindřich	Jindřich	k1gMnSc1
Brabantský	Brabantský	k2eAgMnSc1d1
Ota	Ota	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
z	z	k7c2
Andechsu	Andechs	k1gInSc2
Hadmar	Hadmara	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Kuenringu	Kuenring	k1gInSc2
Pierre	Pierr	k1gInSc5
de	de	k?
Montaigu	Montaig	k1gInSc6
Gaerin	Gaerin	k1gInSc1
de	de	k?
Montaigu	Montaig	k1gInSc2
Heřman	Heřman	k1gMnSc1
ze	z	k7c2
Salzy	Salza	k1gFnSc2
Ondřej	Ondřej	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ugrin	Ugrin	k1gMnSc1
Čák	čáka	k1gFnPc2
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
Odo	Odo	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burgundský	burgundský	k2eAgMnSc1d1
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Rodezu	Rodez	k1gInSc2
†	†	k?
Pelagio	Pelagio	k1gMnSc1
z	z	k7c2
Albana	Alban	k1gMnSc2
</s>
<s>
Al-Kámil	Al-Kámit	k5eAaPmAgMnS
Al-Mu	Al-Ma	k1gFnSc4
<g/>
'	'	kIx"
<g/>
azzam	azzam	k1gInSc1
Al-Mudžahid	Al-Mudžahida	k1gFnPc2
Al-Muzaffar	Al-Muzaffar	k1gInSc1
Mahmúd	Mahmúd	k1gMnSc1
Al-Aziz	Al-Aziza	k1gFnPc2
Muhammad	Muhammad	k1gInSc1
Bahramšáh	Bahramšáh	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
32	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
Pátá	pátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1213	#num#	k4
papežem	papež	k1gMnSc7
Inocentem	Inocent	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1217	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1221	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
mocenské	mocenský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
dynastie	dynastie	k1gFnSc2
Ajúbovců	Ajúbovec	k1gInPc2
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
a	a	k8xC
osvobodit	osvobodit	k5eAaPmF
od	od	k7c2
nich	on	k3xPp3gInPc2
obsazenou	obsazený	k2eAgFnSc4d1
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
a	a	k8xC
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Svolání	svolání	k1gNnSc1
výpravy	výprava	k1gFnSc2
</s>
<s>
Papež	Papež	k1gMnSc1
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1213	#num#	k4
vydal	vydat	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
bulu	bula	k1gFnSc4
<g/>
,	,	kIx,
Quia	Quia	k1gMnSc1
maior	maior	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
svolávala	svolávat	k5eAaImAgFnS
veškeré	veškerý	k3xTgNnSc4
křesťanstvo	křesťanstvo	k1gNnSc4
k	k	k7c3
nové	nový	k2eAgFnSc3d1
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropští	evropský	k2eAgMnPc1d1
králové	král	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
plně	plně	k6eAd1
zaměstnáni	zaměstnat	k5eAaPmNgMnP
bojem	boj	k1gInSc7
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
však	však	k9
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
jejich	jejich	k3xOp3gFnSc4
pomoc	pomoc	k1gFnSc4
nežádal	žádat	k5eNaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
předchozí	předchozí	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
vedli	vést	k5eAaImAgMnP
králové	král	k1gMnPc1
(	(	kIx(
<g/>
Druhá	druhý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
)	)	kIx)
skončila	skončit	k5eAaPmAgFnS
fiaskem	fiasko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
nařídil	nařídit	k5eAaPmAgMnS
začít	začít	k5eAaPmF
s	s	k7c7
náboženskými	náboženský	k2eAgInPc7d1
průvody	průvod	k1gInPc7
<g/>
,	,	kIx,
kázáním	kázání	k1gNnSc7
a	a	k8xC
modlitbami	modlitba	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
věc	věc	k1gFnSc4
získal	získat	k5eAaPmAgMnS
prostý	prostý	k2eAgInSc4d1
lid	lid	k1gInSc4
<g/>
,	,	kIx,
nižší	nízký	k2eAgFnSc4d2
šlechtu	šlechta	k1gFnSc4
a	a	k8xC
rytíře	rytíř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Holandský	holandský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
</s>
<s>
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
byla	být	k5eAaImAgFnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
kázána	kázán	k2eAgFnSc1d1
Robertem	Robert	k1gMnSc7
z	z	k7c2
Courçonu	Courçon	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
neměla	mít	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
informace	informace	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
velký	velký	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
a	a	k8xC
k	k	k7c3
výpravě	výprava	k1gFnSc3
se	se	k3xPyFc4
mnoho	mnoho	k4c1
francouzských	francouzský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
měli	mít	k5eAaImAgMnP
stejně	stejně	k6eAd1
plné	plný	k2eAgFnPc4d1
ruce	ruka	k1gFnPc4
práce	práce	k1gFnSc2
s	s	k7c7
Albigenskou	albigenský	k2eAgFnSc7d1
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
proti	proti	k7c3
heretickým	heretický	k2eAgInPc3d1
katarům	katar	k1gInPc3
<g/>
,	,	kIx,
nepřipojilo	připojit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1215	#num#	k4
papež	papež	k1gMnSc1
svolal	svolat	k5eAaPmAgMnS
Čtvrtý	čtvrtý	k4xOgInSc4
lateránský	lateránský	k2eAgInSc4d1
koncil	koncil	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
patriarchou	patriarcha	k1gMnSc7
Raoulem	Raoul	k1gInSc7
de	de	k?
Mérencourtem	Mérencourt	k1gInSc7
mezi	mezi	k7c7
ostatními	ostatní	k2eAgFnPc7d1
církevními	církevní	k2eAgFnPc7d1
záležitostmi	záležitost	k1gFnPc7
probírali	probírat	k5eAaImAgMnP
také	také	k9
znovuobnovení	znovuobnovení	k1gNnSc4
křesťanského	křesťanský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nová	nový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
pod	pod	k7c7
plným	plný	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
papežství	papežství	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nevymkla	vymknout	k5eNaPmAgFnS
kontrole	kontrola	k1gFnSc3
jako	jako	k9
čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
zneužili	zneužít	k5eAaPmAgMnP
Benátčané	Benátčan	k1gMnPc1
k	k	k7c3
dobytí	dobytí	k1gNnSc3
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1216	#num#	k4
pro	pro	k7c4
křižáky	křižák	k1gInPc4
naplánoval	naplánovat	k5eAaBmAgInS
setkání	setkání	k1gNnPc4
v	v	k7c6
Brindisi	Brindisi	k1gNnSc6
a	a	k8xC
zakázal	zakázat	k5eAaPmAgMnS
obchod	obchod	k1gInSc4
s	s	k7c7
muslimy	muslim	k1gMnPc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
ochrany	ochrana	k1gFnSc2
křižáckých	křižácký	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
křižák	křižák	k1gInSc4
mohl	moct	k5eAaImAgInS
přijmout	přijmout	k5eAaPmF
odpustek	odpustek	k1gInSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
ale	ale	k9
mohl	moct	k5eAaImAgInS
přijmout	přijmout	k5eAaPmF
rovněž	rovněž	k9
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
křižákům	křižák	k1gMnPc3
finančně	finančně	k6eAd1
nebo	nebo	k8xC
jinak	jinak	k6eAd1
vypomohl	vypomoct	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
výpravy	výprava	k1gFnSc2
neúčastnil	účastnit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
a	a	k8xC
Uhersko	Uhersko	k1gNnSc1
</s>
<s>
Zprávu	zpráva	k1gFnSc4
o	o	k7c6
nově	nově	k6eAd1
chystané	chystaný	k2eAgFnSc6d1
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
přinesl	přinést	k5eAaPmAgInS
do	do	k7c2
německých	německý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Oliver	Olivra	k1gFnPc2
z	z	k7c2
Cologne	Cologn	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufský	Štaufský	k2eAgMnSc1d1
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
zkusil	zkusit	k5eAaPmAgMnS
spojit	spojit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1215	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgMnS
ten	ten	k3xDgMnSc1
poslední	poslední	k2eAgMnSc1d1
monarcha	monarcha	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
by	by	kYmCp3nS
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
spojit	spojit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
papežství	papežství	k1gNnSc1
vůči	vůči	k7c3
němu	on	k3xPp3gInSc3
nechalo	nechat	k5eAaPmAgNnS
slyšet	slyšet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1216	#num#	k4
však	však	k9
Inocenc	Inocenc	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Honorius	Honorius	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
účasti	účast	k1gFnSc2
vyškrtl	vyškrtnout	k5eAaPmAgInS
a	a	k8xC
místo	místo	k7c2
něj	on	k3xPp3gInSc2
začal	začít	k5eAaPmAgInS
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
rakouským	rakouský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
Leopoldem	Leopold	k1gMnSc7
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Ondřejem	Ondřej	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
tažení	tažení	k1gNnSc2
</s>
<s>
Chronologický	chronologický	k2eAgInSc4d1
postup	postup	k1gInSc4
páté	pátý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1217	#num#	k4
opustila	opustit	k5eAaPmAgFnS
evropská	evropský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
Akkon	Akkona	k1gFnPc2
a	a	k8xC
spojila	spojit	k5eAaPmAgFnS
se	se	k3xPyFc4
s	s	k7c7
vojsky	vojsky	k6eAd1
kyperského	kyperský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Huga	Hugo	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jana	Jan	k1gMnSc2
z	z	k7c2
Brienne	Brienn	k1gInSc5
a	a	k8xC
antiochijským	antiochijský	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
Bohemundem	Bohemund	k1gMnSc7
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zahájili	zahájit	k5eAaPmAgMnP
útok	útok	k1gInSc4
na	na	k7c4
Ajúbovce	Ajúbovec	k1gInPc4
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
</s>
<s>
V	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
obránci	obránce	k1gMnPc1
preventivně	preventivně	k6eAd1
rozebrali	rozebrat	k5eAaPmAgMnP
zdi	zeď	k1gFnSc3
a	a	k8xC
opevnění	opevnění	k1gNnSc3
města	město	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
křižáci	křižák	k1gMnPc1
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
obsazení	obsazení	k1gNnSc6
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
Jeruzalém	Jeruzalém	k1gInSc4
ubránit	ubránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimové	muslim	k1gMnPc1
poté	poté	k6eAd1
uprchli	uprchnout	k5eAaPmAgMnP
z	z	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
ze	z	k7c2
strachu	strach	k1gInSc2
z	z	k7c2
opakování	opakování	k1gNnSc2
masakru	masakr	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
po	po	k7c4
dobytí	dobytí	k1gNnSc4
města	město	k1gNnSc2
křižáky	křižák	k1gMnPc4
po	po	k7c6
první	první	k4xOgFnSc6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1099	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Ajúbovci	Ajúbovec	k1gMnPc1
se	se	k3xPyFc4
o	o	k7c4
boj	boj	k1gInSc4
nezajímali	zajímat	k5eNaImAgMnP
a	a	k8xC
když	když	k8xS
ani	ani	k8xC
žádná	žádný	k3yNgFnSc1
ofenzíva	ofenzíva	k1gFnSc1
nepřicházela	přicházet	k5eNaImAgFnS
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Bohemund	Bohemund	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
Hugo	Hugo	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1218	#num#	k4
vrátili	vrátit	k5eAaPmAgMnP
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Spojenectví	spojenectví	k1gNnSc1
s	s	k7c7
Ikonyjským	Ikonyjský	k2eAgInSc7d1
sultanátem	sultanát	k1gInSc7
</s>
<s>
Později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1218	#num#	k4
dorazil	dorazit	k5eAaPmAgMnS
Oliver	Oliver	k1gMnSc1
z	z	k7c2
Cologne	Cologn	k1gInSc5
společně	společně	k6eAd1
s	s	k7c7
novou	nový	k2eAgFnSc7d1
německou	německý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
hrabě	hrabě	k1gMnSc1
Vilém	Vilém	k1gMnSc1
s	s	k7c7
holandskými	holandský	k2eAgFnPc7d1
<g/>
,	,	kIx,
vlámskými	vlámský	k2eAgFnPc7d1
a	a	k8xC
frískými	fríský	k2eAgFnPc7d1
posilami	posila	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Leopoldem	Leopold	k1gMnSc7
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
Janem	Jan	k1gMnSc7
z	z	k7c2
Brienne	Brienn	k1gInSc5
naplánovali	naplánovat	k5eAaBmAgMnP
útok	útok	k1gInSc4
na	na	k7c4
egyptskou	egyptský	k2eAgFnSc4d1
Damiettu	Damietta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
plán	plán	k1gInSc1
měl	mít	k5eAaImAgInS
větší	veliký	k2eAgFnSc4d2
naději	naděje	k1gFnSc4
na	na	k7c4
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
spojili	spojit	k5eAaPmAgMnP
s	s	k7c7
ikonyjským	ikonyjský	k2eAgMnSc7d1
sultánem	sultán	k1gMnSc7
Keykavem	Keykav	k1gInSc7
I.	I.	kA
Ten	ten	k3xDgInSc1
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c6
Ajúbovce	Ajúbovka	k1gFnSc6
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
a	a	k8xC
uvolnil	uvolnit	k5eAaPmAgMnS
tak	tak	k6eAd1
křižáků	křižák	k1gMnPc2
cestu	cesta	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Damiettu	Damietta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Srpen	srpen	k1gInSc1
1219	#num#	k4
<g/>
,	,	kIx,
František	František	k1gMnSc1
z	z	k7c2
Assisi	Assis	k1gMnSc3
před	před	k7c7
sultánem	sultán	k1gMnSc7
Al-Kámilem	Al-Kámil	k1gMnSc7
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1218	#num#	k4
začali	začít	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
s	s	k7c7
obléháním	obléhání	k1gNnSc7
Damietty	Damietta	k1gFnSc2
a	a	k8xC
navzdory	navzdory	k7c3
odporu	odpor	k1gInSc3
nepřipraveného	připravený	k2eNgMnSc2d1
sultána	sultán	k1gMnSc2
Al-Adila	Al-Adil	k1gMnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
stála	stát	k5eAaImAgFnS
venku	venku	k6eAd1
za	za	k7c7
městem	město	k1gNnSc7
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobýt	dobýt	k5eAaPmF
samotné	samotný	k2eAgNnSc1d1
město	město	k1gNnSc1
už	už	k6eAd1
bylo	být	k5eAaImAgNnS
složitější	složitý	k2eAgNnSc1d2
<g/>
,	,	kIx,
protože	protože	k8xS
nemoci	nemoc	k1gFnPc1
způsobily	způsobit	k5eAaPmAgFnP
křižákům	křižák	k1gMnPc3
citelné	citelný	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
mrtvými	mrtvý	k1gMnPc7
byl	být	k5eAaImAgMnS
i	i	k9
Robert	Robert	k1gMnSc1
z	z	k7c2
Courçonu	Courçon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Adil	Al-Adil	k1gFnPc2
zatím	zatím	k6eAd1
také	také	k9
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
zaujal	zaujmout	k5eAaPmAgInS
Al-Kámil	Al-Kámil	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Honorius	Honorius	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
vyslal	vyslat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1219	#num#	k4
Pelagia	Pelagius	k1gMnSc2
Galvaniho	Galvani	k1gMnSc2
jako	jako	k8xS,k8xC
nového	nový	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Kámil	Al-Kámil	k1gMnSc1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
s	s	k7c7
křižáky	křižák	k1gInPc7
vyjednávat	vyjednávat	k5eAaImF
o	o	k7c4
míru	míra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
křižákům	křižák	k1gMnPc3
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
přenechá	přenechat	k5eAaPmIp3nS
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
od	od	k7c2
obléhání	obléhání	k1gNnSc2
Damietty	Damietta	k1gFnSc2
ustoupí	ustoupit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
Pelagio	Pelagio	k6eAd1
jeho	jeho	k3xOp3gFnSc4
nabídku	nabídka	k1gFnSc4
nepřijal	přijmout	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
to	ten	k3xDgNnSc4
samé	samý	k3xTgNnSc4
slyšel	slyšet	k5eAaImAgMnS
hrabě	hrabě	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Holandský	holandský	k2eAgMnSc1d1
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgMnS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
bojovníky	bojovník	k1gMnPc7
křižácký	křižácký	k2eAgInSc4d1
tábor	tábor	k1gInSc4
a	a	k8xC
odplul	odplout	k5eAaPmAgMnS
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
nebo	nebo	k8xC
září	zářit	k5eAaImIp3nS
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
křižáckého	křižácký	k2eAgNnSc2d1
ležení	ležení	k1gNnSc2
František	Františka	k1gFnPc2
z	z	k7c2
Assisi	Assis	k1gMnSc3
a	a	k8xC
přešel	přejít	k5eAaPmAgInS
kázat	kázat	k5eAaImF
k	k	k7c3
Al-Kámilovi	Al-Kámil	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
křižáci	křižák	k1gMnPc1
oslabili	oslabit	k5eAaPmAgMnP
sultánovy	sultánův	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
mohli	moct	k5eAaImAgMnP
tak	tak	k6eAd1
konečně	konečně	k6eAd1
obsadit	obsadit	k5eAaPmF
přístav	přístav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Okamžitě	okamžitě	k6eAd1
po	po	k7c6
dobytí	dobytí	k1gNnSc6
města	město	k1gNnSc2
začali	začít	k5eAaPmAgMnP
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
ním	on	k3xPp3gNnSc7
mezi	mezi	k7c7
sebou	se	k3xPyFc7
bojovat	bojovat	k5eAaImF
papežské	papežský	k2eAgFnPc4d1
a	a	k8xC
světské	světský	k2eAgFnPc4d1
zájmové	zájmový	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
města	město	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1220	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelagio	Pelagio	k6eAd1
Galvani	Galvan	k1gMnPc1
toto	tento	k3xDgNnSc4
nepřijal	přijmout	k5eNaPmAgMnS
a	a	k8xC
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelagio	Pelagio	k1gMnSc1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
římský	římský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
přitáhne	přitáhnout	k5eAaPmIp3nS
s	s	k7c7
čerstvými	čerstvý	k2eAgFnPc7d1
posilami	posila	k1gFnPc7
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
to	ten	k3xDgNnSc4
však	však	k9
nikdy	nikdy	k6eAd1
neudělal	udělat	k5eNaPmAgMnS
<g/>
;	;	kIx,
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
po	po	k7c6
roce	rok	k1gInSc6
nečinnosti	nečinnost	k1gFnSc2
a	a	k8xC
nezájmu	nezájem	k1gInSc2
o	o	k7c6
situaci	situace	k1gFnSc6
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
i	i	k8xC
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Jan	Jan	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gInSc5
do	do	k7c2
Damietty	Damietta	k1gMnSc2
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
křižáci	křižák	k1gMnPc1
v	v	k7c6
červenci	červenec	k1gInSc6
1221	#num#	k4
vytáhli	vytáhnout	k5eAaPmAgMnP
směrem	směr	k1gInSc7
na	na	k7c4
Káhiru	Káhira	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgMnS
již	již	k9
Al-Kamil	Al-Kamil	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
se	s	k7c7
syrskými	syrský	k2eAgInPc7d1
Ajúbovci	Ajúbovec	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
neustále	neustále	k6eAd1
útočil	útočit	k5eAaImAgMnS
sultán	sultán	k1gMnSc1
Keykavus	Keykavus	k1gMnSc1
I.	I.	kA
Křižácký	křižácký	k2eAgInSc4d1
pochod	pochod	k1gInSc4
na	na	k7c4
Káhiru	Káhira	k1gFnSc4
byl	být	k5eAaImAgInS
katastrofální	katastrofální	k2eAgInSc1d1
<g/>
;	;	kIx,
řeka	řeka	k1gFnSc1
Nil	Nil	k1gInSc1
se	se	k3xPyFc4
vylila	vylít	k5eAaPmAgFnS
z	z	k7c2
břehů	břeh	k1gInPc2
před	před	k7c4
křižáky	křižák	k1gInPc4
a	a	k8xC
zastavila	zastavit	k5eAaPmAgFnS
tak	tak	k8xC,k8xS
jejich	jejich	k3xOp3gInSc4
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suchý	suchý	k2eAgInSc4d1
kanál	kanál	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
křižáci	křižák	k1gMnPc1
před	před	k7c7
tím	ten	k3xDgNnSc7
překonali	překonat	k5eAaPmAgMnP
byl	být	k5eAaImAgInS
nyní	nyní	k6eAd1
zaplavený	zaplavený	k2eAgInSc1d1
a	a	k8xC
odřízl	odříznout	k5eAaPmAgInS
armádě	armáda	k1gFnSc3
ústupovou	ústupový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásobování	zásobování	k1gNnSc1
vázlo	váznout	k5eAaImAgNnS
a	a	k8xC
křižáci	křižák	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
zahájit	zahájit	k5eAaPmF
ústup	ústup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholem	vrchol	k1gInSc7
byl	být	k5eAaImAgMnS
noční	noční	k2eAgInSc4d1
útok	útok	k1gInSc4
Al-Kamilových	Al-Kamilův	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
za	za	k7c4
následek	následek	k1gInSc4
obrovské	obrovský	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
mezi	mezi	k7c7
křižáky	křižák	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
velitel	velitel	k1gMnSc1
Pelagio	Pelagio	k1gMnSc1
Galvani	Galvaň	k1gFnSc3
nakonec	nakonec	k6eAd1
kapituloval	kapitulovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Podmínky	podmínka	k1gFnPc1
této	tento	k3xDgFnSc2
kapitulace	kapitulace	k1gFnSc2
stanovovaly	stanovovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Damietta	Damietta	k1gFnSc1
vydá	vydat	k5eAaPmIp3nS
do	do	k7c2
rukou	ruka	k1gFnPc2
Al-Kamilovi	Al-Kamilův	k2eAgMnPc1d1
výměnou	výměna	k1gFnSc7
za	za	k7c4
propuštění	propuštění	k1gNnSc4
zajatých	zajatý	k2eAgMnPc2d1
křižáckých	křižácký	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Kamil	Al-Kamil	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
osmiletým	osmiletý	k2eAgInSc7d1
mírem	mír	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mu	on	k3xPp3gMnSc3
Evropané	Evropan	k1gMnPc1
nabídli	nabídnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
s	s	k7c7
navrácením	navrácení	k1gNnSc7
kusu	kus	k1gInSc2
pravého	pravý	k2eAgInSc2d1
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
později	pozdě	k6eAd2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
Al-Kamil	Al-Kamil	k1gMnSc1
nevlastnil	vlastnit	k5eNaImAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Fifth	Fiftha	k1gFnPc2
Crusade	Crusad	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GABRIELI	Gabriel	k1gMnSc5
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
očima	oko	k1gNnPc7
arabských	arabský	k2eAgInPc2d1
kronikářů	kronikář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
333	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
POWELL	POWELL	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
M.	M.	kA
Anatomy	anatom	k1gMnPc4
of	of	k?
a	a	k8xC
Crusade	Crusad	k1gInSc5
<g/>
.	.	kIx.
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1221	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
312	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8122	#num#	k4
<g/>
-	-	kIx~
<g/>
1323	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jonathan	Jonathan	k1gMnSc1
Riley-Smith	Riley-Smith	k1gMnSc1
(	(	kIx(
<g/>
Hrsg	Hrsg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Illustrierte	Illustriert	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
der	drát	k5eAaImRp2nS
Kreuzzüge	Kreuzzüg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankfurt	Frankfurt	k1gInSc1
<g/>
/	/	kIx~
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
1999	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
478	#num#	k4
(	(	kIx(
<g/>
Index	index	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
v.	v.	k?
Damiette	Damiett	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Damietta	Damietta	k1gFnSc1
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pátá	pátá	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc1
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgInPc3d1
Turkům	Turkovi	k1gRnPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
779431	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4204866-7	4204866-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034386	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034386	#num#	k4
</s>
