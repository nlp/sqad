<p>
<s>
Egypt	Egypt	k1gInSc4	Egypt
Central	Central	k1gFnSc2	Central
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
alternativní	alternativní	k2eAgFnSc1d1	alternativní
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Memphisu	Memphis	k1gInSc2	Memphis
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gNnSc2	Tennessee
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
silnice	silnice	k1gFnSc2	silnice
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
právě	právě	k9	právě
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
8	[number]	k4	8
vystoupeních	vystoupení	k1gNnPc6	vystoupení
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
původního	původní	k2eAgMnSc2d1	původní
prezidenta	prezident	k1gMnSc2	prezident
Lava	lava	k1gFnSc1	lava
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
Jasona	Jasona	k1gFnSc1	Jasona
Floma	Floma	k1gFnSc1	Floma
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
kapele	kapela	k1gFnSc6	kapela
dal	dát	k5eAaPmAgMnS	dát
nabídku	nabídka	k1gFnSc4	nabídka
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
viděl	vidět	k5eAaImAgMnS	vidět
jejich	jejich	k3xOp3gNnPc4	jejich
živá	živý	k2eAgNnPc4d1	živé
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Eponymní	Eponymní	k2eAgNnSc1d1	Eponymní
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Joshem	Josh	k1gInSc7	Josh
Abrahamem	Abraham	k1gMnSc7	Abraham
(	(	kIx(	(
<g/>
Velvet	Velvet	k1gMnSc1	Velvet
Revolver	revolver	k1gInSc1	revolver
<g/>
,	,	kIx,	,
Korn	Korna	k1gFnPc2	Korna
<g/>
,	,	kIx,	,
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
<g/>
,	,	kIx,	,
Staind	Stainda	k1gFnPc2	Stainda
<g/>
,	,	kIx,	,
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
zastihlo	zastihnout	k5eAaPmAgNnS	zastihnout
mnoho	mnoho	k4c4	mnoho
opoždění	opoždění	k1gNnPc2	opoždění
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgInP	být
vydány	vydán	k2eAgInPc1d1	vydán
dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Make	Mak	k1gFnSc2	Mak
Me	Me	k1gMnSc1	Me
Sick	Sick	k1gMnSc1	Sick
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Taking	Taking	k1gInSc1	Taking
You	You	k1gMnSc1	You
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
skončily	skončit	k5eAaPmAgInP	skončit
jako	jako	k8xS	jako
soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
WWE	WWE	kA	WWE
SmackDown	SmackDowno	k1gNnPc2	SmackDowno
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Raw	Raw	k?	Raw
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Over	Over	k1gInSc1	Over
and	and	k?	and
Under	Under	k1gInSc1	Under
<g/>
"	"	kIx"	"
hrála	hrát	k5eAaImAgFnS	hrát
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Condemned	Condemned	k1gMnSc1	Condemned
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
slavný	slavný	k2eAgMnSc1d1	slavný
wrestler	wrestler	k1gMnSc1	wrestler
Stone	ston	k1gInSc5	ston
Cold	Colda	k1gFnPc2	Colda
Steve	Steve	k1gMnSc1	Steve
Austin	Austin	k1gMnSc1	Austin
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
známých	známý	k2eAgFnPc2d1	známá
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xS	jako
Disturbed	Disturbed	k1gInSc1	Disturbed
<g/>
,	,	kIx,	,
Seether	Seethra	k1gFnPc2	Seethra
<g/>
,	,	kIx,	,
Sevendust	Sevendust	k1gMnSc1	Sevendust
<g/>
,	,	kIx,	,
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
This	Thisa	k1gFnPc2	Thisa
Moment	moment	k1gInSc1	moment
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
White	Whit	k1gMnSc5	Whit
Rabbit	Rabbit	k1gMnSc5	Rabbit
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Egypt	Egypt	k1gInSc4	Egypt
Central	Central	k1gMnPc1	Central
dokončili	dokončit	k5eAaPmAgMnP	dokončit
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Skidd	Skidd	k1gMnSc1	Skidd
Millsem	Mills	k1gMnSc7	Mills
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
éteru	éter	k1gInSc2	éter
vpuštěn	vpuštěn	k2eAgInSc4d1	vpuštěn
úvodní	úvodní	k2eAgInSc4d1	úvodní
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
American	American	k1gInSc4	American
Dream	Dream	k1gInSc4	Dream
tour	toura	k1gFnPc2	toura
společně	společně	k6eAd1	společně
s	s	k7c7	s
Cold	Coldo	k1gNnPc2	Coldo
a	a	k8xC	a
Kopek	kopka	k1gFnPc2	kopka
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
měla	mít	k5eAaImAgFnS	mít
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
Down	Down	k1gMnSc1	Down
the	the	k?	the
Hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
jej	on	k3xPp3gMnSc4	on
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
Abused	Abused	k1gInSc4	Abused
Romance	romance	k1gFnSc2	romance
a	a	k8xC	a
Candlelight	Candlelight	k1gMnSc1	Candlelight
Red	Red	k1gMnSc1	Red
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
s	s	k7c7	s
Burn	Burna	k1gFnPc2	Burna
Halo	halo	k1gNnSc1	halo
a	a	k8xC	a
Red	Red	k1gFnSc1	Red
Line	linout	k5eAaImIp3nS	linout
Chemistry	Chemistr	k1gMnPc4	Chemistr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pár	pár	k4xCyI	pár
dalším	další	k2eAgInPc3d1	další
festivalům	festival	k1gInPc3	festival
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
připočíst	připočíst	k5eAaPmF	připočíst
i	i	k9	i
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
Hinder	Hinder	k1gInSc1	Hinder
<g/>
,	,	kIx,	,
Saving	Saving	k1gInSc1	Saving
Abel	Abel	k1gMnSc1	Abel
a	a	k8xC	a
Adelitas	Adelitas	k1gMnSc1	Adelitas
Way	Way	k1gMnSc1	Way
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
také	také	k9	také
Pop	pop	k1gMnSc1	pop
Evil	Evil	k1gMnSc1	Evil
na	na	k7c6	na
několika	několik	k4yIc6	několik
vystoupeních	vystoupení	k1gNnPc6	vystoupení
konajících	konající	k2eAgFnPc2d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Předskakovali	předskakovat	k5eAaImAgMnP	předskakovat
skupině	skupina	k1gFnSc3	skupina
Staind	Staind	k1gInSc4	Staind
na	na	k7c4	na
Thanksgiving	Thanksgiving	k1gInSc4	Thanksgiving
Hangover	Hangovra	k1gFnPc2	Hangovra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Puddle	Puddle	k1gFnSc3	Puddle
of	of	k?	of
Mudd	Mudd	k1gInSc1	Mudd
a	a	k8xC	a
Pop	pop	k1gInSc1	pop
Evil	Evila	k1gFnPc2	Evila
na	na	k7c4	na
X	X	kA	X
Nutcracker	Nutcracker	k1gInSc4	Nutcracker
Eleven	Elevna	k1gFnPc2	Elevna
v	v	k7c4	v
Expo	Expo	k1gNnSc4	Expo
Gardens	Gardensa	k1gFnPc2	Gardensa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozpad	rozpad	k1gInSc1	rozpad
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
po	po	k7c6	po
roce	rok	k1gInSc6	rok
nečinnosti	nečinnost	k1gFnSc2	nečinnost
<g/>
,	,	kIx,	,
basista	basista	k1gMnSc1	basista
Joey	Joea	k1gFnSc2	Joea
Chicago	Chicago	k1gNnSc1	Chicago
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpěvák	zpěvák	k1gMnSc1	zpěvák
John	John	k1gMnSc1	John
Falls	Falls	k1gInSc1	Falls
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Jeff	Jeff	k1gMnSc1	Jeff
James	James	k1gMnSc1	James
od	od	k7c2	od
kapely	kapela	k1gFnSc2	kapela
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
samotnou	samotný	k2eAgFnSc4d1	samotná
znamená	znamenat	k5eAaImIp3nS	znamenat
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Joey	Joea	k1gFnPc1	Joea
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Blake	Blake	k1gNnSc2	Blake
Allisonem	Allison	k1gMnSc7	Allison
založili	založit	k5eAaPmAgMnP	založit
kapelu	kapela	k1gFnSc4	kapela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Devour	Devour	k1gMnSc1	Devour
The	The	k1gMnSc1	The
Day	Day	k1gMnSc1	Day
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
dopise	dopis	k1gInSc6	dopis
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
sděleno	sdělen	k2eAgNnSc1d1	sděleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
album	album	k1gNnSc1	album
původně	původně	k6eAd1	původně
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Murder	Murder	k1gInSc1	Murder
in	in	k?	in
the	the	k?	the
French	French	k1gInSc1	French
Quarter	quarter	k1gInSc1	quarter
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
srpen	srpen	k1gInSc4	srpen
2014	[number]	k4	2014
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
oficiální	oficiální	k2eAgInSc4d1	oficiální
konec	konec	k1gInSc4	konec
Egypt	Egypt	k1gInSc1	Egypt
Central	Central	k1gFnSc1	Central
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgInSc1d1	původní
===	===	k?	===
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Falls	Fallsa	k1gFnPc2	Fallsa
–	–	k?	–
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
James	James	k1gMnSc1	James
–	–	k?	–
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Joey	Joea	k1gFnPc1	Joea
Chicago	Chicago	k1gNnSc1	Chicago
–	–	k?	–
basa	basa	k1gFnSc1	basa
</s>
</p>
<p>
<s>
Blake	Blake	k1gFnSc1	Blake
Allison	Allisona	k1gFnPc2	Allisona
–	–	k?	–
bubny	buben	k1gInPc1	buben
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
</s>
</p>
<p>
<s>
Heath	Heath	k1gMnSc1	Heath
Hindman	Hindman	k1gMnSc1	Hindman
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Abaldo	Abaldo	k?	Abaldo
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Egypt	Egypt	k1gInSc1	Egypt
Central	Central	k1gFnSc2	Central
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
jambase	jambase	k6eAd1	jambase
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
