<s>
Springfield	Springfield	k6eAd1	Springfield
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc2	Illinois
a	a	k8xC	a
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
Sangamon	Sangamona	k1gFnPc2	Sangamona
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
117	[number]	k4	117
006	[number]	k4	006
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
po	po	k7c6	po
americkém	americký	k2eAgMnSc6d1	americký
viceprezidentovi	viceprezident	k1gMnSc6	viceprezident
Johnu	John	k1gMnSc6	John
C.	C.	kA	C.
Calhonouvi	Calhonouev	k1gFnSc6	Calhonouev
-	-	kIx~	-
Calhoun	Calhoun	k1gInSc1	Calhoun
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
obyvatele	obyvatel	k1gMnPc4	obyvatel
patřil	patřit	k5eAaImAgInS	patřit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
116	[number]	k4	116
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
75,8	[number]	k4	75,8
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
18,5	[number]	k4	18,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Villach	Villach	k1gInSc1	Villach
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
Killarney	Killarnea	k1gFnPc1	Killarnea
(	(	kIx(	(
<g/>
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
San	San	k1gFnSc1	San
Pedro	Pedro	k1gNnSc1	Pedro
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
Ashikaga	Ashikaga	k1gFnSc1	Ashikaga
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Springfield	Springfielda	k1gFnPc2	Springfielda
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Springfield	Springfielda	k1gFnPc2	Springfielda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
