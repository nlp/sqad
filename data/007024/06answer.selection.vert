<s>
Tento	tento	k3xDgInSc1	tento
snímač	snímač	k1gInSc1	snímač
má	mít	k5eAaImIp3nS	mít
rozlišení	rozlišení	k1gNnSc4	rozlišení
12	[number]	k4	12
Megapixelů	Megapixel	k1gInPc2	Megapixel
(	(	kIx(	(
<g/>
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
snímačem	snímač	k1gInSc7	snímač
v	v	k7c4	v
Nikon	Nikon	k1gInSc4	Nikon
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgNnSc3	jenž
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
menší	malý	k2eAgNnSc1d2	menší
tělo	tělo	k1gNnSc1	tělo
bez	bez	k7c2	bez
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
bateriového	bateriový	k2eAgInSc2d1	bateriový
gripu	grip	k1gInSc2	grip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
