<s>
Nikon	Nikon	k1gNnSc1	Nikon
D700	D700	k1gMnSc2	D700
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
firmy	firma	k1gFnSc2	firma
Nikon	Nikona	k1gFnPc2	Nikona
se	s	k7c7	s
snímačem	snímač	k1gInSc7	snímač
velikosti	velikost	k1gFnSc2	velikost
políčka	políčko	k1gNnSc2	políčko
kinofilmu	kinofilm	k1gInSc2	kinofilm
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
snímač	snímač	k1gInSc1	snímač
má	mít	k5eAaImIp3nS	mít
rozlišení	rozlišení	k1gNnSc4	rozlišení
12	[number]	k4	12
Megapixelů	Megapixel	k1gInPc2	Megapixel
(	(	kIx(	(
<g/>
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
snímačem	snímač	k1gInSc7	snímač
v	v	k7c4	v
Nikon	Nikon	k1gInSc4	Nikon
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgNnSc3	jenž
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
menší	malý	k2eAgNnSc1d2	menší
tělo	tělo	k1gNnSc1	tělo
bez	bez	k7c2	bez
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
bateriového	bateriový	k2eAgInSc2d1	bateriový
gripu	grip	k1gInSc2	grip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nikon	Nikona	k1gFnPc2	Nikona
D700	D700	k1gFnSc2	D700
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
