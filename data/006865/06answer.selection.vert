<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
velkomučedníka	velkomučedník	k1gMnSc2	velkomučedník
a	a	k8xC	a
vítěze	vítěz	k1gMnSc2	vítěz
Jiřího	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
о	о	k?	о
С	С	k?	С
В	В	k?	В
и	и	k?	и
П	П	k?	П
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
О	О	k?	О
С	С	k?	С
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
ruský	ruský	k2eAgInSc4d1	ruský
vojenský	vojenský	k2eAgInSc4d1	vojenský
řád	řád	k1gInSc4	řád
založený	založený	k2eAgInSc4d1	založený
Kateřinou	Kateřina	k1gFnSc7	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
<g/>
.	.	kIx.	.
</s>
