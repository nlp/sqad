<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
velkomučedníka	velkomučedník	k1gMnSc2	velkomučedník
a	a	k8xC	a
vítěze	vítěz	k1gMnSc2	vítěz
Jiřího	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
о	о	k?	о
С	С	k?	С
В	В	k?	В
и	и	k?	и
П	П	k?	П
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Řád	řád	k1gInSc1	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
О	О	k?	О
С	С	k?	С
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
ruský	ruský	k2eAgInSc4d1	ruský
vojenský	vojenský	k2eAgInSc4d1	vojenský
řád	řád	k1gInSc4	řád
založený	založený	k2eAgInSc4d1	založený
Kateřinou	Kateřina	k1gFnSc7	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
udělován	udělovat	k5eAaImNgInS	udělovat
důstojníkům	důstojník	k1gMnPc3	důstojník
a	a	k8xC	a
generálům	generál	k1gMnPc3	generál
za	za	k7c4	za
prokázanou	prokázaný	k2eAgFnSc4d1	prokázaná
osobní	osobní	k2eAgFnSc4d1	osobní
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
řád	řád	k1gInSc1	řád
zaniknul	zaniknout	k5eAaPmAgInS	zaniknout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
monarchii	monarchie	k1gFnSc4	monarchie
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
vojenské	vojenský	k2eAgNnSc4d1	vojenské
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
carevnou	carevna	k1gFnSc7	carevna
Kateřinou	Kateřina	k1gFnSc7	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1769	[number]	k4	1769
<g/>
.	.	kIx.	.
</s>
<s>
Řádový	řádový	k2eAgInSc1d1	řádový
statut	statut	k1gInSc1	statut
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
hned	hned	k9	hned
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
a	a	k8xC	a
stanovoval	stanovovat	k5eAaImAgMnS	stanovovat
nejen	nejen	k6eAd1	nejen
způsoby	způsob	k1gInPc4	způsob
odměňování	odměňování	k1gNnSc2	odměňování
ale	ale	k8xC	ale
také	také	k9	také
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
držitelů	držitel	k1gMnPc2	držitel
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
také	také	k9	také
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
důstojníky	důstojník	k1gMnPc4	důstojník
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
statut	statut	k1gInSc1	statut
několikrát	několikrát	k6eAd1	několikrát
upraven	upravit	k5eAaPmNgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
vydal	vydat	k5eAaPmAgMnS	vydat
nový	nový	k2eAgInSc4d1	nový
statut	statut	k1gInSc4	statut
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spojoval	spojovat	k5eAaImAgMnS	spojovat
všechny	všechen	k3xTgInPc4	všechen
doposud	doposud	k6eAd1	doposud
provedené	provedený	k2eAgFnPc1d1	provedená
úpravy	úprava	k1gFnPc1	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
statut	statut	k1gInSc1	statut
definoval	definovat	k5eAaBmAgInS	definovat
řádový	řádový	k2eAgInSc1d1	řádový
státek	státek	k1gInSc1	státek
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
upřesňoval	upřesňovat	k5eAaImAgMnS	upřesňovat
způsoby	způsoba	k1gFnPc4	způsoba
nošení	nošení	k1gNnSc2	nošení
řádu	řád	k1gInSc2	řád
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
tříd	třída	k1gFnPc2	třída
nebo	nebo	k8xC	nebo
řádový	řádový	k2eAgInSc1d1	řádový
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
statut	statut	k1gInSc1	statut
stanovil	stanovit	k5eAaPmAgInS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
udělení	udělení	k1gNnSc3	udělení
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
výhradně	výhradně	k6eAd1	výhradně
za	za	k7c4	za
"	"	kIx"	"
<g/>
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
chrabrost	chrabrost	k1gFnSc4	chrabrost
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
uvážení	uvážení	k1gNnSc2	uvážení
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
řádového	řádový	k2eAgInSc2d1	řádový
sněmu	sněm	k1gInSc2	sněm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
organizoval	organizovat	k5eAaBmAgMnS	organizovat
hodnostní	hodnostní	k2eAgNnSc4d1	hodnostní
pořadí	pořadí	k1gNnSc4	pořadí
nositelů	nositel	k1gMnPc2	nositel
řádu	řád	k1gInSc2	řád
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
vojenským	vojenský	k2eAgFnPc3d1	vojenská
hodnostem	hodnost	k1gFnPc3	hodnost
a	a	k8xC	a
tabulce	tabulka	k1gFnSc3	tabulka
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nositelé	nositel	k1gMnPc1	nositel
řádu	řád	k1gInSc2	řád
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
měli	mít	k5eAaImAgMnP	mít
u	u	k7c2	u
imperátorského	imperátorský	k2eAgInSc2d1	imperátorský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
veřejných	veřejný	k2eAgFnPc6d1	veřejná
událostech	událost	k1gFnPc6	událost
postavení	postavení	k1gNnSc4	postavení
rovnající	rovnající	k2eAgNnSc4d1	rovnající
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hodnosti	hodnost	k1gFnSc2	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k8xC	i
kdyby	kdyby	kYmCp3nP	kdyby
měli	mít	k5eAaImAgMnP	mít
sami	sám	k3xTgMnPc1	sám
nižší	nízký	k2eAgInPc1d2	nižší
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
definoval	definovat	k5eAaBmAgMnS	definovat
výši	výše	k1gFnSc4	výše
penze	penze	k1gFnSc2	penze
pro	pro	k7c4	pro
vyznamenané	vyznamenaný	k2eAgNnSc4d1	vyznamenané
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
700	[number]	k4	700
rublů	rubl	k1gInPc2	rubl
ročně	ročně	k6eAd1	ročně
u	u	k7c2	u
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
o	o	k7c4	o
400	[number]	k4	400
rublů	rubl	k1gInPc2	rubl
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
měně	měna	k1gFnSc6	měna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
200	[number]	k4	200
rublů	rubl	k1gInPc2	rubl
ročně	ročně	k6eAd1	ročně
u	u	k7c2	u
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
100	[number]	k4	100
rublů	rubl	k1gInPc2	rubl
ročně	ročně	k6eAd1	ročně
u	u	k7c2	u
IV	IV	kA	IV
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
exkluzivnost	exkluzivnost	k1gFnSc1	exkluzivnost
řádu	řád	k1gInSc2	řád
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
udělován	udělovat	k5eAaImNgInS	udělovat
jenom	jenom	k9	jenom
za	za	k7c4	za
projevy	projev	k1gInPc4	projev
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
obdržením	obdržení	k1gNnSc7	obdržení
řádu	řád	k1gInSc2	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Jiřího	Jiří	k1gMnSc2	Jiří
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
stupně	stupeň	k1gInSc2	stupeň
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gMnSc3	jeho
držiteli	držitel	k1gMnSc3	držitel
také	také	k9	také
uděleno	udělen	k2eAgNnSc4d1	uděleno
dědičné	dědičný	k2eAgNnSc4d1	dědičné
šlechtictví	šlechtictví	k1gNnSc4	šlechtictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
řádu	řád	k1gInSc2	řád
uděloval	udělovat	k5eAaImAgInS	udělovat
také	také	k9	také
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
odznak	odznak	k1gInSc1	odznak
Kříž	Kříž	k1gMnSc1	Kříž
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnPc4d2	nižší
vojenské	vojenský	k2eAgFnPc4d1	vojenská
hodnosti	hodnost	k1gFnPc4	hodnost
(	(	kIx(	(
<g/>
poddůstojníky	poddůstojník	k1gMnPc4	poddůstojník
i	i	k8xC	i
vojáky	voják	k1gMnPc4	voják
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
udělením	udělení	k1gNnSc7	udělení
tohoto	tento	k3xDgNnSc2	tento
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
se	se	k3xPyFc4	se
pojily	pojit	k5eAaImAgFnP	pojit
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
určitá	určitý	k2eAgFnSc1d1	určitá
renta	renta	k1gFnSc1	renta
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
nošení	nošení	k1gNnSc4	nošení
<g/>
,	,	kIx,	,
způsoby	způsob	k1gInPc7	způsob
odměňování	odměňování	k1gNnSc2	odměňování
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
okolnosti	okolnost	k1gFnPc1	okolnost
byly	být	k5eAaImAgFnP	být
taktéž	taktéž	k?	taktéž
upraveny	upraven	k2eAgMnPc4d1	upraven
novým	nový	k2eAgInSc7d1	nový
statutem	statut	k1gInSc7	statut
vydaným	vydaný	k2eAgInSc7d1	vydaný
Mikulášem	mikuláš	k1gInSc7	mikuláš
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
udělován	udělován	k2eAgInSc4d1	udělován
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
třídách	třída	k1gFnPc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
velkostuha	velkostuha	k1gFnSc1	velkostuha
s	s	k7c7	s
odznakem	odznak	k1gInSc7	odznak
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
řádu	řád	k1gInSc2	řád
umístěná	umístěný	k2eAgFnSc1d1	umístěná
nalevo	nalevo	k6eAd1	nalevo
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
odznak	odznak	k1gInSc1	odznak
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
umístěná	umístěný	k2eAgFnSc1d1	umístěná
nalevo	nalevo	k6eAd1	nalevo
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
odznak	odznak	k1gInSc1	odznak
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
u	u	k7c2	u
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
odznak	odznak	k1gInSc1	odznak
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Odznak	odznak	k1gInSc1	odznak
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
zlatý	zlatý	k2eAgInSc4d1	zlatý
tlapatý	tlapatý	k2eAgInSc4d1	tlapatý
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
smaltu	smalt	k1gInSc6	smalt
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
kříže	kříž	k1gInSc2	kříž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kulatý	kulatý	k2eAgInSc1d1	kulatý
medailon	medailon	k1gInSc1	medailon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
vyobrazený	vyobrazený	k2eAgInSc1d1	vyobrazený
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
bojující	bojující	k2eAgInPc4d1	bojující
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
moskevský	moskevský	k2eAgInSc1d1	moskevský
erb	erb	k1gInSc1	erb
<g/>
)	)	kIx)	)
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
pozadí	pozadí	k1gNnSc6	pozadí
(	(	kIx(	(
<g/>
u	u	k7c2	u
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
reversu	revers	k1gInSc6	revers
je	být	k5eAaImIp3nS	být
odznaku	odznak	k1gInSc3	odznak
jsou	být	k5eAaImIp3nP	být
iniciály	iniciála	k1gFnPc1	iniciála
S.	S.	kA	S.
<g/>
G.	G.	kA	G.
v	v	k7c6	v
azbuce	azbuka	k1gFnSc6	azbuka
(	(	kIx(	(
<g/>
Svjatyj	Svjatyj	k1gInSc1	Svjatyj
Georgij	Georgij	k1gFnSc2	Georgij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
provedené	provedený	k2eAgInPc1d1	provedený
ve	v	k7c6	v
zlaté	zlatý	k2eAgFnSc6d1	zlatá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
k	k	k7c3	k
I.	I.	kA	I.
třídě	třída	k1gFnSc6	třída
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zlatá	zlatá	k1gFnSc1	zlatá
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
cípy	cíp	k1gInPc7	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
medailon	medailon	k1gInSc1	medailon
s	s	k7c7	s
iniciálami	iniciála	k1gFnPc7	iniciála
S.	S.	kA	S.
<g/>
G.	G.	kA	G.
na	na	k7c6	na
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
medailonu	medailon	k1gInSc2	medailon
se	se	k3xPyFc4	se
vině	vina	k1gFnSc6	vina
modrá	modrý	k2eAgFnSc1d1	modrá
páska	páska	k1gFnSc1	páska
s	s	k7c7	s
ruským	ruský	k2eAgInSc7d1	ruský
nápisem	nápis	k1gInSc7	nápis
З	З	k?	З
с	с	k?	с
и	и	k?	и
х	х	k?	х
(	(	kIx(	(
<g/>
Za	za	k7c4	za
zásluhu	zásluha	k1gFnSc4	zásluha
a	a	k8xC	a
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
)	)	kIx)	)
s	s	k7c7	s
korunkou	korunka	k1gFnSc7	korunka
<g/>
.	.	kIx.	.
</s>
<s>
Stuha	stuha	k1gFnSc1	stuha
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
oranžové	oranžový	k2eAgFnSc6d1	oranžová
barvě	barva	k1gFnSc6	barva
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
nižší	nízký	k2eAgMnPc4d2	nižší
důstojníky	důstojník	k1gMnPc4	důstojník
a	a	k8xC	a
vojsko	vojsko	k1gNnSc4	vojsko
byl	být	k5eAaImAgInS	být
nesmaltovaný	smaltovaný	k2eNgInSc1d1	smaltovaný
tlapatý	tlapatý	k2eAgInSc1d1	tlapatý
kříž	kříž	k1gInSc1	kříž
ve	v	k7c6	v
stříbře	stříbro	k1gNnSc6	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgInS	být
medailon	medailon	k1gInSc4	medailon
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
číslo	číslo	k1gNnSc1	číslo
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
stuhy	stuha	k1gFnSc2	stuha
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svojí	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byl	být	k5eAaImAgInS	být
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
velkostuhou	velkostuha	k1gFnSc7	velkostuha
a	a	k8xC	a
odznakem	odznak	k1gInSc7	odznak
a	a	k8xC	a
hvězdou	hvězda	k1gFnSc7	hvězda
udělen	udělen	k2eAgInSc4d1	udělen
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
ho	on	k3xPp3gInSc4	on
obdržela	obdržet	k5eAaPmAgFnS	obdržet
sama	sám	k3xTgFnSc1	sám
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
řádu	řád	k1gInSc2	řád
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nositelům	nositel	k1gMnPc3	nositel
patřil	patřit	k5eAaImAgMnS	patřit
také	také	k9	také
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
Jan	Jan	k1gMnSc1	Jan
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gInSc2	Radč
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
I.	I.	kA	I.
třídu	třída	k1gFnSc4	třída
řádu	řád	k1gInSc2	řád
udělil	udělit	k5eAaPmAgInS	udělit
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
za	za	k7c4	za
dobytí	dobytí	k1gNnSc4	dobytí
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gMnSc4	on
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
např.	např.	kA	např.
maršál	maršál	k1gMnSc1	maršál
Michail	Michail	k1gMnSc1	Michail
Kutuzov	Kutuzov	k1gInSc1	Kutuzov
<g/>
,	,	kIx,	,
generalissimus	generalissimus	k1gMnSc1	generalissimus
Alexandr	Alexandr	k1gMnSc1	Alexandr
Suvorov	Suvorov	k1gInSc1	Suvorov
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Ivan	Ivan	k1gMnSc1	Ivan
Paskevič	Paskevič	k1gMnSc1	Paskevič
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
Wellington	Wellington	k1gInSc1	Wellington
nebo	nebo	k8xC	nebo
generalissimus	generalissimus	k1gMnSc1	generalissimus
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
udělen	udělit	k5eAaPmNgInS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Nositelem	nositel	k1gMnSc7	nositel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řád	řád	k1gInSc1	řád
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
za	za	k7c4	za
projevenou	projevený	k2eAgFnSc4d1	projevená
osobní	osobní	k2eAgFnSc4d1	osobní
statečnost	statečnost	k1gFnSc4	statečnost
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
