<s>
Husice	Husice	k1gFnSc1	Husice
rezavá	rezavý	k2eAgFnSc1d1	rezavá
obývá	obývat	k5eAaImIp3nS	obývat
stepní	stepní	k2eAgFnSc1d1	stepní
oblasti	oblast	k1gFnPc1	oblast
Eurasie	Eurasie	k1gFnSc2	Eurasie
od	od	k7c2	od
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přes	přes	k7c4	přes
jižní	jižní	k2eAgFnSc4d1	jižní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Írán	Írán	k1gInSc4	Írán
až	až	k6eAd1	až
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
