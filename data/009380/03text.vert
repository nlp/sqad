<p>
<s>
Husice	Husice	k1gFnSc1	Husice
rezavá	rezavý	k2eAgFnSc1d1	rezavá
(	(	kIx(	(
<g/>
Tadorna	Tadorna	k1gFnSc1	Tadorna
ferruginea	ferruginea	k1gFnSc1	ferruginea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
vrubozobých	vrubozobí	k1gMnPc2	vrubozobí
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
husic	husice	k1gInPc2	husice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
vrubozobí	vrubozobý	k2eAgMnPc1d1	vrubozobý
ptáci	pták	k1gMnPc1	pták
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
kachnám	kachna	k1gFnPc3	kachna
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
chování	chování	k1gNnSc6	chování
i	i	k9	i
stavbě	stavba	k1gFnSc3	stavba
těla	tělo	k1gNnSc2	tělo
spojují	spojovat	k5eAaImIp3nP	spojovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hus	husa	k1gFnPc2	husa
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
stejné	stejný	k2eAgFnPc4d1	stejná
velikosti	velikost	k1gFnPc4	velikost
jako	jako	k8xC	jako
blízce	blízce	k6eAd1	blízce
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
a	a	k8xC	a
známější	známý	k2eAgFnSc1d2	známější
husice	husice	k1gFnSc1	husice
liščí	liščit	k5eAaPmIp3nS	liščit
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
58	[number]	k4	58
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
oranžovohnědě	oranžovohnědě	k6eAd1	oranžovohnědě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
světle	světle	k6eAd1	světle
béžovou	béžový	k2eAgFnSc7d1	béžová
nebo	nebo	k8xC	nebo
smetanovou	smetanový	k2eAgFnSc7d1	smetanová
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
a	a	k8xC	a
kostřec	kostřec	k1gInSc1	kostřec
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Letky	letka	k1gFnPc1	letka
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
zelenavě	zelenavě	k6eAd1	zelenavě
lesklé	lesklý	k2eAgNnSc1d1	lesklé
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
liší	lišit	k5eAaImIp3nS	lišit
úzkým	úzký	k2eAgInSc7d1	úzký
černým	černý	k2eAgInSc7d1	černý
obojkem	obojek	k1gInSc7	obojek
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Husice	Husice	k1gFnSc1	Husice
rezavá	rezavý	k2eAgFnSc1d1	rezavá
obývá	obývat	k5eAaImIp3nS	obývat
stepní	stepní	k2eAgFnSc1d1	stepní
oblasti	oblast	k1gFnPc1	oblast
Eurasie	Eurasie	k1gFnSc2	Eurasie
od	od	k7c2	od
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přes	přes	k7c4	přes
jižní	jižní	k2eAgFnSc4d1	jižní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Írán	Írán	k1gInSc4	Írán
až	až	k6eAd1	až
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnPc2	území
je	být	k5eAaImIp3nS	být
tažným	tažný	k2eAgInSc7d1	tažný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
mnohkrát	mnohkrát	k6eAd1	mnohkrát
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
vesměs	vesměs	k6eAd1	vesměs
o	o	k7c4	o
jedince	jedinec	k1gMnPc4	jedinec
uniklé	uniklý	k2eAgMnPc4d1	uniklý
ze	z	k7c2	z
zajetí	zajetí	k1gNnPc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelně	pravidelně	k6eNd1	pravidelně
se	se	k3xPyFc4	se
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
je	být	k5eAaImIp3nS	být
chována	chován	k2eAgFnSc1d1	chována
například	například	k6eAd1	například
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
a	a	k8xC	a
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
husice	husice	k1gFnSc1	husice
liščí	liščí	k2eAgFnSc1d1	liščí
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zahnízdí	zahnízdit	k5eAaPmIp3nS	zahnízdit
i	i	k9	i
v	v	k7c6	v
budkách	budka	k1gFnPc6	budka
<g/>
,	,	kIx,	,
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
rozsedlinách	rozsedlina	k1gFnPc6	rozsedlina
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgMnSc1d1	hnízdní
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
snůška	snůška	k1gFnSc1	snůška
činí	činit	k5eAaImIp3nS	činit
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
krémově	krémově	k6eAd1	krémově
zbarvených	zbarvený	k2eAgNnPc2d1	zbarvené
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
po	po	k7c6	po
28	[number]	k4	28
až	až	k9	až
30	[number]	k4	30
dnech	den	k1gInPc6	den
inkubace	inkubace	k1gFnSc2	inkubace
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
líhnou	líhnout	k5eAaImIp3nP	líhnout
hnědobíle	hnědobíle	k6eAd1	hnědobíle
strakatá	strakatý	k2eAgNnPc4d1	strakaté
housata	house	k1gNnPc4	house
<g/>
,	,	kIx,	,
vodí	vodit	k5eAaImIp3nS	vodit
je	on	k3xPp3gMnPc4	on
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
posvátné	posvátný	k2eAgMnPc4d1	posvátný
ptáky	pták	k1gMnPc4	pták
hinduistů	hinduista	k1gMnPc2	hinduista
a	a	k8xC	a
buddhistů	buddhista	k1gMnPc2	buddhista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
husice	husice	k1gFnSc1	husice
rezavá	rezavý	k2eAgFnSc1d1	rezavá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Tadorna	Tadorno	k1gNnSc2	Tadorno
ferruginea	ferrugine	k1gInSc2	ferrugine
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
