<s>
Šachy	šach	k1gInPc1	šach
nebo	nebo	k8xC	nebo
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
z	z	k7c2	z
perského	perský	k2eAgMnSc2d1	perský
šáh	šáh	k1gMnSc1	šáh
<g/>
,	,	kIx,	,
panovník	panovník	k1gMnSc1	panovník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
desková	deskový	k2eAgFnSc1d1	desková
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
dva	dva	k4xCgMnPc4	dva
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
soutěžní	soutěžní	k2eAgFnSc6d1	soutěžní
podobě	podoba	k1gFnSc6	podoba
zároveň	zároveň	k6eAd1	zároveň
považovaná	považovaný	k2eAgFnSc1d1	považovaná
i	i	k9	i
za	za	k7c4	za
odvětví	odvětví	k1gNnSc4	odvětví
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
