<s>
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Brünn	Brünn	k1gNnSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Bruna	Bruna	k1gMnSc1
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Berén	Berén	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
jidiš	jidiš	k1gNnSc6
ב	ב	k?
Brin	Brin	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
i	i	k8xC
rozlohou	rozloha	k1gFnSc7
druhé	druhý	k4xOgNnSc4
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
bývalé	bývalý	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>