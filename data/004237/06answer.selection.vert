<s>
Město	město	k1gNnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
soutoku	soutok	k1gInSc6
řek	řeka	k1gFnPc2
Svratky	Svratka	k1gFnSc2
a	a	k8xC
Svitavy	Svitava	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
381	[number]	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
metropolitní	metropolitní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
žije	žít	k5eAaImIp3nS
asi	asi	k9
600	[number]	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>