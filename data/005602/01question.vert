<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
heterogenní	heterogenní	k2eAgFnSc1d1	heterogenní
směs	směs	k1gFnSc1	směs
dvou	dva	k4xCgFnPc2	dva
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
samovolně	samovolně	k6eAd1	samovolně
nesměšují	směšovat	k5eNaImIp3nP	směšovat
<g/>
?	?	kIx.	?
</s>
