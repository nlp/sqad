<s>
Emulze	emulze	k1gFnSc1	emulze
je	být	k5eAaImIp3nS	být
heterogenní	heterogenní	k2eAgFnSc1d1	heterogenní
směs	směs	k1gFnSc1	směs
dvou	dva	k4xCgFnPc2	dva
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
samovolně	samovolně	k6eAd1	samovolně
nesměšují	směšovat	k5eNaImIp3nP	směšovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kapaliny	kapalina	k1gFnPc4	kapalina
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
hustotou	hustota	k1gFnSc7	hustota
a	a	k8xC	a
polaritou	polarita	k1gFnSc7	polarita
<g/>
.	.	kIx.	.
</s>
<s>
Emulze	emulze	k1gFnSc1	emulze
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
disperzním	disperzní	k2eAgNnSc7d1	disperzní
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
dispergovanou	dispergovaný	k2eAgFnSc7d1	dispergovaná
(	(	kIx(	(
<g/>
rozptýlenou	rozptýlený	k2eAgFnSc7d1	rozptýlená
<g/>
)	)	kIx)	)
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dispergovaná	dispergovaný	k2eAgFnSc1d1	dispergovaná
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
disperzním	disperzní	k2eAgNnSc6d1	disperzní
prostředí	prostředí	k1gNnSc6	prostředí
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
malých	malý	k2eAgFnPc2d1	malá
kapiček	kapička	k1gFnPc2	kapička
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
kapiček	kapička	k1gFnPc2	kapička
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
stupni	stupeň	k1gInSc6	stupeň
homogenizace	homogenizace	k1gFnSc2	homogenizace
a	a	k8xC	a
na	na	k7c6	na
povaze	povaha	k1gFnSc6	povaha
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Emulze	emulze	k1gFnPc1	emulze
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
emulzí	emulze	k1gFnPc2	emulze
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
emulgace	emulgace	k1gFnSc1	emulgace
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
emulzifikace	emulzifikace	k1gFnSc1	emulzifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smíšení	smíšení	k1gNnSc6	smíšení
dvou	dva	k4xCgFnPc2	dva
kapalin	kapalina	k1gFnPc2	kapalina
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
např.	např.	kA	např.
olej	olej	k1gInSc1	olej
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
emulze	emulze	k1gFnPc4	emulze
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc4	rozdělení
obou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
tak	tak	k9	tak
olej	olej	k1gInSc1	olej
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
fázové	fázový	k2eAgNnSc1d1	fázové
rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
kapalinami	kapalina	k1gFnPc7	kapalina
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
emulgaci	emulgace	k1gFnSc6	emulgace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
silovému	silový	k2eAgNnSc3d1	silové
působení	působení	k1gNnSc3	působení
na	na	k7c4	na
fázové	fázový	k2eAgNnSc4d1	fázové
rozhraní	rozhraní	k1gNnSc4	rozhraní
a	a	k8xC	a
vytvářením	vytváření	k1gNnSc7	vytváření
malých	malý	k2eAgFnPc2d1	malá
kapiček	kapička	k1gFnPc2	kapička
oleje	olej	k1gInSc2	olej
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
povrch	povrch	k1gInSc1	povrch
olejové	olejový	k2eAgFnSc2d1	olejová
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
olej	olej	k1gInSc1	olej
nepolární	polární	k2eNgInSc1d1	nepolární
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
polární	polární	k2eAgFnSc1d1	polární
(	(	kIx(	(
<g/>
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
hydrofobní	hydrofobní	k2eAgInSc1d1	hydrofobní
=	=	kIx~	=
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k6eAd1	rád
vodu	voda	k1gFnSc4	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
kuličky	kulička	k1gFnPc4	kulička
oleje	olej	k1gInSc2	olej
velmi	velmi	k6eAd1	velmi
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
shlukovat	shlukovat	k5eAaImF	shlukovat
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
opět	opět	k6eAd1	opět
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
původní	původní	k2eAgFnSc2d1	původní
plovoucí	plovoucí	k2eAgFnSc2d1	plovoucí
vrstvy	vrstva	k1gFnSc2	vrstva
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
emulzi	emulze	k1gFnSc4	emulze
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
působit	působit	k5eAaImF	působit
určitou	určitý	k2eAgFnSc7d1	určitá
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
rozptýlení	rozptýlení	k1gNnSc4	rozptýlení
jedné	jeden	k4xCgFnSc2	jeden
kapaliny	kapalina	k1gFnSc2	kapalina
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
např.	např.	kA	např.
různě	různě	k6eAd1	různě
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
mícháním	míchání	k1gNnSc7	míchání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
stálosti	stálost	k1gFnSc2	stálost
emulze	emulze	k1gFnSc2	emulze
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nerozpadala	rozpadat	k5eNaImAgFnS	rozpadat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přidat	přidat	k5eAaPmF	přidat
další	další	k2eAgFnSc4d1	další
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
rozptýlené	rozptýlený	k2eAgFnPc4d1	rozptýlená
částice	částice	k1gFnPc4	částice
v	v	k7c6	v
emulzi	emulze	k1gFnSc6	emulze
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
látkám	látka	k1gFnPc3	látka
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
emulgátory	emulgátor	k1gInPc7	emulgátor
<g/>
.	.	kIx.	.
</s>
<s>
Emulgátor	emulgátor	k1gInSc1	emulgátor
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
svůj	svůj	k3xOyFgInSc4	svůj
konec	konec	k1gInSc4	konec
hydrofilní	hydrofilní	k2eAgInSc1d1	hydrofilní
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
hydrofobní	hydrofobní	k2eAgMnSc1d1	hydrofobní
<g/>
.	.	kIx.	.
</s>
<s>
Emulgátor	emulgátor	k1gInSc1	emulgátor
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
shlukování	shlukování	k1gNnSc3	shlukování
tukových	tukový	k2eAgFnPc2d1	tuková
kuliček	kulička	k1gFnPc2	kulička
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
fázové	fázový	k2eAgNnSc4d1	fázové
rozhraní	rozhraní	k1gNnSc4	rozhraní
olej-voda	olejoda	k1gFnSc1	olej-voda
(	(	kIx(	(
<g/>
hydrofilní	hydrofilní	k2eAgFnSc7d1	hydrofilní
částí	část	k1gFnSc7	část
molekuly	molekula	k1gFnSc2	molekula
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
hydrofobní	hydrofobní	k2eAgFnSc7d1	hydrofobní
částí	část	k1gFnSc7	část
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
oleji	olej	k1gInSc3	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
emulgátor	emulgátor	k1gInSc1	emulgátor
zamezí	zamezit	k5eAaPmIp3nS	zamezit
styku	styk	k1gInSc3	styk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
olejových	olejový	k2eAgFnPc2d1	olejová
kapiček	kapička	k1gFnPc2	kapička
a	a	k8xC	a
emulze	emulze	k1gFnSc2	emulze
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
více	hodně	k6eAd2	hodně
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
než	než	k8xS	než
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
emulgátoru	emulgátor	k1gInSc2	emulgátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
látky	látka	k1gFnPc4	látka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
stabilizátory	stabilizátor	k1gInPc4	stabilizátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratorní	laboratorní	k2eAgFnSc6d1	laboratorní
praxi	praxe	k1gFnSc6	praxe
oddělování	oddělování	k1gNnSc2	oddělování
nejčastěji	často	k6eAd3	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
dělící	dělící	k2eAgFnSc6d1	dělící
nálevce	nálevka	k1gFnSc6	nálevka
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělit	oddělit	k5eAaPmF	oddělit
otevřením	otevření	k1gNnSc7	otevření
uzávěru	uzávěr	k1gInSc2	uzávěr
dělící	dělící	k2eAgFnSc2d1	dělící
nálevky	nálevka	k1gFnSc2	nálevka
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
kapalina	kapalina	k1gFnSc1	kapalina
odteče	odtéct	k5eAaPmIp3nS	odtéct
<g/>
,	,	kIx,	,
závěr	závěr	k1gInSc1	závěr
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
oddělování	oddělování	k1gNnSc2	oddělování
však	však	k9	však
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
využití	využití	k1gNnSc4	využití
ideální	ideální	k2eAgFnSc1d1	ideální
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
ekonomicky	ekonomicky	k6eAd1	ekonomicky
výhodný	výhodný	k2eAgInSc1d1	výhodný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
účinnější	účinný	k2eAgNnSc1d2	účinnější
rozdělení	rozdělení	k1gNnSc1	rozdělení
využitím	využití	k1gNnSc7	využití
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
odstředivkách	odstředivka	k1gFnPc6	odstředivka
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělování	rozdělování	k1gNnSc1	rozdělování
emulze	emulze	k1gFnSc2	emulze
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
např.	např.	kA	např.
při	při	k7c6	při
separaci	separace	k1gFnSc6	separace
tuku	tuk	k1gInSc2	tuk
ze	z	k7c2	z
syrového	syrový	k2eAgNnSc2d1	syrové
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Emulze	emulze	k1gFnSc1	emulze
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
polarity	polarita	k1gFnSc2	polarita
disperzního	disperzní	k2eAgNnSc2d1	disperzní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
dispergované	dispergovaný	k2eAgFnSc2d1	dispergovaná
látky	látka	k1gFnSc2	látka
<g/>
:	:	kIx,	:
emulze	emulze	k1gFnSc1	emulze
olej	olej	k1gInSc1	olej
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
emulze	emulze	k1gFnSc1	emulze
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
(	(	kIx(	(
<g/>
v	v	k7c4	v
<g/>
/	/	kIx~	/
<g/>
o	o	k7c6	o
<g/>
)	)	kIx)	)
Emulze	emulze	k1gFnPc4	emulze
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
emulze	emulze	k1gFnPc4	emulze
prvního	první	k4xOgInSc2	první
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
polární	polární	k2eAgFnSc1d1	polární
kapalina	kapalina	k1gFnSc1	kapalina
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
polárnější	polární	k2eAgFnSc6d2	polárnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
emulze	emulze	k1gFnPc4	emulze
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
řadit	řadit	k5eAaImF	řadit
např.	např.	kA	např.
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
<g/>
smetana	smetana	k1gFnSc1	smetana
Emulze	emulze	k1gFnSc1	emulze
(	(	kIx(	(
<g/>
v	v	k7c4	v
<g/>
/	/	kIx~	/
<g/>
o	o	k7c6	o
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
emulze	emulze	k1gFnPc4	emulze
druhého	druhý	k4xOgInSc2	druhý
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
též	též	k9	též
také	také	k9	také
obrácená	obrácený	k2eAgFnSc1d1	obrácená
emulze	emulze	k1gFnSc1	emulze
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
např.	např.	kA	např.
máslo	máslo	k1gNnSc4	máslo
<g/>
,	,	kIx,	,
<g/>
margarín	margarín	k1gInSc4	margarín
</s>
