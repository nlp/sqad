<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Goodman	Goodman	k1gMnSc1	Goodman
Gilman	Gilman	k1gMnSc1	Gilman
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
farmakolog	farmakolog	k1gMnSc1	farmakolog
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Rodbellem	Rodbell	k1gMnSc7	Rodbell
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
za	za	k7c4	za
objev	objev	k1gInSc4	objev
G	G	kA	G
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
úlohy	úloha	k1gFnSc2	úloha
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
signalizaci	signalizace	k1gFnSc6	signalizace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
jiný	jiný	k2eAgMnSc1d1	jiný
známý	známý	k2eAgMnSc1d1	známý
fakmakolog	fakmakolog	k1gMnSc1	fakmakolog
Alfred	Alfred	k1gMnSc1	Alfred
Zack	Zack	k1gMnSc1	Zack
Gilman	Gilman	k1gMnSc1	Gilman
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
učitelskou	učitelský	k2eAgFnSc4d1	učitelská
kariéru	kariéra	k1gFnSc4	kariéra
Alfred	Alfred	k1gMnSc1	Alfred
G.	G.	kA	G.
Gilman	Gilman	k1gMnSc1	Gilman
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
lékařských	lékařský	k2eAgFnPc6d1	lékařská
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Virginia	Virginium	k1gNnSc2	Virginium
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Medicine	Medicin	k1gInSc5	Medicin
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
Virginské	virginský	k2eAgFnSc2d1	virginská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
Southwestern	Southwestern	k1gMnSc1	Southwestern
Medical	Medical	k1gMnSc1	Medical
Center	centrum	k1gNnPc2	centrum
at	at	k?	at
Dallas	Dallas	k1gMnSc1	Dallas
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
systému	systém	k1gInSc2	systém
texaských	texaský	k2eAgFnPc2d1	texaská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alfred	Alfred	k1gMnSc1	Alfred
G.	G.	kA	G.
Gilman	Gilman	k1gMnSc1	Gilman
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
</s>
</p>
