<s>
Alfred	Alfred	k1gMnSc1	Alfred
Goodman	Goodman	k1gMnSc1	Goodman
Gilman	Gilman	k1gMnSc1	Gilman
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
farmakolog	farmakolog	k1gMnSc1	farmakolog
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Rodbellem	Rodbell	k1gMnSc7	Rodbell
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
