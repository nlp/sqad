<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Petrovič	Petrovič	k1gMnSc1	Petrovič
Kesarev	Kesarev	k1gFnSc4	Kesarev
rusky	rusky	k6eAd1	rusky
В	В	k?	В
П	П	k?	П
К	К	k?	К
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1930	[number]	k4	1930
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
fotbalista	fotbalista	k1gMnSc1	fotbalista
ruské	ruský	k2eAgFnSc2d1	ruská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
především	především	k9	především
na	na	k7c6	na
postu	post	k1gInSc6	post
obránce	obránce	k1gMnSc2	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
reprezentací	reprezentace	k1gFnSc7	reprezentace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgNnSc4	první
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
Pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
nezasáhl	zasáhnout	k5eNaPmAgInS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
světového	světový	k2eAgInSc2d1	světový
šampionátu	šampionát	k1gInSc2	šampionát
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
týmu	tým	k1gInSc6	tým
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
ke	k	k7c3	k
14	[number]	k4	14
zápasům	zápas	k1gInPc3	zápas
<g/>
.	.	kIx.	.
<g/>
Takřka	takřka	k6eAd1	takřka
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
FK	FK	kA	FK
Dynamo	dynamo	k1gNnSc1	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
