<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
pštrosa	pštros	k1gMnSc2	pštros
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
