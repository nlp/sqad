<p>
<s>
Tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
devadesát	devadesát	k4xCc1	devadesát
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
čísle	číslo	k1gNnSc6	číslo
tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
osmdesát	osmdesát	k4xCc1	osmdesát
devět	devět	k4xCc1	devět
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
číslu	číslo	k1gNnSc3	číslo
tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
devadesát	devadesát	k4xCc1	devadesát
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
CCCXC	CCCXC	kA	CCCXC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematika	matematika	k1gFnSc1	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
abundantní	abundantní	k2eAgNnSc1d1	abundantní
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
číslosoučet	číslosoučet	k5eAaPmF	číslosoučet
čtyř	čtyři	k4xCgMnPc2	čtyři
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgNnPc2d1	jdoucí
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
(	(	kIx(	(
<g/>
89	[number]	k4	89
+	+	kIx~	+
97	[number]	k4	97
+	+	kIx~	+
101	[number]	k4	101
+	+	kIx~	+
103	[number]	k4	103
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
390	[number]	k4	390
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
–	–	k?	–
Budišov	Budišov	k1gInSc1	Budišov
–	–	k?	–
–	–	k?	–
Osová	osový	k2eAgFnSc1d1	Osová
Bítýška	Bítýška	k1gFnSc1	Bítýška
–	–	k?	–
přerušení	přerušení	k1gNnSc2	přerušení
–	–	k?	–
Drahonín	Drahonín	k1gMnSc1	Drahonín
–	–	k?	–
–	–	k?	–
Nedvědice	Nedvědice	k1gFnSc2	Nedvědice
</s>
</p>
<p>
<s>
==	==	k?	==
Astronomie	astronomie	k1gFnSc1	astronomie
==	==	k?	==
</s>
</p>
<p>
<s>
390	[number]	k4	390
Alma	alma	k1gFnSc1	alma
je	být	k5eAaImIp3nS	být
planetka	planetka	k1gFnSc1	planetka
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Roky	rok	k1gInPc1	rok
==	==	k?	==
</s>
</p>
<p>
<s>
390	[number]	k4	390
</s>
</p>
<p>
<s>
390	[number]	k4	390
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
