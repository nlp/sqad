<s>
Atlas	Atlas	k1gInSc1	Atlas
mraků	mrak	k1gInPc2	mrak
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Cloud	Cloud	k1gInSc1	Cloud
Atlas	Atlas	k1gInSc1	Atlas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postmoderní	postmoderní	k2eAgFnSc1d1	postmoderní
sci-fi	scii	k1gFnSc1	sci-fi
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Režisérská	režisérský	k2eAgFnSc1d1	režisérská
trojice	trojice	k1gFnSc1	trojice
Tom	Tom	k1gMnSc1	Tom
Tykwer	Tykwer	k1gMnSc1	Tykwer
<g/>
,	,	kIx,	,
Lana	lano	k1gNnSc2	lano
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
jej	on	k3xPp3gMnSc4	on
natočila	natočit	k5eAaBmAgFnS	natočit
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Davida	David	k1gMnSc2	David
Mitchella	Mitchell	k1gMnSc2	Mitchell
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
svou	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2012	[number]	k4	2012
na	na	k7c4	na
37	[number]	k4	37
<g/>
.	.	kIx.	.
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
kinodistribuce	kinodistribuce	k1gFnSc2	kinodistribuce
šel	jít	k5eAaImAgInS	jít
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
kinech	kino	k1gNnPc6	kino
měl	mít	k5eAaImAgMnS	mít
premiéru	premiér	k1gMnSc3	premiér
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
filmu	film	k1gInSc2	film
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
románové	románový	k2eAgFnSc6d1	románová
předloze	předloha	k1gFnSc6	předloha
<g/>
,	,	kIx,	,
v	v	k7c6	v
šesti	šest	k4xCc6	šest
časových	časový	k2eAgFnPc6d1	časová
rovinách	rovina	k1gFnPc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
provázané	provázaný	k2eAgInPc1d1	provázaný
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
rozlišené	rozlišený	k2eAgInPc1d1	rozlišený
pomocí	pomocí	k7c2	pomocí
odlišných	odlišný	k2eAgInPc2d1	odlišný
žánrových	žánrový	k2eAgInPc2d1	žánrový
vzorců	vzorec	k1gInPc2	vzorec
<g/>
:	:	kIx,	:
historický	historický	k2eAgInSc1d1	historický
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
životopisné	životopisný	k2eAgNnSc1d1	životopisné
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
paranoidní	paranoidní	k2eAgInSc1d1	paranoidní
thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
fraška	fraška	k1gFnSc1	fraška
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
a	a	k8xC	a
postapokalyptický	postapokalyptický	k2eAgInSc1d1	postapokalyptický
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
románu	román	k1gInSc3	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
přerušené	přerušený	k2eAgMnPc4d1	přerušený
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
dovyprávěny	dovyprávěn	k2eAgFnPc1d1	dovyprávěn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
roviny	rovina	k1gFnSc2	rovina
divoce	divoce	k6eAd1	divoce
střídají	střídat	k5eAaImIp3nP	střídat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
procházejí	procházet	k5eAaImIp3nP	procházet
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
pomocí	pomocí	k7c2	pomocí
kompozičních	kompoziční	k2eAgInPc2d1	kompoziční
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zvukových	zvukový	k2eAgInPc2d1	zvukový
můstků	můstek	k1gInPc2	můstek
<g/>
,	,	kIx,	,
paralelismů	paralelismus	k1gInPc2	paralelismus
či	či	k8xC	či
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgFnPc2	všecek
šest	šest	k4xCc1	šest
rovin	rovina	k1gFnPc2	rovina
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
příběhu	příběh	k1gInSc2	příběh
vždy	vždy	k6eAd1	vždy
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
pěti	pět	k4xCc6	pět
minutách	minuta	k1gFnPc6	minuta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
představeny	představit	k5eAaPmNgInP	představit
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
klíčovém	klíčový	k2eAgInSc6d1	klíčový
momentu	moment	k1gInSc6	moment
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
půlhodině	půlhodina	k1gFnSc6	půlhodina
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
výchozí	výchozí	k2eAgFnPc1d1	výchozí
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
vyprávění	vyprávění	k1gNnSc1	vyprávění
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
příběhy	příběh	k1gInPc1	příběh
<g/>
:	:	kIx,	:
Tichomořský	tichomořský	k2eAgInSc1d1	tichomořský
deník	deník	k1gInSc1	deník
Adama	Adam	k1gMnSc2	Adam
Ewinga	Ewing	k1gMnSc2	Ewing
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
–	–	k?	–
Americký	americký	k2eAgMnSc1d1	americký
právník	právník	k1gMnSc1	právník
Adam	Adam	k1gMnSc1	Adam
Ewing	Ewing	k1gMnSc1	Ewing
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
ze	z	k7c2	z
San	San	k1gFnSc2	San
Francisca	Franciscum	k1gNnSc2	Franciscum
na	na	k7c4	na
Chathamské	Chathamský	k2eAgInPc4d1	Chathamský
ostrovy	ostrov	k1gInPc4	ostrov
uzavřít	uzavřít	k5eAaPmF	uzavřít
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
tchána	tchán	k1gMnSc4	tchán
Haskella	Haskell	k1gMnSc4	Haskell
Moora	Moor	k1gMnSc4	Moor
obchodní	obchodní	k2eAgFnSc4d1	obchodní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
reverendem	reverend	k1gMnSc7	reverend
Gillesem	Gilles	k1gMnSc7	Gilles
Horroxem	Horrox	k1gInSc7	Horrox
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
svědkem	svědek	k1gMnSc7	svědek
bičování	bičování	k1gNnSc2	bičování
moriorského	moriorský	k2eAgMnSc2d1	moriorský
otroka	otrok	k1gMnSc2	otrok
Autuy	Autua	k1gMnSc2	Autua
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poté	poté	k6eAd1	poté
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
zastal	zastat	k5eAaPmAgInS	zastat
u	u	k7c2	u
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Henry	Henry	k1gMnSc1	Henry
Goose	Goos	k1gMnSc2	Goos
mezitím	mezitím	k6eAd1	mezitím
Ewinga	Ewing	k1gMnSc4	Ewing
pomalu	pomalu	k6eAd1	pomalu
tráví	trávit	k5eAaImIp3nS	trávit
jedem	jed	k1gInSc7	jed
s	s	k7c7	s
výmluvou	výmluva	k1gFnSc7	výmluva
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lék	lék	k1gInSc4	lék
na	na	k7c4	na
parazitického	parazitický	k2eAgMnSc4d1	parazitický
červa	červ	k1gMnSc4	červ
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
zmocnit	zmocnit	k5eAaPmF	zmocnit
Ewingova	Ewingův	k2eAgNnSc2d1	Ewingův
jmění	jmění	k1gNnSc2	jmění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podání	podání	k1gNnSc6	podání
poslední	poslední	k2eAgFnSc2d1	poslední
smrtící	smrtící	k2eAgFnSc2d1	smrtící
dávky	dávka	k1gFnSc2	dávka
však	však	k9	však
Autua	Autu	k2eAgFnSc1d1	Autu
Ewinga	Ewinga	k1gFnSc1	Ewinga
zachrání	zachránit	k5eAaPmIp3nS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
Ewing	Ewing	k1gInSc1	Ewing
znechucen	znechutit	k5eAaPmNgInS	znechutit
tchánovým	tchánův	k2eAgInSc7d1	tchánův
podílem	podíl	k1gInSc7	podíl
na	na	k7c6	na
otrokářství	otrokářství	k1gNnSc6	otrokářství
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Tildou	Tilda	k1gFnSc7	Tilda
opustí	opustit	k5eAaPmIp3nP	opustit
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
za	za	k7c4	za
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
ze	z	k7c2	z
Zedelghemu	Zedelghem	k1gInSc2	Zedelghem
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
–	–	k?	–
Anglický	anglický	k2eAgMnSc1d1	anglický
bisexuální	bisexuální	k2eAgMnSc1d1	bisexuální
hudebník	hudebník	k1gMnSc1	hudebník
Robert	Robert	k1gMnSc1	Robert
Frobisher	Frobishra	k1gFnPc2	Frobishra
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
jako	jako	k9	jako
zapisovač	zapisovač	k1gInSc1	zapisovač
u	u	k7c2	u
skladatele	skladatel	k1gMnSc2	skladatel
Vyvyana	Vyvyana	k1gFnSc1	Vyvyana
Ayerse	Ayerse	k1gFnSc1	Ayerse
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
čas	čas	k1gInSc4	čas
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
skládání	skládání	k1gNnSc4	skládání
vlastního	vlastní	k2eAgNnSc2d1	vlastní
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
sextetu	sextet	k1gInSc2	sextet
Atlas	Atlas	k1gInSc1	Atlas
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
si	se	k3xPyFc3	se
přitom	přitom	k6eAd1	přitom
přečte	přečíst	k5eAaPmIp3nS	přečíst
část	část	k1gFnSc4	část
vydání	vydání	k1gNnSc2	vydání
Ewingova	Ewingův	k2eAgInSc2d1	Ewingův
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Ayers	Ayers	k6eAd1	Ayers
se	se	k3xPyFc4	se
ale	ale	k9	ale
chce	chtít	k5eAaImIp3nS	chtít
podepsat	podepsat	k5eAaPmF	podepsat
pod	pod	k7c4	pod
Frobisherovu	Frobisherův	k2eAgFnSc4d1	Frobisherův
symfonii	symfonie	k1gFnSc4	symfonie
a	a	k8xC	a
vydírá	vydírat	k5eAaImIp3nS	vydírat
ho	on	k3xPp3gInSc4	on
odhalením	odhalení	k1gNnSc7	odhalení
a	a	k8xC	a
skandalizací	skandalizace	k1gFnSc7	skandalizace
jeho	jeho	k3xOp3gInPc2	jeho
intimních	intimní	k2eAgInPc2d1	intimní
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Frobisher	Frobishra	k1gFnPc2	Frobishra
Ayerse	Ayerse	k1gFnSc1	Ayerse
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
a	a	k8xC	a
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokončí	dokončit	k5eAaPmIp3nP	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
sextet	sextet	k1gInSc4	sextet
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
svého	svůj	k3xOyFgMnSc2	svůj
milence	milenec	k1gMnSc2	milenec
Rufuse	Rufuse	k1gFnSc2	Rufuse
Sixsmithe	Sixsmith	k1gMnSc2	Sixsmith
však	však	k9	však
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Poločasy	poločas	k1gInPc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
záhada	záhada	k1gFnSc1	záhada
Luisy	Luisa	k1gFnSc2	Luisa
Reyové	Reyová	k1gFnSc2	Reyová
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
–	–	k?	–
Novinářka	novinářka	k1gFnSc1	novinářka
Luisa	Luisa	k1gFnSc1	Luisa
Reyová	Reyová	k1gFnSc1	Reyová
potkává	potkávat	k5eAaImIp3nS	potkávat
už	už	k9	už
staršího	starý	k2eAgMnSc4d2	starší
Sixmithe	Sixmith	k1gMnSc4	Sixmith
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
jaderným	jaderný	k2eAgMnSc7d1	jaderný
fyzikem	fyzik	k1gMnSc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
přivede	přivést	k5eAaPmIp3nS	přivést
na	na	k7c4	na
stopu	stopa	k1gFnSc4	stopa
konspirací	konspirace	k1gFnPc2	konspirace
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
nového	nový	k2eAgInSc2d1	nový
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
provozovaného	provozovaný	k2eAgInSc2d1	provozovaný
Lloydem	Lloyd	k1gInSc7	Lloyd
Hooksem	Hooks	k1gMnSc7	Hooks
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Hooksovým	Hooksův	k2eAgMnSc7d1	Hooksův
zabijákem	zabiják	k1gMnSc7	zabiják
Billem	Bill	k1gMnSc7	Bill
Smokem	smok	k1gMnSc7	smok
ještě	ještě	k6eAd1	ještě
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
stihne	stihnout	k5eAaPmIp3nS	stihnout
předat	předat	k5eAaPmF	předat
důkazní	důkazní	k2eAgFnSc4d1	důkazní
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Reyová	Reyová	k1gFnSc1	Reyová
najde	najít	k5eAaPmIp3nS	najít
a	a	k8xC	a
přečte	přečíst	k5eAaPmIp3nS	přečíst
si	se	k3xPyFc3	se
Frobisherovy	Frobisherův	k2eAgInPc4d1	Frobisherův
dopisy	dopis	k1gInPc4	dopis
adresované	adresovaný	k2eAgInPc4d1	adresovaný
Sixmithovi	Sixmith	k1gMnSc3	Sixmith
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
vědec	vědec	k1gMnSc1	vědec
z	z	k7c2	z
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
Isaac	Isaac	k1gInSc4	Isaac
Sachs	Sachsa	k1gFnPc2	Sachsa
jí	on	k3xPp3gFnSc3	on
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
kopii	kopie	k1gFnSc4	kopie
Sixmithovy	Sixmithův	k2eAgFnSc2d1	Sixmithův
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Smoke	Smoke	k6eAd1	Smoke
však	však	k9	však
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
i	i	k9	i
Sachse	Sachse	k1gFnSc1	Sachse
a	a	k8xC	a
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
novinářku	novinářka	k1gFnSc4	novinářka
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
autě	auto	k1gNnSc6	auto
z	z	k7c2	z
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
šéfa	šéf	k1gMnSc2	šéf
elektrárenské	elektrárenský	k2eAgFnSc2d1	elektrárenská
ochranky	ochranka	k1gFnSc2	ochranka
Joea	Joe	k2eAgFnSc1d1	Joea
Napiera	Napiera	k1gFnSc1	Napiera
unikne	uniknout	k5eAaPmIp3nS	uniknout
Rayová	Rayový	k2eAgFnSc1d1	Rayový
i	i	k8xC	i
dalšímu	další	k2eAgInSc3d1	další
vražednému	vražedný	k2eAgInSc3d1	vražedný
pokusu	pokus	k1gInSc3	pokus
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
zemře	zemřít	k5eAaPmIp3nS	zemřít
sám	sám	k3xTgInSc1	sám
Smoke	Smoke	k1gInSc1	Smoke
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
odhalí	odhalit	k5eAaPmIp3nS	odhalit
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
jadernou	jaderný	k2eAgFnSc7d1	jaderná
havárií	havárie	k1gFnSc7	havárie
podpořit	podpořit	k5eAaPmF	podpořit
zisky	zisk	k1gInPc4	zisk
ropných	ropný	k2eAgFnPc2d1	ropná
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrůzostrašná	hrůzostrašný	k2eAgFnSc1d1	hrůzostrašná
muka	muka	k1gFnSc1	muka
Timothyho	Timothy	k1gMnSc2	Timothy
Cavendishe	Cavendish	k1gMnSc2	Cavendish
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
Pětašedesátiletý	pětašedesátiletý	k2eAgMnSc1d1	pětašedesátiletý
vydavatel	vydavatel	k1gMnSc1	vydavatel
Timothy	Timotha	k1gFnSc2	Timotha
Cavendish	Cavendish	k1gMnSc1	Cavendish
náhle	náhle	k6eAd1	náhle
zbohatne	zbohatnout	k5eAaPmIp3nS	zbohatnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
jím	jíst	k5eAaImIp1nS	jíst
publikovaný	publikovaný	k2eAgMnSc1d1	publikovaný
gangsterský	gangsterský	k2eAgMnSc1d1	gangsterský
autor	autor	k1gMnSc1	autor
Dermont	Dermont	k1gMnSc1	Dermont
Hoggins	Hoggins	k1gInSc4	Hoggins
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Hogginsovi	Hogginsův	k2eAgMnPc1d1	Hogginsův
kumpáni	kumpáni	k?	kumpáni
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
začnou	začít	k5eAaPmIp3nP	začít
vyhrožovat	vyhrožovat	k5eAaImF	vyhrožovat
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
a	a	k8xC	a
Cavendish	Cavendish	k1gInSc1	Cavendish
se	se	k3xPyFc4	se
obrátí	obrátit	k5eAaPmIp3nS	obrátit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Denholma	Denholm	k1gMnSc4	Denholm
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
léčkou	léčka	k1gFnSc7	léčka
zavře	zavřít	k5eAaPmIp3nS	zavřít
do	do	k7c2	do
pečovatelského	pečovatelský	k2eAgInSc2d1	pečovatelský
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
drží	držet	k5eAaImIp3nP	držet
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Obdrží	obdržet	k5eAaPmIp3nS	obdržet
rukopis	rukopis	k1gInSc4	rukopis
románu	román	k1gInSc2	román
o	o	k7c6	o
životě	život	k1gInSc6	život
novinářky	novinářka	k1gFnSc2	novinářka
Reyové	Reyová	k1gFnSc2	Reyová
a	a	k8xC	a
napíše	napsat	k5eAaPmIp3nS	napsat
vlastní	vlastní	k2eAgInSc4d1	vlastní
scénář	scénář	k1gInSc4	scénář
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
příhodách	příhoda	k1gFnPc6	příhoda
v	v	k7c6	v
pečovatelském	pečovatelský	k2eAgInSc6d1	pečovatelský
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Orison	Orison	k1gMnSc1	Orison
Sonmi	Son	k1gFnPc7	Son
<g/>
~	~	kIx~	~
<g/>
451	[number]	k4	451
(	(	kIx(	(
<g/>
2144	[number]	k4	2144
<g/>
)	)	kIx)	)
–	–	k?	–
Sonmi	Son	k1gFnPc7	Son
<g/>
~	~	kIx~	~
<g/>
451	[number]	k4	451
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc4d1	lidský
klon	klon	k1gInSc4	klon
<g/>
,	,	kIx,	,
umělý	umělý	k2eAgInSc4d1	umělý
produkt	produkt	k1gInSc4	produkt
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
servírka	servírka	k1gFnSc1	servírka
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
sedí	sedit	k5eAaImIp3nS	sedit
u	u	k7c2	u
výslechu	výslech	k1gInSc2	výslech
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
popravou	poprava	k1gFnSc7	poprava
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
vyhovujícího	vyhovující	k2eAgInSc2d1	vyhovující
poddanského	poddanský	k2eAgInSc2d1	poddanský
života	život	k1gInSc2	život
vytržena	vytržen	k2eAgFnSc1d1	vytržena
velitelem	velitel	k1gMnSc7	velitel
Hae-Joo	Hae-Joo	k1gNnSc4	Hae-Joo
Changem	Chang	k1gInSc7	Chang
<g/>
,	,	kIx,	,
příslušníkem	příslušník	k1gMnSc7	příslušník
povstaleckého	povstalecký	k2eAgNnSc2d1	povstalecké
hnutí	hnutí	k1gNnSc2	hnutí
známého	známý	k2eAgNnSc2d1	známé
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Unie	unie	k1gFnSc1	unie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
skrývala	skrývat	k5eAaImAgFnS	skrývat
<g/>
,	,	kIx,	,
sledovala	sledovat	k5eAaImAgFnS	sledovat
film	film	k1gInSc4	film
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
Cavedishových	Cavedishův	k2eAgFnPc6d1	Cavedishův
příhodách	příhoda	k1gFnPc6	příhoda
<g/>
.	.	kIx.	.
</s>
<s>
Unijní	unijní	k2eAgMnPc1d1	unijní
povstalci	povstalec	k1gMnPc1	povstalec
jí	on	k3xPp3gFnSc3	on
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
klony	klon	k1gInPc4	klon
jako	jako	k8xS	jako
ona	onen	k3xDgNnPc4	onen
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
zabíjeny	zabíjen	k2eAgInPc1d1	zabíjen
a	a	k8xC	a
"	"	kIx"	"
<g/>
recyklovány	recyklován	k2eAgMnPc4d1	recyklován
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
potrava	potrava	k1gFnSc1	potrava
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
klony	klon	k1gInPc4	klon
<g/>
.	.	kIx.	.
</s>
<s>
Sonmi	Son	k1gFnPc7	Son
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
společenský	společenský	k2eAgInSc1d1	společenský
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
zotročování	zotročování	k1gNnSc4	zotročování
a	a	k8xC	a
vykořisťování	vykořisťování	k1gNnSc4	vykořisťování
klonů	klon	k1gInPc2	klon
nelze	lze	k6eNd1	lze
tolerovat	tolerovat	k5eAaImF	tolerovat
a	a	k8xC	a
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
na	na	k7c4	na
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
veřejně	veřejně	k6eAd1	veřejně
odvysílá	odvysílat	k5eAaPmIp3nS	odvysílat
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
a	a	k8xC	a
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
Hae-Joo	Hae-Joo	k1gMnSc1	Hae-Joo
Chang	Chang	k1gMnSc1	Chang
však	však	k9	však
zahyne	zahynout	k5eAaPmIp3nS	zahynout
při	při	k7c6	při
přestřelce	přestřelka	k1gFnSc6	přestřelka
<g/>
,	,	kIx,	,
Sonmi	Son	k1gFnPc7	Son
je	být	k5eAaImIp3nS	být
dopadena	dopaden	k2eAgFnSc1d1	dopadena
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
výslechu	výslech	k1gInSc6	výslech
popravena	popravit	k5eAaPmNgFnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Slooshova	Slooshův	k2eAgInSc2d1	Slooshův
brodu	brod	k1gInSc2	brod
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
seběhlo	seběhnout	k5eAaPmAgNnS	seběhnout
(	(	kIx(	(
<g/>
106	[number]	k4	106
zim	zima	k1gFnPc2	zima
po	po	k7c6	po
"	"	kIx"	"
<g/>
Úpadku	úpadek	k1gInSc6	úpadek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
datováno	datovat	k5eAaImNgNnS	datovat
2321	[number]	k4	2321
<g/>
)	)	kIx)	)
–	–	k?	–
Zachry	Zachra	k1gFnSc2	Zachra
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
neteří	neteř	k1gFnSc7	neteř
Catkin	Catkina	k1gFnPc2	Catkina
v	v	k7c6	v
primitivním	primitivní	k2eAgNnSc6d1	primitivní
společenství	společenství	k1gNnSc6	společenství
nazývaném	nazývaný	k2eAgNnSc6d1	nazývané
"	"	kIx"	"
<g/>
Údolí	údolí	k1gNnSc6	údolí
<g/>
"	"	kIx"	"
poté	poté	k6eAd1	poté
co	co	k9	co
většina	většina	k1gFnSc1	většina
lidstva	lidstvo	k1gNnSc2	lidstvo
vymřela	vymřít	k5eAaPmAgFnS	vymřít
při	při	k7c6	při
jaderné	jaderný	k2eAgFnSc6d1	jaderná
katastrofě	katastrofa	k1gFnSc6	katastrofa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Úpadku	úpadek	k1gInSc3	úpadek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
z	z	k7c2	z
Údolí	údolí	k1gNnSc2	údolí
uctívají	uctívat	k5eAaImIp3nP	uctívat
Sonmi	Son	k1gFnPc7	Son
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
bohyni	bohyně	k1gFnSc4	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Zachry	Zachra	k1gFnPc1	Zachra
trpí	trpět	k5eAaImIp3nP	trpět
halucinacemi	halucinace	k1gFnPc7	halucinace
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Georgie	Georgie	k1gFnSc2	Georgie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
manipulativně	manipulativně	k6eAd1	manipulativně
nabádá	nabádat	k5eAaImIp3nS	nabádat
ke	k	k7c3	k
zbabělosti	zbabělost	k1gFnSc3	zbabělost
<g/>
.	.	kIx.	.
</s>
<s>
Násilnický	násilnický	k2eAgInSc1d1	násilnický
kmen	kmen	k1gInSc1	kmen
Konů	kon	k1gInPc2	kon
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
zabije	zabít	k5eAaPmIp3nS	zabít
Zachryho	Zachry	k1gMnSc4	Zachry
přítele	přítel	k1gMnSc4	přítel
i	i	k9	i
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Zachryho	Zachryze	k6eAd1	Zachryze
vesnici	vesnice	k1gFnSc3	vesnice
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Meronym	meronymum	k1gNnPc2	meronymum
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
Jasnozřivých	jasnozřivý	k2eAgFnPc2d1	jasnozřivá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
využívající	využívající	k2eAgFnSc2d1	využívající
technologie	technologie	k1gFnSc2	technologie
pozůstalé	pozůstalý	k2eAgFnSc2d1	pozůstalá
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
"	"	kIx"	"
<g/>
Úpadkem	úpadek	k1gInSc7	úpadek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Meronym	meronymum	k1gNnPc2	meronymum
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Zachryho	Zachry	k1gMnSc4	Zachry
neteř	neteř	k1gFnSc4	neteř
Catkin	Catkin	k1gInSc1	Catkin
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
zavede	zavést	k5eAaPmIp3nS	zavést
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhledají	vyhledat	k5eAaPmIp3nP	vyhledat
Atlas	Atlas	k1gInSc4	Atlas
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vysílací	vysílací	k2eAgFnSc6d1	vysílací
stanici	stanice	k1gFnSc6	stanice
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
může	moct	k5eAaImIp3nS	moct
vyslat	vyslat	k5eAaPmF	vyslat
zprávu	zpráva	k1gFnSc4	zpráva
Zemským	zemský	k2eAgFnPc3d1	zemská
koloniím	kolonie	k1gFnPc3	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Meronym	meronymum	k1gNnPc2	meronymum
zde	zde	k6eAd1	zde
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sonmi	Son	k1gFnPc7	Son
nebyla	být	k5eNaImAgFnS	být
bohyní	bohyně	k1gFnSc7	bohyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
smrtelnicí	smrtelnice	k1gFnSc7	smrtelnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Zachry	Zachra	k1gFnSc2	Zachra
najde	najít	k5eAaPmIp3nS	najít
vesnici	vesnice	k1gFnSc4	vesnice
vyvražděnou	vyvražděný	k2eAgFnSc4d1	vyvražděná
kmenem	kmen	k1gInSc7	kmen
Konů	kon	k1gInPc2	kon
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zabít	zabít	k5eAaPmF	zabít
jejich	jejich	k3xOp3gMnPc4	jejich
vůdce	vůdce	k1gMnPc4	vůdce
a	a	k8xC	a
vysvobodit	vysvobodit	k5eAaPmF	vysvobodit
Catkin	Catkin	k1gInSc4	Catkin
<g/>
.	.	kIx.	.
</s>
<s>
Meronym	meronymum	k1gNnPc2	meronymum
je	on	k3xPp3gMnPc4	on
oba	dva	k4xCgMnPc1	dva
zachrání	zachránit	k5eAaPmIp3nP	zachránit
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
kmene	kmen	k1gInSc2	kmen
Konů	kon	k1gInPc2	kon
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
Jasnozřivých	jasnozřivý	k2eAgInPc2d1	jasnozřivý
a	a	k8xC	a
odveze	odvézt	k5eAaPmIp3nS	odvézt
z	z	k7c2	z
Velkého	velký	k2eAgInSc2d1	velký
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
epilog	epilog	k1gInSc1	epilog
filmu	film	k1gInSc2	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Zachry	Zachra	k1gFnPc4	Zachra
už	už	k9	už
coby	coby	k?	coby
stařec	stařec	k1gMnSc1	stařec
svým	svůj	k3xOyFgInSc7	svůj
vnoučatům	vnouče	k1gNnPc3	vnouče
tyto	tento	k3xDgInPc4	tento
příběhy	příběh	k1gInPc4	příběh
někde	někde	k6eAd1	někde
v	v	k7c6	v
mimozemské	mimozemský	k2eAgFnSc6d1	mimozemská
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
stvrzuje	stvrzovat	k5eAaImIp3nS	stvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Meronym	meronymum	k1gNnPc2	meronymum
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
zprávy	zpráva	k1gFnSc2	zpráva
uspěla	uspět	k5eAaPmAgFnS	uspět
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
filmu	film	k1gInSc2	film
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Weaving	Weaving	k1gInSc4	Weaving
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Sturgess	Sturgess	k1gInSc1	Sturgess
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gInSc1	Hugh
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Halle	Halle	k1gInSc1	Halle
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
Sarandon	Sarandon	k1gMnSc1	Sarandon
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Whishaw	Whishaw	k1gFnSc2	Whishaw
<g/>
.	.	kIx.	.
</s>
<s>
Tučně	tučně	k6eAd1	tučně
zvýrazněné	zvýrazněný	k2eAgFnPc1d1	zvýrazněná
jsou	být	k5eAaImIp3nP	být
ústřední	ústřední	k2eAgFnPc1d1	ústřední
postavy	postava	k1gFnPc1	postava
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
mraků	mrak	k1gInPc2	mrak
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
Johnnyho	Johnny	k1gMnSc4	Johnny
Klimeka	Klimeek	k1gMnSc4	Klimeek
<g/>
,	,	kIx,	,
Toma	Tom	k1gMnSc4	Tom
Tykwera	Tykwer	k1gMnSc4	Tykwer
a	a	k8xC	a
Reinholda	Reinhold	k1gMnSc4	Reinhold
Heila	Heil	k1gMnSc4	Heil
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trojice	trojice	k1gFnSc1	trojice
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
už	už	k6eAd1	už
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
předchozích	předchozí	k2eAgInPc2d1	předchozí
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
zejména	zejména	k9	zejména
Lola	Lolum	k1gNnPc1	Lolum
běží	běžet	k5eAaImIp3nP	běžet
o	o	k7c4	o
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
Run	run	k1gInSc1	run
Lola	Lol	k1gInSc2	Lol
Run	Runa	k1gFnPc2	Runa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parfém	parfém	k1gInSc1	parfém
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
vraha	vrah	k1gMnSc2	vrah
(	(	kIx(	(
<g/>
Perfume	Perfum	k1gInSc5	Perfum
<g/>
:	:	kIx,	:
The	The	k1gMnSc3	The
Story	story	k1gFnSc4	story
of	of	k?	of
a	a	k8xC	a
Murderer	Murderer	k1gMnSc1	Murderer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sourozenci	sourozenec	k1gMnPc7	sourozenec
Wachovskými	Wachovský	k2eAgMnPc7d1	Wachovský
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
také	také	k9	také
při	při	k7c6	při
třetím	třetí	k4xOgInSc6	třetí
dílu	díl	k1gInSc6	díl
trilogie	trilogie	k1gFnSc2	trilogie
Matrix	Matrix	k1gInSc1	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cloud	Cloud	k1gInSc4	Cloud
Atlas	Atlas	k1gInSc1	Atlas
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atlas	Atlas	k1gInSc1	Atlas
mraků	mrak	k1gInPc2	mrak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
filmu	film	k1gInSc2	film
</s>
