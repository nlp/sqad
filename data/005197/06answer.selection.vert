<s>
Režisérská	režisérský	k2eAgFnSc1d1	režisérská
trojice	trojice	k1gFnSc1	trojice
Tom	Tom	k1gMnSc1	Tom
Tykwer	Tykwer	k1gMnSc1	Tykwer
<g/>
,	,	kIx,	,
Lana	lano	k1gNnSc2	lano
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Wachowski	Wachowsk	k1gFnSc2	Wachowsk
jej	on	k3xPp3gMnSc4	on
natočila	natočit	k5eAaBmAgFnS	natočit
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Davida	David	k1gMnSc2	David
Mitchella	Mitchell	k1gMnSc2	Mitchell
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
