<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
(	(	kIx(
<g/>
ICAO	ICAO	kA
<g/>
,	,	kIx,
International	International	k1gFnSc1
Civil	civil	k1gMnSc1
Aviation	Aviation	k1gInSc1
Organization	Organization	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezivládní	mezivládní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
přidružená	přidružený	k2eAgFnSc1d1
k	k	k7c3
OSN	OSN	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
koordinovat	koordinovat	k5eAaBmF
mezinárodní	mezinárodní	k2eAgNnSc4d1
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
<g/>
.	.	kIx.
</s>