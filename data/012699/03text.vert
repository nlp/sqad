<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
International	International	k1gFnSc1	International
Civil	civil	k1gMnSc1	civil
Aviation	Aviation	k1gInSc1	Aviation
Organization	Organization	k1gInSc1	Organization
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
organizace	organizace	k1gFnSc1	organizace
přidružená	přidružený	k2eAgFnSc1d1	přidružená
k	k	k7c3	k
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
koordinovat	koordinovat	k5eAaBmF	koordinovat
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
ICAO	ICAO	kA	ICAO
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Chicagskou	chicagský	k2eAgFnSc7d1	Chicagská
úmluvou	úmluva	k1gFnSc7	úmluva
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
52	[number]	k4	52
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
také	také	k9	také
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k8xS	jako
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
147	[number]	k4	147
<g/>
/	/	kIx~	/
<g/>
1947	[number]	k4	1947
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
nabyla	nabýt	k5eAaPmAgFnS	nabýt
platnosti	platnost	k1gFnSc3	platnost
ke	k	k7c3	k
dni	den	k1gInSc3	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
polovinou	polovina	k1gFnSc7	polovina
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
uložením	uložení	k1gNnSc7	uložení
ve	v	k7c6	v
Vládním	vládní	k2eAgInSc6d1	vládní
archivu	archiv	k1gInSc6	archiv
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
ICAO	ICAO	kA	ICAO
stalo	stát	k5eAaPmAgNnS	stát
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
organizací	organizace	k1gFnSc7	organizace
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
ICAO	ICAO	kA	ICAO
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
19	[number]	k4	19
příloh	příloha	k1gFnPc2	příloha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
annexů	annex	k1gInPc2	annex
<g/>
,	,	kIx,	,
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
Annex	Annex	k1gInSc4	Annex
1	[number]	k4	1
až	až	k9	až
Annex	Annex	k1gInSc1	Annex
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
annexy	annex	k1gInPc1	annex
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
standardy	standard	k1gInPc1	standard
a	a	k8xC	a
doporučené	doporučený	k2eAgInPc1d1	doporučený
postupy	postup	k1gInPc1	postup
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
civilní	civilní	k2eAgInSc4d1	civilní
letecký	letecký	k2eAgInSc4d1	letecký
provoz	provoz	k1gInSc4	provoz
<g/>
;	;	kIx,	;
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
schválení	schválení	k1gNnSc6	schválení
v	v	k7c6	v
ICAO	ICAO	kA	ICAO
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
doporučením	doporučení	k1gNnSc7	doporučení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
přebíráno	přebírat	k5eAaImNgNnS	přebírat
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
jako	jako	k8xC	jako
zákonná	zákonný	k2eAgFnSc1d1	zákonná
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Letecký	letecký	k2eAgInSc1d1	letecký
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
zákonodárství	zákonodárství	k1gNnSc6	zákonodárství
tyto	tento	k3xDgInPc4	tento
annexy	annex	k1gInPc4	annex
tvoří	tvořit	k5eAaImIp3nP	tvořit
letecké	letecký	k2eAgInPc4d1	letecký
předpisy	předpis	k1gInPc4	předpis
L1	L1	k1gFnSc2	L1
až	až	k9	až
L	L	kA	L
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíle	Cíla	k1gFnSc6	Cíla
==	==	k?	==
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
definovaným	definovaný	k2eAgInSc7d1	definovaný
v	v	k7c6	v
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozvoj	rozvoj	k1gInSc4	rozvoj
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
civilního	civilní	k2eAgNnSc2d1	civilní
letectví	letectví	k1gNnSc2	letectví
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
a	a	k8xC	a
spořádaným	spořádaný	k2eAgInSc7d1	spořádaný
způsobem	způsob	k1gInSc7	způsob
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
rovnosti	rovnost	k1gFnPc4	rovnost
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
fungovat	fungovat	k5eAaImF	fungovat
spolehlivě	spolehlivě	k6eAd1	spolehlivě
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
.	.	kIx.	.
</s>
<s>
ICAO	ICAO	kA	ICAO
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dokumentech	dokument	k1gInPc6	dokument
"	"	kIx"	"
<g/>
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
plynulost	plynulost	k1gFnSc4	plynulost
civilního	civilní	k2eAgNnSc2d1	civilní
letectví	letectví	k1gNnSc2	letectví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
státní	státní	k2eAgInPc4d1	státní
úřady	úřad	k1gInPc4	úřad
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejich	jejich	k3xOp3gNnPc4	jejich
příslušná	příslušný	k2eAgNnPc4d1	příslušné
ministerstva	ministerstvo	k1gNnPc4	ministerstvo
a	a	k8xC	a
příslušné	příslušný	k2eAgInPc4d1	příslušný
letecké	letecký	k2eAgInPc4d1	letecký
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
shromáždění	shromáždění	k1gNnSc3	shromáždění
ICAO	ICAO	kA	ICAO
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
letecké	letecký	k2eAgFnSc2d1	letecká
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc2d1	komunikační
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
objemu	objem	k1gInSc2	objem
letecké	letecký	k2eAgFnSc2d1	letecká
přepravy	přeprava	k1gFnSc2	přeprava
osob	osoba	k1gFnPc2	osoba
neustále	neustále	k6eAd1	neustále
modernizuje	modernizovat	k5eAaBmIp3nS	modernizovat
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
letištní	letištní	k2eAgNnPc4d1	letištní
zabezpečovací	zabezpečovací	k2eAgNnPc4d1	zabezpečovací
zařízení	zařízení	k1gNnPc4	zařízení
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
leteckého	letecký	k2eAgInSc2d1	letecký
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgInPc1d1	regionální
úřady	úřad	k1gInPc1	úřad
ICAO	ICAO	kA	ICAO
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
OSN	OSN	kA	OSN
a	a	k8xC	a
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
ICAO	ICAO	kA	ICAO
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
chudým	chudý	k2eAgFnPc3d1	chudá
zemím	zem	k1gFnPc3	zem
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
nových	nový	k2eAgNnPc2d1	nové
řízených	řízený	k2eAgNnPc2d1	řízené
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
vyhovujících	vyhovující	k2eAgNnPc2d1	vyhovující
normám	norma	k1gFnPc3	norma
ICAO	ICAO	kA	ICAO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Standardy	standard	k1gInPc7	standard
ICAO	ICAO	kA	ICAO
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
standardy	standard	k1gInPc4	standard
definované	definovaný	k2eAgInPc4d1	definovaný
touto	tento	k3xDgFnSc7	tento
organizací	organizace	k1gFnSc7	organizace
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
jednoznačné	jednoznačný	k2eAgInPc4d1	jednoznačný
kódy	kód	k1gInPc4	kód
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
leteckých	letecký	k2eAgMnPc2d1	letecký
dopravců	dopravce	k1gMnPc2	dopravce
a	a	k8xC	a
typů	typ	k1gInPc2	typ
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
dokumentech	dokument	k1gInPc6	dokument
a	a	k8xC	a
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
ICAO	ICAO	kA	ICAO
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
používání	používání	k1gNnSc1	používání
stanovených	stanovený	k2eAgInPc2d1	stanovený
řízených	řízený	k2eAgInPc2d1	řízený
i	i	k8xC	i
neřízených	řízený	k2eNgInPc2d1	neřízený
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
prostorů	prostor	k1gInPc2	prostor
ve	v	k7c6	v
třídách	třída	k1gFnPc6	třída
označených	označený	k2eAgFnPc2d1	označená
A	a	k8xC	a
až	až	k9	až
G.	G.	kA	G.
Většina	většina	k1gFnSc1	většina
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
A	a	k9	a
až	až	k9	až
E	E	kA	E
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
řízené	řízený	k2eAgInPc4d1	řízený
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
především	především	k9	především
pro	pro	k7c4	pro
řízené	řízený	k2eAgInPc4d1	řízený
lety	let	k1gInPc4	let
podle	podle	k7c2	podle
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
IFR	IFR	kA	IFR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Vimr	Vimr	k1gMnSc1	Vimr
<g/>
:	:	kIx,	:
Letecká	letecký	k2eAgFnSc1d1	letecká
angličtina	angličtina	k1gFnSc1	angličtina
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc4	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87567-61-6	[number]	k4	978-80-87567-61-6
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
IATA	IATA	kA	IATA
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
</s>
</p>
<p>
<s>
ICAO	ICAO	kA	ICAO
kód	kód	k1gInSc1	kód
letiště	letiště	k1gNnSc2	letiště
</s>
</p>
<p>
<s>
Jeppesen	Jeppesen	k2eAgInSc1d1	Jeppesen
</s>
</p>
<p>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
zákon	zákon	k1gInSc1	zákon
</s>
</p>
<p>
<s>
Letecké	letecký	k2eAgInPc1d1	letecký
předpisy	předpis	k1gInPc1	předpis
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc4	dílo
Chicagská	chicagský	k2eAgFnSc1d1	Chicagská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
civilním	civilní	k2eAgNnSc6d1	civilní
letectví	letectví	k1gNnSc6	letectví
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
