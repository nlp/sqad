<p>
<s>
Biologie	biologie	k1gFnSc1	biologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
bio	bio	k?	bio
jako	jako	k8xS	jako
život	život	k1gInSc1	život
a	a	k8xC	a
logie	logie	k1gFnSc1	logie
jako	jako	k8xC	jako
věda	věda	k1gFnSc1	věda
–	–	k?	–
tedy	tedy	k9	tedy
životověda	životověda	k1gFnSc1	životověda
–	–	k?	–
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
organismy	organismus	k1gInPc7	organismus
a	a	k8xC	a
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
od	od	k7c2	od
chemických	chemický	k2eAgInPc2d1	chemický
dějů	děj	k1gInPc2	děj
v	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
ekosystémy	ekosystém	k1gInPc4	ekosystém
–	–	k?	–
tedy	tedy	k8xC	tedy
společenstva	společenstvo	k1gNnSc2	společenstvo
mnoha	mnoho	k4c2	mnoho
populací	populace	k1gFnPc2	populace
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
i	i	k8xC	i
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
bychom	by	kYmCp1nP	by
pak	pak	k6eAd1	pak
biologii	biologie	k1gFnSc4	biologie
mohli	moct	k5eAaImAgMnP	moct
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
zkoumající	zkoumající	k2eAgInPc4d1	zkoumající
organismy	organismus	k1gInPc4	organismus
od	od	k7c2	od
úrovně	úroveň	k1gFnSc2	úroveň
subcelulární	subcelulární	k2eAgFnSc2d1	subcelulární
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
od	od	k7c2	od
úrovně	úroveň	k1gFnSc2	úroveň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
buněčných	buněčný	k2eAgFnPc2d1	buněčná
organel	organela	k1gFnPc2	organela
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
úrovně	úroveň	k1gFnPc4	úroveň
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
jedinců	jedinec	k1gMnPc2	jedinec
až	až	k6eAd1	až
po	po	k7c4	po
úroveň	úroveň	k1gFnSc4	úroveň
populací	populace	k1gFnPc2	populace
<g/>
,	,	kIx,	,
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
,	,	kIx,	,
ekosystémů	ekosystém	k1gInPc2	ekosystém
a	a	k8xC	a
biomů	biom	k1gInPc2	biom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
β	β	k?	β
<g/>
;	;	kIx,	;
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
β	β	k?	β
(	(	kIx(	(
<g/>
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
λ	λ	k?	λ
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
<g/>
))	))	k?	))
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Michaela	Michael	k1gMnSc2	Michael
Christopha	Christoph	k1gMnSc2	Christoph
Hanova	Hanův	k2eAgFnSc1d1	Hanova
–	–	k?	–
Philosophiae	Philosophiae	k1gFnSc1	Philosophiae
naturalis	naturalis	k1gFnSc1	naturalis
sive	sive	k1gFnSc1	sive
physicae	physicae	k1gFnSc1	physicae
dogmaticae	dogmaticae	k1gFnSc1	dogmaticae
<g/>
:	:	kIx,	:
Geologia	Geologia	k1gFnSc1	Geologia
<g/>
,	,	kIx,	,
biologia	biologia	k1gFnSc1	biologia
<g/>
,	,	kIx,	,
phytologia	phytologia	k1gFnSc1	phytologia
generalis	generalis	k1gFnSc1	generalis
et	et	k?	et
dendrologia	dendrologia	k1gFnSc1	dendrologia
<g/>
,	,	kIx,	,
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
"	"	kIx"	"
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
Jeanem-Baptistou	Jeanem-Baptista	k1gMnSc7	Jeanem-Baptista
Lamarckem	Lamarck	k1gInSc7	Lamarck
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historii	historie	k1gFnSc4	historie
biologie	biologie	k1gFnSc2	biologie
můžeme	moct	k5eAaImIp1nP	moct
ovšem	ovšem	k9	ovšem
hledat	hledat	k5eAaImF	hledat
už	už	k9	už
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
dávných	dávný	k2eAgFnPc2d1	dávná
lidských	lidský	k2eAgFnPc2d1	lidská
civilizací	civilizace	k1gFnPc2	civilizace
a	a	k8xC	a
klasických	klasický	k2eAgMnPc2d1	klasický
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
způsob	způsob	k1gInSc1	způsob
chápání	chápání	k1gNnSc2	chápání
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
pojetí	pojetí	k1gNnSc1	pojetí
biologie	biologie	k1gFnSc2	biologie
jako	jako	k8xS	jako
samostatného	samostatný	k2eAgNnSc2d1	samostatné
souvislého	souvislý	k2eAgNnSc2d1	souvislé
odvětví	odvětví	k1gNnSc2	odvětví
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
přírodou	příroda	k1gFnSc7	příroda
zabývali	zabývat	k5eAaImAgMnP	zabývat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
a	a	k8xC	a
Claudius	Claudius	k1gMnSc1	Claudius
Galén	Galén	k1gInSc1	Galén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
bádání	bádání	k1gNnSc6	bádání
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
arabští	arabský	k2eAgMnPc1d1	arabský
lékaři	lékař	k1gMnPc1	lékař
jako	jako	k8xS	jako
al-Jahiz	al-Jahiz	k1gInSc1	al-Jahiz
<g/>
,	,	kIx,	,
Avicenna	Avicenna	k1gFnSc1	Avicenna
<g/>
,	,	kIx,	,
Avenzoar	Avenzoar	k1gInSc1	Avenzoar
a	a	k8xC	a
Ibn	Ibn	k1gFnSc1	Ibn
al-Nafis	al-Nafis	k1gFnSc1	al-Nafis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
novověku	novověk	k1gInSc2	novověk
biologie	biologie	k1gFnSc1	biologie
jako	jako	k8xS	jako
věda	věda	k1gFnSc1	věda
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
vědci	vědec	k1gMnPc7	vědec
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
William	William	k1gInSc4	William
Harvey	Harvea	k1gMnSc2	Harvea
a	a	k8xC	a
Andreas	Andreas	k1gMnSc1	Andreas
Vesalius	Vesalius	k1gMnSc1	Vesalius
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prováděli	provádět	k5eAaImAgMnP	provádět
pokusy	pokus	k1gInPc4	pokus
a	a	k8xC	a
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
Linné	Linná	k1gFnPc4	Linná
a	a	k8xC	a
Buffon	Buffon	k1gInSc1	Buffon
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
zkoumat	zkoumat	k5eAaImF	zkoumat
různorodost	různorodost	k1gFnSc4	různorodost
života	život	k1gInSc2	život
a	a	k8xC	a
fosílie	fosílie	k1gFnPc4	fosílie
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
biologie	biologie	k1gFnSc1	biologie
stala	stát	k5eAaPmAgFnS	stát
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přírodní	přírodní	k2eAgFnSc7d1	přírodní
vědou	věda	k1gFnSc7	věda
určenou	určený	k2eAgFnSc7d1	určená
pouze	pouze	k6eAd1	pouze
odborníkům	odborník	k1gMnPc3	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
jako	jako	k9	jako
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humbolt	Humbolt	k1gMnSc1	Humbolt
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
organismy	organismus	k1gInPc7	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
biologem	biolog	k1gMnSc7	biolog
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
teorií	teorie	k1gFnSc7	teorie
o	o	k7c6	o
evoluci	evoluce	k1gFnSc6	evoluce
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
setkávala	setkávat	k5eAaImAgFnS	setkávat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
jako	jako	k8xS	jako
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
znovuobjevení	znovuobjevení	k1gNnSc2	znovuobjevení
práce	práce	k1gFnSc2	práce
Gregora	Gregor	k1gMnSc2	Gregor
Mendela	Mendel	k1gMnSc2	Mendel
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rapidnímu	rapidní	k2eAgInSc3d1	rapidní
vzrůstu	vzrůst	k1gInSc3	vzrůst
výzkumu	výzkum	k1gInSc2	výzkum
genetiky	genetika	k1gFnSc2	genetika
Thomasem	Thomas	k1gMnSc7	Thomas
Morganem	morgan	k1gMnSc7	morgan
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
studenty	student	k1gMnPc4	student
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
kombinací	kombinace	k1gFnSc7	kombinace
populační	populační	k2eAgFnSc2d1	populační
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
v	v	k7c6	v
"	"	kIx"	"
<g/>
novo-Darwinovské	novo-Darwinovský	k2eAgFnSc6d1	novo-Darwinovský
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
D.	D.	kA	D.
Watson	Watson	k1gInSc1	Watson
a	a	k8xC	a
Francis	Francis	k1gInSc1	Francis
Crick	Crick	k1gMnSc1	Crick
objevili	objevit	k5eAaPmAgMnP	objevit
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
strukturu	struktura	k1gFnSc4	struktura
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celá	k1gFnSc6	celá
dvacáté	dvacátý	k4xOgFnSc6	dvacátý
století	století	k1gNnSc1	století
probíhal	probíhat	k5eAaImAgInS	probíhat
výzkum	výzkum	k1gInSc4	výzkum
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
rozluštění	rozluštění	k1gNnSc2	rozluštění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
až	až	k6eAd1	až
po	po	k7c4	po
postupné	postupný	k2eAgNnSc4d1	postupné
mapování	mapování	k1gNnSc4	mapování
genomů	genom	k1gInPc2	genom
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
pokračující	pokračující	k2eAgFnSc7d1	pokračující
v	v	k7c4	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základy	základ	k1gInPc1	základ
moderní	moderní	k2eAgFnSc2d1	moderní
biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
základů	základ	k1gInPc2	základ
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
teorie	teorie	k1gFnSc1	teorie
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
všechno	všechen	k3xTgNnSc1	všechen
co	co	k9	co
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
základních	základní	k2eAgInPc2d1	základní
stavebních	stavební	k2eAgInPc2d1	stavební
kamenů	kámen	k1gInPc2	kámen
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
pochody	pochod	k1gInPc1	pochod
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
buněk	buňka	k1gFnPc2	buňka
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
jen	jen	k9	jen
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
předešlých	předešlý	k2eAgInPc2d1	předešlý
násobným	násobný	k2eAgNnSc7d1	násobné
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
teorie	teorie	k1gFnSc1	teorie
studuje	studovat	k5eAaImIp3nS	studovat
stavbu	stavba	k1gFnSc4	stavba
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
–	–	k?	–
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označení	označení	k1gNnSc1	označení
procesu	proces	k1gInSc2	proces
postupné	postupný	k2eAgFnSc2d1	postupná
změny	změna	k1gFnSc2	změna
genetických	genetický	k2eAgFnPc2d1	genetická
vlastností	vlastnost	k1gFnPc2	vlastnost
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
procesu	proces	k1gInSc2	proces
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
genů	gen	k1gInPc2	gen
–	–	k?	–
vlastnosti	vlastnost	k1gFnPc4	vlastnost
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
zakódovány	zakódován	k2eAgFnPc1d1	zakódována
v	v	k7c6	v
genech	gen	k1gInPc6	gen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homeostáze	Homeostáze	k1gFnSc1	Homeostáze
–	–	k?	–
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
procesy	proces	k1gInPc4	proces
udržující	udržující	k2eAgFnSc2d1	udržující
stálé	stálý	k2eAgFnSc2d1	stálá
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
podmínky	podmínka	k1gFnSc2	podmínka
těla	tělo	k1gNnSc2	tělo
nehledíc	hledět	k5eNaImSgFnS	hledět
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
vnější	vnější	k2eAgFnPc4d1	vnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
–	–	k?	–
základním	základní	k2eAgInSc7d1	základní
rysem	rys	k1gInSc7	rys
každého	každý	k3xTgInSc2	každý
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc4	využití
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Přežití	přežití	k1gNnSc1	přežití
každého	každý	k3xTgMnSc2	každý
živočicha	živočich	k1gMnSc2	živočich
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
neustálém	neustálý	k2eAgInSc6d1	neustálý
přísunu	přísun	k1gInSc6	přísun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podřízené	podřízený	k2eAgFnPc4d1	podřízená
a	a	k8xC	a
související	související	k2eAgFnPc4d1	související
vědní	vědní	k2eAgFnPc4d1	vědní
obory	obora	k1gFnPc4	obora
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c4	pod
biologii	biologie	k1gFnSc4	biologie
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nalézt	nalézt	k5eAaBmF	nalézt
její	její	k3xOp3gFnSc4	její
úzkou	úzký	k2eAgFnSc4d1	úzká
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
vědními	vědní	k2eAgInPc7d1	vědní
obory	obor	k1gInPc7	obor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
agrobiologie	agrobiologie	k1gFnSc1	agrobiologie
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
biologické	biologický	k2eAgFnPc4d1	biologická
zákonitosti	zákonitost	k1gFnPc4	zákonitost
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
</s>
</p>
<p>
<s>
anatomie	anatomie	k1gFnSc1	anatomie
–	–	k?	–
studium	studium	k1gNnSc1	studium
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
funkcí	funkce	k1gFnPc2	funkce
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
specificky	specificky	k6eAd1	specificky
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
bionomie	bionomie	k1gFnSc1	bionomie
–	–	k?	–
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgNnSc4d1	komplexní
studium	studium	k1gNnSc4	studium
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
prostředí	prostředí	k1gNnSc3	prostředí
</s>
</p>
<p>
<s>
biofyzika	biofyzika	k1gFnSc1	biofyzika
–	–	k?	–
hraniční	hraniční	k2eAgInSc4d1	hraniční
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
biologické	biologický	k2eAgInPc4d1	biologický
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
metodami	metoda	k1gFnPc7	metoda
</s>
</p>
<p>
<s>
biogeografie	biogeografie	k1gFnSc1	biogeografie
–	–	k?	–
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
</s>
</p>
<p>
<s>
biochemie	biochemie	k1gFnSc1	biochemie
–	–	k?	–
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
biologická	biologický	k2eAgFnSc1d1	biologická
systematika	systematika	k1gFnSc1	systematika
–	–	k?	–
(	(	kIx(	(
<g/>
též	též	k9	též
taxonomie	taxonomie	k1gFnSc1	taxonomie
<g/>
)	)	kIx)	)
vědecky	vědecky	k6eAd1	vědecky
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
druhovou	druhový	k2eAgFnSc4d1	druhová
diverzitu	diverzita	k1gFnSc4	diverzita
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
</s>
</p>
<p>
<s>
bionika	bionik	k1gMnSc4	bionik
–	–	k?	–
je	být	k5eAaImIp3nS	být
hraniční	hraniční	k2eAgInSc4d1	hraniční
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
systematicky	systematicky	k6eAd1	systematicky
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
na	na	k7c6	na
uplatňování	uplatňování	k1gNnSc6	uplatňování
poznatků	poznatek	k1gInPc2	poznatek
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
struktur	struktura	k1gFnPc2	struktura
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
</s>
</p>
<p>
<s>
biostatistika	biostatistika	k1gFnSc1	biostatistika
–	–	k?	–
obor	obor	k1gInSc4	obor
využívající	využívající	k2eAgFnSc2d1	využívající
metody	metoda	k1gFnSc2	metoda
matematické	matematický	k2eAgFnSc2d1	matematická
statistiky	statistika	k1gFnSc2	statistika
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
</s>
</p>
<p>
<s>
botanika	botanika	k1gFnSc1	botanika
–	–	k?	–
(	(	kIx(	(
<g/>
též	též	k9	též
rostlinopis	rostlinopis	k1gInSc1	rostlinopis
<g/>
,	,	kIx,	,
fytologie	fytologie	k1gFnSc1	fytologie
<g/>
)	)	kIx)	)
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
flórou	flóra	k1gFnSc7	flóra
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
částí	část	k1gFnSc7	část
přírody	příroda	k1gFnSc2	příroda
</s>
</p>
<p>
<s>
buněčná	buněčný	k2eAgFnSc1d1	buněčná
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
(	(	kIx(	(
<g/>
též	též	k9	též
cytologie	cytologie	k1gFnSc1	cytologie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
buněk	buňka	k1gFnPc2	buňka
</s>
</p>
<p>
<s>
ekologie	ekologie	k1gFnSc1	ekologie
–	–	k?	–
studium	studium	k1gNnSc1	studium
vlivů	vliv	k1gInPc2	vliv
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
neživých	živý	k2eNgFnPc2d1	neživá
složek	složka	k1gFnPc2	složka
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
prostředí	prostředí	k1gNnSc4	prostředí
</s>
</p>
<p>
<s>
etologie	etologie	k1gFnSc1	etologie
–	–	k?	–
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
chování	chování	k1gNnSc2	chování
živočichů	živočich	k1gMnPc2	živočich
</s>
</p>
<p>
<s>
evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
biologickou	biologický	k2eAgFnSc7d1	biologická
evolucí	evoluce	k1gFnSc7	evoluce
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
</s>
</p>
<p>
<s>
fyziologie	fyziologie	k1gFnSc1	fyziologie
–	–	k?	–
studium	studium	k1gNnSc1	studium
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
</s>
</p>
<p>
<s>
genetika	genetika	k1gFnSc1	genetika
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
dědičností	dědičnost	k1gFnSc7	dědičnost
i	i	k8xC	i
proměnlivostí	proměnlivost	k1gFnSc7	proměnlivost
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
příčinami	příčina	k1gFnPc7	příčina
</s>
</p>
<p>
<s>
matematická	matematický	k2eAgFnSc1d1	matematická
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
matematické	matematický	k2eAgFnPc4d1	matematická
metody	metoda	k1gFnPc4	metoda
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
</s>
</p>
<p>
<s>
medicínská	medicínský	k2eAgFnSc1d1	medicínská
biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
lékařské	lékařský	k2eAgFnPc1d1	lékařská
disciplíny	disciplína	k1gFnPc1	disciplína
</s>
</p>
<p>
<s>
mikrobiologie	mikrobiologie	k1gFnSc1	mikrobiologie
–	–	k?	–
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
studující	studující	k1gFnSc2	studující
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
</s>
</p>
<p>
<s>
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
studium	studium	k1gNnSc1	studium
buněčných	buněčný	k2eAgInPc2d1	buněčný
biologických	biologický	k2eAgInPc2d1	biologický
procesů	proces	k1gInPc2	proces
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
</s>
</p>
<p>
<s>
morfologie	morfologie	k1gFnSc1	morfologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
vnější	vnější	k2eAgFnSc7d1	vnější
stavbou	stavba	k1gFnSc7	stavba
organismů	organismus	k1gInPc2	organismus
</s>
</p>
<p>
<s>
mykologie	mykologie	k1gFnSc1	mykologie
–	–	k?	–
studium	studium	k1gNnSc1	studium
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
genetických	genetický	k2eAgFnPc2d1	genetická
a	a	k8xC	a
biochemických	biochemický	k2eAgFnPc2d1	biochemická
vlastností	vlastnost	k1gFnPc2	vlastnost
</s>
</p>
<p>
<s>
paleontologie	paleontologie	k1gFnSc1	paleontologie
–	–	k?	–
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
geologických	geologický	k2eAgNnPc6d1	geologické
obdobích	období	k1gNnPc6	období
</s>
</p>
<p>
<s>
virologie	virologie	k1gFnSc1	virologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
nebuněčných	buněčný	k2eNgInPc2d1	nebuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
viroidů	viroid	k1gInPc2	viroid
a	a	k8xC	a
virusoidů	virusoid	k1gInPc2	virusoid
</s>
</p>
<p>
<s>
vývojová	vývojový	k2eAgFnSc1d1	vývojová
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
procesů	proces	k1gInPc2	proces
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
organismů	organismus	k1gInPc2	organismus
</s>
</p>
<p>
<s>
zoologie	zoologie	k1gFnSc1	zoologie
–	–	k?	–
studie	studie	k1gFnSc1	studie
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
,	,	kIx,	,
fyziologie	fyziologie	k1gFnSc2	fyziologie
<g/>
,	,	kIx,	,
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
chovánía	chovánía	k6eAd1	chovánía
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
vědních	vědní	k2eAgFnPc2d1	vědní
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obory	obor	k1gInPc4	obor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Strukturální	strukturální	k2eAgMnPc4d1	strukturální
===	===	k?	===
</s>
</p>
<p>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
studuje	studovat	k5eAaImIp3nS	studovat
biologické	biologický	k2eAgInPc4d1	biologický
buněčné	buněčný	k2eAgInPc4d1	buněčný
procesy	proces	k1gInPc4	proces
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
podstata	podstata	k1gFnSc1	podstata
dědičnosti	dědičnost	k1gFnSc2	dědičnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
odhalit	odhalit	k5eAaPmF	odhalit
jen	jen	k9	jen
studiem	studio	k1gNnSc7	studio
její	její	k3xOp3gFnSc2	její
molekulární	molekulární	k2eAgFnSc2d1	molekulární
podstaty	podstata	k1gFnSc2	podstata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
popisuje	popisovat	k5eAaImIp3nS	popisovat
biologické	biologický	k2eAgFnSc2d1	biologická
makromolekuly	makromolekula	k1gFnSc2	makromolekula
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
funkční	funkční	k2eAgInPc1d1	funkční
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
věnuje	věnovat	k5eAaImIp3nS	věnovat
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
RNA	RNA	kA	RNA
a	a	k8xC	a
proteinům	protein	k1gInPc3	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
molekulární	molekulární	k2eAgFnSc1d1	molekulární
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znalosti	znalost	k1gFnPc1	znalost
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgInP	využívat
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
genetickém	genetický	k2eAgNnSc6d1	genetické
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cytologie	cytologie	k1gFnSc1	cytologie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
buněčná	buněčný	k2eAgFnSc1d1	buněčná
biologie	biologie	k1gFnSc1	biologie
studuje	studovat	k5eAaImIp3nS	studovat
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc4	fyziologie
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
životním	životní	k2eAgFnPc3d1	životní
funkcím	funkce	k1gFnPc3	funkce
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
provázána	provázat	k5eAaPmNgFnS	provázat
s	s	k7c7	s
molekulární	molekulární	k2eAgFnSc7d1	molekulární
biologii	biologie	k1gFnSc4	biologie
<g/>
,	,	kIx,	,
organickou	organický	k2eAgFnSc7d1	organická
chemií	chemie	k1gFnSc7	chemie
<g/>
,	,	kIx,	,
biochemií	biochemie	k1gFnSc7	biochemie
<g/>
,	,	kIx,	,
biofyzikou	biofyzika	k1gFnSc7	biofyzika
<g/>
,	,	kIx,	,
mikrobiologií	mikrobiologie	k1gFnSc7	mikrobiologie
<g/>
,	,	kIx,	,
mikroskopií	mikroskopie	k1gFnSc7	mikroskopie
a	a	k8xC	a
histologií	histologie	k1gFnSc7	histologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genetika	genetika	k1gFnSc1	genetika
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
geny	gen	k1gInPc7	gen
<g/>
,	,	kIx,	,
dědičností	dědičnost	k1gFnSc7	dědičnost
a	a	k8xC	a
proměnlivostí	proměnlivost	k1gFnSc7	proměnlivost
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
buňka	buňka	k1gFnSc1	buňka
v	v	k7c6	v
sobě	se	k3xPyFc3	se
přechovává	přechovávat	k5eAaImIp3nS	přechovávat
v	v	k7c6	v
chromozomech	chromozom	k1gInPc6	chromozom
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
reprezentovanou	reprezentovaný	k2eAgFnSc7d1	reprezentovaná
strukturou	struktura	k1gFnSc7	struktura
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývojová	vývojový	k2eAgFnSc1d1	vývojová
biologie	biologie	k1gFnSc1	biologie
studuje	studovat	k5eAaImIp3nS	studovat
růst	růst	k1gInSc4	růst
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjející	vyvíjející	k2eAgFnSc1d1	vyvíjející
se	se	k3xPyFc4	se
z	z	k7c2	z
embryologie	embryologie	k1gFnSc2	embryologie
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
vývojová	vývojový	k2eAgFnSc1d1	vývojová
biologie	biologie	k1gFnSc1	biologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
genetické	genetický	k2eAgNnSc1d1	genetické
řízení	řízení	k1gNnSc1	řízení
růstu	růst	k1gInSc2	růst
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
rozdílností	rozdílnost	k1gFnPc2	rozdílnost
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
proces	proces	k1gInSc1	proces
dává	dávat	k5eAaImIp3nS	dávat
vznik	vznik	k1gInSc4	vznik
tkáním	tkáň	k1gFnPc3	tkáň
<g/>
,	,	kIx,	,
orgánům	orgán	k1gInPc3	orgán
a	a	k8xC	a
celé	celý	k2eAgFnSc3d1	celá
anatomii	anatomie	k1gFnSc3	anatomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
===	===	k?	===
</s>
</p>
<p>
<s>
Fyziologie	fyziologie	k1gFnSc1	fyziologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
mechanické	mechanický	k2eAgInPc4d1	mechanický
<g/>
,	,	kIx,	,
fyzické	fyzický	k2eAgInPc4d1	fyzický
a	a	k8xC	a
biochemické	biochemický	k2eAgInPc4d1	biochemický
procesy	proces	k1gInPc4	proces
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1	fyziologie
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc4	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
fyziologii	fyziologie	k1gFnSc4	fyziologie
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
principy	princip	k1gInPc1	princip
fyziologie	fyziologie	k1gFnSc2	fyziologie
jsou	být	k5eAaImIp3nP	být
univerzální	univerzální	k2eAgInPc1d1	univerzální
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
je	být	k5eAaImIp3nS	být
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
organismus	organismus	k1gInSc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
známe	znát	k5eAaImIp1nP	znát
o	o	k7c6	o
buňce	buňka	k1gFnSc6	buňka
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
i	i	k9	i
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc4d1	důležité
odvětví	odvětví	k1gNnSc4	odvětví
fyziologie	fyziologie	k1gFnSc2	fyziologie
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
funkcí	funkce	k1gFnSc7	funkce
orgánovými	orgánový	k2eAgFnPc7d1	orgánová
soustavami	soustava	k1gFnPc7	soustava
jako	jako	k8xC	jako
oběhovou	oběhový	k2eAgFnSc7d1	oběhová
<g/>
,	,	kIx,	,
nervovou	nervový	k2eAgFnSc7d1	nervová
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgFnSc7d1	dýchací
a	a	k8xC	a
endokrinní	endokrinní	k2eAgFnSc7d1	endokrinní
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
v	v	k7c6	v
disciplínách	disciplína	k1gFnPc6	disciplína
jako	jako	k8xS	jako
neurologie	neurologie	k1gFnSc2	neurologie
a	a	k8xC	a
imunologie	imunologie	k1gFnSc2	imunologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evoluce	evoluce	k1gFnSc2	evoluce
===	===	k?	===
</s>
</p>
<p>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
původ	původ	k1gInSc4	původ
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
za	za	k7c4	za
jejich	jejich	k3xOp3gFnPc4	jejich
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vědce	vědec	k1gMnPc4	vědec
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
taxonomicky	taxonomicky	k6eAd1	taxonomicky
orientovaných	orientovaný	k2eAgInPc2d1	orientovaný
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
to	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vědce	vědec	k1gMnPc4	vědec
specializované	specializovaný	k2eAgMnPc4d1	specializovaný
na	na	k7c4	na
obory	obor	k1gInPc4	obor
jako	jako	k8xS	jako
mammalogie	mammalogie	k1gFnPc4	mammalogie
<g/>
,	,	kIx,	,
ornitologie	ornitologie	k1gFnSc1	ornitologie
<g/>
,	,	kIx,	,
botanika	botanika	k1gFnSc1	botanika
nebo	nebo	k8xC	nebo
herpetologie	herpetologie	k1gFnSc1	herpetologie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
přírodním	přírodní	k2eAgInSc6d1	přírodní
řádu	řád	k1gInSc6	řád
k	k	k7c3	k
zodpovědění	zodpovědění	k1gNnSc3	zodpovědění
hlavních	hlavní	k2eAgFnPc2d1	hlavní
otázek	otázka	k1gFnPc2	otázka
ohledně	ohledně	k7c2	ohledně
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
biologie	biologie	k1gFnSc1	biologie
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c4	na
paleontologii	paleontologie	k1gFnSc4	paleontologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
užívá	užívat	k5eAaImIp3nS	užívat
poznatky	poznatek	k1gInPc4	poznatek
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
způsobu	způsob	k1gInSc3	způsob
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
evoluce	evoluce	k1gFnSc2	evoluce
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
oblastí	oblast	k1gFnPc2	oblast
jako	jako	k8xS	jako
populační	populační	k2eAgFnSc1d1	populační
genetika	genetika	k1gFnSc1	genetika
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Taxonomie	taxonomie	k1gFnSc2	taxonomie
===	===	k?	===
</s>
</p>
<p>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
se	se	k3xPyFc4	se
teoreticky	teoreticky	k6eAd1	teoreticky
i	i	k9	i
prakticky	prakticky	k6eAd1	prakticky
zabývá	zabývat	k5eAaImIp3nS	zabývat
klasifikací	klasifikace	k1gFnSc7	klasifikace
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
organismy	organismus	k1gInPc4	organismus
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
taxonů	taxon	k1gInPc2	taxon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
taxonomické	taxonomický	k2eAgFnPc1d1	Taxonomická
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
život	život	k1gInSc1	život
</s>
</p>
<p>
<s>
doména	doména	k1gFnSc1	doména
</s>
</p>
<p>
<s>
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
regnum	regnum	k1gInSc1	regnum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
(	(	kIx(	(
<g/>
phyllum	phyllum	k1gNnSc1	phyllum
<g/>
,	,	kIx,	,
divisio	divisio	k1gNnSc1	divisio
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
classis	classis	k1gFnSc1	classis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
ordo	orda	k1gFnSc5	orda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
čeleď	čeleď	k1gFnSc1	čeleď
(	(	kIx(	(
<g/>
familia	familia	k1gFnSc1	familia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
genus	genus	k1gInSc1	genus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
species	species	k1gFnSc1	species
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
domén	doména	k1gFnPc2	doména
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Archaea	Archaea	k6eAd1	Archaea
</s>
</p>
<p>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
</s>
</p>
<p>
<s>
EukaryotaEukaryota	EukaryotaEukaryota	k1gFnSc1	EukaryotaEukaryota
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
dělí	dělit	k5eAaImIp3nS	dělit
buď	buď	k8xC	buď
do	do	k7c2	do
šesti	šest	k4xCc2	šest
"	"	kIx"	"
<g/>
superskupin	superskupina	k1gFnPc2	superskupina
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Amoebozoa	Amoebozo	k2eAgFnSc1d1	Amoebozo
–	–	k?	–
Opisthokonta	Opisthokonta	k1gFnSc1	Opisthokonta
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tradiční	tradiční	k2eAgFnSc1d1	tradiční
říše	říše	k1gFnSc1	říše
houby	houba	k1gFnSc2	houba
a	a	k8xC	a
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
–	–	k?	–
Rhizaria	Rhizarium	k1gNnSc2	Rhizarium
–	–	k?	–
Archaeplastida	Archaeplastida	k1gFnSc1	Archaeplastida
(	(	kIx(	(
<g/>
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
–	–	k?	–
Chromalveolata	Chromalveole	k1gNnPc1	Chromalveole
–	–	k?	–
Excavata	Excavat	k1gMnSc2	Excavat
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
do	do	k7c2	do
pěti	pět	k4xCc2	pět
říší	říš	k1gFnPc2	říš
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Protozoa	Protozoa	k1gFnSc1	Protozoa
(	(	kIx(	(
<g/>
prvoci	prvok	k1gMnPc1	prvok
<g/>
)	)	kIx)	)
–	–	k?	–
Metazoa	Metazoa	k1gFnSc1	Metazoa
(	(	kIx(	(
<g/>
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
–	–	k?	–
Fungi	Fungi	k1gNnSc1	Fungi
(	(	kIx(	(
<g/>
houby	houba	k1gFnPc1	houba
<g/>
)	)	kIx)	)
–	–	k?	–
Plantae	Plantae	k1gInSc1	Plantae
(	(	kIx(	(
<g/>
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
–	–	k?	–
Chromista	Chromista	k1gMnSc1	Chromista
</s>
</p>
<p>
<s>
===	===	k?	===
Environmentální	environmentální	k2eAgMnPc4d1	environmentální
===	===	k?	===
</s>
</p>
<p>
<s>
Ekologie	ekologie	k1gFnSc1	ekologie
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
původní	původní	k2eAgMnSc1d1	původní
definoval	definovat	k5eAaBmAgInS	definovat
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
biologická	biologický	k2eAgFnSc1d1	biologická
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vztahem	vztah	k1gInSc7	vztah
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
vztahem	vztah	k1gInSc7	vztah
organismů	organismus	k1gInPc2	organismus
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Ekologie	ekologie	k1gFnSc1	ekologie
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
oborů	obor	k1gInPc2	obor
od	od	k7c2	od
obecné	obecný	k2eAgFnSc2d1	obecná
ekologie	ekologie	k1gFnSc2	ekologie
a	a	k8xC	a
globální	globální	k2eAgFnSc2d1	globální
ekologie	ekologie	k1gFnSc2	ekologie
k	k	k7c3	k
ekologii	ekologie	k1gFnSc3	ekologie
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc1	ekologie
lesa	les	k1gInSc2	les
a	a	k8xC	a
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
šíře	šíř	k1gFnSc2	šíř
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
objektů	objekt	k1gInPc2	objekt
se	se	k3xPyFc4	se
ekologie	ekologie	k1gFnSc1	ekologie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
ekologii	ekologie	k1gFnSc4	ekologie
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
autekologie	autekologie	k1gFnSc1	autekologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
populací	populace	k1gFnSc7	populace
(	(	kIx(	(
<g/>
demekologie	demekologie	k1gFnSc1	demekologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenstev	společenstvo	k1gNnPc2	společenstvo
(	(	kIx(	(
<g/>
synekologie	synekologie	k1gFnSc1	synekologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
biomů	biom	k1gInPc2	biom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etologie	etologie	k1gFnSc1	etologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
chování	chování	k1gNnSc2	chování
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
vrozených	vrozený	k2eAgFnPc2d1	vrozená
a	a	k8xC	a
naučených	naučený	k2eAgFnPc2d1	naučená
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
moderním	moderní	k2eAgMnSc7d1	moderní
etologem	etolog	k1gMnSc7	etolog
byl	být	k5eAaImAgMnS	být
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
The	The	k1gFnSc1	The
Expression	Expression	k1gInSc1	Expression
of	of	k?	of
the	the	k?	the
Emotions	Emotionsa	k1gFnPc2	Emotionsa
in	in	k?	in
Man	Man	k1gMnSc1	Man
and	and	k?	and
Animals	Animals	k1gInSc1	Animals
<g/>
)	)	kIx)	)
inspiovala	inspiovat	k5eAaBmAgFnS	inspiovat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
etologů	etolog	k1gMnPc2	etolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biogeografie	biogeografie	k1gFnSc1	biogeografie
studuje	studovat	k5eAaImIp3nS	studovat
rozmístění	rozmístění	k1gNnSc1	rozmístění
organismů	organismus	k1gInPc2	organismus
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
pozornost	pozornost	k1gFnSc1	pozornost
především	především	k9	především
tektonickým	tektonický	k2eAgFnPc3d1	tektonická
deskám	deska	k1gFnPc3	deska
<g/>
,	,	kIx,	,
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
migraci	migrace	k1gFnSc3	migrace
a	a	k8xC	a
kladistice	kladistika	k1gFnSc3	kladistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecná	obecný	k2eAgFnSc1d1	obecná
biologie	biologie	k1gFnSc1	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
biologie	biologie	k1gFnSc1	biologie
je	být	k5eAaImIp3nS	být
průřezové	průřezový	k2eAgNnSc4d1	průřezové
odvětví	odvětví	k1gNnSc4	odvětví
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
zkoumající	zkoumající	k2eAgInPc1d1	zkoumající
obecné	obecný	k2eAgInPc1d1	obecný
základy	základ	k1gInPc1	základ
živých	živý	k2eAgFnPc2d1	živá
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
integrovat	integrovat	k5eAaBmF	integrovat
poznatky	poznatek	k1gInPc1	poznatek
a	a	k8xC	a
zákonitosti	zákonitost	k1gFnPc1	zákonitost
související	související	k2eAgFnPc1d1	související
s	s	k7c7	s
živými	živý	k2eAgFnPc7d1	živá
soustavami	soustava	k1gFnPc7	soustava
a	a	k8xC	a
definovat	definovat	k5eAaBmF	definovat
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
teoretickou	teoretický	k2eAgFnSc7d1	teoretická
biologií	biologie	k1gFnSc7	biologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
biologické	biologický	k2eAgFnPc4d1	biologická
zákonitosti	zákonitost	k1gFnPc4	zákonitost
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
matematického	matematický	k2eAgInSc2d1	matematický
<g/>
,	,	kIx,	,
kybernetického	kybernetický	k2eAgMnSc2d1	kybernetický
a	a	k8xC	a
logického	logický	k2eAgNnSc2d1	logické
modelování	modelování	k1gNnSc2	modelování
a	a	k8xC	a
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
dalších	další	k2eAgFnPc2d1	další
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Biology	biolog	k1gMnPc4	biolog
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BENDA	Benda	k1gMnSc1	Benda
VLADIMÍR	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
BABŮREK	BABŮREK	kA	BABŮREK
IVAN	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
KOTRBA	kotrba	k1gFnSc1	kotrba
PAVEL	Pavla	k1gFnPc2	Pavla
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
VŠCHT	VŠCHT	kA	VŠCHT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7080	[number]	k4	7080
<g/>
-	-	kIx~	-
<g/>
587	[number]	k4	587
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
biologů	biolog	k1gMnPc2	biolog
</s>
</p>
<p>
<s>
Hierarchie	hierarchie	k1gFnSc1	hierarchie
druhů	druh	k1gInPc2	druh
</s>
</p>
<p>
<s>
Prokaryota	Prokaryota	k1gFnSc1	Prokaryota
</s>
</p>
<p>
<s>
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
</s>
</p>
<p>
<s>
Fylogenetika	Fylogenetika	k1gFnSc1	Fylogenetika
</s>
</p>
<p>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
klasifikace	klasifikace	k1gFnSc1	klasifikace
</s>
</p>
<p>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
</s>
</p>
<p>
<s>
Lékařství	lékařství	k1gNnSc1	lékařství
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
biologie	biologie	k1gFnSc2	biologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
biologie	biologie	k1gFnSc2	biologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Biologie	biologie	k1gFnSc2	biologie
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Biologie	biologie	k1gFnSc1	biologie
</s>
</p>
<p>
<s>
Toxikologický	toxikologický	k2eAgInSc1d1	toxikologický
výzkum	výzkum	k1gInSc1	výzkum
–	–	k?	–
CRO	CRO	kA	CRO
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Biomach	Biomach	k1gInSc1	Biomach
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
přehled	přehled	k1gInSc1	přehled
SŠ	SŠ	kA	SŠ
biologie	biologie	k1gFnSc2	biologie
</s>
</p>
