<s>
Biologie	biologie	k1gFnSc1	biologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
bio	bio	k?	bio
jako	jako	k8xC	jako
život	život	k1gInSc1	život
a	a	k8xC	a
logie	logie	k1gFnSc1	logie
jako	jako	k8xC	jako
věda	věda	k1gFnSc1	věda
–	–	k?	–
tedy	tedy	k9	tedy
životověda	životověda	k1gFnSc1	životověda
–	–	k?	–
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
organismy	organismus	k1gInPc7	organismus
a	a	k8xC	a
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
od	od	k7c2	od
chemických	chemický	k2eAgInPc2d1	chemický
dějů	děj	k1gInPc2	děj
v	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
ekosystémy	ekosystém	k1gInPc4	ekosystém
–	–	k?	–
tedy	tedy	k8xC	tedy
společenstva	společenstvo	k1gNnSc2	společenstvo
mnoha	mnoho	k4c2	mnoho
populací	populace	k1gFnPc2	populace
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
i	i	k8xC	i
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
