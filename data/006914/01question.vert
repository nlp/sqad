<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
silniční	silniční	k2eAgInSc1d1	silniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Seinu	Seina	k1gFnSc4	Seina
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
ostrov	ostrov	k1gInSc4	ostrov
Cité	Citá	k1gFnSc2	Citá
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
?	?	kIx.	?
</s>
