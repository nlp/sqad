<s>
Pont	Pont	k1gInSc1	Pont
Saint-Michel	Saint-Michel	k1gInSc1	Saint-Michel
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Most	most	k1gInSc1	most
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silniční	silniční	k2eAgInSc4d1	silniční
most	most	k1gInSc4	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Seinu	Seina	k1gFnSc4	Seina
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
ostrov	ostrov	k1gInSc4	ostrov
Cité	Citá	k1gFnSc2	Citá
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Justiční	justiční	k2eAgInSc1d1	justiční
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
kamenného	kamenný	k2eAgInSc2d1	kamenný
mostu	most	k1gInSc2	most
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pařížský	pařížský	k2eAgInSc1d1	pařížský
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1378	[number]	k4	1378
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
kapitulou	kapitula	k1gFnSc7	kapitula
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
a	a	k8xC	a
pařížskými	pařížský	k2eAgMnPc7d1	pařížský
měšťany	měšťan	k1gMnPc7	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1379	[number]	k4	1379
<g/>
–	–	k?	–
<g/>
1387	[number]	k4	1387
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dokončení	dokončení	k1gNnSc6	dokončení
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
Pont-Neuf	Pont-Neuf	k1gInSc1	Pont-Neuf
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
současným	současný	k2eAgInSc7d1	současný
Pont	Pont	k1gInSc1	Pont
Neuf	Neuf	k1gInSc1	Neuf
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dokončení	dokončení	k1gNnSc6	dokončení
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
mostě	most	k1gInSc6	most
postaveny	postaven	k2eAgInPc4d1	postaven
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1408	[number]	k4	1408
most	most	k1gInSc1	most
strhla	strhnout	k5eAaPmAgFnS	strhnout
povodeň	povodeň	k1gFnSc4	povodeň
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
během	během	k7c2	během
probíhající	probíhající	k2eAgFnSc2d1	probíhající
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
most	most	k1gInSc1	most
postaven	postavit	k5eAaPmNgInS	postavit
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1444	[number]	k4	1444
pařížský	pařížský	k2eAgInSc1d1	pařížský
parlament	parlament	k1gInSc1	parlament
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
část	část	k1gFnSc4	část
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
pokut	pokuta	k1gFnPc2	pokuta
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
zděný	zděný	k2eAgInSc1d1	zděný
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
projektovali	projektovat	k5eAaBmAgMnP	projektovat
Paul-Martin	Paul-Martin	k1gMnSc1	Paul-Martin
Gallocher	Gallochra	k1gFnPc2	Gallochra
de	de	k?	de
Lagalisserie	Lagalisserie	k1gFnSc2	Lagalisserie
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Vaudrey	Vaudrea	k1gFnSc2	Vaudrea
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
zděný	zděný	k2eAgInSc1d1	zděný
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
obloucích	oblouk	k1gInPc6	oblouk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
rozpětí	rozpětí	k1gNnSc1	rozpětí
je	být	k5eAaImIp3nS	být
17,20	[number]	k4	17,20
m.	m.	k?	m.
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
mostu	most	k1gInSc2	most
činí	činit	k5eAaImIp3nS	činit
62	[number]	k4	62
metry	metr	k1gInPc1	metr
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
vozovka	vozovka	k1gFnSc1	vozovka
18	[number]	k4	18
m	m	kA	m
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
chodníky	chodník	k1gInPc4	chodník
6	[number]	k4	6
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pont	Pont	k1gInSc1	Pont
Saint-Michel	Saint-Michel	k1gInSc1	Saint-Michel
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pont	Pont	k1gInSc1	Pont
Saint-Michel	Saint-Michlo	k1gNnPc2	Saint-Michlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Most	most	k1gInSc1	most
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
mostu	most	k1gInSc2	most
na	na	k7c6	na
Structurae	Structurae	k1gFnSc6	Structurae
</s>
