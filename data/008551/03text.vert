<p>
<s>
Leonhard	Leonhard	k1gMnSc1	Leonhard
Paul	Paul	k1gMnSc1	Paul
Euler	Euler	k1gMnSc1	Euler
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1707	[number]	k4	1707
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1783	[number]	k4	1783
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
průkopnický	průkopnický	k2eAgMnSc1d1	průkopnický
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
měsíční	měsíční	k2eAgInSc1d1	měsíční
kráter	kráter	k1gInSc1	kráter
Euler	Euler	k1gInSc1	Euler
<g/>
.	.	kIx.	.
<g/>
Euler	Euler	k1gInSc1	Euler
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
matematika	matematik	k1gMnSc4	matematik
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
matematiků	matematik	k1gMnPc2	matematik
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
plodným	plodný	k2eAgMnSc7d1	plodný
autorem	autor	k1gMnSc7	autor
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gInPc1	jeho
sebrané	sebraný	k2eAgInPc1d1	sebraný
spisy	spis	k1gInPc1	spis
čítají	čítat	k5eAaImIp3nP	čítat
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Eulerův	Eulerův	k2eAgInSc1d1	Eulerův
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
matematiku	matematika	k1gFnSc4	matematika
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
výrok	výrok	k1gInSc1	výrok
připisovaný	připisovaný	k2eAgInSc1d1	připisovaný
Pierru	Pierra	k1gFnSc4	Pierra
Simonu	Simona	k1gFnSc4	Simona
de	de	k?	de
Laplaceovi	Laplaceus	k1gMnSc3	Laplaceus
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čtěte	číst	k5eAaImRp2nP	číst
Eulera	Eulera	k1gFnSc1	Eulera
<g/>
,	,	kIx,	,
čtěte	číst	k5eAaImRp2nP	číst
Eulera	Eulera	k1gFnSc1	Eulera
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
učitel	učitel	k1gMnSc1	učitel
nás	my	k3xPp1nPc2	my
všech	všecek	k3xTgInPc2	všecek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Euler	Euler	k1gInSc1	Euler
provedl	provést	k5eAaPmAgInS	provést
mnoho	mnoho	k4c4	mnoho
objevů	objev	k1gInPc2	objev
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
diferenciálního	diferenciální	k2eAgInSc2d1	diferenciální
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
grafů	graf	k1gInPc2	graf
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
také	také	k9	také
spoustu	spousta	k1gFnSc4	spousta
nových	nový	k2eAgInPc2d1	nový
moderních	moderní	k2eAgInPc2d1	moderní
matematických	matematický	k2eAgInPc2d1	matematický
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
analýze	analýza	k1gFnSc6	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
proslulý	proslulý	k2eAgMnSc1d1	proslulý
svými	svůj	k3xOyFgFnPc7	svůj
pracemi	práce	k1gFnPc7	práce
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
optice	optika	k1gFnSc3	optika
a	a	k8xC	a
astronomii	astronomie	k1gFnSc3	astronomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eulerův	Eulerův	k2eAgInSc1d1	Eulerův
portrét	portrét	k1gInSc1	portrét
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c4	na
desetifranku	desetifranka	k1gFnSc4	desetifranka
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
bankovky	bankovka	k1gFnSc2	bankovka
a	a	k8xC	a
na	na	k7c6	na
švýcarských	švýcarský	k2eAgFnPc6d1	švýcarská
<g/>
,	,	kIx,	,
ruských	ruský	k2eAgFnPc6d1	ruská
a	a	k8xC	a
německých	německý	k2eAgFnPc6d1	německá
poštovních	poštovní	k2eAgFnPc6d1	poštovní
známkách	známka	k1gFnPc6	známka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
asteroid	asteroid	k1gInSc1	asteroid
2002	[number]	k4	2002
Euler	Eulra	k1gFnPc2	Eulra
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
připomínán	připomínat	k5eAaImNgInS	připomínat
v	v	k7c6	v
luteránském	luteránský	k2eAgInSc6d1	luteránský
kalendáři	kalendář	k1gInSc6	kalendář
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Euler	Euler	k1gMnSc1	Euler
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Basileji	Basilej	k1gFnSc6	Basilej
Paulu	Paula	k1gFnSc4	Paula
Eulerovi	Euler	k1gMnSc3	Euler
<g/>
,	,	kIx,	,
pastorovi	pastor	k1gMnSc3	pastor
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
Margueritě	Marguerita	k1gFnSc6	Marguerita
Bruckerové	Bruckerová	k1gFnSc2	Bruckerová
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
mladší	mladý	k2eAgFnPc4d2	mladší
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Annu	Anna	k1gFnSc4	Anna
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
Magdalenu	Magdalena	k1gFnSc4	Magdalena
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Leonhardově	Leonhardův	k2eAgNnSc6d1	Leonhardův
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
z	z	k7c2	z
Basileje	Basilej	k1gFnSc2	Basilej
do	do	k7c2	do
Riehenu	Riehen	k1gInSc2	Riehen
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
vynikal	vynikat	k5eAaImAgMnS	vynikat
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Mladého	mladý	k2eAgMnSc4d1	mladý
Eulera	Euler	k1gMnSc4	Euler
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
přítel	přítel	k1gMnSc1	přítel
jeho	jeho	k3xOp3gMnPc4	jeho
otce	otec	k1gMnPc4	otec
<g/>
,	,	kIx,	,
Johann	Johann	k1gInSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předního	přední	k2eAgMnSc4d1	přední
evropského	evropský	k2eAgMnSc4d1	evropský
matematika	matematik	k1gMnSc4	matematik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1720	[number]	k4	1720
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Euler	Euler	k1gInSc1	Euler
na	na	k7c4	na
Basilejskou	basilejský	k2eAgFnSc4d1	Basilejská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dostával	dostávat	k5eAaImAgMnS	dostávat
od	od	k7c2	od
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
objevil	objevit	k5eAaPmAgMnS	objevit
talent	talent	k1gInSc4	talent
svého	svůj	k3xOyFgMnSc2	svůj
žáka	žák	k1gMnSc2	žák
<g/>
,	,	kIx,	,
sobotní	sobotní	k2eAgFnSc1d1	sobotní
lekce	lekce	k1gFnSc1	lekce
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gMnSc1	Euler
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
zapsal	zapsat	k5eAaPmAgMnS	zapsat
teologické	teologický	k2eAgInPc4d1	teologický
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
,	,	kIx,	,
latinu	latina	k1gFnSc4	latina
a	a	k8xC	a
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
fenomenální	fenomenální	k2eAgFnSc3d1	fenomenální
paměti	paměť	k1gFnSc3	paměť
dokázal	dokázat	k5eAaPmAgInS	dokázat
navíc	navíc	k6eAd1	navíc
studovat	studovat	k5eAaImF	studovat
i	i	k9	i
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
medicínu	medicína	k1gFnSc4	medicína
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
složil	složit	k5eAaPmAgMnS	složit
Euler	Euler	k1gMnSc1	Euler
doktorské	doktorský	k2eAgFnSc2d1	doktorská
zkoušky	zkouška	k1gFnSc2	zkouška
prací	práce	k1gFnPc2	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
rychlosti	rychlost	k1gFnSc2	rychlost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
účastnil	účastnit	k5eAaImAgMnS	účastnit
každoroční	každoroční	k2eAgFnSc2d1	každoroční
soutěže	soutěž	k1gFnSc2	soutěž
francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc4	ten
rok	rok	k1gInSc4	rok
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
najít	najít	k5eAaPmF	najít
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
způsob	způsob	k1gInSc4	způsob
umístění	umístění	k1gNnSc4	umístění
stěžně	stěžeň	k1gInSc2	stěžeň
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
hned	hned	k6eAd1	hned
za	za	k7c7	za
Pierrem	Pierr	k1gMnSc7	Pierr
Bouguerem	Bouguer	k1gMnSc7	Bouguer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
otec	otec	k1gMnSc1	otec
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
pozdější	pozdní	k2eAgFnSc6d2	pozdější
kariéře	kariéra	k1gFnSc6	kariéra
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Euler	Euler	k1gMnSc1	Euler
tuto	tento	k3xDgFnSc4	tento
každoroční	každoroční	k2eAgFnSc4d1	každoroční
soutěž	soutěž	k1gFnSc4	soutěž
dvanáctkrát	dvanáctkrát	k6eAd1	dvanáctkrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracovali	pracovat	k5eAaImAgMnP	pracovat
dva	dva	k4xCgInPc4	dva
Bernoulliho	Bernoulliha	k1gFnSc5	Bernoulliha
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
a	a	k8xC	a
Nicolas	Nicolas	k1gMnSc1	Nicolas
<g/>
,	,	kIx,	,
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1726	[number]	k4	1726
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
stráveném	strávený	k2eAgInSc6d1	strávený
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Nicolas	Nicolas	k1gMnSc1	Nicolas
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zánět	zánět	k1gInSc4	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
Daniel	Daniel	k1gMnSc1	Daniel
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
matematicko-fyzikální	matematickoyzikální	k2eAgFnSc6d1	matematicko-fyzikální
katedře	katedra	k1gFnSc6	katedra
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Eulera	Euler	k1gMnSc4	Euler
na	na	k7c4	na
post	post	k1gInSc4	post
fyziologa	fyziolog	k1gMnSc2	fyziolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1726	[number]	k4	1726
Euler	Euler	k1gMnSc1	Euler
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpozdil	zpozdit	k5eAaPmAgMnS	zpozdit
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
neúspěšné	úspěšný	k2eNgFnSc3d1	neúspěšná
žádosti	žádost	k1gFnSc3	žádost
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1727	[number]	k4	1727
přijel	přijet	k5eAaPmAgMnS	přijet
Euler	Euler	k1gMnSc1	Euler
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
ze	z	k7c2	z
svého	své	k1gNnSc2	své
nižšího	nízký	k2eAgInSc2d2	nižší
postu	post	k1gInSc2	post
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
katedře	katedra	k1gFnSc6	katedra
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
pozice	pozice	k1gFnSc2	pozice
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Danielem	Daniel	k1gMnSc7	Daniel
Bernoullim	Bernoullima	k1gFnPc2	Bernoullima
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gMnSc1	Euler
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
naučil	naučit	k5eAaPmAgMnS	naučit
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
zapadl	zapadnout	k5eAaPmAgMnS	zapadnout
do	do	k7c2	do
života	život	k1gInSc2	život
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
také	také	k9	také
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
jako	jako	k9	jako
zdravotník	zdravotník	k1gMnSc1	zdravotník
u	u	k7c2	u
ruského	ruský	k2eAgNnSc2d1	ruské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Universita	universita	k1gFnSc1	universita
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Petrem	Petr	k1gMnSc7	Petr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pozvedla	pozvednout	k5eAaPmAgFnS	pozvednout
vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
ho	on	k3xPp3gMnSc4	on
zemím	zem	k1gFnPc3	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
byla	být	k5eAaImAgFnS	být
štědře	štědro	k6eAd1	štědro
financována	financovat	k5eAaBmNgFnS	financovat
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
vyučování	vyučování	k1gNnSc4	vyučování
byly	být	k5eAaImAgFnP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
investovala	investovat	k5eAaBmAgFnS	investovat
do	do	k7c2	do
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
jak	jak	k6eAd1	jak
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
svobodu	svoboda	k1gFnSc4	svoboda
pro	pro	k7c4	pro
bádání	bádání	k1gNnSc4	bádání
o	o	k7c6	o
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
vědce	vědec	k1gMnPc4	vědec
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
Euler	Euler	k1gMnSc1	Euler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mecenáška	mecenáška	k1gFnSc1	mecenáška
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
I.	I.	kA	I.
Alexejevna	Alexejevna	k1gFnSc1	Alexejevna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
progresivní	progresivní	k2eAgFnSc6d1	progresivní
vládě	vláda	k1gFnSc6	vláda
svého	svůj	k3xOyFgMnSc2	svůj
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c4	v
den	den	k1gInSc4	den
Eulerova	Eulerův	k2eAgInSc2d1	Eulerův
příjezdu	příjezd	k1gInSc2	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
tím	ten	k3xDgNnSc7	ten
získala	získat	k5eAaPmAgFnS	získat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
vzestupem	vzestup	k1gInSc7	vzestup
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1	dvanáctiletý
Petra	Petr	k1gMnSc2	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
nedůvěřivá	důvěřivý	k2eNgFnSc1d1	nedůvěřivá
k	k	k7c3	k
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
vědcům	vědec	k1gMnPc3	vědec
působícím	působící	k2eAgInSc6d1	působící
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
snížila	snížit	k5eAaPmAgFnS	snížit
financování	financování	k1gNnSc4	financování
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
další	další	k2eAgFnPc4d1	další
potíže	potíž	k1gFnPc4	potíž
Eulerovi	Euler	k1gMnSc3	Euler
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
kolegům	kolega	k1gMnPc3	kolega
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
po	po	k7c6	po
Petrově	Petrův	k2eAgFnSc6d1	Petrova
smrti	smrt	k1gFnSc6	smrt
mírně	mírně	k6eAd1	mírně
zlepšily	zlepšit	k5eAaPmAgFnP	zlepšit
a	a	k8xC	a
Euler	Euler	k1gMnSc1	Euler
změnil	změnit	k5eAaPmAgMnS	změnit
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1731	[number]	k4	1731
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Daniel	Daniel	k1gMnSc1	Daniel
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
otráven	otrávit	k5eAaPmNgInS	otrávit
cenzurou	cenzura	k1gFnSc7	cenzura
a	a	k8xC	a
nepřátelstvím	nepřátelství	k1gNnSc7	nepřátelství
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
čelil	čelit	k5eAaImAgMnS	čelit
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gInSc1	Euler
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
katedry	katedra	k1gFnSc2	katedra
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1734	[number]	k4	1734
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Katharinu	Katharin	k1gInSc6	Katharin
Gsellovou	Gsellová	k1gFnSc4	Gsellová
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
malíře	malíř	k1gMnSc2	malíř
z	z	k7c2	z
akademického	akademický	k2eAgNnSc2d1	akademické
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
pár	pár	k1gInSc1	pár
si	se	k3xPyFc3	se
koupil	koupit	k5eAaPmAgInS	koupit
dům	dům	k1gInSc1	dům
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Něvy	Něva	k1gFnSc2	Něva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
třinácti	třináct	k4xCc2	třináct
dětí	dítě	k1gFnPc2	dítě
jen	jen	k6eAd1	jen
pět	pět	k4xCc1	pět
přežilo	přežít	k5eAaPmAgNnS	přežít
dětství	dětství	k1gNnSc1	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlín	Berlín	k1gInSc1	Berlín
===	===	k?	===
</s>
</p>
<p>
<s>
Znepokojen	znepokojen	k2eAgInSc1d1	znepokojen
pokračujícími	pokračující	k2eAgInPc7d1	pokračující
nepokoji	nepokoj	k1gInPc7	nepokoj
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
Euler	Euler	k1gMnSc1	Euler
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1741	[number]	k4	1741
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velikým	veliký	k2eAgInSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
strávil	strávit	k5eAaPmAgInS	strávit
25	[number]	k4	25
let	léto	k1gNnPc2	léto
a	a	k8xC	a
napsal	napsat	k5eAaPmAgInS	napsat
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
380	[number]	k4	380
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
proslavily	proslavit	k5eAaPmAgFnP	proslavit
<g/>
:	:	kIx,	:
Introductio	Introductio	k6eAd1	Introductio
in	in	k?	in
analysin	analysin	k1gMnSc1	analysin
infinitorum	infinitorum	k1gNnSc1	infinitorum
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
<g/>
,	,	kIx,	,
a	a	k8xC	a
Institutiones	Institutiones	k1gMnSc1	Institutiones
calculi	calcule	k1gFnSc4	calcule
differentialis	differentialis	k1gFnSc2	differentialis
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
o	o	k7c6	o
diferenciálním	diferenciální	k2eAgInSc6d1	diferenciální
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Euler	Euler	k1gMnSc1	Euler
byl	být	k5eAaImAgMnS	být
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
princeznu	princezna	k1gFnSc4	princezna
z	z	k7c2	z
Anhalt-Dessau	Anhalt-Dessaus	k1gInSc2	Anhalt-Dessaus
<g/>
,	,	kIx,	,
Fridrichovu	Fridrichův	k2eAgFnSc4d1	Fridrichova
neteř	neteř	k1gFnSc4	neteř
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gInSc1	Euler
jí	on	k3xPp3gFnSc3	on
napsal	napsat	k5eAaBmAgMnS	napsat
přes	přes	k7c4	přes
200	[number]	k4	200
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
vydány	vydat	k5eAaPmNgInP	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Eulerovy	Eulerův	k2eAgInPc4d1	Eulerův
dopisy	dopis	k1gInPc4	dopis
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
témata	téma	k1gNnPc4	téma
přirozené	přirozený	k2eAgFnSc2d1	přirozená
filosofie	filosofie	k1gFnSc2	filosofie
psané	psaný	k2eAgFnSc2d1	psaná
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
princeznu	princezna	k1gFnSc4	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
Eulerovo	Eulerův	k2eAgNnSc4d1	Eulerovo
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
matematice	matematika	k1gFnSc6	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Nabízejí	nabízet	k5eAaImIp3nP	nabízet
také	také	k9	také
cenný	cenný	k2eAgInSc4d1	cenný
náhled	náhled	k1gInSc4	náhled
na	na	k7c4	na
Eulerovu	Eulerův	k2eAgFnSc4d1	Eulerova
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
čtena	číst	k5eAaImNgFnS	číst
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
jiná	jiný	k2eAgFnSc1d1	jiná
jeho	jeho	k3xOp3gFnSc1	jeho
matematická	matematický	k2eAgFnSc1d1	matematická
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
Dopisů	dopis	k1gInPc2	dopis
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
Eulerovu	Eulerův	k2eAgFnSc4d1	Eulerova
schopnost	schopnost	k1gFnSc4	schopnost
efektivně	efektivně	k6eAd1	efektivně
sdělit	sdělit	k5eAaPmF	sdělit
vědecké	vědecký	k2eAgInPc4d1	vědecký
poznatky	poznatek	k1gInPc4	poznatek
laické	laický	k2eAgFnSc2d1	laická
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
byl	být	k5eAaImAgInS	být
Euler	Euler	k1gInSc1	Euler
navzdory	navzdory	k7c3	navzdory
svým	svůj	k3xOyFgFnPc3	svůj
velkým	velký	k2eAgFnPc3d1	velká
zásluhám	zásluha	k1gFnPc3	zásluha
o	o	k7c4	o
prestiž	prestiž	k1gFnSc4	prestiž
akademie	akademie	k1gFnSc2	akademie
nucen	nutit	k5eAaImNgMnS	nutit
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Částečným	částečný	k2eAgInSc7d1	částečný
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
smýšlel	smýšlet	k5eAaImAgMnS	smýšlet
o	o	k7c6	o
Eulerovi	Euler	k1gMnSc6	Euler
jako	jako	k8xS	jako
o	o	k7c6	o
obyčejném	obyčejný	k2eAgMnSc6d1	obyčejný
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
okruhem	okruh	k1gInSc7	okruh
francouzských	francouzský	k2eAgMnPc2d1	francouzský
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
král	král	k1gMnSc1	král
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
přivedl	přivést	k5eAaPmAgMnS	přivést
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gMnSc1	Euler
<g/>
,	,	kIx,	,
zbožný	zbožný	k2eAgMnSc1d1	zbožný
a	a	k8xC	a
pracovitý	pracovitý	k2eAgMnSc1d1	pracovitý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
přímým	přímý	k2eAgInSc7d1	přímý
opakem	opak	k1gInSc7	opak
Voltaira	Voltair	k1gInSc2	Voltair
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gMnSc1	Euler
nebyl	být	k5eNaImAgMnS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
řečník	řečník	k1gMnSc1	řečník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouštěl	pouštět	k5eAaImAgMnS	pouštět
se	se	k3xPyFc4	se
do	do	k7c2	do
diskuzí	diskuze	k1gFnPc2	diskuze
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
mnoho	mnoho	k6eAd1	mnoho
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
Voltairových	Voltairův	k2eAgInPc2d1	Voltairův
vtipů	vtip	k1gInPc2	vtip
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
také	také	k9	také
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
zklamání	zklamání	k1gNnSc4	zklamání
nad	nad	k7c7	nad
Eulerovými	Eulerův	k2eAgFnPc7d1	Eulerova
praktickými	praktický	k2eAgFnPc7d1	praktická
technickými	technický	k2eAgFnPc7d1	technická
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
vodotrysk	vodotrysk	k1gInSc1	vodotrysk
<g/>
:	:	kIx,	:
Euler	Euler	k1gMnSc1	Euler
vypočítal	vypočítat	k5eAaPmAgMnS	vypočítat
sílu	síla	k1gFnSc4	síla
kol	kola	k1gFnPc2	kola
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
k	k	k7c3	k
čerpání	čerpání	k1gNnSc3	čerpání
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
odváděna	odváděn	k2eAgFnSc1d1	odváděna
kanály	kanál	k1gInPc7	kanál
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
tryskat	tryskat	k5eAaImF	tryskat
v	v	k7c6	v
Sanssouci	Sanssouci	k1gNnSc6	Sanssouci
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
bylo	být	k5eAaImAgNnS	být
zkonstruováno	zkonstruovat	k5eAaPmNgNnS	zkonstruovat
s	s	k7c7	s
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
větší	veliký	k2eAgInPc4d2	veliký
než	než	k8xS	než
padesát	padesát	k4xCc4	padesát
kroků	krok	k1gInPc2	krok
od	od	k7c2	od
rezervoáru	rezervoár	k1gInSc2	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
Nicotná	nicotný	k2eAgFnSc1d1	nicotná
ješitnost	ješitnost	k1gFnSc1	ješitnost
<g/>
!	!	kIx.	!
</s>
<s>
Nicotná	nicotný	k2eAgFnSc1d1	nicotná
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Zhoršení	zhoršení	k1gNnSc2	zhoršení
zraku	zrak	k1gInSc2	zrak
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
Eulerovi	Euler	k1gMnSc3	Euler
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
zrak	zrak	k1gInSc1	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
téměř	téměř	k6eAd1	téměř
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
oslepl	oslepnout	k5eAaPmAgInS	oslepnout
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
na	na	k7c4	na
pravé	pravý	k2eAgNnSc4d1	pravé
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Euler	Euler	k1gMnSc1	Euler
svaloval	svalovat	k5eAaImAgMnS	svalovat
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
špatný	špatný	k2eAgInSc4d1	špatný
zrak	zrak	k1gInSc4	zrak
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
kartografa	kartograf	k1gMnSc2	kartograf
v	v	k7c6	v
Petrohradské	petrohradský	k2eAgFnSc6d1	Petrohradská
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Fridrich	Fridrich	k1gMnSc1	Fridrich
nazýval	nazývat	k5eAaImAgMnS	nazývat
kyklopem	kyklop	k1gMnSc7	kyklop
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
nalezen	nalézt	k5eAaBmNgMnS	nalézt
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
oku	oko	k1gNnSc6	oko
šedý	šedý	k2eAgInSc4d1	šedý
zákal	zákal	k1gInSc4	zákal
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gInSc4	on
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
oslepilo	oslepit	k5eAaPmAgNnS	oslepit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
slepota	slepota	k1gFnSc1	slepota
neměla	mít	k5eNaImAgFnS	mít
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
produktivitu	produktivita	k1gFnSc4	produktivita
<g/>
,	,	kIx,	,
kompenzuje	kompenzovat	k5eAaBmIp3nS	kompenzovat
ji	on	k3xPp3gFnSc4	on
svými	svůj	k3xOyFgFnPc7	svůj
počtářskými	počtářský	k2eAgFnPc7d1	počtářská
schopnostmi	schopnost	k1gFnPc7	schopnost
a	a	k8xC	a
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
dokázal	dokázat	k5eAaPmAgMnS	dokázat
bez	bez	k7c2	bez
zaváhání	zaváhání	k1gNnSc2	zaváhání
zopakovat	zopakovat	k5eAaPmF	zopakovat
celou	celý	k2eAgFnSc4d1	celá
Aeneidu	Aeneida	k1gFnSc4	Aeneida
od	od	k7c2	od
Vergilia	Vergilius	k1gMnSc2	Vergilius
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
pamatoval	pamatovat	k5eAaImAgMnS	pamatovat
který	který	k3yRgInSc4	který
řádek	řádek	k1gInSc4	řádek
byl	být	k5eAaImAgMnS	být
první	první	k4xOgInSc4	první
a	a	k8xC	a
který	který	k3yQgMnSc1	který
poslední	poslední	k2eAgMnSc1d1	poslední
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
výkonnost	výkonnost	k1gFnSc1	výkonnost
dokonce	dokonce	k9	dokonce
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
písaři	písař	k1gMnSc3	písař
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
produkoval	produkovat	k5eAaImAgMnS	produkovat
Euler	Euler	k1gMnSc1	Euler
jeden	jeden	k4xCgMnSc1	jeden
matematický	matematický	k2eAgInSc1d1	matematický
list	list	k1gInSc1	list
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Veliké	veliký	k2eAgFnSc2d1	veliká
se	se	k3xPyFc4	se
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
opět	opět	k6eAd1	opět
prudce	prudko	k6eAd1	prudko
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Euler	Euler	k1gMnSc1	Euler
přijal	přijmout	k5eAaPmAgMnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
na	na	k7c4	na
Petrohradskou	petrohradský	k2eAgFnSc4d1	Petrohradská
akademii	akademie	k1gFnSc4	akademie
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
tam	tam	k6eAd1	tam
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
postihla	postihnout	k5eAaPmAgFnS	postihnout
Eulera	Eulera	k1gFnSc1	Eulera
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
požár	požár	k1gInSc1	požár
ho	on	k3xPp3gMnSc4	on
stál	stát	k5eAaImAgInS	stát
dům	dům	k1gInSc1	dům
a	a	k8xC	a
málem	málem	k6eAd1	málem
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
i	i	k9	i
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
pak	pak	k6eAd1	pak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
byl	být	k5eAaImAgInS	být
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
manželčině	manželčin	k2eAgFnSc6d1	manželčina
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Euler	Euler	k1gMnSc1	Euler
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
nevlastní	vlastní	k2eNgFnSc7d1	nevlastní
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1783	[number]	k4	1783
Euler	Eulero	k1gNnPc2	Eulero
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
se	se	k3xPyFc4	se
svoji	svůj	k3xOyFgMnPc1	svůj
ženou	hnát	k5eAaImIp3nP	hnát
na	na	k7c6	na
Smolenském	Smolenský	k2eAgInSc6d1	Smolenský
luteránském	luteránský	k2eAgInSc6d1	luteránský
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Děkabristů	děkabrista	k1gMnPc2	děkabrista
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
padesátého	padesátý	k4xOgNnSc2	padesátý
výročí	výročí	k1gNnPc2	výročí
narození	narození	k1gNnSc2	narození
byly	být	k5eAaImAgInP	být
Eulerovy	Eulerův	k2eAgInPc1d1	Eulerův
ostatky	ostatek	k1gInPc1	ostatek
přeneseny	přenesen	k2eAgInPc1d1	přenesen
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
na	na	k7c4	na
Lazarevský	Lazarevský	k2eAgInSc4d1	Lazarevský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spočívají	spočívat	k5eAaImIp3nP	spočívat
nedaleko	nedaleko	k7c2	nedaleko
pomníku	pomník	k1gInSc2	pomník
M.	M.	kA	M.
V.	V.	kA	V.
Lomonosova	Lomonosův	k2eAgInSc2d1	Lomonosův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
dílo	dílo	k1gNnSc1	dílo
nemá	mít	k5eNaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
865	[number]	k4	865
prací	práce	k1gFnPc2	práce
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
473	[number]	k4	473
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
pojednání	pojednání	k1gNnPc2	pojednání
po	po	k7c4	po
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
přesným	přesný	k2eAgInSc7d1	přesný
vyjadřováním	vyjadřování	k1gNnSc7	vyjadřování
a	a	k8xC	a
přehlednou	přehledný	k2eAgFnSc7d1	přehledná
symbolikou	symbolika	k1gFnSc7	symbolika
-	-	kIx~	-
dnešní	dnešní	k2eAgInSc1d1	dnešní
způsob	způsob	k1gInSc1	způsob
značení	značení	k1gNnSc2	značení
matematických	matematický	k2eAgInPc2d1	matematický
pojmů	pojem	k1gInPc2	pojem
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
Eulerův	Eulerův	k2eAgInSc1d1	Eulerův
<g/>
.	.	kIx.	.
</s>
<s>
Eulerův	Eulerův	k2eAgInSc1d1	Eulerův
význam	význam	k1gInSc1	význam
snad	snad	k9	snad
nejlépe	dobře	k6eAd3	dobře
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
jiný	jiný	k2eAgMnSc1d1	jiný
velikán	velikán	k1gMnSc1	velikán
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gauss	gauss	k1gInSc1	gauss
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Studium	studium	k1gNnSc1	studium
Eulerova	Eulerův	k2eAgNnSc2d1	Eulerovo
díla	dílo	k1gNnSc2	dílo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
školou	škola	k1gFnSc7	škola
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
oblasti	oblast	k1gFnPc4	oblast
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
je	on	k3xPp3gNnSc4	on
nic	nic	k3yNnSc1	nic
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
nemění	měnit	k5eNaImIp3nS	měnit
ani	ani	k9	ani
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
Euler	Euler	k1gMnSc1	Euler
nevyhnul	vyhnout	k5eNaPmAgMnS	vyhnout
některým	některý	k3yIgNnPc3	některý
chybám	chyba	k1gFnPc3	chyba
plynoucím	plynoucí	k2eAgFnPc3d1	plynoucí
z	z	k7c2	z
nedostatečné	nedostatečná	k1gFnSc2	nedostatečná
vyjasněnosti	vyjasněnost	k1gFnSc2	vyjasněnost
matematických	matematický	k2eAgInPc2d1	matematický
pojmů	pojem	k1gInPc2	pojem
-	-	kIx~	-
např.	např.	kA	např.
pojem	pojem	k1gInSc4	pojem
konvergence	konvergence	k1gFnSc2	konvergence
u	u	k7c2	u
nekonečných	konečný	k2eNgFnPc2d1	nekonečná
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
teorie	teorie	k1gFnSc2	teorie
grafů	graf	k1gInPc2	graf
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
roku	rok	k1gInSc2	rok
1736	[number]	k4	1736
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lze	lze	k6eAd1	lze
projít	projít	k5eAaPmF	projít
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
mostů	most	k1gInPc2	most
v	v	k7c6	v
Königsbergu	Königsberg	k1gInSc6	Königsberg
-	-	kIx~	-
Královci	Královec	k1gInSc6	Královec
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
právě	právě	k9	právě
jednou	jednou	k6eAd1	jednou
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
výchozího	výchozí	k2eAgNnSc2d1	výchozí
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
teorii	teorie	k1gFnSc6	teorie
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pojmu	pojem	k1gInSc3	pojem
eulerovský	eulerovský	k2eAgInSc1d1	eulerovský
graf	graf	k1gInSc1	graf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
metoda	metoda	k1gFnSc1	metoda
variace	variace	k1gFnSc2	variace
konstant	konstanta	k1gFnPc2	konstanta
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
(	(	kIx(	(
<g/>
neprávem	neprávo	k1gNnSc7	neprávo
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
metoda	metoda	k1gFnSc1	metoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
použil	použít	k5eAaPmAgMnS	použít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1740	[number]	k4	1740
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
imaginární	imaginární	k2eAgNnSc4d1	imaginární
číslo	číslo	k1gNnSc4	číslo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
odmocninu	odmocnina	k1gFnSc4	odmocnina
ze	z	k7c2	z
záporného	záporný	k2eAgNnSc2d1	záporné
čísla	číslo	k1gNnSc2	číslo
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Algebra	algebra	k1gFnSc1	algebra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
dvojrozměrný	dvojrozměrný	k2eAgInSc1d1	dvojrozměrný
integrál	integrál	k1gInSc1	integrál
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Eulera	Euler	k1gMnSc2	Euler
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
nyní	nyní	k6eAd1	nyní
používané	používaný	k2eAgNnSc1d1	používané
označení	označení	k1gNnSc1	označení
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
všeobecně	všeobecně	k6eAd1	všeobecně
uznávané	uznávaný	k2eAgFnSc3d1	uznávaná
autoritě	autorita	k1gFnSc3	autorita
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
symbolika	symbolika	k1gFnSc1	symbolika
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
infinitezimálního	infinitezimální	k2eAgInSc2d1	infinitezimální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Euler	Euler	k1gMnSc1	Euler
se	se	k3xPyFc4	se
nezabýval	zabývat	k5eNaImAgMnS	zabývat
jen	jen	k9	jen
teorií	teorie	k1gFnSc7	teorie
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
její	její	k3xOp3gFnSc7	její
aplikací	aplikace	k1gFnSc7	aplikace
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
kartografii	kartografie	k1gFnSc6	kartografie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
např.	např.	kA	např.
i	i	k8xC	i
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
mechaniky	mechanika	k1gFnSc2	mechanika
tuhých	tuhý	k2eAgNnPc2d1	tuhé
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
hydrodynamiky	hydrodynamika	k1gFnSc2	hydrodynamika
(	(	kIx(	(
<g/>
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
např.	např.	kA	např.
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
rovnice	rovnice	k1gFnSc2	rovnice
popisující	popisující	k2eAgInSc1d1	popisující
pohyb	pohyb	k1gInSc1	pohyb
ideální	ideální	k2eAgFnSc2d1	ideální
nevazké	vazký	k2eNgFnSc2d1	vazký
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
Eulerovu	Eulerův	k2eAgFnSc4d1	Eulerova
metodu	metoda	k1gFnSc4	metoda
numerického	numerický	k2eAgNnSc2d1	numerické
řešení	řešení	k1gNnSc2	řešení
obyčejných	obyčejný	k2eAgFnPc2d1	obyčejná
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Leonhard	Leonhard	k1gMnSc1	Leonhard
Euler	Euler	k1gMnSc1	Euler
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc4	sedm
mostů	most	k1gInPc2	most
města	město	k1gNnSc2	město
Královce	Královec	k1gInSc2	Královec
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pojmů	pojem	k1gInPc2	pojem
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
po	po	k7c6	po
Leonhardu	Leonhard	k1gInSc6	Leonhard
Eulerovi	Euler	k1gMnSc3	Euler
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Leonhard	Leonharda	k1gFnPc2	Leonharda
Euler	Eulero	k1gNnPc2	Eulero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Leonhard	Leonhard	k1gMnSc1	Leonhard
Euler	Euler	k1gMnSc1	Euler
</s>
</p>
