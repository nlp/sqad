<s>
Euler	Euler	k1gMnSc1	Euler
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Basileji	Basilej	k1gFnSc6	Basilej
Paulu	Paula	k1gFnSc4	Paula
Eulerovi	Euler	k1gMnSc3	Euler
<g/>
,	,	kIx,	,
pastorovi	pastor	k1gMnSc3	pastor
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
Margueritě	Marguerita	k1gFnSc6	Marguerita
Bruckerové	Bruckerová	k1gFnSc2	Bruckerová
<g/>
.	.	kIx.	.
</s>
