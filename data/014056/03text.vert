<s>
Fermium	fermium	k1gNnSc1
</s>
<s>
Fermium	fermium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
12	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
257	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fm	Fm	kA
</s>
<s>
100	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Fermium	fermium	k1gNnSc1
<g/>
,	,	kIx,
Fm	Fm	k1gFnSc1
<g/>
,	,	kIx,
100	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Fermium	fermium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-72-4	7440-72-4	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
257,092	257,092	k4
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
Fm	Fm	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
115	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Fm	Fm	k1gFnSc1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
97	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Fm	Fm	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
84	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
12	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,68	6,68	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
12,51	12,51	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
22,5	22,5	k4
eV	eV	k?
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1527	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
800,15	800,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Er	Er	k?
<g/>
⋏	⋏	k?
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
≺	≺	k?
<g/>
Fm	Fm	k1gMnSc2
<g/>
≻	≻	k?
Mendelevium	Mendelevium	k1gNnSc1
</s>
<s>
Fermium	fermium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Fm	Fm	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dvanáctým	dvanáctý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
osmým	osmý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
významném	významný	k2eAgNnSc6d1
jaderném	jaderný	k2eAgNnSc6d1
fyzikovi	fyzik	k1gMnSc3
Enricu	Enricum	k1gNnSc3
Fermim	Fermim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Fermium	fermium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
z	z	k7c2
řady	řada	k1gFnSc2
transuranů	transuran	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fyzikálně-chemické	Fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Fermium	fermium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
doposud	doposud	k6eAd1
nebyl	být	k5eNaImAgInS
izolován	izolovat	k5eAaBmNgInS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS
α	α	k?
a	a	k8xC
γ	γ	k?
záření	záření	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
silným	silný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
neutronů	neutron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
za	za	k7c4
dodržování	dodržování	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
radioaktivními	radioaktivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
jeho	jeho	k3xOp3gFnPc6
sloučeninách	sloučenina	k1gFnPc6
a	a	k8xC
jejich	jejich	k3xOp3gNnSc6
chemickém	chemický	k2eAgNnSc6d1
chování	chování	k1gNnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Enrico	Enrico	k6eAd1
Fermi	Fer	k1gFnPc7
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
identifikoval	identifikovat	k5eAaBmAgMnS
fermium	fermium	k1gNnSc4
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1952	#num#	k4
na	na	k7c6
kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výchozím	výchozí	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
byl	být	k5eAaImAgInS
spad	spad	k1gInSc1
po	po	k7c6
výbuchu	výbuch	k1gInSc6
nukleární	nukleární	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
testované	testovaný	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
„	„	k?
<g/>
Operace	operace	k1gFnSc2
Ivy	Iva	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
listopadu	listopad	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikovaným	identifikovaný	k2eAgInSc7d1
izotopem	izotop	k1gInSc7
bylo	být	k5eAaImAgNnS
255	#num#	k4
<g/>
Fm	Fm	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
jádra	jádro	k1gNnSc2
238U	238U	k4
postupným	postupný	k2eAgNnSc7d1
pohlcením	pohlcení	k1gNnSc7
17	#num#	k4
neutronů	neutron	k1gInPc2
a	a	k8xC
následnými	následný	k2eAgFnPc7d1
osmi	osm	k4xCc2
β	β	k?
přeměnami	přeměna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
objev	objev	k1gInSc1
byl	být	k5eAaImAgInS
utajován	utajovat	k5eAaImNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
vzhledem	vzhledem	k7c3
k	k	k7c3
probíhající	probíhající	k2eAgFnSc3d1
studené	studený	k2eAgFnSc3d1
válce	válka	k1gFnSc3
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
jadernými	jaderný	k2eAgFnPc7d1
velmocemi	velmoc	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
umělá	umělý	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
fermia	fermium	k1gNnSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
v	v	k7c6
Nobelově	Nobelův	k2eAgInSc6d1
fyzikálním	fyzikální	k2eAgInSc6d1
institutu	institut	k1gInSc6
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
bombardováním	bombardování	k1gNnSc7
jader	jádro	k1gNnPc2
238U	238U	k4
jádry	jádro	k1gNnPc7
izotopu	izotop	k1gInSc2
kyslíku	kyslík	k1gInSc2
16	#num#	k4
<g/>
O.	O.	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
23892	#num#	k4
U	u	k7c2
+	+	kIx~
168	#num#	k4
O	o	k7c6
→	→	k?
250100	#num#	k4
Fm	Fm	k1gFnPc2
+	+	kIx~
4	#num#	k4
10	#num#	k4
n	n	k0
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
20	#num#	k4
izotopů	izotop	k1gInPc2
fermia	fermium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
nejstabilnější	stabilní	k2eAgNnPc1d3
jsou	být	k5eAaImIp3nP
257	#num#	k4
<g/>
Fm	Fm	k1gFnPc1
s	s	k7c7
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
100,5	100,5	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
253	#num#	k4
<g/>
Fm	Fm	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
3	#num#	k4
dny	dna	k1gFnSc2
a	a	k8xC
252	#num#	k4
<g/>
Fm	Fm	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
25,39	25,39	k4
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ghiorso	Ghiorsa	k1gFnSc5
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
G.	G.	kA
<g/>
;	;	kIx,
Higgins	Higgins	k1gInSc1
<g/>
,	,	kIx,
G.	G.	kA
H.	H.	kA
;	;	kIx,
Seaborg	Seaborg	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
T.	T.	kA
<g/>
;	;	kIx,
Studier	Studier	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
H.	H.	kA
<g/>
;	;	kIx,
Fields	Fields	k1gInSc1
<g/>
,	,	kIx,
P.	P.	kA
R.	R.	kA
<g/>
;	;	kIx,
Fried	Fried	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
M.	M.	kA
<g/>
;	;	kIx,
Diamond	Diamond	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
H.	H.	kA
<g/>
;	;	kIx,
Mech	mech	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
F.	F.	kA
<g/>
;	;	kIx,
Pyle	pyl	k1gInSc5
<g/>
,	,	kIx,
G.	G.	kA
L.	L.	kA
<g/>
;	;	kIx,
Huizenga	Huizeng	k1gMnSc2
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
<g/>
;	;	kIx,
Hirsch	Hirsch	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
Manning	Manning	k1gInSc1
<g/>
,	,	kIx,
W.	W.	kA
M.	M.	kA
<g/>
;	;	kIx,
Browne	Brown	k1gMnSc5
<g/>
,	,	kIx,
C.	C.	kA
I.	I.	kA
<g/>
;	;	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
H.	H.	kA
L.	L.	kA
<g/>
;	;	kIx,
Spence	Spenec	k1gInPc1
<g/>
,	,	kIx,
R.	R.	kA
W.	W.	kA
New	New	k1gFnPc2
Elements	Elementsa	k1gFnPc2
Einsteinium	einsteinium	k1gNnSc1
and	and	k?
Fermium	fermium	k1gNnSc1
<g/>
,	,	kIx,
Atomic	Atomic	k1gMnSc1
Numbers	Numbers	k1gInSc4
99	#num#	k4
and	and	k?
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
1955	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
99	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1048	#num#	k4
<g/>
–	–	k?
<g/>
1049	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRev	PhysRev	k1gFnSc1
<g/>
.99	.99	k4
<g/>
.1048	.1048	k4
<g/>
.	.	kIx.
↑	↑	k?
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Einsteinium	einsteinium	k1gNnSc1
and	and	k?
Fermium	fermium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemical	Chemical	k1gFnSc1
and	and	k?
Engineering	Engineering	k1gInSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Atterling	Atterling	k1gInSc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
<g/>
,	,	kIx,
Forsling	Forsling	k1gInSc1
<g/>
,	,	kIx,
Wilhelm	Wilhelm	k1gInSc1
<g/>
;	;	kIx,
Holm	Holm	k1gInSc1
<g/>
,	,	kIx,
Lennart	Lennart	k1gInSc1
W.	W.	kA
<g/>
;	;	kIx,
Melander	Melander	k1gMnSc1
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
<g/>
;	;	kIx,
Å	Å	k1gInSc1
<g/>
,	,	kIx,
Björn	Björn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Element	element	k1gInSc1
100	#num#	k4
Produced	Produced	k1gInSc4
by	by	k9
Means	Means	k1gInSc4
of	of	k?
Cyclotron-Accelerated	Cyclotron-Accelerated	k1gInSc1
Oxygen	oxygen	k1gInSc1
Ions	Ions	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
1954	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
95	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
585	#num#	k4
<g/>
–	–	k?
<g/>
586	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRev	PhysRev	k1gFnSc1
<g/>
.95	.95	k4
<g/>
.585	.585	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
fermium	fermium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
fermium	fermium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4490995-0	4490995-0	k4
</s>
