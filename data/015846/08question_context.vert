<s>
Frankovka	frankovka	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
Fraňkovka	Fraňkovka	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
Fr	fr	k0
<g/>
,	,	kIx,
název	název	k1gInSc4
dle	dle	k7c2
VIVC	VIVC	kA
Blaufränkisch	Blaufränkisch	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starobylá	starobylý	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc2
vinifera	vinifer	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
červených	červený	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>