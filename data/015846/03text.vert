<s>
Frankovka	frankovka	k1gFnSc1
</s>
<s>
Hrozny	hrozen	k1gInPc1
odrůdy	odrůda	k1gFnSc2
Frankovka	frankovka	k1gFnSc1
v	v	k7c6
Burgenlandu	Burgenland	k1gInSc6
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Frankovka	frankovka	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
Fraňkovka	Fraňkovka	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
Fr	fr	k0
<g/>
,	,	kIx,
název	název	k1gInSc4
dle	dle	k7c2
VIVC	VIVC	kA
Blaufränkisch	Blaufränkisch	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
starobylá	starobylý	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc2
vinifera	vinifer	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
červených	červený	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
spontánního	spontánní	k2eAgMnSc4d1
křížence	kříženec	k1gMnSc4
odrůdy	odrůda	k1gFnSc2
Gouais	Gouais	k1gFnSc1
blanc	blanc	k1gFnSc1
(	(	kIx(
<g/>
Heunisch	Heunisch	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
doposud	doposud	k6eAd1
nezjištěnou	zjištěný	k2eNgFnSc7d1
odrůdou	odrůda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
odrůda	odrůda	k1gFnSc1
Frankovka	frankovka	k1gFnSc1
je	být	k5eAaImIp3nS
jednodomá	jednodomý	k2eAgFnSc1d1
dřevitá	dřevitý	k2eAgFnSc1d1
pnoucí	pnoucí	k2eAgFnSc1d1
liána	liána	k1gFnSc1
dorůstající	dorůstající	k2eAgFnSc1d1
v	v	k7c6
kultuře	kultura	k1gFnSc6
až	až	k9
několika	několik	k4yIc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmen	kmen	k1gInSc4
tloušťky	tloušťka	k1gFnSc2
až	až	k9
několik	několik	k4yIc1
centimetrů	centimetr	k1gInPc2
je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
světlou	světlý	k2eAgFnSc7d1
borkou	borka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
loupe	loupat	k5eAaImIp3nS
v	v	k7c6
pruzích	pruh	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Úponky	úponka	k1gFnPc1
révy	réva	k1gFnSc2
jsou	být	k5eAaImIp3nP
středně	středně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
umožňují	umožňovat	k5eAaImIp3nP
této	tento	k3xDgFnSc3
rostlině	rostlina	k1gFnSc3
pnout	pnout	k5eAaImF
se	se	k3xPyFc4
po	po	k7c6
pevných	pevný	k2eAgInPc6d1
předmětech	předmět	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
bujnější	bujný	k2eAgMnSc1d2
až	až	k8xS
bujný	bujný	k2eAgMnSc1d1
<g/>
,	,	kIx,
se	s	k7c7
vzpřímenými	vzpřímený	k2eAgInPc7d1
až	až	k8xS
polovzpřímenými	polovzpřímený	k2eAgInPc7d1
letorosty	letorost	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včelka	včelka	k1gFnSc1
je	být	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
ochlupená	ochlupený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholek	vrcholek	k1gInSc1
letorostu	letorost	k1gInSc2
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc1d1
<g/>
,	,	kIx,
světle	světle	k6eAd1
zelený	zelený	k2eAgInSc1d1
s	s	k7c7
bronzovým	bronzový	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
pigmentovaný	pigmentovaný	k2eAgMnSc1d1
antokyaniny	antokyanin	k2eAgInPc4d1
<g/>
,	,	kIx,
lesklý	lesklý	k2eAgInSc4d1
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
bíle	bíle	k6eAd1
pavučinovitě	pavučinovitě	k6eAd1
ochmýřený	ochmýřený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internodia	internodium	k1gNnPc1
a	a	k8xC
nodia	nodium	k1gNnPc1
jsou	být	k5eAaImIp3nP
zelená	zelený	k2eAgNnPc1d1
<g/>
,	,	kIx,
z	z	k7c2
osluněné	osluněný	k2eAgFnSc2d1
strany	strana	k1gFnSc2
fialově	fialově	k6eAd1
hnědá	hnědat	k5eAaImIp3nS
<g/>
,	,	kIx,
takřka	takřka	k6eAd1
lysá	lysat	k5eAaImIp3nS
<g/>
,	,	kIx,
pupeny	pupen	k1gInPc4
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
pigmentované	pigmentovaný	k2eAgFnPc1d1
antokyaniny	antokyanina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladé	mladý	k2eAgInPc1d1
lístky	lístek	k1gInPc1
mají	mít	k5eAaImIp3nP
světle	světle	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
s	s	k7c7
vínově	vínově	k6eAd1
červeným	červený	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
takřka	takřka	k6eAd1
bez	bez	k7c2
ochmýření	ochmýření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoleté	jednoletý	k2eAgNnSc1d1
réví	réví	k1gNnSc1
je	být	k5eAaImIp3nS
červenohnědé	červenohnědý	k2eAgNnSc1d1
<g/>
,	,	kIx,
podélně	podélně	k6eAd1
jemně	jemně	k6eAd1
čárkované	čárkovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
eliptického	eliptický	k2eAgInSc2d1
průřezu	průřez	k1gInSc2
<g/>
,	,	kIx,
značně	značně	k6eAd1
křehké	křehký	k2eAgFnPc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znesnadňuje	znesnadňovat	k5eAaImIp3nS
práce	práce	k1gFnSc1
při	při	k7c6
vyvazování	vyvazování	k1gNnSc6
tažnů	tažn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimní	zimní	k2eAgInPc1d1
pupeny	pupen	k1gInPc1
jsou	být	k5eAaImIp3nP
středně	středně	k6eAd1
velké	velký	k2eAgNnSc4d1
<g/>
,	,	kIx,
široké	široký	k2eAgNnSc4d1
<g/>
,	,	kIx,
špičaté	špičatý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
List	list	k1gInSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgInSc1d1
až	až	k8xS
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
okrouhlý	okrouhlý	k2eAgInSc1d1
<g/>
,	,	kIx,
nečleněný	členěný	k2eNgInSc1d1
nebo	nebo	k8xC
tří-	tří-	k?
až	až	k6eAd1
pětilaločnatý	pětilaločnatý	k2eAgInSc1d1
s	s	k7c7
velmi	velmi	k6eAd1
mělkými	mělký	k2eAgInPc7d1
až	až	k8xS
mělkými	mělký	k2eAgInPc7d1
horními	horní	k2eAgInPc7d1
bočními	boční	k2eAgInPc7d1
výkroji	výkroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchní	vrchní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
čepele	čepel	k1gFnSc2
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
hladká	hladký	k2eAgFnSc1d1
<g/>
,	,	kIx,
slabě	slabě	k6eAd1
puchýřnatá	puchýřnatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
tmavozelená	tmavozelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
tvar	tvar	k1gInSc1
čepele	čepel	k1gFnSc2
je	být	k5eAaImIp3nS
ledvinovitý	ledvinovitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
protáhlým	protáhlý	k2eAgInSc7d1
středním	střední	k2eAgInSc7d1
lalokem	lalok	k1gInSc7
<g/>
,	,	kIx,
rub	rub	k1gInSc1
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
lysý	lysý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapíkový	řapíkový	k2eAgInSc1d1
výkroj	výkroj	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
tvaru	tvar	k1gInSc6
písmene	písmeno	k1gNnSc2
„	„	k?
<g/>
V	v	k7c6
<g/>
“	“	k?
nebo	nebo	k8xC
lyrovitý	lyrovitý	k2eAgInSc4d1
s	s	k7c7
ostrým	ostrý	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
otevřený	otevřený	k2eAgInSc1d1
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
lehce	lehko	k6eAd1
překrytý	překrytý	k2eAgInSc1d1
<g/>
,	,	kIx,
řapík	řapík	k1gInSc1
je	být	k5eAaImIp3nS
krátký	krátký	k2eAgInSc1d1
nebo	nebo	k8xC
středně	středně	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
medián	medián	k1gInSc1
listu	list	k1gInSc2
<g/>
,	,	kIx,
narůžovělý	narůžovělý	k2eAgMnSc1d1
<g/>
,	,	kIx,
žilnatina	žilnatina	k1gFnSc1
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
slabě	slabě	k6eAd1
pigmentovaná	pigmentovaný	k2eAgFnSc1d1
antokyaniny	antokyanina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	list	k1gInPc1
se	se	k3xPyFc4
na	na	k7c4
podzim	podzim	k1gInSc4
barví	barvit	k5eAaImIp3nP
do	do	k7c2
vínově	vínově	k6eAd1
červena	červeno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1
pětičetné	pětičetný	k2eAgInPc4d1
květy	květ	k1gInPc4
v	v	k7c6
hroznovitých	hroznovitý	k2eAgNnPc6d1
květenstvích	květenství	k1gNnPc6
jsou	být	k5eAaImIp3nP
žlutozelené	žlutozelený	k2eAgInPc1d1
<g/>
,	,	kIx,
samosprašné	samosprašný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velká	velký	k2eAgFnSc1d1
až	až	k8xS
velká	velký	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
x	x	k?
14	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
1,2	1,2	k4
<g/>
-	-	kIx~
<g/>
1,6	1,6	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kulatá	kulatý	k2eAgFnSc1d1
bobule	bobule	k1gFnSc1
jednotné	jednotný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
silnou	silný	k2eAgFnSc7d1
<g/>
,	,	kIx,
průměrně	průměrně	k6eAd1
<g/>
,	,	kIx,
celoplošně	celoplošně	k6eAd1
voskovitě	voskovitě	k6eAd1
ojíněnou	ojíněný	k2eAgFnSc7d1
slupkou	slupka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
bobule	bobule	k1gFnSc1
je	být	k5eAaImIp3nS
tmavomodrá	tmavomodrý	k2eAgFnSc1d1
až	až	k6eAd1
modročerná	modročerný	k2eAgFnSc1d1
<g/>
,	,	kIx,
dužnina	dužnina	k1gFnSc1
je	být	k5eAaImIp3nS
bez	bez	k7c2
zbarvení	zbarvení	k1gNnPc2
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
řídká	řídký	k2eAgFnSc1d1
<g/>
,	,	kIx,
plné	plný	k2eAgFnSc3d1
chuti	chuť	k1gFnSc3
<g/>
,	,	kIx,
s	s	k7c7
3-4	3-4	k4
semeny	semeno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slupka	slupka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
červeného	červený	k2eAgNnSc2d1
barviva	barvivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeno	semeno	k1gNnSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velké	velký	k2eAgFnSc3d1
<g/>
,	,	kIx,
kulovité	kulovitý	k2eAgFnSc3d1
až	až	k8xS
elipsoidní	elipsoidní	k2eAgFnSc3d1
<g/>
,	,	kIx,
zobáček	zobáček	k1gInSc1
je	být	k5eAaImIp3nS
krátký	krátký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopečky	stopečka	k1gFnSc2
bobulí	bobule	k1gFnPc2
jsou	být	k5eAaImIp3nP
středně	středně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
obtížně	obtížně	k6eAd1
oddělitelné	oddělitelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozen	hrozen	k1gInSc1
je	být	k5eAaImIp3nS
střední	střední	k2eAgMnSc1d1
až	až	k8xS
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
x	x	k?
15-16	15-16	k4
cm	cm	kA
<g/>
,	,	kIx,
142-216	142-216	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
válcovitě-kuželovitý	válcovitě-kuželovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
křídlatý	křídlatý	k2eAgInSc1d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
hustý	hustý	k2eAgInSc1d1
až	až	k8xS
hustý	hustý	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
velmi	velmi	k6eAd1
krátkou	krátký	k2eAgFnSc7d1
(	(	kIx(
<g/>
do	do	k7c2
3	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
silnou	silný	k2eAgFnSc4d1
<g/>
,	,	kIx,
zelenou	zelený	k2eAgFnSc4d1
až	až	k8xS
průměrně	průměrně	k6eAd1
lignifikovanou	lignifikovaný	k2eAgFnSc7d1
stopkou	stopka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Frankovka	frankovka	k1gFnSc1
je	být	k5eAaImIp3nS
moštová	moštový	k2eAgFnSc1d1
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
též	též	k9
stolní	stolní	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
některých	některý	k3yIgInPc2
pramenů	pramen	k1gInPc2
německý	německý	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnSc3d1
ovšem	ovšem	k9
považují	považovat	k5eAaImIp3nP
za	za	k7c4
její	její	k3xOp3gFnSc4
pravlast	pravlast	k1gFnSc4
Dolní	dolní	k2eAgNnSc1d1
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
rozšířit	rozšířit	k5eAaPmF
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
synonym	synonymum	k1gNnPc2
Lemberger	Lembergra	k1gFnPc2
či	či	k8xC
Limberger	Limberger	k1gInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
zase	zase	k9
dal	dát	k5eAaPmAgMnS
odvozovat	odvozovat	k5eAaImF
její	její	k3xOp3gInSc4
původ	původ	k1gInSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Lemberg	Lemberg	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
stejného	stejný	k2eAgInSc2d1
názvu	název	k1gInSc2
ovšem	ovšem	k9
leží	ležet	k5eAaImIp3nS
jak	jak	k6eAd1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
Rheinland-Pfalz	Rheinland-Pfalza	k1gFnPc2
poblíž	poblíž	k6eAd1
francouzských	francouzský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
pomineme	pominout	k5eAaPmIp1nP
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
též	též	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
německý	německý	k2eAgInSc4d1
název	název	k1gInSc4
města	město	k1gNnSc2
Lvov	Lvov	k1gInSc1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Málo	málo	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
odrůda	odrůda	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Alsaska	Alsasko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otcovskou	otcovský	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
je	být	k5eAaImIp3nS
dle	dle	k7c2
provedené	provedený	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
DNA	DNA	kA
odrůda	odrůda	k1gFnSc1
Gouais	Gouais	k1gInSc1
Blanc	Blanc	k1gMnSc1
(	(	kIx(
<g/>
Weisser	Weisser	k1gMnSc1
Heunisch	Heunisch	k1gMnSc1
<g/>
,	,	kIx,
Běl	běl	k1gFnSc1
velká	velká	k1gFnSc1
<g/>
,	,	kIx,
Vídeňka	Vídeňka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
mateřskou	mateřský	k2eAgFnSc4d1
Blaue	Blaue	k1gFnSc4
Zimmettraube	Zimmettraub	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blaue	Blau	k1gInSc2
Zimmettraube	Zimmettraub	k1gInSc5
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
vymizelá	vymizelý	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
s	s	k7c7
ženským	ženský	k2eAgInSc7d1
květem	květ	k1gInSc7
<g/>
,	,	kIx,
pěstovaná	pěstovaný	k2eAgFnSc1d1
ve	v	k7c6
Štýrsku	Štýrsko	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k9
příbuzná	příbuzná	k1gFnSc1
s	s	k7c7
Modrým	modrý	k2eAgInSc7d1
portugalem	portugal	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
otcovskou	otcovský	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
je	být	k5eAaImIp3nS
Sylvánské	sylvánské	k1gNnSc1
zelené	zelená	k1gFnSc2
(	(	kIx(
<g/>
Grüner	Grüner	k1gMnSc1
Silvaner	Silvaner	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Rakousku	Rakousko	k1gNnSc6
byla	být	k5eAaImAgFnS
odrůda	odrůda	k1gFnSc1
roku	rok	k1gInSc2
2007	#num#	k4
pěstována	pěstován	k2eAgFnSc1d1
na	na	k7c4
2.640	2.640	k4
hektarech	hektar	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
tvoří	tvořit	k5eAaImIp3nS
5,5	5,5	k4
<g/>
%	%	kIx~
celkové	celkový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
Burgenland	Burgenlanda	k1gFnPc2
(	(	kIx(
<g/>
17	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
je	být	k5eAaImIp3nS
hojná	hojný	k2eAgFnSc1d1
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Eger	Egera	k1gFnPc2
<g/>
,	,	kIx,
Šoproň	Šoproň	k1gFnSc1
a	a	k8xC
hlavně	hlavně	k9
na	na	k7c6
jihu	jih	k1gInSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Německu	Německo	k1gNnSc6
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2007	#num#	k4
pěstována	pěstován	k2eAgFnSc1d1
na	na	k7c4
1.702	1.702	k4
ha	ha	kA
<g/>
,	,	kIx,
hojná	hojný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Württemberska	Württembersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
pěstována	pěstován	k2eAgFnSc1d1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Itálii	Itálie	k1gFnSc6
a	a	k8xC
v	v	k7c6
zemích	zem	k1gFnPc6
bývalého	bývalý	k2eAgInSc2d1
SSSR	SSSR	kA
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
jsou	být	k5eAaImIp3nP
osazené	osazený	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
roku	rok	k1gInSc2
2010	#num#	k4
odhadovány	odhadován	k2eAgInPc4d1
na	na	k7c4
cca	cca	kA
10.000	10.000	k4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Státní	státní	k2eAgFnSc2d1
odrůdové	odrůdový	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
odrůda	odrůda	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zapsána	zapsat	k5eAaPmNgFnS
také	také	k9
v	v	k7c6
Listině	listina	k1gFnSc6
registrovaných	registrovaný	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
pěstování	pěstování	k1gNnSc2
Bratislava	Bratislava	k1gFnSc1
Rača	Rača	k1gFnSc1
a	a	k8xC
Levicko	Levicko	k1gNnSc1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2007	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
vysazena	vysadit	k5eAaPmNgFnS
na	na	k7c4
7,78	7,78	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
na	na	k7c4
1.176	1.176	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vinařské	vinařský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Čechy	Čech	k1gMnPc7
se	se	k3xPyFc4
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
pozdní	pozdní	k2eAgNnSc4d1
zrání	zrání	k1gNnSc4
nepěstuje	pěstovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
nejrozšířenější	rozšířený	k2eAgFnSc7d3
modrou	modrý	k2eAgFnSc7d1
odrůdou	odrůda	k1gFnSc7
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1999	#num#	k4
byla	být	k5eAaImAgFnS
vysazena	vysadit	k5eAaPmNgFnS
na	na	k7c4
5	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
vinic	vinice	k1gFnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2010	#num#	k4
na	na	k7c4
7,1	7,1	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
pěstována	pěstovat	k5eAaImNgFnS
na	na	k7c4
1.308	1.308	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
ve	v	k7c6
vinařské	vinařský	k2eAgFnSc6d1
podoblasti	podoblast	k1gFnSc6
Velkopavlovické	Velkopavlovický	k2eAgFnSc6d1
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Brna	Brno	k1gNnSc2
a	a	k8xC
Slovácké	slovácký	k2eAgFnSc6d1
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Bzence	Bzenec	k1gInSc2
<g/>
,	,	kIx,
Strážnice	Strážnice	k1gFnSc2
a	a	k8xC
v	v	k7c6
Podluží	Podluží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
nejčastěji	často	k6eAd3
pěstovaná	pěstovaný	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
před	před	k7c7
rokem	rok	k1gInSc7
1900	#num#	k4
ji	on	k3xPp3gFnSc4
začal	začít	k5eAaPmAgInS
nahrazovat	nahrazovat	k5eAaImF
Modrý	modrý	k2eAgInSc1d1
Portugal	portugal	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
Svatovavřinecké	svatovavřinecké	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
vinic	vinice	k1gFnPc2
s	s	k7c7
odrůdou	odrůda	k1gFnSc7
Frankovka	frankovka	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
roku	rok	k1gInSc2
2004	#num#	k4
činil	činit	k5eAaImAgInS
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
ji	on	k3xPp3gFnSc4
reprezentuje	reprezentovat	k5eAaImIp3nS
pět	pět	k4xCc4
povolených	povolený	k2eAgInPc2d1
klonů	klon	k1gInPc2
(	(	kIx(
<g/>
r.	r.	kA
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
PO-	PO-	k1gFnSc1
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
PO-	PO-	k1gFnSc1
<g/>
275	#num#	k4
<g/>
/	/	kIx~
<g/>
i	i	k9
<g/>
,	,	kIx,
VP-	VP-	k1gFnSc1
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
VP-	VP-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
a	a	k8xC
PO-	PO-	k1gMnSc3
<g/>
281	#num#	k4
<g/>
/	/	kIx~
<g/>
E.	E.	kA
Udržovateli	udržovatel	k1gMnPc7
odrůdy	odrůda	k1gFnSc2
jsou	být	k5eAaImIp3nP
Ing.	ing.	kA
Alois	Alois	k1gMnSc1
Tománek	Tománek	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Miloš	Miloš	k1gMnSc1
Michlovský	michlovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Šlechtitelská	šlechtitelský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc2
vinařská	vinařský	k2eAgFnSc1d1
Polešovice	Polešovice	k1gFnSc1
a	a	k8xC
Šlechtitelská	šlechtitelský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
vinařská	vinařský	k2eAgFnSc1d1
Velké	velký	k2eAgFnPc4d1
Pavlovice	Pavlovice	k1gFnPc4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
název	název	k1gInSc1
Frankovka	frankovka	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
některá	některý	k3yIgNnPc1
další	další	k2eAgNnPc1d1
synonyma	synonymum	k1gNnPc1
jsou	být	k5eAaImIp3nP
odvozena	odvodit	k5eAaPmNgNnP
od	od	k7c2
předpokládaného	předpokládaný	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
původu	původ	k1gInSc2
odrůdy	odrůda	k1gFnSc2
(	(	kIx(
<g/>
Blaufränkisch	Blaufränkisch	k1gMnSc1
<g/>
,	,	kIx,
Kékfrankos	Kékfrankos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
synonyma	synonymum	k1gNnSc2
Lemberger	Lembergra	k1gFnPc2
<g/>
,	,	kIx,
Limberger	Limberger	k1gMnSc1
atd.	atd.	kA
z	z	k7c2
předpokládaného	předpokládaný	k2eAgInSc2d1
původu	původ	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Lemberg	Lemberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
<g/>
,	,	kIx,
lokálně	lokálně	k6eAd1
používaná	používaný	k2eAgNnPc1d1
synonyma	synonymum	k1gNnPc1
odrůdy	odrůda	k1gFnSc2
jsou	být	k5eAaImIp3nP
:	:	kIx,
Л	Л	k?
<g/>
,	,	kIx,
Б	Б	k?
л	л	k?
<g/>
,	,	kIx,
Ш	Ш	k?
<g/>
,	,	kIx,
К	К	k?
<g/>
,	,	kIx,
Ф	Ф	k?
<g/>
,	,	kIx,
Ф	Ф	k?
(	(	kIx(
<g/>
vše	všechen	k3xTgNnSc1
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Blanc	Blanc	k1gMnSc1
Doux	Doux	k1gInSc1
<g/>
,	,	kIx,
Blau	Blaus	k1gInSc3
Fränkisch	Fränkischa	k1gFnPc2
<g/>
,	,	kIx,
B.	B.	kA
Fränkische	Fränkische	k1gInSc1
<g/>
,	,	kIx,
Blaufränkisch	Blaufränkisch	k1gMnSc1
<g/>
,	,	kIx,
Blauer	Blauer	k1gMnSc1
Limberger	Limberger	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Blaufränkische	Blaufränkische	k1gFnSc1
<g/>
,	,	kIx,
Blaufranchis	Blaufranchis	k1gFnSc1
<g/>
,	,	kIx,
Blaufranchisch	Blaufranchisch	k1gInSc1
<g/>
,	,	kIx,
Blue	Blue	k1gFnSc1
French	French	k1gMnSc1
<g/>
,	,	kIx,
Burgund	Burgund	k1gMnSc1
Mare	Mar	k1gFnSc2
(	(	kIx(
<g/>
Rumunsko	Rumunsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chirokolistny	Chirokolistn	k1gInPc1
<g/>
,	,	kIx,
Černé	Černé	k2eAgFnPc1d1
Skalické	Skalická	k1gFnPc1
<g/>
,	,	kIx,
Č.	Č.	kA
Starosvětské	starosvětský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Č.	Č.	kA
Muškatel	muškatel	k1gInSc1
<g/>
,	,	kIx,
Čierny	Čierna	k1gFnPc1
Zierfandler	Zierfandler	k1gInSc1
<g/>
,	,	kIx,
Crna	Crn	k2eAgFnSc1d1
Frankovka	frankovka	k1gFnSc1
(	(	kIx(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
C.	C.	kA
Moravka	Moravka	k1gFnSc1
<g/>
,	,	kIx,
Fernon	Fernon	k1gNnSc1
<g/>
,	,	kIx,
Fränkische	Fränkische	k1gNnSc1
<g/>
,	,	kIx,
Fränkische	Fränkische	k1gFnSc1
Schwarz	Schwarz	k1gMnSc1
<g/>
,	,	kIx,
Franconia	Franconium	k1gNnPc1
(	(	kIx(
<g/>
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Franconia	Franconium	k1gNnSc2
Nera	Nero	k1gMnSc2
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
F.	F.	kA
Nero	Nero	k1gMnSc1
<g/>
,	,	kIx,
Franconien	Franconien	k1gInSc1
Bleu	Bleus	k1gInSc2
<g/>
,	,	kIx,
F.	F.	kA
Noir	Noir	k1gInSc1
<g/>
,	,	kIx,
Frankinja	Frankinja	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
F.	F.	kA
Modra	modro	k1gNnSc2
<g/>
,	,	kIx,
Frankovka	frankovka	k1gFnSc1
Černá	Černá	k1gFnSc1
<g/>
,	,	kIx,
F.	F.	kA
Crna	Crn	k1gInSc2
<g/>
,	,	kIx,
Frühschwartze	Frühschwartze	k1gFnSc2
<g/>
,	,	kIx,
Gamay	Gamaa	k1gFnSc2
Noire	Noir	k1gInSc5
(	(	kIx(
<g/>
mylně	mylně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gamé	Gamé	k1gNnSc1
(	(	kIx(
<g/>
Bulharsko	Bulharsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Grossburgunder	Grossburgunder	k1gMnSc1
<g/>
,	,	kIx,
Imberghem	Imbergh	k1gInSc7
<g/>
,	,	kIx,
Imbergher	Imberghra	k1gFnPc2
<g/>
,	,	kIx,
Jubiläumsrebe	Jubiläumsreb	k1gMnSc5
<g/>
,	,	kIx,
Karmazín	karmazín	k1gInSc1
<g/>
,	,	kIx,
Kék	Kék	k1gMnSc1
Frankos	Frankos	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Kékfrank	Kékfrank	k1gInSc1
<g/>
,	,	kIx,
Kékfrankos	Kékfrankos	k1gInSc1
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lampart	Lampart	k1gInSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
,	,	kIx,
SR	SR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lemberger	Lemberger	k1gMnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Limberg	Limberg	k1gMnSc1
<g/>
,	,	kIx,
Limberger	Limberger	k1gMnSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
L.	L.	kA
Blauer	Blauer	k1gInSc1
<g/>
,	,	kIx,
L.	L.	kA
Noir	Noir	k1gInSc1
<g/>
,	,	kIx,
Limburger	Limburger	k1gInSc1
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Limburské	limburský	k2eAgFnPc1d1
<g/>
,	,	kIx,
Maehrische	Maehrisch	k1gInPc1
<g/>
,	,	kIx,
Modra	modro	k1gNnPc1
Frankinja	Frankinj	k2eAgNnPc1d1
(	(	kIx(
<g/>
Slovinsko	Slovinsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Modry	modro	k1gNnPc7
Hyblink	Hyblink	k1gInSc1
<g/>
,	,	kIx,
Morávka	Morávek	k1gMnSc2
<g/>
,	,	kIx,
Moravské	moravský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Muskateller	Muskateller	k1gMnSc1
Schwarz	Schwarz	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Nagy	Nagum	k1gNnPc7
Burgundi	Burgund	k1gMnPc1
<g/>
,	,	kIx,
Nagyburgundi	Nagyburgund	k1gMnPc1
<g/>
,	,	kIx,
Neskorak	Neskorak	k1gMnSc1
<g/>
,	,	kIx,
Neskore	Neskor	k1gMnSc5
<g/>
,	,	kIx,
N.	N.	kA
Čierne	Čiern	k1gInSc5
<g/>
,	,	kIx,
Noir	Noir	k1gInSc1
de	de	k?
Franconie	Franconie	k1gFnSc2
<g/>
,	,	kIx,
Oporto	Oporta	k1gFnSc5
<g/>
,	,	kIx,
Orna	Orna	k1gMnSc1
frankovka	frankovka	k1gFnSc1
<g/>
,	,	kIx,
Portugais	Portugais	k1gFnSc1
Lerouse	Lerouse	k1gFnSc1
<g/>
,	,	kIx,
Portugais	Portugais	k1gFnSc1
Rouge	rouge	k1gFnSc1
<g/>
,	,	kIx,
Portugieser	Portugieser	k1gInSc1
Rother	Rothra	k1gFnPc2
<g/>
,	,	kIx,
Pozdni	Pozdni	k1gFnSc2
<g/>
,	,	kIx,
Pozdní	pozdní	k2eAgFnSc2d1
Skalické	Skalická	k1gFnSc2
černé	černý	k2eAgFnSc2d1
<g/>
,	,	kIx,
Schwarz	Schwarz	k1gMnSc1
Limberger	Limberger	k1gMnSc1
<g/>
,	,	kIx,
Schwarze	Schwarz	k1gMnPc4
Fraenkische	Fraenkisch	k1gMnSc4
<g/>
,	,	kIx,
Schwarzer	Schwarzer	k1gMnSc1
Burgunder	Burgunder	k1gMnSc1
<g/>
,	,	kIx,
Schwarzgrobe	Schwarzgrob	k1gMnSc5
<g/>
,	,	kIx,
Šeřina	šeřina	k1gFnSc1
<g/>
,	,	kIx,
Širokolistý	širokolistý	k2eAgInSc1d1
<g/>
,	,	kIx,
Širokolistnyj	Širokolistnyj	k1gInSc1
<g/>
,	,	kIx,
Skalické	Skalické	k2eAgFnPc1d1
Černé	Černá	k1gFnPc1
<g/>
,	,	kIx,
Starosvětský	starosvětský	k2eAgInSc1d1
Hrozen	hrozen	k1gInSc1
<g/>
,	,	kIx,
Szeleslevelü	Szeleslevelü	k1gMnSc1
<g/>
,	,	kIx,
Teltfürtü	Teltfürtü	k1gMnSc1
Kékfrankos	Kékfrankos	k1gMnSc1
<g/>
,	,	kIx,
Vaghyburgundi	Vaghyburgund	k1gMnPc1
<g/>
,	,	kIx,
Velké	velký	k2eAgFnPc1d1
Bugundské	Bugundský	k2eAgFnPc1d1
<g/>
,	,	kIx,
Vojvodino	Vojvodina	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
bujném	bujný	k2eAgInSc6d1
růstu	růst	k1gInSc6
a	a	k8xC
štěpování	štěpování	k1gNnPc1
na	na	k7c4
nevhodné	vhodný	k2eNgInPc4d1
podnože	podnož	k1gInPc4
má	mít	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
sklon	sklon	k1gInSc4
ke	k	k7c3
sprchávání	sprchávání	k1gNnSc3
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
podporuje	podporovat	k5eAaImIp3nS
také	také	k9
neselektovaný	selektovaný	k2eNgInSc4d1
množitelský	množitelský	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
náchylná	náchylný	k2eAgFnSc1d1
k	k	k7c3
vadnutí	vadnutí	k1gNnSc3
třapiny	třapina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
bujnosti	bujnost	k1gFnSc3
růstu	růst	k1gInSc2
jsou	být	k5eAaImIp3nP
nejvhodnější	vhodný	k2eAgFnPc1d3
podnože	podnož	k1gFnPc1
slaběji	slabo	k6eAd2
rostoucí	rostoucí	k2eAgFnPc1d1
a	a	k8xC
brzdící	brzdící	k2eAgNnSc1d1
sprchávání	sprchávání	k1gNnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
SO-4	SO-4	k1gMnPc4
a	a	k8xC
Cr	cr	k0
2	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
T	T	kA
5	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
nevhodná	vhodný	k2eNgFnSc1d1
je	být	k5eAaImIp3nS
K	K	kA
5	#num#	k4
<g/>
BB	BB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůdě	odrůda	k1gFnSc3
vyhovuje	vyhovovat	k5eAaImIp3nS
střední	střední	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
s	s	k7c7
řezem	řez	k1gInSc7
na	na	k7c4
tažně	tažeň	k1gInPc4
nebo	nebo	k8xC
vysoké	vysoký	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
s	s	k7c7
řezem	řez	k1gInSc7
na	na	k7c4
dva	dva	k4xCgInPc4
tažně	tažeň	k1gInPc4
<g/>
,	,	kIx,
zatížení	zatížení	k1gNnSc2
6	#num#	k4
až	až	k9
9	#num#	k4
oček	očko	k1gNnPc2
<g/>
/	/	kIx~
<g/>
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodnost	plodnost	k1gFnSc1
dřeva	dřevo	k1gNnSc2
je	být	k5eAaImIp3nS
59	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
průměrný	průměrný	k2eAgInSc1d1
počet	počet	k1gInSc1
hroznů	hrozen	k1gInPc2
na	na	k7c4
výhon	výhon	k1gInSc4
je	být	k5eAaImIp3nS
0,97	0,97	k4
<g/>
,	,	kIx,
na	na	k7c6
plodonosném	plodonosný	k2eAgMnSc6d1
1,55	1,55	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
hustotě	hustota	k1gFnSc6
vysazeného	vysazený	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
9-14	9-14	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
víno	víno	k1gNnSc1
dosáhlo	dosáhnout	k5eAaPmAgNnS
alespoň	alespoň	k9
kvality	kvalita	k1gFnSc2
jakostní	jakostní	k2eAgFnSc1d1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
hrozny	hrozen	k1gInPc4
dosáhnout	dosáhnout	k5eAaPmF
fenolovou	fenolový	k2eAgFnSc4d1
zralost	zralost	k1gFnSc4
a	a	k8xC
cukernatost	cukernatost	k1gFnSc4
17	#num#	k4
<g/>
-	-	kIx~
<g/>
19,5	19,5	k4
<g/>
°	°	k?
NM	NM	kA
<g/>
,	,	kIx,
k	k	k7c3
výrobě	výroba	k1gFnSc3
přívlastkových	přívlastkový	k2eAgNnPc2d1
vín	víno	k1gNnPc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
cukernatosti	cukernatost	k1gFnPc1
22-23	22-23	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
umožněno	umožněn	k2eAgNnSc1d1
dosažení	dosažení	k1gNnSc1
vyššího	vysoký	k2eAgInSc2d2
obsahu	obsah	k1gInSc2
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acidita	acidita	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c4
9-12	9-12	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Přetěžováním	přetěžování	k1gNnPc3
keřů	keř	k1gInPc2
vysokými	vysoký	k2eAgInPc7d1
výnosy	výnos	k1gInPc7
klesá	klesat	k5eAaImIp3nS
obsah	obsah	k1gInSc4
barviv	barvivo	k1gNnPc2
a	a	k8xC
roste	růst	k5eAaImIp3nS
acidita	acidita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Fenologie	fenologie	k1gFnSc1
</s>
<s>
Doba	doba	k1gFnSc1
rašení	rašení	k1gNnSc2
oček	očko	k1gNnPc2
a	a	k8xC
květu	květ	k1gInSc2
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
raná	raný	k2eAgFnSc1d1
<g/>
,	,	kIx,
raší	rašit	k5eAaImIp3nS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
dekádě	dekáda	k1gFnSc6
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
kvete	kvést	k5eAaImIp3nS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
dekádě	dekáda	k1gFnSc6
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bobule	bobule	k1gFnSc1
u	u	k7c2
nás	my	k3xPp1nPc2
zaměká	zaměkat	k5eAaImIp3nS
v	v	k7c6
srpnu	srpen	k1gInSc6
<g/>
,	,	kIx,
sklizňová	sklizňový	k2eAgFnSc1d1
zralost	zralost	k1gFnSc1
hroznů	hrozen	k1gInPc2
je	být	k5eAaImIp3nS
pozdní	pozdní	k2eAgInSc1d1
<g/>
,	,	kIx,
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
posunout	posunout	k5eAaPmF
až	až	k9
k	k	k7c3
začátku	začátek	k1gInSc3
listopadu	listopad	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
Oděse	Oděsa	k1gFnSc6
dozrává	dozrávat	k5eAaImIp3nS
již	již	k6eAd1
koncem	koncem	k7c2
září	září	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetační	vegetační	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
odrůdy	odrůda	k1gFnSc2
trvá	trvat	k5eAaImIp3nS
150	#num#	k4
dní	den	k1gInPc2
při	při	k7c6
sumě	suma	k1gFnSc6
aktivních	aktivní	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
</s>
<s>
2900	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Choroby	choroba	k1gFnPc1
a	a	k8xC
škůdci	škůdce	k1gMnPc1
</s>
<s>
Proti	proti	k7c3
napadení	napadení	k1gNnSc3
plísní	plíseň	k1gFnPc2
révovou	révový	k2eAgFnSc7d1
(	(	kIx(
<g/>
Plasmopara	Plasmopara	k1gFnSc1
viticola	viticola	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
padlím	padlí	k1gNnSc7
révovým	révový	k2eAgFnPc3d1
(	(	kIx(
<g/>
Uncinula	Uncinula	k1gFnSc1
necator	necator	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
méně	málo	k6eAd2
odolná	odolný	k2eAgFnSc1d1
<g/>
,	,	kIx,
proti	proti	k7c3
napadení	napadení	k1gNnSc3
plísní	plíseň	k1gFnPc2
šedou	šedý	k2eAgFnSc7d1
(	(	kIx(
<g/>
Botrytis	Botrytis	k1gFnSc7
cinerea	cinere	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
odolná	odolný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
středně	středně	k6eAd1
odolná	odolný	k2eAgNnPc4d1
proti	proti	k7c3
zimním	zimní	k2eAgInPc3d1
mrazům	mráz	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Citlivější	citlivý	k2eAgInSc1d2
je	být	k5eAaImIp3nS
na	na	k7c4
pozdní	pozdní	k2eAgInPc4d1
jarní	jarní	k2eAgInPc4d1
mrazíky	mrazík	k1gInPc4
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
hladké	hladký	k2eAgInPc4d1
listy	list	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
půdy	půda	k1gFnPc1
</s>
<s>
Odrůdě	odrůda	k1gFnSc3
vyhovují	vyhovovat	k5eAaImIp3nP
nejteplejší	teplý	k2eAgInPc1d3
polohy	poloh	k1gInPc1
<g/>
,	,	kIx,
ideální	ideální	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
jižní	jižní	k2eAgInPc1d1
<g/>
,	,	kIx,
JZ	JZ	kA
či	či	k8xC
JV	JV	kA
svahy	svah	k1gInPc1
s	s	k7c7
velmi	velmi	k6eAd1
dobrým	dobrý	k2eAgNnSc7d1
osluněním	oslunění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daří	dařit	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
na	na	k7c6
rovinatých	rovinatý	k2eAgInPc6d1
terénech	terén	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
lehčí	lehký	k2eAgFnSc6d2
<g/>
,	,	kIx,
ale	ale	k8xC
humózní	humózní	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
,	,	kIx,
nevhodné	vhodný	k2eNgFnPc1d1
jsou	být	k5eAaImIp3nP
příliš	příliš	k6eAd1
těžké	těžký	k2eAgFnPc1d1
a	a	k8xC
vlhké	vlhký	k2eAgFnPc1d1
půdy	půda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
ráda	rád	k2eAgFnSc1d1
lehce	lehko	k6eAd1
záhřevné	záhřevný	k2eAgFnPc4d1
půdy	půda	k1gFnPc4
se	s	k7c7
sprašemi	spraš	k1gFnPc7
<g/>
,	,	kIx,
vhodné	vhodný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
půdy	půda	k1gFnPc1
štěrkovité	štěrkovitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snáší	snášet	k5eAaImIp3nS
sucho	sucho	k6eAd1
i	i	k9
vyšší	vysoký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
vápna	vápno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
</s>
<s>
Láhev	láhev	k1gFnSc1
odrůdového	odrůdový	k2eAgNnSc2d1
vína	víno	k1gNnSc2
Graf	graf	k1gInSc1
Adelmann	Adelmanno	k1gNnPc2
<g/>
,	,	kIx,
Württembersko	Württembersko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Hrozny	hrozen	k1gInPc1
v	v	k7c6
kvalitě	kvalita	k1gFnSc6
jakostní	jakostní	k2eAgFnPc1d1
se	se	k3xPyFc4
macerují	macerovat	k5eAaImIp3nP
asi	asi	k9
6	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zamezilo	zamezit	k5eAaPmAgNnS
přílišné	přílišný	k2eAgFnSc3d1
akumulaci	akumulace	k1gFnSc3
tříslovin	tříslovina	k1gFnPc2
ve	v	k7c6
víně	víno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přívlastková	přívlastkový	k2eAgFnSc1d1
kvalita	kvalita	k1gFnSc1
hroznů	hrozen	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
maceraci	macerace	k1gFnSc4
i	i	k9
déle	dlouho	k6eAd2
než	než	k8xS
14	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
plnosti	plnost	k1gFnSc2
vína	víno	k1gNnSc2
i	i	k9
při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
(	(	kIx(
<g/>
35	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankovka	frankovka	k1gFnSc1
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
má	mít	k5eAaImIp3nS
v	v	k7c6
technologii	technologie	k1gFnSc6
důležité	důležitý	k2eAgNnSc4d1
místo	místo	k1gNnSc4
jablečno-mléčná	jablečno-mléčný	k2eAgFnSc1d1
fermentace	fermentace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
příznivě	příznivě	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
plnost	plnost	k1gFnSc1
aromatického	aromatický	k2eAgInSc2d1
a	a	k8xC
chuťového	chuťový	k2eAgInSc2d1
dojmu	dojem	k1gInSc2
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vínech	víno	k1gNnPc6
Frankovky	frankovka	k1gFnSc2
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
vždy	vždy	k6eAd1
poněkud	poněkud	k6eAd1
více	hodně	k6eAd2
kyselin	kyselina	k1gFnPc2
nežli	nežli	k8xS
v	v	k7c6
ostatních	ostatní	k2eAgNnPc6d1
červených	červený	k2eAgNnPc6d1
vínech	víno	k1gNnPc6
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
jsou	být	k5eAaImIp3nP
i	i	k9
třísloviny	tříslovina	k1gFnPc1
tvrdší	tvrdý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc4
vede	vést	k5eAaImIp3nS
producenty	producent	k1gMnPc4
kvalitních	kvalitní	k2eAgFnPc2d1
vín	vína	k1gFnPc2
k	k	k7c3
podstatnějšímu	podstatný	k2eAgNnSc3d2
snížení	snížení	k1gNnSc3
sklizní	sklizeň	k1gFnPc2
redukcí	redukce	k1gFnPc2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
prodloužení	prodloužení	k1gNnSc3
doby	doba	k1gFnSc2
nakvášení	nakvášení	k1gNnSc2
rmutu	rmut	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
k	k	k7c3
získání	získání	k1gNnSc3
vyššího	vysoký	k2eAgInSc2d2
extraktu	extrakt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	se	k3xPyFc4
vína	víno	k1gNnPc1
ponechávají	ponechávat	k5eAaImIp3nP
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
vyzrávat	vyzrávat	k5eAaImF
v	v	k7c6
dřevěných	dřevěný	k2eAgInPc6d1
sudech	sud	k1gInPc6
či	či	k8xC
v	v	k7c6
sudech	sud	k1gInPc6
barrique	barrique	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
v	v	k7c6
přívlastkových	přívlastkový	k2eAgFnPc6d1
kvalitách	kvalita	k1gFnPc6
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
do	do	k7c2
archivu	archiv	k1gInSc2
k	k	k7c3
dlouhodobé	dlouhodobý	k2eAgFnSc3d1
archivaci	archivace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
rozmarné	rozmarný	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
nestálá	stálý	k2eNgFnSc1d1
žena	žena	k1gFnSc1
a	a	k8xC
že	že	k8xS
má	mít	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
její	její	k3xOp3gFnPc4
dobré	dobrý	k2eAgFnPc4d1
i	i	k8xC
špatné	špatný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Barva	barva	k1gFnSc1
typického	typický	k2eAgNnSc2d1
vína	víno	k1gNnSc2
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
i	i	k9
tmavě	tmavě	k6eAd1
rubínová	rubínový	k2eAgFnSc1d1
s	s	k7c7
fialovými	fialový	k2eAgInPc7d1
záblesky	záblesk	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
mladém	mladý	k2eAgNnSc6d1
víně	víno	k1gNnSc6
je	být	k5eAaImIp3nS
travnaté	travnatý	k2eAgNnSc4d1
aroma	aroma	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
při	při	k7c6
zrání	zrání	k1gNnSc6
vína	víno	k1gNnSc2
mění	měnit	k5eAaImIp3nS
na	na	k7c4
ostružinové	ostružinový	k2eAgFnPc4d1
vůně	vůně	k1gFnPc4
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc4
je	být	k5eAaImIp3nS
tvrdší	tvrdý	k2eAgInSc1d2
<g/>
,	,	kIx,
s	s	k7c7
čerstvou	čerstvý	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
a	a	k8xC
vyššími	vysoký	k2eAgFnPc7d2
tříslovinami	tříslovina	k1gFnPc7
<g/>
,	,	kIx,
kořenitá	kořenitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ovocná	ovocný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vyzrálých	vyzrálý	k2eAgNnPc6d1
vínech	víno	k1gNnPc6
hebká	hebký	k2eAgNnPc4d1
a	a	k8xC
sametová	sametový	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vůni	vůně	k1gFnSc6
a	a	k8xC
chuti	chuť	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
hledat	hledat	k5eAaImF
ostružiny	ostružina	k1gFnSc2
<g/>
,	,	kIx,
švestky	švestka	k1gFnSc2
a	a	k8xC
černé	černý	k2eAgFnSc2d1
třešně	třešeň	k1gFnSc2
<g/>
,	,	kIx,
jádra	jádro	k1gNnSc2
peckovin	peckovina	k1gFnPc2
a	a	k8xC
skořici	skořice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Stolování	stolování	k1gNnSc1
</s>
<s>
V	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
vyzrání	vyzrání	k1gNnSc4
hroznů	hrozen	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
víno	víno	k1gNnSc4
Frankovky	frankovka	k1gFnSc2
běžným	běžný	k2eAgNnSc7d1
denním	denní	k2eAgNnSc7d1
vínem	víno	k1gNnSc7
ke	k	k7c3
svačinám	svačina	k1gFnPc3
a	a	k8xC
k	k	k7c3
celé	celý	k2eAgFnSc3d1
řadě	řada	k1gFnSc3
obvyklých	obvyklý	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
všedního	všední	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dobrých	dobrý	k2eAgInPc2d1
ročníků	ročník	k1gInPc2
se	se	k3xPyFc4
výborně	výborně	k6eAd1
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
pečeným	pečený	k2eAgNnPc3d1
masům	maso	k1gNnPc3
<g/>
,	,	kIx,
ke	k	k7c3
zvěřině	zvěřina	k1gFnSc3
<g/>
,	,	kIx,
kachně	kachna	k1gFnSc3
i	i	k8xC
huse	husa	k1gFnSc3
<g/>
,	,	kIx,
ke	k	k7c3
guláši	guláš	k1gInSc3
<g/>
,	,	kIx,
k	k	k7c3
jídlům	jídlo	k1gNnPc3
z	z	k7c2
vnitřností	vnitřnost	k1gFnPc2
<g/>
,	,	kIx,
ke	k	k7c3
kořeněným	kořeněný	k2eAgNnPc3d1
jídlům	jídlo	k1gNnPc3
zeleninovým	zeleninový	k2eAgNnPc3d1
<g/>
,	,	kIx,
k	k	k7c3
polentě	polenta	k1gFnSc3
<g/>
,	,	kIx,
ke	k	k7c3
zrajícím	zrající	k2eAgInPc3d1
i	i	k8xC
bílým	bílý	k2eAgInPc3d1
sýrům	sýr	k1gInPc3
<g/>
,	,	kIx,
k	k	k7c3
těstovinám	těstovina	k1gFnPc3
v	v	k7c6
kořenité	kořenitý	k2eAgFnSc6d1
úpravě	úprava	k1gFnSc6
nebo	nebo	k8xC
k	k	k7c3
některým	některý	k3yIgInPc3
druhům	druh	k1gInPc3
pizzy	pizza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Frankovka	frankovka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Frankovka	frankovka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VITIS	VITIS	kA
VINIFERA	VINIFERA	kA
L.	L.	kA
–	–	k?
réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
/	/	kIx~
vinič	vinič	k1gMnSc1
hroznorodý	hroznorodý	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botany	Botana	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-01-22	2008-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
Foffová	Foffový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Vurm	Vurm	k1gMnSc1
<g/>
,	,	kIx,
Dáša	Dáša	k1gFnSc1
Krausová	Krausová	k1gFnSc1
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
moravského	moravský	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praga	Praga	k1gFnSc1
Mystica	Mystica	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86767	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pierre	Pierr	k1gMnSc5
Galet	Galet	k?
:	:	kIx,
Dictionnaire	Dictionnair	k1gMnSc5
encyclopédique	encyclopédiquus	k1gMnSc5
des	des	k1gNnSc3
cépages	cépages	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hachette	Hachett	k1gInSc5
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
2000	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
236331	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hans	Hans	k1gMnSc1
Ambrosi	Ambrose	k1gFnSc4
<g/>
,	,	kIx,
Bernd	Bernd	k1gInSc4
H.	H.	kA
E.	E.	kA
Hill	Hill	k1gMnSc1
<g/>
,	,	kIx,
Erika	Erika	k1gFnSc1
Maul	maul	k1gInSc1
<g/>
,	,	kIx,
Erst	Erst	k1gMnSc1
H.	H.	kA
Rühl	Rühl	k1gMnSc1
<g/>
,	,	kIx,
Joachim	Joachim	k1gMnSc1
Schmid	Schmid	k1gInSc1
<g/>
,	,	kIx,
Fritz	Fritz	k1gMnSc1
Schuhmann	Schuhmann	k1gMnSc1
<g/>
:	:	kIx,
Farbatlas	Farbatlas	k1gMnSc1
Rebsorten	Rebsorten	k2eAgMnSc1d1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auflage	Auflage	k1gInSc1
<g/>
,	,	kIx,
Eugen	Eugen	k2eAgInSc1d1
Ulmer	Ulmer	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8001	#num#	k4
<g/>
-	-	kIx~
<g/>
5957	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Multimédia	multimédium	k1gNnPc1
</s>
<s>
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Sotolář	Sotolář	k1gMnSc1
:	:	kIx,
Multimediální	multimediální	k2eAgInSc1d1
atlas	atlas	k1gInSc1
podnožových	podnožový	k2eAgFnPc2d1
<g/>
,	,	kIx,
moštových	moštový	k2eAgFnPc2d1
a	a	k8xC
stolních	stolní	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
Mendelova	Mendelův	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
a	a	k8xC
lesnická	lesnický	k2eAgFnSc1d1
universita	universita	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
zahradnická	zahradnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Lednici	Lednice	k1gFnSc6
</s>
<s>
Martin	Martin	k1gMnSc1
Šimek	Šimek	k1gMnSc1
:	:	kIx,
Encyklopédie	Encyklopédie	k1gFnSc1
všemožnejch	všemožnejch	k?
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
z	z	k7c2
celýho	celýho	k?
světa	svět	k1gInSc2
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
již	již	k6eAd1
ouplně	ouplně	k6eAd1
vymizely	vymizet	k5eAaPmAgFnP
<g/>
,	,	kIx,
2008-2012	2008-2012	k4
</s>
