<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
součást	součást	k1gFnSc1	součást
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
druhů	druh	k1gInPc2	druh
sladkostí	sladkost	k1gFnPc2	sladkost
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
bezesporu	bezesporu	k9	bezesporu
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgFnPc3d3	nejpopulárnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
