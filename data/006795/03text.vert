<s>
Daniel	Daniel	k1gMnSc1	Daniel
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
výraz	výraz	k1gInSc1	výraz
ד	ד	k?	ד
<g/>
ַ	ַ	k?	ַ
<g/>
נ	נ	k?	נ
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ל	ל	k?	ל
(	(	kIx(	(
<g/>
Daníél	Daníél	k1gMnSc1	Daníél
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gMnSc1	můj
soudce	soudce	k1gMnSc1	soudce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
je	být	k5eAaImIp3nS	být
Daniel	Daniel	k1gMnSc1	Daniel
židovským	židovský	k2eAgInSc7d1	židovský
mladíkem	mladík	k1gInSc7	mladík
<g/>
,	,	kIx,	,
obdařeným	obdařený	k2eAgInSc7d1	obdařený
výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
moudrostí	moudrost	k1gFnSc7	moudrost
<g/>
,	,	kIx,	,
schopností	schopnost	k1gFnSc7	schopnost
vykládat	vykládat	k5eAaImF	vykládat
sny	sen	k1gInPc4	sen
a	a	k8xC	a
viděním	vidění	k1gNnSc7	vidění
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
Danel	Danel	k1gMnSc1	Danel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
znám	znám	k2eAgMnSc1d1	znám
již	již	k6eAd1	již
z	z	k7c2	z
předžidovské	předžidovský	k2eAgFnSc2d1	předžidovský
literatury	literatura	k1gFnSc2	literatura
jako	jako	k8xC	jako
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
kolovaly	kolovat	k5eAaImAgInP	kolovat
různé	různý	k2eAgInPc4d1	různý
příběhy	příběh	k1gInPc4	příběh
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
několika	několik	k4yIc2	několik
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Procentní	procentní	k2eAgNnSc1d1	procentní
zastoupení	zastoupení	k1gNnSc1	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
značném	značný	k2eAgInSc6d1	značný
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
se	se	k3xPyFc4	se
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
11	[number]	k4	11
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Procentní	procentní	k2eAgNnSc4d1	procentní
zastoupení	zastoupení	k1gNnSc4	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
strmém	strmý	k2eAgInSc6d1	strmý
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dan	Dan	k1gMnSc1	Dan
<g/>
,	,	kIx,	,
Daneček	Daneček	k1gMnSc1	Daneček
<g/>
,	,	kIx,	,
Daník	Daník	k1gMnSc1	Daník
<g/>
,	,	kIx,	,
Dany	Dana	k1gFnSc2	Dana
<g/>
,	,	kIx,	,
Danek	Danka	k1gFnPc2	Danka
<g/>
,	,	kIx,	,
Dáda	Dáda	k1gMnSc1	Dáda
<g/>
,	,	kIx,	,
Danda	Danda	k1gMnSc1	Danda
Latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Daniel	Daniel	k1gMnSc1	Daniel
Italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Daniele	Daniela	k1gFnSc3	Daniela
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Daniil	Daniil	k1gInSc1	Daniil
<g/>
,	,	kIx,	,
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
Dáňa	Dáňa	k1gFnSc1	Dáňa
Srbsky	Srbsko	k1gNnPc7	Srbsko
<g/>
:	:	kIx,	:
Danijel	Danijel	k1gMnPc7	Danijel
Bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
:	:	kIx,	:
Danail	Danail	k1gInSc1	Danail
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Dániel	Dániel	k1gInSc1	Dániel
Irsky	irsky	k6eAd1	irsky
<g/>
:	:	kIx,	:
Domhnall	Domhnall	k1gInSc1	Domhnall
nebo	nebo	k8xC	nebo
Dónall	Dónall	k1gInSc1	Dónall
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Dany	Dana	k1gFnPc1	Dana
nebo	nebo	k8xC	nebo
Daniel	Daniel	k1gMnSc1	Daniel
Data	datum	k1gNnSc2	datum
jmenin	jmeniny	k1gFnPc2	jmeniny
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
kalendář	kalendář	k1gInSc1	kalendář
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Slovenský	slovenský	k2eAgInSc1d1	slovenský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
Římskokatolický	římskokatolický	k2eAgInSc4d1	římskokatolický
církevní	církevní	k2eAgInSc4d1	církevní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
:	:	kIx,	:
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
Daniel	Daniel	k1gMnSc1	Daniel
(	(	kIx(	(
<g/>
prorok	prorok	k1gMnSc1	prorok
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Daniel	Daniel	k1gMnSc1	Daniel
Adam	Adam	k1gMnSc1	Adam
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
–	–	k?	–
český	český	k2eAgMnSc1d1	český
nakladatel	nakladatel	k1gMnSc1	nakladatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Daniel	Daniel	k1gMnSc1	Daniel
Craig	Craig	k1gMnSc1	Craig
-	-	kIx~	-
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Defoe	Defo	k1gFnSc2	Defo
–	–	k?	–
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Daniel	Daniel	k1gMnSc1	Daniel
Gabriel	Gabriel	k1gMnSc1	Gabriel
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
–	–	k?	–
argentinsko-izraelský	argentinskozraelský	k2eAgMnSc1d1	argentinsko-izraelský
pianista	pianista	k1gMnSc1	pianista
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Daniel	Daniel	k1gMnSc1	Daniel
Michael	Michael	k1gMnSc1	Michael
Blake	Blake	k1gFnPc2	Blake
Day-Lewis	Day-Lewis	k1gInSc4	Day-Lewis
–	–	k?	–
irský	irský	k2eAgMnSc1d1	irský
a	a	k8xC	a
anglický	anglický	k2eAgMnSc1d1	anglický
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
–	–	k?	–
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
Daniel	Daniel	k1gMnSc1	Daniel
Kolář	Kolář	k1gMnSc1	Kolář
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Daniel	Daniel	k1gMnSc1	Daniel
Hůlka	Hůlka	k1gMnSc1	Hůlka
–	–	k?	–
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
Daniel	Daniel	k1gMnSc1	Daniel
Krob	Krob	k1gMnSc1	Krob
–	–	k?	–
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
Arakain	Arakain	k1gMnSc1	Arakain
<g/>
,	,	kIx,	,
<g/>
Kreyson	Kreyson	k1gMnSc1	Kreyson
<g/>
,	,	kIx,	,
<g/>
Zeus	Zeus	k1gInSc1	Zeus
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Bambas	Bambas	k1gMnSc1	Bambas
–	–	k?	–
český	český	k2eAgInSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Radcliffe	Radcliff	k1gInSc5	Radcliff
–	–	k?	–
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
Daniel	Daniel	k1gMnSc1	Daniel
Dangl	Dangl	k1gMnSc1	Dangl
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
pořadu	pořad	k1gInSc2	pořad
Partička	partička	k1gFnSc1	partička
Daniel	Daniel	k1gMnSc1	Daniel
James	James	k1gMnSc1	James
Howell	Howell	k1gMnSc1	Howell
–	–	k?	–
britský	britský	k2eAgMnSc1d1	britský
youtuber	youtuber	k1gMnSc1	youtuber
(	(	kIx(	(
<g/>
danisnotonfire	danisnotonfir	k1gInSc5	danisnotonfir
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Stach	Stach	k1gMnSc1	Stach
–	–	k?	–
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
pořadu	pořad	k1gInSc2	pořad
Hyde	Hyd	k1gFnSc2	Hyd
Park	park	k1gInSc1	park
Civilizace	civilizace	k1gFnSc2	civilizace
Daniel	Daniel	k1gMnSc1	Daniel
Štrauch	Štrauch	k1gMnSc1	Štrauch
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
youtuber	youtuber	k1gMnSc1	youtuber
Danny	Danna	k1gFnSc2	Danna
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
–	–	k?	–
literární	literární	k2eAgFnSc4d1	literární
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
románů	román	k1gInPc2	román
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
i	i	k9	i
filmová	filmový	k2eAgFnSc1d1	filmová
postava	postava	k1gFnSc1	postava
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Daniel	Daniela	k1gFnPc2	Daniela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Daniel	Daniel	k1gMnSc1	Daniel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
