<s>
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2004	#num#	k4
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2004	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
výpravyIOC	výpravyIOC	k?
kód	kód	k1gInSc4
</s>
<s>
MDA	MDA	kA
</s>
<s>
ZlatáStříbrnáBronzováCelkem	ZlatáStříbrnáBronzováCelek	k1gInSc7
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
NOV	nov	k1gInSc1
</s>
<s>
Moldavský	moldavský	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Vlajkonoš	vlajkonoš	k1gMnSc1
</s>
<s>
Oleg	Oleg	k1gMnSc1
Moldovan	Moldovan	k1gMnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2004	#num#	k4
v	v	k7c6
Aténách	Atény	k1gFnPc6
reprezentovalo	reprezentovat	k5eAaImAgNnS
33	#num#	k4
sportovců	sportovec	k1gMnPc2
z	z	k7c2
toho	ten	k3xDgNnSc2
26	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
7	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reprezentanti	reprezentant	k1gMnPc1
nevybojovali	vybojovat	k5eNaPmAgMnP
žádnou	žádný	k3yNgFnSc4
medaili	medaile	k1gFnSc4
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
o	o	k7c4
jediné	jediný	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
Moldavsko	Moldavsko	k1gNnSc1
žádnou	žádný	k3yNgFnSc4
medaili	medaile	k1gFnSc4
nezískalo	získat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
zaznamenali	zaznamenat	k5eAaPmAgMnP
judista	judista	k1gMnSc1
Victor	Victor	k1gMnSc1
Bivol	Bivol	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
kategorii	kategorie	k1gFnSc6
do	do	k7c2
73	#num#	k4
kg	kg	kA
celkově	celkově	k6eAd1
pátý	pátý	k4xOgMnSc1
a	a	k8xC
vzpěrač	vzpěrač	k1gMnSc1
Alexandru	Alexandr	k1gMnSc3
Bratan	Bratan	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgInS
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
kategorii	kategorie	k1gFnSc6
do	do	k7c2
105	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
2004	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Země	zem	k1gFnPc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2004	#num#	k4
v	v	k7c6
Athénách	Athéna	k1gFnPc6
Afrika	Afrika	k1gFnSc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
</s>
<s>
Angola	Angola	k1gFnSc1
•	•	k?
</s>
<s>
Benin	Benin	k1gInSc1
•	•	k?
</s>
<s>
Botswana	Botswana	k1gFnSc1
•	•	k?
</s>
<s>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
</s>
<s>
Burundi	Burundi	k1gNnSc1
•	•	k?
</s>
<s>
Čad	Čad	k1gInSc1
•	•	k?
</s>
<s>
Džibutsko	Džibutsko	k6eAd1
•	•	k?
</s>
<s>
Egypt	Egypt	k1gInSc1
•	•	k?
</s>
<s>
Eritrea	Eritrea	k1gFnSc1
•	•	k?
</s>
<s>
Etiopie	Etiopie	k1gFnSc1
•	•	k?
</s>
<s>
Gabon	Gabon	k1gInSc1
•	•	k?
</s>
<s>
Gambie	Gambie	k1gFnSc1
•	•	k?
</s>
<s>
Ghana	Ghana	k1gFnSc1
•	•	k?
</s>
<s>
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Guinea	Guinea	k1gFnSc1
<g/>
‑	‑	k?
<g/>
Bissau	Bissaus	k1gInSc2
•	•	k?
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Kamerun	Kamerun	k1gInSc1
•	•	k?
</s>
<s>
Kapverdy	Kapverdy	k6eAd1
•	•	k?
</s>
<s>
Keňa	Keňa	k1gFnSc1
•	•	k?
</s>
<s>
Komory	komora	k1gFnPc1
•	•	k?
</s>
<s>
Republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
•	•	k?
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
•	•	k?
</s>
<s>
Lesotho	Lesotze	k6eAd1
•	•	k?
</s>
<s>
Libérie	Libérie	k1gFnSc1
•	•	k?
</s>
<s>
Libye	Libye	k1gFnSc1
•	•	k?
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1
•	•	k?
</s>
<s>
Malawi	Malawi	k1gNnSc1
•	•	k?
</s>
<s>
Mali	Mali	k1gNnSc1
•	•	k?
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
•	•	k?
</s>
<s>
Mauricius	Mauricius	k1gInSc1
•	•	k?
</s>
<s>
Maroko	Maroko	k1gNnSc1
•	•	k?
</s>
<s>
Mosambik	Mosambik	k1gInSc1
•	•	k?
</s>
<s>
Namibie	Namibie	k1gFnSc1
•	•	k?
</s>
<s>
Niger	Niger	k1gInSc1
•	•	k?
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
•	•	k?
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
</s>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Rwanda	Rwanda	k1gFnSc1
•	•	k?
</s>
<s>
Senegal	Senegal	k1gInSc1
•	•	k?
</s>
<s>
Seychely	Seychely	k1gFnPc1
•	•	k?
</s>
<s>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
</s>
<s>
Somálsko	Somálsko	k1gNnSc1
•	•	k?
</s>
<s>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Súdán	Súdán	k1gInSc1
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
</s>
<s>
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
</s>
<s>
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
</s>
<s>
Togo	Togo	k1gNnSc1
•	•	k?
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
•	•	k?
</s>
<s>
Uganda	Uganda	k1gFnSc1
•	•	k?
</s>
<s>
Zambie	Zambie	k1gFnSc1
•	•	k?
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
</s>
<s>
Argentina	Argentina	k1gFnSc1
•	•	k?
</s>
<s>
Aruba	Aruba	k1gFnSc1
•	•	k?
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
•	•	k?
</s>
<s>
Barbados	Barbados	k1gInSc1
•	•	k?
</s>
<s>
Belize	Belize	k1gFnSc1
•	•	k?
</s>
<s>
Bermudy	Bermudy	k1gFnPc1
•	•	k?
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
•	•	k?
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
</s>
<s>
Britské	britský	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Dominika	Dominik	k1gMnSc4
•	•	k?
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
</s>
<s>
Grenada	Grenada	k1gFnSc1
•	•	k?
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
•	•	k?
</s>
<s>
Guyana	Guyana	k1gFnSc1
•	•	k?
</s>
<s>
Haiti	Haiti	k1gNnSc4
•	•	k?
</s>
<s>
Honduras	Honduras	k1gInSc1
•	•	k?
</s>
<s>
Chile	Chile	k1gNnSc1
•	•	k?
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
•	•	k?
</s>
<s>
Kanada	Kanada	k1gFnSc1
•	•	k?
</s>
<s>
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
</s>
<s>
Kostarika	Kostarika	k1gFnSc1
•	•	k?
</s>
<s>
Kuba	Kuba	k1gFnSc1
•	•	k?
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
•	•	k?
</s>
<s>
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
</s>
<s>
Panama	Panama	k1gFnSc1
•	•	k?
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
•	•	k?
</s>
<s>
Peru	Peru	k1gNnSc1
•	•	k?
</s>
<s>
Portoriko	Portoriko	k1gNnSc1
•	•	k?
</s>
<s>
Salvador	Salvador	k1gInSc1
•	•	k?
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
</s>
<s>
Surinam	Surinam	k1gInSc1
•	•	k?
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
•	•	k?
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
</s>
<s>
Arménie	Arménie	k1gFnSc1
•	•	k?
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
</s>
<s>
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
</s>
<s>
Bhútán	Bhútán	k1gInSc1
•	•	k?
</s>
<s>
Čína	Čína	k1gFnSc1
•	•	k?
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
•	•	k?
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
•	•	k?
</s>
<s>
Hongkong	Hongkong	k1gInSc1
•	•	k?
</s>
<s>
Indie	Indie	k1gFnSc1
•	•	k?
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
•	•	k?
</s>
<s>
Írán	Írán	k1gInSc1
•	•	k?
</s>
<s>
Irák	Irák	k1gInSc1
•	•	k?
</s>
<s>
Izrael	Izrael	k1gInSc1
•	•	k?
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
•	•	k?
</s>
<s>
Jemen	Jemen	k1gInSc1
•	•	k?
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
</s>
<s>
Katar	katar	k1gInSc1
•	•	k?
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
•	•	k?
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
</s>
<s>
Kypr	Kypr	k1gInSc1
•	•	k?
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
</s>
<s>
Laos	Laos	k1gInSc1
•	•	k?
</s>
<s>
Libanon	Libanon	k1gInSc1
•	•	k?
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
•	•	k?
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
•	•	k?
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
</s>
<s>
Myanmar	Myanmar	k1gInSc1
•	•	k?
</s>
<s>
Nepál	Nepál	k1gInSc1
•	•	k?
</s>
<s>
Omán	Omán	k1gInSc1
•	•	k?
</s>
<s>
Pákistán	Pákistán	k1gInSc1
•	•	k?
</s>
<s>
Palestina	Palestina	k1gFnSc1
•	•	k?
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
</s>
<s>
Singapur	Singapur	k1gInSc1
•	•	k?
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
•	•	k?
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
•	•	k?
</s>
<s>
Šrí	Šrí	k?
Lanka	lanko	k1gNnSc2
•	•	k?
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
•	•	k?
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
</s>
<s>
Vietnam	Vietnam	k1gInSc1
•	•	k?
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
</s>
<s>
Andorra	Andorra	k1gFnSc1
•	•	k?
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
</s>
<s>
Belgie	Belgie	k1gFnSc1
•	•	k?
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
</s>
<s>
Česko	Česko	k1gNnSc1
•	•	k?
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
•	•	k?
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
•	•	k?
</s>
<s>
Finsko	Finsko	k1gNnSc1
•	•	k?
</s>
<s>
Francie	Francie	k1gFnSc1
•	•	k?
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
</s>
<s>
Island	Island	k1gInSc1
•	•	k?
</s>
<s>
Irsko	Irsko	k1gNnSc1
•	•	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
•	•	k?
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
</s>
<s>
Litva	Litva	k1gFnSc1
•	•	k?
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
</s>
<s>
Makedonie	Makedonie	k1gFnSc1
•	•	k?
</s>
<s>
Malta	Malta	k1gFnSc1
•	•	k?
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
</s>
<s>
Monako	Monako	k1gNnSc1
•	•	k?
</s>
<s>
Německo	Německo	k1gNnSc1
•	•	k?
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
</s>
<s>
Norsko	Norsko	k1gNnSc1
•	•	k?
</s>
<s>
Polsko	Polsko	k1gNnSc1
•	•	k?
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
•	•	k?
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
</s>
<s>
Rusko	Rusko	k1gNnSc1
•	•	k?
</s>
<s>
Řecko	Řecko	k1gNnSc1
•	•	k?
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
•	•	k?
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
•	•	k?
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
•	•	k?
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
•	•	k?
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
•	•	k?
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
</s>
<s>
Turecko	Turecko	k1gNnSc1
•	•	k?
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Fidži	Fidž	k1gFnSc3
•	•	k?
</s>
<s>
Guam	Guam	k1gInSc1
•	•	k?
</s>
<s>
Kiribati	Kiribat	k5eAaPmF,k5eAaImF
•	•	k?
</s>
<s>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Mikronésie	Mikronésie	k1gFnSc1
•	•	k?
</s>
<s>
Nauru	Naura	k1gFnSc4
•	•	k?
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
</s>
<s>
Palau	Palau	k6eAd1
•	•	k?
</s>
<s>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Samoa	Samoa	k1gFnSc1
•	•	k?
</s>
<s>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Tonga	Tonga	k1gFnSc1
•	•	k?
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4
•	•	k?
</s>
<s>
Vanuatu	Vanuata	k1gFnSc4
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
sportovci	sportovec	k1gMnPc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Moldavsko	Moldavsko	k1gNnSc1
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
