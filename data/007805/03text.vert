<s>
Polyekran	polyekran	k1gInSc1	polyekran
byl	být	k5eAaImAgInS	být
originální	originální	k2eAgInSc4d1	originální
československý	československý	k2eAgInSc4d1	československý
promítací	promítací	k2eAgInSc4d1	promítací
systém	systém	k1gInSc4	systém
s	s	k7c7	s
více	hodně	k6eAd2	hodně
společně	společně	k6eAd1	společně
ovládanými	ovládaný	k2eAgInPc7d1	ovládaný
projektory	projektor	k1gInPc7	projektor
filmů	film	k1gInPc2	film
i	i	k8xC	i
diapozitivů	diapozitiv	k1gInPc2	diapozitiv
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
plátny	plátno	k1gNnPc7	plátno
a	a	k8xC	a
mnohakanálovým	mnohakanálový	k2eAgInSc7d1	mnohakanálový
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Emila	Emil	k1gMnSc2	Emil
Radoka	Radoek	k1gMnSc2	Radoek
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pechara	Pechar	k1gMnSc2	Pechar
a	a	k8xC	a
pracovníků	pracovník	k1gMnPc2	pracovník
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
zvukové	zvukový	k2eAgFnSc2d1	zvuková
<g/>
,	,	kIx,	,
obrazové	obrazový	k2eAgFnSc2d1	obrazová
a	a	k8xC	a
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
předveden	předvést	k5eAaPmNgInS	předvést
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Friče	Frič	k1gMnSc2	Frič
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tam	tam	k6eAd1	tam
měl	mít	k5eAaImAgInS	mít
Polyekran	polyekran	k1gInSc4	polyekran
7	[number]	k4	7
promítacích	promítací	k2eAgInPc2d1	promítací
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c4	na
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
8	[number]	k4	8
na	na	k7c4	na
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
a	a	k8xC	a
8	[number]	k4	8
pláten	plátno	k1gNnPc2	plátno
<g/>
)	)	kIx)	)
Technické	technický	k2eAgNnSc1d1	technické
vybavení	vybavení	k1gNnSc1	vybavení
polyekranu	polyekran	k1gInSc2	polyekran
sloužilo	sloužit	k5eAaImAgNnS	sloužit
i	i	k9	i
pro	pro	k7c4	pro
představení	představení	k1gNnSc4	představení
Laterny	laterna	k1gFnSc2	laterna
magiky	magika	k1gFnSc2	magika
Otto	Otto	k1gMnSc1	Otto
Levinský	Levinský	k2eAgMnSc1d1	Levinský
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Stránský	Stránský	k1gMnSc1	Stránský
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Film	film	k1gInSc1	film
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
technika	technika	k1gFnSc1	technika
(	(	kIx(	(
<g/>
SNTL	SNTL	kA	SNTL
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Laterna	laterna	k1gFnSc1	laterna
magika	magika	k1gFnSc1	magika
Kinoautomat	Kinoautomat	k1gInSc1	Kinoautomat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Frič-polyekrán	Fričolyekrán	k1gInSc1	Frič-polyekrán
a	a	k8xC	a
monoskop	monoskop	k1gInSc1	monoskop
</s>
