<s>
Alexandrín	alexandrín	k1gInSc1	alexandrín
je	být	k5eAaImIp3nS	být
dvanáctislabičný	dvanáctislabičný	k2eAgInSc1d1	dvanáctislabičný
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
)	)	kIx)	)
a	a	k8xC	a
třináctislabičný	třináctislabičný	k2eAgInSc1d1	třináctislabičný
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
)	)	kIx)	)
rýmovaný	rýmovaný	k2eAgInSc1d1	rýmovaný
verš	verš	k1gInSc1	verš
tvořený	tvořený	k2eAgInSc1d1	tvořený
jambickými	jambický	k2eAgFnPc7d1	jambická
stopami	stopa	k1gFnPc7	stopa
(	(	kIx(	(
<g/>
jambický	jambický	k2eAgInSc1d1	jambický
hexametr	hexametr	k1gInSc1	hexametr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesté	šestý	k4xOgFnSc6	šestý
slabice	slabika	k1gFnSc6	slabika
následuje	následovat	k5eAaImIp3nS	následovat
cézura	cézura	k1gFnSc1	cézura
(	(	kIx(	(
<g/>
přerývka	přerývka	k1gFnSc1	přerývka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
poezii	poezie	k1gFnSc6	poezie
není	být	k5eNaImIp3nS	být
dodržována	dodržován	k2eAgFnSc1d1	dodržována
zcela	zcela	k6eAd1	zcela
důsledně	důsledně	k6eAd1	důsledně
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrínu	alexandrín	k1gInSc3	alexandrín
hojně	hojně	k6eAd1	hojně
využívá	využívat	k5eAaImIp3nS	využívat
stará	starý	k2eAgFnSc1d1	stará
poezie	poezie	k1gFnSc1	poezie
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
staré	starý	k2eAgFnPc1d1	stará
skladby	skladba	k1gFnPc1	skladba
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
starofrancouzské	starofrancouzský	k2eAgFnSc2d1	starofrancouzská
Alexandreidy	Alexandreida	k1gFnSc2	Alexandreida
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
reprezentativním	reprezentativní	k2eAgInSc7d1	reprezentativní
veršem	verš	k1gInSc7	verš
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
sonetech	sonet	k1gInPc6	sonet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představuje	představovat	k5eAaImIp3nS	představovat
exkluzivní	exkluzivní	k2eAgInSc1d1	exkluzivní
verš	verš	k1gInSc1	verš
zdůrazňující	zdůrazňující	k2eAgInSc4d1	zdůrazňující
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
poezií	poezie	k1gFnSc7	poezie
a	a	k8xC	a
prózou	próza	k1gFnSc7	próza
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
užíván	užívat	k5eAaImNgInS	užívat
Otokarem	Otokar	k1gMnSc7	Otokar
Březinou	Březina	k1gMnSc7	Březina
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
proto	proto	k8xC	proto
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
napsal	napsat	k5eAaPmAgMnS	napsat
alexandrínem	alexandrín	k1gInSc7	alexandrín
Smuteční	smuteční	k2eAgFnSc4d1	smuteční
hranu	hrana	k1gFnSc4	hrana
za	za	k7c4	za
Otokara	Otokar	k1gMnSc4	Otokar
Březinu	Březina	k1gMnSc4	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
Karel	Karel	k1gMnSc1	Karel
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
Otokar	Otokar	k1gMnSc1	Otokar
Březina	Březina	k1gMnSc1	Březina
Jiří	Jiří	k1gMnSc1	Jiří
Orten	Ortna	k1gFnPc2	Ortna
Jacek	Jacek	k1gMnSc1	Jacek
Baluch	Baluch	k1gMnSc1	Baluch
<g/>
,	,	kIx,	,
Piotr	Piotr	k1gMnSc1	Piotr
Gierowski	Gierowsk	k1gFnSc2	Gierowsk
<g/>
,	,	kIx,	,
Czesko-polski	Czeskoolsk	k1gFnPc1	Czesko-polsk
słownik	słownik	k1gMnSc1	słownik
terminów	terminów	k?	terminów
literackich	literackich	k1gMnSc1	literackich
<g/>
,	,	kIx,	,
Kraków	Kraków	k1gMnSc1	Kraków
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Poetický	poetický	k2eAgInSc1d1	poetický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
s.	s.	k?	s.
112	[number]	k4	112
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
alexandrin	alexandrin	k1gInSc1	alexandrin
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
41	[number]	k4	41
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
nr	nr	k?	nr
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
459	[number]	k4	459
<g/>
-	-	kIx~	-
<g/>
513	[number]	k4	513
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
O	o	k7c6	o
semantyce	semantyka	k1gFnSc6	semantyka
czeskiego	czeskiego	k6eAd1	czeskiego
aleksandrynu	aleksandrynout	k5eAaPmIp1nS	aleksandrynout
<g/>
,	,	kIx,	,
przeł	przeł	k?	przeł
<g/>
.	.	kIx.	.
</s>
<s>
Lucylla	Lucylla	k1gFnSc1	Lucylla
Pszczołowska	Pszczołowska	k1gFnSc1	Pszczołowska
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
Wiersz	Wiersz	k1gMnSc1	Wiersz
i	i	k8xC	i
poezja	poezja	k1gMnSc1	poezja
<g/>
,	,	kIx,	,
red	red	k?	red
<g/>
.	.	kIx.	.
</s>
<s>
Jacek	Jacek	k6eAd1	Jacek
Trzynadlowski	Trzynadlowski	k1gNnSc1	Trzynadlowski
<g/>
,	,	kIx,	,
Wrocław	Wrocław	k1gFnSc1	Wrocław
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
s.	s.	k?	s.
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Červenka	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
Květa	Květa	k1gFnSc1	Květa
Sgallová	Sgallová	k1gFnSc1	Sgallová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
česká	český	k2eAgNnPc1d1	české
přízvučná	přízvučný	k2eAgNnPc1d1	přízvučné
metra	metro	k1gNnPc1	metro
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
]]	]]	k?	]]
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
Słowiańska	Słowiańska	k1gFnSc1	Słowiańska
metryka	metryek	k1gMnSc2	metryek
porównawcza	porównawcz	k1gMnSc2	porównawcz
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Europejskie	Europejskie	k1gFnSc1	Europejskie
wzorce	wzorec	k1gInSc2	wzorec
metryczne	metrycznout	k5eAaPmIp3nS	metrycznout
w	w	k?	w
literaturach	literaturach	k1gMnSc1	literaturach
słowiańskich	słowiańskich	k1gMnSc1	słowiańskich
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
<s>
Wiktor	Wiktor	k1gMnSc1	Wiktor
Jarosław	Jarosław	k1gMnSc1	Jarosław
Darasz	Darasz	k1gMnSc1	Darasz
<g/>
,	,	kIx,	,
Mały	Mał	k2eAgInPc4d1	Mał
przewodnik	przewodnik	k1gInSc4	przewodnik
po	po	k7c6	po
wierszu	wiersz	k1gInSc6	wiersz
polskim	polskima	k1gFnPc2	polskima
<g/>
,	,	kIx,	,
Kraków	Kraków	k1gFnSc1	Kraków
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
alexandrín	alexandrín	k1gInSc1	alexandrín
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
řeckého	řecký	k2eAgInSc2d1	řecký
a	a	k8xC	a
latinského	latinský	k2eAgInSc2d1	latinský
hexametru	hexametr	k1gInSc2	hexametr
a	a	k8xC	a
pentametru	pentametr	k1gInSc2	pentametr
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
386	[number]	k4	386
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Levý	levý	k2eAgMnSc1d1	levý
<g/>
,	,	kIx,	,
W	W	kA	W
sprawie	sprawie	k1gFnSc1	sprawie
ścisłych	ścisłycha	k1gFnPc2	ścisłycha
metod	metoda	k1gFnPc2	metoda
analizy	analiza	k1gFnSc2	analiza
wiersza	wiersza	k1gFnSc1	wiersza
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
Poetyka	Poetyka	k1gMnSc1	Poetyka
i	i	k8xC	i
matematyka	matematyka	k1gMnSc1	matematyka
<g/>
.	.	kIx.	.
</s>
<s>
Redigovala	redigovat	k5eAaImAgFnS	redigovat
Maria	Maria	k1gFnSc1	Maria
Renata	Renata	k1gFnSc1	Renata
Mayenowa	Mayenowa	k1gFnSc1	Mayenowa
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Roy	Roy	k1gMnSc1	Roy
Lewis	Lewis	k1gInSc1	Lewis
<g/>
,	,	kIx,	,
On	on	k3xPp3gInSc1	on
Reading	Reading	k1gInSc1	Reading
French	French	k1gInSc4	French
Verse	verse	k1gFnSc2	verse
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
study	stud	k1gInPc1	stud
of	of	k?	of
Poetic	Poetice	k1gFnPc2	Poetice
Form	Form	k1gMnSc1	Form
<g/>
,	,	kIx,	,
Clarendon	Clarendon	k1gMnSc1	Clarendon
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
Alexandrín	alexandrín	k1gInSc1	alexandrín
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
