<s>
Alexandrín	alexandrín	k1gInSc1	alexandrín
je	být	k5eAaImIp3nS	být
dvanáctislabičný	dvanáctislabičný	k2eAgInSc1d1	dvanáctislabičný
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
)	)	kIx)	)
a	a	k8xC	a
třináctislabičný	třináctislabičný	k2eAgInSc1d1	třináctislabičný
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
)	)	kIx)	)
rýmovaný	rýmovaný	k2eAgInSc1d1	rýmovaný
verš	verš	k1gInSc1	verš
tvořený	tvořený	k2eAgInSc1d1	tvořený
jambickými	jambický	k2eAgFnPc7d1	jambická
stopami	stopa	k1gFnPc7	stopa
(	(	kIx(	(
<g/>
jambický	jambický	k2eAgInSc1d1	jambický
hexametr	hexametr	k1gInSc1	hexametr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
