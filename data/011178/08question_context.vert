<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc2d1	hrubá
skladby	skladba	k1gFnSc2	skladba
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
planetou	planeta	k1gFnSc7	planeta
<g/>
"	"	kIx"	"
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Eliptická	eliptický	k2eAgFnSc1d1	eliptická
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Venuše	Venuše	k1gFnSc2	Venuše
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejmenší	malý	k2eAgFnSc4d3	nejmenší
výstřednost	výstřednost	k1gFnSc4	výstřednost
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,007	[number]	k4	0,007
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
224,7	[number]	k4	224,7
pozemských	pozemský	k2eAgInPc2d1	pozemský
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
blíže	blíž	k1gFnSc2	blíž
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
úhlová	úhlový	k2eAgFnSc1d1	úhlová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nemůže	moct	k5eNaImIp3nS	moct
překročit	překročit	k5eAaPmF	překročit
určitou	určitý	k2eAgFnSc4d1	určitá
mez	mez	k1gFnSc4	mez
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
elongace	elongace	k1gFnSc1	elongace
je	být	k5eAaImIp3nS	být
47,8	[number]	k4	47,8
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
před	před	k7c7	před
úsvitem	úsvit	k1gInSc7	úsvit
nebo	nebo	k8xC	nebo
po	po	k7c6	po
soumraku	soumrak	k1gInSc6	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
večernice	večernice	k1gFnSc1	večernice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zdaleka	zdaleka	k6eAd1	zdaleka
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
bodový	bodový	k2eAgInSc4d1	bodový
přírodní	přírodní	k2eAgInSc4d1	přírodní
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
magnituda	magnituda	k1gFnSc1	magnituda
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
a	a	k8xC	a
Měsíci	měsíc	k1gInSc3	měsíc
nejjasnějším	jasný	k2eAgInSc7d3	nejjasnější
zdrojem	zdroj	k1gInSc7	zdroj
<g/>
.	.	kIx.	.
</s>
