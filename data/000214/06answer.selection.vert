<s>
Münzova	Münzův	k2eAgFnSc1d1	Münzův
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
vila	vila	k1gFnSc1	vila
Münz	Münza	k1gFnPc2	Münza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
architekta	architekt	k1gMnSc2	architekt
Ernsta	Ernst	k1gMnSc2	Ernst
Wiesnera	Wiesner	k1gMnSc2	Wiesner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
projektoval	projektovat	k5eAaBmAgMnS	projektovat
i	i	k9	i
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
vilu	vila	k1gFnSc4	vila
Stiassni	Stiassni	k1gFnSc2	Stiassni
<g/>
.	.	kIx.	.
</s>
