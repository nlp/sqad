<s>
Münzova	Münzův	k2eAgFnSc1d1	Münzův
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
vila	vila	k1gFnSc1	vila
Münz	Münza	k1gFnPc2	Münza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
architekta	architekt	k1gMnSc2	architekt
Ernsta	Ernst	k1gMnSc2	Ernst
Wiesnera	Wiesner	k1gMnSc2	Wiesner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
projektoval	projektovat	k5eAaBmAgMnS	projektovat
i	i	k9	i
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
vilu	vila	k1gFnSc4	vila
Stiassni	Stiassni	k1gFnSc2	Stiassni
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
rodinné	rodinný	k2eAgFnSc2d1	rodinná
vily	vila	k1gFnSc2	vila
ředitele	ředitel	k1gMnSc2	ředitel
České	český	k2eAgFnSc2d1	Česká
banky	banka	k1gFnSc2	banka
Union	union	k1gInSc1	union
a	a	k8xC	a
perského	perský	k2eAgMnSc2d1	perský
honorárního	honorární	k2eAgMnSc2d1	honorární
konzula	konzul	k1gMnSc2	konzul
Eduarda	Eduard	k1gMnSc2	Eduard
Münze	Münze	k1gFnSc2	Münze
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
architekt	architekt	k1gMnSc1	architekt
Ernst	Ernst	k1gMnSc1	Ernst
Wiesner	Wiesner	k1gMnSc1	Wiesner
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnPc4	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Wiesnerových	Wiesnerových	k2eAgInPc2d1	Wiesnerových
projektů	projekt	k1gInPc2	projekt
funkcionalistických	funkcionalistický	k2eAgInPc2d1	funkcionalistický
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vil	vila	k1gFnPc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
třemi	tři	k4xCgInPc7	tři
různými	různý	k2eAgInPc7d1	různý
horizontálními	horizontální	k2eAgInPc7d1	horizontální
tělesy	těleso	k1gNnPc7	těleso
uspořádanými	uspořádaný	k2eAgMnPc7d1	uspořádaný
do	do	k7c2	do
půdorysu	půdorys	k1gInSc2	půdorys
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
L	L	kA	L
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
obdélníkovými	obdélníkový	k2eAgNnPc7d1	obdélníkové
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
zahrady	zahrada	k1gFnSc2	zahrada
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
stavba	stavba	k1gFnSc1	stavba
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
rovněž	rovněž	k9	rovněž
výrazné	výrazný	k2eAgInPc4d1	výrazný
štíhlé	štíhlý	k2eAgInPc4d1	štíhlý
vysoké	vysoký	k2eAgInPc4d1	vysoký
komíny	komín	k1gInPc4	komín
svědčící	svědčící	k2eAgInPc4d1	svědčící
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
holandského	holandský	k2eAgInSc2d1	holandský
neoplasticismu	neoplasticismus	k1gInSc2	neoplasticismus
<g/>
.	.	kIx.	.
</s>
<s>
Münzova	Münzův	k2eAgFnSc1d1	Münzův
vila	vila	k1gFnSc1	vila
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
poškozena	poškodit	k5eAaPmNgFnS	poškodit
necitlivými	citlivý	k2eNgFnPc7d1	necitlivá
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mělo	mít	k5eAaImAgNnS	mít
sídlo	sídlo	k1gNnSc1	sídlo
generální	generální	k2eAgNnSc1d1	generální
ředitelství	ředitelství	k1gNnSc1	ředitelství
Zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
původním	původní	k2eAgMnPc3d1	původní
dědicům	dědic	k1gMnPc3	dědic
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
majetkem	majetek	k1gInSc7	majetek
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Equity	Equita	k1gFnSc2	Equita
Investment	Investment	k1gMnSc1	Investment
(	(	kIx(	(
<g/>
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
Igor	Igor	k1gMnSc1	Igor
Fait	Fait	k1gMnSc1	Fait
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
finančně	finančně	k6eAd1	finančně
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
kavárny	kavárna	k1gFnSc2	kavárna
Era	Era	k1gFnSc2	Era
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
kavárnu	kavárna	k1gFnSc4	kavárna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
začala	začít	k5eAaPmAgFnS	začít
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
vily	vila	k1gFnSc2	vila
ale	ale	k8xC	ale
zbyly	zbýt	k5eAaPmAgInP	zbýt
jen	jen	k9	jen
obvodové	obvodový	k2eAgFnSc3d1	obvodová
zdi	zeď	k1gFnSc3	zeď
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
narušené	narušený	k2eAgFnSc3d1	narušená
statice	statika	k1gFnSc3	statika
a	a	k8xC	a
nově	nova	k1gFnSc3	nova
zjištěné	zjištěný	k2eAgFnPc1d1	zjištěná
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
budova	budova	k1gFnSc1	budova
nemá	mít	k5eNaImIp3nS	mít
základy	základ	k1gInPc4	základ
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
zbořena	zbořen	k2eAgFnSc1d1	zbořena
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
vily	vila	k1gFnSc2	vila
skutečně	skutečně	k6eAd1	skutečně
zbořena	zbořen	k2eAgFnSc1d1	zbořena
a	a	k8xC	a
místo	místo	k6eAd1	místo
ní	on	k3xPp3gFnSc3	on
postavena	postaven	k2eAgFnSc1d1	postavena
replika	replika	k1gFnSc1	replika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
stavby	stavba	k1gFnSc2	stavba
zbyly	zbýt	k5eAaPmAgInP	zbýt
jen	jen	k6eAd1	jen
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
pásové	pásový	k2eAgNnSc4d1	pásové
okno	okno	k1gNnSc4	okno
nebo	nebo	k8xC	nebo
garážová	garážový	k2eAgNnPc4d1	garážové
vrata	vrata	k1gNnPc4	vrata
<g/>
.	.	kIx.	.
</s>
<s>
Exteriér	exteriér	k1gInSc1	exteriér
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
dle	dle	k7c2	dle
dobových	dobový	k2eAgFnPc2d1	dobová
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
k	k	k7c3	k
interiéru	interiér	k1gInSc3	interiér
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
fotografie	fotografia	k1gFnPc1	fotografia
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
srovnání	srovnání	k1gNnSc2	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
Wiesnerovými	Wiesnerův	k2eAgFnPc7d1	Wiesnerova
stavbami	stavba	k1gFnPc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
stojí	stát	k5eAaImIp3nP	stát
architekti	architekt	k1gMnPc1	architekt
Rusín	Rusín	k1gMnSc1	Rusín
<g/>
,	,	kIx,	,
Shromáždilová	Shromáždilová	k1gFnSc1	Shromáždilová
<g/>
,	,	kIx,	,
Šrom	Šrom	k1gInSc1	Šrom
a	a	k8xC	a
Wahla	Wahla	k1gFnSc1	Wahla
z	z	k7c2	z
ateliéru	ateliér	k1gInSc2	ateliér
RAW	RAW	kA	RAW
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
nákladnější	nákladný	k2eAgInSc4d2	nákladnější
než	než	k8xS	než
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
