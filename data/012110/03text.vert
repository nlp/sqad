<p>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
kvanta	kvantum	k1gNnSc2	kvantum
mají	mít	k5eAaImIp3nP	mít
energii	energie	k1gFnSc4	energie
postačující	postačující	k2eAgFnSc4d1	postačující
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
molekul	molekula	k1gFnPc2	molekula
ozářené	ozářený	k2eAgFnSc2d1	ozářená
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
radioaktivním	radioaktivní	k2eAgInSc6d1	radioaktivní
rozpadu	rozpad	k1gInSc6	rozpad
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
organickou	organický	k2eAgFnSc4d1	organická
tkáň	tkáň	k1gFnSc4	tkáň
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
mutace	mutace	k1gFnPc4	mutace
<g/>
,	,	kIx,	,
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc4	nemoc
z	z	k7c2	z
ozáření	ozáření	k1gNnSc2	ozáření
i	i	k8xC	i
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
lidských	lidský	k2eAgInPc6d1	lidský
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
nebo	nebo	k8xC	nebo
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
neutronové	neutronový	k2eAgNnSc4d1	neutronové
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
záření	záření	k1gNnSc4	záření
beta	beta	k1gNnSc2	beta
je	být	k5eAaImIp3nS	být
kvantifikace	kvantifikace	k1gFnSc1	kvantifikace
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
velmi	velmi	k6eAd1	velmi
pomalé	pomalý	k2eAgFnSc2d1	pomalá
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neutronů	neutron	k1gInPc2	neutron
<g/>
)	)	kIx)	)
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
sekundární	sekundární	k2eAgFnSc4d1	sekundární
ionizaci	ionizace	k1gFnSc4	ionizace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc1d1	obdobný
případ	případ	k1gInSc1	případ
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pozitronů	pozitron	k1gInPc2	pozitron
<g/>
,	,	kIx,	,
anihilujících	anihilující	k2eAgFnPc2d1	anihilující
s	s	k7c7	s
elektrony	elektron	k1gInPc7	elektron
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
velmi	velmi	k6eAd1	velmi
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
záření	záření	k1gNnSc2	záření
γ	γ	k?	γ
</s>
</p>
<p>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
charakter	charakter	k1gInSc4	charakter
ionizačního	ionizační	k2eAgInSc2d1	ionizační
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
přímo	přímo	k6eAd1	přímo
ionizující	ionizující	k2eAgFnSc6d1	ionizující
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
ionizující	ionizující	k2eAgFnSc1d1	ionizující
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
(	(	kIx(	(
<g/>
protony	proton	k1gInPc7	proton
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc7	elektron
<g/>
,	,	kIx,	,
pozitrony	pozitron	k1gInPc7	pozitron
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepřímo	přímo	k6eNd1	přímo
ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nenabité	nabitý	k2eNgFnPc4d1	nenabitá
částice	částice	k1gFnPc4	částice
(	(	kIx(	(
<g/>
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
fotony	foton	k1gInPc4	foton
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prostředí	prostředí	k1gNnSc6	prostředí
samy	sám	k3xTgInPc4	sám
neionizují	ionizovat	k5eNaBmIp3nP	ionizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
prostředím	prostředí	k1gNnSc7	prostředí
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
sekundární	sekundární	k2eAgFnSc4d1	sekundární
přímo	přímo	k6eAd1	přímo
ionizující	ionizující	k2eAgFnSc4d1	ionizující
částice	částice	k1gFnSc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ionizace	ionizace	k1gFnSc1	ionizace
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
způsobena	způsoben	k2eAgFnSc1d1	způsobena
těmito	tento	k3xDgFnPc7	tento
sekundárními	sekundární	k2eAgFnPc7d1	sekundární
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
energetickou	energetický	k2eAgFnSc4d1	energetická
hranici	hranice	k1gFnSc4	hranice
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
energie	energie	k1gFnSc1	energie
5	[number]	k4	5
eV	eV	k?	eV
pro	pro	k7c4	pro
α	α	k?	α
<g/>
,	,	kIx,	,
β	β	k?	β
a	a	k8xC	a
γ	γ	k?	γ
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
veličin	veličina	k1gFnPc2	veličina
charakterizujících	charakterizující	k2eAgInPc2d1	charakterizující
ionizující	ionizující	k2eAgNnSc4d1	ionizující
záření	záření	k1gNnSc4	záření
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgInSc1d1	lineární
přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc7	druh
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
==	==	k?	==
</s>
</p>
<p>
<s>
Záření	záření	k1gNnSc1	záření
alfa	alfa	k1gNnSc1	alfa
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
–	–	k?	–
proud	proud	k1gInSc1	proud
α	α	k?	α
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jader	jádro	k1gNnPc2	jádro
helia	helium	k1gNnPc5	helium
</s>
</p>
<p>
<s>
Záření	záření	k1gNnSc1	záření
beta	beta	k1gNnSc1	beta
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
)	)	kIx)	)
–	–	k?	–
záření	záření	k1gNnSc2	záření
urychlených	urychlený	k2eAgInPc2d1	urychlený
elektronů	elektron	k1gInPc2	elektron
nebo	nebo	k8xC	nebo
pozitronů	pozitron	k1gInPc2	pozitron
</s>
</p>
<p>
<s>
Záření	záření	k1gNnSc1	záření
gama	gama	k1gNnSc1	gama
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
–	–	k?	–
energetické	energetický	k2eAgInPc1d1	energetický
fotony	foton	k1gInPc1	foton
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
druh	druh	k1gInSc1	druh
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
</s>
</p>
<p>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
–	–	k?	–
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
kryje	krýt	k5eAaImIp3nS	krýt
se	s	k7c7	s
zářením	záření	k1gNnSc7	záření
gama	gama	k1gNnSc1	gama
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgNnSc4d1	neutronové
záření	záření	k1gNnSc4	záření
–	–	k?	–
proud	proud	k1gInSc1	proud
volných	volný	k2eAgNnPc2d1	volné
neutronůZáření	neutronůZáření	k1gNnPc2	neutronůZáření
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
přímo	přímo	k6eAd1	přímo
ionizující	ionizující	k2eAgFnPc4d1	ionizující
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
proud	proud	k1gInSc4	proud
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
ionizující	ionizující	k2eAgFnSc1d1	ionizující
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neutrální	neutrální	k2eAgFnSc1d1	neutrální
částice	částice	k1gFnSc1	částice
interaguje	interagovat	k5eAaBmIp3nS	interagovat
a	a	k8xC	a
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
dochází	docházet	k5eAaImIp3nS	docházet
druhotně	druhotně	k6eAd1	druhotně
z	z	k7c2	z
výsledku	výsledek	k1gInSc2	výsledek
této	tento	k3xDgFnSc2	tento
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
elektrony	elektron	k1gInPc7	elektron
uvolněnými	uvolněný	k2eAgInPc7d1	uvolněný
při	při	k7c6	při
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
ionizační	ionizační	k2eAgMnPc1d1	ionizační
záření	záření	k1gNnSc3	záření
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
proud	proud	k1gInSc1	proud
nehmotných	hmotný	k2eNgInPc2d1	nehmotný
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
a	a	k8xC	a
záření	záření	k1gNnSc2	záření
tvořící	tvořící	k2eAgInSc4d1	tvořící
proud	proud	k1gInSc4	proud
hmotných	hmotný	k2eAgFnPc2d1	hmotná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
jádra	jádro	k1gNnPc1	jádro
helia	helium	k1gNnPc1	helium
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
pozitrony	pozitron	k1gInPc1	pozitron
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
kosmické	kosmický	k2eAgNnSc4d1	kosmické
záření	záření	k1gNnSc4	záření
</s>
</p>
<p>
<s>
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgInPc1d1	přírodní
radioizotopy	radioizotop	k1gInPc1	radioizotop
</s>
</p>
<p>
<s>
===	===	k?	===
Umělé	umělý	k2eAgInPc1d1	umělý
zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Urychlovače	urychlovač	k1gInPc1	urychlovač
částic	částice	k1gFnPc2	částice
-	-	kIx~	-
Cyklotron	cyklotron	k1gInSc1	cyklotron
<g/>
,	,	kIx,	,
Synchrotron	synchrotron	k1gInSc1	synchrotron
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
lineární	lineární	k2eAgInPc4d1	lineární
urychlovače	urychlovač	k1gInPc4	urychlovač
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
rentgenky	rentgenka	k1gFnPc1	rentgenka
(	(	kIx(	(
<g/>
Rentgen	rentgen	k1gInSc1	rentgen
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
,	,	kIx,	,
mamograf	mamograf	k1gInSc1	mamograf
<g/>
)	)	kIx)	)
a	a	k8xC	a
CRT	CRT	kA	CRT
obrazovky	obrazovka	k1gFnSc2	obrazovka
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
</s>
</p>
<p>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
reaktor	reaktor	k1gInSc1	reaktor
</s>
</p>
<p>
<s>
Uměle	uměle	k6eAd1	uměle
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
neptunium	neptunium	k1gNnSc1	neptunium
<g/>
,	,	kIx,	,
plutonium	plutonium	k1gNnSc1	plutonium
<g/>
,	,	kIx,	,
americium	americium	k1gNnSc1	americium
<g/>
,	,	kIx,	,
kalifornium	kalifornium	k1gNnSc1	kalifornium
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
scintilační	scintilační	k2eAgFnPc4d1	scintilační
a	a	k8xC	a
stopovací	stopovací	k2eAgFnPc4d1	stopovací
diagnostické	diagnostický	k2eAgFnPc4d1	diagnostická
metody	metoda	k1gFnPc4	metoda
</s>
</p>
<p>
<s>
Terapeutická	terapeutický	k2eAgNnPc4d1	terapeutické
zařízení	zařízení	k1gNnPc4	zařízení
-	-	kIx~	-
cesiové	cesiový	k2eAgNnSc4d1	cesiový
a	a	k8xC	a
kobaltové	kobaltový	k2eAgNnSc4d1	kobaltové
gama	gama	k1gNnSc4	gama
ozařovače	ozařovač	k1gInSc2	ozařovač
<g/>
,	,	kIx,	,
Leksellův	Leksellův	k2eAgMnSc1d1	Leksellův
gama-nůž	gamaůž	k6eAd1	gama-nůž
</s>
</p>
<p>
<s>
Radiofarmaka	Radiofarmak	k1gMnSc2	Radiofarmak
a	a	k8xC	a
tracery	tracera	k1gFnSc2	tracera
</s>
</p>
<p>
<s>
==	==	k?	==
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jak	jak	k6eAd1	jak
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
slabého	slabý	k2eAgNnSc2d1	slabé
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
krátkodobého	krátkodobý	k2eAgNnSc2d1	krátkodobé
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
<g/>
-li	i	k?	-li
na	na	k7c4	na
biologický	biologický	k2eAgInSc4d1	biologický
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
absorpci	absorpce	k1gFnSc3	absorpce
ionizujících	ionizující	k2eAgFnPc2d1	ionizující
částic	částice	k1gFnPc2	částice
nebo	nebo	k8xC	nebo
vlnění	vlnění	k1gNnSc1	vlnění
atomy	atom	k1gInPc1	atom
daného	daný	k2eAgInSc2d1	daný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vyrážení	vyrážení	k1gNnSc1	vyrážení
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
orbitalů	orbital	k1gInPc2	orbital
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
kladně	kladně	k6eAd1	kladně
nabitých	nabitý	k2eAgInPc2d1	nabitý
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
kationtů	kation	k1gInPc2	kation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ionizované	ionizovaný	k2eAgFnPc1d1	ionizovaná
části	část	k1gFnPc1	část
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
vysoce	vysoce	k6eAd1	vysoce
reaktivními	reaktivní	k2eAgInPc7d1	reaktivní
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
buňku	buňka	k1gFnSc4	buňka
buď	buď	k8xC	buď
rovnou	rovnou	k6eAd1	rovnou
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
(	(	kIx(	(
<g/>
reakce	reakce	k1gFnPc1	reakce
radikálů	radikál	k1gMnPc2	radikál
s	s	k7c7	s
DNA	DNA	kA	DNA
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
porušení	porušení	k1gNnSc4	porušení
fosfodiesterových	fosfodiesterův	k2eAgFnPc2d1	fosfodiesterův
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zpřetrhání	zpřetrhání	k1gNnSc4	zpřetrhání
jejího	její	k3xOp3gInSc2	její
řetězce	řetězec	k1gInSc2	řetězec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Detekce	detekce	k1gFnPc1	detekce
a	a	k8xC	a
měření	měření	k1gNnPc1	měření
==	==	k?	==
</s>
</p>
<p>
<s>
Detektory	detektor	k1gInPc1	detektor
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
podle	podle	k7c2	podle
nesené	nesený	k2eAgFnSc2d1	nesená
informace	informace	k1gFnSc2	informace
na	na	k7c4	na
detektory	detektor	k1gInPc4	detektor
počtu	počet	k1gInSc2	počet
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
nespektrometrické	spektrometrický	k2eNgInPc1d1	spektrometrický
detektory	detektor	k1gInPc1	detektor
<g/>
,	,	kIx,	,
určují	určovat	k5eAaImIp3nP	určovat
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
nezjistí	zjistit	k5eNaPmIp3nP	zjistit
energii	energie	k1gFnSc4	energie
ionizačního	ionizační	k2eAgNnSc2d1	ionizační
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
detektory	detektor	k1gInPc4	detektor
spektrometrické	spektrometrický	k2eAgInPc4d1	spektrometrický
(	(	kIx(	(
<g/>
zjistí	zjistit	k5eAaPmIp3nS	zjistit
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
energii	energie	k1gFnSc4	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
spektrometrického	spektrometrický	k2eAgInSc2d1	spektrometrický
detektoru	detektor	k1gInSc2	detektor
jsou	být	k5eAaImIp3nP	být
scintilační	scintilační	k2eAgInPc1d1	scintilační
detektory	detektor	k1gInPc1	detektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
jeho	jeho	k3xOp3gInPc2	jeho
účinků	účinek	k1gInPc2	účinek
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sievert	Sievert	k1gMnSc1	Sievert
</s>
</p>
<p>
<s>
Gray	Graa	k1gFnPc1	Graa
</s>
</p>
<p>
<s>
Rem	Rem	k?	Rem
</s>
</p>
<p>
<s>
Bequerel	Bequerel	k1gMnSc1	Bequerel
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
</s>
</p>
<p>
<s>
Radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
:	:	kIx,	:
Druhy	druh	k1gInPc1	druh
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
záření	záření	k1gNnSc2	záření
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ionizující	ionizující	k2eAgFnSc2d1	ionizující
záření	záření	k1gNnSc4	záření
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
použití	použití	k1gNnSc2	použití
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
</s>
</p>
<p>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
-	-	kIx~	-
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
vystaven	vystaven	k2eAgInSc1d1	vystaven
radiaci	radiace	k1gFnSc4	radiace
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
</s>
</p>
