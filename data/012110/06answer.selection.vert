<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
kvanta	kvantum	k1gNnSc2	kvantum
mají	mít	k5eAaImIp3nP	mít
energii	energie	k1gFnSc4	energie
postačující	postačující	k2eAgFnSc4d1	postačující
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
molekul	molekula	k1gFnPc2	molekula
ozářené	ozářený	k2eAgFnSc2d1	ozářená
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
