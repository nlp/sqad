<p>
<s>
Ježděnec	Ježděnec	k1gInSc1	Ježděnec
je	být	k5eAaImIp3nS	být
rybník	rybník	k1gInSc4	rybník
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Honbic	Honbice	k1gFnPc2	Honbice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podélně-oválný	podélněválný	k2eAgInSc4d1	podélně-oválný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
přitéká	přitékat	k5eAaImIp3nS	přitékat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
potokem	potok	k1gInSc7	potok
Ježděnka	Ježděnka	k1gFnSc1	Ježděnka
a	a	k8xC	a
odtéká	odtékat	k5eAaImIp3nS	odtékat
stavidlem	stavidlo	k1gNnSc7	stavidlo
a	a	k8xC	a
přepadem	přepad	k1gInSc7	přepad
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
tudy	tudy	k6eAd1	tudy
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
silnice	silnice	k1gFnSc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Ježděnec	Ježděnec	k1gInSc1	Ježděnec
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2,4	[number]	k4	2,4
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ježděnec	Ježděnec	k1gInSc1	Ježděnec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
