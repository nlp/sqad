<s>
Lanai	Lanai	k1gNnSc1	Lanai
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Pineapple	Pineapple	k1gFnPc1	Pineapple
Island	Island	k1gInSc1	Island
–	–	k?	–
Ananasový	ananasový	k2eAgInSc1d1	ananasový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgMnSc1	šestý
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
osmi	osm	k4xCc2	osm
hlavních	hlavní	k2eAgInPc2d1	hlavní
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
