<p>
<s>
Lanai	Lanai	k1gNnSc1	Lanai
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Pineapple	Pineapple	k1gFnPc1	Pineapple
Island	Island	k1gInSc1	Island
–	–	k?	–
Ananasový	ananasový	k2eAgInSc1d1	ananasový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgMnSc1	šestý
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
osmi	osm	k4xCc2	osm
hlavních	hlavní	k2eAgInPc2d1	hlavní
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
asi	asi	k9	asi
3100	[number]	k4	3100
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
363,97	[number]	k4	363,97
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Lanai	Lanai	k6eAd1	Lanai
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
státu	stát	k1gInSc2	stát
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
<g/>
.	.	kIx.	.
spolkovým	spolkový	k2eAgInSc7d1	spolkový
státem	stát	k1gInSc7	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lanai	Lana	k1gFnSc2	Lana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
