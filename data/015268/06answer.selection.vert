<s>
Oficiálním	oficiální	k2eAgInSc7d1
cílem	cíl	k1gInSc7
řádu	řád	k1gInSc2
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
odpovědět	odpovědět	k5eAaPmF
na	na	k7c4
hlad	hlad	k1gInSc4
lidí	člověk	k1gMnPc2
a	a	k8xC
zpřístupnit	zpřístupnit	k5eAaPmF
jim	on	k3xPp3gMnPc3
poklady	poklad	k1gInPc7
Boží	boží	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
v	v	k7c6
Eucharistii	eucharistie	k1gFnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Řehole	řehole	k1gFnSc1
<g/>
,	,	kIx,
článek	článek	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
V	v	k7c6
praxi	praxe	k1gFnSc6
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
prohlubovat	prohlubovat	k5eAaImF
úctu	úcta	k1gFnSc4
katolické	katolický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
k	k	k7c3
eucharistii	eucharistie	k1gFnSc3
<g/>
.	.	kIx.
</s>