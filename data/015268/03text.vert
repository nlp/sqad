<s>
Kongregace	kongregace	k1gFnSc1
kněží	kněz	k1gMnPc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Svátosti	svátost	k1gFnSc2
</s>
<s>
Eucharistiáni	Eucharistián	k1gMnPc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Julián	Julián	k1gMnSc1
Eymard	Eymard	k1gMnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1896	#num#	k4
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
1,8	1,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
4,5	4,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.ssscongregatio.org	www.ssscongregatio.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eucharistiáni	Eucharistián	k1gMnPc1
(	(	kIx(
<g/>
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Kongregace	kongregace	k1gFnSc2
kněží	kněz	k1gMnPc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Svátosti	svátost	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Congragatio	Congragatio	k6eAd1
Presbyterorum	Presbyterorum	k1gInSc1
a	a	k8xC
Sanctissimo	Sanctissima	k1gFnSc5
Sacramento	Sacramento	k1gNnSc4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
římskokatolickou	římskokatolický	k2eAgFnSc7d1
řeholní	řeholní	k2eAgFnSc7d1
kongregací	kongregace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1856	#num#	k4
sv.	sv.	kA
Petrem	Petr	k1gMnSc7
Juliánem	Julián	k1gMnSc7
Eymardem	Eymard	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eucharistiáni	Eucharistián	k1gMnPc1
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
„	„	k?
<g/>
řeholní	řeholní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
papežského	papežský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
generální	generální	k2eAgMnSc1d1
představený	představený	k1gMnSc1
řádu	řád	k1gInSc2
podléhá	podléhat	k5eAaImIp3nS
přímo	přímo	k6eAd1
papeži	papež	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Poslání	poslání	k1gNnSc1
řádu	řád	k1gInSc2
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1
cílem	cíl	k1gInSc7
řádu	řád	k1gInSc2
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
odpovědět	odpovědět	k5eAaPmF
na	na	k7c4
hlad	hlad	k1gInSc4
lidí	člověk	k1gMnPc2
a	a	k8xC
zpřístupnit	zpřístupnit	k5eAaPmF
jim	on	k3xPp3gMnPc3
poklady	poklad	k1gInPc7
Boží	boží	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
v	v	k7c6
Eucharistii	eucharistie	k1gFnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Řehole	řehole	k1gFnSc1
<g/>
,	,	kIx,
článek	článek	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
V	v	k7c6
praxi	praxe	k1gFnSc6
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
prohlubovat	prohlubovat	k5eAaImF
úctu	úcta	k1gFnSc4
katolické	katolický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
k	k	k7c3
eucharistii	eucharistie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
ČR	ČR	kA
existoval	existovat	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgMnPc1
čeští	český	k2eAgMnPc1d1
eucharistiáni	eucharistián	k1gMnPc1
patří	patřit	k5eAaImIp3nP
do	do	k7c2
komunity	komunita	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
studuje	studovat	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
bývalé	bývalý	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Rakousko-Jižní	rakousko-jižní	k2eAgNnSc4d1
Tyrolsko	Tyrolsko	k1gNnSc4
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Kongregace	kongregace	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Kongregace	kongregace	k1gFnSc2
v	v	k7c6
USA	USA	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
30933-3	30933-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156889558	#num#	k4
</s>
