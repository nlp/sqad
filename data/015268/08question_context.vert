<s>
Eucharistiáni	Eucharistián	k1gMnPc1
(	(	kIx(
<g/>
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Kongregace	kongregace	k1gFnSc2
kněží	kněz	k1gMnPc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Svátosti	svátost	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Congragatio	Congragatio	k6eAd1
Presbyterorum	Presbyterorum	k1gInSc1
a	a	k8xC
Sanctissimo	Sanctissima	k1gFnSc5
Sacramento	Sacramento	k1gNnSc4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
římskokatolickou	římskokatolický	k2eAgFnSc7d1
řeholní	řeholní	k2eAgFnSc7d1
kongregací	kongregace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1856	#num#	k4
sv.	sv.	kA
Petrem	Petr	k1gMnSc7
Juliánem	Julián	k1gMnSc7
Eymardem	Eymard	k1gMnSc7
<g/>
.	.	kIx.
</s>