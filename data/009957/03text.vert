<p>
<s>
Sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
homogenní	homogenní	k2eAgFnSc1d1	homogenní
a	a	k8xC	a
amorfní	amorfní	k2eAgFnSc1d1	amorfní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nekrystalická	krystalický	k2eNgFnSc1d1	nekrystalická
<g/>
)	)	kIx)	)
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
poměrně	poměrně	k6eAd1	poměrně
rychlým	rychlý	k2eAgNnSc7d1	rychlé
ochlazením	ochlazení	k1gNnSc7	ochlazení
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
nestačí	stačit	k5eNaBmIp3nS	stačit
vytvořit	vytvořit	k5eAaPmF	vytvořit
krystalovou	krystalový	k2eAgFnSc4d1	krystalová
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgInSc4d3	veliký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
se	s	k7c7	s
sklem	sklo	k1gNnSc7	sklo
rozumí	rozumět	k5eAaImIp3nS	rozumět
obvykle	obvykle	k6eAd1	obvykle
právě	právě	k6eAd1	právě
křemičité	křemičitý	k2eAgNnSc4d1	křemičité
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
surovinou	surovina	k1gFnSc7	surovina
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
je	být	k5eAaImIp3nS	být
sklářský	sklářský	k2eAgInSc4d1	sklářský
písek	písek	k1gInSc4	písek
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
teplota	teplota	k1gFnSc1	teplota
tavení	tavení	k1gNnSc2	tavení
SiO	SiO	k1gFnSc2	SiO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
různé	různý	k2eAgFnPc4d1	různá
přísady	přísada	k1gFnPc4	přísada
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
soda	soda	k1gFnSc1	soda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
potaš	potaš	k1gInSc1	potaš
<g/>
)	)	kIx)	)
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
pálené	pálený	k2eAgNnSc1d1	pálené
vápno	vápno	k1gNnSc1	vápno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
chemickou	chemický	k2eAgFnSc4d1	chemická
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejběžnější	běžný	k2eAgNnSc4d3	nejběžnější
sodno-vápenaté	sodnoápenatý	k2eAgNnSc4d1	sodno-vápenatý
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
okenní	okenní	k2eAgFnPc1d1	okenní
tabule	tabule	k1gFnPc1	tabule
<g/>
,	,	kIx,	,
skleněné	skleněný	k2eAgFnPc1d1	skleněná
nádoby	nádoba	k1gFnPc1	nádoba
a	a	k8xC	a
lahve	lahev	k1gFnPc1	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Křemičité	křemičitý	k2eAgNnSc1d1	křemičité
sklo	sklo	k1gNnSc1	sklo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jako	jako	k9	jako
sopečný	sopečný	k2eAgInSc1d1	sopečný
obsidián	obsidián	k1gInSc1	obsidián
a	a	k8xC	a
meteorický	meteorický	k2eAgInSc1d1	meteorický
vltavín	vltavín	k1gInSc1	vltavín
(	(	kIx(	(
<g/>
moldavit	moldavit	k1gInSc1	moldavit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklu	sklo	k1gNnSc3	sklo
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
také	také	k9	také
skrytě	skrytě	k6eAd1	skrytě
krystalický	krystalický	k2eAgInSc4d1	krystalický
pazourek	pazourek	k1gInSc4	pazourek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
užívaném	užívaný	k2eAgInSc6d1	užívaný
ve	v	k7c6	v
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sklem	sklo	k1gNnSc7	sklo
rozumí	rozumět	k5eAaImIp3nS	rozumět
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
amorfních	amorfní	k2eAgFnPc2d1	amorfní
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kovových	kovový	k2eAgInPc2d1	kovový
nebo	nebo	k8xC	nebo
polymerových	polymerový	k2eAgInPc2d1	polymerový
<g/>
,	,	kIx,	,
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
porcelán	porcelán	k1gInSc4	porcelán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Machek	Machek	k1gInSc1	Machek
a	a	k8xC	a
J.	J.	kA	J.
Rejzek	Rejzek	k1gInSc1	Rejzek
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
původ	původ	k1gInSc4	původ
českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
pojmů	pojem	k1gInPc2	pojem
(	(	kIx(	(
<g/>
polské	polský	k2eAgNnSc1d1	polské
szkło	szkło	k1gNnSc1	szkło
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgNnSc1d1	ruské
stekló	stekló	k?	stekló
<g/>
,	,	kIx,	,
srbochorvatské	srbochorvatský	k2eAgFnSc2d1	srbochorvatský
stà	stà	k?	stà
<g/>
,	,	kIx,	,
staroslověnské	staroslověnský	k2eAgFnSc6d1	staroslověnská
stъ	stъ	k?	stъ
<g/>
)	)	kIx)	)
odvozených	odvozený	k2eAgMnPc2d1	odvozený
z	z	k7c2	z
praslovanského	praslovanský	k2eAgInSc2d1	praslovanský
základu	základ	k1gInSc2	základ
*	*	kIx~	*
<g/>
stъ	stъ	k?	stъ
<g/>
,	,	kIx,	,
z	z	k7c2	z
gótského	gótský	k2eAgInSc2d1	gótský
stikls	stikls	k6eAd1	stikls
nebo	nebo	k8xC	nebo
staroněmeckého	staroněmecký	k2eAgMnSc2d1	staroněmecký
stehhal	stehhal	k1gMnSc1	stehhal
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
západogermánského	západogermánský	k2eAgInSc2d1	západogermánský
*	*	kIx~	*
<g/>
stikla-	stikla-	k?	stikla-
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
znamenaly	znamenat	k5eAaImAgInP	znamenat
"	"	kIx"	"
<g/>
pohár	pohár	k1gInSc1	pohár
s	s	k7c7	s
hrotem	hrot	k1gInSc7	hrot
na	na	k7c4	na
zapíchnutí	zapíchnutí	k1gNnSc4	zapíchnutí
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
glass	glass	k1gInSc4	glass
i	i	k8xC	i
německé	německý	k2eAgInPc4d1	německý
Glas	Glas	k1gInSc4	Glas
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
germánského	germánský	k2eAgMnSc2d1	germánský
i	i	k8xC	i
staroanglického	staroanglický	k2eAgMnSc2d1	staroanglický
glaes	glaes	k1gMnSc1	glaes
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podle	podle	k7c2	podle
Tacita	Tacitum	k1gNnSc2	Tacitum
a	a	k8xC	a
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starý	k2eAgInSc2d2	starší
u	u	k7c2	u
Germánů	Germán	k1gMnPc2	Germán
znamenalo	znamenat	k5eAaImAgNnS	znamenat
jantar	jantar	k1gInSc4	jantar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
internetového	internetový	k2eAgInSc2d1	internetový
etymologického	etymologický	k2eAgInSc2d1	etymologický
slovníku	slovník	k1gInSc2	slovník
angličtiny	angličtina	k1gFnSc2	angličtina
slovo	slovo	k1gNnSc1	slovo
glass	glass	k1gInSc1	glass
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
protogermánského	protogermánský	k2eAgMnSc2d1	protogermánský
*	*	kIx~	*
<g/>
glasam	glasam	k1gInSc1	glasam
a	a	k8xC	a
protoindoevropského	protoindoevropský	k2eAgInSc2d1	protoindoevropský
*	*	kIx~	*
<g/>
ghel-	ghel-	k?	ghel-
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
zářit	zářit	k5eAaImF	zářit
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
příbuzná	příbuzný	k2eAgNnPc1d1	příbuzné
například	například	k6eAd1	například
slova	slovo	k1gNnSc2	slovo
žlutý	žlutý	k2eAgMnSc1d1	žlutý
a	a	k8xC	a
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
přírodní	přírodní	k2eAgInSc1d1	přírodní
obsidián	obsidián	k1gInSc1	obsidián
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
ostrých	ostrý	k2eAgInPc2d1	ostrý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
sklo	sklo	k1gNnSc1	sklo
častým	častý	k2eAgInSc7d1	častý
předmětem	předmět	k1gInSc7	předmět
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgNnSc4	první
skutečné	skutečný	k2eAgNnSc4d1	skutečné
sklo	sklo	k1gNnSc4	sklo
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
známé	známý	k2eAgInPc1d1	známý
skleněné	skleněný	k2eAgInPc1d1	skleněný
objekty	objekt	k1gInPc1	objekt
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
třetího	třetí	k4xOgNnSc2	třetí
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
před	před	k7c4	před
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgInP	být
korálky	korálek	k1gInPc1	korálek
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
možná	možná	k9	možná
jako	jako	k9	jako
náhodný	náhodný	k2eAgInSc1d1	náhodný
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
kovové	kovový	k2eAgFnSc2d1	kovová
rudy	ruda	k1gFnSc2	ruda
nebo	nebo	k8xC	nebo
při	při	k7c6	při
glazování	glazování	k1gNnSc6	glazování
egyptské	egyptský	k2eAgFnSc2d1	egyptská
keramiky	keramika	k1gFnSc2	keramika
(	(	kIx(	(
<g/>
fajánse	fajáns	k1gFnSc2	fajáns
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nečisté	čistý	k2eNgNnSc1d1	nečisté
a	a	k8xC	a
užívalo	užívat	k5eAaImAgNnS	užívat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
ozdob	ozdoba	k1gFnPc2	ozdoba
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
amuletů	amulet	k1gInPc2	amulet
a	a	k8xC	a
malých	malý	k2eAgFnPc2d1	malá
nádobek	nádobka	k1gFnPc2	nádobka
na	na	k7c4	na
vonné	vonný	k2eAgInPc4d1	vonný
oleje	olej	k1gInPc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
oxid	oxid	k1gInSc4	oxid
křemičitý	křemičitý	k2eAgInSc4d1	křemičitý
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
sodu	soda	k1gFnSc4	soda
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
sodno-vápenaté	sodnoápenatý	k2eAgNnSc4d1	sodno-vápenatý
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
datovaný	datovaný	k2eAgInSc1d1	datovaný
skleněný	skleněný	k2eAgInSc1d1	skleněný
předmět	předmět	k1gInSc1	předmět
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
keramický	keramický	k2eAgInSc1d1	keramický
pohár	pohár	k1gInSc1	pohár
Thutmose	Thutmosa	k1gFnSc3	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
s	s	k7c7	s
nanášenou	nanášený	k2eAgFnSc7d1	nanášená
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
výzdobou	výzdoba	k1gFnSc7	výzdoba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1450	[number]	k4	1450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokrok	pokrok	k1gInSc1	pokrok
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
udělali	udělat	k5eAaPmAgMnP	udělat
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
čiré	čirý	k2eAgNnSc4d1	čiré
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
císařské	císařský	k2eAgFnSc6d1	císařská
době	doba	k1gFnSc6	doba
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výroba	výroba	k1gFnSc1	výroba
skleněných	skleněný	k2eAgInPc2d1	skleněný
pohárů	pohár	k1gInPc2	pohár
a	a	k8xC	a
nádob	nádoba	k1gFnPc2	nádoba
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc2d1	vysoká
technické	technický	k2eAgFnSc2d1	technická
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
patří	patřit	k5eAaImIp3nS	patřit
skleněné	skleněný	k2eAgInPc4d1	skleněný
dvouvrstvé	dvouvrstvý	k2eAgInPc4d1	dvouvrstvý
poháry	pohár	k1gInPc4	pohár
(	(	kIx(	(
<g/>
diatreton	diatreton	k1gInSc4	diatreton
<g/>
)	)	kIx)	)
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	on	k3xPp3gFnPc4	on
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
nádoba	nádoba	k1gFnSc1	nádoba
a	a	k8xC	a
svrchní	svrchní	k2eAgFnSc1d1	svrchní
dekorativní	dekorativní	k2eAgFnSc1d1	dekorativní
klec	klec	k1gFnSc1	klec
či	či	k8xC	či
mřížka	mřížka	k1gFnSc1	mřížka
z	z	k7c2	z
barevného	barevný	k2eAgNnSc2d1	barevné
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
pohárem	pohár	k1gInSc7	pohár
spojená	spojený	k2eAgFnSc1d1	spojená
skleněnými	skleněný	k2eAgInPc7d1	skleněný
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
patrně	patrně	k6eAd1	patrně
odbroušením	odbroušení	k1gNnSc7	odbroušení
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
svrchní	svrchní	k2eAgFnSc2d1	svrchní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc4	způsob
výroby	výroba	k1gFnSc2	výroba
však	však	k9	však
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
okna	okno	k1gNnPc1	okno
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
katedrál	katedrála	k1gFnPc2	katedrála
zasklívat	zasklívat	k5eAaImF	zasklívat
často	často	k6eAd1	často
figurálními	figurální	k2eAgFnPc7d1	figurální
barevnými	barevný	k2eAgFnPc7d1	barevná
vitrážemi	vitráž	k1gFnPc7	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
francouzských	francouzský	k2eAgFnPc2d1	francouzská
katedrál	katedrála	k1gFnPc2	katedrála
(	(	kIx(	(
<g/>
Katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Chartres	Chartres	k1gInSc1	Chartres
<g/>
,	,	kIx,	,
Soissons	Soissons	k1gInSc1	Soissons
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zasklívají	zasklívat	k5eAaImIp3nP	zasklívat
i	i	k8xC	i
okna	okno	k1gNnPc1	okno
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
radnic	radnice	k1gFnPc2	radnice
a	a	k8xC	a
bohatých	bohatý	k2eAgInPc2d1	bohatý
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
technikou	technika	k1gFnSc7	technika
vitráže	vitráž	k1gFnSc2	vitráž
<g/>
,	,	kIx,	,
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
většími	veliký	k2eAgFnPc7d2	veliký
tabulkami	tabulka	k1gFnPc7	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
střediskem	středisko	k1gNnSc7	středisko
výroby	výroba	k1gFnSc2	výroba
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
ozdobného	ozdobný	k2eAgNnSc2d1	ozdobné
a	a	k8xC	a
luxusního	luxusní	k2eAgNnSc2d1	luxusní
skla	sklo	k1gNnSc2	sklo
jsou	být	k5eAaImIp3nP	být
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
také	také	k9	také
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
křišťál	křišťál	k1gInSc1	křišťál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
pro	pro	k7c4	pro
optické	optický	k2eAgInPc4d1	optický
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
čočky	čočka	k1gFnPc4	čočka
<g/>
,	,	kIx,	,
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
<g/>
,	,	kIx,	,
hranoly	hranol	k1gInPc1	hranol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkých	velký	k2eAgNnPc2d1	velké
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
tabulí	tabule	k1gFnPc2	tabule
a	a	k8xC	a
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
masová	masový	k2eAgFnSc1d1	masová
tovární	tovární	k2eAgFnSc1d1	tovární
výroba	výroba	k1gFnSc1	výroba
tabulového	tabulový	k2eAgNnSc2d1	tabulové
i	i	k8xC	i
obalového	obalový	k2eAgNnSc2d1	obalové
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
výrobní	výrobní	k2eAgFnPc4d1	výrobní
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgInPc1d1	nový
druhy	druh	k1gInPc1	druh
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
nová	nový	k2eAgNnPc4d1	nové
použití	použití	k1gNnPc4	použití
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technice	technika	k1gFnSc6	technika
(	(	kIx(	(
<g/>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
vata	vata	k1gFnSc1	vata
<g/>
,	,	kIx,	,
vakuová	vakuový	k2eAgFnSc1d1	vakuová
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
obrazovky	obrazovka	k1gFnPc1	obrazovka
<g/>
,	,	kIx,	,
optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Sklo	sklo	k1gNnSc1	sklo
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
důležitým	důležitý	k2eAgInSc7d1	důležitý
stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
(	(	kIx(	(
<g/>
velkoplošná	velkoplošný	k2eAgNnPc1d1	velkoplošné
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
obklady	obklad	k1gInPc1	obklad
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
drátosklo	drátosknout	k5eAaPmAgNnS	drátosknout
<g/>
,	,	kIx,	,
luxfery	luxfer	k1gInPc1	luxfer
<g/>
,	,	kIx,	,
pěnové	pěnový	k2eAgNnSc1d1	pěnové
sklo	sklo	k1gNnSc1	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
technice	technika	k1gFnSc6	technika
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
než	než	k8xS	než
křemičitá	křemičitý	k2eAgNnPc4d1	křemičité
skla	sklo	k1gNnPc4	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křemičité	křemičitý	k2eAgNnSc1d1	křemičité
sklo	sklo	k1gNnSc1	sklo
==	==	k?	==
</s>
</p>
<p>
<s>
Čiré	čirý	k2eAgNnSc1d1	čiré
křemičité	křemičitý	k2eAgNnSc1d1	křemičité
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
průhledný	průhledný	k2eAgInSc4d1	průhledný
nebo	nebo	k8xC	nebo
průsvitný	průsvitný	k2eAgInSc4d1	průsvitný
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
pevný	pevný	k2eAgInSc1d1	pevný
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
inertní	inertní	k2eAgNnPc1d1	inertní
a	a	k8xC	a
biologicky	biologicky	k6eAd1	biologicky
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
libovolně	libovolně	k6eAd1	libovolně
tvarovat	tvarovat	k5eAaImF	tvarovat
<g/>
,	,	kIx,	,
brousit	brousit	k5eAaImF	brousit
<g/>
,	,	kIx,	,
barvit	barvit	k5eAaImF	barvit
a	a	k8xC	a
zdobit	zdobit	k5eAaImF	zdobit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
žádané	žádaný	k2eAgFnPc4d1	žádaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jej	on	k3xPp3gMnSc4	on
předurčují	předurčovat	k5eAaImIp3nP	předurčovat
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnPc1d1	lidská
činnosti	činnost	k1gFnPc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgNnSc1d1	křehké
a	a	k8xC	a
rozbíjí	rozbíjet	k5eAaImIp3nS	rozbíjet
se	se	k3xPyFc4	se
na	na	k7c4	na
ostré	ostrý	k2eAgInPc4d1	ostrý
střepy	střep	k1gInPc4	střep
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
lze	lze	k6eAd1	lze
přidáním	přidání	k1gNnSc7	přidání
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
viskózní	viskózní	k2eAgFnSc2d1	viskózní
skloviny	sklovina	k1gFnSc2	sklovina
roztavené	roztavený	k2eAgFnSc2d1	roztavená
ve	v	k7c6	v
sklářské	sklářský	k2eAgFnSc6d1	sklářská
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zchladí	zchladit	k5eAaPmIp3nS	zchladit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemá	mít	k5eNaImIp3nS	mít
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
na	na	k7c4	na
zformování	zformování	k1gNnSc4	zformování
regulérní	regulérní	k2eAgFnSc2d1	regulérní
krystalové	krystalový	k2eAgFnSc2d1	krystalová
mřížky	mřížka	k1gFnSc2	mřížka
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
když	když	k8xS	když
se	se	k3xPyFc4	se
konzumní	konzumní	k2eAgInSc1d1	konzumní
cukr	cukr	k1gInSc1	cukr
roztaví	roztavit	k5eAaPmIp3nS	roztavit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
zchladí	zchladit	k5eAaPmIp3nS	zchladit
vylitím	vylití	k1gNnSc7	vylití
na	na	k7c4	na
chladný	chladný	k2eAgInSc4d1	chladný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
tuhá	tuhý	k2eAgFnSc1d1	tuhá
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
amorfní	amorfní	k2eAgFnSc1d1	amorfní
<g/>
,	,	kIx,	,
s	s	k7c7	s
konchoidální	konchoidální	k2eAgFnSc7d1	konchoidální
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
ne	ne	k9	ne
krystalická	krystalický	k2eAgFnSc1d1	krystalická
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
cukr	cukr	k1gInSc4	cukr
před	před	k7c7	před
roztavením	roztavení	k1gNnSc7	roztavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sklo	sklo	k1gNnSc1	sklo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
křemeni	křemen	k1gInSc6	křemen
nebo	nebo	k8xC	nebo
křemičitém	křemičitý	k2eAgInSc6d1	křemičitý
písku	písek	k1gInSc6	písek
(	(	kIx(	(
<g/>
též	též	k9	též
sklářském	sklářský	k2eAgInSc6d1	sklářský
písku	písek	k1gInSc6	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Křemen	křemen	k1gInSc1	křemen
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
kolem	kolem	k7c2	kolem
2000	[number]	k4	2000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
přidávají	přidávat	k5eAaImIp3nP	přidávat
alkalické	alkalický	k2eAgFnPc1d1	alkalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
soda	soda	k1gFnSc1	soda
a	a	k8xC	a
potaš	potaš	k1gFnSc1	potaš
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc3	tání
na	na	k7c4	na
asi	asi	k9	asi
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
Protože	protože	k8xS	protože
alkálie	alkálie	k1gFnPc1	alkálie
snižují	snižovat	k5eAaImIp3nP	snižovat
odolnost	odolnost	k1gFnSc4	odolnost
skla	sklo	k1gNnSc2	sklo
vůči	vůči	k7c3	vůči
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
také	také	k9	také
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tuto	tento	k3xDgFnSc4	tento
odolnost	odolnost	k1gFnSc4	odolnost
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
měření	měření	k1gNnSc6	měření
na	na	k7c6	na
středověkých	středověký	k2eAgFnPc6d1	středověká
vitrážích	vitráž	k1gFnPc6	vitráž
byla	být	k5eAaImAgNnP	být
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
sklíčka	sklíčko	k1gNnPc1	sklíčko
často	často	k6eAd1	často
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
okraji	okraj	k1gInSc6	okraj
silnější	silný	k2eAgMnSc1d2	silnější
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mylná	mylný	k2eAgFnSc1d1	mylná
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
roztok	roztok	k1gInSc4	roztok
nebo	nebo	k8xC	nebo
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
období	období	k1gNnSc6	období
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
tloušťce	tloušťka	k1gFnSc6	tloušťka
těchto	tento	k3xDgNnPc2	tento
skel	sklo	k1gNnPc2	sklo
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
už	už	k6eAd1	už
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nalezly	naleznout	k5eAaPmAgInP	naleznout
i	i	k9	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sklíčko	sklíčko	k1gNnSc1	sklíčko
nahoře	nahoře	k6eAd1	nahoře
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
starověké	starověký	k2eAgInPc4d1	starověký
skleněné	skleněný	k2eAgInPc4d1	skleněný
výrobky	výrobek	k1gInPc4	výrobek
ani	ani	k8xC	ani
staré	starý	k2eAgInPc4d1	starý
optické	optický	k2eAgInPc4d1	optický
přístroje	přístroj	k1gInPc4	přístroj
žádnou	žádný	k3yNgFnSc4	žádný
takovou	takový	k3xDgFnSc4	takový
deformaci	deformace	k1gFnSc6	deformace
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Složení	složení	k1gNnSc1	složení
křemičitého	křemičitý	k2eAgNnSc2d1	křemičité
skla	sklo	k1gNnSc2	sklo
===	===	k?	===
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
druhy	druh	k1gInPc1	druh
křemičitého	křemičitý	k2eAgNnSc2d1	křemičité
skla	sklo	k1gNnSc2	sklo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
složení	složení	k1gNnSc2	složení
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Křemenné	křemenný	k2eAgNnSc1d1	křemenné
sklo	sklo	k1gNnSc1	sklo
nebo	nebo	k8xC	nebo
tavený	tavený	k2eAgInSc1d1	tavený
křemen	křemen	k1gInSc1	křemen
vzniká	vznikat	k5eAaImIp3nS	vznikat
tavením	tavení	k1gNnSc7	tavení
čistého	čistý	k2eAgInSc2d1	čistý
SiO	SiO	k1gMnSc4	SiO
<g/>
2	[number]	k4	2
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
odolné	odolný	k2eAgFnPc1d1	odolná
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
do	do	k7c2	do
1500	[number]	k4	1500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odolné	odolný	k2eAgNnSc4d1	odolné
vůči	vůči	k7c3	vůči
povětrnostním	povětrnostní	k2eAgInPc3d1	povětrnostní
vlivům	vliv	k1gInPc3	vliv
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
malou	malý	k2eAgFnSc4d1	malá
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
roztažnost	roztažnost	k1gFnSc4	roztažnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průhlednější	průhledný	k2eAgMnSc1d2	průhlednější
a	a	k8xC	a
propouští	propouštět	k5eAaImIp3nS	propouštět
i	i	k9	i
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
UVA	UVA	kA	UVA
<g/>
,	,	kIx,	,
UVB	UVB	kA	UVB
<g/>
,	,	kIx,	,
UVC	UVC	kA	UVC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
teplotě	teplota	k1gFnSc3	teplota
tavení	tavení	k1gNnSc2	tavení
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
energeticky	energeticky	k6eAd1	energeticky
náročnější	náročný	k2eAgFnSc1d2	náročnější
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
<g/>
,	,	kIx,	,
vakuové	vakuový	k2eAgFnSc6d1	vakuová
technice	technika	k1gFnSc6	technika
a	a	k8xC	a
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tepelně	tepelně	k6eAd1	tepelně
namáhaných	namáhaný	k2eAgFnPc2d1	namáhaná
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sodno-vápenaté	Sodnoápenatý	k2eAgNnSc1d1	Sodno-vápenatý
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
plochého	plochý	k2eAgNnSc2d1	ploché
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
na	na	k7c4	na
okenní	okenní	k2eAgFnPc4d1	okenní
tabule	tabule	k1gFnPc4	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
72	[number]	k4	72
<g/>
%	%	kIx~	%
SiO	SiO	k1gFnPc2	SiO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
(	(	kIx(	(
<g/>
CaO	CaO	k1gFnSc1	CaO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
(	(	kIx(	(
<g/>
MgO	MgO	k1gFnSc1	MgO
<g/>
)	)	kIx)	)
a	a	k8xC	a
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
průhledné	průhledný	k2eAgNnSc1d1	průhledné
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
tvaruje	tvarovat	k5eAaImIp3nS	tvarovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotní	teplotní	k2eAgFnSc4d1	teplotní
roztažnost	roztažnost	k1gFnSc4	roztažnost
a	a	k8xC	a
odolává	odolávat	k5eAaImIp3nS	odolávat
teplotě	teplota	k1gFnSc3	teplota
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Propouští	propouštět	k5eAaImIp3nS	propouštět
UVA	UVA	kA	UVA
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
340	[number]	k4	340
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
nm	nm	k?	nm
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
nepropouští	propouštět	k5eNaImIp3nP	propouštět
UVB	UVB	kA	UVB
a	a	k8xC	a
UVC	UVC	kA	UVC
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
315	[number]	k4	315
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
pod	pod	k7c7	pod
sklem	sklo	k1gNnSc7	sklo
sluneční	sluneční	k2eAgFnSc2d1	sluneční
záření	záření	k1gNnSc3	záření
neopaluje	opalovat	k5eNaImIp3nS	opalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obalové	obalový	k2eAgNnSc1d1	obalové
sklo	sklo	k1gNnSc1	sklo
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc1d1	podobné
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
na	na	k7c4	na
hnědo	hnědo	k1gNnSc4	hnědo
nebo	nebo	k8xC	nebo
na	na	k7c4	na
zeleno	zeleno	k1gNnSc4	zeleno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odfiltrovalo	odfiltrovat	k5eAaPmAgNnS	odfiltrovat
veškeré	veškerý	k3xTgFnPc4	veškerý
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
na	na	k7c4	na
běžné	běžný	k2eAgFnPc4d1	běžná
láhve	láhev	k1gFnPc4	láhev
a	a	k8xC	a
sklenice	sklenice	k1gFnPc4	sklenice
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
skleněná	skleněný	k2eAgFnSc1d1	skleněná
drť	drť	k1gFnSc1	drť
z	z	k7c2	z
recyklovaného	recyklovaný	k2eAgNnSc2d1	recyklované
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
šetří	šetřit	k5eAaImIp3nS	šetřit
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borosilikátové	Borosilikátový	k2eAgNnSc1d1	Borosilikátové
sklo	sklo	k1gNnSc1	sklo
(	(	kIx(	(
<g/>
Pyrex	pyrex	k1gInSc1	pyrex
<g/>
,	,	kIx,	,
SIMAX	SIMAX	kA	SIMAX
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
roztažnost	roztažnost	k1gFnSc4	roztažnost
a	a	k8xC	a
odolává	odolávat	k5eAaImIp3nS	odolávat
lépe	dobře	k6eAd2	dobře
teplotám	teplota	k1gFnPc3	teplota
včetně	včetně	k7c2	včetně
teplotních	teplotní	k2eAgInPc2d1	teplotní
šoků	šok	k1gInPc2	šok
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
chemické	chemický	k2eAgNnSc1d1	chemické
a	a	k8xC	a
varné	varný	k2eAgNnSc1d1	varné
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
na	na	k7c6	na
světla	světlo	k1gNnSc2	světlo
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
atd.	atd.	kA	atd.
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
81	[number]	k4	81
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
bóritého	bóritý	k2eAgInSc2d1	bóritý
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olovnaté	olovnatý	k2eAgNnSc1d1	olovnaté
sklo	sklo	k1gNnSc1	sklo
či	či	k8xC	či
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
křišťál	křišťál	k1gInSc1	křišťál
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
specifickou	specifický	k2eAgFnSc4d1	specifická
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vysoký	vysoký	k2eAgInSc4d1	vysoký
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
zářivější	zářivý	k2eAgFnPc1d2	zářivější
a	a	k8xC	a
také	také	k9	také
pružnější	pružný	k2eAgInSc1d2	pružnější
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
(	(	kIx(	(
<g/>
flintové	flintový	k2eAgNnSc1d1	flintové
sklo	sklo	k1gNnSc1	sklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
broušeného	broušený	k2eAgNnSc2d1	broušené
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
bižuterie	bižuterie	k1gFnSc2	bižuterie
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
59	[number]	k4	59
<g/>
%	%	kIx~	%
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
PbO	PbO	k1gFnSc1	PbO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
%	%	kIx~	%
sody	soda	k1gFnSc2	soda
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
zinku	zinek	k1gInSc2	zinek
(	(	kIx(	(
<g/>
ZnO	ZnO	k1gFnSc1	ZnO
<g/>
)	)	kIx)	)
a	a	k8xC	a
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hliníko-křemičité	Hliníkořemičitý	k2eAgNnSc1d1	Hliníko-křemičitý
sklo	sklo	k1gNnSc1	sklo
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
do	do	k7c2	do
sklolaminátů	sklolaminát	k1gInPc2	sklolaminát
a	a	k8xC	a
na	na	k7c4	na
baňky	baňka	k1gFnPc4	baňka
halogenových	halogenový	k2eAgFnPc2d1	halogenová
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sklo	sklo	k1gNnSc1	sklo
podle	podle	k7c2	podle
užití	užití	k1gNnSc2	užití
===	===	k?	===
</s>
</p>
<p>
<s>
Ploché	plochý	k2eAgNnSc1d1	ploché
sklo	sklo	k1gNnSc1	sklo
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zasklívání	zasklívání	k1gNnSc3	zasklívání
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
skleníků	skleník	k1gInPc2	skleník
<g/>
,	,	kIx,	,
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
k	k	k7c3	k
obkládání	obkládání	k1gNnSc3	obkládání
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
rámování	rámování	k1gNnSc1	rámování
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
musela	muset	k5eAaImAgFnS	muset
brousit	brousit	k5eAaImF	brousit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
technologie	technologie	k1gFnSc1	technologie
floating	floating	k1gInSc1	floating
už	už	k9	už
žádné	žádný	k3yNgNnSc1	žádný
broušení	broušení	k1gNnSc1	broušení
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
do	do	k7c2	do
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
se	se	k3xPyFc4	se
často	často	k6eAd1	často
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
například	například	k6eAd1	například
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
infračervené	infračervený	k2eAgInPc4d1	infračervený
paprsky	paprsek	k1gInPc4	paprsek
(	(	kIx(	(
<g/>
determální	determální	k2eAgNnSc4d1	determální
sklo	sklo	k1gNnSc4	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obalové	obalový	k2eAgNnSc1d1	obalové
sklo	sklo	k1gNnSc1	sklo
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
sklenic	sklenice	k1gFnPc2	sklenice
a	a	k8xC	a
skleněného	skleněný	k2eAgNnSc2d1	skleněné
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
,	,	kIx,	,
obalů	obal	k1gInPc2	obal
na	na	k7c4	na
kosmetiku	kosmetika	k1gFnSc4	kosmetika
a	a	k8xC	a
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Technické	technický	k2eAgNnSc1d1	technické
sklo	sklo	k1gNnSc1	sklo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
skleněné	skleněný	k2eAgInPc4d1	skleněný
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
součásti	součást	k1gFnPc4	součást
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
včetně	včetně	k7c2	včetně
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
(	(	kIx(	(
<g/>
izolátory	izolátor	k1gInPc4	izolátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vakuovou	vakuový	k2eAgFnSc4d1	vakuová
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgNnSc1d1	optické
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
čiré	čirý	k2eAgNnSc1d1	čiré
sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesně	přesně	k6eAd1	přesně
definovanými	definovaný	k2eAgFnPc7d1	definovaná
optickými	optický	k2eAgFnPc7d1	optická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
flintové	flintový	k2eAgNnSc1d1	flintové
sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
a	a	k8xC	a
korunové	korunový	k2eAgNnSc4d1	korunové
sklo	sklo	k1gNnSc4	sklo
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autosklo	Autosknout	k5eAaPmAgNnS	Autosknout
je	on	k3xPp3gNnSc4	on
obvykle	obvykle	k6eAd1	obvykle
vrstvené	vrstvený	k2eAgNnSc4d1	vrstvené
(	(	kIx(	(
<g/>
laminované	laminovaný	k2eAgNnSc4d1	laminované
<g/>
)	)	kIx)	)
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
nevysype	vysypat	k5eNaPmIp3nS	vysypat
a	a	k8xC	a
netvoří	tvořit	k5eNaImIp3nS	tvořit
velké	velký	k2eAgInPc4d1	velký
střepy	střep	k1gInPc4	střep
<g/>
.	.	kIx.	.
</s>
<s>
Autoskla	Autosknout	k5eAaPmAgFnS	Autosknout
se	se	k3xPyFc4	se
také	také	k9	také
tepelně	tepelně	k6eAd1	tepelně
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
(	(	kIx(	(
<g/>
kalené	kalený	k2eAgNnSc1d1	kalené
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
se	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
pnutím	pnutí	k1gNnSc7	pnutí
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
kousky	kousek	k1gInPc4	kousek
<g/>
)	)	kIx)	)
a	a	k8xC	a
často	často	k6eAd1	často
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neprůstřelné	průstřelný	k2eNgNnSc1d1	neprůstřelné
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
vícevrstvý	vícevrstvý	k2eAgInSc4d1	vícevrstvý
sendvič	sendvič	k1gInSc4	sendvič
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tlumí	tlumit	k5eAaImIp3nP	tlumit
energii	energie	k1gFnSc4	energie
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
sklo	sklo	k1gNnSc1	sklo
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
ručně	ručně	k6eAd1	ručně
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
velmi	velmi	k6eAd1	velmi
rozmanitých	rozmanitý	k2eAgFnPc2d1	rozmanitá
technik	technika	k1gFnPc2	technika
foukání	foukání	k1gNnSc2	foukání
<g/>
,	,	kIx,	,
ohýbání	ohýbání	k1gNnSc2	ohýbání
<g/>
,	,	kIx,	,
lití	lití	k1gNnSc2	lití
<g/>
,	,	kIx,	,
broušení	broušení	k1gNnSc2	broušení
a	a	k8xC	a
povrchových	povrchový	k2eAgFnPc2d1	povrchová
úprav	úprava	k1gFnPc2	úprava
–	–	k?	–
leptání	leptání	k1gNnSc1	leptání
<g/>
,	,	kIx,	,
rytí	rytí	k1gNnSc1	rytí
i	i	k8xC	i
malování	malování	k1gNnSc1	malování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavební	stavební	k2eAgNnSc1d1	stavební
sklo	sklo	k1gNnSc1	sklo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k8xS	jak
plochá	plochý	k2eAgNnPc1d1	ploché
skla	sklo	k1gNnPc1	sklo
do	do	k7c2	do
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
jako	jako	k9	jako
drátosklo	drátosknout	k5eAaPmAgNnS	drátosknout
<g/>
,	,	kIx,	,
luxfera	luxfera	k1gFnSc1	luxfera
<g/>
,	,	kIx,	,
izolační	izolační	k2eAgNnSc1d1	izolační
pěnové	pěnový	k2eAgNnSc1d1	pěnové
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
skleněná	skleněný	k2eAgFnSc1d1	skleněná
vata	vata	k1gFnSc1	vata
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
stavebního	stavební	k2eAgNnSc2d1	stavební
skla	sklo	k1gNnSc2	sklo
===	===	k?	===
</s>
</p>
<p>
<s>
1226	[number]	k4	1226
–	–	k?	–
"	"	kIx"	"
<g/>
Široká	široký	k2eAgFnSc1d1	široká
tabule	tabule	k1gFnSc1	tabule
<g/>
"	"	kIx"	"
–	–	k?	–
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Broad	Broad	k1gInSc1	Broad
Sheet	Sheet	k1gInSc1	Sheet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
Sussexu	Sussex	k1gInSc6	Sussex
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
</s>
</p>
<p>
<s>
1330	[number]	k4	1330
–	–	k?	–
"	"	kIx"	"
<g/>
Korunní	korunní	k2eAgNnSc1d1	korunní
sklo	sklo	k1gNnSc1	sklo
<g/>
"	"	kIx"	"
–	–	k?	–
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Crown	Crown	k1gNnSc1	Crown
Glass	Glassa	k1gFnPc2	Glassa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1620	[number]	k4	1620
–	–	k?	–
foukané	foukaný	k2eAgNnSc1d1	foukané
sklo	sklo	k1gNnSc1	sklo
–	–	k?	–
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Blown	Blown	k1gInSc1	Blown
Plate	plat	k1gInSc5	plat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
roztočením	roztočení	k1gNnSc7	roztočení
vyfouknuté	vyfouknutý	k2eAgFnSc2d1	vyfouknutá
bubliny	bublina	k1gFnSc2	bublina
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
a	a	k8xC	a
okenní	okenní	k2eAgFnPc4d1	okenní
tabule	tabule	k1gFnPc4	tabule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1843	[number]	k4	1843
–	–	k?	–
plovoucí	plovoucí	k2eAgNnSc1d1	plovoucí
sklo	sklo	k1gNnSc1	sklo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Float	Float	k2eAgInSc1d1	Float
Glass	Glass	k1gInSc1	Glass
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vynašel	vynajít	k5eAaPmAgMnS	vynajít
Henry	Henry	k1gMnSc1	Henry
Bessemer	Bessemer	k1gMnSc1	Bessemer
litím	lití	k1gNnSc7	lití
skla	sklo	k1gNnSc2	sklo
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
roztaveného	roztavený	k2eAgInSc2d1	roztavený
cínu	cín	k1gInSc2	cín
<g/>
;	;	kIx,	;
tabule	tabule	k1gFnPc1	tabule
mají	mít	k5eAaImIp3nP	mít
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
drahá	drahý	k2eAgFnSc1d1	drahá
</s>
</p>
<p>
<s>
1888	[number]	k4	1888
–	–	k?	–
válcované	válcovaný	k2eAgNnSc4d1	válcované
sklo	sklo	k1gNnSc4	sklo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Machine	Machin	k1gMnSc5	Machin
Rolled	Rolled	k1gInSc4	Rolled
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vzorkovaný	vzorkovaný	k2eAgInSc4d1	vzorkovaný
povrch	povrch	k1gInSc4	povrch
tabulí	tabule	k1gFnPc2	tabule
</s>
</p>
<p>
<s>
1898	[number]	k4	1898
–	–	k?	–
drátosklo	drátosknout	k5eAaPmAgNnS	drátosknout
vyztužené	vyztužený	k2eAgNnSc1d1	vyztužené
drátovou	drátový	k2eAgFnSc7d1	drátová
sítí	síť	k1gFnSc7	síť
pro	pro	k7c4	pro
stavební	stavební	k2eAgInPc4d1	stavební
účely	účel	k1gInPc4	účel
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
základní	základní	k2eAgNnSc4d1	základní
sklo	sklo	k1gNnSc4	sklo
(	(	kIx(	(
<g/>
float	float	k1gInSc1	float
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
způsob	způsob	k1gInSc1	způsob
kontinuální	kontinuální	k2eAgFnSc2d1	kontinuální
výroby	výroba	k1gFnSc2	výroba
plochého	plochý	k2eAgNnSc2d1	ploché
skla	sklo	k1gNnSc2	sklo
s	s	k7c7	s
dokonalým	dokonalý	k2eAgInSc7d1	dokonalý
povrchem	povrch	k1gInSc7	povrch
</s>
</p>
<p>
<s>
===	===	k?	===
Barvení	barvení	k1gNnPc2	barvení
skla	sklo	k1gNnSc2	sklo
===	===	k?	===
</s>
</p>
<p>
<s>
Kovy	kov	k1gInPc1	kov
a	a	k8xC	a
oxidy	oxid	k1gInPc1	oxid
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
během	během	k7c2	během
výroby	výroba	k1gFnSc2	výroba
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přidán	přidat	k5eAaPmNgMnS	přidat
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
zeleného	zelený	k2eAgInSc2d1	zelený
odstínu	odstín	k1gInSc2	odstín
od	od	k7c2	od
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
ametystové	ametystový	k2eAgFnSc2d1	ametystová
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
selen	selen	k1gInSc1	selen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
koncentracích	koncentrace	k1gFnPc6	koncentrace
na	na	k7c6	na
dekolorizaci	dekolorizace	k1gFnSc6	dekolorizace
skla	sklo	k1gNnSc2	sklo
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
koncentrace	koncentrace	k1gFnPc1	koncentrace
kobaltu	kobalt	k1gInSc2	kobalt
(	(	kIx(	(
<g/>
0,025	[number]	k4	0,025
<g/>
–	–	k?	–
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
dávají	dávat	k5eAaImIp3nP	dávat
modré	modrý	k2eAgNnSc1d1	modré
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
oxidy	oxid	k1gInPc7	oxid
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
arzénu	arzén	k1gInSc2	arzén
produkuje	produkovat	k5eAaImIp3nS	produkovat
neprůhledné	průhledný	k2eNgNnSc1d1	neprůhledné
bílé	bílý	k2eAgNnSc1d1	bílé
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použité	použitý	k2eAgFnPc1d1	použitá
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
imitace	imitace	k1gFnSc2	imitace
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
tří	tři	k4xCgNnPc2	tři
procent	procento	k1gNnPc2	procento
oxidů	oxid	k1gInPc2	oxid
mědi	měď	k1gFnSc2	měď
produkuje	produkovat	k5eAaImIp3nS	produkovat
tyrkysovou	tyrkysový	k2eAgFnSc4d1	tyrkysová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
kovová	kovový	k2eAgFnSc1d1	kovová
měď	měď	k1gFnSc1	měď
dává	dávat	k5eAaImIp3nS	dávat
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgNnSc1d1	tmavé
červené	červené	k1gNnSc1	červené
<g/>
,	,	kIx,	,
neprůhledné	průhledný	k2eNgNnSc1d1	neprůhledné
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
užívané	užívaný	k2eAgNnSc1d1	užívané
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rubínové	rubínový	k2eAgNnSc4d1	rubínové
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
koncentrace	koncentrace	k1gFnSc2	koncentrace
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
modré	modrý	k2eAgNnSc1d1	modré
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
nebo	nebo	k8xC	nebo
i	i	k9	i
černé	černý	k2eAgNnSc1d1	černé
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
titanu	titan	k1gInSc2	titan
vzniká	vznikat	k5eAaImIp3nS	vznikat
žluto-hnědé	žlutonědý	k2eAgNnSc4d1	žluto-hnědé
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc6d1	malá
koncentracích	koncentrace	k1gFnPc6	koncentrace
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
0,001	[number]	k4	0,001
%	%	kIx~	%
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
silně	silně	k6eAd1	silně
rubínově	rubínově	k6eAd1	rubínově
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
nižší	nízký	k2eAgFnPc1d2	nižší
koncentrace	koncentrace	k1gFnPc1	koncentrace
produkují	produkovat	k5eAaImIp3nP	produkovat
méně	málo	k6eAd2	málo
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
červenou	červená	k1gFnSc4	červená
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
marketingově	marketingově	k6eAd1	marketingově
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
brusinka	brusinka	k1gFnSc1	brusinka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přidán	přidat	k5eAaPmNgInS	přidat
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
fluorescentní	fluorescentní	k2eAgFnSc2d1	fluorescentní
žluté	žlutý	k2eAgFnPc4d1	žlutá
nebo	nebo	k8xC	nebo
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Uranové	uranový	k2eAgNnSc1d1	uranové
sklo	sklo	k1gNnSc1	sklo
typicky	typicky	k6eAd1	typicky
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
leštění	leštění	k1gNnSc6	leštění
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
inhalován	inhalován	k2eAgMnSc1d1	inhalován
(	(	kIx(	(
<g/>
vdechnut	vdechnut	k2eAgMnSc1d1	vdechnut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
karcinogenní	karcinogenní	k2eAgInSc1d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
dusičnan	dusičnan	k1gInSc1	dusičnan
stříbrný	stříbrný	k1gInSc1	stříbrný
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
rozsah	rozsah	k1gInSc4	rozsah
barev	barva	k1gFnPc2	barva
od	od	k7c2	od
oranžově	oranžově	k6eAd1	oranžově
červené	červený	k2eAgFnSc2d1	červená
po	po	k7c4	po
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
zahřátí	zahřátí	k1gNnSc1	zahřátí
a	a	k8xC	a
zchlazení	zchlazení	k1gNnSc1	zchlazení
může	moct	k5eAaImIp3nS	moct
významně	významně	k6eAd1	významně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
barvy	barva	k1gFnPc4	barva
produkované	produkovaný	k2eAgFnPc4d1	produkovaná
těmito	tento	k3xDgFnPc7	tento
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Roztavená	roztavený	k2eAgFnSc1d1	roztavená
sklovina	sklovina	k1gFnSc1	sklovina
se	se	k3xPyFc4	se
tvaruje	tvarovat	k5eAaImIp3nS	tvarovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Foukáním	foukání	k1gNnSc7	foukání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sklář	sklář	k1gMnSc1	sklář
nabere	nabrat	k5eAaPmIp3nS	nabrat
sklovinu	sklovina	k1gFnSc4	sklovina
na	na	k7c4	na
sklářskou	sklářský	k2eAgFnSc4d1	sklářská
píšťalu	píšťala	k1gFnSc4	píšťala
a	a	k8xC	a
vyfukuje	vyfukovat	k5eAaImIp3nS	vyfukovat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
nebo	nebo	k8xC	nebo
kovové	kovový	k2eAgFnSc2d1	kovová
otvírací	otvírací	k2eAgFnSc2d1	otvírací
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
způsobů	způsob	k1gInPc2	způsob
tvarování	tvarování	k1gNnSc2	tvarování
dutého	dutý	k2eAgNnSc2d1	duté
skla	sklo	k1gNnSc2	sklo
sloužil	sloužit	k5eAaImAgInS	sloužit
dříve	dříve	k6eAd2	dříve
i	i	k9	i
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
okenních	okenní	k2eAgFnPc2d1	okenní
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
:	:	kIx,	:
vyfouknutá	vyfouknutý	k2eAgFnSc1d1	vyfouknutá
bublina	bublina	k1gFnSc1	bublina
se	se	k3xPyFc4	se
nůžkami	nůžky	k1gFnPc7	nůžky
rozstřihla	rozstřihnout	k5eAaPmAgFnS	rozstřihnout
a	a	k8xC	a
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
na	na	k7c6	na
kovové	kovový	k2eAgFnSc6d1	kovová
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
foukání	foukání	k1gNnSc1	foukání
často	často	k6eAd1	často
dělá	dělat	k5eAaImIp3nS	dělat
strojem	stroj	k1gInSc7	stroj
(	(	kIx(	(
<g/>
láhve	láhev	k1gFnPc1	láhev
<g/>
,	,	kIx,	,
baňky	baňka	k1gFnPc1	baňka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
lisováním	lisování	k1gNnSc7	lisování
(	(	kIx(	(
<g/>
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
,	,	kIx,	,
nádobky	nádobka	k1gFnSc2	nádobka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lisováním	lisování	k1gNnSc7	lisování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odměřené	odměřený	k2eAgNnSc1d1	odměřené
množství	množství	k1gNnSc1	množství
skloviny	sklovina	k1gFnSc2	sklovina
naleje	nalít	k5eAaBmIp3nS	nalít
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
pohyblivým	pohyblivý	k2eAgInSc7d1	pohyblivý
trnem	trn	k1gInSc7	trn
<g/>
.	.	kIx.	.
</s>
<s>
Formy	forma	k1gFnPc1	forma
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
dělené	dělený	k2eAgFnPc1d1	dělená
a	a	k8xC	a
při	při	k7c6	při
lisování	lisování	k1gNnSc6	lisování
se	se	k3xPyFc4	se
mažou	mazat	k5eAaImIp3nP	mazat
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litím	lití	k1gNnSc7	lití
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sklovina	sklovina	k1gFnSc1	sklovina
naleje	nalít	k5eAaPmIp3nS	nalít
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
případně	případně	k6eAd1	případně
dotvoří	dotvořit	k5eAaPmIp3nS	dotvořit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
druh	druh	k1gInSc1	druh
kontinuálního	kontinuální	k2eAgNnSc2d1	kontinuální
lití	lití	k1gNnSc2	lití
je	být	k5eAaImIp3nS	být
floating	floating	k1gInSc1	floating
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sklovina	sklovina	k1gFnSc1	sklovina
průběžně	průběžně	k6eAd1	průběžně
nalévá	nalévat	k5eAaImIp3nS	nalévat
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
roztaveného	roztavený	k2eAgInSc2d1	roztavený
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
dokonale	dokonale	k6eAd1	dokonale
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
většina	většina	k1gFnSc1	většina
plochého	plochý	k2eAgNnSc2d1	ploché
skla	sklo	k1gNnSc2	sklo
i	i	k9	i
v	v	k7c6	v
zrcadlové	zrcadlový	k2eAgFnSc6d1	zrcadlová
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tažením	tažení	k1gNnSc7	tažení
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
skleněná	skleněný	k2eAgNnPc1d1	skleněné
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozfoukáním	rozfoukání	k1gNnSc7	rozfoukání
taveniny	tavenina	k1gFnSc2	tavenina
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
skleněná	skleněný	k2eAgFnSc1d1	skleněná
vata	vata	k1gFnSc1	vata
<g/>
,	,	kIx,	,
vháněním	vhánění	k1gNnSc7	vhánění
vzduchu	vzduch	k1gInSc2	vzduch
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
pěnové	pěnový	k2eAgNnSc1d1	pěnové
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nekřemičitá	křemičitý	k2eNgNnPc1d1	křemičitý
skla	sklo	k1gNnPc1	sklo
==	==	k?	==
</s>
</p>
<p>
<s>
Tavením	tavení	k1gNnSc7	tavení
některých	některý	k3yIgFnPc2	některý
hornin	hornina	k1gFnPc2	hornina
vznikají	vznikat	k5eAaImIp3nP	vznikat
také	také	k9	také
skla	sklo	k1gNnPc4	sklo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tavením	tavení	k1gNnSc7	tavení
znělce	znělec	k1gInSc2	znělec
vzniká	vznikat	k5eAaImIp3nS	vznikat
tmavé	tmavý	k2eAgNnSc1d1	tmavé
znělcové	znělcový	k2eAgNnSc1d1	znělcové
sklo	sklo	k1gNnSc1	sklo
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
nestárnoucí	stárnoucí	k2eNgNnPc4d1	nestárnoucí
"	"	kIx"	"
<g/>
síťová	síťový	k2eAgNnPc4d1	síťové
skla	sklo	k1gNnPc4	sklo
<g/>
"	"	kIx"	"
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
elektrooptických	elektrooptický	k2eAgFnPc2d1	elektrooptický
součástek	součástka	k1gFnPc2	součástka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
laserové	laserový	k2eAgFnSc6d1	laserová
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžné	běžný	k2eAgFnSc2d1	běžná
CD-RW	CD-RW	k1gFnSc2	CD-RW
disky	disk	k1gInPc1	disk
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
chalkogenidových	chalkogenidový	k2eAgNnPc2d1	chalkogenidový
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
<g/>
Sklokeramika	Sklokeramika	k1gFnSc1	Sklokeramika
vzniká	vznikat	k5eAaImIp3nS	vznikat
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
materiál	materiál	k1gInSc1	materiál
částečně	částečně	k6eAd1	částečně
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
k	k	k7c3	k
tepelným	tepelný	k2eAgInPc3d1	tepelný
šokům	šok	k1gInPc3	šok
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c4	na
varné	varný	k2eAgFnPc4d1	varná
desky	deska	k1gFnPc4	deska
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
kosmických	kosmický	k2eAgNnPc2d1	kosmické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
O.	O.	kA	O.
Drahotová	Drahotový	k2eAgFnSc1d1	Drahotová
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1970	[number]	k4	1970
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Klebsa	Klebsa	k1gFnSc1	Klebsa
<g/>
,	,	kIx,	,
Technologie	technologie	k1gFnSc1	technologie
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
díl	díl	k1gInSc1	díl
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
:	:	kIx,	:
VŠST	VŠST	kA	VŠST
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
S.	S.	kA	S.
Petrová	Petrová	k1gFnSc1	Petrová
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Gallery	Galler	k1gInPc7	Galler
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
23	[number]	k4	23
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
274	[number]	k4	274
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgNnSc4d1	nové
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc4	heslo
Sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1283	[number]	k4	1283
</s>
</p>
<p>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
4972	[number]	k4	4972
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
950	[number]	k4	950
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
M.	M.	kA	M.
B.	B.	kA	B.
Volf	Volf	k1gMnSc1	Volf
<g/>
,	,	kIx,	,
Sklo	sklo	k1gNnSc1	sklo
<g/>
:	:	kIx,	:
Podstata	podstata	k1gFnSc1	podstata
<g/>
,	,	kIx,	,
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc1	užití
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Poláček	Poláček	k1gMnSc1	Poláček
1947	[number]	k4	1947
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Vondruška	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Sklářství	sklářství	k1gNnSc1	sklářství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
fotografie	fotografie	k1gFnSc1	fotografie
skla	sklo	k1gNnSc2	sklo
</s>
</p>
<p>
<s>
sklenář	sklenář	k1gMnSc1	sklenář
</s>
</p>
<p>
<s>
sklobeton	sklobeton	k1gInSc1	sklobeton
</s>
</p>
<p>
<s>
sklolaminát	sklolaminát	k1gInSc1	sklolaminát
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sklo	sklo	k1gNnSc4	sklo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sklo	sklo	k1gNnSc4	sklo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
