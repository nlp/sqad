<s>
Tur	tur	k1gMnSc1	tur
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Bos	bos	k1gMnSc1	bos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
turovitých	turovití	k1gMnPc2	turovití
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
velcí	velký	k2eAgMnPc1d1	velký
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
žijící	žijící	k2eAgMnPc1d1	žijící
býložravci	býložravec	k1gMnPc1	býložravec
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgInPc4d1	žijící
divoké	divoký	k2eAgInPc4d1	divoký
druhy	druh	k1gInPc4	druh
žijí	žít	k5eAaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
tur	tur	k1gMnSc1	tur
domácí	domácí	k2eAgMnSc1d1	domácí
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
člověku	člověk	k1gMnSc3	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
turů	tur	k1gMnPc2	tur
jsou	být	k5eAaImIp3nP	být
rohy	roh	k1gInPc1	roh
oválného	oválný	k2eAgMnSc2d1	oválný
nebo	nebo	k8xC	nebo
okrouhlého	okrouhlý	k2eAgInSc2d1	okrouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
u	u	k7c2	u
tura	tur	k1gMnSc2	tur
domácího	domácí	k1gMnSc2	domácí
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
vyšlechtěna	vyšlechtěn	k2eAgNnPc4d1	vyšlechtěno
i	i	k8xC	i
bezrohá	bezrohý	k2eAgNnPc4d1	bezrohé
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Tuři	tur	k1gMnPc1	tur
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
matriarchálních	matriarchální	k2eAgFnPc6d1	matriarchální
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
telaty	tele	k1gNnPc7	tele
<g/>
,	,	kIx,	,
jalovicemi	jalovice	k1gFnPc7	jalovice
a	a	k8xC	a
krávami	kráva	k1gFnPc7	kráva
<g/>
,	,	kIx,	,
samčí	samčí	k2eAgFnPc1d1	samčí
skupiny	skupina	k1gFnPc1	skupina
žijí	žít	k5eAaImIp3nP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
během	během	k7c2	během
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
sezóny	sezóna	k1gFnSc2	sezóna
jsou	být	k5eAaImIp3nP	být
stáda	stádo	k1gNnPc1	stádo
smíšená	smíšený	k2eAgNnPc1d1	smíšené
<g/>
.	.	kIx.	.
</s>
<s>
Krávy	kráva	k1gFnPc1	kráva
rodí	rodit	k5eAaImIp3nP	rodit
jediné	jediný	k2eAgNnSc4d1	jediné
tele	tele	k1gNnSc4	tele
<g/>
,	,	kIx,	,
vazba	vazba	k1gFnSc1	vazba
mezi	mezi	k7c7	mezi
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
mládětem	mládě	k1gNnSc7	mládě
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Pasou	pást	k5eAaImIp3nP	pást
se	se	k3xPyFc4	se
na	na	k7c4	na
vegetaci	vegetace	k1gFnSc4	vegetace
za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
a	a	k8xC	a
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
na	na	k7c4	na
pastvinu	pastvina	k1gFnSc4	pastvina
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
vedena	vést	k5eAaImNgFnS	vést
starou	starý	k2eAgFnSc7d1	stará
<g/>
,	,	kIx,	,
zkušenou	zkušený	k2eAgFnSc7d1	zkušená
krávou	kráva	k1gFnSc7	kráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
migrují	migrovat	k5eAaImIp3nP	migrovat
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Tur	tur	k1gMnSc1	tur
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
<g/>
)	)	kIx)	)
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
†	†	k?	†
pratur	pratur	k1gMnSc1	pratur
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
primigenius	primigenius	k1gMnSc1	primigenius
<g/>
)	)	kIx)	)
Domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
<g/>
:	:	kIx,	:
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
primigenius	primigenius	k1gMnSc1	primigenius
f.	f.	k?	f.
taurus	taurus	k1gMnSc1	taurus
<g/>
)	)	kIx)	)
Druh	druh	k1gMnSc1	druh
<g/>
:	:	kIx,	:
gaur	gaur	k1gMnSc1	gaur
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
gaurus	gaurus	k1gMnSc1	gaurus
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
gaur	gaur	k1gMnSc1	gaur
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Bos	bos	k2eAgInSc1d1	bos
g.	g.	k?	g.
gaurus	gaurus	k1gInSc1	gaurus
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
gaur	gaur	k1gInSc1	gaur
siamský	siamský	k2eAgInSc1d1	siamský
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
g.	g.	k?	g.
readei	readei	k1gNnPc2	readei
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
gaur	gaur	k1gInSc1	gaur
malajský	malajský	k2eAgInSc1d1	malajský
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
g.	g.	k?	g.
hubbacki	hubbacki	k1gNnPc2	hubbacki
<g/>
)	)	kIx)	)
Domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
<g/>
:	:	kIx,	:
gayal	gayal	k1gInSc1	gayal
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
gaurus	gaurus	k1gInSc1	gaurus
f.	f.	k?	f.
frontalis	frontalis	k1gInSc1	frontalis
<g/>
)	)	kIx)	)
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
banteng	banteng	k1gMnSc1	banteng
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
<g />
.	.	kIx.	.
</s>
<s>
javanicus	javanicus	k1gInSc1	javanicus
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
banteng	banteng	k1gInSc1	banteng
jávský	jávský	k2eAgInSc1d1	jávský
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
j.	j.	k?	j.
javanicus	javanicus	k1gInSc1	javanicus
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
banteng	banteng	k1gInSc1	banteng
bornejský	bornejský	k2eAgInSc1d1	bornejský
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
j.	j.	k?	j.
lowi	lowi	k1gNnPc2	lowi
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
banteng	banteng	k1gInSc1	banteng
barmský	barmský	k2eAgInSc1d1	barmský
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
j.	j.	k?	j.
birmanicus	birmanicus	k1gInSc1	birmanicus
<g/>
)	)	kIx)	)
Domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
<g/>
:	:	kIx,	:
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
baliský	baliský	k1gMnSc1	baliský
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
banteng	banteng	k1gMnSc1	banteng
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Druh	druh	k1gInSc4	druh
<g/>
:	:	kIx,	:
kuprej	kuprat	k5eAaPmRp2nS	kuprat
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
sauveli	sauvet	k5eAaPmAgMnP	sauvet
<g/>
)	)	kIx)	)
Druh	druh	k1gInSc4	druh
<g/>
:	:	kIx,	:
jak	jak	k6eAd1	jak
divoký	divoký	k2eAgInSc1d1	divoký
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
mutus	mutus	k1gInSc1	mutus
<g/>
)	)	kIx)	)
Domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
mutus	mutus	k1gInSc1	mutus
f.	f.	k?	f.
grunniensis	grunniensis	k1gInSc1	grunniensis
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tur	tur	k1gMnSc1	tur
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tur	tur	k1gMnSc1	tur
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Bos	bos	k2eAgInSc1d1	bos
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
