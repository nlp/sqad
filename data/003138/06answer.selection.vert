<s>
Všechny	všechen	k3xTgFnPc1	všechen
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgInPc4d1	žijící
divoké	divoký	k2eAgInPc4d1	divoký
druhy	druh	k1gInPc4	druh
žijí	žít	k5eAaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
tur	tur	k1gMnSc1	tur
domácí	domácí	k2eAgMnSc1d1	domácí
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
člověku	člověk	k1gMnSc3	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
