<s>
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
je	být	k5eAaImIp3nS	být
australský	australský	k2eAgInSc1d1	australský
spolkový	spolkový	k2eAgInSc1d1	spolkový
stát	stát	k1gInSc1	stát
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
334	[number]	k4	334
okolních	okolní	k2eAgInPc6d1	okolní
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
státu	stát	k1gInSc2	stát
činí	činit	k5eAaImIp3nS	činit
68	[number]	k4	68
401	[number]	k4	401
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
žije	žít	k5eAaImIp3nS	žít
519	[number]	k4	519
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Hobart	Hobart	k1gInSc4	Hobart
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
ostrova	ostrov	k1gInSc2	ostrov
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Derwent	Derwent	k1gInSc1	Derwent
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
Launceston	Launceston	k1gInSc4	Launceston
<g/>
,	,	kIx,	,
Devonport	Devonport	k1gInSc4	Devonport
a	a	k8xC	a
Burnie	Burnie	k1gFnPc4	Burnie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
patří	patřit	k5eAaImIp3nS	patřit
subantarktický	subantarktický	k2eAgInSc4d1	subantarktický
ostrov	ostrov	k1gInSc4	ostrov
Macquarie	Macquarie	k1gFnSc2	Macquarie
<g/>
.	.	kIx.	.
</s>
<s>
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
svým	svůj	k3xOyFgNnSc7	svůj
přírodním	přírodní	k2eAgNnSc7d1	přírodní
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
37	[number]	k4	37
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
,	,	kIx,	,
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
a	a	k8xC	a
místa	místo	k1gNnPc1	místo
zapsaná	zapsaný	k2eAgNnPc1d1	zapsané
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
známá	známá	k1gFnSc1	známá
chovem	chov	k1gInSc7	chov
ovcí	ovce	k1gFnPc2	ovce
pro	pro	k7c4	pro
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
přistál	přistát	k5eAaPmAgInS	přistát
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
ostrova	ostrov	k1gInSc2	ostrov
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
holandský	holandský	k2eAgMnSc1d1	holandský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Abel	Abel	k1gMnSc1	Abel
Tasman	Tasman	k1gMnSc1	Tasman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gInSc4	on
sám	sám	k3xTgMnSc1	sám
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Van	van	k1gInSc4	van
Diemenova	Diemenův	k2eAgFnSc1d1	Diemenova
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
anglický	anglický	k2eAgMnSc1d1	anglický
námořní	námořní	k2eAgMnSc1d1	námořní
důstojník	důstojník	k1gMnSc1	důstojník
(	(	kIx(	(
<g/>
poručík	poručík	k1gMnSc1	poručík
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Bowen	Bowen	k1gInSc4	Bowen
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
asi	asi	k9	asi
20	[number]	k4	20
trestanců	trestanec	k1gMnPc2	trestanec
a	a	k8xC	a
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
svobodných	svobodný	k2eAgMnPc2d1	svobodný
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
založil	založit	k5eAaPmAgMnS	založit
kolonii	kolonie	k1gFnSc4	kolonie
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Derwent	Derwent	k1gInSc1	Derwent
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Risdon	Risdona	k1gFnPc2	Risdona
Cove	Cov	k1gFnSc2	Cov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
trestanecká	trestanecký	k2eAgFnSc1d1	trestanecká
kolonie	kolonie	k1gFnSc1	kolonie
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Macquarie	Macquarie	k1gFnSc1	Macquarie
Harbour	Harboura	k1gFnPc2	Harboura
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
kolonie	kolonie	k1gFnPc1	kolonie
Maria	Maria	k1gFnSc1	Maria
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Port	port	k1gInSc1	port
Arthur	Arthura	k1gFnPc2	Arthura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
byl	být	k5eAaImAgMnS	být
guvernérem	guvernér	k1gMnSc7	guvernér
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
David	David	k1gMnSc1	David
Collins	Collins	k1gInSc1	Collins
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
námořní	námořní	k2eAgMnSc1d1	námořní
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
roli	role	k1gFnSc4	role
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
ostrova	ostrov	k1gInSc2	ostrov
sehrál	sehrát	k5eAaPmAgMnS	sehrát
guvernér	guvernér	k1gMnSc1	guvernér
George	Georg	k1gMnSc2	Georg
Arthur	Arthur	k1gMnSc1	Arthur
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
až	až	k9	až
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
organizoval	organizovat	k5eAaBmAgInS	organizovat
bílou	bílý	k2eAgFnSc4d1	bílá
domobranu	domobrana	k1gFnSc4	domobrana
proti	proti	k7c3	proti
původní	původní	k2eAgFnSc3d1	původní
populaci	populace	k1gFnSc3	populace
Tasmánců	Tasmánec	k1gMnPc2	Tasmánec
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
tasmánských	tasmánský	k2eAgMnPc2d1	tasmánský
domorodců	domorodec	k1gMnPc2	domorodec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyhlazena	vyhlazen	k2eAgFnSc1d1	vyhlazena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hrůzné	hrůzný	k2eAgNnSc4d1	hrůzné
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Peklo	peklo	k1gNnSc4	peklo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
Van	vana	k1gFnPc2	vana
Diemenovy	Diemenův	k2eAgFnSc2d1	Diemenova
země	zem	k1gFnSc2	zem
na	na	k7c4	na
Tasmánii	Tasmánie	k1gFnSc4	Tasmánie
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jejího	její	k3xOp3gNnSc2	její
prvního	první	k4xOgMnSc4	první
evropského	evropský	k2eAgMnSc4d1	evropský
objevitele	objevitel	k1gMnSc4	objevitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
byla	být	k5eAaImAgFnS	být
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Australského	australský	k2eAgNnSc2d1	Australské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
těžce	těžce	k6eAd1	těžce
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
požáry	požár	k1gInPc7	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgInPc6	který
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
62	[number]	k4	62
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
