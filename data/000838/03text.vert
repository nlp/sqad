<s>
Hřebčín	hřebčín	k1gInSc1	hřebčín
Mimoň	mimoň	k1gMnSc1	mimoň
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Mimoni	Mimoň	k1gFnSc6	Mimoň
na	na	k7c6	na
Českolipsku	Českolipsko	k1gNnSc6	Českolipsko
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Vychoval	vychovat	k5eAaPmAgMnS	vychovat
zde	zde	k6eAd1	zde
řadu	řada	k1gFnSc4	řada
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
anglických	anglický	k2eAgMnPc2d1	anglický
plnokrevníků	plnokrevník	k1gMnPc2	plnokrevník
včetně	včetně	k7c2	včetně
vítězů	vítěz	k1gMnPc2	vítěz
Československého	československý	k2eAgNnSc2d1	Československé
derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
na	na	k7c4	na
jaře	jař	k1gFnPc4	jař
2013	[number]	k4	2013
do	do	k7c2	do
dražby	dražba	k1gFnSc2	dražba
<g/>
.	.	kIx.	.
</s>
<s>
Hřebčín	hřebčín	k1gInSc1	hřebčín
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Mimoni	Mimoň	k1gFnSc6	Mimoň
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
základem	základ	k1gInSc7	základ
dovoz	dovoz	k1gInSc4	dovoz
plnokrevných	plnokrevný	k2eAgMnPc2d1	plnokrevný
a	a	k8xC	a
polokrevných	polokrevný	k2eAgMnPc2d1	polokrevný
koní	kůň	k1gMnPc2	kůň
z	z	k7c2	z
SPP	SPP	kA	SPP
Chomutov	Chomutov	k1gInSc1	Chomutov
-	-	kIx~	-
Ahníkov	Ahníkov	k1gInSc1	Ahníkov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ze	z	k7c2	z
Zákup	zákup	k1gInSc1	zákup
přestěhovaly	přestěhovat	k5eAaPmAgFnP	přestěhovat
další	další	k2eAgMnPc4d1	další
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
mimoňský	mimoňský	k2eAgInSc1d1	mimoňský
areál	areál	k1gInSc1	areál
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
stáje	stáj	k1gFnPc4	stáj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
začaly	začít	k5eAaPmAgInP	začít
nákupy	nákup	k1gInPc1	nákup
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
koní	kůň	k1gMnPc2	kůň
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dostavby	dostavba	k1gFnPc1	dostavba
areálu	areál	k1gInSc2	areál
následovaly	následovat	k5eAaImAgFnP	následovat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
v	v	k7c6	v
Heřmaničkách	Heřmanička	k1gFnPc6	Heřmanička
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
filiálka	filiálka	k1gFnSc1	filiálka
hřebčína	hřebčín	k1gInSc2	hřebčín
s	s	k7c7	s
26	[number]	k4	26
boxy	box	k1gInPc7	box
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
hřebčín	hřebčín	k1gInSc1	hřebčín
administrativně	administrativně	k6eAd1	administrativně
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
Velkovýkrmen	velkovýkrmna	k1gFnPc2	velkovýkrmna
Zákupy	zákup	k1gInPc1	zákup
a	a	k8xC	a
pak	pak	k6eAd1	pak
prodán	prodat	k5eAaPmNgInS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
hřebčína	hřebčín	k1gInSc2	hřebčín
v	v	k7c6	v
Mimoni	Mimoň	k1gFnSc6	Mimoň
II	II	kA	II
Okrouhlická	okrouhlický	k2eAgFnSc1d1	Okrouhlická
144	[number]	k4	144
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
PhDr.	PhDr.	kA	PhDr.
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Pulec	pulec	k1gMnSc1	pulec
a	a	k8xC	a
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Pulec	pulec	k1gMnSc1	pulec
hřebčín	hřebčín	k1gInSc4	hřebčín
Mimoň	mimoň	k1gMnSc1	mimoň
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
chovatelské	chovatelský	k2eAgFnSc2d1	chovatelská
závodní	závodní	k2eAgFnSc2d1	závodní
a	a	k8xC	a
chovatelské	chovatelský	k2eAgFnSc2d1	chovatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
hřebčín	hřebčín	k1gInSc1	hřebčín
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
jen	jen	k9	jen
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
majitele	majitel	k1gMnSc2	majitel
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Dražba	dražba	k1gFnSc1	dražba
areálu	areál	k1gInSc2	areál
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
nabídka	nabídka	k1gFnSc1	nabídka
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nepodařil	podařit	k5eNaPmAgMnS	podařit
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
prodán	prodat	k5eAaPmNgInS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Kupcem	kupec	k1gMnSc7	kupec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
firma	firma	k1gFnSc1	firma
Maestoso	maestoso	k6eAd1	maestoso
Luna	luna	k1gFnSc1	luna
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
připravila	připravit	k5eAaPmAgFnS	připravit
obnovení	obnovení	k1gNnSc4	obnovení
dostihů	dostih	k1gInPc2	dostih
po	po	k7c6	po
čtyřleté	čtyřletý	k2eAgFnSc6d1	čtyřletá
pauze	pauza	k1gFnSc6	pauza
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc7	první
dostihem	dostih	k1gInSc7	dostih
je	být	k5eAaImIp3nS	být
Cena	cena	k1gFnSc1	cena
společnosti	společnost	k1gFnSc2	společnost
Charvát	Charvát	k1gMnSc1	Charvát
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
odtud	odtud	k6eAd1	odtud
vzešli	vzejít	k5eAaPmAgMnP	vzejít
vítězové	vítěz	k1gMnPc1	vítěz
tehdy	tehdy	k6eAd1	tehdy
prestižního	prestižní	k2eAgNnSc2d1	prestižní
Československého	československý	k2eAgNnSc2d1	Československé
derby	derby	k1gNnSc2	derby
<g/>
,	,	kIx,	,
Taran	taran	k1gInSc4	taran
a	a	k8xC	a
Trocadero	Trocadero	k1gNnSc4	Trocadero
<g/>
,	,	kIx,	,
úspěchy	úspěch	k1gInPc4	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
i	i	k9	i
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Avenir	Avenir	k1gInSc1	Avenir
<g/>
,	,	kIx,	,
Silistra	Silistra	k1gFnSc1	Silistra
<g/>
,	,	kIx,	,
Granda	grand	k1gMnSc2	grand
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
ustájil	ustájit	k5eAaPmAgMnS	ustájit
své	svůj	k3xOyFgMnPc4	svůj
koně	kůň	k1gMnPc4	kůň
čečenský	čečenský	k2eAgMnSc1d1	čečenský
prezident	prezident	k1gMnSc1	prezident
Ramzan	Ramzan	k1gMnSc1	Ramzan
Kadyrov	Kadyrovo	k1gNnPc2	Kadyrovo
<g/>
.	.	kIx.	.
</s>
<s>
Staral	starat	k5eAaImAgInS	starat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
o	o	k7c4	o
ně	on	k3xPp3gNnSc4	on
žokej	žokej	k1gMnSc1	žokej
Arslangirej	Arslangirej	k1gMnSc1	Arslangirej
Šavujev	Šavujev	k1gMnSc1	Šavujev
<g/>
.	.	kIx.	.
</s>
