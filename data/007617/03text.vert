<s>
Albertina	Albertin	k2eAgFnSc1d1	Albertina
je	být	k5eAaImIp3nS	být
galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc1	umění
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vévodou	vévoda	k1gMnSc7	vévoda
Albertem	Albert	k1gMnSc7	Albert
Sasko-Těšínským	saskoěšínský	k2eAgMnSc7d1	sasko-těšínský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
Kristinou	Kristina	k1gFnSc7	Kristina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
<g/>
,	,	kIx,	,
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
dcerou	dcera	k1gFnSc7	dcera
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
světových	světový	k2eAgFnPc2d1	světová
sbírek	sbírka	k1gFnPc2	sbírka
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
galerie	galerie	k1gFnPc1	galerie
tvoří	tvořit	k5eAaImIp3nP	tvořit
cca	cca	kA	cca
50	[number]	k4	50
000	[number]	k4	000
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
leptů	lept	k1gInPc2	lept
a	a	k8xC	a
akvarelů	akvarel	k1gInPc2	akvarel
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
rytin	rytina	k1gFnPc2	rytina
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
uvádí	uvádět	k5eAaImIp3nS	uvádět
43	[number]	k4	43
kreseb	kresba	k1gFnPc2	kresba
Raffaela	Raffael	k1gMnSc2	Raffael
Santiho	Santi	k1gMnSc2	Santi
<g/>
,	,	kIx,	,
145	[number]	k4	145
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürer	k1gMnSc2	Dürer
<g/>
,	,	kIx,	,
70	[number]	k4	70
Rembrandta	Rembrandto	k1gNnSc2	Rembrandto
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
jiná	jiný	k2eAgFnSc1d1	jiná
galerie	galerie	k1gFnSc1	galerie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
či	či	k8xC	či
150	[number]	k4	150
prací	práce	k1gFnPc2	práce
Egona	Egon	k1gMnSc4	Egon
Schieleho	Schiele	k1gMnSc4	Schiele
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgMnPc7d1	další
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
<g/>
,	,	kIx,	,
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarotti	Buonarotť	k1gFnSc5	Buonarotť
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc1	Rubens
<g/>
,	,	kIx,	,
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gMnSc1	Pieter
Brueghel	Brueghel	k1gMnSc1	Brueghel
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Cézanne	Cézann	k1gMnSc5	Cézann
<g/>
,	,	kIx,	,
Pablo	Pabla	k1gMnSc5	Pabla
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Henri	Henr	k1gInSc3	Henr
Matisse	Matisse	k1gFnSc2	Matisse
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Klimt	Klimt	k1gMnSc1	Klimt
či	či	k8xC	či
Oskar	Oskar	k1gMnSc1	Oskar
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sbírka	sbírka	k1gFnSc1	sbírka
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgInSc4d1	dokumentující
vývoj	vývoj	k1gInSc4	vývoj
tohoto	tento	k3xDgNnSc2	tento
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sloužil	sloužit	k5eAaImAgInS	sloužit
palác	palác	k1gInSc1	palác
jako	jako	k8xC	jako
residence	residence	k1gFnSc1	residence
místodržícího	místodržící	k1gMnSc2	místodržící
rakouského	rakouský	k2eAgNnSc2d1	rakouské
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
daroval	darovat	k5eAaPmAgMnS	darovat
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
palác	palác	k1gInSc1	palác
vévodovi	vévoda	k1gMnSc3	vévoda
Albertovi	Albert	k1gMnSc3	Albert
Sasko-Těšínskému	saskoěšínský	k2eAgMnSc3d1	sasko-těšínský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
místodržící	místodržící	k1gMnSc1	místodržící
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
Francouzi	Francouz	k1gMnPc1	Francouz
vypuzen	vypuzen	k2eAgInSc4d1	vypuzen
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
se	s	k7c7	s
sňatkem	sňatek	k1gInSc7	sňatek
přiženil	přiženit	k5eAaPmAgMnS	přiženit
do	do	k7c2	do
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
Albertina	Albertin	k2eAgNnPc4d1	Albertino
majetkem	majetek	k1gInSc7	majetek
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
přístavba	přístavba	k1gFnSc1	přístavba
nového	nový	k2eAgNnSc2d1	nové
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
nábytkem	nábytek	k1gInSc7	nábytek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
Albert	Albert	k1gMnSc1	Albert
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
belgickém	belgický	k2eAgNnSc6d1	Belgické
sídle	sídlo	k1gNnSc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
velká	velký	k2eAgFnSc1d1	velká
adaptace	adaptace	k1gFnSc1	adaptace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g/>
1825	[number]	k4	1825
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Kornhäusela	Kornhäusel	k1gMnSc2	Kornhäusel
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Josefa	Josef	k1gMnSc2	Josef
Kliebera	Klieber	k1gMnSc2	Klieber
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
prostor	prostora	k1gFnPc2	prostora
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
větší	veliký	k2eAgFnSc6d2	veliký
úpravě	úprava	k1gFnSc6	úprava
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získala	získat	k5eAaPmAgFnS	získat
Albertina	Albertin	k2eAgFnSc1d1	Albertina
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
historizujícímu	historizující	k2eAgInSc3d1	historizující
slohu	sloh	k1gInSc3	sloh
sousední	sousední	k2eAgFnSc2d1	sousední
Ringstraße	Ringstraß	k1gFnSc2	Ringstraß
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
palác	palác	k1gInSc4	palác
jeho	jeho	k3xOp3gMnSc3	jeho
majiteli	majitel	k1gMnSc3	majitel
<g/>
,	,	kIx,	,
arcivévodovi	arcivévoda	k1gMnSc3	arcivévoda
Bedřichu	Bedřich	k1gMnSc3	Bedřich
Rakousko-Těšínskému	rakouskoěšínský	k2eAgMnSc3d1	rakousko-těšínský
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příslušníkovi	příslušník	k1gMnSc3	příslušník
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
i	i	k9	i
se	s	k7c7	s
sbírkami	sbírka	k1gFnPc7	sbírka
vyvlastněn	vyvlastnit	k5eAaPmNgInS	vyvlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Arcivévodovi	arcivévoda	k1gMnSc3	arcivévoda
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
odvézt	odvézt	k5eAaPmF	odvézt
si	se	k3xPyFc3	se
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
ponechán	ponechat	k5eAaPmNgInS	ponechat
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
cenné	cenný	k2eAgNnSc1d1	cenné
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
Albertina	Albertin	k2eAgFnSc1d1	Albertina
vážně	vážně	k6eAd1	vážně
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
bombardováním	bombardování	k1gNnSc7	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Albertina	Albertin	k2eAgFnSc1d1	Albertina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
velké	velký	k2eAgFnSc2d1	velká
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc2	veřejnost
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zakoupit	zakoupit	k5eAaPmF	zakoupit
zpět	zpět	k6eAd1	zpět
většinu	většina	k1gFnSc4	většina
původního	původní	k2eAgNnSc2d1	původní
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
odvezeného	odvezený	k2eAgMnSc4d1	odvezený
počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
výstavní	výstavní	k2eAgFnSc1d1	výstavní
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	a
vybudovaly	vybudovat	k5eAaPmAgInP	vybudovat
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
moderní	moderní	k2eAgInPc1d1	moderní
depozitáře	depozitář	k1gInPc1	depozitář
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
datum	datum	k1gNnSc1	datum
4	[number]	k4	4
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americký	americký	k2eAgInSc1d1	americký
kongres	kongres	k1gInSc1	kongres
schválil	schválit	k5eAaPmAgInS	schválit
Prohlášení	prohlášení	k1gNnSc4	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
převzali	převzít	k5eAaPmAgMnP	převzít
vévoda	vévoda	k1gMnSc1	vévoda
Albert	Albert	k1gMnSc1	Albert
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Marie	Marie	k1gFnSc1	Marie
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
od	od	k7c2	od
tamního	tamní	k2eAgMnSc2d1	tamní
rakouského	rakouský	k2eAgMnSc2d1	rakouský
vyslance	vyslanec	k1gMnSc2	vyslanec
pro	pro	k7c4	pro
císařskou	císařský	k2eAgFnSc4d1	císařská
sbírku	sbírka	k1gFnSc4	sbírka
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
mědirytin	mědirytina	k1gFnPc2	mědirytina
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
sbírku	sbírka	k1gFnSc4	sbírka
postupně	postupně	k6eAd1	postupně
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
o	o	k7c4	o
kresby	kresba	k1gFnPc4	kresba
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
,	,	kIx,	,
Raffaela	Raffael	k1gMnSc2	Raffael
<g/>
,	,	kIx,	,
Poussina	Poussin	k2eAgMnSc2d1	Poussin
či	či	k8xC	či
Lorraina	Lorrain	k1gMnSc2	Lorrain
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
získal	získat	k5eAaPmAgMnS	získat
výměnou	výměna	k1gFnSc7	výměna
z	z	k7c2	z
císařské	císařský	k2eAgFnSc2d1	císařská
dvorní	dvorní	k2eAgFnSc2d1	dvorní
knihovny	knihovna	k1gFnSc2	knihovna
díla	dílo	k1gNnSc2	dílo
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürer	k1gMnSc2	Dürer
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
přešly	přejít	k5eAaPmAgInP	přejít
do	do	k7c2	do
budoucí	budoucí	k2eAgFnSc2d1	budoucí
Albertiny	Albertin	k2eAgFnSc2d1	Albertina
i	i	k8xC	i
práce	práce	k1gFnSc2	práce
Rubensovy	Rubensův	k2eAgFnSc2d1	Rubensova
<g/>
,	,	kIx,	,
Rembrandtovy	Rembrandtův	k2eAgFnSc2d1	Rembrandtova
a	a	k8xC	a
van	van	k1gInSc1	van
Dycka	Dycko	k1gNnSc2	Dycko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1738	[number]	k4	1738
<g/>
,	,	kIx,	,
do	do	k7c2	do
knihovny	knihovna	k1gFnSc2	knihovna
dostaly	dostat	k5eAaPmAgInP	dostat
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgMnSc2d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
získal	získat	k5eAaPmAgMnS	získat
vévoda	vévoda	k1gMnSc1	vévoda
Albert	Albert	k1gMnSc1	Albert
pro	pro	k7c4	pro
galerii	galerie	k1gFnSc4	galerie
sbírku	sbírka	k1gFnSc4	sbírka
holandského	holandský	k2eAgMnSc2d1	holandský
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
sběratele	sběratel	k1gMnSc2	sběratel
umění	umění	k1gNnSc2	umění
Cornelise	Cornelise	k1gFnSc2	Cornelise
Ploose	Ploosa	k1gFnSc6	Ploosa
van	vana	k1gFnPc2	vana
Amstela	Amstela	k1gFnSc1	Amstela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Albertinu	Albertin	k2eAgFnSc4d1	Albertina
obohatila	obohatit	k5eAaPmAgFnS	obohatit
o	o	k7c4	o
cenný	cenný	k2eAgInSc4d1	cenný
soubor	soubor	k1gInSc4	soubor
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
kresby	kresba	k1gFnSc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
grafických	grafický	k2eAgFnPc2d1	grafická
a	a	k8xC	a
kresebných	kresebný	k2eAgFnPc2d1	kresebná
sbírek	sbírka	k1gFnPc2	sbírka
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
dokončen	dokončit	k5eAaPmNgMnS	dokončit
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
přírůstky	přírůstek	k1gInPc1	přírůstek
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
inventář	inventář	k1gInSc4	inventář
už	už	k9	už
jen	jen	k9	jen
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
budova	budova	k1gFnSc1	budova
galerie	galerie	k1gFnSc2	galerie
i	i	k8xC	i
její	její	k3xOp3gFnSc2	její
sbírky	sbírka	k1gFnSc2	sbírka
přešly	přejít	k5eAaPmAgInP	přejít
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
Habsburků	Habsburk	k1gMnPc2	Habsburk
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
rytin	rytina	k1gFnPc2	rytina
spojena	spojen	k2eAgFnSc1d1	spojena
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
staré	starý	k2eAgFnSc2d1	stará
dvorní	dvorní	k2eAgFnSc2d1	dvorní
knihovny	knihovna	k1gFnSc2	knihovna
(	(	kIx(	(
<g/>
Hofbibliothek	Hofbibliothek	k1gInSc1	Hofbibliothek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
jméno	jméno	k1gNnSc1	jméno
Albertina	Albertin	k2eAgFnSc1d1	Albertina
dostala	dostat	k5eAaPmAgFnS	dostat
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
řídil	řídit	k5eAaImAgMnS	řídit
Albertinu	Albertin	k2eAgFnSc4d1	Albertina
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Zlovědic	Zlovědic	k1gMnSc1	Zlovědic
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Joseph	Joseph	k1gMnSc1	Joseph
Meder	Meder	k1gMnSc1	Meder
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
Alfred	Alfred	k1gMnSc1	Alfred
Stix	Stix	k1gInSc4	Stix
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
prodat	prodat	k5eAaPmF	prodat
některé	některý	k3yIgInPc4	některý
duplikáty	duplikát	k1gInPc4	duplikát
z	z	k7c2	z
grafické	grafický	k2eAgFnSc2d1	grafická
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
získal	získat	k5eAaPmAgMnS	získat
prostředky	prostředek	k1gInPc7	prostředek
na	na	k7c6	na
zacelení	zacelení	k1gNnSc6	zacelení
mezer	mezera	k1gFnPc2	mezera
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kresby	kresba	k1gFnSc2	kresba
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Simona	Simona	k1gFnSc1	Simona
Mellera	Meller	k1gMnSc4	Meller
tak	tak	k9	tak
do	do	k7c2	do
Albertiny	Albertin	k2eAgFnPc1d1	Albertina
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zakoupeny	zakoupen	k2eAgFnPc4d1	zakoupena
kresby	kresba	k1gFnPc4	kresba
a	a	k8xC	a
akvarely	akvarel	k1gInPc4	akvarel
Ingrese	ingrese	k1gFnSc2	ingrese
<g/>
,	,	kIx,	,
Corota	Corot	k1gMnSc2	Corot
<g/>
,	,	kIx,	,
Delacroixe	Delacroixe	k1gFnSc2	Delacroixe
<g/>
,	,	kIx,	,
Maneta	manet	k1gInSc2	manet
<g/>
,	,	kIx,	,
Degase	Degasa	k1gFnSc6	Degasa
<g/>
,	,	kIx,	,
Renoira	Renoira	k1gFnSc1	Renoira
a	a	k8xC	a
Cézanna	Cézanna	k1gFnSc1	Cézanna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
galerie	galerie	k1gFnSc1	galerie
obohacována	obohacovat	k5eAaImNgFnS	obohacovat
také	také	k9	také
o	o	k7c4	o
rakouské	rakouský	k2eAgMnPc4d1	rakouský
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Klimta	Klimt	k1gMnSc4	Klimt
a	a	k8xC	a
Schieleho	Schiele	k1gMnSc4	Schiele
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
1300	[number]	k4	1300
díly	díl	k1gInPc4	díl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Albertině	Albertin	k2eAgFnSc6d1	Albertina
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
Oskar	Oskar	k1gMnSc1	Oskar
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
daru	dar	k1gInSc3	dar
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Oldy	Olda	k1gFnSc2	Olda
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
i	i	k9	i
modernímu	moderní	k2eAgNnSc3d1	moderní
americkému	americký	k2eAgNnSc3d1	americké
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nákupem	nákup	k1gInSc7	nákup
prací	prací	k2eAgFnSc2d1	prací
např.	např.	kA	např.
Andy	Anda	k1gFnSc2	Anda
Warhola	Warhola	k1gFnSc1	Warhola
<g/>
,	,	kIx,	,
Jacksona	Jacksona	k1gFnSc1	Jacksona
Pollocka	Pollocko	k1gNnSc2	Pollocko
či	či	k8xC	či
Roye	Roy	k1gMnSc4	Roy
Lichtensteina	Lichtenstein	k1gMnSc4	Lichtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2007	[number]	k4	2007
obohatil	obohatit	k5eAaPmAgMnS	obohatit
Albertinu	Albertin	k2eAgFnSc4d1	Albertina
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
darů	dar	k1gInPc2	dar
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Herbert	Herbert	k1gMnSc1	Herbert
a	a	k8xC	a
Rita	Rita	k1gFnSc1	Rita
Batlinerovi	Batlinerův	k2eAgMnPc1d1	Batlinerův
předali	předat	k5eAaPmAgMnP	předat
galerii	galerie	k1gFnSc4	galerie
svou	svůj	k3xOyFgFnSc4	svůj
sbírku	sbírka	k1gFnSc4	sbírka
200	[number]	k4	200
olejomaleb	olejomalba	k1gFnPc2	olejomalba
autorů	autor	k1gMnPc2	autor
evropské	evropský	k2eAgFnSc2d1	Evropská
moderny	moderna	k1gFnSc2	moderna
od	od	k7c2	od
Moneta	moneta	k1gFnSc1	moneta
<g/>
,	,	kIx,	,
Cézanna	Cézann	k1gMnSc4	Cézann
<g/>
,	,	kIx,	,
Picassa	Picass	k1gMnSc4	Picass
<g/>
,	,	kIx,	,
Miróa	Miróus	k1gMnSc4	Miróus
<g/>
,	,	kIx,	,
Chagalla	Chagall	k1gMnSc4	Chagall
<g/>
,	,	kIx,	,
Maleviče	Malevič	k1gMnSc4	Malevič
<g/>
,	,	kIx,	,
Bacona	Bacon	k1gMnSc4	Bacon
<g/>
,	,	kIx,	,
Klea	Kleus	k1gMnSc4	Kleus
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
malířů	malíř	k1gMnPc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
manželů	manžel	k1gMnPc2	manžel
Batlinerových	Batlinerová	k1gFnPc2	Batlinerová
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
stálé	stálý	k2eAgFnSc2d1	stálá
expozice	expozice	k1gFnSc2	expozice
Albertiny	Albertin	k2eAgFnSc2d1	Albertina
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
se	se	k3xPyFc4	se
tak	tak	k9	tak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moderní	moderní	k2eAgFnSc6d1	moderní
podobě	podoba	k1gFnSc6	podoba
přestala	přestat	k5eAaPmAgFnS	přestat
orientovat	orientovat	k5eAaBmF	orientovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
