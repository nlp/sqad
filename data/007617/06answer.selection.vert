<s>
Albertina	Albertin	k2eAgFnSc1d1	Albertina
je	být	k5eAaImIp3nS	být
galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc1	umění
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vévodou	vévoda	k1gMnSc7	vévoda
Albertem	Albert	k1gMnSc7	Albert
Sasko-Těšínským	saskoěšínský	k2eAgMnSc7d1	sasko-těšínský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
Kristinou	Kristina	k1gFnSc7	Kristina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
<g/>
,	,	kIx,	,
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
dcerou	dcera	k1gFnSc7	dcera
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
