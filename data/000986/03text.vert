<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
(	(	kIx(	(
<g/>
kyrgyzsky	kyrgyzsky	k6eAd1	kyrgyzsky
К	К	k?	К
-	-	kIx~	-
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Biškek	Biškek	k6eAd1	Biškek
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Kyrgyzů	Kyrgyz	k1gInPc2	Kyrgyz
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
jazyk	jazyk	k1gInSc1	jazyk
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
turkických	turkický	k2eAgInPc2d1	turkický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Turkické	turkický	k2eAgInPc1d1	turkický
kmeny	kmen	k1gInPc1	kmen
přišly	přijít	k5eAaPmAgInP	přijít
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
původní	původní	k2eAgNnSc1d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
převalila	převalit	k5eAaPmAgFnS	převalit
ohromná	ohromný	k2eAgFnSc1d1	ohromná
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
Čingischánem	Čingischán	k1gMnSc7	Čingischán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
Kyrgyzové	Kyrgyzové	k2eAgInSc4d1	Kyrgyzové
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzové	Kyrgyz	k1gMnPc1	Kyrgyz
vedli	vést	k5eAaImAgMnP	vést
kočovný	kočovný	k2eAgInSc4d1	kočovný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
živili	živit	k5eAaImAgMnP	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
pastevci	pastevec	k1gMnPc1	pastevec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
carské	carský	k2eAgNnSc4d1	carské
Rusko	Rusko	k1gNnSc4	Rusko
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
snadné	snadný	k2eAgMnPc4d1	snadný
pastevce	pastevec	k1gMnPc4	pastevec
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
území	území	k1gNnSc1	území
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
součástí	součást	k1gFnSc7	součást
ruského	ruský	k2eAgInSc2d1	ruský
Turkestánu	Turkestán	k1gInSc2	Turkestán
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
uteklo	utéct	k5eAaPmAgNnS	utéct
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
pozdější	pozdní	k2eAgNnSc1d2	pozdější
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Biškek	Biškky	k1gFnPc2	Biškky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nucená	nucený	k2eAgFnSc1d1	nucená
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zcela	zcela	k6eAd1	zcela
zbořila	zbořit	k5eAaPmAgFnS	zbořit
tradiční	tradiční	k2eAgInSc4d1	tradiční
kočovný	kočovný	k2eAgInSc4d1	kočovný
život	život	k1gInSc4	život
Kyrgyzů	Kyrgyz	k1gMnPc2	Kyrgyz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
jako	jako	k8xS	jako
autonomní	autonomní	k2eAgFnSc1d1	autonomní
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1	Kyrgyzská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
zemi	zem	k1gFnSc4	zem
dnešní	dnešní	k2eAgFnSc2d1	dnešní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
tvořili	tvořit	k5eAaImAgMnP	tvořit
Kyrgyzové	Kyrgyz	k1gMnPc1	Kyrgyz
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Silnou	silný	k2eAgFnSc7d1	silná
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Rusové	Rus	k1gMnPc1	Rus
a	a	k8xC	a
Uzbeci	Uzbek	k1gMnPc1	Uzbek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
propukly	propuknout	k5eAaPmAgInP	propuknout
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
oblasti	oblast	k1gFnSc6	oblast
státu	stát	k1gInSc2	stát
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Oš	Oš	k1gFnSc2	Oš
etnické	etnický	k2eAgInPc4d1	etnický
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Uzbeci	Uzbek	k1gMnPc1	Uzbek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
cítili	cítit	k5eAaImAgMnP	cítit
slábnoucí	slábnoucí	k2eAgFnSc4d1	slábnoucí
moc	moc	k1gFnSc4	moc
rozpadajícího	rozpadající	k2eAgMnSc2d1	rozpadající
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
povstali	povstat	k5eAaPmAgMnP	povstat
proti	proti	k7c3	proti
zde	zde	k6eAd1	zde
menšinovým	menšinový	k2eAgInPc3d1	menšinový
Kyrgyzům	Kyrgyz	k1gInPc3	Kyrgyz
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k2eAgMnSc1d1	Kyrgyzstán
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Askar	Askar	k1gMnSc1	Askar
Akajev	Akajev	k1gMnSc1	Akajev
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
<g/>
.	.	kIx.	.
</s>
<s>
Akajev	Akajet	k5eAaPmDgInS	Akajet
zažil	zažít	k5eAaPmAgMnS	zažít
velký	velký	k2eAgInSc4d1	velký
skandál	skandál	k1gInSc4	skandál
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
museli	muset	k5eAaImAgMnP	muset
podat	podat	k5eAaPmF	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyli	být	k5eNaImAgMnP	být
kyrgyzstánské	kyrgyzstánský	k2eAgFnPc4d1	kyrgyzstánský
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
bylo	být	k5eAaImAgNnS	být
vypsáno	vypsán	k2eAgNnSc1d1	vypsáno
referendum	referendum	k1gNnSc1	referendum
o	o	k7c4	o
setrvání	setrvání	k1gNnSc4	setrvání
Akajeva	Akajevo	k1gNnSc2	Akajevo
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
setrvání	setrvání	k1gNnSc4	setrvání
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
přes	přes	k7c4	přes
96	[number]	k4	96
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
Akajev	Akajev	k1gMnSc1	Akajev
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc3	jeho
kampani	kampaň	k1gFnSc3	kampaň
napomáhala	napomáhat	k5eAaImAgFnS	napomáhat
státem	stát	k1gInSc7	stát
řízená	řízený	k2eAgFnSc1d1	řízená
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
šesti	šest	k4xCc2	šest
opozičních	opoziční	k2eAgMnPc2d1	opoziční
kandidátů	kandidát	k1gMnPc2	kandidát
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
už	už	k6eAd1	už
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
volby	volba	k1gFnPc4	volba
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
demokratické	demokratický	k2eAgNnSc4d1	demokratické
a	a	k8xC	a
svobodné	svobodný	k2eAgNnSc4d1	svobodné
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
referendum	referendum	k1gNnSc1	referendum
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Akajev	Akajev	k1gMnSc1	Akajev
posílil	posílit	k5eAaPmAgMnS	posílit
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Referendem	referendum	k1gNnSc7	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
změnil	změnit	k5eAaPmAgMnS	změnit
Akajev	Akajev	k1gMnSc1	Akajev
ústavu	ústav	k1gInSc2	ústav
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
jejich	jejich	k3xOp3gFnSc4	jejich
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgInS	zrušit
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
svobodě	svoboda	k1gFnSc3	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
veškerá	veškerý	k3xTgNnPc4	veškerý
média	médium	k1gNnPc4	médium
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tulipánová	tulipánový	k2eAgFnSc1d1	Tulipánová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
z	z	k7c2	z
OBSE	OBSE	kA	OBSE
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opoziční	opoziční	k2eAgMnPc1d1	opoziční
kandidáti	kandidát	k1gMnPc1	kandidát
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
všemožně	všemožně	k6eAd1	všemožně
znevýhodňovat	znevýhodňovat	k5eAaImF	znevýhodňovat
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
označeny	označit	k5eAaPmNgFnP	označit
za	za	k7c4	za
nesvobodné	svobodný	k2eNgNnSc4d1	nesvobodné
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
očekávaně	očekávaně	k6eAd1	očekávaně
Akajev	Akajev	k1gMnSc1	Akajev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
opět	opět	k6eAd1	opět
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
z	z	k7c2	z
OBSE	OBSE	kA	OBSE
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
voleb	volba	k1gFnPc2	volba
znamenalo	znamenat	k5eAaImAgNnS	znamenat
určité	určitý	k2eAgNnSc4d1	určité
zlepšení	zlepšení	k1gNnSc4	zlepšení
oproti	oproti	k7c3	oproti
prvnímu	první	k4xOgInSc3	první
kolu	kolo	k1gNnSc3	kolo
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
volby	volba	k1gFnPc1	volba
nevyhověly	vyhovět	k5eNaPmAgFnP	vyhovět
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
standardům	standard	k1gInPc3	standard
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
ze	z	k7c2	z
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
označili	označit	k5eAaPmAgMnP	označit
volby	volba	k1gFnPc4	volba
za	za	k7c4	za
dobře	dobře	k6eAd1	dobře
zorganizované	zorganizovaný	k2eAgNnSc4d1	zorganizované
<g/>
,	,	kIx,	,
svobodné	svobodný	k2eAgNnSc4d1	svobodné
a	a	k8xC	a
spravedlivé	spravedlivý	k2eAgNnSc4d1	spravedlivé
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
ale	ale	k8xC	ale
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
začaly	začít	k5eAaPmAgInP	začít
nepokoje	nepokoj	k1gInPc1	nepokoj
a	a	k8xC	a
demonstrace	demonstrace	k1gFnPc1	demonstrace
požadující	požadující	k2eAgFnPc1d1	požadující
od	od	k7c2	od
Akajeva	Akajev	k1gMnSc2	Akajev
odstoupení	odstoupení	k1gNnSc2	odstoupení
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Tulipánová	tulipánový	k2eAgFnSc1d1	Tulipánová
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
barevných	barevný	k2eAgFnPc2d1	barevná
revolucí	revoluce	k1gFnPc2	revoluce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Růžová	růžový	k2eAgFnSc1d1	růžová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akajev	Akajet	k5eAaPmDgInS	Akajet
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Tanajev	Tanajev	k1gMnSc1	Tanajev
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
opozice	opozice	k1gFnSc1	opozice
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
média	médium	k1gNnPc1	médium
a	a	k8xC	a
policie	policie	k1gFnSc1	policie
se	se	k3xPyFc4	se
z	z	k7c2	z
části	část	k1gFnSc2	část
rozprchla	rozprchnout	k5eAaPmAgFnS	rozprchnout
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
protestujícím	protestující	k2eAgMnPc3d1	protestující
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
přerostly	přerůst	k5eAaPmAgFnP	přerůst
v	v	k7c6	v
rabování	rabování	k1gNnSc6	rabování
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vypsané	vypsaný	k2eAgFnPc1d1	vypsaná
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
opozice	opozice	k1gFnSc1	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Felix	Felix	k1gMnSc1	Felix
Kulov	Kulov	k1gInSc1	Kulov
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
Kurmanbek	Kurmanbek	k1gMnSc1	Kurmanbek
Bakijev	Bakijev	k1gMnSc1	Bakijev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
pověřen	pověřit	k5eAaPmNgMnS	pověřit
již	již	k6eAd1	již
den	den	k1gInSc4	den
po	po	k7c6	po
útěku	útěk	k1gInSc6	útěk
Akajeva	Akajevo	k1gNnSc2	Akajevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšla	vyjít	k5eAaPmAgFnS	vyjít
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
prezident	prezident	k1gMnSc1	prezident
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
Kurmanbek	Kurmanbek	k1gMnSc1	Kurmanbek
Bakijev	Bakijev	k1gMnSc1	Bakijev
jako	jako	k8xS	jako
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
mezinárodně	mezinárodně	k6eAd1	mezinárodně
vnímán	vnímán	k2eAgInSc4d1	vnímán
jako	jako	k8xS	jako
prodemokratický	prodemokratický	k2eAgInSc4d1	prodemokratický
<g/>
,	,	kIx,	,
následný	následný	k2eAgInSc4d1	následný
vývoj	vývoj	k1gInSc4	vývoj
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
před	před	k7c7	před
Tulipánovou	tulipánový	k2eAgFnSc7d1	Tulipánová
revolucí	revoluce	k1gFnSc7	revoluce
(	(	kIx(	(
<g/>
potlačování	potlačování	k1gNnSc4	potlačování
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
nesvobodné	svobodný	k2eNgFnSc2d1	nesvobodná
volby	volba	k1gFnSc2	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
Kurmanbek	Kurmanbek	k1gInSc1	Kurmanbek
Bakijev	Bakijev	k1gFnSc2	Bakijev
zvolen	zvolit	k5eAaPmNgInS	zvolit
opětovně	opětovně	k6eAd1	opětovně
<g/>
,	,	kIx,	,
opozice	opozice	k1gFnPc4	opozice
však	však	k8xC	však
volby	volba	k1gFnPc4	volba
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
zfalšované	zfalšovaný	k2eAgNnSc4d1	zfalšované
<g/>
,	,	kIx,	,
znepokojení	znepokojení	k1gNnSc4	znepokojení
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
i	i	k9	i
OBSE	OBSE	kA	OBSE
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
obvinili	obvinit	k5eAaPmAgMnP	obvinit
opoziční	opoziční	k2eAgMnPc1d1	opoziční
aktivisté	aktivista	k1gMnPc1	aktivista
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
cenzurované	cenzurovaný	k2eAgNnSc4d1	cenzurované
vysílání	vysílání	k1gNnSc4	vysílání
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
blokovat	blokovat	k5eAaImF	blokovat
všechny	všechen	k3xTgInPc4	všechen
nezávislé	závislý	k2eNgInPc4d1	nezávislý
mediální	mediální	k2eAgInPc4d1	mediální
zdroje	zdroj	k1gInPc4	zdroj
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
hájila	hájit	k5eAaImAgFnS	hájit
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc4	tento
kroky	krok	k1gInPc4	krok
učinila	učinit	k5eAaImAgFnS	učinit
v	v	k7c4	v
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvyšujících	zvyšující	k2eAgFnPc2d1	zvyšující
se	se	k3xPyFc4	se
cen	cena	k1gFnPc2	cena
energií	energie	k1gFnPc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
impulzem	impulz	k1gInSc7	impulz
k	k	k7c3	k
následným	následný	k2eAgInPc3d1	následný
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
následně	následně	k6eAd1	následně
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
masově	masově	k6eAd1	masově
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Biškeku	Biškek	k1gInSc2	Biškek
a	a	k8xC	a
v	v	k7c6	v
Talasu	Talas	k1gInSc6	Talas
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnPc1	demonstrace
byly	být	k5eAaImAgFnP	být
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
proti	proti	k7c3	proti
současné	současný	k2eAgFnSc3d1	současná
vládě	vláda	k1gFnSc3	vláda
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
odstoupení	odstoupení	k1gNnSc2	odstoupení
prezidenta	prezident	k1gMnSc2	prezident
Kurmanbeka	Kurmanbeek	k1gMnSc2	Kurmanbeek
Bakijeva	Bakijev	k1gMnSc2	Bakijev
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
protestujících	protestující	k2eAgMnPc2d1	protestující
v	v	k7c6	v
Talasu	Talas	k1gInSc6	Talas
zbil	zbít	k5eAaPmAgMnS	zbít
i	i	k9	i
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stala	stát	k5eAaPmAgFnS	stát
sociální	sociální	k2eAgFnSc1d1	sociální
demokratka	demokratka	k1gFnSc1	demokratka
a	a	k8xC	a
představitelka	představitelka	k1gFnSc1	představitelka
opozice	opozice	k1gFnSc1	opozice
Roza	Roza	k1gFnSc1	Roza
Otunbajevová	Otunbajevová	k1gFnSc1	Otunbajevová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
návrh	návrh	k1gInSc4	návrh
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
svobodné	svobodný	k2eAgFnPc4d1	svobodná
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
etnické	etnický	k2eAgInPc4d1	etnický
nepokoje	nepokoj	k1gInPc4	nepokoj
mezi	mezi	k7c7	mezi
Kyrgyzy	Kyrgyz	k1gInPc7	Kyrgyz
a	a	k8xC	a
Uzbeky	Uzbek	k1gMnPc7	Uzbek
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
útoků	útok	k1gInPc2	útok
proti	proti	k7c3	proti
uzbecké	uzbecký	k2eAgFnSc3d1	Uzbecká
menšině	menšina	k1gFnSc3	menšina
<g/>
,	,	kIx,	,
hlášeno	hlásit	k5eAaImNgNnS	hlásit
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
rabování	rabování	k1gNnSc1	rabování
a	a	k8xC	a
žhářství	žhářství	k1gNnSc1	žhářství
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1	Kyrgyzská
vláda	vláda	k1gFnSc1	vláda
neměla	mít	k5eNaImAgFnS	mít
situaci	situace	k1gFnSc4	situace
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
do	do	k7c2	do
země	zem	k1gFnSc2	zem
vyslala	vyslat	k5eAaPmAgFnS	vyslat
nejméně	málo	k6eAd3	málo
150	[number]	k4	150
výsadkářů	výsadkář	k1gMnPc2	výsadkář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
chránili	chránit	k5eAaImAgMnP	chránit
ruské	ruský	k2eAgInPc4d1	ruský
vojenské	vojenský	k2eAgInPc4d1	vojenský
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
ruské	ruský	k2eAgMnPc4d1	ruský
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepokojích	nepokoj	k1gInPc6	nepokoj
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
400	[number]	k4	400
000	[number]	k4	000
opustilo	opustit	k5eAaPmAgNnS	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
110	[number]	k4	110
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
do	do	k7c2	do
sousedního	sousední	k2eAgInSc2d1	sousední
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
plánuje	plánovat	k5eAaImIp3nS	plánovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Eurasijské	Eurasijský	k2eAgFnSc2d1	Eurasijská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
a	a	k8xC	a
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
do	do	k7c2	do
Eurasijské	Eurasijský	k2eAgFnSc2d1	Eurasijská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
pohoří	pohoří	k1gNnPc2	pohoří
Ťan-Šan	Ťan-Šany	k1gInPc2	Ťan-Šany
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
Alajské	Alajský	k2eAgNnSc4d1	Alajský
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
Zaalajské	Zaalajský	k2eAgNnSc4d1	Zaalajský
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
už	už	k6eAd1	už
k	k	k7c3	k
Pamíru	Pamír	k1gInSc3	Pamír
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
je	být	k5eAaImIp3nS	být
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
polohou	poloha	k1gFnSc7	poloha
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
největší	veliký	k2eAgFnSc4d3	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
také	také	k9	také
značnou	značný	k2eAgFnSc7d1	značná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
94	[number]	k4	94
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
nad	nad	k7c4	nad
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
území	území	k1gNnSc6	území
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
nad	nad	k7c4	nad
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
rozdíly	rozdíl	k1gInPc7	rozdíl
zimních	zimní	k2eAgFnPc2d1	zimní
a	a	k8xC	a
letních	letní	k2eAgFnPc2d1	letní
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
výkyvy	výkyv	k1gInPc1	výkyv
teplot	teplota	k1gFnPc2	teplota
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jezera	jezero	k1gNnSc2	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
podobné	podobný	k2eAgNnSc1d1	podobné
přímořským	přímořský	k2eAgFnPc3d1	přímořská
oblastem	oblast	k1gFnPc3	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jaro	jaro	k1gNnSc1	jaro
bývá	bývat	k5eAaImIp3nS	bývat
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
opakujícími	opakující	k2eAgInPc7d1	opakující
se	se	k3xPyFc4	se
mrazíky	mrazík	k1gInPc7	mrazík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
začíná	začínat	k5eAaImIp3nS	začínat
jaro	jaro	k1gNnSc4	jaro
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Čujské	Čujský	k2eAgFnSc6d1	Čujský
a	a	k8xC	a
Ferganské	Ferganský	k2eAgFnSc6d1	Ferganská
nížině	nížina	k1gFnSc6	nížina
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
(	(	kIx(	(
<g/>
44	[number]	k4	44
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
horách	hora	k1gFnPc6	hora
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
letní	letní	k2eAgNnPc4d1	letní
maxima	maximum	k1gNnPc4	maximum
přes	přes	k7c4	přes
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnPc4d3	nejnižší
teploty	teplota	k1gFnPc4	teplota
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
přes	přes	k7c4	přes
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
Biškeku	Biškek	k1gInSc6	Biškek
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
absolutní	absolutní	k2eAgInSc1d1	absolutní
roční	roční	k2eAgInSc1d1	roční
teplotní	teplotní	k2eAgInSc1d1	teplotní
rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
76	[number]	k4	76
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
41,6	[number]	k4	41,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
min	min	kA	min
<g/>
.	.	kIx.	.
-	-	kIx~	-
<g/>
34,4	[number]	k4	34,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Narynu	Naryn	k1gInSc6	Naryn
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgInSc4d1	maximální
roční	roční	k2eAgInSc4d1	roční
výkyv	výkyv	k1gInSc4	výkyv
teplot	teplota	k1gFnPc2	teplota
71,6	[number]	k4	71,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
Oši	Oši	k1gFnSc6	Oši
64,6	[number]	k4	64,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c6	na
meteorologické	meteorologický	k2eAgFnSc6d1	meteorologická
stanici	stanice	k1gFnSc6	stanice
Ťan-Šan	Ťan-Šany	k1gInPc2	Ťan-Šany
64,3	[number]	k4	64,3
°	°	k?	°
<g/>
C.	C.	kA	C.
Množství	množství	k1gNnSc1	množství
atmosférických	atmosférický	k2eAgFnPc2d1	atmosférická
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
roční	roční	k2eAgNnSc1d1	roční
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
jiných	jiná	k1gFnPc6	jiná
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c6	na
západních	západní	k2eAgFnPc6d1	západní
<g/>
,	,	kIx,	,
jihozápadních	jihozápadní	k2eAgFnPc6d1	jihozápadní
a	a	k8xC	a
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
stranách	strana	k1gFnPc6	strana
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
vlhkých	vlhký	k2eAgInPc2d1	vlhký
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
proudů	proud	k1gInPc2	proud
jdoucích	jdoucí	k2eAgInPc2d1	jdoucí
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
svazích	svah	k1gInPc6	svah
Ferganského	Ferganský	k2eAgInSc2d1	Ferganský
hřbetu	hřbet	k1gInSc2	hřbet
je	být	k5eAaImIp3nS	být
roční	roční	k2eAgNnSc4d1	roční
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
srážkově	srážkově	k6eAd1	srážkově
chudým	chudý	k2eAgNnPc3d1	chudé
místům	místo	k1gNnPc3	místo
patří	patřit	k5eAaImIp3nS	patřit
Čujská	Čujská	k1gFnSc1	Čujská
a	a	k8xC	a
Talaská	Talaský	k2eAgFnSc1d1	Talaský
nížina	nížina	k1gFnSc1	nížina
(	(	kIx(	(
<g/>
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgNnSc1d3	nejmenší
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jezera	jezero	k1gNnSc2	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
(	(	kIx(	(
<g/>
v	v	k7c6	v
Balykči	Balykč	k1gInSc6	Balykč
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Ferganské	Ferganský	k2eAgFnSc2d1	Ferganská
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
Batken	Batken	k1gInSc1	Batken
-	-	kIx~	-
156	[number]	k4	156
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
roky	rok	k1gInPc7	rok
-	-	kIx~	-
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
rozdíly	rozdíl	k1gInPc4	rozdíl
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
(	(	kIx(	(
<g/>
od	od	k7c2	od
110	[number]	k4	110
mm	mm	kA	mm
do	do	k7c2	do
580	[number]	k4	580
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
vodní	vodní	k2eAgNnSc4d1	vodní
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
kyrgyzská	kyrgyzský	k2eAgFnSc1d1	Kyrgyzská
řeka	řeka	k1gFnSc1	řeka
Naryn	Naryn	k1gMnSc1	Naryn
(	(	kIx(	(
<g/>
535	[number]	k4	535
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
povodí	povodí	k1gNnSc2	povodí
53	[number]	k4	53
700	[number]	k4	700
km2	km2	k4	km2
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Syrdarji	Syrdarja	k1gFnSc6	Syrdarja
a	a	k8xC	a
pak	pak	k6eAd1	pak
do	do	k7c2	do
Aralského	aralský	k2eAgNnSc2d1	Aralské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Aralského	aralský	k2eAgNnSc2d1	Aralské
moře	moře	k1gNnSc2	moře
patří	patřit	k5eAaImIp3nS	patřit
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
(	(	kIx(	(
<g/>
76,6	[number]	k4	76,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Tarym	Tarym	k1gInSc1	Tarym
<g/>
,	,	kIx,	,
úmoří	úmoří	k1gNnSc1	úmoří
jezera	jezero	k1gNnSc2	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
a	a	k8xC	a
kazašského	kazašský	k2eAgNnSc2d1	kazašské
jezera	jezero	k1gNnSc2	jezero
Balchaš	Balchaš	k1gInSc1	Balchaš
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Naryn	Naryn	k1gInSc1	Naryn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc4d1	obrovský
potenciál	potenciál	k1gInSc4	potenciál
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
-	-	kIx~	-
elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
představuje	představovat	k5eAaImIp3nS	představovat
kyrgyzskou	kyrgyzský	k2eAgFnSc4d1	Kyrgyzská
hlavní	hlavní	k2eAgFnSc4d1	hlavní
vývozní	vývozní	k2eAgFnSc4d1	vývozní
komoditu	komodita	k1gFnSc4	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
kolem	kolem	k7c2	kolem
750	[number]	k4	750
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc1	jejich
celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
6836	[number]	k4	6836
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
3,4	[number]	k4	3,4
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
přes	přes	k7c4	přes
1	[number]	k4	1
km2	km2	k4	km2
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
11	[number]	k4	11
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Biškek	Biškek	k1gInSc1	Biškek
(	(	kIx(	(
<g/>
samosprávné	samosprávný	k2eAgNnSc1d1	samosprávné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Batkenská	Batkenský	k2eAgFnSc1d1	Batkenský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Batken	Batken	k1gInSc1	Batken
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Čujská	Čujský	k2eAgFnSc1d1	Čujský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Tokmok	Tokmok	k1gInSc1	Tokmok
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Džalalabádská	Džalalabádský	k2eAgFnSc1d1	Džalalabádský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Džalal-Abad	Džalal-Abad	k1gInSc1	Džalal-Abad
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Narynská	Narynský	k2eAgFnSc1d1	Narynský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Naryn	Naryn	k1gInSc1	Naryn
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ošská	Ošský	k2eAgFnSc1d1	Ošský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Oš	Oš	k1gFnSc1	Oš
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Talaská	Talaský	k2eAgFnSc1d1	Talaský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Talas	Talas	k1gInSc1	Talas
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Issykkulská	Issykkulský	k2eAgFnSc1d1	Issykkulský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Karakol	Karakol	k1gInSc1	Karakol
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Oš	Oš	k?	Oš
(	(	kIx(	(
<g/>
samosprávné	samosprávný	k2eAgNnSc1d1	samosprávné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
Nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
národnost	národnost	k1gFnSc1	národnost
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
představují	představovat	k5eAaImIp3nP	představovat
Kyrgyzové	Kyrgyzový	k2eAgFnPc1d1	Kyrgyzový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
jejich	jejich	k3xOp3gFnSc4	jejich
počet	počet	k1gInSc1	počet
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
velké	velký	k2eAgFnPc4d1	velká
většiny	většina	k1gFnPc4	většina
(	(	kIx(	(
<g/>
72,6	[number]	k4	72,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
5,8	[number]	k4	5,8
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
52,4	[number]	k4	52,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
podílu	podíl	k1gInSc2	podíl
Kyrgyzů	Kyrgyz	k1gMnPc2	Kyrgyz
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vliv	k1gInSc7	vliv
odchodu	odchod	k1gInSc2	odchod
především	především	k9	především
ruského	ruský	k2eAgNnSc2d1	ruské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
celkově	celkově	k6eAd1	celkově
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jen	jen	k9	jen
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
opustilo	opustit	k5eAaPmAgNnS	opustit
republiku	republika	k1gFnSc4	republika
110	[number]	k4	110
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
tvořili	tvořit	k5eAaImAgMnP	tvořit
Rusové	Rus	k1gMnPc1	Rus
30,2	[number]	k4	30,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
6,6	[number]	k4	6,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kyrgyzské	kyrgyzský	k2eAgFnSc2d1	Kyrgyzská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
emigrace	emigrace	k1gFnSc2	emigrace
a	a	k8xC	a
nižší	nízký	k2eAgFnSc2d2	nižší
porodnosti	porodnost	k1gFnSc2	porodnost
jejich	jejich	k3xOp3gInSc4	jejich
procentuální	procentuální	k2eAgInSc4d1	procentuální
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
6,4	[number]	k4	6,4
%	%	kIx~	%
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
0,2	[number]	k4	0,2
%	%	kIx~	%
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
žije	žít	k5eAaImIp3nS	žít
stále	stále	k6eAd1	stále
vysoké	vysoký	k2eAgNnSc1d1	vysoké
procento	procento	k1gNnSc1	procento
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
s	s	k7c7	s
nekyrgyzskou	kyrgyzský	k2eNgFnSc7d1	kyrgyzský
národností	národnost	k1gFnSc7	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
podle	podle	k7c2	podle
početnosti	početnost	k1gFnSc2	početnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
cca	cca	kA	cca
836	[number]	k4	836
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
Uzbeci	Uzbek	k1gMnPc1	Uzbek
(	(	kIx(	(
<g/>
14,4	[number]	k4	14,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
relativní	relativní	k2eAgInSc1d1	relativní
i	i	k8xC	i
absolutní	absolutní	k2eAgInSc1d1	absolutní
počet	počet	k1gInSc1	počet
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
80	[number]	k4	80
%	%	kIx~	%
žije	žít	k5eAaImIp3nS	žít
Uzbeků	Uzbek	k1gMnPc2	Uzbek
v	v	k7c4	v
Ošské	Ošský	k2eAgFnPc4d1	Ošský
a	a	k8xC	a
Džalalabadské	Džalalabadský	k2eAgFnPc4d1	Džalalabadský
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jsou	být	k5eAaImIp3nP	být
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
<g/>
,	,	kIx,	,
přesidlovali	přesidlovat	k5eAaImAgMnP	přesidlovat
se	se	k3xPyFc4	se
především	především	k9	především
z	z	k7c2	z
Poltavské	poltavský	k2eAgFnSc2d1	Poltavská
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bělorusy	Bělorus	k1gMnPc7	Bělorus
<g/>
)	)	kIx)	)
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
v	v	k7c6	v
Čujské	Čujský	k2eAgFnSc6d1	Čujský
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
dosahující	dosahující	k2eAgFnSc4d1	dosahující
14	[number]	k4	14
485	[number]	k4	485
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
sedminásobně	sedminásobně	k6eAd1	sedminásobně
nižší	nízký	k2eAgFnSc1d2	nižší
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
skupinou	skupina	k1gFnSc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
sem	sem	k6eAd1	sem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Povolží	Povolží	k1gNnSc2	Povolží
nechal	nechat	k5eAaPmAgMnS	nechat
deportovat	deportovat	k5eAaBmF	deportovat
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
Němců	Němec	k1gMnPc2	Němec
do	do	k7c2	do
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
počítat	počítat	k5eAaImF	počítat
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
německé	německý	k2eAgFnSc2d1	německá
kněžny	kněžna	k1gFnSc2	kněžna
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
provdané	provdaný	k2eAgInPc1d1	provdaný
za	za	k7c2	za
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
carevny	carevna	k1gFnSc2	carevna
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Veliké	veliký	k2eAgFnSc2d1	veliká
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
Němcům	Němec	k1gMnPc3	Němec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svým	svůj	k3xOyFgMnPc3	svůj
krajanům	krajan	k1gMnPc3	krajan
<g/>
,	,	kIx,	,
přestěhování	přestěhování	k1gNnSc2	přestěhování
do	do	k7c2	do
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
zajistila	zajistit	k5eAaPmAgFnS	zajistit
daňové	daňový	k2eAgFnPc4d1	daňová
úlevy	úleva	k1gFnPc4	úleva
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
dolní	dolní	k2eAgFnSc2d1	dolní
Volhy	Volha	k1gFnSc2	Volha
<g/>
,	,	kIx,	,
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
nebo	nebo	k8xC	nebo
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
,	,	kIx,	,
či	či	k8xC	či
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
těžké	těžký	k2eAgNnSc1d1	těžké
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Kyzgyzské	Kyzgyzský	k2eAgFnSc6d1	Kyzgyzský
SSR	SSR	kA	SSR
celkem	celek	k1gInSc7	celek
101	[number]	k4	101
309	[number]	k4	309
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
necelých	celý	k2eNgInPc2d1	necelý
9	[number]	k4	9
tisíc	tisíc	k4xCgInPc2	tisíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
se	se	k3xPyFc4	se
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Tádžikistánem	Tádžikistán	k1gInSc7	Tádžikistán
k	k	k7c3	k
nejméně	málo	k6eAd3	málo
ekonomicky	ekonomicky	k6eAd1	ekonomicky
rozvinutým	rozvinutý	k2eAgInPc3d1	rozvinutý
státům	stát	k1gInPc3	stát
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
těžební	těžební	k2eAgInSc4d1	těžební
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
hydroelektrárnách	hydroelektrárna	k1gFnPc6	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
ovce	ovce	k1gFnSc2	ovce
<g/>
,	,	kIx,	,
skot	skot	k1gInSc4	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc4	drůbež
a	a	k8xC	a
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
prezident	prezident	k1gMnSc1	prezident
Kurmanbek	Kurmanbek	k1gMnSc1	Kurmanbek
Bakijev	Bakijev	k1gMnSc1	Bakijev
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechá	nechat	k5eAaPmIp3nS	nechat
uzavřít	uzavřít	k5eAaPmF	uzavřít
americkou	americký	k2eAgFnSc4d1	americká
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Manasu	Manas	k1gInSc6	Manas
využívanou	využívaný	k2eAgFnSc7d1	využívaná
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
přestupní	přestupní	k2eAgInSc4d1	přestupní
bod	bod	k1gInSc4	bod
pro	pro	k7c4	pro
přesuny	přesun	k1gInPc4	přesun
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
zboží	zboží	k1gNnSc2	zboží
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
sice	sice	k8xC	sice
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
základnu	základna	k1gFnSc4	základna
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pouze	pouze	k6eAd1	pouze
změněna	změněn	k2eAgFnSc1d1	změněna
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
zvýšeno	zvýšen	k2eAgNnSc1d1	zvýšeno
nájemné	nájemné	k1gNnSc1	nájemné
za	za	k7c4	za
základnu	základna	k1gFnSc4	základna
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
existenci	existence	k1gFnSc6	existence
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
byl	být	k5eAaImAgInS	být
Kyrgyzstán	Kyrgyzstán	k1gInSc4	Kyrgyzstán
jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
i	i	k8xC	i
americkou	americký	k2eAgFnSc7d1	americká
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
základnou	základna	k1gFnSc7	základna
<g/>
.	.	kIx.	.
</s>
<s>
Pik	Pik	k1gMnSc1	Pik
Lenina	Lenin	k1gMnSc2	Lenin
Město	město	k1gNnSc4	město
Karakol	Karakola	k1gFnPc2	Karakola
a	a	k8xC	a
možnosti	možnost	k1gFnSc2	možnost
trekování	trekování	k1gNnSc2	trekování
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Ťan-šan	Ťan-šan	k1gInSc1	Ťan-šan
Jezero	jezero	k1gNnSc1	jezero
Issyk-kul	Issykula	k1gFnPc2	Issyk-kula
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Sary	Sara	k1gFnSc2	Sara
Čelek	Čelek	k1gMnSc1	Čelek
Věž	věž	k1gFnSc1	věž
Burana	buran	k1gMnSc2	buran
Hora	hora	k1gFnSc1	hora
Sulamain-Too	Sulamain-Too	k1gMnSc1	Sulamain-Too
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Biškek	Biškky	k1gFnPc2	Biškky
KOKAISL	KOKAISL	kA	KOKAISL
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
USMANOV	USMANOV	kA	USMANOV
<g/>
,	,	kIx,	,
Amirbek	Amirbek	k1gInSc1	Amirbek
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
očima	oko	k1gNnPc7	oko
pamětníků	pamětník	k1gInPc2	pamětník
<g/>
:	:	kIx,	:
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nostalgie	nostalgie	k1gFnSc1	nostalgie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905365	[number]	k4	905365
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905365	[number]	k4	905365
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
KOKAISL	KOKAISL	kA	KOKAISL
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
KOHOUTKOVÁ	Kohoutková	k1gFnSc1	Kohoutková
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
;	;	kIx,	;
KODAR	KODAR	kA	KODAR
<g/>
,	,	kIx,	,
Zamza	Zamza	k1gFnSc1	Zamza
<g/>
;	;	kIx,	;
DORDZHIEVA	DORDZHIEVA	kA	DORDZHIEVA
Yulia	Yulia	k1gFnSc1	Yulia
<g/>
;	;	kIx,	;
KOKAISLOVÁ	KOKAISLOVÁ	kA	KOKAISLOVÁ
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
:	:	kIx,	:
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
Kazachů	Kazach	k1gMnPc2	Kazach
<g/>
,	,	kIx,	,
turkmenských	turkmenský	k2eAgMnPc2d1	turkmenský
Belúdžů	Belúdž	k1gMnPc2	Belúdž
a	a	k8xC	a
Kyrgyzů	Kyrgyz	k1gMnPc2	Kyrgyz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nostalgie	nostalgie	k1gFnSc1	nostalgie
a	a	k8xC	a
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
spolupráci	spolupráce	k1gFnSc4	spolupráce
při	při	k7c6	při
Provozně	provozně	k6eAd1	provozně
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
fakultě	fakulta	k1gFnSc6	fakulta
ČZU	ČZU	kA	ČZU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
9365	[number]	k4	9365
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
KOKAISL	KOKAISL	kA	KOKAISL
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1	Kyrgyzstán
a	a	k8xC	a
Kyrgyzové	Kyrgyzové	k2eAgInSc1d1	Kyrgyzové
<g/>
.	.	kIx.	.
К	К	k?	К
и	и	k?	и
К	К	k?	К
<g/>
..	..	k?	..
Plzeň	Plzeň	k1gFnSc1	Plzeň
:	:	kIx,	:
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
antropologických	antropologický	k2eAgFnPc2d1	antropologická
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7043	[number]	k4	7043
<g/>
-	-	kIx~	-
<g/>
772	[number]	k4	772
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
KOKAISL	KOKAISL	kA	KOKAISL
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
PARGAČ	PARGAČ	kA	PARGAČ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Pastevecká	pastevecký	k2eAgFnSc1d1	pastevecká
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
119	[number]	k4	119
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
PARGAČ	PARGAČ	kA	PARGAČ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
VRHEL	VRHEL	kA	VRHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
etnicita	etnicita	k1gFnSc1	etnicita
<g/>
,	,	kIx,	,
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Poříčany	Poříčan	k1gMnPc7	Poříčan
:	:	kIx,	:
BCS	BCS	kA	BCS
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788090449107	[number]	k4	9788090449107
<g/>
.	.	kIx.	.
</s>
<s>
ROUX	ROUX	kA	ROUX
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
867	[number]	k4	867
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
-	-	kIx~	-
projekt	projekt	k1gInSc1	projekt
Sayakat	Sayakat	k1gFnSc2	Sayakat
Explore	Explor	k1gInSc5	Explor
Kyrgyzstan	Kyrgyzstan	k1gInSc4	Kyrgyzstan
Článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
-	-	kIx~	-
seřaď	seřadit	k5eAaPmRp2nS	seřadit
dle	dle	k7c2	dle
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
listech	list	k1gInPc6	list
Praktické	praktický	k2eAgFnSc2d1	praktická
informace	informace	k1gFnSc2	informace
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
na	na	k7c4	na
Za	za	k7c7	za
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
South	Southa	k1gFnPc2	Southa
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Kyrgyzstan	Kyrgyzstan	k1gInSc4	Kyrgyzstan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Astaně	Astana	k1gFnSc6	Astana
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ALLWORTH	ALLWORTH	kA	ALLWORTH
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
;	;	kIx,	;
SINOR	SINOR	kA	SINOR
<g/>
,	,	kIx,	,
Denis	Denisa	k1gFnPc2	Denisa
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kyrgyzstan	Kyrgyzstan	k1gInSc1	Kyrgyzstan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
