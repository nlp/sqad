<s>
Jacques-Louis	Jacques-Louis	k1gFnSc1	Jacques-Louis
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1748	[number]	k4	1748
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
klasicistní	klasicistní	k2eAgMnSc1d1	klasicistní
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
aktivním	aktivní	k2eAgMnSc7d1	aktivní
podporovatelem	podporovatel	k1gMnSc7	podporovatel
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
Maximiliána	Maximilián	k1gMnSc2	Maximilián
Robespierra	Robespierr	k1gMnSc2	Robespierr
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
jej	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
dvorním	dvorní	k2eAgMnSc7d1	dvorní
malířem	malíř	k1gMnSc7	malíř
<g/>
;	;	kIx,	;
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
musel	muset	k5eAaImAgMnS	muset
David	David	k1gMnSc1	David
opustit	opustit	k5eAaPmF	opustit
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
posledních	poslední	k2eAgNnPc2d1	poslední
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
početné	početný	k2eAgMnPc4d1	početný
žáky	žák	k1gMnPc4	žák
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k8xC	i
Jean	Jean	k1gMnSc1	Jean
Auguste	August	k1gMnSc5	August
Dominique	Dominique	k1gNnSc3	Dominique
Ingres	Ingres	k1gInSc1	Ingres
<g/>
.	.	kIx.	.
</s>
<s>
Jacques-Louis	Jacques-Louis	k1gFnPc2	Jacques-Louis
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1748	[number]	k4	1748
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
zámožného	zámožný	k2eAgMnSc2d1	zámožný
kupce	kupec	k1gMnSc2	kupec
Louise	Louis	k1gMnSc2	Louis
Maurice	Maurika	k1gFnSc3	Maurika
Davida	David	k1gMnSc2	David
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Genévive	Genéviev	k1gFnSc2	Genéviev
Davidové	Davidová	k1gFnSc2	Davidová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1757	[number]	k4	1757
mu	on	k3xPp3gNnSc3	on
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
staral	starat	k5eAaImAgMnS	starat
attický	attický	k2eAgMnSc1d1	attický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
tzv.	tzv.	kA	tzv.
neopoussinismu	neopoussinismus	k1gInSc3	neopoussinismus
Joseph	Joseph	k1gInSc4	Joseph
Maria	Mario	k1gMnSc2	Mario
Vien	Vien	k1gNnSc4	Vien
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
David	David	k1gMnSc1	David
intenzivně	intenzivně	k6eAd1	intenzivně
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
klasické	klasický	k2eAgNnSc4d1	klasické
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
Maloval	malovat	k5eAaImAgMnS	malovat
svoje	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
skici	skica	k1gFnSc2	skica
a	a	k8xC	a
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
filozofem	filozof	k1gMnSc7	filozof
a	a	k8xC	a
literárním	literární	k2eAgMnSc7d1	literární
historikem	historik	k1gMnSc7	historik
Michelem	Michel	k1gMnSc7	Michel
Jeanem	Jean	k1gMnSc7	Jean
Sedainem	Sedain	k1gMnSc7	Sedain
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
bydlel	bydlet	k5eAaImAgMnS	bydlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
římskou	římský	k2eAgFnSc4d1	římská
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
a	a	k8xC	a
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
ho	on	k3xPp3gNnSc4	on
doslova	doslova	k6eAd1	doslova
nadchnul	nadchnout	k5eAaPmAgMnS	nadchnout
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
zde	zde	k6eAd1	zde
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
a	a	k8xC	a
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
namaloval	namalovat	k5eAaPmAgMnS	namalovat
mnoho	mnoho	k4c4	mnoho
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
skic	skica	k1gFnPc2	skica
podle	podle	k7c2	podle
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
podle	podle	k7c2	podle
italských	italský	k2eAgFnPc2d1	italská
maleb	malba	k1gFnPc2	malba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
bolognských	bolognský	k2eAgMnPc2d1	bolognský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1779	[number]	k4	1779
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Pompeje	Pompeje	k1gFnPc4	Pompeje
a	a	k8xC	a
Herkulaneum	Herkulaneum	k1gInSc4	Herkulaneum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gNnSc4	on
nadchla	nadchnout	k5eAaPmAgFnS	nadchnout
antika	antika	k1gFnSc1	antika
a	a	k8xC	a
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Uchvátily	uchvátit	k5eAaPmAgInP	uchvátit
ho	on	k3xPp3gNnSc4	on
také	také	k9	také
obrazy	obraz	k1gInPc1	obraz
tehdy	tehdy	k6eAd1	tehdy
slavného	slavný	k2eAgMnSc4d1	slavný
Skota	Skot	k1gMnSc4	Skot
Gavina	Gavin	k1gMnSc4	Gavin
Hamiltona	Hamilton	k1gMnSc4	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
na	na	k7c6	na
salonu	salon	k1gInSc6	salon
vystavil	vystavit	k5eAaPmAgInS	vystavit
obraz	obraz	k1gInSc1	obraz
Belisar	Belisar	k1gInSc1	Belisar
prosící	prosící	k2eAgInSc1d1	prosící
o	o	k7c4	o
almužnu	almužna	k1gFnSc4	almužna
a	a	k8xC	a
Patroklův	Patroklův	k2eAgInSc4d1	Patroklův
pohřeb	pohřeb	k1gInSc4	pohřeb
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
funérailles	funérailles	k1gInSc1	funérailles
de	de	k?	de
Patrocle	Patrocle	k1gInSc1	Patrocle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechal	nechat	k5eNaPmAgMnS	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
pocty	pocta	k1gFnPc1	pocta
nejsou	být	k5eNaImIp3nP	být
nic	nic	k6eAd1	nic
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
ohlas	ohlas	k1gInSc1	ohlas
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
v	v	k7c6	v
Salonu	salon	k1gInSc6	salon
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
Přísaha	přísaha	k1gFnSc1	přísaha
Horatiů	Horatius	k1gMnPc2	Horatius
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jakoby	jakoby	k8xS	jakoby
malířskou	malířský	k2eAgFnSc7d1	malířská
manifestací	manifestace	k1gFnSc7	manifestace
k	k	k7c3	k
Francouzské	francouzský	k2eAgFnSc3d1	francouzská
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Jean	Jean	k1gMnSc1	Jean
Germain	Germain	k1gMnSc1	Germain
Drouais	Drouais	k1gFnSc1	Drouais
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
-	-	kIx~	-
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
Římskou	římský	k2eAgFnSc4d1	římská
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
Rome	Rom	k1gMnSc5	Rom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
žákem	žák	k1gMnSc7	žák
vydal	vydat	k5eAaPmAgMnS	vydat
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Drouais	Drouais	k1gInSc1	Drouais
namaloval	namalovat	k5eAaPmAgInS	namalovat
svého	svůj	k1gMnSc4	svůj
Marina	Marina	k1gFnSc1	Marina
v	v	k7c4	v
Minturnes	Minturnes	k1gInSc4	Minturnes
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
měl	mít	k5eAaImAgMnS	mít
Davidův	Davidův	k2eAgMnSc1d1	Davidův
Sokrates	Sokrates	k1gMnSc1	Sokrates
veliký	veliký	k2eAgInSc4d1	veliký
ohlas	ohlas	k1gInSc4	ohlas
na	na	k7c6	na
Salonu	salon	k1gInSc6	salon
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
tragický	tragický	k2eAgInSc1d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Germain	Germain	k1gMnSc1	Germain
Drouais	Drouais	k1gInSc4	Drouais
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
přijal	přijmout	k5eAaPmAgMnS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Bude	být	k5eAaImBp3nS	být
mi	já	k3xPp1nSc3	já
chybět	chybět	k5eAaImF	chybět
impuls	impuls	k1gInSc1	impuls
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
dramatický	dramatický	k2eAgInSc1d1	dramatický
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
klonil	klonit	k5eAaImAgMnS	klonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
revolucionářů	revolucionář	k1gMnPc2	revolucionář
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgInS	podporovat
Konvent	konvent	k1gInSc1	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
horlivým	horlivý	k2eAgMnSc7d1	horlivý
příznivcem	příznivec	k1gMnSc7	příznivec
Robespierra	Robespierr	k1gMnSc2	Robespierr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
členů	člen	k1gInPc2	člen
Výboru	výbor	k1gInSc2	výbor
veřejného	veřejný	k2eAgNnSc2d1	veřejné
blaha	blaho	k1gNnSc2	blaho
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
thermidorském	thermidorský	k2eAgInSc6d1	thermidorský
převratu	převrat	k1gInSc6	převrat
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
čas	čas	k1gInSc4	čas
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
měl	mít	k5eAaImAgInS	mít
čas	čas	k1gInSc4	čas
promýšlet	promýšlet	k5eAaImF	promýšlet
budoucí	budoucí	k2eAgFnSc4d1	budoucí
koncepci	koncepce	k1gFnSc4	koncepce
obrazu	obraz	k1gInSc2	obraz
Únos	únos	k1gInSc1	únos
Sabinek	Sabinek	k1gInSc1	Sabinek
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
-	-	kIx~	-
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
fascinován	fascinován	k2eAgInSc1d1	fascinován
svým	svůj	k3xOyFgInSc7	svůj
novým	nový	k2eAgInSc7d1	nový
idolem	idol	k1gInSc7	idol
-	-	kIx~	-
Napoleonem	Napoleon	k1gMnSc7	Napoleon
Bonapartem	bonapart	k1gInSc7	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgNnSc1	první
sezení	sezení	k1gNnSc1	sezení
malíři	malíř	k1gMnSc3	malíř
postačilo	postačit	k5eAaPmAgNnS	postačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděl	vidět	k5eAaImAgMnS	vidět
své	svůj	k3xOyFgNnSc4	svůj
božstvo	božstvo	k1gNnSc4	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
dvorním	dvorní	k2eAgMnSc7d1	dvorní
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
namaloval	namalovat	k5eAaPmAgMnS	namalovat
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poctě	pocta	k1gFnSc3	pocta
řadu	řad	k1gInSc2	řad
dalších	další	k2eAgInPc2d1	další
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
korunovace	korunovace	k1gFnSc1	korunovace
<g/>
,	,	kIx,	,
či	či	k8xC	či
Rozdílení	rozdílení	k1gNnSc1	rozdílení
orlů	orel	k1gMnPc2	orel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Bourbonů	bourbon	k1gInPc2	bourbon
musel	muset	k5eAaImAgMnS	muset
David	David	k1gMnSc1	David
zaplatit	zaplatit	k5eAaPmF	zaplatit
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
minulost	minulost	k1gFnSc4	minulost
královraha	královrah	k1gMnSc2	královrah
a	a	k8xC	a
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
epopeje	epopej	k1gFnSc2	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Belgii	Belgie	k1gFnSc4	Belgie
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
zůstat	zůstat	k5eAaPmF	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
únorového	únorový	k2eAgInSc2d1	únorový
večera	večer	k1gInSc2	večer
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
ho	on	k3xPp3gInSc4	on
srazil	srazit	k5eAaPmAgInS	srazit
povoz	povoz	k1gInSc1	povoz
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zdraví	zdravit	k5eAaImIp3nS	zdravit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nevrátilo	vrátit	k5eNaPmAgNnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
příbuzní	příbuzný	k1gMnPc1	příbuzný
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
převoz	převoz	k1gInSc4	převoz
těla	tělo	k1gNnSc2	tělo
do	do	k7c2	do
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
žádost	žádost	k1gFnSc1	žádost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
revolucionářské	revolucionářský	k2eAgFnSc3d1	revolucionářská
minulosti	minulost	k1gFnSc3	minulost
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
rukou	ruka	k1gFnPc6	ruka
ulpěla	ulpět	k5eAaPmAgFnS	ulpět
krev	krev	k1gFnSc1	krev
nevinných	vinný	k2eNgMnPc2d1	nevinný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
Francie	Francie	k1gFnSc1	Francie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zapovězena	zapovědět	k5eAaPmNgFnS	zapovědět
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
David	David	k1gMnSc1	David
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Viena	Vien	k1gInSc2	Vien
<g/>
,	,	kIx,	,
nepředstavoval	představovat	k5eNaImAgMnS	představovat
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
mistrův	mistrův	k2eAgInSc1d1	mistrův
klasicismus	klasicismus	k1gInSc1	klasicismus
jediný	jediný	k2eAgInSc4d1	jediný
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
rané	raný	k2eAgInPc1d1	raný
obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
barokem	baroko	k1gNnSc7	baroko
a	a	k8xC	a
Boucherovými	Boucherová	k1gFnPc7	Boucherová
rokokovými	rokokový	k2eAgInPc7d1	rokokový
díly	díl	k1gInPc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hledání	hledání	k1gNnSc6	hledání
se	se	k3xPyFc4	se
umělec	umělec	k1gMnSc1	umělec
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Pierre	Pierr	k1gInSc5	Pierr
Peyron	Peyron	k1gMnSc1	Peyron
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
stipendistou	stipendista	k1gMnSc7	stipendista
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
David	David	k1gMnSc1	David
rovněž	rovněž	k9	rovněž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dílo	dílo	k1gNnSc4	dílo
Přísaha	přísaha	k1gFnSc1	přísaha
Horatiů	Horatius	k1gMnPc2	Horatius
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
díla	dílo	k1gNnPc1	dílo
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Portrét	portrét	k1gInSc1	portrét
Stanislawa	Stanislawus	k1gMnSc2	Stanislawus
Kostky	Kostka	k1gMnSc2	Kostka
Potockeho	Potocke	k1gMnSc2	Potocke
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
Belisar	Belisar	k1gInSc1	Belisar
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úchvatný	úchvatný	k2eAgInSc4d1	úchvatný
obraz	obraz	k1gInSc4	obraz
Andromaché	Andromachý	k2eAgNnSc1d1	Andromaché
truchlící	truchlící	k2eAgNnSc1d1	truchlící
nad	nad	k7c7	nad
Hektorovým	Hektorův	k2eAgNnSc7d1	Hektorův
tělem	tělo	k1gNnSc7	tělo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
namaloval	namalovat	k5eAaPmAgMnS	namalovat
David	David	k1gMnSc1	David
obraz	obraz	k1gInSc4	obraz
Liktoři	liktor	k1gMnPc1	liktor
přinášející	přinášející	k2eAgFnSc1d1	přinášející
Brutovi	Brut	k1gMnSc3	Brut
mrtvoly	mrtvola	k1gFnSc2	mrtvola
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
nemalým	malý	k2eNgInSc7d1	nemalý
věhlasem	věhlas	k1gInSc7	věhlas
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
umístění	umístění	k1gNnSc4	umístění
na	na	k7c4	na
Salon	salon	k1gInSc4	salon
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1789	[number]	k4	1789
protestovali	protestovat	k5eAaBmAgMnP	protestovat
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
od	od	k7c2	od
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
Davida	David	k1gMnSc2	David
zcela	zcela	k6eAd1	zcela
pohltila	pohltit	k5eAaPmAgFnS	pohltit
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
velkou	velký	k2eAgFnSc4d1	velká
kresbu	kresba	k1gFnSc4	kresba
na	na	k7c4	na
námět	námět	k1gInSc4	námět
Přísaha	přísaha	k1gFnSc1	přísaha
v	v	k7c6	v
míčovně	míčovna	k1gFnSc6	míčovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledný	výsledný	k2eAgInSc1d1	výsledný
obraz	obraz	k1gInSc1	obraz
nedokončil	dokončit	k5eNaPmAgInS	dokončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mnoho	mnoho	k4c1	mnoho
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zde	zde	k6eAd1	zde
chtěl	chtít	k5eAaImAgMnS	chtít
zobrazit	zobrazit	k5eAaPmF	zobrazit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
popraveno	popraven	k2eAgNnSc1d1	popraveno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
obraz	obraz	k1gInSc4	obraz
Zavražděný	zavražděný	k2eAgInSc1d1	zavražděný
Marat	Marat	k1gInSc1	Marat
(	(	kIx(	(
<g/>
Musée	Musée	k1gFnSc1	Musée
Royaux	Royaux	k1gInSc1	Royaux
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1793	[number]	k4	1793
Marata	Marata	k1gFnSc1	Marata
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
Charlotte	Charlott	k1gInSc5	Charlott
Cordayová	Cordayová	k1gFnSc1	Cordayová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
chtěla	chtít	k5eAaImAgFnS	chtít
zamezit	zamezit	k5eAaPmF	zamezit
dalším	další	k2eAgFnPc3d1	další
masovým	masový	k2eAgFnPc3d1	masová
popravám	poprava	k1gFnPc3	poprava
<g/>
,	,	kIx,	,
osnovaných	osnovaný	k2eAgFnPc2d1	osnovaná
paranoidním	paranoidní	k2eAgMnSc7d1	paranoidní
tyranem	tyran	k1gMnSc7	tyran
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
byl	být	k5eAaImAgMnS	být
Maratovými	Maratův	k2eAgMnPc7d1	Maratův
stoupenci	stoupenec	k1gMnPc7	stoupenec
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
namaloval	namalovat	k5eAaPmAgInS	namalovat
obraz	obraz	k1gInSc1	obraz
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
despoty	despota	k1gMnSc2	despota
jako	jako	k8xS	jako
manifest	manifest	k1gInSc1	manifest
"	"	kIx"	"
<g/>
nezdolnosti	nezdolnost	k1gFnPc1	nezdolnost
revolucionářského	revolucionářský	k2eAgMnSc2d1	revolucionářský
ducha	duch	k1gMnSc2	duch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
obrazem	obraz	k1gInSc7	obraz
David	David	k1gMnSc1	David
jednak	jednak	k8xC	jednak
vnesl	vnést	k5eAaPmAgMnS	vnést
Marata	Marat	k1gMnSc4	Marat
do	do	k7c2	do
podvědomí	podvědomí	k1gNnSc2	podvědomí
národa	národ	k1gInSc2	národ
jako	jako	k8xC	jako
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jím	on	k3xPp3gInSc7	on
pak	pak	k6eAd1	pak
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Marat	Marat	k1gInSc1	Marat
leží	ležet	k5eAaImIp3nS	ležet
zavražděný	zavražděný	k2eAgMnSc1d1	zavražděný
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
stolku	stolek	k1gInSc6	stolek
spočívá	spočívat	k5eAaImIp3nS	spočívat
dopis	dopis	k1gInSc1	dopis
od	od	k7c2	od
Cordayové	Cordayová	k1gFnSc2	Cordayová
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
bezvládně	bezvládně	k6eAd1	bezvládně
spadlá	spadlý	k2eAgFnSc1d1	spadlá
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
pero	pero	k1gNnSc1	pero
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
moderních	moderní	k2eAgMnPc2d1	moderní
básníků	básník	k1gMnPc2	básník
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
drama	drama	k1gNnSc1	drama
pulzující	pulzující	k2eAgNnSc1d1	pulzující
srdceryvnou	srdceryvný	k2eAgFnSc7d1	srdceryvná
hrůzou	hrůza	k1gFnSc7	hrůza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
působením	působení	k1gNnSc7	působení
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
onen	onen	k3xDgInSc1	onen
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
Davidovým	Davidův	k2eAgNnSc7d1	Davidovo
veledílem	veledílo	k1gNnSc7	veledílo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejzajímavějších	zajímavý	k2eAgInPc2d3	nejzajímavější
příkladů	příklad	k1gInPc2	příklad
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
nezasažen	zasažen	k2eNgMnSc1d1	nezasažen
jakoukoli	jakýkoli	k3yIgFnSc7	jakýkoli
trivialitou	trivialita	k1gFnSc7	trivialita
nebo	nebo	k8xC	nebo
čímsi	čísi	k3xOyIgNnPc3	čísi
neušlechtilým	ušlechtilý	k2eNgNnPc3d1	neušlechtilé
<g/>
...	...	k?	...
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potrava	potrava	k1gFnSc1	potrava
silných	silný	k2eAgNnPc2d1	silné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
triumf	triumf	k1gInSc4	triumf
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
krutý	krutý	k2eAgInSc1d1	krutý
jako	jako	k8xC	jako
příroda	příroda	k1gFnSc1	příroda
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
přece	přece	k9	přece
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
aróma	aróma	k1gNnSc7	aróma
ideálu	ideál	k1gInSc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
fanatickým	fanatický	k2eAgMnSc7d1	fanatický
stoupencem	stoupenec	k1gMnSc7	stoupenec
Robespierra	Robespierr	k1gMnSc2	Robespierr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
namaloval	namalovat	k5eAaPmAgMnS	namalovat
svůj	svůj	k3xOyFgInSc4	svůj
autoportrét	autoportrét	k1gInSc4	autoportrét
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
zpodobnit	zpodobnit	k5eAaPmF	zpodobnit
jako	jako	k9	jako
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
namaloval	namalovat	k5eAaPmAgMnS	namalovat
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Lucemburskou	lucemburský	k2eAgFnSc4d1	Lucemburská
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Napoleonem	Napoleon	k1gMnSc7	Napoleon
Bonapartem	bonapart	k1gInSc7	bonapart
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tři	tři	k4xCgInPc4	tři
jeho	jeho	k3xOp3gNnSc1	jeho
portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc7d1	poslední
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
namaloval	namalovat	k5eAaPmAgMnS	namalovat
David	David	k1gMnSc1	David
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
historický	historický	k2eAgInSc4d1	historický
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
korunovace	korunovace	k1gFnSc1	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
Davidovi	David	k1gMnSc6	David
mimořádně	mimořádně	k6eAd1	mimořádně
zdařilo	zdařit	k5eAaPmAgNnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
nalezlo	nalézt	k5eAaBmAgNnS	nalézt
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
Marie	Maria	k1gFnSc2	Maria
Medicejské	Medicejský	k2eAgFnSc2d1	Medicejská
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc2	Jindřich
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
vlámský	vlámský	k2eAgMnSc1d1	vlámský
malíř	malíř	k1gMnSc1	malíř
Rubens	Rubensa	k1gFnPc2	Rubensa
<g/>
.	.	kIx.	.
</s>
<s>
Překvapení	překvapení	k1gNnSc1	překvapení
nad	nad	k7c7	nad
hotovým	hotový	k2eAgNnSc7d1	hotové
dílem	dílo	k1gNnSc7	dílo
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k8xC	i
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
hýbají	hýbat	k5eAaImIp3nP	hýbat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
živí	živit	k5eAaImIp3nP	živit
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
korunovace	korunovace	k1gFnSc1	korunovace
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
hodnotu	hodnota	k1gFnSc4	hodnota
historického	historický	k2eAgInSc2d1	historický
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
statičností	statičnost	k1gFnSc7	statičnost
celé	celý	k2eAgFnSc2d1	celá
kompozice	kompozice	k1gFnSc2	kompozice
se	se	k3xPyFc4	se
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
novoklasicistní	novoklasicistní	k2eAgFnSc3d1	novoklasicistní
estetice	estetika	k1gFnSc3	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
tu	tu	k6eAd1	tu
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
tolik	tolik	k6eAd1	tolik
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
neoklasicistní	neoklasicistní	k2eAgFnSc4d1	neoklasicistní
teatrálnost	teatrálnost	k1gFnSc4	teatrálnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Přísahy	přísaha	k1gFnSc2	přísaha
Horaciů	Horaci	k1gInPc2	Horaci
a	a	k8xC	a
Únosu	únos	k1gInSc2	únos
Sabinek	Sabinek	k1gInSc4	Sabinek
zde	zde	k6eAd1	zde
David	David	k1gMnSc1	David
čerpal	čerpat	k5eAaImAgMnS	čerpat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
ještě	ještě	k9	ještě
obraz	obraz	k1gInSc1	obraz
Rozdílení	rozdílení	k1gNnSc2	rozdílení
orlů	orel	k1gMnPc2	orel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
motivu	motiv	k1gInSc3	motiv
starověku	starověk	k1gInSc2	starověk
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
Leónidás	Leónidása	k1gFnPc2	Leónidása
u	u	k7c2	u
Thermopyl	Thermopyly	k1gFnPc2	Thermopyly
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
David	David	k1gMnSc1	David
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
především	především	k6eAd1	především
portrétům	portrét	k1gInPc3	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zde	zde	k6eAd1	zde
podobizny	podobizna	k1gFnPc1	podobizna
Emmanuela	Emmanuel	k1gMnSc2	Emmanuel
Sieyè	Sieyè	k1gFnSc1	Sieyè
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
Villeneuve	Villeneuev	k1gFnSc2	Villeneuev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
146	[number]	k4	146
soch	socha	k1gFnPc2	socha
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
budovy	budova	k1gFnSc2	budova
Hôtel	Hôtel	k1gInSc1	Hôtel
de	de	k?	de
ville	ville	k1gFnSc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
