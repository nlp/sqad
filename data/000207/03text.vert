<s>
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc4	bond
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Fišer	Fišer	k1gMnSc1	Fišer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
výrazný	výrazný	k2eAgMnSc1d1	výrazný
inspirátor	inspirátor	k1gMnSc1	inspirátor
českého	český	k2eAgInSc2d1	český
undergroundu	underground	k1gInSc2	underground
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
plukovníkem	plukovník	k1gMnSc7	plukovník
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Fišer	Fišer	k1gMnSc1	Fišer
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc4	bond
<g/>
,	,	kIx,	,
přerušil	přerušit	k5eAaPmAgMnS	přerušit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
bohémského	bohémský	k2eAgInSc2d1	bohémský
života	život	k1gInSc2	život
bez	bez	k7c2	bez
stálého	stálý	k2eAgInSc2d1	stálý
pracovního	pracovní	k2eAgInSc2d1	pracovní
poměru	poměr	k1gInSc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zapálený	zapálený	k2eAgMnSc1d1	zapálený
marxista	marxista	k1gMnSc1	marxista
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
vzpomínkové	vzpomínkový	k2eAgFnSc6d1	vzpomínková
knize	kniha	k1gFnSc6	kniha
Prvních	první	k4xOgFnPc2	první
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
rukopis	rukopis	k1gInSc1	rukopis
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
počátkem	počátek	k1gInSc7	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
"	"	kIx"	"
<g/>
zjevně	zjevně	k6eAd1	zjevně
jako	jako	k8xS	jako
individuum	individuum	k1gNnSc1	individuum
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
štítící	štítící	k2eAgFnSc1d1	štítící
<g/>
,	,	kIx,	,
kriminální	kriminální	k2eAgInSc1d1	kriminální
živel	živel	k1gInSc1	živel
a	a	k8xC	a
sanktusák	sanktusák	k?	sanktusák
-	-	kIx~	-
po	po	k7c4	po
několik	několik	k4yIc4	několik
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
zase	zase	k9	zase
i	i	k9	i
párkrát	párkrát	k6eAd1	párkrát
v	v	k7c6	v
blázinci	blázinec	k1gInSc6	blázinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pivo	pivo	k1gNnSc4	pivo
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
všeho	všecek	k3xTgNnSc2	všecek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s.	s.	k?	s.
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
pracovní	pracovní	k2eAgInSc1d1	pracovní
poměr	poměr	k1gInSc1	poměr
neměl	mít	k5eNaImAgInS	mít
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
živil	živit	k5eAaImAgMnS	živit
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
i	i	k9	i
žebrotou	žebrota	k1gFnSc7	žebrota
a	a	k8xC	a
drobnými	drobný	k2eAgFnPc7d1	drobná
krádežemi	krádež	k1gFnPc7	krádež
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Teigem	Teig	k1gMnSc7	Teig
a	a	k8xC	a
Závišem	Záviš	k1gMnSc7	Záviš
Kalandrou	Kalandra	k1gFnSc7	Kalandra
a	a	k8xC	a
navázal	navázat	k5eAaPmAgInS	navázat
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
básníkem	básník	k1gMnSc7	básník
Ivo	Ivo	k1gMnSc1	Ivo
Vodseďálkem	Vodseďálek	k1gMnSc7	Vodseďálek
<g/>
,	,	kIx,	,
nonkonformní	nonkonformní	k2eAgFnSc7d1	nonkonformní
literátkou	literátka	k1gFnSc7	literátka
Honzou	Honza	k1gMnSc7	Honza
Krejcarovou	krejcarový	k2eAgFnSc4d1	Krejcarová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Boudníkem	Boudník	k1gMnSc7	Boudník
a	a	k8xC	a
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Hrabalem	Hrabal	k1gMnSc7	Hrabal
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Honzou	Honza	k1gMnSc7	Honza
Krejcarovou	krejcarový	k2eAgFnSc4d1	Krejcarová
navázal	navázat	k5eAaPmAgMnS	navázat
též	též	k9	též
intimní	intimní	k2eAgInSc4d1	intimní
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
připravil	připravit	k5eAaPmAgMnS	připravit
surrealistický	surrealistický	k2eAgInSc4d1	surrealistický
sborník	sborník	k1gInSc4	sborník
Židovská	židovská	k1gFnSc1	židovská
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
15	[number]	k4	15
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
přispívajících	přispívající	k2eAgInPc2d1	přispívající
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgInS	zvolit
židovský	židovský	k2eAgInSc4d1	židovský
pseudonym	pseudonym	k1gInSc4	pseudonym
<g/>
,	,	kIx,	,
též	též	k9	též
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
antisemitskému	antisemitský	k2eAgNnSc3d1	antisemitské
zabarvení	zabarvení	k1gNnSc3	zabarvení
vykonstruovaných	vykonstruovaný	k2eAgInPc2d1	vykonstruovaný
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Fišer	Fišer	k1gMnSc1	Fišer
začal	začít	k5eAaPmAgMnS	začít
užívat	užívat	k5eAaImF	užívat
jméno	jméno	k1gNnSc4	jméno
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc5	bond
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
psal	psát	k5eAaImAgInS	psát
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
snil	snít	k5eAaImAgMnS	snít
o	o	k7c6	o
světové	světový	k2eAgFnSc6d1	světová
revoluci	revoluce	k1gFnSc6	revoluce
i	i	k8xC	i
o	o	k7c6	o
snadném	snadný	k2eAgNnSc6d1	snadné
zbohatnutí	zbohatnutí	k1gNnSc6	zbohatnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
během	během	k7c2	během
svérázného	svérázný	k2eAgInSc2d1	svérázný
výletu	výlet	k1gInSc2	výlet
(	(	kIx(	(
<g/>
či	či	k8xC	či
pokusu	pokus	k1gInSc2	pokus
o	o	k7c6	o
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
)	)	kIx)	)
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
zažil	zažít	k5eAaPmAgMnS	zažít
kuriózní	kuriózní	k2eAgNnSc1d1	kuriózní
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
provedení	provedení	k1gNnSc4	provedení
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
sovětské	sovětský	k2eAgNnSc4d1	sovětské
vedení	vedení	k1gNnSc4	vedení
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
jak	jak	k8xS	jak
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
dvě	dva	k4xCgFnPc1	dva
krabičky	krabička	k1gFnPc1	krabička
od	od	k7c2	od
sirek	sirka	k1gFnPc2	sirka
s	s	k7c7	s
blechami	blecha	k1gFnPc7	blecha
-	-	kIx~	-
ty	ty	k3xPp2nSc1	ty
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
ochoten	ochoten	k2eAgMnSc1d1	ochoten
dodat	dodat	k5eAaPmF	dodat
gratis	gratis	k6eAd1	gratis
-	-	kIx~	-
naočkovanými	naočkovaný	k2eAgNnPc7d1	naočkované
všemi	všecek	k3xTgInPc7	všecek
prevítskými	prevítský	k2eAgInPc7d1	prevítský
mory	mor	k1gInPc7	mor
světa	svět	k1gInSc2	svět
<g/>
...	...	k?	...
a	a	k8xC	a
moji	můj	k3xOp1gMnPc1	můj
agenti	agent	k1gMnPc1	agent
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
vypustí	vypustit	k5eAaPmIp3nS	vypustit
ty	ten	k3xDgFnPc4	ten
blechy	blecha	k1gFnPc4	blecha
při	při	k7c6	při
manifestaci	manifestace	k1gFnSc6	manifestace
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pod	pod	k7c7	pod
tribunou	tribuna	k1gFnSc7	tribuna
na	na	k7c6	na
Rudém	rudý	k2eAgNnSc6d1	Rudé
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
sice	sice	k8xC	sice
nějaký	nějaký	k3yIgInSc4	nějaký
ten	ten	k3xDgInSc4	ten
počet	počet	k1gInSc4	počet
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
blechy	blecha	k1gFnPc1	blecha
přeskáčou	přeskákat	k5eAaPmIp3nP	přeskákat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zaručeně	zaručeně	k6eAd1	zaručeně
i	i	k9	i
na	na	k7c4	na
tribunu	tribuna	k1gFnSc4	tribuna
a	a	k8xC	a
skočí	skočit	k5eAaPmIp3nS	skočit
i	i	k9	i
na	na	k7c4	na
maršála	maršál	k1gMnSc4	maršál
Stalina	Stalin	k1gMnSc4	Stalin
<g/>
,	,	kIx,	,
na	na	k7c4	na
maršála	maršál	k1gMnSc4	maršál
Vorošilova	Vorošilův	k2eAgMnSc4d1	Vorošilův
<g/>
,	,	kIx,	,
na	na	k7c4	na
soudruha	soudruh	k1gMnSc4	soudruh
Beriju	Beriju	k1gFnPc2	Beriju
i	i	k9	i
na	na	k7c4	na
soudruha	soudruh	k1gMnSc4	soudruh
Molotova	Molotův	k2eAgMnSc4d1	Molotův
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
,	,	kIx,	,
s.	s.	k?	s.
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bondy	bond	k1gInPc4	bond
alespoň	alespoň	k9	alespoň
vyinkasoval	vyinkasovat	k5eAaPmAgInS	vyinkasovat
nějaké	nějaký	k3yIgInPc4	nějaký
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
marxistickou	marxistický	k2eAgFnSc7d1	marxistická
<g/>
,	,	kIx,	,
čínskou	čínský	k2eAgFnSc7d1	čínská
a	a	k8xC	a
indickou	indický	k2eAgFnSc7d1	indická
ho	on	k3xPp3gMnSc4	on
přiměl	přimět	k5eAaPmAgInS	přimět
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
na	na	k7c6	na
Gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hellichově	Hellichův	k2eAgFnSc6d1	Hellichova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
dálkové	dálkový	k2eAgNnSc4d1	dálkové
studium	studium	k1gNnSc4	studium
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
psychologie	psychologie	k1gFnSc2	psychologie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nepodal	podat	k5eNaPmAgMnS	podat
včas	včas	k6eAd1	včas
přihlášku	přihláška	k1gFnSc4	přihláška
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
rovnou	rovnou	k6eAd1	rovnou
za	za	k7c7	za
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
tam	tam	k6eAd1	tam
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
tam	tam	k6eAd1	tam
jeho	jeho	k3xOp3gMnSc1	jeho
tajemník	tajemník	k1gMnSc1	tajemník
Salač	Salač	k1gMnSc1	Salač
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
alkoholik	alkoholik	k1gMnSc1	alkoholik
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jsme	být	k5eAaImIp1nP	být
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
dýchli	dýchnout	k5eAaPmAgMnP	dýchnout
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jsme	být	k5eAaImIp1nP	být
poznali	poznat	k5eAaPmAgMnP	poznat
bratry	bratr	k1gMnPc7	bratr
v	v	k7c6	v
triku	trik	k1gInSc6	trik
<g/>
.	.	kIx.	.
</s>
<s>
Salač	Salač	k1gMnSc1	Salač
mne	já	k3xPp1nSc4	já
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
dělnického	dělnický	k2eAgMnSc4d1	dělnický
kádra	kádr	k1gMnSc4	kádr
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
ke	k	k7c3	k
zkušební	zkušební	k2eAgFnSc3d1	zkušební
komisi	komise	k1gFnSc3	komise
<g/>
,	,	kIx,	,
že	že	k8xS	že
naposled	naposled	k6eAd1	naposled
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
přezkoušet	přezkoušet	k5eAaPmF	přezkoušet
mne	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tím	ten	k3xDgNnSc7	ten
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
hotovo	hotov	k2eAgNnSc1d1	hotovo
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
,	,	kIx,	,
s.	s.	k?	s.
88	[number]	k4	88
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
navíc	navíc	k6eAd1	navíc
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
noční	noční	k2eAgInSc4d1	noční
hlídač	hlídač	k1gInSc4	hlídač
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
v	v	k7c6	v
bibliografickém	bibliografický	k2eAgNnSc6d1	bibliografické
oddělení	oddělení	k1gNnSc6	oddělení
Státní	státní	k2eAgFnSc2d1	státní
knihovny	knihovna	k1gFnSc2	knihovna
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
publikovat	publikovat	k5eAaBmF	publikovat
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
,	,	kIx,	,
Filozofický	filozofický	k2eAgInSc1d1	filozofický
časopis	časopis	k1gInSc1	časopis
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
mu	on	k3xPp3gMnSc3	on
vyšly	vyjít	k5eAaPmAgInP	vyjít
pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Otázky	otázka	k1gFnPc1	otázka
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
Útěcha	útěcha	k1gFnSc1	útěcha
z	z	k7c2	z
ontologie	ontologie	k1gFnSc2	ontologie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
filozofické	filozofický	k2eAgFnPc4d1	filozofická
práce	práce	k1gFnPc4	práce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
hodnost	hodnost	k1gFnSc4	hodnost
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
invalidním	invalidní	k2eAgInSc6d1	invalidní
důchodu	důchod	k1gInSc6	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Martinem	Martin	k1gMnSc7	Martin
Jirousem	Jirous	k1gMnSc7	Jirous
<g/>
,	,	kIx,	,
manažerem	manažer	k1gMnSc7	manažer
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gInPc2	jeho
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
ranou	raný	k2eAgFnSc4d1	raná
tvorbu	tvorba	k1gFnSc4	tvorba
Jáchyma	Jáchym	k1gMnSc2	Jáchym
Topola	Topol	k1gMnSc2	Topol
nebo	nebo	k8xC	nebo
J.	J.	kA	J.
H.	H.	kA	H.
Krchovského	Krchovský	k2eAgMnSc4d1	Krchovský
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bondy	bond	k1gInPc1	bond
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
nikdy	nikdy	k6eAd1	nikdy
neztotožnil	ztotožnit	k5eNaPmAgMnS	ztotožnit
s	s	k7c7	s
podobou	podoba	k1gFnSc7	podoba
poúnorového	poúnorový	k2eAgInSc2d1	poúnorový
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
vytrvalým	vytrvalý	k2eAgMnSc7d1	vytrvalý
kritikem	kritik	k1gMnSc7	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
však	však	k9	však
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Mao	Mao	k1gFnSc1	Mao
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Oskar	Oskar	k1gMnSc1	Oskar
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
udání	udání	k1gNnSc4	udání
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
bývalou	bývalý	k2eAgFnSc4d1	bývalá
družku	družka	k1gFnSc4	družka
Janu	Jana	k1gFnSc4	Jana
Krejcarovou	krejcarový	k2eAgFnSc4d1	Krejcarová
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Jesenskou	Jesenská	k1gFnSc4	Jesenská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc4	její
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
sociologa	sociolog	k1gMnSc4	sociolog
Miloše	Miloš	k1gMnSc4	Miloš
Černého	Černý	k1gMnSc4	Černý
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Bondyho	Bondy	k1gMnSc2	Bondy
udání	udání	k1gNnSc4	udání
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosofy	filosof	k1gMnPc4	filosof
Jana	Jan	k1gMnSc4	Jan
Patočku	Patočka	k1gMnSc4	Patočka
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc4	Ivan
Dubského	Dubský	k1gMnSc4	Dubský
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc4	Jiří
Němce	Němec	k1gMnSc4	Němec
nebo	nebo	k8xC	nebo
mladé	mladý	k2eAgMnPc4d1	mladý
začínající	začínající	k2eAgMnPc4d1	začínající
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
vydával	vydávat	k5eAaPmAgMnS	vydávat
prorežimní	prorežimní	k2eAgFnPc4d1	prorežimní
marxistické	marxistický	k2eAgFnPc4d1	marxistická
publikace	publikace	k1gFnPc4	publikace
a	a	k8xC	a
po	po	k7c4	po
tři	tři	k4xCgNnPc4	tři
desetiletí	desetiletí	k1gNnPc4	desetiletí
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
režimu	režim	k1gInSc3	režim
důležité	důležitý	k2eAgFnSc2d1	důležitá
kontrarozvědné	kontrarozvědný	k2eAgFnSc2d1	kontrarozvědná
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
rozvědné	rozvědný	k2eAgFnPc4d1	rozvědná
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc2d1	velká
výhrady	výhrada	k1gFnSc2	výhrada
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ultralevicových	ultralevicový	k2eAgFnPc6d1	ultralevicová
politických	politický	k2eAgFnPc6d1	politická
formacích	formace	k1gFnPc6	formace
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
Levý	levý	k2eAgInSc1d1	levý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
příspěvky	příspěvek	k1gInPc1	příspěvek
do	do	k7c2	do
Haló	haló	k0	haló
novin	novina	k1gFnPc2	novina
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
politologické	politologický	k2eAgInPc4d1	politologický
eseje	esej	k1gInPc4	esej
<g/>
.	.	kIx.	.
</s>
<s>
Hlásil	hlásit	k5eAaImAgInS	hlásit
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
k	k	k7c3	k
trockismu	trockismus	k1gInSc3	trockismus
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
maoismu	maoismus	k1gInSc3	maoismus
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
k	k	k7c3	k
anarchismu	anarchismus	k1gInSc3	anarchismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
Egona	Egon	k1gMnSc2	Egon
Hostovského	Hostovský	k2eAgMnSc2d1	Hostovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
formy	forma	k1gFnSc2	forma
protestu	protest	k1gInSc2	protest
proti	proti	k7c3	proti
rozdělení	rozdělení	k1gNnSc3	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
tamější	tamější	k2eAgFnSc6d1	tamější
Univerzitě	univerzita	k1gFnSc6	univerzita
Komenského	Komenského	k2eAgFnSc4d1	Komenského
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Požoň	Požoň	k1gMnSc1	Požoň
sentimental	sentimental	k1gMnSc1	sentimental
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc3d1	hrající
hospodské	hospodský	k2eAgFnPc4d1	hospodská
folklórní	folklórní	k2eAgFnPc4d1	folklórní
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Jaroslavou	Jaroslava	k1gFnSc7	Jaroslava
Krčmaříkovou	Krčmaříkův	k2eAgFnSc7d1	Krčmaříkův
má	mít	k5eAaImIp3nS	mít
Bondy	bond	k1gInPc4	bond
syna	syn	k1gMnSc4	syn
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Fišera	Fišer	k1gMnSc2	Fišer
ml.	ml.	kA	ml.
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příležitostného	příležitostný	k2eAgMnSc4d1	příležitostný
básníka	básník	k1gMnSc4	básník
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
Lesbický	lesbický	k2eAgInSc1d1	lesbický
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc7d1	životní
družkou	družka	k1gFnSc7	družka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
stala	stát	k5eAaPmAgFnS	stát
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
Julie	Julie	k1gFnSc1	Julie
Nováková	Nováková	k1gFnSc1	Nováková
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
napsal	napsat	k5eAaBmAgInS	napsat
paměti	paměť	k1gFnSc2	paměť
Prvních	první	k4xOgInPc2	první
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc1d1	zachycující
jeho	jeho	k3xOp3gNnSc4	jeho
bouřlivé	bouřlivý	k2eAgNnSc4d1	bouřlivé
období	období	k1gNnSc4	období
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
období	období	k1gNnSc6	období
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgFnP	mít
vyjít	vyjít	k5eAaPmF	vyjít
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vyšly	vyjít	k5eAaPmAgFnP	vyjít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obecnějšího	obecní	k2eAgNnSc2d2	obecní
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
vešel	vejít	k5eAaPmAgInS	vejít
však	však	k9	však
nejvíce	nejvíce	k6eAd1	nejvíce
díky	díky	k7c3	díky
knihám	kniha	k1gFnPc3	kniha
Bohumil	Bohumila	k1gFnPc2	Bohumila
Hrabala	hrabat	k5eAaImAgFnS	hrabat
Morytáty	morytát	k1gInPc4	morytát
a	a	k8xC	a
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
Něžný	něžný	k2eAgMnSc1d1	něžný
barbar	barbar	k1gMnSc1	barbar
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
některým	některý	k3yIgFnPc3	některý
dalším	další	k2eAgFnPc3d1	další
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
paměti	paměť	k1gFnSc2	paměť
Prvních	první	k4xOgInPc2	první
deset	deset	k4xCc1	deset
let	let	k1gInSc4	let
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
film	film	k1gInSc4	film
3	[number]	k4	3
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
77	[number]	k4	77
let	léto	k1gNnPc2	léto
na	na	k7c4	na
následky	následek	k1gInPc4	následek
popálenin	popálenina	k1gFnPc2	popálenina
třetího	třetí	k4xOgInSc2	třetí
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
po	po	k7c4	po
vzplanutí	vzplanutí	k1gNnSc4	vzplanutí
pyžama	pyžama	k1gNnSc2	pyžama
od	od	k7c2	od
vlastní	vlastní	k2eAgFnSc2d1	vlastní
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Básnické	básnický	k2eAgNnSc1d1	básnické
dílo	dílo	k1gNnSc1	dílo
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
40	[number]	k4	40
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Poezii	poezie	k1gFnSc4	poezie
psal	psát	k5eAaImAgMnS	psát
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnPc1d3	nejstarší
zachované	zachovaný	k2eAgFnPc1d1	zachovaná
práce	práce	k1gFnPc1	práce
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ivo	Ivo	k1gMnSc1	Ivo
Vodseďálkem	Vodseďálek	k1gMnSc7	Vodseďálek
spoluzaložil	spoluzaložit	k5eAaPmAgMnS	spoluzaložit
samizdatovou	samizdatový	k2eAgFnSc4d1	samizdatová
edici	edice	k1gFnSc4	edice
Půlnoc	půlnoc	k1gFnSc4	půlnoc
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
/	/	kIx~	/
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc7	jenž
pro	pro	k7c4	pro
pár	pár	k4xCyI	pár
přátel	přítel	k1gMnPc2	přítel
oba	dva	k4xCgMnPc1	dva
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
11	[number]	k4	11
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Bondyho	Bondy	k1gMnSc2	Bondy
raného	raný	k2eAgNnSc2d1	rané
díla	dílo	k1gNnSc2	dílo
-	-	kIx~	-
Totální	totální	k2eAgInSc1d1	totální
realismus	realismus	k1gInSc1	realismus
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
název	název	k1gInSc1	název
celému	celý	k2eAgInSc3d1	celý
jeho	jeho	k3xOp3gInSc3	jeho
tvůrčímu	tvůrčí	k2eAgInSc3d1	tvůrčí
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
dosud	dosud	k6eAd1	dosud
vyznávaného	vyznávaný	k2eAgInSc2d1	vyznávaný
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
neradostnou	radostný	k2eNgFnSc4d1	neradostná
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
budovatelské	budovatelský	k2eAgFnPc1d1	budovatelská
fráze	fráze	k1gFnPc1	fráze
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
prožívanou	prožívaný	k2eAgFnSc7d1	prožívaná
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Lidovým	lidový	k2eAgInSc7d1	lidový
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
záměrně	záměrně	k6eAd1	záměrně
neumělým	umělý	k2eNgInSc7d1	neumělý
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
užíváním	užívání	k1gNnSc7	užívání
argotu	argot	k1gInSc2	argot
mají	mít	k5eAaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
ironický	ironický	k2eAgInSc4d1	ironický
nádech	nádech	k1gInSc4	nádech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
básních	báseň	k1gFnPc6	báseň
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
Christanem	Christan	k1gInSc7	Christan
Morgensternem	Morgenstern	k1gInSc7	Morgenstern
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
není	být	k5eNaImIp3nS	být
rozsahem	rozsah	k1gInSc7	rozsah
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
jsou	být	k5eAaImIp3nP	být
koncipovány	koncipován	k2eAgInPc1d1	koncipován
větší	veliký	k2eAgInPc1d2	veliký
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
Deník	deník	k1gInSc1	deník
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
Egona	Egon	k1gMnSc4	Egon
Bondyho	Bondy	k1gMnSc4	Bondy
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
politicky	politicky	k6eAd1	politicky
angažované	angažovaný	k2eAgInPc1d1	angažovaný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
tvorbě	tvorba	k1gFnSc6	tvorba
objevují	objevovat	k5eAaImIp3nP	objevovat
expresivní	expresivní	k2eAgInPc1d1	expresivní
výrazy	výraz	k1gInPc1	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
odmítání	odmítání	k1gNnSc2	odmítání
reálného	reálný	k2eAgInSc2d1	reálný
socialismu	socialismus	k1gInSc2	socialismus
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
motiv	motiv	k1gInSc1	motiv
odmítání	odmítání	k1gNnSc2	odmítání
konzumní	konzumní	k2eAgFnSc2d1	konzumní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Výkon	výkon	k1gInSc1	výkon
a	a	k8xC	a
spotřeba	spotřeba	k1gFnSc1	spotřeba
-	-	kIx~	-
to	ten	k3xDgNnSc4	ten
jediné	jediné	k1gNnSc4	jediné
se	se	k3xPyFc4	se
ceníVe	ceníVat	k5eAaPmIp3nS	ceníVat
světě	svět	k1gInSc6	svět
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
sněj	sněj	k?	sněj
o	o	k7c6	o
uplatněnía	uplatněnía	k6eAd1	uplatněnía
kdo	kdo	k3yInSc1	kdo
nevěří	věřit	k5eNaImIp3nS	věřit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
omámenímoc	omámenímoc	k6eAd1	omámenímoc
brzo	brzo	k6eAd1	brzo
zjistí	zjistit	k5eAaPmIp3nS	zjistit
že	že	k8xS	že
už	už	k6eAd1	už
člověkem	člověk	k1gMnSc7	člověk
neníje	neníj	k1gFnSc2	neníj
jen	jen	k9	jen
nástrojem	nástroj	k1gInSc7	nástroj
k	k	k7c3	k
upotřebení	upotřebení	k1gNnSc3	upotřebení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgFnSc1d3	veliký
radost	radost	k1gFnSc1	radost
mám	mít	k5eAaImIp1nS	mít
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
pana	pan	k1gMnSc2	pan
prezidenta	prezident	k1gMnSc2	prezident
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Dvě	dva	k4xCgFnPc1	dva
léta	léto	k1gNnPc4	léto
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
družky	družka	k1gFnSc2	družka
Julie	Julie	k1gFnSc2	Julie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
poezii	poezie	k1gFnSc4	poezie
psát	psát	k5eAaImF	psát
přestal	přestat	k5eAaPmAgInS	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnPc4	nakladatelství
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
soubor	soubor	k1gInSc1	soubor
jeho	on	k3xPp3gNnSc2	on
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
básní	báseň	k1gFnPc2	báseň
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
skupina	skupina	k1gFnSc1	skupina
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusech	pokus	k1gInPc6	pokus
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
např.	např.	kA	např.
román	román	k1gInSc4	román
2000	[number]	k4	2000
z	z	k7c2	z
r.	r.	kA	r.
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
Bondy	bond	k1gInPc4	bond
s	s	k7c7	s
prózou	próza	k1gFnSc7	próza
až	až	k8xS	až
počátkem	počátek	k1gInSc7	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
Sklepní	sklepní	k2eAgFnSc1d1	sklepní
práce	práce	k1gFnSc1	práce
(	(	kIx(	(
<g/>
samizdat	samizdat	k1gInSc1	samizdat
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
Toronto	Toronto	k1gNnSc4	Toronto
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
rovina	rovina	k1gFnSc1	rovina
groteskní	groteskní	k2eAgFnSc2d1	groteskní
současnosti	současnost	k1gFnSc2	současnost
s	s	k7c7	s
linií	linie	k1gFnSc7	linie
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
nadpřirozena	nadpřirozeno	k1gNnSc2	nadpřirozeno
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
dává	dávat	k5eAaImIp3nS	dávat
autor	autor	k1gMnSc1	autor
najevo	najevo	k6eAd1	najevo
znechucení	znechucení	k1gNnSc1	znechucení
reálným	reálný	k2eAgInSc7d1	reálný
socialismem	socialismus	k1gInSc7	socialismus
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
anti-utopie	antitopie	k1gFnSc1	anti-utopie
Invalidní	invalidní	k2eAgMnPc1d1	invalidní
sourozenci	sourozenec	k1gMnPc1	sourozenec
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
epicky	epicky	k6eAd1	epicky
zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
Cesta	cesta	k1gFnSc1	cesta
Českem	Česko	k1gNnSc7	Česko
našich	náš	k3xOp1gMnPc2	náš
otců	otec	k1gMnPc2	otec
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblibu	obliba	k1gFnSc4	obliba
si	se	k3xPyFc3	se
získaly	získat	k5eAaPmAgFnP	získat
historické	historický	k2eAgFnPc1d1	historická
jinotajné	jinotajný	k2eAgFnPc1d1	jinotajná
povídky	povídka	k1gFnPc1	povídka
Mníšek	Mníšek	k1gInSc1	Mníšek
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šaman	šaman	k1gMnSc1	šaman
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
originální	originální	k2eAgNnSc4d1	originální
téma	téma	k1gNnSc4	téma
představuje	představovat	k5eAaImIp3nS	představovat
satirická	satirický	k2eAgFnSc1d1	satirická
sci-fi	scii	k1gFnSc1	sci-fi
z	z	k7c2	z
daleké	daleký	k2eAgFnSc2d1	daleká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
Nepovídka	Nepovídka	k1gFnSc1	Nepovídka
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
artificiální	artificiální	k2eAgFnPc1d1	artificiální
inteligentní	inteligentní	k2eAgFnPc1d1	inteligentní
bytosti	bytost	k1gFnPc1	bytost
konfrontované	konfrontovaný	k2eAgFnPc1d1	konfrontovaná
s	s	k7c7	s
primitivností	primitivnost	k1gFnSc7	primitivnost
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Nepovídka	Nepovídka	k1gFnSc1	Nepovídka
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
reálného	reálný	k2eAgInSc2d1	reálný
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celého	celý	k2eAgNnSc2d1	celé
moderního	moderní	k2eAgNnSc2d1	moderní
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
teze	teze	k1gFnPc4	teze
najdeme	najít	k5eAaPmIp1nP	najít
též	též	k9	též
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
Ministryně	ministryně	k1gFnSc2	ministryně
výživy	výživa	k1gFnSc2	výživa
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
nezřídka	nezřídka	k6eAd1	nezřídka
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
reálné	reálný	k2eAgFnPc1d1	reálná
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
nepřímo	přímo	k6eNd1	přímo
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
Bondy	bond	k1gInPc4	bond
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
družka	družka	k1gFnSc1	družka
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k8xC	ale
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
-	-	kIx~	-
nejzřetelněji	zřetelně	k6eAd3	zřetelně
v	v	k7c6	v
hravé	hravý	k2eAgFnSc6d1	hravá
próze	próza	k1gFnSc6	próza
677	[number]	k4	677
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čtenář	čtenář	k1gMnSc1	čtenář
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
mírně	mírně	k6eAd1	mírně
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Petr	Petr	k1gMnSc1	Petr
Uhl	Uhl	k1gMnSc1	Uhl
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Kosík	Kosík	k1gMnSc1	Kosík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
próza	próza	k1gFnSc1	próza
staví	stavit	k5eAaBmIp3nS	stavit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
kriticky	kriticky	k6eAd1	kriticky
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Bondy	bond	k1gInPc4	bond
píše	psát	k5eAaImIp3nS	psát
cyberpunkový	cyberpunkový	k2eAgInSc1d1	cyberpunkový
román	román	k1gInSc1	román
Cybercomics	Cybercomicsa	k1gFnPc2	Cybercomicsa
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
texty	text	k1gInPc1	text
mají	mít	k5eAaImIp3nP	mít
meditativní	meditativní	k2eAgInSc4d1	meditativní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
skepse	skepse	k1gFnPc1	skepse
<g/>
:	:	kIx,	:
Hatto	Hatto	k1gNnSc1	Hatto
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severin	Severin	k1gMnSc1	Severin
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Údolí	údolí	k1gNnSc1	údolí
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novelu	novela	k1gFnSc4	novela
Epizóda	Epizóda	k1gFnSc1	Epizóda
96	[number]	k4	96
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgMnS	napsat
slovensky	slovensky	k6eAd1	slovensky
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
čítá	čítat	k5eAaImIp3nS	čítat
Bondyho	Bondy	k1gMnSc4	Bondy
bibliografie	bibliografie	k1gFnSc2	bibliografie
asi	asi	k9	asi
dvacítku	dvacítka	k1gFnSc4	dvacítka
próz	próza	k1gFnPc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmiňované	zmiňovaný	k2eAgFnSc2d1	zmiňovaná
Ministryně	ministryně	k1gFnSc2	ministryně
výživy	výživa	k1gFnSc2	výživa
je	být	k5eAaImIp3nS	být
Bondy	bond	k1gInPc4	bond
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc7	několik
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
dalších	další	k2eAgFnPc2d1	další
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Projevil	projevit	k5eAaPmAgMnS	projevit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
překlad	překlad	k1gInSc1	překlad
Šibeničních	šibeniční	k2eAgFnPc2d1	šibeniční
písní	píseň	k1gFnPc2	píseň
od	od	k7c2	od
Christiana	Christian	k1gMnSc2	Christian
Morgensterna	Morgensterna	k1gFnSc1	Morgensterna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Bondy	bond	k1gInPc1	bond
přeložil	přeložit	k5eAaPmAgMnS	přeložit
některé	některý	k3yIgInPc4	některý
texty	text	k1gInPc4	text
filozofické	filozofický	k2eAgNnSc1d1	filozofické
<g/>
,	,	kIx,	,
v	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
dosud	dosud	k6eAd1	dosud
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přebásněná	přebásněný	k2eAgFnSc1d1	přebásněná
poezie	poezie	k1gFnSc1	poezie
Mao	Mao	k1gFnSc1	Mao
Ce-tunga	Ceunga	k1gFnSc1	Ce-tunga
<g/>
.	.	kIx.	.
</s>
<s>
Bondy	bond	k1gInPc4	bond
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejoriginálnějších	originální	k2eAgNnPc2d3	nejoriginálnější
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
také	také	k9	také
nejkontroverznějších	kontroverzní	k2eAgMnPc2d3	nejkontroverznější
českých	český	k2eAgMnPc2d1	český
filozofů	filozof	k1gMnPc2	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1	vědecký
články	článek	k1gInPc1	článek
publikoval	publikovat	k5eAaBmAgMnS	publikovat
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
knihy	kniha	k1gFnPc1	kniha
vyšly	vyjít	k5eAaPmAgFnP	vyjít
rovněž	rovněž	k9	rovněž
díky	díky	k7c3	díky
přičinění	přičinění	k1gNnSc3	přičinění
filozofa	filozof	k1gMnSc2	filozof
Milana	Milan	k1gMnSc2	Milan
Machovce	Machovec	k1gMnSc2	Machovec
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
ho	on	k3xPp3gMnSc4	on
pojilo	pojit	k5eAaImAgNnS	pojit
dlouholeté	dlouholetý	k2eAgNnSc1d1	dlouholeté
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Martin	Martin	k1gMnSc1	Martin
Machovec	Machovec	k1gMnSc1	Machovec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
později	pozdě	k6eAd2	pozdě
editorem	editor	k1gMnSc7	editor
Bondyho	Bondy	k1gMnSc2	Bondy
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
filozofickým	filozofický	k2eAgNnSc7d1	filozofické
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
Útěcha	útěcha	k1gFnSc1	útěcha
z	z	k7c2	z
ontologie	ontologie	k1gFnSc2	ontologie
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
modely	model	k1gInPc7	model
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
modelu	model	k1gInSc3	model
ateistickému	ateistický	k2eAgInSc3d1	ateistický
-	-	kIx~	-
nesubstančnímu	substanční	k2eNgNnSc3d1	nesubstanční
<g/>
.	.	kIx.	.
</s>
<s>
Předkládá	předkládat	k5eAaImIp3nS	předkládat
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
tezí	teze	k1gFnPc2	teze
o	o	k7c6	o
samovzniku	samovznik	k1gInSc6	samovznik
bytí	bytí	k1gNnSc2	bytí
v	v	k7c6	v
absenci	absence	k1gFnSc6	absence
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samovzniku	samovznik	k1gInSc2	samovznik
nemůže	moct	k5eNaImIp3nS	moct
předcházet	předcházet	k5eAaImF	předcházet
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc4	jeho
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
posibilita	posibilita	k1gFnSc1	posibilita
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
něco	něco	k3yInSc4	něco
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Bondy	bond	k1gInPc4	bond
zavedl	zavést	k5eAaPmAgInS	zavést
dále	daleko	k6eAd2	daleko
kategorii	kategorie	k1gFnSc4	kategorie
"	"	kIx"	"
<g/>
ontologického	ontologický	k2eAgNnSc2d1	ontologické
pole	pole	k1gNnSc2	pole
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
existenci	existence	k1gFnSc4	existence
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
realitě	realita	k1gFnSc6	realita
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
otázkami	otázka	k1gFnPc7	otázka
po	po	k7c6	po
smyslu	smysl	k1gInSc6	smysl
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
Bondy	bond	k1gInPc4	bond
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
ontologické	ontologický	k2eAgFnSc2d1	ontologická
reality	realita	k1gFnSc2	realita
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
touto	tento	k3xDgFnSc7	tento
kategorií	kategorie	k1gFnSc7	kategorie
se	se	k3xPyFc4	se
vysvětlování	vysvětlování	k1gNnSc1	vysvětlování
univerza	univerzum	k1gNnSc2	univerzum
vůbec	vůbec	k9	vůbec
nezjednodušuje	zjednodušovat	k5eNaImIp3nS	zjednodušovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
etickou	etický	k2eAgFnSc4d1	etická
analýzu	analýza	k1gFnSc4	analýza
smyslu	smysl	k1gInSc2	smysl
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
možným	možný	k2eAgNnSc7d1	možné
vytvořením	vytvoření	k1gNnSc7	vytvoření
artificiálních	artificiální	k2eAgFnPc2d1	artificiální
a	a	k8xC	a
na	na	k7c6	na
biotické	biotický	k2eAgFnSc6d1	biotická
existenci	existence	k1gFnSc6	existence
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
bytostí	bytost	k1gFnPc2	bytost
(	(	kIx(	(
<g/>
Juliiny	Juliin	k2eAgFnSc2d1	Juliina
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
;	;	kIx,	;
populárně	populárně	k6eAd1	populárně
pak	pak	k6eAd1	pak
próza	próza	k1gFnSc1	próza
Nepovídka	Nepovídka	k1gFnSc1	Nepovídka
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
významně	významně	k6eAd1	významně
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
dialogu	dialog	k1gInSc3	dialog
mezi	mezi	k7c7	mezi
marxismem	marxismus	k1gInSc7	marxismus
a	a	k8xC	a
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
celého	celý	k2eAgInSc2d1	celý
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
Milan	Milan	k1gMnSc1	Milan
Machovec	Machovec	k1gMnSc1	Machovec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
např.	např.	kA	např.
Milana	Milan	k1gMnSc2	Milan
Balabána	Balabán	k1gMnSc2	Balabán
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
orientální	orientální	k2eAgFnSc4d1	orientální
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
vědomí	vědomí	k1gNnSc4	vědomí
dobových	dobový	k2eAgInPc2d1	dobový
nedostatků	nedostatek	k1gInPc2	nedostatek
ve	v	k7c6	v
zpracování	zpracování	k1gNnSc6	zpracování
tématu	téma	k1gNnSc2	téma
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
ho	on	k3xPp3gInSc4	on
přivedly	přivést	k5eAaPmAgFnP	přivést
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
dějin	dějiny	k1gFnPc2	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třinácti	třináct	k4xCc6	třináct
samizdatových	samizdatový	k2eAgInPc6d1	samizdatový
svazcích	svazek	k1gInPc6	svazek
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
vyšly	vyjít	k5eAaPmAgInP	vyjít
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
šesti	šest	k4xCc6	šest
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Zůstaly	zůstat	k5eAaPmAgFnP	zůstat
ovšem	ovšem	k9	ovšem
torzem	torzo	k1gNnSc7	torzo
a	a	k8xC	a
zejména	zejména	k9	zejména
oddíl	oddíl	k1gInSc1	oddíl
o	o	k7c6	o
antice	antika	k1gFnSc6	antika
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
z	z	k7c2	z
vědeckých	vědecký	k2eAgInPc2d1	vědecký
kruhů	kruh	k1gInPc2	kruh
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
ceněny	ceněn	k2eAgInPc1d1	ceněn
jsou	být	k5eAaImIp3nP	být
oddíly	oddíl	k1gInPc1	oddíl
o	o	k7c4	o
orientální	orientální	k2eAgFnSc4d1	orientální
filosofii	filosofie	k1gFnSc4	filosofie
indické	indický	k2eAgFnPc1d1	indická
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnPc1d1	čínská
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnPc1d1	arabská
a	a	k8xC	a
židovské	židovský	k2eAgFnPc1d1	židovská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
připravil	připravit	k5eAaPmAgInS	připravit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Čarnogurskou	Čarnogurský	k2eAgFnSc7d1	Čarnogurská
překlad	překlad	k1gInSc4	překlad
Tao	Tao	k1gFnSc4	Tao
te	te	k?	te
ťingu	ťing	k1gInSc2	ťing
<g/>
.	.	kIx.	.
</s>
<s>
Bondy	bond	k1gInPc4	bond
nadále	nadále	k6eAd1	nadále
píše	psát	k5eAaImIp3nS	psát
filozofické	filozofický	k2eAgInPc4d1	filozofický
eseje	esej	k1gInPc4	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
rozpracovává	rozpracovávat	k5eAaImIp3nS	rozpracovávat
myšlenky	myšlenka	k1gFnPc4	myšlenka
nesubstanční	substanční	k2eNgFnSc2d1	nesubstanční
ontologie	ontologie	k1gFnSc2	ontologie
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
syntézu	syntéza	k1gFnSc4	syntéza
"	"	kIx"	"
<g/>
západních	západní	k2eAgFnPc2d1	západní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
buddhistických	buddhistický	k2eAgInPc2d1	buddhistický
a	a	k8xC	a
taoistických	taoistický	k2eAgInPc2d1	taoistický
přístupů	přístup	k1gInPc2	přístup
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
struktuře	struktura	k1gFnSc3	struktura
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc4	souhrn
těchto	tento	k3xDgFnPc2	tento
dosud	dosud	k6eAd1	dosud
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
poznámek	poznámka	k1gFnPc2	poznámka
a	a	k8xC	a
esejů	esej	k1gInPc2	esej
nese	nést	k5eAaImIp3nS	nést
provizorní	provizorní	k2eAgInSc1d1	provizorní
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
románové	románový	k2eAgFnSc2d1	románová
triloogie	triloogie	k1gFnSc2	triloogie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
i	i	k9	i
mezi	mezi	k7c4	mezi
díla	dílo	k1gNnPc4	dílo
sci-fi	scii	k1gFnSc2	sci-fi
literatury	literatura	k1gFnSc2	literatura
-	-	kIx~	-
Invalidní	invalidní	k2eAgMnPc1d1	invalidní
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
Afghanistan	Afghanistan	k1gInSc1	Afghanistan
a	a	k8xC	a
Bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
normalizace	normalizace	k1gFnSc2	normalizace
i	i	k8xC	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
také	také	k9	také
politologickým	politologický	k2eAgFnPc3d1	politologická
analýzám	analýza	k1gFnPc3	analýza
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
vždy	vždy	k6eAd1	vždy
zůstával	zůstávat	k5eAaImAgInS	zůstávat
věrný	věrný	k2eAgInSc1d1	věrný
svému	svůj	k3xOyFgInSc3	svůj
nedogmaticky	dogmaticky	k6eNd1	dogmaticky
marxistickému	marxistický	k2eAgInSc3d1	marxistický
světonázoru	světonázor	k1gInSc3	světonázor
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
moudrostí	moudrost	k1gFnSc7	moudrost
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
jen	jen	k9	jen
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oporou	opora	k1gFnSc7	opora
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
buzeroval	buzerovat	k5eAaImAgInS	buzerovat
nějakými	nějaký	k3yIgFnPc7	nějaký
pitomými	pitomý	k2eAgFnPc7d1	pitomá
radami	rada	k1gFnPc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
moudrostí	moudrost	k1gFnSc7	moudrost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nad	nad	k7c4	nad
slunce	slunce	k1gNnSc4	slunce
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
řve	řvát	k5eAaImIp3nS	řvát
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
vteřiny	vteřina	k1gFnSc2	vteřina
času	čas	k1gInSc2	čas
a	a	k8xC	a
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
smetí	smetí	k1gNnSc2	smetí
pod	pod	k7c7	pod
nohama	noha	k1gFnPc7	noha
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bouřit	bouřit	k5eAaImF	bouřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
bouřit	bouřit	k5eAaImF	bouřit
se	se	k3xPyFc4	se
a	a	k8xC	a
bouřit	bouřit	k5eAaImF	bouřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jedině	jedině	k6eAd1	jedině
tento	tento	k3xDgInSc4	tento
postoj	postoj	k1gInSc4	postoj
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
oprávněný	oprávněný	k2eAgInSc1d1	oprávněný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sranda	sranda	k1gFnSc1	sranda
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
to	ten	k3xDgNnSc1	ten
muselo	muset	k5eAaImAgNnS	muset
trvat	trvat	k5eAaImF	trvat
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
naší	náš	k3xOp1gFnSc2	náš
zázračné	zázračný	k2eAgFnSc2d1	zázračná
anticko-křesťansko-árijské	antickořesťansko-árijský	k2eAgFnSc2d1	anticko-křesťansko-árijský
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
nakousl	nakousnout	k5eAaPmAgMnS	nakousnout
aspoň	aspoň	k9	aspoň
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
zmilitký	zmilitký	k2eAgMnSc1d1	zmilitký
předseda	předseda	k1gMnSc1	předseda
Mao	Mao	k1gMnSc1	Mao
to	ten	k3xDgNnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
říct	říct	k5eAaPmF	říct
rovnou	rovnou	k6eAd1	rovnou
na	na	k7c4	na
plnou	plný	k2eAgFnSc4d1	plná
hubu	huba	k1gFnSc4	huba
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sklepní	sklepní	k2eAgFnSc1d1	sklepní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
s.	s.	k?	s.
160	[number]	k4	160
<g/>
)	)	kIx)	)
Pražský	pražský	k2eAgInSc1d1	pražský
život	život	k1gInSc1	život
-	-	kIx~	-
Poezie	poezie	k1gFnSc1	poezie
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
dívka	dívka	k1gFnSc1	dívka
-	-	kIx~	-
Poezie	poezie	k1gFnSc1	poezie
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1952	[number]	k4	1952
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
1	[number]	k4	1
(	(	kIx(	(
<g/>
epické	epický	k2eAgFnSc2d1	epická
básně	báseň	k1gFnSc2	báseň
z	z	k7c2	z
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
2	[number]	k4	2
(	(	kIx(	(
<g/>
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
3	[number]	k4	3
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
básnické	básnický	k2eAgFnPc1d1	básnická
sbírky	sbírka	k1gFnPc1	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
Básně	báseň	k1gFnSc2	báseň
1988	[number]	k4	1988
aneb	aneb	k?	aneb
Čas	čas	k1gInSc1	čas
spíše	spíše	k9	spíše
chmurný	chmurný	k2eAgMnSc1d1	chmurný
+	+	kIx~	+
Odplouvání	odplouvání	k1gNnSc1	odplouvání
-	-	kIx~	-
Inverze	inverze	k1gFnSc1	inverze
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
4	[number]	k4	4
(	(	kIx(	(
<g/>
Naivita	naivita	k1gFnSc1	naivita
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
Básnické	básnický	k2eAgInPc1d1	básnický
<g />
.	.	kIx.	.
</s>
<s>
dílo	dílo	k1gNnSc1	dílo
sv.	sv.	kA	sv.
5	[number]	k4	5
(	(	kIx(	(
<g/>
Kádrový	kádrový	k2eAgInSc1d1	kádrový
dotazník	dotazník	k1gInSc1	dotazník
+	+	kIx~	+
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
6	[number]	k4	6
(	(	kIx(	(
<g/>
Deník	deník	k1gInSc1	deník
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
Egona	Egon	k1gMnSc4	Egon
Bondyho	Bondy	k1gMnSc4	Bondy
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
Dvě	dva	k4xCgFnPc1	dva
<g />
.	.	kIx.	.
</s>
<s>
léta	léto	k1gNnPc1	léto
(	(	kIx(	(
<g/>
Básně	báseň	k1gFnSc2	báseň
1989	[number]	k4	1989
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
Inverze	inverze	k1gFnSc2	inverze
1991	[number]	k4	1991
Básnické	básnický	k2eAgNnSc1d1	básnické
dílo	dílo	k1gNnSc1	dílo
sv.	sv.	kA	sv.
7	[number]	k4	7
(	(	kIx(	(
<g/>
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
8	[number]	k4	8
(	(	kIx(	(
<g/>
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Příšerné	příšerný	k2eAgInPc4d1	příšerný
příběhy	příběh	k1gInPc4	příběh
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
Básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
sv.	sv.	kA	sv.
9	[number]	k4	9
(	(	kIx(	(
<g/>
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
Ples	pleso	k1gNnPc2	pleso
upírů	upír	k1gMnPc2	upír
-	-	kIx~	-
Obrys-Kontur	Obrys-Kontura	k1gFnPc2	Obrys-Kontura
/	/	kIx~	/
Poezie	poezie	k1gFnSc1	poezie
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
1995	[number]	k4	1995
<g />
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
eposu	epos	k1gInSc2	epos
-	-	kIx~	-
F.	F.	kA	F.
R.	R.	kA	R.
And	Anda	k1gFnPc2	Anda
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
-	-	kIx~	-
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Sbírečka	sbírečka	k1gFnSc1	sbírečka
-	-	kIx~	-
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Deník	deník	k1gInSc1	deník
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
<g />
.	.	kIx.	.
</s>
<s>
Egona	Egon	k1gMnSc4	Egon
Bondyho	Bondy	k1gMnSc4	Bondy
-	-	kIx~	-
PT	PT	kA	PT
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
zbytečné	zbytečný	k2eAgInPc1d1	zbytečný
verše	verš	k1gInPc1	verš
-	-	kIx~	-
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Invalidní	invalidní	k2eAgMnPc1d1	invalidní
sourozenci	sourozenec	k1gMnPc1	sourozenec
-	-	kIx~	-
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
1981	[number]	k4	1981
(	(	kIx(	(
<g/>
Archa	archa	k1gFnSc1	archa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
psáno	psán	k2eAgNnSc1d1	psáno
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Sklepní	sklepní	k2eAgFnSc2d1	sklepní
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
1988	[number]	k4	1988
(	(	kIx(	(
<g/>
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
-	-	kIx~	-
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
souboru	soubor	k1gInSc2	soubor
Orwelliáda	Orwelliáda	k1gFnSc1	Orwelliáda
<g/>
)	)	kIx)	)
Delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc4	bond
-	-	kIx~	-
Šaman	šaman	k1gMnSc1	šaman
<g/>
,	,	kIx,	,
Mníšek	mníšek	k1gMnSc1	mníšek
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
věk	věk	k1gInSc1	věk
-	-	kIx~	-
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
a	a	k8xC	a
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Gottschalk	Gottschalk	k1gInSc1	Gottschalk
<g/>
,	,	kIx,	,
Kratés	Kratés	k1gInSc1	Kratés
<g/>
,	,	kIx,	,
Jao	Jao	k1gFnSc1	Jao
Li	li	k9	li
<g/>
,	,	kIx,	,
Doslov	doslov	k1gInSc4	doslov
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
psáno	psán	k2eAgNnSc1d1	psáno
<g />
.	.	kIx.	.
</s>
<s>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
Českem	Česko	k1gNnSc7	Česko
našich	náš	k3xOp1gMnPc2	náš
otců	otec	k1gMnPc2	otec
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Nepovídka	Nepovídka	k1gFnSc1	Nepovídka
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Leden	leden	k1gInSc1	leden
na	na	k7c6	na
vsi	ves	k1gFnSc6	ves
-	-	kIx~	-
Torst	Torst	k1gFnSc1	Torst
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Hatto	Hatto	k1gNnSc4	Hatto
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1995	[number]	k4	1995
Příšerné	příšerný	k2eAgInPc4d1	příšerný
příběhy	příběh	k1gInPc4	příběh
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
-	-	kIx~	-
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
Severin	Severin	k1gMnSc1	Severin
-	-	kIx~	-
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1996	[number]	k4	1996
Cybercomics	Cybercomicsa	k1gFnPc2	Cybercomicsa
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1997	[number]	k4	1997
Agónie	agónie	k1gFnPc1	agónie
-	-	kIx~	-
Epizóda	Epizóda	k1gFnSc1	Epizóda
96	[number]	k4	96
-	-	kIx~	-
PT	PT	kA	PT
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1997	[number]	k4	1997
Týden	týden	k1gInSc1	týden
v	v	k7c6	v
tichém	tichý	k2eAgNnSc6d1	tiché
městě	město	k1gNnSc6	město
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
PT	PT	kA	PT
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1998	[number]	k4	1998
Údolí	údolí	k1gNnSc2	údolí
-	-	kIx~	-
PT	PT	kA	PT
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1998	[number]	k4	1998
Bezejmenná	bezejmenný	k2eAgNnPc4d1	bezejmenné
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
vydání	vydání	k1gNnPc4	vydání
<g/>
,	,	kIx,	,
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Brno-Praha	Brno-Praha	k1gFnSc1	Brno-Praha
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
677	[number]	k4	677
-	-	kIx~	-
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Máša	Máša	k1gFnSc1	Máša
a	a	k8xC	a
Běta	Běta	k1gFnSc1	Běta
-	-	kIx~	-
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Šaman	šaman	k1gMnSc1	šaman
-	-	kIx~	-
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
samostatné	samostatný	k2eAgNnSc1d1	samostatné
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Ramazovi	Ramazův	k2eAgMnPc1d1	Ramazův
-	-	kIx~	-
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Ministryně	ministryně	k1gFnSc2	ministryně
výživy	výživa	k1gFnSc2	výživa
-	-	kIx~	-
Arkýř	arkýř	k1gInSc1	arkýř
<g/>
,	,	kIx,	,
Mnchov	Mnchov	k1gInSc1	Mnchov
1989	[number]	k4	1989
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Dilia	Dilia	k1gFnSc1	Dilia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Prvních	první	k4xOgMnPc2	první
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
-	-	kIx~	-
Maťa	Maťa	k1gFnSc1	Maťa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
-	-	kIx~	-
autobiografie	autobiografie	k1gFnSc1	autobiografie
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
-	-	kIx~	-
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
-	-	kIx~	-
soubor	soubor	k1gInSc4	soubor
her	hra	k1gFnPc2	hra
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1968-1970	[number]	k4	1968-1970
Otázky	otázka	k1gFnSc2	otázka
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útěcha	útěcha	k1gFnSc1	útěcha
z	z	k7c2	z
ontologie	ontologie	k1gFnSc2	ontologie
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Filozofické	filozofický	k2eAgInPc1d1	filozofický
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Útěcha	útěcha	k1gFnSc1	útěcha
z	z	k7c2	z
ontologie	ontologie	k1gFnSc2	ontologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Filosofické	filosofický	k2eAgInPc1d1	filosofický
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
2	[number]	k4	2
(	(	kIx(	(
<g/>
Juliiny	Juliin	k2eAgFnPc1d1	Juliina
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
Doslov	doslov	k1gInSc1	doslov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Filozofické	filozofický	k2eAgInPc1d1	filozofický
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Filozofické	filozofický	k2eAgInPc1d1	filozofický
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Indická	indický	k2eAgFnSc1d1	indická
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
Poznámky	poznámka	k1gFnPc4	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
2	[number]	k4	2
(	(	kIx(	(
<g/>
Čínská	čínský	k2eAgFnSc1d1	čínská
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
3	[number]	k4	3
(	(	kIx(	(
<g/>
Antická	antický	k2eAgFnSc1d1	antická
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
4	[number]	k4	4
(	(	kIx(	(
<g/>
Filozofie	filozofie	k1gFnSc1	filozofie
sklonku	sklonek	k1gInSc2	sklonek
antiky	antika	k1gFnSc2	antika
a	a	k8xC	a
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
středověku	středověk	k1gInSc2	středověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
5	[number]	k4	5
(	(	kIx(	(
<g/>
Středověká	středověký	k2eAgFnSc1d1	středověká
islámská	islámský	k2eAgFnSc1d1	islámská
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
,	,	kIx,	,
reformace	reformace	k1gFnSc1	reformace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
sv.	sv.	kA	sv.
6	[number]	k4	6
(	(	kIx(	(
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
filozofie	filozofie	k1gFnSc1	filozofie
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Neuspořádaná	uspořádaný	k2eNgFnSc1d1	neuspořádaná
samomluva	samomluva	k1gFnSc1	samomluva
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
O	o	k7c6	o
globalizaci	globalizace	k1gFnSc6	globalizace
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2005	[number]	k4	2005
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
<g/>
:	:	kIx,	:
Budete	být	k5eAaImBp2nP	být
jako	jako	k9	jako
bohové	bůh	k1gMnPc1	bůh
-	-	kIx~	-
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Lao-c	Lao	k1gFnSc1	Lao-c
<g/>
'	'	kIx"	'
<g/>
:	:	kIx,	:
O	o	k7c6	o
ceste	cesit	k5eAaPmRp2nP	cesit
Tao	Tao	k1gMnSc4	Tao
a	a	k8xC	a
jej	on	k3xPp3gMnSc4	on
tvorivej	tvorivat	k5eAaImRp2nS	tvorivat
energii	energie	k1gFnSc4	energie
te	te	k?	te
<g/>
.	.	kIx.	.
-	-	kIx~	-
Hevi	Hevi	k1gNnSc1	Hevi
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1993	[number]	k4	1993
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
Marínou	Marína	k1gFnSc7	Marína
Čarnogurskou	Čarnogurský	k2eAgFnSc7d1	Čarnogurská
<g/>
)	)	kIx)	)
Christian	Christian	k1gMnSc1	Christian
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
<g/>
:	:	kIx,	:
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
Vida	Vida	k?	Vida
Vida	Vida	k?	Vida
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
podíl	podíl	k1gInSc1	podíl
Bondyho	Bondy	k1gMnSc2	Bondy
<g/>
:	:	kIx,	:
13	[number]	k4	13
překladů	překlad	k1gInPc2	překlad
ze	z	k7c2	z
179	[number]	k4	179
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Lawrence	Lawrenec	k1gInSc2	Lawrenec
Ferlinghetti	Ferlinghetť	k1gFnSc2	Ferlinghetť
<g/>
:	:	kIx,	:
I	i	k9	i
Am	Am	k1gMnSc1	Am
Waiting	Waiting	k1gInSc1	Waiting
<g/>
.	.	kIx.	.
-	-	kIx~	-
Silberman	Silberman	k1gMnSc1	Silberman
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
<g/>
:	:	kIx,	:
Šibeniční	šibeniční	k2eAgFnPc1d1	šibeniční
písně	píseň	k1gFnPc1	píseň
/	/	kIx~	/
Galgenlieder	Galgenlieder	k1gInSc1	Galgenlieder
-	-	kIx~	-
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Lao-c	Lao	k1gFnSc1	Lao-c
<g/>
:	:	kIx,	:
Tao	Tao	k1gMnSc1	Tao
Te	Te	k1gMnSc1	Te
ťing	ťing	k1gMnSc1	ťing
<g/>
:	:	kIx,	:
kánon	kánon	k1gInSc1	kánon
o	o	k7c4	o
Tao	Tao	k1gFnSc4	Tao
a	a	k8xC	a
Te	Te	k1gFnSc4	Te
<g/>
.	.	kIx.	.
</s>
<s>
Podľa	Podľa	k1gMnSc1	Podľa
Wang	Wang	k1gMnSc1	Wang
Piho	piha	k1gFnSc5	piha
kópie	kópie	k1gFnSc2	kópie
originálu	originál	k1gMnSc3	originál
preložili	preložit	k5eAaPmAgMnP	preložit
Marina	Marina	k1gFnSc1	Marina
Čarnogurská	Čarnogurský	k2eAgFnSc1d1	Čarnogurská
a	a	k8xC	a
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc7	bond
<g/>
;	;	kIx,	;
doslov	doslovit	k5eAaPmRp2nS	doslovit
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc1	bond
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Agentúra	Agentúra	k1gFnSc1	Agentúra
Fischer	Fischer	k1gMnSc1	Fischer
Pezinok	Pezinok	k1gInSc1	Pezinok
Formát	formát	k1gInSc1	formát
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
