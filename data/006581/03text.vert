<s>
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
pojmenování	pojmenování	k1gNnSc1	pojmenování
přírodních	přírodní	k2eAgFnPc2d1	přírodní
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
též	též	k9	též
tokoferoly	tokoferola	k1gFnSc2	tokoferola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
derivátů	derivát	k1gInPc2	derivát
6	[number]	k4	6
<g/>
-hydroxychromanu	ydroxychroman	k1gMnSc6	-hydroxychroman
nebo	nebo	k8xC	nebo
tokolu	tokol	k1gInSc3	tokol
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vitamíny	vitamín	k1gInPc4	vitamín
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
a	a	k8xC	a
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
důležitý	důležitý	k2eAgInSc1d1	důležitý
antioxidant	antioxidant	k1gInSc1	antioxidant
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
buněčné	buněčný	k2eAgFnPc4d1	buněčná
membrány	membrána	k1gFnPc4	membrána
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
volnými	volný	k2eAgInPc7d1	volný
radikály	radikál	k1gInPc7	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc4	čtyři
tokoferolové	tokoferolový	k2eAgInPc4d1	tokoferolový
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
tokotrienolové	tokotrienolový	k2eAgInPc4d1	tokotrienolový
izomery	izomer	k1gInPc4	izomer
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
biologickou	biologický	k2eAgFnSc4d1	biologická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
chromanovým	chromanův	k2eAgInSc7d1	chromanův
kruhem	kruh	k1gInSc7	kruh
a	a	k8xC	a
hydrofobním	hydrofobní	k2eAgInSc7d1	hydrofobní
fytylovým	fytylový	k2eAgInSc7d1	fytylový
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
řetězcem	řetězec	k1gInSc7	řetězec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
nerozpustnost	nerozpustnost	k1gFnSc4	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
dobrou	dobrý	k2eAgFnSc4d1	dobrá
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Tokoferoly	Tokoferol	k1gInPc1	Tokoferol
proto	proto	k8xC	proto
snadno	snadno	k6eAd1	snadno
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc7	jejich
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chromanovém	chromanový	k2eAgInSc6d1	chromanový
kruhu	kruh	k1gInSc6	kruh
je	být	k5eAaImIp3nS	být
připojena	připojit	k5eAaPmNgFnS	připojit
jedna	jeden	k4xCgFnSc1	jeden
hydroxylová	hydroxylový	k2eAgFnSc1d1	hydroxylová
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dárcem	dárce	k1gMnSc7	dárce
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
antioxidační	antioxidační	k2eAgInSc4d1	antioxidační
účinek	účinek	k1gInSc4	účinek
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
methylové	methylový	k2eAgFnSc2d1	methylová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
různý	různý	k2eAgInSc1d1	různý
počet	počet	k1gInSc1	počet
určuje	určovat	k5eAaImIp3nS	určovat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
tokoferol	tokoferol	k1gInSc4	tokoferol
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
D-α	D-α	k1gMnSc1	D-α
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
největší	veliký	k2eAgFnSc4d3	veliký
antioxidační	antioxidační	k2eAgFnSc4d1	antioxidační
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
z	z	k7c2	z
pšeničných	pšeničný	k2eAgInPc2d1	pšeničný
klíčků	klíček	k1gInPc2	klíček
<g/>
,	,	kIx,	,
másle	máslo	k1gNnSc6	máslo
<g/>
,	,	kIx,	,
mléce	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
burských	burský	k2eAgInPc6d1	burský
oříšcích	oříšek	k1gInPc6	oříšek
<g/>
,	,	kIx,	,
sóji	sója	k1gFnSc6	sója
<g/>
,	,	kIx,	,
salátu	salát	k1gInSc6	salát
a	a	k8xC	a
v	v	k7c6	v
mase	masa	k1gFnSc6	masa
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
vitamínu	vitamín	k1gInSc2	vitamín
E	E	kA	E
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
při	při	k7c6	při
zvýšeném	zvýšený	k2eAgInSc6d1	zvýšený
příjmu	příjem	k1gInSc6	příjem
nenasycených	nasycený	k2eNgInPc2d1	nenasycený
tuků	tuk	k1gInPc2	tuk
nebo	nebo	k8xC	nebo
zvýšeném	zvýšený	k2eAgNnSc6d1	zvýšené
vystavení	vystavení	k1gNnSc6	vystavení
se	se	k3xPyFc4	se
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
kyslíkové	kyslíkový	k2eAgInPc1d1	kyslíkový
stany	stan	k1gInPc1	stan
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	porucha	k1gFnPc1	porucha
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
tuků	tuk	k1gInPc2	tuk
ze	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
příznakům	příznak	k1gInPc3	příznak
nedostatku	nedostatek	k1gInSc2	nedostatek
tokoferolu	tokoferola	k1gFnSc4	tokoferola
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vitamín	vitamín	k1gInSc1	vitamín
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
jen	jen	k9	jen
společně	společně	k6eAd1	společně
s	s	k7c7	s
tuky	tuk	k1gInPc7	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
Údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
Recommended	Recommended	k1gMnSc1	Recommended
Dietary	Dietara	k1gFnSc2	Dietara
Allowances	Allowances	k1gMnSc1	Allowances
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
th	th	k?	th
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Food	Food	k1gInSc1	Food
and	and	k?	and
Nutrition	Nutrition	k1gInSc1	Nutrition
Board	Board	k1gInSc1	Board
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Research	Research	k1gMnSc1	Research
Council	Council	k1gMnSc1	Council
–	–	k?	–
National	National	k1gMnSc1	National
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
se	se	k3xPyFc4	se
ničí	ničit	k5eAaImIp3nS	ničit
během	během	k7c2	během
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zmrazení	zmrazení	k1gNnSc2	zmrazení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
zásoby	zásoba	k1gFnSc2	zásoba
v	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
antioxidant	antioxidant	k1gInSc4	antioxidant
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
chrání	chránit	k5eAaImIp3nS	chránit
buňky	buňka	k1gFnPc4	buňka
před	před	k7c7	před
oxidačním	oxidační	k2eAgInSc7d1	oxidační
stresem	stres	k1gInSc7	stres
a	a	k8xC	a
účinky	účinek	k1gInPc1	účinek
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
prokazatelně	prokazatelně	k6eAd1	prokazatelně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k8xC	jako
prevence	prevence	k1gFnSc1	prevence
proti	proti	k7c3	proti
nádorovému	nádorový	k2eAgNnSc3d1	nádorové
bujení	bujení	k1gNnSc3	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
poznatky	poznatek	k1gInPc1	poznatek
vědy	věda	k1gFnSc2	věda
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
opak	opak	k1gInSc4	opak
-	-	kIx~	-
Vitamín	vitamín	k1gInSc4	vitamín
E	E	kA	E
např.	např.	kA	např.
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
karcinomu	karcinom	k1gInSc2	karcinom
prostaty	prostata	k1gFnSc2	prostata
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
také	také	k9	také
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
hojení	hojení	k1gNnSc4	hojení
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
plodnost	plodnost	k1gFnSc1	plodnost
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
činnost	činnost	k1gFnSc4	činnost
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
součástí	součást	k1gFnSc7	součást
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
linii	linie	k1gFnSc6	linie
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
peroxidaci	peroxidace	k1gFnSc3	peroxidace
polyenových	polyenův	k2eAgFnPc2d1	polyenův
kyselin	kyselina	k1gFnPc2	kyselina
biologických	biologický	k2eAgFnPc2d1	biologická
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
peroxidaci	peroxidace	k1gFnSc3	peroxidace
nenasycené	nasycený	k2eNgFnSc2d1	nenasycená
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
radikálem	radikál	k1gInSc7	radikál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napadne	napadnout	k5eAaPmIp3nS	napadnout
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
radikálových	radikálový	k2eAgFnPc2d1	radikálová
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
řetězové	řetězový	k2eAgFnPc1d1	řetězová
-	-	kIx~	-
bez	bez	k7c2	bez
obranných	obranný	k2eAgInPc2d1	obranný
mechanismů	mechanismus	k1gInPc2	mechanismus
by	by	kYmCp3nS	by
brzy	brzy	k6eAd1	brzy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
narušení	narušení	k1gNnSc3	narušení
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
narušení	narušení	k1gNnSc3	narušení
nebo	nebo	k8xC	nebo
ztrátě	ztráta	k1gFnSc3	ztráta
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nevratnému	vratný	k2eNgNnSc3d1	nevratné
poškození	poškození	k1gNnSc3	poškození
buňky	buňka	k1gFnSc2	buňka
nebo	nebo	k8xC	nebo
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
zničení	zničení	k1gNnSc3	zničení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
membránách	membrána	k1gFnPc6	membrána
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vystavené	vystavený	k2eAgNnSc4d1	vystavené
působení	působení	k1gNnSc4	působení
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
v	v	k7c6	v
dýchacím	dýchací	k2eAgInSc6d1	dýchací
systému	systém	k1gInSc6	systém
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
membránách	membrána	k1gFnPc6	membrána
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tokoferoly	tokoferola	k1gFnPc1	tokoferola
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
darovat	darovat	k5eAaPmF	darovat
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
atom	atom	k1gInSc4	atom
<g/>
,	,	kIx,	,
přenesením	přenesení	k1gNnSc7	přenesení
vodíku	vodík	k1gInSc2	vodík
z	z	k7c2	z
fenolové	fenolový	k2eAgFnSc2d1	fenolová
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
volný	volný	k2eAgInSc4d1	volný
peroxiradikál	peroxiradikál	k1gInSc4	peroxiradikál
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
radikálové	radikál	k1gMnPc1	radikál
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
α	α	k?	α
+	+	kIx~	+
ROO	ROO	kA	ROO
<g/>
°	°	k?	°
→	→	k?	→
α	α	k?	α
<g/>
°	°	k?	°
+	+	kIx~	+
ROOH	ROOH	kA	ROOH
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
fenoxy-radikál	fenoxyadikál	k1gInSc1	fenoxy-radikál
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
vitamínem	vitamín	k1gInSc7	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
redukovaným	redukovaný	k2eAgInSc7d1	redukovaný
glutathionem	glutathion	k1gInSc7	glutathion
nebo	nebo	k8xC	nebo
koenzymem	koenzym	k1gInSc7	koenzym
Q.	Q.	kA	Q.
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
volným	volný	k2eAgMnSc7d1	volný
peroxilovým	peroxilův	k2eAgMnSc7d1	peroxilův
radikálem	radikál	k1gMnSc7	radikál
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nevratné	vratný	k2eNgFnSc3d1	nevratná
oxidaci	oxidace	k1gFnSc3	oxidace
tokoferolu	tokoferol	k1gInSc2	tokoferol
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
žlučí	žlučit	k5eAaImIp3nS	žlučit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgNnSc2	svůj
antioxidačního	antioxidační	k2eAgNnSc2d1	antioxidační
působení	působení	k1gNnSc2	působení
tokoferoly	tokoferola	k1gFnSc2	tokoferola
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
cholesterol	cholesterol	k1gInSc1	cholesterol
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
membránové	membránový	k2eAgFnPc4d1	membránová
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
propustnost	propustnost	k1gFnSc1	propustnost
membrány	membrána	k1gFnSc2	membrána
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
molekuly	molekula	k1gFnPc4	molekula
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
inhibitor	inhibitor	k1gInSc1	inhibitor
proteinkinasy	proteinkinasa	k1gFnSc2	proteinkinasa
C.	C.	kA	C.
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
E	E	kA	E
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
nebo	nebo	k8xC	nebo
distribuce	distribuce	k1gFnSc2	distribuce
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
chronická	chronický	k2eAgFnSc1d1	chronická
steatorea	steatorea	k1gFnSc1	steatorea
<g/>
,	,	kIx,	,
abetalipoproteinemie	abetalipoproteinemie	k1gFnSc1	abetalipoproteinemie
nebo	nebo	k8xC	nebo
cystická	cystický	k2eAgFnSc1d1	cystická
fibróza	fibróza	k1gFnSc1	fibróza
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
po	po	k7c6	po
resekci	resekce	k1gFnSc6	resekce
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
projevit	projevit	k5eAaPmF	projevit
jako	jako	k9	jako
neurologické	neurologický	k2eAgFnPc4d1	neurologická
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
nebo	nebo	k8xC	nebo
poruchou	porucha	k1gFnSc7	porucha
funkce	funkce	k1gFnSc2	funkce
gonád	gonáda	k1gFnPc2	gonáda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
neplodnosti	neplodnost	k1gFnSc3	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
může	moct	k5eAaImIp3nS	moct
nedostatek	nedostatek	k1gInSc4	nedostatek
vyvolat	vyvolat	k5eAaPmF	vyvolat
anémii	anémie	k1gFnSc4	anémie
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
zkrácením	zkrácení	k1gNnSc7	zkrácení
životnosti	životnost	k1gFnSc2	životnost
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
vitamíny	vitamín	k1gInPc7	vitamín
rozpustnými	rozpustný	k2eAgInPc7d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
je	být	k5eAaImIp3nS	být
tokoferol	tokoferol	k1gInSc1	tokoferol
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
užívání	užívání	k1gNnSc1	užívání
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
vstřebávání	vstřebávání	k1gNnSc1	vstřebávání
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
důsledky	důsledek	k1gInPc7	důsledek
<g/>
.	.	kIx.	.
</s>
