<s>
Jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
chrání	chránit	k5eAaImIp3nS	chránit
buňky	buňka	k1gFnPc4	buňka
před	před	k7c7	před
oxidačním	oxidační	k2eAgInSc7d1	oxidační
stresem	stres	k1gInSc7	stres
a	a	k8xC	a
účinky	účinek	k1gInPc1	účinek
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
stárnutí	stárnutí	k1gNnSc4	stárnutí
a	a	k8xC	a
prokazatelně	prokazatelně	k6eAd1	prokazatelně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k8xC	jako
prevence	prevence	k1gFnSc1	prevence
proti	proti	k7c3	proti
nádorovému	nádorový	k2eAgNnSc3d1	nádorové
bujení	bujení	k1gNnSc3	bujení
<g/>
.	.	kIx.	.
</s>
