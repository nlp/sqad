<s>
S	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Penrosem	Penros	k1gMnSc7	Penros
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc1	čas
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
má	mít	k5eAaImIp3nS	mít
počátek	počátek	k1gInSc4	počátek
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
a	a	k8xC	a
konec	konec	k1gInSc4	konec
v	v	k7c6	v
černých	černý	k2eAgFnPc6d1	černá
dírách	díra	k1gFnPc6	díra
<g/>
.	.	kIx.	.
</s>
