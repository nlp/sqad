<s>
Stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
též	též	k9	též
stíhačka	stíhačka	k1gFnSc1	stíhačka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc4d1	vojenský
letoun	letoun	k1gInSc4	letoun
určený	určený	k2eAgInSc4d1	určený
primárně	primárně	k6eAd1	primárně
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
nepřátelských	přátelský	k2eNgNnPc2d1	nepřátelské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bombardovacího	bombardovací	k2eAgInSc2d1	bombardovací
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
cílů	cíl	k1gInPc2	cíl
pozemních	pozemní	k2eAgFnPc2d1	pozemní
<g/>
.	.	kIx.	.
</s>
