<p>
<s>
Stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
též	též	k9	též
stíhačka	stíhačka	k1gFnSc1	stíhačka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc4d1	vojenský
letoun	letoun	k1gInSc4	letoun
určený	určený	k2eAgInSc4d1	určený
primárně	primárně	k6eAd1	primárně
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
nepřátelských	přátelský	k2eNgNnPc2d1	nepřátelské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bombardovacího	bombardovací	k2eAgInSc2d1	bombardovací
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
cílů	cíl	k1gInPc2	cíl
pozemních	pozemní	k2eAgFnPc2d1	pozemní
<g/>
.	.	kIx.	.
</s>
<s>
Stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
bývají	bývat	k5eAaImIp3nP	bývat
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
a	a	k8xC	a
obratnější	obratní	k2eAgFnSc1d2	obratní
než	než	k8xS	než
ostatní	ostatní	k2eAgNnPc1d1	ostatní
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
lepší	dobrý	k2eAgInSc4d2	lepší
poměr	poměr	k1gInSc4	poměr
tahu	tah	k1gInSc2	tah
<g/>
/	/	kIx~	/
<g/>
výkonu	výkon	k1gInSc2	výkon
k	k	k7c3	k
vzletové	vzletový	k2eAgFnSc3d1	vzletová
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
používání	používání	k1gNnSc4	používání
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
a	a	k8xC	a
balónů	balón	k1gInPc2	balón
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
pozemní	pozemní	k2eAgInPc4d1	pozemní
cíle	cíl	k1gInPc4	cíl
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
až	až	k9	až
1918	[number]	k4	1918
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
dvouplošníky	dvouplošník	k1gInPc1	dvouplošník
s	s	k7c7	s
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
kostrou	kostra	k1gFnSc7	kostra
potaženou	potažený	k2eAgFnSc4d1	potažená
plátnem	plátno	k1gNnSc7	plátno
<g/>
,	,	kIx,	,
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
lehkými	lehký	k2eAgInPc7d1	lehký
kulomety	kulomet	k1gInPc7	kulomet
a	a	k8xC	a
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
160	[number]	k4	160
až	až	k9	až
220	[number]	k4	220
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
už	už	k9	už
byl	být	k5eAaImAgInS	být
typickým	typický	k2eAgMnSc7d1	typický
představitelem	představitel	k1gMnSc7	představitel
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
vojenských	vojenský	k2eAgNnPc2d1	vojenské
letadel	letadlo	k1gNnPc2	letadlo
kovový	kovový	k2eAgInSc1d1	kovový
jednoplošník	jednoplošník	k1gInSc1	jednoplošník
s	s	k7c7	s
kanónovou	kanónový	k2eAgFnSc7d1	kanónová
výzbrojí	výzbroj	k1gFnSc7	výzbroj
nesenou	nesený	k2eAgFnSc7d1	nesená
v	v	k7c6	v
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
500	[number]	k4	500
až	až	k9	až
700	[number]	k4	700
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
proudové	proudový	k2eAgInPc1d1	proudový
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
jsou	být	k5eAaImIp3nP	být
poháněny	pohánět	k5eAaImNgInP	pohánět
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
dvouproudovými	dvouproudový	k2eAgInPc7d1	dvouproudový
motory	motor	k1gInPc7	motor
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
primární	primární	k2eAgFnSc1d1	primární
výzbroj	výzbroj	k1gFnSc1	výzbroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
řízené	řízený	k2eAgInPc4d1	řízený
střely	střel	k1gInPc4	střel
<g/>
,	,	kIx,	,
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
jsou	být	k5eAaImIp3nP	být
naváděny	naváděn	k2eAgInPc1d1	naváděn
palubním	palubní	k2eAgInSc7d1	palubní
radarem	radar	k1gInSc7	radar
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
maximální	maximální	k2eAgFnPc4d1	maximální
rychlosti	rychlost	k1gFnPc4	rychlost
okolo	okolo	k7c2	okolo
2	[number]	k4	2
Ma	Ma	k1gFnPc2	Ma
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přes	přes	k7c4	přes
2000	[number]	k4	2000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc1	první
stíhací	stíhací	k2eAgNnPc1d1	stíhací
letadla	letadlo	k1gNnPc1	letadlo
vznikala	vznikat	k5eAaImAgNnP	vznikat
za	za	k7c4	za
první	první	k4xOgInPc4	první
světové	světový	k2eAgInPc4d1	světový
války	válek	k1gInPc4	válek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
piloti	pilot	k1gMnPc1	pilot
lehkých	lehký	k2eAgInPc2d1	lehký
strojů	stroj	k1gInPc2	stroj
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
a	a	k8xC	a
navádění	navádění	k1gNnSc4	navádění
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
útočit	útočit	k5eAaImF	útočit
všemožnými	všemožný	k2eAgInPc7d1	všemožný
prostředky	prostředek	k1gInPc7	prostředek
na	na	k7c4	na
letouny	letoun	k1gInPc4	letoun
nepřítele	nepřítel	k1gMnSc4	nepřítel
s	s	k7c7	s
obdobným	obdobný	k2eAgNnSc7d1	obdobné
určením	určení	k1gNnSc7	určení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používaly	používat	k5eAaImAgFnP	používat
se	se	k3xPyFc4	se
pušky	puška	k1gFnSc2	puška
<g/>
,	,	kIx,	,
pistole	pistol	k1gFnSc2	pistol
a	a	k8xC	a
brokovnice	brokovnice	k1gFnSc2	brokovnice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
i	i	k9	i
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
kulometu	kulomet	k1gInSc2	kulomet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pokusy	pokus	k1gInPc1	pokus
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc1d1	vysoká
hmotnosti	hmotnost	k1gFnPc1	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
rostl	růst	k5eAaImAgInS	růst
význam	význam	k1gInSc4	význam
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
zabránit	zabránit	k5eAaPmF	zabránit
té	ten	k3xDgFnSc2	ten
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příručce	příručka	k1gFnSc6	příručka
RFC	RFC	kA	RFC
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Flying	Flying	k1gInSc1	Flying
Corps	corps	k1gInSc4	corps
<g/>
)	)	kIx)	)
z	z	k7c2	z
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neočekává	očekávat	k5eNaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pilot	pilot	k1gMnSc1	pilot
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
splnit	splnit	k5eAaPmF	splnit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vyrušen	vyrušit	k5eAaPmNgInS	vyrušit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
o	o	k7c4	o
výhody	výhoda	k1gFnPc4	výhoda
bojuje	bojovat	k5eAaImIp3nS	bojovat
a	a	k8xC	a
význam	význam	k1gInSc4	význam
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
průzkumu	průzkum	k1gInSc2	průzkum
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
budou	být	k5eAaImBp3nP	být
snažit	snažit	k5eAaImF	snažit
zamezit	zamezit	k5eAaPmF	zamezit
té	ten	k3xDgFnSc2	ten
druhé	druhý	k4xOgFnSc6	druhý
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
provádění	provádění	k1gNnSc6	provádění
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
leteckém	letecký	k2eAgNnSc6d1	letecké
boji	boj	k1gInSc6	boj
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
let	let	k1gInSc1	let
posádky	posádka	k1gFnSc2	posádka
Harvey-Kelly	Harvey-Kella	k1gFnSc2	Harvey-Kella
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
letouny	letoun	k1gInPc1	letoun
britské	britský	k2eAgInPc1d1	britský
2	[number]	k4	2
<g/>
.	.	kIx.	.
perutě	peruť	k1gFnPc1	peruť
donutily	donutit	k5eAaPmAgFnP	donutit
přistát	přistát	k5eAaPmF	přistát
německý	německý	k2eAgInSc4d1	německý
dvousedadlový	dvousedadlový	k2eAgInSc4d1	dvousedadlový
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
posádka	posádka	k1gFnSc1	posádka
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Harvey	Harve	k1gMnPc4	Harve
a	a	k8xC	a
Kelly	Kell	k1gMnPc4	Kell
stroj	stroj	k1gInSc4	stroj
zapálili	zapálit	k5eAaPmAgMnP	zapálit
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
<g/>
Kulomet	kulomet	k1gInSc1	kulomet
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
úspěšně	úspěšně	k6eAd1	úspěšně
použit	použít	k5eAaPmNgInS	použít
francouzským	francouzský	k2eAgMnSc7d1	francouzský
pilotem	pilot	k1gMnSc7	pilot
Josephem	Joseph	k1gInSc7	Joseph
Frantzem	Frantz	k1gMnSc7	Frantz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
kulometem	kulomet	k1gInSc7	kulomet
Hotchkiss	Hotchkissa	k1gFnPc2	Hotchkissa
upevněným	upevněný	k2eAgFnPc3d1	upevněná
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
nepřátelský	přátelský	k2eNgMnSc1d1	nepřátelský
Aviatik	aviatik	k1gMnSc1	aviatik
typ	typa	k1gFnPc2	typa
B.	B.	kA	B.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
postupoval	postupovat	k5eAaImAgInS	postupovat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
základní	základní	k2eAgNnSc1d1	základní
členění	členění	k1gNnSc1	členění
na	na	k7c4	na
letouny	letoun	k1gInPc4	letoun
stíhací	stíhací	k2eAgFnSc2d1	stíhací
a	a	k8xC	a
pracovní	pracovní	k2eAgFnSc2d1	pracovní
<g/>
/	/	kIx~	/
<g/>
průzkumné	průzkumný	k2eAgFnSc2d1	průzkumná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
celým	celý	k2eAgInSc7d1	celý
strojem	stroj	k1gInSc7	stroj
s	s	k7c7	s
kulometem	kulomet	k1gInSc7	kulomet
střílejícím	střílející	k2eAgInSc7d1	střílející
skrz	skrz	k7c4	skrz
vrtuli	vrtule	k1gFnSc4	vrtule
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
opancéřovanou	opancéřovaný	k2eAgFnSc7d1	opancéřovaná
vrtulí	vrtule	k1gFnSc7	vrtule
u	u	k7c2	u
typů	typ	k1gInPc2	typ
Morane-Saulnier	Morane-Saulnira	k1gFnPc2	Morane-Saulnira
L	L	kA	L
a	a	k8xC	a
N	N	kA	N
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
synchronizátor	synchronizátor	k1gInSc1	synchronizátor
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
použitelnou	použitelný	k2eAgFnSc4d1	použitelná
verzi	verze	k1gFnSc4	verze
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Anthony	Anthona	k1gFnSc2	Anthona
Fokker	Fokkra	k1gFnPc2	Fokkra
a	a	k8xC	a
nasadil	nasadit	k5eAaPmAgMnS	nasadit
ji	on	k3xPp3gFnSc4	on
u	u	k7c2	u
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
Eindecker	Eindeckra	k1gFnPc2	Eindeckra
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
obdobné	obdobný	k2eAgInPc1d1	obdobný
systémy	systém	k1gInPc1	systém
použily	použít	k5eAaPmAgInP	použít
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Stíhačky	stíhačka	k1gFnPc1	stíhačka
se	se	k3xPyFc4	se
od	od	k7c2	od
vynálezu	vynález	k1gInSc2	vynález
synchronizace	synchronizace	k1gFnSc2	synchronizace
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
podobně	podobně	k6eAd1	podobně
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
lišily	lišit	k5eAaImAgFnP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
výkonem	výkon	k1gInSc7	výkon
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
křídel	křídlo	k1gNnPc2	křídlo
(	(	kIx(	(
<g/>
dvouplošníky	dvouplošník	k1gInPc1	dvouplošník
<g/>
,	,	kIx,	,
trojplošníky	trojplošník	k1gInPc1	trojplošník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
koncepce	koncepce	k1gFnSc1	koncepce
již	již	k6eAd1	již
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
položena	položit	k5eAaPmNgFnS	položit
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
později	pozdě	k6eAd2	pozdě
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
stíhačky	stíhačka	k1gFnPc1	stíhačka
byly	být	k5eAaImAgFnP	být
konvenčního	konvenční	k2eAgNnSc2d1	konvenční
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
motor	motor	k1gInSc4	motor
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
vepředu	vepředu	k6eAd1	vepředu
<g/>
,	,	kIx,	,
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
neboli	neboli	k8xC	neboli
výškovky	výškovka	k1gFnPc1	výškovka
úplně	úplně	k6eAd1	úplně
vzadu	vzadu	k6eAd1	vzadu
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
svislé	svislý	k2eAgFnPc1d1	svislá
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
ovládající	ovládající	k2eAgInSc1d1	ovládající
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
směr	směr	k1gInSc1	směr
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
slavné	slavný	k2eAgInPc1d1	slavný
stroje	stroj	k1gInPc1	stroj
jako	jako	k8xC	jako
Albatros	albatros	k1gMnSc1	albatros
D.	D.	kA	D.
<g/>
III	III	kA	III
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
Fokkery	Fokker	k1gInPc1	Fokker
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
typy	typ	k1gInPc4	typ
jako	jako	k8xC	jako
Nieuport	Nieuport	k1gInSc1	Nieuport
17	[number]	k4	17
<g/>
,	,	kIx,	,
SPAD	spad	k1gInSc1	spad
S.	S.	kA	S.
<g/>
XIII	XIII	kA	XIII
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
E	E	kA	E
<g/>
.5	.5	k4	.5
či	či	k8xC	či
Sopwith	Sopwith	k1gMnSc1	Sopwith
Camel	Camel	k1gMnSc1	Camel
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
také	také	k9	také
stíhačky	stíhačka	k1gFnPc1	stíhačka
začaly	začít	k5eAaPmAgFnP	začít
organizovat	organizovat	k5eAaBmF	organizovat
do	do	k7c2	do
sekcí	sekce	k1gFnPc2	sekce
<g/>
/	/	kIx~	/
<g/>
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
uskupení	uskupení	k1gNnPc2	uskupení
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
až	až	k9	až
letek	letek	k1gInSc4	letek
a	a	k8xC	a
squadron	squadron	k1gInSc4	squadron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
vývoj	vývoj	k1gInSc1	vývoj
stíhaček	stíhačka	k1gFnPc2	stíhačka
spíše	spíše	k9	spíše
stagnoval	stagnovat	k5eAaImAgMnS	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
konzervativního	konzervativní	k2eAgInSc2d1	konzervativní
způsobu	způsob	k1gInSc2	způsob
myšlení	myšlení	k1gNnSc1	myšlení
dominovaly	dominovat	k5eAaImAgInP	dominovat
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
stále	stále	k6eAd1	stále
dvouplošníky	dvouplošník	k1gInPc1	dvouplošník
se	s	k7c7	s
slabou	slabý	k2eAgFnSc7d1	slabá
výzbrojí	výzbroj	k1gFnSc7	výzbroj
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
kulomety	kulomet	k1gInPc4	kulomet
malé	malý	k2eAgFnSc2d1	malá
ráže	ráže	k1gFnSc2	ráže
<g/>
)	)	kIx)	)
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
malou	malý	k2eAgFnSc7d1	malá
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
okolo	okolo	k7c2	okolo
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
400	[number]	k4	400
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
doletem	dolet	k1gInSc7	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
kladem	klad	k1gInSc7	klad
byla	být	k5eAaImAgFnS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
obratnost	obratnost	k1gFnSc1	obratnost
a	a	k8xC	a
stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc4	zlom
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
polovina	polovina	k1gFnSc1	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
hlavních	hlavní	k2eAgFnPc2d1	hlavní
mocností	mocnost	k1gFnPc2	mocnost
začaly	začít	k5eAaPmAgInP	začít
zavádět	zavádět	k5eAaImF	zavádět
samonosné	samonosný	k2eAgInPc1d1	samonosný
dolnoplošníky	dolnoplošník	k1gInPc1	dolnoplošník
vybavené	vybavený	k2eAgInPc1d1	vybavený
mnohdy	mnohdy	k6eAd1	mnohdy
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
kabinou	kabina	k1gFnSc7	kabina
a	a	k8xC	a
disponující	disponující	k2eAgMnSc1d1	disponující
často	často	k6eAd1	často
i	i	k8xC	i
kanónovou	kanónový	k2eAgFnSc7d1	kanónová
výzbrojí	výzbroj	k1gFnSc7	výzbroj
a	a	k8xC	a
zatažitelným	zatažitelný	k2eAgInSc7d1	zatažitelný
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
například	například	k6eAd1	například
od	od	k7c2	od
Polikarpov	Polikarpov	k1gInSc4	Polikarpov
I-	I-	k1gFnSc4	I-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
Morane-Saulnier	Morane-Saulnier	k1gInSc1	Morane-Saulnier
MS.	MS.	k1gFnSc1	MS.
<g/>
406	[number]	k4	406
<g/>
,	,	kIx,	,
Supermarine	Supermarin	k1gInSc5	Supermarin
Spitfire	Spitfir	k1gInSc5	Spitfir
<g/>
,	,	kIx,	,
Curtiss	Curtiss	k1gInSc1	Curtiss
P-36	P-36	k1gMnSc1	P-36
Hawk	Hawk	k1gMnSc1	Hawk
či	či	k8xC	či
Messerschmitt	Messerschmitt	k1gMnSc1	Messerschmitt
Bf	Bf	k1gFnSc2	Bf
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
jen	jen	k9	jen
pár	pár	k4xCyI	pár
dílčích	dílčí	k2eAgInPc2d1	dílčí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
významnějších	významný	k2eAgFnPc2d2	významnější
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgInPc1d2	silnější
motory	motor	k1gInPc1	motor
umožnily	umožnit	k5eAaPmAgInP	umožnit
existenci	existence	k1gFnSc4	existence
rychlých	rychlý	k2eAgInPc2d1	rychlý
jednoplošníků	jednoplošník	k1gInPc2	jednoplošník
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
konvenční	konvenční	k2eAgFnPc1d1	konvenční
konstrukce	konstrukce	k1gFnPc1	konstrukce
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
výjimek	výjimka	k1gFnPc2	výjimka
jako	jako	k8xS	jako
P-38	P-38	k1gFnSc2	P-38
Lightning	Lightning	k1gInSc1	Lightning
či	či	k8xC	či
Twin	Twin	k1gMnSc1	Twin
Mustang	mustang	k1gMnSc1	mustang
z	z	k7c2	z
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
stíhačky	stíhačka	k1gFnSc2	stíhačka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vybojování	vybojování	k1gNnSc1	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
do	do	k7c2	do
čehož	což	k3yQnSc2	což
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sestřelování	sestřelování	k1gNnSc1	sestřelování
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
bombardérů	bombardér	k1gInPc2	bombardér
a	a	k8xC	a
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
stíhaček	stíhačka	k1gFnPc2	stíhačka
</s>
</p>
<p>
<s>
zajištění	zajištění	k1gNnSc1	zajištění
neomezených	omezený	k2eNgFnPc2d1	neomezená
aktivit	aktivita	k1gFnPc2	aktivita
vlastních	vlastní	k2eAgInPc2d1	vlastní
bombardérů	bombardér	k1gInPc2	bombardér
a	a	k8xC	a
letounů	letoun	k1gInPc2	letoun
v	v	k7c6	v
nepřátelském	přátelský	k2eNgInSc6d1	nepřátelský
ale	ale	k8xC	ale
i	i	k9	i
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
prostoruVzdušná	prostoruVzdušný	k2eAgFnSc1d1	prostoruVzdušný
nadvláda	nadvláda	k1gFnSc1	nadvláda
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
cílů	cíl	k1gInPc2	cíl
válečné	válečný	k2eAgFnSc2d1	válečná
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
převahu	převaha	k1gFnSc4	převaha
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
nepřátelské	přátelský	k2eNgFnPc4d1	nepřátelská
pozemní	pozemní	k2eAgFnPc4d1	pozemní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nepříteli	nepřítel	k1gMnSc3	nepřítel
znemožnit	znemožnit	k5eAaPmF	znemožnit
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
vedení	vedení	k1gNnSc4	vedení
i	i	k8xC	i
pozemní	pozemní	k2eAgFnSc2d1	pozemní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInSc1d1	hlavní
důraz	důraz	k1gInSc1	důraz
přenesl	přenést	k5eAaPmAgInS	přenést
na	na	k7c4	na
přepadové	přepadový	k2eAgFnPc4d1	přepadová
a	a	k8xC	a
záchytné	záchytný	k2eAgFnPc4d1	záchytná
stíhačky	stíhačka	k1gFnPc4	stíhačka
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgFnPc4d1	vybavená
řízenými	řízený	k2eAgFnPc7d1	řízená
protivzdušnými	protivzdušný	k2eAgFnPc7d1	protivzdušná
střelami	střela	k1gFnPc7	střela
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnSc2	některý
(	(	kIx(	(
<g/>
AIM-	AIM-	k1gFnSc2	AIM-
<g/>
26	[number]	k4	26
Falcon	Falcona	k1gFnPc2	Falcona
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
nukleární	nukleární	k2eAgFnSc7d1	nukleární
hlavicí	hlavice	k1gFnSc7	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
i	i	k8xC	i
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
dokonce	dokonce	k9	dokonce
v	v	k7c4	v
nadšení	nadšení	k1gNnSc4	nadšení
z	z	k7c2	z
protivzdušných	protivzdušný	k2eAgFnPc2d1	protivzdušná
řízených	řízený	k2eAgFnPc2d1	řízená
střel	střela	k1gFnPc2	střela
neodolaly	odolat	k5eNaPmAgInP	odolat
pokušení	pokušení	k1gNnSc4	pokušení
přepnout	přepnout	k5eAaPmF	přepnout
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
all-missile-armanent	allissilermanent	k1gInSc1	all-missile-armanent
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
letoun	letoun	k1gInSc1	letoun
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
jen	jen	k6eAd1	jen
raketami	raketa	k1gFnPc7	raketa
a	a	k8xC	a
žádnou	žádný	k3yNgFnSc7	žádný
organickou	organický	k2eAgFnSc7d1	organická
hlavňovou	hlavňový	k2eAgFnSc7d1	hlavňová
výzbrojí	výzbroj	k1gFnSc7	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
protivník	protivník	k1gMnSc1	protivník
bude	být	k5eAaImBp3nS	být
zničen	zničit	k5eAaPmNgMnS	zničit
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
raketami	raketa	k1gFnPc7	raketa
a	a	k8xC	a
ke	k	k7c3	k
klasickému	klasický	k2eAgInSc3d1	klasický
dogfightu	dogfight	k1gInSc3	dogfight
–	–	k?	–
známému	známý	k1gMnSc3	známý
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
konfliktů	konflikt	k1gInPc2	konflikt
–	–	k?	–
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
Vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
válce	válka	k1gFnSc6	válka
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nepraktické	praktický	k2eNgNnSc1d1	nepraktické
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
hlavňová	hlavňový	k2eAgFnSc1d1	hlavňová
výzbroj	výzbroj	k1gFnSc1	výzbroj
dodatečně	dodatečně	k6eAd1	dodatečně
přidána	přidat	k5eAaPmNgFnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Souboje	souboj	k1gInPc1	souboj
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
použít	použít	k5eAaPmF	použít
kanón	kanón	k1gInSc4	kanón
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
udržet	udržet	k5eAaPmF	udržet
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
letoun	letoun	k1gInSc1	letoun
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
a	a	k8xC	a
navádět	navádět	k5eAaImF	navádět
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pasivně	pasivně	k6eAd1	pasivně
naváděnou	naváděný	k2eAgFnSc4d1	naváděná
raketu	raketa	k1gFnSc4	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
naváděné	naváděný	k2eAgInPc4d1	naváděný
střely	střel	k1gInPc4	střel
zase	zase	k9	zase
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vymanévrovat	vymanévrovat	k5eAaPmF	vymanévrovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
raketa	raketa	k1gFnSc1	raketa
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
mateřský	mateřský	k2eAgInSc4d1	mateřský
letoun	letoun	k1gInSc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
hodnota	hodnota	k1gFnSc1	hodnota
použité	použitý	k2eAgFnSc2d1	použitá
rakety	raketa	k1gFnSc2	raketa
byla	být	k5eAaImAgFnS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
hodnota	hodnota	k1gFnSc1	hodnota
zničeného	zničený	k2eAgInSc2d1	zničený
letounu	letoun	k1gInSc2	letoun
protivníka	protivník	k1gMnSc4	protivník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
ubíral	ubírat	k5eAaImAgInS	ubírat
spirálovitě	spirálovitě	k6eAd1	spirálovitě
<g/>
,	,	kIx,	,
reakcemi	reakce	k1gFnPc7	reakce
na	na	k7c4	na
letouny	letoun	k1gInPc4	letoun
protivníka	protivník	k1gMnSc2	protivník
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vyvinout	vyvinout	k5eAaPmF	vyvinout
letoun	letoun	k1gInSc4	letoun
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
letoun	letoun	k1gInSc1	letoun
převyšující	převyšující	k2eAgInSc1d1	převyšující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
23	[number]	k4	23
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
F-4	F-4	k1gFnPc7	F-4
Phantom	Phantom	k1gInSc1	Phantom
II	II	kA	II
</s>
</p>
<p>
<s>
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
F-15	F-15	k1gMnSc1	F-15
Eagle	Eagle	k1gNnSc1	Eagle
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c6	na
MiG-	MiG-	k1gFnSc6	MiG-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
F-16	F-16	k1gFnSc1	F-16
jako	jako	k8xS	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c6	na
MiG-	MiG-	k1gFnSc6	MiG-
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
29	[number]	k4	29
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
F-16	F-16	k1gFnSc6	F-16
a	a	k8xC	a
Su-	Su-	k1gFnSc6	Su-
<g/>
27	[number]	k4	27
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
F-15	F-15	k1gFnSc4	F-15
</s>
</p>
<p>
<s>
F-22A	F-22A	k4	F-22A
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
Su-	Su-	k1gFnSc6	Su-
<g/>
27	[number]	k4	27
a	a	k8xC	a
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
29	[number]	k4	29
</s>
</p>
<p>
<s>
PAK	pak	k8xC	pak
FA	fa	k1gNnSc1	fa
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
F-22	F-22	k1gFnSc4	F-22
</s>
</p>
<p>
<s>
===	===	k?	===
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
–	–	k?	–
dnešek	dnešek	k1gInSc4	dnešek
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jen	jen	k9	jen
několik	několik	k4yIc1	několik
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
dokončením	dokončení	k1gNnSc7	dokončení
vývoje	vývoj	k1gInSc2	vývoj
letadel	letadlo	k1gNnPc2	letadlo
započatých	započatý	k2eAgNnPc2d1	započaté
v	v	k7c6	v
době	doba	k1gFnSc6	doba
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
–	–	k?	–
F-	F-	k1gMnSc1	F-
<g/>
15	[number]	k4	15
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
F-	F-	k1gFnSc1	F-
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
Su-	Su-	k1gFnSc1	Su-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
Dassault	Dassault	k1gInSc4	Dassault
Rafale	Rafala	k1gFnSc3	Rafala
a	a	k8xC	a
Eurofighter	Eurofighter	k1gInSc4	Eurofighter
Typhoon	Typhoona	k1gFnPc2	Typhoona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAS-39	JAS-39	k4	JAS-39
Gripen	Gripen	k2eAgMnSc1d1	Gripen
a	a	k8xC	a
modernizace	modernizace	k1gFnPc1	modernizace
jako	jako	k8xS	jako
F-15E	F-15E	k1gFnPc1	F-15E
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
letouny	letoun	k1gInPc1	letoun
4,5	[number]	k4	4,5
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
Eurofighter	Eurofighter	k1gInSc4	Eurofighter
Typhoon	Typhoona	k1gFnPc2	Typhoona
a	a	k8xC	a
Rafale	Rafala	k1gFnSc3	Rafala
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
4,5	[number]	k4	4,5
<g/>
.	.	kIx.	.
generaci	generace	k1gFnSc6	generace
ale	ale	k8xC	ale
po	po	k7c6	po
modernizaci	modernizace	k1gFnSc6	modernizace
budou	být	k5eAaImBp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
novými	nový	k2eAgInPc7d1	nový
letouny	letoun	k1gInPc7	letoun
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
vývojem	vývoj	k1gInSc7	vývoj
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
po	po	k7c6	po
konci	konec	k1gInSc6	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
jsou	být	k5eAaImIp3nP	být
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gMnSc1	A-
<g/>
18	[number]	k4	18
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
Super	super	k2eAgInSc1d1	super
Hornet	Hornet	k1gInSc1	Hornet
a	a	k8xC	a
F-35	F-35	k1gFnSc1	F-35
Lightning	Lightning	k1gInSc1	Lightning
II	II	kA	II
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
kompromisem	kompromis	k1gInSc7	kompromis
mezi	mezi	k7c7	mezi
výkonem	výkon	k1gInSc7	výkon
a	a	k8xC	a
cenou	cena	k1gFnSc7	cena
<g/>
/	/	kIx~	/
<g/>
provozními	provozní	k2eAgInPc7d1	provozní
náklady	náklad	k1gInPc7	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letouny	letoun	k1gInPc1	letoun
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnPc1	generace
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
prostředky	prostředek	k1gInPc1	prostředek
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
zachytitelnosti	zachytitelnost	k1gFnSc2	zachytitelnost
radarem	radar	k1gInSc7	radar
(	(	kIx(	(
<g/>
stealth	stealth	k1gInSc1	stealth
technologie	technologie	k1gFnSc2	technologie
–	–	k?	–
v	v	k7c6	v
případě	případ	k1gInSc6	případ
amerických	americký	k2eAgInPc2d1	americký
strojů	stroj	k1gInPc2	stroj
facetováním	facetování	k1gNnSc7	facetování
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ruských	ruský	k2eAgFnPc2d1	ruská
plazmovou	plazmový	k2eAgFnSc7d1	plazmová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
)	)	kIx)	)
a	a	k8xC	a
sníženou	snížený	k2eAgFnSc7d1	snížená
viditelností	viditelnost	k1gFnSc7	viditelnost
v	v	k7c6	v
optickém	optický	k2eAgNnSc6d1	optické
spektru	spektrum	k1gNnSc6	spektrum
(	(	kIx(	(
<g/>
potah	potah	k1gInSc1	potah
měnící	měnící	k2eAgFnSc4d1	měnící
barvu	barva	k1gFnSc4	barva
podle	podle	k7c2	podle
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc1d1	jediný
používané	používaný	k2eAgInPc1d1	používaný
stroje	stroj	k1gInPc1	stroj
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnPc1	generace
armádou	armáda	k1gFnSc7	armáda
jsou	být	k5eAaImIp3nP	být
F-22	F-22	k1gFnPc1	F-22
a	a	k8xC	a
F-	F-	k1gFnPc1	F-
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Generace	generace	k1gFnSc1	generace
proudových	proudový	k2eAgFnPc2d1	proudová
stíhaček	stíhačka	k1gFnPc2	stíhačka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
pístové	pístový	k2eAgInPc1d1	pístový
motory	motor	k1gInPc1	motor
vytlačeny	vytlačen	k2eAgInPc1d1	vytlačen
motory	motor	k1gInPc1	motor
proudovými	proudový	k2eAgFnPc7d1	proudová
a	a	k8xC	a
řízené	řízený	k2eAgFnPc1d1	řízená
střely	střela	k1gFnPc1	střela
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
hlavňovou	hlavňový	k2eAgFnSc4d1	hlavňová
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
proudové	proudový	k2eAgInPc1d1	proudový
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnSc1d1	orientační
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
jeho	on	k3xPp3gMnSc2	on
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
doby	doba	k1gFnSc2	doba
ap.	ap.	kA	ap.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
generace	generace	k1gFnPc1	generace
jsou	být	k5eAaImIp3nP	být
podzvukové	podzvukový	k2eAgFnPc1d1	podzvuková
stíhačky	stíhačka	k1gFnPc1	stíhačka
ze	z	k7c2	z
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
až	až	k9	až
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
stíhaček	stíhačka	k1gFnPc2	stíhačka
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
let	léto	k1gNnPc2	léto
padesátých	padesátý	k4xOgNnPc2	padesátý
až	až	k9	až
raných	raný	k2eAgFnPc2d1	raná
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
z	z	k7c2	z
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
</s>
</p>
<p>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
generace	generace	k1gFnSc1	generace
z	z	k7c2	z
let	léto	k1gNnPc2	léto
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
až	až	k9	až
polovina	polovina	k1gFnSc1	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
</s>
</p>
<p>
<s>
4,5	[number]	k4	4,5
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
</s>
</p>
<p>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
stíhaček	stíhačka	k1gFnPc2	stíhačka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
stíhačky	stíhačka	k1gFnPc1	stíhačka
začaly	začít	k5eAaPmAgFnP	začít
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Doprovodné	doprovodný	k2eAgMnPc4d1	doprovodný
===	===	k?	===
</s>
</p>
<p>
<s>
Doprovodné	doprovodný	k2eAgFnPc1d1	doprovodná
stíhačky	stíhačka	k1gFnPc1	stíhačka
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
escort	escort	k1gInSc1	escort
fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
primárním	primární	k2eAgInSc7d1	primární
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
letět	letět	k5eAaImF	letět
společně	společně	k6eAd1	společně
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
bombardovacími	bombardovací	k2eAgInPc7d1	bombardovací
letouny	letoun	k1gInPc7	letoun
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
P-51	P-51	k1gMnSc1	P-51
Mustang	mustang	k1gMnSc1	mustang
<g/>
,	,	kIx,	,
Lockheed	Lockheed	k1gMnSc1	Lockheed
P-38	P-38	k1gFnSc2	P-38
Lightning	Lightning	k1gInSc1	Lightning
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
stroje	stroj	k1gInPc1	stroj
upravovány	upravovat	k5eAaImNgInP	upravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
neseny	nést	k5eAaImNgInP	nést
mateřským	mateřský	k2eAgInSc7d1	mateřský
letounem	letoun	k1gInSc7	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měly	mít	k5eAaImAgFnP	mít
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
např.	např.	kA	např.
sovětské	sovětský	k2eAgFnSc2d1	sovětská
sestavy	sestava	k1gFnSc2	sestava
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Zvěno	zvěna	k1gFnSc5	zvěna
(	(	kIx(	(
<g/>
roj	roj	k1gInSc4	roj
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
speciální	speciální	k2eAgInSc1d1	speciální
americký	americký	k2eAgInSc1d1	americký
poválečný	poválečný	k2eAgInSc1d1	poválečný
letounek	letounek	k1gInSc1	letounek
Goblin	Goblin	k2eAgInSc1d1	Goblin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jako	jako	k9	jako
doprovodné	doprovodný	k2eAgMnPc4d1	doprovodný
stíhače	stíhač	k1gMnPc4	stíhač
používají	používat	k5eAaImIp3nP	používat
víceúčelové	víceúčelový	k2eAgInPc1d1	víceúčelový
stroje	stroj	k1gInPc1	stroj
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhací	stíhací	k2eAgInPc1d1	stíhací
bombardéry	bombardér	k1gInPc1	bombardér
===	===	k?	===
</s>
</p>
<p>
<s>
Stíhací	stíhací	k2eAgInPc1d1	stíhací
bombardéry	bombardér	k1gInPc1	bombardér
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
fighter-bomber	fighteromber	k1gMnSc1	fighter-bomber
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
stíhačky	stíhačka	k1gFnPc1	stíhačka
schopné	schopný	k2eAgFnPc1d1	schopná
jak	jak	k8xC	jak
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
pozemní	pozemní	k2eAgInPc4d1	pozemní
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
boje	boj	k1gInPc1	boj
s	s	k7c7	s
nepřátelskými	přátelský	k2eNgMnPc7d1	nepřátelský
stíhači	stíhač	k1gMnPc7	stíhač
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Hawker	Hawker	k1gInSc4	Hawker
Typhoon	Typhoona	k1gFnPc2	Typhoona
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgInSc4d1	moderní
Boeing	boeing	k1gInSc4	boeing
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gMnSc1	A-
<g/>
18	[number]	k4	18
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
Super	super	k2eAgInSc1d1	super
Hornet	Hornet	k1gInSc1	Hornet
či	či	k8xC	či
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc2	Su-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jako	jako	k9	jako
stíhací	stíhací	k2eAgInPc1d1	stíhací
bombardéry	bombardér	k1gInPc1	bombardér
používají	používat	k5eAaImIp3nP	používat
víceúčelové	víceúčelový	k2eAgInPc4d1	víceúčelový
stroje	stroj	k1gInPc4	stroj
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úderné	úderný	k2eAgMnPc4d1	úderný
===	===	k?	===
</s>
</p>
<p>
<s>
Úderný	úderný	k2eAgInSc1d1	úderný
stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
strike	strike	k1gFnSc1	strike
fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
velkou	velký	k2eAgFnSc7d1	velká
rychlostí	rychlost	k1gFnSc7	rychlost
vletí	vletět	k5eAaPmIp3nS	vletět
do	do	k7c2	do
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
sestřelí	sestřelit	k5eAaPmIp3nS	sestřelit
významné	významný	k2eAgInPc4d1	významný
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
vyčistí	vyčistit	k5eAaPmIp3nS	vyčistit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInPc4d1	vlastní
bombardéry	bombardér	k1gInPc4	bombardér
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
či	či	k8xC	či
Lockheed	Lockheed	k1gMnSc1	Lockheed
Martin	Martin	k1gMnSc1	Martin
F-35	F-35	k1gFnSc2	F-35
Lightning	Lightning	k1gInSc1	Lightning
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
se	se	k3xPyFc4	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
překrývá	překrývat	k5eAaImIp3nS	překrývat
jak	jak	k6eAd1	jak
se	s	k7c7	s
stíhacími	stíhací	k2eAgMnPc7d1	stíhací
bombardéry	bombardér	k1gMnPc7	bombardér
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
útočnými	útočný	k2eAgInPc7d1	útočný
letouny	letoun	k1gInPc7	letoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přepadové	přepadový	k2eAgMnPc4d1	přepadový
/	/	kIx~	/
Záchytné	záchytný	k2eAgMnPc4d1	záchytný
===	===	k?	===
</s>
</p>
<p>
<s>
Přepadové	přepadový	k2eAgNnSc1d1	přepadové
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
záchytné	záchytný	k2eAgInPc1d1	záchytný
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Interceptor	Interceptor	k1gInSc1	Interceptor
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
letět	letět	k5eAaImF	letět
vstříc	vstříc	k6eAd1	vstříc
nepřátelským	přátelský	k2eNgMnPc3d1	nepřátelský
bombardérům	bombardér	k1gMnPc3	bombardér
a	a	k8xC	a
sestřelit	sestřelit	k5eAaPmF	sestřelit
je	být	k5eAaImIp3nS	být
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
svrhnou	svrhnout	k5eAaPmIp3nP	svrhnout
svůj	svůj	k3xOyFgInSc4	svůj
náklad	náklad	k1gInSc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Záchytné	záchytný	k2eAgFnPc1d1	záchytná
stíhačky	stíhačka	k1gFnPc1	stíhačka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
taktice	taktika	k1gFnSc3	taktika
spojenců	spojenec	k1gMnPc2	spojenec
využívajících	využívající	k2eAgMnPc2d1	využívající
kobercové	kobercový	k2eAgInPc4d1	kobercový
bombardování	bombardování	k1gNnSc1	bombardování
velkými	velký	k2eAgInPc7d1	velký
bombardovacími	bombardovací	k2eAgInPc7d1	bombardovací
svazy	svaz	k1gInPc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
proto	proto	k8xC	proto
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
záchytné	záchytný	k2eAgMnPc4d1	záchytný
stíhače	stíhač	k1gMnPc4	stíhač
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Messerschmitt	Messerschmitt	k1gInSc4	Messerschmitt
Me	Me	k1gFnSc2	Me
163	[number]	k4	163
nebo	nebo	k8xC	nebo
Bachem	Bach	k1gInSc7	Bach
Ba	ba	k9	ba
349	[number]	k4	349
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
jaderného	jaderný	k2eAgInSc2d1	jaderný
arzenálu	arzenál	k1gInSc2	arzenál
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
důležitější	důležitý	k2eAgMnSc1d2	důležitější
zastavit	zastavit	k5eAaPmF	zastavit
útočící	útočící	k2eAgInPc4d1	útočící
bombardéry	bombardér	k1gInPc4	bombardér
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
vyvíjeny	vyvíjen	k2eAgMnPc4d1	vyvíjen
záchytné	záchytný	k2eAgMnPc4d1	záchytný
stíhače	stíhač	k1gMnPc4	stíhač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
proto	proto	k8xC	proto
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
stroje	stroj	k1gInPc1	stroj
F-102	F-102	k1gFnSc2	F-102
Delta	delta	k1gNnSc2	delta
Dagger	Dagger	k1gMnSc1	Dagger
<g/>
,	,	kIx,	,
Lockheed	Lockheed	k1gMnSc1	Lockheed
F-104	F-104	k1gMnSc1	F-104
Starfighter	Starfighter	k1gMnSc1	Starfighter
,	,	kIx,	,
či	či	k8xC	či
BAe	BAe	k1gFnSc1	BAe
Lightning	Lightning	k1gInSc1	Lightning
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
pak	pak	k6eAd1	pak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Su-	Su-	k1gFnSc4	Su-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
21	[number]	k4	21
nebo	nebo	k8xC	nebo
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
31	[number]	k4	31
Foxhound	Foxhounda	k1gFnPc2	Foxhounda
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc4	označení
záchytný	záchytný	k2eAgMnSc1d1	záchytný
stíhací	stíhací	k2eAgMnSc1d1	stíhací
se	se	k3xPyFc4	se
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
přestalo	přestat	k5eAaPmAgNnS	přestat
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhačky	stíhačka	k1gFnPc1	stíhačka
pro	pro	k7c4	pro
vybojování	vybojování	k1gNnSc4	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
===	===	k?	===
</s>
</p>
<p>
<s>
Stíhačky	stíhačka	k1gFnPc1	stíhačka
pro	pro	k7c4	pro
vybojování	vybojování	k1gNnSc4	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
air	air	k?	air
superiority	superiorita	k1gFnSc2	superiorita
fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k8xS	co
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
země	země	k1gFnSc1	země
schopna	schopen	k2eAgFnSc1d1	schopna
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
omezenou	omezený	k2eAgFnSc4d1	omezená
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc4	žádný
schopnost	schopnost	k1gFnSc4	schopnost
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
pozemní	pozemní	k2eAgInPc4d1	pozemní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
F-4	F-4	k1gFnSc1	F-4
Phantom	Phantom	k1gInSc1	Phantom
II	II	kA	II
<g/>
,	,	kIx,	,
F-15	F-15	k1gFnSc1	F-15
Eagle	Eagle	k1gFnSc1	Eagle
<g/>
,	,	kIx,	,
F-22	F-22	k1gMnSc1	F-22
Raptor	Raptor	k1gMnSc1	Raptor
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
29	[number]	k4	29
Fulcrum	Fulcrum	k1gNnSc1	Fulcrum
<g/>
,	,	kIx,	,
Su-	Su-	k1gFnSc1	Su-
<g/>
27	[number]	k4	27
Flanker	Flankra	k1gFnPc2	Flankra
a	a	k8xC	a
Su-	Su-	k1gFnSc1	Su-
<g/>
47	[number]	k4	47
Berkut	Berkut	k1gMnSc1	Berkut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
ač	ač	k8xS	ač
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
stíhačky	stíhačka	k1gFnPc1	stíhačka
stavěny	stavit	k5eAaImNgFnP	stavit
jako	jako	k8xC	jako
záchytné	záchytný	k2eAgFnPc1d1	záchytná
či	či	k8xC	či
pro	pro	k7c4	pro
vybojování	vybojování	k1gNnSc4	vybojování
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc4	tento
úkoly	úkol	k1gInPc4	úkol
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
prolínají	prolínat	k5eAaImIp3nP	prolínat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
i	i	k9	i
dobré	dobrý	k2eAgInPc1d1	dobrý
stíhací	stíhací	k2eAgInPc1d1	stíhací
bombardéry	bombardér	k1gInPc1	bombardér
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
vysloveně	vysloveně	k6eAd1	vysloveně
útočná	útočný	k2eAgNnPc1d1	útočné
letadla	letadlo	k1gNnPc1	letadlo
(	(	kIx(	(
<g/>
F-	F-	k1gFnSc1	F-
<g/>
4	[number]	k4	4
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
F-	F-	k1gFnSc1	F-
<g/>
15	[number]	k4	15
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
Su-	Su-	k1gFnSc1	Su-
<g/>
30	[number]	k4	30
Flanker	Flankra	k1gFnPc2	Flankra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Noční	noční	k2eAgMnPc1d1	noční
===	===	k?	===
</s>
</p>
<p>
<s>
Noční	noční	k2eAgNnSc1d1	noční
stíhání	stíhání	k1gNnSc1	stíhání
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
již	již	k6eAd1	již
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nutností	nutnost	k1gFnSc7	nutnost
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nočním	noční	k2eAgInPc3d1	noční
náletům	nálet	k1gInPc3	nálet
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
užívány	užíván	k2eAgFnPc1d1	užívána
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
upravené	upravený	k2eAgInPc1d1	upravený
běžné	běžný	k2eAgInPc1d1	běžný
stroje	stroj	k1gInPc1	stroj
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
britské	britský	k2eAgFnSc2d1	britská
B.E	B.E	k1gFnSc2	B.E
<g/>
.2	.2	k4	.2
<g/>
c	c	k0	c
či	či	k8xC	či
modifikovaný	modifikovaný	k2eAgMnSc1d1	modifikovaný
Sopwith	Sopwith	k1gMnSc1	Sopwith
Camel	Camel	k1gMnSc1	Camel
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Sopwith	Sopwith	k1gInSc4	Sopwith
Comic	Comice	k1gInPc2	Comice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
posádky	posádka	k1gFnPc1	posádka
se	se	k3xPyFc4	se
v	v	k7c6	v
detekci	detekce	k1gFnSc6	detekce
protivníka	protivník	k1gMnSc2	protivník
musely	muset	k5eAaImAgFnP	muset
spolehnout	spolehnout	k5eAaPmF	spolehnout
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
stíhače	stíhač	k1gMnSc4	stíhač
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
sestřelu	sestřel	k1gInSc2	sestřel
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
německého	německý	k2eAgNnSc2d1	německé
armádního	armádní	k2eAgNnSc2d1	armádní
letectva	letectvo	k1gNnSc2	letectvo
SL.	SL.	k1gFnSc2	SL.
<g/>
11	[number]	k4	11
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
W.	W.	kA	W.
L.	L.	kA	L.
Robinson	Robinson	k1gMnSc1	Robinson
z	z	k7c2	z
Royal	Royal	k1gInSc4	Royal
Flying	Flying	k1gInSc1	Flying
Corps	corps	k1gInSc1	corps
v	v	k7c6	v
letounu	letoun	k1gInSc6	letoun
typu	typ	k1gInSc2	typ
B.E	B.E	k1gFnSc2	B.E
<g/>
.2	.2	k4	.2
<g/>
c.	c.	k?	c.
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
principu	princip	k1gInSc6	princip
nezměněna	změnit	k5eNaPmNgFnS	změnit
až	až	k8xS	až
do	do	k7c2	do
počátečních	počáteční	k2eAgNnPc2d1	počáteční
let	léto	k1gNnPc2	léto
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc6	zdokonalování
radiolokátorů	radiolokátor	k1gInPc2	radiolokátor
vedl	vést	k5eAaImAgInS	vést
posléze	posléze	k6eAd1	posléze
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
takových	takový	k3xDgInPc2	takový
jejich	jejich	k3xOp3gInPc2	jejich
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
nést	nést	k5eAaImF	nést
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozměrům	rozměr	k1gInPc3	rozměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
těchto	tento	k3xDgInPc2	tento
prvních	první	k4xOgInPc2	první
typů	typ	k1gInPc2	typ
palubních	palubní	k2eAgInPc2d1	palubní
radarů	radar	k1gInPc2	radar
a	a	k8xC	a
složitosti	složitost	k1gFnSc3	složitost
jejich	jejich	k3xOp3gFnSc2	jejich
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pozornost	pozornost	k1gFnSc4	pozornost
specializovaného	specializovaný	k2eAgMnSc4d1	specializovaný
člena	člen	k1gMnSc4	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
dvoumotorové	dvoumotorový	k2eAgInPc4d1	dvoumotorový
vícemístné	vícemístný	k2eAgInPc4d1	vícemístný
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Bristol	Bristol	k1gInSc1	Bristol
Beaufighter	Beaufighter	k1gInSc1	Beaufighter
nebo	nebo	k8xC	nebo
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Bf	Bf	k1gFnSc2	Bf
110	[number]	k4	110
(	(	kIx(	(
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
předchozích	předchozí	k2eAgInPc6d1	předchozí
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
ale	ale	k9	ale
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stroje	stroj	k1gInPc4	stroj
upravené	upravený	k2eAgInPc4d1	upravený
pro	pro	k7c4	pro
noční	noční	k2eAgFnPc4d1	noční
stíháni	stíhat	k5eAaImNgMnP	stíhat
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
ale	ale	k9	ale
i	i	k9	i
konstrukce	konstrukce	k1gFnPc4	konstrukce
navržené	navržený	k2eAgFnPc4d1	navržená
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
k	k	k7c3	k
nočnímu	noční	k2eAgNnSc3d1	noční
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
:	:	kIx,	:
Northrop	Northrop	k1gInSc4	Northrop
P-61	P-61	k1gFnPc2	P-61
Black	Black	k1gMnSc1	Black
Widow	Widow	k1gMnSc1	Widow
<g/>
,	,	kIx,	,
Heinkel	Heinkel	k1gMnSc1	Heinkel
He-	He-	k1gFnSc2	He-
<g/>
219	[number]	k4	219
Uhu	Uhu	k1gFnPc2	Uhu
<g/>
,	,	kIx,	,
či	či	k8xC	či
Focke-Wulf	Focke-Wulf	k1gInSc1	Focke-Wulf
Ta	ten	k3xDgFnSc1	ten
154	[number]	k4	154
Moskito	Moskit	k2eAgNnSc1d1	Moskito
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
pokročilých	pokročilý	k2eAgInPc2d1	pokročilý
elektronických	elektronický	k2eAgInPc2d1	elektronický
navigačních	navigační	k2eAgInPc2d1	navigační
<g/>
,	,	kIx,	,
detekčních	detekční	k2eAgInPc2d1	detekční
a	a	k8xC	a
zaměřovacích	zaměřovací	k2eAgInPc2d1	zaměřovací
systémů	systém	k1gInPc2	systém
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
úkolů	úkol	k1gInPc2	úkol
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
i	i	k9	i
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
ve	v	k7c6	v
dne	den	k1gInSc2	den
za	za	k7c2	za
ztížených	ztížený	k2eAgFnPc2d1	ztížená
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
mlha	mlha	k1gFnSc1	mlha
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
oblačnost	oblačnost	k1gFnSc1	oblačnost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
takových	takový	k3xDgFnPc2	takový
podmínek	podmínka	k1gFnPc2	podmínka
nebyly	být	k5eNaImAgFnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ani	ani	k8xC	ani
ofenzívní	ofenzívní	k2eAgFnSc2d1	ofenzívní
ani	ani	k8xC	ani
defenzívní	defenzívní	k2eAgFnSc2d1	defenzívní
letecké	letecký	k2eAgFnSc2d1	letecká
operace	operace	k1gFnSc2	operace
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
většinou	většinou	k6eAd1	většinou
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
na	na	k7c4	na
stíhací	stíhací	k2eAgInPc4d1	stíhací
letouny	letoun	k1gInPc4	letoun
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
počasí	počasí	k1gNnSc4	počasí
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
all-weather	alleathra	k1gFnPc2	all-weathra
fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takovéhoto	takovýto	k3xDgInSc2	takovýto
typu	typ	k1gInSc2	typ
stroje	stroj	k1gInSc2	stroj
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
Gloster	Gloster	k1gInSc4	Gloster
Javelin	Javelina	k1gFnPc2	Javelina
<g/>
,	,	kIx,	,
SO	So	kA	So
<g/>
.450	.450	k4	.450
Vautour	Vautoura	k1gFnPc2	Vautoura
IIN	IIN	kA	IIN
<g/>
,	,	kIx,	,
Jakovlev	Jakovlev	k1gMnSc1	Jakovlev
Jak-	Jak-	k1gFnSc2	Jak-
<g/>
25	[number]	k4	25
či	či	k8xC	či
F-89	F-89	k1gMnSc4	F-89
Scorpion	Scorpion	k1gInSc4	Scorpion
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
rozvojem	rozvoj	k1gInSc7	rozvoj
a	a	k8xC	a
rozšířením	rozšíření	k1gNnSc7	rozšíření
elektronických	elektronický	k2eAgInPc2d1	elektronický
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
stíhacích	stíhací	k2eAgInPc6d1	stíhací
strojích	stroj	k1gInPc6	stroj
a	a	k8xC	a
vzrůstající	vzrůstající	k2eAgFnPc4d1	vzrůstající
míry	míra	k1gFnPc4	míra
automatizace	automatizace	k1gFnSc2	automatizace
jejich	jejich	k3xOp3gFnSc2	jejich
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přestala	přestat	k5eAaPmAgFnS	přestat
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
pozornost	pozornost	k1gFnSc4	pozornost
operátora	operátor	k1gMnSc2	operátor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
odpadla	odpadnout	k5eAaPmAgFnS	odpadnout
nutnost	nutnost	k1gFnSc4	nutnost
dvojčlenné	dvojčlenný	k2eAgFnSc2d1	dvojčlenná
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
konstrukční	konstrukční	k2eAgInSc4d1	konstrukční
typ	typ	k1gInSc4	typ
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
splynula	splynout	k5eAaPmAgNnP	splynout
se	s	k7c7	s
záchytnými	záchytný	k2eAgMnPc7d1	záchytný
stíhači	stíhač	k1gMnPc7	stíhač
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
splynutí	splynutí	k1gNnSc2	splynutí
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
přepadové	přepadový	k2eAgFnPc1d1	přepadová
stíhačky	stíhačka	k1gFnPc1	stíhačka
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
byly	být	k5eAaImAgFnP	být
standardně	standardně	k6eAd1	standardně
vybavovány	vybavovat	k5eAaImNgFnP	vybavovat
elektronickým	elektronický	k2eAgNnSc7d1	elektronické
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
operovat	operovat	k5eAaImF	operovat
i	i	k9	i
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Palubní	palubní	k2eAgMnPc4d1	palubní
===	===	k?	===
</s>
</p>
<p>
<s>
Palubní	palubní	k2eAgInSc1d1	palubní
stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
carrier	carrier	k1gMnSc1	carrier
fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
primárně	primárně	k6eAd1	primárně
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
mateřské	mateřský	k2eAgFnSc2d1	mateřská
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodě	loď	k1gFnSc2	loď
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ale	ale	k8xC	ale
palubní	palubní	k2eAgFnPc1d1	palubní
stíhačky	stíhačka	k1gFnPc1	stíhačka
plnily	plnit	k5eAaImAgFnP	plnit
více	hodně	k6eAd2	hodně
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ochrany	ochrana	k1gFnSc2	ochrana
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
také	také	k9	také
doprovod	doprovod	k1gInSc1	doprovod
vlastních	vlastní	k2eAgInPc2d1	vlastní
útočných	útočný	k2eAgInPc2d1	útočný
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
stíhacích	stíhací	k2eAgInPc2d1	stíhací
bombardérů	bombardér	k1gInPc2	bombardér
<g/>
,	,	kIx,	,
průzkumných	průzkumný	k2eAgInPc2d1	průzkumný
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgInP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tak	tak	k6eAd1	tak
víceúčelové	víceúčelový	k2eAgInPc4d1	víceúčelový
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přínosem	přínos	k1gInSc7	přínos
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
kapacitou	kapacita	k1gFnSc7	kapacita
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
spadá	spadat	k5eAaImIp3nS	spadat
víceúčelový	víceúčelový	k2eAgMnSc1d1	víceúčelový
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Palubní	palubní	k2eAgInPc1d1	palubní
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
službě	služba	k1gFnSc3	služba
na	na	k7c4	na
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodi	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
vzletovou	vzletový	k2eAgFnSc4d1	vzletová
konfiguraci	konfigurace	k1gFnSc4	konfigurace
CATOBAR	CATOBAR	kA	CATOBAR
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
V	v	k7c6	v
<g/>
/	/	kIx~	/
<g/>
STOL.	STOL.	k1gFnSc6	STOL.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhací	stíhací	k2eAgInPc1d1	stíhací
hydroplány	hydroplán	k1gInPc1	hydroplán
===	===	k?	===
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
stíhacího	stíhací	k2eAgNnSc2d1	stíhací
letectva	letectvo	k1gNnSc2	letectvo
byly	být	k5eAaImAgFnP	být
jako	jako	k9	jako
stíhací	stíhací	k2eAgInPc4d1	stíhací
někdy	někdy	k6eAd1	někdy
užívány	užíván	k2eAgInPc4d1	užíván
i	i	k9	i
hydroplány	hydroplán	k1gInPc4	hydroplán
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
výhodou	výhoda	k1gFnSc7	výhoda
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc1	možnost
operovat	operovat	k5eAaImF	operovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
pozemních	pozemní	k2eAgNnPc2d1	pozemní
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
koncepce	koncepce	k1gFnSc1	koncepce
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
současně	současně	k6eAd1	současně
posádce	posádka	k1gFnSc3	posádka
poněkud	poněkud	k6eAd1	poněkud
větší	veliký	k2eAgFnSc4d2	veliký
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
nouzového	nouzový	k2eAgNnSc2d1	nouzové
přistání	přistání	k1gNnSc2	přistání
na	na	k7c4	na
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
menší	malý	k2eAgFnSc3d2	menší
spolehlivosti	spolehlivost	k1gFnSc3	spolehlivost
motorů	motor	k1gInPc2	motor
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stíhací	stíhací	k2eAgInSc1d1	stíhací
létající	létající	k2eAgInSc1d1	létající
člun	člun	k1gInSc1	člun
====	====	k?	====
</s>
</p>
<p>
<s>
Létající	létající	k2eAgInPc1d1	létající
čluny	člun	k1gInPc1	člun
byly	být	k5eAaImAgInP	být
někdy	někdy	k6eAd1	někdy
užívány	užívat	k5eAaImNgInP	užívat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
stíhacích	stíhací	k2eAgInPc2d1	stíhací
úkolů	úkol	k1gInPc2	úkol
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
letectvem	letectvo	k1gNnSc7	letectvo
rakousko-uherského	rakouskoherský	k2eAgNnSc2d1	rakousko-uherské
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
původně	původně	k6eAd1	původně
průzkumný	průzkumný	k2eAgInSc4d1	průzkumný
Lohner	Lohner	k1gInSc4	Lohner
L	L	kA	L
<g/>
)	)	kIx)	)
a	a	k8xC	a
italského	italský	k2eAgNnSc2d1	italské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
Macchi	Macch	k1gFnSc2	Macch
M.	M.	kA	M.
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
koncipované	koncipovaný	k2eAgInPc1d1	koncipovaný
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
již	již	k6eAd1	již
v	v	k7c6	v
řadové	řadový	k2eAgFnSc6d1	řadová
službě	služba	k1gFnSc6	služba
neobjevily	objevit	k5eNaPmAgFnP	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Plovákový	plovákový	k2eAgInSc1d1	plovákový
stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
stíhačky	stíhačka	k1gFnSc2	stíhačka
vybavené	vybavený	k2eAgInPc4d1	vybavený
plováky	plovák	k1gInPc4	plovák
ať	ať	k8xC	ať
již	již	k6eAd1	již
účelově	účelově	k6eAd1	účelově
konstruované	konstruovaný	k2eAgFnPc1d1	konstruovaná
anebo	anebo	k8xC	anebo
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
přestavbou	přestavba	k1gFnSc7	přestavba
původně	původně	k6eAd1	původně
pozemního	pozemní	k2eAgInSc2d1	pozemní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Užívané	užívaný	k2eAgFnPc1d1	užívaná
byly	být	k5eAaImAgFnP	být
zejména	zejména	k9	zejména
námořními	námořní	k2eAgInPc7d1	námořní
letectvy	letectv	k1gInPc7	letectv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnSc4	představitel
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Spad	spad	k1gInSc4	spad
S.	S.	kA	S.
<g/>
XIV	XIV	kA	XIV
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
W.	W.	kA	W.
<g/>
IV	IV	kA	IV
nebo	nebo	k8xC	nebo
Hansa-Brandenburg	Hansa-Brandenburg	k1gInSc1	Hansa-Brandenburg
W	W	kA	W
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
takto	takto	k6eAd1	takto
koncipovaných	koncipovaný	k2eAgInPc2d1	koncipovaný
strojů	stroj	k1gInPc2	stroj
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Loire	Loir	k1gInSc5	Loir
210	[number]	k4	210
nebo	nebo	k8xC	nebo
IMAM	IMAM	kA	IMAM
Ro	Ro	k1gFnSc1	Ro
<g/>
.44	.44	k4	.44
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
ale	ale	k8xC	ale
plovákové	plovákový	k2eAgInPc1d1	plovákový
letouny	letoun	k1gInPc1	letoun
nebyly	být	k5eNaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
výkony	výkon	k1gInPc1	výkon
konkurovat	konkurovat	k5eAaImF	konkurovat
letounům	letoun	k1gInPc3	letoun
pozemním	pozemní	k2eAgInPc3d1	pozemní
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vzrůstu	vzrůst	k1gInSc3	vzrůst
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
možností	možnost	k1gFnPc2	možnost
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
schopných	schopný	k2eAgFnPc2d1	schopná
zajistit	zajistit	k5eAaPmF	zajistit
stíhací	stíhací	k2eAgNnSc4d1	stíhací
krytí	krytí	k1gNnSc4	krytí
loďstva	loďstvo	k1gNnSc2	loďstvo
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
pozemních	pozemní	k2eAgFnPc2d1	pozemní
základen	základna	k1gFnPc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
účelově	účelově	k6eAd1	účelově
zkonstruovaným	zkonstruovaný	k2eAgMnSc7d1	zkonstruovaný
zástupcem	zástupce	k1gMnSc7	zástupce
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Kawaniši	Kawaniše	k1gFnSc4	Kawaniše
N1K1	N1K1	k1gMnSc1	N1K1
japonského	japonský	k2eAgNnSc2d1	Japonské
námořního	námořní	k2eAgNnSc2d1	námořní
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Víceúčelové	víceúčelový	k2eAgMnPc4d1	víceúčelový
===	===	k?	===
</s>
</p>
<p>
<s>
Víceúčelové	víceúčelový	k2eAgFnPc1d1	víceúčelová
stíhačky	stíhačka	k1gFnPc1	stíhačka
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
multirole	multirole	k1gFnSc1	multirole
combat	combat	k5eAaPmF	combat
aircraft	aircraft	k1gInSc4	aircraft
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
letouny	letoun	k1gInPc4	letoun
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
stavěné	stavěný	k2eAgNnSc1d1	stavěné
pro	pro	k7c4	pro
vykonávání	vykonávání	k1gNnSc4	vykonávání
více	hodně	k6eAd2	hodně
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
preferovány	preferován	k2eAgMnPc4d1	preferován
a	a	k8xC	a
díky	díky	k7c3	díky
schopnosti	schopnost	k1gFnSc3	schopnost
nést	nést	k5eAaImF	nést
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
výzbroje	výzbroj	k1gFnSc2	výzbroj
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
přepadové	přepadový	k2eAgFnSc2d1	přepadová
<g/>
,	,	kIx,	,
záchytné	záchytný	k2eAgFnSc2d1	záchytná
<g/>
,	,	kIx,	,
eskortní	eskortní	k2eAgFnSc2d1	eskortní
<g/>
,	,	kIx,	,
útočné	útočný	k2eAgFnSc2d1	útočná
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
strojům	stroj	k1gInPc3	stroj
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
např.	např.	kA	např.
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
československých	československý	k2eAgFnPc2d1	Československá
stíhaček	stíhačka	k1gFnPc2	stíhačka
==	==	k?	==
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc1	aero
Ae-	Ae-	k1gFnSc2	Ae-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc4	aero
A-18	A-18	k1gFnSc2	A-18
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc4	aero
A-32	A-32	k1gFnSc2	A-32
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc1	aero
A-	A-	k1gFnSc2	A-
<g/>
102	[number]	k4	102
<g/>
Avia	Avia	k1gFnSc1	Avia
BH-3	BH-3	k1gFnSc1	BH-3
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
BH-6	BH-6	k1gFnSc2	BH-6
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
BH-7	BH-7	k1gFnSc2	BH-7
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
BH-21	BH-21	k1gFnSc2	BH-21
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
BH-33	BH-33	k1gFnSc2	BH-33
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
B-34	B-34	k1gFnSc2	B-34
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
B-534	B-534	k1gFnSc2	B-534
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
B-35	B-35	k1gFnSc2	B-35
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
B-135	B-135	k1gFnSc2	B-135
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
S-92	S-92	k1gFnSc2	S-92
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
S-	S-	k1gFnSc2	S-
<g/>
199	[number]	k4	199
<g/>
Letov	Letovo	k1gNnPc2	Letovo
Š-3	Š-3	k1gMnPc2	Š-3
</s>
</p>
<p>
<s>
Letov	Letov	k1gInSc1	Letov
Š-4	Š-4	k1gFnSc2	Š-4
</s>
</p>
<p>
<s>
Letov	Letov	k1gInSc1	Letov
Š-14	Š-14	k1gFnSc2	Š-14
</s>
</p>
<p>
<s>
Letov	Letov	k1gInSc1	Letov
Š-231	Š-231	k1gFnSc2	Š-231
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
stíhaček	stíhačka	k1gFnPc2	stíhačka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nieuport	Nieuport	k1gInSc1	Nieuport
10	[number]	k4	10
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
21	[number]	k4	21
,27	,27	k4	,27
</s>
</p>
<p>
<s>
SPAD	spad	k1gInSc1	spad
S.	S.	kA	S.
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
XII	XII	kA	XII
<g/>
,	,	kIx,	,
XIII	XIII	kA	XIII
</s>
</p>
<p>
<s>
Dewoitine	Dewoitin	k1gMnSc5	Dewoitin
D	D	kA	D
<g/>
.27	.27	k4	.27
</s>
</p>
<p>
<s>
Hanriot	Hanriot	k1gInSc1	Hanriot
HD	HD	kA	HD
<g/>
.1	.1	k4	.1
</s>
</p>
<p>
<s>
Morane	Moranout	k5eAaPmIp3nS	Moranout
N	N	kA	N
(	(	kIx(	(
<g/>
M.	M.	kA	M.
<g/>
S.	S.	kA	S.
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sopwith	Sopwith	k1gInSc1	Sopwith
Pup	pupa	k1gFnPc2	pupa
<g/>
,	,	kIx,	,
Triplane	Triplan	k1gMnSc5	Triplan
<g/>
,	,	kIx,	,
Camel	Camel	k1gMnSc1	Camel
</s>
</p>
<p>
<s>
Martinsyde	Martinsyde	k6eAd1	Martinsyde
BuzzardNěmecko	BuzzardNěmecko	k1gNnSc1	BuzzardNěmecko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Albatros	albatros	k1gMnSc1	albatros
D.I	D.I	k1gMnSc1	D.I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
W.	W.	kA	W.
<g/>
IV	IV	kA	IV
</s>
</p>
<p>
<s>
Fokker	Fokker	k1gMnSc1	Fokker
D.	D.	kA	D.
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
III	III	kA	III
</s>
</p>
<p>
<s>
Halberstadt	Halberstadt	k1gMnSc1	Halberstadt
D.	D.	kA	D.
<g/>
II	II	kA	II
</s>
</p>
<p>
<s>
Junkers	Junkers	k1gInSc1	Junkers
D.I	D.I	k1gFnSc2	D.I
</s>
</p>
<p>
<s>
Pfalz	Pfalz	k1gMnSc1	Pfalz
D.	D.	kA	D.
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
XII	XII	kA	XII
</s>
</p>
<p>
<s>
LFG	LFG	kA	LFG
Roland	Rolando	k1gNnPc2	Rolando
D.	D.	kA	D.
<g/>
VI	VI	kA	VI
</s>
</p>
<p>
<s>
===	===	k?	===
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
BH-21	BH-21	k1gFnSc2	BH-21
</s>
</p>
<p>
<s>
Avia	Avia	k1gFnSc1	Avia
B-	B-	k1gFnSc2	B-
<g/>
534	[number]	k4	534
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Arado	Arado	k1gNnSc4	Arado
Ar	ar	k1gInSc1	ar
68	[number]	k4	68
</s>
</p>
<p>
<s>
Heinkel	Heinkel	k1gMnSc1	Heinkel
He	he	k0	he
112	[number]	k4	112
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fokker	Fokker	k1gMnSc1	Fokker
D.	D.	kA	D.
<g/>
XXI	XXI	kA	XXI
</s>
</p>
<p>
<s>
Fokker	Fokker	k1gMnSc1	Fokker
G.	G.	kA	G.
<g/>
ISpojené	ISpojený	k2eAgNnSc1d1	ISpojený
království	království	k1gNnSc1	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
Whitworth	Whitworth	k1gMnSc1	Whitworth
Siskin	Siskin	k1gMnSc1	Siskin
</s>
</p>
<p>
<s>
Bristol	Bristol	k1gInSc1	Bristol
Bulldog	bulldog	k1gMnSc1	bulldog
</s>
</p>
<p>
<s>
Gloster	Gloster	k1gMnSc1	Gloster
Gladiator	Gladiator	k1gMnSc1	Gladiator
</s>
</p>
<p>
<s>
Hawker	Hawker	k1gInSc1	Hawker
FurySpojené	FurySpojený	k2eAgInPc1d1	FurySpojený
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Seversky	seversky	k6eAd1	seversky
P-35	P-35	k1gFnSc1	P-35
</s>
</p>
<p>
<s>
Curtiss	Curtiss	k6eAd1	Curtiss
P-36	P-36	k1gMnSc1	P-36
Hawk	Hawk	k1gMnSc1	Hawk
</s>
</p>
<p>
<s>
Brewster	Brewster	k1gInSc1	Brewster
F2A	F2A	k1gFnSc2	F2A
BuffaloFrancie	BuffaloFrancie	k1gFnSc2	BuffaloFrancie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dewoitine	Dewoitin	k1gMnSc5	Dewoitin
D	D	kA	D
<g/>
.500	.500	k4	.500
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fiat	fiat	k1gInSc1	fiat
CR	cr	k0	cr
<g/>
.42	.42	k4	.42
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
PZL	PZL	kA	PZL
P.	P.	kA	P.
<g/>
11	[number]	k4	11
<g/>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Polikarpov	Polikarpov	k1gInSc1	Polikarpov
I-15	I-15	k1gFnSc2	I-15
</s>
</p>
<p>
<s>
Polikarpov	Polikarpov	k1gInSc1	Polikarpov
I-16	I-16	k1gFnSc2	I-16
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Republic	Republice	k1gFnPc2	Republice
P-47	P-47	k1gMnSc1	P-47
Thunderbolt	Thunderbolt	k1gMnSc1	Thunderbolt
</s>
</p>
<p>
<s>
Lockheed	Lockheed	k1gMnSc1	Lockheed
P-38	P-38	k1gMnSc1	P-38
Lightning	Lightning	k1gInSc4	Lightning
</s>
</p>
<p>
<s>
Bell	bell	k1gInSc1	bell
P-39	P-39	k1gMnSc2	P-39
Airacobra	Airacobr	k1gMnSc2	Airacobr
</s>
</p>
<p>
<s>
Curtiss	Curtiss	k6eAd1	Curtiss
P-40	P-40	k1gMnSc1	P-40
Warhawk	Warhawk	k1gMnSc1	Warhawk
</s>
</p>
<p>
<s>
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
P-51	P-51	k1gMnSc1	P-51
Mustang	mustang	k1gMnSc1	mustang
</s>
</p>
<p>
<s>
Grumman	Grumman	k1gMnSc1	Grumman
F4F	F4F	k1gMnSc1	F4F
Wildcat	Wildcat	k1gMnSc1	Wildcat
</s>
</p>
<p>
<s>
Grumman	Grumman	k1gMnSc1	Grumman
F6F	F6F	k1gMnSc1	F6F
Hellcat	Hellcat	k1gMnSc1	Hellcat
</s>
</p>
<p>
<s>
Vought	Vought	k1gInSc4	Vought
F4U	F4U	k1gFnSc2	F4U
CorsairNěmecko	CorsairNěmecko	k1gNnSc4	CorsairNěmecko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Bf	Bf	k1gFnSc2	Bf
109	[number]	k4	109
</s>
</p>
<p>
<s>
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Bf	Bf	k1gFnSc2	Bf
110	[number]	k4	110
</s>
</p>
<p>
<s>
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Me	Me	k1gFnSc2	Me
163	[number]	k4	163
</s>
</p>
<p>
<s>
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
Me	Me	k1gFnSc2	Me
262	[number]	k4	262
Schwalbe	Schwalb	k1gInSc5	Schwalb
</s>
</p>
<p>
<s>
Focke-Wulf	Focke-Wulf	k1gInSc1	Focke-Wulf
Fw	Fw	k1gFnSc2	Fw
190	[number]	k4	190
/	/	kIx~	/
Focke-Wulf	Focke-Wulf	k1gInSc1	Focke-Wulf
Ta	ten	k3xDgFnSc1	ten
152	[number]	k4	152
</s>
</p>
<p>
<s>
Heinkel	Heinkel	k1gMnSc1	Heinkel
He	he	k0	he
162	[number]	k4	162
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bristol	Bristol	k1gInSc1	Bristol
Beaufighter	Beaufightra	k1gFnPc2	Beaufightra
</s>
</p>
<p>
<s>
de	de	k?	de
Havilland	Havilland	k1gInSc1	Havilland
Mosquito	Mosquit	k2eAgNnSc4d1	Mosquito
</s>
</p>
<p>
<s>
Hawker	Hawkrat	k5eAaPmRp2nS	Hawkrat
Hurricane	Hurrican	k1gMnSc5	Hurrican
</s>
</p>
<p>
<s>
Hawker	Hawker	k1gMnSc1	Hawker
Typhoon	Typhoon	k1gMnSc1	Typhoon
</s>
</p>
<p>
<s>
Hawker	Hawker	k1gMnSc1	Hawker
Tempest	Tempest	k1gMnSc1	Tempest
</s>
</p>
<p>
<s>
Supermarine	Supermarin	k1gInSc5	Supermarin
Seafire	Seafir	k1gMnSc5	Seafir
</s>
</p>
<p>
<s>
Supermarine	Supermarin	k1gInSc5	Supermarin
Spitfire	Spitfir	k1gInSc5	Spitfir
</s>
</p>
<p>
<s>
Gloster	Gloster	k1gInSc1	Gloster
MeteorFrancie	MeteorFrancie	k1gFnSc2	MeteorFrancie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dewoitine	Dewoitin	k1gMnSc5	Dewoitin
D	D	kA	D
<g/>
.520	.520	k4	.520
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Doflug	Doflug	k1gInSc1	Doflug
D-	D-	k1gFnSc2	D-
<g/>
3802	[number]	k4	3802
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Macchi	Macchi	k6eAd1	Macchi
MC	MC	kA	MC
<g/>
.202	.202	k4	.202
Folgore	Folgor	k1gInSc5	Folgor
<g/>
,	,	kIx,	,
Macchi	Macch	k1gFnPc4	Macch
MC	MC	kA	MC
<g/>
.205	.205	k4	.205
VeltroJaponsko	VeltroJaponsko	k1gNnSc1	VeltroJaponsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kawasaki	Kawasaki	k1gNnSc1	Kawasaki
Ki-	Ki-	k1gFnSc2	Ki-
<g/>
61	[number]	k4	61
</s>
</p>
<p>
<s>
Micubiši	Micubiš	k1gMnPc1	Micubiš
A6M	A6M	k1gFnSc2	A6M
</s>
</p>
<p>
<s>
Nakadžima	Nakadžima	k1gFnSc1	Nakadžima
Ki-	Ki-	k1gFnSc1	Ki-
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
Nakadžima	Nakadžim	k1gMnSc2	Nakadžim
Ki-	Ki-	k1gMnSc2	Ki-
<g/>
84	[number]	k4	84
<g/>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Polikarpov	Polikarpov	k1gInSc1	Polikarpov
I-153	I-153	k1gFnSc2	I-153
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
Lavočkin	Lavočkin	k2eAgMnSc1d1	Lavočkin
LaGG-	LaGG-	k1gMnSc1	LaGG-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
La-	La-	k1gFnSc1	La-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
La-	La-	k1gFnSc1	La-
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
Jakovlev	Jakovlet	k5eAaPmDgInS	Jakovlet
Jak-	Jak-	k1gMnSc7	Jak-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Jak-	Jak-	k1gFnSc1	Jak-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Jak-	Jak-	k1gFnSc1	Jak-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Jak-	Jak-	k1gFnSc1	Jak-
<g/>
9	[number]	k4	9
</s>
</p>
<p>
<s>
===	===	k?	===
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Firmy	firma	k1gFnPc1	firma
prošly	projít	k5eAaPmAgFnP	projít
mnoha	mnoho	k4c7	mnoho
fúzemi	fúze	k1gFnPc7	fúze
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
udávány	udáván	k2eAgInPc1d1	udáván
pod	pod	k7c7	pod
původními	původní	k2eAgInPc7d1	původní
výrobci	výrobce	k1gMnPc7	výrobce
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Gloster	Gloster	k1gInSc1	Gloster
Meteor	meteor	k1gInSc1	meteor
</s>
</p>
<p>
<s>
Hawker	Hawker	k1gMnSc1	Hawker
Hunter	Hunter	k1gMnSc1	Hunter
</s>
</p>
<p>
<s>
English	English	k1gMnSc1	English
Electric	Electric	k1gMnSc1	Electric
Lightning	Lightning	k1gInSc4	Lightning
</s>
</p>
<p>
<s>
Tornado	Tornada	k1gFnSc5	Tornada
ADV	ADV	kA	ADV
–	–	k?	–
Panavia	Panavia	k1gFnSc1	Panavia
GmBhŠvédsko	GmBhŠvédsko	k1gNnSc4	GmBhŠvédsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Saab	Saab	k1gInSc1	Saab
32	[number]	k4	32
Lansen	Lansna	k1gFnPc2	Lansna
</s>
</p>
<p>
<s>
Saab	Saab	k1gInSc1	Saab
35	[number]	k4	35
Draken	Drakna	k1gFnPc2	Drakna
</s>
</p>
<p>
<s>
Saab	Saab	k1gMnSc1	Saab
37	[number]	k4	37
Viggen	Viggen	k1gInSc1	Viggen
</s>
</p>
<p>
<s>
Saab	Saab	k1gInSc1	Saab
JAS-39	JAS-39	k1gFnSc2	JAS-39
GripenSpojené	GripenSpojený	k2eAgInPc1d1	GripenSpojený
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
McDonnell	McDonnell	k1gInSc1	McDonnell
F2H	F2H	k1gMnSc2	F2H
Banshee	Banshe	k1gMnSc2	Banshe
</s>
</p>
<p>
<s>
Lockheed	Lockheed	k1gMnSc1	Lockheed
P-80	P-80	k1gFnSc2	P-80
Shooting	Shooting	k1gInSc1	Shooting
Star	Star	kA	Star
</s>
</p>
<p>
<s>
F-84	F-84	k4	F-84
Thunderjet	Thunderjet	k1gInSc1	Thunderjet
</s>
</p>
<p>
<s>
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
F-86	F-86	k1gMnSc1	F-86
Sabre	Sabr	k1gInSc5	Sabr
</s>
</p>
<p>
<s>
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
F-100	F-100	k1gMnSc1	F-100
Super	super	k6eAd1	super
Sabre	Sabr	k1gInSc5	Sabr
</s>
</p>
<p>
<s>
Convair	Convair	k1gMnSc1	Convair
F-102	F-102	k1gMnSc2	F-102
Delta	delta	k1gNnSc2	delta
Dagger	Dagger	k1gMnSc1	Dagger
</s>
</p>
<p>
<s>
Lockheed	Lockheed	k1gMnSc1	Lockheed
F-104	F-104	k1gMnSc1	F-104
Starfighter	Starfighter	k1gMnSc1	Starfighter
</s>
</p>
<p>
<s>
Republic	Republice	k1gFnPc2	Republice
F-105	F-105	k1gMnSc1	F-105
Thunderchief	Thunderchief	k1gMnSc1	Thunderchief
</s>
</p>
<p>
<s>
Convair	Convair	k1gMnSc1	Convair
F-106	F-106	k1gMnSc2	F-106
Delta	delta	k1gNnSc2	delta
Dart	Dart	k1gMnSc1	Dart
</s>
</p>
<p>
<s>
McDonnell	McDonnell	k1gMnSc1	McDonnell
F-4	F-4	k1gFnSc2	F-4
Phantom	Phantom	k1gInSc1	Phantom
II	II	kA	II
/	/	kIx~	/
F-110	F-110	k1gMnSc1	F-110
</s>
</p>
<p>
<s>
Northrop	Northrop	k1gInSc1	Northrop
F-5	F-5	k1gFnPc2	F-5
Freedom	Freedom	k1gInSc1	Freedom
Fighter	fighter	k1gMnSc1	fighter
</s>
</p>
<p>
<s>
Vought	Vought	k2eAgMnSc1d1	Vought
F-8	F-8	k1gMnSc1	F-8
Crusader	Crusader	k1gMnSc1	Crusader
</s>
</p>
<p>
<s>
Grumman	Grumman	k1gMnSc1	Grumman
F-14	F-14	k1gMnSc1	F-14
Tomcat	Tomcat	k1gMnSc1	Tomcat
</s>
</p>
<p>
<s>
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
F-15	F-15	k1gMnSc1	F-15
Eagle	Eagle	k1gInSc4	Eagle
</s>
</p>
<p>
<s>
General	Generat	k5eAaPmAgInS	Generat
Dynamics	Dynamics	k1gInSc4	Dynamics
F-16	F-16	k1gMnPc2	F-16
Fighting	Fighting	k1gInSc1	Fighting
Falcon	Falcona	k1gFnPc2	Falcona
</s>
</p>
<p>
<s>
Northop	Northop	k1gInSc1	Northop
<g/>
/	/	kIx~	/
<g/>
McDonnel	McDonnel	k1gInSc1	McDonnel
Douglas	Douglas	k1gInSc1	Douglas
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gMnSc2	A-
<g/>
18	[number]	k4	18
HornetSovětský	HornetSovětský	k2eAgInSc1d1	HornetSovětský
svaz	svaz	k1gInSc1	svaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
15	[number]	k4	15
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
17	[number]	k4	17
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
19	[number]	k4	19
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
21	[number]	k4	21
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
23	[number]	k4	23
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
25	[number]	k4	25
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
27	[number]	k4	27
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
29	[number]	k4	29
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
31	[number]	k4	31
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
9	[number]	k4	9
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
15	[number]	k4	15
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
27	[number]	k4	27
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
30	[number]	k4	30
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
33	[number]	k4	33
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
35	[number]	k4	35
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhačky	stíhačka	k1gFnSc2	stíhačka
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eurofighter	Eurofighter	k1gMnSc1	Eurofighter
Typhoon	Typhoon	k1gMnSc1	Typhoon
–	–	k?	–
Eurofighter	Eurofighter	k1gMnSc1	Eurofighter
GmBhFrancie	GmBhFrancie	k1gFnSc2	GmBhFrancie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dassault	Dassault	k5eAaPmF	Dassault
Rafale	Rafala	k1gFnSc3	Rafala
–	–	k?	–
Dassault	Dassault	k1gInSc1	Dassault
AviationŠvédsko	AviationŠvédsko	k1gNnSc1	AviationŠvédsko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jas-	Jas-	k?	Jas-
<g/>
39	[number]	k4	39
Gripen	Gripen	k2eAgInSc4d1	Gripen
NG	NG	kA	NG
–	–	k?	–
SaabSpojené	SaabSpojený	k2eAgInPc1d1	SaabSpojený
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
F-15E	F-15E	k4	F-15E
Strike	Strike	k1gFnSc1	Strike
Eagle	Eagle	k1gFnSc1	Eagle
–	–	k?	–
Boeing	boeing	k1gInSc4	boeing
</s>
</p>
<p>
<s>
F-15E	F-15E	k4	F-15E
Silent	Silent	k1gInSc1	Silent
Eagle	Eagle	k1gInSc4	Eagle
–	–	k?	–
Boeing	boeing	k1gInSc1	boeing
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gMnSc1	A-
<g/>
18	[number]	k4	18
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
Super	super	k2eAgInSc1d1	super
Hornet	Hornet	k1gInSc1	Hornet
–	–	k?	–
Boeing	boeing	k1gInSc4	boeing
</s>
</p>
<p>
<s>
F-22	F-22	k4	F-22
Raptor	Raptor	k1gInSc1	Raptor
–	–	k?	–
Boeing	boeing	k1gInSc1	boeing
<g/>
/	/	kIx~	/
<g/>
Lockheed	Lockheed	k1gMnSc1	Lockheed
Martin	Martin	k1gMnSc1	Martin
</s>
</p>
<p>
<s>
F-35	F-35	k4	F-35
Lightning	Lightning	k1gInSc1	Lightning
II	II	kA	II
–	–	k?	–
Lockheed	Lockheed	k1gMnSc1	Lockheed
Martin	Martin	k1gMnSc1	Martin
<g/>
/	/	kIx~	/
<g/>
BAeRusko	BAeRusko	k1gNnSc1	BAeRusko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
MiG-	MiG-	k?	MiG-
<g/>
35	[number]	k4	35
–	–	k?	–
OKB	OKB	kA	OKB
MiG	mig	k1gInSc1	mig
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
37	[number]	k4	37
–	–	k?	–
OKB	OKB	kA	OKB
Suchoj	Suchoj	k1gInSc1	Suchoj
</s>
</p>
<p>
<s>
Suchoj	Suchoj	k1gInSc1	Suchoj
T-50	T-50	k1gFnSc2	T-50
–	–	k?	–
OKB	OKB	kA	OKB
SuchojČína	SuchojČína	k1gFnSc1	SuchojČína
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chengdu	Chengdu	k6eAd1	Chengdu
J-10	J-10	k1gMnSc4	J-10
–	–	k?	–
Chengdu	Chengda	k1gMnSc4	Chengda
Aircraft	Aircraft	k1gMnSc1	Aircraft
Industry	Industra	k1gFnSc2	Industra
Group	Group	k1gMnSc1	Group
</s>
</p>
<p>
<s>
Chengdu	Chengdu	k6eAd1	Chengdu
J-20	J-20	k1gMnSc4	J-20
–	–	k?	–
Chengdu	Chengda	k1gMnSc4	Chengda
Aircraft	Aircraft	k1gMnSc1	Aircraft
Industry	Industra	k1gFnSc2	Industra
Group	Group	k1gMnSc1	Group
</s>
</p>
<p>
<s>
Shenyang	Shenyang	k1gMnSc1	Shenyang
J-31	J-31	k1gMnSc1	J-31
–	–	k?	–
Shenyang	Shenyang	k1gMnSc1	Shenyang
Aircraft	Aircraft	k2eAgMnSc1d1	Aircraft
CorporationIndie	CorporationIndie	k1gFnPc4	CorporationIndie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
HAL	hala	k1gFnPc2	hala
Tejas	Tejas	k1gMnSc1	Tejas
–	–	k?	–
Hindustan	Hindustan	k1gInSc1	Hindustan
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
Limited	limited	k2eAgFnPc2d1	limited
(	(	kIx(	(
<g/>
HAL	hala	k1gFnPc2	hala
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stíhací	stíhací	k2eAgInSc4d1	stíhací
letoun	letoun	k1gInSc4	letoun
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
