<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
biopolymery	biopolymer	k1gMnPc7	biopolymer
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vysokomolekulární	vysokomolekulární	k2eAgFnPc4d1	vysokomolekulární
přírodní	přírodní	k2eAgFnPc4d1	přírodní
látky	látka	k1gFnPc4	látka
s	s	k7c7	s
relativní	relativní	k2eAgFnSc7d1	relativní
molekulovou	molekulový	k2eAgFnSc7d1	molekulová
hmotností	hmotnost	k1gFnSc7	hmotnost
103	[number]	k4	103
až	až	k9	až
106	[number]	k4	106
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Proteiny	protein	k1gInPc1	protein
jsou	být	k5eAaImIp3nP	být
podstatou	podstata	k1gFnSc7	podstata
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
základní	základní	k2eAgFnSc4d1	základní
povahu	povaha	k1gFnSc4	povaha
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
Braconnot	Braconnota	k1gFnPc2	Braconnota
již	již	k6eAd1	již
v	v	k7c6	v
r.	r.	kA	r.
1819	[number]	k4	1819
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
klihu	klih	k1gInSc2	klih
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
podrobnější	podrobný	k2eAgFnSc4d2	podrobnější
znalost	znalost	k1gFnSc4	znalost
struktury	struktura	k1gFnSc2	struktura
bílkovin	bílkovina	k1gFnPc2	bílkovina
vděčíme	vděčit	k5eAaImIp1nP	vděčit
E.	E.	kA	E.
Fischerovi	Fischerův	k2eAgMnPc1d1	Fischerův
a	a	k8xC	a
L.	L.	kA	L.
Paulingovi	Pauling	k1gMnSc6	Pauling
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
proteinech	protein	k1gInPc6	protein
jsou	být	k5eAaImIp3nP	být
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
vzájemně	vzájemně	k6eAd1	vzájemně
vázány	vázat	k5eAaImNgFnP	vázat
aminoskupinami	aminoskupina	k1gFnPc7	aminoskupina
–	–	k?	–
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
a	a	k8xC	a
karboxylovými	karboxylový	k2eAgFnPc7d1	karboxylová
skupinami	skupina	k1gFnPc7	skupina
–	–	k?	–
<g/>
COOH	COOH	kA	COOH
amidovou	amidový	k2eAgFnSc7d1	amidový
vazbou	vazba	k1gFnSc7	vazba
–	–	k?	–
<g/>
NH	NH	kA	NH
<g/>
–	–	k?	–
<g/>
CO	co	k8xS	co
<g/>
–	–	k?	–
(	(	kIx(	(
<g/>
amidy	amid	k1gInPc4	amid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
proteinů	protein	k1gInPc2	protein
nazývá	nazývat	k5eAaImIp3nS	nazývat
peptidová	peptidový	k2eAgFnSc1d1	peptidová
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
takto	takto	k6eAd1	takto
navázány	navázán	k2eAgFnPc1d1	navázána
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
oligopeptidy	oligopeptida	k1gFnPc1	oligopeptida
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
)	)	kIx)	)
polypeptidy	polypeptid	k1gInPc1	polypeptid
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
)	)	kIx)	)
vlastní	vlastní	k2eAgFnSc2d1	vlastní
bílkoviny	bílkovina	k1gFnSc2	bílkovina
-	-	kIx~	-
proteiny	protein	k1gInPc1	protein
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
peptidy	peptid	k1gInPc7	peptid
a	a	k8xC	a
bílkovinami	bílkovina	k1gFnPc7	bílkovina
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
platilo	platit	k5eAaImAgNnS	platit
<g/>
:	:	kIx,	:
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
50	[number]	k4	50
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
peptid	peptid	k1gInSc4	peptid
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšším	vysoký	k2eAgInSc6d2	vyšší
počtu	počet	k1gInSc6	počet
pak	pak	k6eAd1	pak
o	o	k7c4	o
bílkovinu	bílkovina	k1gFnSc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
posuzována	posuzován	k2eAgFnSc1d1	posuzována
poměrná	poměrný	k2eAgFnSc1d1	poměrná
molekulová	molekulový	k2eAgFnSc1d1	molekulová
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
hodnoty	hodnota	k1gFnSc2	hodnota
Mr	Mr	k1gFnSc2	Mr
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
000	[number]	k4	000
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
peptid	peptid	k1gInSc4	peptid
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
bílkovinu	bílkovina	k1gFnSc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
aminokyselinám	aminokyselina	k1gFnPc3	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc4	pořadí
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
proteinu	protein	k1gInSc2	protein
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
primární	primární	k2eAgFnSc4d1	primární
strukturu	struktura	k1gFnSc4	struktura
nebo	nebo	k8xC	nebo
také	také	k9	také
sekvenci	sekvence	k1gFnSc4	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
20	[number]	k4	20
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
22	[number]	k4	22
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
proteinogenní	proteinogenní	k2eAgFnPc1d1	proteinogenní
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
proteinu	protein	k1gInSc2	protein
<g/>
,	,	kIx,	,
složeného	složený	k2eAgMnSc4d1	složený
ze	z	k7c2	z
100	[number]	k4	100
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
vzniknout	vzniknout	k5eAaPmF	vzniknout
22100	[number]	k4	22100
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
asi	asi	k9	asi
1,747	[number]	k4	1,747
<g/>
×	×	k?	×
<g/>
10134	[number]	k4	10134
)	)	kIx)	)
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
primárních	primární	k2eAgFnPc2d1	primární
proteinových	proteinový	k2eAgFnPc2d1	proteinová
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
mnoha	mnoho	k4c2	mnoho
proteinů	protein	k1gInPc2	protein
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
např.	např.	kA	např.
myoglobinu	myoglobina	k1gFnSc4	myoglobina
a	a	k8xC	a
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
;	;	kIx,	;
u	u	k7c2	u
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
struktury	struktura	k1gFnPc4	struktura
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
proteinů	protein	k1gInPc2	protein
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
protáhlé	protáhlý	k2eAgFnPc1d1	protáhlá
<g/>
,	,	kIx,	,
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustné	rozpustný	k2eNgFnSc2d1	nerozpustná
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
skleroproteiny	skleroproteina	k1gFnSc2	skleroproteina
(	(	kIx(	(
<g/>
též	též	k9	též
fibrilární	fibrilární	k2eAgFnSc1d1	fibrilární
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
kulovité	kulovitý	k2eAgNnSc1d1	kulovité
nebo	nebo	k8xC	nebo
elipsoidní	elipsoidní	k2eAgNnSc1d1	elipsoidní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
sferoproteiny	sferoproteina	k1gFnSc2	sferoproteina
(	(	kIx(	(
<g/>
též	též	k9	též
globulární	globulární	k2eAgFnSc1d1	globulární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
ke	k	k7c3	k
skleroproteinům	skleroproteina	k1gMnPc3	skleroproteina
(	(	kIx(	(
<g/>
kolagen	kolagen	k1gInSc1	kolagen
<g/>
,	,	kIx,	,
keratin	keratin	k1gInSc1	keratin
<g/>
,	,	kIx,	,
fibroin	fibroin	k1gMnSc1	fibroin
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgInPc4d1	tvořící
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
rohovinu	rohovina	k1gFnSc4	rohovina
<g/>
,	,	kIx,	,
chrupavky	chrupavka	k1gFnPc4	chrupavka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
skoro	skoro	k6eAd1	skoro
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
sferoproteinů	sferoprotein	k1gInPc2	sferoprotein
(	(	kIx(	(
<g/>
např.	např.	kA	např.
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
svalová	svalový	k2eAgFnSc1d1	svalová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
)	)	kIx)	)
varem	var	k1gInSc7	var
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
louhů	louh	k1gInPc2	louh
(	(	kIx(	(
<g/>
změnou	změna	k1gFnSc7	změna
hodnoty	hodnota	k1gFnSc2	hodnota
pH	ph	kA	ph
<g/>
)	)	kIx)	)
rozrušit	rozrušit	k5eAaPmF	rozrušit
jejich	jejich	k3xOp3gFnSc4	jejich
terciární	terciární	k2eAgFnSc4d1	terciární
a	a	k8xC	a
sekundární	sekundární	k2eAgFnSc4d1	sekundární
strukturu	struktura	k1gFnSc4	struktura
(	(	kIx(	(
<g/>
srážení	srážení	k1gNnSc1	srážení
<g/>
,	,	kIx,	,
denaturace	denaturace	k1gFnSc1	denaturace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
některé	některý	k3yIgFnPc1	některý
biologické	biologický	k2eAgFnPc1d1	biologická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
např.	např.	kA	např.
schopnost	schopnost	k1gFnSc4	schopnost
enzymů	enzym	k1gInPc2	enzym
štěpit	štěpit	k5eAaImF	štěpit
potravu	potrava	k1gFnSc4	potrava
nebo	nebo	k8xC	nebo
svalovou	svalový	k2eAgFnSc4d1	svalová
kontraktivitu	kontraktivita	k1gFnSc4	kontraktivita
<g/>
.	.	kIx.	.
</s>
<s>
Tělu	tělo	k1gNnSc3	tělo
cizí	cizí	k2eAgInPc1d1	cizí
proteiny	protein	k1gInPc1	protein
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
svou	svůj	k3xOyFgFnSc7	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
reakci	reakce	k1gFnSc3	reakce
antigen	antigen	k1gInSc1	antigen
<g/>
–	–	k?	–
<g/>
protilátka	protilátka	k1gFnSc1	protilátka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
přímo	přímo	k6eAd1	přímo
vpraveny	vpravit	k5eAaPmNgFnP	vpravit
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
primární	primární	k2eAgFnSc4d1	primární
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgFnSc4d1	sekundární
<g/>
,	,	kIx,	,
terciární	terciární	k2eAgFnSc4d1	terciární
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
složitějších	složitý	k2eAgInPc2d2	složitější
proteinů	protein	k1gInPc2	protein
ještě	ještě	k6eAd1	ještě
kvartérní	kvartérní	k2eAgFnSc4d1	kvartérní
strukturu	struktura	k1gFnSc4	struktura
bílkovinových	bílkovinův	k2eAgInPc2d1	bílkovinův
řetězců	řetězec	k1gInPc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
pořadím	pořadí	k1gNnSc7	pořadí
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
v	v	k7c6	v
polypeptidovém	polypeptidový	k2eAgInSc6d1	polypeptidový
řetězci	řetězec	k1gInSc6	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
od	od	k7c2	od
N-konce	Nonec	k1gMnSc2	N-konec
k	k	k7c3	k
C-konci	Conec	k1gInSc3	C-konec
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
určení	určení	k1gNnSc1	určení
primární	primární	k2eAgFnSc2d1	primární
struktury	struktura	k1gFnSc2	struktura
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Frederick	Frederick	k1gMnSc1	Frederick
Sanger	Sanger	k1gMnSc1	Sanger
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
struktura	struktura	k1gFnSc1	struktura
udává	udávat	k5eAaImIp3nS	udávat
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
bílkoviny	bílkovina	k1gFnSc2	bílkovina
a	a	k8xC	a
také	také	k9	také
determinuje	determinovat	k5eAaBmIp3nS	determinovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
skládání	skládání	k1gNnSc3	skládání
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
geometrické	geometrický	k2eAgNnSc4d1	geometrické
uspořádání	uspořádání	k1gNnSc4	uspořádání
polypeptidového	polypeptidový	k2eAgInSc2d1	polypeptidový
řetězce	řetězec	k1gInSc2	řetězec
"	"	kIx"	"
<g/>
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
mezi	mezi	k7c4	mezi
několika	několik	k4yIc2	několik
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucími	jdoucí	k2eAgFnPc7d1	jdoucí
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
studie	studie	k1gFnPc1	studie
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
struktur	struktura	k1gFnPc2	struktura
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozpoznávány	rozpoznáván	k2eAgInPc4d1	rozpoznáván
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
těchto	tento	k3xDgInPc2	tento
stavebních	stavební	k2eAgInPc2d1	stavební
motivů	motiv	k1gInPc2	motiv
<g/>
:	:	kIx,	:
alfa	alfa	k1gNnSc1	alfa
šroubovice	šroubovice	k1gFnSc2	šroubovice
(	(	kIx(	(
<g/>
alfa-helix	alfaelix	k1gInSc1	alfa-helix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
skládaného	skládaný	k2eAgInSc2d1	skládaný
listu	list	k1gInSc2	list
(	(	kIx(	(
<g/>
beta-sheet	betaheet	k1gInSc1	beta-sheet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neuspořádaná	uspořádaný	k2eNgFnSc1d1	neuspořádaná
struktura	struktura	k1gFnSc1	struktura
(	(	kIx(	(
<g/>
random	random	k1gInSc1	random
coil	coil	k1gInSc1	coil
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
i	i	k9	i
"	"	kIx"	"
<g/>
strukturní	strukturní	k2eAgInSc4d1	strukturní
motiv	motiv	k1gInSc4	motiv
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
supersekundární	supersekundární	k2eAgFnSc1d1	supersekundární
struktura	struktura	k1gFnSc1	struktura
jako	jako	k8xC	jako
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
sekundární	sekundární	k2eAgFnSc7d1	sekundární
a	a	k8xC	a
terciární	terciární	k2eAgFnSc7d1	terciární
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
uspořádání	uspořádání	k1gNnSc2	uspořádání
několika	několik	k4yIc2	několik
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
řecký	řecký	k2eAgInSc4d1	řecký
klíč	klíč	k1gInSc4	klíč
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
několika	několik	k4yIc2	několik
beta	beta	k1gNnPc2	beta
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
beta-vlásenka	betalásenka	k1gFnSc1	beta-vlásenka
(	(	kIx(	(
<g/>
beta-hairpin	betaairpin	k1gInSc1	beta-hairpin
<g/>
)	)	kIx)	)
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
beta	beta	k1gNnPc2	beta
listů	list	k1gInPc2	list
a	a	k8xC	a
otočky	otočka	k1gFnSc2	otočka
nebo	nebo	k8xC	nebo
helix-smyčka-helix	helixmyčkaelix	k1gInSc1	helix-smyčka-helix
tvořící	tvořící	k2eAgInSc4d1	tvořící
základ	základ	k1gInSc4	základ
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
rodin	rodina	k1gFnPc2	rodina
transkripčních	transkripční	k2eAgInPc2d1	transkripční
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
basic	basice	k1gFnPc2	basice
helix-loop-helix	helixoopelix	k1gInSc1	helix-loop-helix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
trojrozměrné	trojrozměrný	k2eAgNnSc1d1	trojrozměrné
uspořádání	uspořádání	k1gNnSc1	uspořádání
celého	celý	k2eAgInSc2d1	celý
peptidového	peptidový	k2eAgInSc2d1	peptidový
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
střídáním	střídání	k1gNnSc7	střídání
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
strukturu	struktura	k1gFnSc4	struktura
globulární	globulární	k2eAgInPc1d1	globulární
(	(	kIx(	(
<g/>
albumin	albumin	k1gInSc1	albumin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
klubka	klubko	k1gNnSc2	klubko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
fibrilární	fibrilární	k2eAgInSc4d1	fibrilární
(	(	kIx(	(
<g/>
myosin	myosin	k1gInSc4	myosin
<g/>
)	)	kIx)	)
vláknitou	vláknitý	k2eAgFnSc4d1	vláknitá
strukturu	struktura	k1gFnSc4	struktura
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustnou	rozpustný	k2eNgFnSc7d1	nerozpustná
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
stabilizována	stabilizovat	k5eAaBmNgFnS	stabilizovat
kovalentními	kovalentní	k2eAgFnPc7d1	kovalentní
vazbami	vazba	k1gFnPc7	vazba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
vazba	vazba	k1gFnSc1	vazba
S-S	S-S	k1gFnSc2	S-S
tzv.	tzv.	kA	tzv.
disulfidový	disulfidový	k2eAgInSc4d1	disulfidový
můstek	můstek	k1gInSc4	můstek
<g/>
)	)	kIx)	)
v	v	k7c6	v
postranních	postranní	k2eAgInPc6d1	postranní
řetězcích	řetězec	k1gInPc6	řetězec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nS	řešit
uspořádání	uspořádání	k1gNnSc1	uspořádání
podjednotek	podjednotka	k1gFnPc2	podjednotka
v	v	k7c6	v
proteinových	proteinový	k2eAgInPc6d1	proteinový
aglomerátech	aglomerát	k1gInPc6	aglomerát
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgInPc2d1	tvořící
jednu	jeden	k4xCgFnSc4	jeden
funkční	funkční	k2eAgFnSc4d1	funkční
bílkovinu	bílkovina	k1gFnSc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Podjednotky	podjednotka	k1gFnPc1	podjednotka
jsou	být	k5eAaImIp3nP	být
samostatné	samostatný	k2eAgFnPc1d1	samostatná
polypeptidické	polypeptidický	k2eAgFnPc1d1	polypeptidický
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
spojeny	spojit	k5eAaPmNgInP	spojit
nekovalentními	kovalentní	k2eNgFnPc7d1	nekovalentní
interakcemi	interakce	k1gFnPc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Kvartérní	kvartérní	k2eAgFnSc1d1	kvartérní
struktura	struktura	k1gFnSc1	struktura
též	též	k9	též
řeší	řešit	k5eAaImIp3nS	řešit
prostorové	prostorový	k2eAgNnSc4d1	prostorové
uspořádání	uspořádání	k1gNnSc4	uspořádání
těchto	tento	k3xDgFnPc2	tento
podjednotek	podjednotka	k1gFnPc2	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc4	takovýto
uspořádání	uspořádání	k1gNnSc4	uspořádání
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
jen	jen	k9	jen
složitější	složitý	k2eAgInPc4d2	složitější
komplexy	komplex	k1gInPc4	komplex
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
fibrily	fibrila	k1gFnSc2	fibrila
kolagenu	kolagen	k1gInSc2	kolagen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lidské	lidský	k2eAgFnSc2d1	lidská
DNA	DNA	kA	DNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
podjednotky	podjednotka	k1gFnPc4	podjednotka
přináší	přinášet	k5eAaImIp3nS	přinášet
mnohé	mnohý	k2eAgFnPc4d1	mnohá
evoluční	evoluční	k2eAgFnPc4d1	evoluční
výhody	výhoda	k1gFnPc4	výhoda
oproti	oproti	k7c3	oproti
existenci	existence	k1gFnSc3	existence
jednoho	jeden	k4xCgInSc2	jeden
ohromného	ohromný	k2eAgInSc2d1	ohromný
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výskytu	výskyt	k1gInSc6	výskyt
poruchy	porucha	k1gFnSc2	porucha
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
stačí	stačit	k5eAaBmIp3nS	stačit
nahradit	nahradit	k5eAaPmF	nahradit
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
podjednotku	podjednotka	k1gFnSc4	podjednotka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgFnSc3d1	podobná
stavbě	stavba	k1gFnSc3	stavba
budov	budova	k1gFnPc2	budova
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
prefabrikátů	prefabrikát	k1gInPc2	prefabrikát
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
výstavby	výstavba	k1gFnSc2	výstavba
podjednotky	podjednotka	k1gFnSc2	podjednotka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
navíc	navíc	k6eAd1	navíc
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
podjednotek	podjednotka	k1gFnPc2	podjednotka
buď	buď	k8xC	buď
odlišných	odlišný	k2eAgInPc2d1	odlišný
(	(	kIx(	(
<g/>
heteromultimery	heteromultimera	k1gFnPc1	heteromultimera
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
shodných	shodný	k2eAgInPc2d1	shodný
(	(	kIx(	(
<g/>
homomultimery	homomultimera	k1gFnPc1	homomultimera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnSc3	oblast
styku	styk	k1gInSc2	styk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
podjednotek	podjednotka	k1gFnPc2	podjednotka
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
slabými	slabý	k2eAgFnPc7d1	slabá
vazbami	vazba	k1gFnPc7	vazba
(	(	kIx(	(
<g/>
nekovalentními	kovalentní	k2eNgFnPc7d1	nekovalentní
interakcemi	interakce	k1gFnPc7	interakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
vodíkovými	vodíkový	k2eAgInPc7d1	vodíkový
můstky	můstek	k1gInPc7	můstek
nebo	nebo	k8xC	nebo
hydrofóbním	hydrofóbní	k2eAgInSc7d1	hydrofóbní
efektem	efekt	k1gInSc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
většinou	většina	k1gFnSc7	většina
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
spoje	spoj	k1gFnSc2	spoj
prvky	prvek	k1gInPc1	prvek
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
symetrií	symetrie	k1gFnSc7	symetrie
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
symetrie	symetrie	k1gFnSc1	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
Cn	Cn	k1gFnSc7	Cn
<g/>
,	,	kIx,	,
kde	kde	k9	kde
n	n	k0	n
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
protomerů	protomer	k1gInPc2	protomer
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
protomery	protomera	k1gFnPc1	protomera
spolu	spolu	k6eAd1	spolu
svírají	svírat	k5eAaImIp3nP	svírat
úhel	úhel	k1gInSc4	úhel
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
/	/	kIx~	/
<g/>
n.	n.	k?	n.
Nejobvyklejší	obvyklý	k2eAgFnSc1d3	nejobvyklejší
je	být	k5eAaImIp3nS	být
C2	C2	k1gFnSc1	C2
symetrie	symetrie	k1gFnSc1	symetrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
podjednotky	podjednotka	k1gFnPc4	podjednotka
přímo	přímo	k6eAd1	přímo
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
cyklické	cyklický	k2eAgFnPc1d1	cyklická
symetrie	symetrie	k1gFnPc1	symetrie
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgFnSc1d2	složitější
symetrie	symetrie	k1gFnSc1	symetrie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diedrální	diedrální	k2eAgFnSc1d1	diedrální
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
Dn	Dn	k1gMnSc2	Dn
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
dimery	dimer	k1gInPc1	dimer
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tvořeny	tvořit	k5eAaImNgInP	tvořit
dvěma	dva	k4xCgFnPc7	dva
cyklicky	cyklicky	k6eAd1	cyklicky
symetrickými	symetrický	k2eAgFnPc7d1	symetrická
polovinami	polovina	k1gFnPc7	polovina
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
symetrie	symetrie	k1gFnSc2	symetrie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
spojnice	spojnice	k1gFnPc1	spojnice
středů	střed	k1gInPc2	střed
symetrie	symetrie	k1gFnSc1	symetrie
obou	dva	k4xCgFnPc2	dva
cyklicky	cyklicky	k6eAd1	cyklicky
symetrických	symetrický	k2eAgFnPc2d1	symetrická
polovin	polovina	k1gFnPc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Polypeptidy	polypeptid	k1gInPc1	polypeptid
s	s	k7c7	s
takovýmto	takovýto	k3xDgNnSc7	takovýto
uspořádáním	uspořádání	k1gNnSc7	uspořádání
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
disociovatelné	disociovatelný	k2eAgFnPc1d1	disociovatelný
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
cyklicky	cyklicky	k6eAd1	cyklicky
symetrické	symetrický	k2eAgInPc4d1	symetrický
oligomery	oligomer	k1gInPc4	oligomer
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
další	další	k2eAgFnSc3d1	další
disociaci	disociace	k1gFnSc3	disociace
na	na	k7c4	na
protomery	protomera	k1gFnPc4	protomera
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
obvykle	obvykle	k6eAd1	obvykle
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
dosažení	dosažení	k1gNnSc3	dosažení
by	by	kYmCp3nS	by
už	už	k9	už
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
značně	značně	k6eAd1	značně
drastických	drastický	k2eAgFnPc2d1	drastická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
plní	plnit	k5eAaImIp3nP	plnit
různé	různý	k2eAgFnPc4d1	různá
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
(	(	kIx(	(
<g/>
kolagen	kolagen	k1gInSc1	kolagen
<g/>
,	,	kIx,	,
elastin	elastin	k1gInSc1	elastin
<g/>
,	,	kIx,	,
keratin	keratin	k1gInSc1	keratin
<g/>
)	)	kIx)	)
Transportní	transportní	k2eAgInSc1d1	transportní
a	a	k8xC	a
skladovací	skladovací	k2eAgInSc1d1	skladovací
(	(	kIx(	(
<g/>
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
<g/>
,	,	kIx,	,
transferin	transferin	k1gInSc1	transferin
<g/>
)	)	kIx)	)
Zajišťující	zajišťující	k2eAgInSc1d1	zajišťující
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
aktin	aktin	k1gMnSc1	aktin
<g/>
,	,	kIx,	,
myosin	myosin	k2eAgMnSc1d1	myosin
<g/>
)	)	kIx)	)
Katalytické	katalytický	k2eAgInPc4d1	katalytický
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInPc4d1	řídící
a	a	k8xC	a
regulační	regulační	k2eAgInPc4d1	regulační
(	(	kIx(	(
<g/>
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
hormony	hormon	k1gInPc4	hormon
<g/>
,	,	kIx,	,
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
a	a	k8xC	a
obranné	obranný	k2eAgNnSc1d1	obranné
(	(	kIx(	(
<g/>
imunoglobulin	imunoglobulin	k2eAgInSc4d1	imunoglobulin
<g/>
,	,	kIx,	,
fibrin	fibrin	k1gInSc4	fibrin
<g/>
,	,	kIx,	,
fibrinogen	fibrinogen	k1gInSc4	fibrinogen
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
<g/>
#	#	kIx~	#
<g/>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
proteinech	protein	k1gInPc6	protein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
proteinech	protein	k1gInPc6	protein
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
20	[number]	k4	20
kódovaných	kódovaný	k2eAgFnPc2d1	kódovaná
proteinogenních	proteinogenní	k2eAgFnPc2d1	proteinogenní
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
organismy	organismus	k1gInPc1	organismus
schopné	schopný	k2eAgInPc1d1	schopný
zainkorporovat	zainkorporovat	k5eAaPmF	zainkorporovat
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
speciálními	speciální	k2eAgInPc7d1	speciální
mechanismy	mechanismus	k1gInPc7	mechanismus
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
21	[number]	k4	21
<g/>
.	.	kIx.	.
proteinogenní	proteinogenní	k2eAgFnSc1d1	proteinogenní
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
selenocystein	selenocystein	k2eAgInSc1d1	selenocystein
(	(	kIx(	(
<g/>
Sec	sec	k1gInSc1	sec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
cystein	cystein	k1gInSc4	cystein
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
enzymu	enzym	k1gInSc6	enzym
glutathionperoxidáze	glutathionperoxidáze	k1gFnSc2	glutathionperoxidáze
a	a	k8xC	a
v	v	k7c6	v
enzymech	enzym	k1gInPc6	enzym
některých	některý	k3yIgNnPc2	některý
bakterií	bakterium	k1gNnPc2	bakterium
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
22	[number]	k4	22
<g/>
.	.	kIx.	.
proteinogenní	proteinogenní	k2eAgFnSc1d1	proteinogenní
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pyrolysin	pyrolysin	k1gInSc1	pyrolysin
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakteriích	bakterie	k1gFnPc6	bakterie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
plastidů	plastid	k1gInPc2	plastid
a	a	k8xC	a
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
N-formylmethionin	Normylmethionin	k1gInSc1	N-formylmethionin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ke	k	k7c3	k
důležitý	důležitý	k2eAgInSc4d1	důležitý
pro	pro	k7c4	pro
iniciaci	iniciace	k1gFnSc4	iniciace
translace	translace	k1gFnSc2	translace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
aminokyselinou	aminokyselina	k1gFnSc7	aminokyselina
zařazenou	zařazený	k2eAgFnSc7d1	zařazená
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
21	[number]	k4	21
proteinogenních	proteinogenní	k2eAgFnPc2d1	proteinogenní
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
standardních	standardní	k2eAgInPc2d1	standardní
20	[number]	k4	20
+	+	kIx~	+
selenocystein	selenocystein	k1gInSc1	selenocystein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
organismus	organismus	k1gInSc4	organismus
neumí	umět	k5eNaImIp3nS	umět
sám	sám	k3xTgMnSc1	sám
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
je	on	k3xPp3gNnPc4	on
přijímat	přijímat	k5eAaImF	přijímat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
esenciální	esenciální	k2eAgFnPc1d1	esenciální
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
důkaz	důkaz	k1gInSc4	důkaz
bílkovin	bílkovina	k1gFnPc2	bílkovina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgFnPc1d1	následující
reakce	reakce	k1gFnPc1	reakce
<g/>
:	:	kIx,	:
xantoproteinová	xantoproteinový	k2eAgFnSc1d1	xantoproteinový
reakce	reakce	k1gFnSc1	reakce
biuretová	biuretový	k2eAgFnSc1d1	biuretový
reakce	reakce	k1gFnSc1	reakce
Metabolismus	metabolismus	k1gInSc1	metabolismus
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc1	souhrn
různých	různý	k2eAgInPc2d1	různý
biochemických	biochemický	k2eAgInPc2d1	biochemický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
syntetizovány	syntetizován	k2eAgFnPc4d1	syntetizována
a	a	k8xC	a
rozkládány	rozkládán	k2eAgFnPc4d1	rozkládána
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bílkoviny	bílkovina	k1gFnSc2	bílkovina
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
translace	translace	k1gFnSc2	translace
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc2	biologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
částicí	částice	k1gFnSc7	částice
bílkovin	bílkovina	k1gFnPc2	bílkovina
jsou	být	k5eAaImIp3nP	být
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
tzv.	tzv.	kA	tzv.
proteosyntéza	proteosyntéza	k1gFnSc1	proteosyntéza
neobejde	obejde	k6eNd1	obejde
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
je	on	k3xPp3gNnSc4	on
schopné	schopný	k2eAgNnSc4d1	schopné
tělo	tělo	k1gNnSc4	tělo
vyrábět	vyrábět	k5eAaImF	vyrábět
samo	sám	k3xTgNnSc1	sám
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
musí	muset	k5eAaImIp3nS	muset
přijímat	přijímat	k5eAaImF	přijímat
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
(	(	kIx(	(
<g/>
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
tzv.	tzv.	kA	tzv.
esenciálním	esenciální	k2eAgFnPc3d1	esenciální
aminokyselinám	aminokyselina	k1gFnPc3	aminokyselina
patří	patřit	k5eAaImIp3nS	patřit
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
12	[number]	k4	12
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
kódovány	kódován	k2eAgInPc1d1	kódován
v	v	k7c6	v
specifických	specifický	k2eAgInPc6d1	specifický
úsecích	úsek	k1gInPc6	úsek
v	v	k7c6	v
DNA	DNA	kA	DNA
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
úseky	úsek	k1gInPc1	úsek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
geny	gen	k1gInPc1	gen
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přepisovány	přepisovat	k5eAaImNgFnP	přepisovat
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
transkripce	transkripce	k1gFnSc2	transkripce
do	do	k7c2	do
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
na	na	k7c6	na
ribozomu	ribozom	k1gInSc6	ribozom
následně	následně	k6eAd1	následně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
translaci	translace	k1gFnSc4	translace
<g/>
)	)	kIx)	)
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
této	tento	k3xDgFnSc2	tento
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
napojených	napojený	k2eAgFnPc2d1	napojená
na	na	k7c4	na
specifické	specifický	k2eAgFnPc4d1	specifická
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
některé	některý	k3yIgInPc4	některý
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgInPc4d1	krátký
polypeptidy	polypeptid	k1gInPc4	polypeptid
nevznikají	vznikat	k5eNaImIp3nP	vznikat
podle	podle	k7c2	podle
mRNA	mRNA	k?	mRNA
na	na	k7c6	na
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
syntézou	syntéza	k1gFnSc7	syntéza
pomocí	pomocí	k7c2	pomocí
složitých	složitý	k2eAgInPc2d1	složitý
komplexů	komplex	k1gInPc2	komplex
enzymů	enzym	k1gInPc2	enzym
(	(	kIx(	(
<g/>
NonRibosomal	NonRibosomal	k1gInSc1	NonRibosomal
Peptide	peptid	k1gInSc5	peptid
Synthetase	Synthetas	k1gMnSc5	Synthetas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
neribozomální	ribozomální	k2eNgInPc1d1	ribozomální
peptidy	peptid	k1gInPc1	peptid
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
některá	některý	k3yIgNnPc1	některý
polypeptidová	polypeptidový	k2eAgNnPc1d1	polypeptidový
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
produkce	produkce	k1gFnSc1	produkce
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
zablokujeme	zablokovat	k5eAaPmIp1nP	zablokovat
<g/>
-li	i	k?	-li
ribozomální	ribozomální	k2eAgFnSc4d1	ribozomální
mašinérii	mašinérie	k1gFnSc4	mašinérie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
polypeptidům	polypeptid	k1gInPc3	polypeptid
patří	patřit	k5eAaImIp3nS	patřit
chloramfenikol	chloramfenikol	k1gInSc1	chloramfenikol
a	a	k8xC	a
graminicin	graminicin	k2eAgInSc1d1	graminicin
S.	S.	kA	S.
Mechanismus	mechanismus	k1gInSc1	mechanismus
syntézy	syntéza	k1gFnSc2	syntéza
těchto	tento	k3xDgNnPc2	tento
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
podobný	podobný	k2eAgInSc1d1	podobný
syntéze	syntéza	k1gFnSc3	syntéza
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
proteinů	protein	k1gInPc2	protein
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
proteolýza	proteolýza	k1gFnSc1	proteolýza
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
se	se	k3xPyFc4	se
tráví	trávit	k5eAaImIp3nP	trávit
na	na	k7c4	na
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
,	,	kIx,	,
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
určitá	určitý	k2eAgFnSc1d1	určitá
stálá	stálý	k2eAgFnSc1d1	stálá
hladina	hladina	k1gFnSc1	hladina
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
jsou	být	k5eAaImIp3nP	být
jednak	jednak	k8xC	jednak
bílkoviny	bílkovina	k1gFnPc1	bílkovina
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
opotřebované	opotřebovaný	k2eAgFnPc1d1	opotřebovaná
bílkoviny	bílkovina	k1gFnPc1	bílkovina
z	z	k7c2	z
tkání	tkáň	k1gFnPc2	tkáň
<g/>
;	;	kIx,	;
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
přeměně	přeměna	k1gFnSc6	přeměna
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
potřebné	potřebný	k2eAgNnSc4d1	potřebné
<g/>
:	:	kIx,	:
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
stavebních	stavební	k2eAgFnPc2d1	stavební
bílkovin	bílkovina	k1gFnPc2	bílkovina
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
plazmatických	plazmatický	k2eAgFnPc2d1	plazmatická
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
na	na	k7c4	na
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
se	se	k3xPyFc4	se
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
se	se	k3xPyFc4	se
neukládají	ukládat	k5eNaImIp3nP	ukládat
do	do	k7c2	do
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
katabolickém	katabolický	k2eAgNnSc6d1	katabolické
odbourání	odbourání	k1gNnSc6	odbourání
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
dochází	docházet	k5eAaImIp3nS	docházet
nejdříve	dříve	k6eAd3	dříve
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
deaminaci	deaminace	k1gFnSc3	deaminace
<g/>
.	.	kIx.	.
</s>
<s>
Aminové	aminový	k2eAgFnPc1d1	aminová
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
toxického	toxický	k2eAgInSc2d1	toxický
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jaterních	jaterní	k2eAgFnPc6d1	jaterní
buňkách	buňka	k1gFnPc6	buňka
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
ornitinovém	ornitinový	k2eAgNnSc6d1	ornitinový
cyklu	cyklus	k1gInSc2	cyklus
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
močovinu	močovina	k1gFnSc4	močovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
krví	krev	k1gFnSc7	krev
zanesena	zanést	k5eAaPmNgFnS	zanést
do	do	k7c2	do
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
močí	moč	k1gFnSc7	moč
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkaté	uhlíkatý	k2eAgInPc1d1	uhlíkatý
zbytky	zbytek	k1gInPc1	zbytek
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
se	se	k3xPyFc4	se
začleňují	začleňovat	k5eAaImIp3nP	začleňovat
do	do	k7c2	do
Krebsova	Krebsův	k2eAgInSc2d1	Krebsův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dekarboxylovány	dekarboxylován	k2eAgFnPc1d1	dekarboxylován
a	a	k8xC	a
dehydrogenovány	dehydrogenován	k2eAgFnPc1d1	dehydrogenován
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
1	[number]	k4	1
molekula	molekula	k1gFnSc1	molekula
močoviny	močovina	k1gFnSc2	močovina
=	=	kIx~	=
3	[number]	k4	3
ATP	atp	kA	atp
=	=	kIx~	=
ornitinový	ornitinový	k2eAgInSc1d1	ornitinový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
enzymy	enzym	k1gInPc4	enzym
štěpící	štěpící	k2eAgFnSc2d1	štěpící
bílkoviny	bílkovina	k1gFnSc2	bílkovina
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
pepsin	pepsin	k1gInSc1	pepsin
(	(	kIx(	(
<g/>
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
enzymů	enzym	k1gInPc2	enzym
zvaná	zvaný	k2eAgFnSc1d1	zvaná
erepsin	erepsin	k1gInSc1	erepsin
(	(	kIx(	(
<g/>
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
trypsin	trypsin	k1gInSc4	trypsin
a	a	k8xC	a
chymotrypsin	chymotrypsin	k1gInSc4	chymotrypsin
(	(	kIx(	(
<g/>
produkované	produkovaný	k2eAgInPc4d1	produkovaný
slinivkou	slinivka	k1gFnSc7	slinivka
břišní	břišní	k2eAgFnSc7d1	břišní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c4	mezi
hormony	hormon	k1gInPc4	hormon
řídící	řídící	k2eAgNnSc4d1	řídící
štěpení	štěpení	k1gNnSc4	štěpení
bílkovin	bílkovina	k1gFnPc2	bílkovina
patří	patřit	k5eAaImIp3nP	patřit
glukokortikoidy	glukokortikoid	k1gInPc1	glukokortikoid
<g/>
,	,	kIx,	,
somatotropin	somatotropin	k1gInSc1	somatotropin
<g/>
,	,	kIx,	,
testosteron	testosteron	k1gInSc1	testosteron
a	a	k8xC	a
insulin	insulin	k1gInSc1	insulin
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Reisenauer	Reisenauer	k1gInSc4	Reisenauer
R.	R.	kA	R.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
CO	co	k3yInSc1	co
JE	být	k5eAaImIp3nS	být
CO	co	k3yRnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Pressfoto	Pressfota	k1gFnSc5	Pressfota
-	-	kIx~	-
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Bílkoviny	bílkovina	k1gFnSc2	bílkovina
ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
výživě	výživa	k1gFnSc6	výživa
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bílkovina	bílkovina	k1gFnSc1	bílkovina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bílkovina	bílkovina	k1gFnSc1	bílkovina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
