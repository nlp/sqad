<s>
Flettner	Flettner	k1gInSc1	Flettner
Fl	Fl	k1gFnSc2	Fl
282	[number]	k4	282
Kolibri	kolibri	k1gMnSc1	kolibri
(	(	kIx(	(
<g/>
Kolibřík	kolibřík	k1gMnSc1	kolibřík
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
jednomístný	jednomístný	k2eAgInSc1d1	jednomístný
vrtulník	vrtulník	k1gInSc1	vrtulník
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
kokpitem	kokpit	k1gInSc7	kokpit
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
prolínajícími	prolínající	k2eAgInPc7d1	prolínající
se	s	k7c7	s
protiběžnými	protiběžný	k2eAgInPc7d1	protiběžný
rotory	rotor	k1gInPc7	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
konstruktérem	konstruktér	k1gMnSc7	konstruktér
byl	být	k5eAaImAgMnS	být
vrtulníkový	vrtulníkový	k2eAgMnSc1d1	vrtulníkový
průkopník	průkopník	k1gMnSc1	průkopník
Anton	Anton	k1gMnSc1	Anton
Flettner	Flettner	k1gMnSc1	Flettner
<g/>
.	.	kIx.	.
</s>
<s>
Kolibri	kolibri	k1gMnSc1	kolibri
byl	být	k5eAaImAgMnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
helikoptér	helikoptéra	k1gFnPc2	helikoptéra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
testovalo	testovat	k5eAaImAgNnS	testovat
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
tisíců	tisíc	k4xCgInPc2	tisíc
Kolibříků	kolibřík	k1gMnPc2	kolibřík
byly	být	k5eAaImAgInP	být
opuštěny	opustit	k5eAaPmNgInP	opustit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Spojenci	spojenec	k1gMnPc1	spojenec
vybombardovali	vybombardovat	k5eAaPmAgMnP	vybombardovat
Flettnerovy	Flettnerův	k2eAgFnPc4d1	Flettnerův
továrny	továrna	k1gFnPc4	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
přežily	přežít	k5eAaPmAgFnP	přežít
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
Fl	Fl	k1gFnPc4	Fl
282	[number]	k4	282
<g/>
;	;	kIx,	;
zbylé	zbylý	k2eAgFnPc1d1	zbylá
byly	být	k5eAaImAgFnP	být
zničeny	zničen	k2eAgFnPc1d1	zničena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepadly	padnout	k5eNaPmAgInP	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
pilot	pilot	k1gMnSc1	pilot
<g/>
)	)	kIx)	)
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
6,65	[number]	k4	6,65
m	m	kA	m
Průměr	průměr	k1gInSc1	průměr
rotoru	rotor	k1gInSc2	rotor
<g/>
:	:	kIx,	:
11,96	[number]	k4	11,96
m	m	kA	m
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
2,20	[number]	k4	2,20
m	m	kA	m
Plocha	plocha	k1gFnSc1	plocha
rotoru	rotor	k1gInSc2	rotor
<g/>
:	:	kIx,	:
224,69	[number]	k4	224,69
m2	m2	k4	m2
Hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
760	[number]	k4	760
kg	kg	kA	kg
Maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
1	[number]	k4	1
000	[number]	k4	000
kg	kg	kA	kg
Pohon	pohon	k1gInSc1	pohon
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
×	×	k?	×
Bramo	brama	k1gFnSc5	brama
Sh	Sh	k1gMnSc2	Sh
14A	[number]	k4	14A
sedmiválcový	sedmiválcový	k2eAgInSc1d1	sedmiválcový
hvězdicový	hvězdicový	k2eAgInSc1d1	hvězdicový
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
119	[number]	k4	119
kW	kW	kA	kW
(	(	kIx(	(
<g/>
160	[number]	k4	160
k	k	k7c3	k
<g/>
)	)	kIx)	)
Maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
:	:	kIx,	:
150	[number]	k4	150
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
170	[number]	k4	170
km	km	kA	km
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
3	[number]	k4	3
300	[number]	k4	300
m	m	kA	m
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Flettner	Flettnra	k1gFnPc2	Flettnra
Fl	Fl	k1gFnSc2	Fl
282	[number]	k4	282
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
