<p>
<s>
Mírný	mírný	k2eAgInSc1d1	mírný
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
neboli	neboli	k8xC	neboli
mírné	mírný	k2eAgFnSc2d1	mírná
šířky	šířka	k1gFnSc2	šířka
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgFnPc1d1	mírná
šířky	šířka	k1gFnPc1	šířka
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
se	s	k7c7	s
subtropickým	subtropický	k2eAgNnSc7d1	subtropické
podnebím	podnebí	k1gNnSc7	podnebí
a	a	k8xC	a
oblastmi	oblast	k1gFnPc7	oblast
se	s	k7c7	s
subpolárním	subpolární	k2eAgNnSc7d1	subpolární
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
leží	ležet	k5eAaImIp3nP	ležet
mírné	mírný	k2eAgFnPc1d1	mírná
šířky	šířka	k1gFnPc1	šířka
mezi	mezi	k7c7	mezi
40	[number]	k4	40
<g/>
°	°	k?	°
a	a	k8xC	a
60	[number]	k4	60
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
sahá	sahat	k5eAaImIp3nS	sahat
mírné	mírný	k2eAgNnSc4d1	mírné
podnebí	podnebí	k1gNnSc4	podnebí
až	až	k9	až
k	k	k7c3	k
polárnímu	polární	k2eAgInSc3d1	polární
kruhu	kruh	k1gInSc3	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
šířkách	šířka	k1gFnPc6	šířka
probíhá	probíhat	k5eAaImIp3nS	probíhat
rychlá	rychlý	k2eAgFnSc1d1	rychlá
výměna	výměna	k1gFnSc1	výměna
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
tlakových	tlakový	k2eAgInPc2d1	tlakový
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
jsou	být	k5eAaImIp3nP	být
mírné	mírný	k2eAgFnSc2d1	mírná
šířky	šířka	k1gFnSc2	šířka
nejvíce	nejvíce	k6eAd1	nejvíce
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
polární	polární	k2eAgFnSc2d1	polární
vzduchové	vzduchový	k2eAgFnSc2d1	vzduchová
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
převládajících	převládající	k2eAgInPc2d1	převládající
západních	západní	k2eAgInPc2d1	západní
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mírné	mírný	k2eAgNnSc4d1	mírné
pásmo	pásmo	k1gNnSc4	pásmo
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
střídání	střídání	k1gNnSc1	střídání
čtyř	čtyři	k4xCgFnPc2	čtyři
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Alisovovy	Alisovův	k2eAgFnSc2d1	Alisovův
klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
mírný	mírný	k2eAgInSc1d1	mírný
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
omezen	omezit	k5eAaPmNgInS	omezit
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
letní	letní	k2eAgFnSc7d1	letní
polohou	poloha	k1gFnSc7	poloha
polární	polární	k2eAgFnSc2d1	polární
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
zimní	zimní	k2eAgFnSc7d1	zimní
polohou	poloha	k1gFnSc7	poloha
arktické	arktický	k2eAgFnSc2d1	arktická
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podnebí	podnebí	k1gNnSc4	podnebí
mírných	mírný	k2eAgFnPc2d1	mírná
šířek	šířka	k1gFnPc2	šířka
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
pevninské	pevninský	k2eAgInPc4d1	pevninský
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oceánické	oceánický	k2eAgFnSc2d1	oceánická
<g/>
,	,	kIx,	,
západních	západní	k2eAgInPc2d1	západní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
(	(	kIx(	(
<g/>
monzunové	monzunový	k2eAgInPc4d1	monzunový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
šířkách	šířka	k1gFnPc6	šířka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
listnaté	listnatý	k2eAgInPc1d1	listnatý
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
a	a	k8xC	a
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
stepi	step	k1gFnPc1	step
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pouště	poušť	k1gFnPc4	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
šířkách	šířka	k1gFnPc6	šířka
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
:	:	kIx,	:
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
<g/>
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
<g/>
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
<g/>
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Města	město	k1gNnSc2	město
mírných	mírný	k2eAgFnPc2d1	mírná
šířek	šířka	k1gFnPc2	šířka
==	==	k?	==
</s>
</p>
<p>
<s>
severní	severní	k2eAgMnSc1d1	severní
polokouleLondýn	polokouleLondýn	k1gMnSc1	polokouleLondýn
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
RuskoPeking	RuskoPeking	k1gInSc1	RuskoPeking
<g/>
,	,	kIx,	,
ČínaMontréal	ČínaMontréal	k1gInSc1	ČínaMontréal
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
</s>
</p>
<p>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
NETOPIL	topit	k5eNaImAgMnS	topit
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
124	[number]	k4	124
<g/>
–	–	k?	–
<g/>
125	[number]	k4	125
<g/>
,	,	kIx,	,
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
</s>
</p>
