<p>
<s>
Maria	Maria	k1gFnSc1	Maria
Josefa	Josefa	k1gFnSc1	Josefa
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
Maria	Maria	k1gFnSc1	Maria
Josepha	Joseph	k1gMnSc2	Joseph
Benedikta	Benedikt	k1gMnSc2	Benedikt
Antonia	Antonio	k1gMnSc2	Antonio
Theresia	Theresius	k1gMnSc2	Theresius
Xaveria	Xaverius	k1gMnSc2	Xaverius
Philippine	Philippin	k1gInSc5	Philippin
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1757	[number]	k4	1757
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc4	Sasko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Augustem	August	k1gMnSc7	August
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Saským	saský	k2eAgMnPc3d1	saský
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
stala	stát	k5eAaPmAgFnS	stát
saskou	saský	k2eAgFnSc7d1	saská
kurfiřtkou	kurfiřtka	k1gFnSc7	kurfiřtka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
polskou	polský	k2eAgFnSc7d1	polská
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
byla	být	k5eAaImAgFnS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
dítětem	dítě	k1gNnSc7	dítě
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Amálie	Amálie	k1gFnSc2	Amálie
Vilemíny	Vilemína	k1gFnSc2	Vilemína
Brunšvické	brunšvický	k2eAgFnSc2d1	Brunšvická
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgMnPc4	dva
mladší	mladý	k2eAgMnPc4d2	mladší
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
bratra	bratr	k1gMnSc4	bratr
Leopolda	Leopold	k1gMnSc4	Leopold
Josefa	Josef	k1gMnSc4	Josef
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
Marii	Maria	k1gFnSc4	Maria
Amálii	Amálie	k1gFnSc4	Amálie
(	(	kIx(	(
<g/>
1701	[number]	k4	1701
<g/>
–	–	k?	–
<g/>
1756	[number]	k4	1756
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
měla	mít	k5eAaImAgFnS	mít
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
šanci	šance	k1gFnSc4	šance
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
dědičkou	dědička	k1gFnSc7	dědička
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
ani	ani	k8xC	ani
strýc	strýc	k1gMnSc1	strýc
nezplodili	zplodit	k5eNaPmAgMnP	zplodit
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
šanci	šance	k1gFnSc4	šance
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
připravila	připravit	k5eAaPmAgFnS	připravit
Pragmatická	pragmatický	k2eAgFnSc1d1	pragmatická
sankce	sankce	k1gFnSc1	sankce
jejího	její	k3xOp3gMnSc2	její
strýce	strýc	k1gMnSc2	strýc
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
mezi	mezi	k7c7	mezi
Marií	Maria	k1gFnSc7	Maria
Josefou	Josefa	k1gFnSc7	Josefa
a	a	k8xC	a
Augustem	August	k1gMnSc7	August
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
však	však	k9	však
nesměla	smět	k5eNaImAgFnS	smět
vdát	vdát	k5eAaPmF	vdát
za	za	k7c4	za
nekatolíka	nekatolík	k1gMnSc4	nekatolík
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
pohledná	pohledný	k2eAgFnSc1d1	pohledná
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
žádoucí	žádoucí	k2eAgFnSc7d1	žádoucí
nevěstou	nevěsta	k1gFnSc7	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
August	August	k1gMnSc1	August
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jednání	jednání	k1gNnSc2	jednání
začala	začít	k5eAaPmAgFnS	začít
nabírat	nabírat	k5eAaImF	nabírat
hmatatelnější	hmatatelný	k2eAgInPc4d2	hmatatelnější
obrysy	obrys	k1gInPc4	obrys
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1719	[number]	k4	1719
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Augusta	August	k1gMnSc4	August
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1733	[number]	k4	1733
stal	stát	k5eAaPmAgMnS	stát
saským	saský	k2eAgMnSc7d1	saský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
králem	král	k1gMnSc7	král
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
jako	jako	k9	jako
August	August	k1gMnSc1	August
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Saský	saský	k2eAgMnSc1d1	saský
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1734	[number]	k4	1734
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
korunována	korunován	k2eAgFnSc1d1	korunována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tohoto	tento	k3xDgInSc2	tento
sňatku	sňatek	k1gInSc2	sňatek
mezi	mezi	k7c7	mezi
Wettiny	Wettin	k1gInPc7	Wettin
a	a	k8xC	a
Habsburky	Habsburk	k1gMnPc7	Habsburk
ženichův	ženichův	k2eAgMnSc1d1	ženichův
otec	otec	k1gMnSc1	otec
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgMnSc1d1	silný
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajistí	zajistit	k5eAaPmIp3nS	zajistit
Sasku	Saska	k1gFnSc4	Saska
lepší	dobrý	k2eAgFnSc4d2	lepší
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
případné	případný	k2eAgFnSc6d1	případná
válce	válka	k1gFnSc6	válka
o	o	k7c6	o
následnictví	následnictví	k1gNnSc6	následnictví
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
páru	pár	k1gInSc2	pár
Fridrich	Fridrich	k1gMnSc1	Fridrich
Kristián	Kristián	k1gMnSc1	Kristián
Saský	saský	k2eAgMnSc1d1	saský
ale	ale	k8xC	ale
následoval	následovat	k5eAaImAgInS	následovat
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
saským	saský	k2eAgMnSc7d1	saský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1740	[number]	k4	1740
nárokovala	nárokovat	k5eAaImAgFnS	nárokovat
rakouský	rakouský	k2eAgInSc4d1	rakouský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc4	boj
vzdala	vzdát	k5eAaPmAgFnS	vzdát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
alianci	aliance	k1gFnSc4	aliance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
měla	mít	k5eAaImAgFnS	mít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
14	[number]	k4	14
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1728	[number]	k4	1728
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
Kristián	Kristián	k1gMnSc1	Kristián
(	(	kIx(	(
<g/>
1722	[number]	k4	1722
<g/>
–	–	k?	–
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc1	Amálie
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1760	[number]	k4	1760
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Markéta	Markéta	k1gFnSc1	Markéta
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
1734	[number]	k4	1734
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Saská	saský	k2eAgFnSc1d1	saská
(	(	kIx(	(
<g/>
1728	[number]	k4	1728
<g/>
–	–	k?	–
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
–	–	k?	–
∞	∞	k?	∞
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
<g/>
,	,	kIx,	,
bavorský	bavorský	k2eAgMnSc1d1	bavorský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
saský	saský	k2eAgMnSc1d1	saský
regent	regent	k1gMnSc1	regent
v	v	k7c6	v
letech	let	k1gInPc6	let
1763	[number]	k4	1763
<g/>
–	–	k?	–
<g/>
1768	[number]	k4	1768
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
Karolína	Karolína	k1gFnSc1	Karolína
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
∞	∞	k?	∞
francouzský	francouzský	k2eAgMnSc1d1	francouzský
dauphin	dauphin	k1gMnSc1	dauphin
Ludvík	Ludvík	k1gMnSc1	Ludvík
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Bourbonský	bourbonský	k2eAgMnSc1d1	bourbonský
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kristián	Kristián	k1gMnSc1	Kristián
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Courlandu	Courland	k1gInSc2	Courland
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Kristina	Kristina	k1gFnSc1	Kristina
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
remiremontská	remiremontský	k2eAgFnSc1d1	remiremontský
abatyše	abatyše	k1gFnSc1	abatyše
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Eliška	Eliška	k1gFnSc1	Eliška
(	(	kIx(	(
<g/>
1736	[number]	k4	1736
<g/>
–	–	k?	–
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Kazimír	Kazimír	k1gMnSc1	Kazimír
(	(	kIx(	(
<g/>
1738	[number]	k4	1738
<g/>
–	–	k?	–
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místodržící	místodržící	k1gMnSc1	místodržící
Uher	Uher	k1gMnSc1	Uher
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
Sasko-Těšínský	saskoěšínský	k2eAgMnSc1d1	sasko-těšínský
∞	∞	k?	∞
Marie	Marie	k1gFnSc1	Marie
Kristina	Kristina	k1gFnSc1	Kristina
Habsbursko-Lotrinská	habsburskootrinský	k2eAgFnSc1d1	habsbursko-lotrinská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
</s>
</p>
<p>
<s>
Klement	Klement	k1gMnSc1	Klement
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1739	[number]	k4	1739
<g/>
–	–	k?	–
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
a	a	k8xC	a
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
trevírský	trevírský	k2eAgMnSc1d1	trevírský
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Kunigunda	Kunigunda	k1gFnSc1	Kunigunda
(	(	kIx(	(
<g/>
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abatyše	abatyše	k1gFnSc1	abatyše
z	z	k7c2	z
Thornu	Thorn	k1gInSc2	Thorn
a	a	k8xC	a
Essenu	Essen	k1gInSc2	Essen
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Maria	Mario	k1gMnSc2	Mario
Josepha	Joseph	k1gMnSc2	Joseph
of	of	k?	of
Austria	Austrium	k1gNnPc1	Austrium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marie	Maria	k1gFnSc2	Maria
Josefa	Josefa	k1gFnSc1	Josefa
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
