<s>
Maria	Maria	k1gFnSc1	Maria
Josefa	Josefa	k1gFnSc1	Josefa
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
Maria	Maria	k1gFnSc1	Maria
Josepha	Joseph	k1gMnSc2	Joseph
Benedikta	Benedikt	k1gMnSc2	Benedikt
Antonia	Antonio	k1gMnSc2	Antonio
Theresia	Theresius	k1gMnSc2	Theresius
Xaveria	Xaverius	k1gMnSc2	Xaverius
Philippine	Philippin	k1gInSc5	Philippin
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1757	[number]	k4	1757
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc4	Sasko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Augustem	August	k1gMnSc7	August
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Saským	saský	k2eAgMnPc3d1	saský
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
stala	stát	k5eAaPmAgFnS	stát
saskou	saský	k2eAgFnSc7d1	saská
kurfiřtkou	kurfiřtka	k1gFnSc7	kurfiřtka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
polskou	polský	k2eAgFnSc7d1	polská
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
byla	být	k5eAaImAgFnS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
dítětem	dítě	k1gNnSc7	dítě
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Amálie	Amálie	k1gFnSc2	Amálie
Vilemíny	Vilemína	k1gFnSc2	Vilemína
Brunšvické	brunšvický	k2eAgFnSc2d1	Brunšvická
<g/>
.	.	kIx.	.
</s>

