<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
alokace	alokace	k1gFnSc1	alokace
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
rezervaci	rezervace	k1gFnSc4	rezervace
části	část	k1gFnSc2	část
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
spravuje	spravovat	k5eAaImIp3nS	spravovat
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
část	část	k1gFnSc1	část
jádra	jádro	k1gNnSc2	jádro
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
správce	správce	k1gMnSc2	správce
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
memory	memora	k1gFnSc2	memora
manager	manager	k1gMnSc1	manager
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
paměť	paměť	k1gFnSc1	paměť
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
nejen	nejen	k6eAd1	nejen
alokovat	alokovat	k5eAaImF	alokovat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
proces	proces	k1gInSc1	proces
paměť	paměť	k1gFnSc4	paměť
od	od	k7c2	od
systému	systém	k1gInSc2	systém
získá	získat	k5eAaPmIp3nS	získat
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vracet	vracet	k5eAaImF	vracet
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
vrací	vracet	k5eAaImIp3nS	vracet
nepotřebnou	potřebný	k2eNgFnSc4d1	nepotřebná
paměť	paměť	k1gFnSc4	paměť
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
opět	opět	k6eAd1	opět
přidělena	přidělen	k2eAgFnSc1d1	přidělena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dynamické	dynamický	k2eAgFnSc2d1	dynamická
alokace	alokace	k1gFnSc2	alokace
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
programu	program	k1gInSc2	program
známá	známý	k2eAgFnSc1d1	známá
velikost	velikost	k1gFnSc1	velikost
potřebné	potřebný	k2eAgFnSc2d1	potřebná
části	část	k1gFnSc2	část
paměti	paměť	k1gFnSc2	paměť
pro	pro	k7c4	pro
ukládaná	ukládaný	k2eAgNnPc4d1	ukládané
data	datum	k1gNnPc4	datum
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
statické	statický	k2eAgFnSc2d1	statická
alokace	alokace	k1gFnSc2	alokace
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staticky	staticky	k6eAd1	staticky
alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
výhodná	výhodný	k2eAgFnSc1d1	výhodná
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
rychlosti	rychlost	k1gFnSc2	rychlost
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
uloženým	uložený	k2eAgNnPc3d1	uložené
datům	datum	k1gNnPc3	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nevýhodná	výhodný	k2eNgFnSc1d1	nevýhodná
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
neefektivnosti	neefektivnost	k1gFnSc2	neefektivnost
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
datovým	datový	k2eAgInSc7d1	datový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Představme	představit	k5eAaPmRp1nP	představit
si	se	k3xPyFc3	se
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
setřídí	setřídit	k5eAaPmIp3nS	setřídit
zadané	zadaný	k2eAgInPc4d1	zadaný
řetězce	řetězec	k1gInPc4	řetězec
(	(	kIx(	(
<g/>
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
řetězců	řetězec	k1gInPc2	řetězec
bude	být	k5eAaImBp3nS	být
zadáno	zadat	k5eAaPmNgNnS	zadat
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gFnSc4	jejich
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
pole	pole	k1gNnSc4	pole
o	o	k7c6	o
takovém	takový	k3xDgInSc6	takový
počtu	počet	k1gInSc6	počet
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
položek	položka	k1gFnPc2	položka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nebude	být	k5eNaImBp3nS	být
překročeno	překročit	k5eAaPmNgNnS	překročit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
500	[number]	k4	500
položek	položka	k1gFnPc2	položka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
128	[number]	k4	128
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
statická	statický	k2eAgFnSc1d1	statická
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
výpočet	výpočet	k1gInSc4	výpočet
a	a	k8xC	a
zabere	zabrat	k5eAaPmIp3nS	zabrat
zbytečně	zbytečně	k6eAd1	zbytečně
mnoho	mnoho	k6eAd1	mnoho
paměťového	paměťový	k2eAgInSc2d1	paměťový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
setřídit	setřídit	k5eAaPmF	setřídit
501	[number]	k4	501
položek	položka	k1gFnPc2	položka
nebo	nebo	k8xC	nebo
že	že	k8xS	že
položka	položka	k1gFnSc1	položka
bude	být	k5eAaImBp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
ošetřeny	ošetřen	k2eAgFnPc1d1	ošetřena
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
dynamicky	dynamicky	k6eAd1	dynamicky
alokované	alokovaný	k2eAgFnSc2d1	alokovaná
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
kdykoliv	kdykoliv	k6eAd1	kdykoliv
uvolnit	uvolnit	k5eAaPmF	uvolnit
již	již	k6eAd1	již
zabraný	zabraný	k2eAgInSc4d1	zabraný
paměťový	paměťový	k2eAgInSc4d1	paměťový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc4	rychlost
samotného	samotný	k2eAgInSc2d1	samotný
programu	program	k1gInSc2	program
i	i	k9	i
přes	přes	k7c4	přes
zvýšené	zvýšený	k2eAgInPc4d1	zvýšený
datové	datový	k2eAgInPc4d1	datový
nároky	nárok	k1gInPc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Vytváření	vytváření	k1gNnSc1	vytváření
dynamických	dynamický	k2eAgFnPc2d1	dynamická
proměnných	proměnná	k1gFnPc2	proměnná
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
je	být	k5eAaImIp3nS	být
však	však	k9	však
časově	časově	k6eAd1	časově
náročnější	náročný	k2eAgMnSc1d2	náročnější
než	než	k8xS	než
u	u	k7c2	u
statických	statický	k2eAgInPc2d1	statický
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
neuváženém	uvážený	k2eNgNnSc6d1	neuvážené
používání	používání	k1gNnSc6	používání
takových	takový	k3xDgFnPc2	takový
struktur	struktura	k1gFnPc2	struktura
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
celé	celý	k2eAgFnSc2d1	celá
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
dva	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
paměťovým	paměťový	k2eAgInSc7d1	paměťový
prostorem	prostor	k1gInSc7	prostor
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
i	i	k8xC	i
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
paměť	paměť	k1gFnSc1	paměť
alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
staticky	staticky	k6eAd1	staticky
je	být	k5eAaImIp3nS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
po	po	k7c6	po
přesně	přesně	k6eAd1	přesně
stanovené	stanovený	k2eAgFnSc6d1	stanovená
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
dynamicky	dynamicky	k6eAd1	dynamicky
alokovanou	alokovaný	k2eAgFnSc4d1	alokovaná
paměť	paměť	k1gFnSc4	paměť
musí	muset	k5eAaImIp3nS	muset
uvolnit	uvolnit	k5eAaPmF	uvolnit
sám	sám	k3xTgInSc1	sám
program	program	k1gInSc1	program
nebo	nebo	k8xC	nebo
garbage	garbage	k1gFnSc1	garbage
collector	collector	k1gInSc1	collector
<g/>
.	.	kIx.	.
</s>
<s>
Dynamicky	dynamicky	k6eAd1	dynamicky
alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
paměť	paměť	k1gFnSc1	paměť
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
též	též	k9	též
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
halda	halda	k1gFnSc1	halda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
veškerou	veškerý	k3xTgFnSc4	veškerý
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
používáno	používat	k5eAaImNgNnS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
části	část	k1gFnSc2	část
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
alokaci	alokace	k1gFnSc4	alokace
dynamické	dynamický	k2eAgFnSc2d1	dynamická
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
dynamických	dynamický	k2eAgFnPc2d1	dynamická
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
předem	předem	k6eAd1	předem
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
paměti	paměť	k1gFnSc2	paměť
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
datový	datový	k2eAgInSc4d1	datový
prostor	prostor	k1gInSc4	prostor
adresován	adresovat	k5eAaBmNgInS	adresovat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
ukazatelů	ukazatel	k1gMnPc2	ukazatel
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
pointer	pointer	k1gInSc1	pointer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatele	ukazatel	k1gInPc1	ukazatel
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
adresu	adresa	k1gFnSc4	adresa
začátku	začátek	k1gInSc2	začátek
alokované	alokovaný	k2eAgFnSc2d1	alokovaná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatele	ukazatel	k1gInPc1	ukazatel
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
využívají	využívat	k5eAaImIp3nP	využívat
programátoři	programátor	k1gMnPc1	programátor
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
nebo	nebo	k8xC	nebo
Pascalu	pascal	k1gInSc6	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
programovací	programovací	k2eAgInPc1d1	programovací
jazyky	jazyk	k1gInPc1	jazyk
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
používat	používat	k5eAaImF	používat
ukazatele	ukazatel	k1gInPc4	ukazatel
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
je	být	k5eAaImIp3nS	být
referencemi	reference	k1gFnPc7	reference
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
(	(	kIx(	(
<g/>
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
Visual	Visual	k1gMnSc1	Visual
Basic	Basic	kA	Basic
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
,	,	kIx,	,
Java	Jav	k2eAgFnSc1d1	Java
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
programátor	programátor	k1gMnSc1	programátor
nemohl	moct	k5eNaImAgMnS	moct
pracovat	pracovat	k5eAaImF	pracovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
adresami	adresa	k1gFnPc7	adresa
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
vyvaroval	vyvarovat	k5eAaPmAgMnS	vyvarovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
zbytečných	zbytečný	k2eAgFnPc2d1	zbytečná
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc4	uvolnění
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
funkce	funkce	k1gFnSc1	funkce
malloc	malloc	k1gFnSc1	malloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
a	a	k8xC	a
free	free	k6eAd1	free
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
definovaných	definovaný	k2eAgInPc2d1	definovaný
v	v	k7c6	v
hlavičkovém	hlavičkový	k2eAgInSc6d1	hlavičkový
souboru	soubor	k1gInSc6	soubor
z	z	k7c2	z
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
(	(	kIx(	(
<g/>
stdlib	stdlib	k1gMnSc1	stdlib
<g/>
.	.	kIx.	.
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
void	void	k1gInSc1	void
*	*	kIx~	*
<g/>
malloc	malloc	k1gInSc1	malloc
(	(	kIx(	(
<g/>
size_t	size_t	k2eAgInSc1d1	size_t
size	size	k1gInSc1	size
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Funkce	funkce	k1gFnSc1	funkce
malloc	malloc	k1gFnSc1	malloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
alokuje	alokovat	k5eAaImIp3nS	alokovat
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
blok	blok	k1gInSc1	blok
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
size	siz	k1gFnSc2	siz
(	(	kIx(	(
<g/>
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
návratová	návratový	k2eAgFnSc1d1	návratová
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
funkce	funkce	k1gFnSc1	funkce
neproběhne	proběhnout	k5eNaPmIp3nS	proběhnout
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
velikost	velikost	k1gFnSc1	velikost
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
vrací	vracet	k5eAaImIp3nS	vracet
ukazatel	ukazatel	k1gMnSc1	ukazatel
null	null	k1gMnSc1	null
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
alokaci	alokace	k1gFnSc6	alokace
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
paměť	paměť	k1gFnSc4	paměť
náhodné	náhodný	k2eAgFnSc2d1	náhodná
hodnoty	hodnota	k1gFnSc2	hodnota
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
nedefinovaný	definovaný	k2eNgInSc1d1	nedefinovaný
<g/>
.	.	kIx.	.
void	void	k1gInSc1	void
free	fre	k1gInSc2	fre
(	(	kIx(	(
<g/>
void	void	k1gInSc1	void
*	*	kIx~	*
<g/>
ptr	ptr	k?	ptr
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Funkce	funkce	k1gFnSc1	funkce
free	free	k1gFnSc1	free
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
alokován	alokovat	k5eAaImNgInS	alokovat
funkcemi	funkce	k1gFnPc7	funkce
malloc	malloc	k6eAd1	malloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
calloc	calloc	k1gFnSc1	calloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
realloc	realloc	k6eAd1	realloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
argument	argument	k1gInSc1	argument
je	být	k5eAaImIp3nS	být
funkci	funkce	k1gFnSc4	funkce
předán	předán	k2eAgInSc4d1	předán
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
alokovaného	alokovaný	k2eAgInSc2d1	alokovaný
bloku	blok	k1gInSc2	blok
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nebudeme	být	k5eNaImBp1nP	být
alokovanou	alokovaný	k2eAgFnSc4d1	alokovaná
paměť	paměť	k1gFnSc4	paměť
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opakovanému	opakovaný	k2eAgNnSc3d1	opakované
zabírání	zabírání	k1gNnSc3	zabírání
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
vyčerpány	vyčerpat	k5eAaPmNgInP	vyčerpat
všechny	všechen	k3xTgInPc1	všechen
paměťové	paměťový	k2eAgInPc1d1	paměťový
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
void	void	k1gInSc1	void
*	*	kIx~	*
<g/>
calloc	calloc	k1gInSc1	calloc
(	(	kIx(	(
<g/>
size_t	size_t	k1gInSc1	size_t
num	num	k?	num
<g/>
,	,	kIx,	,
size_t	size_t	k2eAgInSc1d1	size_t
size	size	k1gInSc1	size
)	)	kIx)	)
<g/>
;	;	kIx,	;
Funkce	funkce	k1gFnSc1	funkce
calloc	calloc	k1gFnSc1	calloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alternativa	alternativa	k1gFnSc1	alternativa
funkce	funkce	k1gFnSc1	funkce
malloc	malloc	k1gFnSc1	malloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
calloc	calloc	k6eAd1	calloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
alokuje	alokovat	k5eAaImIp3nS	alokovat
paměťový	paměťový	k2eAgInSc4d1	paměťový
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
num	num	k?	num
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
size	siz	k1gFnSc2	siz
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
paměť	paměť	k1gFnSc1	paměť
ale	ale	k9	ale
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
náhodné	náhodný	k2eAgFnPc4d1	náhodná
hodnoty	hodnota	k1gFnPc4	hodnota
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
nulami	nula	k1gFnPc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
trvá	trvat	k5eAaImIp3nS	trvat
alokace	alokace	k1gFnSc1	alokace
pomocí	pomocí	k7c2	pomocí
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
o	o	k7c4	o
něco	něco	k3yInSc4	něco
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
void	void	k6eAd1	void
*	*	kIx~	*
<g/>
realloc	realloc	k1gFnSc1	realloc
(	(	kIx(	(
<g/>
void	void	k1gInSc1	void
*	*	kIx~	*
<g/>
ptr	ptr	k?	ptr
<g/>
,	,	kIx,	,
size_t	size_t	k2eAgInSc1d1	size_t
size	size	k1gInSc1	size
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Funkce	funkce	k1gFnSc1	funkce
realloc	realloc	k1gFnSc1	realloc
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
velikosti	velikost	k1gFnSc2	velikost
alokované	alokovaný	k2eAgFnSc2d1	alokovaná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
argumentem	argument	k1gInSc7	argument
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc1	ukazatel
na	na	k7c4	na
část	část	k1gFnSc4	část
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
změněna	změnit	k5eAaPmNgFnS	změnit
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
argumentem	argument	k1gInSc7	argument
je	být	k5eAaImIp3nS	být
výsledná	výsledný	k2eAgFnSc1d1	výsledná
velikost	velikost	k1gFnSc1	velikost
rezervované	rezervovaný	k2eAgFnSc2d1	rezervovaná
paměti	paměť	k1gFnSc2	paměť
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
datové	datový	k2eAgFnSc2d1	datová
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
již	již	k6eAd1	již
rezervovanou	rezervovaný	k2eAgFnSc7d1	rezervovaná
pamětí	paměť	k1gFnSc7	paměť
bude	být	k5eAaImBp3nS	být
další	další	k2eAgInSc1d1	další
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nový	nový	k2eAgInSc4d1	nový
ukazatel	ukazatel	k1gInSc4	ukazatel
začátku	začátek	k1gInSc2	začátek
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
úplně	úplně	k6eAd1	úplně
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
ukazatel	ukazatel	k1gInSc1	ukazatel
*	*	kIx~	*
<g/>
ptr	ptr	k?	ptr
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
užívat	užívat	k5eAaImF	užívat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
dealokovat	dealokovat	k5eAaBmF	dealokovat
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
http://www.sallyx.org/sally/c/c18.php	[url]	k1gInSc1	http://www.sallyx.org/sally/c/c18.php
http://www.cplusplus.com/reference/clibrary/cstdlib/free/	[url]	k?	http://www.cplusplus.com/reference/clibrary/cstdlib/free/
http://www.cplusplus.com/reference/clibrary/cstdlib/malloc/	[url]	k?	http://www.cplusplus.com/reference/clibrary/cstdlib/malloc/
http://www.cplusplus.com/reference/clibrary/cstdlib/calloc/	[url]	k?	http://www.cplusplus.com/reference/clibrary/cstdlib/calloc/
http://www.cplusplus.com/reference/clibrary/cstdlib/realloc/	[url]	k?	http://www.cplusplus.com/reference/clibrary/cstdlib/realloc/
</s>
