<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
New	New	k1gFnSc4	New
Zealand	Zealanda	k1gFnPc2	Zealanda
<g/>
,	,	kIx,	,
maorsky	maorsky	k6eAd1	maorsky
Aotearoa	Aotearo	k2eAgFnSc1d1	Aotearo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
menší	malý	k2eAgInPc1d2	menší
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgInSc1d3	Nejbližší
sousední	sousední	k2eAgInSc1d1	sousední
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2000	[number]	k4	2000
km	km	kA	km
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
šelfu	šelf	k1gInSc6	šelf
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
však	však	k9	však
částí	část	k1gFnSc7	část
ponořeného	ponořený	k2eAgInSc2d1	ponořený
kontinentu	kontinent	k1gInSc2	kontinent
Zélandie	Zélandie	k1gFnSc2	Zélandie
<g/>
.	.	kIx.	.
</s>
<s>
Zélandie	Zélandie	k1gFnSc1	Zélandie
společně	společně	k6eAd1	společně
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
tvoří	tvořit	k5eAaImIp3nS	tvořit
širší	široký	k2eAgInSc4d2	širší
region	region	k1gInSc4	region
Australasie	Australasie	k1gFnSc2	Australasie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
měli	mít	k5eAaImAgMnP	mít
Maorové	Maorová	k1gFnPc4	Maorová
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
název	název	k1gInSc4	název
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
název	název	k1gInSc4	název
pro	pro	k7c4	pro
Severní	severní	k2eAgInSc4d1	severní
ostrov	ostrov	k1gInSc4	ostrov
(	(	kIx(	(
<g/>
Te	Te	k1gMnSc1	Te
Ika	Ika	k1gMnSc1	Ika
a	a	k8xC	a
Mā	Mā	k1gMnSc1	Mā
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Te	Te	k1gFnSc1	Te
Wai	Wai	k1gFnSc2	Wai
Pounamu	Pounam	k1gInSc2	Pounam
nebo	nebo	k8xC	nebo
Te	Te	k1gFnSc2	Te
Waka	Wak	k1gInSc2	Wak
o	o	k7c6	o
Aoraki	Aorak	k1gInSc6	Aorak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
Severní	severní	k2eAgInSc1d1	severní
ostrov	ostrov	k1gInSc1	ostrov
také	také	k6eAd1	také
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Aotearoa	Aotearoa	k1gFnSc1	Aotearoa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
země	země	k1gFnSc1	země
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
bílého	bílý	k2eAgInSc2d1	bílý
oblaku	oblak	k1gInSc2	oblak
(	(	kIx(	(
<g/>
ao	ao	k?	ao
=	=	kIx~	=
oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
tea	tea	k1gFnSc1	tea
=	=	kIx~	=
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
roa	roa	k?	roa
=	=	kIx~	=
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
maorštině	maorština	k1gFnSc6	maorština
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Aotearoa	Aotearo	k1gInSc2	Aotearo
se	se	k3xPyFc4	se
také	také	k9	také
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
novozélandské	novozélandský	k2eAgFnSc6d1	novozélandská
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
evropský	evropský	k2eAgMnSc1d1	evropský
objevitel	objevitel	k1gMnSc1	objevitel
ostrovů	ostrov	k1gInPc2	ostrov
Abel	Abel	k1gMnSc1	Abel
Tasman	Tasman	k1gMnSc1	Tasman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
přistál	přistát	k5eAaPmAgInS	přistát
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Staten	Staten	k2eAgInSc4d1	Staten
Landt	Landt	k1gInSc4	Landt
<g/>
.	.	kIx.	.
</s>
<s>
Tasman	Tasman	k1gMnSc1	Tasman
si	se	k3xPyFc3	se
však	však	k9	však
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgMnS	objevit
část	část	k1gFnSc4	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
objevenou	objevený	k2eAgFnSc4d1	objevená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1615	[number]	k4	1615
nizozemským	nizozemský	k2eAgMnSc7d1	nizozemský
námořníkem	námořník	k1gMnSc7	námořník
Jacobem	Jacob	k1gMnSc7	Jacob
Le	Le	k1gMnSc7	Le
Mairem	Mair	k1gMnSc7	Mair
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
kartografů	kartograf	k1gMnPc2	kartograf
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ostrovy	ostrov	k1gInPc4	ostrov
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Nova	nova	k1gFnSc1	nova
Zeelandia	Zeelandium	k1gNnPc4	Zeelandium
po	po	k7c6	po
nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
provincii	provincie	k1gFnSc6	provincie
Zeeland	Zeelanda	k1gFnPc2	Zeelanda
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
přesný	přesný	k2eAgInSc1d1	přesný
nynější	nynější	k2eAgInSc1d1	nynější
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
a	a	k8xC	a
název	název	k1gInSc4	název
asi	asi	k9	asi
zvolil	zvolit	k5eAaPmAgMnS	zvolit
kartograf	kartograf	k1gMnSc1	kartograf
Johan	Johan	k1gMnSc1	Johan
Blaeu	Blaea	k1gMnSc4	Blaea
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
cestovatel	cestovatel	k1gMnSc1	cestovatel
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
následovně	následovně	k6eAd1	následovně
poangličtil	poangličtit	k5eAaPmAgMnS	poangličtit
název	název	k1gInSc4	název
na	na	k7c4	na
New	New	k1gFnSc4	New
Zealand	Zealanda	k1gFnPc2	Zealanda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
Zealand	Zealanda	k1gFnPc2	Zealanda
pro	pro	k7c4	pro
dánský	dánský	k2eAgInSc4d1	dánský
ostrov	ostrov	k1gInSc4	ostrov
Sjæ	Sjæ	k1gFnSc2	Sjæ
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnSc1d2	starší
maorské	maorský	k2eAgNnSc1d1	maorské
jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
zní	znět	k5eAaImIp3nP	znět
Niu	Niu	k1gMnPc1	Niu
Tireni	Tiren	k2eAgMnPc1d1	Tiren
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zkomolený	zkomolený	k2eAgInSc1d1	zkomolený
přepis	přepis	k1gInSc1	přepis
anglického	anglický	k2eAgMnSc2d1	anglický
New	New	k1gMnSc2	New
Zealand	Zealand	k1gInSc4	Zealand
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
usadili	usadit	k5eAaPmAgMnP	usadit
první	první	k4xOgFnPc4	první
Maorové	Maorová	k1gFnPc4	Maorová
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
objevil	objevit	k5eAaPmAgInS	objevit
tyto	tento	k3xDgInPc4	tento
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Abel	Abel	k1gMnSc1	Abel
Janszoon	Janszoon	k1gMnSc1	Janszoon
Tasman	Tasman	k1gMnSc1	Tasman
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
návštěvník	návštěvník	k1gMnSc1	návštěvník
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
dorazil	dorazit	k5eAaPmAgMnS	dorazit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1769	[number]	k4	1769
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Maory	Maor	k1gMnPc7	Maor
existovala	existovat	k5eAaImAgFnS	existovat
válečnická	válečnický	k2eAgFnSc1d1	válečnická
kultura	kultura	k1gFnSc1	kultura
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
kanibalismem	kanibalismus	k1gInSc7	kanibalismus
a	a	k8xC	a
lovem	lov	k1gInSc7	lov
lebek	lebka	k1gFnPc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
domorodcům	domorodec	k1gMnPc3	domorodec
prodávali	prodávat	k5eAaImAgMnP	prodávat
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
mezikmenových	mezikmenová	k1gFnPc6	mezikmenová
"	"	kIx"	"
<g/>
mušketových	mušketový	k2eAgFnPc6d1	mušketový
válkách	válka	k1gFnPc6	válka
<g/>
"	"	kIx"	"
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1801	[number]	k4	1801
až	až	k9	až
1840	[number]	k4	1840
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
asi	asi	k9	asi
600	[number]	k4	600
bitvách	bitva	k1gFnPc6	bitva
a	a	k8xC	a
při	při	k7c6	při
nájezdech	nájezd	k1gInPc6	nájezd
až	až	k9	až
40	[number]	k4	40
000	[number]	k4	000
Maorů	Maor	k1gInPc2	Maor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
založili	založit	k5eAaPmAgMnP	založit
britští	britský	k2eAgMnPc1d1	britský
osadníci	osadník	k1gMnPc1	osadník
na	na	k7c6	na
Severním	severní	k2eAgInSc6d1	severní
ostrově	ostrov	k1gInSc6	ostrov
město	město	k1gNnSc1	město
Wellington	Wellington	k1gInSc1	Wellington
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
lukrativní	lukrativní	k2eAgMnSc1d1	lukrativní
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
mírné	mírný	k2eAgNnSc4d1	mírné
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
jako	jako	k8xC	jako
závislé	závislý	k2eAgNnSc1d1	závislé
území	území	k1gNnSc1	území
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
postoupení	postoupení	k1gNnSc3	postoupení
suverenity	suverenita	k1gFnSc2	suverenita
ovšem	ovšem	k9	ovšem
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
po	po	k7c6	po
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
s	s	k7c7	s
náčelníky	náčelník	k1gMnPc7	náčelník
domorodých	domorodý	k2eAgMnPc2d1	domorodý
Maorů	Maor	k1gMnPc2	Maor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
především	především	k6eAd1	především
díky	díky	k7c3	díky
misionářským	misionářský	k2eAgFnPc3d1	misionářská
snahám	snaha	k1gFnPc3	snaha
byli	být	k5eAaImAgMnP	být
uchráněni	uchráněn	k2eAgMnPc1d1	uchráněn
před	před	k7c7	před
vyhubením	vyhubení	k1gNnSc7	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
z	z	k7c2	z
Waitangi	Waitang	k1gFnSc2	Waitang
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
je	být	k5eAaImIp3nS	být
Maorům	Maor	k1gMnPc3	Maor
<g/>
,	,	kIx,	,
tvořícím	tvořící	k2eAgInSc6d1	tvořící
12	[number]	k4	12
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
zaručena	zaručen	k2eAgFnSc1d1	zaručena
rovnoprávnost	rovnoprávnost	k1gFnSc1	rovnoprávnost
<g/>
,	,	kIx,	,
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
také	také	k9	také
základem	základ	k1gInSc7	základ
novozélandské	novozélandský	k2eAgFnSc2d1	novozélandská
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
pevném	pevný	k2eAgNnSc6d1	pevné
spojení	spojení	k1gNnSc6	spojení
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
s	s	k7c7	s
metropolí	metropol	k1gFnSc7	metropol
mají	mít	k5eAaImIp3nP	mít
skotští	skotský	k2eAgMnPc1d1	skotský
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
zabírání	zabírání	k1gNnSc3	zabírání
půdy	půda	k1gFnSc2	půda
drobných	drobný	k2eAgMnPc2d1	drobný
rolníků	rolník	k1gMnPc2	rolník
velkostatkáři	velkostatkář	k1gMnSc3	velkostatkář
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
mateřské	mateřský	k2eAgFnSc6d1	mateřská
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
přivedli	přivést	k5eAaPmAgMnP	přivést
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
zem	zem	k1gFnSc4	zem
k	k	k7c3	k
prosperitě	prosperita	k1gFnSc3	prosperita
a	a	k8xC	a
hrdosti	hrdost	k1gFnSc3	hrdost
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
společenství	společenství	k1gNnSc4	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Jižním	jižní	k2eAgInSc6d1	jižní
ostrově	ostrov	k1gInSc6	ostrov
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
objeveno	objeven	k2eAgNnSc4d1	objeveno
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
vlna	vlna	k1gFnSc1	vlna
osadníků	osadník	k1gMnPc2	osadník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
přijít	přijít	k5eAaPmF	přijít
k	k	k7c3	k
nějakému	nějaký	k3yIgNnSc3	nějaký
jmění	jmění	k1gNnSc3	jmění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
osadníci	osadník	k1gMnPc1	osadník
a	a	k8xC	a
Maorové	Maor	k1gMnPc1	Maor
přeli	přít	k5eAaImAgMnP	přít
o	o	k7c4	o
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
krutým	krutý	k2eAgFnPc3d1	krutá
válkám	válka	k1gFnPc3	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
důsledku	důsledek	k1gInSc6	důsledek
Maorové	Maorové	k2eAgMnPc1d1	Maorové
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
zděděných	zděděný	k2eAgInPc2d1	zděděný
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
stal	stát	k5eAaPmAgInS	stát
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
však	však	k9	však
vázán	vázat	k5eAaImNgInS	vázat
silnými	silný	k2eAgInPc7d1	silný
politickými	politický	k2eAgInPc7d1	politický
a	a	k8xC	a
hospodářskými	hospodářský	k2eAgNnPc7d1	hospodářské
pojítky	pojítko	k1gNnPc7	pojítko
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
fakticky	fakticky	k6eAd1	fakticky
nezávislým	závislý	k2eNgInSc7d1	nezávislý
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
více	hodně	k6eAd2	hodně
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
státy	stát	k1gInPc7	stát
jižního	jižní	k2eAgNnSc2d1	jižní
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
maorských	maorský	k2eAgInPc2d1	maorský
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dnes	dnes	k6eAd1	dnes
znovu	znovu	k6eAd1	znovu
ožívá	ožívat	k5eAaImIp3nS	ožívat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
maorskou	maorský	k2eAgFnSc4d1	maorská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
izolován	izolovat	k5eAaBmNgMnS	izolovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
oddělených	oddělený	k2eAgInPc2d1	oddělený
Cookovým	Cookův	k2eAgInSc7d1	Cookův
průlivem	průliv	k1gInSc7	průliv
(	(	kIx(	(
<g/>
známých	známá	k1gFnPc2	známá
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k8xC	jako
Severní	severní	k2eAgInSc1d1	severní
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
četných	četný	k2eAgInPc2d1	četný
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
menších	malý	k2eAgInPc2d2	menší
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
Cookovy	Cookův	k2eAgInPc4d1	Cookův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Niue	Niue	k1gFnSc4	Niue
nebo	nebo	k8xC	nebo
Tokelau	Tokelaa	k1gFnSc4	Tokelaa
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
je	být	k5eAaImIp3nS	být
268	[number]	k4	268
680	[number]	k4	680
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
méně	málo	k6eAd2	málo
než	než	k8xS	než
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
pevninou	pevnina	k1gFnSc7	pevnina
je	být	k5eAaImIp3nS	být
2000	[number]	k4	2000
km	km	kA	km
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
nacházely	nacházet	k5eAaImAgFnP	nacházet
3	[number]	k4	3
lokality	lokalita	k1gFnPc1	lokalita
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
-	-	kIx~	-
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Tongariro	Tongariro	k1gNnSc4	Tongariro
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc4	Te
Wahipounamu	Wahipounam	k1gInSc2	Wahipounam
a	a	k8xC	a
novozélandské	novozélandský	k2eAgInPc4d1	novozélandský
subantarktické	subantarktický	k2eAgInPc4d1	subantarktický
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
hlavních	hlavní	k2eAgInPc6d1	hlavní
ostrovech	ostrov	k1gInPc6	ostrov
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc1	všechen
místa	místo	k1gNnPc1	místo
vzdálená	vzdálený	k2eAgNnPc1d1	vzdálené
maximálně	maximálně	k6eAd1	maximálně
130	[number]	k4	130
km	km	kA	km
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
vanoucí	vanoucí	k2eAgInPc1d1	vanoucí
od	od	k7c2	od
oceánu	oceán	k1gInSc2	oceán
zde	zde	k6eAd1	zde
udržují	udržovat	k5eAaImIp3nP	udržovat
mírné	mírný	k2eAgNnSc4d1	mírné
a	a	k8xC	a
vlhké	vlhký	k2eAgNnSc4d1	vlhké
podnebí	podnebí	k1gNnSc4	podnebí
s	s	k7c7	s
opravdu	opravdu	k6eAd1	opravdu
silnými	silný	k2eAgFnPc7d1	silná
srážkami	srážka	k1gFnPc7	srážka
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
je	být	k5eAaImIp3nS	být
horko	horko	k1gNnSc1	horko
a	a	k8xC	a
deštivo	deštivo	k1gNnSc1	deštivo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
a	a	k8xC	a
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
výškách	výška	k1gFnPc6	výška
je	být	k5eAaImIp3nS	být
chladněji	chladně	k6eAd2	chladně
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
pobřeží	pobřeží	k1gNnSc1	pobřeží
s	s	k7c7	s
několika	několik	k4yIc7	několik
poloostrovy	poloostrov	k1gInPc7	poloostrov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Úrodné	úrodný	k2eAgFnPc1d1	úrodná
nížiny	nížina	k1gFnPc1	nížina
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c6	v
kopce	kopka	k1gFnSc6	kopka
lemované	lemovaný	k2eAgFnPc1d1	lemovaná
zátokami	zátoka	k1gFnPc7	zátoka
a	a	k8xC	a
písčitými	písčitý	k2eAgFnPc7d1	písčitá
plážemi	pláž	k1gFnPc7	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
Severního	severní	k2eAgInSc2d1	severní
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblast	oblast	k1gFnSc1	oblast
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
činné	činný	k2eAgInPc1d1	činný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
bublajících	bublající	k2eAgInPc2d1	bublající
horkých	horký	k2eAgInPc2d1	horký
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Severního	severní	k2eAgInSc2d1	severní
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
sopka	sopka	k1gFnSc1	sopka
Ruapehu	Ruapeh	k1gInSc2	Ruapeh
(	(	kIx(	(
<g/>
2797	[number]	k4	2797
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
nejsou	být	k5eNaImIp3nP	být
ničím	ničí	k3xOyNgInSc7	ničí
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
rozeklané	rozeklaný	k2eAgInPc1d1	rozeklaný
kopce	kopec	k1gInPc1	kopec
svažují	svažovat	k5eAaImIp3nP	svažovat
a	a	k8xC	a
předcházejí	předcházet	k5eAaImIp3nP	předcházet
v	v	k7c4	v
roviny	rovina	k1gFnPc4	rovina
a	a	k8xC	a
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
nížiny	nížina	k1gFnPc4	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu	Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatah	k1gInSc2	Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatah
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
85	[number]	k4	85
písmen	písmeno	k1gNnPc2	písmeno
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Jižnímu	jižní	k2eAgInSc3d1	jižní
ostrovu	ostrov	k1gInSc3	ostrov
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
od	od	k7c2	od
Severního	severní	k2eAgNnSc2d1	severní
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
26	[number]	k4	26
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Cookův	Cookův	k2eAgInSc1d1	Cookův
průliv	průliv	k1gInSc1	průliv
<g/>
,	,	kIx,	,
dominují	dominovat	k5eAaImIp3nP	dominovat
nebetyčné	nebetyčný	k2eAgInPc1d1	nebetyčný
vrcholky	vrcholek	k1gInPc1	vrcholek
Jižních	jižní	k2eAgFnPc2d1	jižní
Alp	Alpy	k1gFnPc2	Alpy
se	s	k7c7	s
sněžnými	sněžný	k2eAgNnPc7d1	Sněžné
poli	pole	k1gNnPc7	pole
a	a	k8xC	a
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Jižního	jižní	k2eAgInSc2d1	jižní
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gMnSc1	Mount
Cook	Cook	k1gMnSc1	Cook
(	(	kIx(	(
<g/>
3754	[number]	k4	3754
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
nádherných	nádherný	k2eAgNnPc2d1	nádherné
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
zalesněných	zalesněný	k2eAgNnPc6d1	zalesněné
horských	horský	k2eAgNnPc6d1	horské
údolích	údolí	k1gNnPc6	údolí
<g/>
,	,	kIx,	,
napájejí	napájet	k5eAaImIp3nP	napájet
prudké	prudký	k2eAgFnPc1d1	prudká
řeky	řeka	k1gFnPc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Jižních	jižní	k2eAgFnPc2d1	jižní
Alp	Alpy	k1gFnPc2	Alpy
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
úrodná	úrodný	k2eAgFnSc1d1	úrodná
Canterburská	Canterburský	k2eAgFnSc1d1	Canterburská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
rovina	rovina	k1gFnSc1	rovina
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
do	do	k7c2	do
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
půdy	půda	k1gFnSc2	půda
zařezávají	zařezávat	k5eAaImIp3nP	zařezávat
v	v	k7c6	v
úzkých	úzký	k2eAgInPc6d1	úzký
pruzích	pruh	k1gInPc6	pruh
fjordy	fjord	k1gInPc1	fjord
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
skýtají	skýtat	k5eAaImIp3nP	skýtat
překrásnou	překrásný	k2eAgFnSc7d1	překrásná
podívanou	podívaná	k1gFnSc7	podívaná
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Pacifiku	Pacifik	k1gInSc6	Pacifik
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
2000	[number]	k4	2000
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
pevninou	pevnina	k1gFnSc7	pevnina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
souostroví	souostroví	k1gNnSc3	souostroví
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
<g/>
,	,	kIx,	,
Fidži	Fidž	k1gFnSc6	Fidž
a	a	k8xC	a
Tonga	Tonga	k1gFnSc1	Tonga
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podnebí	podnebí	k1gNnSc2	podnebí
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
oceánské	oceánský	k2eAgNnSc1d1	oceánské
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
podnebí	podnebí	k1gNnSc4	podnebí
subtropické	subtropický	k2eAgNnSc4d1	subtropické
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
Severní	severní	k2eAgFnSc3d1	severní
polokouli	polokoule	k1gFnSc3	polokoule
obrácená	obrácený	k2eAgFnSc1d1	obrácená
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejší	teplý	k2eAgInPc1d3	nejteplejší
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
leden	leden	k1gInSc4	leden
a	a	k8xC	a
únor	únor	k1gInSc4	únor
a	a	k8xC	a
nejchladnější	chladný	k2eAgInSc4d3	nejchladnější
červenec	červenec	k1gInSc4	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Severním	severní	k2eAgInSc6d1	severní
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
13	[number]	k4	13
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
mírné	mírný	k2eAgFnPc1d1	mírná
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgInPc1d1	převládající
západní	západní	k2eAgInPc1d1	západní
větry	vítr	k1gInPc1	vítr
přinášejí	přinášet	k5eAaImIp3nP	přinášet
celoročně	celoročně	k6eAd1	celoročně
bohaté	bohatý	k2eAgFnPc1d1	bohatá
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Izolovanost	izolovanost	k1gFnSc1	izolovanost
ostrovů	ostrov	k1gInPc2	ostrov
uchovala	uchovat	k5eAaPmAgFnS	uchovat
některé	některý	k3yIgInPc4	některý
endemické	endemický	k2eAgInPc4d1	endemický
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
se	se	k3xPyFc4	se
od	od	k7c2	od
Gondwany	Gondwana	k1gFnSc2	Gondwana
oddělil	oddělit	k5eAaPmAgMnS	oddělit
ještě	ještě	k9	ještě
před	před	k7c7	před
rozšířením	rozšíření	k1gNnSc7	rozšíření
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
před	před	k7c7	před
méně	málo	k6eAd2	málo
než	než	k8xS	než
900	[number]	k4	900
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
nevyskytovali	vyskytovat	k5eNaImAgMnP	vyskytovat
žádní	žádný	k3yNgMnPc1	žádný
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
sem	sem	k6eAd1	sem
mohli	moct	k5eAaImAgMnP	moct
připlavat	připlavat	k5eAaPmF	připlavat
(	(	kIx(	(
<g/>
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
,	,	kIx,	,
lvouni	lvoun	k1gMnPc1	lvoun
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přiletět	přiletět	k5eAaPmF	přiletět
(	(	kIx(	(
<g/>
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
nežijí	žít	k5eNaImIp3nP	žít
žádní	žádný	k3yNgMnPc1	žádný
hadi	had	k1gMnPc1	had
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
žádná	žádný	k3yNgNnPc4	žádný
jedovatá	jedovatý	k2eAgNnPc4d1	jedovaté
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
endemických	endemický	k2eAgFnPc2d1	endemická
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šarlatově	šarlatově	k6eAd1	šarlatově
kvetoucí	kvetoucí	k2eAgFnSc2d1	kvetoucí
pohutukawy	pohutukawa	k1gFnSc2	pohutukawa
a	a	k8xC	a
jerlínu	jerlín	k1gInSc2	jerlín
se	s	k7c7	s
zářivě	zářivě	k6eAd1	zářivě
žlutými	žlutý	k2eAgInPc7d1	žlutý
kvítky	kvítek	k1gInPc7	kvítek
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
porostlé	porostlý	k2eAgFnPc1d1	porostlá
lesem	les	k1gInSc7	les
nebo	nebo	k8xC	nebo
buší	buš	k1gFnSc7	buš
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
převládají	převládat	k5eAaImIp3nP	převládat
formy	forma	k1gFnPc1	forma
trsnatých	trsnatý	k2eAgFnPc2d1	trsnatá
trav	tráva	k1gFnPc2	tráva
na	na	k7c6	na
bažinatých	bažinatý	k2eAgFnPc6d1	bažinatá
plochách	plocha	k1gFnPc6	plocha
a	a	k8xC	a
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
zalesněna	zalesněn	k2eAgFnSc1d1	zalesněna
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgInPc1d1	stávající
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
vavřínových	vavřínový	k2eAgInPc2d1	vavřínový
a	a	k8xC	a
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
palmy	palma	k1gFnPc1	palma
a	a	k8xC	a
stromovité	stromovitý	k2eAgFnPc1d1	stromovitá
kapradiny	kapradina	k1gFnPc1	kapradina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jižním	jižní	k2eAgInSc6d1	jižní
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
pabukové	pabukový	k2eAgInPc1d1	pabukový
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
představitelem	představitel	k1gMnSc7	představitel
fauny	fauna	k1gFnSc2	fauna
je	být	k5eAaImIp3nS	být
kivi	kivi	k1gMnSc1	kivi
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neumějí	umět	k5eNaImIp3nP	umět
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
žily	žít	k5eAaImAgInP	žít
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
množství	množství	k1gNnSc4	množství
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
a	a	k8xC	a
několik	několik	k4yIc4	několik
druhů	druh	k1gMnPc2	druh
plazů	plaz	k1gInPc2	plaz
–	–	k?	–
gekoni	gekon	k1gMnPc1	gekon
a	a	k8xC	a
scinkové	scink	k1gMnPc1	scink
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
najdeme	najít	k5eAaPmIp1nP	najít
rovněž	rovněž	k9	rovněž
hatérii	hatérie	k1gFnSc4	hatérie
novozélandskou	novozélandský	k2eAgFnSc4d1	novozélandská
<g/>
,	,	kIx,	,
starodávného	starodávný	k2eAgMnSc4d1	starodávný
plaza	plaz	k1gMnSc4	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
opeřenci	opeřenec	k1gMnPc7	opeřenec
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
slípky	slípka	k1gFnPc1	slípka
takahe	takahe	k1gFnSc1	takahe
a	a	k8xC	a
papoušci	papoušek	k1gMnPc1	papoušek
kakapo	kakapa	k1gFnSc5	kakapa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
14	[number]	k4	14
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandské	novozélandský	k2eAgNnSc1d1	novozélandské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Realm	Realm	k1gInSc1	Realm
of	of	k?	of
New	New	k1gFnSc2	New
Zealand	Zealanda	k1gFnPc2	Zealanda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
královny	královna	k1gFnSc2	královna
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Cookovy	Cookův	k2eAgInPc4d1	Cookův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
Niue	Niue	k1gFnSc4	Niue
<g/>
,	,	kIx,	,
Tokelau	Tokelaa	k1gFnSc4	Tokelaa
a	a	k8xC	a
sporné	sporný	k2eAgNnSc4d1	sporné
území	území	k1gNnSc4	území
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
označované	označovaný	k2eAgFnSc6d1	označovaná
jako	jako	k8xS	jako
Rossova	Rossův	k2eAgFnSc1d1	Rossova
dependence	dependence	k1gFnSc1	dependence
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandské	novozélandský	k2eAgInPc1d1	novozélandský
regiony	region	k1gInPc1	region
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgNnSc7d1	základní
administrativním	administrativní	k2eAgNnSc7d1	administrativní
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandské	novozélandský	k2eAgInPc1d1	novozélandský
distrikty	distrikt	k1gInPc1	distrikt
jsou	být	k5eAaImIp3nP	být
druhým	druhý	k4xOgNnSc7	druhý
administrativním	administrativní	k2eAgNnSc7d1	administrativní
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
73	[number]	k4	73
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
je	být	k5eAaImIp3nS	být
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Human	Humany	k1gInPc2	Humany
Development	Development	k1gInSc4	Development
Index	index	k1gInSc1	index
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
časopisu	časopis	k1gInSc2	časopis
The	The	k1gMnSc1	The
Economist	Economist	k1gMnSc1	Economist
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
vláda	vláda	k1gFnSc1	vláda
země	zem	k1gFnSc2	zem
zaneprázdněna	zaneprázdněn	k2eAgFnSc1d1	zaneprázdněna
významnou	významný	k2eAgFnSc7d1	významná
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
restrukturalizací	restrukturalizace	k1gFnSc7	restrukturalizace
<g/>
,	,	kIx,	,
transformující	transformující	k2eAgInSc4d1	transformující
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
z	z	k7c2	z
vysoko	vysoko	k6eAd1	vysoko
protekcionistické	protekcionistický	k2eAgFnSc2d1	protekcionistická
a	a	k8xC	a
regulované	regulovaný	k2eAgFnSc2d1	regulovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
liberalizovanou	liberalizovaný	k2eAgFnSc4d1	liberalizovaná
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
privatizovala	privatizovat	k5eAaImAgFnS	privatizovat
vláda	vláda	k1gFnSc1	vláda
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
svých	svůj	k3xOyFgFnPc2	svůj
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
železničních	železniční	k2eAgFnPc2d1	železniční
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
finančních	finanční	k2eAgFnPc2d1	finanční
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
vláda	vláda	k1gFnSc1	vláda
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
mnoho	mnoho	k4c1	mnoho
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xS	jako
State-Owned	State-Owned	k1gInSc1	State-Owned
Enterprises	Enterprisesa	k1gFnPc2	Enterprisesa
(	(	kIx(	(
<g/>
státní	státní	k2eAgInPc1d1	státní
podniky	podnik	k1gInPc1	podnik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
podniky	podnik	k1gInPc1	podnik
jsou	být	k5eAaImIp3nP	být
řízeny	řízen	k2eAgInPc1d1	řízen
samostatnými	samostatný	k2eAgInPc7d1	samostatný
podniky	podnik	k1gInPc7	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
musí	muset	k5eAaImIp3nP	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
zisk	zisk	k1gInSc4	zisk
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
každá	každý	k3xTgFnSc1	každý
jiná	jiný	k2eAgFnSc1d1	jiná
soukromá	soukromý	k2eAgFnSc1d1	soukromá
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
náhlé	náhlý	k2eAgFnSc3d1	náhlá
transformaci	transformace	k1gFnSc3	transformace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
bublina	bublina	k1gFnSc1	bublina
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1987	[number]	k4	1987
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
trhu	trh	k1gInSc2	trh
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
ztracená	ztracený	k2eAgFnSc1d1	ztracená
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
této	tento	k3xDgFnSc2	tento
bubliny	bublina	k1gFnSc2	bublina
bylo	být	k5eAaImAgNnS	být
období	období	k1gNnSc1	období
nízkého	nízký	k2eAgInSc2d1	nízký
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
vládu	vláda	k1gFnSc4	vláda
také	také	k6eAd1	také
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
programu	program	k1gInSc2	program
masivní	masivní	k2eAgFnSc2d1	masivní
imigrace	imigrace	k1gFnSc2	imigrace
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
má	mít	k5eAaImIp3nS	mít
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
období	období	k1gNnSc2	období
relativního	relativní	k2eAgInSc2d1	relativní
silného	silný	k2eAgInSc2d1	silný
růstu	růst	k1gInSc2	růst
s	s	k7c7	s
jen	jen	k9	jen
slabými	slabý	k2eAgInPc7d1	slabý
inflačními	inflační	k2eAgInPc7d1	inflační
tlaky	tlak	k1gInPc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
priority	priorita	k1gFnPc1	priorita
vlády	vláda	k1gFnSc2	vláda
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
relativně	relativně	k6eAd1	relativně
nízké	nízký	k2eAgFnSc2d1	nízká
pozice	pozice	k1gFnSc2	pozice
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
uzavření	uzavření	k1gNnSc1	uzavření
smluv	smlouva	k1gFnPc2	smlouva
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
snižovaní	snižovaný	k2eAgMnPc1d1	snižovaný
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
budování	budování	k1gNnSc1	budování
ekonomiky	ekonomika	k1gFnSc2	ekonomika
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
vědomostech	vědomost	k1gFnPc6	vědomost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
začal	začít	k5eAaPmAgInS	začít
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
rozhovory	rozhovor	k1gInPc4	rozhovor
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
globálními	globální	k2eAgInPc7d1	globální
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
zpomaleními	zpomalení	k1gNnPc7	zpomalení
a	a	k8xC	a
propadem	propad	k1gInSc7	propad
cen	cena	k1gFnPc2	cena
komodit	komodita	k1gFnPc2	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
jsou	být	k5eAaImIp3nP	být
ceny	cena	k1gFnPc4	cena
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
kurzové	kurzový	k2eAgFnPc4d1	kurzová
směny	směna	k1gFnPc4	směna
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
spotřebitelského	spotřebitelský	k2eAgNnSc2d1	spotřebitelské
zboží	zboží	k1gNnSc2	zboží
je	být	k5eAaImIp3nS	být
dovážena	dovážen	k2eAgFnSc1d1	dovážena
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
směny	směna	k1gFnPc4	směna
novozélandského	novozélandský	k2eAgInSc2d1	novozélandský
dolaru	dolar	k1gInSc2	dolar
silný	silný	k2eAgInSc4d1	silný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
primární	primární	k2eAgNnSc1d1	primární
exportní	exportní	k2eAgNnSc1d1	exportní
odvětví	odvětví	k1gNnSc1	odvětví
jsou	být	k5eAaImIp3nP	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
hortikultura	hortikultura	k1gFnSc1	hortikultura
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc1	lesnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc1d1	informační
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
je	být	k5eAaImIp3nS	být
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
příroda	příroda	k1gFnSc1	příroda
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
turisty	turist	k1gMnPc4	turist
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc4d1	filmový
a	a	k8xC	a
vinařský	vinařský	k2eAgInSc4d1	vinařský
průmysl	průmysl	k1gInSc4	průmysl
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
je	být	k5eAaImIp3nS	být
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
obyvatele	obyvatel	k1gMnSc4	obyvatel
připadá	připadat	k5eAaImIp3nS	připadat
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
asi	asi	k9	asi
20	[number]	k4	20
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Ovce	ovce	k1gFnPc1	ovce
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
proslulého	proslulý	k2eAgNnSc2d1	proslulé
jehněčího	jehněčí	k1gNnSc2	jehněčí
a	a	k8xC	a
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyvážejí	vyvážet	k5eAaImIp3nP	vyvážet
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
dalším	další	k2eAgFnPc3d1	další
hlavním	hlavní	k2eAgFnPc3d1	hlavní
položkám	položka	k1gFnPc3	položka
vývozu	vývoz	k1gInSc2	vývoz
státu	stát	k1gInSc2	stát
-	-	kIx~	-
hovězímu	hovězí	k1gNnSc3	hovězí
<g/>
,	,	kIx,	,
mléku	mléko	k1gNnSc3	mléko
<g/>
,	,	kIx,	,
sýru	sýr	k1gInSc3	sýr
a	a	k8xC	a
máslu	máslo	k1gNnSc3	máslo
-	-	kIx~	-
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
navíc	navíc	k6eAd1	navíc
chovají	chovat	k5eAaImIp3nP	chovat
velká	velký	k2eAgNnPc1d1	velké
stáda	stádo	k1gNnPc1	stádo
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
nadstandardní	nadstandardní	k2eAgFnSc2d1	nadstandardní
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
početné	početný	k2eAgInPc1d1	početný
farmové	farmový	k2eAgInPc1d1	farmový
chovy	chov	k1gInPc1	chov
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pěstovaným	pěstovaný	k2eAgFnPc3d1	pěstovaná
plodinám	plodina	k1gFnPc3	plodina
patří	patřit	k5eAaImIp3nS	patřit
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
hrách	hrách	k1gInSc1	hrách
a	a	k8xC	a
jablka	jablko	k1gNnPc1	jablko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
počet	počet	k1gInSc1	počet
vinic	vinice	k1gFnPc2	vinice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
stále	stále	k6eAd1	stále
zelené	zelený	k2eAgInPc1d1	zelený
lesy	les	k1gInPc1	les
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
chráněné	chráněný	k2eAgInPc1d1	chráněný
<g/>
,	,	kIx,	,
pily	pila	k1gFnPc1	pila
a	a	k8xC	a
papírenský	papírenský	k2eAgInSc1d1	papírenský
průmysl	průmysl	k1gInSc1	průmysl
jsou	být	k5eAaImIp3nP	být
zásobovány	zásobovat	k5eAaImNgInP	zásobovat
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
kulturami	kultura	k1gFnPc7	kultura
jehličnanů	jehličnan	k1gInPc2	jehličnan
a	a	k8xC	a
eukalyptů	eukalypt	k1gInPc2	eukalypt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
takové	takový	k3xDgFnPc1	takový
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
hoki	hok	k1gFnSc2	hok
<g/>
,	,	kIx,	,
pražmy	pražma	k1gFnSc2	pražma
a	a	k8xC	a
barakudy	barakuda	k1gFnSc2	barakuda
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
korýši	korýš	k1gMnPc1	korýš
včetně	včetně	k7c2	včetně
slávek	slávka	k1gFnPc2	slávka
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandský	novozélandský	k2eAgInSc1d1	novozélandský
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
zdejších	zdejší	k2eAgInPc2d1	zdejší
mléčných	mléčný	k2eAgInPc2d1	mléčný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ostatním	ostatní	k2eAgNnPc3d1	ostatní
průmyslovým	průmyslový	k2eAgNnPc3d1	průmyslové
odvětvím	odvětví	k1gNnPc3	odvětví
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
výroba	výroba	k1gFnSc1	výroba
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
zužitkovává	zužitkovávat	k5eAaImIp3nS	zužitkovávat
své	svůj	k3xOyFgNnSc4	svůj
přírodní	přírodní	k2eAgNnSc4d1	přírodní
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zdejší	zdejší	k2eAgFnSc2d1	zdejší
prudké	prudký	k2eAgFnSc2d1	prudká
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
též	též	k9	též
zásobami	zásoba	k1gFnPc7	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
jsou	být	k5eAaImIp3nP	být
polynésští	polynésský	k2eAgMnPc1d1	polynésský
Maorové	Maor	k1gMnPc1	Maor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
připluli	připlout	k5eAaPmAgMnP	připlout
během	během	k7c2	během
9	[number]	k4	9
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Chathamových	Chathamový	k2eAgInPc6d1	Chathamový
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
Moriorové	Morior	k1gMnPc1	Morior
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
vyhlazeni	vyhlazen	k2eAgMnPc1d1	vyhlazen
Maory	Maora	k1gFnSc2	Maora
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
Novozélanďané	Novozélanďan	k1gMnPc1	Novozélanďan
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
74	[number]	k4	74
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
britského	britský	k2eAgInSc2d1	britský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Maorové	Maor	k1gMnPc1	Maor
(	(	kIx(	(
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
nedávno	nedávno	k6eAd1	nedávno
přistěhovalí	přistěhovalý	k2eAgMnPc1d1	přistěhovalý
Asiaté	Asiat	k1gMnPc1	Asiat
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
tichomořských	tichomořský	k2eAgInPc2d1	tichomořský
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
92	[number]	k4	92
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
7	[number]	k4	7
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
Maorové	Maor	k1gMnPc1	Maor
<g/>
.	.	kIx.	.
</s>
<s>
Negramotnost	negramotnost	k1gFnSc1	negramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
88	[number]	k4	88
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
75,5	[number]	k4	75,5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
81,6	[number]	k4	81,6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
párů	pár	k1gInPc2	pár
bez	bez	k7c2	bez
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
76	[number]	k4	76
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Auckland	Auckland	k1gInSc1	Auckland
–	–	k?	–
1,3	[number]	k4	1,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
Wellington	Wellington	k1gInSc1	Wellington
–	–	k?	–
369	[number]	k4	369
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
Christchurch	Christchurch	k1gInSc1	Christchurch
–	–	k?	–
364	[number]	k4	364
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hamilton	Hamilton	k1gInSc4	Hamilton
–	–	k?	–
188	[number]	k4	188
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
ragbyová	ragbyový	k2eAgFnSc1d1	ragbyová
reprezentace	reprezentace	k1gFnSc1	reprezentace
All	All	k1gMnPc2	All
Blacks	Blacksa	k1gFnPc2	Blacksa
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
veslování	veslování	k1gNnSc1	veslování
<g/>
.	.	kIx.	.
</s>
<s>
Skifař	skifař	k1gMnSc1	skifař
Mahé	Mahý	k2eAgFnSc3d1	Mahý
Drysdale	Drysdala	k1gFnSc3	Drysdala
je	být	k5eAaImIp3nS	být
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
vítězem	vítěz	k1gMnSc7	vítěz
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
z	z	k7c2	z
nepárové	párový	k2eNgFnSc2d1	nepárová
dvojky	dvojka	k1gFnSc2	dvojka
bez	bez	k7c2	bez
kormidelníka	kormidelník	k1gMnSc2	kormidelník
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
7	[number]	k4	7
let	léto	k1gNnPc2	léto
neporaženými	poražený	k2eNgNnPc7d1	neporažené
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
mají	mít	k5eAaImIp3nP	mít
2	[number]	k4	2
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
LOH	LOH	kA	LOH
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
všechny	všechen	k3xTgInPc4	všechen
rekordy	rekord	k1gInPc4	rekord
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
zajeté	zajetý	k2eAgNnSc1d1	zajeté
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
Rocket	Rocketa	k1gFnPc2	Rocketa
Lab	Lab	k1gFnSc1	Lab
vypustila	vypustit	k5eAaPmAgFnS	vypustit
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
raketu	raketa	k1gFnSc4	raketa
Electron	Electron	k1gInSc1	Electron
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
vynášet	vynášet	k5eAaImF	vynášet
tzv.	tzv.	kA	tzv.
CubeSaty	CubeSat	k1gInPc1	CubeSat
<g/>
,	,	kIx,	,
miniaturní	miniaturní	k2eAgInPc1d1	miniaturní
obdélníkové	obdélníkový	k2eAgInPc1d1	obdélníkový
satelity	satelit	k1gInPc1	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
raketu	raketa	k1gFnSc4	raketa
vypuštěnou	vypuštěný	k2eAgFnSc4d1	vypuštěná
z	z	k7c2	z
území	území	k1gNnSc2	území
Nového	Nového	k2eAgInSc2d1	Nového
Zélendu	Zélend	k1gInSc2	Zélend
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
komerčními	komerční	k2eAgMnPc7d1	komerční
lety	léto	k1gNnPc7	léto
pokračovat	pokračovat	k5eAaImF	pokračovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
důležitý	důležitý	k2eAgInSc4d1	důležitý
krok	krok	k1gInSc4	krok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
využívání	využívání	k1gNnSc2	využívání
vesmíru	vesmír	k1gInSc2	vesmír
soukromými	soukromý	k2eAgFnPc7d1	soukromá
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
