<s>
Jim	on	k3xPp3gMnPc3	on
Sturgess	Sturgess	k1gInSc1	Sturgess
<g/>
,	,	kIx,	,
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
James	James	k1gMnSc1	James
Anthony	Anthona	k1gFnSc2	Anthona
Sturgess	Sturgess	k1gInSc1	Sturgess
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
též	též	k9	též
věnuje	věnovat	k5eAaImIp3nS	věnovat
psaní	psaní	k1gNnSc4	psaní
textů	text	k1gInPc2	text
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
Červený	červený	k2eAgInSc1d1	červený
Bedrník	bedrník	k1gInSc1	bedrník
(	(	kIx(	(
<g/>
Erik	Erik	k1gMnSc1	Erik
–	–	k?	–
2	[number]	k4	2
epizody	epizoda	k1gFnSc2	epizoda
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
dvanáctého	dvanáctý	k4xOgInSc2	dvanáctý
(	(	kIx(	(
<g/>
Martin	Martin	k2eAgInSc1d1	Martin
Bannister	Bannister	k1gInSc1	Bannister
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
(	(	kIx(	(
<g/>
Red	Red	k1gFnSc1	Red
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Napříč	napříč	k7c7	napříč
vesmírem	vesmír	k1gInSc7	vesmír
(	(	kIx(	(
<g/>
Jude	Jude	k1gInSc1	Jude
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Králova	Králův	k2eAgInSc2d1	Králův
<g />
.	.	kIx.	.
</s>
<s>
přízeň	přízeň	k1gFnSc1	přízeň
(	(	kIx(	(
<g/>
George	George	k1gFnSc1	George
Boleyn	Boleyn	k1gMnSc1	Boleyn
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Oko	oko	k1gNnSc1	oko
bere	brát	k5eAaImIp3nS	brát
(	(	kIx(	(
<g/>
Ben	Ben	k1gInSc1	Ben
Campbell	Campbell	k1gMnSc1	Campbell
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Štvanec	štvanec	k1gMnSc1	štvanec
IRA	Ir	k1gMnSc2	Ir
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
McGartland	McGartlanda	k1gFnPc2	McGartlanda
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Heartless	Heartlessa	k1gFnPc2	Heartlessa
(	(	kIx(	(
<g/>
Jamie	Jamie	k1gFnSc1	Jamie
Morgan	morgan	k1gMnSc1	morgan
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Imigranti	imigrant	k1gMnPc1	imigrant
(	(	kIx(	(
<g/>
Gavin	Gavin	k1gMnSc1	Gavin
Kossef	Kossef	k1gMnSc1	Kossef
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Útěk	útěk	k1gInSc1	útěk
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
(	(	kIx(	(
<g/>
Janusz	Janusz	k1gInSc1	Janusz
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
Jeden	jeden	k4xCgInSc4	jeden
den	dna	k1gFnPc2	dna
(	(	kIx(	(
<g/>
Dexter	Dexter	k1gMnSc1	Dexter
Mayhew	Mayhew	k1gMnSc1	Mayhew
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
Atlas	Atlas	k1gInSc4	Atlas
mraků	mrak	k1gInPc2	mrak
(	(	kIx(	(
<g/>
Adam	Adam	k1gMnSc1	Adam
Ewing	Ewing	k1gMnSc1	Ewing
/	/	kIx~	/
chudý	chudý	k2eAgMnSc1d1	chudý
hotelový	hotelový	k2eAgMnSc1d1	hotelový
host	host	k1gMnSc1	host
/	/	kIx~	/
Meganin	Meganin	k2eAgMnSc1d1	Meganin
otec	otec	k1gMnSc1	otec
/	/	kIx~	/
Skot	Skot	k1gMnSc1	Skot
/	/	kIx~	/
Hae-Joo	Hae-Joo	k1gMnSc1	Hae-Joo
Chang	Chang	k1gMnSc1	Chang
/	/	kIx~	/
Adam	Adam	k1gMnSc1	Adam
/	/	kIx~	/
Zachryho	Zachry	k1gMnSc2	Zachry
švagr	švagr	k1gMnSc1	švagr
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
Paralelní	paralelní	k2eAgInPc1d1	paralelní
světy	svět	k1gInPc1	svět
(	(	kIx(	(
<g/>
Adam	Adam	k1gMnSc1	Adam
Kirk	Kirk	k1gMnSc1	Kirk
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
sovích	soví	k2eAgMnPc6d1	soví
strážcích	strážce	k1gMnPc6	strážce
(	(	kIx(	(
<g/>
Soren	Soren	k1gInSc1	Soren
<g/>
)	)	kIx)	)
</s>
