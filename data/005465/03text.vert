<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1939	[number]	k4	1939
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Velká	velký	k2eAgFnSc1d1	velká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
globální	globální	k2eAgInSc1d1	globální
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
probíhající	probíhající	k2eAgInSc1d1	probíhající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
probíhala	probíhat	k5eAaImAgFnS	probíhat
ve	v	k7c6	v
světových	světový	k2eAgInPc6d1	světový
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
stál	stát	k5eAaImAgInS	stát
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
a	a	k8xC	a
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
odvetou	odveta	k1gFnSc7	odveta
válku	válka	k1gFnSc4	válka
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozích	předchozí	k2eAgFnPc2d1	předchozí
smluv	smlouva	k1gFnPc2	smlouva
následovala	následovat	k5eAaImAgFnS	následovat
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
a	a	k8xC	a
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
konfliktu	konflikt	k1gInSc6	konflikt
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
většina	většina	k1gFnSc1	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
propukla	propuknout	k5eAaPmAgFnS	propuknout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
koalicemi	koalice	k1gFnPc7	koalice
<g/>
:	:	kIx,	:
mocnostmi	mocnost	k1gFnPc7	mocnost
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
Ústředními	ústřední	k2eAgFnPc7d1	ústřední
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Mocnostmi	mocnost	k1gFnPc7	mocnost
Dohody	dohoda	k1gFnPc1	dohoda
při	při	k7c6	při
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
války	válka	k1gFnSc2	válka
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
německého	německý	k2eAgInSc2d1	německý
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
carské	carský	k2eAgNnSc1d1	carské
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
K	k	k7c3
Dohodě	dohoda	k1gFnSc3
se	se	k3xPyFc4
postupně	postupně	k6eAd1
připojovaly	připojovat	k5eAaImAgInP
další	další	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
Japonsko	Japonsko	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
Rumunsko	Rumunsko	k1gNnSc1
a	a	k8xC
Portugalsko	Portugalsko	k1gNnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
a	a	k8xC
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Ústředními	ústřední	k2eAgFnPc7d1	ústřední
mocnostmi	mocnost	k1gFnPc7	mocnost
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Ústředním	ústřední	k2eAgFnPc3d1	ústřední
mocnostem	mocnost	k1gFnPc3	mocnost
se	se	k3xPyFc4	se
také	také	k6eAd1	také
přidala	přidat	k5eAaPmAgFnS	přidat
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
neutrálními	neutrální	k2eAgInPc7d1	neutrální
pouze	pouze	k6eAd1	pouze
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
státy	stát	k1gInPc1	stát
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc4	boj
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
na	na	k7c6	na
několika	několik	k4yIc6	několik
frontách	fronta	k1gFnPc6	fronta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
(	(	kIx(	(
<g/>
zákopová	zákopový	k2eAgFnSc1d1	zákopová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
až	až	k9	až
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
mobilizováno	mobilizovat	k5eAaBmNgNnS	mobilizovat
přes	přes	k7c4	přes
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
konec	konec	k1gInSc1	konec
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
udáván	udáván	k2eAgMnSc1d1	udáván
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
oslavován	oslavován	k2eAgMnSc1d1	oslavován
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
od	od	k7c2	od
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
zavládlo	zavládnout	k5eAaPmAgNnS	zavládnout
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
frontách	fronta	k1gFnPc6	fronta
příměří	příměří	k1gNnSc2	příměří
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
v	v	k7c4	v
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podepsané	podepsaný	k2eAgFnSc3d1	podepsaná
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c4	v
5.05	[number]	k4	5.05
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
německou	německý	k2eAgFnSc7d1	německá
generalitou	generalita	k1gFnSc7	generalita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
štábním	štábní	k2eAgInSc6d1	štábní
vagóně	vagón	k1gInSc6	vagón
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
dohodových	dohodový	k2eAgFnPc2d1	dohodová
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc4d1	francouzský
maršála	maršál	k1gMnSc4	maršál
Foche	Foch	k1gFnSc2	Foch
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
<g/>
.	.	kIx.	.
</s>
<s>
Formálním	formální	k2eAgNnSc7d1	formální
zakončením	zakončení	k1gNnSc7	zakončení
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
Pařížské	pařížský	k2eAgFnPc1d1	Pařížská
předměstské	předměstský	k2eAgFnPc1d1	předměstská
smlouvy	smlouva	k1gFnPc1	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Příčiny	příčina	k1gFnSc2	příčina
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Gavrilo	Gavrila	k1gFnSc5	Gavrila
Princip	princip	k1gInSc1	princip
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
a	a	k8xC	a
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
Františka	František	k1gMnSc2	František
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Gavrilo	Gavrit	k5eAaBmAgNnS	Gavrit
Princip	princip	k1gInSc1	princip
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
radikálního	radikální	k2eAgNnSc2d1	radikální
hnutí	hnutí	k1gNnSc2	hnutí
Mladá	mladý	k2eAgFnSc1d1	mladá
Bosna	Bosna	k1gFnSc1	Bosna
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
sjednocení	sjednocení	k1gNnSc1	sjednocení
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
Rakousko-Uhersku	Rakousko-Uherska	k1gFnSc4	Rakousko-Uherska
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nenalezeno	nalezen	k2eNgNnSc1d1	nenalezeno
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
zdroji	zdroj	k1gInSc6	zdroj
<g/>
]	]	kIx)	]
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
choť	choť	k1gFnSc4	choť
Žofii	Žofie	k1gFnSc4	Žofie
Chotkovou	Chotková	k1gFnSc4	Chotková
následně	následně	k6eAd1	následně
spustil	spustit	k5eAaPmAgInS	spustit
rychlý	rychlý	k2eAgInSc1d1	rychlý
sled	sled	k1gInSc1	sled
událostí	událost	k1gFnPc2	událost
vedoucích	vedoucí	k1gMnPc2	vedoucí
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
vlnu	vlna	k1gFnSc4	vlna
sympatií	sympatie	k1gFnPc2	sympatie
k	k	k7c3	k
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc3	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
z	z	k7c2	z
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
Srbsku	Srbsko	k1gNnSc6	Srbsko
radovala	radovat	k5eAaImAgFnS	radovat
a	a	k8xC	a
chápala	chápat	k5eAaImAgFnS	chápat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
vůči	vůči	k7c3	vůči
jakési	jakýsi	k3yIgFnSc3	jakýsi
podivné	podivný	k2eAgFnSc3d1	podivná
zemi	zem	k1gFnSc3	zem
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Původní	původní	k2eAgFnSc1d1	původní
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
bleskové	bleskový	k2eAgFnSc6d1	blesková
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
rozplynula	rozplynout	k5eAaPmAgFnS	rozplynout
a	a	k8xC	a
nadšení	nadšení	k1gNnPc1	nadšení
z	z	k7c2	z
války	válka	k1gFnSc2	válka
vystřídaly	vystřídat	k5eAaPmAgFnP	vystřídat
existenční	existenční	k2eAgFnPc1d1	existenční
obavy	obava	k1gFnPc1	obava
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
spojenecké	spojenecký	k2eAgInPc4d1	spojenecký
bloky	blok	k1gInPc4	blok
imperiálních	imperiální	k2eAgFnPc2d1	imperiální
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
doba	doba	k1gFnSc1	doba
vypjatého	vypjatý	k2eAgInSc2d1	vypjatý
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
těchto	tento	k3xDgInPc2	tento
bloků	blok	k1gInPc2	blok
položilo	položit	k5eAaPmAgNnS	položit
spojenectví	spojenectví	k1gNnSc1	spojenectví
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
<g/>
,	,	kIx,	,
Dvojspolek	dvojspolek	k1gInSc1	dvojspolek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
připojila	připojit	k5eAaPmAgFnS	připojit
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Trojspolek	trojspolek	k1gInSc1	trojspolek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
podepsaly	podepsat	k5eAaPmAgInP	podepsat
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
sblížily	sblížit	k5eAaPmAgFnP	sblížit
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
podepsaly	podepsat	k5eAaPmAgInP	podepsat
Srdečnou	srdečný	k2eAgFnSc4d1	srdečná
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dotvoření	dotvoření	k1gNnSc1	dotvoření
druhého	druhý	k4xOgInSc2	druhý
vojenského	vojenský	k2eAgInSc2d1	vojenský
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zakončilo	zakončit	k5eAaPmAgNnS	zakončit
podepsáním	podepsání	k1gNnSc7	podepsání
rusko-anglické	ruskonglický	k2eAgFnSc2d1	rusko-anglický
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
přisvojil	přisvojit	k5eAaPmAgMnS	přisvojit
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Hercegovinu	Hercegovina	k1gFnSc4	Hercegovina
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
usilovalo	usilovat	k5eAaImAgNnS	usilovat
také	také	k9	také
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
armáda	armáda	k1gFnSc1	armáda
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
velké	velký	k2eAgInPc4d1	velký
vojenské	vojenský	k2eAgInPc4d1	vojenský
manévry	manévr	k1gInPc4	manévr
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
se	se	k3xPyFc4	se
nechtěli	chtít	k5eNaImAgMnP	chtít
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
nedávným	dávný	k2eNgNnSc7d1	nedávné
připojením	připojení	k1gNnSc7	připojení
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc3	Rakousko-Uhersek
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
odboj	odboj	k1gInSc4	odboj
podporovalo	podporovat	k5eAaImAgNnS	podporovat
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Manévry	manévr	k1gInPc1	manévr
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
při	při	k7c6	při
srbských	srbský	k2eAgFnPc6d1	Srbská
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
jako	jako	k8xS	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
inspektor	inspektor	k1gMnSc1	inspektor
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
manévry	manévr	k1gInPc4	manévr
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
chotí	choť	k1gFnSc7	choť
<g/>
,	,	kIx,	,
Žofií	Žofie	k1gFnSc7	Žofie
Chotkovou	Chotková	k1gFnSc7	Chotková
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
závěr	závěr	k1gInSc4	závěr
manželský	manželský	k2eAgInSc1d1	manželský
pár	pár	k1gInSc1	pár
navštívil	navštívit	k5eAaPmAgInS	navštívit
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
na	na	k7c4	na
ně	on	k3xPp3gNnPc4	on
byl	být	k5eAaImAgMnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
spáchán	spáchat	k5eAaPmNgInS	spáchat
srbskými	srbský	k2eAgMnPc7d1	srbský
radikály	radikál	k1gMnPc7	radikál
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
ještě	ještě	k9	ještě
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
on	on	k3xPp3gMnSc1	on
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
následně	následně	k6eAd1	následně
zaslalo	zaslat	k5eAaPmAgNnS	zaslat
Srbsku	Srbsko	k1gNnSc3	Srbsko
ultimátum	ultimátum	k1gNnSc1	ultimátum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
formulováno	formulovat	k5eAaImNgNnS	formulovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
protistranou	protistrana	k1gFnSc7	protistrana
odmítnuto	odmítnut	k2eAgNnSc4d1	odmítnuto
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
tak	tak	k9	tak
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
byla	být	k5eAaImAgFnS	být
rakouská	rakouský	k2eAgFnSc1d1	rakouská
strana	strana	k1gFnSc1	strana
podporována	podporovat	k5eAaImNgFnS	podporovat
německou	německý	k2eAgFnSc7d1	německá
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
léto	léto	k1gNnSc4	léto
1914	[number]	k4	1914
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
okamžik	okamžik	k1gInSc4	okamžik
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
považovala	považovat	k5eAaImAgFnS	považovat
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
za	za	k7c4	za
nevyhnutelnou	vyhnutelný	k2eNgFnSc4d1	nevyhnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
formulované	formulovaný	k2eAgFnPc1d1	formulovaná
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Srbsko	Srbsko	k1gNnSc4	Srbsko
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nechtělo	chtít	k5eNaImAgNnS	chtít
ztratit	ztratit	k5eAaPmF	ztratit
suverenitu	suverenita	k1gFnSc4	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
udržovalo	udržovat	k5eAaImAgNnS	udržovat
blízké	blízký	k2eAgInPc4d1	blízký
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
a	a	k8xC	a
přijalo	přijmout	k5eAaPmAgNnS	přijmout
devět	devět	k4xCc1	devět
z	z	k7c2	z
deseti	deset	k4xCc2	deset
bodů	bod	k1gInPc2	bod
ultimáta	ultimátum	k1gNnSc2	ultimátum
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
požadavek	požadavek	k1gInSc4	požadavek
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
policií	policie	k1gFnSc7	policie
osoby	osoba	k1gFnSc2	osoba
podezřelé	podezřelý	k2eAgFnSc2d1	podezřelá
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
manželského	manželský	k2eAgInSc2d1	manželský
páru	pár	k1gInSc2	pár
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
Srbsku	Srbsko	k1gNnSc6	Srbsko
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
začalo	začít	k5eAaPmAgNnS	začít
jeho	jeho	k3xOp3gNnSc4	jeho
podunajské	podunajský	k2eAgNnSc4d1	podunajské
loďstvo	loďstvo	k1gNnSc4	loďstvo
ostřelovat	ostřelovat	k5eAaImF	ostřelovat
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
na	na	k7c6	na
základě	základ	k1gInSc6	základ
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
smluv	smlouva	k1gFnPc2	smlouva
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
rakouský	rakouský	k2eAgInSc4d1	rakouský
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc1	Srbsko
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
přípravné	přípravný	k2eAgFnSc2d1	přípravná
fáze	fáze	k1gFnSc2	fáze
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
částečné	částečný	k2eAgFnPc1d1	částečná
mobilizace	mobilizace	k1gFnPc1	mobilizace
třinácti	třináct	k4xCc2	třináct
armádních	armádní	k2eAgMnPc2d1	armádní
sborů	sbor	k1gInPc2	sbor
u	u	k7c2	u
rakouské	rakouský	k2eAgFnSc2d1	rakouská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
"	"	kIx"	"
<g/>
lokalizaci	lokalizace	k1gFnSc4	lokalizace
<g/>
"	"	kIx"	"
konfliktu	konflikt	k1gInSc2	konflikt
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
se	se	k3xPyFc4	se
rozplynula	rozplynout	k5eAaPmAgNnP	rozplynout
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
mobilizaci	mobilizace	k1gFnSc6	mobilizace
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
aliančních	alianční	k2eAgInPc2d1	alianční
bloků	blok	k1gInPc2	blok
–	–	k?	–
Trojdohodě	Trojdohoda	k1gFnSc3	Trojdohoda
a	a	k8xC	a
Trojspolku	trojspolek	k1gInSc2	trojspolek
spustil	spustit	k5eAaPmAgInS	spustit
tak	tak	k6eAd1	tak
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
vedoucí	vedoucí	k1gFnSc2	vedoucí
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
konfliktu	konflikt	k1gInSc6	konflikt
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
většina	většina	k1gFnSc1	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Německo	Německo	k1gNnSc1	Německo
válku	válka	k1gFnSc4	válka
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Francii	Francie	k1gFnSc4	Francie
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vstoupila	vstoupit	k5eAaPmAgNnP	vstoupit
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
do	do	k7c2	do
neutrální	neutrální	k2eAgFnSc2d1	neutrální
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
průchod	průchod	k1gInSc4	průchod
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Porušení	porušení	k1gNnSc1	porušení
neutrality	neutralita	k1gFnSc2	neutralita
Belgie	Belgie	k1gFnSc2	Belgie
dalo	dát	k5eAaPmAgNnS	dát
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
podnět	podnět	k1gInSc4	podnět
na	na	k7c6	na
vypovězení	vypovězení	k1gNnSc6	vypovězení
války	válka	k1gFnSc2	válka
Německu	Německo	k1gNnSc3	Německo
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
válku	válek	k1gInSc2	válek
Rusku	Ruska	k1gFnSc4	Ruska
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Rakousko-Uherskem	Rakousko-Uhersek	k1gInSc7	Rakousko-Uhersek
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc3	Rakousko-Uhersko
i	i	k8xC	i
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
navzdory	navzdory	k7c3	navzdory
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
smlouvě	smlouva	k1gFnSc3	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Trojspolek	trojspolek	k1gInSc1	trojspolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1917	[number]	k4	1917
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
mocností	mocnost	k1gFnPc2	mocnost
přidaly	přidat	k5eAaPmAgInP	přidat
i	i	k9	i
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
asijských	asijský	k2eAgFnPc2d1	asijská
zemí	zem	k1gFnPc2	zem
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Dohody	dohoda	k1gFnSc2	dohoda
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
zabralo	zabrat	k5eAaPmAgNnS	zabrat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
německé	německý	k2eAgNnSc4d1	německé
teritorium	teritorium	k1gNnSc4	teritorium
Čching-tao	Čchingao	k6eAd1	Čching-tao
a	a	k8xC	a
rakouský	rakouský	k2eAgInSc1d1	rakouský
Tchien-ťin	Tchien-ťin	k1gInSc1	Tchien-ťin
<g/>
)	)	kIx)	)
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
generálním	generální	k2eAgInSc6d1	generální
štábu	štáb	k1gInSc6	štáb
měli	mít	k5eAaImAgMnP	mít
vypracován	vypracován	k2eAgInSc4d1	vypracován
Schlieffenův	Schlieffenův	k2eAgInSc4d1	Schlieffenův
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
pomalou	pomalý	k2eAgFnSc4d1	pomalá
mobilizaci	mobilizace	k1gFnSc4	mobilizace
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc1	Německo
rychlým	rychlý	k2eAgInSc7d1	rychlý
manévrem	manévr	k1gInSc7	manévr
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
vniknout	vniknout	k5eAaPmF	vniknout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
obklíčit	obklíčit	k5eAaPmF	obklíčit
její	její	k3xOp3gNnPc4	její
vojska	vojsko	k1gNnPc4	vojsko
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
chtěli	chtít	k5eAaImAgMnP	chtít
němečtí	německý	k2eAgMnPc1d1	německý
generálové	generál	k1gMnPc1	generál
rychle	rychle	k6eAd1	rychle
přesunout	přesunout	k5eAaPmF	přesunout
jádro	jádro	k1gNnSc4	jádro
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
nerespektovalo	respektovat	k5eNaImAgNnS	respektovat
neutralitu	neutralita	k1gFnSc4	neutralita
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
a	a	k8xC	a
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podřídilo	podřídit	k5eAaPmAgNnS	podřídit
všechno	všechen	k3xTgNnSc1	všechen
rychlému	rychlý	k2eAgInSc3d1	rychlý
vojenskému	vojenský	k2eAgInSc3d1	vojenský
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
války	válka	k1gFnSc2	válka
stala	stát	k5eAaPmAgFnS	stát
rozhodujícím	rozhodující	k2eAgNnSc7d1	rozhodující
bojištěm	bojiště	k1gNnSc7	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Francouzským	francouzský	k2eAgInSc7d1	francouzský
vojskům	vojsko	k1gNnPc3	vojsko
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
kleští	kleště	k1gFnPc2	kleště
včas	včas	k6eAd1	včas
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
kladla	klást	k5eAaImAgFnS	klást
také	také	k9	také
nečekaně	nečekaně	k6eAd1	nečekaně
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
mezi	mezi	k7c7	mezi
Sedanem	sedan	k1gInSc7	sedan
a	a	k8xC	a
Verdunem	Verdun	k1gInSc7	Verdun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
po	po	k7c6	po
strategicky	strategicky	k6eAd1	strategicky
významné	významný	k2eAgFnSc6d1	významná
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Marně	marně	k6eAd1	marně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zázrak	zázrak	k1gInSc1	zázrak
na	na	k7c4	na
Marně	marně	k6eAd1	marně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
fronta	fronta	k1gFnSc1	fronta
zastavila	zastavit	k5eAaPmAgFnS	zastavit
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
o	o	k7c4	o
průlom	průlom	k1gInSc4	průlom
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
útok	útok	k1gInSc1	útok
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
zákopovou	zákopový	k2eAgFnSc4d1	zákopová
(	(	kIx(	(
<g/>
poziční	poziční	k2eAgFnSc4d1	poziční
<g/>
)	)	kIx)	)
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
plán	plán	k1gInSc4	plán
bleskové	bleskový	k2eAgFnSc2d1	blesková
války	válka	k1gFnSc2	válka
dostal	dostat	k5eAaPmAgMnS	dostat
první	první	k4xOgFnSc4	první
vážnou	vážný	k2eAgFnSc4d1	vážná
trhlinu	trhlina	k1gFnSc4	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
trhlina	trhlina	k1gFnSc1	trhlina
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
dokončila	dokončit	k5eAaPmAgFnS	dokončit
mobilizaci	mobilizace	k1gFnSc4	mobilizace
<g/>
,	,	kIx,	,
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
proti	proti	k7c3	proti
Východnímu	východní	k2eAgNnSc3d1	východní
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
,	,	kIx,	,
porazila	porazit	k5eAaPmAgNnP	porazit
slabá	slabý	k2eAgNnPc1d1	slabé
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
u	u	k7c2	u
Gumbinnene	Gumbinnen	k1gMnSc5	Gumbinnen
(	(	kIx(	(
<g/>
Gusevu	Gusev	k1gMnSc6	Gusev
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupovala	postupovat	k5eAaImAgFnS	postupovat
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
situaci	situace	k1gFnSc6	situace
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
německý	německý	k2eAgMnSc1d1	německý
císař	císař	k1gMnSc1	císař
jejím	její	k3xOp3gMnSc7	její
velitelem	velitel	k1gMnSc7	velitel
Paula	Paul	k1gMnSc2	Paul
von	von	k1gInSc4	von
Hindenburga	Hindenburg	k1gMnSc2	Hindenburg
a	a	k8xC	a
náčelníkem	náčelník	k1gInSc7	náčelník
štábu	štáb	k1gInSc2	štáb
Ericha	Erich	k1gMnSc4	Erich
Ludendorffa	Ludendorff	k1gMnSc4	Ludendorff
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
pět	pět	k4xCc4	pět
divizí	divize	k1gFnPc2	divize
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
fronty	fronta	k1gFnSc2	fronta
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Hindenburgovi	Hindenburg	k1gMnSc3	Hindenburg
a	a	k8xC	a
Ludendorffovi	Ludendorff	k1gMnSc3	Ludendorff
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tannenbergu	Tannenberg	k1gInSc2	Tannenberg
a	a	k8xC	a
Mazurských	mazurský	k2eAgNnPc2d1	mazurský
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
zničit	zničit	k5eAaPmF	zničit
ruskou	ruský	k2eAgFnSc4d1	ruská
2	[number]	k4	2
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
vytlačit	vytlačit	k5eAaPmF	vytlačit
tak	tak	k9	tak
Rusy	Rus	k1gMnPc4	Rus
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgInSc1d1	původní
německý	německý	k2eAgInSc1d1	německý
plán	plán	k1gInSc1	plán
bleskové	bleskový	k2eAgFnSc2d1	blesková
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nepodaří	podařit	k5eNaPmIp3nS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c6	na
rakousko-uherském	rakouskoherský	k2eAgInSc6d1	rakousko-uherský
úseku	úsek	k1gInSc6	úsek
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
na	na	k7c6	na
haličské	haličský	k2eAgFnSc6d1	Haličská
frontě	fronta	k1gFnSc6	fronta
postupovala	postupovat	k5eAaImAgFnS	postupovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
úvodním	úvodní	k2eAgInSc6d1	úvodní
nezdaru	nezdar	k1gInSc6	nezdar
u	u	k7c2	u
Krašniku	Krašnik	k1gInSc2	Krašnik
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1914	[number]	k4	1914
pronikla	proniknout	k5eAaPmAgFnS	proniknout
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Halič	Halič	k1gFnSc4	Halič
za	za	k7c7	za
Karpaty	Karpaty	k1gInPc7	Karpaty
na	na	k7c6	na
území	území	k1gNnSc6	území
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
během	během	k7c2	během
celé	celý	k2eAgFnSc2d1	celá
zimy	zima	k1gFnSc2	zima
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
pronikla	proniknout	k5eAaPmAgNnP	proniknout
ruská	ruský	k2eAgNnPc1d1	ruské
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c4	na
území	území	k1gNnSc4	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgNnP	obsadit
východoslovenská	východoslovenský	k2eAgNnPc1d1	Východoslovenské
města	město	k1gNnPc1	město
Bardejov	Bardejov	k1gInSc1	Bardejov
<g/>
,	,	kIx,	,
Svidník	Svidník	k1gInSc1	Svidník
<g/>
,	,	kIx,	,
Stropkov	Stropkov	k1gInSc1	Stropkov
<g/>
,	,	kIx,	,
Medzilaborce	Medzilaborka	k1gFnSc3	Medzilaborka
<g/>
,	,	kIx,	,
Sninu	Snina	k1gFnSc4	Snina
a	a	k8xC	a
Humenné	Humenné	k1gNnSc4	Humenné
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
slovenské	slovenský	k2eAgFnSc2d1	slovenská
inteligence	inteligence	k1gFnSc2	inteligence
tehdy	tehdy	k6eAd1	tehdy
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
osvobozením	osvobození	k1gNnSc7	osvobození
Slovenska	Slovensko	k1gNnSc2	Slovensko
Rusy	Rus	k1gMnPc4	Rus
a	a	k8xC	a
připravovala	připravovat	k5eAaImAgFnS	připravovat
vznik	vznik	k1gInSc4	vznik
česko-slovenského	českolovenský	k2eAgInSc2d1	česko-slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnPc3	jejichž
severovýchodním	severovýchodní	k2eAgFnPc3d1	severovýchodní
hranicím	hranice	k1gFnPc3	hranice
se	se	k3xPyFc4	se
Rusové	Rus	k1gMnPc1	Rus
též	též	k6eAd1	též
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
<g/>
)	)	kIx)	)
zase	zase	k9	zase
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
Milanem	Milan	k1gMnSc7	Milan
Rastislavem	Rastislav	k1gMnSc7	Rastislav
Štefánikem	Štefánik	k1gMnSc7	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
protiútokem	protiútok	k1gInSc7	protiútok
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
i	i	k9	i
Rakousko-Uhersku	Rakousko-Uherska	k1gFnSc4	Rakousko-Uherska
zatlačit	zatlačit	k5eAaPmF	zatlačit
ruskou	ruský	k2eAgFnSc4d1	ruská
armádu	armáda	k1gFnSc4	armáda
za	za	k7c4	za
Karpaty	Karpaty	k1gInPc4	Karpaty
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
i	i	k8xC	i
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
přešla	přejít	k5eAaPmAgFnS	přejít
válka	válka	k1gFnSc1	válka
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
do	do	k7c2	do
zákopů	zákop	k1gInPc2	zákop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
srbské	srbský	k2eAgFnSc6d1	Srbská
frontě	fronta	k1gFnSc6	fronta
mělo	mít	k5eAaImAgNnS	mít
zpočátku	zpočátku	k6eAd1	zpočátku
iniciativu	iniciativa	k1gFnSc4	iniciativa
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
první	první	k4xOgFnSc2	první
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
rakousko-uherská	rakouskoherský	k2eAgNnPc1d1	rakousko-uherské
vojska	vojsko	k1gNnPc4	vojsko
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
opět	opět	k6eAd1	opět
v	v	k7c6	v
září	září	k1gNnSc6	září
1914	[number]	k4	1914
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
zahájila	zahájit	k5eAaPmAgFnS	zahájit
srbská	srbský	k2eAgNnPc4d1	srbské
vojska	vojsko	k1gNnPc4	vojsko
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
protiútok	protiútok	k1gInSc4	protiútok
a	a	k8xC	a
útočníka	útočník	k1gMnSc2	útočník
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
až	až	k6eAd1	až
za	za	k7c4	za
srbské	srbský	k2eAgFnPc4d1	Srbská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Bilance	bilance	k1gFnPc1	bilance
prvních	první	k4xOgInPc2	první
měsíců	měsíc	k1gInPc2	měsíc
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ústřední	ústřední	k2eAgFnPc4d1	ústřední
mocnosti	mocnost	k1gFnPc4	mocnost
nepříznivá	příznivý	k2eNgFnSc1d1	nepříznivá
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
realizovat	realizovat	k5eAaBmF	realizovat
jejich	jejich	k3xOp3gInPc4	jejich
strategické	strategický	k2eAgInPc4d1	strategický
plány	plán	k1gInPc4	plán
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
bojovat	bojovat	k5eAaImF	bojovat
současně	současně	k6eAd1	současně
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
frontách	fronta	k1gFnPc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
námořním	námořní	k2eAgInSc6d1	námořní
incidentu	incident	k1gInSc6	incident
vyvolaném	vyvolaný	k2eAgInSc6d1	vyvolaný
Němci	Němec	k1gMnPc7	Němec
Dohoda	dohoda	k1gFnSc1	dohoda
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
válku	válka	k1gFnSc4	válka
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
(	(	kIx(	(
<g/>
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
Turecku	Turecko	k1gNnSc3	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
a	a	k8xC	a
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
záminka	záminka	k1gFnSc1	záminka
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohla	pomoct	k5eAaPmAgFnS	pomoct
centrálním	centrální	k2eAgFnPc3d1	centrální
mocnostem	mocnost	k1gFnPc3	mocnost
porazit	porazit	k5eAaPmF	porazit
její	její	k3xOp3gMnPc4	její
úhlavního	úhlavní	k2eAgMnSc2d1	úhlavní
nepřítele	nepřítel	k1gMnSc2	nepřítel
-	-	kIx~	-
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
a	a	k8xC	a
také	také	k9	také
aby	aby	kYmCp3nS	aby
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
nato	nato	k6eAd1	nato
překročila	překročit	k5eAaPmAgNnP	překročit
ruská	ruský	k2eAgNnPc1d1	ruské
vojska	vojsko	k1gNnPc1	vojsko
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
při	při	k7c6	při
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Arménii	Arménie	k1gFnSc6	Arménie
a	a	k8xC	a
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
(	(	kIx(	(
<g/>
turecká	turecký	k2eAgFnSc1d1	turecká
<g/>
)	)	kIx)	)
armáda	armáda	k1gFnSc1	armáda
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spustila	spustit	k5eAaPmAgFnS	spustit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
protiofenzívu	protiofenzíva	k1gFnSc4	protiofenzíva
a	a	k8xC	a
zatlačila	zatlačit	k5eAaPmAgNnP	zatlačit
ruská	ruský	k2eAgNnPc1d1	ruské
vojska	vojsko	k1gNnPc1	vojsko
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
zahájila	zahájit	k5eAaPmAgNnP	zahájit
osmanská	osmanský	k2eAgNnPc1d1	osmanské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
Envera	Enver	k1gMnSc2	Enver
Paši	paše	k1gFnSc4	paše
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
probila	probít	k5eAaPmAgFnS	probít
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Gruzie	Gruzie	k1gFnSc2	Gruzie
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
území	území	k1gNnSc1	území
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
okupující	okupující	k2eAgInPc1d1	okupující
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
probila	probít	k5eAaPmAgFnS	probít
přes	přes	k7c4	přes
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
až	až	k9	až
k	k	k7c3	k
Bagdádu	Bagdád	k1gInSc3	Bagdád
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
však	však	k9	však
byla	být	k5eAaImAgFnS	být
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
armádou	armáda	k1gFnSc7	armáda
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
dostat	dostat	k5eAaPmF	dostat
až	až	k9	až
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgNnSc4d1	těžké
ruské	ruský	k2eAgFnPc4d1	ruská
obranné	obranný	k2eAgFnPc4d1	obranná
linie	linie	k1gFnPc4	linie
ale	ale	k8xC	ale
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
odrazily	odrazit	k5eAaPmAgFnP	odrazit
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
1915	[number]	k4	1915
byla	být	k5eAaImAgFnS	být
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
zase	zase	k9	zase
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
kolapsu	kolaps	k1gInSc2	kolaps
této	tento	k3xDgFnSc2	tento
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
ofenzivy	ofenziva	k1gFnSc2	ofenziva
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
tuto	tento	k3xDgFnSc4	tento
ofenzivu	ofenziva	k1gFnSc4	ofenziva
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
100	[number]	k4	100
000	[number]	k4	000
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
málo	málo	k6eAd1	málo
k	k	k7c3	k
proražení	proražení	k1gNnSc3	proražení
ruských	ruský	k2eAgFnPc2d1	ruská
obranných	obranný	k2eAgFnPc2d1	obranná
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	s	k7c7	s
zatlačením	zatlačení	k1gNnSc7	zatlačení
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
armády	armáda	k1gFnSc2	armáda
Rusy	Rus	k1gMnPc4	Rus
k	k	k7c3	k
rusko	rusko	k6eAd1	rusko
-	-	kIx~	-
osmanským	osmanský	k2eAgFnPc3d1	Osmanská
hranicím	hranice	k1gFnPc3	hranice
se	se	k3xPyFc4	se
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
začala	začít	k5eAaPmAgFnS	začít
přebíjet	přebíjet	k5eAaImF	přebíjet
Rusy	Rus	k1gMnPc4	Rus
okupovaným	okupovaný	k2eAgInSc7d1	okupovaný
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
až	až	k9	až
za	za	k7c4	za
Urmijské	Urmijský	k2eAgNnSc4d1	Urmijský
jezero	jezero	k1gNnSc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
překročila	překročit	k5eAaPmAgFnS	překročit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Egyptě	Egypt	k1gInSc6	Egypt
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
při	při	k7c6	při
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
přebila	přebít	k5eAaPmAgFnS	přebít
až	až	k9	až
za	za	k7c4	za
nilskou	nilský	k2eAgFnSc4d1	nilská
deltu	delta	k1gFnSc4	delta
nedaleko	nedaleko	k7c2	nedaleko
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
zase	zase	k9	zase
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
přebila	přebít	k5eAaPmAgFnS	přebít
za	za	k7c4	za
jezero	jezero	k1gNnSc4	jezero
Van	vana	k1gFnPc2	vana
Goüü	Goüü	k1gFnPc2	Goüü
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc3	Rakousko-Uhersek
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
bývalému	bývalý	k2eAgMnSc3d1	bývalý
spojenci	spojenec	k1gMnSc3	spojenec
v	v	k7c6	v
Trojspolku	trojspolek	k1gInSc6	trojspolek
<g/>
)	)	kIx)	)
i	i	k9	i
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
<g/>
,	,	kIx,	,
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
tady	tady	k6eAd1	tady
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rakousko-uherská	rakouskoherský	k2eAgNnPc4d1	rakousko-uherské
vojska	vojsko	k1gNnPc4	vojsko
udržela	udržet	k5eAaPmAgFnS	udržet
obranné	obranný	k2eAgFnPc4d1	obranná
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Soči	Soči	k1gNnSc2	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
hlavní	hlavní	k2eAgNnSc1d1	hlavní
velení	velení	k1gNnSc1	velení
se	se	k3xPyFc4	se
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
východní	východní	k2eAgFnSc2d1	východní
protiofenzívy	protiofenzíva	k1gFnSc2	protiofenzíva
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
strategický	strategický	k2eAgInSc4d1	strategický
plán	plán	k1gInSc4	plán
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Hindenburga	Hindenburg	k1gMnSc2	Hindenburg
a	a	k8xC	a
Ludendorffa	Ludendorff	k1gMnSc2	Ludendorff
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1915	[number]	k4	1915
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
útok	útok	k1gInSc4	útok
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
rakousko-uherskými	rakouskoherský	k2eAgNnPc7d1	rakousko-uherské
vojsky	vojsko	k1gNnPc7	vojsko
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
německé	německý	k2eAgFnSc3d1	německá
armádě	armáda	k1gFnSc3	armáda
zasadit	zasadit	k5eAaPmF	zasadit
ruským	ruský	k2eAgFnPc3d1	ruská
jednotkám	jednotka	k1gFnPc3	jednotka
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
porazit	porazit	k5eAaPmF	porazit
ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
se	se	k3xPyFc4	se
však	však	k9	však
Německu	Německo	k1gNnSc3	Německo
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1915	[number]	k4	1915
se	se	k3xPyFc4	se
fronta	fronta	k1gFnSc1	fronta
ustálila	ustálit	k5eAaPmAgFnS	ustálit
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
Západní	západní	k2eAgFnSc1d1	západní
Dvina	Dvina	k1gFnSc1	Dvina
(	(	kIx(	(
<g/>
Daugava	Daugava	k1gFnSc1	Daugava
<g/>
)	)	kIx)	)
–	–	k?	–
Narošské	Narošský	k2eAgNnSc1d1	Narošský
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Strypa	Strypa	k1gFnSc1	Strypa
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
protékající	protékající	k2eAgFnSc1d1	protékající
také	také	k6eAd1	také
haličským	haličský	k2eAgMnPc3d1	haličský
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ukrajinským	ukrajinský	k2eAgInSc7d1	ukrajinský
Zborovem	Zborov	k1gInSc7	Zborov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začali	začít	k5eAaPmAgMnP	začít
Němci	Němec	k1gMnPc1	Němec
tajně	tajně	k6eAd1	tajně
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
ruské	ruský	k2eAgMnPc4d1	ruský
exilové	exilový	k2eAgMnPc4d1	exilový
bolševické	bolševický	k2eAgMnPc4d1	bolševický
revolucionáře	revolucionář	k1gMnPc4	revolucionář
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
padne	padnout	k5eAaImIp3nS	padnout
carský	carský	k2eAgInSc4d1	carský
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
muset	muset	k5eAaImF	muset
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
nadále	nadále	k6eAd1	nadále
válčit	válčit	k5eAaImF	válčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvolněné	uvolněný	k2eAgMnPc4d1	uvolněný
vojáky	voják	k1gMnPc4	voják
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
přesunout	přesunout	k5eAaPmF	přesunout
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnPc1d1	ústřední
mocnosti	mocnost	k1gFnPc1	mocnost
byly	být	k5eAaImAgFnP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
i	i	k9	i
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
do	do	k7c2	do
války	válka	k1gFnSc2	válka
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
straně	strana	k1gFnSc6	strana
toto	tento	k3xDgNnSc1	tento
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
soustředěný	soustředěný	k2eAgInSc4d1	soustředěný
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
srbských	srbský	k2eAgMnPc2d1	srbský
vojáků	voják	k1gMnPc2	voják
obsadily	obsadit	k5eAaPmAgFnP	obsadit
ústřední	ústřední	k2eAgFnPc1d1	ústřední
mocnosti	mocnost	k1gFnPc1	mocnost
do	do	k7c2	do
konce	konec	k1gInSc2	konec
listopadu	listopad	k1gInSc2	listopad
celé	celý	k2eAgNnSc4d1	celé
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1916	[number]	k4	1916
i	i	k8xC	i
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Balkánská	balkánský	k2eAgFnSc1d1	balkánská
fronta	fronta	k1gFnSc1	fronta
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
získalo	získat	k5eAaPmAgNnS	získat
přes	přes	k7c4	přes
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
přímý	přímý	k2eAgInSc1d1	přímý
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
strategické	strategický	k2eAgFnSc3d1	strategická
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
Dohodě	dohoda	k1gFnSc6	dohoda
nepodařil	podařit	k5eNaPmAgInS	podařit
útok	útok	k1gInSc1	útok
na	na	k7c4	na
dardanelskou	dardanelský	k2eAgFnSc4d1	dardanelský
úžinu	úžina	k1gFnSc4	úžina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Gallipoli	Gallipoli	k1gNnSc6	Gallipoli
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
operace	operace	k1gFnSc1	operace
hladinových	hladinový	k2eAgFnPc2d1	hladinová
sil	síla	k1gFnPc2	síla
koncentrovaly	koncentrovat	k5eAaBmAgInP	koncentrovat
do	do	k7c2	do
evropských	evropský	k2eAgFnPc2d1	Evropská
vod	voda	k1gFnPc2	voda
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
britské	britský	k2eAgFnSc2d1	britská
Royal	Royal	k1gInSc4	Royal
Navy	Navy	k?	Navy
zlikvidovalo	zlikvidovat	k5eAaPmAgNnS	zlikvidovat
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
a	a	k8xC	a
1915	[number]	k4	1915
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
německých	německý	k2eAgNnPc2d1	německé
plavidel	plavidlo	k1gNnPc2	plavidlo
operujících	operující	k2eAgMnPc2d1	operující
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1915	[number]	k4	1915
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
Dogger	Doggra	k1gFnPc2	Doggra
Banku	bank	k1gInSc2	bank
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
mezi	mezi	k7c7	mezi
britskými	britský	k2eAgInPc7d1	britský
a	a	k8xC	a
německými	německý	k2eAgInPc7d1	německý
bitevními	bitevní	k2eAgInPc7d1	bitevní
křižníky	křižník	k1gInPc7	křižník
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
početně	početně	k6eAd1	početně
slabší	slabý	k2eAgFnSc1d2	slabší
německá	německý	k2eAgFnSc1d1	německá
eskadra	eskadra	k1gFnSc1	eskadra
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
před	před	k7c7	před
úplným	úplný	k2eAgNnSc7d1	úplné
zničením	zničení	k1gNnSc7	zničení
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyváznout	vyváznout	k5eAaPmF	vyváznout
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
navíc	navíc	k6eAd1	navíc
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
k	k	k7c3	k
námořní	námořní	k2eAgFnSc3d1	námořní
blokádě	blokáda	k1gFnSc3	blokáda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
odříznutím	odříznutí	k1gNnPc3	odříznutí
Německa	Německo	k1gNnSc2	Německo
od	od	k7c2	od
nepostradatelných	postradatelný	k2eNgFnPc2d1	nepostradatelná
vojenských	vojenský	k2eAgFnPc2d1	vojenská
i	i	k8xC	i
nevojenských	vojenský	k2eNgFnPc2d1	nevojenská
dodávek	dodávka	k1gFnPc2	dodávka
projevila	projevit	k5eAaPmAgFnS	projevit
velmi	velmi	k6eAd1	velmi
efektivně	efektivně	k6eAd1	efektivně
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
blokádou	blokáda	k1gFnSc7	blokáda
porušila	porušit	k5eAaPmAgFnS	porušit
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
dané	daný	k2eAgNnSc4d1	dané
několika	několik	k4yIc7	několik
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
dohodami	dohoda	k1gFnPc7	dohoda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulých	minulý	k2eAgInPc2d1	minulý
dvou	dva	k4xCgInPc2	dva
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
zaminovala	zaminovat	k5eAaPmAgFnS	zaminovat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
vody	voda	k1gFnPc4	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
vstupu	vstup	k1gInSc6	vstup
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
lodi	loď	k1gFnSc2	loď
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
i	i	k8xC	i
pro	pro	k7c4	pro
neutrální	neutrální	k2eAgFnPc4d1	neutrální
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
na	na	k7c4	na
porušení	porušení	k1gNnSc4	porušení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
byla	být	k5eAaImAgFnS	být
minimální	minimální	k2eAgFnSc1d1	minimální
odezva	odezva	k1gFnSc1	odezva
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
očekávalo	očekávat	k5eAaImAgNnS	očekávat
stejnou	stejný	k2eAgFnSc4d1	stejná
odezvu	odezva	k1gFnSc4	odezva
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
neomezenou	omezený	k2eNgFnSc4d1	neomezená
ponorkovou	ponorkový	k2eAgFnSc4d1	ponorková
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
začátkem	začátek	k1gInSc7	začátek
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
dohody	dohoda	k1gFnSc2	dohoda
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
napadnout	napadnout	k5eAaPmF	napadnout
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
i	i	k9	i
výsadkem	výsadek	k1gInSc7	výsadek
vojsk	vojsko	k1gNnPc2	vojsko
Australsko	Australsko	k1gNnSc4	Australsko
-	-	kIx~	-
novozélandského	novozélandský	k2eAgInSc2d1	novozélandský
sboru	sbor	k1gInSc2	sbor
(	(	kIx(	(
<g/>
ANZAC	ANZAC	kA	ANZAC
<g/>
)	)	kIx)	)
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Gallipoli	Gallipoli	k1gNnSc2	Gallipoli
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
obsazením	obsazení	k1gNnSc7	obsazení
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
vojska	vojsko	k1gNnSc2	vojsko
ANZAC	ANZAC	kA	ANZAC
<g/>
,,	,,	k?	,,
vpochodovat	vpochodovat	k5eAaPmF	vpochodovat
"	"	kIx"	"
<g/>
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
-	-	kIx~	-
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
kontrolovaný	kontrolovaný	k2eAgInSc1d1	kontrolovaný
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
přes	přes	k7c4	přes
Dardanely	Dardanely	k1gFnPc4	Dardanely
<g/>
,	,	kIx,	,
zjednodušilo	zjednodušit	k5eAaPmAgNnS	zjednodušit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
odlehčená	odlehčený	k2eAgFnSc1d1	odlehčená
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
i	i	k9	i
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
srbském	srbský	k2eAgInSc6d1	srbský
frontě	fronta	k1gFnSc3	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
britského	britský	k2eAgNnSc2d1	Britské
společenství	společenství	k1gNnSc2	společenství
nepostupovali	postupovat	k5eNaImAgMnP	postupovat
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
v	v	k7c6	v
krvavých	krvavý	k2eAgInPc6d1	krvavý
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Dohodě	dohoda	k1gFnSc6	dohoda
nepřinesly	přinést	k5eNaPmAgInP	přinést
žádné	žádný	k3yNgInPc1	žádný
výsledky	výsledek	k1gInPc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
Dohody	dohoda	k1gFnSc2	dohoda
i	i	k8xC	i
německé	německý	k2eAgNnSc4d1	německé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
velení	velení	k1gNnSc4	velení
pochopily	pochopit	k5eAaPmAgInP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
musí	muset	k5eAaImIp3nS	muset
přinést	přinést	k5eAaPmF	přinést
západní	západní	k2eAgFnSc1d1	západní
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
proto	proto	k8xC	proto
plánovaly	plánovat	k5eAaImAgInP	plánovat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
frontě	fronta	k1gFnSc6	fronta
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1916	[number]	k4	1916
velké	velký	k2eAgFnSc2d1	velká
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
začala	začít	k5eAaPmAgFnS	začít
už	už	k6eAd1	už
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
směřovala	směřovat	k5eAaImAgFnS	směřovat
na	na	k7c4	na
pevnost	pevnost	k1gFnSc4	pevnost
Verdun	Verduna	k1gFnPc2	Verduna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
počítali	počítat	k5eAaImAgMnP	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francouzi	Francouz	k1gMnPc1	Francouz
budou	být	k5eAaImBp3nP	být
bránit	bránit	k5eAaImF	bránit
Verdun	Verdun	k1gInSc4	Verdun
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
prestiže	prestiž	k1gFnSc2	prestiž
až	až	k8xS	až
do	do	k7c2	do
posledního	poslední	k2eAgMnSc2d1	poslední
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
tak	tak	k6eAd1	tak
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
armádu	armáda	k1gFnSc4	armáda
nechat	nechat	k5eAaPmF	nechat
vykrvácet	vykrvácet	k5eAaPmF	vykrvácet
a	a	k8xC	a
otevřít	otevřít	k5eAaPmF	otevřít
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Úporné	úporný	k2eAgInPc1d1	úporný
boje	boj	k1gInPc1	boj
o	o	k7c4	o
Verdun	Verdun	k1gNnSc4	Verdun
však	však	k8xC	však
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nepřinesly	přinést	k5eNaPmAgFnP	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
ztratili	ztratit	k5eAaPmAgMnP	ztratit
315	[number]	k4	315
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
281	[number]	k4	281
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Verdun	Verdun	k1gNnSc4	Verdun
podnikla	podniknout	k5eAaPmAgFnS	podniknout
Dohoda	dohoda	k1gFnSc1	dohoda
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
ofenzívy	ofenzíva	k1gFnPc4	ofenzíva
–	–	k?	–
na	na	k7c6	na
východě	východ	k1gInSc6	východ
i	i	k8xC	i
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
anglofrancouzská	anglofrancouzský	k2eAgFnSc1d1	anglofrancouzský
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sommě	Somma	k1gFnSc6	Somma
–	–	k?	–
nejkrvavější	krvavý	k2eAgFnSc1d3	nejkrvavější
bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
padlo	padnout	k5eAaPmAgNnS	padnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
300	[number]	k4	300
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nepohnula	pohnout	k5eNaPmAgFnS	pohnout
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
použili	použít	k5eAaPmAgMnP	použít
Angličané	Angličan	k1gMnPc1	Angličan
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
poprvé	poprvé	k6eAd1	poprvé
tanky	tank	k1gInPc1	tank
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnější	úspěšný	k2eAgFnSc1d2	úspěšnější
byla	být	k5eAaImAgFnS	být
letní	letní	k2eAgFnSc1d1	letní
ruská	ruský	k2eAgFnSc1d1	ruská
Brusilovova	Brusilovův	k2eAgFnSc1d1	Brusilovova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
postoupila	postoupit	k5eAaPmAgFnS	postoupit
místy	místy	k6eAd1	místy
až	až	k9	až
o	o	k7c4	o
150	[number]	k4	150
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
na	na	k7c4	na
jihozápadně	jihozápadně	k6eAd1	jihozápadně
fronty	fronta	k1gFnPc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
zde	zde	k6eAd1	zde
získali	získat	k5eAaPmAgMnP	získat
východní	východní	k2eAgFnSc4d1	východní
Halič	Halič	k1gFnSc4	Halič
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Lešického	Lešický	k2eAgMnSc2d1	Lešický
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
také	také	k9	také
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
Bukovinu	Bukovina	k1gFnSc4	Bukovina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
válku	válka	k1gFnSc4	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Dohody	dohoda	k1gFnSc2	dohoda
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
do	do	k7c2	do
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
však	však	k9	však
ruské	ruský	k2eAgFnSc3d1	ruská
protiofenzívě	protiofenzíva	k1gFnSc3	protiofenzíva
nepomohl	pomoct	k5eNaPmAgInS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
roztáhla	roztáhnout	k5eAaPmAgFnS	roztáhnout
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
proniklo	proniknout	k5eAaPmAgNnS	proniknout
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rakousko-německá	rakouskoěmecký	k2eAgFnSc1d1	rakousko-německá
armáda	armáda	k1gFnSc1	armáda
Rumuny	Rumun	k1gMnPc4	Rumun
přinutila	přinutit	k5eAaPmAgFnS	přinutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
ústřední	ústřední	k2eAgFnPc1d1	ústřední
mocnosti	mocnost	k1gFnPc1	mocnost
obsadily	obsadit	k5eAaPmAgFnP	obsadit
skoro	skoro	k6eAd1	skoro
celé	celý	k2eAgNnSc4d1	celé
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
dobytí	dobytí	k1gNnSc2	dobytí
Bukurešti	Bukurešť	k1gFnSc2	Bukurešť
zemřel	zemřít	k5eAaPmAgMnS	zemřít
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
prasynovec	prasynovec	k1gMnSc1	prasynovec
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
boje	boj	k1gInPc1	boj
dalšími	další	k2eAgFnPc7d1	další
bitvami	bitva	k1gFnPc7	bitva
na	na	k7c6	na
Soči	Soči	k1gNnSc6	Soči
<g/>
,	,	kIx,	,
tvrdě	tvrdě	k6eAd1	tvrdě
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
i	i	k9	i
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
1916	[number]	k4	1916
se	se	k3xPyFc4	se
Anglii	Anglie	k1gFnSc3	Anglie
podařilo	podařit	k5eAaPmAgNnS	podařit
přimět	přimět	k5eAaPmF	přimět
německé	německý	k2eAgNnSc1d1	německé
loďstvo	loďstvo	k1gNnSc1	loďstvo
k	k	k7c3	k
dlouho	dlouho	k6eAd1	dlouho
očekávané	očekávaný	k2eAgFnSc3d1	očekávaná
konfrontaci	konfrontace	k1gFnSc3	konfrontace
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Jutska	Jutsko	k1gNnSc2	Jutsko
se	se	k3xPyFc4	se
početně	početně	k6eAd1	početně
slabší	slabý	k2eAgFnSc6d2	slabší
německé	německý	k2eAgFnSc6d1	německá
flotile	flotila	k1gFnSc6	flotila
podařilo	podařit	k5eAaPmAgNnS	podařit
vyváznout	vyváznout	k5eAaPmF	vyváznout
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Britové	Brit	k1gMnPc1	Brit
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnPc4d2	vyšší
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jádro	jádro	k1gNnSc1	jádro
obou	dva	k4xCgFnPc2	dva
loďstev	loďstvo	k1gNnPc2	loďstvo
nedotčeno	dotknout	k5eNaPmNgNnS	dotknout
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Početní	početní	k2eAgFnSc1d1	početní
převaha	převaha	k1gFnSc1	převaha
Royal	Royal	k1gMnSc1	Royal
Navy	Navy	k?	Navy
tak	tak	k9	tak
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
blokádě	blokáda	k1gFnSc6	blokáda
německého	německý	k2eAgNnSc2d1	německé
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
po	po	k7c6	po
šťastném	šťastný	k2eAgNnSc6d1	šťastné
vyváznutí	vyváznutí	k1gNnSc6	vyváznutí
u	u	k7c2	u
Jutska	Jutsko	k1gNnSc2	Jutsko
již	již	k6eAd1	již
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
konfrontaci	konfrontace	k1gFnSc3	konfrontace
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sil	síla	k1gFnPc2	síla
nenašlo	najít	k5eNaPmAgNnS	najít
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Frontové	frontový	k2eAgFnPc1d1	frontová
linie	linie	k1gFnPc1	linie
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
moc	moc	k6eAd1	moc
neměnily	měnit	k5eNaImAgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
nastala	nastat	k5eAaPmAgFnS	nastat
výraznější	výrazný	k2eAgFnSc1d2	výraznější
změna	změna	k1gFnSc1	změna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
přebila	přebít	k5eAaPmAgFnS	přebít
až	až	k9	až
k	k	k7c3	k
jižním	jižní	k2eAgInPc3d1	jižní
břehům	břeh	k1gInPc3	břeh
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
držela	držet	k5eAaImAgFnS	držet
linie	linie	k1gFnSc1	linie
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
severovýchodě	severovýchod	k1gInSc6	severovýchod
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
však	však	k9	však
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
defenzivním	defenzivní	k2eAgNnSc6d1	defenzivní
postavení	postavení	k1gNnSc6	postavení
dokázala	dokázat	k5eAaPmAgFnS	dokázat
tuto	tento	k3xDgFnSc4	tento
linii	linie	k1gFnSc4	linie
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
až	až	k9	až
začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
zahájila	zahájit	k5eAaPmAgFnS	zahájit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
další	další	k2eAgFnSc4d1	další
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Britského	britský	k2eAgInSc2d1	britský
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
až	až	k9	až
za	za	k7c4	za
Káhiru	Káhira	k1gFnSc4	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
dostala	dostat	k5eAaPmAgFnS	dostat
hlouběji	hluboko	k6eAd2	hluboko
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
Íránu	Írán	k1gInSc2	Írán
na	na	k7c6	na
východě	východ	k1gInSc6	východ
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
ruskou	ruský	k2eAgFnSc7d1	ruská
armádou	armáda	k1gFnSc7	armáda
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Egyptě	Egypt	k1gInSc6	Egypt
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
Osmanů	Osman	k1gMnPc2	Osman
nevypadalo	vypadat	k5eNaPmAgNnS	vypadat
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zdejší	zdejší	k2eAgFnSc1d1	zdejší
Britská	britský	k2eAgFnSc1d1	britská
armáda	armáda	k1gFnSc1	armáda
zatlačovala	zatlačovat	k5eAaImAgFnS	zatlačovat
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
armádu	armáda	k1gFnSc4	armáda
opět	opět	k6eAd1	opět
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
vyslat	vyslat	k5eAaPmF	vyslat
do	do	k7c2	do
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
své	svůj	k3xOyFgFnSc2	svůj
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
vojska	vojsko	k1gNnPc1	vojsko
ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c6	na
nepřátelském	přátelský	k2eNgNnSc6d1	nepřátelské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
už	už	k9	už
však	však	k9	však
síly	síla	k1gFnPc1	síla
na	na	k7c4	na
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Zdlouhavá	zdlouhavý	k2eAgFnSc1d1	zdlouhavá
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
izolace	izolace	k1gFnPc1	izolace
neposkytovaly	poskytovat	k5eNaImAgFnP	poskytovat
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
spojencům	spojenec	k1gMnPc3	spojenec
příznivé	příznivý	k2eAgFnPc4d1	příznivá
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
Dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
mírovým	mírový	k2eAgInSc7d1	mírový
návrhem	návrh	k1gInSc7	návrh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
Dohoda	dohoda	k1gFnSc1	dohoda
rozhodně	rozhodně	k6eAd1	rozhodně
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dohadování	dohadování	k1gNnSc1	dohadování
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
separátního	separátní	k2eAgInSc2d1	separátní
míru	mír	k1gInSc2	mír
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
nevedlo	vést	k5eNaImAgNnS	vést
však	však	k9	však
k	k	k7c3	k
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
výsledku	výsledek	k1gInSc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
úsilí	úsilí	k1gNnSc6	úsilí
překonat	překonat	k5eAaPmF	překonat
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
neomezenou	omezený	k2eNgFnSc4d1	neomezená
ponorkovou	ponorkový	k2eAgFnSc4d1	ponorková
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mimořádně	mimořádně	k6eAd1	mimořádně
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
jeho	jeho	k3xOp3gInPc4	jeho
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
úspěchů	úspěch	k1gInPc2	úspěch
i	i	k9	i
na	na	k7c6	na
mimoevropských	mimoevropský	k2eAgNnPc6d1	mimoevropské
bojištích	bojiště	k1gNnPc6	bojiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
na	na	k7c4	na
Německou	německý	k2eAgFnSc4d1	německá
východní	východní	k2eAgFnSc4d1	východní
Afriku	Afrika	k1gFnSc4	Afrika
obsadila	obsadit	k5eAaPmAgFnS	obsadit
všechny	všechen	k3xTgFnPc4	všechen
německé	německý	k2eAgFnPc4d1	německá
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
podnikla	podniknout	k5eAaPmAgFnS	podniknout
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
Turecku	Turecko	k1gNnSc3	Turecko
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
,	,	kIx,	,
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
a	a	k8xC	a
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
německé	německý	k2eAgInPc4d1	německý
a	a	k8xC	a
turecké	turecký	k2eAgFnPc4d1	turecká
síly	síla	k1gFnPc4	síla
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bojujících	bojující	k2eAgFnPc6d1	bojující
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
příznaky	příznak	k1gInPc4	příznak
vyčerpanosti	vyčerpanost	k1gFnSc2	vyčerpanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
revoluční	revoluční	k2eAgFnSc1d1	revoluční
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
v	v	k7c6	v
Únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
padl	padnout	k5eAaPmAgInS	padnout
nenáviděný	nenáviděný	k2eAgInSc1d1	nenáviděný
carismus	carismus	k1gInSc1	carismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
německé	německý	k2eAgFnSc2d1	německá
ponorkové	ponorkový	k2eAgFnSc2d1	ponorková
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Dohodou	dohoda	k1gFnSc7	dohoda
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
i	i	k8xC	i
USA	USA	kA	USA
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
vyslaly	vyslat	k5eAaPmAgFnP	vyslat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
své	svůj	k3xOyFgFnPc4	svůj
expediční	expediční	k2eAgFnPc4d1	expediční
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
velká	velký	k2eAgFnSc1d1	velká
materiální	materiální	k2eAgFnSc1d1	materiální
i	i	k8xC	i
lidská	lidský	k2eAgFnSc1d1	lidská
převaha	převaha	k1gFnSc1	převaha
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
neprojevila	projevit	k5eNaPmAgFnS	projevit
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
vojska	vojsko	k1gNnSc2	vojsko
Dohody	dohoda	k1gFnSc2	dohoda
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
(	(	kIx(	(
<g/>
Nivellova	Nivellův	k2eAgFnSc1d1	Nivellova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
spojené	spojený	k2eAgFnSc2d1	spojená
německo-rakousko-uherské	německoakouskoherský	k2eAgFnSc2d1	německo-rakousko-uherský
síly	síla	k1gFnSc2	síla
porazily	porazit	k5eAaPmAgFnP	porazit
italskou	italský	k2eAgFnSc4d1	italská
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Caporetta	Caporett	k1gInSc2	Caporett
a	a	k8xC	a
fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
až	až	k9	až
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Piavě	Piava	k1gFnSc6	Piava
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
1917	[number]	k4	1917
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Dohody	dohoda	k1gFnSc2	dohoda
i	i	k8xC	i
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gFnPc6	jehož
severních	severní	k2eAgFnPc6d1	severní
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nová	nový	k2eAgFnSc1d1	nová
balkánská	balkánský	k2eAgFnSc1d1	balkánská
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Únorová	únorový	k2eAgFnSc1d1	únorová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zaktivizovala	zaktivizovat	k5eAaPmAgFnS	zaktivizovat
i	i	k9	i
národně-osvobozenecké	národněsvobozenecký	k2eAgInPc4d1	národně-osvobozenecký
boje	boj	k1gInPc4	boj
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
nový	nový	k2eAgMnSc1d1	nový
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
totálně	totálně	k6eAd1	totálně
vyčerpaného	vyčerpaný	k2eAgInSc2d1	vyčerpaný
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
Otakar	Otakar	k1gMnSc1	Otakar
Černín	Černín	k1gMnSc1	Černín
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
říše	říše	k1gFnSc1	říše
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
uzavřít	uzavřít	k5eAaPmF	uzavřít
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
míru	mír	k1gInSc2	mír
s	s	k7c7	s
Dohodou	dohoda	k1gFnSc7	dohoda
bez	bez	k7c2	bez
Německa	Německo	k1gNnSc2	Německo
skončily	skončit	k5eAaPmAgFnP	skončit
bez	bez	k1gInSc4	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
požadovala	požadovat	k5eAaImAgFnS	požadovat
část	část	k1gFnSc1	část
rakousko-uherského	rakouskoherský	k2eAgNnSc2d1	rakousko-uherské
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
národů	národ	k1gInPc2	národ
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
požadujících	požadující	k2eAgFnPc6d1	požadující
u	u	k7c2	u
dohodových	dohodový	k2eAgInPc2d1	dohodový
států	stát	k1gInPc2	stát
odtržení	odtržení	k1gNnSc1	odtržení
nebo	nebo	k8xC	nebo
zánik	zánik	k1gInSc1	zánik
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
)	)	kIx)	)
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc4d1	československý
odboj	odboj	k1gInSc4	odboj
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgNnP	vznikat
česko-slovenská	českolovenský	k2eAgNnPc1d1	česko-slovenské
zahraniční	zahraniční	k2eAgNnPc1d1	zahraniční
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1917	[number]	k4	1917
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1918	[number]	k4	1918
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
bojovaly	bojovat	k5eAaImAgFnP	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
"	"	kIx"	"
<g/>
zasloužit	zasloužit	k5eAaPmF	zasloužit
<g/>
"	"	kIx"	"
Čechům	Čech	k1gMnPc3	Čech
a	a	k8xC	a
Slovákům	Slovák	k1gMnPc3	Slovák
vznik	vznik	k1gInSc4	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
se	se	k3xPyFc4	se
vyznamenaly	vyznamenat	k5eAaPmAgFnP	vyznamenat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1917	[number]	k4	1917
u	u	k7c2	u
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
vesnice	vesnice	k1gFnSc2	vesnice
Zborov	Zborov	k1gInSc1	Zborov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazily	porazit	k5eAaPmAgFnP	porazit
rakousko-uherskou	rakouskoherský	k2eAgFnSc4d1	rakousko-uherská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
československých	československý	k2eAgMnPc2d1	československý
legionářů	legionář	k1gMnPc2	legionář
a	a	k8xC	a
autorita	autorita	k1gFnSc1	autorita
československého	československý	k2eAgInSc2d1	československý
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bachmače	Bachmač	k1gInSc2	Bachmač
porazili	porazit	k5eAaPmAgMnP	porazit
Němce	Němec	k1gMnPc4	Němec
a	a	k8xC	a
donutili	donutit	k5eAaPmAgMnP	donutit
je	on	k3xPp3gInPc4	on
uzavřít	uzavřít	k5eAaPmF	uzavřít
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
chaotické	chaotický	k2eAgFnSc6d1	chaotická
situaci	situace	k1gFnSc6	situace
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
prvních	první	k4xOgInPc2	první
dekretů	dekret	k1gInPc2	dekret
–	–	k?	–
Dekretem	dekret	k1gInSc7	dekret
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
–	–	k?	–
obrátila	obrátit	k5eAaPmAgFnS	obrátit
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
bojující	bojující	k2eAgFnPc4d1	bojující
strany	strana	k1gFnPc4	strana
s	s	k7c7	s
výzvou	výzva	k1gFnSc7	výzva
uzavřít	uzavřít	k5eAaPmF	uzavřít
demokratický	demokratický	k2eAgInSc4d1	demokratický
mír	mír	k1gInSc4	mír
bez	bez	k7c2	bez
anexí	anexe	k1gFnPc2	anexe
a	a	k8xC	a
placení	placení	k1gNnSc2	placení
válečných	válečný	k2eAgFnPc2d1	válečná
náhrad	náhrada	k1gFnPc2	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Ustřední	Ustřední	k2eAgFnSc3d1	Ustřední
mocnosti	mocnost	k1gFnSc3	mocnost
kapitulaci	kapitulace	k1gFnSc3	kapitulace
Ruska	Rusko	k1gNnSc2	Rusko
přijaly	přijmout	k5eAaPmAgFnP	přijmout
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1917	[number]	k4	1917
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
jednání	jednání	k1gNnPc1	jednání
o	o	k7c6	o
mírové	mírový	k2eAgFnSc6d1	mírová
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Bolševická	bolševický	k2eAgFnSc1d1	bolševická
vláda	vláda	k1gFnSc1	vláda
nakonec	nakonec	k6eAd1	nakonec
podepsala	podepsat	k5eAaPmAgFnS	podepsat
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
v	v	k7c6	v
Brestu	Brest	k1gInSc6	Brest
brestlitevský	brestlitevský	k2eAgInSc4d1	brestlitevský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
se	se	k3xPyFc4	se
zaplatit	zaplatit	k5eAaPmF	zaplatit
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
znárodněné	znárodněný	k2eAgInPc4d1	znárodněný
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
kapitál	kapitál	k1gInSc1	kapitál
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
miliard	miliarda	k4xCgFnPc2	miliarda
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
odevzdat	odevzdat	k5eAaPmF	odevzdat
Německu	Německo	k1gNnSc6	Německo
245	[number]	k4	245
564	[number]	k4	564
kilogramů	kilogram	k1gInPc2	kilogram
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Bereziny	Berezina	k1gFnSc2	Berezina
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
donuceno	donucen	k2eAgNnSc1d1	donuceno
podepsat	podepsat	k5eAaPmF	podepsat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
fronta	fronta	k1gFnSc1	fronta
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgFnSc1	všechen
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
západní	západní	k2eAgFnSc4d1	západní
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Německo	Německo	k1gNnSc1	Německo
přesouvalo	přesouvat	k5eAaImAgNnS	přesouvat
své	své	k1gNnSc4	své
divize	divize	k1gFnSc2	divize
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
na	na	k7c4	na
italskou	italský	k2eAgFnSc4d1	italská
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zase	zase	k9	zase
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
své	své	k1gNnSc1	své
síly	síla	k1gFnSc2	síla
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
československý	československý	k2eAgInSc4d1	československý
odboj	odboj	k1gInSc4	odboj
měl	mít	k5eAaImAgInS	mít
tento	tento	k3xDgInSc4	tento
mír	mír	k1gInSc4	mír
negativní	negativní	k2eAgInPc4d1	negativní
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
československé	československý	k2eAgNnSc4d1	Československé
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
vydal	vydat	k5eAaPmAgMnS	vydat
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
svých	svůj	k3xOyFgInPc2	svůj
14	[number]	k4	14
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
poválečného	poválečný	k2eAgNnSc2d1	poválečné
uspořádání	uspořádání	k1gNnSc2	uspořádání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
odstoupení	odstoupení	k1gNnSc1	odstoupení
z	z	k7c2	z
války	válka	k1gFnSc2	válka
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
armádě	armáda	k1gFnSc3	armáda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
zaujmout	zaujmout	k5eAaPmF	zaujmout
veškerou	veškerý	k3xTgFnSc4	veškerý
pozornost	pozornost	k1gFnSc4	pozornost
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vojska	vojsko	k1gNnPc4	vojsko
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
postupovaly	postupovat	k5eAaImAgFnP	postupovat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Bagdád	Bagdád	k1gInSc4	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
známý	známý	k2eAgMnSc1d1	známý
britský	britský	k2eAgMnSc1d1	britský
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
voják	voják	k1gMnSc1	voják
T.	T.	kA	T.
E.	E.	kA	E.
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
pomohl	pomoct	k5eAaPmAgInS	pomoct
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1916	[number]	k4	1916
protiosmanské	protiosmanský	k2eAgFnSc6d1	protiosmanský
vzpouře	vzpoura	k1gFnSc6	vzpoura
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
Britové	Brit	k1gMnPc1	Brit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Araby	Arab	k1gMnPc7	Arab
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1917	[number]	k4	1917
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1917	[number]	k4	1917
získali	získat	k5eAaPmAgMnP	získat
Britové	Brit	k1gMnPc1	Brit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
i	i	k8xC	i
turecké	turecký	k2eAgFnPc1d1	turecká
síly	síla	k1gFnPc1	síla
byly	být	k5eAaImAgFnP	být
vytištěny	vytisknout	k5eAaPmNgFnP	vytisknout
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
vyčerpána	vyčerpán	k2eAgFnSc1d1	vyčerpána
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
armády	armáda	k1gFnSc2	armáda
vyslala	vyslat	k5eAaPmAgFnS	vyslat
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zásobovala	zásobovat	k5eAaImAgFnS	zásobovat
a	a	k8xC	a
zabránila	zabránit	k5eAaPmAgFnS	zabránit
Britskému	britský	k2eAgNnSc3d1	Britské
vojsku	vojsko	k1gNnSc3	vojsko
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
území	území	k1gNnSc4	území
říše	říš	k1gFnSc2	říš
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
totiž	totiž	k9	totiž
Írán	Írán	k1gInSc1	Írán
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
ho	on	k3xPp3gMnSc4	on
začali	začít	k5eAaPmAgMnP	začít
okupovat	okupovat	k5eAaBmF	okupovat
Britská	britský	k2eAgNnPc4d1	Britské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
ofenzívy	ofenzíva	k1gFnPc4	ofenzíva
nebo	nebo	k8xC	nebo
protiofenzivy	protiofenziva	k1gFnPc4	protiofenziva
se	se	k3xPyFc4	se
armáda	armáda	k1gFnSc1	armáda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
už	už	k6eAd1	už
nezmohla	zmoct	k5eNaPmAgFnS	zmoct
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
statečnému	statečný	k2eAgInSc3d1	statečný
boji	boj	k1gInSc3	boj
osmanských	osmanský	k2eAgMnPc2d1	osmanský
vojáků	voják	k1gMnPc2	voják
byla	být	k5eAaImAgFnS	být
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
zatlačována	zatlačovat	k5eAaImNgFnS	zatlačovat
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
československé	československý	k2eAgNnSc4d1	Československé
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
léta	léto	k1gNnSc2	léto
1918	[number]	k4	1918
československá	československý	k2eAgFnSc1d1	Československá
vojska	vojsko	k1gNnPc4	vojsko
obsadila	obsadit	k5eAaPmAgFnS	obsadit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Transsibiřskou	transsibiřský	k2eAgFnSc4d1	Transsibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
a	a	k8xC	a
dobyla	dobýt	k5eAaPmAgFnS	dobýt
všechna	všechen	k3xTgNnPc4	všechen
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
s	s	k7c7	s
maďarskými	maďarský	k2eAgMnPc7d1	maďarský
zajatci	zajatec	k1gMnPc7	zajatec
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
legionáři	legionář	k1gMnPc1	legionář
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
odzbrojit	odzbrojit	k5eAaPmF	odzbrojit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dohodlo	dohodnout	k5eAaPmAgNnS	dohodnout
Německo	Německo	k1gNnSc1	Německo
s	s	k7c7	s
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
si	se	k3xPyFc3	se
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
uznání	uznání	k1gNnSc1	uznání
dohodových	dohodový	k2eAgFnPc2d1	dohodová
mocností	mocnost	k1gFnPc2	mocnost
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
dohodové	dohodový	k2eAgFnPc1d1	dohodová
země	zem	k1gFnPc1	zem
začaly	začít	k5eAaPmAgFnP	začít
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
uznávat	uznávat	k5eAaImF	uznávat
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
jejího	její	k3xOp3gMnSc4	její
předsedu	předseda	k1gMnSc4	předseda
profesora	profesor	k1gMnSc4	profesor
T.G.	T.G.	k1gMnSc4	T.G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
jen	jen	k9	jen
krůček	krůček	k1gInSc4	krůček
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
již	již	k6eAd1	již
zachránit	zachránit	k5eAaPmF	zachránit
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
legií	legie	k1gFnPc2	legie
do	do	k7c2	do
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
vyvražděna	vyvraždit	k5eAaPmNgFnS	vyvraždit
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
příkaz	příkaz	k1gInSc4	příkaz
Rudých	rudý	k2eAgMnPc2d1	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Jarní	jarní	k2eAgFnSc1d1	jarní
německá	německý	k2eAgFnSc1d1	německá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
však	však	k9	však
přes	přes	k7c4	přes
přesun	přesun	k1gInSc4	přesun
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
neměla	mít	k5eNaImAgFnS	mít
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
Dohoda	dohoda	k1gFnSc1	dohoda
prolomila	prolomit	k5eAaPmAgFnS	prolomit
německou	německý	k2eAgFnSc4d1	německá
obranu	obrana	k1gFnSc4	obrana
mezi	mezi	k7c7	mezi
Albertem	Albert	k1gMnSc7	Albert
a	a	k8xC	a
Montdidierem	Montdidier	k1gMnSc7	Montdidier
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Arden	Ardeny	k1gFnPc2	Ardeny
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
francouzsko-britská	francouzskoritský	k2eAgNnPc1d1	francouzsko-britský
vojska	vojsko	k1gNnPc1	vojsko
valila	valit	k5eAaImAgNnP	valit
na	na	k7c4	na
Sedan	sedan	k1gInSc4	sedan
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
válku	válka	k1gFnSc4	válka
prohráli	prohrát	k5eAaPmAgMnP	prohrát
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
(	(	kIx(	(
<g/>
spojenci	spojenec	k1gMnPc1	spojenec
ho	on	k3xPp3gMnSc4	on
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
i	i	k9	i
rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
na	na	k7c6	na
Piavě	Piava	k1gFnSc6	Piava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Italové	Ital	k1gMnPc1	Ital
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
začali	začít	k5eAaPmAgMnP	začít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
útok	útok	k1gInSc4	útok
a	a	k8xC	a
rakousko-uherští	rakouskoherský	k2eAgMnPc1d1	rakousko-uherský
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
bezhlavý	bezhlavý	k2eAgInSc4d1	bezhlavý
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vznik	vznik	k1gInSc4	vznik
Československa	Československo	k1gNnSc2	Československo
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
vláda	vláda	k1gFnSc1	vláda
ještě	ještě	k9	ještě
podepsala	podepsat	k5eAaPmAgFnS	podepsat
příměří	příměří	k1gNnSc4	příměří
s	s	k7c7	s
Dohodou	dohoda	k1gFnSc7	dohoda
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
ve	v	k7c4	v
Villa	Vill	k1gMnSc4	Vill
Giusti	Giust	k1gFnSc2	Giust
u	u	k7c2	u
Padovy	Padova	k1gFnSc2	Padova
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
u	u	k7c2	u
Padovy	Padova	k1gFnSc2	Padova
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
mezi	mezi	k7c7	mezi
zmocněnci	zmocněnec	k1gMnPc7	zmocněnec
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
příměří	příměří	k1gNnSc2	příměří
došlo	dojít	k5eAaPmAgNnS	dojít
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c4	v
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
platnost	platnost	k1gFnSc4	platnost
začínala	začínat	k5eAaImAgFnS	začínat
od	od	k7c2	od
stejné	stejný	k2eAgFnSc2d1	stejná
hodiny	hodina	k1gFnSc2	hodina
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
poblíž	poblíž	k7c2	poblíž
Compiè	Compiè	k1gFnSc2	Compiè
začali	začít	k5eAaPmAgMnP	začít
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Dohody	dohoda	k1gFnSc2	dohoda
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
Němci	Němec	k1gMnSc3	Němec
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
Německý	německý	k2eAgMnSc1d1	německý
císař	císař	k1gMnSc1	císař
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
nátlakem	nátlak	k1gInSc7	nátlak
prohlášení	prohlášení	k1gNnSc4	prohlášení
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
všech	všecek	k3xTgInPc2	všecek
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
státních	státní	k2eAgFnPc2d1	státní
záležitostí	záležitost	k1gFnPc2	záležitost
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
císař	císař	k1gMnSc1	císař
opustil	opustit	k5eAaPmAgMnS	opustit
i	i	k9	i
Schönbrunnský	Schönbrunnský	k2eAgInSc4d1	Schönbrunnský
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
definitivně	definitivně	k6eAd1	definitivně
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Osamocené	osamocený	k2eAgNnSc1d1	osamocené
Německo	Německo	k1gNnSc1	Německo
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
Srbsku	Srbsko	k1gNnSc6	Srbsko
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
začalo	začít	k5eAaPmAgNnS	začít
ostřelování	ostřelování	k1gNnSc1	ostřelování
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
(	(	kIx(	(
<g/>
Srbská	srbský	k2eAgFnSc1d1	Srbská
kampaň	kampaň	k1gFnSc1	kampaň
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
tři	tři	k4xCgInPc1	tři
rakousko-uherské	rakouskoherský	k2eAgInPc1d1	rakousko-uherský
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
<g/>
,	,	kIx,	,
útočící	útočící	k2eAgNnPc1d1	útočící
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
pokaždé	pokaždé	k6eAd1	pokaždé
poražena	poražen	k2eAgNnPc1d1	poraženo
a	a	k8xC	a
zahnána	zahnán	k2eAgNnPc1d1	zahnáno
zpět	zpět	k6eAd1	zpět
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
současně	současně	k6eAd1	současně
rakousko-uherská	rakouskoherský	k2eAgNnPc1d1	rakousko-uherské
a	a	k8xC	a
bulharská	bulharský	k2eAgNnPc1d1	bulharské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
srbská	srbský	k2eAgFnSc1d1	Srbská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
zdecimovaná	zdecimovaný	k2eAgFnSc1d1	zdecimovaná
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
zbytky	zbytek	k1gInPc1	zbytek
byly	být	k5eAaImAgInP	být
přepraveny	přepraven	k2eAgInPc1d1	přepraven
po	po	k7c6	po
Jaderském	jaderský	k2eAgNnSc6d1	Jaderské
moři	moře	k1gNnSc6	moře
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Korfu	Korfu	k1gNnSc4	Korfu
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
byly	být	k5eAaImAgFnP	být
srbské	srbský	k2eAgFnPc1d1	Srbská
jednotky	jednotka	k1gFnPc1	jednotka
přepraveny	přepravit	k5eAaPmNgFnP	přepravit
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
na	na	k7c4	na
Soluňskou	soluňský	k2eAgFnSc4d1	soluňská
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
válku	válek	k1gInSc2	válek
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc2	Rakousku-Uhersek
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
1917	[number]	k4	1917
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
Řecko	Řecko	k1gNnSc1	Řecko
válku	válka	k1gFnSc4	válka
Centrálním	centrální	k2eAgFnPc3d1	centrální
mocnostem	mocnost	k1gFnPc3	mocnost
v	v	k7c6	v
září	září	k1gNnSc6	září
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Soluňské	soluňský	k2eAgFnSc6d1	soluňská
frontě	fronta	k1gFnSc6	fronta
zahájena	zahájen	k2eAgFnSc1d1	zahájena
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
bulharská	bulharský	k2eAgFnSc1d1	bulharská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
celé	celý	k2eAgFnSc2d1	celá
balkánské	balkánský	k2eAgFnSc2d1	balkánská
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
osvobození	osvobození	k1gNnSc2	osvobození
Srbska	Srbsko	k1gNnSc2	Srbsko
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Italská	italský	k2eAgFnSc1d1	italská
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byla	být	k5eAaImAgFnS	být
Itálie	Itálie	k1gFnSc1	Itálie
dříve	dříve	k6eAd2	dříve
spojencem	spojenec	k1gMnSc7	spojenec
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Trojspolku	trojspolek	k1gInSc2	trojspolek
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
války	válka	k1gFnSc2	válka
zprvu	zprvu	k6eAd1	zprvu
nevstoupila	vstoupit	k5eNaPmAgFnS	vstoupit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
neutrálním	neutrální	k2eAgInSc7d1	neutrální
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
se	se	k3xPyFc4	se
ale	ale	k9	ale
přidala	přidat	k5eAaPmAgFnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
za	za	k7c4	za
slib	slib	k1gInSc4	slib
území	území	k1gNnSc2	území
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
–	–	k?	–
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
(	(	kIx(	(
<g/>
Terst	Terst	k1gInSc1	Terst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
11	[number]	k4	11
bitev	bitva	k1gFnPc2	bitva
mezi	mezi	k7c7	mezi
italskou	italský	k2eAgFnSc7d1	italská
a	a	k8xC	a
rakousko-uherskou	rakouskoherský	k2eAgFnSc7d1	rakousko-uherská
armádou	armáda	k1gFnSc7	armáda
o	o	k7c4	o
řeku	řeka	k1gFnSc4	řeka
Soču	Sočus	k1gInSc2	Sočus
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Julských	Julský	k2eAgFnPc2d1	Julský
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Caporetta	Caporett	k1gInSc2	Caporett
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
fronta	fronta	k1gFnSc1	fronta
posunula	posunout	k5eAaPmAgFnS	posunout
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Piavě	Piava	k1gFnSc3	Piava
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
probíhala	probíhat	k5eAaImAgFnS	probíhat
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Piavě	Piava	k1gFnSc6	Piava
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
také	také	k9	také
ve	v	k7c6	v
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
prostorách	prostora	k1gFnPc6	prostora
Alp	Alpy	k1gFnPc2	Alpy
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
až	až	k9	až
po	po	k7c4	po
současné	současný	k2eAgFnPc4d1	současná
Slovinské	slovinský	k2eAgFnPc4d1	slovinská
Alpy	Alpy	k1gFnPc4	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Dějištěm	dějiště	k1gNnSc7	dějiště
bojů	boj	k1gInPc2	boj
byla	být	k5eAaImAgFnS	být
plošina	plošina	k1gFnSc1	plošina
Ortles	Ortlesa	k1gFnPc2	Ortlesa
<g/>
,	,	kIx,	,
Fleimstalské	Fleimstalský	k2eAgFnPc1d1	Fleimstalský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Dolomity	Dolomity	k1gInPc1	Dolomity
<g/>
,	,	kIx,	,
Karnské	Karnský	k2eAgFnPc1d1	Karnský
Alpy	Alpy	k1gFnPc1	Alpy
a	a	k8xC	a
Julské	Julský	k2eAgFnPc1d1	Julský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Západní	západní	k2eAgFnSc1d1	západní
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
přes	přes	k7c4	přes
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnulo	vyhnout	k5eAaPmAgNnS	vyhnout
francouzským	francouzský	k2eAgFnPc3d1	francouzská
pevnostem	pevnost	k1gFnPc3	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Schlieffenplan	Schlieffenplan	k1gInSc1	Schlieffenplan
<g/>
,	,	kIx,	,
plán	plán	k1gInSc1	plán
dobytí	dobytí	k1gNnSc2	dobytí
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
mohlo	moct	k5eAaImAgNnS	moct
následně	následně	k6eAd1	následně
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
se	se	k3xPyFc4	se
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
se	se	k3xPyFc4	se
však	však	k9	však
zastavila	zastavit	k5eAaPmAgFnS	zastavit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Marně	marně	k6eAd1	marně
kvůli	kvůli	k7c3	kvůli
protiútoku	protiútok	k1gInSc3	protiútok
Dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vedla	vést	k5eAaImAgFnS	vést
zákopová	zákopový	k2eAgFnSc1d1	zákopová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1915	[number]	k4	1915
–	–	k?	–
vážné	vážný	k2eAgNnSc4d1	vážné
střetnutí	střetnutí	k1gNnSc4	střetnutí
u	u	k7c2	u
belgického	belgický	k2eAgNnSc2d1	Belgické
města	město	k1gNnSc2	město
Ypry	Ypry	k1gInPc1	Ypry
(	(	kIx(	(
<g/>
použit	použit	k2eAgInSc1d1	použit
plyn	plyn	k1gInSc1	plyn
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
plyn	plyn	k1gInSc1	plyn
yperit	yperit	k1gInSc1	yperit
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
poprvé	poprvé	k6eAd1	poprvé
však	však	k9	však
byl	být	k5eAaImAgInS	být
plyn	plyn	k1gInSc1	plyn
použit	použit	k2eAgInSc1d1	použit
Němci	Němec	k1gMnPc7	Němec
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c4	o
xylylbromid	xylylbromid	k1gInSc4	xylylbromid
vystřelovaný	vystřelovaný	k2eAgInSc4d1	vystřelovaný
v	v	k7c6	v
150	[number]	k4	150
<g/>
mm	mm	kA	mm
granátech	granát	k1gInPc6	granát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vývojáři	vývojář	k1gMnPc1	vývojář
opomenuli	opomenout	k5eAaPmAgMnP	opomenout
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
xylylbromid	xylylbromida	k1gFnPc2	xylylbromida
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
,	,	kIx,	,
neodpařuje	odpařovat	k5eNaImIp3nS	odpařovat
se	se	k3xPyFc4	se
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
tudíž	tudíž	k8xC	tudíž
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
účinek	účinek	k1gInSc4	účinek
únor	únor	k1gInSc1	únor
1916	[number]	k4	1916
–	–	k?	–
zahájena	zahájen	k2eAgFnSc1d1	zahájena
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Verdunu	Verdun	k1gInSc2	Verdun
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Sommě	Somma	k1gFnSc6	Somma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
využili	využít	k5eAaPmAgMnP	využít
Britové	Brit	k1gMnPc1	Brit
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
tank	tank	k1gInSc1	tank
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
Nivellova	Nivellův	k2eAgFnSc1d1	Nivellova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
dubnu	duben	k1gInSc6	duben
nic	nic	k3yNnSc4	nic
nevyřešila	vyřešit	k5eNaPmAgFnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cambrai	Cambra	k1gFnSc2	Cambra
byla	být	k5eAaImAgFnS	být
úspěšnější	úspěšný	k2eAgFnSc1d2	úspěšnější
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
poprvé	poprvé	k6eAd1	poprvé
využili	využít	k5eAaPmAgMnP	využít
tanky	tank	k1gInPc4	tank
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
pěchotou	pěchota	k1gFnSc7	pěchota
německá	německý	k2eAgFnSc1d1	německá
Ludendorffova	Ludendorffův	k2eAgFnSc1d1	Ludendorffova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
útoky	útok	k1gInPc4	útok
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
června	červen	k1gInSc2	červen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
ohrozila	ohrozit	k5eAaPmAgFnS	ohrozit
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
na	na	k7c4	na
Marně	marně	k6eAd1	marně
zahnala	zahnat	k5eAaPmAgFnS	zahnat
Němce	Němec	k1gMnSc4	Němec
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Východní	východní	k2eAgFnSc1d1	východní
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
Pjotr	Pjotr	k1gMnSc1	Pjotr
Durnovo	Durnův	k2eAgNnSc1d1	Durnovo
napsal	napsat	k5eAaPmAgInS	napsat
carovi	car	k1gMnSc3	car
memorandum	memorandum	k1gNnSc4	memorandum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
zkázu	zkáza	k1gFnSc4	zkáza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
překvapivě	překvapivě	k6eAd1	překvapivě
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
zmobilizovala	zmobilizovat	k5eAaPmAgFnS	zmobilizovat
a	a	k8xC	a
podnikla	podniknout	k5eAaPmAgFnS	podniknout
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Východní	východní	k2eAgNnSc4d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
<g/>
.	.	kIx.	.
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
bojích	boj	k1gInPc6	boj
porazila	porazit	k5eAaPmAgFnS	porazit
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
Rusy	Rus	k1gMnPc4	Rus
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tannenbergu	Tannenberg	k1gInSc2	Tannenberg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
fronty	fronta	k1gFnSc2	fronta
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Krašniku	Krašnik	k1gInSc2	Krašnik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
poražena	porazit	k5eAaPmNgFnS	porazit
rakousko-uherskou	rakouskoherský	k2eAgFnSc7d1	rakousko-uherská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Halič	Halič	k1gFnSc4	Halič
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
na	na	k7c4	na
východní	východní	k2eAgNnSc4d1	východní
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gorlice	Gorlice	k1gFnSc2	Gorlice
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byla	být	k5eAaImAgFnS	být
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
stála	stát	k5eAaImAgNnP	stát
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
<g />
.	.	kIx.	.
</s>
<s>
měst	město	k1gNnPc2	město
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Baranoviči	Baranovič	k1gMnPc7	Baranovič
<g/>
,	,	kIx,	,
Stripa	Stripa	k1gFnSc1	Stripa
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1916	[number]	k4	1916
začala	začít	k5eAaPmAgFnS	začít
Brusilovova	Brusilovův	k2eAgFnSc1d1	Brusilovova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
Rusko	Rusko	k1gNnSc1	Rusko
dobylo	dobýt	k5eAaPmAgNnS	dobýt
Bukovinu	Bukovina	k1gFnSc4	Bukovina
a	a	k8xC	a
část	část	k1gFnSc4	část
Haliče	Halič	k1gFnSc2	Halič
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
odražena	odrazit	k5eAaPmNgFnS	odrazit
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
musela	muset	k5eAaImAgFnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g />
.	.	kIx.	.
</s>
<s>
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
zahájily	zahájit	k5eAaPmAgFnP	zahájit
Centrální	centrální	k2eAgFnPc4d1	centrální
mocnosti	mocnost	k1gFnPc4	mocnost
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Rusko	Rusko	k1gNnSc1	Rusko
Brestlitevský	brestlitevský	k2eAgInSc4d1	brestlitevský
mír	mír	k1gInSc4	mír
Bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
Německu	Německo	k1gNnSc6	Německo
patřily	patřit	k5eAaImAgInP	patřit
ostrovy	ostrov	k1gInPc1	ostrov
severně	severně	k6eAd1	severně
od	od	k7c2	od
Austrálie	Austrálie	k1gFnSc2	Austrálie
–	–	k?	–
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
,	,	kIx,	,
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
–	–	k?	–
kromě	kromě	k7c2	kromě
ponorkové	ponorkový	k2eAgFnSc2d1	ponorková
války	válka	k1gFnSc2	válka
zejména	zejména	k9	zejména
okolo	okolo	k7c2	okolo
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Indonésii	Indonésie	k1gFnSc6	Indonésie
a	a	k8xC	a
vzpomínaných	vzpomínaný	k2eAgInPc6d1	vzpomínaný
německých	německý	k2eAgInPc6d1	německý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
měli	mít	k5eAaImAgMnP	mít
dále	daleko	k6eAd2	daleko
čtyři	čtyři	k4xCgFnPc4	čtyři
kolonie	kolonie	k1gFnPc4	kolonie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmocňovaly	zmocňovat	k5eAaImAgInP	zmocňovat
státy	stát	k1gInPc1	stát
Dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
data	datum	k1gNnSc2	datum
německé	německý	k2eAgFnSc2d1	německá
kapitulace	kapitulace	k1gFnSc2	kapitulace
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Togo	Togo	k1gNnSc1	Togo
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc1	srpen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
významným	významný	k2eAgNnSc7d1	významné
bojištěm	bojiště	k1gNnSc7	bojiště
byl	být	k5eAaImAgInS	být
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
námořním	námořní	k2eAgInSc6d1	námořní
incidentu	incident	k1gInSc6	incident
vyvolaném	vyvolaný	k2eAgMnSc6d1	vyvolaný
Němci	Němec	k1gMnSc6	Němec
Dohoda	dohoda	k1gFnSc1	dohoda
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1914	[number]	k4	1914
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
válku	válka	k1gFnSc4	válka
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
a	a	k8xC	a
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
střetnutím	střetnutí	k1gNnSc7	střetnutí
mezi	mezi	k7c7	mezi
Dohodou	dohoda	k1gFnSc7	dohoda
a	a	k8xC	a
Turky	Turek	k1gMnPc4	Turek
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
Dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
obsazení	obsazení	k1gNnSc4	obsazení
nebo	nebo	k8xC	nebo
zničení	zničení	k1gNnSc4	zničení
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
baterie	baterie	k1gFnPc4	baterie
na	na	k7c6	na
Dardanelách	Dardanely	k1gFnPc6	Dardanely
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
Dardanelskou	dardanelský	k2eAgFnSc4d1	dardanelský
expedici	expedice	k1gFnSc4	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Gallipoli	Gallipoli	k1gNnSc6	Gallipoli
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
porážkou	porážka	k1gFnSc7	porážka
a	a	k8xC	a
evakuací	evakuace	k1gFnSc7	evakuace
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
postupovali	postupovat	k5eAaImAgMnP	postupovat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
Angličané	Angličan	k1gMnPc1	Angličan
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Bagdád	Bagdád	k1gInSc4	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
známý	známý	k2eAgMnSc1d1	známý
britský	britský	k2eAgMnSc1d1	britský
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
voják	voják	k1gMnSc1	voják
T.	T.	kA	T.
E.	E.	kA	E.
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
pomohl	pomoct	k5eAaPmAgInS	pomoct
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1916	[number]	k4	1916
protiosmanské	protiosmanský	k2eAgFnSc6d1	protiosmanský
vzpouře	vzpoura	k1gFnSc6	vzpoura
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
Angličané	Angličan	k1gMnPc1	Angličan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Araby	Arab	k1gMnPc7	Arab
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1917	[number]	k4	1917
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
1917	[number]	k4	1917
britská	britský	k2eAgFnSc1d1	britská
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
zachytila	zachytit	k5eAaPmAgFnS	zachytit
Zimmermannův	Zimmermannův	k2eAgInSc4d1	Zimmermannův
telegram	telegram	k1gInSc4	telegram
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Německo	Německo	k1gNnSc1	Německo
navrhovalo	navrhovat	k5eAaImAgNnS	navrhovat
Mexiku	Mexiko	k1gNnSc3	Mexiko
spojenectví	spojenectví	k1gNnSc2	spojenectví
proti	proti	k7c3	proti
USA	USA	kA	USA
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
mělo	mít	k5eAaImAgNnS	mít
Mexiko	Mexiko	k1gNnSc1	Mexiko
získat	získat	k5eAaPmF	získat
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
USA	USA	kA	USA
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
státy	stát	k1gInPc4	stát
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
popudilo	popudit	k5eAaPmAgNnS	popudit
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1917	[number]	k4	1917
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
války	válka	k1gFnSc2	válka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
z	z	k7c2	z
evropského	evropský	k2eAgInSc2d1	evropský
konfliktu	konflikt	k1gInSc2	konflikt
stává	stávat	k5eAaImIp3nS	stávat
světovou	světový	k2eAgFnSc7d1	světová
a	a	k8xC	a
Amerika	Amerika	k1gFnSc1	Amerika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ekonomickým	ekonomický	k2eAgMnSc7d1	ekonomický
vítězem	vítěz	k1gMnSc7	vítěz
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Důsledky	důsledek	k1gInPc4	důsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
dějinný	dějinný	k2eAgInSc4d1	dějinný
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
geopolitické	geopolitický	k2eAgFnSc6d1	geopolitická
i	i	k8xC	i
vojenské	vojenský	k2eAgFnSc6d1	vojenská
<g/>
,	,	kIx,	,
také	také	k9	také
však	však	k9	však
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dějin	dějiny	k1gFnPc2	dějiny
každodennosti	každodennost	k1gFnSc2	každodennost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celospolečenskou	celospolečenský	k2eAgFnSc4d1	celospolečenská
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnPc4d1	tradiční
morální	morální	k2eAgFnSc4d1	morální
hodnoty	hodnota	k1gFnPc4	hodnota
nebo	nebo	k8xC	nebo
kulturní	kulturní	k2eAgFnPc4d1	kulturní
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
společenské	společenský	k2eAgInPc4d1	společenský
návyky	návyk	k1gInPc4	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zásadních	zásadní	k2eAgInPc2d1	zásadní
zlomů	zlom	k1gInPc2	zlom
moderních	moderní	k2eAgFnPc2d1	moderní
dějin	dějiny	k1gFnPc2	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
důsledky	důsledek	k1gInPc4	důsledek
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
světu	svět	k1gInSc3	svět
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
novinky	novinka	k1gFnSc2	novinka
–	–	k?	–
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
první	první	k4xOgInSc4	první
průmyslově	průmyslově	k6eAd1	průmyslově
vedenou	vedený	k2eAgFnSc7d1	vedená
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
nasazovány	nasazován	k2eAgInPc4d1	nasazován
nové	nový	k2eAgInPc4d1	nový
typy	typ	k1gInPc4	typ
zbraní	zbraň	k1gFnPc2	zbraň
s	s	k7c7	s
nebývalou	bývalý	k2eNgFnSc7d1	bývalý
ničivou	ničivý	k2eAgFnSc7d1	ničivá
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
bojová	bojový	k2eAgNnPc1d1	bojové
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc1d1	chemická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
kulomety	kulomet	k1gInPc1	kulomet
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Politické	politický	k2eAgFnPc1d1	politická
změny	změna	k1gFnPc1	změna
–	–	k?	–
Rozpad	rozpad	k1gInSc1	rozpad
předválečné	předválečný	k2eAgFnSc2d1	předválečná
struktury	struktura	k1gFnSc2	struktura
států	stát	k1gInPc2	stát
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
států	stát	k1gInPc2	stát
národních	národní	k2eAgInPc2d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
zánik	zánik	k1gInSc1	zánik
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
,	,	kIx,	,
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k6eAd1	Rakousko-Uhersko
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
byly	být	k5eAaImAgInP	být
nové	nový	k2eAgInPc1d1	nový
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgNnSc1d1	zbylé
území	území	k1gNnSc1	území
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
Besarábie	Besarábie	k1gFnSc1	Besarábie
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
k	k	k7c3	k
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
získala	získat	k5eAaPmAgFnS	získat
zpět	zpět	k6eAd1	zpět
od	od	k7c2	od
Německa	Německo	k1gNnSc2	Německo
Alsasko	Alsasko	k1gNnSc4	Alsasko
a	a	k8xC	a
Lotrinsko	Lotrinsko	k1gNnSc4	Lotrinsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
získaly	získat	k5eAaPmAgFnP	získat
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
národní	národní	k2eAgInPc1d1	národní
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgFnP	mít
podporu	podpora	k1gFnSc4	podpora
jen	jen	k9	jen
části	část	k1gFnSc2	část
jejich	jejich	k3xOp3gNnSc2	jejich
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
slabé	slabý	k2eAgFnPc1d1	slabá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
agresivnímu	agresivní	k2eAgInSc3d1	agresivní
nacismu	nacismus	k1gInSc3	nacismus
vznikajícímu	vznikající	k2eAgInSc3d1	vznikající
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
také	také	k9	také
vzedmula	vzedmout	k5eAaPmAgFnS	vzedmout
vlna	vlna	k1gFnSc1	vlna
sociálního	sociální	k2eAgInSc2d1	sociální
i	i	k8xC	i
nacionálního	nacionální	k2eAgInSc2d1	nacionální
radikalismu	radikalismus	k1gInSc2	radikalismus
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
odstranění	odstranění	k1gNnSc4	odstranění
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proběhly	proběhnout	k5eAaPmAgInP	proběhnout
také	také	k9	také
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
revoluci	revoluce	k1gFnSc4	revoluce
bolševického	bolševický	k2eAgInSc2d1	bolševický
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
pacifismus	pacifismus	k1gInSc1	pacifismus
–	–	k?	–
Jako	jako	k8xC	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
rozmohl	rozmoct	k5eAaPmAgInS	rozmoct
pacifismus	pacifismus	k1gInSc1	pacifismus
odmítající	odmítající	k2eAgFnSc4d1	odmítající
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
světová	světový	k2eAgFnSc1d1	světová
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
mír	mír	k1gInSc4	mír
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
národů	národ	k1gInPc2	národ
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
sporů	spor	k1gInPc2	spor
jednáním	jednání	k1gNnPc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
také	také	k9	také
smírčí	smírčí	k2eAgInSc1d1	smírčí
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
změny	změna	k1gFnPc1	změna
–	–	k?	–
Vlivem	vliv	k1gInSc7	vliv
úmrtí	úmrtí	k1gNnSc2	úmrtí
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
mužů	muž	k1gMnPc2	muž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
věkové	věkový	k2eAgFnSc2d1	věková
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
struktury	struktura	k1gFnSc2	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
domáhat	domáhat	k5eAaImF	domáhat
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
tradiční	tradiční	k2eAgFnSc7d1	tradiční
výsadou	výsada	k1gFnSc7	výsada
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně-morální	Kulturněorální	k2eAgFnPc1d1	Kulturně-morální
změny	změna	k1gFnPc1	změna
–	–	k?	–
Nadšení	nadšení	k1gNnSc1	nadšení
z	z	k7c2	z
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
s	s	k7c7	s
otrlostí	otrlost	k1gFnSc7	otrlost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
vypěstovala	vypěstovat	k5eAaPmAgFnS	vypěstovat
během	během	k7c2	během
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
nebo	nebo	k8xC	nebo
i	i	k9	i
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
předválečného	předválečný	k2eAgInSc2d1	předválečný
vkusu	vkus	k1gInSc2	vkus
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc2d1	tradiční
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc4d1	náboženská
změny	změna	k1gFnPc4	změna
–	–	k?	–
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
rodin	rodina	k1gFnPc2	rodina
válka	válka	k1gFnSc1	válka
znamenala	znamenat	k5eAaImAgFnS	znamenat
velký	velký	k2eAgInSc4d1	velký
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc2d1	velká
státní	státní	k2eAgFnSc2d1	státní
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
fronty	fronta	k1gFnSc2	fronta
se	se	k3xPyFc4	se
během	během	k7c2	během
války	válka	k1gFnSc2	válka
aktivně	aktivně	k6eAd1	aktivně
zapojovaly	zapojovat	k5eAaImAgFnP	zapojovat
do	do	k7c2	do
štvavé	štvavý	k2eAgFnSc2d1	štvavá
válečné	válečný	k2eAgFnSc2d1	válečná
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
věřících	věřící	k1gMnPc2	věřící
proto	proto	k8xC	proto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
církví	církev	k1gFnPc2	církev
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
nebo	nebo	k8xC	nebo
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
nábožensky	nábožensky	k6eAd1	nábožensky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgNnPc1d1	konečné
mírová	mírový	k2eAgNnPc1d1	Mírové
jednání	jednání	k1gNnPc1	jednání
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Versailles	Versailles	k1gFnSc2	Versailles
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
