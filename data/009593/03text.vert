<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
zbraň	zbraň	k1gFnSc1	zbraň
nebo	nebo	k8xC	nebo
též	též	k9	též
atomová	atomový	k2eAgFnSc1d1	atomová
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
zbraň	zbraň	k1gFnSc4	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
neřízené	řízený	k2eNgFnSc2d1	neřízená
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
jader	jádro	k1gNnPc2	jádro
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
zbraně	zbraň	k1gFnPc4	zbraň
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
slučování	slučování	k1gNnSc6	slučování
jader	jádro	k1gNnPc2	jádro
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
materiál	materiál	k1gInSc1	materiál
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
zamoření	zamoření	k1gNnSc2	zamoření
cílové	cílový	k2eAgFnSc2d1	cílová
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
radiologické	radiologický	k2eAgInPc1d1	radiologický
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
bomba	bomba	k1gFnSc1	bomba
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vojenského	vojenský	k2eAgInSc2d1	vojenský
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
výzkum	výzkum	k1gInSc1	výzkum
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
Roberta	Robert	k1gMnSc2	Robert
Jacoba	Jacoba	k1gFnSc1	Jacoba
Oppenheimera	Oppenheimera	k1gFnSc1	Oppenheimera
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
pokusný	pokusný	k2eAgInSc1d1	pokusný
jaderný	jaderný	k2eAgInSc1d1	jaderný
výbuch	výbuch	k1gInSc1	výbuch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
White	Whit	k1gInSc5	Whit
Sands	Sandsa	k1gFnPc2	Sandsa
poblíž	poblíž	k6eAd1	poblíž
města	město	k1gNnSc2	město
Alamogordo	Alamogordo	k1gNnSc1	Alamogordo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
bomby	bomba	k1gFnPc1	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
a	a	k8xC	a
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
byly	být	k5eAaImAgFnP	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
svrženy	svrhnout	k5eAaPmNgInP	svrhnout
z	z	k7c2	z
bombardérů	bombardér	k1gInPc2	bombardér
B-29	B-29	k1gFnSc2	B-29
na	na	k7c4	na
japonská	japonský	k2eAgNnPc4d1	Japonské
města	město	k1gNnPc4	město
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gMnSc1	letoun
B-29	B-29	k1gMnSc1	B-29
Enola	Enola	k1gMnSc1	Enola
Gay	gay	k1gMnSc1	gay
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
v	v	k7c4	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
uranovou	uranový	k2eAgFnSc4d1	uranová
jadernou	jaderný	k2eAgFnSc4d1	jaderná
pumu	puma	k1gFnSc4	puma
s	s	k7c7	s
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
mezi	mezi	k7c4	mezi
13	[number]	k4	13
a	a	k8xC	a
18	[number]	k4	18
kilotunami	kilotuna	k1gFnPc7	kilotuna
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gMnSc1	letoun
B-29	B-29	k1gMnSc1	B-29
Bock	Bock	k1gMnSc1	Bock
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Car	car	k1gMnSc1	car
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
v	v	k7c4	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
plutoniovou	plutoniový	k2eAgFnSc4d1	plutoniová
bombu	bomba	k1gFnSc4	bomba
na	na	k7c4	na
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
pumy	puma	k1gFnPc1	puma
zabily	zabít	k5eAaPmAgFnP	zabít
okamžitě	okamžitě	k6eAd1	okamžitě
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
100	[number]	k4	100
000	[number]	k4	000
umíralo	umírat	k5eAaImAgNnS	umírat
na	na	k7c4	na
následky	následek	k1gInPc4	následek
výbuchu	výbuch	k1gInSc2	výbuch
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
postiženy	postihnout	k5eAaPmNgFnP	postihnout
byly	být	k5eAaImAgFnP	být
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
generace	generace	k1gFnPc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
výbuchy	výbuch	k1gInPc1	výbuch
dosud	dosud	k6eAd1	dosud
představují	představovat	k5eAaImIp3nP	představovat
jediné	jediný	k2eAgNnSc4d1	jediné
použití	použití	k1gNnSc4	použití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
či	či	k8xC	či
v	v	k7c6	v
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
konfliktu	konflikt	k1gInSc6	konflikt
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
atomovou	atomový	k2eAgFnSc7d1	atomová
mocností	mocnost	k1gFnSc7	mocnost
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
stal	stát	k5eAaPmAgInS	stát
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
výbuchem	výbuch	k1gInSc7	výbuch
zařízení	zařízení	k1gNnSc1	zařízení
RDS-1	RDS-1	k1gFnSc2	RDS-1
označováno	označovat	k5eAaImNgNnS	označovat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
jako	jako	k8xC	jako
Joe-	Joe-	k1gFnSc6	Joe-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Američané	Američan	k1gMnPc1	Američan
sovětský	sovětský	k2eAgInSc4d1	sovětský
jaderný	jaderný	k2eAgInSc4d1	jaderný
vývoj	vývoj	k1gInSc4	vývoj
tajně	tajně	k6eAd1	tajně
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
projektem	projekt	k1gInSc7	projekt
Mogul	mogul	k1gMnSc1	mogul
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
první	první	k4xOgFnSc1	první
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
byla	být	k5eAaImAgFnS	být
de-facto	deacto	k1gNnSc4	de-facto
značně	značně	k6eAd1	značně
okopírovaná	okopírovaný	k2eAgFnSc1d1	okopírovaná
americká	americký	k2eAgFnSc1d1	americká
implozivní	implozivní	k2eAgFnSc1d1	implozivní
plutoniová	plutoniový	k2eAgFnSc1d1	plutoniová
bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
plány	plán	k1gInPc4	plán
SSSR	SSSR	kA	SSSR
získal	získat	k5eAaPmAgInS	získat
díky	díky	k7c3	díky
špionážní	špionážní	k2eAgFnSc3d1	špionážní
práci	práce	k1gFnSc3	práce
jaderného	jaderný	k2eAgMnSc2d1	jaderný
fyzika	fyzik	k1gMnSc2	fyzik
Klause	Klaus	k1gMnSc2	Klaus
Fuchse	Fuchs	k1gMnSc2	Fuchs
podílejícího	podílející	k2eAgMnSc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
předané	předaný	k2eAgInPc1d1	předaný
materiály	materiál	k1gInPc1	materiál
podstatně	podstatně	k6eAd1	podstatně
urychlily	urychlit	k5eAaPmAgInP	urychlit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
sestrojení	sestrojení	k1gNnSc6	sestrojení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
vedl	vést	k5eAaImAgMnS	vést
akademik	akademik	k1gMnSc1	akademik
Igor	Igor	k1gMnSc1	Igor
Kurčatov	Kurčatovo	k1gNnPc2	Kurčatovo
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Dmitrijevič	Dmitrijevič	k1gMnSc1	Dmitrijevič
Sacharov	Sacharovo	k1gNnPc2	Sacharovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
vodíkovou	vodíkový	k2eAgFnSc4d1	vodíková
pumu	puma	k1gFnSc4	puma
otestovaly	otestovat	k5eAaPmAgInP	otestovat
USA	USA	kA	USA
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
následovala	následovat	k5eAaImAgFnS	následovat
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tu	tu	k6eAd1	tu
však	však	k9	však
roky	rok	k1gInPc1	rok
utajovaný	utajovaný	k2eAgInSc1d1	utajovaný
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
ta	ten	k3xDgFnSc1	ten
americká	americký	k2eAgFnSc1d1	americká
používala	používat	k5eAaImAgFnS	používat
tekutý	tekutý	k2eAgInSc4d1	tekutý
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
zařízení	zařízení	k1gNnSc4	zařízení
pak	pak	k6eAd1	pak
mělo	mít	k5eAaImAgNnS	mít
velikost	velikost	k1gFnSc4	velikost
menšího	malý	k2eAgInSc2d2	menší
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
na	na	k7c4	na
sovětské	sovětský	k2eAgFnPc4d1	sovětská
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
lithiumdeuterid	lithiumdeuterid	k1gInSc1	lithiumdeuterid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
tedy	tedy	k8xC	tedy
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
<g/>
,	,	kIx,	,
připravena	připraven	k2eAgFnSc1d1	připravena
k	k	k7c3	k
vojenskému	vojenský	k2eAgNnSc3d1	vojenské
nasazení	nasazení	k1gNnSc3	nasazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
pozdní	pozdní	k2eAgInSc1d1	pozdní
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
raná	raný	k2eAgFnSc1d1	raná
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
byla	být	k5eAaImAgFnS	být
obdobím	období	k1gNnSc7	období
horečného	horečný	k2eAgNnSc2d1	horečné
testování	testování	k1gNnSc2	testování
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
obou	dva	k4xCgFnPc2	dva
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gInPc2	jejich
závodů	závod	k1gInPc2	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
otestoval	otestovat	k5eAaPmAgInS	otestovat
největší	veliký	k2eAgFnSc4d3	veliký
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bombu	bomba	k1gFnSc4	bomba
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
car-bomba	caromba	k1gFnSc1	car-bomba
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Ц	Ц	k?	Ц
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
třífázová	třífázový	k2eAgFnSc1d1	třífázová
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
fáze	fáze	k1gFnPc1	fáze
byly	být	k5eAaImAgFnP	být
vodíkové	vodíkový	k2eAgFnPc1d1	vodíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
silou	síla	k1gFnSc7	síla
přes	přes	k7c4	přes
100	[number]	k4	100
Mt	Mt	k1gFnPc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
se	se	k3xPyFc4	se
však	však	k9	však
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
a	a	k8xC	a
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
U	u	k7c2	u
<g/>
238	[number]	k4	238
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
experimentálním	experimentální	k2eAgInSc6d1	experimentální
výbuchu	výbuch	k1gInSc6	výbuch
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
car-bomby	caromba	k1gFnSc2	car-bomba
zjitřil	zjitřit	k5eAaPmAgInS	zjitřit
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Nikita	Nikita	k1gFnSc1	Nikita
Sergejevič	Sergejevič	k1gInSc1	Sergejevič
Chruščov	Chruščov	k1gInSc1	Chruščov
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
ohrožování	ohrožování	k1gNnSc2	ohrožování
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
lidstva	lidstvo	k1gNnSc2	lidstvo
jako	jako	k8xC	jako
takového	takový	k3xDgMnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Monstrózně	monstrózně	k6eAd1	monstrózně
velké	velký	k2eAgFnPc1d1	velká
jaderné	jaderný	k2eAgFnPc1d1	jaderná
bomby	bomba	k1gFnPc1	bomba
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
vojensky	vojensky	k6eAd1	vojensky
obtížně	obtížně	k6eAd1	obtížně
použitelné	použitelný	k2eAgInPc1d1	použitelný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rozumně	rozumně	k6eAd1	rozumně
takovou	takový	k3xDgFnSc4	takový
zbraň	zbraň	k1gFnSc4	zbraň
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc1	rozsah
zničení	zničení	k1gNnSc2	zničení
brání	bránit	k5eAaImIp3nS	bránit
vlastní	vlastní	k2eAgFnSc3d1	vlastní
armádě	armáda	k1gFnSc3	armáda
v	v	k7c6	v
obsazení	obsazení	k1gNnSc6	obsazení
území	území	k1gNnSc2	území
protivníka	protivník	k1gMnSc2	protivník
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k9	tak
pumy	puma	k1gFnSc2	puma
extrémní	extrémní	k2eAgFnSc2d1	extrémní
velikosti	velikost	k1gFnSc2	velikost
na	na	k7c6	na
mezikontinentálních	mezikontinentální	k2eAgFnPc6d1	mezikontinentální
balistických	balistický	k2eAgFnPc6d1	balistická
střelách	střela	k1gFnPc6	střela
postupně	postupně	k6eAd1	postupně
nahradily	nahradit	k5eAaPmAgFnP	nahradit
multihlavice	multihlavice	k1gFnPc1	multihlavice
s	s	k7c7	s
několika	několik	k4yIc7	několik
menšími	malý	k2eAgInPc7d2	menší
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
naváděnými	naváděný	k2eAgFnPc7d1	naváděná
jadernými	jaderný	k2eAgFnPc7d1	jaderná
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1	fyzické
zmenšení	zmenšení	k1gNnSc1	zmenšení
dále	daleko	k6eAd2	daleko
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
použití	použití	k1gNnSc1	použití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
raket	raketa	k1gFnPc2	raketa
na	na	k7c6	na
strategických	strategický	k2eAgFnPc6d1	strategická
ponorkách	ponorka	k1gFnPc6	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nebezpečně	bezpečně	k6eNd1	bezpečně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
menších	malý	k2eAgFnPc2d2	menší
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
taktických	taktický	k2eAgFnPc2d1	taktická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
ústředí	ústředí	k1gNnSc2	ústředí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
armádním	armádní	k2eAgFnPc3d1	armádní
složkám	složka	k1gFnPc3	složka
na	na	k7c6	na
nižším	nízký	k2eAgInSc6d2	nižší
stupni	stupeň	k1gInSc6	stupeň
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
SALT	salto	k1gNnPc2	salto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následující	následující	k2eAgNnPc4d1	následující
desetiletí	desetiletí	k1gNnPc4	desetiletí
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
jaderného	jaderný	k2eAgNnSc2d1	jaderné
odzbrojování	odzbrojování	k1gNnSc2	odzbrojování
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
Michaila	Michail	k1gMnSc2	Michail
Gorbačova	Gorbačův	k2eAgMnSc2d1	Gorbačův
a	a	k8xC	a
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Obratem	obratem	k6eAd1	obratem
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
schůzka	schůzka	k1gFnSc1	schůzka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
provedl	provést	k5eAaPmAgInS	provést
poslední	poslední	k2eAgInSc4d1	poslední
jaderný	jaderný	k2eAgInSc4d1	jaderný
pokus	pokus	k1gInSc4	pokus
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
upustil	upustit	k5eAaPmAgMnS	upustit
od	od	k7c2	od
testování	testování	k1gNnSc2	testování
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
smlouvy	smlouva	k1gFnPc1	smlouva
START	start	k1gInSc1	start
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
START	start	k1gInSc1	start
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zbraň	zbraň	k1gFnSc4	zbraň
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
k	k	k7c3	k
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
přiznala	přiznat	k5eAaPmAgFnS	přiznat
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
kusů	kus	k1gInPc2	kus
vlastní	vlastní	k2eAgMnSc1d1	vlastní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
oficiálně	oficiálně	k6eAd1	oficiálně
to	ten	k3xDgNnSc4	ten
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pokoušely	pokoušet	k5eAaImAgInP	pokoušet
i	i	k9	i
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
obviňován	obviňován	k2eAgInSc1d1	obviňován
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
svůj	svůj	k3xOyFgInSc4	svůj
jaderný	jaderný	k2eAgInSc4d1	jaderný
program	program	k1gInSc4	program
zastavily	zastavit	k5eAaPmAgFnP	zastavit
či	či	k8xC	či
zrušily	zrušit	k5eAaPmAgFnP	zrušit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smluv	smlouva	k1gFnPc2	smlouva
o	o	k7c6	o
nešíření	nešíření	k1gNnSc6	nešíření
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
JAR	jar	k1gFnSc1	jar
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
své	svůj	k3xOyFgFnPc4	svůj
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
získaly	získat	k5eAaPmAgFnP	získat
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jeho	jeho	k3xOp3gFnSc2	jeho
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
však	však	k9	však
předaly	předat	k5eAaPmAgInP	předat
zpět	zpět	k6eAd1	zpět
Rusku	Ruska	k1gFnSc4	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
představovaly	představovat	k5eAaImAgFnP	představovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
odstrašující	odstrašující	k2eAgInSc4d1	odstrašující
prostředek	prostředek	k1gInSc4	prostředek
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dualistického	dualistický	k2eAgNnSc2d1	dualistické
pojetí	pojetí	k1gNnSc2	pojetí
světa	svět	k1gInSc2	svět
dvou	dva	k4xCgFnPc2	dva
soupeřících	soupeřící	k2eAgFnPc2d1	soupeřící
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
"	"	kIx"	"
<g/>
vzájemně	vzájemně	k6eAd1	vzájemně
zaručeného	zaručený	k2eAgNnSc2d1	zaručené
zničení	zničení	k1gNnSc2	zničení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
kritického	kritický	k2eAgInSc2d1	kritický
stavu	stav	k1gInSc2	stav
ve	v	k7c6	v
štěpném	štěpný	k2eAgInSc6d1	štěpný
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
objemu	objem	k1gInSc6	objem
přítomné	přítomný	k2eAgNnSc1d1	přítomné
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
nárazy	náraz	k1gInPc4	náraz
do	do	k7c2	do
atomárních	atomární	k2eAgNnPc2d1	atomární
jader	jádro	k1gNnPc2	jádro
štěpí	štěpit	k5eAaImIp3nS	štěpit
štěpný	štěpný	k2eAgInSc1d1	štěpný
materiál	materiál	k1gInSc1	materiál
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
uvolnění	uvolnění	k1gNnSc2	uvolnění
dalších	další	k2eAgInPc2d1	další
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
neutronový	neutronový	k2eAgInSc1d1	neutronový
mrak	mrak	k1gInSc1	mrak
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
zásahu	zásah	k1gInSc2	zásah
atomárních	atomární	k2eAgNnPc2d1	atomární
jader	jádro	k1gNnPc2	jádro
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
neutrony	neutron	k1gInPc1	neutron
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
v	v	k7c6	v
potřebném	potřebné	k1gNnSc6	potřebné
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
štěpná	štěpný	k2eAgFnSc1d1	štěpná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
dělového	dělový	k2eAgInSc2d1	dělový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
oddělených	oddělený	k2eAgNnPc2d1	oddělené
podkritických	podkritický	k2eAgNnPc2d1	podkritické
množství	množství	k1gNnPc2	množství
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
neuvolňují	uvolňovat	k5eNaImIp3nP	uvolňovat
při	při	k7c6	při
rozpadu	rozpad	k1gInSc6	rozpad
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
nutných	nutný	k2eAgFnPc2d1	nutná
ke	k	k7c3	k
štěpné	štěpný	k2eAgFnSc3d1	štěpná
reakci	reakce	k1gFnSc3	reakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
spojení	spojení	k1gNnSc6	spojení
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
nadkritického	nadkritický	k2eAgInSc2d1	nadkritický
stavu	stav	k1gInSc2	stav
-	-	kIx~	-
uvolněná	uvolněný	k2eAgNnPc1d1	uvolněné
množství	množství	k1gNnSc4	množství
neutronů	neutron	k1gInPc2	neutron
se	se	k3xPyFc4	se
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
objemu	objem	k1gInSc6	objem
sečtou	sečíst	k5eAaPmIp3nP	sečíst
(	(	kIx(	(
<g/>
materiálu	materiál	k1gInSc2	materiál
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
jednotky	jednotka	k1gFnPc1	jednotka
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
vystřelen	vystřelit	k5eAaPmNgInS	vystřelit
a	a	k8xC	a
stlačen	stlačit	k5eAaPmNgInS	stlačit
explozí	exploze	k1gFnSc7	exploze
klasické	klasický	k2eAgFnSc2d1	klasická
výbušniny	výbušnina	k1gFnSc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
výbuchu	výbuch	k1gInSc2	výbuch
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
nebudou	být	k5eNaImBp3nP	být
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
několika	několik	k4yIc2	několik
milisekund	milisekunda	k1gFnPc2	milisekunda
odhozeny	odhozen	k2eAgFnPc1d1	odhozena
teplem	teplo	k1gNnSc7	teplo
počínající	počínající	k2eAgFnSc2d1	počínající
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
tlakem	tlak	k1gInSc7	tlak
vylétajících	vylétající	k2eAgInPc2d1	vylétající
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadkritickém	nadkritický	k2eAgInSc6d1	nadkritický
stavu	stav	k1gInSc6	stav
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nastartována	nastartován	k2eAgFnSc1d1	nastartována
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
typem	typ	k1gInSc7	typ
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
implozní	implozní	k2eAgFnSc1d1	implozní
puma	puma	k1gFnSc1	puma
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byl	být	k5eAaImAgInS	být
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
shozený	shozený	k2eAgMnSc1d1	shozený
na	na	k7c4	na
Nagasaki	Nagasaki	k1gNnPc4	Nagasaki
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
silou	síla	k1gFnSc7	síla
výbuchu	výbuch	k1gInSc2	výbuch
21	[number]	k4	21
Kt	Kt	k1gFnPc2	Kt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
použito	použít	k5eAaPmNgNnS	použít
plutonium	plutonium	k1gNnSc4	plutonium
namísto	namísto	k7c2	namísto
uranu	uran	k1gInSc2	uran
235	[number]	k4	235
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
principem	princip	k1gInSc7	princip
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
nadkritického	nadkritický	k2eAgInSc2d1	nadkritický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
konvenční	konvenční	k2eAgFnSc2d1	konvenční
trhaviny	trhavina	k1gFnSc2	trhavina
(	(	kIx(	(
<g/>
sestava	sestava	k1gFnSc1	sestava
pomalé	pomalý	k2eAgFnSc2d1	pomalá
a	a	k8xC	a
rychlé	rychlý	k2eAgFnSc2d1	rychlá
trhaviny	trhavina	k1gFnSc2	trhavina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podkritická	podkritický	k2eAgFnSc1d1	podkritická
konfigurace	konfigurace	k1gFnSc1	konfigurace
plutonia	plutonium	k1gNnSc2	plutonium
stlačena	stlačen	k2eAgFnSc1d1	stlačena
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nadkritického	nadkritický	k2eAgInSc2d1	nadkritický
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
jaderného	jaderný	k2eAgInSc2d1	jaderný
materiálu	materiál	k1gInSc2	materiál
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tradovaná	tradovaný	k2eAgFnSc1d1	tradovaná
představa	představa	k1gFnSc1	představa
spojování	spojování	k1gNnSc2	spojování
několika	několik	k4yIc2	několik
menších	malý	k2eAgNnPc2d2	menší
množství	množství	k1gNnPc2	množství
není	být	k5eNaImIp3nS	být
správně	správně	k6eAd1	správně
chápána	chápán	k2eAgFnSc1d1	chápána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
konvenční	konvenční	k2eAgFnSc2d1	konvenční
trhaviny	trhavina	k1gFnSc2	trhavina
zároveň	zároveň	k6eAd1	zároveň
udrží	udržet	k5eAaPmIp3nP	udržet
masu	masa	k1gFnSc4	masa
plutonia	plutonium	k1gNnSc2	plutonium
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
po	po	k7c4	po
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
rozmetání	rozmetání	k1gNnSc2	rozmetání
materiálu	materiál	k1gInSc2	materiál
probíhat	probíhat	k5eAaImF	probíhat
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
rychlý	rychlý	k2eAgInSc1d1	rychlý
průběh	průběh	k1gInSc1	průběh
jaderného	jaderný	k2eAgInSc2d1	jaderný
výbuchu	výbuch	k1gInSc2	výbuch
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
rozmetal	rozmetat	k5eAaImAgInS	rozmetat
masu	masa	k1gFnSc4	masa
plutonia	plutonium	k1gNnSc2	plutonium
a	a	k8xC	a
rozptýlil	rozptýlit	k5eAaPmAgInS	rozptýlit
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
kritického	kritický	k2eAgInSc2d1	kritický
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
implozního	implozní	k2eAgInSc2d1	implozní
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
zdroj	zdroj	k1gInSc1	zdroj
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jaderná	jaderný	k2eAgFnSc1d1	jaderná
roznětka	roznětka	k1gFnSc1	roznětka
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
inicializační	inicializační	k2eAgInSc1d1	inicializační
neutronový	neutronový	k2eAgInSc1d1	neutronový
zářič	zářič	k1gInSc1	zářič
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
poměrně	poměrně	k6eAd1	poměrně
těžko	těžko	k6eAd1	těžko
štěpitelný	štěpitelný	k2eAgInSc1d1	štěpitelný
v	v	k7c6	v
řetězové	řetězový	k2eAgFnSc6d1	řetězová
reakci	reakce	k1gFnSc6	reakce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
neutronů	neutron	k1gInPc2	neutron
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
štěpení	štěpení	k1gNnSc4	štěpení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ve	v	k7c6	v
vhodném	vhodný	k2eAgInSc6d1	vhodný
okamžiku	okamžik	k1gInSc6	okamžik
přispěje	přispět	k5eAaPmIp3nS	přispět
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bývá	bývat	k5eAaImIp3nS	bývat
puma	puma	k1gFnSc1	puma
vylepšena	vylepšit	k5eAaPmNgFnS	vylepšit
vnějším	vnější	k2eAgInSc7d1	vnější
pláštěm	plášť	k1gInSc7	plášť
z	z	k7c2	z
odražeče	odražeč	k1gInSc2	odražeč
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
neutronového	neutronový	k2eAgNnSc2d1	neutronové
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
(	(	kIx(	(
<g/>
z	z	k7c2	z
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
Be	Be	k1gFnPc2	Be
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
takto	takto	k6eAd1	takto
vraceny	vracet	k5eAaImNgInP	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
zahušťují	zahušťovat	k5eAaImIp3nP	zahušťovat
neutronový	neutronový	k2eAgInSc4d1	neutronový
mrak	mrak	k1gInSc4	mrak
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Konfigurace	konfigurace	k1gFnSc1	konfigurace
je	být	k5eAaImIp3nS	být
výhodná	výhodný	k2eAgFnSc1d1	výhodná
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
postačuje	postačovat	k5eAaImIp3nS	postačovat
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jednak	jednak	k8xC	jednak
snižuje	snižovat	k5eAaImIp3nS	snižovat
zčásti	zčásti	k6eAd1	zčásti
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
jaderného	jaderný	k2eAgInSc2d1	jaderný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosažení	dosažení	k1gNnSc4	dosažení
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dělové	dělový	k2eAgInPc4d1	dělový
jaderné	jaderný	k2eAgInPc4d1	jaderný
granáty	granát	k1gInPc4	granát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přenosné	přenosný	k2eAgFnSc2d1	přenosná
miny	mina	k1gFnSc2	mina
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
správné	správný	k2eAgFnSc2d1	správná
konfigurace	konfigurace	k1gFnSc2	konfigurace
takovéhoto	takovýto	k3xDgInSc2	takovýto
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nutné	nutný	k2eAgFnPc1d1	nutná
špičkové	špičkový	k2eAgFnPc1d1	špičková
znalosti	znalost	k1gFnPc1	znalost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
trhavin	trhavina	k1gFnPc2	trhavina
-	-	kIx~	-
jinak	jinak	k6eAd1	jinak
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
stlačení	stlačení	k1gNnSc3	stlačení
a	a	k8xC	a
udržení	udržení	k1gNnSc4	udržení
ve	v	k7c6	v
vhodném	vhodný	k2eAgInSc6d1	vhodný
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
rozmetání	rozmetání	k1gNnSc3	rozmetání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
překážkou	překážka	k1gFnSc7	překážka
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
samotných	samotný	k2eAgInPc2d1	samotný
dílů	díl	k1gInPc2	díl
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
-	-	kIx~	-
díly	díl	k1gInPc4	díl
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
třískovým	třískový	k2eAgNnSc7d1	třískové
obráběním	obrábění	k1gNnSc7	obrábění
z	z	k7c2	z
větších	veliký	k2eAgInPc2d2	veliký
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
plutonium	plutonium	k1gNnSc1	plutonium
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
snadno	snadno	k6eAd1	snadno
zápalné	zápalný	k2eAgNnSc1d1	zápalné
a	a	k8xC	a
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
od	od	k7c2	od
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
(	(	kIx(	(
<g/>
nejmenší	malý	k2eAgFnPc1d3	nejmenší
nálože	nálož	k1gFnPc1	nálož
-	-	kIx~	-
např.	např.	kA	např.
viz	vidět	k5eAaImRp2nS	vidět
Davy	Dav	k1gInPc1	Dav
Crockett	Crockett	k1gInSc1	Crockett
<g/>
)	)	kIx)	)
až	až	k9	až
miliónům	milión	k4xCgInPc3	milión
tun	tuna	k1gFnPc2	tuna
klasické	klasický	k2eAgFnSc2d1	klasická
výbušniny	výbušnina	k1gFnSc2	výbušnina
TNT	TNT	kA	TNT
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
známá	známý	k2eAgFnSc1d1	známá
odpálená	odpálený	k2eAgFnSc1d1	odpálená
bomba	bomba	k1gFnSc1	bomba
byla	být	k5eAaImAgFnS	být
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
57	[number]	k4	57
Mt	Mt	k1gFnPc2	Mt
TNT	TNT	kA	TNT
(	(	kIx(	(
<g/>
Car-bomba	Caromba	k1gMnSc1	Car-bomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
postupu	postup	k1gInSc2	postup
útočících	útočící	k2eAgNnPc2d1	útočící
vojsk	vojsko	k1gNnPc2	vojsko
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k6eAd1	rovněž
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
a	a	k8xC	a
otestován	otestován	k2eAgInSc1d1	otestován
typ	typ	k1gInSc1	typ
zbraně	zbraň	k1gFnSc2	zbraň
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
neutronová	neutronový	k2eAgFnSc1d1	neutronová
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
konstrukci	konstrukce	k1gFnSc4	konstrukce
upravenou	upravený	k2eAgFnSc4d1	upravená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
co	co	k9	co
největší	veliký	k2eAgFnSc2d3	veliký
emise	emise	k1gFnSc2	emise
neutronového	neutronový	k2eAgNnSc2d1	neutronové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
likvidace	likvidace	k1gFnSc1	likvidace
živé	živý	k2eAgFnSc2d1	živá
síly	síla	k1gFnSc2	síla
nepřítele	nepřítel	k1gMnSc2	nepřítel
bez	bez	k7c2	bez
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
materiálních	materiální	k2eAgFnPc2d1	materiální
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Výbušná	výbušný	k2eAgFnSc1d1	výbušná
síla	síla	k1gFnSc1	síla
takové	takový	k3xDgFnSc2	takový
bomby	bomba	k1gFnSc2	bomba
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
nižší	nízký	k2eAgInSc1d2	nižší
spad	spad	k1gInSc1	spad
(	(	kIx(	(
<g/>
jednak	jednak	k8xC	jednak
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
konstruovaná	konstruovaný	k2eAgFnSc1d1	konstruovaná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
určená	určený	k2eAgFnSc1d1	určená
ke	k	k7c3	k
vzdušnému	vzdušný	k2eAgInSc3d1	vzdušný
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neutronové	neutronový	k2eAgNnSc1d1	neutronové
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
intenzitu	intenzita	k1gFnSc4	intenzita
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc4d2	veliký
zasaženou	zasažený	k2eAgFnSc4d1	zasažená
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
vážnější	vážní	k2eAgInSc4d2	vážnější
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
v	v	k7c6	v
zasažené	zasažený	k2eAgFnSc6d1	zasažená
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
atomovým	atomový	k2eAgInSc7d1	atomový
výbuchem	výbuch	k1gInSc7	výbuch
==	==	k?	==
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
atomovým	atomový	k2eAgInSc7d1	atomový
výbuchem	výbuch	k1gInSc7	výbuch
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
podle	podle	k7c2	podle
známého	známý	k2eAgInSc2d1	známý
vzorce	vzorec	k1gInSc2	vzorec
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
*	*	kIx~	*
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
a	a	k8xC	a
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
řádově	řádově	k6eAd1	řádově
procenta	procento	k1gNnSc2	procento
hmoty	hmota	k1gFnSc2	hmota
štěpného	štěpný	k2eAgInSc2d1	štěpný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
místa	místo	k1gNnSc2	místo
výbuchu	výbuch	k1gInSc2	výbuch
je	být	k5eAaImIp3nS	být
přenášena	přenášen	k2eAgFnSc1d1	přenášena
zhruba	zhruba	k6eAd1	zhruba
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tlaková	tlakový	k2eAgFnSc1d1	tlaková
vlna	vlna	k1gFnSc1	vlna
–	–	k?	–
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
<s>
tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
–	–	k?	–
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
<s>
ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
–	–	k?	–
5	[number]	k4	5
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
<s>
radioaktivní	radioaktivní	k2eAgFnPc4d1	radioaktivní
látky	látka	k1gFnPc4	látka
–	–	k?	–
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
<s>
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
impulsZatímco	impulsZatímco	k6eAd1	impulsZatímco
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc4	typ
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
vyzářeny	vyzářen	k2eAgFnPc1d1	vyzářena
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jednak	jednak	k8xC	jednak
zbytky	zbytek	k1gInPc1	zbytek
samotné	samotný	k2eAgFnSc2d1	samotná
pumy	puma	k1gFnSc2	puma
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
následně	následně	k6eAd1	následně
ozářeným	ozářený	k2eAgInSc7d1	ozářený
materiálem	materiál	k1gInSc7	materiál
okolí	okolí	k1gNnSc2	okolí
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
rozprášeným	rozprášený	k2eAgInSc7d1	rozprášený
výbuchem	výbuch	k1gInSc7	výbuch
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mrak	mrak	k1gInSc1	mrak
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
koncentraci	koncentrace	k1gFnSc6	koncentrace
nesen	nesen	k2eAgInSc1d1	nesen
a	a	k8xC	a
ukládán	ukládán	k2eAgInSc1d1	ukládán
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
největších	veliký	k2eAgInPc2d3	veliký
výbuchů	výbuch	k1gInPc2	výbuch
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
mrak	mrak	k1gInSc4	mrak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
radiace	radiace	k1gFnSc2	radiace
pozadí	pozadí	k1gNnSc2	pozadí
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
indikátor	indikátor	k1gInSc1	indikátor
jaderných	jaderný	k2eAgInPc2d1	jaderný
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
indikátory	indikátor	k1gInPc7	indikátor
jaderných	jaderný	k2eAgInPc2d1	jaderný
testů	test	k1gInPc2	test
jsou	být	k5eAaImIp3nP	být
částicové	částicový	k2eAgInPc1d1	částicový
záblesky	záblesk	k1gInPc1	záblesk
<g/>
.	.	kIx.	.
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
puls	puls	k1gInSc1	puls
a	a	k8xC	a
také	také	k9	také
seismická	seismický	k2eAgFnSc1d1	seismická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
uvolněné	uvolněný	k2eAgNnSc1d1	uvolněné
jaderným	jaderný	k2eAgInSc7d1	jaderný
výbuchem	výbuch	k1gInSc7	výbuch
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
typu	typ	k1gInSc6	typ
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
absorbováno	absorbován	k2eAgNnSc4d1	absorbováno
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
tlumeno	tlumit	k5eAaImNgNnS	tlumit
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
epicentra	epicentrum	k1gNnSc2	epicentrum
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
tedy	tedy	k9	tedy
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
největší	veliký	k2eAgFnPc4d3	veliký
škody	škoda	k1gFnPc4	škoda
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bomby	bomba	k1gFnSc2	bomba
shozené	shozený	k2eAgFnSc2d1	shozená
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
(	(	kIx(	(
<g/>
explodovala	explodovat	k5eAaBmAgFnS	explodovat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
550	[number]	k4	550
m	m	kA	m
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
epicentru	epicentrum	k1gNnSc6	epicentrum
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
povrch	povrch	k1gInSc1	povrch
slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
5	[number]	k4	5
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
byla	být	k5eAaImAgFnS	být
dosažena	dosažen	k2eAgFnSc1d1	dosažena
teplota	teplota	k1gFnSc1	teplota
asi	asi	k9	asi
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
několika	několik	k4yIc2	několik
milisekund	milisekunda	k1gFnPc2	milisekunda
<g/>
)	)	kIx)	)
i	i	k9	i
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
desítek	desítka	k1gFnPc2	desítka
<g/>
)	)	kIx)	)
milionů	milion	k4xCgInPc2	milion
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zbraň	zbraň	k1gFnSc4	zbraň
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
(	(	kIx(	(
<g/>
chemických	chemický	k2eAgFnPc2d1	chemická
<g/>
)	)	kIx)	)
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
elektromagnetického	elektromagnetický	k2eAgInSc2d1	elektromagnetický
impulsu	impuls	k1gInSc2	impuls
<g/>
,	,	kIx,	,
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
uvolnění	uvolnění	k1gNnSc4	uvolnění
množství	množství	k1gNnSc2	množství
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
spad	spad	k1gInSc1	spad
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
na	na	k7c6	na
celkově	celkově	k6eAd1	celkově
uvolněné	uvolněný	k2eAgFnSc6d1	uvolněná
energii	energie	k1gFnSc6	energie
není	být	k5eNaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
dávka	dávka	k1gFnSc1	dávka
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
oběti	oběť	k1gFnPc1	oběť
atomového	atomový	k2eAgInSc2d1	atomový
útoku	útok	k1gInSc2	útok
vystaveny	vystaven	k2eAgInPc1d1	vystaven
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
devastující	devastující	k2eAgInPc4d1	devastující
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
zdraví	zdraví	k1gNnSc4	zdraví
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zdraví	zdraví	k1gNnSc2	zdraví
jejich	jejich	k3xOp3gMnPc2	jejich
případných	případný	k2eAgMnPc2d1	případný
potomků	potomek	k1gMnPc2	potomek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
velmoc	velmoc	k1gFnSc1	velmoc
</s>
</p>
<p>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
test	test	k1gInSc1	test
</s>
</p>
<p>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
výbuch	výbuch	k1gInSc1	výbuch
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
zima	zima	k1gFnSc1	zima
</s>
</p>
<p>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
zbraň	zbraň	k1gFnSc1	zbraň
</s>
</p>
<p>
<s>
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HOLLOWAY	HOLLOWAY	kA	HOLLOWAY
<g/>
,	,	kIx,	,
DAVID	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
jaderná	jaderný	k2eAgFnSc1d1	jaderná
energie	energie	k1gFnSc1	energie
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
PITSCHMANN	PITSCHMANN	kA	PITSCHMANN
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
:	:	kIx,	:
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
forma	forma	k1gFnSc1	forma
zabíjení	zabíjení	k1gNnSc2	zabíjení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
784	[number]	k4	784
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ULVR	ULVR	kA	ULVR
<g/>
,	,	kIx,	,
MICHAL	Michal	k1gMnSc1	Michal
<g/>
:	:	kIx,	:
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-496-7	[number]	k4	978-80-7308-496-7
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jaderná	jaderný	k2eAgFnSc1d1	jaderná
zbraň	zbraň	k1gFnSc1	zbraň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Mezníky	mezník	k1gInPc1	mezník
jaderného	jaderný	k2eAgInSc2d1	jaderný
věku	věk	k1gInSc2	věk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Archív	archív	k1gInSc1	archív
o	o	k7c6	o
jaderných	jaderný	k2eAgFnPc6d1	jaderná
zbraních	zbraň	k1gFnPc6	zbraň
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
půjde	jít	k5eAaImIp3nS	jít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
arzenál	arzenál	k1gInSc4	arzenál
</s>
</p>
<p>
<s>
Neznámá	známý	k2eNgFnSc1d1	neznámá
historie	historie	k1gFnSc1	historie
jaderného	jaderný	k2eAgNnSc2d1	jaderné
zbrojení	zbrojení	k1gNnSc2	zbrojení
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
</s>
</p>
