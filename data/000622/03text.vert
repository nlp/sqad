<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
France	Franc	k1gMnSc2	Franc
nebo	nebo	k8xC	nebo
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
Valois	Valois	k1gFnPc2	Valois
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1401	[number]	k4	1401
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1437	[number]	k4	1437
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
manželka	manželka	k1gFnSc1	manželka
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
Díky	díky	k7c3	díky
tajnému	tajný	k2eAgNnSc3d1	tajné
manželství	manželství	k1gNnSc3	manželství
s	s	k7c7	s
Owenem	Owen	k1gMnSc7	Owen
Tudorem	tudor	k1gInSc7	tudor
byla	být	k5eAaImAgFnS	být
také	také	k9	také
babičkou	babička	k1gFnSc7	babička
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudora	tudor	k1gMnSc4	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Saint-Poljako	Saint-Poljako	k1gNnSc1	Saint-Poljako
dcera	dcera	k1gFnSc1	dcera
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Isabely	Isabela	k1gFnSc2	Isabela
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Štěpána	Štěpán	k1gMnSc2	Štěpán
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
princezny	princezna	k1gFnSc2	princezna
Kateřiny	Kateřina	k1gFnSc2	Kateřina
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Anglického	anglický	k2eAgInSc2d1	anglický
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
o	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
jeho	jeho	k3xOp3gInSc1	jeho
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svým	svůj	k3xOyFgNnSc7	svůj
vítězstvím	vítězství	k1gNnSc7	vítězství
u	u	k7c2	u
Azincourtu	Azincourt	k1gInSc2	Azincourt
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
kapitolu	kapitola	k1gFnSc4	kapitola
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
také	také	k9	také
zvěčnil	zvěčnit	k5eAaPmAgInS	zvěčnit
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hře	hra	k1gFnSc6	hra
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
mělo	mít	k5eAaImAgNnS	mít
těmto	tento	k3xDgNnPc3	tento
dvěma	dva	k4xCgInPc3	dva
národům	národ	k1gInPc3	národ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spolu	spolu	k6eAd1	spolu
válčily	válčit	k5eAaImAgInP	válčit
už	už	k6eAd1	už
víc	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přinést	přinést	k5eAaPmF	přinést
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1420	[number]	k4	1420
byla	být	k5eAaImAgFnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
uznal	uznat	k5eAaPmAgMnS	uznat
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Anglického	anglický	k2eAgMnSc4d1	anglický
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
dědice	dědic	k1gMnSc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1420	[number]	k4	1420
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c4	v
Troyes	Troyes	k1gInSc4	Troyes
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
prý	prý	k9	prý
byla	být	k5eAaImAgFnS	být
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zřejmě	zřejmě	k6eAd1	zřejmě
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Odebrali	odebrat	k5eAaPmAgMnP	odebrat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
Kateřina	Kateřina	k1gFnSc1	Kateřina
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
korunována	korunovat	k5eAaBmNgFnS	korunovat
anglickou	anglický	k2eAgFnSc7d1	anglická
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1421	[number]	k4	1421
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
Kateřina	Kateřina	k1gFnSc1	Kateřina
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
těhotná	těhotná	k1gFnSc1	těhotná
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
svého	svůj	k1gMnSc4	svůj
syna	syn	k1gMnSc4	syn
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1422	[number]	k4	1422
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
úplavici	úplavice	k1gFnSc6	úplavice
<g/>
.	.	kIx.	.
</s>
<s>
Jednadvacetiletá	jednadvacetiletý	k2eAgFnSc1d1	jednadvacetiletá
vdova	vdova	k1gFnSc1	vdova
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
i	i	k8xC	i
o	o	k7c6	o
otce	otka	k1gFnSc6	otka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
dědicem	dědic	k1gMnSc7	dědic
anglického	anglický	k2eAgInSc2d1	anglický
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
francouzským	francouzský	k2eAgInSc7d1	francouzský
trůnem	trůn	k1gInSc7	trůn
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
stoleté	stoletý	k2eAgFnSc6d1	stoletá
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gMnSc1	Owen
Tudor	tudor	k1gInSc4	tudor
byl	být	k5eAaImAgMnS	být
garderobiér	garderobiér	k1gMnSc1	garderobiér
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
královny	královna	k1gFnSc2	královna
vdovy	vdova	k1gFnSc2	vdova
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
sňatek	sňatek	k1gInSc1	sňatek
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zpochybňován	zpochybňován	k2eAgMnSc1d1	zpochybňován
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
zmínila	zmínit	k5eAaPmAgFnS	zmínit
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
sňatek	sňatek	k1gInSc1	sňatek
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
argumentem	argument	k1gInSc7	argument
pro	pro	k7c4	pro
legitimitu	legitimita	k1gFnSc4	legitimita
dynastie	dynastie	k1gFnSc2	dynastie
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gInSc1	Owen
Tudor	tudor	k1gInSc1	tudor
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Královnu	královna	k1gFnSc4	královna
nejvíce	nejvíce	k6eAd1	nejvíce
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
svým	svůj	k3xOyFgInSc7	svůj
"	"	kIx"	"
<g/>
činem	čin	k1gInSc7	čin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
opil	opít	k5eAaPmAgMnS	opít
a	a	k8xC	a
usnul	usnout	k5eAaPmAgMnS	usnout
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
klíně	klín	k1gInSc6	klín
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
a	a	k8xC	a
Owen	Owen	k1gNnSc4	Owen
Tudor	tudor	k1gInSc4	tudor
spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
syna	syn	k1gMnSc4	syn
Edmunda	Edmund	k1gMnSc4	Edmund
Tudora	tudor	k1gMnSc4	tudor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gInSc1	Owen
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
měli	mít	k5eAaImAgMnP	mít
přinejmenším	přinejmenším	k6eAd1	přinejmenším
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
čtyři	čtyři	k4xCgInPc1	čtyři
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
dospělosti	dospělost	k1gFnPc4	dospělost
<g/>
,	,	kIx,	,
syny	syn	k1gMnPc4	syn
Edmunda	Edmund	k1gMnSc2	Edmund
<g/>
,	,	kIx,	,
Jaspera	Jasper	k1gMnSc2	Jasper
a	a	k8xC	a
Owena	Owen	k1gMnSc2	Owen
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Markétu	Markéta	k1gFnSc4	Markéta
a	a	k8xC	a
Tacinu	Tacina	k1gFnSc4	Tacina
<g/>
.	.	kIx.	.
</s>
<s>
Tajný	tajný	k2eAgInSc1d1	tajný
sňatek	sňatek	k1gInSc1	sňatek
Kateřiny	Kateřina	k1gFnSc2	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
a	a	k8xC	a
Owena	Owen	k1gMnSc2	Owen
Tudora	tudor	k1gMnSc2	tudor
velmi	velmi	k6eAd1	velmi
pobouřil	pobouřit	k5eAaPmAgInS	pobouřit
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
domluvila	domluvit	k5eAaPmAgFnS	domluvit
přijmout	přijmout	k5eAaPmF	přijmout
nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
oddat	oddat	k5eAaPmF	oddat
až	až	k9	až
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
zemřela	zemřít	k5eAaPmAgFnS	zemřít
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1437	[number]	k4	1437
společně	společně	k6eAd1	společně
s	s	k7c7	s
novorozenou	novorozený	k2eAgFnSc7d1	novorozená
dcerou	dcera	k1gFnSc7	dcera
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
však	však	k9	však
léčila	léčit	k5eAaImAgFnS	léčit
z	z	k7c2	z
jakési	jakýsi	k3yIgFnSc2	jakýsi
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
možná	možná	k9	možná
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Owen	Owen	k1gInSc1	Owen
Tudor	tudor	k1gInSc1	tudor
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
zadržen	zadržet	k5eAaPmNgMnS	zadržet
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
nespecifikovaných	specifikovaný	k2eNgInPc2d1	nespecifikovaný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1461	[number]	k4	1461
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgInSc1d1	popraven
yorkisty	yorkista	k1gMnPc7	yorkista
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mortimers	Mortimersa	k1gFnPc2	Mortimersa
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc3	jejich
synům	syn	k1gMnPc3	syn
udělil	udělit	k5eAaPmAgInS	udělit
jejich	jejich	k3xOp3gMnSc4	jejich
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
hrabství	hrabství	k1gNnSc1	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
alabastrový	alabastrový	k2eAgInSc1d1	alabastrový
památník	památník	k1gInSc1	památník
možná	možná	k9	možná
nechal	nechat	k5eAaPmAgInS	nechat
zničit	zničit	k5eAaPmF	zničit
její	její	k3xOp3gMnSc1	její
vnuk	vnuk	k1gMnSc1	vnuk
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
oprostil	oprostit	k5eAaPmAgMnS	oprostit
od	od	k7c2	od
podezření	podezření	k1gNnSc2	podezření
nemanželského	manželský	k2eNgInSc2d1	nemanželský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
veřejně	veřejně	k6eAd1	veřejně
vystaveny	vystavit	k5eAaPmNgInP	vystavit
a	a	k8xC	a
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
až	až	k6eAd1	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Kateřiny	Kateřina	k1gFnSc2	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
popisuje	popisovat	k5eAaImIp3nS	popisovat
děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
Mari	Mar	k1gFnSc2	Mar
Griffithové	Griffithová	k1gFnSc2	Griffithová
Kořeny	kořen	k1gInPc1	kořen
růže	růž	k1gFnSc2	růž
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
Kateřinin	Kateřinin	k2eAgInSc4d1	Kateřinin
život	život	k1gInSc4	život
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k9	co
opustila	opustit	k5eAaPmAgFnS	opustit
klášter	klášter	k1gInSc4	klášter
až	až	k9	až
po	po	k7c4	po
její	její	k3xOp3gNnSc4	její
úmrtí	úmrtí	k1gNnSc4	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Catherine	Catherin	k1gInSc5	Catherin
of	of	k?	of
Valois	Valois	k1gFnPc1	Valois
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
