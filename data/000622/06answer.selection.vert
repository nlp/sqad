<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
France	Franc	k1gMnSc2	Franc
nebo	nebo	k8xC	nebo
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
Valois	Valois	k1gFnPc2	Valois
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1401	[number]	k4	1401
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1437	[number]	k4	1437
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
manželka	manželka	k1gFnSc1	manželka
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
Díky	díky	k7c3	díky
tajnému	tajný	k2eAgNnSc3d1	tajné
manželství	manželství	k1gNnSc3	manželství
s	s	k7c7	s
Owenem	Owen	k1gMnSc7	Owen
Tudorem	tudor	k1gInSc7	tudor
byla	být	k5eAaImAgFnS	být
také	také	k9	také
babičkou	babička	k1gFnSc7	babička
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
