<s>
Timogenes	Timogenes	k1gInSc1
dorbignyi	dorbigny	k1gFnSc2
</s>
<s>
Timogenes	Timogenes	k1gMnSc1
dorbignyi	dorbignyi	k6eAd1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
klepítkatci	klepítkatec	k1gMnPc1
(	(	kIx(
<g/>
Chelicerata	Chelicerat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
pavoukovci	pavoukovec	k1gMnPc1
(	(	kIx(
<g/>
Arachnida	Arachnida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
štíři	štír	k1gMnPc1
(	(	kIx(
<g/>
Scorpionida	Scorpionida	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
Bothriuridae	Bothriuridae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
Timogenes	Timogenes	k1gInSc1
(	(	kIx(
<g/>
Timogenes	Timogenes	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Timogenes	Timogenes	k1gInSc1
dorbignyiGuérin	dorbignyiGuérin	k1gInSc1
Méneville	Méneville	k1gFnSc1
<g/>
,	,	kIx,
1843	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Timogenes	Timogenes	k1gInSc1
dorbignyi	dorbigny	k1gFnSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
druhů	druh	k1gInPc2
celé	celý	k2eAgFnSc2d1
čeledi	čeleď	k1gFnSc2
štírů	štír	k1gMnPc2
Bothriuridae	Bothriurida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInPc1d2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
Timogenes	Timogenes	k1gInSc4
elegans	elegansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS
velikosti	velikost	k1gFnSc2
60-90	60-90	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvení	zbarvení	k1gNnPc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
žluté	žlutý	k2eAgNnSc1d1
až	až	k6eAd1
světložluté	světložlutý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
,	,	kIx,
Bolívii	Bolívie	k1gFnSc6
<g/>
,	,	kIx,
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
Paraguayi	Paraguay	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
mláďat	mládě	k1gNnPc2
ve	v	k7c6
vrhu	vrh	k1gInSc6
činil	činit	k5eAaImAgInS
18	#num#	k4
larev	larva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jed	jed	k1gInSc1
není	být	k5eNaImIp3nS
nebezpečný	bezpečný	k2eNgMnSc1d1
a	a	k8xC
štír	štír	k1gMnSc1
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
agresivní	agresivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
