<s>
Halle	Halle	k1gFnSc1
Open	Opena	k1gFnPc2
</s>
<s>
Halle	Halle	k6eAd1
OpenNoventi	OpenNovent	k1gMnPc1
Open	Open	k1gNnSc4
Průčelí	průčelí	k1gNnSc2
stadionu	stadion	k1gInSc2
se	s	k7c7
starým	starý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Gerry	Gerra	k1gFnSc2
Weber	Weber	k1gMnSc1
Stadion	stadion	k1gInSc1
(	(	kIx(
<g/>
leden	leden	k1gInSc1
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1993	#num#	k4
Odehráno	odehrát	k5eAaPmNgNnS
</s>
<s>
Noventi	Novent	k1gMnPc1
Open	Open	k1gMnSc1
2019	#num#	k4
Místo	místo	k1gNnSc1
</s>
<s>
Halle	Halle	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgNnSc1d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc1
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
52	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Povrch	povrch	k1gInSc1
</s>
<s>
tráva	tráva	k1gFnSc1
/	/	kIx~
venku	venku	k6eAd1
Soutěže	soutěž	k1gFnPc1
</s>
<s>
32	#num#	k4
dvouhra	dvouhra	k1gFnSc1
(	(	kIx(
<g/>
16	#num#	k4
kval	kvala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
/	/	kIx~
16	#num#	k4
čtyřhra	čtyřhra	k1gFnSc1
Dotace	dotace	k1gFnSc1
</s>
<s>
1	#num#	k4
966	#num#	k4
095	#num#	k4
EUR	euro	k1gNnPc2
Období	období	k1gNnSc2
</s>
<s>
červen	červen	k1gInSc1
</s>
<s>
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
1990	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
</s>
<s>
World	World	k1gInSc1
Series	Series	k1gInSc1
1998	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Series	Series	k1gInSc1
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
250	#num#	k4
2015	#num#	k4
<g/>
–	–	k?
</s>
<s>
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
500	#num#	k4
</s>
<s>
www.gerryweber-open.de	www.gerryweber-open.de	k6eAd1
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Philipp	Philipp	k1gMnSc1
Kohlschreiber	Kohlschreiber	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Halle	Halle	k1gFnSc1
Open	Opena	k1gFnPc2
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
se	s	k7c7
jménem	jméno	k1gNnSc7
sponzora	sponzor	k1gMnSc2
Noventi	Novent	k1gMnPc1
Open	Open	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1993	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Gerry	Gerr	k1gInPc7
Weber	Weber	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mužský	mužský	k2eAgInSc4d1
profesionální	profesionální	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
v	v	k7c6
tenise	tenis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
otevřených	otevřený	k2eAgInPc6d1
travnatých	travnatý	k2eAgInPc6d1
dvorcích	dvorec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostitelským	hostitelský	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Halle	Halle	k1gFnSc1
v	v	k7c6
německé	německý	k2eAgFnSc6d1
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Severní	severní	k2eAgNnSc4d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Premiérový	premiérový	k2eAgInSc1d1
ročník	ročník	k1gInSc1
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
patří	patřit	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
okruhu	okruh	k1gInSc2
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
do	do	k7c2
kategorie	kategorie	k1gFnSc2
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezónách	sezóna	k1gFnPc6
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
kategorie	kategorie	k1gFnSc2
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
dvorec	dvorec	k1gInSc1
areálu	areál	k1gInSc2
–	–	k?
OWL	OWL	kA
Arena	Areen	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
12	#num#	k4
300	#num#	k4
diváků	divák	k1gMnPc2
je	být	k5eAaImIp3nS
opatřen	opatřit	k5eAaPmNgInS
zatahovací	zatahovací	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
uzavře	uzavřít	k5eAaPmIp3nS
dvorec	dvorec	k1gInSc4
před	před	k7c7
deštěm	dešť	k1gInSc7
za	za	k7c4
88	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadión	stadión	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
k	k	k7c3
dalším	další	k2eAgFnPc3d1
sportovním	sportovní	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
jakými	jaký	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
jsou	být	k5eAaImIp3nP
házená	házená	k1gFnSc1
<g/>
,	,	kIx,
basketbal	basketbal	k1gInSc1
<g/>
,	,	kIx,
volejbal	volejbal	k1gInSc1
a	a	k8xC
box	box	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konají	konat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
také	také	k9
hudební	hudební	k2eAgInSc4d1
koncerty	koncert	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
vítězství	vítězství	k1gNnSc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgMnS
Švýcar	Švýcar	k1gMnSc1
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
devětkrát	devětkrát	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
a	a	k8xC
2011	#num#	k4
z	z	k7c2
události	událost	k1gFnSc2
odhlásil	odhlásit	k5eAaPmAgMnS
pro	pro	k7c4
únavu	únava	k1gFnSc4
po	po	k7c6
třech	tři	k4xCgNnPc6
prohraných	prohraný	k2eAgNnPc6d1
finále	finále	k1gNnPc6
na	na	k7c6
pařížském	pařížský	k2eAgInSc6d1
grandslamu	grandslam	k1gInSc6
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Rafaelu	Rafael	k1gMnSc6
Nadalovi	Nadal	k1gMnSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgMnS
druhým	druhý	k4xOgMnSc7
mužem	muž	k1gMnSc7
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
na	na	k7c6
jediném	jediný	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
triumfoval	triumfovat	k5eAaBmAgMnS
alespoň	alespoň	k9
devětkrát	devětkrát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2010	#num#	k4
Federer	Federer	k1gInSc1
uzavřel	uzavřít	k5eAaPmAgInS
s	s	k7c7
pořadateli	pořadatel	k1gMnPc7
turnaje	turnaj	k1gInSc2
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
každoroční	každoroční	k2eAgFnSc4d1
účast	účast	k1gFnSc4
na	na	k7c6
této	tento	k3xDgFnSc6
události	událost	k1gFnSc6
až	až	k9
do	do	k7c2
skončení	skončení	k1gNnSc2
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekordy	rekord	k1gInPc1
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
:	:	kIx,
10	#num#	k4
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
:	:	kIx,
4	#num#	k4
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
bez	bez	k7c2
ztráty	ztráta	k1gFnSc2
setu	set	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
finále	finále	k1gNnSc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
:	:	kIx,
13	#num#	k4
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
finále	finále	k1gNnSc2
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
1993	#num#	k4
Henri	Henri	k1gNnSc1
Leconte	Lecont	k1gInSc5
Andrej	Andrej	k1gMnSc1
Medveděv	Medveděv	k1gFnSc2
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1994	#num#	k4
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
Magnus	Magnus	k1gMnSc1
Larsson	Larsson	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1995	#num#	k4
Marc	Marc	k1gFnSc1
Rosset	Rosseta	k1gFnPc2
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
13	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
Nicklas	Nicklas	k1gInSc1
Kulti	Kulť	k1gFnSc2
Jevgenij	Jevgenij	k1gFnSc2
Kafelnikov	Kafelnikov	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1997	#num#	k4
Jevgenij	Jevgenij	k1gFnPc2
Kafelnikov	Kafelnikov	k1gInSc1
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc4
Magnus	Magnus	k1gInSc1
Larsson	Larsson	k1gNnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1999	#num#	k4
Nicolas	Nicolas	k1gMnSc1
Kiefer	Kiefer	k1gMnSc1
Nicklas	Nicklas	k1gMnSc1
Kulti	Kulť	k1gFnSc2
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2000	#num#	k4
David	David	k1gMnSc1
Prinosil	Prinosil	k1gMnSc1
Richard	Richard	k1gMnSc1
Krajicek	Krajicka	k1gFnPc2
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2001	#num#	k4
Thomas	Thomas	k1gMnSc1
Johansson	Johansson	k1gMnSc1
Fabrice	fabrika	k1gFnSc3
Santoro	Santora	k1gFnSc5
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2002	#num#	k4
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc4
Nicolas	Nicolas	k1gInSc1
Kiefer	Kiefer	k1gInSc1
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2003	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Nicolas	Nicolas	k1gMnSc1
Kiefer	Kiefer	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2004	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
Mardy	Marda	k1gFnSc2
Fish	Fish	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2005	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Marat	Marat	k2eAgInSc4d1
Safin	Safin	k1gInSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2006	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2007	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
Marcos	Marcos	k1gMnSc1
Baghdatis	Baghdatis	k1gInSc4
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2008	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Philipp	Philipp	k1gMnSc1
Kohlschreiber	Kohlschreiber	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2009	#num#	k4
Tommy	Tomma	k1gFnSc2
Haas	Haasa	k1gFnPc2
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
2010	#num#	k4
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k2eAgInSc4d1
Roger	Roger	k1gInSc4
Federer	Federra	k1gFnPc2
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2011	#num#	k4
Philipp	Philipp	k1gMnSc1
Kohlschreiber	Kohlschreiber	k1gMnSc1
Philipp	Philipp	k1gMnSc1
Petzschner	Petzschner	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
skreč	skreč	k1gFnSc1
</s>
<s>
2012	#num#	k4
Tommy	Tomma	k1gFnSc2
Haas	Haasa	k1gFnPc2
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2013	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Michail	Michail	k1gMnSc1
Južnyj	Južnyj	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2014	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Alejandro	Alejandra	k1gFnSc5
Falla	Fall	k1gMnSc4
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Andreas	Andreas	k1gMnSc1
Seppi	Sepp	k1gFnSc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2016	#num#	k4
Florian	Florian	k1gMnSc1
Mayer	Mayer	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Zverev	Zverev	k1gFnSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2017	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Zverev	Zverev	k1gFnSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2018	#num#	k4
Borna	Born	k1gInSc2
Ćorić	Ćorić	k1gMnSc1
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2019	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
David	David	k1gMnSc1
Goffin	Goffin	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
2020	#num#	k4
<g/>
Turnaj	turnaj	k1gInSc1
nebude	být	k5eNaImBp3nS
uspořádán	uspořádat	k5eAaPmNgInS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
koronaviru	koronavir	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
Finalisté	finalista	k1gMnPc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
1993	#num#	k4
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Suk	Suk	k1gMnSc1
Mike	Mik	k1gFnSc2
Bauer	Bauer	k1gMnSc1
Marc-Kevin	Marc-Kevina	k1gFnPc2
Goellner	Goellner	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1994	#num#	k4
Olivier	Olivier	k1gMnSc1
Delaître	Delaîtr	k1gInSc5
Guy	Guy	k1gMnSc4
Forget	Forget	k1gMnSc1
Henri	Henr	k1gFnSc2
Leconte	Lecont	k1gInSc5
Gary	Gary	k1gInPc4
Muller	Muller	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1995	#num#	k4
Jacco	Jacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
Paul	Paul	k1gMnSc1
Haarhuis	Haarhuis	k1gFnPc2
Jevgenij	Jevgenij	k1gFnPc2
Kafelnikov	Kafelnikov	k1gInSc1
Andrej	Andrej	k1gMnSc1
Olchovskij	Olchovskij	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1996	#num#	k4
Byron	Byrona	k1gFnPc2
Black	Blacka	k1gFnPc2
Grant	grant	k1gInSc1
Connell	Connell	k1gMnSc1
Jevgenij	Jevgenij	k1gFnPc2
Kafelnikov	Kafelnikov	k1gInSc1
Daniel	Daniel	k1gMnSc1
Vacek	Vacek	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
1997	#num#	k4
Karsten	Karsten	k2eAgInSc4d1
Braasch	Braasch	k1gInSc4
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
David	David	k1gMnSc1
Adams	Adamsa	k1gFnPc2
Marius	Marius	k1gMnSc1
Barnard	Barnard	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1998	#num#	k4
Ellis	Ellis	k1gFnPc2
Ferreira	Ferreira	k1gMnSc1
Rick	Rick	k1gMnSc1
Leach	Leach	k1gMnSc1
John-Laffnie	John-Laffnie	k1gFnSc2
de	de	k?
Jager	Jager	k1gInSc1
Marc-Kevin	Marc-Kevina	k1gFnPc2
Goellner	Goellnero	k1gNnPc2
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
1999	#num#	k4
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
Paul	Paul	k1gMnSc1
Haarhuis	Haarhuis	k1gFnPc2
Jared	Jared	k1gMnSc1
Palmer	Palmer	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
2000	#num#	k4
Nicklas	Nicklas	k1gInSc1
Kulti	Kulť	k1gFnSc2
Mikael	Mikael	k1gMnSc1
Tillström	Tillström	k1gMnSc1
Mahesh	Mahesh	k1gMnSc1
Bhupathi	Bhupath	k1gFnSc2
David	David	k1gMnSc1
Prinosil	Prinosil	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
Sandon	Sandon	k1gMnSc1
Stolle	Stolle	k1gFnSc2
Max	Max	k1gMnSc1
Mirnyj	Mirnyj	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
2002	#num#	k4
David	David	k1gMnSc1
Prinosil	Prinosil	k1gMnSc1
David	David	k1gMnSc1
Rikl	Rikl	k1gMnSc1
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
Todd	Todd	k1gMnSc1
Woodbridge	Woodbridge	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
2003	#num#	k4
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
Todd	Todd	k1gMnSc1
Woodbridge	Woodbridge	k1gFnPc2
Martin	Martin	k1gMnSc1
Damm	Damm	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Suk	Suk	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2004	#num#	k4
Leander	Leander	k1gMnSc1
Paes	Paesa	k1gFnPc2
David	David	k1gMnSc1
Rikl	Rikl	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Cibulec	Cibulec	k1gMnSc1
Petr	Petr	k1gMnSc1
Pála	Pála	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
2005	#num#	k4
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
Yves	Yvesa	k1gFnPc2
Allegro	allegro	k1gNnSc1
Joachim	Joachim	k1gMnSc1
Johansson	Johansson	k1gMnSc1
Marat	Marat	k2eAgInSc4d1
Safin	Safin	k1gInSc4
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2006	#num#	k4
Fabrice	fabrika	k1gFnSc3
Santoro	Santora	k1gFnSc5
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gMnSc1
Michael	Michael	k1gMnSc1
Kohlmann	Kohlmann	k1gMnSc1
Rainer	Rainer	k1gMnSc1
Schüttler	Schüttler	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2007	#num#	k4
Simon	Simona	k1gFnPc2
Aspelin	Aspelin	k2eAgMnSc1d1
Julian	Julian	k1gMnSc1
Knowle	Knowle	k1gFnSc2
Fabrice	fabrika	k1gFnSc3
Santoro	Santora	k1gFnSc5
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Michail	Michail	k1gMnSc1
Južnyj	Južnyj	k1gMnSc1
Mischa	Mischa	k1gMnSc1
Zverev	Zverva	k1gFnPc2
Lukáš	Lukáš	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
Leander	Leander	k1gMnSc1
Paes	Paes	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2009	#num#	k4
Christopher	Christophra	k1gFnPc2
Kas	kasa	k1gFnPc2
Philipp	Philipp	k1gInSc1
Kohlschreiber	Kohlschreiber	k1gMnSc1
Andreas	Andreas	k1gMnSc1
Beck	Beck	k1gMnSc1
Marco	Marco	k6eAd1
Chiudinelli	Chiudinelle	k1gFnSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2010	#num#	k4
Sergij	Sergij	k1gMnSc1
Stachovskij	Stachovskij	k1gMnSc1
Michail	Michail	k1gMnSc1
Južnyj	Južnyj	k1gMnSc1
Martin	Martin	k1gMnSc1
Damm	Damm	k1gMnSc1
Filip	Filip	k1gMnSc1
Polášek	Polášek	k1gMnSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
Rohan	Rohan	k1gMnSc1
Bopanna	Bopann	k1gInSc2
Ajsám	Ajsa	k1gFnPc3
Kúreší	Kúrechý	k2eAgMnPc1d1
Robin	Robina	k1gFnPc2
Haase	Haase	k1gFnSc2
Milos	Milos	k1gMnSc1
Raonic	Raonice	k1gFnPc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
11	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
Ajsám	Ajsa	k1gFnPc3
Kúreší	Kúrechý	k2eAgMnPc1d1
Jean-Julien	Jean-Julien	k2eAgMnSc1d1
Rojer	Rojer	k1gMnSc1
Treat	Treat	k2eAgInSc4d1
Conrad	Conrad	k1gInSc4
Huey	Huea	k1gFnSc2
Scott	Scott	k2eAgInSc4d1
Lipsky	lipsky	k6eAd1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2013	#num#	k4
Santiago	Santiago	k1gNnSc1
González	Gonzáleza	k1gFnPc2
Scott	Scotta	k1gFnPc2
Lipsky	lipsky	k6eAd1
Daniele	Daniela	k1gFnSc6
Bracciali	Bracciali	k1gFnSc6
Jonatan	Jonatan	k1gMnSc1
Erlich	Erlich	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Andre	Andr	k1gInSc5
Begemann	Begemanno	k1gNnPc2
Julian	Julian	k1gMnSc1
Knowle	Knowle	k1gNnSc2
Marco	Marco	k1gMnSc1
Chiudinelli	Chiudinell	k1gMnPc1
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
Raven	Raven	k1gInSc1
Klaasen	Klaasen	k2eAgInSc1d1
Rajeev	Rajeev	k1gFnSc4
Ram	Ram	k1gFnSc2
Rohan	Rohan	k1gMnSc1
Bopanna	Bopanno	k1gNnSc2
Florin	Florin	k1gInSc1
Mergea	Merge	k1gInSc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2016	#num#	k4
Raven	Raven	k1gInSc1
Klaasen	Klaasen	k2eAgInSc1d1
Rajeev	Rajeev	k1gFnSc4
Ram	Ram	k1gFnSc2
Łukasz	Łukasz	k1gInSc1
Kubot	Kubot	k1gInSc1
Alexander	Alexandra	k1gFnPc2
Peya	Pey	k1gInSc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2017	#num#	k4
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
Alexander	Alexandra	k1gFnPc2
Zverev	Zverev	k1gFnSc1
Mischa	Mischa	k1gFnSc1
Zverev	Zverev	k1gFnSc1
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
Mischa	Mischa	k1gMnSc1
Zverev	Zverev	k1gFnSc4
Alexander	Alexandra	k1gFnPc2
Zverev	Zvervo	k1gNnPc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
2019	#num#	k4
Raven	Ravna	k1gFnPc2
Klaasen	Klaasen	k2eAgMnSc1d1
Michael	Michael	k1gMnSc1
Venus	Venus	k1gMnSc1
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
<g/>
Turnaj	turnaj	k1gInSc1
nebude	být	k5eNaImBp3nS
uspořádán	uspořádat	k5eAaPmNgInS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
koronaviru	koronavir	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Halle	Halle	k1gFnSc2
Open	Opena	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ZABLOUDIL	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gInSc1
smetl	smetnout	k5eAaPmAgInS
Zvereva	Zverevo	k1gNnPc4
a	a	k8xC
má	mít	k5eAaImIp3nS
92	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Halle	Hall	k1gInSc6
vyhrál	vyhrát	k5eAaPmAgMnS
už	už	k6eAd1
podeváté	podeváté	k4xO
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-06-25	2017-06-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Federer	Federer	k1gInSc1
se	se	k3xPyFc4
upsal	upsat	k5eAaPmAgInS
Halle	Halle	k1gInSc1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
tam	tam	k6eAd1
hrát	hrát	k5eAaImF
do	do	k7c2
konce	konec	k1gInSc2
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
tenisportál	tenisportál	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-07	2010-06-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gerry	Gerra	k1gFnSc2
Weber	weber	k1gInSc1
Open	Open	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
NOVENTI	NOVENTI	kA
OPEN	OPEN	kA
–	–	k?
oficiální	oficiální	k2eAgInSc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
NOVENTI	NOVENTI	kA
OPEN	OPEN	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
ATP	atp	kA
Tour	Tour	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
Hrané	hraný	k2eAgInPc1d1
</s>
<s>
Rotterdam	Rotterdam	k1gInSc1
•	•	k?
Rio	Rio	k1gFnPc2
de	de	k?
Janeiro	Janeiro	k1gNnSc4
•	•	k?
Acapulco	Acapulco	k1gMnSc1
•	•	k?
Dubaj	Dubaj	k1gMnSc1
•	•	k?
Barcelona	Barcelona	k1gFnSc1
•	•	k?
Halle	Halle	k1gInSc1
•	•	k?
Londýn	Londýn	k1gInSc1
•	•	k?
Hamburk	Hamburk	k1gInSc1
•	•	k?
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
•	•	k?
Peking	Peking	k1gInSc1
•	•	k?
Tokio	Tokio	k1gNnSc1
•	•	k?
Vídeň	Vídeň	k1gFnSc1
•	•	k?
Basilej	Basilej	k1gFnSc1
Vyřazené	vyřazený	k2eAgFnPc1d1
</s>
<s>
Memphis	Memphis	k1gFnSc1
•	•	k?
Valencie	Valencie	k1gFnSc2
•	•	k?
Petrohrad	Petrohrad	k1gInSc1
Seznam	seznam	k1gInSc1
vítězů	vítěz	k1gMnPc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gMnSc1
250	#num#	k4
Kalendář	kalendář	k1gInSc1
2019	#num#	k4
</s>
<s>
Dauhá	Dauhý	k2eAgFnSc1d1
•	•	k?
Puné	Puná	k1gFnSc3
•	•	k?
Brisbane	Brisban	k1gMnSc5
•	•	k?
Sydney	Sydney	k1gNnSc4
•	•	k?
Auckland	Auckland	k1gInSc1
•	•	k?
Montpellier	Montpellier	k1gInSc1
•	•	k?
Córdoba	Córdoba	k1gFnSc1
•	•	k?
Sofie	Sofie	k1gFnSc1
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
•	•	k?
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
•	•	k?
Marseille	Marseille	k1gFnPc4
•	•	k?
Delray	Delraa	k1gFnSc2
Beach	Beach	k1gMnSc1
•	•	k?
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
•	•	k?
Houston	Houston	k1gInSc1
•	•	k?
Estoril	Estoril	k1gInSc1
•	•	k?
Casablanca	Casablanca	k1gFnSc1
•	•	k?
Budapešť	Budapešť	k1gFnSc1
•	•	k?
Mnichov	Mnichov	k1gInSc1
•	•	k?
Ženeva	Ženeva	k1gFnSc1
•	•	k?
Lyon	Lyon	k1gInSc1
•	•	k?
'	'	kIx"
<g/>
s-Hertogenbosch	s-Hertogenbosch	k1gMnSc1
•	•	k?
Stuttgart	Stuttgart	k1gInSc1
•	•	k?
Antalya	Antaly	k1gInSc2
•	•	k?
Eastbourne	Eastbourn	k1gInSc5
•	•	k?
Newport	Newport	k1gInSc1
•	•	k?
Bå	Bå	k1gInSc1
•	•	k?
Umag	Umag	k1gInSc1
•	•	k?
Atlanta	Atlanta	k1gFnSc1
•	•	k?
Los	los	k1gInSc1
Cabos	Cabos	k1gInSc1
•	•	k?
Gstaad	Gstaad	k1gInSc1
•	•	k?
Kitzbühel	Kitzbühel	k1gInSc1
•	•	k?
Winston-Salem	Winston-Sal	k1gInSc7
•	•	k?
Mety	meta	k1gFnSc2
•	•	k?
Petrohrad	Petrohrad	k1gInSc1
•	•	k?
Ču-chaj	Ču-chaj	k1gInSc1
•	•	k?
Čcheng-tu	Čcheng-t	k1gInSc2
•	•	k?
Stockholm	Stockholm	k1gInSc1
•	•	k?
Moskva	Moskva	k1gFnSc1
•	•	k?
Antverpy	Antverpy	k1gFnPc5
Vyřazené	vyřazený	k2eAgFnSc6d1
</s>
<s>
Johannesburg	Johannesburg	k1gInSc1
•	•	k?
Bělehrad	Bělehrad	k1gInSc1
•	•	k?
Los	los	k1gInSc1
Angeles	Angeles	k1gMnSc1
•	•	k?
San	San	k1gMnSc1
Jose	Jos	k1gFnSc2
•	•	k?
Bangkok	Bangkok	k1gInSc1
•	•	k?
Viñ	Viñ	k1gInSc2
del	del	k?
Mar	Mar	k1gMnSc1
•	•	k?
Oeiras	Oeiras	k1gMnSc1
•	•	k?
Düsseldorf	Düsseldorf	k1gMnSc1
•	•	k?
Halle	Halle	k1gInSc1
•	•	k?
Londýn	Londýn	k1gInSc1
•	•	k?
Vídeň	Vídeň	k1gFnSc1
•	•	k?
Záhřeb	Záhřeb	k1gInSc1
•	•	k?
Bogotá	Bogotá	k1gFnSc2
•	•	k?
Valencie	Valencie	k1gFnSc2
•	•	k?
Kuala	Kuala	k1gMnSc1
Lumpur	Lumpur	k1gMnSc1
•	•	k?
Bukurešť	Bukurešť	k1gFnSc1
•	•	k?
Nottingham	Nottingham	k1gInSc1
•	•	k?
Nice	Nice	k1gFnSc1
•	•	k?
Memphis	Memphis	k1gFnPc2
•	•	k?
Quito	Quito	k1gNnSc4
•	•	k?
Istanbul	Istanbul	k1gInSc1
•	•	k?
Šen-čen	Šen-čen	k2eAgInSc1d1
Seznam	seznam	k1gInSc1
vítězů	vítěz	k1gMnPc2
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
|	|	kIx~
Tenis	tenis	k1gInSc1
</s>
