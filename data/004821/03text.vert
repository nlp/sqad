<s>
Bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
triku	trik	k1gInSc6	trik
je	být	k5eAaImIp3nS	být
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
vyrábějící	vyrábějící	k2eAgInPc1d1	vyrábějící
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
samostatných	samostatný	k2eAgInPc2d1	samostatný
ateliérů	ateliér	k1gInPc2	ateliér
<g/>
,	,	kIx,	,
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
tvůrců	tvůrce	k1gMnPc2	tvůrce
animace	animace	k1gFnSc2	animace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
za	za	k7c2	za
totality	totalita	k1gFnSc2	totalita
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
dotlačeni	dotlačen	k2eAgMnPc1d1	dotlačen
pod	pod	k7c4	pod
centrální	centrální	k2eAgInSc4d1	centrální
dozor	dozor	k1gInSc4	dozor
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
Krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Slavné	slavný	k2eAgNnSc4d1	slavné
logo	logo	k1gNnSc4	logo
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
uklánějící	uklánějící	k2eAgMnPc1d1	uklánějící
se	se	k3xPyFc4	se
chlapečky	chlapeček	k1gMnPc7	chlapeček
v	v	k7c6	v
pruhovaných	pruhovaný	k2eAgInPc6d1	pruhovaný
tričkách	tričko	k1gNnPc6	tričko
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pozdější	pozdní	k2eAgMnSc1d2	pozdější
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgMnSc2d1	slavný
Krtečka	krteček	k1gMnSc2	krteček
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
studiu	studio	k1gNnSc6	studio
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přes	přes	k7c4	přes
1600	[number]	k4	1600
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
získaly	získat	k5eAaPmAgInP	získat
stovky	stovka	k1gFnPc4	stovka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
ocenění	ocenění	k1gNnSc4	ocenění
včetně	včetně	k7c2	včetně
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
Munro	Munro	k1gNnSc1	Munro
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc1	tento
oddělení	oddělení	k1gNnSc1	oddělení
privatizovaného	privatizovaný	k2eAgInSc2d1	privatizovaný
Krátkého	Krátkého	k2eAgInSc2d1	Krátkého
filmu	film	k1gInSc2	film
a.s.	a.s.	k?	a.s.
35	[number]	k4	35
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Specializuje	specializovat	k5eAaBmIp3nS	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
kreslené	kreslený	k2eAgInPc4d1	kreslený
filmy	film	k1gInPc4	film
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
oddělením	oddělení	k1gNnSc7	oddělení
Krátkého	Krátkého	k2eAgInSc2d1	Krátkého
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
Studiem	studio	k1gNnSc7	studio
Jiřího	Jiří	k1gMnSc2	Jiří
Trnky	Trnka	k1gMnSc2	Trnka
–	–	k?	–
původně	původně	k6eAd1	původně
samostatným	samostatný	k2eAgNnSc7d1	samostatné
studiem	studio	k1gNnSc7	studio
Jiřího	Jiří	k1gMnSc2	Jiří
Trnky	Trnka	k1gMnSc2	Trnka
<g/>
,	,	kIx,	,
postiženým	postižený	k2eAgInSc7d1	postižený
stejným	stejný	k2eAgInSc7d1	stejný
osudem	osud	k1gInSc7	osud
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnSc1d1	ostatní
–	–	k?	–
které	který	k3yRgNnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
filmy	film	k1gInPc4	film
loutkové	loutkový	k2eAgInPc4d1	loutkový
<g/>
.	.	kIx.	.
</s>
<s>
Krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
Praha	Praha	k1gFnSc1	Praha
a.s.	a.s.	k?	a.s.
</s>
