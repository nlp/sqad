<s>
Ethereum	Ethereum	k1gInSc1
je	být	k5eAaImIp3nS
kryptoměna	kryptoměn	k2eAgFnSc1d1
(	(	kIx(
<g/>
ETH	ETH	kA
<g/>
)	)	kIx)
a	a	k8xC
open-source	open-sourka	k1gFnSc6
platforma	platforma	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
decentralizované	decentralizovaný	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
blockchainu	blockchain	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
neoprávněným	oprávněný	k2eNgInSc7d1
zásahem	zásah	k1gInSc7
z	z	k7c2
vnější	vnější	k2eAgFnSc2d1
i	i	k8xC
z	z	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>