<s>
Autorem	autor	k1gMnSc7
platformy	platforma	k1gFnSc2
a	a	k8xC
kryptoměny	kryptoměn	k2eAgInPc1d1
Ethereum	Ethereum	k1gInSc1
je	být	k5eAaImIp3nS
Vitalik	Vitalik	k1gMnSc1
Buterin	Buterin	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
koncept	koncept	k1gInSc1
poprvé	poprvé	k6eAd1
představil	představit	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Gavinem	Gavin	k1gMnSc7
Woodem	Wood	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
koncept	koncept	k1gInSc4
formalizoval	formalizovat	k5eAaBmAgMnS
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
v	v	k7c6
takzvaném	takzvaný	k2eAgMnSc6d1
Yellow	Yellow	k1gMnSc6
Paperu	Paper	k1gMnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>