<s>
Třída	třída	k1gFnSc1
Ukraina	Ukraina	k1gFnSc1
</s>
<s>
Třída	třída	k1gFnSc1
Ukraina	Ukrain	k2eAgFnSc1d1
UkrainaObecné	UkrainaObecný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
carské	carský	k2eAgNnSc1d1
námořnictvoSovětské	námořnictvoSovětský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
torpédoborec	torpédoborec	k1gInSc1
Lodě	loď	k1gFnSc2
</s>
<s>
8	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
1	#num#	k4
potopen	potopen	k2eAgInSc4d1
<g/>
7	#num#	k4
vyřazeno	vyřazen	k2eAgNnSc4d1
Předchůdce	předchůdce	k1gMnSc5
</s>
<s>
třída	třída	k1gFnSc1
Storoževoj	Storoževoj	k1gInSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
třída	třída	k1gFnSc1
Emir	Emira	k1gFnPc2
Bucharskij	Bucharskij	k1gFnPc2
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Výtlak	výtlak	k1gInSc4
</s>
<s>
580	#num#	k4
t	t	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
74	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
7,16	7,16	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
2,28	2,28	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
2	#num#	k4
parní	parní	k2eAgInPc1d1
stroje	stroj	k1gInPc1
<g/>
,	,	kIx,
4	#num#	k4
kotle	kotel	k1gInSc2
Rychlost	rychlost	k1gFnSc1
</s>
<s>
26	#num#	k4
uzlů	uzel	k1gInPc2
Dosah	dosah	k1gInSc1
</s>
<s>
1100	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
při	při	k7c6
12	#num#	k4
uzlech	uzel	k1gInPc6
Posádka	posádka	k1gFnSc1
</s>
<s>
90	#num#	k4
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
75	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
×	×	k?
57	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
7,6	7,6	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
×	×	k?
2	#num#	k4
a	a	k8xC
1	#num#	k4
×	×	k?
1	#num#	k4
381	#num#	k4
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
(	(	kIx(
<g/>
první	první	k4xOgMnPc1
4	#num#	k4
lodě	loď	k1gFnSc2
v	v	k7c6
sérii	série	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
×	×	k?
1	#num#	k4
457	#num#	k4
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
(	(	kIx(
<g/>
zbytek	zbytek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Ukraina	Ukraina	k1gFnSc1
byla	být	k5eAaImAgFnS
třída	třída	k1gFnSc1
torpédoborců	torpédoborec	k1gInPc2
ruského	ruský	k2eAgNnSc2d1
carského	carský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
osm	osm	k4xCc1
jednotek	jednotka	k1gFnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
byly	být	k5eAaImAgInP
zařazeny	zařadit	k5eAaPmNgInP
do	do	k7c2
Baltského	baltský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
první	první	k4xOgInPc1
postavené	postavený	k2eAgInPc1d1
velké	velký	k2eAgInPc1d1
ruské	ruský	k2eAgInPc1d1
torpédoborce	torpédoborec	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jeden	jeden	k4xCgInSc1
byl	být	k5eAaImAgInS
potopen	potopit	k5eAaPmNgInS
za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgFnPc1d1
sloužily	sloužit	k5eAaImAgFnP
v	v	k7c6
řadách	řada	k1gFnPc6
sovětského	sovětský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tři	tři	k4xCgFnPc4
byly	být	k5eAaImAgFnP
ve	v	k7c6
službě	služba	k1gFnSc6
ještě	ještě	k9
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
osm	osm	k4xCc1
jednotek	jednotka	k1gFnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhla	navrhnout	k5eAaPmAgFnS
je	on	k3xPp3gNnSc4
německá	německý	k2eAgFnSc1d1
loděnice	loděnice	k1gFnSc1
AG	AG	kA
Vulcan	Vulcan	k1gMnSc1
Stettin	Stettin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbu	stavba	k1gFnSc4
provedly	provést	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1904	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
loděnice	loděnice	k1gFnSc1
Lange	Lang	k1gFnSc2
&	&	k?
Sohn	Sohn	k1gMnSc1
v	v	k7c6
Rize	Riga	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
se	s	k7c7
stabilitou	stabilita	k1gFnSc7
na	na	k7c4
ně	on	k3xPp3gNnSc4
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
dodatečně	dodatečně	k6eAd1
přidáno	přidat	k5eAaPmNgNnS
35	#num#	k4
tun	tuna	k1gFnPc2
balastu	balast	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc1
třídy	třída	k1gFnSc2
Ukraina	Ukraina	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JménoZaložený	JménoZaložený	k2eAgInSc1d1
kýluSpuštěnaVstup	kýluSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gInSc1
</s>
<s>
Ukraina	Ukraina	k1gFnSc1
<g/>
190419041905	#num#	k4
<g/>
Roku	rok	k1gInSc2
1920	#num#	k4
převeden	převést	k5eAaPmNgInS
na	na	k7c4
Kaspické	kaspický	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přejmenován	přejmenován	k2eAgInSc4d1
na	na	k7c4
Karl	Karl	k1gInSc4
Marks	Marksa	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c4
Bakinskij	Bakinskij	k1gFnSc4
Rabočij	Rabočij	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazen	vyřazen	k2eAgInSc4d1
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vojskovoj	Vojskovoj	k1gInSc1
<g/>
190419041905	#num#	k4
<g/>
Roku	rok	k1gInSc2
1920	#num#	k4
převeden	převést	k5eAaPmNgInS
na	na	k7c4
Kaspické	kaspický	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přejmenován	přejmenován	k2eAgInSc4d1
na	na	k7c4
Fridrich	Fridrich	k1gMnSc1
Engels	Engels	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c4
Markin	Markin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazen	vyřazen	k2eAgInSc4d1
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turkmeněc	Turkmeněc	k1gFnSc1
Stavropolskij	Stavropolskij	k1gFnSc2
<g/>
190419051905	#num#	k4
<g/>
Roku	rok	k1gInSc2
1920	#num#	k4
převeden	převést	k5eAaPmNgInS
na	na	k7c4
Kaspické	kaspický	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přejmenován	přejmenovat	k5eAaPmNgMnS
na	na	k7c4
Mirza	Mirz	k1gMnSc4
Kučuk	Kučuk	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
na	na	k7c4
Altfatěr	Altfatěr	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
1945	#num#	k4
na	na	k7c6
Sovetskij	Sovetskij	k1gFnSc6
Dagestan	Dagestan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazen	vyřazen	k2eAgInSc4d1
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kazaněc	Kazaněc	k6eAd1
<g/>
190419051905	#num#	k4
<g/>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1916	#num#	k4
se	se	k3xPyFc4
u	u	k7c2
ostrova	ostrov	k1gInSc2
Vormsi	Vormse	k1gFnSc4
potopil	potopit	k5eAaPmAgInS
na	na	k7c6
mině	mina	k1gFnSc6
položené	položená	k1gFnSc2
německou	německý	k2eAgFnSc7d1
ponorkou	ponorka	k1gFnSc7
SM	SM	kA
UC-	UC-	k1gFnSc7
<g/>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stěreguščij	Stěreguščít	k5eAaPmRp2nS
<g/>
190419051906	#num#	k4
<g/>
Vyřazen	vyřazen	k2eAgInSc4d1
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strašnyj	Strašnyj	k1gInSc4
<g/>
190519051906	#num#	k4
<g/>
Vyřazen	vyřazen	k2eAgInSc4d1
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Donskoj	Donskoj	k1gInSc1
Kozak	Kozak	k1gInSc1
<g/>
190519061906	#num#	k4
<g/>
Vyřazen	vyřazen	k2eAgInSc4d1
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zabajkalec	Zabajkalec	k1gMnSc1
<g/>
190419061906	#num#	k4
<g/>
Vyřazen	vyřazen	k2eAgInSc4d1
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Ukraina	Ukraina	k1gFnSc1
během	během	k7c2
stavby	stavba	k1gFnSc2
</s>
<s>
Výzbroj	výzbroj	k1gFnSc4
tvořily	tvořit	k5eAaImAgInP
dva	dva	k4xCgInPc1
75	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgMnPc4
57	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
až	až	k9
čtyři	čtyři	k4xCgInPc1
7,62	7,62	k4
<g/>
mm	mm	kA
kulomety	kulomet	k1gInPc4
a	a	k8xC
tři	tři	k4xCgFnPc4
457	#num#	k4
<g/>
mm	mm	kA
torpédomety	torpédomet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc4d1
systém	systém	k1gInSc4
tvořily	tvořit	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
kotle	kotel	k1gInPc1
Normand	Normand	k?
a	a	k8xC
dva	dva	k4xCgInPc4
parní	parní	k2eAgInPc4d1
stroje	stroj	k1gInPc4
o	o	k7c6
výkonu	výkon	k1gInSc6
7000	#num#	k4
hp	hp	k?
<g/>
,	,	kIx,
pohánějící	pohánějící	k2eAgInPc4d1
dva	dva	k4xCgInPc4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
26	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dosah	dosah	k1gInSc1
byl	být	k5eAaImAgInS
1100	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
12	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modifikace	modifikace	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1910	#num#	k4
byly	být	k5eAaImAgFnP
původní	původní	k2eAgInSc4d1
75	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc4
nahrazeny	nahrazen	k2eAgInPc4d1
dvěma	dva	k4xCgInPc7
102	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
457	#num#	k4
<g/>
mm	mm	kA
torpédomety	torpédomet	k1gInPc1
zůstaly	zůstat	k5eAaPmAgInP
jen	jen	k6eAd1
dva	dva	k4xCgInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
výzbroj	výzbroj	k1gFnSc1
upravena	upravit	k5eAaPmNgFnS
na	na	k7c4
tři	tři	k4xCgInPc4
102	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc4
umístěné	umístěný	k2eAgInPc4d1
na	na	k7c6
zádi	záď	k1gFnSc6
a	a	k8xC
jeden	jeden	k4xCgMnSc1
40	#num#	k4
<g/>
mm	mm	kA
protiletadlový	protiletadlový	k2eAgInSc4d1
kanón	kanón	k1gInSc4
na	na	k7c6
přídi	příď	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
WATTS	WATTS	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
J.	J.	kA
The	The	k1gMnSc1
Imperial	Imperial	k1gMnSc1
Russian	Russian	k1gMnSc1
Navy	Navy	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Arms	Arms	k1gInSc1
and	and	k?
Armour	Armoura	k1gFnPc2
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85368	#num#	k4
<g/>
-	-	kIx~
<g/>
912	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
UKRAYNA	UKRAYNA	kA
torpedo	torpedo	k1gNnSc1
cruisers	cruisers	k1gInSc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
-	-	kIx~
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navypedia	Navypedium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
232	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
173	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Třída	třída	k1gFnSc1
Ukraina	Ukrain	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
