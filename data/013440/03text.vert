<p>
<s>
Sofisté	sofista	k1gMnPc1	sofista
je	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
starořecké	starořecký	k2eAgMnPc4d1	starořecký
učitele	učitel	k1gMnPc4	učitel
a	a	k8xC	a
filosofy	filosof	k1gMnPc4	filosof
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
sofos	sofos	k1gInSc4	sofos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
sofistai	sofistai	k6eAd1	sofistai
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
učitelé	učitel	k1gMnPc1	učitel
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
–	–	k?	–
sofisté	sofista	k1gMnPc1	sofista
obcházeli	obcházet	k5eAaImAgMnP	obcházet
města	město	k1gNnSc2	město
a	a	k8xC	a
profesionálně	profesionálně	k6eAd1	profesionálně
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
nejrůznějším	různý	k2eAgFnPc3d3	nejrůznější
naukám	nauka	k1gFnPc3	nauka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
počtářství	počtářství	k1gNnSc3	počtářství
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc3	astronomie
<g/>
,	,	kIx,	,
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
zejména	zejména	k9	zejména
rétorice	rétorika	k1gFnSc6	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Zabývali	zabývat	k5eAaImAgMnP	zabývat
se	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
řečí	řeč	k1gFnPc2	řeč
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
případně	případně	k6eAd1	případně
také	také	k9	také
sami	sám	k3xTgMnPc1	sám
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
řečnictvím	řečnictví	k1gNnSc7	řečnictví
ve	v	k7c6	v
shromážděních	shromáždění	k1gNnPc6	shromáždění
a	a	k8xC	a
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
(	(	kIx(	(
<g/>
Platón	platón	k1gInSc1	platón
<g/>
,	,	kIx,	,
Faidros	Faidrosa	k1gFnPc2	Faidrosa
261	[number]	k4	261
<g/>
b	b	k?	b
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gFnPc4	náš
vědomosti	vědomost	k1gFnPc4	vědomost
o	o	k7c6	o
sofistech	sofista	k1gMnPc6	sofista
čerpáme	čerpat	k5eAaImIp1nP	čerpat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Platónových	Platónův	k2eAgInPc2d1	Platónův
dialogů	dialog	k1gInPc2	dialog
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Sókratés	Sókratés	k1gInSc1	Sókratés
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
jejich	jejich	k3xOp3gMnPc4	jejich
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
sofista	sofista	k1gMnSc1	sofista
hanlivé	hanlivý	k2eAgNnSc4d1	hanlivé
označení	označení	k1gNnSc4	označení
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
sofistiku	sofistika	k1gFnSc4	sofistika
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
záměrně	záměrně	k6eAd1	záměrně
používá	používat	k5eAaImIp3nS	používat
klamavé	klamavý	k2eAgNnSc1d1	klamavé
a	a	k8xC	a
logicky	logicky	k6eAd1	logicky
nekorektní	korektní	k2eNgInPc4d1	nekorektní
úsudky	úsudek	k1gInPc4	úsudek
a	a	k8xC	a
argumenty	argument	k1gInPc4	argument
k	k	k7c3	k
vyvozovaní	vyvozovaný	k2eAgMnPc1d1	vyvozovaný
chybných	chybný	k2eAgInPc2d1	chybný
závěrů	závěr	k1gInPc2	závěr
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
něco	něco	k3yInSc1	něco
nesprávného	správný	k2eNgNnSc2d1	nesprávné
<g/>
,	,	kIx,	,
nepravdivého	pravdivý	k2eNgNnSc2d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Sofisté	sofista	k1gMnPc1	sofista
netvořili	tvořit	k5eNaImAgMnP	tvořit
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
konkurenty	konkurent	k1gMnPc7	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obcích	obec	k1gFnPc6	obec
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
způsob	způsob	k1gInSc1	způsob
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
vyučování	vyučování	k1gNnSc2	vyučování
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
společenské	společenský	k2eAgFnSc3d1	společenská
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
filosofů	filosof	k1gMnPc2	filosof
se	se	k3xPyFc4	se
odlišovali	odlišovat	k5eAaImAgMnP	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
moudrostí	moudrost	k1gFnSc7	moudrost
a	a	k8xC	a
výřečností	výřečnost	k1gFnSc7	výřečnost
zabývali	zabývat	k5eAaImAgMnP	zabývat
pro	pro	k7c4	pro
výdělek	výdělek	k1gInSc4	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Platónův	Platónův	k2eAgInSc1d1	Platónův
Gorgiás	Gorgiás	k1gInSc1	Gorgiás
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
schopnost	schopnost	k1gFnSc4	schopnost
přemlouvat	přemlouvat	k5eAaImF	přemlouvat
na	na	k7c6	na
soudě	soud	k1gInSc6	soud
<g/>
,	,	kIx,	,
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
i	i	k8xC	i
ve	v	k7c6	v
sněmu	sněm	k1gInSc6	sněm
<g/>
"	"	kIx"	"
a	a	k8xC	a
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
náznaků	náznak	k1gInPc2	náznak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
sofisté	sofista	k1gMnPc1	sofista
nabízeli	nabízet	k5eAaImAgMnP	nabízet
řečnické	řečnický	k2eAgFnPc4d1	řečnická
služby	služba	k1gFnPc4	služba
u	u	k7c2	u
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
slibovali	slibovat	k5eAaImAgMnP	slibovat
také	také	k9	také
svým	svůj	k3xOyFgMnPc3	svůj
žákům	žák	k1gMnPc3	žák
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
demokratických	demokratický	k2eAgFnPc6d1	demokratická
obcích	obec	k1gFnPc6	obec
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
mnozí	mnohý	k2eAgMnPc1d1	mnohý
uvítali	uvítat	k5eAaPmAgMnP	uvítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Někteří	některý	k3yIgMnPc1	některý
významní	významný	k2eAgMnPc1d1	významný
sofisté	sofista	k1gMnPc1	sofista
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
výroky	výrok	k1gInPc1	výrok
===	===	k?	===
</s>
</p>
<p>
<s>
Sofisty	sofista	k1gMnPc4	sofista
lze	lze	k6eAd1	lze
chronologicky	chronologicky	k6eAd1	chronologicky
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc4d2	mladší
a	a	k8xC	a
tak	tak	k6eAd1	tak
zvanou	zvaný	k2eAgFnSc4d1	zvaná
druhou	druhý	k4xOgFnSc4	druhý
sofistiku	sofistika	k1gFnSc4	sofistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prótagorás	Prótagorás	k6eAd1	Prótagorás
z	z	k7c2	z
Abdér	Abdéra	k1gFnPc2	Abdéra
(	(	kIx(	(
<g/>
asi	asi	k9	asi
481-411	[number]	k4	481-411
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Démokritův	Démokritův	k2eAgMnSc1d1	Démokritův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
mu	on	k3xPp3gMnSc3	on
vložených	vložený	k2eAgInPc2d1	vložený
Platónovým	Platónův	k2eAgInSc7d1	Platónův
dialogem	dialog	k1gInSc7	dialog
Protágoras	Protágorasa	k1gFnPc2	Protágorasa
(	(	kIx(	(
<g/>
319	[number]	k4	319
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
F.	F.	kA	F.
<g/>
N.	N.	kA	N.
<g/>
)	)	kIx)	)
učil	učít	k5eAaPmAgMnS	učít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jeho	jeho	k3xOp3gMnSc7	jeho
žákem	žák	k1gMnSc7	žák
"	"	kIx"	"
<g/>
rozvážnost	rozvážnost	k1gFnSc1	rozvážnost
i	i	k9	i
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yRnSc4	co
nejlépe	dobře	k6eAd3	dobře
spravoval	spravovat	k5eAaImAgMnS	spravovat
své	svůj	k3xOyFgNnSc4	svůj
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
i	i	k9	i
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
obecních	obecní	k2eAgFnPc6d1	obecní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
co	co	k3yRnSc4	co
nejschopnější	schopný	k2eAgMnPc4d3	nejschopnější
činem	čin	k1gInSc7	čin
i	i	k9	i
slovem	slovem	k6eAd1	slovem
říditi	řídit	k5eAaImF	řídit
věci	věc	k1gFnPc4	věc
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
<g/>
;	;	kIx,	;
jsoucích	jsoucí	k2eAgFnPc2d1	jsoucí
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejsoucích	nejsoucí	k2eAgInPc2d1	nejsoucí
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
1	[number]	k4	1
ze	z	k7c2	z
Sexta	sexta	k1gFnSc1	sexta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
O	o	k7c6	o
bozích	bůh	k1gMnPc6	bůh
nelze	lze	k6eNd1	lze
věděti	vědět	k5eAaImF	vědět
ani	ani	k8xC	ani
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jakou	jaký	k3yIgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
4	[number]	k4	4
z	z	k7c2	z
Diogena	Diogenes	k1gMnSc2	Diogenes
<g/>
,	,	kIx,	,
Eusebia	Eusebius	k1gMnSc2	Eusebius
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
Gorgiás	Gorgiás	k1gInSc1	Gorgiás
z	z	k7c2	z
Leontín	Leontína	k1gFnPc2	Leontína
(	(	kIx(	(
<g/>
asi	asi	k9	asi
483-375	[number]	k4	483-375
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Koraxe	Koraxe	k1gFnSc2	Koraxe
ze	z	k7c2	z
Syrakús	Syrakúsa	k1gFnPc2	Syrakúsa
<g/>
,	,	kIx,	,
rétor	rétor	k1gMnSc1	rétor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nic	nic	k3yNnSc1	nic
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
něco	něco	k6eAd1	něco
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nepoznatelné	poznatelný	k2eNgNnSc1d1	nepoznatelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
něco	něco	k6eAd1	něco
poznatelné	poznatelný	k2eAgNnSc1d1	poznatelné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nesdělitelné	sdělitelný	k2eNgNnSc1d1	nesdělitelné
a	a	k8xC	a
nevysvětlitelné	vysvětlitelný	k2eNgNnSc1d1	nevysvětlitelné
<g/>
.	.	kIx.	.
<g/>
Thrasymachos	Thrasymachos	k1gInSc1	Thrasymachos
z	z	k7c2	z
Chalkedónu	Chalkedón	k1gInSc2	Chalkedón
(	(	kIx(	(
<g/>
asi	asi	k9	asi
459	[number]	k4	459
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
Platónově	Platónův	k2eAgFnSc6d1	Platónova
Ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
tvrdím	tvrdit	k5eAaImIp1nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
pro	pro	k7c4	pro
silnějšího	silný	k2eAgMnSc4d2	silnější
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
Ústava	ústava	k1gFnSc1	ústava
338	[number]	k4	338
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Hippias	Hippias	k1gInSc1	Hippias
z	z	k7c2	z
Elidy	Elida	k1gFnSc2	Elida
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Platónova	Platónův	k2eAgInSc2d1	Platónův
dialogu	dialog	k1gInSc2	dialog
(	(	kIx(	(
<g/>
Protágoras	Protágoras	k1gInSc1	Protágoras
<g/>
)	)	kIx)	)
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
počtářství	počtářství	k1gNnSc4	počtářství
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
všem	všecek	k3xTgMnPc3	všecek
či	či	k8xC	či
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neboť	neboť	k8xC	neboť
ti	ten	k3xDgMnPc1	ten
jiní	jiný	k1gMnPc1	jiný
týrají	týrat	k5eAaImIp3nP	týrat
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
;	;	kIx,	;
kteří	který	k3yQgMnPc1	který
utekli	utéct	k5eAaPmAgMnP	utéct
od	od	k7c2	od
odborných	odborný	k2eAgFnPc2d1	odborná
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
vůli	vůle	k1gFnSc3	vůle
vodí	vodit	k5eAaImIp3nS	vodit
zase	zase	k9	zase
nazpět	nazpět	k6eAd1	nazpět
a	a	k8xC	a
vrhají	vrhat	k5eAaImIp3nP	vrhat
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
odborných	odborný	k2eAgFnPc2d1	odborná
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
učíce	učit	k5eAaImSgMnP	učit
je	být	k5eAaImIp3nS	být
počtářství	počtářství	k1gNnSc2	počtářství
a	a	k8xC	a
astronomii	astronomie	k1gFnSc3	astronomie
a	a	k8xC	a
geometrii	geometrie	k1gFnSc3	geometrie
a	a	k8xC	a
hudbě	hudba	k1gFnSc3	hudba
–	–	k?	–
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
pohlédl	pohlédnout	k5eAaPmAgMnS	pohlédnout
na	na	k7c6	na
Hippiu	Hippium	k1gNnSc6	Hippium
–	–	k?	–
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
Protágoras	Protágoras	k1gMnSc1	Protágoras
318	[number]	k4	318
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
sofisty	sofista	k1gMnPc4	sofista
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Prodikos	Prodikos	k1gInSc1	Prodikos
z	z	k7c2	z
Keu	Keu	k1gFnSc2	Keu
<g/>
,	,	kIx,	,
Lykofrón	Lykofrón	k1gInSc1	Lykofrón
<g/>
,	,	kIx,	,
Kalliklés	Kalliklés	k1gInSc1	Kalliklés
<g/>
,	,	kIx,	,
Antifón	Antifón	k1gInSc1	Antifón
či	či	k8xC	či
Kratylos	Kratylos	k1gInSc1	Kratylos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
sofistika	sofistika	k1gFnSc1	sofistika
je	být	k5eAaImIp3nS	být
oživení	oživení	k1gNnSc4	oživení
sofistické	sofistický	k2eAgFnSc2d1	sofistická
argumentace	argumentace	k1gFnSc2	argumentace
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dio	Dio	k?	Dio
Chrysostomos	Chrysostomos	k1gMnSc1	Chrysostomos
<g/>
,	,	kIx,	,
Herodes	Herodes	k1gMnSc1	Herodes
Atticus	Atticus	k1gMnSc1	Atticus
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sofisté	sofista	k1gMnPc1	sofista
<g/>
,	,	kIx,	,
Sókratés	Sókratés	k1gInSc1	Sókratés
a	a	k8xC	a
Platón	platón	k1gInSc1	platón
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
našich	náš	k3xOp1gFnPc2	náš
vědomostí	vědomost	k1gFnPc2	vědomost
o	o	k7c6	o
sofistech	sofista	k1gMnPc6	sofista
jsou	být	k5eAaImIp3nP	být
Sókratovy	Sókratův	k2eAgFnPc1d1	Sókratova
polemiky	polemika	k1gFnPc1	polemika
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
v	v	k7c6	v
Platónových	Platónův	k2eAgInPc6d1	Platónův
dialozích	dialog	k1gInPc6	dialog
Gorgias	Gorgias	k1gInSc1	Gorgias
<g/>
,	,	kIx,	,
Protagoras	Protagoras	k1gInSc1	Protagoras
<g/>
,	,	kIx,	,
Hippias	Hippias	k1gInSc1	Hippias
Větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
Hippias	Hippias	k1gInSc1	Hippias
Menší	malý	k2eAgInSc1d2	menší
a	a	k8xC	a
Sofistés	Sofistés	k1gInSc1	Sofistés
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Platónově	Platónův	k2eAgInSc6d1	Platónův
dialogu	dialog	k1gInSc6	dialog
Protágoras	Protágorasa	k1gFnPc2	Protágorasa
(	(	kIx(	(
<g/>
313	[number]	k4	313
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
Sókratés	Sókratés	k1gInSc1	Sókratés
Hippokrata	Hippokrat	k1gMnSc2	Hippokrat
(	(	kIx(	(
<g/>
osoba	osoba	k1gFnSc1	osoba
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
Hippokrata	Hippokrat	k1gMnSc2	Hippokrat
z	z	k7c2	z
Kósu	Kós	k1gInSc2	Kós
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
užívání	užívání	k1gNnSc4	užívání
řečnictví	řečnictví	k1gNnSc2	řečnictví
–	–	k?	–
úlisného	úlisný	k2eAgNnSc2d1	úlisné
lahodění	lahodění	k1gNnSc2	lahodění
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ošklivý	ošklivý	k2eAgInSc1d1	ošklivý
druh	druh	k1gInSc1	druh
řečnictví	řečnictví	k1gNnSc2	řečnictví
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Gorgias	Gorgiasa	k1gFnPc2	Gorgiasa
-	-	kIx~	-
<g/>
,	,	kIx,	,
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
svých	svůj	k3xOyFgMnPc2	svůj
žáků	žák	k1gMnPc2	žák
sofisty	sofista	k1gMnSc2	sofista
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sókr	Sókr	k1gMnSc1	Sókr
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
Hippokrate	Hippokrat	k1gMnSc5	Hippokrat
<g/>
,	,	kIx,	,
sofista	sofista	k1gMnSc1	sofista
jakýsi	jakýsi	k3yIgInSc4	jakýsi
obchodník	obchodník	k1gMnSc1	obchodník
nebo	nebo	k8xC	nebo
kupec	kupec	k1gMnSc1	kupec
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
duše	duše	k1gFnPc1	duše
<g/>
?	?	kIx.	?
</s>
<s>
Mně	já	k3xPp1nSc3	já
aspoň	aspoň	k9	aspoň
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
takový	takový	k3xDgMnSc1	takový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
čím	čí	k3xOyRgNnSc7	čí
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
duše	duše	k1gFnPc1	duše
<g/>
,	,	kIx,	,
Sókrate	Sókrat	k1gMnSc5	Sókrat
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Sókr	Sókr	k1gMnSc1	Sókr
<g/>
.	.	kIx.	.
</s>
<s>
Zajisté	zajisté	k9	zajisté
naukami	nauka	k1gFnPc7	nauka
<g/>
,	,	kIx,	,
děl	dít	k5eAaImAgMnS	dít
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jen	jen	k9	jen
ať	ať	k9	ať
nás	my	k3xPp1nPc4	my
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
sofista	sofista	k1gMnSc1	sofista
neoklame	oklamat	k5eNaPmIp3nS	oklamat
vychvalováním	vychvalování	k1gNnSc7	vychvalování
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
prodává	prodávat	k5eAaImIp3nS	prodávat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
to	ten	k3xDgNnSc4	ten
dělají	dělat	k5eAaImIp3nP	dělat
prodavači	prodavač	k1gMnPc1	prodavač
tělesných	tělesný	k2eAgFnPc2d1	tělesná
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
kupec	kupec	k1gMnSc1	kupec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Sofistés	Sofistésa	k1gFnPc2	Sofistésa
Platón	Platón	k1gMnSc1	Platón
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
na	na	k7c6	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
sofista	sofista	k1gMnSc1	sofista
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
stejný	stejný	k2eAgInSc4d1	stejný
název	název	k1gInSc4	název
sofista	sofista	k1gMnSc1	sofista
označoval	označovat	k5eAaImAgMnS	označovat
různé	různý	k2eAgFnPc4d1	různá
dovednosti	dovednost	k1gFnPc4	dovednost
(	(	kIx(	(
<g/>
Sofistés	Sofistés	k1gInSc1	Sofistés
232	[number]	k4	232
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
závěrů	závěr	k1gInPc2	závěr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
:	:	kIx,	:
Říci	říct	k5eAaPmF	říct
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
ho	on	k3xPp3gInSc4	on
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c2	za
neznalého	znalý	k2eNgInSc2d1	neznalý
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
napodobitelem	napodobitel	k1gMnSc7	napodobitel
moudrého	moudrý	k2eAgNnSc2d1	moudré
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
patrně	patrně	k6eAd1	patrně
jakési	jakýsi	k3yIgNnSc1	jakýsi
pojmenování	pojmenování	k1gNnSc1	pojmenování
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
už	už	k9	už
skoro	skoro	k6eAd1	skoro
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
tom	ten	k3xDgMnSc6	ten
muži	muž	k1gMnSc6	muž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doopravdy	doopravdy	k6eAd1	doopravdy
ten	ten	k3xDgMnSc1	ten
docela	docela	k6eAd1	docela
skutečný	skutečný	k2eAgMnSc1d1	skutečný
sofista	sofista	k1gMnSc1	sofista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Host	host	k1gMnSc1	host
z	z	k7c2	z
Eleje	Elea	k1gFnSc2	Elea
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
tedy	tedy	k9	tedy
napodobovací	napodobovací	k2eAgFnSc4d1	napodobovací
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
zaplétati	zaplétat	k5eAaImF	zaplétat
do	do	k7c2	do
rozporů	rozpor	k1gInPc2	rozpor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
záludné	záludný	k2eAgFnSc3d1	záludná
části	část	k1gFnSc3	část
umění	umění	k1gNnSc2	umění
pracujícího	pracující	k1gMnSc2	pracující
se	s	k7c7	s
zdáním	zdání	k1gNnSc7	zdání
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc2	rod
přeludového	přeludový	k2eAgInSc2d1	přeludový
<g/>
,	,	kIx,	,
kejklířství	kejklířství	k1gNnSc3	kejklířství
v	v	k7c6	v
řečech	řeč	k1gFnPc6	řeč
<g/>
,	,	kIx,	,
ne	ne	k9	ne
božská	božská	k1gFnSc1	božská
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
lidská	lidský	k2eAgFnSc1d1	lidská
část	část	k1gFnSc1	část
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgFnSc1d1	oddělená
od	od	k7c2	od
umění	umění	k1gNnSc2	umění
obrazotvorného	obrazotvorný	k2eAgNnSc2d1	obrazotvorné
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečný	skutečný	k2eAgMnSc1d1	skutečný
sofista	sofista	k1gMnSc1	sofista
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
takového	takový	k3xDgInSc2	takový
původu	původ	k1gInSc2	původ
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
,	,	kIx,	,
čistou	čistý	k2eAgFnSc4d1	čistá
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc4	překlad
Františka	František	k1gMnSc4	František
Novotného	Novotný	k1gMnSc4	Novotný
268	[number]	k4	268
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
třeba	třeba	k6eAd1	třeba
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
k	k	k7c3	k
Prodikovi	Prodik	k1gMnSc3	Prodik
z	z	k7c2	z
Kéu	Kéu	k1gFnPc2	Kéu
nebývá	bývat	k5eNaImIp3nS	bývat
Platón	platón	k1gInSc1	platón
příliš	příliš	k6eAd1	příliš
kritický	kritický	k2eAgInSc1d1	kritický
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
Gorgiu	Gorgium	k1gNnSc3	Gorgium
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Gorgias	Gorgiasa	k1gFnPc2	Gorgiasa
líčí	líčit	k5eAaImIp3nS	líčit
jako	jako	k9	jako
mírného	mírný	k2eAgMnSc4d1	mírný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
schopného	schopný	k2eAgMnSc4d1	schopný
naslouchat	naslouchat	k5eAaImF	naslouchat
<g/>
;	;	kIx,	;
snad	snad	k9	snad
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
některým	některý	k3yIgMnPc3	některý
sofistům	sofista	k1gMnPc3	sofista
přisuzoval	přisuzovat	k5eAaImAgMnS	přisuzovat
určitou	určitý	k2eAgFnSc4d1	určitá
míru	míra	k1gFnSc4	míra
vědění	vědění	k1gNnSc2	vědění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Athénská	athénský	k2eAgFnSc1d1	Athénská
žaloba	žaloba	k1gFnSc1	žaloba
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
Sókratovi	Sókrat	k1gMnSc3	Sókrat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
věci	věc	k1gFnPc4	věc
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
i	i	k9	i
nebeské	nebeský	k2eAgFnPc1d1	nebeská
<g/>
,	,	kIx,	,
že	že	k8xS	že
slabší	slabý	k2eAgInPc4d2	slabší
důvody	důvod	k1gInPc4	důvod
činí	činit	k5eAaImIp3nS	činit
silnějšími	silný	k2eAgInPc7d2	silnější
a	a	k8xC	a
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
učí	učit	k5eAaImIp3nP	učit
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
učení	učení	k1gNnSc4	učení
sofistů	sofista	k1gMnPc2	sofista
<g/>
.	.	kIx.	.
</s>
<s>
Aristofanova	Aristofanův	k2eAgFnSc1d1	Aristofanova
satirická	satirický	k2eAgFnSc1d1	satirická
komedie	komedie	k1gFnSc1	komedie
Oblaka	oblaka	k1gNnPc4	oblaka
líčí	líčit	k5eAaImIp3nS	líčit
Sókratovu	Sókratův	k2eAgFnSc4d1	Sókratova
školu	škola	k1gFnSc4	škola
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
učí	učit	k5eAaImIp3nP	učit
přehádat	přehádat	k5eAaPmF	přehádat
podivnými	podivný	k2eAgInPc7d1	podivný
argumenty	argument	k1gInPc7	argument
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
učitel	učitel	k1gMnSc1	učitel
i	i	k8xC	i
žáci	žák	k1gMnPc1	žák
zoufale	zoufale	k6eAd1	zoufale
shánějí	shánět	k5eAaImIp3nP	shánět
něco	něco	k6eAd1	něco
k	k	k7c3	k
snědku	snědek	k1gInSc3	snědek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
W.	W.	kA	W.
K.	K.	kA	K.
C.	C.	kA	C.
Guthrie	Guthrie	k1gFnSc1	Guthrie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
"	"	kIx"	"
<g/>
Historii	historie	k1gFnSc4	historie
řecké	řecký	k2eAgFnSc2d1	řecká
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
Sókrata	Sókrata	k1gFnSc1	Sókrata
mezi	mezi	k7c7	mezi
sofisty	sofista	k1gMnPc7	sofista
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
polemický	polemický	k2eAgInSc1d1	polemický
tón	tón	k1gInSc1	tón
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
Platónovi	Platón	k1gMnSc3	Platón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sofistika	sofistika	k1gFnSc1	sofistika
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
a	a	k8xC	a
možná	možná	k9	možná
ne	ne	k9	ne
nestranného	nestranný	k2eAgNnSc2d1	nestranné
Platónova	Platónův	k2eAgNnSc2d1	Platónovo
vylíčení	vylíčení	k1gNnSc2	vylíčení
mohl	moct	k5eAaImAgMnS	moct
i	i	k8xC	i
nemusel	muset	k5eNaImAgMnS	muset
vzniknout	vzniknout	k5eAaPmF	vzniknout
pozdější	pozdní	k2eAgInSc4d2	pozdější
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
sofista	sofista	k1gMnSc1	sofista
a	a	k8xC	a
sofistika	sofistika	k1gFnSc1	sofistika
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
přesvědčivý	přesvědčivý	k2eAgInSc4d1	přesvědčivý
způsob	způsob	k1gInSc4	způsob
argumentace	argumentace	k1gFnSc2	argumentace
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
však	však	k9	však
často	často	k6eAd1	často
na	na	k7c6	na
dvojznačnostech	dvojznačnost	k1gFnPc6	dvojznačnost
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
tricích	trik	k1gInPc6	trik
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Eristika	Eristika	k1gFnSc1	Eristika
-	-	kIx~	-
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
posluchač	posluchač	k1gMnSc1	posluchač
v	v	k7c6	v
první	první	k4xOgFnSc6	první
chvíli	chvíle	k1gFnSc6	chvíle
neprohlédne	prohlédnout	k5eNaPmIp3nS	prohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
sofistiky	sofistika	k1gFnSc2	sofistika
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
argumentace	argumentace	k1gFnSc1	argumentace
(	(	kIx(	(
<g/>
druhem	druh	k1gInSc7	druh
argumentace	argumentace	k1gFnSc2	argumentace
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
sylogismus	sylogismus	k1gInSc4	sylogismus
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myš	myš	k1gFnSc1	myš
je	být	k5eAaImIp3nS	být
slabika	slabika	k1gFnSc1	slabika
<g/>
.	.	kIx.	.
-	-	kIx~	-
Myš	myš	k1gFnSc1	myš
hryže	hryzat	k5eAaImIp3nS	hryzat
<g/>
.	.	kIx.	.
-	-	kIx~	-
Takže	takže	k9	takže
slabika	slabika	k1gFnSc1	slabika
hryže	hryzat	k5eAaImIp3nS	hryzat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Nesmysl	nesmysl	k1gInSc1	nesmysl
vzniká	vznikat	k5eAaImIp3nS	vznikat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
myš	myš	k1gFnSc1	myš
<g/>
"	"	kIx"	"
poprvé	poprvé	k6eAd1	poprvé
znamená	znamenat	k5eAaImIp3nS	znamenat
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
myš	myš	k1gFnSc1	myš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
hlodavce	hlodavec	k1gMnSc4	hlodavec
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zhodnocení	zhodnocení	k1gNnSc2	zhodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
hodnocení	hodnocení	k1gNnSc1	hodnocení
sofistů	sofista	k1gMnPc2	sofista
tedy	tedy	k8xC	tedy
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
opatrné	opatrný	k2eAgFnPc1d1	opatrná
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
patrně	patrně	k6eAd1	patrně
první	první	k4xOgMnPc1	první
profesionální	profesionální	k2eAgMnPc1d1	profesionální
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
nabízeli	nabízet	k5eAaImAgMnP	nabízet
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnPc1d1	moderní
badatelé	badatel	k1gMnPc1	badatel
jim	on	k3xPp3gMnPc3	on
proto	proto	k8xC	proto
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
zásluhu	zásluha	k1gFnSc4	zásluha
o	o	k7c4	o
jistou	jistý	k2eAgFnSc4d1	jistá
"	"	kIx"	"
<g/>
demokratizaci	demokratizace	k1gFnSc4	demokratizace
<g/>
"	"	kIx"	"
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
privilegiem	privilegium	k1gNnSc7	privilegium
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
výroků	výrok	k1gInPc2	výrok
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
sofisté	sofista	k1gMnPc1	sofista
skutečně	skutečně	k6eAd1	skutečně
byli	být	k5eAaImAgMnP	být
myslitelé	myslitel	k1gMnPc1	myslitel
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovedli	dovést	k5eAaPmAgMnP	dovést
nečekaně	nečekaně	k6eAd1	nečekaně
zpochybňovat	zpochybňovat	k5eAaImF	zpochybňovat
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
samozřejmosti	samozřejmost	k1gFnSc2	samozřejmost
–	–	k?	–
například	například	k6eAd1	například
možnosti	možnost	k1gFnPc4	možnost
lidského	lidský	k2eAgNnSc2d1	lidské
poznání	poznání	k1gNnSc2	poznání
–	–	k?	–
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
zavedené	zavedený	k2eAgInPc4d1	zavedený
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
obyčeje	obyčej	k1gInPc4	obyčej
a	a	k8xC	a
náboženství	náboženství	k1gNnSc4	náboženství
své	svůj	k3xOyFgFnSc2	svůj
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Kallikleovu	Kallikleův	k2eAgFnSc4d1	Kallikleův
obhajobu	obhajoba	k1gFnSc4	obhajoba
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
proti	proti	k7c3	proti
právu	právo	k1gNnSc3	právo
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
jen	jen	k9	jen
nepřirozené	přirozený	k2eNgInPc4d1	nepřirozený
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
otroci	otrok	k1gMnPc1	otrok
a	a	k8xC	a
slabí	slabý	k2eAgMnPc1d1	slabý
podrobují	podrobovat	k5eAaImIp3nP	podrobovat
ty	ten	k3xDgFnPc1	ten
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
a	a	k8xC	a
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sofisté	sofista	k1gMnPc1	sofista
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
relativizovat	relativizovat	k5eAaImF	relativizovat
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
jako	jako	k9	jako
řečníci	řečník	k1gMnPc1	řečník
obhajovat	obhajovat	k5eAaImF	obhajovat
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
i	i	k8xC	i
věci	věc	k1gFnPc4	věc
těžko	těžko	k6eAd1	těžko
obhajitelné	obhajitelný	k2eAgFnPc4d1	obhajitelná
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
tak	tak	k9	tak
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
totiž	totiž	k9	totiž
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
patrně	patrně	k6eAd1	patrně
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
a	a	k8xC	a
vyháněni	vyháněn	k2eAgMnPc1d1	vyháněn
z	z	k7c2	z
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
cítili	cítit	k5eAaImAgMnP	cítit
jejich	jejich	k3xOp3gNnSc7	jejich
učením	učení	k1gNnSc7	učení
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
podobného	podobný	k2eAgNnSc2d1	podobné
ovšem	ovšem	k9	ovšem
platilo	platit	k5eAaImAgNnS	platit
i	i	k9	i
o	o	k7c6	o
Sókratovi	Sókrat	k1gMnSc6	Sókrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
snad	snad	k9	snad
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
sofisty	sofista	k1gMnPc7	sofista
byli	být	k5eAaImAgMnP	být
jak	jak	k6eAd1	jak
vážní	vážní	k2eAgMnPc1d1	vážní
myslitelé	myslitel	k1gMnPc1	myslitel
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
skrupulí	skrupule	k1gFnPc2	skrupule
<g/>
,	,	kIx,	,
ochotní	ochotný	k2eAgMnPc1d1	ochotný
hájit	hájit	k5eAaImF	hájit
kohokoli	kdokoli	k3yInSc4	kdokoli
a	a	k8xC	a
cokoli	cokoli	k3yInSc1	cokoli
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
asi	asi	k9	asi
působili	působit	k5eAaImAgMnP	působit
jako	jako	k9	jako
profesionální	profesionální	k2eAgMnPc1d1	profesionální
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
advokáti	advokát	k1gMnPc1	advokát
a	a	k8xC	a
popularizátoři	popularizátor	k1gMnPc1	popularizátor
<g/>
.	.	kIx.	.
</s>
<s>
Sofisté	sofista	k1gMnPc1	sofista
však	však	k9	však
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
měli	mít	k5eAaImAgMnP	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
rétoriky	rétorika	k1gFnSc2	rétorika
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnPc4	zakladatel
kritiky	kritika	k1gFnSc2	kritika
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
filosofická	filosofický	k2eAgFnSc1d1	filosofická
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
dílem	dílem	k6eAd1	dílem
Platónovým	Platónův	k2eAgMnPc3d1	Platónův
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
jednostranně	jednostranně	k6eAd1	jednostranně
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgInPc4d2	veliký
efekt	efekt	k1gInSc4	efekt
pak	pak	k6eAd1	pak
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
například	například	k6eAd1	například
Nietzsche	Nietzsche	k1gNnSc4	Nietzsche
začal	začít	k5eAaPmAgMnS	začít
jejich	jejich	k3xOp3gNnPc4	jejich
stanoviska	stanovisko	k1gNnPc4	stanovisko
hájit	hájit	k5eAaImF	hájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
===	===	k?	===
</s>
</p>
<p>
<s>
Sofisté	sofista	k1gMnPc1	sofista
relativizovali	relativizovat	k5eAaImAgMnP	relativizovat
vládnoucí	vládnoucí	k2eAgInPc4d1	vládnoucí
názory	názor	k1gInPc4	názor
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc4	hodnota
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
lze	lze	k6eAd1	lze
jim	on	k3xPp3gMnPc3	on
přiznat	přiznat	k5eAaPmF	přiznat
zásluhu	zásluha	k1gFnSc4	zásluha
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
obrat	obrat	k1gInSc4	obrat
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
době	doba	k1gFnSc6	doba
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
přičiněním	přičinění	k1gNnSc7	přičinění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přenesení	přenesení	k1gNnSc1	přenesení
pozornosti	pozornost	k1gFnSc2	pozornost
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
předmětem	předmět	k1gInSc7	předmět
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
myšlení	myšlení	k1gNnSc1	myšlení
samo	sám	k3xTgNnSc1	sám
<g/>
,	,	kIx,	,
kritické	kritický	k2eAgNnSc4d1	kritické
zkoumání	zkoumání	k1gNnSc4	zkoumání
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
hranic	hranice	k1gFnPc2	hranice
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
kritické	kritický	k2eAgNnSc4d1	kritické
zkoumání	zkoumání	k1gNnSc4	zkoumání
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
filosofie	filosofie	k1gFnSc2	filosofie
jako	jako	k8xC	jako
etiky	etika	k1gFnSc2	etika
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
rozvoj	rozvoj	k1gInSc4	rozvoj
stylistiky	stylistika	k1gFnSc2	stylistika
a	a	k8xC	a
rétoriky	rétorika	k1gFnSc2	rétorika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kirk	Kirk	k1gInSc1	Kirk
–	–	k?	–
Raven	Raven	k2eAgInSc1d1	Raven
–	–	k?	–
Schofield	Schofieldo	k1gNnPc2	Schofieldo
<g/>
,	,	kIx,	,
Předsókratovští	Předsókratovský	k2eAgMnPc1d1	Předsókratovský
filosofové	filosof	k1gMnPc1	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikumené	Oikumený	k2eAgNnSc1d1	Oikumený
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Graeser	Graeser	k1gInSc1	Graeser
<g/>
,	,	kIx,	,
Řecká	řecký	k2eAgFnSc1d1	řecká
filosofie	filosofie	k1gFnSc1	filosofie
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikumené	Oikumený	k2eAgNnSc1d1	Oikumený
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Řecký	řecký	k2eAgInSc1d1	řecký
zázrak	zázrak	k1gInSc1	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
1972	[number]	k4	1972
</s>
</p>
<p>
<s>
H.	H.	kA	H.
J.	J.	kA	J.
Störig	Störiga	k1gFnPc2	Störiga
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnPc1d1	Malé
dějiny	dějiny	k1gFnPc1	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zvon	zvon	k1gInSc1	zvon
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Antické	antický	k2eAgNnSc1d1	antické
Řecko	Řecko	k1gNnSc1	Řecko
</s>
</p>
<p>
<s>
Antická	antický	k2eAgFnSc1d1	antická
filosofie	filosofie	k1gFnSc1	filosofie
</s>
</p>
<p>
<s>
Předsókratici	Předsókratik	k1gMnPc1	Předsókratik
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sofisté	sofista	k1gMnPc1	sofista
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Sofisté	sofista	k1gMnPc1	sofista
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
