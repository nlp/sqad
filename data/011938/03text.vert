<p>
<s>
Vidlička	vidlička	k1gFnSc1	vidlička
resp.	resp.	kA	resp.
jídelní	jídelní	k2eAgFnSc1d1	jídelní
vidlička	vidlička	k1gFnSc1	vidlička
tvoří	tvořit	k5eAaImIp3nS	tvořit
vedle	vedle	k7c2	vedle
lžíce	lžíce	k1gFnSc2	lžíce
a	a	k8xC	a
jídelního	jídelní	k2eAgInSc2d1	jídelní
nože	nůž	k1gInSc2	nůž
součást	součást	k1gFnSc1	součást
jídelního	jídelní	k2eAgInSc2d1	jídelní
příboru	příbor	k1gInSc2	příbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Vidlička	vidlička	k1gFnSc1	vidlička
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
a	a	k8xC	a
při	při	k7c6	při
stolování	stolování	k1gNnSc6	stolování
k	k	k7c3	k
napichování	napichování	k1gNnSc3	napichování
a	a	k8xC	a
přemisťování	přemisťování	k1gNnSc3	přemisťování
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
při	při	k7c6	při
jídle	jídlo	k1gNnSc6	jídlo
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
přenosu	přenos	k1gInSc3	přenos
nákazy	nákaza	k1gFnSc2	nákaza
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
na	na	k7c4	na
konzumované	konzumovaný	k2eAgNnSc4d1	konzumované
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
stolování	stolování	k1gNnSc6	stolování
především	především	k9	především
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
<g/>
;	;	kIx,	;
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jídelní	jídelní	k2eAgFnPc1d1	jídelní
hůlky	hůlka	k1gFnPc1	hůlka
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
)	)	kIx)	)
rovnoběžných	rovnoběžný	k2eAgInPc2d1	rovnoběžný
mírně	mírně	k6eAd1	mírně
prohnutých	prohnutý	k2eAgInPc2d1	prohnutý
hrotů	hrot	k1gInPc2	hrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvar	tvar	k1gInSc1	tvar
celé	celý	k2eAgFnSc2d1	celá
vidličky	vidlička	k1gFnSc2	vidlička
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
rukojeti	rukojeť	k1gFnSc2	rukojeť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
rukojeť	rukojeť	k1gFnSc1	rukojeť
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
zdobena	zdoben	k2eAgFnSc1d1	zdobena
nebo	nebo	k8xC	nebo
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
porcelán	porcelán	k1gInSc1	porcelán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
materiál	materiál	k1gInSc1	materiál
<g/>
:	:	kIx,	:
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejhojněji	hojně	k6eAd3	hojně
nerezová	rezový	k2eNgFnSc1d1	nerezová
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zase	zase	k9	zase
často	často	k6eAd1	často
luxusnější	luxusní	k2eAgNnSc1d2	luxusnější
stříbro	stříbro	k1gNnSc1	stříbro
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc1	jeho
slitiny	slitina	k1gFnPc1	slitina
(	(	kIx(	(
<g/>
alpaka	alpaka	k1gFnSc1	alpaka
<g/>
)	)	kIx)	)
či	či	k8xC	či
(	(	kIx(	(
<g/>
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
vrstvách	vrstva	k1gFnPc6	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
používání	používání	k1gNnSc1	používání
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
především	především	k9	především
ve	v	k7c4	v
fast	fast	k1gInSc4	fast
foodech	food	k1gInPc6	food
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
i	i	k9	i
používání	používání	k1gNnSc3	používání
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
plastových	plastový	k2eAgFnPc2d1	plastová
vidliček	vidlička	k1gFnPc2	vidlička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vidličky	vidlička	k1gFnPc1	vidlička
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
jídelního	jídelní	k2eAgInSc2d1	jídelní
příboru	příbor	k1gInSc2	příbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
velikostech	velikost	k1gFnPc6	velikost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jídelní	jídelní	k2eAgFnSc1d1	jídelní
vidlička	vidlička	k1gFnSc1	vidlička
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
jenom	jenom	k9	jenom
"	"	kIx"	"
<g/>
vidlička	vidlička	k1gFnSc1	vidlička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
pevných	pevný	k2eAgFnPc2d1	pevná
částí	část	k1gFnPc2	část
stravy	strava	k1gFnSc2	strava
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
;	;	kIx,	;
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
sousta	sousto	k1gNnPc1	sousto
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
napichují	napichovat	k5eAaImIp3nP	napichovat
nebo	nebo	k8xC	nebo
nahrnují	nahrnovat	k5eAaImIp3nP	nahrnovat
na	na	k7c4	na
vidličku	vidlička	k1gFnSc4	vidlička
</s>
</p>
<p>
<s>
dezertní	dezertní	k2eAgFnSc1d1	dezertní
vidlička	vidlička	k1gFnSc1	vidlička
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
dezertů	dezert	k1gInPc2	dezert
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
krajní	krajní	k2eAgInSc1d1	krajní
hrot	hrot	k1gInSc1	hrot
je	být	k5eAaImIp3nS	být
zesílen	zesílit	k5eAaPmNgInS	zesílit
a	a	k8xC	a
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
odkrajovat	odkrajovat	k5eAaImF	odkrajovat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
sousta	sousto	k1gNnPc4	sousto
</s>
</p>
<p>
<s>
servírovací	servírovací	k2eAgFnSc1d1	servírovací
vidlička	vidlička	k1gFnSc1	vidlička
–	–	k?	–
větší	veliký	k2eAgFnSc1d2	veliký
vidlička	vidlička	k1gFnSc1	vidlička
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
delšími	dlouhý	k2eAgInPc7d2	delší
hroty	hrot	k1gInPc7	hrot
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
porcí	porce	k1gFnPc2	porce
jídla	jídlo	k1gNnSc2	jídlo
se	se	k3xPyFc4	se
servírovacích	servírovací	k2eAgFnPc2d1	servírovací
mis	mísa	k1gFnPc2	mísa
na	na	k7c6	na
talířExistuje	talířExistovat	k5eAaBmIp3nS	talířExistovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
speciálních	speciální	k2eAgFnPc2d1	speciální
vidliček	vidlička	k1gFnPc2	vidlička
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vidličky	vidlička	k1gFnPc4	vidlička
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
hroty	hrot	k1gInPc7	hrot
na	na	k7c4	na
fondue	fondue	k1gFnSc4	fondue
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
vidličky	vidlička	k1gFnPc4	vidlička
užívali	užívat	k5eAaImAgMnP	užívat
již	již	k6eAd1	již
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tohoto	tento	k3xDgNnSc2	tento
nářadí	nářadí	k1gNnSc2	nářadí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Samuel	Samuel	k1gMnSc1	Samuel
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
archeologických	archeologický	k2eAgInPc6d1	archeologický
nálezech	nález	k1gInPc6	nález
z	z	k7c2	z
období	období	k1gNnSc2	období
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
doklady	doklad	k1gInPc1	doklad
vidliček	vidlička	k1gFnPc2	vidlička
datované	datovaný	k2eAgNnSc1d1	datované
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
rovnými	rovný	k2eAgInPc7d1	rovný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Širšího	široký	k2eAgNnSc2d2	širší
užití	užití	k1gNnSc2	užití
jídelní	jídelní	k2eAgFnSc1d1	jídelní
vidlička	vidlička	k1gFnSc1	vidlička
doznala	doznat	k5eAaPmAgFnS	doznat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nepostradatelným	postradatelný	k2eNgInSc7d1	nepostradatelný
předmětem	předmět	k1gInSc7	předmět
při	při	k7c6	při
stolování	stolování	k1gNnSc6	stolování
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
šlechty	šlechta	k1gFnSc2	šlechta
bohatých	bohatý	k2eAgMnPc2d1	bohatý
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
vidlice	vidlice	k1gFnPc1	vidlice
především	především	k6eAd1	především
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
či	či	k8xC	či
servírování	servírování	k1gNnSc3	servírování
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgNnSc1d1	jiné
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Slovem	slovem	k6eAd1	slovem
vidlice	vidlice	k1gFnSc1	vidlice
a	a	k8xC	a
vidlička	vidlička	k1gFnSc1	vidlička
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
označovány	označován	k2eAgInPc4d1	označován
ale	ale	k8xC	ale
mnohé	mnohý	k2eAgInPc4d1	mnohý
jiné	jiný	k2eAgInPc4d1	jiný
biologické	biologický	k2eAgInPc4d1	biologický
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgInPc4d1	strojní
či	či	k8xC	či
elektrotechnické	elektrotechnický	k2eAgInPc4d1	elektrotechnický
objekty	objekt	k1gInPc4	objekt
či	či	k8xC	či
pracovní	pracovní	k2eAgInPc4d1	pracovní
nástroje	nástroj	k1gInPc4	nástroj
–	–	k?	–
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
slova	slovo	k1gNnSc2	slovo
nůž	nůž	k1gInSc4	nůž
<g/>
)	)	kIx)	)
obecným	obecný	k2eAgInSc7d1	obecný
technickým	technický	k2eAgInSc7d1	technický
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vidlička	vidlička	k1gFnSc1	vidlička
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vidlička	vidlička	k1gFnSc1	vidlička
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
