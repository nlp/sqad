<s>
Josef	Josef	k1gMnSc1
Friml	Friml	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Friml	Friml	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1861	#num#	k4
<g/>
Nový	nový	k2eAgInSc4d1
Hrádek	hrádek	k1gInSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1946	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
84	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Třebechovice	Třebechovice	k1gFnPc1
pod	pod	k7c4
Orebem-Bědovice	Orebem-Bědovice	k1gFnPc4
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Friml	Friml	k1gMnSc1xF
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1861	#num#	k4
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Hrádek	hrádek	k1gInSc1
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1946	#num#	k4
<g/>
,	,	kIx,
Třebechovice	Třebechovice	k1gFnPc1
pod	pod	k7c7
Orebem-Bědovice	Oreb-Bědovice	k1gInSc7
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
sekerník	sekerník	k1gMnSc1
<g/>
,	,	kIx,
povozník	povozník	k1gMnSc1
a	a	k8xC
autor	autor	k1gMnSc1
mechanismu	mechanismus	k1gInSc2
Třebechovického	třebechovický	k2eAgInSc2d1
betlému	betlém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Údajně	údajně	k6eAd1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
posledních	poslední	k2eAgMnPc2d1
mistrů	mistr	k1gMnPc2
sekernického	sekernický	k2eAgNnSc2d1
řemesla	řemeslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živil	živit	k5eAaImAgMnS
se	se	k3xPyFc4
převážně	převážně	k6eAd1
stavbou	stavba	k1gFnSc7
mechanismů	mechanismus	k1gInPc2
mlýnů	mlýn	k1gInPc2
na	na	k7c4
vodní	vodní	k2eAgInSc4d1
pohon	pohon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorovi	autor	k1gMnSc3
Třebechovického	třebechovický	k2eAgInSc2d1
betlému	betlém	k1gInSc2
Josefu	Josef	k1gMnSc3
Proboštovi	probošt	k1gMnSc3
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
využije	využít	k5eAaPmIp3nS
svých	svůj	k3xOyFgFnPc2
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
uvede	uvést	k5eAaPmIp3nS
pomocí	pomocí	k7c2
dřevěného	dřevěný	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
betlém	betlém	k1gInSc4
s	s	k7c7
figurkami	figurka	k1gFnPc7
do	do	k7c2
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1861	#num#	k4
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Hrádku	Hrádok	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
syn	syn	k1gMnSc1
zedníka	zedník	k1gMnSc2
Josefa	Josef	k1gMnSc2
Frimla	Friml	k1gMnSc2
a	a	k8xC
matky	matka	k1gFnSc2
Anny	Anna	k1gFnSc2
<g/>
,	,	kIx,
rozené	rozený	k2eAgFnSc2d1
Fialové	Fialová	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
dvakrát	dvakrát	k6eAd1
ženat	ženat	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
18.4	18.4	k4
<g/>
.1887	.1887	k4
s	s	k7c7
Marií	Maria	k1gFnSc7
Umlaufovou	Umlaufův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1888	#num#	k4
a	a	k8xC
1892	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodili	narodit	k5eAaPmAgMnP
synové	syn	k1gMnPc1
Karel	Karel	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Jako	jako	k8xS,k8xC
vdovec	vdovec	k1gMnSc1
se	s	k7c7
30.5	30.5	k4
<g/>
.1918	.1918	k4
oženil	oženit	k5eAaPmAgMnS
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
s	s	k7c7
Františkou	Františka	k1gFnSc7
Kubíkovou	Kubíková	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Bědovicích	Bědovice	k1gFnPc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
část	část	k1gFnSc4
Třebechovic	Třebechovice	k1gFnPc2
pod	pod	k7c7
Orebem	Oreb	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ZEMANOVÁ	Zemanová	k1gFnSc1
<g/>
,	,	kIx,
Zita	Zita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakta	faktum	k1gNnPc1
a	a	k8xC
úvahy	úvaha	k1gFnPc1
o	o	k7c6
Třebechovickém	třebechovický	k2eAgInSc6d1
betlému	betlém	k1gInSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Betlémář	Betlémář	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VACLÍK	VACLÍK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	list	k1gInPc7
z	z	k7c2
kroniky	kronika	k1gFnSc2
českého	český	k2eAgNnSc2d1
betlémářství	betlémářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
:	:	kIx,
Oftis	Oftis	k1gFnSc7
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7405	#num#	k4
<g/>
-	-	kIx~
<g/>
143	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Nadšení	nadšení	k1gNnSc2
<g/>
,	,	kIx,
slzy	slza	k1gFnSc2
i	i	k8xC
prokletí	prokletí	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Nový	nový	k2eAgInSc1d1
Hrádek	hrádek	k1gInSc1
<g/>
,	,	kIx,
matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgInPc2d1
1858	#num#	k4
<g/>
-	-	kIx~
<g/>
1877	#num#	k4
<g/>
,	,	kIx,
snímek	snímek	k1gInSc1
50	#num#	k4
<g/>
,	,	kIx,
Záznam	záznam	k1gInSc1
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
↑	↑	k?
Třebechovice	Třebechovice	k1gFnPc1
<g/>
,	,	kIx,
index	index	k1gInSc1
narozených	narozený	k2eAgInPc2d1
1864	#num#	k4
<g/>
-	-	kIx~
<g/>
1894	#num#	k4
<g/>
,	,	kIx,
snímek	snímek	k1gInSc1
25	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
hka	hka	k?
<g/>
2011666256	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
4021	#num#	k4
110X	110X	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
186976213	#num#	k4
</s>
