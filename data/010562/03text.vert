<p>
<s>
David	David	k1gMnSc1	David
Andrew	Andrew	k1gMnSc1	Andrew
Jenkins	Jenkinsa	k1gFnPc2	Jenkinsa
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
Pointe-à	Pointe-à	k1gFnPc2	Pointe-à
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
britský	britský	k2eAgMnSc1d1	britský
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
běh	běh	k1gInSc4	běh
na	na	k7c4	na
400	[number]	k4	400
m	m	kA	m
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
jako	jako	k9	jako
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
britský	britský	k2eAgMnSc1d1	britský
reprezentant	reprezentant	k1gMnSc1	reprezentant
nečekaně	nečekaně	k6eAd1	nečekaně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
druhý	druhý	k4xOgMnSc1	druhý
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
finišmanem	finišman	k1gMnSc7	finišman
vítězné	vítězný	k2eAgFnSc2d1	vítězná
britské	britský	k2eAgFnSc2d1	britská
štafety	štafeta	k1gFnSc2	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
úspěchem	úspěch	k1gInSc7	úspěch
byla	být	k5eAaImAgFnS	být
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
medaile	medaile	k1gFnSc1	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
osobní	osobní	k2eAgInSc4d1	osobní
rekord	rekord	k1gInSc4	rekord
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
44,93	[number]	k4	44,93
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Jenkins	Jenkins	k1gInSc4	Jenkins
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
sports-reference	sportseferenec	k1gInSc2	sports-referenec
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
