<s>
Muchova	Muchův	k2eAgFnSc1d1	Muchova
dokumentace	dokumentace	k1gFnSc1	dokumentace
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
Říjnovou	říjnový	k2eAgFnSc7d1	říjnová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
tvořila	tvořit	k5eAaImAgFnS	tvořit
důležité	důležitý	k2eAgInPc4d1	důležitý
podklady	podklad	k1gInPc4	podklad
právě	právě	k9	právě
pro	pro	k7c4	pro
cyklus	cyklus	k1gInSc4	cyklus
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
.	.	kIx.	.
</s>
