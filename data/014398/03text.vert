<s>
GPS	GPS	kA
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
GPS	GPS	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
družice	družice	k1gFnSc2
GPS	GPS	kA
na	na	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
bloku	blok	k1gInSc2
IIF	IIF	kA
(	(	kIx(
<g/>
obrázek	obrázek	k1gInSc1
NASA	NASA	kA
<g/>
)	)	kIx)
</s>
<s>
GPS	GPS	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Global	globat	k5eAaImAgInS
Positioning	Positioning	k1gInSc1
System	Systo	k1gNnSc7
<g/>
,	,	kIx,
česky	česky	k6eAd1
globální	globální	k2eAgInSc4d1
polohový	polohový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
džípíeska	džípíeska	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
vlastněný	vlastněný	k2eAgInSc1d1
USA	USA	kA
a	a	k8xC
provozovaný	provozovaný	k2eAgInSc1d1
Vesmírnými	vesmírný	k2eAgFnPc7d1
silami	síla	k1gFnPc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
umožňuje	umožňovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
elektronického	elektronický	k2eAgInSc2d1
přijímače	přijímač	k1gInSc2
určit	určit	k5eAaPmF
přesnou	přesný	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
na	na	k7c6
povrchu	povrch	k1gInSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
nahrazuje	nahrazovat	k5eAaImIp3nS
tak	tak	k6eAd1
starší	starý	k2eAgFnPc4d2
metody	metoda	k1gFnPc4
založené	založený	k2eAgFnPc4d1
na	na	k7c4
pozorování	pozorování	k1gNnPc4
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
používání	používání	k1gNnSc1
sextantu	sextant	k1gInSc2
nebo	nebo	k8xC
triangulace	triangulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
GPS	GPS	kA
přešlo	přejít	k5eAaPmAgNnS
do	do	k7c2
obecné	obecný	k2eAgFnSc2d1
mluvy	mluva	k1gFnSc2
jako	jako	k8xC,k8xS
označení	označení	k1gNnSc2
pro	pro	k7c4
jakýkoliv	jakýkoliv	k3yIgInSc4
elektronický	elektronický	k2eAgInSc4d1
systém	systém	k1gInSc4
zjišťování	zjišťování	k1gNnSc2
polohy	poloha	k1gFnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
existují	existovat	k5eAaImIp3nP
další	další	k2eAgInPc1d1
systémy	systém	k1gInPc1
(	(	kIx(
<g/>
GLONASS	GLONASS	kA
<g/>
,	,	kIx,
Galileo	Galilea	k1gFnSc5
<g/>
,	,	kIx,
BeiDou	BeiDý	k2eAgFnSc7d1
a	a	k8xC
další	další	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
GPS	GPS	kA
systém	systém	k1gInSc1
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
v	v	k7c6
mnoha	mnoho	k4c6
oborech	obor	k1gInPc6
lidské	lidský	k2eAgFnPc1d1
činnosti	činnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Příprava	příprava	k1gFnSc1
startu	start	k1gInSc2
rakety	raketa	k1gFnSc2
Delta	delta	k1gFnSc1
II	II	kA
s	s	k7c7
družicí	družice	k1gFnSc7
bloku	blok	k1gInSc2
IIA	IIA	kA
nesoucí	nesoucí	k2eAgNnSc1d1
označení	označení	k1gNnSc1
"	"	kIx"
<g/>
NAVSTAR	NAVSTAR	kA
33	#num#	k4
<g/>
"	"	kIx"
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
fotografie	fotografia	k1gFnSc2
NASA	NASA	kA
<g/>
)	)	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
předchozí	předchozí	k2eAgInSc4d1
GNSS	GNSS	kA
Transit	transit	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
rozšiřuje	rozšiřovat	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
především	především	k6eAd1
kvalitou	kvalita	k1gFnSc7
<g/>
,	,	kIx,
dostupností	dostupnost	k1gFnSc7
<g/>
,	,	kIx,
přesností	přesnost	k1gFnSc7
a	a	k8xC
službami	služba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
NAVSTAR	NAVSTAR	kA
GPS	GPS	kA
(	(	kIx(
<g/>
Navigation	Navigation	k1gInSc1
Signal	Signal	k1gFnPc2
Timing	Timing	k1gInSc4
and	and	k?
Ranging	Ranging	k1gInSc1
Global	globat	k5eAaImAgInS
Positioning	Positioning	k1gInSc1
System	Syst	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
nesou	nést	k5eAaImIp3nP
také	také	k9
družice	družice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
systém	systém	k1gInSc1
GPS	GPS	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
činnosti	činnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
NAVSTAR	NAVSTAR	kA
GPS	GPS	kA
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
sloučením	sloučení	k1gNnSc7
dvou	dva	k4xCgInPc2
projektů	projekt	k1gInPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
určování	určování	k1gNnSc4
polohy	poloha	k1gFnSc2
System	Syst	k1gInSc7
621B	621B	k4
(	(	kIx(
<g/>
USAF	USAF	kA
<g/>
)	)	kIx)
a	a	k8xC
pro	pro	k7c4
přesné	přesný	k2eAgNnSc4d1
určování	určování	k1gNnSc4
času	čas	k1gInSc2
Timation	Timation	k1gInSc1
(	(	kIx(
<g/>
US	US	kA
Navy	Navy	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
léty	léto	k1gNnPc7
1974	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
byly	být	k5eAaImAgFnP
prováděny	provádět	k5eAaImNgInP
testy	test	k1gInPc1
na	na	k7c6
pozemních	pozemní	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
a	a	k8xC
byl	být	k5eAaImAgMnS
zkonstruován	zkonstruován	k2eAgInSc4d1
experimentální	experimentální	k2eAgInSc4d1
přijímač	přijímač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
začalo	začít	k5eAaPmAgNnS
vypouštění	vypouštění	k1gNnSc1
11	#num#	k4
vývojových	vývojový	k2eAgFnPc2d1
družic	družice	k1gFnPc2
bloku	blok	k1gInSc2
I.	I.	kA
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
byl	být	k5eAaImAgInS
rozšířen	rozšířit	k5eAaPmNgInS
původní	původní	k2eAgInSc1d1
návrh	návrh	k1gInSc1
z	z	k7c2
nedostačujících	dostačující	k2eNgNnPc2d1
18	#num#	k4
na	na	k7c4
24	#num#	k4
družic	družice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
začalo	začít	k5eAaPmAgNnS
vypouštění	vypouštění	k1gNnSc1
družic	družice	k1gFnPc2
GPS	GPS	kA
společně	společně	k6eAd1
se	se	k3xPyFc4
senzory	senzor	k1gInPc1
pro	pro	k7c4
detekci	detekce	k1gFnSc4
jaderných	jaderný	k2eAgInPc2d1
výbuchů	výbuch	k1gInPc2
a	a	k8xC
startů	start	k1gInPc2
balistických	balistický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
dohod	dohoda	k1gFnPc2
o	o	k7c6
zákazu	zákaz	k1gInSc6
jaderných	jaderný	k2eAgInPc2d1
testů	test	k1gInPc2
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
SSSR	SSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
projekt	projekt	k1gInSc1
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
finančních	finanční	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sovětská	sovětský	k2eAgFnSc1d1
stíhačka	stíhačka	k1gFnSc1
ve	v	k7c6
vzdušném	vzdušný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
SSSR	SSSR	kA
sestřelila	sestřelit	k5eAaPmAgFnS
civilní	civilní	k2eAgNnSc4d1
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
Korean	Korean	k1gMnSc1
Air	Air	k1gMnSc1
Flight	Flight	k1gMnSc1
007	#num#	k4
(	(	kIx(
<g/>
KAL	kal	k1gInSc1
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
všech	všecek	k3xTgMnPc2
269	#num#	k4
lidí	člověk	k1gMnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
zahynulo	zahynout	k5eAaPmAgNnS
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
dokončení	dokončení	k1gNnSc6
bude	být	k5eAaImBp3nS
GPS	GPS	kA
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k8xC
pro	pro	k7c4
civilní	civilní	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Zálivu	záliv	k1gInSc6
byla	být	k5eAaImAgFnS
dočasně	dočasně	k6eAd1
deaktivována	deaktivován	k2eAgFnSc1d1
selektivní	selektivní	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
pro	pro	k7c4
neautorizované	autorizovaný	k2eNgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
armádních	armádní	k2eAgMnPc2d1
přijímačů	přijímač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapojena	zapojen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
opět	opět	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1
operační	operační	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
(	(	kIx(
<g/>
IOC	IOC	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1993	#num#	k4
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
operační	operační	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
pak	pak	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
na	na	k7c4
orbitu	orbita	k1gFnSc4
umístěna	umístěn	k2eAgFnSc1d1
kompletní	kompletní	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
24	#num#	k4
družic	družice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Definitivní	definitivní	k2eAgNnSc1d1
zrušení	zrušení	k1gNnSc1
selektivní	selektivní	k2eAgFnSc2d1
dostupnosti	dostupnost	k1gFnSc2
nastalo	nastat	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
v	v	k7c6
civilním	civilní	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
určit	určit	k5eAaPmF
geografickou	geografický	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
přijímače	přijímač	k1gMnSc2
nacházejícího	nacházející	k2eAgMnSc2d1
se	se	k3xPyFc4
kdekoliv	kdekoliv	k6eAd1
na	na	k7c4
Zemi	zem	k1gFnSc4
nebo	nebo	k8xC
nad	nad	k7c7
Zemí	zem	k1gFnSc7
s	s	k7c7
přesností	přesnost	k1gFnSc7
asi	asi	k9
5	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
také	také	k9
čas	čas	k1gInSc4
s	s	k7c7
přesností	přesnost	k1gFnSc7
na	na	k7c4
jednotky	jednotka	k1gFnPc4
nanosekund	nanosekunda	k1gFnPc2
(	(	kIx(
<g/>
přesnost	přesnost	k1gFnSc1
určení	určení	k1gNnSc2
polohy	poloha	k1gFnSc2
pomocí	pomocí	k7c2
GPS	GPS	kA
lze	lze	k6eAd1
s	s	k7c7
použitím	použití	k1gNnSc7
dalších	další	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
zvýšit	zvýšit	k5eAaPmF
až	až	k9
na	na	k7c4
jednotky	jednotka	k1gFnPc4
centimetrů	centimetr	k1gInPc2
<g/>
)	)	kIx)
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
výše	vysoce	k6eAd2
než	než	k8xS
18	#num#	k4
km	km	kA
a	a	k8xC
nepohybuje	pohybovat	k5eNaImIp3nS
se	se	k3xPyFc4
rychleji	rychle	k6eAd2
než	než	k8xS
2000	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
První	první	k4xOgFnSc1
družice	družice	k1gFnSc1
bloku	blok	k1gInSc2
IIR-M	IIR-M	k1gFnSc2
podporující	podporující	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
civilní	civilní	k2eAgInSc1d1
signál	signál	k1gInSc1
označovaný	označovaný	k2eAgInSc1d1
L2C	L2C	k1gFnSc4
byla	být	k5eAaImAgFnS
vypuštěna	vypuštěn	k2eAgFnSc1d1
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
trh	trh	k1gInSc4
uveden	uvést	k5eAaPmNgInS
první	první	k4xOgInSc1
čip	čip	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
umí	umět	k5eAaImIp3nS
využívat	využívat	k5eAaPmF,k5eAaImF
kromě	kromě	k7c2
signálu	signál	k1gInSc2
L1	L1	k1gFnSc2
též	též	k9
signál	signál	k1gInSc4
L	L	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
určit	určit	k5eAaPmF
pozici	pozice	k1gFnSc4
s	s	k7c7
přesností	přesnost	k1gFnSc7
až	až	k9
na	na	k7c4
30	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2023	#num#	k4
bude	být	k5eAaImBp3nS
spuštěna	spustit	k5eAaPmNgFnS
GPS	GPS	kA
III	III	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přinese	přinést	k5eAaPmIp3nS
trojnásobně	trojnásobně	k6eAd1
lepší	dobrý	k2eAgFnSc4d2
přesnost	přesnost	k1gFnSc4
a	a	k8xC
vyšší	vysoký	k2eAgFnSc4d2
spolehlivost	spolehlivost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Bude	být	k5eAaImBp3nS
zdvojnásobena	zdvojnásoben	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
družic	družice	k1gFnPc2
na	na	k7c4
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
přesnost	přesnost	k1gFnSc1
bude	být	k5eAaImBp3nS
1	#num#	k4
až	až	k9
3	#num#	k4
metry	metr	k1gInPc4
a	a	k8xC
bude	být	k5eAaImBp3nS
vysílat	vysílat	k5eAaImF
silnější	silný	k2eAgInSc4d2
signál	signál	k1gInSc4
L	L	kA
<g/>
1	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
bude	být	k5eAaImBp3nS
používat	používat	k5eAaImF
stejnou	stejný	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
jako	jako	k8xC,k8xS
evropská	evropský	k2eAgFnSc1d1
síť	síť	k1gFnSc1
Galileo	Galilea	k1gFnSc5
<g/>
,	,	kIx,
japonská	japonský	k2eAgFnSc1d1
QZSS	QZSS	kA
a	a	k8xC
čínská	čínský	k2eAgFnSc1d1
Beidou	Beida	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
jsou	být	k5eAaImIp3nP
na	na	k7c6
orbitě	orbita	k1gFnSc6
již	již	k6eAd1
dva	dva	k4xCgInPc1
satelity	satelit	k1gInPc1
(	(	kIx(
<g/>
Vespucci	Vespucce	k1gMnPc1
<g/>
,	,	kIx,
Magellan	Magellan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
červnu	červen	k1gInSc6
2020	#num#	k4
bude	být	k5eAaImBp3nS
vypuštěn	vypuštěn	k2eAgInSc1d1
další	další	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
provoz	provoz	k1gInSc4
GPS	GPS	kA
je	být	k5eAaImIp3nS
ročně	ročně	k6eAd1
vynakládána	vynakládán	k2eAgFnSc1d1
částka	částka	k1gFnSc1
přibližně	přibližně	k6eAd1
600	#num#	k4
až	až	k9
900	#num#	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
z	z	k7c2
rozpočtu	rozpočet	k1gInSc2
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Princip	princip	k1gInSc1
funkce	funkce	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Globální	globální	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
systému	systém	k1gInSc2
</s>
<s>
Celý	celý	k2eAgInSc1d1
systém	systém	k1gInSc1
GPS	GPS	kA
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
3	#num#	k4
segmentů	segment	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
kosmický	kosmický	k2eAgInSc1d1
</s>
<s>
řídící	řídící	k2eAgNnSc1d1
</s>
<s>
uživatelský	uživatelský	k2eAgInSc1d1
</s>
<s>
Kosmický	kosmický	k2eAgInSc1d1
segment	segment	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
družic	družice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Blok	blok	k1gInSc1
(	(	kIx(
<g/>
životnost	životnost	k1gFnSc1
<g/>
#	#	kIx~
<g/>
)	)	kIx)
<g/>
ObdobíVypuštěnoAktivníSignály	ObdobíVypuštěnoAktivníSignála	k1gFnPc1
<g/>
&	&	k?
</s>
<s>
I	i	k9
</s>
<s>
1978	#num#	k4
<g/>
–	–	k?
<g/>
198510	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
✝	✝	k?
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
II	II	kA
(	(	kIx(
<g/>
7,5	7,5	k4
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990901	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
IIA	IIA	kA
(	(	kIx(
<g/>
7,5	7,5	k4
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
19961901	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
IIR	IIR	kA
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
200412	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
✝	✝	k?
<g/>
101	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
IIR-M	IIR-M	k?
(	(	kIx(
<g/>
8,5	8,5	k4
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2009872	#num#	k4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
</s>
<s>
IIF	IIF	kA
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
201612123	#num#	k4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
</s>
<s>
IIIA	IIIA	kA
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
20232	#num#	k4
<g/>
+	+	kIx~
<g/>
8	#num#	k4
<g/>
$	$	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
</s>
<s>
IIIF	IIIF	kA
</s>
<s>
2026	#num#	k4
<g/>
–	–	k?
<g/>
20340	#num#	k4
<g/>
+	+	kIx~
<g/>
22	#num#	k4
<g/>
v	v	k7c4
<g/>
2	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
72	#num#	k4
+2	+2	k4
<g/>
✝	✝	k?
+8	+8	k4
<g/>
$	$	kIx~
+22	+22	k4
<g/>
v	v	k7c4
<g/>
31	#num#	k4
</s>
<s>
v	v	k7c4
Výhled	výhled	k1gInSc4
<g/>
,	,	kIx,
plán	plán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
✝	✝	k?
Ztracena	ztraceno	k1gNnPc1
při	při	k7c6
startu	start	k1gInSc6
nebo	nebo	k8xC
selhalo	selhat	k5eAaPmAgNnS
oživení	oživení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
$	$	kIx~
V	v	k7c6
přípravě	příprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
#	#	kIx~
Plánovaná	plánovaný	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
<g/>
.	.	kIx.
<g/>
&	&	k?
Vysílané	vysílaný	k2eAgInPc4d1
civilní	civilní	k2eAgInPc4d1
+	+	kIx~
vojenské	vojenský	k2eAgInPc4d1
signály	signál	k1gInPc4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
Poslední	poslední	k2eAgFnSc1d1
změna	změna	k1gFnSc1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
satelitů	satelit	k1gMnPc2
GPS	GPS	kA
<g/>
.	.	kIx.
<g/>
Schema	schema	k1gNnSc1
změn	změna	k1gFnPc2
poloh	poloha	k1gFnPc2
družic	družice	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
viditelnosti	viditelnost	k1gFnPc1
z	z	k7c2
konkrétního	konkrétní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Kosmický	kosmický	k2eAgInSc1d1
segment	segment	k1gInSc1
byl	být	k5eAaImAgInS
projektován	projektovat	k5eAaBmNgInS
na	na	k7c4
24	#num#	k4
družic	družice	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
až	až	k9
na	na	k7c4
mezní	mezní	k2eAgInSc4d1
počet	počet	k1gInSc4
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
v	v	k7c6
datovém	datový	k2eAgInSc6d1
rámci	rámec	k1gInSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
číslo	číslo	k1gNnSc4
satelitu	satelit	k1gInSc2
určeno	určit	k5eAaPmNgNnS
jen	jen	k9
5	#num#	k4
bitů	bit	k1gInPc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
=	=	kIx~
<g/>
32	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
třeba	třeba	k6eAd1
pro	pro	k7c4
další	další	k2eAgNnSc4d1
navyšování	navyšování	k1gNnSc4
počtu	počet	k1gInSc2
změnit	změnit	k5eAaPmF
strukturu	struktura	k1gFnSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Družice	družice	k1gFnPc1
obíhají	obíhat	k5eAaImIp3nP
ve	v	k7c6
výšce	výška	k1gFnSc6
20	#num#	k4
350	#num#	k4
km	km	kA
nad	nad	k7c7
povrchem	povrch	k1gInSc7
Země	zem	k1gFnSc2
na	na	k7c6
6	#num#	k4
kruhových	kruhový	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
se	se	k3xPyFc4
sklonem	sklon	k1gInSc7
55	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráhy	dráha	k1gFnPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
posunuty	posunout	k5eAaPmNgFnP
o	o	k7c4
60	#num#	k4
<g/>
°	°	k?
a	a	k8xC
na	na	k7c6
každé	každý	k3xTgFnSc6
dráze	dráha	k1gFnSc6
jsou	být	k5eAaImIp3nP
původně	původně	k6eAd1
4	#num#	k4
pravidelně	pravidelně	k6eAd1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
5-6	5-6	k4
nepravidelně	pravidelně	k6eNd1
<g/>
,	,	kIx,
rozmístěné	rozmístěný	k2eAgFnPc1d1
pozice	pozice	k1gFnPc1
pro	pro	k7c4
družice	družice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Družice	družice	k1gFnPc1
váží	vážit	k5eAaImIp3nP
asi	asi	k9
1,8	1,8	k4
tuny	tuna	k1gFnSc2
a	a	k8xC
na	na	k7c6
střední	střední	k2eAgFnSc6d1
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
(	(	kIx(
<g/>
MEO	MEO	kA
<g/>
,	,	kIx,
Medium	medium	k1gNnSc1
Earth	Eartha	k1gFnPc2
Orbit	orbita	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
3,8	3,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
s	s	k7c7
dobou	doba	k1gFnSc7
oběhu	oběh	k1gInSc2
kolem	kolem	k7c2
Země	zem	k1gFnSc2
11	#num#	k4
h	h	k?
58	#num#	k4
min	mina	k1gFnPc2
(	(	kIx(
<g/>
polovina	polovina	k1gFnSc1
siderického	siderický	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Klíčové	klíčový	k2eAgFnSc2d1
části	část	k1gFnSc2
družic	družice	k1gFnPc2
NAVSTAR	NAVSTAR	kA
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
až	až	k6eAd1
4	#num#	k4
velmi	velmi	k6eAd1
přesné	přesný	k2eAgNnSc1d1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
13	#num#	k4
<g/>
s	s	k7c7
<g/>
)	)	kIx)
atomové	atomový	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
s	s	k7c7
rubidiovým	rubidiový	k2eAgInSc7d1
dříve	dříve	k6eAd2
také	také	k6eAd1
s	s	k7c7
cesiovým	cesiový	k2eAgInSc7d1
oscilátorem	oscilátor	k1gInSc7
</s>
<s>
12	#num#	k4
antén	anténa	k1gFnPc2
RHCP	RHCP	kA
pro	pro	k7c4
vysílání	vysílání	k1gNnSc4
rádiových	rádiový	k2eAgInPc2d1
kódů	kód	k1gInPc2
v	v	k7c6
pásmu	pásmo	k1gNnSc6
L	L	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
1000	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
antény	anténa	k1gFnPc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
pozemními	pozemní	k2eAgFnPc7d1
kontrolními	kontrolní	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
v	v	k7c6
pásmu	pásmo	k1gNnSc6
S	s	k7c7
(	(	kIx(
<g/>
2204,4	2204,4	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
</s>
<s>
antény	anténa	k1gFnPc1
pro	pro	k7c4
vzájemnou	vzájemný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
družic	družice	k1gFnPc2
v	v	k7c6
pásmu	pásmo	k1gNnSc6
UHF	UHF	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
označováno	označovat	k5eAaImNgNnS
UKV	UKV	kA
<g/>
)	)	kIx)
</s>
<s>
optické	optický	k2eAgFnPc1d1
<g/>
,	,	kIx,
rentgenové	rentgenový	k2eAgFnPc1d1
a	a	k8xC
pulzní-elektromagnetické	pulzní-elektromagnetický	k2eAgInPc1d1
detektory	detektor	k1gInPc1
odhalující	odhalující	k2eAgInPc1d1
starty	start	k1gInPc1
balistických	balistický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
a	a	k8xC
jaderné	jaderný	k2eAgInPc1d1
výbuchy	výbuch	k1gInPc1
(	(	kIx(
<g/>
IONDS	IONDS	kA
<g/>
)	)	kIx)
</s>
<s>
solární	solární	k2eAgInPc4d1
panely	panel	k1gInPc4
a	a	k8xC
baterie	baterie	k1gFnPc4
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc4
energie	energie	k1gFnSc2
</s>
<s>
Na	na	k7c6
dvou	dva	k4xCgInPc6
exemplářích	exemplář	k1gInPc6
družic	družice	k1gFnPc2
PRN	PRN	kA
35	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
bloku	blok	k1gInSc2
IIR	IIR	kA
vypuštěných	vypuštěný	k2eAgFnPc2d1
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
a	a	k8xC
1994	#num#	k4
byla	být	k5eAaImAgFnS
testována	testovat	k5eAaImNgFnS
odrazová	odrazový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
pro	pro	k7c4
měření	měření	k1gNnSc4
polohy	poloha	k1gFnSc2
družice	družice	k1gFnSc2
laserovými	laserový	k2eAgNnPc7d1
měřidly	měřidlo	k1gNnPc7
(	(	kIx(
<g/>
SLR	SLR	kA
<g/>
)	)	kIx)
projektu	projekt	k1gInSc2
NASA	NASA	kA
ILRS	ILRS	kA
(	(	kIx(
<g/>
International	International	k1gFnSc1
Laser	laser	k1gInSc4
Ranging	Ranging	k1gInSc1
Service	Service	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Konstrukce	konstrukce	k1gFnSc1
zrcadla	zrcadlo	k1gNnSc2
vážila	vážit	k5eAaImAgFnS
asi	asi	k9
10	#num#	k4
kg	kg	kA
o	o	k7c6
úhlopříčce	úhlopříčka	k1gFnSc6
půdorysného	půdorysný	k2eAgInSc2d1
obdélníku	obdélník	k1gInSc2
20	#num#	k4
cm	cm	kA
a	a	k8xC
byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
32	#num#	k4
dílčími	dílčí	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
některém	některý	k3yIgInSc6
z	z	k7c2
bloků	blok	k1gInPc2
III	III	kA
bude	být	k5eAaImBp3nS
opět	opět	k6eAd1
tato	tento	k3xDgFnSc1
technologie	technologie	k1gFnSc1
využita	využít	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
nejčetnější	četný	k2eAgFnSc1d3
viditelnost	viditelnost	k1gFnSc1
8	#num#	k4
družic	družice	k1gFnPc2
(	(	kIx(
<g/>
medián	medián	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
minimum	minimum	k1gNnSc4
pak	pak	k8xC
6	#num#	k4
<g/>
,	,	kIx,
maximum	maximum	k1gNnSc1
12	#num#	k4
družic	družice	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
elevační	elevační	k2eAgFnSc6d1
masce	maska	k1gFnSc6
10	#num#	k4
<g/>
°	°	k?
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Družice	družice	k1gFnPc1
jsou	být	k5eAaImIp3nP
několikrát	několikrát	k6eAd1
do	do	k7c2
roka	rok	k1gInSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
plánovaně	plánovaně	k6eAd1
<g/>
,	,	kIx,
odstaveny	odstaven	k2eAgInPc4d1
pro	pro	k7c4
údržbu	údržba	k1gFnSc4
atomových	atomový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
korekci	korekce	k1gFnSc4
dráhy	dráha	k1gFnSc2
družice	družice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údržba	údržba	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
12-24	12-24	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
družice	družice	k1gFnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
10	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obměna	obměna	k1gFnSc1
kosmického	kosmický	k2eAgInSc2d1
segmentu	segment	k1gInSc2
trvá	trvat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
popis	popis	k1gInSc4
stavu	stav	k1gInSc2
kosmického	kosmický	k2eAgInSc2d1
segmentu	segment	k1gInSc2
jsou	být	k5eAaImIp3nP
definovány	definován	k2eAgInPc1d1
dva	dva	k4xCgInPc1
stavy	stav	k1gInPc1
implementace	implementace	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
plná	plný	k2eAgFnSc1d1
operační	operační	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
(	(	kIx(
<g/>
FOC	FOC	kA
<g/>
,	,	kIx,
Full	Full	k1gMnSc1
Operational	Operational	k1gFnSc2
Capability	Capabilita	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
označení	označení	k1gNnSc1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
24	#num#	k4
družic	družice	k1gFnPc2
plně	plně	k6eAd1
funkčních	funkční	k2eAgFnPc2d1
<g/>
,	,	kIx,
podporující	podporující	k2eAgFnSc4d1
novou	nový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1995	#num#	k4
po	po	k7c4
vypuštění	vypuštění	k1gNnSc4
a	a	k8xC
zprovoznění	zprovoznění	k1gNnSc4
24	#num#	k4
družic	družice	k1gFnPc2
Bloku	blok	k1gInSc2
II	II	kA
a	a	k8xC
IIA	IIA	kA
<g/>
.	.	kIx.
</s>
<s>
částečná	částečný	k2eAgFnSc1d1
operační	operační	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
(	(	kIx(
<g/>
IOC	IOC	kA
<g/>
,	,	kIx,
Initial	Initial	k1gMnSc1
Operational	Operational	k1gFnSc2
Capability	Capabilita	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
označení	označení	k1gNnSc1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
18	#num#	k4
družic	družice	k1gFnPc2
plně	plně	k6eAd1
funkčních	funkční	k2eAgFnPc2d1
<g/>
,	,	kIx,
podporující	podporující	k2eAgFnSc4d1
novou	nový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1993	#num#	k4
po	po	k7c4
vypuštění	vypuštění	k1gNnSc4
a	a	k8xC
zprovoznění	zprovoznění	k1gNnSc4
18	#num#	k4
družic	družice	k1gFnPc2
Bloku	blok	k1gInSc2
I	I	kA
<g/>
,	,	kIx,
II	II	kA
a	a	k8xC
IIA	IIA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byly	být	k5eAaImAgInP
uzavřeny	uzavřen	k2eAgInPc1d1
kontrakty	kontrakt	k1gInPc1
mezi	mezi	k7c7
US	US	kA
Air	Air	k1gFnSc7
Force	force	k1gFnSc7
a	a	k8xC
firmou	firma	k1gFnSc7
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
na	na	k7c4
vývoj	vývoj	k1gInSc4
a	a	k8xC
výrobu	výroba	k1gFnSc4
16	#num#	k4
družic	družice	k1gFnPc2
bloku	blok	k1gInSc2
IIIA	IIIA	kA
v	v	k7c6
ceně	cena	k1gFnSc6
1,5	1,5	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
roky	rok	k1gInPc4
2014	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
na	na	k7c4
12	#num#	k4
družic	družice	k1gFnPc2
bloku	blok	k1gInSc2
IIIB	IIIB	kA
v	v	k7c6
ceně	cena	k1gFnSc6
3	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Řídící	řídící	k2eAgInSc4d1
a	a	k8xC
kontrolní	kontrolní	k2eAgInSc4d1
segment	segment	k1gInSc4
</s>
<s>
Operátorka	operátorka	k1gFnSc1
řídicího	řídicí	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Schriever	Schrievra	k1gFnPc2
monitorující	monitorující	k2eAgInSc1d1
stav	stav	k1gInSc1
kosmického	kosmický	k2eAgInSc2d1
segmentu	segment	k1gInSc2
(	(	kIx(
<g/>
fotografie	fotografia	k1gFnSc2
USAF	USAF	kA
<g/>
)	)	kIx)
</s>
<s>
Segment	segment	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
velitelství	velitelství	k1gNnSc4
–	–	k?
Navstar	Navstar	k1gInSc1
Headquarters	Headquarters	k1gInSc1
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
řídicí	řídicí	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
(	(	kIx(
<g/>
MSC	MSC	kA
<g/>
,	,	kIx,
Master	master	k1gMnSc1
Control	Controla	k1gFnPc2
Station	station	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
Schrieverově	Schrieverův	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc3d1
základně	základna	k1gFnSc3
USAF	USAF	kA
v	v	k7c4
Colorado	Colorado	k1gNnSc4
Springs	Springsa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
nd	nd	k?
Space	Spaec	k1gInSc2
Operations	Operationsa	k1gFnPc2
Sq	Sq	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záložní	záložní	k2eAgFnSc1d1
řídící	řídící	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
(	(	kIx(
<g/>
BMCS	BMCS	kA
<g/>
,	,	kIx,
Backup	backup	k1gInSc1
Master	master	k1gMnSc1
Control	Controla	k1gFnPc2
Station	station	k1gInSc1
<g/>
)	)	kIx)
umístěné	umístěný	k2eAgFnSc2d1
v	v	k7c6
Gaithersburg	Gaithersburg	k1gMnSc1
(	(	kIx(
<g/>
Maryland	Maryland	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
přebírá	přebírat	k5eAaImIp3nS
cvičně	cvičně	k6eAd1
4	#num#	k4
<g/>
×	×	k?
do	do	k7c2
roka	rok	k1gInSc2
řízení	řízení	k1gNnSc2
systému	systém	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
nouzi	nouze	k1gFnSc6
je	být	k5eAaImIp3nS
připravena	připraven	k2eAgFnSc1d1
do	do	k7c2
24	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
povelové	povelový	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
(	(	kIx(
<g/>
Ground	Ground	k1gInSc1
Antenna	Antenn	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
na	na	k7c6
základnách	základna	k1gFnPc6
USAF	USAF	kA
<g/>
:	:	kIx,
Kwajalein	Kwajalein	k1gMnSc1
<g/>
,	,	kIx,
Diego	Diego	k1gMnSc1
Garcia	Garcia	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
Island	Island	k1gInSc1
případně	případně	k6eAd1
i	i	k9
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
monitorovacích	monitorovací	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
(	(	kIx(
<g/>
Monitor	monitor	k1gInSc1
Stations	Stations	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
na	na	k7c6
základnách	základna	k1gFnPc6
USAF	USAF	kA
<g/>
:	:	kIx,
Havaj	Havaj	k1gFnSc1
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
<g/>
,	,	kIx,
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
Diego	Diego	k6eAd1
Garcia	Garcia	k1gFnSc1
<g/>
,	,	kIx,
Kwajalein	Kwajalein	k1gInSc1
a	a	k8xC
dále	daleko	k6eAd2
stanice	stanice	k1gFnSc1
spravující	spravující	k2eAgFnSc2d1
NGA	NGA	kA
<g/>
:	:	kIx,
Fairbanks	Fairbanks	k1gInSc1
(	(	kIx(
<g/>
Aljaška	Aljaška	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
Papeete	Papee	k1gNnSc2
(	(	kIx(
<g/>
Tahiti	Tahiti	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
DC	DC	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Quitto	Quitto	k1gNnSc1
(	(	kIx(
<g/>
Ekvádor	Ekvádor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hermitage	Hermitage	k1gFnSc1
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pretoria	Pretorium	k1gNnPc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Manama	Manama	k?
(	(	kIx(
<g/>
Bahrajn	Bahrajn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Osan	Osan	k1gNnSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adelaide	Adelaid	k1gMnSc5
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Wellington	Wellington	k1gInSc1
(	(	kIx(
<g/>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řídící	řídící	k2eAgInSc1d1
a	a	k8xC
kontrolní	kontrolní	k2eAgInSc1d1
segment	segment	k1gInSc1
monitoruje	monitorovat	k5eAaImIp3nS
kosmický	kosmický	k2eAgInSc1d1
segment	segment	k1gInSc1
<g/>
,	,	kIx,
zasílá	zasílat	k5eAaImIp3nS
povely	povel	k1gInPc1
družicím	družice	k1gFnPc3
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
jejich	jejich	k3xOp3gInPc4
manévry	manévr	k1gInPc4
a	a	k8xC
údržbu	údržba	k1gFnSc4
atomových	atomový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
jejich	jejich	k3xOp3gInSc2
monitoringu	monitoring	k1gInSc2
je	být	k5eAaImIp3nS
zveřejňován	zveřejňovat	k5eAaImNgInS
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
každé	každý	k3xTgFnSc2
družice	družice	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
platnost	platnost	k1gFnSc1
je	být	k5eAaImIp3nS
řádově	řádově	k6eAd1
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
data	datum	k1gNnPc4
pro	pro	k7c4
model	model	k1gInSc4
ionosférické	ionosférický	k2eAgFnSc2d1
refrakce	refrakce	k1gFnSc2
</s>
<s>
predikce	predikce	k1gFnSc1
dráhy	dráha	k1gFnSc2
družice	družice	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
efemerid	efemerida	k1gFnPc2
</s>
<s>
korekce	korekce	k1gFnSc1
atomových	atomový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
</s>
<s>
přibližné	přibližný	k2eAgFnPc1d1
pozice	pozice	k1gFnPc1
ostatních	ostatní	k2eAgFnPc2d1
družic	družice	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Řídící	řídící	k2eAgInSc1d1
a	a	k8xC
kontrolní	kontrolní	k2eAgInSc1d1
segment	segment	k1gInSc1
komunikuje	komunikovat	k5eAaImIp3nS
s	s	k7c7
uživateli	uživatel	k1gMnPc7
také	také	k6eAd1
prostřednictvím	prostřednictvím	k7c2
zpráv	zpráva	k1gFnPc2
GPS	GPS	kA
NANU	NANU	kA
(	(	kIx(
<g/>
Notice	Notice	k?
Advisory	Advisor	k1gInPc1
to	ten	k3xDgNnSc1
NAVSTAR	NAVSTAR	kA
Users	Usersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
zveřejňuje	zveřejňovat	k5eAaImIp3nS
plánované	plánovaný	k2eAgInPc4d1
odstávky	odstávek	k1gInPc4
družic	družice	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc1
stažení	stažení	k1gNnSc1
a	a	k8xC
uvedení	uvedení	k1gNnSc1
do	do	k7c2
provozu	provoz	k1gInSc2
nebo	nebo	k8xC
i	i	k9
zpětně	zpětně	k6eAd1
informace	informace	k1gFnPc1
o	o	k7c6
nezdravé	zdravý	k2eNgFnSc6d1
družici	družice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zničení	zničení	k1gNnSc3
pozemních	pozemní	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
řídícího	řídící	k2eAgInSc2d1
a	a	k8xC
kontrolního	kontrolní	k2eAgInSc2d1
segmentu	segment	k1gInSc2
<g/>
,	,	kIx,
přechází	přecházet	k5eAaImIp3nS
družice	družice	k1gFnSc1
do	do	k7c2
režimu	režim	k1gInSc2
AUTONAV	AUTONAV	kA
<g/>
(	(	kIx(
<g/>
Autonomous	Autonomous	k1gInSc1
Navigation	Navigation	k1gInSc1
Mode	modus	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
dále	daleko	k6eAd2
pracovat	pracovat	k5eAaImF
až	až	k9
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
režimu	režim	k1gInSc6
spolu	spolu	k6eAd1
družice	družice	k1gFnPc4
komunikují	komunikovat	k5eAaImIp3nP
a	a	k8xC
porovnávají	porovnávat	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
své	svůj	k3xOyFgFnPc4
efemeridy	efemerida	k1gFnPc4
a	a	k8xC
stav	stav	k1gInSc4
palubních	palubní	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc7
poskytují	poskytovat	k5eAaImIp3nP
uživatelskému	uživatelský	k2eAgInSc3d1
segmentu	segment	k1gInSc3
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
režim	režim	k1gInSc1
však	však	k9
nikdy	nikdy	k6eAd1
nenastal	nastat	k5eNaPmAgInS
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
ani	ani	k8xC
známy	znám	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
jeho	jeho	k3xOp3gInPc2
případných	případný	k2eAgInPc2d1
testů	test	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uživatelský	uživatelský	k2eAgInSc1d1
segment	segment	k1gInSc1
</s>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1
přijímač	přijímač	k1gInSc1
GPS	GPS	kA
vyžívající	vyžívající	k2eAgFnSc2d1
metod	metoda	k1gFnPc2
kódového	kódový	k2eAgNnSc2d1
měření	měření	k1gNnSc2
(	(	kIx(
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
kód	kód	k1gInSc4
<g/>
)	)	kIx)
s	s	k7c7
integrovanou	integrovaný	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
<g/>
,	,	kIx,
konektorem	konektor	k1gInSc7
pro	pro	k7c4
napájení	napájení	k1gNnSc4
a	a	k8xC
komunikaci	komunikace	k1gFnSc4
</s>
<s>
Navigační	navigační	k2eAgInPc1d1
turistické	turistický	k2eAgInPc1d1
počítače	počítač	k1gInPc1
s	s	k7c7
integrovaným	integrovaný	k2eAgInSc7d1
přijímačem	přijímač	k1gInSc7
GPS	GPS	kA
vyžívající	vyžívající	k2eAgFnPc1d1
metod	metoda	k1gFnPc2
kódového	kódový	k2eAgNnSc2d1
měření	měření	k1gNnSc2
(	(	kIx(
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
kód	kód	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
–	–	k?
podzvuková	podzvukový	k2eAgFnSc1d1
střela	střela	k1gFnSc1
s	s	k7c7
plochou	plochý	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
letu	let	k1gInSc2
BGM-109	BGM-109	k1gMnPc1
Tomahawk	tomahawk	k1gInSc4
využívající	využívající	k2eAgInPc4d1
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
orientaci	orientace	k1gFnSc3
v	v	k7c6
prostoru	prostor	k1gInSc6
a	a	k8xC
navádění	navádění	k1gNnSc1
na	na	k7c4
cíl	cíl	k1gInSc4
mimo	mimo	k6eAd1
inerciálních	inerciální	k2eAgInPc2d1
systémů	systém	k1gInPc2
také	také	k9
systém	systém	k1gInSc1
GPS	GPS	kA
(	(	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
kód	kód	k1gInSc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
fotografie	fotografie	k1gFnSc1
Navy	Navy	k?
US	US	kA
<g/>
)	)	kIx)
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
pomocí	pomocí	k7c2
GPS	GPS	kA
přijímače	přijímač	k1gInSc2
přijímají	přijímat	k5eAaImIp3nP
signály	signál	k1gInPc1
z	z	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
družic	družice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c4
danou	daný	k2eAgFnSc4d1
chvíli	chvíle	k1gFnSc4
nad	nad	k7c7
obzorem	obzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
přijatých	přijatý	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
časových	časový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
z	z	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
družic	družice	k1gFnPc2
a	a	k8xC
znalosti	znalost	k1gFnSc2
jejich	jejich	k3xOp3gFnSc2
polohy	poloha	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
předem	předem	k6eAd1
definovaných	definovaný	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
přijímač	přijímač	k1gInSc1
vypočítá	vypočítat	k5eAaPmIp3nS
polohu	poloha	k1gFnSc4
antény	anténa	k1gFnSc2
<g/>
,	,	kIx,
nadmořskou	nadmořský	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
a	a	k8xC
zobrazí	zobrazit	k5eAaPmIp3nS
přesné	přesný	k2eAgNnSc4d1
datum	datum	k1gNnSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikace	komunikace	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
pouze	pouze	k6eAd1
od	od	k7c2
družic	družice	k1gFnPc2
k	k	k7c3
uživateli	uživatel	k1gMnSc3
<g/>
,	,	kIx,
GPS	GPS	kA
přijímač	přijímač	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
pasivní	pasivní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
přijímačů	přijímač	k1gMnPc2
podle	podle	k7c2
přijímaných	přijímaný	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
jednofrekvenční	jednofrekvenční	k2eAgFnSc1d1
</s>
<s>
dvoufrekvenční	dvoufrekvenční	k2eAgFnSc1d1
</s>
<s>
vícefrekvenční	vícefrekvenční	k2eAgMnPc1d1
(	(	kIx(
<g/>
připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
pro	pro	k7c4
pásmo	pásmo	k1gNnSc4
L	L	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
přijímačů	přijímač	k1gInPc2
podle	podle	k7c2
kanálů	kanál	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
jednokanálové	jednokanálový	k2eAgInPc4d1
(	(	kIx(
<g/>
používané	používaný	k2eAgInPc4d1
v	v	k7c6
raných	raný	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
projektu	projekt	k1gInSc2
GPS	GPS	kA
<g/>
)	)	kIx)
</s>
<s>
vícekanálové	vícekanálový	k2eAgNnSc1d1
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
přijímačů	přijímač	k1gInPc2
podle	podle	k7c2
principu	princip	k1gInSc2
výpočtů	výpočet	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
kódová	kódový	k2eAgFnSc1d1
</s>
<s>
fázová	fázový	k2eAgFnSc1d1
a	a	k8xC
kódová	kódový	k2eAgFnSc1d1
</s>
<s>
Běžně	běžně	k6eAd1
dostupné	dostupný	k2eAgInPc1d1
přijímače	přijímač	k1gInPc1
k	k	k7c3
amatérskému	amatérský	k2eAgInSc3d1
(	(	kIx(
<g/>
tj.	tj.	kA
negeodetickému	geodetický	k2eNgMnSc3d1
a	a	k8xC
nevojenskému	vojenský	k2eNgMnSc3d1
<g/>
)	)	kIx)
vyžití	vyžití	k1gNnSc3
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
jako	jako	k9
jednofrekvenční	jednofrekvenční	k2eAgNnSc1d1
<g/>
,	,	kIx,
vícekanálové	vícekanálový	k2eAgNnSc1d1
a	a	k8xC
kódové	kódový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduchý	jednoduchý	k2eAgInSc1d1
přijímač	přijímač	k1gInSc1
signálu	signál	k1gInSc2
GPS	GPS	kA
pro	pro	k7c4
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
antény	anténa	k1gFnPc1
</s>
<s>
předzesilovače	předzesilovač	k1gInPc1
</s>
<s>
procesoru	procesor	k1gInSc3
</s>
<s>
časové	časový	k2eAgFnPc4d1
základny	základna	k1gFnPc4
(	(	kIx(
<g/>
často	často	k6eAd1
křemíkový	křemíkový	k2eAgInSc1d1
krystal	krystal	k1gInSc1
o	o	k7c6
přesnosti	přesnost	k1gFnSc6
<	<	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
komunikačního	komunikační	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
využívající	využívající	k2eAgInSc4d1
systém	systém	k1gInSc4
GPS	GPS	kA
můžeme	moct	k5eAaImIp1nP
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
dvou	dva	k4xCgFnPc2
skupin	skupina	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
autorizovaní	autorizovaný	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
(	(	kIx(
<g/>
vojenský	vojenský	k2eAgInSc4d1
sektor	sektor	k1gInSc4
USA	USA	kA
a	a	k8xC
vybrané	vybraný	k2eAgFnSc2d1
spojenecké	spojenecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
využívající	využívající	k2eAgFnSc4d1
službu	služba	k1gFnSc4
Precise	Precise	k1gFnSc2
Positioning	Positioning	k1gInSc4
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
PPS	PPS	kA
<g/>
)	)	kIx)
mající	mající	k2eAgInSc1d1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dekódovací	dekódovací	k2eAgInPc4d1
klíče	klíč	k1gInPc4
k	k	k7c3
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
kódu	kód	k1gInSc2
na	na	k7c6
frekvencích	frekvence	k1gFnPc6
L1	L1	k1gFnSc2
a	a	k8xC
L	L	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
uživatelé	uživatel	k1gMnPc1
mají	mít	k5eAaImIp3nP
zaručenou	zaručený	k2eAgFnSc4d1
vyšší	vysoký	k2eAgFnSc4d2
přesnost	přesnost	k1gFnSc4
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplatňují	uplatňovat	k5eAaImIp3nP
se	se	k3xPyFc4
především	především	k9
v	v	k7c6
aplikacích	aplikace	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
podpora	podpora	k1gFnSc1
velení	velení	k1gNnSc2
a	a	k8xC
vojáků	voják	k1gMnPc2
v	v	k7c6
poli	pole	k1gNnSc6
</s>
<s>
doprava	doprava	k1gFnSc1
</s>
<s>
navádění	navádění	k1gNnSc1
zbraňových	zbraňový	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
vojenská	vojenský	k2eAgFnSc1d1
geodézie	geodézie	k1gFnSc1
a	a	k8xC
mapování	mapování	k1gNnSc1
</s>
<s>
přesný	přesný	k2eAgInSc4d1
čas	čas	k1gInSc4
(	(	kIx(
<g/>
<	<	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
ostatní	ostatní	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
(	(	kIx(
<g/>
především	především	k6eAd1
civilní	civilní	k2eAgInSc1d1
sektor	sektor	k1gInSc1
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaImF,k5eAaPmF
Standard	standard	k1gInSc4
Positioning	Positioning	k1gInSc1
Service	Service	k1gFnSc1
(	(	kIx(
<g/>
SPS	SPS	kA
<g/>
)	)	kIx)
a	a	k8xC
mají	mít	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
C	C	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
kód	kód	k1gInSc4
na	na	k7c6
frekvencích	frekvence	k1gFnPc6
L	L	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímače	přijímač	k1gInSc2
vyrobené	vyrobený	k2eAgFnPc4d1
v	v	k7c6
USA	USA	kA
nesmějí	smát	k5eNaImIp3nP
být	být	k5eAaImF
exportovány	exportován	k2eAgInPc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
nemají	mít	k5eNaImIp3nP
nastavená	nastavený	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
výšky	výška	k1gFnSc2
do	do	k7c2
18	#num#	k4
km	km	kA
(	(	kIx(
<g/>
60	#num#	k4
000	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
a	a	k8xC
rychlosti	rychlost	k1gFnSc2
do	do	k7c2
515	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
000	#num#	k4
knots	knotsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc4
limity	limit	k1gInPc4
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
prevence	prevence	k1gFnSc2
možného	možný	k2eAgNnSc2d1
zneužití	zneužití	k1gNnSc2
jako	jako	k8xC,k8xS
systému	systém	k1gInSc2
orientace	orientace	k1gFnSc2
v	v	k7c6
prostoru	prostor	k1gInSc6
ve	v	k7c6
zbraních	zbraň	k1gFnPc6
obdobných	obdobný	k2eAgFnPc2d1
balistickým	balistický	k2eAgFnPc3d1
raketám	raketa	k1gFnPc3
nebo	nebo	k8xC
střelám	střela	k1gFnPc3
s	s	k7c7
plochou	plochý	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickými	typický	k2eAgFnPc7d1
profesemi	profes	k1gFnPc7
a	a	k8xC
odvětvími	odvětví	k1gNnPc7
civilních	civilní	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
doprava	doprava	k1gFnSc1
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
letectví	letectví	k1gNnSc1
<g/>
,	,	kIx,
námořnictvo	námořnictvo	k1gNnSc1
<g/>
,	,	kIx,
kosmické	kosmický	k2eAgInPc4d1
lety	let	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
geologie	geologie	k1gFnSc1
a	a	k8xC
geofyzika	geofyzika	k1gFnSc1
</s>
<s>
geodézie	geodézie	k1gFnPc4
a	a	k8xC
geografické	geografický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
</s>
<s>
archeologie	archeologie	k1gFnSc1
</s>
<s>
lesnictví	lesnictví	k1gNnSc1
a	a	k8xC
zemědělství	zemědělství	k1gNnSc1
</s>
<s>
turistika	turistika	k1gFnSc1
a	a	k8xC
zábava	zábava	k1gFnSc1
</s>
<s>
přesný	přesný	k2eAgInSc4d1
čas	čas	k1gInSc4
(	(	kIx(
<g/>
<	<	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
Rádiové	rádiový	k2eAgInPc1d1
signály	signál	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Rádiové	rádiový	k2eAgMnPc4d1
signály	signál	k1gInPc4
GPS	GPS	kA
<g/>
.	.	kIx.
</s>
<s>
Družice	družice	k1gFnPc1
vysílají	vysílat	k5eAaImIp3nP
v	v	k7c6
pásmech	pásmo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
zvolena	zvolit	k5eAaPmNgNnP
záměrně	záměrně	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
minimálně	minimálně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
meteorologickými	meteorologický	k2eAgInPc7d1
vlivy	vliv	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Systému	systém	k1gInSc2
GPS	GPS	kA
je	být	k5eAaImIp3nS
přiděleno	přidělit	k5eAaPmNgNnS
5	#num#	k4
frekvencí	frekvence	k1gFnPc2
a	a	k8xC
každé	každý	k3xTgNnSc4
frekvenci	frekvence	k1gFnSc4
odpovídá	odpovídat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
vysílací	vysílací	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
L1	L1	k4
(	(	kIx(
<g/>
1575,42	1575,42	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
vysílán	vysílán	k2eAgMnSc1d1
C	C	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
kód	kód	k1gInSc4
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
pro	pro	k7c4
civilní	civilní	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
šířen	šířen	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
kód	kód	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
šifrovaný	šifrovaný	k2eAgInSc1d1
a	a	k8xC
přístupný	přístupný	k2eAgInSc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
autorizované	autorizovaný	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družice	družice	k1gFnSc1
bloku	blok	k1gInSc2
IIR-M	IIR-M	k1gFnSc1
a	a	k8xC
novější	nový	k2eAgFnPc1d2
jsou	být	k5eAaImIp3nP
připraveny	připraven	k2eAgFnPc1d1
vysílat	vysílat	k5eAaImF
vojenský	vojenský	k2eAgInSc4d1
M	M	kA
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
L2	L2	k4
(	(	kIx(
<g/>
1227,62	1227,62	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
vysílán	vysílán	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
kód	kód	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Družice	družice	k1gFnSc1
bloku	blok	k1gInSc2
IIR-M	IIR-M	k1gFnSc1
a	a	k8xC
novější	nový	k2eAgFnPc1d2
jsou	být	k5eAaImIp3nP
připraveny	připraven	k2eAgFnPc1d1
vysílat	vysílat	k5eAaImF
vojenský	vojenský	k2eAgInSc4d1
M	M	kA
kód	kód	k1gInSc4
a	a	k8xC
civilní	civilní	k2eAgInSc1d1
C	C	kA
kód	kód	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
L3	L3	k4
(	(	kIx(
<g/>
1381,05	1381,05	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
od	od	k7c2
bloku	blok	k1gInSc2
družic	družice	k1gFnPc2
IIR	IIR	kA
vysílá	vysílat	k5eAaImIp3nS
signály	signál	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
obsahují	obsahovat	k5eAaImIp3nP
data	datum	k1gNnPc4
monitorování	monitorování	k1gNnSc3
startů	start	k1gInPc2
balistických	balistický	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
<g/>
,	,	kIx,
detekci	detekce	k1gFnSc4
jaderných	jaderný	k2eAgInPc2d1
výbuchů	výbuch	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
vysokoenergetických	vysokoenergetický	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
náleží	náležet	k5eAaImIp3nS
k	k	k7c3
The	The	k1gFnSc3
United	United	k1gMnSc1
States	States	k1gMnSc1
Nuclear	Nuclear	k1gMnSc1
Detonation	Detonation	k1gInSc1
(	(	kIx(
<g/>
NUDET	NUDET	kA
<g/>
)	)	kIx)
a	a	k8xC
United	United	k1gInSc4
States	States	k1gInSc1
Nuclear	Nuclear	k1gInSc1
Detonation	Detonation	k1gInSc1
Detection	Detection	k1gInSc1
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
USNDS	USNDS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
L4	L4	k4
(	(	kIx(
<g/>
1379.913	1379.913	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
měření	měření	k1gNnSc4
ionosférické	ionosférický	k2eAgFnSc2d1
refrakce	refrakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průchod	průchod	k1gInSc1
signálu	signál	k1gInSc2
ionosférou	ionosféra	k1gFnSc7
způsobuje	způsobovat	k5eAaImIp3nS
zpoždění	zpoždění	k1gNnSc1
rádiového	rádiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
promítá	promítat	k5eAaImIp3nS
do	do	k7c2
chyb	chyba	k1gFnPc2
při	při	k7c6
určení	určení	k1gNnSc6
polohy	poloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc4
ionosférické	ionosférický	k2eAgNnSc4d1
zpoždění	zpoždění	k1gNnSc4
lze	lze	k6eAd1
eliminovat	eliminovat	k5eAaBmF
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
měříme	měřit	k5eAaImIp1nP
zpoždění	zpoždění	k1gNnSc4
na	na	k7c6
dvou	dva	k4xCgInPc6
kmitočtech	kmitočet	k1gInPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
získáním	získání	k1gNnSc7
korekcí	korekce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
L5	L5	k4
(	(	kIx(
<g/>
1176,45	1176,45	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
plánuje	plánovat	k5eAaImIp3nS
jako	jako	k9
civilní	civilní	k2eAgFnSc1d1
Safety-of-life	Safety-of-lif	k1gInSc5
(	(	kIx(
<g/>
SoL	sol	k1gNnPc6
<g/>
)	)	kIx)
signál	signál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
frekvence	frekvence	k1gFnSc1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
mezinárodně	mezinárodně	k6eAd1
chráněné	chráněný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
letecké	letecký	k2eAgFnSc2d1
navigace	navigace	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	on	k3xPp3gFnPc4
malé	malý	k2eAgFnPc4d1
nebo	nebo	k8xC
žádné	žádný	k3yNgNnSc4
rušení	rušení	k1gNnSc4
za	za	k7c2
všech	všecek	k3xTgFnPc2
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vypuštěním	vypuštění	k1gNnSc7
první	první	k4xOgFnSc2
družice	družice	k1gFnSc2
bloku	blok	k1gInSc2
IIF	IIF	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
poskytovat	poskytovat	k5eAaImF
tento	tento	k3xDgInSc1
signál	signál	k1gInSc1
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
na	na	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
trh	trh	k1gInSc4
uveden	uvést	k5eAaPmNgInS
první	první	k4xOgInSc1
dvoupásmový	dvoupásmový	k2eAgInSc1d1
čip	čip	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pomocí	pomocí	k7c2
signálu	signál	k1gInSc2
L5	L5	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
zpřesnit	zpřesnit	k5eAaPmF
pozici	pozice	k1gFnSc4
z	z	k7c2
původních	původní	k2eAgInPc2d1
5	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c4
30	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
telefon	telefon	k1gInSc1
jej	on	k3xPp3gInSc4
využívající	využívající	k2eAgInSc4d1
byl	být	k5eAaImAgInS
Xiaomi	Xiao	k1gFnPc7
Mi	já	k3xPp1nSc3
8	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Určování	určování	k1gNnSc1
polohy	poloha	k1gFnSc2
a	a	k8xC
času	čas	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vztažné	vztažný	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
charakteristiku	charakteristika	k1gFnSc4
Země	zem	k1gFnSc2
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
vztažné	vztažný	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
geoid	geoid	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
ale	ale	k9
pro	pro	k7c4
matematický	matematický	k2eAgInSc4d1
popis	popis	k1gInSc4
nevhodný	vhodný	k2eNgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
používáme	používat	k5eAaImIp1nP
jeho	jeho	k3xOp3gFnSc4
aproximaci	aproximace	k1gFnSc4
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
–	–	k?
koule	koule	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
druhého	druhý	k4xOgInSc2
stupně	stupeň	k1gInSc2
–	–	k?
elipsoid	elipsoid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
uživatelů	uživatel	k1gMnPc2
GPS	GPS	kA
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
užívaný	užívaný	k2eAgInSc1d1
geografický	geografický	k2eAgInSc1d1
referenční	referenční	k2eAgInSc1d1
systém	systém	k1gInSc1
WGS	WGS	kA
84	#num#	k4
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
pod	pod	k7c7
kódem	kód	k1gInSc7
EPSG	EPSG	kA
<g/>
:	:	kIx,
<g/>
4326	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
geodetického	geodetický	k2eAgNnSc2d1
data	datum	k1gNnSc2
<g/>
:	:	kIx,
elipsoid	elipsoid	k1gInSc1
s	s	k7c7
poloosami	poloosa	k1gFnPc7
přibližně	přibližně	k6eAd1
6	#num#	k4
378	#num#	k4
a	a	k8xC
6	#num#	k4
356	#num#	k4
km	km	kA
s	s	k7c7
počátkem	počátek	k1gInSc7
ve	v	k7c6
středu	střed	k1gInSc6
Země	zem	k1gFnSc2
</s>
<s>
systému	systém	k1gInSc2
zeměpisných	zeměpisný	k2eAgFnPc2d1
souřadnic	souřadnice	k1gFnPc2
(	(	kIx(
<g/>
zeměpisná	zeměpisný	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
a	a	k8xC
délka	délka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pro	pro	k7c4
výpočty	výpočet	k1gInPc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
geocentrický	geocentrický	k2eAgInSc1d1
referenční	referenční	k2eAgInSc1d1
systém	systém	k1gInSc1
WGS	WGS	kA
84	#num#	k4
se	s	k7c7
shodným	shodný	k2eAgNnSc7d1
datem	datum	k1gNnSc7
ale	ale	k8xC
s	s	k7c7
kartézskými	kartézský	k2eAgFnPc7d1
souřadnicemi	souřadnice	k1gFnPc7
v	v	k7c6
systému	systém	k1gInSc6
ECEF	ECEF	kA
(	(	kIx(
<g/>
Earth-Centered	Earth-Centered	k1gMnSc1
<g/>
,	,	kIx,
Earth-Fixed	Earth-Fixed	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
GPS	GPS	kA
čas	čas	k1gInSc1
je	být	k5eAaImIp3nS
měřen	měřit	k5eAaImNgInS
na	na	k7c4
týdny	týden	k1gInPc4
(	(	kIx(
<g/>
week	week	k1gInSc4
<g/>
)	)	kIx)
s	s	k7c7
maximem	maximum	k1gNnSc7
1024	#num#	k4
<g/>
,	,	kIx,
díky	díky	k7c3
čemu	co	k3yRnSc3,k3yQnSc3,k3yInSc3
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
vynulování	vynulování	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
naposledy	naposledy	k6eAd1
pro	pro	k7c4
7	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
časová	časový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
je	být	k5eAaImIp3nS
pořadí	pořadí	k1gNnSc4
podrámce	podrámec	k1gMnSc2
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nabývá	nabývat	k5eAaImIp3nS
hodnot	hodnota	k1gFnPc2
s	s	k7c7
maximem	maxim	k1gInSc7
100	#num#	k4
800	#num#	k4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
slova	slovo	k1gNnSc2
podrámce	podrámec	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
datové	datový	k2eAgInPc1d1
bity	bit	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
délku	délka	k1gFnSc4
0,02	0,02	k4
s.	s.	k?
Poslední	poslední	k2eAgInSc4d1
podrobný	podrobný	k2eAgInSc4d1
časový	časový	k2eAgInSc4d1
otisk	otisk	k1gInSc4
je	být	k5eAaImIp3nS
samotný	samotný	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
<g/>
/	/	kIx~
<g/>
A	a	k8xC
kód	kód	k1gInSc4
rozděluje	rozdělovat	k5eAaImIp3nS
čas	čas	k1gInSc4
po	po	k7c6
bitech	bit	k1gInPc6
dlouhých	dlouhý	k2eAgInPc6d1
~	~	kIx~
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
s	s	k7c7
a	a	k8xC
P	P	kA
kód	kód	k1gInSc1
na	na	k7c6
~	~	kIx~
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
s.	s.	k?
Porovnáním	porovnání	k1gNnSc7
vzestupných	vzestupný	k2eAgFnPc2d1
a	a	k8xC
sestupných	sestupný	k2eAgFnPc2d1
hran	hrana	k1gFnPc2
PRN	PRN	kA
kódů	kód	k1gInPc2
modulovaných	modulovaný	k2eAgInPc2d1
na	na	k7c4
nosnou	nosný	k2eAgFnSc4d1
s	s	k7c7
frekvencí	frekvence	k1gFnSc7
nosné	nosný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
může	moct	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
elektrotechnika	elektrotechnika	k1gFnSc1
změřit	změřit	k5eAaPmF
rozdíl	rozdíl	k1gInSc4
až	až	k9
na	na	k7c4
tisíciny	tisícina	k1gFnPc4
času	čas	k1gInSc2
bitu	bit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
předpokladu	předpoklad	k1gInSc2
přesnosti	přesnost	k1gFnSc2
1	#num#	k4
%	%	kIx~
bitu	bit	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
přibližně	přibližně	k6eAd1
10	#num#	k4
ns	ns	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
s	s	k7c7
<g/>
)	)	kIx)
pro	pro	k7c4
C	C	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
kód	kód	k1gInSc4
a	a	k8xC
1	#num#	k4
ns	ns	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
s	s	k7c7
<g/>
)	)	kIx)
pro	pro	k7c4
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
signál	signál	k1gInSc1
GPS	GPS	kA
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
blízkou	blízký	k2eAgFnSc7d1
rychlosti	rychlost	k1gFnSc3
světla	světlo	k1gNnSc2
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
krok	krok	k1gInSc1
měření	měření	k1gNnSc2
při	při	k7c6
1	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
bitu	bit	k1gInSc2
řádově	řádově	k6eAd1
~	~	kIx~
<g/>
3	#num#	k4
m	m	kA
u	u	k7c2
C	C	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
P	P	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
~	~	kIx~
<g/>
0,3	0,3	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
je	být	k5eAaImIp3nS
definována	definován	k2eAgFnSc1d1
299	#num#	k4
792	#num#	k4
458	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Odeslaný	odeslaný	k2eAgInSc1d1
signál	signál	k1gInSc1
má	mít	k5eAaImIp3nS
při	při	k7c6
přijetí	přijetí	k1gNnSc6
zpoždění	zpoždění	k1gNnSc2
mezi	mezi	k7c7
67	#num#	k4
ms	ms	k?
při	při	k7c6
elevaci	elevace	k1gFnSc6
družice	družice	k1gFnSc1
90	#num#	k4
<g/>
°	°	k?
a	a	k8xC
86	#num#	k4
ms	ms	k?
při	při	k7c6
elevaci	elevace	k1gFnSc6
0	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s>
Přesnost	přesnost	k1gFnSc1
měření	měření	k1gNnSc2
</s>
<s>
Přesnost	přesnost	k1gFnSc1
výpočtu	výpočet	k1gInSc2
polohy	poloha	k1gFnSc2
přijímače	přijímač	k1gInSc2
podléhá	podléhat	k5eAaImIp3nS
vlivům	vliv	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
vnáší	vnášet	k5eAaImIp3nP
do	do	k7c2
výpočtu	výpočet	k1gInSc2
chyby	chyba	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
náhodné	náhodný	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
chyby	chyba	k1gFnSc2
popisujeme	popisovat	k5eAaImIp1nP
statistickým	statistický	k2eAgInSc7d1
parametrem	parametr	k1gInSc7
efektivní	efektivní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
chyby	chyba	k1gFnSc2
(	(	kIx(
<g/>
RMS	RMS	kA
<g/>
,	,	kIx,
Root	Root	k2eAgInSc1d1
Mean	Mean	k1gInSc1
Square	square	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
odmocnina	odmocnina	k1gFnSc1
z	z	k7c2
průměru	průměr	k1gInSc2
kvadrátu	kvadrát	k1gInSc2
chyby	chyba	k1gFnSc2
</s>
<s>
R	R	kA
</s>
<s>
M	M	kA
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
c	c	k0
</s>
<s>
h	h	k?
</s>
<s>
y	y	k?
</s>
<s>
b	b	k?
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
E	E	kA
</s>
<s>
(	(	kIx(
</s>
<s>
X	X	kA
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
RMS	RMS	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
chyba	chyba	k1gFnSc1
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}}}	}}}}	k?
<g/>
=	=	kIx~
<g/>
E	E	kA
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Přesnost	přesnost	k1gFnSc1
výpočtu	výpočet	k1gInSc2
polohy	poloha	k1gFnSc2
kódového	kódový	k2eAgNnSc2d1
měření	měření	k1gNnSc2
ovlivňují	ovlivňovat	k5eAaImIp3nP
zejména	zejména	k9
<g/>
:	:	kIx,
</s>
<s>
Dílčí	dílčí	k2eAgInPc4d1
parametry	parametr	k1gInPc4
RMS	RMS	kA
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PříčinaVelikost	PříčinaVelikost	k1gFnSc1
RMS	RMS	kA
při	při	k7c6
GDOP	GDOP	kA
<g/>
=	=	kIx~
<g/>
1	#num#	k4
</s>
<s>
Efemeridy	efemerida	k1gFnPc1
družic	družice	k1gFnPc2
<g/>
±	±	k?
2,1	2,1	k4
<g/>
m	m	kA
</s>
<s>
Družicové	družicový	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
<g/>
±	±	k?
2,1	2,1	k4
<g/>
m	m	kA
</s>
<s>
Ionosférická	ionosférický	k2eAgFnSc1d1
refrakce	refrakce	k1gFnSc1
<g/>
±	±	k?
4,0	4,0	k4
<g/>
m	m	kA
</s>
<s>
Troposférické	troposférický	k2eAgFnSc2d1
refrakce	refrakce	k1gFnSc2
<g/>
±	±	k?
0,7	0,7	k4
<g/>
m	m	kA
</s>
<s>
Vícecestné	Vícecestný	k2eAgNnSc1d1
šíření	šíření	k1gNnSc1
signálu	signál	k1gInSc2
<g/>
±	±	k?
1,4	1,4	k4
<g/>
m	m	kA
</s>
<s>
Přijímač	přijímač	k1gInSc1
<g/>
±	±	k?
0,5	0,5	k4
<g/>
m	m	kA
</s>
<s>
Efemeridy	efemerida	k1gFnPc1
</s>
<s>
Efemeridy	efemerida	k1gFnPc1
jsou	být	k5eAaImIp3nP
predikované	predikovaný	k2eAgFnPc1d1
polohy	poloha	k1gFnPc1
družic	družice	k1gFnPc2
na	na	k7c6
oběžných	oběžný	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
téměř	téměř	k6eAd1
kruhových	kruhový	k2eAgInPc6d1
<g/>
,	,	kIx,
mírně	mírně	k6eAd1
elipsovitých	elipsovitý	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
velkou	velký	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
a	a	k8xC
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnSc1
dráhy	dráha	k1gFnPc1
stabilní	stabilní	k2eAgFnPc1d1
a	a	k8xC
dobře	dobře	k6eAd1
matematicky	matematicky	k6eAd1
popsatelné	popsatelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	s	k7c7
vlivem	vliv	k1gInSc7
kolísání	kolísání	k1gNnSc2
tíhových	tíhový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Slunce	slunce	k1gNnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
a	a	k8xC
sluneční	sluneční	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
jejich	jejich	k3xOp3gFnSc1
dráha	dráha	k1gFnSc1
mírně	mírně	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Předpoklad	předpoklad	k1gInSc1
vývoje	vývoj	k1gInSc2
trajektorie	trajektorie	k1gFnSc2
je	být	k5eAaImIp3nS
popsán	popsat	k5eAaPmNgInS
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Družicové	družicový	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
a	a	k8xC
relativistické	relativistický	k2eAgInPc1d1
efekty	efekt	k1gInPc1
</s>
<s>
Graf	graf	k1gInSc1
vlivu	vliv	k1gInSc2
relativistických	relativistický	k2eAgInPc2d1
efektů	efekt	k1gInPc2
na	na	k7c4
čas	čas	k1gInSc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
gravitačního	gravitační	k2eAgNnSc2d1
pole	pole	k1gNnSc2
Země	zem	k1gFnSc2
a	a	k8xC
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
obvyklých	obvyklý	k2eAgFnPc2d1
u	u	k7c2
umělých	umělý	k2eAgFnPc2d1
družic	družice	k1gFnPc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Družice	družice	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
atomové	atomový	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
s	s	k7c7
rubidiovým	rubidiový	k2eAgInSc7d1
nebo	nebo	k8xC
cesiovým	cesiový	k2eAgInSc7d1
oscilátorem	oscilátor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
stabilní	stabilní	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
přesných	přesný	k2eAgFnPc2d1
a	a	k8xC
synchronních	synchronní	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
na	na	k7c6
všech	všecek	k3xTgFnPc6
družicích	družice	k1gFnPc6
i	i	k8xC
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Einsteinova	Einsteinův	k2eAgInSc2d1
principu	princip	k1gInSc2
relativity	relativita	k1gFnSc2
je	být	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
čas	čas	k1gInSc1
hodin	hodina	k1gFnPc2
družice	družice	k1gFnSc2
NAVSTAR	NAVSTAR	kA
GPS	GPS	kA
na	na	k7c4
orbitu	orbita	k1gFnSc4
nutno	nutno	k6eAd1
korigovat	korigovat	k5eAaBmF
na	na	k7c4
souřadnicový	souřadnicový	k2eAgInSc4d1
čas	čas	k1gInSc4
vztažený	vztažený	k2eAgInSc4d1
k	k	k7c3
Zemi	zem	k1gFnSc3
jako	jako	k8xS,k8xC
inerciálnímu	inerciální	k2eAgInSc3d1
referenčnímu	referenční	k2eAgInSc3d1
systému	systém	k1gInSc3
těmito	tento	k3xDgInPc7
efekty	efekt	k1gInPc7
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
pohybová	pohybový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
družice	družice	k1gFnSc1
(	(	kIx(
<g/>
Speciální	speciální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
hodiny	hodina	k1gFnPc1
se	se	k3xPyFc4
zpomalují	zpomalovat	k5eAaImIp3nP
o	o	k7c6
−	−	k?
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
%	%	kIx~
vůči	vůči	k7c3
pozemským	pozemský	k2eAgFnPc3d1
</s>
<s>
rozdílné	rozdílný	k2eAgInPc4d1
gravitační	gravitační	k2eAgInPc4d1
potenciály	potenciál	k1gInPc4
(	(	kIx(
<g/>
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
<g/>
)	)	kIx)
ve	v	k7c6
značné	značný	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
nad	nad	k7c7
Zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
intenzita	intenzita	k1gFnSc1
gravitačního	gravitační	k2eAgNnSc2d1
pole	pole	k1gNnSc2
klesá	klesat	k5eAaImIp3nS
s	s	k7c7
druhou	druhý	k4xOgFnSc7
mocninou	mocnina	k1gFnSc7
vzdálenosti	vzdálenost	k1gFnSc2
(	(	kIx(
<g/>
pro	pro	k7c4
orbit	orbita	k1gFnPc2
družic	družice	k1gFnPc2
16	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
hodiny	hodina	k1gFnPc1
se	se	k3xPyFc4
zrychlují	zrychlovat	k5eAaImIp3nP
+50	+50	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
Když	když	k8xS
oba	dva	k4xCgInPc1
efekty	efekt	k1gInPc1
zkombinujeme	zkombinovat	k5eAaPmIp1nP
je	on	k3xPp3gNnSc4
výsledkem	výsledek	k1gInSc7
+45,5	+45,5	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
%	%	kIx~
oproti	oproti	k7c3
pozemským	pozemský	k2eAgFnPc3d1
hodinám	hodina	k1gFnPc3
±	±	k?
<g/>
0	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řešení	řešení	k1gNnSc1
tohoto	tento	k3xDgInSc2
efektu	efekt	k1gInSc2
je	být	k5eAaImIp3nS
hardwarové	hardwarový	k2eAgNnSc4d1
nastavení	nastavení	k1gNnSc4
základní	základní	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
na	na	k7c4
10,229	10,229	k4
<g/>
99999543	#num#	k4
MHz	Mhz	kA
místo	místo	k1gNnSc1
očekávaných	očekávaný	k2eAgInPc2d1
a	a	k8xC
pozemských	pozemský	k2eAgInPc2d1
10,230	10,230	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
variabilní	variabilní	k2eAgFnSc1d1
korekce	korekce	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
navigační	navigační	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
řádově	řádově	k6eAd1
v	v	k7c6
desítkách	desítka	k1gFnPc6
nanosekund	nanosekunda	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
ns	ns	k?
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
výška	výška	k1gFnSc1
orbitu	orbita	k1gFnSc4
družice	družice	k1gFnSc2
je	být	k5eAaImIp3nS
proměnná	proměnná	k1gFnSc1
<g/>
,	,	kIx,
rozložení	rozložení	k1gNnSc1
gravitace	gravitace	k1gFnSc2
není	být	k5eNaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
a	a	k8xC
samotné	samotný	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
vykazují	vykazovat	k5eAaImIp3nP
odchylku	odchylka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ionosférická	ionosférický	k2eAgFnSc1d1
a	a	k8xC
troposférická	troposférický	k2eAgFnSc1d1
refrakce	refrakce	k1gFnSc1
</s>
<s>
Ukázka	ukázka	k1gFnSc1
modelu	model	k1gInSc2
ionosférické	ionosférický	k2eAgFnSc2d1
refrakce	refrakce	k1gFnSc2
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgFnSc1d1
vizualizace	vizualizace	k1gFnSc1
modelu	model	k1gInSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
JPL	JPL	kA
NASA	NASA	kA
<g/>
)	)	kIx)
</s>
<s>
Radiový	radiový	k2eAgInSc4d1
signál	signál	k1gInSc4
vysílaný	vysílaný	k2eAgInSc4d1
z	z	k7c2
družice	družice	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
500-20200	500-20200	k4
km	km	kA
od	od	k7c2
povrchu	povrch	k1gInSc2
šíří	šířit	k5eAaImIp3nP
téměř	téměř	k6eAd1
vakuem	vakuum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
50-500	50-500	k4
km	km	kA
nad	nad	k7c7
povrchem	povrch	k1gInSc7
země	zem	k1gFnSc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
volných	volný	k2eAgInPc2d1
elektronů	elektron	k1gInPc2
a	a	k8xC
iontů	ion	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
způsobují	způsobovat	k5eAaImIp3nP
refrakci	refrakce	k1gFnSc4
(	(	kIx(
<g/>
lom	lom	k1gInSc1
<g/>
)	)	kIx)
rádiového	rádiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
jeho	jeho	k3xOp3gFnSc4
delší	dlouhý	k2eAgFnSc4d2
dráhu	dráha	k1gFnSc4
a	a	k8xC
zpoždění	zpoždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
ionosféry	ionosféra	k1gFnSc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
aktuální	aktuální	k2eAgFnSc1d1
i	i	k8xC
cyklická	cyklický	k2eAgFnSc1d1
(	(	kIx(
<g/>
11	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
aktivita	aktivita	k1gFnSc1
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
globální	globální	k2eAgInPc4d1
meteorologické	meteorologický	k2eAgInPc4d1
vlivy	vliv	k1gInPc4
<g/>
,	,	kIx,
roční	roční	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
,	,	kIx,
fáze	fáze	k1gFnSc1
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přijímačích	přijímač	k1gInPc6
je	být	k5eAaImIp3nS
implementován	implementován	k2eAgInSc4d1
základní	základní	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tyto	tento	k3xDgFnPc4
změny	změna	k1gFnPc4
zohledňuje	zohledňovat	k5eAaImIp3nS
a	a	k8xC
navigační	navigační	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
vstupní	vstupní	k2eAgInPc4d1
parametry	parametr	k1gInPc4
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
výpočet	výpočet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výhodu	výhoda	k1gFnSc4
lze	lze	k6eAd1
modelovat	modelovat	k5eAaImF
pomocí	pomocí	k7c2
kulových	kulový	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
klíčovou	klíčový	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
v	v	k7c6
modelu	model	k1gInSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
TEC	TEC	k?
(	(	kIx(
<g/>
Total	totat	k5eAaImAgMnS
Electronic	Electronice	k1gFnPc2
Content	Content	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Analýzou	analýza	k1gFnSc7
signálu	signál	k1gInSc2
na	na	k7c6
různých	různý	k2eAgInPc6d1
kmitočtech	kmitočet	k1gInPc6
(	(	kIx(
<g/>
L	L	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
L	L	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
lze	lze	k6eAd1
tuto	tento	k3xDgFnSc4
chybu	chyba	k1gFnSc4
eliminovat	eliminovat	k5eAaBmF
<g/>
,	,	kIx,
protože	protože	k8xS
změna	změna	k1gFnSc1
rychlosti	rychlost	k1gFnSc2
rádiového	rádiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
ionosférou	ionosféra	k1gFnSc7
je	být	k5eAaImIp3nS
frekvenčně	frekvenčně	k6eAd1
závislá	závislý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
má	mít	k5eAaImIp3nS
troposféra	troposféra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
od	od	k7c2
0-15	0-15	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc4
stav	stav	k1gInSc4
ovlivňují	ovlivňovat	k5eAaImIp3nP
především	především	k9
lokální	lokální	k2eAgInPc4d1
meteorologické	meteorologický	k2eAgInPc4d1
vlivy	vliv	k1gInPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
<g/>
,	,	kIx,
tlak	tlak	k1gInSc1
<g/>
,	,	kIx,
vlhkost	vlhkost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
možnost	možnost	k1gFnSc4
predikce	predikce	k1gFnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
globální	globální	k2eAgInSc4d1
systém	systém	k1gInSc4
minimální	minimální	k2eAgInSc4d1
a	a	k8xC
eliminuje	eliminovat	k5eAaBmIp3nS
se	se	k3xPyFc4
diferenčními	diferenční	k2eAgInPc7d1
systémy	systém	k1gInPc7
nebo	nebo	k8xC
lokálními	lokální	k2eAgInPc7d1
modely	model	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Vícecestné	Vícecestný	k2eAgNnSc1d1
šíření	šíření	k1gNnSc1
signálu	signál	k1gInSc2
a	a	k8xC
přijímač	přijímač	k1gMnSc1
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
anténa	anténa	k1gFnSc1
přijímače	přijímač	k1gInSc2
částečně	částečně	k6eAd1
zastíněna	zastíněn	k2eAgFnSc1d1
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
odrazivé	odrazivý	k2eAgInPc4d1
materiály	materiál	k1gInPc4
existuje	existovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
přijímá	přijímat	k5eAaImIp3nS
také	také	k9
signály	signál	k1gInPc4
odražené	odražený	k2eAgInPc4d1
a	a	k8xC
tedy	tedy	k9
opožděné	opožděný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
této	tento	k3xDgFnSc2
chyby	chyba	k1gFnSc2
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
vlastnostech	vlastnost	k1gFnPc6
okolí	okolí	k1gNnSc2
a	a	k8xC
míře	míra	k1gFnSc6
zastínění	zastínění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
profesionálních	profesionální	k2eAgFnPc6d1
aparaturách	aparatura	k1gFnPc6
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
eliminovat	eliminovat	k5eAaBmF
vhodně	vhodně	k6eAd1
polarizovanou	polarizovaný	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
signál	signál	k1gInSc1
původně	původně	k6eAd1
polarizovaný	polarizovaný	k2eAgInSc1d1
pravotočivě	pravotočivě	k6eAd1
RHCP	RHCP	kA
po	po	k7c6
odrazu	odraz	k1gInSc6
mění	měnit	k5eAaImIp3nS
polarizaci	polarizace	k1gFnSc4
na	na	k7c4
levotočivý	levotočivý	k2eAgInSc4d1
LHCP	LHCP	kA
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
je	být	k5eAaImIp3nS
nastavení	nastavení	k1gNnSc1
elevační	elevační	k2eAgFnSc2d1
masky	maska	k1gFnSc2
na	na	k7c4
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
není	být	k5eNaImIp3nS
možný	možný	k2eAgInSc1d1
příjem	příjem	k1gInSc1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
filtrace	filtrace	k1gFnSc1
Narrow	Narrow	k1gFnSc2
correlator	correlator	k1gMnSc1
spacing	spacing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
přijímače	přijímač	k1gInSc2
dříve	dříve	k6eAd2
výrazně	výrazně	k6eAd1
ovlivňovala	ovlivňovat	k5eAaImAgFnS
měření	měření	k1gNnSc4
díky	díky	k7c3
malému	malý	k2eAgInSc3d1
počtu	počet	k1gInSc3
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
snížené	snížený	k2eAgFnSc2d1
přesnosti	přesnost	k1gFnSc2
u	u	k7c2
8	#num#	k4
<g/>
bitových	bitový	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
<g/>
,	,	kIx,
malé	malý	k2eAgFnSc2d1
citlivosti	citlivost	k1gFnSc2
na	na	k7c6
vstupu	vstup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
má	mít	k5eAaImIp3nS
vliv	vliv	k1gInSc4
především	především	k9
metodika	metodika	k1gFnSc1
výpočtu	výpočet	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
do	do	k7c2
algoritmu	algoritmus	k1gInSc2
vnášeny	vnášen	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
podle	podle	k7c2
způsobu	způsob	k1gInSc2
využití	využití	k1gNnSc2
přijímače	přijímač	k1gInSc2
(	(	kIx(
<g/>
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
letectví	letectví	k1gNnSc1
<g/>
,	,	kIx,
turistika	turistika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc4
antény	anténa	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
konstrukce	konstrukce	k1gFnSc2
a	a	k8xC
umístění	umístění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geometrické	geometrický	k2eAgNnSc1d1
rozmístění	rozmístění	k1gNnSc1
družic	družice	k1gFnPc2
</s>
<s>
Chybu	chyba	k1gFnSc4
měření	měření	k1gNnSc2
výrazně	výrazně	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
rozmístění	rozmístění	k1gNnSc1
družic	družice	k1gFnPc2
na	na	k7c6
hemisféře	hemisféra	k1gFnSc6
a	a	k8xC
obecně	obecně	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
DOP	DOP	kA
(	(	kIx(
<g/>
Dilution	Dilution	k1gInSc1
of	of	k?
Precision	Precision	k1gInSc1
<g/>
,	,	kIx,
rozptyl	rozptyl	k1gInSc1
přesnosti	přesnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnný	souhrnný	k2eAgInSc1d1
GDOP	GDOP	kA
z	z	k7c2
intervalu	interval	k1gInSc2
1-50	1-50	k4
nabývá	nabývat	k5eAaImIp3nS
v	v	k7c6
našich	náš	k3xOp1gFnPc6
zeměpisných	zeměpisný	k2eAgFnPc6d1
šířkách	šířka	k1gFnPc6
a	a	k8xC
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
hodnot	hodnota	k1gFnPc2
1-4	1-4	k4
a	a	k8xC
je	být	k5eAaImIp3nS
zastoupen	zastoupit	k5eAaPmNgInS
dílčími	dílčí	k2eAgFnPc7d1
DOP	DOP	kA
<g/>
:	:	kIx,
</s>
<s>
Horizontální	horizontální	k2eAgFnSc1d1
–	–	k?
HDOP	HDOP	kA
</s>
<s>
Vertikální	vertikální	k2eAgFnSc1d1
–	–	k?
VDOP	VDOP	kA
</s>
<s>
Prostorový	prostorový	k2eAgMnSc1d1
–	–	k?
PDOP	PDOP	kA
</s>
<s>
P	P	kA
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
=	=	kIx~
</s>
<s>
H	H	kA
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
V	v	k7c6
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
PDOP	PDOP	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gMnSc1
{	{	kIx(
<g/>
HDOP	HDOP	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
VDOP	VDOP	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Časový	časový	k2eAgMnSc1d1
–	–	k?
TDOP	TDOP	kA
</s>
<s>
Geometrický	geometrický	k2eAgMnSc1d1
–	–	k?
GDOP	GDOP	kA
</s>
<s>
G	G	kA
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
=	=	kIx~
</s>
<s>
T	T	kA
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
H	H	kA
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
V	v	k7c6
</s>
<s>
D	D	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
GDOP	GDOP	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gMnSc1
{	{	kIx(
<g/>
TDOP	TDOP	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
HDOP	HDOP	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
VDOP	VDOP	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Zatímco	zatímco	k8xS
hodnota	hodnota	k1gFnSc1
HDOP	HDOP	kA
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
se	s	k7c7
zeměpisnou	zeměpisný	k2eAgFnSc7d1
polohou	poloha	k1gFnSc7
jen	jen	k9
málo	málo	k4c4
<g/>
,	,	kIx,
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
hodnota	hodnota	k1gFnSc1
VDOP	VDOP	kA
se	s	k7c7
zeměpisnou	zeměpisný	k2eAgFnSc7d1
šířkou	šířka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zeměpisné	zeměpisný	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
±	±	k?
56	#num#	k4
<g/>
°	°	k?
dosahuje	dosahovat	k5eAaImIp3nS
svého	svůj	k3xOyFgNnSc2
minima	minimum	k1gNnSc2
a	a	k8xC
s	s	k7c7
dalším	další	k2eAgNnSc7d1
zvyšováním	zvyšování	k1gNnSc7
zeměpisné	zeměpisný	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
pak	pak	k6eAd1
výrazně	výrazně	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
nárůst	nárůst	k1gInSc4
chyby	chyba	k1gFnPc1
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
zeměpisných	zeměpisný	k2eAgFnPc6d1
šířkách	šířka	k1gFnPc6
je	být	k5eAaImIp3nS
způsoben	způsobit	k5eAaPmNgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
překročení	překročení	k1gNnSc6
zeměpisné	zeměpisný	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
inklinaci	inklinace	k1gFnSc4
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
družice	družice	k1gFnPc1
nedosahují	dosahovat	k5eNaImIp3nP
nadhlavníku	nadhlavník	k1gInSc3
a	a	k8xC
kulminují	kulminovat	k5eAaImIp3nP
ve	v	k7c6
stále	stále	k6eAd1
nižších	nízký	k2eAgFnPc6d2
elevacích	elevace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třídimenzionální	třídimenzionální	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
určení	určení	k1gNnSc2
polohy	poloha	k1gFnSc2
prakticky	prakticky	k6eAd1
sleduje	sledovat	k5eAaImIp3nS
průběh	průběh	k1gInSc4
dominantní	dominantní	k2eAgFnSc2d1
chyby	chyba	k1gFnSc2
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
průměrné	průměrný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
PDOP	PDOP	kA
=	=	kIx~
1,9	1,9	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
min	mina	k1gFnPc2
<g/>
(	(	kIx(
<g/>
PDOP	PDOP	kA
<g/>
)	)	kIx)
=	=	kIx~
1,35	1,35	k4
a	a	k8xC
max	max	kA
<g/>
(	(	kIx(
<g/>
PDOP	PDOP	kA
<g/>
)	)	kIx)
=	=	kIx~
3,6	3,6	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Selektivní	selektivní	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
(	(	kIx(
<g/>
Selective	Selectiv	k1gInSc5
Availability	Availabilita	k1gFnPc5
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
do	do	k7c2
C	C	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
kódu	kód	k1gInSc2
radiového	radiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
zanášena	zanášen	k2eAgFnSc1d1
umělá	umělý	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
opatření	opatření	k1gNnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Selective	Selectiv	k1gInSc5
Availability	Availabilita	k1gFnSc2
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
mělo	mít	k5eAaImAgNnS
zabránit	zabránit	k5eAaPmF
zneužití	zneužití	k1gNnSc2
např.	např.	kA
možnosti	možnost	k1gFnSc2
navádět	navádět	k5eAaImF
balistické	balistický	k2eAgFnPc4d1
rakety	raketa	k1gFnPc4
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
nepřesných	přesný	k2eNgFnPc2d1
efemerid	efemerida	k1gFnPc2
a	a	k8xC
časových	časový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
SA	SA	kA
způsobovalo	způsobovat	k5eAaImAgNnS
chybu	chyba	k1gFnSc4
45	#num#	k4
m	m	kA
horizontálně	horizontálně	k6eAd1
(	(	kIx(
<g/>
95	#num#	k4
<g/>
%	%	kIx~
RMS	RMS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Tuto	tento	k3xDgFnSc4
chybu	chyba	k1gFnSc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
výrazně	výrazně	k6eAd1
potlačit	potlačit	k5eAaPmF
diferenčním	diferenční	k2eAgNnSc7d1
měřením	měření	k1gNnSc7
nebo	nebo	k8xC
dlouhodobým	dlouhodobý	k2eAgNnSc7d1
statickým	statický	k2eAgNnSc7d1
měřením	měření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
USA	USA	kA
vyvinuly	vyvinout	k5eAaPmAgFnP
systém	systém	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
lokálně	lokálně	k6eAd1
rušit	rušit	k5eAaImF
signál	signál	k1gInSc4
GPS	GPS	kA
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
SA	SA	kA
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2000	#num#	k4
zrušeno	zrušit	k5eAaPmNgNnS
a	a	k8xC
přesnost	přesnost	k1gFnSc1
kódového	kódový	k2eAgNnSc2d1
měření	měření	k1gNnSc2
polohy	poloha	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
na	na	k7c4
první	první	k4xOgFnSc4
desítku	desítka	k1gFnSc4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
lokální	lokální	k2eAgNnSc1d1
rušení	rušení	k1gNnSc1
družicového	družicový	k2eAgInSc2d1
signálu	signál	k1gInSc2
GPS	GPS	kA
pozemním	pozemní	k2eAgInSc7d1
vysílačem	vysílač	k1gInSc7
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
např.	např.	kA
při	při	k7c6
cvičení	cvičení	k1gNnSc6
NATO	NATO	kA
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
a	a	k8xC
2015	#num#	k4
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Nějaký	nějaký	k3yIgInSc1
typ	typ	k1gInSc1
znepřesnění	znepřesnění	k1gNnSc2
signálu	signál	k1gInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
pozorováno	pozorovat	k5eAaImNgNnS
také	také	k9
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
v	v	k7c6
okolí	okolí	k1gNnSc6
Kremlu	Kreml	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sagnacovo	Sagnacův	k2eAgNnSc1d1
zakřivení	zakřivení	k1gNnSc1
</s>
<s>
Při	při	k7c6
sledování	sledování	k1gNnSc6
družic	družice	k1gFnPc2
musíme	muset	k5eAaImIp1nP
také	také	k9
kompenzovat	kompenzovat	k5eAaBmF
Sagnacův	Sagnacův	k2eAgInSc4d1
efekt	efekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časový	časový	k2eAgInSc1d1
referenční	referenční	k2eAgInSc1d1
rámec	rámec	k1gInSc1
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
pro	pro	k7c4
inerciální	inerciální	k2eAgInSc4d1
systém	systém	k1gInSc4
ECEF	ECEF	kA
(	(	kIx(
<g/>
Earth-centered	Earth-centered	k1gMnSc1
<g/>
,	,	kIx,
Earth-fixed	Earth-fixed	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
rotační	rotační	k2eAgInSc4d1
systém	systém	k1gInSc4
WGS	WGS	kA
84	#num#	k4
(	(	kIx(
<g/>
obvodová	obvodový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
Země	zem	k1gFnSc2
na	na	k7c6
rovníku	rovník	k1gInSc6
0,465	0,465	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přepočet	přepočet	k1gInSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
Lorentzovou	Lorentzový	k2eAgFnSc7d1
transformací	transformace	k1gFnSc7
a	a	k8xC
výsledné	výsledný	k2eAgFnPc1d1
korekce	korekce	k1gFnPc1
mají	mít	k5eAaImIp3nP
kladné	kladný	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
pro	pro	k7c4
družice	družice	k1gFnPc4
na	na	k7c4
východní	východní	k2eAgFnPc4d1
a	a	k8xC
záporné	záporný	k2eAgFnPc4d1
na	na	k7c6
západní	západní	k2eAgFnSc6d1
nebeské	nebeský	k2eAgFnSc6d1
hemisféře	hemisféra	k1gFnSc6
a	a	k8xC
pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
ve	v	k7c6
stovkách	stovka	k1gFnPc6
nanosekund	nanosekunda	k1gFnPc2
(	(	kIx(
<g/>
~	~	kIx~
<g/>
desítky	desítka	k1gFnPc4
metrů	metr	k1gInPc2
v	v	k7c6
pozici	pozice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zpřesňující	zpřesňující	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
zpřesnění	zpřesnění	k1gNnSc4
stávajícího	stávající	k2eAgNnSc2d1
přesnosti	přesnost	k1gFnSc2
GPS	GPS	kA
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
různé	různý	k2eAgFnPc1d1
(	(	kIx(
<g/>
komerční	komerční	k2eAgInPc4d1
<g/>
)	)	kIx)
systémy	systém	k1gInPc4
založené	založený	k2eAgInPc4d1
na	na	k7c6
pozemních	pozemní	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
jejich	jejich	k3xOp3gFnSc1
poloha	poloha	k1gFnSc1
a	a	k8xC
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
tuto	tento	k3xDgFnSc4
porovnávají	porovnávat	k5eAaImIp3nP
s	s	k7c7
polohou	poloha	k1gFnSc7
vypočtenou	vypočtený	k2eAgFnSc7d1
pomocí	pomocí	k7c2
signálu	signál	k1gInSc2
GPS	GPS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
jako	jako	k8xC,k8xS
korekce	korekce	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
zpřesnění	zpřesnění	k1gNnSc4
<g/>
)	)	kIx)
pro	pro	k7c4
poblíž	poblíž	k6eAd1
naměřené	naměřený	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgInPc4
systémy	systém	k1gInPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
WAAS	WAAS	kA
<g/>
,	,	kIx,
EGNOS	EGNOS	kA
<g/>
,	,	kIx,
DGPS	DGPS	kA
a	a	k8xC
QZSS	QZSS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Rapant	Rapant	k1gInSc1
s.	s.	k?
171	#num#	k4
<g/>
↑	↑	k?
http://www.lib.cas.cz/space.40/1980/032A.HTM	http://www.lib.cas.cz/space.40/1980/032A.HTM	k4
<g/>
↑	↑	k?
kowoma	kowoma	k1gNnSc1
<g/>
:	:	kIx,
History	Histor	k1gInPc1
of	of	k?
NAVSTAR	NAVSTAR	kA
GPS	GPS	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
20081	#num#	k4
2	#num#	k4
MOORE	MOORE	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
K.	K.	kA
Superaccurate	Superaccurat	k1gInSc5
GPS	GPS	kA
Chips	chips	k1gInSc1
Coming	Coming	k1gInSc1
to	ten	k3xDgNnSc1
Smartphones	Smartphones	k1gInSc1
in	in	k?
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Spectrum	Spectrum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-09-21	2017-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KILIÁN	Kilián	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
III	III	kA
bude	být	k5eAaImBp3nS
spuštěna	spuštěn	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2023	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přinese	přinést	k5eAaPmIp3nS
třikrát	třikrát	k6eAd1
lepší	dobrý	k2eAgFnSc1d2
přesnost	přesnost	k1gFnSc1
a	a	k8xC
vyšší	vysoký	k2eAgFnSc1d2
spolehlivostVíce	spolehlivostVit	k5eAaImSgFnP
na	na	k7c6
<g/>
:	:	kIx,
https://vtm.zive.cz/clanky/gps-iii-bude-spustena-v-roce-2023-prinese-trikrat-lepsi-presnost-a-vyssi-spolehlivost/sc-870-a-200334/default.aspx.	https://vtm.zive.cz/clanky/gps-iii-bude-spustena-v-roce-2023-prinese-trikrat-lepsi-presnost-a-vyssi-spolehlivost/sc-870-a-200334/default.aspx.	k4
VTM	VTM	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-21	2019-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Page	Pag	k1gFnPc4
FY	fy	kA
2008	#num#	k4
Presidential	Presidential	k1gInSc1
Budget	budget	k1gInSc4
Request	Request	k1gInSc4
for	forum	k1gNnPc2
National	National	k1gMnSc2
Security	Securita	k1gFnSc2
Space	Space	k1gMnSc2
Activities	Activitiesa	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
UNITED	UNITED	kA
STATES	STATES	kA
NAVAL	navalit	k5eAaPmRp2nS
OBSERVATORY	OBSERVATORY	kA
(	(	kIx(
<g/>
USNO	USNO	kA
<g/>
)	)	kIx)
-	-	kIx~
BLOCK	BLOCK	kA
II	II	kA
SATELLITE	SATELLITE	kA
INFORMATION	INFORMATION	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GPS	GPS	kA
constellation	constellation	k1gInSc1
status	status	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russian	Russiany	k1gInPc2
Space	Space	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
GPS	GPS	kA
Constelation	Constelation	k1gInSc1
Status	status	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NAVSTAR	NAVSTAR	kA
Navigation	Navigation	k1gInSc1
Center	centrum	k1gNnPc2
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
Doug	Doug	k1gInSc1
Louden	Loudna	k1gFnPc2
<g/>
:	:	kIx,
Navstar	Navstar	k1gInSc1
GPS	GPS	kA
Constellation	Constellation	k1gInSc1
Status	status	k1gInSc1
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
23	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2006	#num#	k4
<g/>
↑	↑	k?
Matthew	Matthew	k1gFnSc1
Smitham	Smitham	k1gInSc1
<g/>
:	:	kIx,
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
↑	↑	k?
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Current	Current	k1gMnSc1
and	and	k?
Future	Futur	k1gMnSc5
Satellite	Satellit	k1gInSc5
Generations	Generations	k1gInSc1
http://www.gps.gov/systems/gps/space/	http://www.gps.gov/systems/gps/space/	k?
<g/>
]	]	kIx)
↑	↑	k?
GPS	GPS	kA
III	III	kA
9	#num#	k4
and	and	k?
10	#num#	k4
procured	procured	k1gInSc1
<g/>
,	,	kIx,
targeting	targeting	k1gInSc1
2022	#num#	k4
launch	launcha	k1gFnPc2
October	Octobra	k1gFnPc2
3	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
↑	↑	k?
NavtechGPS	NavtechGPS	k1gMnSc1
<g/>
:	:	kIx,
GNSS	GNSS	kA
Facts	Facts	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
ILRS	ILRS	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
GLONASS-95	GLONASS-95	k1gFnSc1
Satellite	Satellit	k1gInSc5
Information	Information	k1gInSc4
<g/>
↑	↑	k?
Beutler	Beutler	k1gInSc1
G.	G.	kA
<g/>
:	:	kIx,
[	[	kIx(
<g/>
GPS	GPS	kA
and	and	k?
GNSS	GNSS	kA
from	from	k1gMnSc1
the	the	k?
International	International	k1gMnSc1
Geosciences	Geosciences	k1gMnSc1
Perspective	Perspectiv	k1gInSc5
http://pnt.gov/advisory/2008-03/beutler.pdf	http://pnt.gov/advisory/2008-03/beutler.pdf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
]	]	kIx)
<g/>
;	;	kIx,
ILRS	ILRS	kA
str	str	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
↑	↑	k?
http://www.nasa.gov/directorates/heo/scan/news_GPS_III_SLR_Implementation_Team_Honored_with_Award.html1	http://www.nasa.gov/directorates/heo/scan/news_GPS_III_SLR_Implementation_Team_Honored_with_Award.html1	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Analýza	analýza	k1gFnSc1
četnosti	četnost	k1gFnSc2
výskytu	výskyt	k1gInSc2
družic	družice	k1gFnPc2
GPS	GPS	kA
a	a	k8xC
GLONASS	GLONASS	kA
Trimble	Trimble	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Planning	Planning	k1gInSc1
Software	software	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
Glen	Glen	k1gInSc1
Gibbons	Gibbons	k1gInSc1
<g/>
:	:	kIx,
Lockheed	Lockheed	k1gInSc1
Martin	Martin	k1gMnSc1
Wins	Winsa	k1gFnPc2
GPS	GPS	kA
IIIA	IIIA	kA
Contract	Contract	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
in	in	k?
Inside	Insid	k1gInSc5
GNSS	GNSS	kA
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
květen	květen	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
Glen	Glen	k1gInSc1
Gibbons	Gibbons	k1gInSc1
<g/>
:	:	kIx,
Lockheed	Lockheed	k1gInSc1
Martin	Martin	k1gInSc1
Team	team	k1gInSc1
Completes	Completes	k1gInSc1
Requirements	Requirements	k1gInSc1
Review	Review	k1gFnPc2
for	forum	k1gNnPc2
GPS	GPS	kA
IIIB	IIIB	kA
Program	program	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
5	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
in	in	k?
Inside	Insid	k1gInSc5
GNSS	GNSS	kA
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2010	#num#	k4
<g/>
↑	↑	k?
Tom	Tom	k1gMnSc1
Creel	Creel	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
J.	J.	kA
Dorsey	Dorsea	k1gFnPc1
<g/>
,	,	kIx,
Philip	Philip	k1gInSc1
J.	J.	kA
Mendicki	Mendick	k1gFnSc2
and	and	k?
col	cola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
<g/>
,	,	kIx,
Improved	Improved	k1gMnSc1
GPS	GPS	kA
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
GPS	GPS	kA
World	World	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2006	#num#	k4
<g/>
↑	↑	k?
Navigační	navigační	k2eAgInSc1d1
systém	systém	k1gInSc1
GPS	GPS	kA
Archivováno	archivován	k2eAgNnSc4d1
22	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Šíma	Šíma	k1gMnSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2003	#num#	k4
<g/>
↑	↑	k?
NAVSTAR	NAVSTAR	kA
GPS	GPS	kA
Block	Blocko	k1gNnPc2
IIR	IIR	kA
Archivováno	archivován	k2eAgNnSc4d1
21	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Security	Securita	k1gFnSc2
Space	Space	k1gFnSc1
Road	Road	k1gInSc1
12	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1999	#num#	k4
<g/>
↑	↑	k?
Satellite	Satellit	k1gInSc5
integrity	integrita	k1gFnPc1
monitor	monitor	k1gInSc1
and	and	k?
alert	alert	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
Patent	patent	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2003	#num#	k4
<g/>
↑	↑	k?
Arms	Arms	k1gInSc1
Control	Control	k1gInSc1
Association	Association	k1gInSc4
<g/>
.	.	kIx.
<g/>
Missile	Missil	k1gMnSc5
Technology	technolog	k1gMnPc4
Control	Control	k1gInSc1
Regime	Regim	k1gInSc5
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
May	May	k1gMnSc1
2006	#num#	k4
<g/>
↑	↑	k?
Kosmický	kosmický	k2eAgInSc1d1
segment	segment	k1gInSc1
GPS	GPS	kA
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
budoucnost	budoucnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2005	#num#	k4
<g/>
↑	↑	k?
National	National	k1gMnSc1
Security	Securita	k1gFnSc2
Space	Space	k1gMnSc1
Road	Road	k1gMnSc1
Maps	Mapsa	k1gFnPc2
<g/>
:	:	kIx,
http://www.fas.org/spp/military/program/nssrm/initiatives/usnds.htm	http://www.fas.org/spp/military/program/nssrm/initiatives/usnds.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1999	#num#	k4
<g/>
↑	↑	k?
First	First	k1gMnSc1
GPS	GPS	kA
IIF	IIF	kA
Satellite	Satellit	k1gInSc5
Undergoes	Undergoes	k1gMnSc1
Environmental	Environmental	k1gMnSc1
Testing	Testing	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
World	Worlda	k1gFnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
first	first	k1gMnSc1
dual-frequency	dual-frequenca	k1gMnSc2
GNSS	GNSS	kA
smartphone	smartphon	k1gInSc5
hits	hits	k6eAd1
the	the	k?
market	market	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
EGSA	EGSA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-04	2018-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Uko	Uko	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
<g/>
:	:	kIx,
GPS	GPS	kA
navigace	navigace	k1gFnSc2
na	na	k7c4
FPGA	FPGA	kA
Archivováno	archivován	k2eAgNnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
ČVUT	ČVUT	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
dostupné	dostupný	k2eAgFnPc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GPS	GPS	kA
SPS	SPS	kA
Signal	Signal	k1gMnSc1
Specification	Specification	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
23	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
odstavec	odstavec	k1gInSc1
2.5	2.5	k4
<g/>
.1	.1	k4
Mathematical	Mathematical	k1gFnPc2
Constants	Constants	k1gInSc4
<g/>
↑	↑	k?
GPS	GPS	kA
Errors	Errors	k1gInSc1
&	&	k?
Estimating	Estimating	k1gInSc1
Your	Your	k1gMnSc1
Receiver	Receiver	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Accuracy	Accurac	k1gMnPc7
Archivováno	archivován	k2eAgNnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samuel	Samuela	k1gFnPc2
J.	J.	kA
Wormley	Wormlea	k1gFnSc2
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2007	#num#	k4
<g/>
↑	↑	k?
Kostelecký	Kostelecký	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
přednesy	přednes	k1gInPc1
Vyšší	vysoký	k2eAgFnSc2d2
geodézie	geodézie	k1gFnSc2
1	#num#	k4
Pohyb	pohyb	k1gInSc1
družic	družice	k1gFnPc2
GNSS	GNSS	kA
v	v	k7c6
reálném	reálný	k2eAgNnSc6d1
silovém	silový	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSV	FSV	kA
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
<g/>
↑	↑	k?
Tom	Tom	k1gMnSc1
Van	vana	k1gFnPc2
Flandern	Flandern	k1gMnSc1
<g/>
:	:	kIx,
What	What	k1gMnSc1
the	the	k?
GPS	GPS	kA
Tells	Tells	k1gInSc4
Us	Us	k1gFnSc2
about	about	k1gInSc4
Relativity	relativita	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Univ	Univa	k1gFnPc2
<g/>
.	.	kIx.
of	of	k?
Maryland	Maryland	k1gInSc1
&	&	k?
Meta	meta	k1gFnSc1
Research	Research	k1gInSc1
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
1999	#num#	k4
<g/>
↑	↑	k?
Wagner	Wagner	k1gMnSc1
V.	V.	kA
<g/>
:	:	kIx,
Přesnost	přesnost	k1gFnSc1
atomových	atomový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
GPS	GPS	kA
a	a	k8xC
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
20081	#num#	k4
2	#num#	k4
Kostelecký	Kostelecký	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
přednesy	přednes	k1gInPc1
Vyšší	vysoký	k2eAgFnSc2d2
geodézie	geodézie	k1gFnSc2
1	#num#	k4
Princip	princip	k1gInSc1
zpracování	zpracování	k1gNnSc2
měření	měření	k1gNnSc2
GPS	GPS	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSV	FSV	kA
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
<g/>
↑	↑	k?
How	How	k1gMnSc1
good	good	k1gMnSc1
is	is	k?
GPS	GPS	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
MachineChris	MachineChris	k1gFnPc2
Rizos	Rizos	k1gMnSc1
<g/>
,	,	kIx,
SNAP-UNSW	SNAP-UNSW	k1gMnSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
↑	↑	k?
Removal	Removal	k1gMnSc3
of	of	k?
GPS	GPS	kA
Selective	Selectiv	k1gInSc5
Availability	Availabilita	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
The	The	k1gMnSc6
Federal	Federal	k1gFnSc7
Geographic	Geographice	k1gInPc2
Data	datum	k1gNnSc2
Committee	Committe	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Selective	Selectiv	k1gInSc5
Availability	Availabilita	k1gFnPc1
National	National	k1gMnSc1
Executive	Executiv	k1gInSc5
Committee	Committee	k1gNnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
for	forum	k1gNnPc2
Space-Based	Space-Based	k1gMnSc1
PNT	PNT	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Comparison	Comparison	k1gInSc1
of	of	k?
Positions	Positions	k1gInSc1
With	With	k1gMnSc1
and	and	k?
Without	Without	k1gMnSc1
Selective	Selectiv	k1gInSc5
Availability	Availabilita	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
National	National	k1gMnSc2
Geodetic	Geodetice	k1gFnPc2
Survey	Survea	k1gMnSc2
<g/>
,	,	kIx,
NOAA	NOAA	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2000	#num#	k4
<g/>
↑	↑	k?
GPS	GPS	kA
to	ten	k3xDgNnSc1
be	be	k?
jammed	jammed	k1gInSc1
in	in	k?
Scotland	Scotland	k1gInSc1
during	during	k1gInSc1
Nato	nato	k6eAd1
war	war	k?
games	games	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc4
2.10	2.10	k4
<g/>
.2015	.2015	k4
<g/>
↑	↑	k?
https://www.yahoo.com/news/russians-seek-answers-central-moscow-gps-anomaly-091145932.html	https://www.yahoo.com/news/russians-seek-answers-central-moscow-gps-anomaly-091145932.html	k1gMnSc1
<g/>
↑	↑	k?
Ashby	Ashba	k1gFnSc2
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Relativity	relativita	k1gFnSc2
and	and	k?
GPS	GPS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physics	Physics	k1gInSc1
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2002	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japonsko	Japonsko	k1gNnSc1
buduje	budovat	k5eAaImIp3nS
navigační	navigační	k2eAgInSc4d1
systém	systém	k1gInSc4
s	s	k7c7
přesností	přesnost	k1gFnSc7
1	#num#	k4
cm	cm	kA
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Rapant	Rapant	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
Družicové	družicový	k2eAgInPc1d1
polohové	polohový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VŠB-TU	VŠB-TU	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
200	#num#	k4
str	str	kA
<g/>
.	.	kIx.
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
124	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
dostupné	dostupný	k2eAgInPc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Dana	Dana	k1gFnSc1
<g/>
,	,	kIx,
P.	P.	kA
H.	H.	kA
<g/>
:	:	kIx,
Global	globat	k5eAaImAgInS
Positioning	Positioning	k1gInSc1
System	Syst	k1gInSc7
Overview	Overview	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
dostupné	dostupný	k2eAgInPc1d1
on-line	on-lin	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Koukl	kouknout	k5eAaPmAgMnS
J.	J.	kA
<g/>
:	:	kIx,
Společné	společný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
měření	měření	k1gNnSc2
totální	totální	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
a	a	k8xC
GPS	GPS	kA
výtah	výtah	k1gInSc4
DP	DP	kA
na	na	k7c4
blogu	bloga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČVUT	ČVUT	kA
1999	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rádiové	rádiový	k2eAgInPc1d1
signály	signál	k1gInPc1
GPS	GPS	kA
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1
GPS	GPS	kA
</s>
<s>
Geocaching	Geocaching	k1gInSc1
</s>
<s>
Degree	Degree	k1gFnSc1
Confluence	Confluence	k1gFnSc2
Project	Projecta	k1gFnPc2
</s>
<s>
OpenStreetMap	OpenStreetMap	k1gMnSc1
</s>
<s>
GLONASS	GLONASS	kA
</s>
<s>
Galileo	Galilea	k1gFnSc5
</s>
<s>
Globální	globální	k2eAgInSc1d1
družicový	družicový	k2eAgInSc1d1
polohový	polohový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
GPS	GPS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
GPS	GPS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CZEPOS	CZEPOS	kA
Česká	český	k2eAgFnSc1d1
síť	síť	k1gFnSc1
referenčních	referenční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
GNSS	GNSS	kA
</s>
<s>
EUREF	EUREF	kA
Evropská	evropský	k2eAgFnSc1d1
síť	síť	k1gFnSc1
referenčních	referenční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
GNSS	GNSS	kA
</s>
<s>
GPS	GPS	kA
SPS	SPS	kA
Performance	performance	k1gFnSc1
Standard	standard	k1gInSc1
—	—	k?
Oficiální	oficiální	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
Standardní	standardní	k2eAgFnSc2d1
polohové	polohový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
SPS	SPS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GPS	GPS	kA
PPS	PPS	kA
Performance	performance	k1gFnSc1
Standard	standard	k1gInSc1
—	—	k?
Oficiální	oficiální	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
Přesné	přesný	k2eAgFnSc2d1
polohové	polohový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
PPS	PPS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Globální	globální	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
GNSS	GNSS	kA
<g/>
)	)	kIx)
Zastaralé	zastaralý	k2eAgFnSc2d1
</s>
<s>
Transit	transit	k1gInSc1
•	•	k?
PARUS	PARUS	kA
•	•	k?
CIKADA	CIKADA	kA
Provozované	provozovaný	k2eAgFnSc2d1
</s>
<s>
GPS	GPS	kA
•	•	k?
GLONASS	GLONASS	kA
•	•	k?
DORIS	DORIS	kA
Ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Galileo	Galilea	k1gFnSc5
•	•	k?
BeiDou	BeiDa	k1gMnSc7
•	•	k?
QZSS	QZSS	kA
•	•	k?
IRNSS	IRNSS	kA
Rozšiřující	rozšiřující	k2eAgFnSc1d1
</s>
<s>
EGNOS	EGNOS	kA
•	•	k?
WAAS	WAAS	kA
•	•	k?
MSAS	MSAS	kA
•	•	k?
GAGAN	GAGAN	kA
•	•	k?
CWAAS	CWAAS	kA
Technologie	technologie	k1gFnSc2
</s>
<s>
SBAS	SBAS	kA
<g/>
,	,	kIx,
LAAS	LAAS	kA
<g/>
,	,	kIx,
DGPS	DGPS	kA
</s>
<s>
Geografická	geografický	k2eAgNnPc1d1
témata	téma	k1gNnPc1
obory	obor	k1gInPc4
</s>
<s>
socioekonomická	socioekonomický	k2eAgFnSc1d1
</s>
<s>
behaviorální	behaviorální	k2eAgFnSc1d1
•	•	k?
demografie	demografie	k1gFnSc1
•	•	k?
ekonomická	ekonomický	k2eAgNnPc1d1
(	(	kIx(
<g/>
zemědělství	zemědělství	k1gNnSc1
•	•	k?
průmyslu	průmysl	k1gInSc2
•	•	k?
služeb	služba	k1gFnPc2
•	•	k?
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
historická	historický	k2eAgFnSc1d1
•	•	k?
kulturní	kulturní	k2eAgFnSc1d1
•	•	k?
politická	politický	k2eAgFnSc1d1
•	•	k?
regionální	regionální	k2eAgFnSc1d1
•	•	k?
regionálního	regionální	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
•	•	k?
sídel	sídlo	k1gNnPc2
•	•	k?
služeb	služba	k1gFnPc2
fyzická	fyzický	k2eAgFnSc1d1
</s>
<s>
biogeografie	biogeografie	k1gFnSc1
•	•	k?
environmentální	environmentální	k2eAgFnSc1d1
•	•	k?
geodézie	geodézie	k1gFnSc1
•	•	k?
geomorfologie	geomorfologie	k1gFnSc2
•	•	k?
glaciologie	glaciologie	k1gFnSc2
•	•	k?
hydrologie	hydrologie	k1gFnSc2
•	•	k?
klimatologie	klimatologie	k1gFnSc2
•	•	k?
krajinná	krajinný	k2eAgFnSc1d1
ekologie	ekologie	k1gFnSc1
•	•	k?
limnologie	limnologie	k1gFnSc1
•	•	k?
hydrografie	hydrografie	k1gFnSc1
(	(	kIx(
<g/>
limnografie	limnografie	k1gFnSc1
•	•	k?
potamografie	potamografie	k1gFnSc2
•	•	k?
oceánografie	oceánografie	k1gFnSc2
•	•	k?
glaciografie	glaciografie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
paleogeografie	paleogeografie	k1gFnSc1
•	•	k?
pedologie	pedologie	k1gFnSc1
(	(	kIx(
<g/>
pedogeografie	pedogeografie	k1gFnSc1
•	•	k?
pedogeneze	pedogeneze	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
pobřeží	pobřeží	k1gNnSc2
</s>
<s>
postupy	postup	k1gInPc1
</s>
<s>
analýza	analýza	k1gFnSc1
prostorových	prostorový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
•	•	k?
dálkový	dálkový	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
Země	zem	k1gFnSc2
•	•	k?
geografické	geografický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
GIS	gis	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
geostatistika	geostatistika	k1gFnSc1
•	•	k?
Global	globat	k5eAaImAgMnS
Positioning	Positioning	k1gInSc4
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
GPS	GPS	kA
<g/>
)	)	kIx)
•	•	k?
kartografie	kartografie	k1gFnSc2
•	•	k?
kvantitativní	kvantitativní	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
•	•	k?
kvalitativní	kvalitativní	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
geografie	geografie	k1gFnSc2
•	•	k?
geografové	geograf	k1gMnPc1
•	•	k?
dějiny	dějiny	k1gFnPc4
geografie	geografie	k1gFnSc2
•	•	k?
geografie	geografie	k1gFnSc2
podle	podle	k7c2
zemí	zem	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4216824-7	4216824-7	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12407	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
86003934	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
86003934	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
