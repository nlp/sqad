<s>
Miroslav	Miroslav	k1gMnSc1
Bažík	Bažík	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
BažíkOsobní	BažíkOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1967	#num#	k4
(	(	kIx(
<g/>
53	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
záložník	záložník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
1996	#num#	k4
<g/>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
161	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Bažík	Bažík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
slovenský	slovenský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
záložník	záložník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Duklu	Dukla	k1gFnSc4
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastoupil	nastoupit	k5eAaPmAgMnS
celkem	celkem	k6eAd1
ve	v	k7c6
161	#num#	k4
ligových	ligový	k2eAgInPc6d1
utkáních	utkání	k1gNnPc6
a	a	k8xC
dal	dát	k5eAaPmAgMnS
19	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Mars	Mars	k1gInSc1
superliga	superliga	k1gFnSc1
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Mars	Mars	k1gInSc1
superliga	superliga	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Dukla	Dukla	k1gFnSc1
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
CELKEM	celkem	k6eAd1
</s>
<s>
161	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JEŘÁBEK	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
a	a	k8xC
československý	československý	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
:	:	kIx,
lexikon	lexikon	k1gNnSc1
osobností	osobnost	k1gFnPc2
a	a	k8xC
klubů	klub	k1gInPc2
1906	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Player	Player	k1gInSc1
History	Histor	k1gInPc1
</s>
<s>
Worldfootball	Worldfootball	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
