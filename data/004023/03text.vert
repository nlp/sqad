<s>
Metabolit	metabolit	k1gInSc1	metabolit
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc4	produkt
látkové	látkový	k2eAgFnSc2d1	látková
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metabolity	metabolit	k1gInPc1	metabolit
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
malé	malý	k2eAgFnPc1d1	malá
molekuly	molekula	k1gFnPc1	molekula
nebo	nebo	k8xC	nebo
ligandy	liganda	k1gFnPc1	liganda
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
primární	primární	k2eAgInPc4d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgInPc4d1	sekundární
metabolity	metabolit	k1gInPc4	metabolit
<g/>
.	.	kIx.	.
</s>
