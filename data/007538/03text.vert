<s>
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
drak	drak	k1gMnSc1	drak
je	být	k5eAaImIp3nS	být
vycpaný	vycpaný	k2eAgMnSc1d1	vycpaný
krokodýl	krokodýl	k1gMnSc1	krokodýl
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
u	u	k7c2	u
stropu	strop	k1gInSc2	strop
průjezdu	průjezd	k1gInSc2	průjezd
Staré	Staré	k2eAgFnSc2d1	Staré
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
Brněnského	brněnský	k2eAgMnSc2d1	brněnský
draka	drak	k1gMnSc2	drak
není	být	k5eNaImIp3nS	být
věrohodně	věrohodně	k6eAd1	věrohodně
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Brna	Brno	k1gNnSc2	Brno
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
koluje	kolovat	k5eAaImIp3nS	kolovat
řada	řada	k1gFnSc1	řada
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedněch	jeden	k4xCgMnPc2	jeden
byl	být	k5eAaImAgMnS	být
krokodýl	krokodýl	k1gMnSc1	krokodýl
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
dovezen	dovezen	k2eAgInSc1d1	dovezen
již	již	k6eAd1	již
vycpaný	vycpaný	k2eAgInSc1d1	vycpaný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
žil	žíla	k1gFnPc2	žíla
původně	původně	k6eAd1	původně
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krokodýla	krokodýl	k1gMnSc4	krokodýl
daroval	darovat	k5eAaPmAgMnS	darovat
Brnu	Brno	k1gNnSc6	Brno
markrabě	markrabě	k1gMnSc1	markrabě
Matyáš	Matyáš	k1gMnSc1	Matyáš
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
byla	být	k5eAaImAgFnS	být
uložena	uložen	k2eAgFnSc1d1	uložena
do	do	k7c2	do
báně	báně	k1gFnSc2	báně
radniční	radniční	k2eAgFnSc2d1	radniční
věže	věž	k1gFnSc2	věž
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gMnSc1	drak
však	však	k9	však
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
účtů	účet	k1gInPc2	účet
z	z	k7c2	z
brněnského	brněnský	k2eAgInSc2d1	brněnský
archivu	archiv	k1gInSc2	archiv
byl	být	k5eAaImAgInS	být
restaurován	restaurován	k2eAgInSc1d1	restaurován
a	a	k8xC	a
odčervován	odčervován	k2eAgInSc1d1	odčervován
již	již	k6eAd1	již
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1578	[number]	k4	1578
<g/>
,	,	kIx,	,
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průjezdu	průjezd	k1gInSc6	průjezd
je	být	k5eAaImIp3nS	být
krokodýl	krokodýl	k1gMnSc1	krokodýl
zavěšen	zavěšen	k2eAgMnSc1d1	zavěšen
<g/>
,	,	kIx,	,
však	však	k9	však
na	na	k7c6	na
vstupenkách	vstupenka	k1gFnPc6	vstupenka
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
krokodýl	krokodýl	k1gMnSc1	krokodýl
byl	být	k5eAaImAgMnS	být
darem	dar	k1gInSc7	dar
tureckých	turecký	k2eAgMnPc2d1	turecký
poslů	posel	k1gMnPc2	posel
králi	král	k1gMnSc6	král
Matyášovi	Matyášův	k2eAgMnPc1d1	Matyášův
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
draka	drak	k1gMnSc4	drak
přivezli	přivézt	k5eAaPmAgMnP	přivézt
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
rytíři	rytíř	k1gMnPc1	rytíř
z	z	k7c2	z
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
pověsti	pověst	k1gFnSc2	pověst
narazili	narazit	k5eAaPmAgMnP	narazit
lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
draka	drak	k1gMnSc4	drak
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Draka	drak	k1gMnSc4	drak
ulovili	ulovit	k5eAaPmAgMnP	ulovit
<g/>
,	,	kIx,	,
zabili	zabít	k5eAaPmAgMnP	zabít
a	a	k8xC	a
vycpaného	vycpaný	k2eAgMnSc4d1	vycpaný
uložili	uložit	k5eAaPmAgMnP	uložit
na	na	k7c6	na
trutnovském	trutnovský	k2eAgInSc6d1	trutnovský
hradu	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
odehrát	odehrát	k5eAaPmF	odehrát
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1006	[number]	k4	1006
<g/>
.	.	kIx.	.
</s>
<s>
Trutnovští	trutnovský	k2eAgMnPc1d1	trutnovský
jej	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
věnovali	věnovat	k5eAaImAgMnP	věnovat
roku	rok	k1gInSc2	rok
1024	[number]	k4	1024
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
knížeti	kníže	k1gNnSc6wR	kníže
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
právo	právo	k1gNnSc4	právo
mít	mít	k5eAaImF	mít
pečeť	pečeť	k1gFnSc4	pečeť
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
draka	drak	k1gMnSc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Vlakový	vlakový	k2eAgMnSc1d1	vlakový
expres	expres	k2eAgMnSc1d1	expres
Brno-Praha	Brno-Praha	k1gMnSc1	Brno-Praha
Časopis-občasník	Časopisbčasník	k1gMnSc1	Časopis-občasník
vydávaný	vydávaný	k2eAgMnSc1d1	vydávaný
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Motocyklová	motocyklový	k2eAgFnSc1d1	motocyklová
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c6	na
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
okruhu	okruh	k1gInSc6	okruh
Bowlingová	Bowlingový	k2eAgFnSc1d1	Bowlingová
soutěž	soutěž	k1gFnSc1	soutěž
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
tým	tým	k1gInSc4	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Brno	Brno	k1gNnSc1	Brno
Alligators	Alligators	k1gInSc1	Alligators
Sportovní	sportovní	k2eAgInSc1d1	sportovní
baseballový	baseballový	k2eAgInSc1d1	baseballový
klub	klub	k1gInSc1	klub
Draci	drak	k1gMnPc1	drak
Brno	Brno	k1gNnSc1	Brno
Závody	závod	k1gInPc1	závod
dračích	dračí	k2eAgFnPc2d1	dračí
lodí	loď	k1gFnPc2	loď
Druh	druh	k1gMnSc1	druh
masitého	masitý	k2eAgNnSc2d1	masité
jídla	jídlo	k1gNnSc2	jídlo
(	(	kIx(	(
<g/>
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
Brněnského	brněnský	k2eAgMnSc4d1	brněnský
brabce	brabce	k?	brabce
<g/>
)	)	kIx)	)
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
soutěž	soutěž	k1gFnSc1	soutěž
stavitelů	stavitel	k1gMnPc2	stavitel
plastikových	plastikový	k2eAgInPc2d1	plastikový
modelů	model	k1gInPc2	model
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Druh	druh	k1gInSc4	druh
piva	pivo	k1gNnSc2	pivo
z	z	k7c2	z
pivovaru	pivovar	k1gInSc2	pivovar
Starobrno	Starobrno	k1gNnSc4	Starobrno
Týmový	týmový	k2eAgInSc1d1	týmový
orientační	orientační	k2eAgInSc1d1	orientační
běh	běh	k1gInSc1	běh
po	po	k7c6	po
brněnských	brněnský	k2eAgNnPc6d1	brněnské
pohostinstvích	pohostinství	k1gNnPc6	pohostinství
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brněnský	brněnský	k2eAgInSc4d1	brněnský
drak	drak	k1gInSc4	drak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
