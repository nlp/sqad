<s>
Ideálním	ideální	k2eAgInSc7d1	ideální
pohonem	pohon	k1gInSc7	pohon
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
závaží	závaží	k1gNnSc1	závaží
<g/>
,	,	kIx,	,
zavěšené	zavěšený	k2eAgNnSc1d1	zavěšené
buďto	buďto	k8xC	buďto
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
či	či	k8xC	či
struně	struna	k1gFnSc6	struna
<g/>
,	,	kIx,	,
navinuté	navinutý	k2eAgFnSc6d1	navinutá
na	na	k7c6	na
bubnu	buben	k1gInSc6	buben
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
na	na	k7c6	na
řetízku	řetízek	k1gInSc6	řetízek
<g/>
,	,	kIx,	,
zachyceném	zachycený	k2eAgNnSc6d1	zachycené
na	na	k7c6	na
řetězovém	řetězový	k2eAgNnSc6d1	řetězové
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
