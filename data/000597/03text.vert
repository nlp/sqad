<s>
Beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
stavební	stavební	k2eAgInSc4d1	stavební
materiál	materiál	k1gInSc4	materiál
sestávající	sestávající	k2eAgInPc4d1	sestávající
z	z	k7c2	z
pojiva	pojivo	k1gNnSc2	pojivo
<g/>
,	,	kIx,	,
plniva	plnivo	k1gNnSc2	plnivo
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
přísad	přísada	k1gFnPc2	přísada
a	a	k8xC	a
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zatuhnutí	zatuhnutí	k1gNnSc6	zatuhnutí
pojiva	pojivo	k1gNnSc2	pojivo
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
pevný	pevný	k2eAgInSc4d1	pevný
umělý	umělý	k2eAgInSc4d1	umělý
slepenec	slepenec	k1gInSc4	slepenec
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
druhem	druh	k1gInSc7	druh
betonu	beton	k1gInSc2	beton
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
cementový	cementový	k2eAgInSc1d1	cementový
beton	beton	k1gInSc1	beton
(	(	kIx(	(
<g/>
CB	CB	kA	CB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pojivem	pojivo	k1gNnSc7	pojivo
cement	cement	k1gInSc1	cement
a	a	k8xC	a
plnivem	plnivo	k1gNnSc7	plnivo
kamenivo	kamenivo	k1gNnSc1	kamenivo
<g/>
;	;	kIx,	;
dalším	další	k2eAgInSc7d1	další
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
často	často	k6eAd1	často
používaným	používaný	k2eAgMnSc7d1	používaný
druhem	druh	k1gMnSc7	druh
betonu	beton	k1gInSc2	beton
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
asfaltový	asfaltový	k2eAgInSc1d1	asfaltový
beton	beton	k1gInSc1	beton
(	(	kIx(	(
<g/>
AB	AB	kA	AB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
asfaltových	asfaltový	k2eAgFnPc2d1	asfaltová
vozovek	vozovka	k1gFnPc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
využití	využití	k1gNnSc2	využití
kompozitních	kompozitní	k2eAgInPc2d1	kompozitní
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
betonu	beton	k1gInSc2	beton
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
pojivo	pojivo	k1gNnSc4	pojivo
používal	používat	k5eAaImAgMnS	používat
jíl	jíl	k1gInSc4	jíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
pojivo	pojivo	k1gNnSc4	pojivo
užívala	užívat	k5eAaImAgFnS	užívat
sádra	sádra	k1gFnSc1	sádra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
hydraulického	hydraulický	k2eAgInSc2d1	hydraulický
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
podobného	podobný	k2eAgInSc2d1	podobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
s	s	k7c7	s
pojivy	pojivo	k1gNnPc7	pojivo
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hydraulických	hydraulický	k2eAgNnPc2d1	hydraulické
vápen	vápno	k1gNnPc2	vápno
<g/>
,	,	kIx,	,
přírodního	přírodní	k2eAgInSc2d1	přírodní
nebo	nebo	k8xC	nebo
portlandského	portlandský	k2eAgInSc2d1	portlandský
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
republikánského	republikánský	k2eAgNnSc2d1	republikánské
období	období	k1gNnSc2	období
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
pojiva	pojivo	k1gNnSc2	pojivo
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
sopečný	sopečný	k2eAgInSc1d1	sopečný
produkt	produkt	k1gInSc1	produkt
pucolán	pucolán	k1gInSc1	pucolán
-	-	kIx~	-
přírodní	přírodní	k2eAgInSc1d1	přírodní
hydraulický	hydraulický	k2eAgInSc1d1	hydraulický
cement	cement	k1gInSc1	cement
s	s	k7c7	s
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
pojiv	pojivo	k1gNnPc2	pojivo
umožnil	umožnit	k5eAaPmAgInS	umožnit
vybudování	vybudování	k1gNnSc4	vybudování
významných	významný	k2eAgFnPc2d1	významná
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
přístavních	přístavní	k2eAgFnPc2d1	přístavní
hrází	hráz	k1gFnPc2	hráz
<g/>
,	,	kIx,	,
akvaduktů	akvadukt	k1gInPc2	akvadukt
a	a	k8xC	a
mostů	most	k1gInPc2	most
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cementového	cementový	k2eAgInSc2d1	cementový
betonu	beton	k1gInSc2	beton
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
z	z	k7c2	z
nejmonumentálnějších	monumentální	k2eAgFnPc2d3	nejmonumentálnější
staveb	stavba	k1gFnPc2	stavba
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vynikajících	vynikající	k2eAgInPc2d1	vynikající
příkladů	příklad	k1gInPc2	příklad
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
ještě	ještě	k6eAd1	ještě
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Technologickým	technologický	k2eAgInSc7d1	technologický
zázrakem	zázrak	k1gInSc7	zázrak
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
obrovská	obrovský	k2eAgFnSc1d1	obrovská
monolitická	monolitický	k2eAgFnSc1d1	monolitická
kopule	kopule	k1gFnSc1	kopule
na	na	k7c6	na
Pantheonu	Pantheon	k1gInSc6	Pantheon
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
43,3	[number]	k4	43,3
m	m	kA	m
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
5000	[number]	k4	5000
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
technologií	technologie	k1gFnSc7	technologie
litého	litý	k2eAgInSc2d1	litý
betonu	beton	k1gInSc2	beton
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
118	[number]	k4	118
<g/>
-	-	kIx~	-
<g/>
125	[number]	k4	125
n.	n.	k?	n.
l.	l.	k?	l.
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kopule	kopule	k1gFnSc1	kopule
srovnatelné	srovnatelný	k2eAgFnSc2d1	srovnatelná
velikosti	velikost	k1gFnSc2	velikost
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
stavěny	stavit	k5eAaImNgInP	stavit
technologií	technologie	k1gFnSc7	technologie
kamenné	kamenný	k2eAgFnSc2d1	kamenná
klenby	klenba	k1gFnSc2	klenba
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všeobecně	všeobecně	k6eAd1	všeobecně
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
názoru	názor	k1gInSc2	názor
byla	být	k5eAaImAgFnS	být
znalost	znalost	k1gFnSc4	znalost
používání	používání	k1gNnSc2	používání
hydraulických	hydraulický	k2eAgNnPc2d1	hydraulické
pojiv	pojivo	k1gNnPc2	pojivo
ztracena	ztraceno	k1gNnSc2	ztraceno
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
znovuobjevena	znovuobjeven	k2eAgFnSc1d1	znovuobjeven
až	až	k9	až
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novověkými	novověký	k2eAgInPc7d1	novověký
pokusy	pokus	k1gInPc7	pokus
Smeatona	Smeaton	k1gMnSc2	Smeaton
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
překvapivé	překvapivý	k2eAgNnSc4d1	překvapivé
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
analýzy	analýza	k1gFnPc1	analýza
původního	původní	k2eAgNnSc2d1	původní
zdiva	zdivo	k1gNnSc2	zdivo
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
uskutečněné	uskutečněný	k2eAgFnSc6d1	uskutečněná
na	na	k7c6	na
VŠCHT	VŠCHT	kA	VŠCHT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
unikátní	unikátní	k2eAgInSc4d1	unikátní
příklad	příklad	k1gInSc4	příklad
pokračování	pokračování	k1gNnSc2	pokračování
antické	antický	k2eAgFnSc2d1	antická
tradice	tradice	k1gFnSc2	tradice
použití	použití	k1gNnSc2	použití
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
malt	malta	k1gFnPc2	malta
<g/>
/	/	kIx~	/
<g/>
betonů	beton	k1gInPc2	beton
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgNnSc7d1	hydraulické
pojivem	pojivo	k1gNnSc7	pojivo
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
středověké	středověký	k2eAgFnSc6d1	středověká
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Citace	citace	k1gFnPc1	citace
<g/>
:	:	kIx,	:
Kvalita	kvalita	k1gFnSc1	kvalita
a	a	k8xC	a
homogenita	homogenita	k1gFnSc1	homogenita
výplňového	výplňový	k2eAgNnSc2d1	výplňové
zdiva	zdivo	k1gNnSc2	zdivo
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
suroviny	surovina	k1gFnSc2	surovina
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nebyly	být	k5eNaImAgInP	být
získávány	získáván	k2eAgInPc1d1	získáván
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
pečlivě	pečlivě	k6eAd1	pečlivě
vybírány	vybírán	k2eAgInPc1d1	vybírán
a	a	k8xC	a
zpracovávány	zpracováván	k2eAgInPc1d1	zpracováván
osvědčenými	osvědčený	k2eAgFnPc7d1	osvědčená
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
dobře	dobře	k6eAd1	dobře
tajené	tajený	k2eAgNnSc4d1	tajené
znalostní	znalostní	k2eAgNnSc4d1	znalostní
zázemí	zázemí	k1gNnSc4	zázemí
středověkých	středověký	k2eAgFnPc2d1	středověká
stavebních	stavební	k2eAgFnPc2d1	stavební
hutí	huť	k1gFnPc2	huť
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
součástí	součást	k1gFnSc7	součást
strategie	strategie	k1gFnSc2	strategie
utajování	utajování	k1gNnSc2	utajování
skutečných	skutečný	k2eAgInPc2d1	skutečný
postupů	postup	k1gInPc2	postup
bylo	být	k5eAaImAgNnS	být
vytváření	vytváření	k1gNnSc1	vytváření
"	"	kIx"	"
<g/>
falešných	falešný	k2eAgMnPc2d1	falešný
<g/>
"	"	kIx"	"
veřejně	veřejně	k6eAd1	veřejně
přístupných	přístupný	k2eAgFnPc2d1	přístupná
receptur	receptura	k1gFnPc2	receptura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
organických	organický	k2eAgFnPc6d1	organická
přísadách	přísada	k1gFnPc6	přísada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
odvést	odvést	k5eAaPmF	odvést
pozornost	pozornost	k1gFnSc4	pozornost
případných	případný	k2eAgMnPc2d1	případný
napodobitelů	napodobitel	k1gMnPc2	napodobitel
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
výplňového	výplňový	k2eAgNnSc2d1	výplňové
zdiva	zdivo	k1gNnSc2	zdivo
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
nízká	nízký	k2eAgFnSc1d1	nízká
objemová	objemový	k2eAgFnSc1d1	objemová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
moderním	moderní	k2eAgInSc7d1	moderní
tzv.	tzv.	kA	tzv.
lehkým	lehký	k2eAgInPc3d1	lehký
konstrukčním	konstrukční	k2eAgInPc3d1	konstrukční
betonům	beton	k1gInPc3	beton
<g/>
.	.	kIx.	.
</s>
<s>
Zdivo	zdivo	k1gNnSc4	zdivo
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
parametry	parametr	k1gInPc7	parametr
tak	tak	k6eAd1	tak
celkovou	celkový	k2eAgFnSc4d1	celková
stavbu	stavba	k1gFnSc4	stavba
nejen	nejen	k6eAd1	nejen
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
nevystavuje	vystavovat	k5eNaImIp3nS	vystavovat
její	její	k3xOp3gFnSc4	její
vnější	vnější	k2eAgFnSc4d1	vnější
konstrukci	konstrukce	k1gFnSc4	konstrukce
(	(	kIx(	(
<g/>
lícní	lícní	k2eAgNnSc1d1	lícní
kvádrové	kvádrový	k2eAgNnSc1d1	kvádrové
zdivo	zdivo	k1gNnSc1	zdivo
<g/>
)	)	kIx)	)
nadbytečnému	nadbytečný	k2eAgNnSc3d1	nadbytečné
zatížení	zatížení	k1gNnSc3	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
portlandský	portlandský	k2eAgInSc1d1	portlandský
cement	cement	k1gInSc1	cement
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
britský	britský	k2eAgMnSc1d1	britský
inženýr	inženýr	k1gMnSc1	inženýr
John	John	k1gMnSc1	John
Smeaton	Smeaton	k1gInSc4	Smeaton
<g/>
.	.	kIx.	.
</s>
<s>
Míchačky	míchačka	k1gFnPc1	míchačka
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
míchání	míchání	k1gNnSc3	míchání
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Ruční	ruční	k2eAgNnSc1d1	ruční
míchání	míchání	k1gNnSc1	míchání
vápenné	vápenný	k2eAgFnSc2d1	vápenná
malty	malta	k1gFnSc2	malta
nebyl	být	k5eNaImAgInS	být
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
pomocné	pomocný	k2eAgFnSc2d1	pomocná
práce	práce	k1gFnSc2	práce
prováděly	provádět	k5eAaImAgFnP	provádět
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
levná	levný	k2eAgFnSc1d1	levná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
namíchán	namíchat	k5eAaPmNgInS	namíchat
v	v	k7c6	v
přesném	přesný	k2eAgInSc6d1	přesný
poměru	poměr	k1gInSc6	poměr
a	a	k8xC	a
ve	v	k7c6	v
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgFnSc6d1	stejná
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
např.	např.	kA	např.
u	u	k7c2	u
železobetonových	železobetonový	k2eAgInPc2d1	železobetonový
nosníků	nosník	k1gInPc2	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
tu	tu	k6eAd1	tu
i	i	k9	i
důvody	důvod	k1gInPc4	důvod
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
<g/>
:	:	kIx,	:
do	do	k7c2	do
ručně	ručně	k6eAd1	ručně
míchaného	míchaný	k2eAgInSc2d1	míchaný
betonu	beton	k1gInSc2	beton
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dávat	dávat	k5eAaImF	dávat
více	hodně	k6eAd2	hodně
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
nedokonalou	dokonalý	k2eNgFnSc7d1	nedokonalá
ruční	ruční	k2eAgFnSc7d1	ruční
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
výrok	výrok	k1gInSc1	výrok
polírů	polír	k1gMnPc2	polír
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
S	s	k7c7	s
betonem	beton	k1gInSc7	beton
jdou	jít	k5eAaImIp3nP	jít
dělat	dělat	k5eAaImF	dělat
největší	veliký	k2eAgNnPc4d3	veliký
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
poprvé	poprvé	k6eAd1	poprvé
zavedl	zavést	k5eAaPmAgMnS	zavést
lití	lití	k1gNnSc4	lití
betonu	beton	k1gInSc2	beton
do	do	k7c2	do
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
základů	základ	k1gInPc2	základ
stavby	stavba	k1gFnSc2	stavba
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
beton	beton	k1gInSc1	beton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
budovy	budova	k1gFnSc2	budova
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hydratace	hydratace	k1gFnSc2	hydratace
a	a	k8xC	a
tvrdnutí	tvrdnutí	k1gNnSc2	tvrdnutí
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
fyzikální	fyzikální	k2eAgMnSc1d1	fyzikální
a	a	k8xC	a
chemické	chemický	k2eAgInPc1d1	chemický
procesy	proces	k1gInPc1	proces
(	(	kIx(	(
<g/>
provázené	provázený	k2eAgNnSc1d1	provázené
uvolňováním	uvolňování	k1gNnSc7	uvolňování
tepla	teplo	k1gNnSc2	teplo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
beton	beton	k1gInSc1	beton
získává	získávat	k5eAaImIp3nS	získávat
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
chemická	chemický	k2eAgFnSc1d1	chemická
stabilita	stabilita	k1gFnSc1	stabilita
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
neztvrdne	ztvrdnout	k5eNaPmIp3nS	ztvrdnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyschne	vyschnout	k5eAaPmIp3nS	vyschnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
týdnů	týden	k1gInPc2	týden
vykrystalizuje	vykrystalizovat	k5eAaPmIp3nS	vykrystalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
začne	začít	k5eAaPmIp3nS	začít
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
namíchání	namíchání	k1gNnSc6	namíchání
<g/>
,	,	kIx,	,
a	a	k8xC	a
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
tepleji	teple	k6eAd2	teple
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
krystalizace	krystalizace	k1gFnSc1	krystalizace
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
panelárnách	panelárna	k1gFnPc6	panelárna
se	se	k3xPyFc4	se
beton	beton	k1gInSc1	beton
ohříval	ohřívat	k5eAaImAgInS	ohřívat
až	až	k9	až
na	na	k7c4	na
80	[number]	k4	80
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
nelze	lze	k6eNd1	lze
nijak	nijak	k6eAd1	nijak
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
například	například	k6eAd1	například
beton	beton	k1gInSc1	beton
v	v	k7c6	v
automixu	automix	k1gInSc6	automix
zbude	zbýt	k5eAaPmIp3nS	zbýt
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
krystalech	krystal	k1gInPc6	krystal
betonu	beton	k1gInSc2	beton
nesmí	smět	k5eNaImIp3nS	smět
zmrznout	zmrznout	k5eAaPmF	zmrznout
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
beton	beton	k1gInSc1	beton
zcela	zcela	k6eAd1	zcela
znehodnocen	znehodnotit	k5eAaPmNgInS	znehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
při	při	k7c6	při
tuhnutí	tuhnutí	k1gNnSc6	tuhnutí
není	být	k5eNaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
i	i	k9	i
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Prostý	prostý	k2eAgInSc1d1	prostý
beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
především	především	k9	především
vůči	vůči	k7c3	vůči
namáhání	namáhání	k1gNnPc4	namáhání
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
snese	snést	k5eAaPmIp3nS	snést
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc1d1	malé
tahové	tahový	k2eAgNnSc1d1	tahové
zatížení	zatížení	k1gNnSc1	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
beton	beton	k1gInSc1	beton
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
s	s	k7c7	s
železnou	železný	k2eAgFnSc7d1	železná
výztuží	výztuž	k1gFnSc7	výztuž
-	-	kIx~	-
vzniká	vznikat	k5eAaImIp3nS	vznikat
železobeton	železobeton	k1gInSc4	železobeton
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výztuž	výztuž	k1gFnSc1	výztuž
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
kabely	kabel	k1gInPc1	kabel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
napnou	napnout	k5eAaPmIp3nP	napnout
a	a	k8xC	a
vnáší	vnášet	k5eAaImIp3nP	vnášet
do	do	k7c2	do
betonu	beton	k1gInSc2	beton
tlak	tlak	k1gInSc1	tlak
-	-	kIx~	-
předpjatý	předpjatý	k2eAgInSc1d1	předpjatý
beton	beton	k1gInSc1	beton
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
přidat	přidat	k5eAaPmF	přidat
různá	různý	k2eAgNnPc1d1	různé
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
drátky	drátek	k1gInPc1	drátek
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
vláknobeton	vláknobeton	k1gInSc4	vláknobeton
či	či	k8xC	či
drátkobeton	drátkobeton	k1gInSc4	drátkobeton
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příměs	příměs	k1gFnSc1	příměs
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
i	i	k9	i
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
uhlíková	uhlíkový	k2eAgNnPc1d1	uhlíkové
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
tím	ten	k3xDgNnSc7	ten
pevnost	pevnost	k1gFnSc4	pevnost
betonu	beton	k1gInSc2	beton
ještě	ještě	k9	ještě
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vláknobeton	vláknobeton	k1gInSc1	vláknobeton
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
kovové	kovový	k2eAgFnSc2d1	kovová
výztuže	výztuž	k1gFnSc2	výztuž
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
konstrukční	konstrukční	k2eAgInSc4d1	konstrukční
beton	beton	k1gInSc4	beton
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vláknobetonu	vláknobeton	k1gInSc2	vláknobeton
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
staticky	staticky	k6eAd1	staticky
určit	určit	k5eAaPmF	určit
rozmístění	rozmístění	k1gNnSc4	rozmístění
<g/>
,	,	kIx,	,
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
orientaci	orientace	k1gFnSc4	orientace
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
drátků	drátek	k1gInPc2	drátek
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
výsledná	výsledný	k2eAgFnSc1d1	výsledná
konstrukce	konstrukce	k1gFnSc1	konstrukce
staticky	staticky	k6eAd1	staticky
spočítat	spočítat	k5eAaPmF	spočítat
<g/>
.	.	kIx.	.
</s>
<s>
Vláknobeton	Vláknobeton	k1gInSc1	Vláknobeton
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
konstrukce	konstrukce	k1gFnPc4	konstrukce
ležící	ležící	k2eAgFnPc4d1	ležící
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Lehčený	Lehčený	k2eAgInSc1d1	Lehčený
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
bylo	být	k5eAaImAgNnS	být
vylehčení	vylehčení	k1gNnSc1	vylehčení
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vytvořením	vytvoření	k1gNnSc7	vytvoření
pórů	pór	k1gInPc2	pór
do	do	k7c2	do
hmoty	hmota	k1gFnSc2	hmota
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
pórobeton	pórobeton	k1gInSc4	pórobeton
<g/>
.	.	kIx.	.
</s>
<s>
Pórobeton	pórobeton	k1gInSc1	pórobeton
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
svojí	svůj	k3xOyFgFnSc7	svůj
lehkostí	lehkost	k1gFnSc7	lehkost
<g/>
,	,	kIx,	,
dobrými	dobrý	k2eAgFnPc7d1	dobrá
tepelněizolačními	tepelněizolační	k2eAgFnPc7d1	tepelněizolační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
jednoduchým	jednoduchý	k2eAgNnSc7d1	jednoduché
použitím	použití	k1gNnSc7	použití
(	(	kIx(	(
<g/>
pórobetonové	pórobetonový	k2eAgFnSc2d1	pórobetonová
tvárnice	tvárnice	k1gFnSc2	tvárnice
lze	lze	k6eAd1	lze
řezat	řezat	k5eAaImF	řezat
speciální	speciální	k2eAgFnSc7d1	speciální
pilou	pila	k1gFnSc7	pila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pórobetonu	pórobeton	k1gInSc6	pórobeton
lze	lze	k6eAd1	lze
jako	jako	k9	jako
příměs	příměs	k1gFnSc4	příměs
využít	využít	k5eAaPmF	využít
mimo	mimo	k6eAd1	mimo
písku	písek	k1gInSc2	písek
také	také	k9	také
popílek	popílek	k1gInSc1	popílek
z	z	k7c2	z
uhelného	uhelný	k2eAgNnSc2d1	uhelné
spalování	spalování	k1gNnSc2	spalování
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
tvarovkami	tvarovka	k1gFnPc7	tvarovka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
luxferami	luxfera	k1gFnPc7	luxfera
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
sklobeton	sklobeton	k1gInSc4	sklobeton
<g/>
.	.	kIx.	.
</s>
<s>
Sklobetonem	sklobeton	k1gInSc7	sklobeton
je	být	k5eAaImIp3nS	být
též	též	k9	též
nazýván	nazývat	k5eAaImNgInS	nazývat
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
materiál	materiál	k1gInSc1	materiál
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
portlandského	portlandský	k2eAgInSc2d1	portlandský
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
skleněných	skleněný	k2eAgNnPc2d1	skleněné
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
anorganického	anorganický	k2eAgNnSc2d1	anorganické
plniva	plnivo	k1gNnSc2	plnivo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
přísad	přísada	k1gFnPc2	přísada
dle	dle	k7c2	dle
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
vlastností	vlastnost	k1gFnPc2	vlastnost
konečného	konečný	k2eAgInSc2d1	konečný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
betonu	beton	k1gInSc2	beton
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
ovlivňujícími	ovlivňující	k2eAgInPc7d1	ovlivňující
faktory	faktor	k1gInPc7	faktor
jsou	být	k5eAaImIp3nP	být
vlastnosti	vlastnost	k1gFnSc2	vlastnost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kameniva	kamenivo	k1gNnSc2	kamenivo
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
cementu	cement	k1gInSc2	cement
(	(	kIx(	(
<g/>
1	[number]	k4	1
:	:	kIx,	:
2	[number]	k4	2
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
za	za	k7c2	za
teplého	teplý	k2eAgNnSc2d1	teplé
počasí	počasí	k1gNnSc2	počasí
už	už	k6eAd1	už
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
téměř	téměř	k6eAd1	téměř
poloviční	poloviční	k2eAgFnSc1d1	poloviční
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
po	po	k7c6	po
28	[number]	k4	28
dnech	den	k1gInPc6	den
považuje	považovat	k5eAaImIp3nS	považovat
beton	beton	k1gInSc1	beton
za	za	k7c4	za
hotový	hotový	k2eAgInSc4d1	hotový
(	(	kIx(	(
<g/>
vyzrálý	vyzrálý	k2eAgInSc4d1	vyzrálý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
se	se	k3xPyFc4	se
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tzv.	tzv.	kA	tzv.
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
teplo	teplo	k1gNnSc1	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
betonáži	betonáž	k1gFnSc6	betonáž
přehrady	přehrada	k1gFnSc2	přehrada
v	v	k7c6	v
šíři	šíř	k1gFnSc6	šíř
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
by	by	kYmCp3nS	by
vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
beton	beton	k1gInSc1	beton
znehodnotila	znehodnotit	k5eAaPmAgFnS	znehodnotit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
do	do	k7c2	do
betonu	beton	k1gInSc2	beton
vkládají	vkládat	k5eAaImIp3nP	vkládat
ocelové	ocelový	k2eAgFnPc4d1	ocelová
roury	roura	k1gFnPc4	roura
např.	např.	kA	např.
půlmetrového	půlmetrový	k2eAgInSc2d1	půlmetrový
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
nepřetržitě	přetržitě	k6eNd1	přetržitě
proudí	proudit	k5eAaPmIp3nS	proudit
studená	studený	k2eAgFnSc1d1	studená
chladící	chladící	k2eAgFnSc1d1	chladící
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
roury	roura	k1gFnPc4	roura
pak	pak	k6eAd1	pak
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Betonáž	betonáž	k1gFnSc1	betonáž
takovýchto	takovýto	k3xDgInPc2	takovýto
masívů	masív	k1gInPc2	masív
probíhá	probíhat	k5eAaImIp3nS	probíhat
nepřetržitě	přetržitě	k6eNd1	přetržitě
dnem	den	k1gInSc7	den
i	i	k8xC	i
nocí	noc	k1gFnSc7	noc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
noční	noční	k2eAgFnSc6d1	noční
přestávce	přestávka	k1gFnSc6	přestávka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
již	již	k6eAd1	již
beton	beton	k1gInSc1	beton
nespojil	spojit	k5eNaPmAgInS	spojit
kvalitně	kvalitně	k6eAd1	kvalitně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
+5	+5	k4	+5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
krystalizace	krystalizace	k1gFnSc1	krystalizace
betonu	beton	k1gInSc2	beton
velmi	velmi	k6eAd1	velmi
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
teplota	teplota	k1gFnSc1	teplota
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Betony	beton	k1gInPc1	beton
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
značkou	značka	k1gFnSc7	značka
C	C	kA	C
následovanou	následovaný	k2eAgFnSc7d1	následovaná
dvěma	dva	k4xCgNnPc7	dva
čísly	číslo	k1gNnPc7	číslo
-	-	kIx~	-
válcovou	válcový	k2eAgFnSc7d1	válcová
pevností	pevnost	k1gFnSc7	pevnost
a	a	k8xC	a
krychelnou	krychelný	k2eAgFnSc7d1	krychelná
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc4d1	různá
metodiky	metodika	k1gFnPc4	metodika
měření	měření	k1gNnSc2	měření
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
)	)	kIx)	)
v	v	k7c6	v
MPa	MPa	k1gFnPc6	MPa
<g/>
,	,	kIx,	,
např.	např.	kA	např.
C	C	kA	C
<g/>
16	[number]	k4	16
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Přísady	přísada	k1gFnPc1	přísada
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
různé	různý	k2eAgFnPc1d1	různá
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
;	;	kIx,	;
příměsi	příměs	k1gFnPc1	příměs
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
kameniva	kamenivo	k1gNnSc2	kamenivo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
organické	organický	k2eAgFnPc1d1	organická
nebo	nebo	k8xC	nebo
anorganické	anorganický	k2eAgInPc1d1	anorganický
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
tekutiny	tekutina	k1gFnPc1	tekutina
i	i	k9	i
sypké	sypký	k2eAgFnPc1d1	sypká
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
záměsi	záměse	k1gFnSc3	záměse
<g/>
,	,	kIx,	,
aby	aby	k9	aby
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
pozměnily	pozměnit	k5eAaPmAgFnP	pozměnit
vlastnosti	vlastnost	k1gFnPc1	vlastnost
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
tvoří	tvořit	k5eAaImIp3nS	tvořit
maximálně	maximálně	k6eAd1	maximálně
5	[number]	k4	5
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
<g/>
:	:	kIx,	:
Zrychlovače	Zrychlovač	k1gInPc1	Zrychlovač
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
-	-	kIx~	-
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
hydrataci	hydratace	k1gFnSc4	hydratace
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
rychleji	rychle	k6eAd2	rychle
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
počáteční	počáteční	k2eAgFnSc3d1	počáteční
pevnosti	pevnost	k1gFnSc3	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
přimíchání	přimíchání	k1gNnSc1	přimíchání
vodního	vodní	k2eAgNnSc2d1	vodní
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
pak	pak	k6eAd1	pak
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
to	ten	k3xDgNnSc4	ten
napřed	napřed	k6eAd1	napřed
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
-	-	kIx~	-
tuhost	tuhost	k1gFnSc1	tuhost
nebo	nebo	k8xC	nebo
tvrdost	tvrdost	k1gFnSc1	tvrdost
betonu	beton	k1gInSc2	beton
s	s	k7c7	s
vodním	vodní	k2eAgNnSc7d1	vodní
sklem	sklo	k1gNnSc7	sklo
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
pouze	pouze	k6eAd1	pouze
technologická	technologický	k2eAgFnSc1d1	technologická
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
umožní	umožnit	k5eAaPmIp3nS	umožnit
s	s	k7c7	s
betonem	beton	k1gInSc7	beton
hned	hned	k6eAd1	hned
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
betonu	beton	k1gInSc2	beton
ale	ale	k8xC	ale
také	také	k9	také
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
čtyřtýdenní	čtyřtýdenní	k2eAgFnSc1d1	čtyřtýdenní
krystalizace	krystalizace	k1gFnSc1	krystalizace
do	do	k7c2	do
konečné	konečný	k2eAgFnSc2d1	konečná
tvrdosti	tvrdost	k1gFnSc2	tvrdost
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
mýlky	mýlka	k1gFnSc2	mýlka
<g/>
,	,	kIx,	,
že	že	k8xS	že
beton	beton	k1gInSc1	beton
s	s	k7c7	s
vodním	vodní	k2eAgNnSc7d1	vodní
sklem	sklo	k1gNnSc7	sklo
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
hotový	hotový	k2eAgInSc1d1	hotový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
havarijních	havarijní	k2eAgFnPc6d1	havarijní
opravách	oprava	k1gFnPc6	oprava
proti	proti	k7c3	proti
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
tažení	tažení	k1gNnSc1	tažení
římsy	římsa	k1gFnSc2	římsa
z	z	k7c2	z
cementové	cementový	k2eAgFnSc2d1	cementová
malty	malta	k1gFnSc2	malta
při	při	k7c6	při
5	[number]	k4	5
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
by	by	k9	by
tuhnutí	tuhnutí	k1gNnSc1	tuhnutí
vrstev	vrstva	k1gFnPc2	vrstva
malty	malta	k1gFnPc1	malta
trvalo	trvat	k5eAaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Přidá	přidat	k5eAaPmIp3nS	přidat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
vodní	vodní	k2eAgNnSc1d1	vodní
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
malta	malta	k1gFnSc1	malta
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
tuhost	tuhost	k1gFnSc1	tuhost
pak	pak	k6eAd1	pak
umožní	umožnit	k5eAaPmIp3nS	umožnit
i	i	k9	i
natažení	natažení	k1gNnSc4	natažení
jemné	jemný	k2eAgFnSc2d1	jemná
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Římsa	římsa	k1gFnSc1	římsa
je	být	k5eAaImIp3nS	být
hotová	hotový	k2eAgFnSc1d1	hotová
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ať	ať	k9	ať
si	se	k3xPyFc3	se
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
krystalizace	krystalizace	k1gFnSc2	krystalizace
<g/>
.	.	kIx.	.
</s>
<s>
Zpomalovače	zpomalovač	k1gInPc1	zpomalovač
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
-	-	kIx~	-
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
hydrataci	hydratace	k1gFnSc4	hydratace
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
déle	dlouho	k6eAd2	dlouho
zpracovatelný	zpracovatelný	k2eAgInSc1d1	zpracovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Provzdušňovací	provzdušňovací	k2eAgFnPc1d1	provzdušňovací
přísady	přísada	k1gFnPc1	přísada
-	-	kIx~	-
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
dutiny	dutina	k1gFnPc1	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Plastifikátory	plastifikátor	k1gInPc1	plastifikátor
-	-	kIx~	-
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
zpracovatelnost	zpracovatelnost	k1gFnSc4	zpracovatelnost
betonové	betonový	k2eAgFnSc2d1	betonová
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Protizmrazovací	Protizmrazovací	k2eAgFnPc1d1	Protizmrazovací
přísady	přísada	k1gFnPc1	přísada
-	-	kIx~	-
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
dřívější	dřívější	k2eAgNnSc4d1	dřívější
dosažení	dosažení	k1gNnSc4	dosažení
pevnosti	pevnost	k1gFnSc2	pevnost
nutné	nutný	k2eAgFnSc2d1	nutná
pro	pro	k7c4	pro
odolnost	odolnost	k1gFnSc4	odolnost
betonu	beton	k1gInSc2	beton
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
mrazu	mráz	k1gInSc2	mráz
na	na	k7c4	na
záměsovou	záměsový	k2eAgFnSc4d1	záměsová
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vodotěsnicí	Vodotěsnice	k1gFnSc7	Vodotěsnice
přísady	přísada	k1gFnSc2	přísada
-	-	kIx~	-
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
vodonepropustnost	vodonepropustnost	k1gFnSc1	vodonepropustnost
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
přísady	přísada	k1gFnPc1	přísada
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
póry	pór	k1gInPc4	pór
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
přimíchání	přimíchání	k1gNnSc1	přimíchání
mazlavého	mazlavý	k2eAgNnSc2d1	mazlavé
mýdla	mýdlo	k1gNnSc2	mýdlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jádrového	jádrový	k2eAgMnSc4d1	jádrový
-	-	kIx~	-
starý	starý	k2eAgInSc4d1	starý
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
už	už	k6eAd1	už
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc4	způsob
objevila	objevit	k5eAaPmAgFnS	objevit
praxe	praxe	k1gFnSc1	praxe
-	-	kIx~	-
nevědělo	vědět	k5eNaImAgNnS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
teoreticky	teoreticky	k6eAd1	teoreticky
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vodou	voda	k1gFnSc7	voda
nerozpustná	rozpustný	k2eNgNnPc1d1	nerozpustné
vápenatá	vápenatý	k2eAgNnPc1d1	vápenaté
mýdla	mýdlo	k1gNnPc1	mýdlo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
mýdlobetonu	mýdlobeton	k1gInSc2	mýdlobeton
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
běžný	běžný	k2eAgInSc1d1	běžný
poměr	poměr	k1gInSc1	poměr
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
štěrku	štěrk	k1gInSc2	štěrk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k1gNnSc1	místo
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc1d1	použit
roztok	roztok	k1gInSc1	roztok
mazlavého	mazlavý	k2eAgNnSc2d1	mazlavé
mýdla	mýdlo	k1gNnSc2	mýdlo
v	v	k7c6	v
hmotnostním	hmotnostní	k2eAgInSc6d1	hmotnostní
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
:	:	kIx,	:
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
1	[number]	k4	1
kg	kg	kA	kg
mazlavého	mazlavý	k2eAgNnSc2d1	mazlavé
mýdla	mýdlo	k1gNnSc2	mýdlo
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
dostupného	dostupný	k2eAgInSc2d1	dostupný
např.	např.	kA	např.
v	v	k7c4	v
drogerii	drogerie	k1gFnSc4	drogerie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
10	[number]	k4	10
litrech	litr	k1gInPc6	litr
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
koncentrát	koncentrát	k1gInSc1	koncentrát
vlije	vlít	k5eAaPmIp3nS	vlít
do	do	k7c2	do
sudu	sud	k1gInSc2	sud
s	s	k7c7	s
90	[number]	k4	90
litry	litr	k1gInPc7	litr
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
řádně	řádně	k6eAd1	řádně
promíchá	promíchat	k5eAaPmIp3nS	promíchat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mazlavé	mazlavý	k2eAgNnSc1d1	mazlavé
mýdlo	mýdlo	k1gNnSc1	mýdlo
dobře	dobře	k6eAd1	dobře
rozpustilo	rozpustit	k5eAaPmAgNnS	rozpustit
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
mýdlový	mýdlový	k2eAgInSc1d1	mýdlový
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
již	již	k6eAd1	již
použije	použít	k5eAaPmIp3nS	použít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
voda	voda	k1gFnSc1	voda
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
promíchat	promíchat	k5eAaPmF	promíchat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
zrníčkách	zrníčko	k1gNnPc6	zrníčko
písku	písek	k1gInSc2	písek
či	či	k8xC	či
štěrku	štěrk	k1gInSc2	štěrk
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
mýdlový	mýdlový	k2eAgInSc1d1	mýdlový
povlak	povlak	k1gInSc1	povlak
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
mýdlobeton	mýdlobeton	k1gInSc1	mýdlobeton
lze	lze	k6eAd1	lze
výborně	výborně	k6eAd1	výborně
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
rekonstrukcích	rekonstrukce	k1gFnPc6	rekonstrukce
starších	starý	k2eAgInPc2d2	starší
objektů	objekt	k1gInPc2	objekt
-	-	kIx~	-
zejména	zejména	k9	zejména
podlah	podlaha	k1gFnPc2	podlaha
nebo	nebo	k8xC	nebo
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
či	či	k8xC	či
podlahách	podlaha	k1gFnPc6	podlaha
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc4d2	menší
příležitost	příležitost	k1gFnSc4	příležitost
vzlínat	vzlínat	k5eAaImF	vzlínat
betonem	beton	k1gInSc7	beton
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
např.	např.	kA	např.
suchou	suchý	k2eAgFnSc7d1	suchá
podlahou	podlaha	k1gFnSc7	podlaha
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
asfaltové	asfaltový	k2eAgFnSc2d1	asfaltová
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Staří	starý	k2eAgMnPc1d1	starý
praktici	praktik	k1gMnPc1	praktik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
jako	jako	k8xC	jako
beton	beton	k1gInSc1	beton
bez	bez	k7c2	bez
přísady	přísada	k1gFnSc2	přísada
mýdla	mýdlo	k1gNnSc2	mýdlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrofobizační	Hydrofobizační	k2eAgFnPc1d1	Hydrofobizační
přísady	přísada	k1gFnPc1	přísada
-	-	kIx~	-
přísady	přísada	k1gFnPc1	přísada
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pórů	pór	k1gInPc2	pór
vodoodpuzující	vodoodpuzující	k2eAgInSc4d1	vodoodpuzující
povlak	povlak	k1gInSc4	povlak
a	a	k8xC	a
snižují	snižovat	k5eAaImIp3nP	snižovat
jejich	jejich	k3xOp3gFnSc4	jejich
propustnost	propustnost	k1gFnSc4	propustnost
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
Barviva	barvivo	k1gNnSc2	barvivo
-	-	kIx~	-
mění	měnit	k5eAaImIp3nS	měnit
barvu	barva	k1gFnSc4	barva
hotového	hotový	k2eAgInSc2d1	hotový
betonu	beton	k1gInSc2	beton
Beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
univerzálním	univerzální	k2eAgInSc7d1	univerzální
stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
na	na	k7c4	na
nosné	nosný	k2eAgFnPc4d1	nosná
konstrukce	konstrukce	k1gFnPc4	konstrukce
(	(	kIx(	(
<g/>
skelety	skelet	k1gInPc4	skelet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
panelů	panel	k1gInPc2	panel
<g/>
;	;	kIx,	;
v	v	k7c6	v
dopravním	dopravní	k2eAgNnSc6d1	dopravní
stavitelství	stavitelství	k1gNnSc6	stavitelství
je	být	k5eAaImIp3nS	být
beton	beton	k1gInSc4	beton
hlavním	hlavní	k2eAgInSc7d1	hlavní
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
vozovek	vozovka	k1gFnPc2	vozovka
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
;	;	kIx,	;
v	v	k7c6	v
podzemním	podzemní	k2eAgNnSc6d1	podzemní
stavitelství	stavitelství	k1gNnSc6	stavitelství
se	se	k3xPyFc4	se
beton	beton	k1gInSc1	beton
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
dočasná	dočasný	k2eAgFnSc1d1	dočasná
i	i	k8xC	i
trvalá	trvalý	k2eAgFnSc1d1	trvalá
výztuž	výztuž	k1gFnSc1	výztuž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
betonu	beton	k1gInSc2	beton
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnPc1d1	optimální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgInPc1d1	minimální
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
životnost	životnost	k1gFnSc1	životnost
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
tento	tento	k3xDgInSc4	tento
materiál	materiál	k1gInSc4	materiál
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
na	na	k7c6	na
dopravních	dopravní	k2eAgFnPc6d1	dopravní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
léty	léto	k1gNnPc7	léto
ověřených	ověřený	k2eAgFnPc2d1	ověřená
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
vymývaný	vymývaný	k2eAgInSc1d1	vymývaný
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
estetické	estetický	k2eAgFnPc4d1	estetická
a	a	k8xC	a
funkční	funkční	k2eAgFnPc4d1	funkční
plochy	plocha	k1gFnPc4	plocha
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
životností	životnost	k1gFnSc7	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobit	vyrobit	k5eAaPmF	vyrobit
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
beton	beton	k1gInSc4	beton
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
tak	tak	k6eAd1	tak
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
drobní	drobný	k2eAgMnPc1d1	drobný
stavebníci	stavebník	k1gMnPc1	stavebník
a	a	k8xC	a
kutilové	kutil	k1gMnPc1	kutil
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
praného	praný	k2eAgInSc2d1	praný
písku	písek	k1gInSc2	písek
frakce	frakce	k1gFnSc2	frakce
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
mm	mm	kA	mm
nebo	nebo	k8xC	nebo
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
mm	mm	kA	mm
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc1d1	správný
poměr	poměr	k1gInSc1	poměr
písku	písek	k1gInSc2	písek
k	k	k7c3	k
cementu	cement	k1gInSc3	cement
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
třetinu	třetina	k1gFnSc4	třetina
písku	písek	k1gInSc2	písek
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
kamenivem	kamenivo	k1gNnSc7	kamenivo
frakce	frakce	k1gFnSc2	frakce
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
mm	mm	kA	mm
<g/>
,	,	kIx,	,
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
pětinové	pětinový	k2eAgFnPc1d1	pětinová
úspory	úspora	k1gFnPc1	úspora
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
kvalitnější	kvalitní	k2eAgNnSc1d2	kvalitnější
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgFnPc1d1	čistá
vody	voda	k1gFnPc1	voda
(	(	kIx(	(
<g/>
ideálně	ideálně	k6eAd1	ideálně
pitné	pitný	k2eAgFnSc2d1	pitná
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
kvalita	kvalita	k1gFnSc1	kvalita
vhodnosti	vhodnost	k1gFnSc2	vhodnost
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ve	v	k7c6	v
spádové	spádový	k2eAgFnSc6d1	spádová
míchačce	míchačka	k1gFnSc6	míchačka
beton	beton	k1gInSc1	beton
převaloval	převalovat	k5eAaImAgInS	převalovat
a	a	k8xC	a
po	po	k7c6	po
vyklopení	vyklopení	k1gNnSc6	vyklopení
do	do	k7c2	do
kolečka	kolečko	k1gNnSc2	kolečko
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
kužel	kužel	k1gInSc1	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
konzistence	konzistence	k1gFnPc1	konzistence
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
míchačkách	míchačka	k1gFnPc6	míchačka
velice	velice	k6eAd1	velice
obtížně	obtížně	k6eAd1	obtížně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
přidat	přidat	k5eAaPmF	přidat
do	do	k7c2	do
betonové	betonový	k2eAgFnSc2d1	betonová
směsi	směs	k1gFnSc2	směs
i	i	k9	i
plastifikátor	plastifikátor	k1gInSc4	plastifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgNnSc1d1	ideální
je	být	k5eAaImIp3nS	být
zakoupit	zakoupit	k5eAaPmF	zakoupit
si	se	k3xPyFc3	se
nějaký	nějaký	k3yIgMnSc1	nějaký
ve	v	k7c6	v
stavebninách	stavebnina	k1gFnPc6	stavebnina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
nenáročných	náročný	k2eNgFnPc2d1	nenáročná
staveb	stavba	k1gFnPc2	stavba
postačí	postačit	k5eAaPmIp3nS	postačit
i	i	k9	i
polévková	polévkový	k2eAgFnSc1d1	polévková
lžíce	lžíce	k1gFnSc1	lžíce
saponátu	saponát	k1gInSc2	saponát
na	na	k7c4	na
nádobí	nádobí	k1gNnSc4	nádobí
na	na	k7c4	na
cca	cca	kA	cca
2	[number]	k4	2
kolečka	kolečko	k1gNnSc2	kolečko
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předávkování	předávkování	k1gNnSc6	předávkování
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
beton	beton	k1gInSc1	beton
znehodnotit	znehodnotit	k5eAaPmF	znehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
saponátem	saponát	k1gInSc7	saponát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
pracuje	pracovat	k5eAaImIp3nS	pracovat
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
s	s	k7c7	s
dříve	dříve	k6eAd2	dříve
používaným	používaný	k2eAgNnSc7d1	používané
mazlavým	mazlavý	k2eAgNnSc7d1	mazlavé
mýdlem	mýdlo	k1gNnSc7	mýdlo
<g/>
.	.	kIx.	.
</s>
<s>
Saponát	saponát	k1gInSc1	saponát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
se	s	k7c7	s
směsí	směs	k1gFnSc7	směs
promíchán	promíchat	k5eAaPmNgMnS	promíchat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
2	[number]	k4	2
minut	minuta	k1gFnPc2	minuta
navíc	navíc	k6eAd1	navíc
beton	beton	k1gInSc1	beton
provzdušní	provzdušnit	k5eAaPmIp3nS	provzdušnit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
odolnějším	odolný	k2eAgInSc7d2	odolnější
vůči	vůči	k7c3	vůči
nasáknutí	nasáknutý	k2eAgMnPc1d1	nasáknutý
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vůči	vůči	k7c3	vůči
mrazu	mráz	k1gInSc3	mráz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Úplně	úplně	k6eAd1	úplně
vodotěsný	vodotěsný	k2eAgInSc1d1	vodotěsný
beton	beton	k1gInSc1	beton
však	však	k9	však
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
písku	písek	k1gInSc2	písek
a	a	k8xC	a
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
domácích	domácí	k2eAgFnPc6d1	domácí
podmínkách	podmínka	k1gFnPc6	podmínka
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
kamenivo	kamenivo	k1gNnSc4	kamenivo
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
frakce	frakce	k1gFnSc1	frakce
16	[number]	k4	16
mm	mm	kA	mm
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
písek	písek	k1gInSc1	písek
neobsahoval	obsahovat	k5eNaImAgInS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
prachových	prachový	k2eAgFnPc2d1	prachová
odplavitelných	odplavitelný	k2eAgFnPc2d1	odplavitelný
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
beton	beton	k1gInSc1	beton
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
další	další	k2eAgFnPc4d1	další
ztekucující	ztekucující	k2eAgFnPc4d1	ztekucující
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
zpomalující	zpomalující	k2eAgFnPc1d1	zpomalující
přísady	přísada	k1gFnPc1	přísada
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
přidané	přidaný	k2eAgFnSc2d1	přidaná
při	při	k7c6	při
míchání	míchání	k1gNnSc6	míchání
betonu	beton	k1gInSc2	beton
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
praskání	praskání	k1gNnSc3	praskání
a	a	k8xC	a
k	k	k7c3	k
prašnosti	prašnost	k1gFnSc3	prašnost
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
drolit	drolit	k5eAaImF	drolit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
popsaných	popsaný	k2eAgInPc2d1	popsaný
důvodů	důvod	k1gInPc2	důvod
nelze	lze	k6eNd1	lze
vůbec	vůbec	k9	vůbec
doporučit	doporučit	k5eAaPmF	doporučit
ruční	ruční	k2eAgNnSc4d1	ruční
míchání	míchání	k1gNnSc4	míchání
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Tabulky	tabulka	k1gFnPc1	tabulka
pro	pro	k7c4	pro
míchání	míchání	k1gNnPc4	míchání
betonu	beton	k1gInSc2	beton
uvádí	uvádět	k5eAaImIp3nS	uvádět
hmotnosti	hmotnost	k1gFnPc1	hmotnost
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
štěrku	štěrk	k1gInSc2	štěrk
a	a	k8xC	a
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
domácí	domácí	k2eAgNnPc4d1	domácí
míchání	míchání	k1gNnPc4	míchání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
možnostech	možnost	k1gFnPc6	možnost
stavebníka	stavebník	k1gMnSc2	stavebník
lze	lze	k6eAd1	lze
užít	užít	k5eAaPmF	užít
jen	jen	k9	jen
míchání	míchání	k1gNnSc1	míchání
objemové	objemový	k2eAgNnSc1d1	objemové
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
poměrové	poměrový	k2eAgFnPc1d1	poměrová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odměřované	odměřovaný	k2eAgNnSc1d1	odměřované
na	na	k7c4	na
lopaty	lopata	k1gFnPc4	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
minimálního	minimální	k2eAgNnSc2d1	minimální
množství	množství	k1gNnSc2	množství
cementu	cement	k1gInSc2	cement
na	na	k7c4	na
1	[number]	k4	1
m3	m3	k4	m3
písku	písek	k1gInSc2	písek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
písku	písek	k1gInSc2	písek
se	s	k7c7	s
štěrkem	štěrk	k1gInSc7	štěrk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
:	:	kIx,	:
2	[number]	k4	2
<g/>
..	..	k?	..
<g/>
.600	.600	k4	.600
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
3	[number]	k4	3
<g/>
..	..	k?	..
<g/>
.400	.400	k4	.400
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
4	[number]	k4	4
<g/>
..	..	k?	..
<g/>
.300	.300	k4	.300
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
5	[number]	k4	5
<g/>
..	..	k?	..
<g />
.	.	kIx.	.
</s>
<s>
<g/>
.240	.240	k4	.240
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
6	[number]	k4	6
<g/>
..	..	k?	..
<g/>
.200	.200	k4	.200
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
7	[number]	k4	7
<g/>
..	..	k?	..
<g/>
.170	.170	k4	.170
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
1	[number]	k4	1
:	:	kIx,	:
8	[number]	k4	8
<g/>
..	..	k?	..
<g/>
.150	.150	k4	.150
kg	kg	kA	kg
cementu	cement	k1gInSc2	cement
Záleží	záležet	k5eAaImIp3nS	záležet
však	však	k9	však
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
beton	beton	k1gInSc1	beton
udusat	udusat	k5eAaPmF	udusat
<g/>
,	,	kIx,	,
spotřeba	spotřeba	k1gFnSc1	spotřeba
cementu	cement	k1gInSc2	cement
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
o	o	k7c4	o
50	[number]	k4	50
kg	kg	kA	kg
na	na	k7c4	na
m3	m3	k4	m3
větší	veliký	k2eAgInPc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
betonu	beton	k1gInSc2	beton
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximální	maximální	k2eAgNnSc4d1	maximální
množství	množství	k1gNnSc4	množství
cementu	cement	k1gInSc2	cement
na	na	k7c4	na
m3	m3	k4	m3
písku	písek	k1gInSc2	písek
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
600	[number]	k4	600
kg	kg	kA	kg
<g/>
,	,	kIx,	,
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
se	se	k3xPyFc4	se
už	už	k9	už
pevnost	pevnost	k1gFnSc1	pevnost
betonu	beton	k1gInSc2	beton
dále	daleko	k6eAd2	daleko
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Betony	beton	k1gInPc1	beton
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
:	:	kIx,	:
6	[number]	k4	6
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jsou	být	k5eAaImIp3nP	být
betony	beton	k1gInPc1	beton
hubené	hubený	k2eAgInPc1d1	hubený
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
použijeme	použít	k5eAaPmIp1nP	použít
jako	jako	k9	jako
podklad	podklad	k1gInSc4	podklad
pod	pod	k7c4	pod
dlažbu	dlažba	k1gFnSc4	dlažba
ve	v	k7c6	v
vjezdu	vjezd	k1gInSc6	vjezd
pro	pro	k7c4	pro
osobní	osobní	k2eAgNnSc4d1	osobní
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
cca	cca	kA	cca
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
pod	pod	k7c4	pod
dlažbu	dlažba	k1gFnSc4	dlažba
chodníků	chodník	k1gInPc2	chodník
a	a	k8xC	a
vjezdů	vjezd	k1gInPc2	vjezd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
uložené	uložený	k2eAgFnSc2d1	uložená
kabely	kabela	k1gFnSc2	kabela
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
betony	beton	k1gInPc1	beton
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vykopat	vykopat	k5eAaPmF	vykopat
krumpáčem	krumpáč	k1gInSc7	krumpáč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
namíchat	namíchat	k5eAaBmF	namíchat
co	co	k9	co
nejsušší	suchý	k2eAgInSc4d3	nejsušší
beton	beton	k1gInSc4	beton
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
suchého	suchý	k2eAgInSc2d1	suchý
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Mokrý	mokrý	k2eAgInSc1d1	mokrý
písek	písek	k1gInSc1	písek
tvoří	tvořit	k5eAaImIp3nS	tvořit
chuchvalce	chuchvalec	k1gInPc4	chuchvalec
<g/>
,	,	kIx,	,
beton	beton	k1gInSc4	beton
téměř	téměř	k6eAd1	téměř
nelze	lze	k6eNd1	lze
namíchat	namíchat	k5eAaBmF	namíchat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
písek	písek	k1gInSc1	písek
přikrytý	přikrytý	k2eAgInSc1d1	přikrytý
plachtou	plachta	k1gFnSc7	plachta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezmokl	zmoknout	k5eNaPmAgMnS	zmoknout
<g/>
.	.	kIx.	.
</s>
<s>
Uložení	uložení	k1gNnSc1	uložení
betonu	beton	k1gInSc2	beton
do	do	k7c2	do
konstrukce	konstrukce	k1gFnSc2	konstrukce
má	mít	k5eAaImIp3nS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
co	co	k3yQnSc4	co
nejdříve	dříve	k6eAd3	dříve
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
následně	následně	k6eAd1	následně
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
zhutněn	zhutnit	k5eAaPmNgMnS	zhutnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
vypudily	vypudit	k5eAaPmAgFnP	vypudit
velké	velký	k2eAgFnPc1d1	velká
vzduchové	vzduchový	k2eAgFnPc1d1	vzduchová
bubliny	bublina	k1gFnPc1	bublina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácích	domácí	k2eAgFnPc6d1	domácí
podmínkách	podmínka	k1gFnPc6	podmínka
lze	lze	k6eAd1	lze
hutnit	hutnit	k5eAaImF	hutnit
střídavým	střídavý	k2eAgNnSc7d1	střídavé
vpichováním	vpichování	k1gNnSc7	vpichování
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poklepem	poklep	k1gInSc7	poklep
na	na	k7c4	na
bednění	bednění	k1gNnSc4	bednění
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
armatura	armatura	k1gFnSc1	armatura
v	v	k7c6	v
bednění	bednění	k1gNnSc6	bednění
těsná	těsnat	k5eAaImIp3nS	těsnat
<g/>
,	,	kIx,	,
nutí	nutit	k5eAaImIp3nS	nutit
nás	my	k3xPp1nPc4	my
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
namíchání	namíchání	k1gNnSc3	namíchání
řidšího	řídký	k2eAgNnSc2d2	řidší
<g/>
,	,	kIx,	,
až	až	k9	až
tekutého	tekutý	k2eAgInSc2d1	tekutý
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
všude	všude	k6eAd1	všude
dobře	dobře	k6eAd1	dobře
zatekl	zatéct	k5eAaPmAgMnS	zatéct
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
cementu	cement	k1gInSc2	cement
přidat	přidat	k5eAaPmF	přidat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
beton	beton	k1gInSc1	beton
tak	tak	k6eAd1	tak
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
beton	beton	k1gInSc1	beton
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nadále	nadále	k6eAd1	nadále
ochránit	ochránit	k5eAaPmF	ochránit
proti	proti	k7c3	proti
vyschnutí	vyschnutí	k1gNnSc3	vyschnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gNnSc2	jeho
zrání	zrání	k1gNnSc2	zrání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
+15	+15	k4	+15
až	až	k9	až
+25	+25	k4	+25
°	°	k?	°
<g/>
C	C	kA	C
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
28	[number]	k4	28
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
vyschnutí	vyschnutí	k1gNnSc3	vyschnutí
je	být	k5eAaImIp3nS	být
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
máčení	máčení	k1gNnSc1	máčení
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jeho	jeho	k3xOp3gNnSc1	jeho
překrytí	překrytí	k1gNnSc1	překrytí
např.	např.	kA	např.
geotextilií	geotextilie	k1gFnSc7	geotextilie
a	a	k8xC	a
polystyrénovými	polystyrénový	k2eAgFnPc7d1	polystyrénová
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
PE	PE	kA	PE
fólií	fólie	k1gFnSc7	fólie
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgFnPc6d1	výjimečná
situacích	situace	k1gFnPc6	situace
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
vyrobit	vyrobit	k5eAaPmF	vyrobit
beton	beton	k1gInSc4	beton
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ohlídat	ohlídat	k5eAaImF	ohlídat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zrání	zrání	k1gNnSc2	zrání
betonu	beton	k1gInSc2	beton
neklesala	klesat	k5eNaImAgFnS	klesat
pod	pod	k7c4	pod
+5	+5	k4	+5
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
teploty	teplota	k1gFnSc2	teplota
pod	pod	k7c7	pod
+5	+5	k4	+5
°	°	k?	°
<g/>
C	C	kA	C
přestává	přestávat	k5eAaImIp3nS	přestávat
beton	beton	k1gInSc1	beton
zrát	zrát	k5eAaImF	zrát
(	(	kIx(	(
<g/>
přerušuje	přerušovat	k5eAaImIp3nS	přerušovat
se	se	k3xPyFc4	se
tvorba	tvorba	k1gFnSc1	tvorba
pevných	pevný	k2eAgFnPc2d1	pevná
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
cementovými	cementový	k2eAgNnPc7d1	cementové
zrny	zrno	k1gNnPc7	zrno
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
teploty	teplota	k1gFnSc2	teplota
betonu	beton	k1gInSc2	beton
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
zrání	zrání	k1gNnSc2	zrání
pod	pod	k7c4	pod
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
trvalému	trvalý	k2eAgNnSc3d1	trvalé
znehodnocení	znehodnocení	k1gNnSc3	znehodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pamatovat	pamatovat	k5eAaImF	pamatovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
+5	+5	k4	+5
°	°	k?	°
<g/>
C	C	kA	C
zraje	zrát	k5eAaImIp3nS	zrát
beton	beton	k1gInSc1	beton
podstatně	podstatně	k6eAd1	podstatně
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
28	[number]	k4	28
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
popsanými	popsaný	k2eAgInPc7d1	popsaný
problémy	problém	k1gInPc7	problém
nám	my	k3xPp1nPc3	my
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
přísady	přísada	k1gFnPc1	přísada
urychlující	urychlující	k2eAgNnSc1d1	urychlující
tuhnutí	tuhnutí	k1gNnSc1	tuhnutí
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
způsobit	způsobit	k5eAaPmF	způsobit
vydávání	vydávání	k1gNnSc4	vydávání
tepla	teplo	k1gNnSc2	teplo
chemickým	chemický	k2eAgInSc7d1	chemický
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
krystalizace	krystalizace	k1gFnSc2	krystalizace
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
těchto	tento	k3xDgInPc2	tento
opatření	opatření	k1gNnPc4	opatření
však	však	k9	však
beton	beton	k1gInSc1	beton
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
kolem	kolem	k7c2	kolem
+5	+5	k4	+5
°	°	k?	°
<g/>
C	C	kA	C
nabude	nabýt	k5eAaPmIp3nS	nabýt
pevnosti	pevnost	k1gFnSc3	pevnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
mrazu	mráz	k1gInSc3	mráz
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
za	za	k7c4	za
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
<g/>
!	!	kIx.	!
</s>
<s>
Při	při	k7c6	při
betonování	betonování	k1gNnSc6	betonování
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ohřívat	ohřívat	k5eAaImF	ohřívat
betonovou	betonový	k2eAgFnSc4d1	betonová
směs	směs	k1gFnSc4	směs
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
na	na	k7c4	na
+10	+10	k4	+10
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
na	na	k7c4	na
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
cement	cement	k1gInSc1	cement
nesmí	smět	k5eNaImIp3nS	smět
sypat	sypat	k5eAaImF	sypat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
směsi	směs	k1gFnPc4	směs
horké	horký	k2eAgFnPc4d1	horká
více	hodně	k6eAd2	hodně
než	než	k8xS	než
65	[number]	k4	65
°	°	k?	°
<g/>
C	C	kA	C
<g/>
!	!	kIx.	!
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
betonáž	betonáž	k1gFnSc4	betonáž
v	v	k7c6	v
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
prostorech	prostor	k1gInPc6	prostor
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
dotápění	dotápění	k1gNnSc1	dotápění
diesel	diesel	k1gInSc4	diesel
agregáty	agregát	k1gInPc1	agregát
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Výfukové	výfukový	k2eAgFnPc1d1	výfuková
splodiny	splodina	k1gFnPc1	splodina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc1d1	způsobující
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
čerstvých	čerstvý	k2eAgInPc2d1	čerstvý
betonů	beton	k1gInPc2	beton
<g/>
)	)	kIx)	)
karbonataci	karbonatace	k1gFnSc4	karbonatace
-	-	kIx~	-
znehodnocení	znehodnocení	k1gNnSc4	znehodnocení
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
soudržnosti	soudržnost	k1gFnSc2	soudržnost
betonové	betonový	k2eAgFnSc2d1	betonová
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vyrábíme	vyrábět	k5eAaImIp1nP	vyrábět
železobeton	železobeton	k1gInSc4	železobeton
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
kari	kari	k1gNnSc2	kari
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
krycí	krycí	k2eAgFnSc1d1	krycí
vrstva	vrstva	k1gFnSc1	vrstva
výztuže	výztuž	k1gFnSc2	výztuž
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
je	být	k5eAaImIp3nS	být
min	min	kA	min
<g/>
.	.	kIx.	.
35	[number]	k4	35
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
u	u	k7c2	u
vodorovných	vodorovný	k2eAgFnPc2d1	vodorovná
betonových	betonový	k2eAgFnPc2d1	betonová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
tloušťka	tloušťka	k1gFnSc1	tloušťka
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
80	[number]	k4	80
mm	mm	kA	mm
(	(	kIx(	(
<g/>
např.	např.	kA	např.
betonáž	betonáž	k1gFnSc1	betonáž
desky	deska	k1gFnSc2	deska
uložené	uložený	k2eAgFnSc2d1	uložená
na	na	k7c6	na
podloží	podloží	k1gNnSc6	podloží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
používání	používání	k1gNnSc1	používání
kari	kari	k1gNnSc2	kari
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
výztuž	výztuž	k1gFnSc1	výztuž
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
průřezu	průřez	k1gInSc2	průřez
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgNnSc4	žádný
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
neutrálné	utrálný	k2eNgFnSc3d1	utrálný
ose	osa	k1gFnSc3	osa
<g/>
)	)	kIx)	)
a	a	k8xC	a
takováto	takovýto	k3xDgFnSc1	takovýto
výztuž	výztuž	k1gFnSc1	výztuž
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
přidání	přidání	k1gNnSc4	přidání
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
betonářských	betonářský	k2eAgInPc2d1	betonářský
drátků	drátek	k1gInPc2	drátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
směs	směs	k1gFnSc4	směs
zpevní	zpevnit	k5eAaPmIp3nP	zpevnit
při	při	k7c6	při
tuhnutí	tuhnutí	k1gNnSc6	tuhnutí
<g/>
,	,	kIx,	,
neplní	plnit	k5eNaImIp3nS	plnit
ale	ale	k9	ale
funkci	funkce	k1gFnSc4	funkce
statickou	statický	k2eAgFnSc7d1	statická
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
výpočtem	výpočet	k1gInSc7	výpočet
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
nelze	lze	k6eNd1	lze
doporučit	doporučit	k5eAaPmF	doporučit
vyrábět	vyrábět	k5eAaImF	vyrábět
v	v	k7c6	v
domácích	domácí	k2eAgFnPc6d1	domácí
podmínkách	podmínka	k1gFnPc6	podmínka
betony	beton	k1gInPc4	beton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
trvale	trvale	k6eAd1	trvale
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
povětrnostním	povětrnostní	k2eAgFnPc3d1	povětrnostní
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdy	nikdy	k6eAd1	nikdy
nedosáhneme	dosáhnout	k5eNaPmIp1nP	dosáhnout
životnosti	životnost	k1gFnPc1	životnost
betonu	beton	k1gInSc2	beton
z	z	k7c2	z
betonárny	betonárna	k1gFnSc2	betonárna
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc3d1	odpovídající
pořizovací	pořizovací	k2eAgFnSc3d1	pořizovací
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
námaze	námaha	k1gFnSc3	námaha
<g/>
.	.	kIx.	.
</s>
<s>
Praktici	praktik	k1gMnPc1	praktik
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebepečlivěji	sebepečlivě	k6eAd2	sebepečlivě
namíchaný	namíchaný	k2eAgInSc1d1	namíchaný
domácí	domácí	k2eAgInSc1d1	domácí
beton	beton	k1gInSc1	beton
na	na	k7c6	na
venkovní	venkovní	k2eAgFnSc6d1	venkovní
ploše	plocha	k1gFnSc6	plocha
začne	začít	k5eAaPmIp3nS	začít
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zvětrávat	zvětrávat	k5eAaImF	zvětrávat
a	a	k8xC	a
odlupovat	odlupovat	k5eAaImF	odlupovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vadu	vada	k1gFnSc4	vada
estetickou	estetický	k2eAgFnSc4d1	estetická
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beton	beton	k1gInSc1	beton
z	z	k7c2	z
betonárny	betonárna	k1gFnSc2	betonárna
vydrží	vydržet	k5eAaPmIp3nS	vydržet
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
probarvené	probarvený	k2eAgInPc1d1	probarvený
betony	beton	k1gInPc1	beton
nelze	lze	k6eNd1	lze
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
vyrobit	vyrobit	k5eAaPmF	vyrobit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
vodotěsnosti	vodotěsnost	k1gFnSc2	vodotěsnost
betonu	beton	k1gInSc2	beton
se	se	k3xPyFc4	se
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
různé	různý	k2eAgFnPc1d1	různá
hrubé	hrubý	k2eAgFnPc1d1	hrubá
poruchy	porucha	k1gFnPc1	porucha
(	(	kIx(	(
<g/>
trhliny	trhlina	k1gFnPc1	trhlina
<g/>
,	,	kIx,	,
štěrková	štěrkový	k2eAgNnPc1d1	štěrkové
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
,	,	kIx,	,
díry	díra	k1gFnPc1	díra
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pronikání	pronikání	k1gNnSc4	pronikání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
beton	beton	k1gInSc4	beton
opatřit	opatřit	k5eAaPmF	opatřit
vodotěsnou	vodotěsný	k2eAgFnSc7d1	vodotěsná
izolací	izolace	k1gFnSc7	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Vodotěsnost	vodotěsnost	k1gFnSc1	vodotěsnost
betonu	beton	k1gInSc2	beton
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
voda	voda	k1gFnSc1	voda
může	moct	k5eAaImIp3nS	moct
procházet	procházet	k5eAaImF	procházet
cementovou	cementový	k2eAgFnSc7d1	cementová
maltou	malta	k1gFnSc7	malta
nebo	nebo	k8xC	nebo
stykem	styk	k1gInSc7	styk
mezi	mezi	k7c7	mezi
maltou	malta	k1gFnSc7	malta
a	a	k8xC	a
kamennými	kamenný	k2eAgNnPc7d1	kamenné
zrny	zrno	k1gNnPc7	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
zdravý	zdravý	k2eAgInSc1d1	zdravý
kámen	kámen	k1gInSc1	kámen
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
vodotěsný	vodotěsný	k2eAgInSc1d1	vodotěsný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
vodotěsnosti	vodotěsnost	k1gFnSc2	vodotěsnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
betonovou	betonový	k2eAgFnSc4d1	betonová
směs	směs	k1gFnSc4	směs
řádně	řádně	k6eAd1	řádně
složit	složit	k5eAaPmF	složit
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
zhutnit	zhutnit	k5eAaPmF	zhutnit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
vodotěsnosti	vodotěsnost	k1gFnSc2	vodotěsnost
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
povrchově	povrchově	k6eAd1	povrchově
aktivní	aktivní	k2eAgFnSc2d1	aktivní
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
plastifikovaný	plastifikovaný	k2eAgInSc1d1	plastifikovaný
a	a	k8xC	a
provzdušněný	provzdušněný	k2eAgInSc1d1	provzdušněný
beton	beton	k1gInSc1	beton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zhotovit	zhotovit	k5eAaPmF	zhotovit
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
se	s	k7c7	s
spodní	spodní	k2eAgFnSc7d1	spodní
tlakovou	tlakový	k2eAgFnSc7d1	tlaková
vodou	voda	k1gFnSc7	voda
betonovou	betonový	k2eAgFnSc4d1	betonová
šachtu	šachta	k1gFnSc4	šachta
se	s	k7c7	s
zárukou	záruka	k1gFnSc7	záruka
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nepropustnosti	nepropustnost	k1gFnSc2	nepropustnost
<g/>
,	,	kIx,	,
postupuje	postupovat	k5eAaImIp3nS	postupovat
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Z	z	k7c2	z
ocelových	ocelový	k2eAgInPc2d1	ocelový
plechů	plech	k1gInPc2	plech
tloušťky	tloušťka	k1gFnSc2	tloušťka
5	[number]	k4	5
mm	mm	kA	mm
se	se	k3xPyFc4	se
svaří	svařit	k5eAaPmIp3nS	svařit
nepropustná	propustný	k2eNgFnSc1d1	nepropustná
ocelová	ocelový	k2eAgFnSc1d1	ocelová
bedna	bedna	k1gFnSc1	bedna
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Vybetonuje	vybetonovat	k5eAaPmIp3nS	vybetonovat
se	se	k3xPyFc4	se
základová	základový	k2eAgFnSc1d1	základová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
do	do	k7c2	do
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bedna	bedna	k1gFnSc1	bedna
postaví	postavit	k5eAaPmIp3nS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
zvenku	zvenku	k6eAd1	zvenku
zabuduje	zabudovat	k5eAaPmIp3nS	zabudovat
šalung	šalung	k1gInSc4	šalung
a	a	k8xC	a
po	po	k7c6	po
přidání	přidání	k1gNnSc6	přidání
armatury	armatura	k1gFnSc2	armatura
se	se	k3xPyFc4	se
vybetonuje	vybetonovat	k5eAaPmIp3nS	vybetonovat
<g/>
.	.	kIx.	.
</s>
<s>
Ocelová	ocelový	k2eAgFnSc1d1	ocelová
bedna	bedna	k1gFnSc1	bedna
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
uvnitř	uvnitř	k7c2	uvnitř
železobetonové	železobetonový	k2eAgFnSc2d1	železobetonová
bedny	bedna	k1gFnSc2	bedna
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
jakosti	jakost	k1gFnSc2	jakost
betonu	beton	k1gInSc2	beton
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
destruktivních	destruktivní	k2eAgFnPc2d1	destruktivní
a	a	k8xC	a
nedestruktivních	destruktivní	k2eNgFnPc2d1	nedestruktivní
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
betonem	beton	k1gInSc7	beton
označujeme	označovat	k5eAaImIp1nP	označovat
beton	beton	k1gInSc4	beton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
vlastnost	vlastnost	k1gFnSc4	vlastnost
(	(	kIx(	(
<g/>
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
speciální	speciální	k2eAgInPc4d1	speciální
betony	beton	k1gInPc4	beton
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
lehký	lehký	k2eAgInSc1d1	lehký
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
těžký	těžký	k2eAgInSc1d1	těžký
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
rozpínavý	rozpínavý	k2eAgInSc1d1	rozpínavý
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
vysokopevnostní	vysokopevnostní	k2eAgInSc1d1	vysokopevnostní
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
samozhutnitelný	samozhutnitelný	k2eAgInSc1d1	samozhutnitelný
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
vláknobeton	vláknobeton	k1gInSc1	vláknobeton
<g/>
,	,	kIx,	,
vodotěsný	vodotěsný	k2eAgInSc1d1	vodotěsný
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
stříkaný	stříkaný	k2eAgInSc1d1	stříkaný
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Správným	správný	k2eAgNnSc7d1	správné
složením	složení	k1gNnSc7	složení
lze	lze	k6eAd1	lze
docílit	docílit	k5eAaPmF	docílit
mnoha	mnoho	k4c2	mnoho
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
vlastností	vlastnost	k1gFnPc2	vlastnost
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
950	[number]	k4	950
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
4972	[number]	k4	4972
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
železobeton	železobeton	k1gInSc1	železobeton
předpjatý	předpjatý	k2eAgInSc1d1	předpjatý
beton	beton	k1gInSc1	beton
sklobeton	sklobeton	k1gInSc1	sklobeton
vláknobeton	vláknobeton	k1gInSc1	vláknobeton
torkretování	torkretování	k1gNnSc6	torkretování
polymerbeton	polymerbeton	k1gInSc1	polymerbeton
vodotěsný	vodotěsný	k2eAgInSc4d1	vodotěsný
beton	beton	k1gInSc4	beton
zhutněný	zhutněný	k2eAgInSc1d1	zhutněný
beton	beton	k1gInSc1	beton
betonový	betonový	k2eAgInSc4d1	betonový
potěr	potěr	k1gInSc4	potěr
cihlobeton	cihlobeton	k1gInSc4	cihlobeton
imitace	imitace	k1gFnSc2	imitace
betonu	beton	k1gInSc2	beton
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Beton	beton	k1gInSc1	beton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
beton	beton	k1gInSc1	beton
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Beton	beton	k1gInSc1	beton
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
BETON	beton	k1gInSc1	beton
TKS	TKS	kA	TKS
(	(	kIx(	(
<g/>
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
sanace	sanace	k1gFnSc1	sanace
<g/>
)	)	kIx)	)
-	-	kIx~	-
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
betonu	beton	k1gInSc6	beton
eBeton	eBeton	k1gInSc1	eBeton
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
betonu	beton	k1gInSc6	beton
V	v	k7c6	v
čem	co	k3yRnSc6	co
spočívalo	spočívat	k5eAaImAgNnS	spočívat
tajemství	tajemství	k1gNnSc1	tajemství
betonu	beton	k1gInSc2	beton
Římanů	Říman	k1gMnPc2	Říman
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
článek	článek	k1gInSc4	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Osel	osít	k5eAaPmAgMnS	osít
</s>
