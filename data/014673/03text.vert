<s>
Rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Animace	animace	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
rotace	rotace	k1gFnSc2
okolo	okolo	k7c2
skutečné	skutečný	k2eAgFnSc2d1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
planety	planeta	k1gFnSc2
Země	zem	k1gFnSc2
kolem	kolem	k7c2
její	její	k3xOp3gFnSc2
vlastní	vlastní	k2eAgFnSc2d1
osy	osa	k1gFnSc2
(	(	kIx(
<g/>
pomyslná	pomyslný	k2eAgFnSc1d1
přímka	přímka	k1gFnSc1
spojující	spojující	k2eAgFnSc2d1
severní	severní	k2eAgFnSc2d1
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
zeměpisný	zeměpisný	k2eAgInSc4d1
pól	pól	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
rotuje	rotovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
východu	východ	k1gInSc3
<g/>
,	,	kIx,
z	z	k7c2
pohledu	pohled	k1gInSc2
Polárky	Polárka	k1gFnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
pohyb	pohyb	k1gInSc4
proti	proti	k7c3
směru	směr	k1gInSc3
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
otočka	otočka	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
23	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
56	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
4	#num#	k4
sekundy	sekunda	k1gFnPc1
(	(	kIx(
<g/>
tj.	tj.	kA
1	#num#	k4
siderický	siderický	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Charakteristiky	charakteristika	k1gFnPc1
zemské	zemský	k2eAgFnSc2d1
rotace	rotace	k1gFnSc2
</s>
<s>
Sklon	sklon	k1gInSc1
osy	osa	k1gFnSc2
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
při	při	k7c6
jarní	jarní	k2eAgFnSc6d1
rovnodennosti	rovnodennost	k1gFnSc6
</s>
<s>
Z	z	k7c2
fyzikálního	fyzikální	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
Země	země	k1gFnSc1
chová	chovat	k5eAaImIp3nS
jako	jako	k9
obří	obří	k2eAgInSc4d1
setrvačník	setrvačník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Osa	osa	k1gFnSc1
rotace	rotace	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
rotuje	rotovat	k5eAaImIp3nS
kolem	kolem	k7c2
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
kromě	kromě	k7c2
zemských	zemský	k2eAgInPc2d1
pólů	pól	k1gInPc2
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
protíná	protínat	k5eAaImIp3nS
nebeské	nebeský	k2eAgInPc4d1
póly	pól	k1gInPc4
na	na	k7c6
pomyslné	pomyslný	k2eAgFnSc6d1
nebeské	nebeský	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklon	sklon	k1gInSc1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
od	od	k7c2
kolmice	kolmice	k1gFnSc2
na	na	k7c4
ekliptiku	ekliptika	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
sklon	sklon	k1gInSc1
vůči	vůči	k7c3
ose	osa	k1gFnSc3
<g/>
,	,	kIx,
kolem	kolem	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
Země	zem	k1gFnSc2
obíhá	obíhat	k5eAaImIp3nS
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
asi	asi	k9
23,5	23,5	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
působení	působení	k1gNnSc3
různých	různý	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
se	se	k3xPyFc4
však	však	k9
sklon	sklon	k1gInSc1
osy	osa	k1gFnSc2
mění	měnit	k5eAaImIp3nS
a	a	k8xC
tyto	tento	k3xDgInPc1
pohyby	pohyb	k1gInPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
precese	precese	k1gFnPc1
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
a	a	k8xC
nutace	nutace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polohu	poloh	k1gInSc2
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
také	také	k9
mohou	moct	k5eAaImIp3nP
pozměnit	pozměnit	k5eAaPmF
silná	silný	k2eAgNnPc1d1
zemětřesení	zemětřesení	k1gNnPc1
<g/>
,	,	kIx,
např.	např.	kA
zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
ji	on	k3xPp3gFnSc4
vychýlilo	vychýlit	k5eAaPmAgNnS
asi	asi	k9
o	o	k7c4
16	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změny	změna	k1gFnPc1
rychlosti	rychlost	k1gFnSc2
rotace	rotace	k1gFnSc1
</s>
<s>
Simulace	simulace	k1gFnSc1
změny	změna	k1gFnSc2
délky	délka	k1gFnSc2
dne	den	k1gInSc2
během	během	k7c2
historie	historie	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Analýzou	analýza	k1gFnSc7
starých	starý	k2eAgInPc2d1
astronomických	astronomický	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
z	z	k7c2
dob	doba	k1gFnPc2
700	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
až	až	k9
1600	#num#	k4
n.	n.	k?
<g/>
l.	l.	k?
doplněných	doplněná	k1gFnPc2
o	o	k7c4
novější	nový	k2eAgInPc4d2
údaje	údaj	k1gInPc4
se	se	k3xPyFc4
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
2700	#num#	k4
let	léto	k1gNnPc2
se	se	k3xPyFc4
prodlužovala	prodlužovat	k5eAaImAgFnS
střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
dne	den	k1gInSc2
v	v	k7c6
průměru	průměr	k1gInSc6
o	o	k7c4
0,0017	0,0017	k4
sekundy	sekunda	k1gFnPc4
za	za	k7c4
století	století	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
kombinaci	kombinace	k1gFnSc6
vlivu	vliv	k1gInSc2
výměny	výměna	k1gFnSc2
momentu	moment	k1gInSc2
hybnosti	hybnost	k1gFnSc2
mezi	mezi	k7c7
Zemí	zem	k1gFnSc7
a	a	k8xC
Měsícem	měsíc	k1gInSc7
(	(	kIx(
<g/>
Země	země	k1gFnSc1
je	být	k5eAaImIp3nS
zpomalována	zpomalovat	k5eAaImNgFnS
o	o	k7c4
0,0023	0,0023	k4
s	s	k7c7
<g/>
/	/	kIx~
<g/>
století	století	k1gNnSc1
a	a	k8xC
Měsíc	měsíc	k1gInSc1
se	se	k3xPyFc4
vzdaluje	vzdalovat	k5eAaImIp3nS
o	o	k7c4
3,8	3,8	k4
cm	cm	kA
za	za	k7c4
rok	rok	k1gInSc4
<g/>
)	)	kIx)
s	s	k7c7
vlivem	vliv	k1gInSc7
zvětšování	zvětšování	k1gNnSc2
zploštění	zploštění	k1gNnSc1
Země	zem	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
úbytku	úbytek	k1gInSc2
ledovců	ledovec	k1gInPc2
z	z	k7c2
vysokých	vysoký	k2eAgFnPc2d1
zeměpisných	zeměpisný	k2eAgFnPc2d1
šířek	šířka	k1gFnPc2
po	po	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
ledové	ledový	k2eAgFnSc2d1
a	a	k8xC
přesunu	přesunout	k5eAaPmIp1nS
táním	tání	k1gNnSc7
vzniklé	vzniklý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
do	do	k7c2
oblasti	oblast	k1gFnSc2
rovníku	rovník	k1gInSc2
(	(	kIx(
<g/>
Nárůst	nárůst	k1gInSc1
zploštění	zploštění	k1gNnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
zvětšení	zvětšení	k1gNnSc3
momentu	moment	k1gInSc2
setrvačnosti	setrvačnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
moment	moment	k1gInSc1
hybnosti	hybnost	k1gFnSc2
je	být	k5eAaImIp3nS
zachován	zachovat	k5eAaPmNgInS
<g/>
,	,	kIx,
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
zpomalení	zpomalení	k1gNnSc3
rotace	rotace	k1gFnSc2
–	–	k?
podobně	podobně	k6eAd1
jako	jako	k9
když	když	k8xS
baletka	baletka	k1gFnSc1
v	v	k7c6
piruetě	pirueta	k1gFnSc6
zpomalí	zpomalet	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
rotaci	rotace	k1gFnSc4
rozpažením	rozpažení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
zpomalování	zpomalování	k1gNnSc2
zemské	zemský	k2eAgFnPc1d1
rotace	rotace	k1gFnPc1
nicméně	nicméně	k8xC
nejde	jít	k5eNaImIp3nS
jednoduše	jednoduše	k6eAd1
promítnout	promítnout	k5eAaPmF
do	do	k7c2
minulosti	minulost	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c4
rychlost	rychlost	k1gFnSc4
rotace	rotace	k1gFnSc2
má	mít	k5eAaImIp3nS
vliv	vliv	k1gInSc4
i	i	k9
v	v	k7c6
geologických	geologický	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
proměnlivé	proměnlivý	k2eAgNnSc1d1
rozložení	rozložení	k1gNnSc1
hmoty	hmota	k1gFnSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
důsledku	důsledek	k1gInSc6
pohybu	pohyb	k1gInSc2
kontinentů	kontinent	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
jejího	její	k3xOp3gNnSc2
chemického	chemický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
její	její	k3xOp3gFnPc4
viskozity	viskozita	k1gFnPc4
a	a	k8xC
elastičnosti	elastičnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
miliardou	miliarda	k4xCgFnSc7
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
podle	podle	k7c2
různých	různý	k2eAgInPc2d1
modelů	model	k1gInPc2
Měsíc	měsíc	k1gInSc1
46	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
zemských	zemský	k2eAgInPc2d1
poloměrů	poloměr	k1gInPc2
daleko	daleko	k6eAd1
(	(	kIx(
<g/>
oproti	oproti	k7c3
dnešním	dnešní	k2eAgFnPc3d1
60	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
délka	délka	k1gFnSc1
dne	den	k1gInSc2
činila	činit	k5eAaImAgFnS
14	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Před	před	k7c7
500	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
délka	délka	k1gFnSc1
dne	den	k1gInSc2
mezi	mezi	k7c7
21	#num#	k4
a	a	k8xC
22	#num#	k4
hodinami	hodina	k1gFnPc7
<g/>
,	,	kIx,
před	před	k7c7
75	#num#	k4
až	až	k8xS
72	#num#	k4
milióny	milión	k4xCgInPc7
let	léto	k1gNnPc2
(	(	kIx(
<g/>
období	období	k1gNnSc2
kampánu	kampán	k2eAgFnSc4d1
<g/>
)	)	kIx)
přibližně	přibližně	k6eAd1
23,5	23,5	k4
hodiny	hodina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
budoucnu	budoucno	k1gNnSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
dál	daleko	k6eAd2
Měsíc	měsíc	k1gInSc1
od	od	k7c2
Země	zem	k1gFnSc2
vzdalovat	vzdalovat	k5eAaImF
a	a	k8xC
tak	tak	k6eAd1
zpomalovat	zpomalovat	k5eAaImF
zemskou	zemský	k2eAgFnSc4d1
rotaci	rotace	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zpomalování	zpomalování	k1gNnSc1
bude	být	k5eAaImBp3nS
pomalejší	pomalý	k2eAgNnSc1d2
než	než	k8xS
bylo	být	k5eAaImAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
slapové	slapový	k2eAgFnPc1d1
síly	síla	k1gFnPc1
budou	být	k5eAaImBp3nP
slabší	slabý	k2eAgFnPc1d2
a	a	k8xC
tak	tak	k6eAd1
i	i	k9
přenos	přenos	k1gInSc4
momentu	moment	k1gInSc2
hybnosti	hybnost	k1gFnSc2
bude	být	k5eAaImBp3nS
nižší	nízký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
asi	asi	k9
500	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
bude	být	k5eAaImBp3nS
vzdálenost	vzdálenost	k1gFnSc4
Měsíce	měsíc	k1gInSc2
činit	činit	k5eAaImF
62	#num#	k4
zemských	zemský	k2eAgInPc2d1
poloměrů	poloměr	k1gInPc2
a	a	k8xC
den	den	k1gInSc1
se	se	k3xPyFc4
prodlouží	prodloužit	k5eAaPmIp3nS
na	na	k7c4
26	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důsledky	důsledek	k1gInPc1
rotace	rotace	k1gFnSc2
</s>
<s>
Ze	z	k7c2
Země	zem	k1gFnSc2
se	se	k3xPyFc4
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
zdánlivého	zdánlivý	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
nebeských	nebeský	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
na	na	k7c6
obloze	obloha	k1gFnSc6
(	(	kIx(
<g/>
kromě	kromě	k7c2
meteorů	meteor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vznikají	vznikat	k5eAaImIp3nP
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
)	)	kIx)
jeví	jevit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
pohyb	pohyb	k1gInSc1
směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
15	#num#	k4
<g/>
°	°	k?
<g/>
/	/	kIx~
<g/>
h	h	k?
=	=	kIx~
15	#num#	k4
<g/>
'	'	kIx"
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
přibližně	přibližně	k6eAd1
o	o	k7c4
sluneční	sluneční	k2eAgInSc4d1
průměr	průměr	k1gInSc4
každé	každý	k3xTgFnPc4
dvě	dva	k4xCgFnPc4
minuty	minuta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
</s>
<s>
Rotace	rotace	k1gFnSc1
okolo	okolo	k7c2
vlastní	vlastní	k2eAgFnSc2d1
osy	osa	k1gFnSc2
vyvolává	vyvolávat	k5eAaImIp3nS
(	(	kIx(
<g/>
zdánlivou	zdánlivý	k2eAgFnSc4d1
<g/>
)	)	kIx)
odstředivou	odstředivý	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
působením	působení	k1gNnSc7
vzniká	vznikat	k5eAaImIp3nS
vydutí	vydutí	k1gNnSc1
na	na	k7c6
rovníku	rovník	k1gInSc6
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
důsledku	důsledek	k1gInSc6
zploštění	zploštění	k1gNnSc4
na	na	k7c6
obou	dva	k4xCgInPc6
zemských	zemský	k2eAgInPc6d1
pólech	pól	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Vlivem	vliv	k1gInSc7
odstředivé	odstředivý	k2eAgFnSc2d1
síly	síla	k1gFnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
rovníku	rovník	k1gInSc6
menší	malý	k2eAgFnSc1d2
tíha	tíha	k1gFnSc1
než	než	k8xS
na	na	k7c6
pólech	pól	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
hmotnosti	hmotnost	k1gFnSc3
má	mít	k5eAaImIp3nS
Země	země	k1gFnSc1
vlivem	vlivem	k7c2
setrvačnosti	setrvačnost	k1gFnSc2
velkou	velký	k2eAgFnSc4d1
kinetickou	kinetický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
resp.	resp.	kA
moment	moment	k1gInSc4
setrvačnosti	setrvačnost	k1gFnSc2
<g/>
,	,	kIx,
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
veliký	veliký	k2eAgInSc4d1
setrvačník	setrvačník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zemském	zemský	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
se	se	k3xPyFc4
rotace	rotace	k1gFnSc1
projevuje	projevovat	k5eAaImIp3nS
jako	jako	k9
(	(	kIx(
<g/>
zdánlivá	zdánlivý	k2eAgFnSc1d1
<g/>
)	)	kIx)
Coriolisova	Coriolisův	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
zejména	zejména	k9
na	na	k7c4
globální	globální	k2eAgNnSc4d1
proudění	proudění	k1gNnSc4
vzduchu	vzduch	k1gInSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
a	a	k8xC
dále	daleko	k6eAd2
i	i	k9
na	na	k7c4
pohyb	pohyb	k1gInSc4
vody	voda	k1gFnSc2
ve	v	k7c6
světových	světový	k2eAgInPc6d1
oceánech	oceán	k1gInPc6
resp.	resp.	kA
na	na	k7c4
mořské	mořský	k2eAgInPc4d1
proudy	proud	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Biosféra	biosféra	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Cirkadiánní	Cirkadiánní	k2eAgInSc1d1
rytmus	rytmus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Střídání	střídání	k1gNnSc1
dne	den	k1gInSc2
a	a	k8xC
noci	noc	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k9
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
část	část	k1gFnSc1
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
je	být	k5eAaImIp3nS
a	a	k8xC
není	být	k5eNaImIp3nS
vystavena	vystavit	k5eAaPmNgFnS
slunečnímu	sluneční	k2eAgNnSc3d1
světlu	světlo	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
kolísáním	kolísání	k1gNnSc7
teploty	teplota	k1gFnSc2
mezi	mezi	k7c7
dnem	den	k1gInSc7
a	a	k8xC
nocí	noc	k1gFnSc7
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
a	a	k8xC
má	mít	k5eAaImIp3nS
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
pro	pro	k7c4
adaptaci	adaptace	k1gFnSc4
téměř	téměř	k6eAd1
veškerého	veškerý	k3xTgInSc2
života	život	k1gInSc2
na	na	k7c4
Zemi	zem	k1gFnSc4
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
organismů	organismus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
slunečním	sluneční	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
nepřicházejí	přicházet	k5eNaImIp3nP
do	do	k7c2
styku	styk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelené	Zelené	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
ve	v	k7c6
dne	den	k1gInSc2
mění	měnit	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
dýchání	dýchání	k1gNnSc1
na	na	k7c4
příjem	příjem	k1gInSc4
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
<g/>
;	;	kIx,
někteří	některý	k3yIgMnPc1
živočichové	živočich	k1gMnPc1
se	se	k3xPyFc4
např.	např.	kA
specializovali	specializovat	k5eAaBmAgMnP
na	na	k7c4
lov	lov	k1gInSc4
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
lepším	dobrý	k2eAgNnSc7d2
viděním	vidění	k1gNnSc7
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Časová	časový	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlivem	vliv	k1gInSc7
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
kolem	kolem	k7c2
své	svůj	k3xOyFgFnSc2
osy	osa	k1gFnSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
přesunují	přesunovat	k5eAaImIp3nP
oblast	oblast	k1gFnSc4
Sluncem	slunce	k1gNnSc7
osvětlená	osvětlený	k2eAgFnSc1d1
a	a	k8xC
oblast	oblast	k1gFnSc1
od	od	k7c2
Slunce	slunce	k1gNnSc2
odkloněná	odkloněný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
na	na	k7c6
povrchu	povrch	k1gInSc6
projevuje	projevovat	k5eAaImIp3nS
jako	jako	k9
střídání	střídání	k1gNnSc1
dne	den	k1gInSc2
a	a	k8xC
noci	noc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
vznikla	vzniknout	k5eAaPmAgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
rozdělila	rozdělit	k5eAaPmAgFnS
celý	celý	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
dle	dle	k7c2
poledníků	poledník	k1gInPc2
na	na	k7c4
24	#num#	k4
časových	časový	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
po	po	k7c6
15	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pásmový	pásmový	k2eAgInSc4d1
(	(	kIx(
<g/>
místní	místní	k2eAgInSc4d1
<g/>
)	)	kIx)
čas	čas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgNnSc6
pásmu	pásmo	k1gNnSc6
stejný	stejný	k2eAgInSc4d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
dle	dle	k7c2
hodnoty	hodnota	k1gFnSc2
na	na	k7c6
středním	střední	k2eAgInSc6d1
poledníku	poledník	k1gInSc6
daného	daný	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
pro	pro	k7c4
poledníky	poledník	k1gInPc4
0	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
°	°	k?
atd.	atd.	kA
Tento	tento	k3xDgInSc1
místní	místní	k2eAgInSc1d1
čas	čas	k1gInSc1
bývá	bývat	k5eAaImIp3nS
doplněn	doplnit	k5eAaPmNgInS
názvem	název	k1gInSc7
pásmového	pásmový	k2eAgInSc2d1
času	čas	k1gInSc2
slovně	slovně	k6eAd1
nebo	nebo	k8xC
zkratkou	zkratka	k1gFnSc7
(	(	kIx(
<g/>
například	například	k6eAd1
SEČ	SEČ	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
uvedením	uvedení	k1gNnSc7
posunu	posun	k1gInSc2
místního	místní	k2eAgNnSc2d1
časového	časový	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
vzhledem	vzhledem	k7c3
ke	k	k7c3
koordinovanému	koordinovaný	k2eAgInSc3d1
světovému	světový	k2eAgInSc3d1
času	čas	k1gInSc3
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posun	posun	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
<g/>
)	)	kIx)
celistvým	celistvý	k2eAgInSc7d1
počtem	počet	k1gInSc7
hodin	hodina	k1gFnPc2
a	a	k8xC
znaménkem	znaménko	k1gNnSc7
buď	buď	k8xC
plus	plus	k6eAd1
nebo	nebo	k8xC
minus	minus	k6eAd1
třeba	třeba	k9
SEČ	SEČ	kA
=	=	kIx~
UTC	UTC	kA
+	+	kIx~
1	#num#	k4
hod	hod	k1gInSc1
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
složitý	složitý	k2eAgInSc1d1
geofyzikální	geofyzikální	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
ovlivňuje	ovlivňovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
fyzikálních	fyzikální	k2eAgInPc2d1
<g/>
,	,	kIx,
geofyzikálních	geofyzikální	k2eAgInPc2d1
a	a	k8xC
astrofyzikálních	astrofyzikální	k2eAgInPc2d1
jevů	jev	k1gInPc2
a	a	k8xC
procesů	proces	k1gInPc2
<g/>
:	:	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
slapové	slapový	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Slunce	slunce	k1gNnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
změny	změna	k1gFnPc1
ve	v	k7c6
zploštění	zploštění	k1gNnSc6
na	na	k7c6
zemských	zemský	k2eAgInPc6d1
pólech	pól	k1gInPc6
<g/>
,	,	kIx,
pohyby	pohyb	k1gInPc1
magnetických	magnetický	k2eAgInPc2d1
pólů	pól	k1gInPc2
<g/>
,	,	kIx,
procesy	proces	k1gInPc1
probíhající	probíhající	k2eAgInPc1d1
uvnitř	uvnitř	k7c2
zemského	zemský	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
a	a	k8xC
pláště	plášť	k1gInSc2
<g/>
,	,	kIx,
precesní	precesní	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
Země	zem	k1gFnSc2
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gInSc7
spojená	spojený	k2eAgFnSc1d1
nutace	nutace	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
<g/>
,	,	kIx,
pohyby	pohyb	k1gInPc1
kontinentů	kontinent	k1gInPc2
a	a	k8xC
tektonických	tektonický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
apod.	apod.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Earth	Eartha	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
rotation	rotation	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.latimes.com/news/nationworld/world/la-sci-japan-quake-science-20110313,0,5782113.story	http://www.latimes.com/news/nationworld/world/la-sci-japan-quake-science-20110313,0,5782113.stor	k1gInPc1
Los	los	k1gInSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
:	:	kIx,
Japan	japan	k1gInSc1
earthquake	earthquak	k1gFnSc2
shifted	shifted	k1gMnSc1
Earth	Earth	k1gMnSc1
on	on	k3xPp3gMnSc1
its	its	k?
axis	axis	k1gInSc1
<g/>
↑	↑	k?
STEPHENSON	STEPHENSON	kA
<g/>
,	,	kIx,
F.	F.	kA
R.	R.	kA
<g/>
;	;	kIx,
MORRISON	MORRISON	kA
<g/>
,	,	kIx,
L.	L.	kA
V.	V.	kA
Long-Term	Long-Term	k1gInSc1
Fluctuations	Fluctuations	k1gInSc1
in	in	k?
the	the	k?
Earth	Earth	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Rotation	Rotation	k1gInSc1
<g/>
:	:	kIx,
700	#num#	k4
BC	BC	kA
to	ten	k3xDgNnSc1
AD	ad	k7c4
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philosophical	Philosophical	k1gFnSc1
Transactions	Transactionsa	k1gFnPc2
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
A	A	kA
<g/>
:	:	kIx,
Mathematical	Mathematical	k1gMnSc1
<g/>
,	,	kIx,
Physical	Physical	k1gMnSc1
and	and	k?
Engineering	Engineering	k1gInSc1
Sciences	Sciences	k1gInSc1
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
351	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1695	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
165	#num#	k4
<g/>
–	–	k?
<g/>
202	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1364	#num#	k4
<g/>
-	-	kIx~
<g/>
503	#num#	k4
<g/>
X.	X.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsta	rsta	k6eAd1
<g/>
.1995	.1995	k4
<g/>
.0028	.0028	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019PA003723	https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019PA003723	k4
-	-	kIx~
Subdaily‐	Subdaily‐	k1gFnSc6
Chemical	Chemical	k1gFnPc2
Variability	variabilita	k1gFnSc2
in	in	k?
a	a	k8xC
Torreites	Torreites	k1gMnSc1
Sanchezi	Sancheze	k1gFnSc4
Rudist	Rudist	k1gMnSc1
Shell	Shell	k1gMnSc1
<g/>
:	:	kIx,
Implications	Implicationsa	k1gFnPc2
for	forum	k1gNnPc2
Rudist	Rudist	k1gMnSc1
Paleobiology	paleobiolog	k1gMnPc4
and	and	k?
the	the	k?
Cretaceous	Cretaceous	k1gInSc1
Day‐	Day‐	k1gFnPc2
Cycle	Cycle	k1gFnSc2
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgInSc1d1
rok	rok	k1gInSc1
měl	mít	k5eAaImAgInS
372	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
VONDRÁK	Vondrák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změny	změna	k1gFnSc2
v	v	k7c6
rotaci	rotace	k1gFnSc6
Země	zem	k1gFnSc2
a	a	k8xC
vývoj	vývoj	k1gInSc4
dráhy	dráha	k1gFnSc2
Měsíce	měsíc	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IAN	IAN	kA
<g/>
,	,	kIx,
2000-06-08	2000-06-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převzato	převzít	k5eAaPmNgNnS
ze	z	k7c2
zpravodaje	zpravodaj	k1gInSc2
Corona	Corona	k1gFnSc1
Pragensis	Pragensis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Encyklopédia	Encyklopédium	k1gNnSc2
Zeme	Zeme	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Obzor	obzor	k1gInSc1
1985	#num#	k4
<g/>
.	.	kIx.
720	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
9	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
</s>
<s>
Precese	precese	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
</s>
<s>
Sklon	sklon	k1gInSc1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4015205-4	4015205-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85040448	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85040448	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
|	|	kIx~
Planetární	planetární	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
|	|	kIx~
Sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
