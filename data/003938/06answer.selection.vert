<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2006	[number]	k4	2006
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
několik	několik	k4yIc1	několik
protivládních	protivládní	k2eAgFnPc2d1	protivládní
demonstrací	demonstrace	k1gFnPc2	demonstrace
za	za	k7c4	za
odstoupení	odstoupení	k1gNnSc4	odstoupení
premiéra	premiér	k1gMnSc2	premiér
Ference	Ferenc	k1gMnSc2	Ferenc
Gyurcsánye	Gyurcsány	k1gMnSc2	Gyurcsány
z	z	k7c2	z
MSZP	MSZP	kA	MSZP
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
veřejnosti	veřejnost	k1gFnSc3	veřejnost
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
vítězství	vítězství	k1gNnSc2	vítězství
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
lhal	lhát	k5eAaImAgMnS	lhát
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
rozpočtu	rozpočet	k1gInSc6	rozpočet
a	a	k8xC	a
skutečném	skutečný	k2eAgInSc6d1	skutečný
stavu	stav	k1gInSc6	stav
maďarské	maďarský	k2eAgFnSc2d1	maďarská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
