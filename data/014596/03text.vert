<s>
Částice	částice	k1gFnSc1
(	(	kIx(
<g/>
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
jazykovědě	jazykověda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
částice	částice	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Částice	částice	k1gFnSc1
(	(	kIx(
<g/>
partikule	partikule	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neohebný	ohebný	k2eNgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
signalizuje	signalizovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
mluvčího	mluvčí	k1gMnSc2
k	k	k7c3
výpovědi	výpověď	k1gFnSc3
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
modalitu	modalita	k1gFnSc4
věty	věta	k1gFnSc2
nebo	nebo	k8xC
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS
větný	větný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
uvozuje	uvozovat	k5eAaImIp3nS
větu	věta	k1gFnSc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stojí	stát	k5eAaImIp3nS
na	na	k7c6
jejím	její	k3xOp3gInSc6
začátku	začátek	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
i	i	k9
uvnitř	uvnitř	k7c2
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
částice	částice	k1gFnPc1
mají	mít	k5eAaImIp3nP
platnost	platnost	k1gFnSc4
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
Ano	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikoli	nikoli	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajisté	zajisté	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
jsou	být	k5eAaImIp3nP
částice	částice	k1gFnPc1
relativně	relativně	k6eAd1
málo	málo	k6eAd1
početné	početný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
částic	částice	k1gFnPc2
</s>
<s>
Podle	podle	k7c2
původu	původ	k1gInSc2
částice	částice	k1gFnSc2
dělíme	dělit	k5eAaImIp1nP
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1
–	–	k?
nemohou	moct	k5eNaImIp3nP
zastávat	zastávat	k5eAaImF
funkci	funkce	k1gFnSc4
jiného	jiný	k2eAgInSc2d1
slovního	slovní	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
:	:	kIx,
ať	ať	k9
<g/>
,	,	kIx,
kéž	kéž	k9
<g/>
,	,	kIx,
ano	ano	k9
<g/>
.	.	kIx.
</s>
<s>
Nevlastní	vlastní	k2eNgInSc1d1
–	–	k?
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
podle	podle	k7c2
souvislosti	souvislost	k1gFnSc2
i	i	k8xC
jiným	jiný	k2eAgInSc7d1
slovním	slovní	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
:	:	kIx,
jistě	jistě	k6eAd1
<g/>
,	,	kIx,
asi	asi	k9
<g/>
,	,	kIx,
prý	prý	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
je	on	k3xPp3gInPc4
odlišovat	odlišovat	k5eAaImF
zejména	zejména	k9
od	od	k7c2
spojek	spojka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
I	i	k9
promluvil	promluvit	k5eAaPmAgMnS
ke	k	k7c3
králi	král	k1gMnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
částice	částice	k1gFnSc1
<g/>
)	)	kIx)
x	x	k?
Promluvil	promluvit	k5eAaPmAgMnS
ke	k	k7c3
králi	král	k1gMnSc3
i	i	k8xC
ke	k	k7c3
královně	královna	k1gFnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
spojka	spojka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Že	že	k8xS
ses	ses	k?
do	do	k7c2
toho	ten	k3xDgNnSc2
pletla	plést	k5eAaImAgFnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
částice	částice	k1gFnPc1
<g/>
)	)	kIx)
x	x	k?
Zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ses	ses	k?
do	do	k7c2
toho	ten	k3xDgNnSc2
pletla	plést	k5eAaImAgFnS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
spojka	spojka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Podle	podle	k7c2
významu	význam	k1gInSc2
dělíme	dělit	k5eAaImIp1nP
částice	částice	k1gFnSc2
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
tázací	tázací	k2eAgInSc1d1
–	–	k?
uvozují	uvozovat	k5eAaImIp3nP
tázací	tázací	k2eAgFnPc4d1
věty	věta	k1gFnPc4
<g/>
:	:	kIx,
Jestlipak	jestlipak	k9
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
přečetl	přečíst	k5eAaPmAgMnS
<g/>
?	?	kIx.
</s>
<s>
apelové	apelový	k2eAgFnPc1d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
žádací	žádací	k2eAgMnPc1d1
–	–	k?
Ať	ať	k8xS,k8xC
mi	já	k3xPp1nSc3
přinese	přinést	k5eAaPmIp3nS
knihu	kniha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
přací	přací	k2eAgInSc1d1
–	–	k?
Kéž	kéž	k9
by	by	kYmCp3nS
už	už	k6eAd1
skončila	skončit	k5eAaPmAgFnS
zima	zima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Částice	částice	k1gFnPc1
dále	daleko	k6eAd2
můžeme	moct	k5eAaImIp1nP
dělit	dělit	k5eAaImF
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
modální	modální	k2eAgInSc1d1
–	–	k?
nejspíš	nejspíš	k9
<g/>
,	,	kIx,
zajisté	zajisté	k9
<g/>
,	,	kIx,
možná	možná	k9
<g/>
,	,	kIx,
asi	asi	k9
</s>
<s>
intenzifikační	intenzifikační	k2eAgMnPc1d1
–	–	k?
velmi	velmi	k6eAd1
<g/>
,	,	kIx,
velice	velice	k6eAd1
<g/>
,	,	kIx,
příliš	příliš	k6eAd1
<g/>
,	,	kIx,
moc	moc	k1gFnSc1
(	(	kIx(
<g/>
pozor	pozor	k1gInSc1
na	na	k7c4
platnost	platnost	k1gFnSc4
slovního	slovní	k2eAgInSc2d1
druhu	druh	k1gInSc2
ve	v	k7c6
větě	věta	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
vytýkací	vytýkací	k2eAgInSc1d1
–	–	k?
jen	jen	k9
<g/>
,	,	kIx,
také	také	k9
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
<g/>
,	,	kIx,
přímo	přímo	k6eAd1
</s>
<s>
modifikační	modifikační	k2eAgInSc1d1
–	–	k?
přece	přece	k9
<g/>
,	,	kIx,
ale	ale	k8xC
<g/>
,	,	kIx,
prostě	prostě	k9
<g/>
,	,	kIx,
snad	snad	k9
<g/>
,	,	kIx,
jen	jen	k9
<g/>
,	,	kIx,
klidně	klidně	k6eAd1
</s>
<s>
Klidně	klidně	k6eAd1
mluv	mluv	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
částice	částice	k1gFnPc1
<g/>
)	)	kIx)
x	x	k?
Mluv	mluv	k1gInSc1
klidně	klidně	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
příslovce	příslovce	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
odpověďové	odpověďus	k1gMnPc1
–	–	k?
ano	ano	k9
<g/>
,	,	kIx,
ne	ne	k9
</s>
<s>
negační	negační	k2eAgInSc1d1
–	–	k?
ne	ne	k9
<g/>
,	,	kIx,
nikoli	nikoli	k9
</s>
<s>
větná	větný	k2eAgNnPc1d1
adverbia	adverbium	k1gNnPc1
–	–	k?
samozřejmě	samozřejmě	k6eAd1
<g/>
,	,	kIx,
bohužel	bohužel	k9
<g/>
,	,	kIx,
každopádně	každopádně	k6eAd1
<g/>
,	,	kIx,
naštěstí	naštěstí	k6eAd1
</s>
<s>
Tento	tento	k3xDgInSc1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgNnSc1d1
rozlišit	rozlišit	k5eAaPmF
od	od	k7c2
příslovce	příslovce	k1gNnSc2
nebo	nebo	k8xC
spojky	spojka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
skoro	skoro	k6eAd1
vždy	vždy	k6eAd1
jsou	být	k5eAaImIp3nP
na	na	k7c6
začátku	začátek	k1gInSc6
věty	věta	k1gFnSc2
nebo	nebo	k8xC
souvětí	souvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Částice	částice	k1gFnPc1
v	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
Částice	částice	k1gFnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
fungují	fungovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
jako	jako	k8xC,k8xS
větné	větný	k2eAgFnPc1d1
částice	částice	k1gFnPc1
(	(	kIx(
<g/>
modifikují	modifikovat	k5eAaBmIp3nP
význam	význam	k1gInSc4
celé	celý	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
日	日	k?
–	–	k?
japonština	japonština	k1gFnSc1
<g/>
)	)	kIx)
existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
typů	typ	k1gInPc2
částic	částice	k1gFnPc2
např.	např.	kA
pádové	pádový	k2eAgFnPc1d1
<g/>
,	,	kIx,
kontextové	kontextový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
pak	pak	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
jednotlivá	jednotlivý	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
<g/>
,	,	kIx,
slovní	slovní	k2eAgNnPc1d1
spojení	spojení	k1gNnPc1
nebo	nebo	k8xC
úseky	úsek	k1gInPc1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
vymezují	vymezovat	k5eAaImIp3nP
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
větnými	větný	k2eAgInPc7d1
členy	člen	k1gInPc7
apod.	apod.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Modalita	modalita	k1gFnSc1
(	(	kIx(
<g/>
lingvistika	lingvistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
částice	částice	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Částice	částice	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Partikula	Partikula	k?
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
6861	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85098373	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85098373	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
