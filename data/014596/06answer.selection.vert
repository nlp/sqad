<s>
Částice	částice	k1gFnSc1
(	(	kIx(
<g/>
partikule	partikule	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neohebný	ohebný	k2eNgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
signalizuje	signalizovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
mluvčího	mluvčí	k1gMnSc2
k	k	k7c3
výpovědi	výpověď	k1gFnSc3
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
modalitu	modalita	k1gFnSc4
věty	věta	k1gFnSc2
nebo	nebo	k8xC
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS
větný	větný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
<g/>
.	.	kIx.
</s>