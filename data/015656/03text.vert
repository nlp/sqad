<s>
Twiggy	Twigga	k1gFnPc1
</s>
<s>
Lesley	Lesle	k2eAgFnPc1d1
Hornby	Hornba	k1gFnPc1
Narození	narození	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1949	#num#	k4
(	(	kIx(
<g/>
71	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Neasden	Neasdno	k1gNnPc2
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Další	další	k2eAgNnSc1d1
jména	jméno	k1gNnSc2
</s>
<s>
Twiggy	Twigg	k1gInPc1
Národnost	národnost	k1gFnSc1
</s>
<s>
britská	britský	k2eAgFnSc1d1
Výška	výška	k1gFnSc1
</s>
<s>
168	#num#	k4
cm	cm	kA
Barva	barva	k1gFnSc1
vlasů	vlas	k1gInPc2
</s>
<s>
blonďatá	blonďatý	k2eAgFnSc1d1
Barva	barva	k1gFnSc1
očí	oko	k1gNnPc2
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
Velikost	velikost	k1gFnSc1
oděvu	oděv	k1gInSc2
</s>
<s>
UK	UK	kA
12	#num#	k4
–	–	k?
US	US	kA
10	#num#	k4
–	–	k?
EU	EU	kA
40	#num#	k4
Agentura	agentura	k1gFnSc1
</s>
<s>
Models	Modelsa	k1gFnPc2
1	#num#	k4
Choť	choť	k1gFnSc4
</s>
<s>
Michael	Michael	k1gMnSc1
Witney	Witnea	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1977	#num#	k4
–	–	k?
1983	#num#	k4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
Leigh	Leigh	k1gInSc1
Lawson	Lawson	k1gNnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
Website	Websit	k1gInSc5
</s>
<s>
http://www.twiggylawson.co.uk/	http://www.twiggylawson.co.uk/	k?
</s>
<s>
Lesley	Leslea	k1gFnPc1
Lawson	Lawsona	k1gFnPc2
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Hornby	Hornba	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
veřejně	veřejně	k6eAd1
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
přezdívkou	přezdívka	k1gFnSc7
Twiggy	Twiggy	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
a	a	k8xC
zpěvačka	zpěvačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
prominentním	prominentní	k2eAgFnPc3d1
dospívajícím	dospívající	k2eAgFnPc3d1
britským	britský	k2eAgFnPc3d1
modelkám	modelka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
hýbaly	hýbat	k5eAaImAgInP
Londýnem	Londýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Twiggy	Twigga	k1gFnPc4
byla	být	k5eAaImAgFnS
zpočátku	zpočátku	k6eAd1
známá	známý	k2eAgFnSc1d1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
hubenou	hubený	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
i	i	k9
její	její	k3xOp3gFnSc1
přezdívka	přezdívka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gInSc1
androgynní	androgynní	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
velkých	velký	k2eAgNnPc2d1
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
dlouhých	dlouhý	k2eAgFnPc2d1
řas	řasa	k1gFnPc2
a	a	k8xC
krátkých	krátký	k2eAgInPc2d1
vlasů	vlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
byla	být	k5eAaImAgFnS
jmenována	jmenovat	k5eAaImNgFnS,k5eAaBmNgFnS
"	"	kIx"
<g/>
Tváří	tvářet	k5eAaImIp3nS
roku	rok	k1gInSc2
<g/>
"	"	kIx"
a	a	k8xC
byla	být	k5eAaImAgFnS
zvolenou	zvolený	k2eAgFnSc7d1
i	i	k8xC
Britkou	Britka	k1gFnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
pracovala	pracovat	k5eAaImAgFnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Japonsku	Japonsko	k1gNnSc6
i	i	k8xC
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
a	a	k8xC
objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c6
obálkách	obálka	k1gFnPc6
časopisů	časopis	k1gInPc2
Vogue	Vogue	k1gInSc1
a	a	k8xC
The	The	k1gMnSc1
Tatler	Tatler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
proslulost	proslulost	k1gFnSc4
se	se	k3xPyFc4
šířila	šířit	k5eAaImAgFnS
celosvětově	celosvětově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
konci	konec	k1gInSc6
s	s	k7c7
modelingem	modeling	k1gInSc7
pokračovala	pokračovat	k5eAaImAgFnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
úspěšné	úspěšný	k2eAgFnSc6d1
kariéře	kariéra	k1gFnSc6
jako	jako	k8xS,k8xC
filmová	filmový	k2eAgFnSc1d1
<g/>
,	,	kIx,
televizní	televizní	k2eAgFnSc1d1
a	a	k8xC
divadelní	divadelní	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderovala	moderovat	k5eAaBmAgFnS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
televizní	televizní	k2eAgInSc4d1
pořad	pořad	k1gInSc4
<g/>
,	,	kIx,
Twiggy	Twigg	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
People	People	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
zpovídala	zpovídat	k5eAaImAgFnS
celebrity	celebrita	k1gFnSc2
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
jako	jako	k9
porotkyně	porotkyně	k1gFnSc1
v	v	k7c4
reality	realita	k1gFnPc4
show	show	k1gFnSc2
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Next	Next	k1gInSc1
Top	topit	k5eAaImRp2nS
Model	model	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
kniha	kniha	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
,	,	kIx,
Twiggy	Twigga	k1gFnPc1
in	in	k?
Black	Black	k1gMnSc1
and	and	k?
White	Whit	k1gMnSc5
<g/>
,	,	kIx,
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
rychle	rychle	k6eAd1
bestsellerem	bestseller	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
dělala	dělat	k5eAaImAgFnS
modelku	modelka	k1gFnSc4
pro	pro	k7c4
společnost	společnost	k1gFnSc4
Marks	Marksa	k1gFnPc2
and	and	k?
Spencer	Spencra	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
podporu	podpora	k1gFnSc4
jejich	jejich	k3xOp3gFnPc6
nejnovějších	nový	k2eAgFnPc6d3
kampaních	kampaň	k1gFnPc6
<g/>
,	,	kIx,
objevovala	objevovat	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
tištěných	tištěný	k2eAgFnPc6d1
i	i	k8xC
televizních	televizní	k2eAgFnPc6d1
reklamách	reklama	k1gFnPc6
po	po	k7c6
boku	bok	k1gInSc6
Myleene	Myleen	k1gInSc5
Klass	Klass	k1gInSc1
<g/>
,	,	kIx,
Erin	Erin	k1gInSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Connor	Connor	k1gMnSc1
<g/>
,	,	kIx,
Lily	lít	k5eAaImAgFnP
Cole	cola	k1gFnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
pracovala	pracovat	k5eAaImAgFnS
společně	společně	k6eAd1
s	s	k7c7
návrháři	návrhář	k1gMnPc7
Marks	Marksa	k1gFnPc2
&	&	k?
Spencer	Spencra	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vytvořili	vytvořit	k5eAaPmAgMnP
exkluzivní	exkluzivní	k2eAgFnSc4d1
kolekci	kolekce	k1gFnSc4
dámského	dámský	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
pro	pro	k7c4
tento	tento	k3xDgInSc4
módní	módní	k2eAgInSc4d1
řetězec	řetězec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
The	The	k?
Boy	boy	k1gMnSc1
Friend	Friend	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W	W	kA
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Butterfly	butterfly	k1gInSc1
Ball	Ball	k1gInSc1
and	and	k?
the	the	k?
Grasshopper	Grasshopper	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Feast	Feast	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
There	Ther	k1gMnSc5
Goes	Goes	k1gInSc1
The	The	k1gMnSc5
Bride	Brid	k1gMnSc5
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bratři	bratr	k1gMnPc1
Bluesovi	Bluesův	k2eAgMnPc1d1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Doktor	doktor	k1gMnSc1
a	a	k8xC
ďáblové	ďábel	k1gMnPc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klub	klub	k1gInSc1
ráj	ráj	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Little	Little	k1gFnSc1
Match	Match	k1gInSc1
Girl	girl	k1gFnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Madame	madame	k1gFnSc1
Sousatzká	Sousatzká	k1gFnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Diamond	Diamond	k1gInSc1
Trap	trap	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sun	Sun	kA
Child	Child	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tulák	tulák	k1gMnSc1
Charlie	Charlie	k1gMnSc1
Chaplin	Chaplin	k2eAgMnSc1d1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Istanbul	Istanbul	k1gInSc1
(	(	kIx(
<g/>
Keep	Keep	k1gInSc1
Your	Your	k1gMnSc1
Eyes	Eyesa	k1gFnPc2
Open	Open	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Body	bod	k1gInPc1
Bags	Bagsa	k1gFnPc2
<g/>
:	:	kIx,
Historky	historka	k1gFnPc1
z	z	k7c2
márnice	márnice	k1gFnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vezmeš	vzít	k5eAaPmIp2nS
si	se	k3xPyFc3
mě	já	k3xPp1nSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Edge	Edge	k1gFnSc1
of	of	k?
Seventeen	Seventeen	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Brand	Brand	k1gMnSc1
New	New	k1gMnSc1
World	World	k1gMnSc1
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
hře	hra	k1gFnSc6
Jeffa	Jeff	k1gMnSc2
Noona	Noon	k1gMnSc2
<g/>
,	,	kIx,
Woundings	Woundings	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
Popelka	Popelka	k1gMnSc1
<g/>
,	,	kIx,
Casino	Casina	k1gMnSc5
Theatre	Theatr	k1gMnSc5
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Butterfly	butterfly	k1gInSc1
Ball	Ball	k1gInSc1
and	and	k?
the	the	k?
Grasshopper	Grasshopper	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Feast	Feast	k1gInSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Albert	Albert	k1gMnSc1
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eliza	Eliza	k1gFnSc1
Doolittle	Doolittle	k1gFnSc1
<g/>
,	,	kIx,
Pygmalion	Pygmalion	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Captain	Captain	k1gMnSc1
Beaky	Beaka	k1gFnSc2
and	and	k?
His	his	k1gNnSc1
Musical	musical	k1gInSc1
Christmas	Christmas	k1gMnSc1
(	(	kIx(
<g/>
pantomima	pantomima	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Apollo	Apollo	k1gMnSc1
Victoria	Victorium	k1gNnSc2
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
My	my	k3xPp1nPc1
One	One	k1gMnSc2
and	and	k?
Only	Onla	k1gMnSc2
<g/>
,	,	kIx,
St.	st.	kA
James	James	k1gMnSc1
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Blithe	Blithat	k5eAaPmIp3nS
Spirit	Spirit	k1gInSc1
<g/>
,	,	kIx,
Chichester	Chichester	k1gInSc1
Festival	festival	k1gInSc1
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Noel	Noel	k1gInSc1
and	and	k?
Gertie	Gertie	k1gFnSc1
<g/>
,	,	kIx,
Bay	Bay	k1gMnSc1
Street	Street	k1gMnSc1
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
Long	Long	k1gMnSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
If	If	k?
Love	lov	k1gInSc5
Were	Wer	k1gInSc2
All	All	k1gMnPc4
<g/>
,	,	kIx,
Lucille	Lucill	k1gMnPc4
Lortel	Lortela	k1gFnPc2
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
City	City	k1gFnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Blithe	Blithat	k5eAaPmIp3nS
Spirit	Spirit	k1gInSc1
<g/>
,	,	kIx,
Bay	Bay	k1gMnSc1
Street	Street	k1gMnSc1
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
Long	Long	k1gMnSc1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mrs	Mrs	k?
Warren	Warrno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Profession	Profession	k1gInSc1
<g/>
,	,	kIx,
turné	turné	k1gNnSc1
<g/>
,	,	kIx,
England	England	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Twiggs	Twiggs	k1gInSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Muppet	Muppet	k1gInSc1
Show	show	k1gFnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
epizoda	epizoda	k1gFnSc1
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Victorian	Victorian	k1gInSc1
Scandals	Scandals	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bing	bingo	k1gNnPc2
Crosby	Crosba	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Merrie	Merrie	k1gFnSc2
Olde	Old	k1gMnSc4
Christmas	Christmas	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Donna	donna	k1gFnSc1
Summer	Summer	k1gMnSc1
Special	Special	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
Gift	Gift	k1gMnSc1
of	of	k?
Music	Music	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Princesses	Princesses	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2	#num#	k4
epizody	epizoda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Tales	Tales	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Crypt	Crypt	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1	#num#	k4
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Chůva	chůva	k1gFnSc1
k	k	k7c3
pohledání	pohledání	k1gNnSc3
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1	#num#	k4
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Heartbeat	Heartbeat	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1	#num#	k4
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Absolutely	Absolutela	k1gFnPc1
Fabulous	Fabulous	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
This	This	k1gInSc1
Morning	Morning	k1gInSc1
(	(	kIx(
<g/>
moderátorka	moderátorka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Take	Tak	k1gFnPc1
Time	Tim	k1gInSc2
With	With	k1gInSc1
Twiggy	Twigg	k1gInPc1
(	(	kIx(
<g/>
moderátorka	moderátorka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
America	America	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Next	Next	k1gInSc1
Top	topit	k5eAaImRp2nS
Model	model	k1gInSc1
(	(	kIx(
<g/>
porotce	porotce	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ShakespeaRe-Told	ShakespeaRe-Told	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Taming	Taming	k1gInSc1
of	of	k?
the	the	k?
Shrew	Shrew	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Friday	Frida	k2eAgInPc1d1
Night	Night	k2eAgMnSc1d1
with	with	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
Ross	Ross	k1gInSc1
(	(	kIx(
<g/>
host	host	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Frock	Frock	k1gInSc1
Exchange	Exchang	k1gInPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alan	Alan	k1gMnSc1
Titchmarsh	Titchmarsh	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Walks	Walks	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1
</s>
<s>
The	The	k?
Boy	boy	k1gMnSc1
Friend	Friend	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
and	and	k?
the	the	k?
Girlfriends	Girlfriends	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cole	cola	k1gFnSc3
Porter	porter	k1gInSc1
in	in	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
In	In	k1gFnSc1
My	my	k3xPp1nPc1
Life	Life	k1gFnPc6
<g/>
“	“	k?
(	(	kIx(
<g/>
píseň	píseň	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
The	The	k1gFnSc6
Muppet	Muppeta	k1gFnPc2
Show	show	k1gFnPc2
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Please	Please	k6eAd1
Get	Get	k1gFnSc1
My	my	k3xPp1nPc1
Name	Name	k1gFnSc1
Right	Right	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Captain	Captain	k1gMnSc1
Beaky	Beaka	k1gFnSc2
and	and	k?
His	his	k1gNnSc1
Band	bando	k1gNnPc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pieces	Pieces	k1gMnSc1
of	of	k?
April	April	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
My	my	k3xPp1nPc1
One	One	k1gMnSc2
and	and	k?
Only	Onla	k1gMnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Doctor	Doctor	k1gInSc1
and	and	k?
the	the	k?
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Feel	Feel	k1gInSc1
Emotion	Emotion	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Technocolor	Technocolor	k1gInSc1
Featuring	Featuring	k1gInSc1
Twiggy	Twigga	k1gFnSc2
–	–	k?
Unchained	Unchained	k1gMnSc1
Melody	Meloda	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Boy	boy	k1gMnSc1
Friend	Friend	k1gMnSc1
&	&	k?
Highlights	Highlights	k1gInSc1
from	from	k1gMnSc1
Goodbye	Goodbye	k1gFnSc1
<g/>
,	,	kIx,
Mr	Mr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chips	chips	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
and	and	k?
the	the	k?
Silver	Silver	k1gInSc1
Screen	Screen	k1gInSc1
Syncopaters	Syncopaters	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
London	London	k1gMnSc1
Pride	Prid	k1gInSc5
–	–	k?
Songs	Songs	k1gInSc1
from	from	k1gMnSc1
the	the	k?
London	London	k1gMnSc1
Stage	Stag	k1gMnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Beautiful	Beautiful	k1gInSc1
Dreams	Dreams	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dead	Dead	k1gMnSc1
Man	Man	k1gMnSc1
on	on	k3xPp3gMnSc1
Campus	Campus	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
Twiggy	Twigga	k1gFnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
If	If	k?
Love	lov	k1gInSc5
Were	Were	k1gFnSc7
All	All	k1gMnSc7
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Peter	Peter	k1gMnSc1
Pan	Pan	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Midnight	Midnight	k2eAgInSc1d1
Blue	Blue	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Twiggy	Twigga	k1gFnPc1
&	&	k?
Linda	Linda	k1gFnSc1
Thorson	Thorson	k1gMnSc1
–	–	k?
A	a	k9
Snapshot	Snapshot	k1gMnSc1
of	of	k?
Swinging	Swinging	k1gInSc1
London	London	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gotta	Gotta	k1gMnSc1
Sing	Sing	k1gMnSc1
Gotta	Gotta	k1gMnSc1
Dance	Danka	k1gFnSc3
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Romantically	Romanticall	k1gInPc1
Yours	Yoursa	k1gFnPc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Twiggy	Twigg	k1gInPc1
<g/>
,	,	kIx,
Twiggy	Twigg	k1gInPc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Autobiography	Autobiographa	k1gFnSc2
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-246-10895-1	978-0-246-10895-1	k4
</s>
<s>
Twiggy	Twigga	k1gFnPc1
<g/>
,	,	kIx,
Twiggy	Twigga	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Looking	Looking	k1gInSc4
Good	Good	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-00-636672-0	978-0-00-636672-0	k4
</s>
<s>
Twiggy	Twigga	k1gFnPc1
<g/>
,	,	kIx,
Twiggy	Twigga	k1gFnPc1
in	in	k?
Black	Black	k1gMnSc1
and	and	k?
White	Whit	k1gInSc5
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-671-51645-1	978-0-671-51645-1	k4
</s>
<s>
Emma	Emma	k1gFnSc1
Midgley	Midglea	k1gFnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
London	London	k1gMnSc1
Swings	Swingsa	k1gFnPc2
Again	Again	k1gMnSc1
With	With	k1gMnSc1
Ossie	Ossie	k1gFnSc2
Clark	Clark	k1gInSc1
Show	show	k1gFnSc2
At	At	k1gFnSc2
The	The	k1gFnSc2
V	v	k7c6
<g/>
&	&	k?
<g/>
A	a	k9
<g/>
"	"	kIx"
(	(	kIx(
<g/>
22	#num#	k4
July	Jula	k1gFnSc2
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Culture	Cultur	k1gInSc5
<g/>
24	#num#	k4
</s>
<s>
Twiggy	Twigg	k1gInPc1
<g/>
,	,	kIx,
Twiggy	Twigg	k1gInPc1
<g/>
:	:	kIx,
Please	Pleas	k1gMnSc5
Get	Get	k1gMnSc5
My	my	k3xPp1nPc1
Name	Name	k1gFnSc1
Right	Right	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Word	Word	kA
Power	Power	k1gInSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-4-939102-57-8	978-4-939102-57-8	k4
</s>
<s>
Iain	Iain	k1gMnSc1
R	R	kA
Webb	Webb	k1gMnSc1
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
Gibb	Gibb	k1gMnSc1
<g/>
:	:	kIx,
Fashion	Fashion	k1gInSc1
and	and	k?
Fantasy	fantas	k1gInPc4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
foreword	foreword	k6eAd1
by	by	kYmCp3nP
Twiggy	Twigg	k1gInPc4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-1-85177-548-4	978-1-85177-548-4	k4
</s>
<s>
Twiggy	Twigg	k1gInPc1
<g/>
,	,	kIx,
A	a	k9
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Looking	Looking	k1gInSc1
and	and	k?
Feeling	Feeling	k1gInSc1
Fabulous	Fabulous	k1gMnSc1
Over	Over	k1gMnSc1
Forty	Forty	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-7181-5404-2	978-0-7181-5404-2	k4
</s>
<s>
The	The	k?
Model	model	k1gInSc1
as	as	k1gInSc1
Muse	Musa	k1gFnSc3
<g/>
:	:	kIx,
Embodying	Embodying	k1gInSc1
Fashion	Fashion	k1gInSc1
<g/>
,	,	kIx,
Metropolitan	metropolitan	k1gInSc1
Museum	museum	k1gNnSc1
of	of	k?
Art	Art	k1gMnSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
<g/>
–	–	k?
<g/>
August	August	k1gMnSc1
2009	#num#	k4
</s>
<s>
Twiggy	Twigga	k1gFnPc1
<g/>
:	:	kIx,
A	a	k9
Life	Life	k1gNnSc6
in	in	k?
Photographs	Photographs	k1gInSc1
<g/>
,	,	kIx,
Terence	Terence	k1gFnSc1
Pepper	Pepper	k1gInSc1
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
Muir	Muir	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Melvin	Melvina	k1gFnPc2
Sokolsky	sokolsky	k6eAd1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-1-85514-414-9	978-1-85514-414-9	k4
</s>
<s>
Twiggy	Twigga	k1gFnPc1
<g/>
:	:	kIx,
A	a	k9
Life	Life	k1gNnSc6
in	in	k?
Photographs	Photographs	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gFnSc1
Portrait	Portrait	k1gInSc1
Gallery	Galler	k1gInPc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Twiggy	Twigga	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Twiggy	Twigga	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Snímky	snímek	k1gInPc1
s	s	k7c7
Twiggy	Twigg	k1gInPc7
na	na	k7c6
National	National	k1gFnSc6
Portrait	Portrait	k2eAgInSc4d1
Gallery	Galler	k1gInPc4
</s>
<s>
Twiggy	Twigg	k1gInPc1
na	na	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databas	k1gInSc6
</s>
<s>
Twiggy	Twigga	k1gFnPc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Twiggy	Twigg	k1gInPc1
na	na	k7c4
Internet	Internet	k1gInSc4
Broadway	Broadwaa	k1gFnSc2
Database	Databas	k1gInSc5
</s>
<s>
Twiggy	Twigg	k1gInPc1
na	na	k7c4
Internet	Internet	k1gInSc4
Off-Broadway	Off-Broadwaa	k1gFnSc2
Database	Databas	k1gMnSc5
</s>
<s>
Twiggy	Twigg	k1gInPc1
na	na	k7c4
Fashion	Fashion	k1gInSc4
Model	modla	k1gFnPc2
Directory	Director	k1gInPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
komedie	komedie	k1gFnSc1
/	/	kIx~
muzikál	muzikál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Judy	judo	k1gNnPc7
Hollidayová	Hollidayová	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
June	jun	k1gMnSc5
Allysonová	Allysonová	k1gFnSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Susan	Susan	k1gInSc1
Haywardová	Haywardová	k1gFnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ethel	Ethel	k1gInSc1
Mermanová	Mermanová	k1gFnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Judy	judo	k1gNnPc7
Garlandová	Garlandový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jean	Jean	k1gMnSc1
Simmonsová	Simmonsová	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Deborah	Deborah	k1gInSc1
Kerrová	Kerrová	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kay	Kay	k?
Kendallová	Kendallová	k1gFnSc1
/	/	kIx~
Taina	Taien	k2eAgFnSc1d1
Elgová	Elgová	k1gFnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Monroe	Monroe	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gInSc5
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gInSc5
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Julie	Julie	k1gFnSc1
Andrewsová	Andrewsová	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Julie	Julie	k1gFnSc1
Andrewsová	Andrewsová	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lynn	Lynn	k1gInSc1
Redgraveová	Redgraveová	k1gFnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Anne	Anne	k1gFnSc1
Bancroftová	Bancroftová	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Barbra	Barbra	k6eAd1
Streisandová	Streisandový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Patty	Patt	k1gInPc1
Dukeová	Dukeová	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Twiggy	Twigga	k1gFnPc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Liza	Liza	k6eAd1
Minnelli	Minnell	k1gMnPc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Glenda	Glenda	k1gFnSc1
Jacksonová	Jacksonová	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Raquel	Raquel	k1gInSc1
Welchová	Welchová	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ann-Margret	Ann-Margret	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
145751	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
107463695	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1020	#num#	k4
1226	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50018938	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2661164	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50018938	#num#	k4
</s>
