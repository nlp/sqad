<s>
The	The	k?	The
Beatles	Beatles	k1gFnSc1	Beatles
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
