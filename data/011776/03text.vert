<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1951	[number]	k4	1951
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
statistik	statistik	k1gMnSc1	statistik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
předseda	předseda	k1gMnSc1	předseda
tzv.	tzv.	kA	tzv.
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nestranický	stranický	k2eNgMnSc1d1	nestranický
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
favorita	favorit	k1gMnSc4	favorit
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
postupem	postup	k1gInSc7	postup
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
začal	začít	k5eAaPmAgInS	začít
ztrácet	ztrácet	k5eAaImF	ztrácet
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
16,35	[number]	k4	16,35
%	%	kIx~	%
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
získal	získat	k5eAaPmAgMnS	získat
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Federálním	federální	k2eAgInSc6d1	federální
statistickém	statistický	k2eAgInSc6d1	statistický
úřadě	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kvůli	kvůli	k7c3	kvůli
služebnímu	služební	k2eAgInSc3d1	služební
postupu	postup	k1gInSc3	postup
r.	r.	kA	r.
1980	[number]	k4	1980
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
a	a	k8xC	a
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
předsedou	předseda	k1gMnSc7	předseda
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
premiér	premiér	k1gMnSc1	premiér
do	do	k7c2	do
konce	konec	k1gInSc2	konec
českého	český	k2eAgNnSc2d1	české
předsednictví	předsednictví	k1gNnSc2	předsednictví
EU	EU	kA	EU
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
také	také	k9	také
předsedal	předsedat	k5eAaImAgInS	předsedat
Radě	rada	k1gFnSc3	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2010	[number]	k4	2010
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
2012	[number]	k4	2012
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
viceprezident	viceprezident	k1gMnSc1	viceprezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Fischerův	Fischerův	k2eAgMnSc1d1	Fischerův
otec	otec	k1gMnSc1	otec
RNDr.	RNDr.	kA	RNDr.
Otto	Otto	k1gMnSc1	Otto
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
pojistnou	pojistný	k2eAgFnSc4d1	pojistná
matematiku	matematika	k1gFnSc4	matematika
<g/>
;	;	kIx,	;
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jako	jako	k8xS	jako
žid	žid	k1gMnSc1	žid
prošel	projít	k5eAaPmAgMnS	projít
ghetto	ghetto	k1gNnSc4	ghetto
Terezín	Terezín	k1gInSc1	Terezín
<g/>
,	,	kIx,	,
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
Auschwitz-Birkenau	Auschwitz-Birkena	k2eAgFnSc4d1	Auschwitz-Birkena
a	a	k8xC	a
kladskou	kladský	k2eAgFnSc4d1	Kladská
pobočku	pobočka	k1gFnSc4	pobočka
tábora	tábor	k1gInSc2	tábor
Gross-Rosen	Gross-Rosen	k2eAgInSc1d1	Gross-Rosen
<g/>
;	;	kIx,	;
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
přežil	přežít	k5eAaPmAgMnS	přežít
holokaust	holokaust	k1gInSc4	holokaust
pouze	pouze	k6eAd1	pouze
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
matematicko-statistickými	matematickotatistický	k2eAgFnPc7d1	matematicko-statistická
aplikacemi	aplikace	k1gFnPc7	aplikace
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
v	v	k7c6	v
Matematickém	matematický	k2eAgInSc6d1	matematický
ústavu	ústav	k1gInSc6	ústav
ČSAV	ČSAV	kA	ČSAV
a	a	k8xC	a
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
zejména	zejména	k9	zejména
analýzu	analýza	k1gFnSc4	analýza
rozptylu	rozptyl	k1gInSc2	rozptyl
na	na	k7c4	na
MFF	MFF	kA	MFF
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
RNDr.	RNDr.	kA	RNDr.
Věra	Věra	k1gFnSc1	Věra
Fischerová	Fischerová	k1gFnSc1	Fischerová
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
také	také	k9	také
statistička	statistička	k1gFnSc1	statistička
z	z	k7c2	z
původně	původně	k6eAd1	původně
katolické	katolický	k2eAgFnSc2d1	katolická
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nehalachický	halachický	k2eNgMnSc1d1	halachický
Žid	Žid	k1gMnSc1	Žid
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
a	a	k8xC	a
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
židovství	židovství	k1gNnSc1	židovství
dědí	dědit	k5eAaImIp3nS	dědit
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
Fischer	Fischer	k1gMnSc1	Fischer
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ivanou	Ivana	k1gFnSc7	Ivana
Janeckou	Janecká	k1gFnSc7	Janecká
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
dětská	dětský	k2eAgFnSc1d1	dětská
sestra	sestra	k1gFnSc1	sestra
<g/>
;	;	kIx,	;
syn	syn	k1gMnSc1	syn
Jakub	Jakub	k1gMnSc1	Jakub
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
docentem	docent	k1gMnSc7	docent
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
statistiky	statistika	k1gFnSc2	statistika
a	a	k8xC	a
prorektorem	prorektor	k1gMnSc7	prorektor
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Kateřina	Kateřina	k1gFnSc1	Kateřina
Drbohlavová	Drbohlavový	k2eAgFnSc1d1	Drbohlavová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
pracovala	pracovat	k5eAaImAgFnS	pracovat
ve	v	k7c6	v
vzdělávací	vzdělávací	k2eAgFnSc6d1	vzdělávací
společnosti	společnost	k1gFnSc6	společnost
Scio	Scio	k6eAd1	Scio
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mateřské	mateřský	k2eAgFnSc6d1	mateřská
dovolené	dovolená	k1gFnSc6	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
Fischer	Fischer	k1gMnSc1	Fischer
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
a	a	k8xC	a
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
s	s	k7c7	s
rozvedenou	rozvedený	k2eAgFnSc7d1	rozvedená
Danielou	Daniela	k1gFnSc7	Daniela
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Ulrichovou	Ulrichová	k1gFnSc4	Ulrichová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
skotské	skotský	k2eAgFnSc6d1	skotská
univerzitě	univerzita	k1gFnSc6	univerzita
St	St	kA	St
Andrews	Andrews	k1gInSc1	Andrews
<g/>
,	,	kIx,	,
a	a	k8xC	a
fenku	fenka	k1gFnSc4	fenka
zlatého	zlatý	k2eAgNnSc2d1	Zlaté
retrievera	retrievero	k1gNnSc2	retrievero
(	(	kIx(	(
<g/>
*	*	kIx~	*
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
věřícího	věřící	k1gMnSc4	věřící
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
oslovil	oslovit	k5eAaPmAgMnS	oslovit
judaismus	judaismus	k1gInSc4	judaismus
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
k	k	k7c3	k
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
mluví	mluvit	k5eAaImIp3nS	mluvit
anglicky	anglicky	k6eAd1	anglicky
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Respektu	respekt	k1gInSc2	respekt
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
politice	politika	k1gFnSc6	politika
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
angličtinářům	angličtinář	k1gMnPc3	angličtinář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
nominaci	nominace	k1gFnSc6	nominace
na	na	k7c6	na
premiéra	premiér	k1gMnSc2	premiér
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
až	až	k9	až
1989	[number]	k4	1989
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nikdy	nikdy	k6eAd1	nikdy
netajil	tajit	k5eNaImAgMnS	tajit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
životopisech	životopis	k1gInPc6	životopis
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životopise	životopis	k1gInSc6	životopis
na	na	k7c6	na
webu	web	k1gInSc6	web
ČSÚ	ČSÚ	kA	ČSÚ
i	i	k8xC	i
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
jmenování	jmenování	k1gNnSc6	jmenování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
to	ten	k3xDgNnSc1	ten
však	však	k9	však
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
nebylo	být	k5eNaImAgNnS	být
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc4	ten
neuvedl	uvést	k5eNaPmAgMnS	uvést
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hesle	heslo	k1gNnSc6	heslo
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
kampani	kampaň	k1gFnSc6	kampaň
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
absolventů	absolvent	k1gMnPc2	absolvent
mohl	moct	k5eAaImAgInS	moct
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
akademické	akademický	k2eAgFnSc3d1	akademická
práci	práce	k1gFnSc3	práce
<g/>
"	"	kIx"	"
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
;	;	kIx,	;
přijal	přijmout	k5eAaPmAgInS	přijmout
až	až	k9	až
třetí	třetí	k4xOgFnSc4	třetí
výzvu	výzva	k1gFnSc4	výzva
"	"	kIx"	"
<g/>
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
<g/>
,	,	kIx,	,
ze	z	k7c2	z
pokud	pokud	k6eAd1	pokud
odmítnu	odmítnout	k5eAaPmIp1nS	odmítnout
<g/>
,	,	kIx,	,
nebudu	být	k5eNaImBp1nS	být
moci	moct	k5eAaImF	moct
dělat	dělat	k5eAaImF	dělat
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
a	a	k8xC	a
z	z	k7c2	z
ústavu	ústav	k1gInSc2	ústav
budu	být	k5eAaImBp1nS	být
nucen	nucen	k2eAgInSc1d1	nucen
odejít	odejít	k5eAaPmF	odejít
nebo	nebo	k8xC	nebo
mě	já	k3xPp1nSc4	já
přeřadí	přeřadit	k5eAaPmIp3nS	přeřadit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
práci	práce	k1gFnSc4	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistik	statistika	k1gFnPc2	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
matematické	matematický	k2eAgNnSc4d1	matematické
gymnázium	gymnázium	k1gNnSc4	gymnázium
Wilhelma	Wilhelmum	k1gNnSc2	Wilhelmum
Piecka	Piecko	k1gNnSc2	Piecko
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
r.	r.	kA	r.
1974	[number]	k4	1974
obor	obora	k1gFnPc2	obora
statistika	statistika	k1gFnSc1	statistika
a	a	k8xC	a
ekonometrie	ekonometrie	k1gFnSc1	ekonometrie
na	na	k7c6	na
Národohospodářské	národohospodářský	k2eAgFnSc6d1	Národohospodářská
fakultě	fakulta	k1gFnSc6	fakulta
VŠE	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
sociálně	sociálně	k6eAd1	sociálně
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
Federálního	federální	k2eAgInSc2d1	federální
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
odborným	odborný	k2eAgMnSc7d1	odborný
referentem	referent	k1gMnSc7	referent
FSÚ	FSÚ	kA	FSÚ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
postgraduální	postgraduální	k2eAgNnSc4d1	postgraduální
studium	studium	k1gNnSc4	studium
titulem	titul	k1gInSc7	titul
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
statistika	statistika	k1gFnSc1	statistika
prací	práce	k1gFnPc2	práce
Statistické	statistický	k2eAgFnSc2d1	statistická
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
soubory	soubor	k1gInPc4	soubor
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc4	nástroj
integrace	integrace	k1gFnSc2	integrace
statistického	statistický	k2eAgInSc2d1	statistický
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ředitelem	ředitel	k1gMnSc7	ředitel
odboru	odbor	k1gInSc2	odbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
náměstkem	náměstek	k1gMnSc7	náměstek
předsedy	předseda	k1gMnSc2	předseda
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
FSÚ	FSÚ	kA	FSÚ
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Český	český	k2eAgInSc4d1	český
statistický	statistický	k2eAgInSc4d1	statistický
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vrchního	vrchní	k2eAgMnSc2d1	vrchní
ředitele	ředitel	k1gMnSc2	ředitel
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vedl	vést	k5eAaImAgMnS	vést
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpracovával	zpracovávat	k5eAaImAgInS	zpracovávat
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
tak	tak	k6eAd1	tak
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
televizních	televizní	k2eAgNnPc6d1	televizní
volebních	volební	k2eAgNnPc6d1	volební
studiích	studio	k1gNnPc6	studio
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
předseda	předseda	k1gMnSc1	předseda
ČSÚ	ČSÚ	kA	ČSÚ
Edvard	Edvard	k1gMnSc1	Edvard
Outrata	outrata	k1gMnSc1	outrata
ho	on	k3xPp3gMnSc4	on
pověřil	pověřit	k5eAaPmAgMnS	pověřit
vystupováním	vystupování	k1gNnSc7	vystupování
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
působnosti	působnost	k1gFnSc2	působnost
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
kontakt	kontakt	k1gInSc1	kontakt
se	s	k7c7	s
statistickým	statistický	k2eAgInSc7d1	statistický
úřadem	úřad	k1gInSc7	úřad
EU	EU	kA	EU
Eurostatem	Eurostat	k1gInSc7	Eurostat
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
statistik	statistika	k1gFnPc2	statistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Outratově	Outratův	k2eAgInSc6d1	Outratův
odchodu	odchod	k1gInSc6	odchod
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
Fischer	Fischer	k1gMnSc1	Fischer
jako	jako	k9	jako
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
ČSÚ	ČSÚ	kA	ČSÚ
předním	přední	k2eAgMnSc7d1	přední
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
však	však	k9	však
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Marii	Maria	k1gFnSc4	Maria
Bohatou	bohatý	k2eAgFnSc4d1	bohatá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
nepracovala	pracovat	k5eNaImAgFnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
Fischera	Fischer	k1gMnSc2	Fischer
v	v	k7c6	v
září	září	k1gNnSc6	září
2000	[number]	k4	2000
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zdůvodnila	zdůvodnit	k5eAaPmAgFnS	zdůvodnit
"	"	kIx"	"
<g/>
nedostatky	nedostatek	k1gInPc4	nedostatek
v	v	k7c6	v
manažerské	manažerský	k2eAgFnSc6d1	manažerská
práci	práce	k1gFnSc6	práce
<g/>
"	"	kIx"	"
a	a	k8xC	a
přípravě	příprava	k1gFnSc3	příprava
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
odlišné	odlišný	k2eAgInPc4d1	odlišný
názory	názor	k1gInPc4	názor
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
řízení	řízení	k1gNnSc2	řízení
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
oddělení	oddělení	k1gNnSc2	oddělení
produkce	produkce	k1gFnSc2	produkce
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
trhu	trh	k1gInSc2	trh
Taylor	Taylor	k1gMnSc1	Taylor
Nelson	Nelson	k1gMnSc1	Nelson
Sofres	Sofres	k1gInSc4	Sofres
Factum	Factum	k1gNnSc1	Factum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2001	[number]	k4	2001
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
mise	mise	k1gFnSc2	mise
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
možnost	možnost	k1gFnSc4	možnost
vybudování	vybudování	k1gNnSc2	vybudování
statistické	statistický	k2eAgFnSc2d1	statistická
služby	služba	k1gFnSc2	služba
na	na	k7c6	na
Východním	východní	k2eAgInSc6d1	východní
Timoru	Timor	k1gInSc6	Timor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k1gMnPc3	vedoucí
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
pracovišť	pracoviště	k1gNnPc2	pracoviště
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
informatiky	informatika	k1gFnSc2	informatika
a	a	k8xC	a
statistiky	statistika	k1gFnPc4	statistika
VŠE	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
nucena	nucen	k2eAgFnSc1d1	nucena
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
v	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
bilanci	bilance	k1gFnSc6	bilance
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2002	[number]	k4	2002
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
ji	on	k3xPp3gFnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
nahradit	nahradit	k5eAaPmF	nahradit
Fischerem	Fischer	k1gMnSc7	Fischer
<g/>
;	;	kIx,	;
jelikož	jelikož	k8xS	jelikož
však	však	k9	však
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
předsedu	předseda	k1gMnSc4	předseda
ČSÚ	ČSÚ	kA	ČSÚ
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Fischer	Fischer	k1gMnSc1	Fischer
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
až	až	k9	až
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g />
.	.	kIx.	.
</s>
<s>
2003	[number]	k4	2003
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Fischerově	Fischerův	k2eAgNnSc6d1	Fischerovo
funkčním	funkční	k2eAgNnSc6d1	funkční
období	období	k1gNnSc6	období
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
ČSÚ	ČSÚ	kA	ČSÚ
za	za	k7c4	za
přidělení	přidělení	k1gNnSc4	přidělení
zakázek	zakázka	k1gFnPc2	zakázka
na	na	k7c4	na
úklid	úklid	k1gInSc4	úklid
<g/>
,	,	kIx,	,
ostrahu	ostraha	k1gFnSc4	ostraha
a	a	k8xC	a
stěhování	stěhování	k1gNnSc4	stěhování
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
46	[number]	k4	46
miliónů	milión	k4xCgInPc2	milión
Kč	Kč	kA	Kč
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jeho	jeho	k3xOp3gNnSc4	jeho
staré	starý	k2eAgNnSc4d1	staré
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c6	na
Invalidovně	invalidovna	k1gFnSc6	invalidovna
<g />
.	.	kIx.	.
</s>
<s>
vytopila	vytopit	k5eAaPmAgFnS	vytopit
povodeň	povodeň	k1gFnSc1	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
<g/>
Fischerova	Fischerův	k2eAgFnSc1d1	Fischerova
vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
posledním	poslední	k2eAgNnSc6d1	poslední
řádném	řádný	k2eAgNnSc6d1	řádné
zasedání	zasedání	k1gNnSc6	zasedání
před	před	k7c7	před
demisí	demise	k1gFnSc7	demise
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
jednohlasně	jednohlasně	k6eAd1	jednohlasně
přijala	přijmout	k5eAaPmAgFnS	přijmout
Fischerův	Fischerův	k2eAgInSc4d1	Fischerův
dodatečně	dodatečně	k6eAd1	dodatečně
na	na	k7c4	na
program	program	k1gInSc4	program
doplněný	doplněný	k2eAgInSc4d1	doplněný
návrh	návrh	k1gInSc4	návrh
jmenovat	jmenovat	k5eAaBmF	jmenovat
novou	nový	k2eAgFnSc7d1	nová
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
ČSÚ	ČSÚ	kA	ČSÚ
docentku	docentka	k1gFnSc4	docentka
VŠE	všechen	k3xTgNnSc4	všechen
Ivu	Iva	k1gFnSc4	Iva
Ritschelovou	Ritschelová	k1gFnSc4	Ritschelová
(	(	kIx(	(
<g/>
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
Fischera	Fischer	k1gMnSc2	Fischer
<g />
.	.	kIx.	.
</s>
<s>
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Bartuška	Bartuška	k1gMnSc1	Bartuška
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
zmocněncem	zmocněnec	k1gMnSc7	zmocněnec
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
Ritschelovou	Ritschelová	k1gFnSc4	Ritschelová
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
a	a	k8xC	a
odvolal	odvolat	k5eAaPmAgMnS	odvolat
Fischera	Fischer	k1gMnSc4	Fischer
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
<g/>
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
mj.	mj.	kA	mj.
České	český	k2eAgFnSc2d1	Česká
statistické	statistický	k2eAgFnSc2d1	statistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
statistického	statistický	k2eAgInSc2d1	statistický
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
ISI	ISI	kA	ISI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
VŠE	všechen	k3xTgNnSc1	všechen
a	a	k8xC	a
vědecké	vědecký	k2eAgFnPc1d1	vědecká
rady	rada	k1gFnSc2	rada
Univerzity	univerzita	k1gFnSc2	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyjádření	vyjádření	k1gNnSc6	vyjádření
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
vládě	vláda	k1gFnSc6	vláda
Mirka	Mirek	k1gMnSc4	Mirek
Topolánka	Topolánek	k1gMnSc4	Topolánek
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
koalice	koalice	k1gFnSc2	koalice
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
strany	strana	k1gFnSc2	strana
následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
dočasné	dočasný	k2eAgFnSc2d1	dočasná
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednání	jednání	k1gNnSc6	jednání
o	o	k7c6	o
podrobnostech	podrobnost	k1gFnPc6	podrobnost
zveřejnily	zveřejnit	k5eAaPmAgFnP	zveřejnit
večer	večer	k6eAd1	večer
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
navrhnou	navrhnout	k5eAaPmIp3nP	navrhnout
Fischera	Fischer	k1gMnSc2	Fischer
jako	jako	k8xC	jako
premiéra	premiér	k1gMnSc2	premiér
"	"	kIx"	"
<g/>
překlenovací	překlenovací	k2eAgFnSc2d1	překlenovací
vlády	vláda	k1gFnSc2	vláda
složené	složený	k2eAgFnSc6d1	složená
z	z	k7c2	z
nestranických	stranický	k2eNgMnPc2d1	nestranický
odborníků	odborník	k1gMnPc2	odborník
<g/>
"	"	kIx"	"
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
jmenování	jmenování	k1gNnSc4	jmenování
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
po	po	k7c6	po
klíčovém	klíčový	k2eAgInSc6d1	klíčový
summitu	summit	k1gInSc6	summit
EU	EU	kA	EU
o	o	k7c6	o
Východním	východní	k2eAgNnSc6d1	východní
partnerství	partnerství	k1gNnSc6	partnerství
s	s	k7c7	s
postsovětskými	postsovětský	k2eAgFnPc7d1	postsovětská
zeměmi	zem	k1gFnPc7	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
koalice	koalice	k1gFnSc1	koalice
mu	on	k3xPp3gMnSc3	on
doporučí	doporučit	k5eAaPmIp3nS	doporučit
8	[number]	k4	8
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
přijmou	přijmout	k5eAaPmIp3nP	přijmout
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
předčasné	předčasný	k2eAgFnPc1d1	předčasná
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
sobotu	sobota	k1gFnSc4	sobota
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Fischer	Fischer	k1gMnSc1	Fischer
chtěl	chtít	k5eAaImAgMnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c6	na
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
podmínil	podmínit	k5eAaPmAgMnS	podmínit
své	svůj	k3xOyFgNnSc4	svůj
jmenování	jmenování	k1gNnSc4	jmenování
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
guvernér	guvernér	k1gMnSc1	guvernér
ČNB	ČNB	kA	ČNB
Josef	Josef	k1gMnSc1	Josef
Tošovský	Tošovský	k1gMnSc1	Tošovský
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
funkce	funkce	k1gFnPc1	funkce
předsedy	předseda	k1gMnSc2	předseda
ČSÚ	ČSÚ	kA	ČSÚ
jako	jako	k8xS	jako
jiného	jiný	k2eAgInSc2d1	jiný
ústředního	ústřední	k2eAgInSc2d1	ústřední
orgánu	orgán	k1gInSc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
není	být	k5eNaImIp3nS	být
slučitelná	slučitelný	k2eAgFnSc1d1	slučitelná
s	s	k7c7	s
členstvím	členství	k1gNnSc7	členství
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
řízením	řízení	k1gNnSc7	řízení
ČSÚ	ČSÚ	kA	ČSÚ
byl	být	k5eAaImAgMnS	být
mezitím	mezitím	k6eAd1	mezitím
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
pověřen	pověřit	k5eAaPmNgMnS	pověřit
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
místopředsedů	místopředseda	k1gMnPc2	místopředseda
Jiří	Jiří	k1gMnSc1	Jiří
Křovák	Křovák	k1gMnSc1	Křovák
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
nebyl	být	k5eNaImAgMnS	být
mezi	mezi	k7c4	mezi
tipy	tip	k1gInPc4	tip
na	na	k7c4	na
premiéra	premiér	k1gMnSc4	premiér
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
jednání	jednání	k1gNnSc2	jednání
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
křeslo	křesnout	k5eAaPmAgNnS	křesnout
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
premiér	premiér	k1gMnSc1	premiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
služebně	služebně	k6eAd1	služebně
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Topolánek	Topolánek	k1gMnSc1	Topolánek
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fischera	Fischer	k1gMnSc2	Fischer
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
"	"	kIx"	"
<g/>
kolegů	kolega	k1gMnPc2	kolega
z	z	k7c2	z
koalice	koalice	k1gFnSc2	koalice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kdo	kdo	k3yQnSc1	kdo
konkrétně	konkrétně	k6eAd1	konkrétně
<g/>
,	,	kIx,	,
neuvedl	uvést	k5eNaPmAgMnS	uvést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řada	řada	k1gFnSc1	řada
ministrů	ministr	k1gMnPc2	ministr
u	u	k7c2	u
jednání	jednání	k1gNnSc2	jednání
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
distancovala	distancovat	k5eAaBmAgFnS	distancovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
jeho	jeho	k3xOp3gFnPc4	jeho
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
můj	můj	k3xOp1gInSc1	můj
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
<g/>
,	,	kIx,	,
přivlastnil	přivlastnit	k5eAaPmAgMnS	přivlastnit
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc4	ten
trochu	trochu	k6eAd1	trochu
byla	být	k5eAaImAgFnS	být
moje	můj	k3xOp1gFnSc1	můj
slepota	slepota	k1gFnSc1	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
na	na	k7c6	na
schůzích	schůze	k1gFnPc6	schůze
vlády	vláda	k1gFnSc2	vláda
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
sedí	sedit	k5eAaImIp3nS	sedit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
normálně	normálně	k6eAd1	normálně
rozpravy	rozprava	k1gFnSc2	rozprava
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Zná	znát	k5eAaImIp3nS	znát
celou	celý	k2eAgFnSc4d1	celá
agendu	agenda	k1gFnSc4	agenda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
evropských	evropský	k2eAgFnPc2d1	Evropská
i	i	k8xC	i
světových	světový	k2eAgFnPc2d1	světová
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nemá	mít	k5eNaImIp3nS	mít
problém	problém	k1gInSc4	problém
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
<g/>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
jeho	jeho	k3xOp3gInSc4	jeho
výběr	výběr	k1gInSc4	výběr
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
padala	padat	k5eAaImAgNnP	padat
nejrůznější	různý	k2eAgNnPc1d3	nejrůznější
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mě	já	k3xPp1nSc4	já
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
nenapadlo	napadnout	k5eNaPmAgNnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
včera	včera	k6eAd1	včera
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Inženýra	inženýr	k1gMnSc2	inženýr
Fischera	Fischer	k1gMnSc2	Fischer
znám	znát	k5eAaImIp1nS	znát
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
při	při	k7c6	při
nějakých	nějaký	k3yIgFnPc6	nějaký
statistických	statistický	k2eAgFnPc6d1	statistická
debatách	debata	k1gFnPc6	debata
a	a	k8xC	a
konferencích	konference	k1gFnPc6	konference
setkávali	setkávat	k5eAaImAgMnP	setkávat
už	už	k6eAd1	už
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
chalupu	chalupa	k1gFnSc4	chalupa
někde	někde	k6eAd1	někde
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Potkávali	potkávat	k5eAaImAgMnP	potkávat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
milé	milý	k2eAgFnSc6d1	Milá
hospůdce	hospůdka	k1gFnSc6	hospůdka
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
tam	tam	k6eAd1	tam
všichni	všechen	k3xTgMnPc1	všechen
jeli	jet	k5eAaImAgMnP	jet
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
na	na	k7c4	na
chalupy	chalupa	k1gFnPc4	chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
uvážlivý	uvážlivý	k2eAgMnSc1d1	uvážlivý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
nijak	nijak	k6eAd1	nijak
politicky	politicky	k6eAd1	politicky
radikálně	radikálně	k6eAd1	radikálně
orientován	orientovat	k5eAaBmNgInS	orientovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
či	či	k8xC	či
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
na	na	k7c4	na
dotazy	dotaz	k1gInPc4	dotaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chaty	chata	k1gFnPc4	chata
máme	mít	k5eAaImIp1nP	mít
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
jedna	jeden	k4xCgFnSc1	jeden
malá	malý	k2eAgFnSc1d1	malá
hospůdka	hospůdka	k1gFnSc1	hospůdka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
máme	mít	k5eAaImIp1nP	mít
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
potkávali	potkávat	k5eAaImAgMnP	potkávat
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
náhodou	náhodou	k6eAd1	náhodou
<g/>
.	.	kIx.	.
</s>
<s>
Pohovořili	pohovořit	k5eAaPmAgMnP	pohovořit
jsme	být	k5eAaImIp1nP	být
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
rozešli	rozejít	k5eAaPmAgMnP	rozejít
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Já	já	k3xPp1nSc1	já
dělal	dělat	k5eAaImAgMnS	dělat
statistiky	statistika	k1gFnPc4	statistika
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
se	se	k3xPyFc4	se
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
odborných	odborný	k2eAgFnPc2d1	odborná
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsme	být	k5eAaImIp1nP	být
měli	mít	k5eAaImAgMnP	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
pedagogice	pedagogika	k1gFnSc6	pedagogika
a	a	k8xC	a
takové	takový	k3xDgFnSc2	takový
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
nepotkali	potkat	k5eNaPmAgMnP	potkat
<g/>
.	.	kIx.	.
<g/>
Grémia	grémium	k1gNnPc4	grémium
malých	malý	k2eAgFnPc2d1	malá
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
dohodu	dohoda	k1gFnSc4	dohoda
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
Topolánek	Topolánek	k1gInSc1	Topolánek
ale	ale	k8xC	ale
začal	začít	k5eAaPmAgInS	začít
sbírat	sbírat	k5eAaImF	sbírat
podpisy	podpis	k1gInPc4	podpis
poslanců	poslanec	k1gMnPc2	poslanec
ODS	ODS	kA	ODS
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
slibujících	slibující	k2eAgFnPc2d1	slibující
dát	dát	k5eAaPmF	dát
Fischerově	Fischerův	k2eAgFnSc3d1	Fischerova
vládě	vláda	k1gFnSc3	vláda
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
před	před	k7c4	před
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
odpolední	odpolední	k2eAgMnSc1d1	odpolední
Klaus	Klaus	k1gMnSc1	Klaus
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nazítří	nazítří	k1gNnSc1	nazítří
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Fischera	Fischer	k1gMnSc2	Fischer
premiérem	premiér	k1gMnSc7	premiér
<g/>
;	;	kIx,	;
v	v	k7c6	v
16.30	[number]	k4	16.30
Fischer	Fischer	k1gMnSc1	Fischer
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dopoledne	dopoledne	k6eAd1	dopoledne
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Topolánkem	Topolánek	k1gInSc7	Topolánek
a	a	k8xC	a
Paroubkem	Paroubek	k1gInSc7	Paroubek
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
ho	on	k3xPp3gNnSc4	on
informovali	informovat	k5eAaBmAgMnP	informovat
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
stvrzené	stvrzený	k2eAgFnSc2d1	stvrzená
i	i	k9	i
Kanceláří	kancelář	k1gFnSc7	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g />
.	.	kIx.	.
</s>
<s>
ji	on	k3xPp3gFnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
zahájit	zahájit	k5eAaPmF	zahájit
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
velikonocích	velikonoce	k1gFnPc6	velikonoce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
<g/>
Prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
ve	v	k7c4	v
14.00	[number]	k4	14.00
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Fischera	Fischer	k1gMnSc2	Fischer
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
ho	on	k3xPp3gNnSc4	on
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
dva	dva	k4xCgMnPc4	dva
premiéry	premiér	k1gMnPc4	premiér
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
výslovně	výslovně	k6eAd1	výslovně
neupravuje	upravovat	k5eNaImIp3nS	upravovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
postupovalo	postupovat	k5eAaImAgNnS	postupovat
už	už	k9	už
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
vládních	vládní	k2eAgFnPc6d1	vládní
krizích	krize	k1gFnPc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
odpoledne	odpoledne	k1gNnSc2	odpoledne
se	se	k3xPyFc4	se
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
objevila	objevit	k5eAaPmAgFnS	objevit
jména	jméno	k1gNnSc2	jméno
většiny	většina	k1gFnSc2	většina
doporučených	doporučený	k2eAgMnPc2d1	doporučený
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
Topolánkem	Topolánek	k1gInSc7	Topolánek
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
s	s	k7c7	s
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
ministry	ministr	k1gMnPc4	ministr
a	a	k8xC	a
s	s	k7c7	s
končícími	končící	k2eAgMnPc7d1	končící
ministry	ministr	k1gMnPc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jmenování	jmenování	k1gNnSc2	jmenování
vlády	vláda	k1gFnSc2	vláda
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
měl	mít	k5eAaImAgMnS	mít
Fischer	Fischer	k1gMnSc1	Fischer
kancelář	kancelář	k1gFnSc4	kancelář
v	v	k7c6	v
Hrzánském	Hrzánský	k2eAgInSc6d1	Hrzánský
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fischerovo	Fischerův	k2eAgNnSc4d1	Fischerovo
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
skoro	skoro	k6eAd1	skoro
bez	bez	k7c2	bez
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
jej	on	k3xPp3gMnSc4	on
napadly	napadnout	k5eAaPmAgFnP	napadnout
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
průzkumu	průzkum	k1gInSc2	průzkum
Fischerovo	Fischerův	k2eAgNnSc1d1	Fischerovo
členství	členství	k1gNnSc1	členství
nevadilo	vadit	k5eNaImAgNnS	vadit
63	[number]	k4	63
%	%	kIx~	%
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Respektu	respekt	k1gInSc2	respekt
byl	být	k5eAaImAgMnS	být
Fischer	Fischer	k1gMnSc1	Fischer
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jako	jako	k9	jako
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětloval	vysvětlovat	k5eAaImAgInS	vysvětlovat
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gNnSc7	jeho
civilním	civilní	k2eAgNnSc7d1	civilní
a	a	k8xC	a
slušným	slušný	k2eAgInSc7d1	slušný
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k8xC	i
Daniel	Daniel	k1gMnSc1	Daniel
Kunštát	Kunštát	k1gInSc1	Kunštát
z	z	k7c2	z
Centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obvinění	obvinění	k1gNnPc4	obvinění
z	z	k7c2	z
vazeb	vazba	k1gFnPc2	vazba
na	na	k7c4	na
uhelnou	uhelný	k2eAgFnSc4d1	uhelná
lobby	lobby	k1gFnSc4	lobby
===	===	k?	===
</s>
</p>
<p>
<s>
Vicepremiér	vicepremiér	k1gMnSc1	vicepremiér
padlé	padlý	k2eAgFnSc2d1	padlá
Topolánkovy	Topolánkův	k2eAgFnSc2d1	Topolánkova
vlády	vláda	k1gFnSc2	vláda
Martin	Martin	k1gMnSc1	Martin
Bursík	Bursík	k1gMnSc1	Bursík
(	(	kIx(	(
<g/>
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
)	)	kIx)	)
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
při	při	k7c6	při
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
novelu	novela	k1gFnSc4	novela
horního	horní	k2eAgInSc2d1	horní
zákona	zákon	k1gInSc2	zákon
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Fischera	Fischer	k1gMnSc2	Fischer
z	z	k7c2	z
podlehnutí	podlehnutí	k1gNnSc2	podlehnutí
uhelné	uhelný	k2eAgFnSc2d1	uhelná
lobby	lobby	k1gFnSc2	lobby
s	s	k7c7	s
náznaky	náznak	k1gInPc7	náznak
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
vazby	vazba	k1gFnPc4	vazba
mezi	mezi	k7c7	mezi
uhelnou	uhelný	k2eAgFnSc7d1	uhelná
společností	společnost	k1gFnSc7	společnost
Czech	Czecha	k1gFnPc2	Czecha
Coal	Coala	k1gFnPc2	Coala
a	a	k8xC	a
Fischerovým	Fischerův	k2eAgMnSc7d1	Fischerův
poradcem	poradce	k1gMnSc7	poradce
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Mlynářem	Mlynář	k1gMnSc7	Mlynář
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Fischerem	Fischer	k1gMnSc7	Fischer
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Czech	Czech	k1gMnSc1	Czech
Coal	Coal	k1gMnSc1	Coal
Janem	Jan	k1gMnSc7	Jan
Dobrovským	Dobrovský	k1gMnSc7	Dobrovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
pro	pro	k7c4	pro
slabost	slabost	k1gFnSc4	slabost
===	===	k?	===
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
začal	začít	k5eAaPmAgMnS	začít
Fischera	Fischer	k1gMnSc4	Fischer
brzo	brzo	k6eAd1	brzo
kritizovat	kritizovat	k5eAaImF	kritizovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
prodloužení	prodloužení	k1gNnSc4	prodloužení
mandátu	mandát	k1gInSc2	mandát
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proslulý	proslulý	k2eAgInSc1d1	proslulý
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
,	,	kIx,	,
vytýkal	vytýkat	k5eAaImAgInS	vytýkat
Fischerovi	Fischer	k1gMnSc3	Fischer
osobní	osobní	k2eAgInSc4d1	osobní
nedostatek	nedostatek	k1gInSc4	nedostatek
razance	razance	k1gFnSc1	razance
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Gusta	Gusta	k1gFnSc1	Gusta
Slamečka	Slamečka	k1gFnSc1	Slamečka
<g/>
,	,	kIx,	,
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
opravdu	opravdu	k6eAd1	opravdu
do	do	k7c2	do
tuhého	tuhý	k2eAgNnSc2d1	tuhé
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
mám	mít	k5eAaImIp1nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhne	uhnout	k5eAaPmIp3nS	uhnout
<g/>
,	,	kIx,	,
jo	jo	k9	jo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc4	ten
ten	ten	k3xDgMnSc1	ten
Fischer	Fischer	k1gMnSc1	Fischer
prostě	prostě	k9	prostě
je	být	k5eAaImIp3nS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gay	gay	k1gMnSc1	gay
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
uhne	uhnout	k5eAaPmIp3nS	uhnout
ještě	ještě	k9	ještě
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
jo	jo	k9	jo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
prostě	prostě	k9	prostě
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
gay	gay	k1gMnSc1	gay
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
charakterem	charakter	k1gInSc7	charakter
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Fischer	Fischer	k1gMnSc1	Fischer
uznával	uznávat	k5eAaImAgMnS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Topolánek	Topolánek	k1gMnSc1	Topolánek
"	"	kIx"	"
<g/>
mluvil	mluvit	k5eAaImAgMnS	mluvit
dost	dost	k6eAd1	dost
nesouvisle	souvisle	k6eNd1	souvisle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyložil	vyložit	k5eAaPmAgMnS	vyložit
si	se	k3xPyFc3	se
výrok	výrok	k1gInSc4	výrok
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
Slamečkovi	Slameček	k1gMnSc6	Slameček
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnPc1	jehož
po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
pochválil	pochválit	k5eAaPmAgInS	pochválit
coby	coby	k?	coby
"	"	kIx"	"
<g/>
skvělého	skvělý	k2eAgMnSc2d1	skvělý
ministra	ministr	k1gMnSc2	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
dvacetiletí	dvacetiletí	k1gNnSc6	dvacetiletí
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
uměl	umět	k5eAaImAgMnS	umět
se	se	k3xPyFc4	se
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
dopravu	doprava	k1gFnSc4	doprava
porvat	porvat	k5eAaPmF	porvat
a	a	k8xC	a
stál	stát	k5eAaImAgMnS	stát
pevně	pevně	k6eAd1	pevně
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mám	mít	k5eAaImIp1nS	mít
rád	rád	k2eAgMnSc1d1	rád
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
chvílích	chvíle	k1gFnPc6	chvíle
připraven	připraven	k2eAgMnSc1d1	připraven
krvácet	krvácet	k5eAaImF	krvácet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Topolánek	Topolánek	k1gMnSc1	Topolánek
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
aspoň	aspoň	k9	aspoň
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gustav	Gustav	k1gMnSc1	Gustav
neuhýbá	uhýbat	k5eNaImIp3nS	uhýbat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
gay	gay	k1gMnSc1	gay
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
já	já	k3xPp1nSc1	já
uhnu	uhnout	k5eAaPmIp1nS	uhnout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsem	být	k5eAaImIp1nS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
antisemitská	antisemitský	k2eAgFnSc1d1	antisemitská
a	a	k8xC	a
rasistická	rasistický	k2eAgFnSc1d1	rasistická
urážka	urážka	k1gFnSc1	urážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
dohnala	dohnat	k5eAaPmAgFnS	dohnat
ke	k	k7c3	k
křiku	křik	k1gInSc3	křik
<g/>
.	.	kIx.	.
</s>
<s>
Topolánkovu	Topolánkův	k2eAgFnSc4d1	Topolánkova
omluvu	omluva	k1gFnSc4	omluva
po	po	k7c6	po
SMS	SMS	kA	SMS
i	i	k9	i
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Fischerův	Fischerův	k2eAgMnSc1d1	Fischerův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
následně	následně	k6eAd1	následně
na	na	k7c4	na
protest	protest	k1gInSc4	protest
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
hned	hned	k6eAd1	hned
od	od	k7c2	od
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Topolánek	Topolánek	k1gMnSc1	Topolánek
kritiku	kritika	k1gFnSc4	kritika
ještě	ještě	k6eAd1	ještě
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nominace	nominace	k1gFnSc1	nominace
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
chvíli	chvíle	k1gFnSc4	chvíle
logická	logický	k2eAgFnSc1d1	logická
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
tragický	tragický	k2eAgInSc1d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
jako	jako	k9	jako
premiér	premiér	k1gMnSc1	premiér
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
ministry	ministr	k1gMnPc4	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
něco	něco	k3yInSc4	něco
vyčítám	vyčítat	k5eAaImIp1nS	vyčítat
Janu	Jan	k1gMnSc3	Jan
Fischerovi	Fischer	k1gMnSc3	Fischer
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
jinak	jinak	k6eAd1	jinak
mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
a	a	k8xC	a
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
naprosto	naprosto	k6eAd1	naprosto
splnil	splnit	k5eAaPmAgMnS	splnit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
dělat	dělat	k5eAaImF	dělat
razantní	razantní	k2eAgNnSc4d1	razantní
exekutivní	exekutivní	k2eAgNnSc4d1	exekutivní
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
stávce	stávec	k1gInPc4	stávec
<g/>
...	...	k?	...
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
negativu	negativ	k1gInSc2	negativ
<g/>
,	,	kIx,	,
konzumoval	konzumovat	k5eAaBmAgMnS	konzumovat
pozitiva	pozitivum	k1gNnPc4	pozitivum
<g/>
.	.	kIx.	.
</s>
<s>
Vyčítám	vyčítat	k5eAaImIp1nS	vyčítat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvolil	zvolit	k5eAaPmAgInS	zvolit
jednodušší	jednoduchý	k2eAgFnSc4d2	jednodušší
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
přináší	přinášet	k5eAaImIp3nS	přinášet
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Psali	psát	k5eAaImAgMnP	psát
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
esemesky	esemeska	k1gFnPc4	esemeska
<g/>
,	,	kIx,	,
myslím	myslet	k5eAaImIp1nS	myslet
že	že	k8xS	že
se	se	k3xPyFc4	se
urazil	urazit	k5eAaPmAgMnS	urazit
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
slabý	slabý	k2eAgMnSc1d1	slabý
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
trvám	trvat	k5eAaImIp1nS	trvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pozdní	pozdní	k2eAgNnSc1d1	pozdní
zastavení	zastavení	k1gNnSc1	zastavení
boomu	boom	k1gInSc2	boom
solárních	solární	k2eAgFnPc2d1	solární
elektráren	elektrárna	k1gFnPc2	elektrárna
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
Topolánek	Topolánek	k1gMnSc1	Topolánek
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dodnes	dodnes	k6eAd1	dodnes
lituji	litovat	k5eAaImIp1nS	litovat
spousty	spousta	k1gFnPc4	spousta
kompromisů	kompromis	k1gInPc2	kompromis
a	a	k8xC	a
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
personálních	personální	k2eAgInPc2d1	personální
<g/>
.	.	kIx.	.
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
zvolení	zvolení	k1gNnSc1	zvolení
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Premiér	premiér	k1gMnSc1	premiér
z	z	k7c2	z
nouze	nouze	k1gFnSc2	nouze
–	–	k?	–
král	král	k1gMnSc1	král
populistů	populista	k1gMnPc2	populista
<g/>
.	.	kIx.	.
</s>
<s>
Nikoho	nikdo	k3yNnSc4	nikdo
z	z	k7c2	z
ministrů	ministr	k1gMnPc2	ministr
nepodržel	podržet	k5eNaPmAgMnS	podržet
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
kontroverznímu	kontroverzní	k2eAgNnSc3d1	kontroverzní
se	se	k3xPyFc4	se
nehlásil	hlásit	k5eNaImAgMnS	hlásit
a	a	k8xC	a
budoval	budovat	k5eAaImAgMnS	budovat
si	se	k3xPyFc3	se
jen	jen	k6eAd1	jen
vlastní	vlastní	k2eAgFnSc4d1	vlastní
publicitu	publicita	k1gFnSc4	publicita
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
dělat	dělat	k5eAaImF	dělat
něco	něco	k3yInSc1	něco
třeba	třeba	k6eAd1	třeba
s	s	k7c7	s
fotovoltaikou	fotovoltaika	k1gFnSc7	fotovoltaika
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
nyní	nyní	k6eAd1	nyní
prudce	prudko	k6eAd1	prudko
stoupají	stoupat	k5eAaImIp3nP	stoupat
ceny	cena	k1gFnPc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Paroubek	Paroubek	k1gInSc1	Paroubek
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Přinesli	přinést	k5eAaPmAgMnP	přinést
jsme	být	k5eAaImIp1nP	být
Fischerovi	Fischerův	k2eAgMnPc1d1	Fischerův
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
oněch	onen	k3xDgInPc2	onen
200	[number]	k4	200
miliard	miliarda	k4xCgFnPc2	miliarda
dodatečných	dodatečný	k2eAgInPc2d1	dodatečný
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
prosadit	prosadit	k5eAaPmF	prosadit
změnu	změna	k1gFnSc4	změna
zákona	zákon	k1gInSc2	zákon
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
přes	přes	k7c4	přes
zájmy	zájem	k1gInPc4	zájem
členů	člen	k1gMnPc2	člen
ODS	ODS	kA	ODS
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Slíbili	slíbit	k5eAaPmAgMnP	slíbit
jsme	být	k5eAaImIp1nP	být
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
Paroubkem	Paroubek	k1gInSc7	Paroubek
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
pomůžeme	pomoct	k5eAaPmIp1nP	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Neudělal	udělat	k5eNaPmAgMnS	udělat
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
si	se	k3xPyFc3	se
s	s	k7c7	s
zadat	zadat	k5eAaPmF	zadat
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterých	který	k3yRgFnPc2	který
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgMnS	být
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
sám	sám	k3xTgMnSc1	sám
neměl	mít	k5eNaImAgMnS	mít
odvahu	odvaha	k1gFnSc4	odvaha
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
vinu	vina	k1gFnSc4	vina
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Novela	novela	k1gFnSc1	novela
však	však	k9	však
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
Parlamentu	parlament	k1gInSc6	parlament
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
primárně	primárně	k6eAd1	primárně
zaviněno	zavinit	k5eAaPmNgNnS	zavinit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
odmítaly	odmítat	k5eAaImAgFnP	odmítat
vládní	vládní	k2eAgInPc1d1	vládní
předlohou	předloha	k1gFnSc7	předloha
zabývat	zabývat	k5eAaImF	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
ještě	ještě	k6eAd1	ještě
razantnějších	razantní	k2eAgNnPc2d2	razantnější
opatření	opatření	k1gNnPc2	opatření
chyběla	chybět	k5eAaImAgFnS	chybět
vůle	vůle	k1gFnSc1	vůle
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
jejich	jejich	k3xOp3gFnPc2	jejich
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Podobně	podobně	k6eAd1	podobně
Fischerovu	Fischerův	k2eAgFnSc4d1	Fischerova
vládu	vláda	k1gFnSc4	vláda
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Martin	Martin	k1gMnSc1	Martin
Bursík	Bursík	k1gMnSc1	Bursík
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
prezidenta	prezident	k1gMnSc2	prezident
na	na	k7c4	na
Fischera	Fischer	k1gMnSc4	Fischer
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
i	i	k9	i
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
počínala	počínat	k5eAaImAgFnS	počínat
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
každá	každý	k3xTgFnSc1	každý
rodina	rodina	k1gFnSc1	rodina
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
ročně	ročně	k6eAd1	ročně
za	za	k7c4	za
elektřinu	elektřina	k1gFnSc4	elektřina
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
více	hodně	k6eAd2	hodně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tiskovou	tiskový	k2eAgFnSc7d1	tisková
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zeman	Zeman	k1gMnSc1	Zeman
lže	lhát	k5eAaImIp3nS	lhát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Fischer	Fischer	k1gMnSc1	Fischer
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
zalíbení	zalíbení	k1gNnSc2	zalíbení
nenašel	najít	k5eNaPmAgMnS	najít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
však	však	k9	však
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
premiére	premiér	k1gMnSc5	premiér
aneb	aneb	k?	aneb
Rozhašená	rozhašený	k2eAgFnSc1d1	rozhašená
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
končící	končící	k2eAgInPc1d1	končící
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
přijmout	přijmout	k5eAaPmF	přijmout
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mu	on	k3xPp3gMnSc3	on
budou	být	k5eAaImBp3nP	být
nabídnuty	nabídnout	k5eAaPmNgFnP	nabídnout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Takže	takže	k8xS	takže
si	se	k3xPyFc3	se
občas	občas	k6eAd1	občas
vzpomeňte	vzpomenout	k5eAaPmRp2nP	vzpomenout
na	na	k7c4	na
Honzu	Honza	k1gMnSc4	Honza
Fischera	Fischer	k1gMnSc4	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
se	se	k3xPyFc4	se
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
ještě	ještě	k9	ještě
někdy	někdy	k6eAd1	někdy
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
přímá	přímý	k2eAgFnSc1d1	přímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
různých	různý	k2eAgInPc2d1	různý
průzkumů	průzkum	k1gInPc2	průzkum
a	a	k8xC	a
anket	anketa	k1gFnPc2	anketa
konaných	konaný	k2eAgFnPc2d1	konaná
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
měl	mít	k5eAaImAgMnS	mít
Fischer	Fischer	k1gMnSc1	Fischer
značnou	značný	k2eAgFnSc4d1	značná
podporu	podpora	k1gFnSc4	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
pro	pro	k7c4	pro
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
SC	SC	kA	SC
&	&	k?	&
C	C	kA	C
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
by	by	kYmCp3nS	by
dostal	dostat	k5eAaPmAgInS	dostat
27	[number]	k4	27
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dle	dle	k7c2	dle
prosincové	prosincový	k2eAgFnSc2d1	prosincová
ankety	anketa	k1gFnSc2	anketa
serveru	server	k1gInSc2	server
Týden	týden	k1gInSc1	týden
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
by	by	kYmCp3nS	by
získal	získat	k5eAaPmAgMnS	získat
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
–	–	k?	–
26	[number]	k4	26
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kampaň	kampaň	k1gFnSc4	kampaň
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
přímé	přímý	k2eAgFnSc2d1	přímá
volby	volba	k1gFnSc2	volba
prezidenta	prezident	k1gMnSc2	prezident
Senátem	senát	k1gInSc7	senát
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
Fischer	Fischer	k1gMnSc1	Fischer
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
úmysl	úmysl	k1gInSc4	úmysl
jít	jít	k5eAaImF	jít
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
a	a	k8xC	a
nestranický	stranický	k2eNgMnSc1d1	nestranický
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Oficiální	oficiální	k2eAgFnSc4d1	oficiální
předvolební	předvolební	k2eAgFnSc4d1	předvolební
kampaň	kampaň	k1gFnSc4	kampaň
zahájil	zahájit	k5eAaPmAgMnS	zahájit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgMnS	začít
sbírat	sbírat	k5eAaImF	sbírat
podpisy	podpis	k1gInPc4	podpis
občanů	občan	k1gMnPc2	občan
potřebné	potřebný	k2eAgNnSc1d1	potřebné
pro	pro	k7c4	pro
kandidaturu	kandidatura	k1gFnSc4	kandidatura
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
prázdnin	prázdniny	k1gFnPc2	prázdniny
opustí	opustit	k5eAaPmIp3nS	opustit
funkci	funkce	k1gFnSc4	funkce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
EBRD	EBRD	kA	EBRD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
neplacené	placený	k2eNgNnSc4d1	neplacené
volno	volno	k1gNnSc4	volno
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
zaslal	zaslat	k5eAaPmAgMnS	zaslat
prezidentovi	prezident	k1gMnSc3	prezident
EBRD	EBRD	kA	EBRD
rezignační	rezignační	k2eAgInSc4d1	rezignační
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
Fischera	Fischer	k1gMnSc2	Fischer
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
zaregistrovalo	zaregistrovat	k5eAaPmAgNnS	zaregistrovat
jako	jako	k8xS	jako
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Registraci	registrace	k1gFnSc4	registrace
předcházel	předcházet	k5eAaImAgInS	předcházet
sběr	sběr	k1gInSc1	sběr
podpisů	podpis	k1gInPc2	podpis
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
dle	dle	k7c2	dle
svých	svůj	k3xOyFgInPc2	svůj
výpočtů	výpočet	k1gInPc2	výpočet
získal	získat	k5eAaPmAgInS	získat
101	[number]	k4	101
761	[number]	k4	761
<g/>
;	;	kIx,	;
více	hodně	k6eAd2	hodně
měl	mít	k5eAaImAgMnS	mít
jen	jen	k9	jen
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
o	o	k7c4	o
necelých	celý	k2eNgInPc2d1	necelý
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
kontrole	kontrola	k1gFnSc6	kontrola
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
11,75	[number]	k4	11,75
%	%	kIx~	%
chybných	chybný	k2eAgInPc2d1	chybný
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc4	třetí
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
podíl	podíl	k1gInSc4	podíl
po	po	k7c6	po
Okamurovi	Okamur	k1gMnSc6	Okamur
a	a	k8xC	a
Dlouhém	Dlouhý	k1gMnSc6	Dlouhý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
klesli	klesnout	k5eAaPmAgMnP	klesnout
pod	pod	k7c4	pod
hranici	hranice	k1gFnSc4	hranice
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Státní	státní	k2eAgFnSc2d1	státní
volební	volební	k2eAgFnSc2d1	volební
komise	komise	k1gFnSc2	komise
vylosovala	vylosovat	k5eAaPmAgNnP	vylosovat
pořadová	pořadová	k1gNnPc1	pořadová
čísla	číslo	k1gNnPc1	číslo
kandidátních	kandidátní	k2eAgFnPc2d1	kandidátní
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
dostal	dostat	k5eAaPmAgMnS	dostat
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
podporovateli	podporovatel	k1gMnPc7	podporovatel
Fischerovy	Fischerův	k2eAgFnSc2d1	Fischerova
kampaně	kampaň	k1gFnSc2	kampaň
byl	být	k5eAaImAgMnS	být
majitel	majitel	k1gMnSc1	majitel
mediální	mediální	k2eAgFnSc2d1	mediální
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
reklamy	reklama	k1gFnSc2	reklama
Médea	Médea	k1gFnSc1	Médea
a	a	k8xC	a
několika	několik	k4yIc2	několik
časopisů	časopis	k1gInPc2	časopis
Jaromír	Jaromír	k1gMnSc1	Jaromír
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
financoval	financovat	k5eAaBmAgMnS	financovat
volební	volební	k2eAgFnSc4d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
Strany	strana	k1gFnSc2	strana
zelených	zelený	k2eAgMnPc2d1	zelený
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgMnS	být
náměstkem	náměstek	k1gMnSc7	náměstek
ministryně	ministryně	k1gFnSc2	ministryně
školství	školství	k1gNnSc2	školství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
podporoval	podporovat	k5eAaImAgMnS	podporovat
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
zelených	zelená	k1gFnPc2	zelená
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
inzerci	inzerce	k1gFnSc4	inzerce
straně	strana	k1gFnSc3	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gMnSc4	on
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Jan	Jan	k1gMnSc1	Jan
Pirk	Pirk	k1gMnSc1	Pirk
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
volebního	volební	k2eAgInSc2d1	volební
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Troška	troška	k1gFnSc1	troška
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Hein	Hein	k1gMnSc1	Hein
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Masopust	Masopust	k1gMnSc1	Masopust
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Outrata	outrata	k1gMnSc1	outrata
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Přeučil	přeučit	k5eAaPmAgMnS	přeučit
nebo	nebo	k8xC	nebo
Petr	Petr	k1gMnSc1	Petr
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
předvolebních	předvolební	k2eAgInPc6d1	předvolební
průzkumech	průzkum	k1gInPc6	průzkum
agentur	agentura	k1gFnPc2	agentura
PPM	PPM	kA	PPM
Factum	Factum	k1gNnSc4	Factum
<g/>
,	,	kIx,	,
STEM	sto	k4xCgNnSc7	sto
i	i	k9	i
Median	Mediany	k1gInPc2	Mediany
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
měl	mít	k5eAaImAgMnS	mít
Fischer	Fischer	k1gMnSc1	Fischer
vždy	vždy	k6eAd1	vždy
největší	veliký	k2eAgFnSc1d3	veliký
preference	preference	k1gFnSc1	preference
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
19,7	[number]	k4	19,7
<g/>
–	–	k?	–
<g/>
35,5	[number]	k4	35,5
%	%	kIx~	%
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
ostré	ostrý	k2eAgFnSc2d1	ostrá
kampaně	kampaň	k1gFnSc2	kampaň
a	a	k8xC	a
několika	několik	k4yIc2	několik
nepřesvědčivých	přesvědčivý	k2eNgFnPc2d1	nepřesvědčivá
vystoupeních	vystoupení	k1gNnPc6	vystoupení
v	v	k7c6	v
debatách	debata	k1gFnPc6	debata
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
mírnou	mírný	k2eAgFnSc4d1	mírná
ztrátu	ztráta	k1gFnSc4	ztráta
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
v	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
končil	končit	k5eAaImAgMnS	končit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obrat	obrat	k1gInSc1	obrat
k	k	k7c3	k
Zemanovi	Zeman	k1gMnSc3	Zeman
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
kolem	kolo	k1gNnSc7	kolo
voleb	volba	k1gFnPc2	volba
Fischer	Fischer	k1gMnSc1	Fischer
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
protikandidátů	protikandidát	k1gMnPc2	protikandidát
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
by	by	k9	by
Česko	Česko	k1gNnSc4	Česko
"	"	kIx"	"
<g/>
izoloval	izolovat	k5eAaBmAgInS	izolovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
porážce	porážka	k1gFnSc6	porážka
nechtěl	chtít	k5eNaImAgMnS	chtít
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
jasně	jasně	k6eAd1	jasně
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
koho	kdo	k3yRnSc4	kdo
podpoří	podpořit	k5eAaPmIp3nS	podpořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dřívější	dřívější	k2eAgFnSc4d1	dřívější
podporu	podpora	k1gFnSc4	podpora
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
mohl	moct	k5eAaImAgMnS	moct
počítat	počítat	k5eAaImF	počítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
potřebovat	potřebovat	k5eAaImF	potřebovat
i	i	k8xC	i
hlasy	hlas	k1gInPc1	hlas
jeho	jeho	k3xOp3gMnPc2	jeho
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ale	ale	k9	ale
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Před	před	k7c7	před
druhým	druhý	k4xOgNnSc7	druhý
kolem	kolo	k1gNnSc7	kolo
nakonec	nakonec	k6eAd1	nakonec
Schwarzenberga	Schwarzenberg	k1gMnSc4	Schwarzenberg
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Názorově	názorově	k6eAd1	názorově
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
svých	svůj	k3xOyFgMnPc2	svůj
poradců	poradce	k1gMnPc2	poradce
a	a	k8xC	a
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
naopak	naopak	k6eAd1	naopak
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
Schwarzenberga	Schwarzenberga	k1gFnSc1	Schwarzenberga
<g/>
.18	.18	k4	.18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Fischer	Fischer	k1gMnSc1	Fischer
i	i	k9	i
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Zemana	Zeman	k1gMnSc4	Zeman
v	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
jeho	jeho	k3xOp3gFnSc2	jeho
kampaně	kampaň	k1gFnSc2	kampaň
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fischer	Fischer	k1gMnSc1	Fischer
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
post	post	k1gInSc4	post
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Fischer	Fischer	k1gMnSc1	Fischer
to	ten	k3xDgNnSc4	ten
však	však	k9	však
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Financování	financování	k1gNnSc1	financování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
Fischer	Fischer	k1gMnSc1	Fischer
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
založil	založit	k5eAaPmAgInS	založit
transparentní	transparentní	k2eAgInSc1d1	transparentní
účet	účet	k1gInSc1	účet
u	u	k7c2	u
Raiffeisenbank	Raiffeisenbanka	k1gFnPc2	Raiffeisenbanka
č.	č.	k?	č.
44774411	[number]	k4	44774411
<g/>
/	/	kIx~	/
<g/>
5500	[number]	k4	5500
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
donátory	donátor	k1gMnPc7	donátor
byli	být	k5eAaImAgMnP	být
majitel	majitel	k1gMnSc1	majitel
směnárny	směnárna	k1gFnSc2	směnárna
Exchange	Exchang	k1gFnSc2	Exchang
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Šveda	Šveda	k1gMnSc1	Šveda
(	(	kIx(	(
<g/>
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
Moravia	Moravius	k1gMnSc2	Moravius
Steel	Steel	k1gMnSc1	Steel
a	a	k8xC	a
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
miliardář	miliardář	k1gMnSc1	miliardář
Tomáš	Tomáš	k1gMnSc1	Tomáš
Chrenek	Chrenka	k1gFnPc2	Chrenka
(	(	kIx(	(
<g/>
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hudebník	hudebník	k1gMnSc1	hudebník
Michal	Michal	k1gMnSc1	Michal
Šindelář	Šindelář	k1gMnSc1	Šindelář
<g/>
,	,	kIx,	,
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
majitel	majitel	k1gMnSc1	majitel
cukrářské	cukrářský	k2eAgFnSc2d1	cukrářská
a	a	k8xC	a
pekařské	pekařský	k2eAgFnSc2d1	Pekařská
firmy	firma	k1gFnSc2	firma
MIKO	MIKO	kA	MIKO
international	internationat	k5eAaPmAgInS	internationat
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
Gevorg	Gevorg	k1gMnSc1	Gevorg
Avetisyan	Avetisyan	k1gMnSc1	Avetisyan
<g/>
,	,	kIx,	,
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
sám	sám	k3xTgMnSc1	sám
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
voleb	volba	k1gFnPc2	volba
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
předvolební	předvolební	k2eAgFnSc4d1	předvolební
kampaň	kampaň	k1gFnSc4	kampaň
převýšily	převýšit	k5eAaPmAgInP	převýšit
příjmy	příjem	k1gInPc1	příjem
o	o	k7c4	o
7	[number]	k4	7
až	až	k9	až
8	[number]	k4	8
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
Fischer	Fischer	k1gMnSc1	Fischer
z	z	k7c2	z
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
USA	USA	kA	USA
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc4	tento
dluhy	dluh	k1gInPc4	dluh
uhradí	uhradit	k5eAaPmIp3nS	uhradit
jeho	jeho	k3xOp3gMnSc1	jeho
rodinný	rodinný	k2eAgMnSc1d1	rodinný
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
pražsko-izraelský	pražskozraelský	k2eAgMnSc1d1	pražsko-izraelský
developer	developer	k1gMnSc1	developer
Tamir	Tamir	k1gMnSc1	Tamir
Winterstein	Winterstein	k1gMnSc1	Winterstein
spoluvlastnící	spoluvlastnící	k2eAgFnSc4d1	spoluvlastnící
firmu	firma	k1gFnSc4	firma
Lighthouse	Lighthouse	k1gFnSc2	Lighthouse
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
nato	nato	k6eAd1	nato
však	však	k9	však
Winterstein	Winterstein	k1gMnSc1	Winterstein
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
pouze	pouze	k6eAd1	pouze
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
negativní	negativní	k2eAgFnSc1d1	negativní
kampaň	kampaň	k1gFnSc1	kampaň
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
propagační	propagační	k2eAgFnSc6d1	propagační
návštěvě	návštěva	k1gFnSc6	návštěva
USA	USA	kA	USA
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
Fischer	Fischer	k1gMnSc1	Fischer
vynaložil	vynaložit	k5eAaPmAgMnS	vynaložit
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
PR	pr	k0	pr
agentuře	agentura	k1gFnSc3	agentura
Guestbookers	Guestbookers	k1gInSc1	Guestbookers
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
a	a	k8xC	a
PR	pr	k0	pr
poradci	poradce	k1gMnPc7	poradce
Garrettu	Garretta	k1gFnSc4	Garretta
Marquisovi	Marquisův	k2eAgMnPc1d1	Marquisův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
publicitu	publicita	k1gFnSc4	publicita
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
republikánským	republikánský	k2eAgMnSc7d1	republikánský
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
r.	r.	kA	r.
2008	[number]	k4	2008
Johnem	John	k1gMnSc7	John
McCainem	McCain	k1gMnSc7	McCain
<g/>
.	.	kIx.	.
</s>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
vynaložených	vynaložený	k2eAgInPc2d1	vynaložený
peněz	peníze	k1gInPc2	peníze
šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
běžnou	běžný	k2eAgFnSc4d1	běžná
propagaci	propagace	k1gFnSc4	propagace
<g/>
,	,	kIx,	,
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
Fischer	Fischer	k1gMnSc1	Fischer
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
pořadu	pořad	k1gInSc6	pořad
televize	televize	k1gFnSc2	televize
CNN	CNN	kA	CNN
Quest	Quest	k1gInSc1	Quest
Means	Means	k1gInSc1	Means
Business	business	k1gInSc1	business
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Nixonovým	Nixonův	k2eAgMnSc7d1	Nixonův
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Henrym	Henry	k1gMnSc7	Henry
Kissingerem	Kissinger	k1gMnSc7	Kissinger
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
billboardy	billboard	k1gInPc1	billboard
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Fischerem	Fischer	k1gMnSc7	Fischer
a	a	k8xC	a
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Ti	ten	k3xDgMnPc1	ten
nejschopnější	schopný	k2eAgMnPc1d3	nejschopnější
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Imitovaly	imitovat	k5eAaBmAgInP	imitovat
oficiální	oficiální	k2eAgInPc1d1	oficiální
propagační	propagační	k2eAgInPc1d1	propagační
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
tedy	tedy	k9	tedy
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
negativní	negativní	k2eAgFnSc1d1	negativní
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
neobsahovaly	obsahovat	k5eNaImAgInP	obsahovat
logo	logo	k1gNnSc4	logo
skutečného	skutečný	k2eAgMnSc2d1	skutečný
zadavatele	zadavatel	k1gMnSc2	zadavatel
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gMnPc1	mluvčí
kanceláře	kancelář	k1gFnSc2	kancelář
Fischerovy	Fischerův	k2eAgFnSc2d1	Fischerova
kampaně	kampaň	k1gFnSc2	kampaň
je	být	k5eAaImIp3nS	být
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
neetické	etický	k2eNgFnPc4d1	neetická
a	a	k8xC	a
protizákonné	protizákonný	k2eAgFnPc4d1	protizákonná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
reklamní	reklamní	k2eAgFnSc1d1	reklamní
agentura	agentura	k1gFnSc1	agentura
rychle	rychle	k6eAd1	rychle
odstranila	odstranit	k5eAaPmAgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
Balbínova	Balbínův	k2eAgFnSc1d1	Balbínova
poetická	poetický	k2eAgFnSc1d1	poetická
strana	strana	k1gFnSc1	strana
nechala	nechat	k5eAaPmAgFnS	nechat
vyvěsit	vyvěsit	k5eAaPmF	vyvěsit
několik	několik	k4yIc4	několik
billboardů	billboard	k1gInPc2	billboard
s	s	k7c7	s
fotomontáží	fotomontáž	k1gFnSc7	fotomontáž
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
Lidových	lidový	k2eAgFnPc2d1	lidová
milic	milice	k1gFnPc2	milice
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
V	v	k7c6	v
KSČ	KSČ	kA	KSČ
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
milicích	milice	k1gFnPc6	milice
ne	ne	k9	ne
!?!	!?!	k?	!?!
–	–	k?	–
Věrni	věren	k2eAgMnPc1d1	věren
zůstaneme	zůstat	k5eAaPmIp1nP	zůstat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
BPS	BPS	kA	BPS
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
Fischera	Fischer	k1gMnSc2	Fischer
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
kandidatury	kandidatura	k1gFnSc2	kandidatura
a	a	k8xC	a
obvinilo	obvinit	k5eAaPmAgNnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1978	[number]	k4	1978
až	až	k9	až
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
LM	LM	kA	LM
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
milicionář	milicionář	k1gMnSc1	milicionář
by	by	kYmCp3nS	by
nemohl	moct	k5eNaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
lustrační	lustrační	k2eAgNnSc4d1	lustrační
osvědčení	osvědčení	k1gNnSc4	osvědčení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
;	;	kIx,	;
seznamy	seznam	k1gInPc1	seznam
milicionářů	milicionář	k1gMnPc2	milicionář
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
lustrační	lustrační	k2eAgFnSc4d1	lustrační
osvědčení	osvědčení	k1gNnSc3	osvědčení
tak	tak	k9	tak
dokládá	dokládat	k5eAaImIp3nS	dokládat
pouze	pouze	k6eAd1	pouze
poměr	poměr	k1gInSc1	poměr
k	k	k7c3	k
StB	StB	k1gFnSc3	StB
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
se	se	k3xPyFc4	se
čestným	čestný	k2eAgNnSc7d1	čestné
prohlášením	prohlášení	k1gNnSc7	prohlášení
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
LM	LM	kA	LM
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fischerova	Fischerův	k2eAgFnSc1d1	Fischerova
mluvčí	mluvčí	k1gFnSc1	mluvčí
označila	označit	k5eAaPmAgFnS	označit
i	i	k9	i
tyto	tento	k3xDgInPc4	tento
billboardy	billboard	k1gInPc4	billboard
za	za	k7c4	za
protizákonné	protizákonný	k2eAgFnPc4d1	protizákonná
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
divadelník	divadelník	k1gMnSc1	divadelník
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Fischera	Fischer	k1gMnSc4	Fischer
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
z	z	k7c2	z
kandidatury	kandidatura	k1gFnSc2	kandidatura
kvůli	kvůli	k7c3	kvůli
komunistické	komunistický	k2eAgFnSc3d1	komunistická
minulosti	minulost	k1gFnSc3	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Hanák	Hanák	k1gMnSc1	Hanák
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
kvůli	kvůli	k7c3	kvůli
karierismu	karierismus	k1gInSc3	karierismus
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yQnSc2	co
naopak	naopak	k6eAd1	naopak
nevinil	vinit	k5eNaImAgMnS	vinit
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
okupací	okupace	k1gFnSc7	okupace
a	a	k8xC	a
normalizací	normalizace	k1gFnSc7	normalizace
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
skupiny	skupina	k1gFnSc2	skupina
Dekomunizace	dekomunizace	k1gFnSc2	dekomunizace
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
reagoval	reagovat	k5eAaBmAgInS	reagovat
nepřímým	přímý	k2eNgInSc7d1	nepřímý
výpadem	výpad	k1gInSc7	výpad
na	na	k7c4	na
Zemanovy	Zemanův	k2eAgMnPc4d1	Zemanův
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
a	a	k8xC	a
kauzy	kauza	k1gFnPc4	kauza
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
Zemanovou	Zemanův	k2eAgFnSc7d1	Zemanova
vládou	vláda	k1gFnSc7	vláda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každý	každý	k3xTgInSc1	každý
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
zvolili	zvolit	k5eAaPmAgMnP	zvolit
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
a	a	k8xC	a
já	já	k3xPp1nSc1	já
jejich	jejich	k3xOp3gNnSc1	jejich
nesouhlasný	souhlasný	k2eNgInSc1d1	nesouhlasný
názor	názor	k1gInSc1	názor
respektuji	respektovat	k5eAaImIp1nS	respektovat
<g/>
.	.	kIx.	.
</s>
<s>
Divím	divit	k5eAaImIp1nS	divit
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorům	autor	k1gMnPc3	autor
výzvy	výzva	k1gFnSc2	výzva
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
na	na	k7c6	na
Hrad	hrad	k1gInSc1	hrad
Šloufovi	Šlouf	k1gMnSc3	Šlouf
<g/>
,	,	kIx,	,
Zbytkovi	Zbytek	k1gMnSc3	Zbytek
<g/>
,	,	kIx,	,
aféře	aféra	k1gFnSc3	aféra
Bamberk	Bamberk	k1gInSc4	Bamberk
<g/>
,	,	kIx,	,
akci	akce	k1gFnSc4	akce
Olovo	olovo	k1gNnSc4	olovo
<g/>
,	,	kIx,	,
kauze	kauza	k1gFnSc6	kauza
D47	D47	k1gFnSc6	D47
a	a	k8xC	a
dalším	další	k2eAgInSc6d1	další
synonymům	synonymum	k1gNnPc3	synonymum
nečistých	čistý	k2eNgFnPc2d1	nečistá
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hnutí	hnutí	k1gNnSc1	hnutí
Dekomunizace	dekomunizace	k1gFnSc2	dekomunizace
používalo	používat	k5eAaImAgNnS	používat
pro	pro	k7c4	pro
Jana	Jan	k1gMnSc4	Jan
Fischera	Fischer	k1gMnSc4	Fischer
posměšnou	posměšný	k2eAgFnSc4d1	posměšná
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Želé	želé	k1gNnSc2	želé
<g/>
"	"	kIx"	"
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
motiv	motiv	k1gInSc1	motiv
využíval	využívat	k5eAaImAgInS	využívat
i	i	k9	i
jejich	jejich	k3xOp3gInSc4	jejich
videoklip	videoklip	k1gInSc4	videoklip
kombinující	kombinující	k2eAgInPc4d1	kombinující
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
pořadu	pořad	k1gInSc2	pořad
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
šéfe	šéf	k1gMnSc5	šéf
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
seriálu	seriál	k1gInSc2	seriál
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc4	portrét
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
namapován	namapován	k2eAgInSc1d1	namapován
na	na	k7c4	na
želatinový	želatinový	k2eAgInSc4d1	želatinový
pokrm	pokrm	k1gInSc4	pokrm
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
amarouny	amaroun	k1gInPc1	amaroun
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
je	být	k5eAaImIp3nS	být
negativními	negativní	k2eAgFnPc7d1	negativní
hodnotícími	hodnotící	k2eAgFnPc7d1	hodnotící
větami	věta	k1gFnPc7	věta
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Pohlreicha	Pohlreich	k1gMnSc2	Pohlreich
(	(	kIx(	(
<g/>
užity	užit	k2eAgFnPc1d1	užita
bez	bez	k7c2	bez
jeho	on	k3xPp3gInSc2	on
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
byl	být	k5eAaImAgInS	být
klip	klip	k1gInSc4	klip
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
zablokován	zablokovat	k5eAaPmNgInS	zablokovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
duelu	duel	k1gInSc6	duel
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
na	na	k7c6	na
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fischerova	Fischerův	k2eAgFnSc1d1	Fischerova
kancelář	kancelář	k1gFnSc1	kancelář
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
najala	najmout	k5eAaPmAgFnS	najmout
čtyřicet	čtyřicet	k4xCc4	čtyřicet
komparzistů	komparzista	k1gMnPc2	komparzista
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
kolem	kolem	k6eAd1	kolem
osmi	osm	k4xCc2	osm
set	sto	k4xCgNnPc2	sto
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vyhovět	vyhovět	k5eAaPmF	vyhovět
dramaturgii	dramaturgie	k1gFnSc4	dramaturgie
TV	TV	kA	TV
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gFnPc4	on
pověřila	pověřit	k5eAaPmAgFnS	pověřit
zajištěním	zajištění	k1gNnSc7	zajištění
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
podporovatelů	podporovatel	k1gMnPc2	podporovatel
do	do	k7c2	do
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
najímání	najímání	k1gNnSc2	najímání
komparsistů	komparsista	k1gMnPc2	komparsista
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
duel	duel	k1gInSc1	duel
předtáčel	předtáčet	k5eAaImAgInS	předtáčet
ve	v	k7c4	v
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
mezi	mezi	k7c7	mezi
dvanáctou	dvanáctý	k4xOgFnSc7	dvanáctý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
hodinou	hodina	k1gFnSc7	hodina
odpolední	odpolední	k2eAgFnSc7d1	odpolední
<g/>
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
byla	být	k5eAaImAgFnS	být
široce	široko	k6eAd1	široko
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
pro	pro	k7c4	pro
neprofesionální	profesionální	k2eNgFnSc4d1	neprofesionální
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
průběh	průběh	k1gInSc4	průběh
debaty	debata	k1gFnSc2	debata
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sledovalo	sledovat	k5eAaImAgNnS	sledovat
944	[number]	k4	944
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Fischer	Fischer	k1gMnSc1	Fischer
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebyla	být	k5eNaImAgFnS	být
to	ten	k3xDgNnSc1	ten
bohužel	bohužel	k9	bohužel
standardní	standardní	k2eAgFnSc1d1	standardní
volební	volební	k2eAgFnSc1d1	volební
debata	debata	k1gFnSc1	debata
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
"	"	kIx"	"
<g/>
zábavný	zábavný	k2eAgInSc1d1	zábavný
pořad	pořad	k1gInSc1	pořad
komerční	komerční	k2eAgFnSc2d1	komerční
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
a	a	k8xC	a
primárním	primární	k2eAgInSc7d1	primární
cílem	cíl	k1gInSc7	cíl
komerčních	komerční	k2eAgNnPc2d1	komerční
médií	médium	k1gNnPc2	médium
je	být	k5eAaImIp3nS	být
sledovanost	sledovanost	k1gFnSc1	sledovanost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
rovněž	rovněž	k9	rovněž
vystupovala	vystupovat	k5eAaImAgNnP	vystupovat
<g/>
,	,	kIx,	,
diváci	divák	k1gMnPc1	divák
ze	z	k7c2	z
Zemanova	Zemanův	k2eAgInSc2d1	Zemanův
tábora	tábor	k1gInSc2	tábor
volali	volat	k5eAaImAgMnP	volat
vulgarismy	vulgarismus	k1gInPc4	vulgarismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
Rusnokově	Rusnokův	k2eAgFnSc6d1	Rusnokova
vládě	vláda	k1gFnSc6	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
Fischerově	Fischerův	k2eAgNnSc6d1	Fischerovo
angažmá	angažmá	k1gNnSc6	angažmá
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
v	v	k7c6	v
Kramářově	kramářův	k2eAgFnSc6d1	Kramářova
vile	vila	k1gFnSc6	vila
s	s	k7c7	s
designovaným	designovaný	k2eAgMnSc7d1	designovaný
premiérem	premiér	k1gMnSc7	premiér
Jiřím	Jiří	k1gMnSc7	Jiří
Rusnokem	Rusnok	k1gMnSc7	Rusnok
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
Fischer	Fischer	k1gMnSc1	Fischer
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Rusnokem	Rusnok	k1gInSc7	Rusnok
o	o	k7c6	o
žádné	žádný	k3yNgFnSc6	žádný
personální	personální	k2eAgFnSc6d1	personální
nabídce	nabídka	k1gFnSc6	nabídka
nemluvili	mluvit	k5eNaImAgMnP	mluvit
<g/>
,	,	kIx,	,
Rusnok	Rusnok	k1gInSc1	Rusnok
se	se	k3xPyFc4	se
prý	prý	k9	prý
jen	jen	k9	jen
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
jeho	jeho	k3xOp3gInPc4	jeho
názory	názor	k1gInPc4	názor
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
proces	proces	k1gInSc4	proces
skládání	skládání	k1gNnSc4	skládání
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Fischer	Fischer	k1gMnSc1	Fischer
se	se	k3xPyFc4	se
s	s	k7c7	s
Rusnokem	Rusnok	k1gInSc7	Rusnok
znovu	znovu	k6eAd1	znovu
sešel	sejít	k5eAaPmAgInS	sejít
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k9	už
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
od	od	k7c2	od
oznámení	oznámení	k1gNnSc2	oznámení
přijetí	přijetí	k1gNnSc2	přijetí
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
Fischerovi	Fischer	k1gMnSc3	Fischer
podařilo	podařit	k5eAaPmAgNnS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
zhruba	zhruba	k6eAd1	zhruba
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ještě	ještě	k6eAd1	ještě
dlužil	dlužit	k5eAaImAgMnS	dlužit
za	za	k7c4	za
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sled	sled	k1gInSc1	sled
událostí	událost	k1gFnSc7	událost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
řadu	řada	k1gFnSc4	řada
spekulací	spekulace	k1gFnPc2	spekulace
o	o	k7c6	o
možných	možný	k2eAgFnPc6d1	možná
protislužbách	protislužba	k1gFnPc6	protislužba
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
dárce	dárce	k1gMnPc4	dárce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Fischer	Fischer	k1gMnSc1	Fischer
popřel	popřít	k5eAaPmAgMnS	popřít
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
už	už	k9	už
nebude	být	k5eNaImBp3nS	být
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
otázce	otázka	k1gFnSc3	otázka
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaImF	věnovat
řízení	řízení	k1gNnSc3	řízení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
<g/>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakým	jaký	k3yRgInSc7	jaký
rozpočtem	rozpočet	k1gInSc7	rozpočet
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
odvolal	odvolat	k5eAaPmAgMnS	odvolat
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
náměstky	náměstek	k1gMnPc4	náměstek
<g/>
,	,	kIx,	,
Tomáše	Tomáš	k1gMnSc2	Tomáš
Zídka	Zídek	k1gMnSc2	Zídek
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Zajíčka	Zajíček	k1gMnSc2	Zajíček
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
náměstkyní	náměstkyně	k1gFnSc7	náměstkyně
zároveň	zároveň	k6eAd1	zároveň
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Evu	Eva	k1gFnSc4	Eva
Anderovou	Anderová	k1gFnSc4	Anderová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k8xC	jako
poradkyně	poradkyně	k1gFnSc1	poradkyně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jeho	jeho	k3xOp3gFnSc2	jeho
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
143	[number]	k4	143
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
161	[number]	k4	161
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
</s>
</p>
<p>
<s>
http://www.jan-fischer.cz	[url]	k1gInSc1	http://www.jan-fischer.cz
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
hostem	host	k1gMnSc7	host
pořadu	pořad	k1gInSc2	pořad
Osobnost	osobnost	k1gFnSc1	osobnost
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plus	plus	k1gInSc1	plus
21	[number]	k4	21
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
</s>
</p>
