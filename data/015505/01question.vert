<s>
Jaká	jaký	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
výčtech	výčet	k1gInPc6
v	v	k7c6
textu	text	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
chce	chtít	k5eAaImIp3nS
pisatel	pisatel	k1gMnSc1
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výčet	výčet	k1gInSc4
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>