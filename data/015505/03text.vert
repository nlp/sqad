<s>
Atd.	atd.	kA
</s>
<s>
atd.	atd.	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
„	„	k?
<g/>
a	a	k8xC
tak	tak	k6eAd1
dále	daleko	k6eAd2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
při	při	k7c6
výčtech	výčet	k1gInPc6
v	v	k7c6
textu	text	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
chce	chtít	k5eAaImIp3nS
pisatel	pisatel	k1gMnSc1
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výčet	výčet	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
že	že	k8xS
se	se	k3xPyFc4
vlastnost	vlastnost	k1gFnSc1
opakuje	opakovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
atd.	atd.	kA
se	se	k3xPyFc4
podle	podle	k7c2
kontextu	kontext	k1gInSc2
píše	psát	k5eAaImIp3nS
nebo	nebo	k8xC
nepíše	psát	k5eNaImIp3nS
čárka	čárka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnSc2d1
atd.	atd.	kA
odpovídá	odpovídat	k5eAaImIp3nS
latinskému	latinský	k2eAgNnSc3d1
<g/>
,	,	kIx,
anglickému	anglický	k2eAgNnSc3d1
a	a	k8xC
francouzskému	francouzský	k2eAgNnSc3d1
etc	etc	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tedy	tedy	k8xC
et	et	k?
cetera	cetera	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
fráze	fráze	k1gFnSc1
„	„	k?
<g/>
a	a	k8xC
tak	tak	k6eAd1
dále	daleko	k6eAd2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
kalkem	kalk	k1gInSc7
(	(	kIx(
<g/>
doslovným	doslovný	k2eAgInSc7d1
překladem	překlad	k1gInSc7
<g/>
)	)	kIx)
z	z	k7c2
německého	německý	k2eAgInSc2d1
und	und	k?
so	so	k?
weiter	weitra	k1gFnPc2
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
usw	usw	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Atd.	atd.	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
atd.	atd.	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
