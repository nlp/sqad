<p>
<s>
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Mährisch	Mährisch	k1gMnSc1	Mährisch
Budwitz	Budwitz	k1gMnSc1	Budwitz
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Třebíč	Třebíč	k1gFnSc1	Třebíč
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
19	[number]	k4	19
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
,	,	kIx,	,
na	na	k7c6	na
dopravní	dopravní	k2eAgFnSc6d1	dopravní
spojnici	spojnice	k1gFnSc6	spojnice
mezi	mezi	k7c7	mezi
Jihlavou	Jihlava	k1gFnSc7	Jihlava
a	a	k8xC	a
Znojmem	Znojmo	k1gNnSc7	Znojmo
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
Rokytka	Rokytka	k1gFnSc1	Rokytka
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
465	[number]	k4	465
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
části	část	k1gFnPc1	část
Jackov	Jackov	k1gInSc1	Jackov
<g/>
,	,	kIx,	,
Lažínky	Lažínek	k1gInPc1	Lažínek
<g/>
,	,	kIx,	,
Vesce	Vesec	k1gInPc1	Vesec
a	a	k8xC	a
Vranín	Vranín	k1gInSc1	Vranín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Sousedními	sousední	k2eAgFnPc7d1	sousední
obcemi	obec	k1gFnPc7	obec
sídla	sídlo	k1gNnSc2	sídlo
jsou	být	k5eAaImIp3nP	být
Zvěrkovice	Zvěrkovice	k1gFnPc1	Zvěrkovice
<g/>
,	,	kIx,	,
Blížkovice	Blížkovice	k1gFnPc1	Blížkovice
<g/>
,	,	kIx,	,
Rácovice	Rácovice	k1gFnPc1	Rácovice
<g/>
,	,	kIx,	,
Domamil	Domamil	k1gFnPc1	Domamil
<g/>
,	,	kIx,	,
Dědice	Dědice	k1gFnPc1	Dědice
<g/>
,	,	kIx,	,
Komárovice	Komárovice	k1gFnSc1	Komárovice
<g/>
,	,	kIx,	,
Častohostice	Častohostika	k1gFnSc6	Častohostika
<g/>
,	,	kIx,	,
Litohoř	Litohoř	k1gFnSc1	Litohoř
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
Syrovice	syrovice	k1gFnPc1	syrovice
<g/>
,	,	kIx,	,
Blatnice	Blatnice	k1gFnPc1	Blatnice
a	a	k8xC	a
Lukov	Lukov	k1gInSc1	Lukov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1231	[number]	k4	1231
<g/>
,	,	kIx,	,
městečko	městečko	k1gNnSc1	městečko
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
bere	brát	k5eAaImIp3nS	brát
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
ochranu	ochrana	k1gFnSc4	ochrana
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
Přemyslu	Přemysl	k1gMnSc6	Přemysl
Otakarovi	Otakar	k1gMnSc6	Otakar
I.	I.	kA	I.
i	i	k9	i
jejími	její	k3xOp3gInPc7	její
věnnými	věnný	k2eAgInPc7d1	věnný
statky	statek	k1gInPc7	statek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Konstancie	Konstancie	k1gFnPc4	Konstancie
Uherská	uherský	k2eAgFnSc1d1	uherská
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
tehdy	tehdy	k6eAd1	tehdy
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Haberské	Haberský	k2eAgFnSc6d1	Haberská
stezce	stezka	k1gFnSc6	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
existoval	existovat	k5eAaImAgMnS	existovat
i	i	k9	i
opevněný	opevněný	k2eAgInSc4d1	opevněný
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
datováno	datovat	k5eAaImNgNnS	datovat
založení	založení	k1gNnSc1	založení
tržiště	tržiště	k1gNnSc2	tržiště
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
dnešního	dnešní	k2eAgNnSc2d1	dnešní
centrálního	centrální	k2eAgNnSc2d1	centrální
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
tehdy	tehdy	k6eAd1	tehdy
patřilo	patřit	k5eAaImAgNnS	patřit
Lichtenburkům	Lichtenburek	k1gMnPc3	Lichtenburek
<g/>
,	,	kIx,	,
majitelům	majitel	k1gMnPc3	majitel
bítovského	bítovský	k2eAgInSc2d1	bítovský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1498	[number]	k4	1498
se	se	k3xPyFc4	se
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
staly	stát	k5eAaPmAgFnP	stát
poddanským	poddanský	k2eAgNnSc7d1	poddanské
městem	město	k1gNnSc7	město
a	a	k8xC	a
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
městem	město	k1gNnSc7	město
krajským	krajský	k2eAgMnPc3d1	krajský
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
plnilo	plnit	k5eAaImAgNnS	plnit
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
majiteli	majitel	k1gMnPc7	majitel
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
Valdštejnů	Valdštejn	k1gMnPc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
vydlážděno	vydlážděn	k2eAgNnSc1d1	vydlážděno
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
město	město	k1gNnSc4	město
postihl	postihnout	k5eAaPmAgInS	postihnout
i	i	k9	i
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1532	[number]	k4	1532
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
i	i	k9	i
zeměpanský	zeměpanský	k2eAgInSc1d1	zeměpanský
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgInS	být
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
a	a	k8xC	a
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
brzy	brzy	k6eAd1	brzy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslední	poslední	k2eAgInSc4d1	poslední
z	z	k7c2	z
Valdštejnů	Valdštejn	k1gInPc2	Valdštejn
–	–	k?	–
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Brtnický	brtnický	k2eAgMnSc1d1	brtnický
a	a	k8xC	a
město	město	k1gNnSc1	město
po	po	k7c6	po
konfiskaci	konfiskace	k1gFnSc6	konfiskace
jako	jako	k8xS	jako
zápočet	zápočet	k1gInSc1	zápočet
za	za	k7c4	za
pohledávky	pohledávka	k1gFnPc4	pohledávka
vůči	vůči	k7c3	vůči
císaři	císař	k1gMnSc3	císař
získali	získat	k5eAaPmAgMnP	získat
rakouští	rakouský	k2eAgMnPc1d1	rakouský
katoličtí	katolický	k2eAgMnPc1d1	katolický
páni	pan	k1gMnPc1	pan
ze	z	k7c2	z
Schaumburka	Schaumburek	k1gMnSc2	Schaumburek
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
majitelem	majitel	k1gMnSc7	majitel
byl	být	k5eAaImAgMnS	být
Hanibal	Hanibal	k1gMnSc1	Hanibal
ze	z	k7c2	z
Schaumburka	Schaumburek	k1gMnSc2	Schaumburek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
město	město	k1gNnSc4	město
získal	získat	k5eAaPmAgMnS	získat
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jindřich	Jindřich	k1gMnSc1	Jindřich
ze	z	k7c2	z
Schaumburka	Schaumburek	k1gMnSc2	Schaumburek
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
usadit	usadit	k5eAaPmF	usadit
rod	rod	k1gInSc4	rod
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
tři	tři	k4xCgInPc4	tři
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
domy	dům	k1gInPc4	dům
a	a	k8xC	a
radnici	radnice	k1gFnSc4	radnice
konfiskoval	konfiskovat	k5eAaBmAgMnS	konfiskovat
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základu	základ	k1gInSc6	základ
postavil	postavit	k5eAaPmAgMnS	postavit
budovu	budova	k1gFnSc4	budova
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1673	[number]	k4	1673
město	město	k1gNnSc4	město
opět	opět	k6eAd1	opět
postihl	postihnout	k5eAaPmAgInS	postihnout
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
shořet	shořet	k5eAaPmF	shořet
polovina	polovina	k1gFnSc1	polovina
města	město	k1gNnSc2	město
a	a	k8xC	a
město	město	k1gNnSc1	město
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
majiteli	majitel	k1gMnPc7	majitel
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1736	[number]	k4	1736
stal	stát	k5eAaPmAgInS	stát
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Wallisu	Wallis	k1gInSc2	Wallis
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jmenovali	jmenovat	k5eAaBmAgMnP	jmenovat
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgMnPc4d1	další
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
spravovali	spravovat	k5eAaImAgMnP	spravovat
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
tudy	tudy	k6eAd1	tudy
táhli	táhnout	k5eAaImAgMnP	táhnout
napoleonští	napoleonský	k2eAgMnPc1d1	napoleonský
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zřízen	zřídit	k5eAaPmNgInS	zřídit
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
berní	berní	k2eAgInSc1d1	berní
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
politického	politický	k2eAgInSc2d1	politický
okresu	okres	k1gInSc2	okres
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
okresní	okresní	k2eAgNnSc1d1	okresní
hejtmanství	hejtmanství	k1gNnSc1	hejtmanství
a	a	k8xC	a
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
městem	město	k1gNnSc7	město
okresním	okresní	k2eAgNnSc7d1	okresní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
český	český	k2eAgInSc1d1	český
spolek	spolek	k1gInSc1	spolek
Čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
slovanský	slovanský	k2eAgInSc4d1	slovanský
Budivoj	Budivoj	k1gInSc4	Budivoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
organizoval	organizovat	k5eAaBmAgInS	organizovat
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgInS	zřídit
městskou	městský	k2eAgFnSc4d1	městská
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
podnětu	podnět	k1gInSc2	podnět
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
a	a	k8xC	a
např.	např.	kA	např.
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
založena	založit	k5eAaPmNgFnS	založit
i	i	k9	i
Městská	městský	k2eAgFnSc1d1	městská
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Továrna	továrna	k1gFnSc1	továrna
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
slévárny	slévárna	k1gFnPc1	slévárna
Matěje	Matěj	k1gMnSc2	Matěj
Smrčky	Smrčka	k1gMnSc2	Smrčka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
i	i	k9	i
sokolovna	sokolovna	k1gFnSc1	sokolovna
pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
členy	člen	k1gMnPc4	člen
sokola	sokol	k1gMnSc4	sokol
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1869	[number]	k4	1869
a	a	k8xC	a
1871	[number]	k4	1871
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
do	do	k7c2	do
Jemnice	Jemnice	k1gFnSc2	Jemnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
čtenářském	čtenářský	k2eAgInSc6d1	čtenářský
spolku	spolek	k1gInSc6	spolek
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
spolky	spolek	k1gInPc1	spolek
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hasičský	hasičský	k2eAgInSc1d1	hasičský
a	a	k8xC	a
občanský	občanský	k2eAgInSc1d1	občanský
střelecký	střelecký	k2eAgInSc1d1	střelecký
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
spolek	spolek	k1gInSc1	spolek
učitelský	učitelský	k2eAgInSc1d1	učitelský
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
okrašlovací	okrašlovací	k2eAgInSc1d1	okrašlovací
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
Matice	matice	k1gFnSc1	matice
školská	školská	k1gFnSc1	školská
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
německý	německý	k2eAgInSc1d1	německý
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g />
.	.	kIx.	.
</s>
<s>
1889	[number]	k4	1889
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
ženský	ženský	k2eAgInSc4d1	ženský
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
spolek	spolek	k1gInSc4	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
okres	okres	k1gInSc4	okres
moravskobudějovický	moravskobudějovický	k2eAgInSc4d1	moravskobudějovický
"	"	kIx"	"
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1921	[number]	k4	1921
se	se	k3xPyFc4	se
přetvořil	přetvořit	k5eAaPmAgInS	přetvořit
na	na	k7c4	na
Okresní	okresní	k2eAgInSc4d1	okresní
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
spolek	spolek	k1gInSc4	spolek
a	a	k8xC	a
r.	r.	kA	r.
1943	[number]	k4	1943
Okresní	okresní	k2eAgInSc1d1	okresní
spolek	spolek	k1gInSc1	spolek
Svazu	svaz	k1gInSc2	svaz
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
lesnictví	lesnictví	k1gNnSc2	lesnictví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
župní	župní	k2eAgInSc1d1	župní
hasičský	hasičský	k2eAgInSc1d1	hasičský
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
Národní	národní	k2eAgFnSc1d1	národní
jednota	jednota	k1gFnSc1	jednota
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
legionářský	legionářský	k2eAgMnSc1d1	legionářský
<g/>
,	,	kIx,	,
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgInSc4d1	židovský
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
utrakvistická	utrakvistický	k2eAgFnSc1d1	utrakvistická
obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
i	i	k9	i
hlavní	hlavní	k2eAgFnSc7d1	hlavní
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
reálkou	reálka	k1gFnSc7	reálka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1889	[number]	k4	1889
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
i	i	k9	i
dívčí	dívčí	k2eAgFnSc1d1	dívčí
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
i	i	k9	i
živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
pokračovací	pokračovací	k2eAgFnSc1d1	pokračovací
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
matiční	matiční	k2eAgFnSc4d1	matiční
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
pod	pod	k7c4	pod
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
přemístěn	přemístěn	k2eAgMnSc1d1	přemístěn
i	i	k8xC	i
učitelský	učitelský	k2eAgInSc1d1	učitelský
koedukační	koedukační	k2eAgInSc1d1	koedukační
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc1	týž
rok	rok	k1gInSc4	rok
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
i	i	k9	i
obchodní	obchodní	k2eAgFnSc1d1	obchodní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
ženská	ženský	k2eAgNnPc4d1	ženské
povolání	povolání	k1gNnPc4	povolání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Peněžní	peněžní	k2eAgInPc1d1	peněžní
ústavy	ústav	k1gInPc1	ústav
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
"	"	kIx"	"
<g/>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
rolnická	rolnický	k2eAgFnSc1d1	rolnická
záložna	záložna	k1gFnSc1	záložna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Spar-	Spar-	k1gMnSc1	Spar-
u.	u.	k?	u.
Vorschußverein	Vorschußverein	k1gMnSc1	Vorschußverein
<g/>
"	"	kIx"	"
/	/	kIx~	/
německá	německý	k2eAgFnSc1d1	německá
Městská	městský	k2eAgFnSc1d1	městská
spořitelna	spořitelna	k1gFnSc1	spořitelna
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
záložna	záložna	k1gFnSc1	záložna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lidová	lidový	k2eAgFnSc1d1	lidová
záložna	záložna	k1gFnSc1	záložna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
"	"	kIx"	"
<g/>
Hasičská	hasičský	k2eAgFnSc1d1	hasičská
záložna	záložna	k1gFnSc1	záložna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
katolicko-národní	katolickoárodní	k2eAgFnSc2d1	katolicko-národní
strany	strana	k1gFnSc2	strana
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
jednota	jednota	k1gFnSc1	jednota
českých	český	k2eAgNnPc2d1	české
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
společenstev	společenstvo	k1gNnPc2	společenstvo
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
ústředí	ústředí	k1gNnSc1	ústředí
úvěrních	úvěrní	k2eAgNnPc2d1	úvěrní
družstev	družstvo	k1gNnPc2	družstvo
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
86	[number]	k4	86
společenstev	společenstvo	k1gNnPc2	společenstvo
s	s	k7c7	s
6	[number]	k4	6
206	[number]	k4	206
členy	člen	k1gMnPc7	člen
<g/>
,	,	kIx,	,
zakládala	zakládat	k5eAaImAgNnP	zakládat
úvěrní	úvěrní	k2eAgNnPc1d1	úvěrní
družstva	družstvo	k1gNnPc1	družstvo
-	-	kIx~	-
raiffeisenky	raiffeisenka	k1gFnSc2	raiffeisenka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
katolicky	katolicky	k6eAd1	katolicky
orientovaných	orientovaný	k2eAgNnPc6d1	orientované
družstvech	družstvo	k1gNnPc6	družstvo
převažovaly	převažovat	k5eAaImAgFnP	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
založeno	založit	k5eAaPmNgNnS	založit
Hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nabízelo	nabízet	k5eAaImAgNnS	nabízet
umělá	umělý	k2eAgNnPc1d1	umělé
hnojiva	hnojivo	k1gNnPc1	hnojivo
Přerovské	přerovský	k2eAgFnSc2d1	přerovská
rol	rola	k1gFnPc2	rola
<g/>
.	.	kIx.	.
továrny	továrna	k1gFnPc1	továrna
a	a	k8xC	a
v	v	k7c6	v
místnostech	místnost	k1gFnPc6	místnost
obilního	obilní	k2eAgNnSc2d1	obilní
skladiště	skladiště	k1gNnSc2	skladiště
na	na	k7c6	na
"	"	kIx"	"
<g/>
Palírně	palírna	k1gFnSc6	palírna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hostinec	hostinec	k1gInSc1	hostinec
Palírna	palírna	k1gFnSc1	palírna
Ferd	Ferd	k1gInSc1	Ferd
<g/>
.	.	kIx.	.
</s>
<s>
Šrámka	Šrámek	k1gMnSc4	Šrámek
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
žitné	žitný	k2eAgInPc4d1	žitný
i	i	k8xC	i
pšeničné	pšeničný	k2eAgInPc4d1	pšeničný
otruby	otruby	k1gInPc4	otruby
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1903	[number]	k4	1903
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
družstva	družstvo	k1gNnSc2	družstvo
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
zakoupení	zakoupení	k1gNnSc4	zakoupení
nového	nový	k2eAgInSc2d1	nový
pozemku	pozemek	k1gInSc2	pozemek
pro	pro	k7c4	pro
skladiště	skladiště	k1gNnSc4	skladiště
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
prosperita	prosperita	k1gFnSc1	prosperita
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc1	nepochopení
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rolníků	rolník	k1gMnPc2	rolník
provázely	provázet	k5eAaImAgInP	provázet
družstvo	družstvo	k1gNnSc4	družstvo
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
nakonec	nakonec	k6eAd1	nakonec
ustala	ustat	k5eAaPmAgFnS	ustat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1914	[number]	k4	1914
otevřela	otevřít	k5eAaPmAgFnS	otevřít
strana	strana	k1gFnSc1	strana
nár	nár	k?	nár
<g/>
.	.	kIx.	.
socialistů	socialist	k1gMnPc2	socialist
v	v	k7c6	v
Morav	Morava	k1gFnPc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Budějovicích	Budějovice	k1gInPc6	Budějovice
obchod	obchod	k1gInSc4	obchod
svého	svůj	k3xOyFgMnSc2	svůj
Prvního	první	k4xOgMnSc2	první
zásobního	zásobní	k2eAgMnSc2d1	zásobní
a	a	k8xC	a
potravního	potravní	k2eAgNnSc2d1	potravní
družstva	družstvo	k1gNnSc2	družstvo
"	"	kIx"	"
<g/>
Svépomoc	svépomoc	k1gFnSc1	svépomoc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
ve	v	k7c6	v
spolkových	spolkový	k2eAgFnPc6d1	spolková
místnostech	místnost	k1gFnPc6	místnost
Národního	národní	k2eAgInSc2d1	národní
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Naše	náš	k3xOp1gFnPc4	náš
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
23.8	[number]	k4	23.8
<g/>
.1902	.1902	k4	.1902
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
držela	držet	k5eAaImAgFnS	držet
pozici	pozice	k1gFnSc4	pozice
lidoveckého	lidovecký	k2eAgNnSc2d1	lidovecké
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
"	"	kIx"	"
<g/>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
jednota	jednota	k1gFnSc1	jednota
českých	český	k2eAgNnPc2d1	české
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
společenstev	společenstvo	k1gNnPc2	společenstvo
úvěrních	úvěrní	k2eAgNnPc2d1	úvěrní
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
složku	složka	k1gFnSc4	složka
lidoveckého	lidovecký	k2eAgNnSc2d1	lidovecké
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
tvořila	tvořit	k5eAaImAgFnS	tvořit
tzv.	tzv.	kA	tzv.
lidová	lidový	k2eAgNnPc1d1	lidové
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
družstva	družstvo	k1gNnPc1	družstvo
(	(	kIx(	(
<g/>
zádruhy	zádruha	k1gFnSc2	zádruha
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
družiny	družina	k1gFnSc2	družina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stranické	stranický	k2eAgInPc1d1	stranický
spory	spor	k1gInPc1	spor
vedly	vést	k5eAaImAgInP	vést
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
"	"	kIx"	"
<g/>
Okresní	okresní	k2eAgNnSc1d1	okresní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
"	"	kIx"	"
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
a	a	k8xC	a
malorolnického	malorolnický	k2eAgInSc2d1	malorolnický
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
lidovci	lidovec	k1gMnPc1	lidovec
založili	založit	k5eAaPmAgMnP	založit
Zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
družinu	družina	k1gFnSc4	družina
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Okresní	okresní	k2eAgNnSc1d1	okresní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
"	"	kIx"	"
získalo	získat	k5eAaPmAgNnS	získat
právo	právo	k1gNnSc1	právo
vykupovat	vykupovat	k5eAaImF	vykupovat
obilí	obilí	k1gNnSc4	obilí
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
subkomissionářem	subkomissionář	k1gMnSc7	subkomissionář
"	"	kIx"	"
<g/>
Obilního	obilní	k2eAgInSc2d1	obilní
ústavu	ústav	k1gInSc2	ústav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
ředitelství	ředitelství	k1gNnSc2	ředitelství
a	a	k8xC	a
splnomocněným	splnomocněný	k2eAgMnSc7d1	splnomocněný
obchodním	obchodní	k2eAgMnSc7d1	obchodní
zástupcem	zástupce	k1gMnSc7	zástupce
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
Filipský	Filipský	k2eAgMnSc1d1	Filipský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
pražské	pražský	k2eAgNnSc1d1	Pražské
ústředí	ústředí	k1gNnSc1	ústředí
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
"	"	kIx"	"
<g/>
Oddělení	oddělení	k1gNnSc1	oddělení
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
úpravou	úprava	k1gFnSc7	úprava
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
důvěrník	důvěrník	k1gMnSc1	důvěrník
okresní	okresní	k2eAgFnSc2d1	okresní
organizace	organizace	k1gFnSc2	organizace
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
říšského	říšský	k2eAgNnSc2d1	říšské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
strany	strana	k1gFnSc2	strana
Em	Ema	k1gFnPc2	Ema
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
agenda	agenda	k1gFnSc1	agenda
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
ustanovilo	ustanovit	k5eAaPmAgNnS	ustanovit
Zemské	zemský	k2eAgNnSc1d1	zemské
sdružení	sdružení	k1gNnSc1	sdružení
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
zaměstnaných	zaměstnaný	k2eAgInPc2d1	zaměstnaný
v	v	k7c6	v
moravských	moravský	k2eAgNnPc6d1	Moravské
družstvech	družstvo	k1gNnPc6	družstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
družina	družina	k1gFnSc1	družina
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
nevýhodném	výhodný	k2eNgNnSc6d1	nevýhodné
postavení	postavení	k1gNnSc6	postavení
zejména	zejména	k9	zejména
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
agrárnického	agrárnický	k2eAgInSc2d1	agrárnický
obilního	obilní	k2eAgInSc2d1	obilní
monopolu	monopol	k1gInSc2	monopol
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
regulovaného	regulovaný	k2eAgNnSc2d1	regulované
hospodářství	hospodářství	k1gNnSc2	hospodářství
obilního	obilní	k2eAgNnSc2d1	obilní
a	a	k8xC	a
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
"	"	kIx"	"
<g/>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
obilní	obilní	k2eAgFnSc1d1	obilní
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Moravě	Morava	k1gFnSc6	Morava
mohla	moct	k5eAaImAgFnS	moct
komisionálně	komisionálně	k6eAd1	komisionálně
vykupovat	vykupovat	k5eAaImF	vykupovat
obilí	obilí	k1gNnSc4	obilí
pouze	pouze	k6eAd1	pouze
filiálka	filiálka	k1gFnSc1	filiálka
Zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
družiny	družina	k1gFnSc2	družina
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Agrární	agrární	k2eAgFnSc1d1	agrární
buržoazie	buržoazie	k1gFnSc1	buržoazie
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
i	i	k8xC	i
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
peněžních	peněžní	k2eAgInPc2d1	peněžní
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935-1937	[number]	k4	1935-1937
se	se	k3xPyFc4	se
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
jednota	jednota	k1gFnSc1	jednota
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
společenstev	společenstvo	k1gNnPc2	společenstvo
úvěrních	úvěrní	k2eAgMnPc2d1	úvěrní
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Fr.	Fr.	k1gMnSc1	Fr.
Wenzl	Wenzl	k1gMnSc1	Wenzl
<g/>
)	)	kIx)	)
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c6	na
budování	budování	k1gNnSc6	budování
nových	nový	k2eAgNnPc2d1	nové
družstev	družstvo	k1gNnPc2	družstvo
a	a	k8xC	a
obilních	obilní	k2eAgFnPc2d1	obilní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Stráž	stráž	k1gFnSc1	stráž
<g/>
,	,	kIx,	,
17.10	[number]	k4	17.10
<g/>
.1936	.1936	k4	.1936
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgNnSc1d1	okresní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
sloučilo	sloučit	k5eAaPmAgNnS	sloučit
s	s	k7c7	s
Jemnicí	Jemnice	k1gFnSc7	Jemnice
<g/>
,	,	kIx,	,
Třebelovicemi	Třebelovice	k1gFnPc7	Třebelovice
<g/>
,	,	kIx,	,
Grešlovým	Grešlův	k2eAgNnSc7d1	Grešlův
Mýtem	mýto	k1gNnSc7	mýto
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	s	k7c7	s
Želetavou	Želetava	k1gFnSc7	Želetava
<g/>
.	.	kIx.	.
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
družstva	družstvo	k1gNnSc2	družstvo
<g/>
"	"	kIx"	"
Dům	dům	k1gInSc1	dům
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
"	"	kIx"	"
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
obilní	obilní	k2eAgNnSc4d1	obilní
silo	silo	k1gNnSc4	silo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Domě	dům	k1gInSc6	dům
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
osvěty	osvěta	k1gFnSc2	osvěta
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
odbornou	odborný	k2eAgFnSc4d1	odborná
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
archiv	archiv	k1gInSc4	archiv
pro	pro	k7c4	pro
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
spolek	spolek	k1gInSc4	spolek
a	a	k8xC	a
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
poradnu	poradna	k1gFnSc4	poradna
<g/>
,	,	kIx,	,
spolková	spolkový	k2eAgFnSc1d1	spolková
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
porady	porada	k1gFnPc4	porada
<g/>
,	,	kIx,	,
cvičná	cvičný	k2eAgFnSc1d1	cvičná
kuchyně	kuchyně	k1gFnSc1	kuchyně
pro	pro	k7c4	pro
občasné	občasný	k2eAgInPc4d1	občasný
kurzy	kurz	k1gInPc4	kurz
<g/>
,	,	kIx,	,
v	v	k7c6	v
poschodí	poschodí	k1gNnSc6	poschodí
velká	velký	k2eAgFnSc1d1	velká
přednášková	přednáškový	k2eAgFnSc1d1	přednášková
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Venkov	venkov	k1gInSc1	venkov
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
8.10	[number]	k4	8.10
<g/>
.1936	.1936	k4	.1936
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
založili	založit	k5eAaPmAgMnP	založit
lidovci	lidovec	k1gMnPc1	lidovec
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
"	"	kIx"	"
<g/>
Lidovou	lidový	k2eAgFnSc4d1	lidová
záložnu	záložna	k1gFnSc4	záložna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sklady	sklad	k1gInPc1	sklad
Zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
družiny	družina	k1gFnSc2	družina
(	(	kIx(	(
<g/>
ZD	ZD	kA	ZD
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
<g/>
,	,	kIx,	,
Dačicích	Dačice	k1gFnPc6	Dačice
<g/>
,	,	kIx,	,
Lukách	luka	k1gNnPc6	luka
n.	n.	k?	n.
Jihl	jihnout	k5eAaImAgMnS	jihnout
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Jaroměřicích	Jaroměřice	k1gFnPc6	Jaroměřice
n.	n.	k?	n.
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Třebelovicích	Třebelovice	k1gFnPc6	Třebelovice
a	a	k8xC	a
Vícenicích	Vícenice	k1gFnPc6	Vícenice
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
družina	družina	k1gFnSc1	družina
vykupovala	vykupovat	k5eAaImAgFnS	vykupovat
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
prodávala	prodávat	k5eAaImAgNnP	prodávat
umělá	umělý	k2eAgNnPc1d1	umělé
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
krmiva	krmivo	k1gNnPc1	krmivo
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
benzín	benzín	k1gInSc1	benzín
<g/>
,	,	kIx,	,
oleje	olej	k1gInPc1	olej
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
potřeby	potřeba	k1gFnPc1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
otevřela	otevřít	k5eAaPmAgFnS	otevřít
ZD	ZD	kA	ZD
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
nové	nový	k2eAgNnSc1d1	nové
sedmipatrové	sedmipatrový	k2eAgNnSc1d1	sedmipatrový
obilné	obilný	k2eAgNnSc1d1	obilné
silo	silo	k1gNnSc1	silo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
družina	družina	k1gFnSc1	družina
v	v	k7c6	v
M.	M.	kA	M.
Budějovicích	Budějovice	k1gInPc6	Budějovice
komisionářem	komisionář	k1gMnSc7	komisionář
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
obilní	obilní	k2eAgFnSc2d1	obilní
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
<g/>
:	:	kIx,	:
Mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Jaroměřice	Jaroměřice	k1gFnPc1	Jaroměřice
n.	n.	k?	n.
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Blížkovice	Blížkovice	k1gFnPc1	Blížkovice
<g/>
,	,	kIx,	,
Třebelovice	Třebelovice	k1gFnPc1	Třebelovice
<g/>
,	,	kIx,	,
Jemnice	Jemnice	k1gFnPc1	Jemnice
<g/>
,	,	kIx,	,
Dačice	Dačice	k1gFnPc1	Dačice
<g/>
,	,	kIx,	,
Telč	Telč	k1gFnSc1	Telč
<g/>
,	,	kIx,	,
Maršov	Maršov	k1gInSc1	Maršov
<g/>
,	,	kIx,	,
Třešť	třeštit	k5eAaImRp2nS	třeštit
<g/>
,	,	kIx,	,
Luka	luka	k1gNnPc1	luka
<g/>
,	,	kIx,	,
Kamenice	Kamenice	k1gFnPc1	Kamenice
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Budeč	Budeč	k1gFnSc1	Budeč
<g/>
,	,	kIx,	,
Želetava	Želetava	k1gFnSc1	Želetava
<g/>
,	,	kIx,	,
Krasonice	Krasonice	k1gFnSc1	Krasonice
<g/>
,	,	kIx,	,
Grešlové	Grešlový	k2eAgNnSc1d1	Grešlový
Mýto	mýto	k1gNnSc1	mýto
<g/>
,	,	kIx,	,
Batelov	Batelov	k1gInSc1	Batelov
<g/>
,	,	kIx,	,
Stonařov	Stonařov	k1gInSc1	Stonařov
<g/>
,	,	kIx,	,
Studená	Studená	k1gFnSc1	Studená
<g/>
,	,	kIx,	,
Brtnice	Brtnice	k1gFnSc1	Brtnice
<g/>
,	,	kIx,	,
Jamné	Jamné	k2eAgFnSc1d1	Jamné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Stráž	stráž	k1gFnSc1	stráž
<g/>
:	:	kIx,	:
orgán	orgán	k1gInSc1	orgán
strany	strana	k1gFnSc2	strana
katolické	katolický	k2eAgFnSc2d1	katolická
národní	národní	k2eAgFnSc2d1	národní
na	na	k7c6	na
Západní	západní	k2eAgFnSc6d1	západní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
1.7	[number]	k4	1.7
<g/>
.1937	.1937	k4	.1937
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
započala	započnout	k5eAaPmAgFnS	započnout
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
družina	družina	k1gFnSc1	družina
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
družstevní	družstevní	k2eAgFnSc2d1	družstevní
mlékárny	mlékárna	k1gFnSc2	mlékárna
<g/>
.	.	kIx.	.
</s>
<s>
Mlékárna	mlékárna	k1gFnSc1	mlékárna
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
nejmodernějšími	moderní	k2eAgInPc7d3	nejmodernější
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
disponovala	disponovat	k5eAaBmAgFnS	disponovat
výrobou	výroba	k1gFnSc7	výroba
másla	máslo	k1gNnSc2	máslo
a	a	k8xC	a
sýrů	sýr	k1gInPc2	sýr
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
odstředěného	odstředěný	k2eAgNnSc2d1	odstředěné
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
zahuštěného	zahuštěný	k2eAgNnSc2d1	zahuštěné
a	a	k8xC	a
kondenzovaného	kondenzovaný	k2eAgNnSc2d1	kondenzované
<g/>
,	,	kIx,	,
smetany	smetana	k1gFnPc1	smetana
<g/>
,	,	kIx,	,
podmáslí	podmáslí	k1gNnSc1	podmáslí
<g/>
,	,	kIx,	,
tvarohu	tvaroh	k1gInSc2	tvaroh
a	a	k8xC	a
sušeného	sušený	k2eAgNnSc2d1	sušené
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
výrobou	výroba	k1gFnSc7	výroba
umělých	umělý	k2eAgFnPc2d1	umělá
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Dostavěny	dostavěn	k2eAgFnPc1d1	dostavěna
byly	být	k5eAaImAgFnP	být
chladírny	chladírna	k1gFnPc1	chladírna
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
nová	nový	k2eAgFnSc1d1	nová
samostatná	samostatný	k2eAgFnSc1d1	samostatná
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
sběrnu	sběrna	k1gFnSc4	sběrna
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Českomoravský	českomoravský	k2eAgInSc1d1	českomoravský
svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc4	tuk
a	a	k8xC	a
vejce	vejce	k1gNnPc4	vejce
"	"	kIx"	"
<g/>
Mlékárnu	mlékárna	k1gFnSc4	mlékárna
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
družiny	družina	k1gFnSc2	družina
Moravské	moravský	k2eAgInPc4d1	moravský
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
"	"	kIx"	"
obvodní	obvodní	k2eAgFnSc4d1	obvodní
sběrnou	sběrna	k1gFnSc7	sběrna
pro	pro	k7c4	pro
vejce	vejce	k1gNnPc4	vejce
z	z	k7c2	z
obcí	obec	k1gFnPc2	obec
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
M.	M.	kA	M.
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
z	z	k7c2	z
obcí	obec	k1gFnPc2	obec
bývalého	bývalý	k2eAgInSc2d1	bývalý
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
Vranov	Vranov	k1gInSc1	Vranov
<g/>
:	:	kIx,	:
Vysočany	Vysočany	k1gInPc1	Vysočany
<g/>
,	,	kIx,	,
Oslnovice	Oslnovice	k1gFnPc1	Oslnovice
<g/>
,	,	kIx,	,
Zblovice	Zblovice	k1gFnPc1	Zblovice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Bítov	Bítov	k1gInSc1	Bítov
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
převzal	převzít	k5eAaPmAgMnS	převzít
"	"	kIx"	"
<g/>
Okresní	okresní	k2eAgNnSc1d1	okresní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
po	po	k7c6	po
zrušené	zrušený	k2eAgFnSc6d1	zrušená
straně	strana	k1gFnSc6	strana
agrárníků	agrárník	k1gMnPc2	agrárník
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Jednotný	jednotný	k2eAgInSc1d1	jednotný
svaz	svaz	k1gInSc1	svaz
českých	český	k2eAgMnPc2d1	český
zemědělců	zemědělec	k1gMnPc2	zemědělec
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
JSČZ	JSČZ	kA	JSČZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
Ďurišovým	Ďurišův	k2eAgNnSc7d1	Ďurišův
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
Okresního	okresní	k2eAgNnSc2d1	okresní
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
družstva	družstvo	k1gNnSc2	družstvo
se	se	k3xPyFc4	se
také	také	k9	také
nalézala	nalézat	k5eAaImAgFnS	nalézat
Okresní	okresní	k2eAgFnSc1d1	okresní
úřadovna	úřadovna	k1gFnSc1	úřadovna
JSČZ	JSČZ	kA	JSČZ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zřídil	zřídit	k5eAaPmAgInS	zřídit
JSČZ	JSČZ	kA	JSČZ
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Okresního	okresní	k2eAgNnSc2d1	okresní
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
družstva	družstvo	k1gNnSc2	družstvo
Strojní	strojní	k2eAgFnSc4d1	strojní
traktorovou	traktorový	k2eAgFnSc4d1	traktorová
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
bylo	být	k5eAaImAgNnS	být
Okresní	okresní	k2eAgNnSc1d1	okresní
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
družstvo	družstvo	k1gNnSc1	družstvo
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
ministra	ministr	k1gMnSc2	ministr
výkupu	výkup	k1gInSc2	výkup
č.	č.	k?	č.
362	[number]	k4	362
<g/>
/	/	kIx~	/
<g/>
1952	[number]	k4	1952
Ú.l.	Ú.l.	k1gFnPc2	Ú.l.
ze	z	k7c2	z
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
znárodněno	znárodnit	k5eAaPmNgNnS	znárodnit
<g/>
.	.	kIx.	.
</s>
<s>
Mlékárna	mlékárna	k1gFnSc1	mlékárna
byla	být	k5eAaImAgFnS	být
začleněna	začlenit	k5eAaPmNgFnS	začlenit
pod	pod	k7c4	pod
mlékárnu	mlékárna	k1gFnSc4	mlékárna
v	v	k7c6	v
Jaroměřicích	Jaroměřice	k1gFnPc6	Jaroměřice
n.	n.	k?	n.
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
a	a	k8xC	a
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
a	a	k8xC	a
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
"	"	kIx"	"
<g/>
Okresní	okresní	k2eAgFnSc4d1	okresní
družstevní	družstevní	k2eAgFnSc4d1	družstevní
mlékárnu	mlékárna	k1gFnSc4	mlékárna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Okresní	okresní	k2eAgFnSc1d1	okresní
družstevní	družstevní	k2eAgFnSc1d1	družstevní
mlékárna	mlékárna	k1gFnSc1	mlékárna
<g/>
,	,	kIx,	,
z.	z.	k?	z.
s.	s.	k?	s.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
;	;	kIx,	;
1952	[number]	k4	1952
Horácké	horácký	k2eAgFnPc1d1	Horácká
mlékárny	mlékárna	k1gFnPc1	mlékárna
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
1953	[number]	k4	1953
Mlékařské	mlékařský	k2eAgInPc1d1	mlékařský
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
;	;	kIx,	;
1958	[number]	k4	1958
Jihlavské	jihlavský	k2eAgFnPc1d1	Jihlavská
mlékárny	mlékárna	k1gFnPc1	mlékárna
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Jaroměřice	Jaroměřice	k1gFnPc1	Jaroměřice
<g/>
;	;	kIx,	;
1960	[number]	k4	1960
Lacrum	lacrum	k1gInSc1	lacrum
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc4	provoz
Jaroměřice	Jaroměřice	k1gFnPc4	Jaroměřice
<g/>
;	;	kIx,	;
1994	[number]	k4	1994
a.s.	a.s.	k?	a.s.
Lacrum	lacrum	k1gInSc1	lacrum
Brno	Brno	k1gNnSc1	Brno
<g/>
;	;	kIx,	;
1992	[number]	k4	1992
J	J	kA	J
+	+	kIx~	+
R	R	kA	R
MLÉKÁRNA	mlékárna	k1gFnSc1	mlékárna
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
Noví	nový	k2eAgMnPc1d1	nový
majitelé	majitel	k1gMnPc1	majitel
mlékárny	mlékárna	k1gFnSc2	mlékárna
(	(	kIx(	(
<g/>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Štecher	Štechra	k1gFnPc2	Štechra
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
zrušili	zrušit	k5eAaPmAgMnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
družstvo	družstvo	k1gNnSc4	družstvo
komunisté	komunista	k1gMnPc1	komunista
transformovali	transformovat	k5eAaBmAgMnP	transformovat
na	na	k7c4	na
JZD	JZD	kA	JZD
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
traktorovou	traktorový	k2eAgFnSc7d1	traktorová
stanicí	stanice	k1gFnSc7	stanice
(	(	kIx(	(
<g/>
STS	STS	kA	STS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
privatizováno	privatizovat	k5eAaImNgNnS	privatizovat
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
Zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
družstvo	družstvo	k1gNnSc1	družstvo
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
likvidaci	likvidace	k1gFnSc6	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
živnostníci	živnostník	k1gMnPc1	živnostník
==	==	k?	==
</s>
</p>
<p>
<s>
Řemeslníky	řemeslník	k1gMnPc4	řemeslník
a	a	k8xC	a
živnostníky	živnostník	k1gMnPc4	živnostník
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
Zemská	zemský	k2eAgFnSc1d1	zemská
jednota	jednota	k1gFnSc1	jednota
živnostenských	živnostenský	k2eAgNnPc2d1	živnostenské
společenstev	společenstvo	k1gNnPc2	společenstvo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Okresní	okresní	k2eAgFnSc1d1	okresní
jednota	jednota	k1gFnSc1	jednota
živnostenských	živnostenský	k2eAgNnPc2d1	živnostenské
společenstev	společenstvo	k1gNnPc2	společenstvo
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Smrčkové	Smrčková	k1gFnSc2	Smrčková
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
slévárna	slévárna	k1gFnSc1	slévárna
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
původně	původně	k6eAd1	původně
malé	malý	k2eAgFnSc2d1	malá
rodinné	rodinný	k2eAgFnSc2d1	rodinná
dílny	dílna	k1gFnSc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
236	[number]	k4	236
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
působila	působit	k5eAaImAgFnS	působit
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
Bohumila	Bohumil	k1gMnSc2	Bohumil
Jenerála	Jenerála	k?	Jenerála
<g/>
,	,	kIx,	,
parní	parní	k2eAgFnSc1d1	parní
pila	pila	k1gFnSc1	pila
a	a	k8xC	a
velkoobchod	velkoobchod	k1gInSc1	velkoobchod
se	s	k7c7	s
dřevem	dřevo	k1gNnSc7	dřevo
Antonína	Antonín	k1gMnSc2	Antonín
Bachela	Bachel	k1gMnSc2	Bachel
<g/>
,	,	kIx,	,
tesařství	tesařství	k1gNnSc1	tesařství
a	a	k8xC	a
parní	parní	k2eAgFnSc1d1	parní
pila	pila	k1gFnSc1	pila
Leopolda	Leopold	k1gMnSc2	Leopold
Jahody	Jahoda	k1gMnSc2	Jahoda
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc4	mlýn
Josefa	Josef	k1gMnSc2	Josef
Černohouse	Černohouse	k1gFnSc2	Černohouse
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
drobných	drobný	k2eAgFnPc2d1	drobná
živnostenských	živnostenský	k2eAgFnPc2d1	Živnostenská
firem	firma	k1gFnPc2	firma
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
(	(	kIx(	(
<g/>
cukrářství	cukrářství	k1gNnSc1	cukrářství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rajmund	Rajmund	k1gInSc1	Rajmund
Aujeský	Aujeský	k2eAgInSc1d1	Aujeský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
pekařství	pekařství	k1gNnSc1	pekařství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
cukrářství	cukrářství	k1gNnSc1	cukrářství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
radiozávod	radiozávod	k1gInSc1	radiozávod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Klimt	Klimt	k1gMnSc1	Klimt
(	(	kIx(	(
<g/>
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šplíchal	Šplíchal	k1gMnSc1	Šplíchal
(	(	kIx(	(
<g/>
sklad	sklad	k1gInSc1	sklad
nábytku	nábytek	k1gInSc2	nábytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šabata	Šabata	k1gMnSc1	Šabata
(	(	kIx(	(
<g/>
kolářství	kolářství	k1gNnSc1	kolářství
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
karoserie	karoserie	k1gFnSc1	karoserie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Brych	Brych	k1gMnSc1	Brych
(	(	kIx(	(
<g/>
řeznictví	řeznictví	k1gNnSc1	řeznictví
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
velkoobchod	velkoobchod	k1gInSc1	velkoobchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kopeček	Kopeček	k1gMnSc1	Kopeček
(	(	kIx(	(
<g/>
koncesovaná	koncesovaný	k2eAgFnSc1d1	koncesovaná
nákladní	nákladní	k2eAgFnSc1d1	nákladní
<g/>
,	,	kIx,	,
autobusová	autobusový	k2eAgFnSc1d1	autobusová
a	a	k8xC	a
osobní	osobní	k2eAgFnSc1d1	osobní
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Vašta	Vašto	k1gNnSc2	Vašto
(	(	kIx(	(
<g/>
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Trojan	Trojan	k1gMnSc1	Trojan
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
oděvů	oděv	k1gInPc2	oděv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Špát	Špát	k1gMnSc1	Špát
(	(	kIx(	(
<g/>
smíšené	smíšený	k2eAgNnSc1d1	smíšené
zboží	zboží	k1gNnSc1	zboží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kaletus	Kaletus	k1gMnSc1	Kaletus
(	(	kIx(	(
<g/>
nožířství	nožířství	k1gNnSc1	nožířství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
Kreuterová	Kreuterová	k1gFnSc1	Kreuterová
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc1d1	národní
správa	správa	k1gFnSc1	správa
sklo-porcelán	skloorcelán	k1gInSc1	sklo-porcelán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kreuter	Kreuter	k1gMnSc1	Kreuter
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc1d1	národní
správa	správa	k1gFnSc1	správa
rámování	rámování	k1gNnSc2	rámování
obrazů	obraz	k1gInPc2	obraz
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
sklenářství	sklenářství	k1gNnSc1	sklenářství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Babák	Babák	k1gInSc1	Babák
a	a	k8xC	a
V.	V.	kA	V.
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
stavitelství	stavitelství	k1gNnSc1	stavitelství
a	a	k8xC	a
tesařství	tesařství	k1gNnSc1	tesařství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Hromádka	hromádka	k1gFnSc1	hromádka
(	(	kIx(	(
<g/>
železo-porcelán-sklo-smaltované	železoorcelánklomaltovaný	k2eAgNnSc1d1	železo-porcelán-sklo-smaltovaný
nádobí	nádobí	k1gNnSc1	nádobí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Sedlák	Sedlák	k1gMnSc1	Sedlák
(	(	kIx(	(
<g/>
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
závod	závod	k1gInSc1	závod
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
a	a	k8xC	a
prodejna	prodejna	k1gFnSc1	prodejna
s	s	k7c7	s
papírem	papír	k1gInSc7	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
oprava	oprava	k1gFnSc1	oprava
<g/>
,	,	kIx,	,
motocykly	motocykl	k1gInPc1	motocykl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Nesládek	Nesládek	k1gInSc1	Nesládek
(	(	kIx(	(
<g/>
drogerie	drogerie	k1gFnSc1	drogerie
<g/>
)	)	kIx)	)
a	a	k8xC	a
prodejna	prodejna	k1gFnSc1	prodejna
obuvi	obuv	k1gFnSc2	obuv
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Baťa	Baťa	k1gMnSc1	Baťa
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
znárodnění	znárodnění	k1gNnSc6	znárodnění
působily	působit	k5eAaImAgInP	působit
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
závody	závod	k1gInPc1	závod
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
strojírna	strojírna	k1gFnSc1	strojírna
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Smrčkova	Smrčkův	k2eAgFnSc1d1	Smrčkova
továrna	továrna	k1gFnSc1	továrna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
pily	pila	k1gFnPc1	pila
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Bachelova	Bachelův	k2eAgFnSc1d1	Bachelův
pila	pila	k1gFnSc1	pila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kovo	kovo	k1gNnSc1	kovo
a	a	k8xC	a
dřevoprůmysl	dřevoprůmysl	k1gInSc1	dřevoprůmysl
(	(	kIx(	(
<g/>
fa	fa	kA	fa
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
M.	M.	kA	M.
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sběrné	sběrný	k2eAgFnPc1d1	sběrná
suroviny	surovina	k1gFnPc1	surovina
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
(	(	kIx(	(
<g/>
Osvačilova	Osvačilův	k2eAgFnSc1d1	Osvačilův
koželužna	koželužna	k1gFnSc1	koželužna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Středomoravské	středomoravský	k2eAgInPc1d1	středomoravský
mlýny	mlýn	k1gInPc1	mlýn
(	(	kIx(	(
<g/>
mlýn	mlýn	k1gInSc1	mlýn
J.	J.	kA	J.
Černohouse	Černohouse	k1gFnSc2	Černohouse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
lihovary	lihovar	k1gInPc1	lihovar
a	a	k8xC	a
cukrárny	cukrárna	k1gFnPc1	cukrárna
(	(	kIx(	(
<g/>
fa	fa	kA	fa
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janoštík	Janoštík	k1gMnSc1	Janoštík
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
ovocných	ovocný	k2eAgFnPc2d1	ovocná
šťáv	šťáva	k1gFnPc2	šťáva
<g/>
,	,	kIx,	,
Mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
patřily	patřit	k5eAaImAgInP	patřit
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
městům	město	k1gNnPc3	město
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
převahou	převaha	k1gFnSc7	převaha
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
hospodářsky	hospodářsky	k6eAd1	hospodářsky
silnou	silný	k2eAgFnSc7d1	silná
německou	německý	k2eAgFnSc7d1	německá
minoritou	minorita	k1gFnSc7	minorita
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
dlouho	dlouho	k6eAd1	dlouho
dobu	doba	k1gFnSc4	doba
udržovalo	udržovat	k5eAaImAgNnS	udržovat
řemeslný	řemeslný	k2eAgInSc4d1	řemeslný
ráz	ráz	k1gInSc4	ráz
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
cechem	cech	k1gInSc7	cech
obuvníků	obuvník	k1gMnPc2	obuvník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pracovali	pracovat	k5eAaImAgMnP	pracovat
koželuhové	koželuh	k1gMnPc1	koželuh
<g/>
,	,	kIx,	,
výrobci	výrobce	k1gMnPc1	výrobce
podešví	podešev	k1gFnPc2	podešev
<g/>
,	,	kIx,	,
řemenáři	řemenář	k1gMnPc1	řemenář
a	a	k8xC	a
sedláři	sedlář	k1gMnPc1	sedlář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnPc1	krejčí
<g/>
,	,	kIx,	,
tkalci	tkadlec	k1gMnPc1	tkadlec
<g/>
,	,	kIx,	,
barvíři	barvíř	k1gMnPc1	barvíř
<g/>
,	,	kIx,	,
kloboučníci	kloboučník	k1gMnPc1	kloboučník
<g/>
,	,	kIx,	,
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
pekaři	pekař	k1gMnPc1	pekař
a	a	k8xC	a
mlynáři	mlynář	k1gMnPc1	mlynář
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rolnických	rolnický	k2eAgFnPc2d1	rolnická
rodin	rodina	k1gFnPc2	rodina
sídlila	sídlit	k5eAaImAgFnS	sídlit
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
a	a	k8xC	a
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
zřídil	zřídit	k5eAaPmAgMnS	zřídit
MNV	MNV	kA	MNV
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
internační	internační	k2eAgInSc1d1	internační
pracovní	pracovní	k2eAgInSc1d1	pracovní
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
internováni	internován	k2eAgMnPc1d1	internován
občané	občan	k1gMnPc1	občan
z	z	k7c2	z
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
a	a	k8xC	a
z	z	k7c2	z
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
obcí	obec	k1gFnPc2	obec
okresu	okres	k1gInSc2	okres
M.	M.	kA	M.
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
Lovčovic	Lovčovice	k1gFnPc2	Lovčovice
<g/>
,	,	kIx,	,
Chvalkovic	Chvalkovice	k1gFnPc2	Chvalkovice
<g/>
,	,	kIx,	,
Županovic	Županovice	k1gFnPc2	Županovice
<g/>
,	,	kIx,	,
Rancířova	Rancířův	k2eAgInSc2d1	Rancířův
<g/>
,	,	kIx,	,
Vratěnína	Vratěnín	k1gInSc2	Vratěnín
<g/>
,	,	kIx,	,
Mešovic	Mešovice	k1gFnPc2	Mešovice
<g/>
,	,	kIx,	,
Dančovic	Dančovice	k1gFnPc2	Dančovice
<g/>
,	,	kIx,	,
Korolup	Korolup	k1gInSc1	Korolup
<g/>
,	,	kIx,	,
Dešné	Dešný	k2eAgFnSc2d1	Dešná
<g/>
,	,	kIx,	,
Hluboké	Hluboká	k1gFnSc2	Hluboká
<g/>
,	,	kIx,	,
Lubnice	Lubnice	k1gFnSc2	Lubnice
a	a	k8xC	a
Uherčic	Uherčice	k1gFnPc2	Uherčice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgInS	zadržet
i	i	k9	i
MUDr.	MUDr.	kA	MUDr.
Ernst	Ernst	k1gMnSc1	Ernst
Bruckner	Bruckner	k1gMnSc1	Bruckner
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Arnošt	Arnošt	k1gMnSc1	Arnošt
Bruckner	Bruckner	k1gMnSc1	Bruckner
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
okresní	okresní	k2eAgMnSc1d1	okresní
zdravotní	zdravotní	k2eAgMnSc1d1	zdravotní
rada	rada	k1gMnSc1	rada
ze	z	k7c2	z
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
protektorátu	protektorát	k1gInSc2	protektorát
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-45	[number]	k4	1939-45
působil	působit	k5eAaImAgInS	působit
jako	jako	k8xS	jako
okresní	okresní	k2eAgFnSc1d1	okresní
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
podporoval	podporovat	k5eAaImAgMnS	podporovat
svého	svůj	k1gMnSc4	svůj
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
Josefa	Josef	k1gMnSc4	Josef
Grňu	Grňa	k1gMnSc4	Grňa
v	v	k7c6	v
odbojové	odbojový	k2eAgFnSc6d1	odbojová
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
rozvodové	rozvodový	k2eAgNnSc4d1	rozvodové
řízení	řízení	k1gNnSc4	řízení
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
německou	německý	k2eAgFnSc7d1	německá
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
internace	internace	k1gFnSc2	internace
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
zabaven	zabaven	k2eAgInSc4d1	zabaven
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
3,5	[number]	k4	3,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Brucknerovi	Bruckner	k1gMnSc3	Bruckner
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
zejména	zejména	k9	zejména
MNV	MNV	kA	MNV
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
žádosti	žádost	k1gFnSc3	žádost
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
Bruckner	Bruckner	k1gMnSc1	Bruckner
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
jako	jako	k8xC	jako
obvodní	obvodní	k2eAgMnSc1d1	obvodní
lékař	lékař	k1gMnSc1	lékař
do	do	k7c2	do
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
Vratěnín	Vratěnín	k1gInSc1	Vratěnín
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1947	[number]	k4	1947
trestní	trestní	k2eAgFnSc1d1	trestní
nalézací	nalézací	k2eAgFnSc1d1	nalézací
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
TNT	TNT	kA	TNT
<g/>
)	)	kIx)	)
ONV	ONV	kA	ONV
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nevině	nevina	k1gFnSc6	nevina
<g/>
.	.	kIx.	.
</s>
<s>
Čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
občanství	občanství	k1gNnSc4	občanství
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
navráceno	navrácen	k2eAgNnSc1d1	navráceno
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1948	[number]	k4	1948
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
vystěhování	vystěhování	k1gNnSc6	vystěhování
německých	německý	k2eAgMnPc2d1	německý
lékařů	lékař	k1gMnPc2	lékař
z	z	k7c2	z
pohraničí	pohraničí	k1gNnSc2	pohraničí
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Bruckner	Bruckner	k1gMnSc1	Bruckner
působil	působit	k5eAaImAgMnS	působit
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
jako	jako	k8xS	jako
praktický	praktický	k2eAgMnSc1d1	praktický
lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
Gastern	Gastern	k1gMnSc1	Gastern
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
odsun	odsun	k1gInSc4	odsun
asi	asi	k9	asi
300	[number]	k4	300
Němců	Němec	k1gMnPc2	Němec
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
přibyly	přibýt	k5eAaPmAgFnP	přibýt
kamenné	kamenný	k2eAgFnPc4d1	kamenná
desky	deska	k1gFnPc4	deska
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Stráž	stráž	k1gFnSc1	stráž
na	na	k7c6	na
Dyji	Dyje	k1gFnSc6	Dyje
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
4.04	[number]	k4	4.04
<g/>
.1947	.1947	k4	.1947
</s>
</p>
<p>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
M.	M.	kA	M.
Budějovicích	Budějovice	k1gInPc6	Budějovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
pokles	pokles	k1gInSc4	pokles
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
oproti	oproti	k7c3	oproti
předválečným	předválečný	k2eAgNnPc3d1	předválečné
létům	léto	k1gNnPc3	léto
<g/>
;	;	kIx,	;
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
4	[number]	k4	4
345	[number]	k4	345
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
038	[number]	k4	038
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
2	[number]	k4	2
307	[number]	k4	307
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
byl	být	k5eAaImAgInS	být
odůvodněn	odůvodnit	k5eAaPmNgInS	odůvodnit
odchodem	odchod	k1gInSc7	odchod
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
1	[number]	k4	1
025	[number]	k4	025
domech	dům	k1gInPc6	dům
o	o	k7c4	o
1	[number]	k4	1
338	[number]	k4	338
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
ještě	ještě	k9	ještě
16	[number]	k4	16
domů	dům	k1gInPc2	dům
úplně	úplně	k6eAd1	úplně
zbořených	zbořený	k2eAgInPc2d1	zbořený
a	a	k8xC	a
15	[number]	k4	15
domů	dům	k1gInPc2	dům
k	k	k7c3	k
obývání	obývání	k1gNnSc3	obývání
nezpůsobilých	způsobilý	k2eNgMnPc2d1	nezpůsobilý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
koná	konat	k5eAaImIp3nS	konat
Horákův	Horákův	k2eAgInSc1d1	Horákův
Horácký	horácký	k2eAgInSc1d1	horácký
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jej	on	k3xPp3gMnSc4	on
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
léta	léto	k1gNnSc2	léto
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
probíhá	probíhat	k5eAaImIp3nS	probíhat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Václava	Václav	k1gMnSc2	Václav
Hudečka	Hudeček	k1gMnSc2	Hudeček
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
hostů	host	k1gMnPc2	host
(	(	kIx(	(
<g/>
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
již	již	k6eAd1	již
11	[number]	k4	11
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
ve	v	k7c6	v
městě	město	k1gNnSc6	město
probíhá	probíhat	k5eAaImIp3nS	probíhat
tradiční	tradiční	k2eAgNnSc1d1	tradiční
Císařské	císařský	k2eAgNnSc1d1	císařské
posvícení	posvícení	k1gNnSc1	posvícení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doprovázeno	doprovázet	k5eAaImNgNnS	doprovázet
výstavami	výstava	k1gFnPc7	výstava
<g/>
,	,	kIx,	,
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
trhy	trh	k1gInPc7	trh
<g/>
,	,	kIx,	,
průvodem	průvod	k1gInSc7	průvod
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
posvícenským	posvícenský	k2eAgInSc7d1	posvícenský
programem	program	k1gInSc7	program
a	a	k8xC	a
pouťovými	pouťový	k2eAgFnPc7d1	pouťová
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
152	[number]	k4	152
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
151	[number]	k4	151
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
Raabs	Raabs	k1gInSc1	Raabs
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
–	–	k?	–
<g/>
Okříšky	Okříšek	k1gInPc7	Okříšek
a	a	k8xC	a
výletní	výletní	k2eAgFnSc1d1	výletní
trasa	trasa	k1gFnSc1	trasa
z	z	k7c2	z
Jemnice	Jemnice	k1gFnSc2	Jemnice
do	do	k7c2	do
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
výletní	výletní	k2eAgFnSc1d1	výletní
trasa	trasa	k1gFnSc1	trasa
se	se	k3xPyFc4	se
v	v	k7c6	v
městě	město	k1gNnSc6	město
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
cestovní	cestovní	k2eAgFnSc4d1	cestovní
trasu	trasa	k1gFnSc4	trasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
Správu	správa	k1gFnSc4	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
zastávka	zastávka	k1gFnSc1	zastávka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
kilometry	kilometr	k1gInPc7	kilometr
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
výpravčí	výpravčí	k1gFnSc2	výpravčí
budovy	budova	k1gFnSc2	budova
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
vichřici	vichřice	k1gFnSc6	vichřice
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
stržena	stržen	k2eAgFnSc1d1	stržena
část	část	k1gFnSc1	část
střechy	střecha	k1gFnSc2	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
a	a	k8xC	a
plány	plán	k1gInPc4	plán
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
modernizace	modernizace	k1gFnSc2	modernizace
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
expozice	expozice	k1gFnSc2	expozice
uzavřeno	uzavřen	k2eAgNnSc4d1	uzavřeno
muzeum	muzeum	k1gNnSc4	muzeum
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
termín	termín	k1gInSc1	termín
znovuotevření	znovuotevření	k1gNnSc2	znovuotevření
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
2015	[number]	k4	2015
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
u	u	k7c2	u
letiště	letiště	k1gNnSc2	letiště
stavět	stavět	k5eAaImF	stavět
nová	nový	k2eAgFnSc1d1	nová
ulice	ulice	k1gFnSc1	ulice
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lažínská	Lažínský	k2eAgFnSc1d1	Lažínský
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniká	vznikat	k5eAaImIp3nS	vznikat
např.	např.	kA	např.
i	i	k8xC	i
ulice	ulice	k1gFnSc2	ulice
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
Spravedlností	spravedlnost	k1gFnSc7	spravedlnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krajská	krajský	k2eAgFnSc1d1	krajská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
projektování	projektování	k1gNnSc2	projektování
obchvatu	obchvat	k1gInSc2	obchvat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Obchvat	obchvat	k1gInSc1	obchvat
má	mít	k5eAaImIp3nS	mít
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
sjezd	sjezd	k1gInSc4	sjezd
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
na	na	k7c4	na
Jaroměřice	Jaroměřice	k1gFnPc4	Jaroměřice
nad	nad	k7c7	nad
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
<g/>
.	.	kIx.	.
</s>
<s>
Obchvat	obchvat	k1gInSc1	obchvat
by	by	kYmCp3nP	by
začínal	začínat	k5eAaImAgInS	začínat
napojením	napojení	k1gNnSc7	napojení
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4118	[number]	k4	4118
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c6	na
Vesci	Vesce	k1gFnSc6	Vesce
<g/>
,	,	kIx,	,
končil	končit	k5eAaImAgInS	končit
by	by	kYmCp3nS	by
napojením	napojení	k1gNnSc7	napojení
na	na	k7c4	na
stávající	stávající	k2eAgFnSc4d1	stávající
komunikaci	komunikace	k1gFnSc4	komunikace
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
152	[number]	k4	152
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Jaroměřice	Jaroměřice	k1gFnPc4	Jaroměřice
nad	nad	k7c7	nad
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
úseku	úsek	k1gInSc2	úsek
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
tří	tři	k4xCgInPc2	tři
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
za	za	k7c2	za
podmínky	podmínka	k1gFnSc2	podmínka
získání	získání	k1gNnSc2	získání
potřebných	potřebný	k2eAgInPc2d1	potřebný
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
evropské	evropský	k2eAgFnPc4d1	Evropská
dotace	dotace	k1gFnPc4	dotace
ne	ne	k9	ne
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Majetková	majetkový	k2eAgFnSc1d1	majetková
příprava	příprava	k1gFnSc1	příprava
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
úředníci	úředník	k1gMnPc1	úředník
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
stupně	stupeň	k1gInPc4	stupeň
projektové	projektový	k2eAgFnSc2d1	projektová
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2022	[number]	k4	2022
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Litohoř	Litohoř	k1gFnSc4	Litohoř
být	být	k5eAaImF	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
31	[number]	k4	31
hektarů	hektar	k1gInPc2	hektar
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
525	[number]	k4	525
pracovními	pracovní	k2eAgNnPc7d1	pracovní
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
podpořila	podpořit	k5eAaPmAgFnS	podpořit
dotací	dotace	k1gFnPc2	dotace
vznik	vznik	k1gInSc4	vznik
této	tento	k3xDgFnSc2	tento
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
městem	město	k1gNnSc7	město
Moravské	moravský	k2eAgFnSc2d1	Moravská
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
však	však	k9	však
narazila	narazit	k5eAaPmAgFnS	narazit
hned	hned	k6eAd1	hned
po	po	k7c6	po
prvotním	prvotní	k2eAgNnSc6d1	prvotní
schválení	schválení	k1gNnSc6	schválení
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vykoupit	vykoupit	k5eAaPmF	vykoupit
několik	několik	k4yIc1	několik
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
budoucím	budoucí	k2eAgNnSc6d1	budoucí
území	území	k1gNnSc6	území
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
město	město	k1gNnSc1	město
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
pokračovat	pokračovat	k5eAaImF	pokračovat
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
by	by	kYmCp3nS	by
tak	tak	k9	tak
muselo	muset	k5eAaImAgNnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
dotace	dotace	k1gFnPc4	dotace
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
však	však	k9	však
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
menší	malý	k2eAgFnSc2d2	menší
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
cca	cca	kA	cca
10	[number]	k4	10
hektarů	hektar	k1gInPc2	hektar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
financování	financování	k1gNnSc1	financování
by	by	kYmCp3nP	by
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
10	[number]	k4	10
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
to	ten	k3xDgNnSc1	ten
schválilo	schválit	k5eAaPmAgNnS	schválit
město	město	k1gNnSc1	město
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Výkup	výkup	k1gInSc1	výkup
pozemků	pozemek	k1gInPc2	pozemek
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
začít	začít	k5eAaPmF	začít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Tyršova	Tyršův	k2eAgFnSc1d1	Tyršova
365	[number]	k4	365
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
praktická	praktický	k2eAgFnSc1d1	praktická
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
BUDÍK	budík	k1gInSc1	budík
</s>
</p>
<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Husova	Husův	k2eAgFnSc1d1	Husova
</s>
</p>
<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Šafaříkova	Šafaříkův	k2eAgFnSc1d1	Šafaříkova
</s>
</p>
<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Fišerova	Fišerův	k2eAgFnSc1d1	Fišerova
</s>
</p>
<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
JABULA	JABULA	kA	JABULA
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
2005	[number]	k4	2005
-	-	kIx~	-
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
postaven	postaven	k2eAgInSc1d1	postaven
nákladem	náklad	k1gInSc7	náklad
Čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
obce	obec	k1gFnSc2	obec
legionářské	legionářský	k2eAgFnSc2d1	legionářská
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
stával	stávat	k5eAaImAgInS	stávat
ve	v	k7c6	v
Wilsonově	Wilsonův	k2eAgFnSc6d1	Wilsonova
sadě	sada	k1gFnSc6	sada
před	před	k7c7	před
gymnáziem	gymnázium	k1gNnSc7	gymnázium
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
akad	akada	k1gFnPc2	akada
<g/>
.	.	kIx.	.
sochaře	sochař	k1gMnSc2	sochař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Břízy	Bříza	k1gMnSc2	Bříza
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
války	válka	k1gFnSc2	válka
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
Wilsonův	Wilsonův	k2eAgInSc1d1	Wilsonův
sad	sad	k1gInSc1	sad
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
na	na	k7c4	na
Park	park	k1gInSc4	park
plk.	plk.	kA	plk.
Ing.	ing.	kA	ing.
Miloslava	Miloslav	k1gMnSc2	Miloslav
Kršky	Krška	k1gMnSc2	Krška
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
románskou	románský	k2eAgFnSc7d1	románská
rotundou	rotunda	k1gFnSc7	rotunda
</s>
</p>
<p>
<s>
Masné	masný	k2eAgInPc1d1	masný
krámy	krám	k1gInPc1	krám
</s>
</p>
<p>
<s>
Karner	Karner	k1gInSc1	Karner
(	(	kIx(	(
<g/>
rotunda	rotunda	k1gFnSc1	rotunda
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fara	fara	k1gFnSc1	fara
</s>
</p>
<p>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
spořitelna	spořitelna	k1gFnSc1	spořitelna
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
</s>
</p>
<p>
<s>
Sochy	socha	k1gFnPc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
blahoslavného	blahoslavný	k2eAgMnSc2d1	blahoslavný
Jana	Jan	k1gMnSc2	Jan
Sarkandra	Sarkandr	k1gMnSc2	Sarkandr
na	na	k7c6	na
mostě	most	k1gInSc6	most
přes	přes	k7c4	přes
Rokytku	Rokytka	k1gFnSc4	Rokytka
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgInSc1d1	pamětní
kámen	kámen	k1gInSc1	kámen
v	v	k7c6	v
Gymnazijním	gymnazijní	k2eAgInSc6d1	gymnazijní
parku	park	k1gInSc6	park
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
manželů	manžel	k1gMnPc2	manžel
Kvízových	kvízový	k2eAgInPc2d1	kvízový
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
Josefa	Josef	k1gMnSc2	Josef
Salače	Salač	k1gMnSc2	Salač
</s>
</p>
<p>
<s>
Radnice	radnice	k1gFnSc1	radnice
čp.	čp.	k?	čp.
31	[number]	k4	31
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
32	[number]	k4	32
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
56	[number]	k4	56
</s>
</p>
<p>
<s>
Blažkův	Blažkův	k2eAgInSc1d1	Blažkův
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
60	[number]	k4	60
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
64	[number]	k4	64
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
65	[number]	k4	65
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ralph	Ralph	k1gInSc1	Ralph
Benatzky	Benatzka	k1gFnSc2	Benatzka
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Doležal	Doležal	k1gMnSc1	Doležal
(	(	kIx(	(
<g/>
Dolezal	Dolezal	k1gMnSc1	Dolezal
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geodet	geodet	k1gMnSc1	geodet
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
fotogrammetrie	fotogrammetrie	k1gFnSc2	fotogrammetrie
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Fišer	Fišer	k1gMnSc1	Fišer
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Förster	Förster	k1gMnSc1	Förster
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Funtíček	Funtíček	k1gMnSc1	Funtíček
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hendrich	Hendrich	k1gMnSc1	Hendrich
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komeniolog	komeniolog	k1gMnSc1	komeniolog
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Herle	Herle	k1gFnSc2	Herle
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hubáček	Hubáček	k1gMnSc1	Hubáček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
rektor	rektor	k1gMnSc1	rektor
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chalabala	Chalabal	k1gMnSc2	Chalabal
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jaroš	Jaroš	k1gMnSc1	Jaroš
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Jech	Jech	k1gMnSc1	Jech
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Jenerál	Jenerál	k?	Jenerál
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kalenský	Kalenský	k2eAgMnSc1d1	Kalenský
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
</s>
</p>
<p>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Palliardi	Palliard	k1gMnPc1	Palliard
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
</s>
</p>
<p>
<s>
Jára	Jára	k1gMnSc1	Jára
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Purcner	Purcner	k1gMnSc1	Purcner
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
statkář	statkář	k1gMnSc1	statkář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
</s>
</p>
<p>
<s>
Michaela	Michaela	k1gFnSc1	Michaela
Salačová	Salačová	k1gFnSc1	Salačová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
DJka	DJka	k1gFnSc1	DJka
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Špatinka	Špatinka	k1gFnSc1	Špatinka
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
cellista	cellista	k1gMnSc1	cellista
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
a	a	k8xC	a
muzeolog	muzeolog	k1gMnSc1	muzeolog
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Urbanec	Urbanec	k1gMnSc1	Urbanec
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bankéř	bankéř	k1gMnSc1	bankéř
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Živnostenské	živnostenský	k2eAgFnSc2d1	Živnostenská
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Urbánek	Urbánek	k1gMnSc1	Urbánek
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Augustin	Augustin	k1gMnSc1	Augustin
Urbánek	Urbánek	k1gMnSc1	Urbánek
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřizovatel	zřizovatel	k1gMnSc1	zřizovatel
prvního	první	k4xOgNnSc2	první
českého	český	k2eAgNnSc2d1	české
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
hudebnin	hudebnina	k1gFnPc2	hudebnina
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
dědeček	dědeček	k1gMnSc1	dědeček
Jana	Jan	k1gMnSc2	Jan
Hanuše	Hanuš	k1gMnSc2	Hanuš
</s>
</p>
<p>
<s>
Velebín	Velebín	k1gMnSc1	Velebín
Urbánek	Urbánek	k1gMnSc1	Urbánek
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Františka	František	k1gMnSc2	František
Augustina	Augustin	k1gMnSc2	Augustin
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Venhoda	Venhoda	k1gMnSc1	Venhoda
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Pražských	pražský	k2eAgInPc2d1	pražský
madrigalistů	madrigalista	k1gMnPc2	madrigalista
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Kautzen	Kautzen	k2eAgMnSc1d1	Kautzen
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
Pulkau	Pulkau	k6eAd1	Pulkau
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Moravskobudějovická	moravskobudějovický	k2eAgFnSc1d1	moravskobudějovická
kotlina	kotlina	k1gFnSc1	kotlina
</s>
</p>
<p>
<s>
Jaroměřická	jaroměřický	k2eAgFnSc1d1	Jaroměřická
kotlina	kotlina	k1gFnSc1	kotlina
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Moravské	moravský	k2eAgFnSc2d1	Moravská
Budějovice	Budějovice	k1gInPc1	Budějovice
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
</s>
</p>
<p>
<s>
===	===	k?	===
Publikace	publikace	k1gFnSc1	publikace
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Beroun	Beroun	k1gInSc1	Beroun
<g/>
:	:	kIx,	:
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lojda	Lojda	k1gMnSc1	Lojda
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Moravských	moravský	k2eAgInPc6d1	moravský
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc4	obec
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
Moravskobudějovicko	Moravskobudějovicko	k6eAd1	Moravskobudějovicko
–	–	k?	–
informační	informační	k2eAgInSc4d1	informační
portál	portál	k1gInSc4	portál
</s>
</p>
