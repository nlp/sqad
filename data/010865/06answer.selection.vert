<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
je	být	k5eAaImIp3nS	být
nepředvídaná	předvídaný	k2eNgFnSc1d1	nepředvídaná
kolize	kolize	k1gFnSc1	kolize
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
hmotné	hmotný	k2eAgFnSc3d1	hmotná
škodě	škoda	k1gFnSc3	škoda
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
