<p>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
je	být	k5eAaImIp3nS	být
nepředvídaná	předvídaný	k2eNgFnSc1d1	nepředvídaná
kolize	kolize	k1gFnSc1	kolize
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
hmotné	hmotný	k2eAgFnSc3d1	hmotná
škodě	škoda	k1gFnSc3	škoda
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
termínem	termín	k1gInSc7	termín
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
označuje	označovat	k5eAaImIp3nS	označovat
nehoda	nehoda	k1gFnSc1	nehoda
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehodami	nehoda	k1gFnPc7	nehoda
jsou	být	k5eAaImIp3nP	být
též	též	k6eAd1	též
obdobné	obdobný	k2eAgFnPc1d1	obdobná
události	událost	k1gFnPc1	událost
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc6d1	vodní
nebo	nebo	k8xC	nebo
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
snížit	snížit	k5eAaPmF	snížit
nebo	nebo	k8xC	nebo
vyloučit	vyloučit	k5eAaPmF	vyloučit
riziko	riziko	k1gNnSc4	riziko
dalších	další	k2eAgFnPc2d1	další
škod	škoda	k1gFnPc2	škoda
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
výbuch	výbuch	k1gInSc1	výbuch
<g/>
)	)	kIx)	)
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
první	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
zraněným	zraněný	k1gMnPc3	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
závažnějším	závažný	k2eAgFnPc3d2	závažnější
nehodám	nehoda	k1gFnPc3	nehoda
bývá	bývat	k5eAaImIp3nS	bývat
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
volána	volán	k2eAgFnSc1d1	volána
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
hasiči	hasič	k1gMnPc1	hasič
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
záchranné	záchranný	k2eAgInPc1d1	záchranný
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInPc4d1	právní
řády	řád	k1gInPc4	řád
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
za	za	k7c2	za
jakých	jaký	k3yQgFnPc2	jaký
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
volat	volat	k5eAaImF	volat
i	i	k9	i
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
jaký	jaký	k3yIgInSc4	jaký
postup	postup	k1gInSc4	postup
má	mít	k5eAaImIp3nS	mít
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
příčin	příčina	k1gFnPc2	příčina
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
k	k	k7c3	k
nehodám	nehoda	k1gFnPc3	nehoda
voláni	volat	k5eAaImNgMnP	volat
i	i	k9	i
zástupci	zástupce	k1gMnPc1	zástupce
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Silniční	silniční	k2eAgFnPc1d1	silniční
nehody	nehoda	k1gFnPc1	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
na	na	k7c4	na
pozemní	pozemní	k2eAgFnSc4d1	pozemní
komunikaci	komunikace	k1gFnSc4	komunikace
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
nebo	nebo	k8xC	nebo
zranění	zranění	k1gNnSc3	zranění
osoby	osoba	k1gFnSc2	osoba
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
škodě	škoda	k1gFnSc3	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
vozidla	vozidlo	k1gNnSc2	vozidlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
osoby	osoba	k1gFnPc1	osoba
podílející	podílející	k2eAgFnPc1d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
záchranných	záchranný	k2eAgFnPc6d1	záchranná
pracích	práce	k1gFnPc6	práce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nehody	nehoda	k1gFnSc2	nehoda
mají	mít	k5eAaImIp3nP	mít
zabránit	zabránit	k5eAaPmF	zabránit
vzniku	vznik	k1gInSc3	vznik
dalších	další	k2eAgFnPc2d1	další
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
zajistit	zajistit	k5eAaPmF	zajistit
první	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
splnit	splnit	k5eAaPmF	splnit
povinnosti	povinnost	k1gFnPc4	povinnost
směřující	směřující	k2eAgFnSc4d1	směřující
k	k	k7c3	k
řádnému	řádný	k2eAgNnSc3d1	řádné
vyšetření	vyšetření	k1gNnSc3	vyšetření
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
případů	případ	k1gInPc2	případ
jsou	být	k5eAaImIp3nP	být
účastníci	účastník	k1gMnPc1	účastník
povinni	povinen	k2eAgMnPc1d1	povinen
přivolat	přivolat	k5eAaPmF	přivolat
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
policii	policie	k1gFnSc3	policie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mimořádné	mimořádný	k2eAgFnPc1d1	mimořádná
události	událost	k1gFnPc1	událost
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
dopravě	doprava	k1gFnSc6	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Drážními	drážní	k2eAgFnPc7d1	drážní
nehodami	nehoda	k1gFnPc7	nehoda
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
železniční	železniční	k2eAgFnPc1d1	železniční
nehody	nehoda	k1gFnPc1	nehoda
a	a	k8xC	a
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
nehody	nehoda	k1gFnPc1	nehoda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nehody	nehoda	k1gFnPc1	nehoda
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
kolejových	kolejový	k2eAgFnPc6d1	kolejová
drahách	draha	k1gFnPc6	draha
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
právního	právní	k2eAgInSc2d1	právní
režimu	režim	k1gInSc2	režim
i	i	k8xC	i
visuté	visutý	k2eAgFnSc2d1	visutá
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
také	také	k9	také
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
nehody	nehoda	k1gFnPc1	nehoda
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
zároveň	zároveň	k6eAd1	zároveň
vždy	vždy	k6eAd1	vždy
i	i	k8xC	i
silničními	silniční	k2eAgFnPc7d1	silniční
nehodami	nehoda	k1gFnPc7	nehoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nehody	nehoda	k1gFnPc1	nehoda
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
podle	podle	k7c2	podle
závažnosti	závažnost	k1gFnSc2	závažnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
za	za	k7c4	za
závažnou	závažný	k2eAgFnSc4d1	závažná
nehodu	nehoda	k1gFnSc4	nehoda
považuje	považovat	k5eAaImIp3nS	považovat
srážka	srážka	k1gFnSc1	srážka
nebo	nebo	k8xC	nebo
vykolejení	vykolejení	k1gNnSc1	vykolejení
drážních	drážní	k2eAgNnPc2d1	drážní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
souvislosti	souvislost	k1gFnPc4	souvislost
s	s	k7c7	s
provozováním	provozování	k1gNnSc7	provozování
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
smrti	smrt	k1gFnSc2	smrt
či	či	k8xC	či
újmy	újma	k1gFnSc2	újma
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
nejméně	málo	k6eAd3	málo
5	[number]	k4	5
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
škody	škoda	k1gFnSc2	škoda
velkého	velký	k2eAgInSc2d1	velký
rozsahu	rozsah	k1gInSc2	rozsah
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgFnPc4d1	ostatní
nehody	nehoda	k1gFnPc4	nehoda
patří	patřit	k5eAaImIp3nS	patřit
ty	ten	k3xDgFnPc4	ten
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
následkem	následek	k1gInSc7	následek
někdo	někdo	k3yInSc1	někdo
zemře	zemřít	k5eAaPmIp3nS	zemřít
nebo	nebo	k8xC	nebo
dojde	dojít	k5eAaPmIp3nS	dojít
újmy	újma	k1gFnSc2	újma
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
nebo	nebo	k8xC	nebo
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
značná	značný	k2eAgFnSc1d1	značná
škoda	škoda	k1gFnSc1	škoda
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
nehod	nehoda	k1gFnPc2	nehoda
evidují	evidovat	k5eAaImIp3nP	evidovat
a	a	k8xC	a
vyšetřují	vyšetřovat	k5eAaImIp3nP	vyšetřovat
také	také	k9	také
jiné	jiný	k2eAgFnPc1d1	jiná
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sice	sice	k8xC	sice
nezpůsobí	způsobit	k5eNaPmIp3nP	způsobit
újmu	újma	k1gFnSc4	újma
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
ani	ani	k8xC	ani
větší	veliký	k2eAgFnSc4d2	veliký
škodu	škoda	k1gFnSc4	škoda
<g/>
,	,	kIx,	,
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
nebo	nebo	k8xC	nebo
narušuje	narušovat	k5eAaImIp3nS	narušovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
pravidelnost	pravidelnost	k1gFnSc4	pravidelnost
a	a	k8xC	a
plynulost	plynulost	k1gFnSc4	plynulost
provozování	provozování	k1gNnSc2	provozování
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
funkci	funkce	k1gFnSc4	funkce
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
nebo	nebo	k8xC	nebo
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
události	událost	k1gFnPc1	událost
se	se	k3xPyFc4	se
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
právu	právo	k1gNnSc6	právo
nazývají	nazývat	k5eAaImIp3nP	nazývat
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Nehody	nehoda	k1gFnPc1	nehoda
i	i	k8xC	i
ohrožení	ohrožení	k1gNnSc1	ohrožení
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnut	k2eAgInPc1d1	zahrnut
pod	pod	k7c4	pod
společný	společný	k2eAgInSc4d1	společný
pojem	pojem	k1gInSc4	pojem
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
událost	událost	k1gFnSc1	událost
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drážní	drážní	k2eAgFnPc1d1	drážní
nehody	nehoda	k1gFnPc1	nehoda
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
události	událost	k1gFnPc4	událost
(	(	kIx(	(
<g/>
nehody	nehoda	k1gFnPc4	nehoda
i	i	k8xC	i
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
)	)	kIx)	)
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
speciální	speciální	k2eAgFnSc1d1	speciální
odborná	odborný	k2eAgFnSc1d1	odborná
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
Drážní	drážní	k2eAgFnSc1d1	drážní
inspekce	inspekce	k1gFnSc1	inspekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nehody	nehoda	k1gFnPc1	nehoda
na	na	k7c6	na
vodní	vodní	k2eAgFnSc6d1	vodní
cestě	cesta	k1gFnSc6	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Příčinou	příčina	k1gFnSc7	příčina
nehod	nehoda	k1gFnPc2	nehoda
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
dopravě	doprava	k1gFnSc6	doprava
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
například	například	k6eAd1	například
srážka	srážka	k1gFnSc1	srážka
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
náraz	náraz	k1gInSc4	náraz
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
nebo	nebo	k8xC	nebo
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
technická	technický	k2eAgFnSc1d1	technická
závada	závada	k1gFnSc1	závada
nebo	nebo	k8xC	nebo
špatná	špatný	k2eAgFnSc1d1	špatná
konstrukce	konstrukce	k1gFnSc1	konstrukce
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejtragičtější	tragický	k2eAgInPc1d3	nejtragičtější
následky	následek	k1gInPc1	následek
při	při	k7c6	při
vodní	vodní	k2eAgFnSc6d1	vodní
dopravě	doprava	k1gFnSc6	doprava
mívá	mívat	k5eAaImIp3nS	mívat
potopení	potopení	k1gNnSc4	potopení
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
nehody	nehoda	k1gFnPc1	nehoda
si	se	k3xPyFc3	se
často	často	k6eAd1	často
vynutí	vynutit	k5eAaPmIp3nP	vynutit
náročnou	náročný	k2eAgFnSc4d1	náročná
opravu	oprava	k1gFnSc4	oprava
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Letecké	letecký	k2eAgFnPc1d1	letecká
nehody	nehoda	k1gFnPc1	nehoda
a	a	k8xC	a
incidenty	incident	k1gInPc1	incident
==	==	k?	==
</s>
</p>
<p>
<s>
Nejtypičtější	typický	k2eAgFnSc7d3	nejtypičtější
a	a	k8xC	a
nejtragičtější	tragický	k2eAgFnSc7d3	nejtragičtější
leteckou	letecký	k2eAgFnSc7d1	letecká
nehodou	nehoda	k1gFnSc7	nehoda
je	být	k5eAaImIp3nS	být
pád	pád	k1gInSc1	pád
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
závažnosti	závažnost	k1gFnSc3	závažnost
nehod	nehoda	k1gFnPc2	nehoda
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
se	se	k3xPyFc4	se
sledují	sledovat	k5eAaImIp3nP	sledovat
a	a	k8xC	a
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
také	také	k9	také
méně	málo	k6eAd2	málo
závažné	závažný	k2eAgFnPc1d1	závažná
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
taková	takový	k3xDgFnSc1	takový
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
incident	incident	k1gInSc1	incident
<g/>
,	,	kIx,	,
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
letecké	letecký	k2eAgFnSc3d1	letecká
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vážný	vážný	k2eAgInSc1d1	vážný
incident	incident	k1gInSc1	incident
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřováním	vyšetřování	k1gNnSc7	vyšetřování
příčin	příčina	k1gFnPc2	příčina
nehod	nehoda	k1gFnPc2	nehoda
a	a	k8xC	a
incidentů	incident	k1gInPc2	incident
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
odborný	odborný	k2eAgInSc4d1	odborný
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BREČKA	BREČKA	kA	BREČKA
<g/>
,	,	kIx,	,
Tibor	Tibor	k1gMnSc1	Tibor
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Mgr.	Mgr.	kA	Mgr.
Vymáhání	vymáhání	k1gNnSc4	vymáhání
odškodného	odškodné	k1gNnSc2	odškodné
pro	pro	k7c4	pro
účastníky	účastník	k1gMnPc4	účastník
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
archiv	archiv	k1gInSc1	archiv
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Rescue	Rescuat	k5eAaPmIp3nS	Rescuat
Report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
nehodovost	nehodovost	k1gFnSc1	nehodovost
</s>
</p>
<p>
<s>
Poškozený	poškozený	k2eAgMnSc1d1	poškozený
(	(	kIx(	(
<g/>
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zajeté	zajetý	k2eAgNnSc1d1	zajeté
zvíře	zvíře	k1gNnSc1	zvíře
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
policie	policie	k1gFnSc1	policie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Statistika	statistika	k1gFnSc1	statistika
silničních	silniční	k2eAgFnPc2d1	silniční
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Kuriózní	kuriózní	k2eAgFnPc1d1	kuriózní
dopravní	dopravní	k2eAgFnPc1d1	dopravní
nehody	nehoda	k1gFnPc1	nehoda
</s>
</p>
