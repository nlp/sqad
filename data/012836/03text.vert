<p>
<s>
Enologie	enologie	k1gFnSc1	enologie
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
pojednávající	pojednávající	k2eAgInSc4d1	pojednávající
o	o	k7c6	o
víně	víno	k1gNnSc6	víno
a	a	k8xC	a
vinařství	vinařství	k1gNnSc6	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
oinos	oinos	k1gInSc1	oinos
=	=	kIx~	=
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
logos	logos	k1gInSc1	logos
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
enolog	enolog	k1gInSc1	enolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
enologie	enologie	k1gFnSc1	enologie
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
např.	např.	kA	např.
na	na	k7c6	na
Mendelově	Mendelův	k2eAgFnSc6d1	Mendelova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
enologie	enologie	k1gFnSc2	enologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
enologie	enologie	k1gFnSc2	enologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Enolog	Enolog	k1gInSc1	Enolog
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Znalec	znalec	k1gMnSc1	znalec
vín	víno	k1gNnPc2	víno
–	–	k?	–
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
vinařství	vinařství	k1gNnSc2	vinařství
a	a	k8xC	a
vinohradnictví	vinohradnictví	k1gNnSc2	vinohradnictví
</s>
</p>
