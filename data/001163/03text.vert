<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
nebo	nebo	k8xC	nebo
též	též	k9	též
Amsterodam	Amsterodam	k1gInSc1	Amsterodam
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
<s>
Nesídlí	sídlet	k5eNaImIp3nS	sídlet
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
však	však	k9	však
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
ani	ani	k8xC	ani
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
instituce	instituce	k1gFnPc1	instituce
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Den	den	k1gInSc4	den
Haag	Haag	k1gInSc1	Haag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
nizozemským	nizozemský	k2eAgNnSc7d1	Nizozemské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
finančním	finanční	k2eAgNnSc7d1	finanční
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
761	[number]	k4	761
395	[number]	k4	395
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
177	[number]	k4	177
různým	různý	k2eAgFnPc3d1	různá
národnostem	národnost	k1gFnPc3	národnost
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
konurbace	konurbace	k1gFnSc2	konurbace
Randstad	Randstad	k1gInSc1	Randstad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
či	či	k8xC	či
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
památky	památka	k1gFnPc1	památka
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
amsterdamské	amsterdamský	k2eAgInPc4d1	amsterdamský
kanály	kanál	k1gInPc4	kanál
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
obrannou	obranný	k2eAgFnSc4d1	obranná
linii	linie	k1gFnSc4	linie
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
okolo	okolo	k7c2	okolo
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
od	od	k7c2	od
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
oblast	oblast	k1gFnSc1	oblast
zvána	zván	k2eAgFnSc1d1	zvána
Aemestelle	Aemestelle	k1gFnSc1	Aemestelle
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
bažin	bažina	k1gFnPc2	bažina
byly	být	k5eAaImAgInP	být
kopány	kopán	k2eAgInPc1d1	kopán
odvodňovací	odvodňovací	k2eAgInPc1d1	odvodňovací
kanály	kanál	k1gInPc1	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
hladina	hladina	k1gFnSc1	hladina
vody	voda	k1gFnSc2	voda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kanálech	kanál	k1gInPc6	kanál
udržována	udržován	k2eAgFnSc1d1	udržována
díky	díky	k7c3	díky
přehradám	přehrada	k1gFnPc3	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přehrada	přehrada	k1gFnSc1	přehrada
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Amstel	Amstela	k1gFnPc2	Amstela
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jméno	jméno	k1gNnSc1	jméno
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1275	[number]	k4	1275
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
panovník	panovník	k1gMnSc1	panovník
Floris	Floris	k1gFnSc3	Floris
V.	V.	kA	V.
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
jeho	jeho	k3xOp3gMnPc4	jeho
obyvatele	obyvatel	k1gMnPc4	obyvatel
od	od	k7c2	od
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1306	[number]	k4	1306
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
město	město	k1gNnSc1	město
vzkvétá	vzkvétat	k5eAaImIp3nS	vzkvétat
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
hanzovními	hanzovní	k2eAgNnPc7d1	hanzovní
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Hamburkem	Hamburk	k1gInSc7	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
obchodně	obchodně	k6eAd1	obchodně
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
holandským	holandský	k2eAgNnSc7d1	holandské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
bažinách	bažina	k1gFnPc6	bažina
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
se	se	k3xPyFc4	se
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
kopat	kopat	k5eAaImF	kopat
další	další	k2eAgInPc4d1	další
kanály	kanál	k1gInPc4	kanál
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
zakládala	zakládat	k5eAaImAgFnS	zakládat
na	na	k7c6	na
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
kůlech	kůl	k1gInPc6	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vzpoura	vzpoura	k1gFnSc1	vzpoura
proti	proti	k7c3	proti
Filipovi	Filip	k1gMnSc3	Filip
Španělskému	španělský	k2eAgMnSc3d1	španělský
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
Osmiletou	osmiletý	k2eAgFnSc4d1	osmiletá
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Republiky	republika	k1gFnSc2	republika
spojených	spojený	k2eAgFnPc2d1	spojená
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
svoji	svůj	k3xOyFgFnSc4	svůj
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
tolerancí	tolerance	k1gFnSc7	tolerance
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
bohatí	bohatý	k2eAgMnPc1d1	bohatý
obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
Antwerp	Antwerp	k1gMnSc1	Antwerp
a	a	k8xC	a
Hugenoti	hugenot	k1gMnPc1	hugenot
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
migrace	migrace	k1gFnSc1	migrace
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Flander	Flandry	k1gInPc2	Flandry
pak	pak	k6eAd1	pak
dala	dát	k5eAaPmAgFnS	dát
městu	město	k1gNnSc3	město
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInSc1	jejich
brabantský	brabantský	k2eAgInSc1d1	brabantský
dialekt	dialekt	k1gInSc1	dialekt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
psané	psaný	k2eAgFnSc2d1	psaná
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
<g/>
)	)	kIx)	)
a	a	k8xC	a
udělala	udělat	k5eAaPmAgFnS	udělat
z	z	k7c2	z
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc4	takový
růst	růst	k1gInSc4	růst
obyvatel	obyvatel	k1gMnPc2	obyvatel
podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
další	další	k2eAgNnSc4d1	další
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Neapol	Neapol	k1gFnSc1	Neapol
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnácté	sedmnáctý	k4xOgNnSc1	sedmnáctý
století	století	k1gNnSc1	století
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
"	"	kIx"	"
<g/>
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
období	období	k1gNnSc4	období
<g/>
"	"	kIx"	"
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
nejbohatším	bohatý	k2eAgNnSc7d3	nejbohatší
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
vyplouvaly	vyplouvat	k5eAaImAgFnP	vyplouvat
lodě	loď	k1gFnPc1	loď
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Indonésie	Indonésie	k1gFnSc2	Indonésie
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
představovalo	představovat	k5eAaImAgNnS	představovat
základnu	základna	k1gFnSc4	základna
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdamští	amsterdamský	k2eAgMnPc1d1	amsterdamský
obchodníci	obchodník	k1gMnPc1	obchodník
měli	mít	k5eAaImAgMnP	mít
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
Nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
východoindické	východoindický	k2eAgFnSc6d1	Východoindická
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
v	v	k7c6	v
Nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
západoindické	západoindický	k2eAgFnSc6d1	Západoindická
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
společnostem	společnost	k1gFnPc3	společnost
byly	být	k5eAaImAgFnP	být
navázány	navázán	k2eAgInPc4d1	navázán
zámořské	zámořský	k2eAgInPc4d1	zámořský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
tvorby	tvorba	k1gFnSc2	tvorba
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
byl	být	k5eAaImAgInS	být
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
místem	místo	k1gNnSc7	místo
námořní	námořní	k2eAgFnSc2d1	námořní
přepravy	přeprava	k1gFnSc2	přeprava
zboží	zboží	k1gNnSc2	zboží
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
předním	přední	k2eAgMnSc7d1	přední
finančním	finanční	k2eAgMnSc7d1	finanční
centrem	centr	k1gMnSc7	centr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdamská	amsterdamský	k2eAgFnSc1d1	Amsterdamská
burza	burza	k1gFnSc1	burza
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Osmnácté	osmnáctý	k4xOgNnSc1	osmnáctý
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nesou	nést	k5eAaImIp3nP	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
poklesu	pokles	k1gInSc2	pokles
prosperity	prosperita	k1gFnSc2	prosperita
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc4	válka
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
vybraly	vybrat	k5eAaPmAgFnP	vybrat
i	i	k9	i
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
města	město	k1gNnSc2	město
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
nejnižší	nízký	k2eAgInSc4d3	nejnižší
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
obracet	obracet	k5eAaImF	obracet
k	k	k7c3	k
lepšímu	dobrý	k2eAgMnSc3d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
město	město	k1gNnSc4	město
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Amsterdamsko-rýnský	amsterdamskoýnský	k2eAgInSc1d1	amsterdamsko-rýnský
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přímo	přímo	k6eAd1	přímo
spojil	spojit	k5eAaPmAgMnS	spojit
město	město	k1gNnSc4	město
s	s	k7c7	s
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
Severomořský	severomořský	k2eAgInSc4d1	severomořský
kanál	kanál	k1gInSc4	kanál
spojil	spojit	k5eAaPmAgMnS	spojit
přístav	přístav	k1gInSc4	přístav
se	s	k7c7	s
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
kanály	kanál	k1gInPc1	kanál
výrazně	výrazně	k6eAd1	výrazně
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
podnět	podnět	k1gInSc1	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1850	[number]	k4	1850
a	a	k8xC	a
1900	[number]	k4	1900
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
asi	asi	k9	asi
na	na	k7c4	na
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
nazýván	nazývat	k5eAaImNgInS	nazývat
druhou	druhý	k4xOgFnSc7	druhý
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgNnP	být
postavena	postaven	k2eAgNnPc1d1	postaveno
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
a	a	k8xC	a
koncertní	koncertní	k2eAgFnSc1d1	koncertní
hala	hala	k1gFnSc1	hala
(	(	kIx(	(
<g/>
Concertgebouw	Concertgebouw	k1gFnSc1	Concertgebouw
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
neutrální	neutrální	k2eAgNnSc1d1	neutrální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
poznal	poznat	k5eAaPmAgInS	poznat
její	její	k3xOp3gInPc4	její
následky	následek	k1gInPc4	následek
v	v	k7c6	v
nedostatku	nedostatek	k1gInSc6	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okupovala	okupovat	k5eAaBmAgFnS	okupovat
město	město	k1gNnSc4	město
německá	německý	k2eAgNnPc4d1	německé
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
bylo	být	k5eAaImAgNnS	být
deportováno	deportovat	k5eAaBmNgNnS	deportovat
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Franková	Franková	k1gFnSc1	Franková
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
byla	být	k5eAaImAgFnS	být
vyhlazena	vyhladit	k5eAaPmNgFnS	vyhladit
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
ondřejských	ondřejský	k2eAgInPc2d1	ondřejský
křížů	kříž	k1gInPc2	kříž
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
svisle	svisle	k6eAd1	svisle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
jsou	být	k5eAaImIp3nP	být
otočeny	otočen	k2eAgFnPc1d1	otočena
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
kříže	kříž	k1gInPc1	kříž
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
tři	tři	k4xCgNnPc1	tři
hlavní	hlavní	k2eAgNnPc1d1	hlavní
nebezpečí	nebezpečí	k1gNnPc1	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sužovaly	sužovat	k5eAaImAgFnP	sužovat
město	město	k1gNnSc4	město
<g/>
:	:	kIx,	:
povodně	povodeň	k1gFnPc1	povodeň
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
a	a	k8xC	a
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
mottem	motto	k1gNnSc7	motto
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Heldhaftig	Heldhaftig	k1gInSc1	Heldhaftig
<g/>
,	,	kIx,	,
Vastberaden	Vastberaden	k2eAgInSc1d1	Vastberaden
<g/>
,	,	kIx,	,
Barmhartig	Barmhartig	k1gInSc1	Barmhartig
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stateční	statečný	k2eAgMnPc1d1	statečný
<g/>
,	,	kIx,	,
odhodlaní	odhodlaný	k2eAgMnPc1d1	odhodlaný
<g/>
,	,	kIx,	,
milosrdní	milosrdný	k2eAgMnPc1d1	milosrdný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
motto	motto	k1gNnSc1	motto
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
i	i	k9	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
města	město	k1gNnSc2	město
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
královnou	královna	k1gFnSc7	královna
Vilemínou	Vilemína	k1gFnSc7	Vilemína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
jako	jako	k9	jako
reflexe	reflexe	k1gFnPc1	reflexe
na	na	k7c4	na
hrdinství	hrdinství	k1gNnSc4	hrdinství
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
žijí	žít	k5eAaImIp3nP	žít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
hledali	hledat	k5eAaImAgMnP	hledat
svobodu	svoboda	k1gFnSc4	svoboda
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
francouzští	francouzštit	k5eAaImIp3nP	francouzštit
Hugenoti	hugenot	k1gMnPc1	hugenot
<g/>
,	,	kIx,	,
protestantští	protestantský	k2eAgMnPc1d1	protestantský
Vlámové	Vlám	k1gMnPc1	Vlám
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Frísové	Frís	k1gMnPc1	Frís
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Skandinávci	Skandinávec	k1gMnPc1	Skandinávec
do	do	k7c2	do
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
přicházeli	přicházet	k5eAaImAgMnP	přicházet
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
byla	být	k5eAaImAgFnS	být
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
skupina	skupina	k1gFnSc1	skupina
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
Čína	Čína	k1gFnSc1	Čína
stala	stát	k5eAaPmAgFnS	stát
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
Číňanů	Číňan	k1gMnPc2	Číňan
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
přicházeli	přicházet	k5eAaImAgMnP	přicházet
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Surinamu	Surinam	k1gInSc2	Surinam
a	a	k8xC	a
Nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
patřila	patřit	k5eAaImAgNnP	patřit
do	do	k7c2	do
Nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
království	království	k1gNnSc2	království
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
zákonem	zákon	k1gInSc7	zákon
oprávněni	oprávnit	k5eAaPmNgMnP	oprávnit
vstupovat	vstupovat	k5eAaImF	vstupovat
na	na	k7c6	na
území	území	k1gNnSc6	území
evropského	evropský	k2eAgNnSc2d1	Evropské
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
mnoho	mnoho	k4c4	mnoho
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
velmi	velmi	k6eAd1	velmi
příznivě	příznivě	k6eAd1	příznivě
a	a	k8xC	a
chyběli	chybět	k5eAaImAgMnP	chybět
pracovníci	pracovník	k1gMnPc1	pracovník
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
vyžadujících	vyžadující	k2eAgFnPc2d1	vyžadující
nízkou	nízký	k2eAgFnSc4d1	nízká
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
zemí	zem	k1gFnPc2	zem
přicházeli	přicházet	k5eAaImAgMnP	přicházet
hostující	hostující	k2eAgMnPc1d1	hostující
dělníci	dělník	k1gMnPc1	dělník
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
gastarbeitři	gastarbeitr	k1gMnPc1	gastarbeitr
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mezivládních	mezivládní	k2eAgFnPc2d1	mezivládní
dohod	dohoda	k1gFnPc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgFnP	následovat
dohody	dohoda	k1gFnPc1	dohoda
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
,	,	kIx,	,
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc1d1	obdobná
dohody	dohoda	k1gFnPc1	dohoda
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
Marokem	Maroko	k1gNnSc7	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hostujícími	hostující	k2eAgMnPc7d1	hostující
dělníky	dělník	k1gMnPc7	dělník
přicházely	přicházet	k5eAaImAgFnP	přicházet
jejich	jejich	k3xOp3gFnPc1	jejich
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgInP	pokusit
imigraci	imigrace	k1gFnSc4	imigrace
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
imigrantů	imigrant	k1gMnPc2	imigrant
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
)	)	kIx)	)
přijímala	přijímat	k5eAaImAgFnS	přijímat
uprchlíky	uprchlík	k1gMnPc4	uprchlík
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
zemí	zem	k1gFnSc7	zem
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
nebo	nebo	k8xC	nebo
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
po	po	k7c6	po
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
uprchlíků	uprchlík	k1gMnPc2	uprchlík
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
populaci	populace	k1gFnSc3	populace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
stahovali	stahovat	k5eAaImAgMnP	stahovat
do	do	k7c2	do
měst	město	k1gNnPc2	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
Almere	Almer	k1gMnSc5	Almer
<g/>
,	,	kIx,	,
Amstelveen	Amstelvena	k1gFnPc2	Amstelvena
<g/>
,	,	kIx,	,
Haarlem	Haarlo	k1gNnSc7	Haarlo
<g/>
,	,	kIx,	,
Purmerend	Purmerend	k1gInSc1	Purmerend
a	a	k8xC	a
Zaandstad	Zaandstad	k1gInSc1	Zaandstad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mělo	mít	k5eAaImAgNnS	mít
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
původ	původ	k1gInSc1	původ
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgNnSc2d1	celé
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
9	[number]	k4	9
%	%	kIx~	%
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
žijí	žít	k5eAaImIp3nP	žít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
177	[number]	k4	177
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
ho	on	k3xPp3gInSc4	on
Antverpy	Antverpy	k1gFnPc1	Antverpy
se	s	k7c7	s
164	[number]	k4	164
národnostmi	národnost	k1gFnPc7	národnost
<g/>
,	,	kIx,	,
Wageningen	Wageningen	k1gInSc1	Wageningen
se	s	k7c7	s
152	[number]	k4	152
národnostmi	národnost	k1gFnPc7	národnost
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
se	s	k7c7	s
150	[number]	k4	150
národnostmi	národnost	k1gFnPc7	národnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
architektonickým	architektonický	k2eAgFnPc3d1	architektonická
památkám	památka	k1gFnPc3	památka
patří	patřit	k5eAaImIp3nS	patřit
dva	dva	k4xCgInPc4	dva
původem	původ	k1gMnSc7	původ
gotické	gotický	k2eAgInPc4d1	gotický
farní	farní	k2eAgInPc4d1	farní
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Starý	starý	k2eAgInSc1d1	starý
kostel	kostel	k1gInSc1	kostel
Oude	oud	k1gInSc5	oud
Kerk	Kerka	k1gFnPc2	Kerka
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
kostel	kostel	k1gInSc1	kostel
Nieuwe	Nieuwe	k1gFnSc4	Nieuwe
Kerk	Kerko	k1gNnPc2	Kerko
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
výstavní	výstavní	k2eAgFnPc4d1	výstavní
síně	síň	k1gFnPc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
mladší	mladý	k2eAgMnSc1d2	mladší
-	-	kIx~	-
až	až	k6eAd1	až
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století-	století-	k?	století-
jsou	být	k5eAaImIp3nP	být
jezuitský	jezuitský	k2eAgInSc4d1	jezuitský
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
katolický	katolický	k2eAgInSc1d1	katolický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
s	s	k7c7	s
novobarokní	novobarokní	k2eAgFnSc7d1	novobarokní
kupolí	kupole	k1gFnSc7	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
obchodní	obchodní	k2eAgFnSc1d1	obchodní
Váha	váha	k1gFnSc1	váha
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
přestavována	přestavován	k2eAgFnSc1d1	přestavována
sloužila	sloužit	k5eAaImAgFnS	sloužit
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
je	být	k5eAaImIp3nS	být
zastavěno	zastavěn	k2eAgNnSc1d1	zastavěno
převážně	převážně	k6eAd1	převážně
domy	dům	k1gInPc1	dům
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
nazývají	nazývat	k5eAaImIp3nP	nazývat
Zlatým	zlatý	k2eAgInSc7d1	zlatý
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
systém	systém	k1gInSc1	systém
soustředných	soustředný	k2eAgInPc2d1	soustředný
kanálů	kanál	k1gInPc2	kanál
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
nejznámější	známý	k2eAgInPc1d3	nejznámější
kanály	kanál	k1gInPc1	kanál
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Herengracht	Herengracht	k1gMnSc1	Herengracht
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
pánů	pan	k1gMnPc2	pan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kaizersgracht	Kaizersgracht	k1gInSc1	Kaizersgracht
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
císařů	císař	k1gMnPc2	císař
<g/>
)	)	kIx)	)
a	a	k8xC	a
Prinsengracht	Prinsengracht	k2eAgInSc1d1	Prinsengracht
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
princů	princ	k1gMnPc2	princ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
pomník	pomník	k1gInSc1	pomník
před	před	k7c7	před
univerzitou	univerzita	k1gFnSc7	univerzita
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
nizozemskému	nizozemský	k2eAgMnSc3d1	nizozemský
filozofovi	filozof	k1gMnSc3	filozof
Baruchu	Baruch	k1gMnSc3	Baruch
Spinozovi	Spinoz	k1gMnSc3	Spinoz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Panel	panel	k1gInSc1	panel
s	s	k7c7	s
textem	text	k1gInSc7	text
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
malovaný	malovaný	k2eAgInSc1d1	malovaný
portrét	portrét	k1gInSc1	portrét
a	a	k8xC	a
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c6	v
Židovském	židovský	k2eAgNnSc6d1	Židovské
historickém	historický	k2eAgNnSc6d1	historické
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
proslaveno	proslaven	k2eAgNnSc1d1	proslaveno
muzei	muzeum	k1gNnPc7	muzeum
a	a	k8xC	a
galeriemi	galerie	k1gFnPc7	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozlehlém	rozlehlý	k2eAgNnSc6d1	rozlehlé
Muzejním	muzejní	k2eAgNnSc6d1	muzejní
náměstí	náměstí	k1gNnSc6	náměstí
Museumplein	Museumpleina	k1gFnPc2	Museumpleina
stojí	stát	k5eAaImIp3nS	stát
blízko	blízko	k6eAd1	blízko
sebe	sebe	k3xPyFc4	sebe
situována	situován	k2eAgFnSc1d1	situována
tři	tři	k4xCgNnPc1	tři
muzea	muzeum	k1gNnPc1	muzeum
<g/>
:	:	kIx,	:
Rijksmuseum	Rijksmuseum	k1gInSc1	Rijksmuseum
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
Gogh	Gogha	k1gFnPc2	Gogha
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
Stedelijk	Stedelijk	k1gInSc1	Stedelijk
a	a	k8xC	a
koncertní	koncertní	k2eAgFnSc1d1	koncertní
budova	budova	k1gFnSc1	budova
Concertgebouw	Concertgebouw	k1gFnSc1	Concertgebouw
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rijksmuseum	Rijksmuseum	k1gNnSc1	Rijksmuseum
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Říšské	říšský	k2eAgNnSc1d1	říšské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
republiky	republika	k1gFnSc2	republika
Batávie	Batávie	k1gFnSc2	Batávie
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
místodržitel	místodržitel	k1gMnSc1	místodržitel
princ	princ	k1gMnSc1	princ
Vilém	Vilém	k1gMnSc1	Vilém
V.	V.	kA	V.
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
zabaveny	zabaven	k2eAgFnPc1d1	zabavena
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
200	[number]	k4	200
nevýznamnějších	významný	k2eNgInPc2d2	nevýznamnější
objektů	objekt	k1gInPc2	objekt
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodních	úvodní	k2eAgInPc6d1	úvodní
sálech	sál	k1gInPc6	sál
Rijksmusea	Rijksmuse	k1gInSc2	Rijksmuse
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
modelech	model	k1gInPc6	model
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
portrétech	portrét	k1gInPc6	portrét
námořních	námořní	k2eAgMnPc2d1	námořní
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
mapách	mapa	k1gFnPc6	mapa
a	a	k8xC	a
uměleckořemeslných	uměleckořemeslný	k2eAgInPc6d1	uměleckořemeslný
výrobcích	výrobek	k1gInPc6	výrobek
z	z	k7c2	z
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
ilustrovány	ilustrován	k2eAgFnPc4d1	ilustrována
nizozemské	nizozemský	k2eAgFnPc4d1	nizozemská
dějiny	dějiny	k1gFnPc4	dějiny
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
představeny	představen	k2eAgInPc4d1	představen
portréty	portrét	k1gInPc4	portrét
a	a	k8xC	a
emblémy	emblém	k1gInPc4	emblém
vlády	vláda	k1gFnSc2	vláda
princů	princ	k1gMnPc2	princ
dynastie	dynastie	k1gFnSc2	dynastie
Oranžsko-Nasavské	Oranžsko-Nasavský	k2eAgInPc1d1	Oranžsko-Nasavský
a	a	k8xC	a
skupinové	skupinový	k2eAgInPc1d1	skupinový
portréty	portrét	k1gInPc1	portrét
představitelů	představitel	k1gMnPc2	představitel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
plátnům	plátno	k1gNnPc3	plátno
patří	patřit	k5eAaImIp3nS	patřit
Hostina	hostina	k1gFnSc1	hostina
Svatojiřského	svatojiřský	k2eAgInSc2d1	svatojiřský
spolku	spolek	k1gInSc2	spolek
ostrostřelců	ostrostřelec	k1gMnPc2	ostrostřelec
<g/>
,	,	kIx,	,
konaná	konaný	k2eAgFnSc1d1	konaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
podepsání	podepsání	k1gNnSc4	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
v	v	k7c6	v
Münsteru	Münster	k1gInSc6	Münster
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc4	obraz
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Bartholomeus	Bartholomeus	k1gMnSc1	Bartholomeus
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Helst	Helst	k1gInSc4	Helst
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
sbírek	sbírka	k1gFnPc2	sbírka
tvoří	tvořit	k5eAaImIp3nP	tvořit
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
expozice	expozice	k1gFnSc2	expozice
olejomalby	olejomalba	k1gFnSc2	olejomalba
a	a	k8xC	a
kresby	kresba	k1gFnSc2	kresba
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
a	a	k8xC	a
vlámských	vlámský	k2eAgMnPc2d1	vlámský
malířů	malíř	k1gMnPc2	malíř
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
obrazy	obraz	k1gInPc7	obraz
jsou	být	k5eAaImIp3nP	být
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
Židovská	židovský	k2eAgFnSc1d1	židovská
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
autoportréty	autoportrét	k1gInPc1	autoportrét
a	a	k8xC	a
zátiší	zátiší	k1gNnPc1	zátiší
od	od	k7c2	od
Rembrandta	Rembrandt	k1gInSc2	Rembrandt
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
Mlékařka	mlékařka	k1gFnSc1	mlékařka
a	a	k8xC	a
Žena	žena	k1gFnSc1	žena
čtoucí	čtoucí	k2eAgFnSc1d1	čtoucí
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Vermeera	Vermeer	k1gMnSc2	Vermeer
<g/>
,	,	kIx,	,
portréty	portrét	k1gInPc1	portrét
Franse	Frans	k1gMnSc2	Frans
Halse	Hals	k1gMnSc2	Hals
nebo	nebo	k8xC	nebo
Žena	žena	k1gFnSc1	žena
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
Veselá	veselý	k2eAgFnSc1d1	veselá
rodina	rodina	k1gFnSc1	rodina
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Steena	Steen	k1gMnSc2	Steen
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc4	van
Goghovo	Goghův	k2eAgNnSc1d1	Goghovo
muzeum	muzeum	k1gNnSc1	muzeum
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
obrazů	obraz	k1gInPc2	obraz
Vincenta	Vincent	k1gMnSc2	Vincent
van	vana	k1gFnPc2	vana
Gogha	Gogha	k1gMnSc1	Gogha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
uložena	uložit	k5eAaPmNgFnS	uložit
také	také	k9	také
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
malíř	malíř	k1gMnSc1	malíř
psal	psát	k5eAaImAgMnS	psát
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Theovi	Thea	k1gMnSc3	Thea
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
vystaveny	vystaven	k2eAgInPc4d1	vystaven
obrazy	obraz	k1gInPc4	obraz
Paula	Paul	k1gMnSc2	Paul
Gauguina	Gauguin	k1gMnSc2	Gauguin
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
Moneta	moneta	k1gFnSc1	moneta
<g/>
,	,	kIx,	,
Felixe	Felix	k1gMnSc2	Felix
Valottona	Valotton	k1gMnSc2	Valotton
<g/>
,	,	kIx,	,
Pabla	Pabl	k1gMnSc2	Pabl
Picassa	Picass	k1gMnSc2	Picass
a	a	k8xC	a
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Toulouse-Lautreca	Toulouse-Lautreca	k1gMnSc1	Toulouse-Lautreca
<g/>
.	.	kIx.	.
</s>
<s>
Stedelijk	Stedelijk	k6eAd1	Stedelijk
museum	museum	k1gNnSc1	museum
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
velkou	velký	k2eAgFnSc4d1	velká
sbírku	sbírka	k1gFnSc4	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ukrývala	ukrývat	k5eAaImAgFnS	ukrývat
rodina	rodina	k1gFnSc1	rodina
židovského	židovský	k2eAgMnSc2d1	židovský
zubního	zubní	k2eAgMnSc2d1	zubní
lékaře	lékař	k1gMnSc2	lékař
Franka	Frank	k1gMnSc2	Frank
až	až	k9	až
do	do	k7c2	do
prozrazení	prozrazení	k1gNnSc2	prozrazení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
věnován	věnován	k2eAgInSc1d1	věnován
expozici	expozice	k1gFnSc4	expozice
o	o	k7c6	o
utajeném	utajený	k2eAgInSc6d1	utajený
životě	život	k1gInSc6	život
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnácti-	Čtrnácti-	k?	Čtrnácti-
až	až	k9	až
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
dcera	dcera	k1gFnSc1	dcera
Anna	Anna	k1gFnSc1	Anna
Franková	Franková	k1gFnSc1	Franková
psala	psát	k5eAaImAgFnS	psát
svůj	svůj	k3xOyFgInSc4	svůj
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgNnSc1d1	Židovské
historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
spravuje	spravovat	k5eAaImIp3nS	spravovat
významné	významný	k2eAgFnPc4d1	významná
historické	historický	k2eAgFnPc4d1	historická
a	a	k8xC	a
umělecké	umělecký	k2eAgFnPc4d1	umělecká
sbírky	sbírka	k1gFnPc4	sbírka
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
židovských	židovský	k2eAgFnPc2d1	židovská
rodin	rodina	k1gFnPc2	rodina
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
přistavěné	přistavěný	k2eAgFnSc6d1	přistavěná
ke	k	k7c3	k
staré	stará	k1gFnSc3	stará
synagóze	synagóga	k1gFnSc3	synagóga
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
stojí	stát	k5eAaImIp3nS	stát
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
synagóga	synagóga	k1gFnSc1	synagóga
<g/>
.	.	kIx.	.
</s>
<s>
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
nedaleko	daleko	k6eNd1	daleko
odtud	odtud	k6eAd1	odtud
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgNnSc1d1	námořní
muzeum	muzeum	k1gNnSc1	muzeum
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
námořní	námořní	k2eAgFnSc2d1	námořní
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
VOC	VOC	kA	VOC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
traktech	trakt	k1gInPc6	trakt
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
představují	představovat	k5eAaImIp3nP	představovat
jednak	jednak	k8xC	jednak
sbírky	sbírka	k1gFnPc1	sbírka
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
křídle	křídlo	k1gNnSc6	křídlo
je	být	k5eAaImIp3nS	být
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
oddílech	oddíl	k1gInPc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
zámořských	zámořský	k2eAgFnPc2d1	zámořská
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
o	o	k7c4	o
velrybářství	velrybářství	k1gNnSc4	velrybářství
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgNnSc1	třetí
o	o	k7c6	o
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
námořnících	námořník	k1gMnPc6	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
muzeem	muzeum	k1gNnSc7	muzeum
kotví	kotvit	k5eAaImIp3nS	kotvit
přesná	přesný	k2eAgFnSc1d1	přesná
kopie	kopie	k1gFnSc1	kopie
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
námořních	námořní	k2eAgFnPc2d1	námořní
lodí	loď	k1gFnPc2	loď
společnosti	společnost	k1gFnSc2	společnost
VOC	VOC	kA	VOC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
se	s	k7c7	s
vstupenkou	vstupenka	k1gFnSc7	vstupenka
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
slavné	slavný	k2eAgFnPc4d1	slavná
etapy	etapa	k1gFnPc4	etapa
vývoje	vývoj	k1gInSc2	vývoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgFnSc2d1	slavná
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Begijnenconvent	Begijnenconvent	k1gInSc1	Begijnenconvent
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgInSc1d1	bývalý
konvent	konvent	k1gInSc1	konvent
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
bekyň	bekyně	k1gFnPc2	bekyně
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
několika	několik	k4yIc2	několik
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
žily	žít	k5eAaImAgFnP	žít
neprovdané	provdaný	k2eNgFnPc1d1	neprovdaná
zbožné	zbožný	k2eAgFnPc1d1	zbožná
ženy	žena	k1gFnPc1	žena
ctnostným	ctnostný	k2eAgInSc7d1	ctnostný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
domky	domek	k1gInPc1	domek
vyhořely	vyhořet	k5eAaPmAgInP	vyhořet
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
postavené	postavený	k2eAgFnPc1d1	postavená
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přešly	přejít	k5eAaPmAgInP	přejít
pod	pod	k7c4	pod
anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
slouží	sloužit	k5eAaImIp3nS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Rembrandthuis	Rembrandthuis	k1gFnSc1	Rembrandthuis
-	-	kIx~	-
Rembrandtův	Rembrandtův	k2eAgInSc1d1	Rembrandtův
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
památníkem	památník	k1gInSc7	památník
malíře	malíř	k1gMnSc2	malíř
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
rekonstruovanými	rekonstruovaný	k2eAgInPc7d1	rekonstruovaný
interiéry	interiér	k1gInPc7	interiér
od	od	k7c2	od
kuchyně	kuchyně	k1gFnSc2	kuchyně
přes	přes	k7c4	přes
přijímací	přijímací	k2eAgInSc4d1	přijímací
sál	sál	k1gInSc4	sál
s	s	k7c7	s
vystavenými	vystavený	k2eAgInPc7d1	vystavený
obrazy	obraz	k1gInPc7	obraz
<g/>
,	,	kIx,	,
ložnici	ložnice	k1gFnSc4	ložnice
a	a	k8xC	a
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
se	se	k3xPyFc4	se
také	také	k9	také
významná	významný	k2eAgFnSc1d1	významná
sbírka	sbírka	k1gFnSc1	sbírka
Rembrandtových	Rembrandtův	k2eAgInPc2d1	Rembrandtův
grafických	grafický	k2eAgInPc2d1	grafický
listů	list	k1gInPc2	list
a	a	k8xC	a
měditiskových	měditiskový	k2eAgFnPc2d1	měditiskový
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
4	[number]	k4	4
linky	linka	k1gFnSc2	linka
metra	metro	k1gNnSc2	metro
+	+	kIx~	+
1	[number]	k4	1
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
1	[number]	k4	1
trasa	trasa	k1gFnSc1	trasa
městské	městský	k2eAgFnSc2d1	městská
železnice	železnice	k1gFnSc2	železnice
17	[number]	k4	17
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tras	trasa	k1gFnPc2	trasa
mnoho	mnoho	k6eAd1	mnoho
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
několik	několik	k4yIc4	několik
trajektů	trajekt	k1gInPc2	trajekt
přes	přes	k7c4	přes
IJ	IJ	kA	IJ
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
(	(	kIx(	(
<g/>
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
lodní	lodní	k2eAgFnSc2d1	lodní
linky	linka	k1gFnSc2	linka
Fast	Fast	k1gMnSc1	Fast
<g />
.	.	kIx.	.
</s>
<s>
Flying	Flying	k1gInSc1	Flying
Ferrie	Ferrie	k1gFnSc2	Ferrie
do	do	k7c2	do
Velsen-Zuid	Velsen-Zuida	k1gFnPc2	Velsen-Zuida
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
Nová	nový	k2eAgFnSc1d1	nová
trasa	trasa	k1gFnSc1	trasa
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Severojižní	severojižní	k2eAgFnSc1d1	severojižní
trasa	trasa	k1gFnSc1	trasa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Noord	Noord	k1gInSc1	Noord
<g/>
/	/	kIx~	/
<g/>
Zuidlijn	Zuidlijn	k1gInSc1	Zuidlijn
<g/>
)	)	kIx)	)
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trasa	trasa	k1gFnSc1	trasa
na	na	k7c4	na
IJburg	IJburg	k1gInSc4	IJburg
(	(	kIx(	(
<g/>
nová	nový	k2eAgFnSc1d1	nová
čtvrť	čtvrť	k1gFnSc1	čtvrť
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
na	na	k7c6	na
umělém	umělý	k2eAgInSc6d1	umělý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
městě	město	k1gNnSc6	město
na	na	k7c6	na
bicyklu	bicykl	k1gInSc6	bicykl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
systém	systém	k1gInSc1	systém
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
Jízda	jízda	k1gFnSc1	jízda
autem	auto	k1gNnSc7	auto
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
kvůli	kvůli	k7c3	kvůli
dopravním	dopravní	k2eAgFnPc3d1	dopravní
zácpám	zácpa	k1gFnPc3	zácpa
a	a	k8xC	a
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
parkováním	parkování	k1gNnSc7	parkování
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Schiphol	Schiphol	k1gInSc1	Schiphol
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
asi	asi	k9	asi
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
vlakem	vlak	k1gInSc7	vlak
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
největší	veliký	k2eAgFnSc6d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
odbaví	odbavit	k5eAaPmIp3nS	odbavit
asi	asi	k9	asi
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgNnSc7d1	domovské
letištěm	letiště	k1gNnSc7	letiště
společnosti	společnost	k1gFnSc2	společnost
KLM	KLM	kA	KLM
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Amstel	Amstela	k1gFnPc2	Amstela
a	a	k8xC	a
Ij	Ij	k1gFnPc2	Ij
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdamský	amsterdamský	k2eAgInSc1d1	amsterdamský
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
mořským	mořský	k2eAgInSc7d1	mořský
kanálem	kanál	k1gInSc7	kanál
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
amsterdamském	amsterdamský	k2eAgInSc6d1	amsterdamský
přístavu	přístav	k1gInSc6	přístav
koná	konat	k5eAaImIp3nS	konat
světová	světový	k2eAgFnSc1d1	světová
přehlídka	přehlídka	k1gFnSc1	přehlídka
lodí	loď	k1gFnPc2	loď
SAIL	SAIL	kA	SAIL
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgNnSc7d1	domovské
městem	město	k1gNnSc7	město
týmu	tým	k1gInSc2	tým
Ajax	Ajax	k1gInSc4	Ajax
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
týmu	tým	k1gInSc2	tým
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgInSc1d1	moderní
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Arena	Arena	k1gFnSc1	Arena
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ajax	Ajax	k1gInSc1	Ajax
sdílí	sdílet	k5eAaImIp3nS	sdílet
stadion	stadion	k1gInSc4	stadion
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Admirals	Admirals	k1gInSc1	Admirals
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
World	World	k1gMnSc1	World
Bowl	Bowl	k1gMnSc1	Bowl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
konaly	konat	k5eAaImAgFnP	konat
IX	IX	kA	IX
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zrekonstruován	zrekonstruovat	k5eAaPmNgInS	zrekonstruovat
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
sportovním	sportovní	k2eAgFnPc3d1	sportovní
a	a	k8xC	a
kulturním	kulturní	k2eAgFnPc3d1	kulturní
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
stadionu	stadion	k1gInSc6	stadion
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
některých	některý	k3yIgFnPc2	některý
vrhačských	vrhačský	k2eAgFnPc2d1	vrhačská
disciplín	disciplína	k1gFnPc2	disciplína
(	(	kIx(	(
<g/>
hod	hod	k1gInSc1	hod
diskem	disk	k1gInSc7	disk
a	a	k8xC	a
hod	hod	k1gInSc1	hod
oštěpěm	oštěpěm	k6eAd1	oštěpěm
mužů	muž	k1gMnPc2	muž
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
však	však	k9	však
netradičně	tradičně	k6eNd1	tradičně
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Museumplein	Museumpleina	k1gFnPc2	Museumpleina
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
Beira	Beir	k1gInSc2	Beir
<g/>
,	,	kIx,	,
Mosambik	Mosambik	k1gInSc1	Mosambik
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Managua	Managua	k1gFnSc1	Managua
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
Montreal	Montreal	k1gInSc1	Montreal
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Jan	Jan	k1gMnSc1	Jan
Pietrszoon	Pietrszoon	k1gMnSc1	Pietrszoon
Sweelinck	Sweelinck	k1gMnSc1	Sweelinck
(	(	kIx(	(
<g/>
1562	[number]	k4	1562
<g/>
-	-	kIx~	-
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
John	John	k1gMnSc1	John
<g />
.	.	kIx.	.
</s>
<s>
Smyth	Smyth	k1gInSc1	Smyth
(	(	kIx(	(
<g/>
1570	[number]	k4	1570
-	-	kIx~	-
1612	[number]	k4	1612
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
baptistický	baptistický	k2eAgMnSc1d1	baptistický
kazatel	kazatel	k1gMnSc1	kazatel
a	a	k8xC	a
zastánce	zastánce	k1gMnSc1	zastánce
principu	princip	k1gInSc2	princip
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
Willem	Will	k1gMnSc7	Will
Blaeu	Blaeus	k1gInSc2	Blaeus
(	(	kIx(	(
<g/>
1571	[number]	k4	1571
-	-	kIx~	-
1638	[number]	k4	1638
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
atlasů	atlas	k1gInPc2	atlas
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Dirck	Dirck	k1gMnSc1	Dirck
Hartog	Hartog	k1gMnSc1	Hartog
(	(	kIx(	(
<g/>
1580	[number]	k4	1580
<g/>
-	-	kIx~	-
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
námořník	námořník	k1gMnSc1	námořník
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
průzkumník	průzkumník	k1gMnSc1	průzkumník
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spatřil	spatřit	k5eAaPmAgMnS	spatřit
australský	australský	k2eAgInSc4d1	australský
kontinent	kontinent	k1gInSc4	kontinent
Esaias	Esaiasa	k1gFnPc2	Esaiasa
van	vana	k1gFnPc2	vana
de	de	k?	de
Velde	Veld	k1gInSc5	Veld
(	(	kIx(	(
<g/>
1587	[number]	k4	1587
<g/>
-	-	kIx~	-
<g/>
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
-	-	kIx~	-
krajinář	krajinář	k1gMnSc1	krajinář
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
-	-	kIx~	-
1670	[number]	k4	1670
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
biskup	biskup	k1gMnSc1	biskup
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
Nicolaes	Nicolaes	k1gMnSc1	Nicolaes
Tulp	Tulp	k1gInSc1	Tulp
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
-	-	kIx~	-
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chirurg	chirurg	k1gMnSc1	chirurg
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
Aernout	Aernout	k1gMnSc2	Aernout
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Neer	Neer	k1gMnSc1	Neer
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
/	/	kIx~	/
<g/>
1604	[number]	k4	1604
-	-	kIx~	-
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
-	-	kIx~	-
krajinář	krajinář	k1gMnSc1	krajinář
Rembrandt	Rembrandt	k1gMnSc1	Rembrandt
van	vana	k1gFnPc2	vana
Rijn	Rijn	k1gMnSc1	Rijn
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1606	[number]	k4	1606
-	-	kIx~	-
1669	[number]	k4	1669
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Paulus	Paulus	k1gMnSc1	Paulus
Potter	Potter	k1gMnSc1	Potter
(	(	kIx(	(
<g/>
1625	[number]	k4	1625
<g/>
-	-	kIx~	-
<g/>
1654	[number]	k4	1654
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Willem	Willo	k1gNnSc7	Willo
van	van	k1gInSc1	van
Aelst	Aelst	k1gInSc1	Aelst
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
-	-	kIx~	-
cca	cca	kA	cca
1683	[number]	k4	1683
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
zátiší	zátiší	k1gNnSc2	zátiší
Baruch	Baruch	k1gMnSc1	Baruch
Spinoza	Spinoz	k1gMnSc2	Spinoz
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
-	-	kIx~	-
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
Meindert	Meindert	k1gMnSc1	Meindert
<g />
.	.	kIx.	.
</s>
<s>
Hobbema	Hobbema	k1gFnSc1	Hobbema
(	(	kIx(	(
<g/>
1638	[number]	k4	1638
-	-	kIx~	-
1709	[number]	k4	1709
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř-krajinář	malířrajinář	k1gMnSc1	malíř-krajinář
Frederik	Frederik	k1gMnSc1	Frederik
Ruysch	Ruysch	k1gMnSc1	Ruysch
(	(	kIx(	(
<g/>
1638	[number]	k4	1638
<g/>
-	-	kIx~	-
<g/>
1731	[number]	k4	1731
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anatom	anatom	k1gMnSc1	anatom
a	a	k8xC	a
botanik	botanik	k1gMnSc1	botanik
Maarten	Maarten	k2eAgInSc4d1	Maarten
Houttuyn	Houttuyn	k1gInSc4	Houttuyn
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
-	-	kIx~	-
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Coenraad	Coenraad	k1gInSc4	Coenraad
Jacob	Jacoba	k1gFnPc2	Jacoba
Temminck	Temmincka	k1gFnPc2	Temmincka
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aristokrat	aristokrat	k1gMnSc1	aristokrat
a	a	k8xC	a
zoolog	zoolog	k1gMnSc1	zoolog
Martinus	Martinus	k1gMnSc1	Martinus
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Beijerinck	Beijerinck	k1gMnSc1	Beijerinck
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Karl	Karl	k1gMnSc1	Karl
Kautsky	Kautsky	k1gMnSc1	Kautsky
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
-	-	kIx~	-
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
socialistický	socialistický	k2eAgMnSc1d1	socialistický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
marxismu	marxismus	k1gInSc2	marxismus
Pieter	Pieter	k1gMnSc1	Pieter
Zeeman	Zeeman	k1gMnSc1	Zeeman
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
-	-	kIx~	-
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Paul	Paul	k1gMnSc1	Paul
Ehrenfest	Ehrenfest	k1gMnSc1	Ehrenfest
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Frits	Fritsa	k1gFnPc2	Fritsa
Zernike	Zernike	k1gFnPc2	Zernike
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
-	-	kIx~	-
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Max	Max	k1gMnSc1	Max
Euwe	Euwe	k1gInSc1	Euwe
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
-	-	kIx~	-
1981	[number]	k4	1981
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
Annie	Annie	k1gFnSc2	Annie
M.	M.	kA	M.
G.	G.	kA	G.
Schmidt	Schmidt	k1gMnSc1	Schmidt
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
a	a	k8xC	a
dramatička	dramatička	k1gFnSc1	dramatička
Paul	Paul	k1gMnSc1	Paul
Verhoeven	Verhoeven	k2eAgMnSc1d1	Verhoeven
(	(	kIx(	(
<g/>
*	*	kIx~	*
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
<g />
.	.	kIx.	.
</s>
<s>
producent	producent	k1gMnSc1	producent
Ton	Ton	k1gMnSc1	Ton
Koopman	Koopman	k1gMnSc1	Koopman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
cembalista	cembalista	k1gMnSc1	cembalista
Johan	Johan	k1gMnSc1	Johan
Cruijff	Cruijff	k1gMnSc1	Cruijff
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
André	André	k1gMnSc1	André
Hazes	Hazes	k1gMnSc1	Hazes
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Theo	Thea	k1gFnSc5	Thea
van	van	k1gInSc4	van
Gogh	Gogh	k1gInSc1	Gogh
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
André	André	k1gMnSc1	André
Kuipers	Kuipersa	k1gFnPc2	Kuipersa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
ESA	eso	k1gNnSc2	eso
Ruud	Ruud	k1gMnSc1	Ruud
Gullit	Gullit	k1gInSc1	Gullit
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Frank	Frank	k1gMnSc1	Frank
Rijkaard	Rijkaard	k1gMnSc1	Rijkaard
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
záložník	záložník	k1gMnSc1	záložník
Dennis	Dennis	k1gFnSc2	Dennis
Bergkamp	Bergkamp	k1gMnSc1	Bergkamp
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Patrick	Patrick	k1gMnSc1	Patrick
Kluivert	Kluivert	k1gMnSc1	Kluivert
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
Denny	Denna	k1gFnSc2	Denna
Landzaat	Landzaat	k2eAgMnSc1d1	Landzaat
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
Martin	Martin	k1gMnSc1	Martin
Garrix	Garrix	k1gInSc1	Garrix
(	(	kIx(	(
<g/>
*	*	kIx~	*
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Galerie	galerie	k1gFnSc2	galerie
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Holandská	holandský	k2eAgFnSc1d1	holandská
informační	informační	k2eAgFnSc1d1	informační
turistická	turistický	k2eAgFnSc1d1	turistická
kancelář	kancelář	k1gFnSc1	kancelář
(	(	kIx(	(
<g/>
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnPc1	zajímavost
z	z	k7c2	z
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
-	-	kIx~	-
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
(	(	kIx(	(
<g/>
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
org	org	k?	org
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
)	)	kIx)	)
Průvodce	průvodka	k1gFnSc6	průvodka
Amsterdamem	Amsterdam	k1gInSc7	Amsterdam
Concertgebouw	Concertgebouw	k1gFnSc2	Concertgebouw
(	(	kIx(	(
<g/>
koncertní	koncertní	k2eAgFnSc1d1	koncertní
hala	hala	k1gFnSc1	hala
<g/>
)	)	kIx)	)
Heslo	heslo	k1gNnSc1	heslo
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Encarta	Encart	k1gMnSc2	Encart
Průvodce	průvodce	k1gMnSc2	průvodce
po	po	k7c6	po
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
-	-	kIx~	-
Wikivoyage	Wikivoyage	k1gInSc1	Wikivoyage
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
Rijksmuseum	Rijksmuseum	k1gNnSc1	Rijksmuseum
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
Van	vana	k1gFnPc2	vana
Goghovo	Goghův	k2eAgNnSc1d1	Goghovo
muzeum	muzeum	k1gNnSc1	muzeum
Velmi	velmi	k6eAd1	velmi
podrobná	podrobný	k2eAgFnSc1d1	podrobná
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
Stránka	stránka	k1gFnSc1	stránka
olympijského	olympijský	k2eAgInSc2d1	olympijský
stadionu	stadion	k1gInSc2	stadion
Ulice	ulice	k1gFnSc2	ulice
centra	centrum	k1gNnSc2	centrum
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
Klepnutím	klepnutí	k1gNnSc7	klepnutí
na	na	k7c4	na
fotografie	fotografia	k1gFnPc4	fotografia
můžete	moct	k5eAaImIp2nP	moct
procházet	procházet	k5eAaImF	procházet
skrz	skrz	k7c4	skrz
město	město	k1gNnSc4	město
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
univerzální	univerzální	k2eAgMnSc1d1	univerzální
český	český	k2eAgMnSc1d1	český
průvodce	průvodce	k1gMnSc1	průvodce
Amsterdamem	Amsterdam	k1gInSc7	Amsterdam
jeden	jeden	k4xCgInSc4	jeden
víkend	víkend	k1gInSc4	víkend
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
Amsterodam	Amsterodam	k1gInSc1	Amsterodam
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
