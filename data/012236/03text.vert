<p>
<s>
Jeřáb	jeřáb	k1gInSc1	jeřáb
popelavý	popelavý	k2eAgInSc1d1	popelavý
(	(	kIx(	(
<g/>
Grus	Grus	k1gInSc1	Grus
grus	grus	k1gInSc1	grus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jeřábovitých	jeřábovitý	k2eAgMnPc2d1	jeřábovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
2	[number]	k4	2
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
jeřáb	jeřáb	k1gInSc1	jeřáb
popelavý	popelavý	k2eAgInSc1d1	popelavý
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Grus	Grus	k1gInSc1	Grus
grus	grus	k6eAd1	grus
grus	grus	k6eAd1	grus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
evropskou	evropský	k2eAgFnSc4d1	Evropská
část	část	k1gFnSc4	část
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeřáb	jeřáb	k1gInSc1	jeřáb
popelavý	popelavý	k2eAgInSc1d1	popelavý
sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
(	(	kIx(	(
<g/>
Grus	Grus	k1gInSc1	Grus
grus	grusa	k1gFnPc2	grusa
lilfordi	lilford	k1gMnPc1	lilford
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
asijskou	asijský	k2eAgFnSc4d1	asijská
část	část	k1gFnSc4	část
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
96	[number]	k4	96
<g/>
–	–	k?	–
<g/>
119	[number]	k4	119
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
222	[number]	k4	222
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhýma	dlouhý	k2eAgFnPc7d1	dlouhá
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
štíhlým	štíhlý	k2eAgInSc7d1	štíhlý
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
Abnormálně	abnormálně	k6eAd1	abnormálně
prodloužené	prodloužený	k2eAgFnSc2d1	prodloužená
ramenní	ramenní	k2eAgFnSc2d1	ramenní
letky	letka	k1gFnSc2	letka
tvoří	tvořit	k5eAaImIp3nS	tvořit
načechraný	načechraný	k2eAgInSc1d1	načechraný
"	"	kIx"	"
<g/>
ocas	ocas	k1gInSc1	ocas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
skutečný	skutečný	k2eAgInSc1d1	skutečný
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
patrný	patrný	k2eAgInSc1d1	patrný
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
šedavě	šedavě	k6eAd1	šedavě
modrošedé	modrošedý	k2eAgFnPc1d1	modrošedá
<g/>
,	,	kIx,	,
s	s	k7c7	s
šedočernými	šedočerný	k2eAgFnPc7d1	šedočerná
letkami	letka	k1gFnPc7	letka
a	a	k8xC	a
černobílou	černobílý	k2eAgFnSc7d1	černobílá
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
Lysina	lysina	k1gFnSc1	lysina
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
temene	temeno	k1gNnSc2	temeno
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
rezavohnědě	rezavohnědě	k6eAd1	rezavohnědě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
šedavý	šedavý	k2eAgMnSc1d1	šedavý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
světle	světle	k6eAd1	světle
rezavohnědým	rezavohnědý	k2eAgNnSc7d1	rezavohnědé
zbarvením	zbarvení	k1gNnSc7	zbarvení
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
a	a	k8xC	a
méně	málo	k6eAd2	málo
objemnými	objemný	k2eAgFnPc7d1	objemná
ramenními	ramenní	k2eAgFnPc7d1	ramenní
letkami	letka	k1gFnPc7	letka
<g/>
.	.	kIx.	.
<g/>
Létá	létat	k5eAaImIp3nS	létat
v	v	k7c6	v
klínovitých	klínovitý	k2eAgFnPc6d1	klínovitá
formacích	formace	k1gFnPc6	formace
nebo	nebo	k8xC	nebo
v	v	k7c6	v
šikmých	šikmý	k2eAgFnPc6d1	šikmá
řadách	řada	k1gFnPc6	řada
<g/>
,	,	kIx,	,
s	s	k7c7	s
nataženým	natažený	k2eAgInSc7d1	natažený
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Ozývá	ozývat	k5eAaImIp3nS	ozývat
se	se	k3xPyFc4	se
nezaměnitelnými	zaměnitelný	k2eNgInPc7d1	nezaměnitelný
troubivými	troubivý	k2eAgInPc7d1	troubivý
zvuky	zvuk	k1gInPc7	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
jeřábů	jeřáb	k1gMnPc2	jeřáb
v	v	k7c6	v
lesích	les	k1gInPc6	les
u	u	k7c2	u
Josvainiů	Josvaini	k1gInPc2	Josvaini
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
palearktický	palearktický	k2eAgInSc4d1	palearktický
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pokrývající	pokrývající	k2eAgInSc4d1	pokrývající
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
pás	pás	k1gInSc4	pás
severských	severský	k2eAgInPc2d1	severský
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
asijských	asijský	k2eAgFnPc2d1	asijská
stepí	step	k1gFnPc2	step
rozkládajících	rozkládající	k2eAgFnPc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
značně	značně	k6eAd1	značně
zredukován	zredukován	k2eAgMnSc1d1	zredukován
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gFnPc1	jeho
hranice	hranice	k1gFnPc1	hranice
opět	opět	k6eAd1	opět
posunují	posunovat	k5eAaImIp3nP	posunovat
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeřáb	jeřáb	k1gInSc1	jeřáb
popelavý	popelavý	k2eAgInSc1d1	popelavý
se	se	k3xPyFc4	se
jako	jako	k9	jako
hnízdič	hnízdič	k1gMnSc1	hnízdič
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odlétá	odlétat	k5eAaImIp3nS	odlétat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
během	během	k7c2	během
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
jeřáb	jeřáb	k1gInSc1	jeřáb
popelavý	popelavý	k2eAgInSc1d1	popelavý
objevoval	objevovat	k5eAaImAgInS	objevovat
až	až	k6eAd1	až
do	do	k7c2	do
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
řídce	řídce	k6eAd1	řídce
protahující	protahující	k2eAgInSc4d1	protahující
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
výskytu	výskyt	k1gInSc3	výskyt
v	v	k7c6	v
hnízdní	hnízdní	k2eAgFnSc6d1	hnízdní
době	doba	k1gFnSc6	doba
možné	možný	k2eAgNnSc1d1	možné
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c6	na
hnízdění	hnízdění	k1gNnSc6	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
u	u	k7c2	u
Doks	Doksy	k1gInPc2	Doksy
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
lokalitách	lokalita	k1gFnPc6	lokalita
Českolipska	Českolipsko	k1gNnSc2	Českolipsko
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
v	v	k7c6	v
Heřmaničkách	Heřmanička	k1gFnPc6	Heřmanička
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
opět	opět	k6eAd1	opět
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
<g/>
.	.	kIx.	.
</s>
<s>
Českolipsko	Českolipsko	k1gNnSc1	Českolipsko
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hnízdní	hnízdní	k2eAgFnSc7d1	hnízdní
oblastí	oblast	k1gFnSc7	oblast
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
zde	zde	k6eAd1	zde
pravidelně	pravidelně	k6eAd1	pravidelně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
minimálně	minimálně	k6eAd1	minimálně
6	[number]	k4	6
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
Děčínsko	Děčínsko	k1gNnSc1	Děčínsko
<g/>
,	,	kIx,	,
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
,	,	kIx,	,
Královéhradecko	Královéhradecko	k1gNnSc1	Královéhradecko
<g/>
,	,	kIx,	,
Mladoboleslavsko	Mladoboleslavsko	k1gNnSc1	Mladoboleslavsko
<g/>
,	,	kIx,	,
Strakonicko	Strakonicko	k1gNnSc1	Strakonicko
a	a	k8xC	a
Hodonínsko	Hodonínsko	k1gNnSc1	Hodonínsko
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
i	i	k8xC	i
počet	počet	k1gInSc1	počet
obsazených	obsazený	k2eAgFnPc2d1	obsazená
lokalit	lokalita	k1gFnPc2	lokalita
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985-89	[number]	k4	1985-89
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
1-5	[number]	k4	1-5
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990-94	[number]	k4	1990-94
na	na	k7c4	na
5-10	[number]	k4	5-10
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
hnízdění	hnízdění	k1gNnSc4	hnízdění
u	u	k7c2	u
8	[number]	k4	8
párů	pár	k1gInPc2	pár
a	a	k8xC	a
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
4	[number]	k4	4
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
možné	možný	k2eAgNnSc4d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2001-2004	[number]	k4	2001-2004
se	se	k3xPyFc4	se
početnost	početnost	k1gFnSc1	početnost
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001 14	[number]	k4	2001 14
párů	pár	k1gInPc2	pár
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
pravděpodobných	pravděpodobný	k2eAgNnPc2d1	pravděpodobné
a	a	k8xC	a
možných	možný	k2eAgNnPc2d1	možné
hnízdění	hnízdění	k1gNnPc2	hnízdění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002 18	[number]	k4	2002 18
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003 28	[number]	k4	2003 28
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004 35	[number]	k4	2004 35
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
a	a	k8xC	a
rákosiny	rákosina	k1gFnPc4	rákosina
u	u	k7c2	u
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
v	v	k7c6	v
lesnatých	lesnatý	k2eAgFnPc6d1	lesnatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
přilétá	přilétat	k5eAaImIp3nS	přilétat
již	již	k9	již
v	v	k7c6	v
párech	pár	k1gInPc6	pár
nebo	nebo	k8xC	nebo
hejnech	hejno	k1gNnPc6	hejno
o	o	k7c6	o
třech	tři	k4xCgMnPc6	tři
jedincích	jedinec	k1gMnPc6	jedinec
(	(	kIx(	(
<g/>
dospělý	dospělý	k1gMnSc1	dospělý
pár	pár	k4xCyI	pár
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jejich	jejich	k3xOp3gNnSc1	jejich
loňské	loňský	k2eAgNnSc1d1	loňské
mládě	mládě	k1gNnSc1	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
toku	tok	k1gInSc6	tok
hlasitě	hlasitě	k6eAd1	hlasitě
troubí	troubit	k5eAaImIp3nS	troubit
a	a	k8xC	a
předvádí	předvádět	k5eAaImIp3nS	předvádět
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc4d1	tvořený
úklony	úklon	k1gInPc4	úklon
a	a	k8xC	a
výskoky	výskok	k1gInPc4	výskok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
kupa	kupa	k1gFnSc1	kupa
z	z	k7c2	z
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
cm	cm	kA	cm
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
bažinatém	bažinatý	k2eAgInSc6d1	bažinatý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1x	[number]	k4	1x
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
při	při	k7c6	při
zničení	zničení	k1gNnSc6	zničení
snůšky	snůška	k1gFnSc2	snůška
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
hnízdění	hnízdění	k1gNnSc2	hnízdění
klade	klást	k5eAaImIp3nS	klást
snůšku	snůška	k1gFnSc4	snůška
náhradní	náhradní	k2eAgFnSc4d1	náhradní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
snáší	snášet	k5eAaImIp3nP	snášet
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
světle	světle	k6eAd1	světle
béžová	béžový	k2eAgFnSc1d1	béžová
<g/>
,	,	kIx,	,
olivově	olivově	k6eAd1	olivově
a	a	k8xC	a
hnědě	hnědě	k6eAd1	hnědě
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
vejce	vejce	k1gNnPc1	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
94	[number]	k4	94
x	x	k?	x
62	[number]	k4	62
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
střídavě	střídavě	k6eAd1	střídavě
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
28-31	[number]	k4	28-31
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
2	[number]	k4	2
dnů	den	k1gInPc2	den
a	a	k8xC	a
1-2	[number]	k4	1-2
dny	den	k1gInPc7	den
poté	poté	k6eAd1	poté
hnízdo	hnízdo	k1gNnSc4	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
vodí	vodit	k5eAaImIp3nP	vodit
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
hnízda	hnízdo	k1gNnSc2	hnízdo
ve	v	k7c6	v
skrytu	skryt	k1gInSc6	skryt
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
od	od	k7c2	od
stáří	stáří	k1gNnSc2	stáří
30	[number]	k4	30
dnů	den	k1gInPc2	den
je	on	k3xPp3gNnPc4	on
rodiče	rodič	k1gMnPc1	rodič
začínají	začínat	k5eAaImIp3nP	začínat
vodit	vodit	k5eAaImF	vodit
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzletnosti	vzletnost	k1gFnPc1	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
60	[number]	k4	60
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
5-6	[number]	k4	5-6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
31	[number]	k4	31
hnízdění	hnízdění	k1gNnPc2	hnízdění
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989-2000	[number]	k4	1989-2000
bylo	být	k5eAaImAgNnS	být
neúspěšných	úspěšný	k2eNgInPc2d1	neúspěšný
9	[number]	k4	9
(	(	kIx(	(
<g/>
29	[number]	k4	29
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
5	[number]	k4	5
případech	případ	k1gInPc6	případ
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
predace	predace	k1gFnSc2	predace
prasetem	prase	k1gNnSc7	prase
divokým	divoký	k2eAgNnSc7d1	divoké
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
případech	případ	k1gInPc6	případ
příčinou	příčina	k1gFnSc7	příčina
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
v	v	k7c6	v
1	[number]	k4	1
případě	případ	k1gInSc6	případ
nadměrným	nadměrný	k2eAgNnSc7d1	nadměrné
rušením	rušení	k1gNnSc7	rušení
a	a	k8xC	a
ve	v	k7c6	v
2	[number]	k4	2
případech	případ	k1gInPc6	případ
krádeží	krádež	k1gFnPc2	krádež
vajec	vejce	k1gNnPc2	vejce
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
1	[number]	k4	1
případě	případ	k1gInSc6	případ
byla	být	k5eAaImAgFnS	být
příčina	příčina	k1gFnSc1	příčina
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převažují	převažovat	k5eAaImIp3nP	převažovat
zelené	zelený	k2eAgFnPc1d1	zelená
části	část	k1gFnPc1	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
a	a	k8xC	a
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nevelkém	velký	k2eNgNnSc6d1	nevelké
množství	množství	k1gNnSc6	množství
je	být	k5eAaImIp3nS	být
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
také	také	k9	také
živočišná	živočišný	k2eAgFnSc1d1	živočišná
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc7	červ
a	a	k8xC	a
drobnými	drobný	k2eAgMnPc7d1	drobný
obratlovci	obratlovec	k1gMnPc7	obratlovec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
i	i	k8xC	i
mláďat	mládě	k1gNnPc2	mládě
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tahu	tah	k1gInSc6	tah
hledá	hledat	k5eAaImIp3nS	hledat
potravu	potrava	k1gFnSc4	potrava
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Krása	krása	k1gFnSc1	krása
jeřábů	jeřáb	k1gInPc2	jeřáb
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
námluvní	námluvní	k2eAgInPc1d1	námluvní
tance	tanec	k1gInPc1	tanec
člověka	člověk	k1gMnSc2	člověk
odedávna	odedávna	k6eAd1	odedávna
fascinovaly	fascinovat	k5eAaBmAgFnP	fascinovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgInS	být
jeřáb	jeřáb	k1gInSc1	jeřáb
přisuzován	přisuzovat	k5eAaImNgInS	přisuzovat
Apollónovi	Apollón	k1gMnSc3	Apollón
<g/>
,	,	kIx,	,
Deméter	Deméter	k1gInSc1	Deméter
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hermovi	Hermův	k2eAgMnPc1d1	Hermův
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
symbolem	symbol	k1gInSc7	symbol
bdělosti	bdělost	k1gFnSc2	bdělost
a	a	k8xC	a
opatrnosti	opatrnost	k1gFnSc2	opatrnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
ptáka	pták	k1gMnSc2	pták
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
moudrost	moudrost	k1gFnSc1	moudrost
<g/>
,	,	kIx,	,
věk	věk	k1gInSc1	věk
a	a	k8xC	a
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
jeřáb	jeřáb	k1gInSc1	jeřáb
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
dlouhověkosti	dlouhověkost	k1gFnSc2	dlouhověkost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
je	být	k5eAaImIp3nS	být
jeřáb	jeřáb	k1gInSc4	jeřáb
symbolem	symbol	k1gInSc7	symbol
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
ostražitosti	ostražitost	k1gFnSc2	ostražitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
je	být	k5eAaImIp3nS	být
jeřáb	jeřáb	k1gInSc4	jeřáb
symbolem	symbol	k1gInSc7	symbol
něčeho	něco	k3yInSc2	něco
"	"	kIx"	"
<g/>
vznešeného	vznešený	k2eAgMnSc2d1	vznešený
<g/>
"	"	kIx"	"
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2019	[number]	k4	2019
chován	chovat	k5eAaImNgInS	chovat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
110	[number]	k4	110
evropských	evropský	k2eAgFnPc6d1	Evropská
zoo	zoo	k1gFnPc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
zoo	zoo	k1gFnPc4	zoo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
</s>
</p>
<p>
<s>
Zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Sedlec	Sedlec	k1gInSc1	Sedlec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
jeřáb	jeřáb	k1gInSc4	jeřáb
popelavý	popelavý	k2eAgInSc4d1	popelavý
se	se	k3xPyFc4	se
do	do	k7c2	do
Zoo	zoo	k1gFnSc2	zoo
Praha	Praha	k1gFnSc1	Praha
dostal	dostat	k5eAaPmAgInS	dostat
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
chov	chov	k1gInSc1	chov
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dovezena	dovezen	k2eAgFnSc1d1	dovezena
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
přibyl	přibýt	k5eAaPmAgMnS	přibýt
samec	samec	k1gMnSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
chován	chovat	k5eAaImNgInS	chovat
pár	pár	k1gInSc1	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kranich	Kranicha	k1gFnPc2	Kranicha
(	(	kIx(	(
<g/>
Art	Art	k1gFnSc1	Art
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
jeřáb	jeřáb	k1gMnSc1	jeřáb
popelavý	popelavý	k2eAgMnSc1d1	popelavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jeřáb	jeřáb	k1gMnSc1	jeřáb
popelavý	popelavý	k2eAgMnSc1d1	popelavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
jeřáb	jeřáb	k1gInSc4	jeřáb
popelavý	popelavý	k2eAgInSc4d1	popelavý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Grus	Grus	k1gInSc1	Grus
grus	grus	k1gInSc1	grus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Jeřáb	jeřáb	k1gInSc1	jeřáb
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Ohrada	ohrada	k1gFnSc1	ohrada
</s>
</p>
