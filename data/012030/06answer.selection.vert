<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
Kronika	kronika	k1gFnSc1	kronika
trojánská	trojánský	k2eAgFnSc1d1	Trojánská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytištěna	vytisknout	k5eAaPmNgFnS	vytisknout
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
nejspíše	nejspíše	k9	nejspíše
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
<g/>
.	.	kIx.	.
</s>
