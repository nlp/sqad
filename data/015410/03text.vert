<s>
Duben	duben	k1gInSc1
2010	#num#	k4
</s>
<s>
<<	<<	k?
</s>
<s>
duben	duben	k1gInSc1
</s>
<s>
>>	>>	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
Út	Út	kA
</s>
<s>
St	St	kA
</s>
<s>
Čt	Čt	kA
</s>
<s>
Pá	pá	k0
</s>
<s>
So	So	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Ve	v	k7c6
věku	věk	k1gInSc6
78	#num#	k4
roků	rok	k1gInPc2
zemřel	zemřít	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Koníček	Koníček	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
choreograf	choreograf	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Z	z	k7c2
floridského	floridský	k2eAgInSc2d1
mysu	mys	k1gInSc2
Canaveral	Canaveral	k1gFnPc7
odstartoval	odstartovat	k5eAaPmAgInS
raketoplán	raketoplán	k1gInSc1
Discovery	Discovera	k1gFnSc2
se	s	k7c7
sedmi	sedm	k4xCc7
astronauty	astronaut	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
k	k	k7c3
vesmírné	vesmírný	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
ISS	ISS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmické	kosmický	k2eAgNnSc1d1
plavidlo	plavidlo	k1gNnSc1
přepravuje	přepravovat	k5eAaImIp3nS
výbavu	výbava	k1gFnSc4
<g/>
,	,	kIx,
jídlo	jídlo	k1gNnSc4
a	a	k8xC
zásoby	zásoba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Během	během	k7c2
mohutných	mohutný	k2eAgInPc2d1
útoků	útok	k1gInPc2
maoistických	maoistický	k2eAgMnPc2d1
vzbouřenců	vzbouřenec	k1gMnPc2
v	v	k7c6
Indii	Indie	k1gFnSc6
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
73	#num#	k4
příslušníků	příslušník	k1gMnPc2
indických	indický	k2eAgFnPc2d1
milicí	milice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Dmitrij	Dmitrij	k1gMnSc1
Medvěděv	Medvěděv	k1gMnSc1
přiletěl	přiletět	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
nešíření	nešíření	k1gNnSc6
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
slavnostní	slavnostní	k2eAgFnSc6d1
společné	společný	k2eAgFnSc6d1
večeři	večeře	k1gFnSc6
se	se	k3xPyFc4
přitom	přitom	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
prezidentem	prezident	k1gMnSc7
ČR	ČR	kA
Václavem	Václav	k1gMnSc7
Klausem	Klaus	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Opoziční	opoziční	k2eAgFnSc1d1
demonstrace	demonstrace	k1gFnSc1
proti	proti	k7c3
vládě	vláda	k1gFnSc3
prezidenta	prezident	k1gMnSc2
Kurmanbeka	Kurmanbeek	k1gMnSc2
Bakijeva	Bakijeva	k1gFnSc1
v	v	k7c6
mnoha	mnoho	k4c6
městech	město	k1gNnPc6
v	v	k7c6
Kyrgyzstánu	Kyrgyzstán	k1gInSc6
přerostly	přerůst	k5eAaPmAgFnP
v	v	k7c4
násilné	násilný	k2eAgInPc4d1
pouliční	pouliční	k2eAgInPc4d1
nepokoje	nepokoj	k1gInPc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
armáda	armáda	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
palbu	palba	k1gFnSc4
do	do	k7c2
demonstrantů	demonstrant	k1gMnPc2
v	v	k7c6
Biškeku	Biškek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstalci	povstalec	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
budovy	budova	k1gFnPc4
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
parlamentu	parlament	k1gInSc2
a	a	k8xC
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
ozbrojeným	ozbrojený	k2eAgInPc3d1
střetům	střet	k1gInPc3
demonstrantů	demonstrant	k1gMnPc2
s	s	k7c7
policií	policie	k1gFnSc7
a	a	k8xC
k	k	k7c3
rabování	rabování	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
vyhlášen	vyhlásit	k5eAaPmNgInS
výjimečný	výjimečný	k2eAgInSc1d1
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
hlásí	hlásit	k5eAaImIp3nP
68	#num#	k4
až	až	k9
100	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
hostila	hostit	k5eAaImAgFnS
bilaterální	bilaterální	k2eAgInSc4d1
rusko-americký	rusko-americký	k2eAgInSc4d1
summit	summit	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgNnSc6,k3yQgNnSc6,k3yRgNnSc6
prezidenti	prezident	k1gMnPc1
Barack	Baracka	k1gFnPc2
Obama	Obama	k?
a	a	k8xC
Dmitrij	Dmitrij	k1gMnPc1
Medveděv	Medveděv	k1gFnSc2
podepsali	podepsat	k5eAaPmAgMnP
dohodu	dohoda	k1gFnSc4
Novou	nový	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
START	start	k1gInSc1
o	o	k7c6
jaderném	jaderný	k2eAgNnSc6d1
odzbrojení	odzbrojení	k1gNnSc6
<g/>
,	,	kIx,
navazující	navazující	k2eAgFnSc6d1
na	na	k7c4
smlouvu	smlouva	k1gFnSc4
START	start	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vypršela	vypršet	k5eAaPmAgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
a	a	k8xC
smlouvu	smlouva	k1gFnSc4
SORT	sorta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Večer	večer	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
se	s	k7c7
středoevropskými	středoevropský	k2eAgMnPc7d1
státníky	státník	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
násilné	násilný	k2eAgInPc4d1
nepokoje	nepokoj	k1gInPc4
rezignovala	rezignovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
moci	moct	k5eAaImF
se	se	k3xPyFc4
ujala	ujmout	k5eAaPmAgFnS
dočasná	dočasný	k2eAgFnSc1d1
opoziční	opoziční	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
sociálně	sociálně	k6eAd1
demokratickou	demokratický	k2eAgFnSc7d1
vůdkyní	vůdkyně	k1gFnSc7
Rozou	Rozý	k2eAgFnSc4d1
Otunbajevovou	Otunbajevová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Kurmanbek	Kurmanbek	k1gMnSc1
Bakijev	Bakijev	k1gMnSc1
uprchl	uprchnout	k5eAaPmAgMnS
údajně	údajně	k6eAd1
do	do	k7c2
svého	svůj	k3xOyFgInSc2
domovského	domovský	k2eAgInSc2d1
regionu	region	k1gInSc2
Džalalabad	Džalalabad	k1gInSc1
na	na	k7c6
jihu	jih	k1gInSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
úkrytu	úkryt	k1gInSc2
však	však	k9
vydal	vydat	k5eAaPmAgInS
prohlášení	prohlášení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
rezignovat	rezignovat	k5eAaBmF
nehodlá	hodlat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Vládní	vládní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
Tu-	Tu-	k1gFnSc2
<g/>
154	#num#	k4
s	s	k7c7
polským	polský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Lechem	Lech	k1gMnSc7
Kaczyńským	Kaczyńský	k1gMnSc7
a	a	k8xC
doprovodem	doprovod	k1gInSc7
na	na	k7c6
palubě	paluba	k1gFnSc6
havarovalo	havarovat	k5eAaPmAgNnS
při	při	k7c6
přistání	přistání	k1gNnSc6
ve	v	k7c6
Smolensku	Smolensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřelo	zemřít	k5eAaPmAgNnS
celkem	celek	k1gInSc7
96	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
nepřežil	přežít	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
arcibiskup	arcibiskup	k1gMnSc1
pražský	pražský	k2eAgMnSc1d1
Dominik	Dominik	k1gMnSc1
Duka	Duka	k1gMnSc1
byl	být	k5eAaImAgMnS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
v	v	k7c6
chrámu	chrám	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
převzal	převzít	k5eAaPmAgInS
berlu	berla	k1gFnSc4
svatého	svatý	k2eAgMnSc2d1
Vojtěcha	Vojtěch	k1gMnSc2
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
Miloslava	Miloslav	k1gMnSc2
kardinála	kardinál	k1gMnSc2
Vlka	Vlk	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
V	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
maďarských	maďarský	k2eAgFnPc2d1
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
vyhrála	vyhrát	k5eAaPmAgFnS
s	s	k7c7
velkým	velký	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
pravicová	pravicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Fidesz	Fidesza	k1gFnPc2
Viktora	Viktor	k1gMnSc2
Orbána	Orbán	k1gMnSc2
(	(	kIx(
<g/>
52,8	52,8	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
skončila	skončit	k5eAaPmAgFnS
vládnoucí	vládnoucí	k2eAgFnSc1d1
Maďarská	maďarský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
19,3	19,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
ultrapravicová	ultrapravicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Jobbik	Jobbik	k1gMnSc1
(	(	kIx(
<g/>
16,7	16,7	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
parlamentu	parlament	k1gInSc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
i	i	k9
liberálně-ekologicky	liberálně-ekologicky	k6eAd1
zaměřená	zaměřený	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Lehet	Lehet	k1gMnSc1
Más	Más	k1gMnSc1
a	a	k8xC
Politika	politika	k1gFnSc1
(	(	kIx(
<g/>
7,4	7,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
kolo	kolo	k1gNnSc1
se	se	k3xPyFc4
uskuteční	uskutečnit	k5eAaPmIp3nS
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Země	země	k1gFnSc1
eurozóny	eurozóna	k1gFnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Mezinárodním	mezinárodní	k2eAgInSc7d1
měnovým	měnový	k2eAgInSc7d1
fondem	fond	k1gInSc7
nabídly	nabídnout	k5eAaPmAgFnP
Řecku	Řecko	k1gNnSc6
nouzové	nouzový	k2eAgInPc4d1
úvěry	úvěr	k1gInPc4
v	v	k7c6
objemu	objem	k1gInSc6
až	až	k9
30	#num#	k4
mld.	mld.	k?
eur	euro	k1gNnPc2
(	(	kIx(
<g/>
757	#num#	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
)	)	kIx)
s	s	k7c7
úrokovou	úrokový	k2eAgFnSc7d1
sazbou	sazba	k1gFnSc7
u	u	k7c2
tříletých	tříletý	k2eAgInPc2d1
úvěrů	úvěr	k1gInPc2
kolem	kolem	k7c2
5	#num#	k4
%	%	kIx~
na	na	k7c4
zmírnění	zmírnění	k1gNnSc4
dopadů	dopad	k1gInPc2
finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Čínskou	čínský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
Čching-chaj	Čching-chaj	k1gFnSc4
a	a	k8xC
Tibet	Tibet	k1gInSc4
zasáhlo	zasáhnout	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
o	o	k7c6
síle	síla	k1gFnSc6
6,9	6,9	k4
stupňů	stupeň	k1gInPc2
Richterovy	Richterův	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyžádalo	vyžádat	k5eAaPmAgNnS
si	se	k3xPyFc3
nejméně	málo	k6eAd3
617	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
9000	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Rut	rout	k5eAaImNgInS
Bízková	Bízková	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
novou	nový	k2eAgFnSc7d1
ministryní	ministryně	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
v	v	k7c6
kabinetu	kabinet	k1gInSc6
Jana	Jan	k1gMnSc2
Fischera	Fischer	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Několikaměsíční	několikaměsíční	k2eAgFnSc1d1
vulkanická	vulkanický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
islandské	islandský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
Eyjafjallajökull	Eyjafjallajökulla	k1gFnPc2
vyústila	vyústit	k5eAaPmAgFnS
ve	v	k7c6
vyvrhnutí	vyvrhnutí	k1gNnSc6
oblaku	oblak	k1gInSc2
popela	popel	k1gInSc2
a	a	k8xC
dýmu	dým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
kvůli	kvůli	k7c3
meteorologickým	meteorologický	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
směřuje	směřovat	k5eAaImIp3nS
ke	k	k7c3
kontinentální	kontinentální	k2eAgFnSc3d1
Evropě	Evropa	k1gFnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
uzavření	uzavření	k1gNnSc2
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
severních	severní	k2eAgFnPc2d1
částí	část	k1gFnPc2
Norska	Norsko	k1gNnSc2
<g/>
,	,	kIx,
Finska	Finsko	k1gNnSc2
<g/>
,	,	kIx,
Švédska	Švédsko	k1gNnSc2
a	a	k8xC
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
V	v	k7c6
Římě	Řím	k1gInSc6
zemřel	zemřít	k5eAaPmAgMnS
kardinál	kardinál	k1gMnSc1
Špidlík	Špidlík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
oblaku	oblak	k1gInSc3
prachu	prach	k1gInSc2
šířícímu	šířící	k2eAgInSc3d1
se	se	k3xPyFc4
z	z	k7c2
Islandu	Island	k1gInSc2
byl	být	k5eAaImAgInS
ve	v	k7c6
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
uzavřen	uzavřen	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
prostor	prostor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
byla	být	k5eAaImAgFnS
pouze	pouze	k6eAd1
letiště	letiště	k1gNnSc4
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
Ostravě	Ostrava	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
až	až	k6eAd1
okolo	okolo	k7c2
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařízení	nařízení	k1gNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
dopravních	dopravní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c4
rekreační	rekreační	k2eAgNnSc4d1
létání	létání	k1gNnSc4
se	s	k7c7
(	(	kIx(
<g/>
zatím	zatím	k6eAd1
<g/>
)	)	kIx)
nevztahuje	vztahovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníky	vrtulník	k1gInPc7
letecké	letecký	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
nelétají	létat	k5eNaImIp3nP
od	od	k7c2
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
V	v	k7c6
bolivijském	bolivijský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Cochabamba	Cochabamba	k1gFnSc1
začala	začít	k5eAaPmAgFnS
Konference	konference	k1gFnSc1
národů	národ	k1gInPc2
světa	svět	k1gInSc2
o	o	k7c6
změnách	změna	k1gFnPc6
klimatu	klima	k1gNnSc2
a	a	k8xC
právech	právo	k1gNnPc6
Matky	matka	k1gFnPc1
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Evo	Eva	k1gFnSc5
Morales	Morales	k1gMnSc1
ji	on	k3xPp3gFnSc4
svolal	svolat	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
alternativu	alternativa	k1gFnSc4
ke	k	k7c3
klimatické	klimatický	k2eAgFnSc3d1
konferenci	konference	k1gFnSc3
OSN	OSN	kA
v	v	k7c6
Kodani	Kodaň	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
nezohlednila	zohlednit	k5eNaPmAgFnS
hlas	hlas	k1gInSc4
chudších	chudý	k2eAgInPc2d2
národů	národ	k1gInPc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Předsedou	předseda	k1gMnSc7
České	český	k2eAgFnSc2d1
biskupské	biskupský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
pražský	pražský	k2eAgMnSc1d1
Dominik	Dominik	k1gMnSc1
Duka	Duka	k1gMnSc1
<g/>
,	,	kIx,
místopředsedou	místopředseda	k1gMnSc7
pak	pak	k8xC
arcibiskup	arcibiskup	k1gMnSc1
olomoucký	olomoucký	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Graubner	Graubner	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
věku	věk	k1gInSc6
89	#num#	k4
let	léto	k1gNnPc2
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
barcelonské	barcelonský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
MOV	MOV	kA
Juan	Juan	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Samaranch	Samaranch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Ve	v	k7c6
věku	věk	k1gInSc6
49	#num#	k4
let	léto	k1gNnPc2
zemřel	zemřít	k5eAaPmAgMnS
spisovatel	spisovatel	k1gMnSc1
Jan	Jan	k1gMnSc1
Balabán	Balabán	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
V	v	k7c6
nedělních	nedělní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
hlavy	hlava	k1gFnSc2
státu	stát	k1gInSc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Heinz	Heinz	k1gMnSc1
Fischer	Fischer	k1gMnSc1
a	a	k8xC
bude	být	k5eAaImBp3nS
na	na	k7c4
příštích	příští	k2eAgNnPc2d1
6	#num#	k4
let	léto	k1gNnPc2
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
funkci	funkce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgFnSc1d1
účast	účast	k1gFnSc1
byla	být	k5eAaImAgFnS
rekordně	rekordně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
-	-	kIx~
přibližně	přibližně	k6eAd1
49	#num#	k4
%	%	kIx~
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Také	také	k9
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
kole	kolo	k1gNnSc6
maďarských	maďarský	k2eAgFnPc2d1
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
uhájila	uhájit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
převahu	převaha	k1gFnSc4
pravicová	pravicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Fidesz	Fidesza	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
parlamentu	parlament	k1gInSc6
dokonce	dokonce	k9
ústavní	ústavní	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druzí	druhý	k4xOgMnPc1
skončili	skončit	k5eAaPmAgMnP
dosud	dosud	k6eAd1
vládnoucí	vládnoucí	k2eAgMnPc1d1
socialisté	socialist	k1gMnPc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
ultrapravicová	ultrapravicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Jobbik	Jobbik	k1gInSc1
<g/>
,	,	kIx,
čtvrtá	čtvrtá	k1gFnSc1
byla	být	k5eAaImAgFnS
strana	strana	k1gFnSc1
Lehet	Lehet	k1gMnSc1
Más	Más	k1gMnSc1
a	a	k8xC
Politika	politika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Vítězem	vítěz	k1gMnSc7
ankety	anketa	k1gFnSc2
Ropák	ropák	k1gMnSc1
roku	rok	k1gInSc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
vyhlášen	vyhlášen	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
Vladimír	Vladimír	k1gMnSc1
Tošovský	Tošovský	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
čínské	čínský	k2eAgFnSc6d1
Šanghaji	Šanghaj	k1gFnSc6
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
světová	světový	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
Expo	Expo	k1gNnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavuje	vystavovat	k5eAaImIp3nS
zde	zde	k6eAd1
189	#num#	k4
zemí	zem	k1gFnPc2
a	a	k8xC
57	#num#	k4
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInPc1
výběžky	výběžek	k1gInPc1
ropné	ropný	k2eAgFnSc2d1
skvrny	skvrna	k1gFnSc2
v	v	k7c6
Mexickém	mexický	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
dosáhly	dosáhnout	k5eAaPmAgInP
pobřeží	pobřeží	k1gNnSc4
amerického	americký	k2eAgInSc2d1
státu	stát	k1gInSc2
Louisiana	Louisiana	k1gFnSc1
v	v	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Mississippi	Mississippi	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ropa	ropa	k1gFnSc1
vytéká	vytékat	k5eAaImIp3nS
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
po	po	k7c6
výbuchu	výbuch	k1gInSc6
těžební	těžební	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
Deepwater	Deepwater	k1gMnSc1
Horizon	Horizon	k1gMnSc1
v	v	k7c6
množství	množství	k1gNnSc6
5000	#num#	k4
barelů	barel	k1gInPc2
denně	denně	k6eAd1
z	z	k7c2
havarovaného	havarovaný	k2eAgInSc2d1
podmořského	podmořský	k2eAgInSc2d1
vrtu	vrt	k1gInSc2
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
tak	tak	k6eAd1
patrně	patrně	k6eAd1
největší	veliký	k2eAgFnSc1d3
ekologická	ekologický	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
v	v	k7c6
historii	historie	k1gFnSc6
ropné	ropný	k2eAgFnSc2d1
těžby	těžba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuality	aktualita	k1gFnSc2
roku	rok	k1gInSc2
2010	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
•	•	k?
únor	únor	k1gInSc1
•	•	k?
březen	březen	k1gInSc1
•	•	k?
duben	duben	k1gInSc1
•	•	k?
květen	květen	k1gInSc4
•	•	k?
červen	červen	k1gInSc1
•	•	k?
červenec	červenec	k1gInSc1
•	•	k?
srpen	srpen	k1gInSc1
•	•	k?
září	září	k1gNnSc2
•	•	k?
říjen	říjen	k1gInSc1
•	•	k?
listopad	listopad	k1gInSc1
•	•	k?
prosinec	prosinec	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Koníček	Koníček	k1gMnSc1
-	-	kIx~
kytarista	kytarista	k1gMnSc1
ze	z	k7c2
Starců	stařec	k1gMnPc2
na	na	k7c6
chmelu	chmel	k1gInSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
zdroj	zdroj	k1gInSc4
<g/>
]	]	kIx)
–	–	k?
České	český	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
↑	↑	k?
http://aktualne.centrum.cz/veda/clanek.phtml?id=665126	http://aktualne.centrum.cz/veda/clanek.phtml?id=665126	k4
<g/>
↑	↑	k?
http://www.novinky.cz/zahranicni/svet/196774-tisic-maoistu-zmasakrovalo-v-indii-sedmdesat-vojaku.html	http://www.novinky.cz/zahranicni/svet/196774-tisic-maoistu-zmasakrovalo-v-indii-sedmdesat-vojaku.htmla	k1gFnPc2
<g/>
↑	↑	k?
http://www.novinky.cz/domaci/196944-on-line-rusky-prezident-medvedev-je-v-praze.html	http://www.novinky.cz/domaci/196944-on-line-rusky-prezident-medvedev-je-v-praze.htmla	k1gFnPc2
<g/>
↑	↑	k?
Demonstranti	demonstrant	k1gMnPc1
v	v	k7c6
Kyrgyzstánu	Kyrgyzstán	k1gInSc6
prý	prý	k9
ubili	ubít	k5eAaPmAgMnP
ministra	ministr	k1gMnSc4
<g/>
,	,	kIx,
vláda	vláda	k1gFnSc1
předala	předat	k5eAaPmAgFnS
moc	moc	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
opozici	opozice	k1gFnSc6
<g/>
↑	↑	k?
http://www.ct24.cz/domaci/86341-prazsky-summit-vrcholi-obama-s-medvedevem-podepisou-novy-start/,	http://www.ct24.cz/domaci/86341-prazsky-summit-vrcholi-obama-s-medvedevem-podepisou-novy-start/,	k4
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
8.4	8.4	k4
<g/>
.2010	.2010	k4
<g/>
↑	↑	k?
Kyrgyzové	Kyrgyz	k1gMnPc1
vyhnali	vyhnat	k5eAaPmAgMnP
prezidenta	prezident	k1gMnSc4
a	a	k8xC
změnili	změnit	k5eAaPmAgMnP
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
sousedé	soused	k1gMnPc1
raději	rád	k6eAd2
uzavřeli	uzavřít	k5eAaPmAgMnP
hranice	hranice	k1gFnPc4
<g/>
↑	↑	k?
Letadlo	letadlo	k1gNnSc1
s	s	k7c7
polským	polský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Kaczyńským	Kaczyńský	k1gMnSc7
havarovalo	havarovat	k5eAaPmAgNnS
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
nepřežil	přežít	k5eNaPmAgMnS
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
↑	↑	k?
С	С	k?
с	с	k?
п	п	k?
П	П	k?
р	р	k?
в	в	k?
С	С	k?
<g />
.	.	kIx.
</s>
<s hack="1">
о	о	k?
<g/>
,	,	kIx,
Vesti	Vesť	k1gFnPc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
↑	↑	k?
Arcibiskup	arcibiskup	k1gMnSc1
Dominik	Dominik	k1gMnSc1
Duka	Duka	k1gMnSc1
dostal	dostat	k5eAaPmAgInS
berlu	berla	k1gFnSc4
svatého	svatý	k2eAgMnSc2d1
Vojtěcha	Vojtěch	k1gMnSc2
<g/>
,	,	kIx,
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
otočilo	otočit	k5eAaPmAgNnS
ostře	ostro	k6eAd1
doprava	doprava	k6eAd1
<g/>
↑	↑	k?
Země	zem	k1gFnSc2
eurozóny	eurozóna	k1gFnSc2
Řecku	Řecko	k1gNnSc6
nabídly	nabídnout	k5eAaPmAgInP
úvěry	úvěr	k1gInPc1
za	za	k7c4
30	#num#	k4
miliard	miliarda	k4xCgFnPc2
eur	euro	k1gNnPc2
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Idnes	Idnes	k1gInSc1
<g/>
:	:	kIx,
Zemětřesení	zemětřesení	k1gNnSc2
si	se	k3xPyFc3
v	v	k7c6
Číně	Čína	k1gFnSc6
vyžádalo	vyžádat	k5eAaPmAgNnS
přes	přes	k7c4
600	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
je	on	k3xPp3gMnPc4
vytahují	vytahovat	k5eAaImIp3nP
holýma	holý	k2eAgFnPc7d1
rukama	ruka	k1gFnPc7
<g/>
↑	↑	k?
Bízková	Bízková	k1gFnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ministryní	ministryně	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
už	už	k9
šestou	šestý	k4xOgFnSc7
od	od	k7c2
voleb	volba	k1gFnPc2
<g/>
↑	↑	k?
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Popel	popel	k1gInSc1
z	z	k7c2
islandské	islandský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
zavřel	zavřít	k5eAaPmAgMnS
letiště	letiště	k1gNnSc4
v	v	k7c6
Británii	Británie	k1gFnSc6
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Skandinávii	Skandinávie	k1gFnSc6
<g/>
↑	↑	k?
http://aktualne.centrum.cz/domaci/spolecnost/clanek.phtml?id=666094	http://aktualne.centrum.cz/domaci/spolecnost/clanek.phtml?id=666094	k4
<g/>
↑	↑	k?
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Kvůli	kvůli	k7c3
popelu	popel	k1gInSc3
nelétají	létat	k5eNaImIp3nP
záchranáři	záchranář	k1gMnPc1
<g/>
,	,	kIx,
ultralighty	ultralight	k1gInPc1
mohou	moct	k5eAaImIp3nP
<g/>
↑	↑	k?
http://www.denikreferendum.cz/clanek/3037-v-bolivii-zacala-klimaticka-konference-narodu-reakce-na-zpackanou-kodan	http://www.denikreferendum.cz/clanek/3037-v-bolivii-zacala-klimaticka-konference-narodu-reakce-na-zpackanou-kodan	k1gInSc1
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Balabán	Balabán	k1gMnSc1
<g/>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
spisovatel	spisovatel	k1gMnSc1
J.	J.	kA
Balabán	Balabán	k1gMnSc1
<g/>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
ostravský	ostravský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Jan	Jan	k1gMnSc1
Balabán	Balabán	k1gMnSc1
<g/>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
spisovatel	spisovatel	k1gMnSc1
Jan	Jan	k1gMnSc1
Balabán	Balabán	k1gMnSc1
<g/>
↑	↑	k?
http://www.novinky.cz/zahranicni/evropa/198535-v-cele-rakouska-stane-opet-heinz-fischer.html	http://www.novinky.cz/zahranicni/evropa/198535-v-cele-rakouska-stane-opet-heinz-fischer.html	k1gMnSc1
<g/>
↑	↑	k?
Pravicový	pravicový	k2eAgMnSc1d1
Fidesz	Fidesz	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
ústavní	ústavní	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
↑	↑	k?
http://www.ceskenoviny.cz/zpravy/ropaka-i-zelenou-perlu-ovladly-tezebni-limity/470437	http://www.ceskenoviny.cz/zpravy/ropaka-i-zelenou-perlu-ovladly-tezebni-limity/470437	k4
Ropáka	ropák	k1gMnSc2
i	i	k9
Zelenou	Zelená	k1gFnSc7
perlu	perl	k1gInSc2
ovládly	ovládnout	k5eAaPmAgInP
těžební	těžební	k2eAgInPc1d1
limity	limit	k1gInPc1
<g/>
↑	↑	k?
http://www.novinky.cz/zahranicni/svet/199047-sanghajske-expo-2010-zacalo-velkolepou-show.html	http://www.novinky.cz/zahranicni/svet/199047-sanghajske-expo-2010-zacalo-velkolepou-show.html	k1gInSc1
<g/>
↑	↑	k?
http://www.novinky.cz/zahranicni/amerika/198996-ropna-skvrna-uz-je-u-pobrezi-usa-ohrozuje-deltu-mississippi.html	http://www.novinky.cz/zahranicni/amerika/198996-ropna-skvrna-uz-je-u-pobrezi-usa-ohrozuje-deltu-mississippi.html	k1gInSc1
</s>
