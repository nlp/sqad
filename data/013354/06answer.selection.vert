<s>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgFnPc2d1	lasicovitá
šelem	šelma	k1gFnPc2	šelma
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Aonyx	Aonyx	k1gInSc1	Aonyx
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
náleží	náležet	k5eAaImIp3nS	náležet
ještě	ještě	k6eAd1	ještě
středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
vydra	vydra	k1gFnSc1	vydra
konžská	konžský	k2eAgFnSc1d1	Konžská
(	(	kIx(	(
<g/>
Aonyx	Aonyx	k1gInSc1	Aonyx
congica	congic	k1gInSc2	congic
<g/>
)	)	kIx)	)
a	a	k8xC	a
asijská	asijský	k2eAgFnSc1d1	asijská
vydra	vydra	k1gFnSc1	vydra
malá	malý	k2eAgFnSc1d1	malá
(	(	kIx(	(
<g/>
Aonyx	Aonyx	k1gInSc1	Aonyx
cinerea	cinere	k1gInSc2	cinere
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
