<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
(	(	kIx(	(
<g/>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gInSc1	capensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
vydry	vydra	k1gFnSc2	vydra
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
mohutné	mohutný	k2eAgNnSc1d1	mohutné
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
svými	svůj	k3xOyFgFnPc7	svůj
až	až	k9	až
21	[number]	k4	21
kg	kg	kA	kg
váhy	váha	k1gFnSc2	váha
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
známou	známý	k2eAgFnSc7d1	známá
sladkovodní	sladkovodní	k2eAgFnSc7d1	sladkovodní
vydrou	vydra	k1gFnSc7	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
tlapy	tlapa	k1gFnPc1	tlapa
postrádají	postrádat	k5eAaImIp3nP	postrádat
drápy	dráp	k1gInPc4	dráp
a	a	k8xC	a
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
páru	pár	k1gInSc6	pár
nohou	noha	k1gFnPc2	noha
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
částečné	částečný	k2eAgFnPc1d1	částečná
plovací	plovací	k2eAgFnPc1d1	plovací
blány	blána	k1gFnPc1	blána
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
především	především	k6eAd1	především
kraby	krab	k1gInPc7	krab
a	a	k8xC	a
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
norem	norma	k1gFnPc2	norma
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomické	taxonomický	k2eAgNnSc4d1	taxonomické
zařazení	zařazení	k1gNnSc4	zařazení
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgFnPc2d1	lasicovitá
šelem	šelma	k1gFnPc2	šelma
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Aonyx	Aonyx	k1gInSc1	Aonyx
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
náleží	náležet	k5eAaImIp3nS	náležet
ještě	ještě	k6eAd1	ještě
středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
vydra	vydra	k1gFnSc1	vydra
konžská	konžský	k2eAgFnSc1d1	Konžská
(	(	kIx(	(
<g/>
Aonyx	Aonyx	k1gInSc1	Aonyx
congica	congic	k1gInSc2	congic
<g/>
)	)	kIx)	)
a	a	k8xC	a
asijská	asijský	k2eAgFnSc1d1	asijská
vydra	vydra	k1gFnSc1	vydra
malá	malý	k2eAgFnSc1d1	malá
(	(	kIx(	(
<g/>
Aonyx	Aonyx	k1gInSc1	Aonyx
cinerea	cinere	k1gInSc2	cinere
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
však	však	k9	však
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
a	a	k8xC	a
vydra	vydra	k1gFnSc1	vydra
asijská	asijský	k2eAgFnSc1d1	asijská
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Amblonyx	Amblonyx	k1gInSc1	Amblonyx
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
capensis	capensis	k1gFnSc2	capensis
(	(	kIx(	(
<g/>
Schinz	Schinz	k1gMnSc1	Schinz
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
hindei	hinde	k1gFnSc2	hinde
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
meneleki	menelek	k1gFnSc2	menelek
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gInSc1	capensis
congica	congica	k1gFnSc1	congica
(	(	kIx(	(
<g/>
vydra	vydra	k1gFnSc1	vydra
konžská	konžský	k2eAgFnSc1d1	Konžská
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Lönnberg	Lönnberg	k1gMnSc1	Lönnberg
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
microdon	microdona	k1gFnPc2	microdona
(	(	kIx(	(
<g/>
vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
malozubá	malozubý	k2eAgFnSc1d1	malozubý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Pohle	Pohl	k1gMnSc5	Pohl
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
philippsi	philippse	k1gFnSc4	philippse
(	(	kIx(	(
<g/>
vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
tmavouchá	tmavouchat	k5eAaPmIp3nS	tmavouchat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Hinton	Hinton	k1gInSc1	Hinton
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
žijící	žijící	k2eAgFnSc7d1	žijící
vydrou	vydra	k1gFnSc7	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
a	a	k8xC	a
vydra	vydra	k1gFnSc1	vydra
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
mohutné	mohutný	k2eAgNnSc1d1	mohutné
se	se	k3xPyFc4	se
silným	silný	k2eAgInSc7d1	silný
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
1,4	[number]	k4	1,4
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
1,2	[number]	k4	1,2
<g/>
-	-	kIx~	-
<g/>
1,3	[number]	k4	1,3
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
nejčastěji	často	k6eAd3	často
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
mezi	mezi	k7c4	mezi
12-18	[number]	k4	12-18
kg	kg	kA	kg
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
masivní	masivní	k2eAgInSc1d1	masivní
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
velkým	velký	k2eAgInSc7d1	velký
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
znaky	znak	k1gInPc4	znak
patří	patřit	k5eAaImIp3nS	patřit
jejich	jejich	k3xOp3gFnPc4	jejich
tlapy	tlapa	k1gFnPc4	tlapa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
drápů	dráp	k1gInPc2	dráp
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
2	[number]	k4	2
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
prstu	prst	k1gInSc2	prst
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
plovacími	plovací	k2eAgFnPc7d1	plovací
blanami	blána	k1gFnPc7	blána
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Kožešina	kožešina	k1gFnSc1	kožešina
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
a	a	k8xC	a
lesklá	lesklý	k2eAgFnSc1d1	lesklá
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
(	(	kIx(	(
<g/>
krémová	krémový	k2eAgNnPc1d1	krémové
až	až	k6eAd1	až
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
)	)	kIx)	)
náprsenkou	náprsenka	k1gFnSc7	náprsenka
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
mívá	mívat	k5eAaImIp3nS	mívat
různé	různý	k2eAgInPc4d1	různý
odstíny	odstín	k1gInPc4	odstín
hnědé	hnědý	k2eAgInPc4d1	hnědý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Africké	africký	k2eAgFnPc1d1	africká
vydry	vydra	k1gFnPc1	vydra
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kolem	kolem	k7c2	kolem
trvalých	trvalý	k2eAgFnPc2d1	trvalá
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
říčních	říční	k2eAgInPc2d1	říční
systémů	systém	k1gInPc2	systém
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k9	až
po	po	k7c4	po
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
3	[number]	k4	3
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
udávají	udávat	k5eAaImIp3nP	udávat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Kalahari	Kalahar	k1gFnSc6	Kalahar
a	a	k8xC	a
v	v	k7c6	v
Namibské	Namibský	k2eAgFnSc6d1	Namibská
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
Preferují	preferovat	k5eAaImIp3nP	preferovat
mělčí	mělký	k2eAgFnSc4d2	mělčí
sladkou	sladký	k2eAgFnSc4d1	sladká
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgFnPc1	některý
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
při	při	k7c6	při
mořském	mořský	k2eAgNnSc6d1	mořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
ústím	ústit	k5eAaImIp1nS	ústit
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
v	v	k7c6	v
mangrovových	mangrovový	k2eAgInPc6d1	mangrovový
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chování	chování	k1gNnSc1	chování
===	===	k?	===
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
obvykle	obvykle	k6eAd1	obvykle
žije	žít	k5eAaImIp3nS	žít
osamoceně	osamoceně	k6eAd1	osamoceně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
rodinných	rodinný	k2eAgFnPc6d1	rodinná
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
je	být	k5eAaImIp3nS	být
především	především	k9	především
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
během	během	k7c2	během
dne	den	k1gInSc2	den
obvykle	obvykle	k6eAd1	obvykle
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
v	v	k7c6	v
doupěti	doupě	k1gNnSc6	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
jejího	její	k3xOp3gNnSc2	její
teritoria	teritorium	k1gNnSc2	teritorium
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
1	[number]	k4	1
000	[number]	k4	000
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
si	se	k3xPyFc3	se
označuje	označovat	k5eAaImIp3nS	označovat
trusem	trus	k1gInSc7	trus
a	a	k8xC	a
pachovými	pachový	k2eAgFnPc7d1	pachová
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
jedinci	jedinec	k1gMnPc7	jedinec
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
pestrou	pestrý	k2eAgFnSc7d1	pestrá
škálou	škála	k1gFnSc7	škála
zvuků	zvuk	k1gInPc2	zvuk
od	od	k7c2	od
hvízdání	hvízdání	k1gNnSc2	hvízdání
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vrčení	vrčení	k1gNnSc4	vrčení
<g/>
,	,	kIx,	,
kňučení	kňučení	k1gNnSc4	kňučení
a	a	k8xC	a
sténání	sténání	k1gNnSc4	sténání
<g/>
,	,	kIx,	,
po	po	k7c6	po
"	"	kIx"	"
<g/>
hahání	hahání	k1gNnSc6	hahání
<g/>
"	"	kIx"	"
a	a	k8xC	a
mňoukání	mňoukání	k1gNnSc1	mňoukání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
na	na	k7c4	na
chytání	chytání	k1gNnSc4	chytání
a	a	k8xC	a
požírání	požírání	k1gNnSc4	požírání
vodních	vodní	k2eAgMnPc2d1	vodní
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
tlapy	tlapa	k1gFnPc1	tlapa
sice	sice	k8xC	sice
nemají	mít	k5eNaImIp3nP	mít
drápy	dráp	k1gInPc4	dráp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
hrubé	hrubý	k2eAgFnSc3d1	hrubá
kůži	kůže	k1gFnSc3	kůže
dokáží	dokázat	k5eAaPmIp3nP	dokázat
udržet	udržet	k5eAaPmF	udržet
kluzkou	kluzký	k2eAgFnSc4d1	kluzká
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
a	a	k8xC	a
rozlousknutí	rozlousknutí	k1gNnSc3	rozlousknutí
krabích	krabí	k2eAgFnPc2d1	krabí
schránek	schránka	k1gFnPc2	schránka
a	a	k8xC	a
rybích	rybí	k2eAgNnPc2d1	rybí
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
ponoru	ponor	k1gInSc2	ponor
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
je	být	k5eAaImIp3nS	být
6-49	[number]	k4	6-49
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
jí	on	k3xPp3gFnSc7	on
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
kormidlo	kormidlo	k1gNnSc4	kormidlo
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnPc4d1	zadní
nohy	noha	k1gFnPc4	noha
jako	jako	k8xC	jako
pádla	pádlo	k1gNnPc4	pádlo
<g/>
.	.	kIx.	.
</s>
<s>
Kraby	krab	k1gMnPc4	krab
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
pomalejší	pomalý	k2eAgMnPc4d2	pomalejší
tvory	tvor	k1gMnPc4	tvor
chytá	chytat	k5eAaImIp3nS	chytat
předními	přední	k2eAgFnPc7d1	přední
prackami	pracka	k1gFnPc7	pracka
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
většinou	většina	k1gFnSc7	většina
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
tlamy	tlama	k1gFnSc2	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Sladkovodní	sladkovodní	k2eAgMnPc1d1	sladkovodní
krabi	krab	k1gMnPc1	krab
tvoří	tvořit	k5eAaImIp3nP	tvořit
42-65	[number]	k4	42-65
%	%	kIx~	%
potravy	potrava	k1gFnPc1	potrava
této	tento	k3xDgFnSc2	tento
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
žáby	žába	k1gFnPc4	žába
a	a	k8xC	a
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slaných	slaný	k2eAgFnPc6d1	slaná
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převažují	převažovat	k5eAaImIp3nP	převažovat
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgFnPc4d1	následovaná
kraby	krab	k1gMnPc7	krab
<g/>
,	,	kIx,	,
humry	humr	k1gMnPc7	humr
a	a	k8xC	a
chobotnicemi	chobotnice	k1gFnPc7	chobotnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
napadá	napadat	k5eAaBmIp3nS	napadat
i	i	k9	i
vodní	vodní	k2eAgNnSc1d1	vodní
ptactvo	ptactvo	k1gNnSc1	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
živočichů	živočich	k1gMnPc2	živočich
si	se	k3xPyFc3	se
nevšímá	všímat	k5eNaImIp3nS	všímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
striktní	striktní	k2eAgNnSc4d1	striktní
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
63	[number]	k4	63
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
je	být	k5eAaImIp3nS	být
1-3	[number]	k4	1-3
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
16-30	[number]	k4	16-30
dní	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
doupěti	doupě	k1gNnSc6	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
měsících	měsíc	k1gInPc6	měsíc
jsou	být	k5eAaImIp3nP	být
odstavena	odstaven	k2eAgNnPc1d1	odstaveno
<g/>
.	.	kIx.	.
</s>
<s>
Osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
1	[number]	k4	1
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zároveň	zároveň	k6eAd1	zároveň
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
10-12	[number]	k4	10-12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predace	Predace	k1gFnPc1	Predace
<g/>
,	,	kIx,	,
paraziti	parazit	k1gMnPc1	parazit
===	===	k?	===
</s>
</p>
<p>
<s>
Vydry	vydra	k1gFnPc1	vydra
africké	africký	k2eAgFnPc1d1	africká
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
kořistí	kořist	k1gFnSc7	kořist
krokodýlů	krokodýl	k1gMnPc2	krokodýl
nilských	nilský	k2eAgMnPc2d1	nilský
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgMnPc4d1	mladý
jedince	jedinec	k1gMnPc4	jedinec
občas	občas	k6eAd1	občas
chytne	chytnout	k5eAaPmIp3nS	chytnout
orel	orel	k1gMnSc1	orel
jasnohlasý	jasnohlasý	k2eAgMnSc1d1	jasnohlasý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc4d1	další
přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
usmrtit	usmrtit	k5eAaPmF	usmrtit
nějaká	nějaký	k3yIgFnSc1	nějaký
větší	veliký	k2eAgFnSc1d2	veliký
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
šelma	šelma	k1gFnSc1	šelma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
lev	lev	k1gMnSc1	lev
či	či	k8xC	či
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
<g/>
Vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
je	být	k5eAaImIp3nS	být
hostitelem	hostitel	k1gMnSc7	hostitel
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
endoparazitů	endoparazit	k1gMnPc2	endoparazit
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Baschkirovitrema	Baschkirovitrema	k1gNnSc4	Baschkirovitrema
incrassatum	incrassatum	k1gNnSc1	incrassatum
<g/>
,	,	kIx,	,
Clinostomum	Clinostomum	k1gNnSc1	Clinostomum
pyriforme	pyriform	k1gInSc5	pyriform
<g/>
,	,	kIx,	,
Prudhoella	Prudhoella	k1gFnSc1	Prudhoella
rhodesiensis	rhodesiensis	k1gFnSc1	rhodesiensis
<g/>
,	,	kIx,	,
Cloeoascaris	Cloeoascaris	k1gFnSc1	Cloeoascaris
spinicollis	spinicollis	k1gFnSc1	spinicollis
<g/>
.	.	kIx.	.
</s>
<s>
Ektoparazité	ektoparazit	k1gMnPc1	ektoparazit
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
nebyly	být	k5eNaImAgFnP	být
doposud	doposud	k6eAd1	doposud
zaznamenáni	zaznamenat	k5eAaPmNgMnP	zaznamenat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrozby	hrozba	k1gFnSc2	hrozba
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
pro	pro	k7c4	pro
vydru	vydra	k1gFnSc4	vydra
africkou	africký	k2eAgFnSc4d1	africká
je	být	k5eAaImIp3nS	být
zhoršování	zhoršování	k1gNnSc3	zhoršování
kvality	kvalita	k1gFnSc2	kvalita
vodních	vodní	k2eAgInPc2d1	vodní
ekosystémů	ekosystém	k1gInPc2	ekosystém
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc1	jejich
úplná	úplný	k2eAgFnSc1d1	úplná
ztráta	ztráta	k1gFnSc1	ztráta
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
meliorací	meliorace	k1gFnPc2	meliorace
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
lidských	lidský	k2eAgFnPc2d1	lidská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
je	on	k3xPp3gFnPc4	on
také	také	k9	také
loví	lovit	k5eAaImIp3nP	lovit
kvůli	kvůli	k7c3	kvůli
kožešině	kožešina	k1gFnSc3	kožešina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
škůdce	škůdce	k1gMnPc4	škůdce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
stavy	stav	k1gInPc4	stav
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
vodní	vodní	k2eAgFnSc4d1	vodní
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LARIVIÈ	LARIVIÈ	k?	LARIVIÈ
<g/>
,	,	kIx,	,
Serge	Serge	k1gFnSc1	Serge
<g/>
.	.	kIx.	.
</s>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
<g/>
.	.	kIx.	.
</s>
<s>
Mammalian	Mammalian	k1gInSc1	Mammalian
Species	species	k1gFnSc2	species
No	no	k9	no
<g/>
.	.	kIx.	.
671	[number]	k4	671
<g/>
.	.	kIx.	.
</s>
<s>
Published	Published	k1gInSc1	Published
5	[number]	k4	5
June	jun	k1gMnSc5	jun
2001	[number]	k4	2001
by	by	kYmCp3nS	by
the	the	k?	the
American	American	k1gMnSc1	American
Society	societa	k1gFnSc2	societa
of	of	k?	of
Mammalogists	Mammalogists	k1gInSc1	Mammalogists
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vydra	vydra	k1gFnSc1	vydra
africká	africký	k2eAgFnSc1d1	africká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
JACQUES	Jacques	k1gMnSc1	Jacques
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
REED-SMITH	REED-SMITH	k1gFnSc1	REED-SMITH
<g/>
,	,	kIx,	,
J.	J.	kA	J.
&	&	k?	&
SOMERS	SOMERS	kA	SOMERS
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
J.	J.	kA	J.
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gFnSc2	capensis
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
IUCN	IUCN	kA	IUCN
Red	Red	k1gFnSc2	Red
List	list	k1gInSc1	list
of	of	k?	of
Threatened	Threatened	k1gInSc1	Threatened
Species	species	k1gFnSc1	species
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
KOWALSKY	KOWALSKY	kA	KOWALSKY
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
.	.	kIx.	.
</s>
<s>
Aonyx	Aonyx	k1gInSc1	Aonyx
capensis	capensis	k1gInSc1	capensis
(	(	kIx(	(
<g/>
African	African	k1gInSc1	African
clawless	clawless	k1gInSc1	clawless
otter	otter	k1gInSc4	otter
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc1	Diversit
Web	web	k1gInSc1	web
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
