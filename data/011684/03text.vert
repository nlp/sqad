<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
červená	červená	k1gFnSc1	červená
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
muscaria	muscarium	k1gNnSc2	muscarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
houba	houba	k1gFnSc1	houba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
muchomůrkovitých	muchomůrkovitý	k2eAgMnPc2d1	muchomůrkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
jedovatým	jedovatý	k2eAgFnPc3d1	jedovatá
houbám	houba	k1gFnPc3	houba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
fatální	fatální	k2eAgFnPc1d1	fatální
otravy	otrava	k1gFnPc1	otrava
jsou	být	k5eAaImIp3nP	být
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
průměr	průměr	k1gInSc1	průměr
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejdříve	dříve	k6eAd3	dříve
polokulovitý	polokulovitý	k2eAgInSc1d1	polokulovitý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
sklenutý	sklenutý	k2eAgInSc1d1	sklenutý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
rozložený	rozložený	k2eAgInSc4d1	rozložený
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
mírně	mírně	k6eAd1	mírně
miskovitý	miskovitý	k2eAgInSc1d1	miskovitý
s	s	k7c7	s
hřebenitým	hřebenitý	k2eAgNnSc7d1	hřebenité
rýhováním	rýhování	k1gNnSc7	rýhování
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
klobouku	klobouk	k1gInSc2	klobouk
může	moct	k5eAaImIp3nS	moct
kolísat	kolísat	k5eAaImF	kolísat
mezi	mezi	k7c7	mezi
jasně	jasně	k6eAd1	jasně
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
až	až	k8xS	až
nachově	nachově	k6eAd1	nachově
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
bílými	bílý	k2eAgFnPc7d1	bílá
bradavkami	bradavka	k1gFnPc7	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Lupeny	lupen	k1gInPc1	lupen
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
u	u	k7c2	u
třeně	třeň	k1gInSc2	třeň
volné	volný	k2eAgNnSc1d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Třeň	třeň	k1gInSc1	třeň
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
<g/>
;	;	kIx,	;
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hlízovitě	hlízovitě	k6eAd1	hlízovitě
ztlustlý	ztlustlý	k2eAgMnSc1d1	ztlustlý
<g/>
,	,	kIx,	,
obalený	obalený	k2eAgMnSc1d1	obalený
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
bradavičnatou	bradavičnatý	k2eAgFnSc7d1	bradavičnatá
pochvou	pochva	k1gFnSc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Prsten	prsten	k1gInSc1	prsten
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
převislý	převislý	k2eAgInSc1d1	převislý
<g/>
,	,	kIx,	,
rýhovaný	rýhovaný	k2eAgInSc1d1	rýhovaný
<g/>
.	.	kIx.	.
</s>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnPc1d1	jemná
chuti	chuť	k1gFnPc1	chuť
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
pachu	pach	k1gInSc2	pach
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusný	výtrusný	k2eAgInSc1d1	výtrusný
prach	prach	k1gInSc1	prach
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možnost	možnost	k1gFnSc1	možnost
záměny	záměna	k1gFnSc2	záměna
===	===	k?	===
</s>
</p>
<p>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
muchomůrkou	muchomůrkou	k?	muchomůrkou
královskou	královský	k2eAgFnSc4d1	královská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
možná	možná	k9	možná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
muchomůrkou	muchomůrkou	k?	muchomůrkou
císařskou	císařský	k2eAgFnSc4d1	císařská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
jedlá	jedlý	k2eAgFnSc1d1	jedlá
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
výtečná	výtečný	k2eAgFnSc1d1	výtečná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
chráněná	chráněný	k2eAgFnSc1d1	chráněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
hojná	hojný	k2eAgFnSc1d1	hojná
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
velmi	velmi	k6eAd1	velmi
hojná	hojný	k2eAgFnSc1d1	hojná
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
smrkových	smrkový	k2eAgInPc6d1	smrkový
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
a	a	k8xC	a
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
jedovatost	jedovatost	k1gFnSc1	jedovatost
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
nepříliš	příliš	k6eNd1	příliš
významného	významný	k2eAgInSc2d1	významný
obsahu	obsah	k1gInSc2	obsah
muskarinu	muskarin	k1gInSc2	muskarin
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
muscimol	muscimol	k1gInSc1	muscimol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
poruchy	porucha	k1gFnPc4	porucha
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
červená	červená	k1gFnSc1	červená
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
používána	používán	k2eAgFnSc1d1	používána
k	k	k7c3	k
navození	navození	k1gNnSc3	navození
stavů	stav	k1gInPc2	stav
opojení	opojení	k1gNnPc2	opojení
nebo	nebo	k8xC	nebo
halucinací	halucinace	k1gFnPc2	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
užívání	užívání	k1gNnSc1	užívání
muchomůrky	muchomůrky	k?	muchomůrky
červené	červené	k1gNnSc1	červené
narušuje	narušovat	k5eAaImIp3nS	narušovat
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
ve	v	k7c6	v
vzácných	vzácný	k2eAgInPc6d1	vzácný
případech	případ	k1gInPc6	případ
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
muchomůrka	muchomůrka	k?	muchomůrka
červená	červená	k1gFnSc1	červená
dala	dát	k5eAaPmAgFnS	dát
celému	celý	k2eAgInSc3d1	celý
rodu	rod	k1gInSc3	rod
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
lidovým	lidový	k2eAgInSc7d1	lidový
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
much	moucha	k1gFnPc2	moucha
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
muchomorka	muchomorka	k1gFnSc1	muchomorka
–	–	k?	–
od	od	k7c2	od
"	"	kIx"	"
<g/>
mořit	mořit	k5eAaImF	mořit
mouchy	moucha	k1gFnPc4	moucha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
se	se	k3xPyFc4	se
vymáčel	vymáčet	k5eAaPmAgInS	vymáčet
v	v	k7c6	v
oslazené	oslazený	k2eAgFnSc6d1	oslazená
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
oslazeném	oslazený	k2eAgNnSc6d1	oslazené
mléce	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
namáčel	namáčet	k5eAaImAgMnS	namáčet
v	v	k7c6	v
mléce	mléko	k1gNnSc6	mléko
či	či	k8xC	či
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
pak	pak	k6eAd1	pak
posypal	posypat	k5eAaPmAgInS	posypat
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Mouchy	moucha	k1gFnPc1	moucha
sály	sát	k5eAaImAgFnP	sát
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
klobouku	klobouk	k1gInSc2	klobouk
sladký	sladký	k2eAgInSc4d1	sladký
roztok	roztok	k1gInSc4	roztok
i	i	k9	i
s	s	k7c7	s
rozpuštěnými	rozpuštěný	k2eAgInPc7d1	rozpuštěný
jedy	jed	k1gInPc7	jed
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
omámení	omámení	k1gNnSc3	omámení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
úplnému	úplný	k2eAgNnSc3d1	úplné
usmrcení	usmrcení	k1gNnSc3	usmrcení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KEIZER	KEIZER	kA	KEIZER
<g/>
,	,	kIx,	,
Gerrit	Gerrit	k1gInSc1	Gerrit
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rebo	Rebo	k1gMnSc1	Rebo
Productios	Productios	k1gMnSc1	Productios
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Garnweidner	Garnweidner	k1gMnSc1	Garnweidner
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc5	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
muchomůrka	muchomůrka	k?	muchomůrka
červená	červenat	k5eAaImIp3nS	červenat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
