<s>
Elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc4
elektrického	elektrický	k2eAgInSc2d1
náboje	náboj	k1gInSc2
ve	v	k7c6
vodiči	vodič	k1gInSc6
s	s	k7c7
jednotkovým	jednotkový	k2eAgInSc7d1
elektrickým	elektrický	k2eAgInSc7d1
potenciálem	potenciál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
vodiče	vodič	k1gInSc2
uchovat	uchovat	k5eAaPmF
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
větší	veliký	k2eAgFnSc1d2
kapacita	kapacita	k1gFnSc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
větší	veliký	k2eAgNnSc1d2
množství	množství	k1gNnSc1
náboje	náboj	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
na	na	k7c6
vodiči	vodič	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
obecně	obecně	k6eAd1
vlastností	vlastnost	k1gFnSc7
každého	každý	k3xTgMnSc2
vodiče	vodič	k1gMnSc2
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
v	v	k7c6
kondenzátoru	kondenzátor	k1gInSc6
<g/>
,	,	kIx,
pro	pro	k7c4
nějž	jenž	k3xRgInSc4
je	být	k5eAaImIp3nS
kapacita	kapacita	k1gFnSc1
definována	definovat	k5eAaBmNgFnS
jako	jako	k8xC,k8xS
množství	množství	k1gNnSc1
náboje	náboj	k1gInSc2
na	na	k7c6
deskách	deska	k1gFnPc6
kondenzátoru	kondenzátor	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
mezi	mezi	k7c7
deskami	deska	k1gFnPc7
jednotkové	jednotkový	k2eAgNnSc4d1
elektrické	elektrický	k2eAgNnSc4d1
napětí	napětí	k1gNnSc4
(	(	kIx(
<g/>
1	#num#	k4
V	V	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Značení	značení	k1gNnSc1
</s>
<s>
Značka	značka	k1gFnSc1
veličiny	veličina	k1gFnSc2
<g/>
:	:	kIx,
C	C	kA
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	si	kA
<g/>
:	:	kIx,
farad	farad	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
F	F	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
Z	z	k7c2
praktického	praktický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
farad	farad	k1gInSc4
příliš	příliš	k6eAd1
velká	velký	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
běžně	běžně	k6eAd1
používají	používat	k5eAaImIp3nP
dílčí	dílčí	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
především	především	k6eAd1
</s>
<s>
milifarad	milifarad	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
mF	mF	k?
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
F	F	kA
</s>
<s>
mikrofarad	mikrofarad	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
μ	μ	k?
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
F	F	kA
</s>
<s>
nanofarad	nanofarad	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
nF	nF	k?
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
F	F	kA
</s>
<s>
pikofarad	pikofarad	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
pF	pf	kA
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
12	#num#	k4
F	F	kA
</s>
<s>
V	v	k7c6
elektronice	elektronika	k1gFnSc6
se	se	k3xPyFc4
kapacita	kapacita	k1gFnSc1
kondenzátorů	kondenzátor	k1gInPc2
někdy	někdy	k6eAd1
udává	udávat	k5eAaImIp3nS
v	v	k7c6
pikofaradech	pikofarad	k1gInPc6
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
např.	např.	kA
3	#num#	k4
<g/>
k	k	k7c3
<g/>
3	#num#	k4
=	=	kIx~
3300	#num#	k4
pF	pf	kA
=	=	kIx~
3,3	3,3	k4
nF	nF	k?
nebo	nebo	k8xC
10M	10M	k4
=	=	kIx~
10	#num#	k4
μ	μ	k?
<g/>
.	.	kIx.
</s>
<s>
Výpočet	výpočet	k1gInSc1
</s>
<s>
Izolované	izolovaný	k2eAgNnSc1d1
vodivé	vodivý	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
s	s	k7c7
nábojem	náboj	k1gInSc7
</s>
<s>
Q	Q	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q	Q	kA
<g/>
}	}	kIx)
</s>
<s>
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
okolí	okolí	k1gNnSc6
potenciál	potenciál	k1gInSc4
</s>
<s>
φ	φ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varphi	k1gNnPc3
}	}	kIx)
</s>
<s>
Pokud	pokud	k8xS
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
změně	změna	k1gFnSc3
náboje	náboj	k1gInSc2
tělesa	těleso	k1gNnSc2
na	na	k7c4
</s>
<s>
Q	Q	kA
</s>
<s>
′	′	k?
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
Q	Q	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q	Q	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
=	=	kIx~
<g/>
kQ	kQ	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
konstanta	konstanta	k1gFnSc1
<g/>
,	,	kIx,
změní	změnit	k5eAaPmIp3nS
se	se	k3xPyFc4
také	také	k9
potenciál	potenciál	k1gInSc1
na	na	k7c4
</s>
<s>
φ	φ	k?
</s>
<s>
′	′	k?
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
φ	φ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
=	=	kIx~
<g/>
k	k	k7c3
<g/>
\	\	kIx~
<g/>
varphi	varph	k1gFnSc3
}	}	kIx)
</s>
<s>
Bude	být	k5eAaImBp3nS
tedy	tedy	k9
platit	platit	k5eAaImF
</s>
<s>
Q	Q	kA
</s>
<s>
′	′	k?
</s>
<s>
φ	φ	k?
</s>
<s>
′	′	k?
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
Q	Q	kA
</s>
<s>
φ	φ	k?
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
konst	konst	k1gFnSc1
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
Q	Q	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
)	)	kIx)
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
Q	Q	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
)	)	kIx)
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mbox	mbox	k1gInSc1
<g/>
{	{	kIx(
<g/>
konst	konst	k1gInSc1
<g/>
}}}	}}}	k?
</s>
<s>
Poměr	poměr	k1gInSc1
velikosti	velikost	k1gFnSc2
náboje	náboj	k1gInSc2
tělesa	těleso	k1gNnSc2
a	a	k8xC
hodnoty	hodnota	k1gFnSc2
potenciálu	potenciál	k1gInSc2
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
bodě	bod	k1gInSc6
tedy	tedy	k9
závisí	záviset	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
geometrickém	geometrický	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
tělesa	těleso	k1gNnSc2
a	a	k8xC
daného	daný	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
hodnota	hodnota	k1gFnSc1
potenciálu	potenciál	k1gInSc2
na	na	k7c6
povrchu	povrch	k1gInSc6
tělesa	těleso	k1gNnSc2
s	s	k7c7
nábojem	náboj	k1gInSc7
</s>
<s>
Q	Q	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q	Q	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
pak	pak	k6eAd1
platí	platit	k5eAaImIp3nS
</s>
<s>
C	C	kA
</s>
<s>
=	=	kIx~
</s>
<s>
Q	Q	kA
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
Q	Q	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
tvaru	tvar	k1gInSc6
a	a	k8xC
velikosti	velikost	k1gFnSc6
tělesa	těleso	k1gNnSc2
a	a	k8xC
na	na	k7c6
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
osamoceného	osamocený	k2eAgNnSc2d1
vodivého	vodivý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
vyjadřuje	vyjadřovat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
tohoto	tento	k3xDgNnSc2
tělesa	těleso	k1gNnSc2
shromažďovat	shromažďovat	k5eAaImF
elektrický	elektrický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těleso	těleso	k1gNnSc1
s	s	k7c7
menší	malý	k2eAgFnSc7d2
kapacitou	kapacita	k1gFnSc7
bude	být	k5eAaImBp3nS
daným	daný	k2eAgInSc7d1
nábojem	náboj	k1gInSc7
přivedeno	přivést	k5eAaPmNgNnS
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
potenciál	potenciál	k1gInSc4
než	než	k8xS
těleso	těleso	k1gNnSc4
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
kapacitou	kapacita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Potenciálové	potenciálový	k2eAgInPc4d1
<g/>
,	,	kIx,
kapacitní	kapacitní	k2eAgInPc4d1
a	a	k8xC
influenční	influenční	k2eAgInPc4d1
koeficienty	koeficient	k1gInPc4
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP
dvě	dva	k4xCgNnPc1
vodivá	vodivý	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedno	jeden	k4xCgNnSc1
je	být	k5eAaImIp3nS
nabité	nabitý	k2eAgNnSc1d1
s	s	k7c7
nábojem	náboj	k1gInSc7
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
druhé	druhý	k4xOgNnSc1
je	být	k5eAaImIp3nS
nenabité	nabitý	k2eNgNnSc1d1
<g/>
,	,	kIx,
tzn.	tzn.	kA
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Pokud	pokud	k8xS
by	by	k9
první	první	k4xOgNnSc1
těleso	těleso	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
prostoru	prostor	k1gInSc6
samo	sám	k3xTgNnSc1
<g/>
,	,	kIx,
potom	potom	k8xC
by	by	kYmCp3nS
platilo	platit	k5eAaImAgNnS
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
C	C	kA
</s>
<s>
01	#num#	k4
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
C_	C_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
C	C	kA
</s>
<s>
01	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
kapacita	kapacita	k1gFnSc1
a	a	k8xC
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
potenciál	potenciál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nyní	nyní	k6eAd1
druhé	druhý	k4xOgFnPc4
<g/>
,	,	kIx,
původně	původně	k6eAd1
nenabité	nabitý	k2eNgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
umístíme	umístit	k5eAaPmIp1nP
v	v	k7c6
dosahu	dosah	k1gInSc6
působení	působení	k1gNnSc2
elektrických	elektrický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
prvního	první	k4xOgNnSc2
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
druhém	druhý	k4xOgNnSc6
tělese	těleso	k1gNnSc6
objeví	objevit	k5eAaPmIp3nS
indukovaný	indukovaný	k2eAgInSc1d1
náboj	náboj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
povrchu	povrch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
ovšem	ovšem	k9
zpětně	zpětně	k6eAd1
vliv	vliv	k1gInSc4
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
náboje	náboj	k1gInSc2
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
na	na	k7c6
povrchu	povrch	k1gInSc6
prvního	první	k4xOgNnSc2
tělesa	těleso	k1gNnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
zachován	zachovat	k5eAaPmNgInS
konstantní	konstantní	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
obou	dva	k4xCgNnPc2
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojde	dojít	k5eAaPmIp3nS
tak	tak	k6eAd1
ke	k	k7c3
změně	změna	k1gFnSc3
potenciálů	potenciál	k1gInPc2
obou	dva	k4xCgNnPc2
těles	těleso	k1gNnPc2
na	na	k7c6
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Jestliže	jestliže	k8xS
na	na	k7c6
prvním	první	k4xOgNnSc6
tělese	těleso	k1gNnSc6
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
změně	změna	k1gFnSc3
náboje	náboj	k1gInSc2
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
na	na	k7c4
hodnotu	hodnota	k1gFnSc4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
′	′	k?
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
=	=	kIx~
<g/>
kQ_	kQ_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
získáme	získat	k5eAaPmIp1nP
na	na	k7c6
tělesech	těleso	k1gNnPc6
potenciály	potenciál	k1gInPc4
</s>
<s>
k	k	k7c3
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc3
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
k	k	k7c3
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc3
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
danému	daný	k2eAgNnSc3d1
rozložení	rozložení	k1gNnSc3
náboje	náboj	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
určitý	určitý	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
existovat	existovat	k5eAaImF
určité	určitý	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
charakterizují	charakterizovat	k5eAaBmIp3nP
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
potenciály	potenciál	k1gInPc7
a	a	k8xC
nábojem	náboj	k1gInSc7
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
přičemž	přičemž	k6eAd1
tyto	tento	k3xDgFnPc1
konstanty	konstanta	k1gFnPc1
jsou	být	k5eAaImIp3nP
závislé	závislý	k2eAgFnPc1d1
pouze	pouze	k6eAd1
na	na	k7c6
geometrickém	geometrický	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
tedy	tedy	k9
psát	psát	k5eAaImF
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
11	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
21	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
B	B	kA
</s>
<s>
11	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
B	B	kA
</s>
<s>
21	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
jsou	být	k5eAaImIp3nP
konstanty	konstanta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Použijeme	použít	k5eAaPmIp1nP
<g/>
-li	-li	k?
stejnou	stejný	k2eAgFnSc4d1
úvahu	úvaha	k1gFnSc4
pro	pro	k7c4
případ	případ	k1gInSc4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
dostaneme	dostat	k5eAaPmIp1nP
obdobné	obdobný	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
popisují	popisovat	k5eAaImIp3nP
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
nábojem	náboj	k1gInSc7
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
potenciály	potenciál	k1gInPc4
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
tedy	tedy	k9
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
12	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
22	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
22	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Superpozicí	superpozice	k1gFnSc7
předchozích	předchozí	k2eAgInPc2d1
případů	případ	k1gInPc2
dostaneme	dostat	k5eAaPmIp1nP
zobecnění	zobecnění	k1gNnSc4
pro	pro	k7c4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
tzn.	tzn.	kA
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
01	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
11	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
B	B	kA
</s>
<s>
12	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc1
_	_	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
φ	φ	k?
</s>
<s>
02	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
21	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
B	B	kA
</s>
<s>
22	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
varphi	varphi	k1gNnSc1
_	_	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
22	#num#	k4
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Pro	pro	k7c4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
těles	těleso	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
-té	-té	k?
těleso	těleso	k1gNnSc1
má	mít	k5eAaImIp3nS
náboj	náboj	k1gInSc4
</s>
<s>
Q	Q	kA
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gFnSc7
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}}	}}	k?
</s>
<s>
lze	lze	k6eAd1
postupným	postupný	k2eAgNnSc7d1
opakováním	opakování	k1gNnSc7
předchozího	předchozí	k2eAgInSc2d1
postupu	postup	k1gInSc2
získat	získat	k5eAaPmF
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
Q	Q	kA
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
i	i	k8xC
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc2
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
i	i	k8xC
<g/>
}}	}}	k?
</s>
<s>
označuje	označovat	k5eAaImIp3nS
potenciál	potenciál	k1gInSc4
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
-tého	-téze	k6eAd1
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koeficienty	koeficient	k1gInPc1
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k9
potenciálové	potenciálový	k2eAgInPc4d1
koeficienty	koeficient	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
koeficienty	koeficient	k1gInPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgInP
rozměry	rozměr	k1gInPc1
<g/>
,	,	kIx,
tvarem	tvar	k1gInSc7
a	a	k8xC
vzájemnými	vzájemný	k2eAgFnPc7d1
polohami	poloha	k1gFnPc7
všech	všecek	k3xTgFnPc2
vodivých	vodivý	k2eAgFnPc2d1
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Lze	lze	k6eAd1
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
potenciálové	potenciálový	k2eAgInPc1d1
koeficienty	koeficient	k1gInPc1
splňují	splňovat	k5eAaImIp3nP
vztahy	vztah	k1gInPc4
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
B	B	kA
</s>
<s>
j	j	k?
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
ji	on	k3xPp3gFnSc4
<g/>
}}	}}	k?
</s>
<s>
tedy	tedy	k9
matice	matice	k1gFnSc1
koeficientů	koeficient	k1gInPc2
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
symetrická	symetrický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zápis	zápis	k1gInSc1
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
Q	Q	kA
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
varphi	varph	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
i	i	k8xC
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc2
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}	}}	k?
</s>
<s>
představuje	představovat	k5eAaImIp3nS
soustavu	soustava	k1gFnSc4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
lineárních	lineární	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
neznámých	známý	k2eNgMnPc2d1
</s>
<s>
Q	Q	kA
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gFnSc7
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}}	}}	k?
</s>
<s>
Tato	tento	k3xDgFnSc1
soustava	soustava	k1gFnSc1
má	mít	k5eAaImIp3nS
právě	právě	k9
jedno	jeden	k4xCgNnSc4
řešení	řešení	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
determinant	determinant	k1gInSc4
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}}	}}	k?
</s>
<s>
nenulový	nulový	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řešení	řešení	k1gNnSc1
této	tento	k3xDgFnSc2
soustavy	soustava	k1gFnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
možné	možný	k2eAgNnSc1d1
zapsat	zapsat	k5eAaPmF
jako	jako	k9
</s>
<s>
Q	Q	kA
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
φ	φ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gFnSc7
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
k	k	k7c3
<g/>
}}	}}	k?
</s>
<s>
Diagonální	diagonální	k2eAgInPc1d1
prvky	prvek	k1gInPc1
matice	matice	k1gFnSc2
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
tzn.	tzn.	kA
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ii	ii	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k9
kapacitní	kapacitní	k2eAgInPc4d1
koeficienty	koeficient	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
nediagonální	diagonální	k2eNgInPc1d1
prvky	prvek	k1gInPc1
matice	matice	k1gFnSc2
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
tzn.	tzn.	kA
prvky	prvek	k1gInPc7
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
pro	pro	k7c4
</s>
<s>
i	i	k9
</s>
<s>
≠	≠	k?
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
\	\	kIx~
<g/>
neq	neq	k?
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
influenčními	influenční	k2eAgInPc7d1
koeficienty	koeficient	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
matice	matice	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
symetrická	symetrický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kapacita	kapacita	k1gFnSc1
a	a	k8xC
koeficienty	koeficient	k1gInPc1
</s>
<s>
Kapacitní	kapacitní	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ii	ii	k?
<g/>
}}	}}	k?
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
-tého	-téze	k6eAd1
vodivého	vodivý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
je	být	k5eAaImIp3nS
odlišný	odlišný	k2eAgInSc1d1
od	od	k7c2
kapacity	kapacita	k1gFnSc2
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
stejného	stejný	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
osamocené	osamocený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
osamoceném	osamocený	k2eAgNnSc6d1
vodivém	vodivý	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
vyvolá	vyvolat	k5eAaPmIp3nS
kladný	kladný	k2eAgInSc1d1
náboj	náboj	k1gInSc1
kladný	kladný	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
a	a	k8xC
záporný	záporný	k2eAgInSc4d1
náboj	náboj	k1gInSc4
záporný	záporný	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
dalších	další	k2eAgNnPc2d1
vodivých	vodivý	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
pokles	pokles	k1gInSc1
potenciálu	potenciál	k1gInSc2
na	na	k7c6
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
-tém	-tém	k6eAd1
vodiči	vodič	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
>	>	kIx)
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ii	ii	k?
<g/>
}	}	kIx)
<g/>
>	>	kIx)
<g/>
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
Nemůže	moct	k5eNaImIp3nS
přitom	přitom	k6eAd1
dojít	dojít	k5eAaPmF
ke	k	k7c3
změně	změna	k1gFnSc3
znaménka	znaménko	k1gNnSc2
potenciálu	potenciál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
vliv	vliv	k1gInSc1
okolních	okolní	k2eAgInPc2d1
vodičů	vodič	k1gInPc2
bude	být	k5eAaImBp3nS
snižovat	snižovat	k5eAaImF
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
se	se	k3xPyFc4
hodnoty	hodnota	k1gFnPc4
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ii	ii	k?
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
k	k	k7c3
sobě	se	k3xPyFc3
blížit	blížit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
kladně	kladně	k6eAd1
nabité	nabitý	k2eAgNnSc1d1
vodivé	vodivý	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
indukuje	indukovat	k5eAaBmIp3nS
na	na	k7c6
bližší	blízký	k2eAgFnSc6d2
straně	strana	k1gFnSc6
druhého	druhý	k4xOgNnSc2
vodivého	vodivý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
záporný	záporný	k2eAgInSc4d1
náboj	náboj	k1gInSc4
lze	lze	k6eAd1
odvodit	odvodit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
influenční	influenční	k2eAgInPc4d1
koeficienty	koeficient	k1gInPc4
vždy	vždy	k6eAd1
platí	platit	k5eAaImIp3nP
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
<	<	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
<	<	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
</s>
<s>
i	i	k9
</s>
<s>
≠	≠	k?
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
\	\	kIx~
<g/>
neq	neq	k?
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
snižuje	snižovat	k5eAaImIp3nS
vliv	vliv	k1gInSc1
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
-tého	-téze	k6eAd1
vodiče	vodič	k1gInPc1
na	na	k7c4
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
-tý	-tý	k?
<g/>
,	,	kIx,
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
influenční	influenční	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
</s>
<s>
C	C	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C_	C_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
k	k	k7c3
nule	nula	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Influenční	Influenční	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
vodiči	vodič	k1gInPc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
zcela	zcela	k6eAd1
obklopuje	obklopovat	k5eAaImIp3nS
druhý	druhý	k4xOgMnSc1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
roven	roven	k2eAgInSc1d1
kapacitě	kapacita	k1gFnSc3
vnitřního	vnitřní	k2eAgInSc2d1
vodiče	vodič	k1gInSc2
s	s	k7c7
opačným	opačný	k2eAgNnSc7d1
znaménkem	znaménko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
je	být	k5eAaImIp3nS
významné	významný	k2eAgNnSc1d1
pro	pro	k7c4
konstrukci	konstrukce	k1gFnSc4
kondenzátorů	kondenzátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NEČÁSEK	NEČÁSEK	kA
<g/>
,	,	kIx,
Sláva	Sláva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiotechnika	radiotechnika	k1gFnSc1
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Základní	základní	k2eAgInPc1d1
elektrotechnické	elektrotechnický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
náboj	náboj	k1gInSc1
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
Kondenzátor	kondenzátor	k1gInSc1
</s>
<s>
Měřič	měřič	k1gInSc1
kapacity	kapacita	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4163236-9	4163236-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
3349	#num#	k4
</s>
