<s>
Savana	savana	k1gFnSc1	savana
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
travnaté	travnatý	k2eAgFnPc4d1	travnatá
oblasti	oblast	k1gFnPc4	oblast
tropických	tropický	k2eAgMnPc2d1	tropický
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc2d1	subtropická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vyhraněné	vyhraněný	k2eAgNnSc1d1	vyhraněné
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
a	a	k8xC	a
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobných	podobný	k2eAgFnPc6d1	podobná
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
opadavé	opadavý	k2eAgInPc4d1	opadavý
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vegetační	vegetační	k2eAgInSc1d1	vegetační
typ	typ	k1gInSc1	typ
převládne	převládnout	k5eAaPmIp3nS	převládnout
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
klima	klima	k1gNnSc1	klima
a	a	k8xC	a
půdní	půdní	k2eAgFnPc1d1	půdní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Traviny	travina	k1gFnPc1	travina
i	i	k8xC	i
dřeviny	dřevina	k1gFnPc1	dřevina
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc4d1	odlišný
ekologické	ekologický	k2eAgInPc4d1	ekologický
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
potlačují	potlačovat	k5eAaImIp3nP	potlačovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
podstatu	podstata	k1gFnSc4	podstata
savany	savana	k1gFnSc2	savana
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vlhké	vlhký	k2eAgFnSc2d1	vlhká
savany	savana	k1gFnSc2	savana
suché	suchý	k2eAgFnSc2d1	suchá
(	(	kIx(	(
<g/>
africké	africký	k2eAgFnSc2d1	africká
<g/>
)	)	kIx)	)
savany	savana	k1gFnSc2	savana
trnité	trnitý	k2eAgFnSc2d1	trnitá
savany	savana	k1gFnSc2	savana
zaplavované	zaplavovaný	k2eAgFnSc2d1	zaplavovaná
savany	savana	k1gFnSc2	savana
Savany	savana	k1gFnSc2	savana
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
opakovaného	opakovaný	k2eAgNnSc2d1	opakované
kácení	kácení	k1gNnSc2	kácení
a	a	k8xC	a
vypalování	vypalování	k1gNnSc2	vypalování
tropických	tropický	k2eAgInPc2d1	tropický
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgFnSc7d1	převládající
vegetační	vegetační	k2eAgFnSc7d1	vegetační
formací	formace	k1gFnSc7	formace
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
metry	metr	k1gMnPc7	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
traviny	travina	k1gFnSc2	travina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Vlhké	vlhký	k2eAgFnPc1d1	vlhká
savany	savana	k1gFnPc1	savana
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
guinejské	guinejský	k2eAgFnSc2d1	Guinejská
savanové	savanový	k2eAgFnSc2d1	savanová
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
až	až	k9	až
do	do	k7c2	do
jižního	jižní	k2eAgInSc2d1	jižní
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vlhké	vlhký	k2eAgFnPc1d1	vlhká
savany	savana	k1gFnPc1	savana
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
orinocké	orinocký	k2eAgInPc4d1	orinocký
llanos	llanos	k1gInPc4	llanos
<g/>
,	,	kIx,	,
brazilské	brazilský	k2eAgNnSc1d1	brazilské
campos	campos	k1gInSc4	campos
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
guayanské	guayanský	k2eAgFnSc2d1	Guayanská
náhorní	náhorní	k2eAgFnSc2d1	náhorní
savany	savana	k1gFnSc2	savana
<g/>
.	.	kIx.	.
</s>
<s>
Orinocké	orinocký	k2eAgInPc1d1	orinocký
llanos	llanos	k1gInPc1	llanos
(	(	kIx(	(
<g/>
šp	šp	k?	šp
<g/>
.	.	kIx.	.
llano	llano	k1gNnSc1	llano
=	=	kIx~	=
rovina	rovina	k1gFnSc1	rovina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
edaficky	edaficky	k6eAd1	edaficky
podmíněnou	podmíněný	k2eAgFnSc7d1	podmíněná
savanou	savana	k1gFnSc7	savana
<g/>
.	.	kIx.	.
</s>
<s>
Llanos	Llanos	k1gInPc2	Llanos
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
asi	asi	k9	asi
50	[number]	k4	50
cm	cm	kA	cm
vysokými	vysoký	k2eAgFnPc7d1	vysoká
travinami	travina	k1gFnPc7	travina
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
nazývaných	nazývaný	k2eAgInPc6d1	nazývaný
"	"	kIx"	"
<g/>
matas	matasa	k1gFnPc2	matasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brazilské	brazilský	k2eAgNnSc1d1	brazilské
campos	campos	k1gInSc4	campos
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
v	v	k7c6	v
Brazilské	brazilský	k2eAgFnSc6d1	brazilská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Campos	Campos	k1gMnSc1	Campos
cerrados	cerrados	k1gMnSc1	cerrados
představují	představovat	k5eAaImIp3nP	představovat
jakýsi	jakýsi	k3yIgInSc4	jakýsi
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
lesem	les	k1gInSc7	les
a	a	k8xC	a
savanou	savana	k1gFnSc7	savana
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
nazvat	nazvat	k5eAaBmF	nazvat
savanovým	savanův	k2eAgFnPc3d1	savanův
řídkolesím	řídkolese	k1gFnPc3	řídkolese
<g/>
.	.	kIx.	.
</s>
<s>
Campos	Campos	k1gMnSc1	Campos
abiertos	abiertos	k1gMnSc1	abiertos
představují	představovat	k5eAaImIp3nP	představovat
savanovou	savanová	k1gFnSc4	savanová
formaci	formace	k1gFnSc4	formace
bez	bez	k7c2	bez
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
požárů	požár	k1gInPc2	požár
v	v	k7c4	v
campos	campos	k1gInSc4	campos
cerrados	cerradosa	k1gFnPc2	cerradosa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
guayanské	guayanský	k2eAgFnSc2d1	Guayanská
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
edaficky	edaficky	k6eAd1	edaficky
podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
savana	savana	k1gFnSc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1500	[number]	k4	1500
až	až	k9	až
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
se	se	k3xPyFc4	se
z	z	k7c2	z
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
;	;	kIx,	;
především	především	k9	především
v	v	k7c6	v
súdánské	súdánský	k2eAgFnSc6d1	súdánská
zóně	zóna	k1gFnSc6	zóna
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
podstatu	podstata	k1gFnSc4	podstata
tvoří	tvořit	k5eAaImIp3nS	tvořit
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
metry	metr	k1gMnPc7	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
traviny	travina	k1gFnSc2	travina
a	a	k8xC	a
roztroušeně	roztroušeně	k6eAd1	roztroušeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc4	výška
maximálně	maximálně	k6eAd1	maximálně
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
baobab	baobab	k1gInSc1	baobab
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgFnPc1d1	africká
suché	suchý	k2eAgFnPc1d1	suchá
savany	savana	k1gFnPc1	savana
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
pastvu	pastva	k1gFnSc4	pastva
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Savany	savana	k1gFnPc1	savana
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
se	se	k3xPyFc4	se
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
afrických	africký	k2eAgInPc2d1	africký
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgInPc1d1	tamní
blahovičníky	blahovičník	k1gInPc1	blahovičník
se	se	k3xPyFc4	se
s	s	k7c7	s
travinami	travina	k1gFnPc7	travina
dobře	dobře	k6eAd1	dobře
snášejí	snášet	k5eAaImIp3nP	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
prostory	prostora	k1gFnPc4	prostora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Austrálie	Austrálie	k1gFnSc2	Austrálie
v	v	k7c6	v
Arnhemské	Arnhemský	k2eAgFnSc6d1	Arnhemská
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Queenslandu	Queensland	k1gInSc2	Queensland
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
travnaté	travnatý	k2eAgFnPc1d1	travnatá
pláně	pláň	k1gFnPc1	pláň
řazené	řazený	k2eAgFnPc1d1	řazená
rovněž	rovněž	k9	rovněž
mezi	mezi	k7c4	mezi
tropické	tropický	k2eAgFnPc4d1	tropická
suché	suchý	k2eAgFnPc4d1	suchá
savany	savana	k1gFnPc4	savana
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
oblastech	oblast	k1gFnPc6	oblast
střídavě	střídavě	k6eAd1	střídavě
vlhkých	vlhký	k2eAgMnPc2d1	vlhký
tropů	trop	k1gMnPc2	trop
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jsou	být	k5eAaImIp3nP	být
trnité	trnitý	k2eAgFnPc1d1	trnitá
savany	savana	k1gFnPc1	savana
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
v	v	k7c6	v
sahelské	sahelský	k2eAgFnSc6d1	sahelská
zóně	zóna	k1gFnSc6	zóna
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
.	.	kIx.	.
</s>
<s>
Dřeviny	dřevina	k1gFnPc1	dřevina
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
trnité	trnitý	k2eAgFnSc6d1	trnitá
savaně	savana	k1gFnSc6	savana
zcela	zcela	k6eAd1	zcela
chybět	chybět	k5eAaImF	chybět
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
akácie	akácie	k1gFnPc1	akácie
<g/>
.	.	kIx.	.
</s>
<s>
Dominující	dominující	k2eAgFnSc7d1	dominující
vegetační	vegetační	k2eAgFnSc7d1	vegetační
formací	formace	k1gFnSc7	formace
jsou	být	k5eAaImIp3nP	být
prořídlé	prořídlý	k2eAgNnSc4d1	prořídlé
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
cm	cm	kA	cm
vysoké	vysoký	k2eAgFnSc2d1	vysoká
traviny	travina	k1gFnSc2	travina
<g/>
.	.	kIx.	.
</s>
<s>
Zaplavované	zaplavovaný	k2eAgFnPc1d1	zaplavovaná
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stagnace	stagnace	k1gFnSc2	stagnace
srážkové	srážkový	k2eAgFnSc2d1	srážková
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
nepropustném	propustný	k2eNgNnSc6d1	nepropustné
podloží	podloží	k1gNnSc6	podloží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
savanách	savana	k1gFnPc6	savana
nacházejí	nacházet	k5eAaImIp3nP	nacházet
optimální	optimální	k2eAgNnSc4d1	optimální
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
býložravci	býložravec	k1gMnSc3	býložravec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zvířata	zvíře	k1gNnPc1	zvíře
schopná	schopný	k2eAgFnSc1d1	schopná
rychlého	rychlý	k2eAgInSc2d1	rychlý
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
mohou	moct	k5eAaImIp3nP	moct
unikat	unikat	k5eAaImF	unikat
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
velmi	velmi	k6eAd1	velmi
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
krypsis	krypsis	k1gFnSc7	krypsis
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
fauny	fauna	k1gFnSc2	fauna
je	být	k5eAaImIp3nS	být
zvířena	zvířena	k1gFnSc1	zvířena
hrabavá	hrabavý	k2eAgFnSc1d1	hrabavá
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
různého	různý	k2eAgInSc2d1	různý
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
,	,	kIx,	,
termiti	termit	k1gMnPc1	termit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
zebry	zebra	k1gFnPc4	zebra
<g/>
,	,	kIx,	,
žirafy	žirafa	k1gFnPc4	žirafa
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
antilop	antilopa	k1gFnPc2	antilopa
<g/>
,	,	kIx,	,
pakůň	pakůň	k1gMnSc1	pakůň
<g/>
,	,	kIx,	,
nosorožec	nosorožec	k1gMnSc1	nosorožec
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šelem	šelma	k1gFnPc2	šelma
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
hyenovitý	hyenovitý	k2eAgMnSc1d1	hyenovitý
<g/>
;	;	kIx,	;
z	z	k7c2	z
endemických	endemický	k2eAgMnPc2d1	endemický
ptáků	pták	k1gMnPc2	pták
<g/>
:	:	kIx,	:
hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
marabu	marabu	k1gMnSc1	marabu
nebo	nebo	k8xC	nebo
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mravenečník	mravenečník	k1gMnSc1	mravenečník
nebo	nebo	k8xC	nebo
pásovec	pásovec	k1gMnSc1	pásovec
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
kopytníci	kopytník	k1gMnPc1	kopytník
zde	zde	k6eAd1	zde
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
,	,	kIx,	,
z	z	k7c2	z
malých	malý	k2eAgMnPc2d1	malý
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
pudu	pud	k1gInSc2	pud
jižní	jižní	k2eAgFnSc2d1	jižní
(	(	kIx(	(
<g/>
jelínek	jelínek	k1gMnSc1	jelínek
pudu	pud	k1gInSc2	pud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
šelem	šelma	k1gFnPc2	šelma
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
puma	puma	k1gFnSc1	puma
<g/>
,	,	kIx,	,
jaguár	jaguár	k1gMnSc1	jaguár
a	a	k8xC	a
vlk	vlk	k1gMnSc1	vlk
hřivnatý	hřivnatý	k2eAgMnSc1d1	hřivnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
savanách	savana	k1gFnPc6	savana
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
kontinentě	kontinent	k1gInSc6	kontinent
žije	žít	k5eAaImIp3nS	žít
klokan	klokan	k1gMnSc1	klokan
<g/>
,	,	kIx,	,
koala	koala	k1gFnSc1	koala
<g/>
,	,	kIx,	,
mravencojed	mravencojed	k1gMnSc1	mravencojed
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
dingo	dingo	k1gMnSc1	dingo
nebo	nebo	k8xC	nebo
emu	emu	k1gMnSc1	emu
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Louka	louka	k1gFnSc1	louka
Step	step	k1gFnSc1	step
Pampa	pampa	k1gFnSc1	pampa
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
savana	savana	k1gFnSc1	savana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
savana	savana	k1gFnSc1	savana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
