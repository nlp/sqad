<s>
Termín	termín	k1gInSc1	termín
agnostik	agnostik	k1gMnSc1	agnostik
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
agnostic	agnostice	k1gFnPc2	agnostice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
razit	razit	k5eAaImF	razit
poprvé	poprvé	k6eAd1	poprvé
Thomas	Thomas	k1gMnSc1	Thomas
Henry	henry	k1gInSc2	henry
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
anglický	anglický	k2eAgMnSc1d1	anglický
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
darwinismu	darwinismus	k1gInSc2	darwinismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
