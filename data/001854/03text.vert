<s>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravdivost	pravdivost	k1gFnSc1	pravdivost
některých	některý	k3yIgNnPc2	některý
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
existence	existence	k1gFnSc1	existence
či	či	k8xC	či
neexistence	neexistence	k1gFnSc1	neexistence
jakéhokoliv	jakýkoliv	k3yIgMnSc2	jakýkoliv
boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
prokázat	prokázat	k5eAaPmF	prokázat
ani	ani	k8xC	ani
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
a	a	k8xC	a
že	že	k8xS	že
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
náboženská	náboženský	k2eAgNnPc4d1	náboženské
a	a	k8xC	a
metafyzická	metafyzický	k2eAgNnPc4d1	metafyzické
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
celkově	celkově	k6eAd1	celkově
zpochybňujícího	zpochybňující	k2eAgInSc2d1	zpochybňující
či	či	k8xC	či
skeptického	skeptický	k2eAgInSc2d1	skeptický
postoje	postoj	k1gInSc2	postoj
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
k	k	k7c3	k
náboženským	náboženský	k2eAgFnPc3d1	náboženská
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
nejedná	jednat	k5eNaImIp3nS	jednat
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
o	o	k7c4	o
světonázor	světonázor	k1gInSc4	světonázor
či	či	k8xC	či
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
spíše	spíše	k9	spíše
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
metodického	metodický	k2eAgInSc2d1	metodický
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
rozporům	rozpor	k1gInPc3	rozpor
a	a	k8xC	a
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
vírou	víra	k1gFnSc7	víra
(	(	kIx(	(
<g/>
zvl.	zvl.	kA	zvl.
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
<g/>
)	)	kIx)	)
a	a	k8xC	a
věděním	vědění	k1gNnSc7	vědění
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
vědecky	vědecky	k6eAd1	vědecky
prokazatelného	prokazatelný	k2eAgNnSc2d1	prokazatelné
poznání	poznání	k1gNnSc2	poznání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
agnostik	agnostik	k1gMnSc1	agnostik
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
agnostic	agnostice	k1gFnPc2	agnostice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
razit	razit	k5eAaImF	razit
poprvé	poprvé	k6eAd1	poprvé
Thomas	Thomas	k1gMnSc1	Thomas
Henry	henry	k1gInSc2	henry
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
anglický	anglický	k2eAgMnSc1d1	anglický
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
darwinismu	darwinismus	k1gInSc2	darwinismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
agnostického	agnostický	k2eAgNnSc2d1	agnostické
uvažování	uvažování	k1gNnSc2	uvažování
se	se	k3xPyFc4	se
však	však	k9	však
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celé	celý	k2eAgFnSc2d1	celá
historie	historie	k1gFnSc2	historie
filosofického	filosofický	k2eAgNnSc2d1	filosofické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
řeckých	řecký	k2eAgMnPc2d1	řecký
filosofů	filosof	k1gMnPc2	filosof
Prótagora	Prótagor	k1gMnSc2	Prótagor
<g/>
,	,	kIx,	,
Pyrrhóna	Pyrrhón	k1gMnSc2	Pyrrhón
a	a	k8xC	a
Karneada	Karneada	k1gFnSc1	Karneada
či	či	k8xC	či
ve	v	k7c6	v
védské	védský	k2eAgFnSc6d1	védská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
Huxleyho	Huxley	k1gMnSc2	Huxley
aktivitě	aktivita	k1gFnSc6	aktivita
při	při	k7c6	při
prosazování	prosazování	k1gNnSc6	prosazování
termínu	termín	k1gInSc2	termín
ho	on	k3xPp3gMnSc4	on
převzalo	převzít	k5eAaPmAgNnS	převzít
mnoho	mnoho	k6eAd1	mnoho
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
pro	pro	k7c4	pro
Huxleyho	Huxley	k1gMnSc4	Huxley
byl	být	k5eAaImAgInS	být
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pohlížet	pohlížet	k5eAaImF	pohlížet
na	na	k7c4	na
vědecká	vědecký	k2eAgNnPc4d1	vědecké
a	a	k8xC	a
náboženská	náboženský	k2eAgNnPc4d1	náboženské
tvrzení	tvrzení	k1gNnPc4	tvrzení
"	"	kIx"	"
<g/>
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
přechodový	přechodový	k2eAgInSc1d1	přechodový
most	most	k1gInSc1	most
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
či	či	k8xC	či
"	"	kIx"	"
<g/>
váhavá	váhavý	k2eAgFnSc1d1	váhavá
<g/>
"	"	kIx"	"
pozice	pozice	k1gFnSc1	pozice
mezi	mezi	k7c7	mezi
teismem	teismus	k1gInSc7	teismus
a	a	k8xC	a
ateismem	ateismus	k1gInSc7	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
řeckého	řecký	k2eAgInSc2d1	řecký
záporu	zápor	k1gInSc2	zápor
ἀ	ἀ	k?	ἀ
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gnósis	gnósis	k1gFnSc1	gnósis
=	=	kIx~	=
poznání	poznání	k1gNnSc1	poznání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
á-gnósis	ánósis	k1gFnSc1	á-gnósis
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
nemožnost	nemožnost	k1gFnSc1	nemožnost
poznání	poznání	k1gNnSc2	poznání
<g/>
:	:	kIx,	:
nepoznatelnost	nepoznatelnost	k1gFnSc1	nepoznatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Konceptem	koncept	k1gInSc7	koncept
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
se	se	k3xPyFc4	se
Huxley	Huxlea	k1gFnSc2	Huxlea
zabýval	zabývat	k5eAaImAgInS	zabývat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
r.	r.	kA	r.
1860	[number]	k4	1860
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
termín	termín	k1gInSc1	termín
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
anglické	anglický	k2eAgFnSc2d1	anglická
Metafyzické	metafyzický	k2eAgFnSc2d1	metafyzická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Metaphysical	Metaphysical	k1gFnSc1	Metaphysical
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
popsal	popsat	k5eAaPmAgMnS	popsat
svou	svůj	k3xOyFgFnSc4	svůj
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
odmítající	odmítající	k2eAgFnSc1d1	odmítající
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
možnost	možnost	k1gFnSc4	možnost
duchovních	duchovní	k1gMnPc2	duchovní
či	či	k8xC	či
mystických	mystický	k2eAgNnPc2d1	mystické
poznání	poznání	k1gNnPc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgFnPc1d1	dřívější
autority	autorita	k1gFnPc1	autorita
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
totiž	totiž	k9	totiž
slovo	slovo	k1gNnSc4	slovo
gnósis	gnósis	k1gFnSc2	gnósis
používaly	používat	k5eAaImAgInP	používat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
duchovní	duchovní	k2eAgNnSc1d1	duchovní
poznání	poznání	k1gNnSc1	poznání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
nelze	lze	k6eNd1	lze
ovšem	ovšem	k9	ovšem
plést	plést	k5eAaImF	plést
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
postoji	postoj	k1gInPc7	postoj
<g/>
,	,	kIx,	,
vymezujícími	vymezující	k2eAgInPc7d1	vymezující
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
starověkým	starověký	k2eAgFnPc3d1	starověká
náboženským	náboženský	k2eAgFnPc3d1	náboženská
herezím	hereze	k1gFnPc3	hereze
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
třeba	třeba	k6eAd1	třeba
gnosticismus	gnosticismus	k1gInSc1	gnosticismus
<g/>
,	,	kIx,	,
Huxley	Huxle	k1gMnPc4	Huxle
používal	používat	k5eAaImAgInS	používat
termín	termín	k1gInSc1	termín
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
širším	široký	k2eAgInSc6d2	širší
a	a	k8xC	a
abstraktnějším	abstraktní	k2eAgInSc6d2	abstraktnější
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
chápal	chápat	k5eAaImAgMnS	chápat
ho	on	k3xPp3gNnSc4	on
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
víru	víra	k1gFnSc4	víra
či	či	k8xC	či
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
metodu	metoda	k1gFnSc4	metoda
skeptického	skeptický	k2eAgInSc2d1	skeptický
základního	základní	k2eAgInSc2d1	základní
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
přinášejícího	přinášející	k2eAgInSc2d1	přinášející
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
neurovědou	neurověda	k1gFnSc7	neurověda
a	a	k8xC	a
psychologií	psychologie	k1gFnSc7	psychologie
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
objevovat	objevovat	k5eAaImF	objevovat
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
agnostic	agnostice	k1gFnPc2	agnostice
<g/>
"	"	kIx"	"
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
nelokalizovatelný	lokalizovatelný	k2eNgMnSc1d1	lokalizovatelný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnSc3d1	počítačová
a	a	k8xC	a
marketingové	marketingový	k2eAgFnSc3d1	marketingová
literatuře	literatura	k1gFnSc3	literatura
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
získalo	získat	k5eAaPmAgNnS	získat
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
agnostický	agnostický	k2eAgInSc4d1	agnostický
<g/>
"	"	kIx"	"
význam	význam	k1gInSc4	význam
"	"	kIx"	"
<g/>
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
univerzální	univerzální	k2eAgMnSc1d1	univerzální
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
"	"	kIx"	"
<g/>
platformě	platforma	k1gFnSc3	platforma
agnostický	agnostický	k2eAgInSc1d1	agnostický
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
běžet	běžet	k5eAaImF	běžet
pod	pod	k7c7	pod
libovolným	libovolný	k2eAgInSc7d1	libovolný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
existuje	existovat	k5eAaImIp3nS	existovat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
hardware	hardware	k1gInSc1	hardware
agnostic	agnostice	k1gFnPc2	agnostice
<g/>
"	"	kIx"	"
označující	označující	k2eAgFnSc4d1	označující
softwarovou	softwarový	k2eAgFnSc4d1	softwarová
platformu	platforma	k1gFnSc4	platforma
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provozovat	provozovat	k5eAaImF	provozovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
hardwaru	hardware	k1gInSc2	hardware
atd.	atd.	kA	atd.
Za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
a	a	k8xC	a
výchozí	výchozí	k2eAgFnSc4d1	výchozí
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
brát	brát	k5eAaImF	brát
definici	definice	k1gFnSc4	definice
otce	otec	k1gMnSc2	otec
termínu	termín	k1gInSc2	termín
Thomase	Thomas	k1gMnSc2	Thomas
Henryho	Henry	k1gMnSc2	Henry
Huxleyho	Huxley	k1gMnSc2	Huxley
<g/>
.	.	kIx.	.
</s>
<s>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
ostře	ostro	k6eAd1	ostro
napadán	napadán	k2eAgMnSc1d1	napadán
jak	jak	k8xS	jak
teisty	teista	k1gMnPc7	teista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ateisty	ateista	k1gMnSc2	ateista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
definice	definice	k1gFnSc1	definice
stoupenců	stoupenec	k1gMnPc2	stoupenec
obou	dva	k4xCgInPc2	dva
těchto	tento	k3xDgInPc2	tento
"	"	kIx"	"
<g/>
táborů	tábor	k1gInPc2	tábor
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
smyslu	smysl	k1gInSc2	smysl
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
formuloval	formulovat	k5eAaImAgMnS	formulovat
Huxley	Huxlea	k1gMnSc2	Huxlea
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
mj.	mj.	kA	mj.
i	i	k8xC	i
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snadněji	snadno	k6eAd2	snadno
vyvracely	vyvracet	k5eAaImAgFnP	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Huxleyho	Huxley	k1gMnSc2	Huxley
je	být	k5eAaImIp3nS	být
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
důslednou	důsledný	k2eAgFnSc7d1	důsledná
aplikací	aplikace	k1gFnSc7	aplikace
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
:	:	kIx,	:
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
myšlení	myšlení	k1gNnSc2	myšlení
následujte	následovat	k5eAaImRp2nP	následovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
rozum	rozum	k1gInSc4	rozum
až	až	k6eAd1	až
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vás	vy	k3xPp2nPc4	vy
zavede	zavést	k5eAaPmIp3nS	zavést
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
"	"	kIx"	"
negativně	negativně	k6eAd1	negativně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
myšlení	myšlení	k1gNnSc2	myšlení
neberte	brát	k5eNaImRp2nP	brát
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
jisté	jistý	k2eAgInPc4d1	jistý
úsudky	úsudek	k1gInPc4	úsudek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nejsou	být	k5eNaImIp3nP	být
či	či	k8xC	či
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
dokázány	dokázán	k2eAgFnPc1d1	dokázána
<g/>
"	"	kIx"	"
Nejčastěji	často	k6eAd3	často
používanou	používaný	k2eAgFnSc7d1	používaná
definicí	definice	k1gFnSc7	definice
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
filosofický	filosofický	k2eAgInSc1d1	filosofický
názor	názor	k1gInSc1	názor
tvrdící	tvrdící	k2eAgInSc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
existenci	existence	k1gFnSc4	existence
čehokoli	cokoli	k3yInSc2	cokoli
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nelze	lze	k6eNd1	lze
poznat	poznat	k5eAaPmF	poznat
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
nemůžeme	moct	k5eNaImIp1nP	moct
ani	ani	k8xC	ani
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
však	však	k9	však
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
(	(	kIx(	(
<g/>
preagnostika	preagnostik	k1gMnSc2	preagnostik
<g/>
)	)	kIx)	)
Davida	David	k1gMnSc2	David
Huma	Hum	k1gInSc2	Hum
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
s	s	k7c7	s
kritickou	kritický	k2eAgFnSc7d1	kritická
glosou	glosa	k1gFnSc7	glosa
definuje	definovat	k5eAaBmIp3nS	definovat
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
jako	jako	k8xC	jako
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
nepoznatelnosti	nepoznatelnost	k1gFnSc6	nepoznatelnost
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
světového	světový	k2eAgInSc2d1	světový
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
duševního	duševní	k2eAgInSc2d1	duševní
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
prvních	první	k4xOgInPc6	první
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
posledních	poslední	k2eAgInPc2d1	poslední
účelů	účel	k1gInPc2	účel
a	a	k8xC	a
o	o	k7c6	o
nemožnosti	nemožnost	k1gFnSc6	nemožnost
rozřešiti	rozřešit	k5eAaPmF	rozřešit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
záhadu	záhada	k1gFnSc4	záhada
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
hybu	hyb	k1gInSc2	hyb
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
však	však	k9	však
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
(	(	kIx(	(
<g/>
preagnostika	preagnostik	k1gMnSc2	preagnostik
<g/>
)	)	kIx)	)
Augusta	August	k1gMnSc2	August
Comta	Comt	k1gInSc2	Comt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
teisté	teista	k1gMnPc1	teista
<g/>
,	,	kIx,	,
např.	např.	kA	např.
český	český	k2eAgMnSc1d1	český
katolický	katolický	k2eAgMnSc1d1	katolický
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
<g/>
,	,	kIx,	,
definují	definovat	k5eAaBmIp3nP	definovat
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vágní	vágní	k2eAgFnSc1d1	vágní
náboženskost	náboženskost	k1gFnSc1	náboženskost
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
něcismus	něcismus	k1gInSc1	něcismus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
církví	církev	k1gFnPc2	církev
nevěřím	věřit	k5eNaImIp1nS	věřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
,	,	kIx,	,
<g/>
něco	něco	k3yInSc1	něco
<g/>
'	'	kIx"	'
nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
náboženský	náboženský	k2eAgInSc1d1	náboženský
diletantismus	diletantismus	k1gInSc1	diletantismus
(	(	kIx(	(
<g/>
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
skeptikové	skeptik	k1gMnPc1	skeptik
odmítají	odmítat	k5eAaImIp3nP	odmítat
ztotožňování	ztotožňování	k1gNnSc4	ztotožňování
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
se	s	k7c7	s
skepticismem	skepticismus	k1gInSc7	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
antických	antický	k2eAgMnPc2d1	antický
skeptiků	skeptik	k1gMnPc2	skeptik
<g/>
,	,	kIx,	,
Sextus	Sextus	k1gMnSc1	Sextus
Empiricus	Empiricus	k1gMnSc1	Empiricus
<g/>
,	,	kIx,	,
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
skeptickému	skeptický	k2eAgInSc3d1	skeptický
postoji	postoj	k1gInSc3	postoj
zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
zapovídá	zapovídat	k5eAaImIp3nS	zapovídat
ho	on	k3xPp3gMnSc4	on
směšovat	směšovat	k5eAaImF	směšovat
jak	jak	k6eAd1	jak
s	s	k7c7	s
dogmatismem	dogmatismus	k1gInSc7	dogmatismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
s	s	k7c7	s
agnosticismem	agnosticismus	k1gInSc7	agnosticismus
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
argumentací	argumentace	k1gFnSc7	argumentace
<g/>
,	,	kIx,	,
termíny	termín	k1gInPc1	termín
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
pozdější	pozdní	k2eAgMnSc1d2	pozdější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
americký	americký	k2eAgMnSc1d1	americký
novinář	novinář	k1gMnSc1	novinář
Ron	Ron	k1gMnSc1	Ron
Rosenbaum	Rosenbaum	k1gNnSc4	Rosenbaum
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
Agnostickém	agnostický	k2eAgInSc6d1	agnostický
manifestu	manifest	k1gInSc6	manifest
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
An	An	k1gFnSc1	An
Agnostic	Agnostice	k1gFnPc2	Agnostice
Manifesto	Manifesta	k1gMnSc5	Manifesta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
za	za	k7c4	za
krajní	krajní	k2eAgInSc4d1	krajní
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
není	být	k5eNaImIp3nS	být
ateismus	ateismus	k1gInSc4	ateismus
nebo	nebo	k8xC	nebo
teismus	teismus	k1gInSc4	teismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
radikálním	radikální	k2eAgInSc7d1	radikální
skepticismem	skepticismus	k1gInSc7	skepticismus
pochybujícím	pochybující	k2eAgInSc7d1	pochybující
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
jistoty	jistota	k1gFnSc2	jistota
<g/>
,	,	kIx,	,
opozicí	opozice	k1gFnSc7	opozice
vůči	vůči	k7c3	vůči
nezaručeným	zaručený	k2eNgFnPc3d1	nezaručená
jistotám	jistota	k1gFnPc3	jistota
nabízeným	nabízený	k2eAgFnPc3d1	nabízená
ateismem	ateismus	k1gInSc7	ateismus
i	i	k9	i
teismem	teismus	k1gInSc7	teismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Manifest	manifest	k1gInSc1	manifest
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
ostře	ostro	k6eAd1	ostro
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pro	pro	k7c4	pro
autorovu	autorův	k2eAgFnSc4d1	autorova
minimální	minimální	k2eAgFnSc4d1	minimální
erudici	erudice	k1gFnSc4	erudice
a	a	k8xC	a
špatnou	špatný	k2eAgFnSc4d1	špatná
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
pojmech	pojem	k1gInPc6	pojem
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Australský	australský	k2eAgMnSc1d1	australský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
religionista	religionista	k1gMnSc1	religionista
Graham	Graham	k1gMnSc1	Graham
Robert	Robert	k1gMnSc1	Robert
Oppy	Oppa	k1gFnSc2	Oppa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
"	"	kIx"	"
<g/>
Rozumný	rozumný	k2eAgMnSc1d1	rozumný
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
odmítnout	odmítnout	k5eAaPmF	odmítnout
řešení	řešení	k1gNnSc4	řešení
otázky	otázka	k1gFnSc2	otázka
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Silný	silný	k2eAgInSc1d1	silný
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
striktní	striktní	k2eAgInSc1d1	striktní
<g/>
,	,	kIx,	,
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
nebo	nebo	k8xC	nebo
absolutní	absolutní	k2eAgInSc1d1	absolutní
<g/>
)	)	kIx)	)
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
popírá	popírat	k5eAaImIp3nS	popírat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
možnost	možnost	k1gFnSc4	možnost
poznání	poznání	k1gNnSc2	poznání
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
bohů	bůh	k1gMnPc2	bůh
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
nejzazší	zadní	k2eAgFnSc2d3	nejzazší
jsoucnosti	jsoucnost	k1gFnSc2	jsoucnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
osobní	osobní	k2eAgFnSc4d1	osobní
zkušenost	zkušenost	k1gFnSc4	zkušenost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
potvrdit	potvrdit	k5eAaPmF	potvrdit
nebo	nebo	k8xC	nebo
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
zase	zase	k9	zase
jenom	jenom	k9	jenom
další	další	k2eAgFnSc7d1	další
subjektivní	subjektivní	k2eAgFnSc7d1	subjektivní
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	slabý	k2eAgInSc1d1	slabý
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
"	"	kIx"	"
<g/>
Rozumný	rozumný	k2eAgMnSc1d1	rozumný
člověk	člověk	k1gMnSc1	člověk
nevidí	vidět	k5eNaImIp3nS	vidět
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
neměla	mít	k5eNaImAgFnS	mít
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
zatím	zatím	k6eAd1	zatím
za	za	k7c4	za
vyřešenou	vyřešený	k2eAgFnSc4d1	vyřešená
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Slabý	slabý	k2eAgInSc1d1	slabý
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
empirický	empirický	k2eAgInSc1d1	empirický
<g/>
,	,	kIx,	,
mírný	mírný	k2eAgInSc1d1	mírný
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgInSc1d1	negativní
nebo	nebo	k8xC	nebo
dočasný	dočasný	k2eAgInSc1d1	dočasný
<g/>
)	)	kIx)	)
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
otázka	otázka	k1gFnSc1	otázka
existence	existence	k1gFnSc2	existence
nebo	nebo	k8xC	nebo
neexistence	neexistence	k1gFnSc2	neexistence
Boha	bůh	k1gMnSc2	bůh
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
zodpovězena	zodpovědět	k5eAaPmNgFnS	zodpovědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
ověřitelná	ověřitelný	k2eAgFnSc1d1	ověřitelná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
další	další	k2eAgInPc4d1	další
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Oppym	Oppym	k1gInSc1	Oppym
i	i	k9	i
Huxleyem	Huxleyem	k1gInSc1	Huxleyem
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
lidověji	lidově	k6eAd2	lidově
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
slabý	slabý	k2eAgInSc1d1	slabý
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
charakterizován	charakterizován	k2eAgInSc1d1	charakterizován
výpověďmi	výpověď	k1gFnPc7	výpověď
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgMnSc1d1	silný
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc1	ten
nevím	vědět	k5eNaImIp1nS	vědět
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nevíš	vědět	k5eNaImIp2nS	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	Slabý	k1gMnSc1	Slabý
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc1	ten
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ty	k3xPp2nSc1	ty
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
víš	vědět	k5eAaImIp2nS	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Britský	britský	k2eAgMnSc1d1	britský
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
evolucionista	evolucionista	k1gMnSc1	evolucionista
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc1	Dawkins
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
ovšem	ovšem	k9	ovšem
jinak	jinak	k6eAd1	jinak
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
z	z	k7c2	z
ateistických	ateistický	k2eAgFnPc2d1	ateistická
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
a	a	k8xC	a
člení	členit	k5eAaImIp3nS	členit
je	on	k3xPp3gInPc4	on
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
Členění	členění	k1gNnSc3	členění
TAP	TAP	kA	TAP
a	a	k8xC	a
PAP	PAP	kA	PAP
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
TAP	TAP	kA	TAP
-	-	kIx~	-
dočasný	dočasný	k2eAgInSc1d1	dočasný
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Temporary	Temporara	k1gFnSc2	Temporara
Agnosticism	Agnosticism	k1gInSc1	Agnosticism
in	in	k?	in
Practice	Practice	k1gFnSc1	Practice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
legitimní	legitimní	k2eAgFnSc4d1	legitimní
nerozhodnost	nerozhodnost	k1gFnSc4	nerozhodnost
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odpověď	odpověď	k1gFnSc1	odpověď
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
jsme	být	k5eAaImIp1nP	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
nedošli	dojít	k5eNaPmAgMnP	dojít
(	(	kIx(	(
<g/>
nemáme	mít	k5eNaImIp1nP	mít
dostatek	dostatek	k1gInSc4	dostatek
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
nerozumíme	rozumět	k5eNaImIp1nP	rozumět
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
nemáme	mít	k5eNaImIp1nP	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
rozluštění	rozluštění	k1gNnSc4	rozluštění
<g/>
"	"	kIx"	"
-	-	kIx~	-
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
slabému	slabý	k2eAgInSc3d1	slabý
agnosticismu	agnosticismus	k1gInSc3	agnosticismus
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
PAP	PAP	kA	PAP
-	-	kIx~	-
trvalý	trvalý	k2eAgInSc4d1	trvalý
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
z	z	k7c2	z
principu	princip	k1gInSc2	princip
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Permanent	permanent	k1gInSc1	permanent
Agnosticism	Agnosticism	k1gMnSc1	Agnosticism
in	in	k?	in
Principle	Principle	k1gMnSc1	Principle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
otázky	otázka	k1gFnPc4	otázka
nelze	lze	k6eNd1	lze
nikdy	nikdy	k6eAd1	nikdy
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
shromážděných	shromážděný	k2eAgInPc2d1	shromážděný
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
otázek	otázka	k1gFnPc2	otázka
důkazy	důkaz	k1gInPc1	důkaz
nelze	lze	k6eNd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
skutečně	skutečně	k6eAd1	skutečně
vidíte	vidět	k5eAaImIp2nP	vidět
stejnou	stejný	k2eAgFnSc4d1	stejná
červenou	červená	k1gFnSc4	červená
jako	jako	k8xS	jako
já	já	k3xPp1nSc1	já
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
silnému	silný	k2eAgInSc3d1	silný
agnosticismu	agnosticismus	k1gInSc3	agnosticismus
Členění	členění	k1gNnSc2	členění
na	na	k7c6	na
škále	škála	k1gFnSc6	škála
teista	teista	k1gMnSc1	teista
<g/>
/	/	kIx~	/
<g/>
ateista	ateista	k1gMnSc1	ateista
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
existenci	existence	k1gFnSc4	existence
Boha	bůh	k1gMnSc2	bůh
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
Prakticky	prakticky	k6eAd1	prakticky
vzato	vzít	k5eAaPmNgNnS	vzít
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tíhne	tíhnout	k5eAaImIp3nS	tíhnout
k	k	k7c3	k
teismu	teismus	k1gInSc3	teismus
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
velmi	velmi	k6eAd1	velmi
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíš	spíš	k9	spíš
bych	by	kYmCp1nS	by
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
50	[number]	k4	50
%	%	kIx~	%
Zcela	zcela	k6eAd1	zcela
nestranný	nestranný	k2eAgMnSc1d1	nestranný
agnostik	agnostik	k1gMnSc1	agnostik
"	"	kIx"	"
<g/>
Existence	existence	k1gFnSc1	existence
a	a	k8xC	a
neexistence	neexistence	k1gFnPc1	neexistence
Boha	bůh	k1gMnSc2	bůh
jsou	být	k5eAaImIp3nP	být
přesně	přesně	k6eAd1	přesně
stejně	stejně	k6eAd1	stejně
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
Prakticky	prakticky	k6eAd1	prakticky
vzato	vzít	k5eAaPmNgNnS	vzít
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tíhne	tíhnout	k5eAaImIp3nS	tíhnout
k	k	k7c3	k
ateismu	ateismus	k1gInSc3	ateismus
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Bůh	bůh	k1gMnSc1	bůh
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíš	spíš	k9	spíš
bych	by	kYmCp1nS	by
byl	být	k5eAaImAgInS	být
skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skotský	skotský	k2eAgMnSc1d1	skotský
osvícenský	osvícenský	k2eAgMnSc1d1	osvícenský
filosof	filosof	k1gMnSc1	filosof
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
-	-	kIx~	-
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
smysluplné	smysluplný	k2eAgInPc1d1	smysluplný
výroky	výrok	k1gInPc1	výrok
o	o	k7c6	o
universu	universum	k1gNnSc6	universum
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
podmíněny	podmínit	k5eAaPmNgFnP	podmínit
určitou	určitý	k2eAgFnSc7d1	určitá
mírou	míra	k1gFnSc7	míra
pochybnosti	pochybnost	k1gFnSc2	pochybnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
skeptikem	skeptik	k1gMnSc7	skeptik
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
žádnou	žádný	k3yNgFnSc4	žádný
jistotu	jistota	k1gFnSc4	jistota
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
předmětů	předmět	k1gInPc2	předmět
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
neexistují	existovat	k5eNaImIp3nP	existovat
mimo	mimo	k7c4	mimo
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Hume	Hume	k6eAd1	Hume
dále	daleko	k6eAd2	daleko
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
metafyziky	metafyzika	k1gFnSc2	metafyzika
není	být	k5eNaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
popírá	popírat	k5eAaImIp3nS	popírat
průkaznost	průkaznost	k1gFnSc1	průkaznost
zázraků	zázrak	k1gInPc2	zázrak
i	i	k8xC	i
autoritu	autorita	k1gFnSc4	autorita
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
věřící	věřící	k1gMnPc4	věřící
jako	jako	k8xC	jako
antropocentristy	antropocentrista	k1gMnPc4	antropocentrista
a	a	k8xC	a
egoisty	egoista	k1gMnPc4	egoista
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
motivy	motiv	k1gInPc4	motiv
odměny	odměna	k1gFnSc2	odměna
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
morálce	morálka	k1gFnSc6	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Hume	Humat	k5eAaPmIp3nS	Humat
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
důkazu	důkaz	k1gInSc2	důkaz
Boží	boží	k2eAgFnSc2d1	boží
neexistence	neexistence	k1gFnSc2	neexistence
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
po	po	k7c6	po
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
pouze	pouze	k6eAd1	pouze
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
evidentní	evidentní	k2eAgNnSc4d1	evidentní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
prokazována	prokazovat	k5eAaImNgFnS	prokazovat
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
lidskou	lidský	k2eAgFnSc7d1	lidská
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
-	-	kIx~	-
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
autonomii	autonomie	k1gFnSc4	autonomie
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
transcendentalismu	transcendentalismus	k1gInSc2	transcendentalismus
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
vědění	vědění	k1gNnSc2	vědění
ve	v	k7c6	v
"	"	kIx"	"
<g/>
světě	svět	k1gInSc6	svět
pro	pro	k7c4	pro
nás	my	k3xPp1nPc6	my
<g/>
"	"	kIx"	"
a	a	k8xC	a
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
dogmatickému	dogmatický	k2eAgInSc3d1	dogmatický
rozumu	rozum	k1gInSc3	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
poznání	poznání	k1gNnSc1	poznání
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
přijímání	přijímání	k1gNnSc1	přijímání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
objektu	objekt	k1gInSc6	objekt
(	(	kIx(	(
<g/>
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
věc	věc	k1gFnSc1	věc
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
subjekt	subjekt	k1gInSc4	subjekt
(	(	kIx(	(
<g/>
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
tzv.	tzv.	kA	tzv.
koperníkovským	koperníkovský	k2eAgInSc7d1	koperníkovský
obratem	obrat	k1gInSc7	obrat
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
aktivní	aktivní	k2eAgFnSc1d1	aktivní
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
subjekt	subjekt	k1gInSc1	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ve	v	k7c6	v
vědomí	vědomí	k1gNnSc6	vědomí
máme	mít	k5eAaImIp1nP	mít
pak	pak	k6eAd1	pak
právě	právě	k9	právě
tolik	tolik	k4xDc4	tolik
<g/>
,	,	kIx,	,
na	na	k7c4	na
kolik	kolik	k4yQc4	kolik
se	se	k3xPyFc4	se
ptáme	ptat	k5eAaImIp1nP	ptat
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
objekt	objekt	k1gInSc4	objekt
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dá	dát	k5eAaPmIp3nS	dát
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
ho	on	k3xPp3gNnSc4	on
vidět	vidět	k5eAaImF	vidět
<g/>
)	)	kIx)	)
-	-	kIx~	-
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
objekt	objekt	k1gInSc1	objekt
"	"	kIx"	"
<g/>
věcí	věc	k1gFnSc7	věc
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ding	Ding	k1gInSc1	Ding
für	für	k?	für
Sich	Sich	k1gInSc1	Sich
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
sám	sám	k3xTgInSc1	sám
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
věc	věc	k1gFnSc1	věc
o	o	k7c6	o
sobě	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ding	Ding	k1gInSc1	Ding
an	an	k?	an
sich	sich	k1gInSc1	sich
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
věc	věc	k1gFnSc1	věc
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
"	"	kIx"	"
nemůžeme	moct	k5eNaImIp1nP	moct
vnímat	vnímat	k5eAaImF	vnímat
ani	ani	k8xC	ani
poznávat	poznávat	k5eAaImF	poznávat
(	(	kIx(	(
<g/>
poznat	poznat	k5eAaPmF	poznat
můžeme	moct	k5eAaImIp1nP	moct
jenom	jenom	k9	jenom
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
věci	věc	k1gFnPc1	věc
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
svět	svět	k1gInSc1	svět
jevů	jev	k1gInPc2	jev
<g/>
;	;	kIx,	;
podstatu	podstata	k1gFnSc4	podstata
věci	věc	k1gFnSc2	věc
<g/>
,	,	kIx,	,
věc	věc	k1gFnSc1	věc
"	"	kIx"	"
<g/>
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
"	"	kIx"	"
poznat	poznat	k5eAaPmF	poznat
nemůžeme	moct	k5eNaImIp1nP	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
-	-	kIx~	-
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
poznání	poznání	k1gNnSc2	poznání
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
jen	jen	k6eAd1	jen
gnozeologický	gnozeologický	k2eAgInSc4d1	gnozeologický
subjekt	subjekt	k1gInSc4	subjekt
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Comte	Comit	k5eAaPmRp2nP	Comit
přejímá	přejímat	k5eAaImIp3nS	přejímat
ze	z	k7c2	z
soudobé	soudobý	k2eAgFnSc2d1	soudobá
přírodovědy	přírodověda	k1gFnSc2	přírodověda
antiteologičnost	antiteologičnost	k1gFnSc1	antiteologičnost
a	a	k8xC	a
antimetafyzičnost	antimetafyzičnost	k1gFnSc1	antimetafyzičnost
a	a	k8xC	a
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
pátrání	pátrání	k1gNnSc4	pátrání
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
prvních	první	k4xOgNnPc6	první
a	a	k8xC	a
posledních	poslední	k2eAgFnPc6d1	poslední
příčinách	příčina	k1gFnPc6	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Comte	Comit	k5eAaBmRp2nP	Comit
omezuje	omezovat	k5eAaImIp3nS	omezovat
koncept	koncept	k1gInSc1	koncept
poznání	poznání	k1gNnSc2	poznání
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
poznatelné	poznatelný	k2eAgNnSc1d1	poznatelné
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
empirické	empirický	k2eAgFnSc2d1	empirická
přírodovědy	přírodověda	k1gFnSc2	přírodověda
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
spekulativní	spekulativní	k2eAgFnSc4d1	spekulativní
metafyziku	metafyzika	k1gFnSc4	metafyzika
i	i	k8xC	i
metafyzické	metafyzický	k2eAgFnPc4d1	metafyzická
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Henry	Henry	k1gMnSc1	Henry
Huxley	Huxlea	k1gFnSc2	Huxlea
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dbám	dbát	k5eAaImIp1nS	dbát
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
učil	učít	k5eAaPmAgMnS	učít
své	svůj	k3xOyFgFnSc2	svůj
touhy	touha	k1gFnSc2	touha
podřizovat	podřizovat	k5eAaImF	podřizovat
se	se	k3xPyFc4	se
faktům	fakt	k1gInPc3	fakt
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
abych	aby	kYmCp1nS	aby
fakta	faktum	k1gNnSc2	faktum
nutil	nutit	k5eAaImAgInS	nutit
být	být	k5eAaImF	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
mými	můj	k3xOp1gFnPc7	můj
touhami	touha	k1gFnPc7	touha
<g/>
...	...	k?	...
Stůjte	stát	k5eAaImRp2nP	stát
před	před	k7c4	před
fakty	fakt	k1gInPc4	fakt
jako	jako	k8xS	jako
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
buďte	budit	k5eAaImRp2nP	budit
připraveni	připravit	k5eAaPmNgMnP	připravit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
apriorního	apriorní	k2eAgInSc2d1	apriorní
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
následujte	následovat	k5eAaImRp2nP	následovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
vás	vy	k3xPp2nPc4	vy
vede	vést	k5eAaImIp3nS	vést
kamkoli	kamkoli	k6eAd1	kamkoli
<g/>
,	,	kIx,	,
k	k	k7c3	k
jakýmkoli	jakýkoli	k3yIgFnPc3	jakýkoli
propastem	propast	k1gFnPc3	propast
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
nepoznáte	poznat	k5eNaPmIp2nP	poznat
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Huxley	Huxlea	k1gFnSc2	Huxlea
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stvoření	stvoření	k1gNnSc1	stvoření
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
docela	docela	k6eAd1	docela
dobře	dobře	k6eAd1	dobře
představitelné	představitelný	k2eAgNnSc1d1	představitelné
<g/>
.	.	kIx.	.
</s>
<s>
Nevidím	vidět	k5eNaImIp1nS	vidět
nic	nic	k3yNnSc1	nic
problematického	problematický	k2eAgNnSc2d1	problematické
na	na	k7c6	na
představě	představa	k1gFnSc6	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nějakých	nějaký	k3yIgFnPc6	nějaký
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
tento	tento	k3xDgInSc1	tento
vesmír	vesmír	k1gInSc4	vesmír
neexistoval	existovat	k5eNaImAgInS	existovat
a	a	k8xC	a
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
existovat	existovat	k5eAaImF	existovat
až	až	k6eAd1	až
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vědomého	vědomý	k2eAgNnSc2d1	vědomé
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
nějaké	nějaký	k3yIgFnSc2	nějaký
prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
apriorní	apriorní	k2eAgInPc1d1	apriorní
argumenty	argument	k1gInPc1	argument
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
zpochybňující	zpochybňující	k2eAgFnSc1d1	zpochybňující
možnost	možnost	k1gFnSc1	možnost
stvořitelských	stvořitelský	k2eAgInPc2d1	stvořitelský
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
mi	já	k3xPp1nSc3	já
připadají	připadat	k5eAaPmIp3nP	připadat
zbavené	zbavený	k2eAgNnSc1d1	zbavené
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
racionálního	racionální	k2eAgInSc2d1	racionální
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Robert	Robert	k1gMnSc1	Robert
G.	G.	kA	G.
Ingersoll	Ingersoll	k1gMnSc1	Ingersoll
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
řečník	řečník	k1gMnSc1	řečník
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
jsem	být	k5eAaImIp1nS	být
agnostikem	agnostik	k1gMnSc7	agnostik
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g />
.	.	kIx.	.
</s>
<s>
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
nekonečném	konečný	k2eNgInSc6d1	nekonečný
řetězu	řetěz	k1gInSc6	řetěz
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
žádná	žádný	k3yNgFnSc1	žádný
souvislost	souvislost	k1gFnSc1	souvislost
osamocená	osamocený	k2eAgFnSc1d1	osamocená
nebo	nebo	k8xC	nebo
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
modlitby	modlitba	k1gFnPc4	modlitba
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
uctívání	uctívání	k1gNnSc1	uctívání
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
mohlo	moct	k5eAaImAgNnS	moct
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
či	či	k8xC	či
něco	něco	k3yInSc4	něco
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ingersoll	Ingersoll	k1gMnSc1	Ingersoll
dále	daleko	k6eAd2	daleko
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
někde	někde	k6eAd1	někde
Bůh	bůh	k1gMnSc1	bůh
<g/>
?	?	kIx.	?
</s>
<s>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
nesmrtelný	nesmrtelný	k1gMnSc1	nesmrtelný
<g/>
?	?	kIx.	?
</s>
<s>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
ale	ale	k8xC	ale
vím	vědět	k5eAaImIp1nS	vědět
jistě	jistě	k9	jistě
<g/>
:	:	kIx,	:
žádná	žádný	k3yNgFnSc1	žádný
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
víra	víra	k1gFnSc1	víra
ani	ani	k8xC	ani
žádné	žádný	k3yNgNnSc4	žádný
popírání	popírání	k1gNnSc4	popírání
nemohou	moct	k5eNaImIp3nP	moct
změnit	změnit	k5eAaPmF	změnit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
musí	muset	k5eAaImIp3nS	muset
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Můžeme	moct	k5eAaImIp1nP	moct
být	být	k5eAaImF	být
upřímní	upřímný	k2eAgMnPc1d1	upřímný
v	v	k7c6	v
přiznání	přiznání	k1gNnSc6	přiznání
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
jsme	být	k5eAaImIp1nP	být
neznalí	znalý	k2eNgMnPc1d1	neznalý
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
dotázáni	dotázat	k5eAaPmNgMnP	dotázat
<g/>
,	,	kIx,	,
co	co	k8xS	co
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
známého	známý	k1gMnSc2	známý
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ingersoll	Ingersoll	k1gInSc1	Ingersoll
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
nádherné	nádherný	k2eAgNnSc1d1	nádherné
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
království	království	k1gNnSc6	království
myšlenek	myšlenka	k1gFnPc2	myšlenka
jste	být	k5eAaImIp2nP	být
bez	bez	k7c2	bez
pout	pouto	k1gNnPc2	pouto
<g/>
,	,	kIx,	,
že	že	k8xS	že
máte	mít	k5eAaImIp2nP	mít
právo	právo	k1gNnSc4	právo
<g />
.	.	kIx.	.
</s>
<s>
zkoumat	zkoumat	k5eAaImF	zkoumat
všechny	všechen	k3xTgFnPc4	všechen
výšiny	výšina	k1gFnPc4	výšina
a	a	k8xC	a
hlubiny	hlubina	k1gFnPc4	hlubina
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
prostor	prostora	k1gFnPc2	prostora
myšlení	myšlení	k1gNnSc2	myšlení
neexistují	existovat	k5eNaImIp3nP	existovat
zdi	zeď	k1gFnPc4	zeď
ani	ani	k8xC	ani
ploty	plot	k1gInPc4	plot
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgNnPc1	žádný
zakázaná	zakázaný	k2eAgNnPc1d1	zakázané
místa	místo	k1gNnPc1	místo
ani	ani	k8xC	ani
žádné	žádný	k3yNgInPc4	žádný
posvátné	posvátný	k2eAgInPc4d1	posvátný
kouty	kout	k1gInPc4	kout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Esej	esej	k1gInSc1	esej
nositele	nositel	k1gMnSc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Bertranda	Bertrando	k1gNnSc2	Bertrando
Russella	Russello	k1gNnSc2	Russello
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
nejsem	být	k5eNaImIp1nS	být
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
projevu	projev	k1gInSc2	projev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zahrnutý	zahrnutý	k2eAgMnSc1d1	zahrnutý
do	do	k7c2	do
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
klasiku	klasika	k1gFnSc4	klasika
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
Russelovy	Russelův	k2eAgFnPc4d1	Russelova
námitky	námitka	k1gFnPc4	námitka
proti	proti	k7c3	proti
možnosti	možnost	k1gFnSc3	možnost
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
následně	následně	k6eAd1	následně
jeho	jeho	k3xOp3gFnPc1	jeho
morální	morální	k2eAgFnPc1d1	morální
výhrady	výhrada	k1gFnPc1	výhrada
proti	proti	k7c3	proti
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
učení	učení	k1gNnSc3	učení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Russel	Russel	k1gInSc1	Russel
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
postavili	postavit	k5eAaPmAgMnP	postavit
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
dívali	dívat	k5eAaImAgMnP	dívat
se	se	k3xPyFc4	se
na	na	k7c4	na
svět	svět	k1gInSc4	svět
otevřeně	otevřeně	k6eAd1	otevřeně
<g/>
,	,	kIx,	,
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
se	s	k7c7	s
svobodným	svobodný	k2eAgInSc7d1	svobodný
rozumem	rozum	k1gInSc7	rozum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
eseji	esej	k1gInSc6	esej
"	"	kIx"	"
<g/>
Mystika	mystika	k1gFnSc1	mystika
a	a	k8xC	a
logika	logika	k1gFnSc1	logika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mysticism	Mysticism	k1gMnSc1	Mysticism
and	and	k?	and
logic	logic	k1gMnSc1	logic
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Russellovi	Russellův	k2eAgMnPc1d1	Russellův
"	"	kIx"	"
<g/>
plně	plně	k6eAd1	plně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
mystika	mystika	k1gFnSc1	mystika
jeví	jevit	k5eAaImIp3nS	jevit
pomýlená	pomýlený	k2eAgFnSc1d1	pomýlená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
za	za	k7c4	za
důležitou	důležitý	k2eAgFnSc4d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
eseji	esej	k1gInSc6	esej
zároveň	zároveň	k6eAd1	zároveň
odmítá	odmítat	k5eAaImIp3nS	odmítat
evolucionismus	evolucionismus	k1gInSc1	evolucionismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
v	v	k7c6	v
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
O	o	k7c6	o
existenci	existence	k1gFnSc6	existence
a	a	k8xC	a
podstatě	podstata	k1gFnSc6	podstata
Boha	bůh	k1gMnSc2	bůh
<g/>
"	"	kIx"	"
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
jako	jako	k8xS	jako
ateistu	ateista	k1gMnSc4	ateista
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
přednášce	přednáška	k1gFnSc6	přednáška
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
ne-antropomorfní	ntropomorfní	k2eNgNnSc1d1	-antropomorfní
pojetí	pojetí	k1gNnSc1	pojetí
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Russell	Russell	k1gMnSc1	Russell
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
ateistu	ateista	k1gMnSc4	ateista
i	i	k8xC	i
agnostika	agnostik	k1gMnSc4	agnostik
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
však	však	k9	však
na	na	k7c6	na
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
v	v	k7c6	v
eseji	esej	k1gInSc6	esej
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
agnostik	agnostik	k1gMnSc1	agnostik
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Russell	Russell	k1gMnSc1	Russell
definuje	definovat	k5eAaBmIp3nS	definovat
agnostika	agnostik	k1gMnSc4	agnostik
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Agnostik	agnostik	k1gMnSc1	agnostik
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
poznat	poznat	k5eAaPmF	poznat
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
důležité	důležitý	k2eAgMnPc4d1	důležitý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
či	či	k8xC	či
příští	příští	k2eAgInSc1d1	příští
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ne	ne	k9	ne
<g/>
-li	i	k?	-li
naprosto	naprosto	k6eAd1	naprosto
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
nemožné	možný	k2eNgNnSc1d1	nemožné
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Připouští	připouštět	k5eAaImIp3nS	připouštět
však	však	k9	však
možnost	možnost	k1gFnSc4	možnost
změny	změna	k1gFnSc2	změna
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zcela	zcela	k6eAd1	zcela
průkazného	průkazný	k2eAgInSc2d1	průkazný
osobního	osobní	k2eAgInSc2d1	osobní
zážitku	zážitek	k1gInSc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Russell	Russell	k1gInSc1	Russell
ve	v	k7c6	v
slavném	slavný	k2eAgMnSc6d1	slavný
"	"	kIx"	"
<g/>
příkladě	příklad	k1gInSc6	příklad
o	o	k7c6	o
čajové	čajový	k2eAgFnSc6d1	čajová
konvici	konvice	k1gFnSc6	konvice
<g/>
"	"	kIx"	"
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
dogmatiky	dogmatik	k1gMnPc4	dogmatik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
skeptici	skeptik	k1gMnPc1	skeptik
jejich	jejich	k3xOp3gNnPc4	jejich
dogmata	dogma	k1gNnPc4	dogma
vyvraceli	vyvracet	k5eAaImAgMnP	vyvracet
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
dokazovali	dokazovat	k5eAaImAgMnP	dokazovat
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
Kdo	kdo	k3yInSc1	kdo
seje	sít	k5eAaImIp3nS	sít
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
Inherit	Inherit	k1gInSc1	Inherit
the	the	k?	the
Wind	Wind	k1gInSc1	Wind
<g/>
)	)	kIx)	)
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
r.	r.	kA	r.
1960	[number]	k4	1960
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgFnPc4d1	inspirovaná
tzv.	tzv.	kA	tzv.
opičím	opičí	k2eAgInSc7d1	opičí
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
typické	typický	k2eAgInPc4d1	typický
argumenty	argument	k1gInPc4	argument
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnSc1d1	státní
žalobce	žalobce	k1gMnSc1	žalobce
Brady	brada	k1gFnSc2	brada
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
ptá	ptat	k5eAaImIp3nS	ptat
obhájce	obhájce	k1gMnSc4	obhájce
a	a	k8xC	a
silného	silný	k2eAgMnSc4d1	silný
agnostika	agnostik	k1gMnSc4	agnostik
Drummonda	Drummond	k1gMnSc4	Drummond
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Copak	copak	k3yQnSc1	copak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
vůbec	vůbec	k9	vůbec
něco	něco	k3yInSc1	něco
svatého	svatý	k2eAgNnSc2d1	svaté
pro	pro	k7c4	pro
takového	takový	k3xDgMnSc4	takový
věhlasného	věhlasný	k2eAgMnSc4d1	věhlasný
agnostika	agnostik	k1gMnSc4	agnostik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jste	být	k5eAaImIp2nP	být
vy	vy	k3xPp2nPc1	vy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
A	a	k9	a
Drummond	Drummond	k1gInSc1	Drummond
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
!	!	kIx.	!
</s>
<s>
Jedinečnost	jedinečnost	k1gFnSc1	jedinečnost
lidského	lidský	k2eAgNnSc2d1	lidské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
dětské	dětský	k2eAgFnSc6d1	dětská
síle	síla	k1gFnSc6	síla
zvládnout	zvládnout	k5eAaPmF	zvládnout
násobilku	násobilka	k1gFnSc4	násobilka
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
posvátnosti	posvátnost	k1gFnSc2	posvátnost
než	než	k8xS	než
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vašich	váš	k3xOp2gInPc6	váš
výkřicích	výkřik	k1gInPc6	výkřik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Amen	amen	k1gNnSc1	amen
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Svatá	svatý	k2eAgFnSc1d1	svatá
svátost	svátost	k1gFnSc1	svátost
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hosana	hosana	k1gNnSc1	hosana
<g/>
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
monument	monument	k1gInSc1	monument
než	než	k8xS	než
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
rozvoj	rozvoj	k1gInSc4	rozvoj
lidského	lidský	k2eAgNnSc2d1	lidské
poznání	poznání	k1gNnSc2	poznání
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
zázrak	zázrak	k1gInSc1	zázrak
s	s	k7c7	s
holemi	hole	k1gFnPc7	hole
změněnými	změněný	k2eAgFnPc7d1	změněná
v	v	k7c4	v
hady	had	k1gMnPc4	had
nebo	nebo	k8xC	nebo
rozestoupení	rozestoupení	k1gNnSc4	rozestoupení
vod	voda	k1gFnPc2	voda
<g/>
!	!	kIx.	!
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
nás	my	k3xPp1nPc2	my
dovedl	dovést	k5eAaPmAgMnS	dovést
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
ohlédnout	ohlédnout	k5eAaPmF	ohlédnout
a	a	k8xC	a
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsme	být	k5eAaImIp1nP	být
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
nahlédnutí	nahlédnutí	k1gNnSc4	nahlédnutí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
poznání	poznání	k1gNnSc4	poznání
musíme	muset	k5eAaImIp1nP	muset
opustit	opustit	k5eAaPmF	opustit
naši	náš	k3xOp1gFnSc4	náš
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
poezii	poezie	k1gFnSc4	poezie
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Agnosticismus	agnosticismus	k1gInSc4	agnosticismus
je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
jak	jak	k6eAd1	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
teistů	teista	k1gMnPc2	teista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ateistů	ateista	k1gMnPc2	ateista
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
argumenty	argument	k1gInPc1	argument
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
opačná	opačný	k2eAgNnPc4d1	opačné
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Teisté	teista	k1gMnPc1	teista
i	i	k8xC	i
ateisté	ateista	k1gMnPc1	ateista
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
někdy	někdy	k6eAd1	někdy
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
agnosticismu	agnosticismus	k1gInSc3	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
britský	britský	k2eAgMnSc1d1	britský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
kněz	kněz	k1gMnSc1	kněz
Hugh	Hugh	k1gMnSc1	Hugh
Ross	Rossa	k1gFnPc2	Rossa
Williamson	Williamson	k1gMnSc1	Williamson
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
choval	chovat	k5eAaImAgMnS	chovat
v	v	k7c6	v
úctě	úcta	k1gFnSc6	úcta
oddané	oddaný	k2eAgFnSc2d1	oddaná
zbožné	zbožný	k2eAgFnSc2d1	zbožná
věřící	věřící	k1gFnSc2	věřící
a	a	k8xC	a
oddané	oddaný	k2eAgMnPc4d1	oddaný
ateisty	ateista	k1gMnPc4	ateista
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
své	svůj	k3xOyFgNnSc4	svůj
pohrdání	pohrdání	k1gNnSc4	pohrdání
si	se	k3xPyFc3	se
schovával	schovávat	k5eAaImAgMnS	schovávat
pro	pro	k7c4	pro
nanicovaté	nanicovatý	k2eAgMnPc4d1	nanicovatý
bezpáteřní	bezpáteřní	k2eAgMnPc4d1	bezpáteřní
tuctové	tuctový	k2eAgMnPc4d1	tuctový
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
plácají	plácat	k5eAaImIp3nP	plácat
kdesi	kdesi	k6eAd1	kdesi
uprostřed	uprostřed	k7c2	uprostřed
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
teisté	teista	k1gMnPc1	teista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ateisté	ateista	k1gMnPc1	ateista
přitom	přitom	k6eAd1	přitom
nepracují	pracovat	k5eNaImIp3nP	pracovat
s	s	k7c7	s
Huxleyho	Huxleyha	k1gFnSc5	Huxleyha
definicí	definice	k1gFnSc7	definice
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
jako	jako	k8xS	jako
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zásadně	zásadně	k6eAd1	zásadně
ho	on	k3xPp3gMnSc4	on
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k8xC	jako
střední	střední	k2eAgFnSc4d1	střední
pozici	pozice	k1gFnSc4	pozice
mezi	mezi	k7c7	mezi
teismem	teismus	k1gInSc7	teismus
a	a	k8xC	a
ateismem	ateismus	k1gInSc7	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Teisté	teista	k1gMnPc1	teista
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
vnímají	vnímat	k5eAaImIp3nP	vnímat
agnostiky	agnostik	k1gMnPc4	agnostik
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
slabé	slabý	k2eAgMnPc4d1	slabý
věřící	věřící	k1gMnPc4	věřící
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
je	on	k3xPp3gNnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
něcisty	něcist	k1gInPc4	něcist
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ateisté	ateista	k1gMnPc1	ateista
jako	jako	k8xC	jako
nevěřící	nevěřící	k1gMnPc1	nevěřící
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zatím	zatím	k6eAd1	zatím
ještě	ještě	k6eAd1	ještě
vše	všechen	k3xTgNnSc4	všechen
důkladně	důkladně	k6eAd1	důkladně
nepromysleli	promyslet	k5eNaPmAgMnP	promyslet
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
nerozhodní	rozhodný	k2eNgMnPc1d1	nerozhodný
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
náboženští	náboženský	k2eAgMnPc1d1	náboženský
myslitelé	myslitel	k1gMnPc1	myslitel
zavrhují	zavrhovat	k5eAaImIp3nP	zavrhovat
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
jeho	jeho	k3xOp3gFnSc6	jeho
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Religiózní	religiózní	k2eAgMnPc1d1	religiózní
učenci	učenec	k1gMnPc1	učenec
největších	veliký	k2eAgNnPc2d3	veliký
tří	tři	k4xCgNnPc2	tři
tzv.	tzv.	kA	tzv.
abrahámovských	abrahámovský	k2eAgNnPc2d1	abrahámovské
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
jisti	jist	k2eAgMnPc1d1	jist
možností	možnost	k1gFnSc7	možnost
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tak	tak	k6eAd1	tak
metafyzických	metafyzický	k2eAgFnPc2d1	metafyzická
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidské	lidský	k2eAgNnSc1d1	lidské
myšlení	myšlení	k1gNnSc1	myšlení
má	mít	k5eAaImIp3nS	mít
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
též	též	k9	též
ne-materiální	ateriální	k2eNgFnSc4d1	-materiální
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgFnSc4d1	duchovní
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
je	on	k3xPp3gNnSc4	on
vidět	vidět	k5eAaImF	vidět
či	či	k8xC	či
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
dotknout	dotknout	k5eAaPmF	dotknout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
toho	ten	k3xDgNnSc2	ten
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
entropie	entropie	k1gFnSc2	entropie
<g/>
,	,	kIx,	,
rozumu	rozum	k1gInSc2	rozum
nebo	nebo	k8xC	nebo
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Náboženští	náboženský	k2eAgMnPc1d1	náboženský
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Laurence	Laurenec	k1gMnSc4	Laurenec
B.	B.	kA	B.
Brown	Brown	k1gNnSc1	Brown
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gInSc1	Ronald
Tacelli	Tacelle	k1gFnSc4	Tacelle
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Kreeft	Kreeft	k1gMnSc1	Kreeft
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
četné	četný	k2eAgInPc4d1	četný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
Boží	boží	k2eAgFnSc4d1	boží
existenci	existence	k1gFnSc4	existence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
Bůh	bůh	k1gMnSc1	bůh
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Kreeft	Kreeft	k1gMnSc1	Kreeft
a	a	k8xC	a
Ronald	Ronald	k1gMnSc1	Ronald
Tacelli	Tacell	k1gMnSc3	Tacell
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádějí	uvádět	k5eAaImIp3nP	uvádět
20	[number]	k4	20
"	"	kIx"	"
<g/>
argumentů	argument	k1gInPc2	argument
<g/>
"	"	kIx"	"
Boží	boží	k2eAgFnSc1d1	boží
existence	existence	k1gFnSc1	existence
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
agnostický	agnostický	k2eAgInSc1d1	agnostický
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
důkaz	důkaz	k1gInSc4	důkaz
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
laboratorně	laboratorně	k6eAd1	laboratorně
ověřitelný	ověřitelný	k2eAgInSc1d1	ověřitelný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
výzvou	výzva	k1gFnSc7	výzva
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejvyššímu	vysoký	k2eAgNnSc3d3	nejvyšší
bytí	bytí	k1gNnSc3	bytí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lidským	lidský	k2eAgMnSc7d1	lidský
služebníkem	služebník	k1gMnSc7	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Argumentují	argumentovat	k5eAaImIp3nP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
Boha	bůh	k1gMnSc2	bůh
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
poznatelnými	poznatelný	k2eAgInPc7d1	poznatelný
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pod	pod	k7c7	pod
námi	my	k3xPp1nPc7	my
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
filozof	filozof	k1gMnSc1	filozof
Blaise	Blaise	k1gFnSc2	Blaise
Pascal	Pascal	k1gMnSc1	Pascal
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
opravdu	opravdu	k6eAd1	opravdu
neexistoval	existovat	k5eNaImAgInS	existovat
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
by	by	k9	by
agnostici	agnostik	k1gMnPc1	agnostik
zvážit	zvážit	k5eAaPmF	zvážit
argument	argument	k1gInSc1	argument
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Pascalova	Pascalův	k2eAgFnSc1d1	Pascalova
sázka	sázka	k1gFnSc1	sázka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nekonečně	konečně	k6eNd1	konečně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
bezpečnější	bezpečný	k2eAgNnSc1d2	bezpečnější
"	"	kIx"	"
<g/>
vsadit	vsadit	k5eAaPmF	vsadit
si	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
na	na	k7c4	na
Boží	boží	k2eAgFnSc4d1	boží
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
silný	silný	k2eAgInSc1d1	silný
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
<g/>
,	,	kIx,	,
úvaha	úvaha	k1gFnSc1	úvaha
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
a	a	k8xC	a
protiřečící	protiřečící	k2eAgFnSc1d1	protiřečící
si	se	k3xPyFc3	se
v	v	k7c4	v
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
rozumu	rozum	k1gInSc2	rozum
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
pravdy	pravda	k1gFnSc2	pravda
vědecké	vědecký	k2eAgFnSc2d1	vědecká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
již	již	k9	již
pravdy	pravda	k1gFnPc1	pravda
duchovní	duchovní	k2eAgFnPc1d1	duchovní
či	či	k8xC	či
filosofické	filosofický	k2eAgFnPc1d1	filosofická
<g/>
.	.	kIx.	.
</s>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
etiky	etika	k1gFnSc2	etika
z	z	k7c2	z
myšlení	myšlení	k1gNnSc2	myšlení
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nebezpečným	bezpečný	k2eNgFnPc3d1	nebezpečná
patologickým	patologický	k2eAgFnPc3d1	patologická
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
i	i	k9	i
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
k	k	k7c3	k
lidským	lidský	k2eAgFnPc3d1	lidská
i	i	k8xC	i
ekologickým	ekologický	k2eAgFnPc3d1	ekologická
katastrofám	katastrofa	k1gFnPc3	katastrofa
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
plodem	plod	k1gInSc7	plod
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
člověku	člověk	k1gMnSc3	člověk
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Možnost	možnost	k1gFnSc1	možnost
poznat	poznat	k5eAaPmF	poznat
Boha	bůh	k1gMnSc4	bůh
existovala	existovat	k5eAaImAgFnS	existovat
vždycky	vždycky	k6eAd1	vždycky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
Ratzingera	Ratzingero	k1gNnPc4	Ratzingero
je	být	k5eAaImIp3nS	být
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
volbou	volba	k1gFnSc7	volba
pohodlí	pohodlí	k1gNnSc2	pohodlí
<g/>
,	,	kIx,	,
pýchy	pýcha	k1gFnSc2	pýcha
<g/>
,	,	kIx,	,
dominance	dominance	k1gFnSc2	dominance
a	a	k8xC	a
užitku	užitek	k1gInSc2	užitek
před	před	k7c7	před
pravdou	pravda	k1gFnSc7	pravda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
postoji	postoj	k1gInSc3	postoj
co	co	k9	co
nejhorlivěji	horlivě	k6eAd3	horlivě
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
sebekritiku	sebekritika	k1gFnSc4	sebekritika
<g/>
,	,	kIx,	,
pokorně	pokorně	k6eAd1	pokorně
naslouchat	naslouchat	k5eAaImF	naslouchat
celku	celek	k1gInSc3	celek
jsoucna	jsoucno	k1gNnSc2	jsoucno
<g/>
,	,	kIx,	,
korigovat	korigovat	k5eAaBmF	korigovat
své	svůj	k3xOyFgFnPc4	svůj
vědecké	vědecký	k2eAgFnPc4d1	vědecká
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
být	být	k5eAaImF	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
očištění	očištění	k1gNnSc3	očištění
skrze	skrze	k?	skrze
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
teologů	teolog	k1gMnPc2	teolog
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
není	být	k5eNaImIp3nS	být
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
prakticky	prakticky	k6eAd1	prakticky
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
pouze	pouze	k6eAd1	pouze
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
Boha	bůh	k1gMnSc2	bůh
nebylo	být	k5eNaImAgNnS	být
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
etsi	etsit	k5eAaPmRp2nS	etsit
Deus	Deus	k1gInSc1	Deus
non	non	k?	non
daretur	daretura	k1gFnPc2	daretura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
etsi	etsi	k1gNnSc1	etsi
Deus	Deusa	k1gFnPc2	Deusa
daretur	daretura	k1gFnPc2	daretura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
učenci	učenec	k1gMnPc1	učenec
míní	mínit	k5eAaImIp3nP	mínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
nenávratným	návratný	k2eNgInSc7d1	nenávratný
krokem	krok	k1gInSc7	krok
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
nerozhodnost	nerozhodnost	k1gFnSc1	nerozhodnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
není	být	k5eNaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
vše	všechen	k3xTgNnSc1	všechen
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
účel	účel	k1gInSc4	účel
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ateismus	ateismus	k1gInSc4	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
křesťané	křesťan	k1gMnPc1	křesťan
také	také	k9	také
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
agnostik	agnostik	k1gMnSc1	agnostik
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
pokládat	pokládat	k5eAaImF	pokládat
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
za	za	k7c4	za
měřítko	měřítko	k1gNnSc4	měřítko
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zkušenost	zkušenost	k1gFnSc4	zkušenost
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zkušenosti	zkušenost	k1gFnSc2	zkušenost
s	s	k7c7	s
fungováním	fungování	k1gNnSc7	fungování
vlastního	vlastní	k2eAgInSc2d1	vlastní
rozumu	rozum	k1gInSc2	rozum
<g/>
)	)	kIx)	)
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
způsob	způsob	k1gInSc4	způsob
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
obraz	obraz	k1gInSc4	obraz
světa	svět	k1gInSc2	svět
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
přijatelný	přijatelný	k2eAgInSc4d1	přijatelný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dovozují	dovozovat	k5eAaImIp3nP	dovozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
serióznost	serióznost	k1gFnSc4	serióznost
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
poctivost	poctivost	k1gFnSc4	poctivost
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
brání	bránit	k5eAaImIp3nP	bránit
brát	brát	k5eAaImF	brát
vážně	vážně	k6eAd1	vážně
vlastní	vlastní	k2eAgNnSc4d1	vlastní
"	"	kIx"	"
<g/>
tušení	tušení	k1gNnSc4	tušení
"	"	kIx"	"
<g/>
něčeho	něco	k3yInSc2	něco
za	za	k7c4	za
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgMnSc7	všecek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
něčeho	něco	k3yInSc2	něco
svrchovaně	svrchovaně	k6eAd1	svrchovaně
významného	významný	k2eAgNnSc2d1	významné
<g/>
"	"	kIx"	"
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zdráhá	zdráhat	k5eAaImIp3nS	zdráhat
"	"	kIx"	"
<g/>
opustit	opustit	k5eAaPmF	opustit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
učinit	učinit	k5eAaPmF	učinit
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
krok	krok	k1gInSc4	krok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1	stoupenec
hnutí	hnutí	k1gNnSc2	hnutí
Hare	Hare	k1gNnSc2	Hare
Kršna	Kršna	k1gMnSc1	Kršna
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
částí	část	k1gFnPc2	část
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
verifikovány	verifikovat	k5eAaBmNgInP	verifikovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smyslově-racionálním	smyslověacionální	k2eAgInSc6d1	smyslově-racionální
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
na	na	k7c4	na
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
konečné	konečný	k2eAgNnSc4d1	konečné
poznání	poznání	k1gNnSc4	poznání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
metodě	metoda	k1gFnSc3	metoda
dosažení	dosažení	k1gNnSc2	dosažení
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
jaksi	jaksi	k6eAd1	jaksi
mlhavé	mlhavý	k2eAgNnSc4d1	mlhavé
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
neurčité	určitý	k2eNgNnSc1d1	neurčité
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
zatímco	zatímco	k8xS	zatímco
poznání	poznání	k1gNnSc1	poznání
ve	v	k7c6	v
védské	védský	k2eAgFnSc6d1	védská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
sestupného	sestupný	k2eAgInSc2d1	sestupný
<g/>
,	,	kIx,	,
deduktivního	deduktivní	k2eAgInSc2d1	deduktivní
a	a	k8xC	a
nespekulativního	spekulativní	k2eNgInSc2d1	spekulativní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
celku	celek	k1gInSc2	celek
usuzuji	usuzovat	k5eAaImIp1nS	usuzovat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
védské	védský	k2eAgFnSc6d1	védská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
Pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Metodou	metoda	k1gFnSc7	metoda
poznání	poznání	k1gNnSc2	poznání
je	být	k5eAaImIp3nS	být
dotazování	dotazování	k1gNnSc1	dotazování
se	se	k3xPyFc4	se
na	na	k7c4	na
Absolutní	absolutní	k2eAgFnSc4d1	absolutní
Pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyjevena	vyjevit	k5eAaPmNgFnS	vyjevit
před	před	k7c7	před
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
a	a	k8xC	a
tradovala	tradovat	k5eAaImAgFnS	tradovat
se	se	k3xPyFc4	se
ústním	ústní	k2eAgNnSc7d1	ústní
podáním	podání	k1gNnSc7	podání
z	z	k7c2	z
učitele	učitel	k1gMnSc2	učitel
na	na	k7c4	na
žáka	žák	k1gMnSc4	žák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Marxisté	marxista	k1gMnPc1	marxista
vždy	vždy	k6eAd1	vždy
odmítali	odmítat	k5eAaImAgMnP	odmítat
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
zcela	zcela	k6eAd1	zcela
zásadně	zásadně	k6eAd1	zásadně
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
již	již	k6eAd1	již
Marxovou	Marxův	k2eAgFnSc7d1	Marxova
a	a	k8xC	a
Engelsovou	Engelsův	k2eAgFnSc7d1	Engelsova
kritikou	kritika	k1gFnSc7	kritika
Huma	Humus	k1gMnSc2	Humus
a	a	k8xC	a
Kanta	Kant	k1gMnSc2	Kant
a	a	k8xC	a
následně	následně	k6eAd1	následně
Leninovou	Leninův	k2eAgFnSc7d1	Leninova
kritikou	kritika	k1gFnSc7	kritika
moderního	moderní	k2eAgInSc2d1	moderní
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
marxisty	marxista	k1gMnPc4	marxista
"	"	kIx"	"
<g/>
podstata	podstata	k1gFnSc1	podstata
poznání	poznání	k1gNnSc2	poznání
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
odrážení	odrážení	k1gNnSc6	odrážení
objektivního	objektivní	k2eAgInSc2d1	objektivní
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
vědomí	vědomí	k1gNnSc6	vědomí
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgNnSc1d1	vědecké
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
odraz	odraz	k1gInSc1	odraz
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
teoretické	teoretický	k2eAgNnSc4d1	teoretické
přetvoření	přetvoření	k1gNnSc4	přetvoření
objektivní	objektivní	k2eAgFnSc2d1	objektivní
reality	realita	k1gFnSc2	realita
ve	v	k7c4	v
vědomí	vědomí	k1gNnSc4	vědomí
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
poznání	poznání	k1gNnSc2	poznání
od	od	k7c2	od
jevů	jev	k1gInPc2	jev
k	k	k7c3	k
podstatě	podstata	k1gFnSc3	podstata
věcí	věc	k1gFnPc2	věc
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
Lenina	Lenin	k1gMnSc2	Lenin
"	"	kIx"	"
<g/>
od	od	k7c2	od
živého	živý	k2eAgNnSc2d1	živé
nazírání	nazírání	k1gNnSc2	nazírání
k	k	k7c3	k
abstraktnímu	abstraktní	k2eAgNnSc3d1	abstraktní
myšlení	myšlení	k1gNnSc3	myšlení
a	a	k8xC	a
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
k	k	k7c3	k
praxi	praxe	k1gFnSc3	praxe
-	-	kIx~	-
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
dialektická	dialektický	k2eAgFnSc1d1	dialektická
cesta	cesta	k1gFnSc1	cesta
poznání	poznání	k1gNnSc2	poznání
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
poznání	poznání	k1gNnSc2	poznání
objektivní	objektivní	k2eAgFnSc2d1	objektivní
reality	realita	k1gFnSc2	realita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
marxistů	marxista	k1gMnPc2	marxista
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
vědeckým	vědecký	k2eAgInSc7d1	vědecký
světovým	světový	k2eAgInSc7d1	světový
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
objektivní	objektivní	k2eAgFnSc7d1	objektivní
pravdou	pravda	k1gFnSc7	pravda
a	a	k8xC	a
materialistickou	materialistický	k2eAgFnSc7d1	materialistická
dialektikou	dialektika	k1gFnSc7	dialektika
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ateisté	ateista	k1gMnPc1	ateista
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
agnosticismus	agnosticismus	k1gInSc1	agnosticismus
je	být	k5eAaImIp3nS	být
vnitřně	vnitřně	k6eAd1	vnitřně
rozporný	rozporný	k2eAgInSc1d1	rozporný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
nepoznatelnosti	nepoznatelnost	k1gFnSc6	nepoznatelnost
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
výsledkem	výsledek	k1gInSc7	výsledek
nějakého	nějaký	k3yIgNnSc2	nějaký
poznání	poznání	k1gNnSc2	poznání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
etické	etický	k2eAgFnSc2d1	etická
konsekvence	konsekvence	k1gFnSc2	konsekvence
-	-	kIx~	-
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
všem	všecek	k3xTgInPc3	všecek
totalitním	totalitní	k2eAgInPc3d1	totalitní
režimům	režim	k1gInPc3	režim
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejvýraznější	výrazný	k2eAgMnSc1d3	nejvýraznější
současný	současný	k2eAgMnSc1d1	současný
obhájce	obhájce	k1gMnSc1	obhájce
ateismu	ateismus	k1gInSc2	ateismus
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
teismu	teismus	k1gInSc3	teismus
(	(	kIx(	(
<g/>
odmítající	odmítající	k2eAgFnSc3d1	odmítající
toleranci	tolerance	k1gFnSc3	tolerance
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc4	Dawkins
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
agnostici	agnostik	k1gMnPc1	agnostik
se	se	k3xPyFc4	se
dopuští	dopuštit	k5eAaPmIp3nP	dopuštit
logické	logický	k2eAgFnPc1d1	logická
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
když	když	k8xS	když
otázku	otázka	k1gFnSc4	otázka
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
PAP	PAP	kA	PAP
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hypotézám	hypotéza	k1gFnPc3	hypotéza
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
neexistence	neexistence	k1gFnSc2	neexistence
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
stejnou	stejný	k2eAgFnSc4d1	stejná
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
zařadili	zařadit	k5eAaPmAgMnP	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
TAP	TAP	kA	TAP
a	a	k8xC	a
zvážili	zvážit	k5eAaPmAgMnP	zvážit
by	by	kYmCp3nP	by
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
obou	dva	k4xCgFnPc2	dva
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
prakticky	prakticky	k6eAd1	prakticky
ateisty	ateista	k1gMnPc4	ateista
<g/>
.	.	kIx.	.
</s>
<s>
Dawkins	Dawkins	k6eAd1	Dawkins
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xC	jako
příklad	příklad	k1gInSc4	příklad
parodické	parodický	k2eAgNnSc4d1	parodické
náboženství	náboženství	k1gNnSc4	náboženství
uctívající	uctívající	k2eAgNnSc4d1	uctívající
Létající	létající	k2eAgNnSc4d1	létající
špagetové	špagetový	k2eAgNnSc4d1	špagetové
monstrum	monstrum	k1gNnSc4	monstrum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
pastafariáni	pastafarián	k1gMnPc1	pastafarián
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
v	v	k7c6	v
USA	USA	kA	USA
formálně	formálně	k6eAd1	formálně
zažádali	zažádat	k5eAaPmAgMnP	zažádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
špagetový	špagetový	k2eAgInSc1d1	špagetový
monsterismus	monsterismus	k1gInSc1	monsterismus
vyučován	vyučovat	k5eAaImNgInS	vyučovat
jako	jako	k8xC	jako
další	další	k2eAgInSc1d1	další
rovnocenný	rovnocenný	k2eAgInSc1d1	rovnocenný
názor	názor	k1gInSc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Dawkinsova	Dawkinsův	k2eAgInSc2d1	Dawkinsův
názoru	názor	k1gInSc2	názor
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
existence	existence	k1gFnSc1	existence
špagetového	špagetový	k2eAgNnSc2d1	špagetové
monstra	monstrum	k1gNnSc2	monstrum
(	(	kIx(	(
<g/>
Russelovy	Russelův	k2eAgFnSc2d1	Russelova
čajové	čajový	k2eAgFnSc2d1	čajová
konvice	konvice	k1gFnSc2	konvice
<g/>
,	,	kIx,	,
jednorožce	jednorožec	k1gMnSc2	jednorožec
<g/>
,	,	kIx,	,
rusalek	rusalka	k1gFnPc2	rusalka
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
jejich	jejich	k3xOp3gFnSc2	jejich
neexistence	neexistence	k1gFnSc2	neexistence
a	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
Abrahámovského	abrahámovský	k2eAgMnSc4d1	abrahámovský
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
považuje	považovat	k5eAaImIp3nS	považovat
Dawkins	Dawkins	k1gInSc4	Dawkins
agnosticismus	agnosticismus	k1gInSc4	agnosticismus
za	za	k7c4	za
chybný	chybný	k2eAgInSc4d1	chybný
v	v	k7c6	v
samé	samý	k3xTgFnSc6	samý
jeho	jeho	k3xOp3gFnSc6	jeho
podstatě	podstata	k1gFnSc6	podstata
<g/>
.	.	kIx.	.
</s>
