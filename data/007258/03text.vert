<s>
Čelist	čelist	k1gFnSc1	čelist
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
čelisti	čelist	k1gFnSc2	čelist
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
protistojných	protistojný	k2eAgFnPc2d1	protistojný
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
úst	ústa	k1gNnPc2	ústa
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
blízko	blízko	k7c2	blízko
nich	on	k3xPp3gFnPc2	on
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
a	a	k8xC	a
drcení	drcení	k1gNnSc3	drcení
(	(	kIx(	(
<g/>
kousání	kousání	k1gNnPc1	kousání
<g/>
,	,	kIx,	,
žvýkání	žvýkání	k1gNnPc1	žvýkání
<g/>
)	)	kIx)	)
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Čelist	čelist	k1gFnSc1	čelist
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
čelistnatí	čelistnatý	k2eAgMnPc1d1	čelistnatý
<g/>
,	,	kIx,	,
Gnathostomata	Gnathostoma	k1gNnPc1	Gnathostoma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezávisle	závisle	k6eNd1	závisle
i	i	k9	i
u	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Čelistnatci	čelistnatec	k1gMnSc3	čelistnatec
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
čelisti	čelist	k1gFnPc4	čelist
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obratlovci	obratlovec	k1gMnPc1	obratlovec
s	s	k7c7	s
čelistmi	čelist	k1gFnPc7	čelist
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
siluru	silur	k1gInSc2	silur
<g/>
.	.	kIx.	.
</s>
<s>
Sesterskou	sesterský	k2eAgFnSc7d1	sesterská
skupinou	skupina	k1gFnSc7	skupina
čelistnatců	čelistnatec	k1gMnPc2	čelistnatec
jsou	být	k5eAaImIp3nP	být
bezčelistnatci	bezčelistnatec	k1gMnPc1	bezčelistnatec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zejména	zejména	k9	zejména
kruhoústí	kruhoústí	k1gMnPc1	kruhoústí
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
čelisti	čelist	k1gFnPc4	čelist
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čelistnatým	čelistnatý	k2eAgMnPc3d1	čelistnatý
obratlovcům	obratlovec	k1gMnPc3	obratlovec
patří	patřit	k5eAaImIp3nP	patřit
paryby	paryba	k1gFnPc1	paryba
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
čtyřnožci	čtyřnožec	k1gMnPc1	čtyřnožec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
savci	savec	k1gMnPc1	savec
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
rybovitých	rybovitý	k2eAgMnPc2d1	rybovitý
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
k	k	k7c3	k
obratlovcům	obratlovec	k1gMnPc3	obratlovec
bez	bez	k7c2	bez
čelistí	čelist	k1gFnPc2	čelist
patří	patřit	k5eAaImIp3nP	patřit
sliznatky	sliznatka	k1gFnPc1	sliznatka
a	a	k8xC	a
mihule	mihule	k1gFnPc1	mihule
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zástupci	zástupce	k1gMnPc1	zástupce
kruhoústých	kruhoústí	k1gMnPc2	kruhoústí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
také	také	k9	také
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přeměnou	přeměna	k1gFnSc7	přeměna
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Čelisti	čelist	k1gFnPc1	čelist
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
(	(	kIx(	(
<g/>
maxila	maxila	k1gFnSc1	maxila
<g/>
)	)	kIx)	)
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
(	(	kIx(	(
<g/>
mandibula	mandibula	k1gFnSc1	mandibula
<g/>
)	)	kIx)	)
čelisti	čelist	k1gFnPc1	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
čelist	čelist	k1gFnSc1	čelist
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
fixovaná	fixovaný	k2eAgFnSc1d1	fixovaná
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
většinou	většinou	k6eAd1	většinou
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
chrup	chrup	k1gInSc4	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Čelisti	čelist	k1gFnPc1	čelist
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ze	z	k7c2	z
žaberních	žaberní	k2eAgInPc2d1	žaberní
oblouků	oblouk	k1gInPc2	oblouk
<g/>
;	;	kIx,	;
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
to	ten	k3xDgNnSc4	ten
studium	studium	k1gNnSc4	studium
kostí	kost	k1gFnPc2	kost
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
palatoquadratum	palatoquadratum	k1gNnSc4	palatoquadratum
<g/>
)	)	kIx)	)
a	a	k8xC	a
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
(	(	kIx(	(
<g/>
mandibulare	mandibular	k1gMnSc5	mandibular
či	či	k8xC	či
Meckelův	Meckelův	k2eAgInSc1d1	Meckelův
element	element	k1gInSc1	element
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
stavbě	stavba	k1gFnSc6	stavba
podílí	podílet	k5eAaImIp3nS	podílet
třetí	třetí	k4xOgInSc1	třetí
žaberní	žaberní	k2eAgInSc1d1	žaberní
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
čelistmi	čelist	k1gFnPc7	čelist
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
buď	buď	k8xC	buď
v	v	k7c6	v
dermálních	dermální	k2eAgFnPc6d1	dermální
kostech	kost	k1gFnPc6	kost
(	(	kIx(	(
<g/>
na	na	k7c6	na
maxile	maxila	k1gFnSc6	maxila
<g/>
,	,	kIx,	,
premaxile	premaxila	k1gFnSc6	premaxila
<g/>
,	,	kIx,	,
dentale	dental	k1gMnSc5	dental
a	a	k8xC	a
coronoidu	coronoid	k1gInSc6	coronoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
palatoquadratu	palatoquadrat	k1gInSc6	palatoquadrat
a	a	k8xC	a
mandibulare	mandibular	k1gMnSc5	mandibular
(	(	kIx(	(
<g/>
dermální	dermální	k2eAgFnPc1d1	dermální
kosti	kost	k1gFnPc1	kost
nemají	mít	k5eNaImIp3nP	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žaberní	žaberní	k2eAgInPc1d1	žaberní
oblouky	oblouk	k1gInPc1	oblouk
–	–	k?	–
arci	arci	k0	arci
branchiales	branchiales	k1gMnSc1	branchiales
jsou	být	k5eAaImIp3nP	být
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
lidského	lidský	k2eAgNnSc2d1	lidské
splanchnokrania	splanchnokranium	k1gNnSc2	splanchnokranium
–	–	k?	–
obličejové	obličejový	k2eAgFnSc2d1	obličejová
části	část	k1gFnSc2	část
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
žaberní	žaberní	k2eAgInSc1d1	žaberní
oblouk	oblouk	k1gInSc1	oblouk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
čelistním	čelistní	k2eAgMnSc7d1	čelistní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
lidskou	lidský	k2eAgFnSc4d1	lidská
praemaxillu	praemaxilla	k1gFnSc4	praemaxilla
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
horního	horní	k2eAgNnSc2d1	horní
alveolárního	alveolární	k2eAgNnSc2d1	alveolární
(	(	kIx(	(
<g/>
čelistního	čelistní	k2eAgMnSc2d1	čelistní
<g/>
)	)	kIx)	)
výběžku	výběžek	k1gInSc2	výběžek
<g/>
,	,	kIx,	,
od	od	k7c2	od
špičáku	špičák	k1gInSc2	špičák
po	po	k7c6	po
<g />
.	.	kIx.	.
</s>
<s>
špičák	špičák	k1gInSc1	špičák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maxillu	maxillat	k5eAaPmIp1nS	maxillat
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
alveolárního	alveolární	k2eAgInSc2d1	alveolární
výběžku	výběžek	k1gInSc2	výběžek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
os	osa	k1gFnPc2	osa
zygomaticum	zygomaticum	k1gInSc1	zygomaticum
(	(	kIx(	(
<g/>
lícní	lícní	k2eAgFnSc1d1	lícní
kost	kost	k1gFnSc1	kost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
os	osa	k1gFnPc2	osa
pallatum	pallatum	k1gNnSc1	pallatum
(	(	kIx(	(
<g/>
patrovou	patrový	k2eAgFnSc4d1	patrová
kost	kost	k1gFnSc4	kost
<g/>
)	)	kIx)	)
a	a	k8xC	a
incus	incus	k1gInSc4	incus
(	(	kIx(	(
<g/>
kovadlinku	kovadlinka	k1gFnSc4	kovadlinka
–	–	k?	–
ušní	ušní	k2eAgFnSc1d1	ušní
kůstka	kůstka	k1gFnSc1	kůstka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
žaberního	žaberní	k2eAgInSc2d1	žaberní
oblouku	oblouk	k1gInSc2	oblouk
tvoří	tvořit	k5eAaImIp3nP	tvořit
mandibulu	mandibula	k1gFnSc4	mandibula
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc4d1	dolní
čelist	čelist	k1gFnSc4	čelist
<g/>
)	)	kIx)	)
a	a	k8xC	a
malleus	malleus	k1gInSc1	malleus
(	(	kIx(	(
<g/>
kladívko	kladívko	k1gNnSc1	kladívko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
žaberní	žaberní	k2eAgInSc1d1	žaberní
oblouk	oblouk	k1gInSc1	oblouk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hyoidní	hyoidní	k2eAgInSc1d1	hyoidní
–	–	k?	–
jazylkový	jazylkový	k2eAgInSc1d1	jazylkový
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
stapes	stapes	k1gInSc1	stapes
(	(	kIx(	(
<g/>
třmínek	třmínek	k1gInSc1	třmínek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
processus	processus	k1gMnSc1	processus
styloideus	styloideus	k1gMnSc1	styloideus
ossis	ossis	k1gFnSc1	ossis
temporalis	temporalis	k1gFnSc1	temporalis
(	(	kIx(	(
<g/>
bodcovitý	bodcovitý	k2eAgInSc1d1	bodcovitý
výběžek	výběžek	k1gInSc1	výběžek
kosti	kost	k1gFnSc2	kost
<g />
.	.	kIx.	.
</s>
<s>
spánkové	spánkový	k2eAgInPc4d1	spánkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
liggamentum	liggamentum	k1gNnSc1	liggamentum
stylohyoideum	stylohyoideum	k1gInSc1	stylohyoideum
(	(	kIx(	(
<g/>
vaz	vaz	k1gInSc1	vaz
mezi	mezi	k7c7	mezi
bodcovitým	bodcovitý	k2eAgInSc7d1	bodcovitý
výběžkem	výběžek	k1gInSc7	výběžek
a	a	k8xC	a
jazylkou	jazylka	k1gFnSc7	jazylka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cornu	cornout	k5eAaPmIp1nS	cornout
minus	minus	k1gInSc1	minus
(	(	kIx(	(
<g/>
malý	malý	k2eAgInSc1d1	malý
roh	roh	k1gInSc1	roh
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
corpis	corpis	k1gFnSc1	corpis
ossis	ossis	k1gFnSc2	ossis
hyoidei	hyoide	k1gFnSc2	hyoide
(	(	kIx(	(
<g/>
tělo	tělo	k1gNnSc1	tělo
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
žaberní	žaberní	k2eAgInSc1d1	žaberní
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
nenese	nést	k5eNaImIp3nS	nést
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g />
.	.	kIx.	.
</s>
<s>
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
cornu	cornout	k5eAaImIp1nS	cornout
majus	majus	k1gInSc1	majus
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
roh	roh	k1gInSc1	roh
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
jazylky	jazylka	k1gFnSc2	jazylka
corpus	corpus	k1gNnSc2	corpus
ossis	ossis	k1gFnSc2	ossis
hyoidei	hyoide	k1gFnSc2	hyoide
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
žaberní	žaberní	k2eAgInSc1d1	žaberní
oblouk	oblouk	k1gInSc1	oblouk
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
chrupavek	chrupavka	k1gFnPc2	chrupavka
hrtanu	hrtan	k1gInSc2	hrtan
–	–	k?	–
štítné	štítný	k2eAgNnSc1d1	Štítné
–	–	k?	–
cartilago	cartilago	k1gNnSc1	cartilago
thyroidea	thyroide	k1gInSc2	thyroide
a	a	k8xC	a
prstenčité	prstenčitý	k2eAgFnSc2d1	prstenčitý
–	–	k?	–
cartilago	cartilago	k1gMnSc1	cartilago
cricoidea	cricoidea	k1gMnSc1	cricoidea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
jaw	jawa	k1gFnPc2	jawa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čelist	čelist	k1gFnSc1	čelist
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
