<s>
Čelist	čelist	k1gFnSc1	čelist
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
čelistnatí	čelistnatý	k2eAgMnPc1d1	čelistnatý
<g/>
,	,	kIx,	,
Gnathostomata	Gnathostoma	k1gNnPc1	Gnathostoma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezávisle	závisle	k6eNd1	závisle
i	i	k9	i
u	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
