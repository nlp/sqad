<p>
<s>
Winkler	Winkler	k1gMnSc1	Winkler
County	Counta	k1gFnSc2	Counta
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Texas	Texas	k1gInSc1	Texas
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
7	[number]	k4	7
110	[number]	k4	110
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Správním	správní	k2eAgNnSc7d1	správní
městem	město	k1gNnSc7	město
okresu	okres	k1gInSc2	okres
je	být	k5eAaImIp3nS	být
Kermit	Kermit	k1gInSc1	Kermit
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
okresu	okres	k1gInSc2	okres
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
178	[number]	k4	178
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Winkler	Winkler	k1gMnSc1	Winkler
County	Counta	k1gFnPc4	Counta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
