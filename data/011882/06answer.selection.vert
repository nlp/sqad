<s>
Vlčí	vlčí	k2eAgFnSc1d1	vlčí
srst	srst	k1gFnSc1	srst
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hustými	hustý	k2eAgInPc7d1	hustý
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
podsada	podsada	k1gFnSc1	podsada
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
</s>
