<s>
Tequila	tequila	k1gFnSc1	tequila
Sunrise	Sunrise	k1gFnSc2	Sunrise
je	být	k5eAaImIp3nS	být
koktejl	koktejl	k1gInSc4	koktejl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tequily	tequila	k1gFnSc2	tequila
<g/>
,	,	kIx,	,
pomerančového	pomerančový	k2eAgInSc2d1	pomerančový
džusu	džus	k1gInSc2	džus
a	a	k8xC	a
grenadiny	grenadina	k1gFnSc2	grenadina
<g/>
.	.	kIx.	.
</s>
<s>
Drink	drink	k1gInSc1	drink
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
servírovaný	servírovaný	k2eAgInSc1d1	servírovaný
v	v	k7c6	v
Arizona	Arizona	k1gFnSc1	Arizona
Biltmore	Biltmor	k1gInSc5	Biltmor
Hotel	hotel	k1gInSc1	hotel
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
pozdních	pozdní	k2eAgNnPc2d1	pozdní
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tequila	tequila	k1gFnSc1	tequila
Sunrise	Sunrise	k1gFnSc1	Sunrise
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
díky	díky	k7c3	díky
tequile	tequila	k1gFnSc3	tequila
a	a	k8xC	a
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
východ	východ	k1gInSc4	východ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
led	led	k1gInSc4	led
nalijeme	nalít	k5eAaPmIp1nP	nalít
4,5	[number]	k4	4,5
cl	cl	k?	cl
Tequily	tequila	k1gFnSc2	tequila
a	a	k8xC	a
9	[number]	k4	9
cl	cl	k?	cl
pomerančového	pomerančový	k2eAgInSc2d1	pomerančový
džusu	džus	k1gInSc2	džus
<g/>
.	.	kIx.	.
</s>
<s>
Přidáme	přidat	k5eAaPmIp1nP	přidat
1,5	[number]	k4	1,5
cl	cl	k?	cl
grenadiny	grenadina	k1gFnSc2	grenadina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
klesne	klesnout	k5eAaPmIp3nS	klesnout
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Nemícháme	míchat	k5eNaImIp1nP	míchat
<g/>
.	.	kIx.	.
</s>
<s>
Tequila	tequila	k1gFnSc1	tequila
Sunset	Sunset	k1gMnSc1	Sunset
mojito	mojit	k2eAgNnSc4d1	mojito
Cuba	Cubum	k1gNnSc2	Cubum
Libre	Libr	k1gMnSc5	Libr
</s>
