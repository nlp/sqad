<p>
<s>
Stefani	Stefan	k1gMnPc1	Stefan
Joanne	Joann	k1gInSc5	Joann
Angelina	Angelin	k2eAgNnPc4d1	Angelino
Germanotta	Germanotto	k1gNnPc4	Germanotto
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1986	[number]	k4	1986
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
City	city	k1gNnSc2	city
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamá	znamat	k5eAaPmIp3nS	znamat
především	především	k9	především
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Lady	Lada	k1gFnSc2	Lada
Gaga	Gag	k1gInSc2	Gag
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
,	,	kIx,	,
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
producentka	producentka	k1gFnSc1	producentka
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
umělecký	umělecký	k2eAgInSc1d1	umělecký
pseudonym	pseudonym	k1gInSc1	pseudonym
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
Rob	roba	k1gFnPc2	roba
Fusari	Fusar	k1gFnSc2	Fusar
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
producentů	producent	k1gMnPc2	producent
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc4	radio
Ga	Ga	k1gFnPc1	Ga
Ga	Ga	k1gMnPc2	Ga
<g/>
"	"	kIx"	"
od	od	k7c2	od
Queen	Quena	k1gFnPc2	Quena
<g/>
.	.	kIx.	.
</s>
<s>
Telefon	telefon	k1gInSc4	telefon
automaticky	automaticky	k6eAd1	automaticky
opravil	opravit	k5eAaPmAgMnS	opravit
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
radio	radio	k1gNnSc4	radio
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
lady	lady	k1gFnSc4	lady
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zprávu	zpráva	k1gFnSc4	zpráva
dostala	dostat	k5eAaPmAgFnS	dostat
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
Lady	lady	k1gFnSc3	lady
Gaga	Gag	k1gInSc2	Gag
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravé	pravý	k2eAgNnSc1d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
začala	začít	k5eAaPmAgFnS	začít
psaním	psaní	k1gNnSc7	psaní
písniček	písnička	k1gFnPc2	písnička
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
zpěvačky	zpěvačka	k1gFnPc4	zpěvačka
jako	jako	k8xS	jako
Britney	Britney	k1gInPc4	Britney
Spears	Spearsa	k1gFnPc2	Spearsa
nebo	nebo	k8xC	nebo
Jennifer	Jennifra	k1gFnPc2	Jennifra
Lopez	Lopeza	k1gFnPc2	Lopeza
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
songwriter	songwriter	k1gInSc4	songwriter
pro	pro	k7c4	pro
Sony	Sony	kA	Sony
<g/>
/	/	kIx~	/
<g/>
ATV	ATV	kA	ATV
Music	Music	k1gMnSc1	Music
Publishing	Publishing	k1gInSc1	Publishing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
Julliard	Julliard	k1gInSc4	Julliard
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tisch	Tisch	k1gInSc1	Tisch
School	School	k1gInSc1	School
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
říjnu	říjen	k1gInSc3	říjen
2016	[number]	k4	2016
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
odhad	odhad	k1gInSc1	odhad
prodaných	prodaný	k2eAgFnPc2d1	prodaná
nahrávek	nahrávka	k1gFnPc2	nahrávka
přes	přes	k7c4	přes
30	[number]	k4	30
miliónů	milión	k4xCgInPc2	milión
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
150	[number]	k4	150
miliónů	milión	k4xCgInPc2	milión
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
interpretku	interpretka	k1gFnSc4	interpretka
řadilo	řadit	k5eAaImAgNnS	řadit
mezi	mezi	k7c4	mezi
nejlépe	dobře	k6eAd3	dobře
prodávající	prodávající	k2eAgMnPc4d1	prodávající
zpěváky	zpěvák	k1gMnPc4	zpěvák
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Časopisy	časopis	k1gInPc1	časopis
Time	Time	k1gFnPc2	Time
a	a	k8xC	a
Forbes	forbes	k1gInSc1	forbes
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zařadily	zařadit	k5eAaPmAgFnP	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osobností	osobnost	k1gFnPc2	osobnost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
100	[number]	k4	100
největších	veliký	k2eAgFnPc2d3	veliký
celebrit	celebrita	k1gFnPc2	celebrita
(	(	kIx(	(
<g/>
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Forbes	forbes	k1gInSc1	forbes
ji	on	k3xPp3gFnSc4	on
navíc	navíc	k6eAd1	navíc
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
jako	jako	k9	jako
7	[number]	k4	7
<g/>
.	.	kIx.	.
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
ženu	žena	k1gFnSc4	žena
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
nejmocnější	mocný	k2eAgFnSc4d3	nejmocnější
ženu	žena	k1gFnSc4	žena
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
obdržela	obdržet	k5eAaPmAgFnS	obdržet
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
Cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
třináct	třináct	k4xCc4	třináct
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Board	Board	k1gMnSc1	Board
of	of	k?	of
Review	Review	k1gMnSc1	Review
a	a	k8xC	a
Critics	Critics	k1gInSc1	Critics
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
2	[number]	k4	2
Zlaté	zlatý	k2eAgInPc1d1	zlatý
glóby	glóbus	k1gInPc1	glóbus
za	za	k7c4	za
roli	role	k1gFnSc4	role
hraběnky	hraběnka	k1gFnSc2	hraběnka
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
American	American	k1gInSc1	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
píseň	píseň	k1gFnSc1	píseň
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
9	[number]	k4	9
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Til	til	k1gInSc1	til
It	It	k1gFnSc2	It
Happens	Happensa	k1gFnPc2	Happensa
To	to	k9	to
You	You	k1gMnPc2	You
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
The	The	k1gMnPc2	The
Hunting	Hunting	k1gInSc1	Hunting
Ground	Grounda	k1gFnPc2	Grounda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
<g/>
:	:	kIx,	:
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
/	/	kIx~	/
<g/>
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gNnPc2	on
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
402	[number]	k4	402
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proslavila	proslavit	k5eAaPmAgFnS	proslavit
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
debutovým	debutový	k2eAgNnSc7d1	debutové
albem	album	k1gNnSc7	album
The	The	k1gMnSc2	The
Fame	Fam	k1gMnSc2	Fam
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc1	poker
Face	Fac	k1gInSc2	Fac
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
EP	EP	kA	EP
The	The	k1gFnSc1	The
Fame	Fam	k1gInSc2	Fam
Monster	monstrum	k1gNnPc2	monstrum
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
singly	singl	k1gInPc7	singl
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc1	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gInSc5	Telephon
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Alejandro	Alejandra	k1gFnSc5	Alejandra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Born	Borna	k1gFnPc2	Borna
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
žebříčků	žebříček	k1gInPc2	žebříček
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
včetně	včetně	k7c2	včetně
pilotního	pilotní	k2eAgInSc2d1	pilotní
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Born	Born	k1gInSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
ARTPOP	ARTPOP	kA	ARTPOP
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydala	vydat	k5eAaPmAgFnS	vydat
jazzové	jazzový	k2eAgNnSc4d1	jazzové
album	album	k1gNnSc4	album
Cheek	Cheek	k6eAd1	Cheek
To	to	k9	to
Cheek	Cheek	k6eAd1	Cheek
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
s	s	k7c7	s
Tony	Tony	k1gMnSc1	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
artRAVE	artRAVE	k?	artRAVE
The	The	k1gMnSc1	The
ARTPOP	ARTPOP	kA	ARTPOP
Ball	Ball	k1gMnSc1	Ball
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
hudebním	hudební	k2eAgInSc7d1	hudební
magazínem	magazín	k1gInSc7	magazín
Billboard	billboard	k1gInSc1	billboard
Ženou	hnát	k5eAaImIp3nP	hnát
roku	rok	k1gInSc3	rok
a	a	k8xC	a
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
ji	on	k3xPp3gFnSc4	on
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
"	"	kIx"	"
<g/>
umělcem	umělec	k1gMnSc7	umělec
dekády	dekáda	k1gFnSc2	dekáda
2010	[number]	k4	2010
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Joanne	Joann	k1gInSc5	Joann
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
umělcem	umělec	k1gMnSc7	umělec
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
desetiletí	desetiletí	k1gNnSc2	desetiletí
třetího	třetí	k4xOgNnSc2	třetí
milénia	milénium	k1gNnSc2	milénium
<g/>
,	,	kIx,	,
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
alby	album	k1gNnPc7	album
umístěnými	umístěný	k2eAgFnPc7d1	umístěná
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dala	dát	k5eAaPmAgFnS	dát
s	s	k7c7	s
partou	parta	k1gFnSc7	parta
kamarádů	kamarád	k1gMnPc2	kamarád
dohromady	dohromady	k6eAd1	dohromady
vlastní	vlastní	k2eAgMnPc1d1	vlastní
Stefani	Stefan	k1gMnPc1	Stefan
Germanotta	Germanott	k1gInSc2	Germanott
Band	banda	k1gFnPc2	banda
a	a	k8xC	a
společně	společně	k6eAd1	společně
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
štěstí	štěstí	k1gNnSc4	štěstí
v	v	k7c6	v
newyorských	newyorský	k2eAgInPc6d1	newyorský
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
však	však	k9	však
ke	k	k7c3	k
skutečně	skutečně	k6eAd1	skutečně
slávě	sláva	k1gFnSc3	sláva
přece	přece	k9	přece
jen	jen	k6eAd1	jen
vedla	vést	k5eAaImAgFnS	vést
trnitá	trnitý	k2eAgFnSc1d1	trnitá
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hudebnice	hudebnice	k1gFnSc1	hudebnice
s	s	k7c7	s
italskými	italský	k2eAgInPc7d1	italský
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
netajila	tajit	k5eNaImAgFnS	tajit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zažila	zažít	k5eAaPmAgFnS	zažít
školní	školní	k2eAgFnSc4d1	školní
šikanu	šikana	k1gFnSc4	šikana
i	i	k9	i
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
dokonce	dokonce	k9	dokonce
znásilnil	znásilnit	k5eAaPmAgInS	znásilnit
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
starší	starší	k1gMnSc1	starší
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vypsala	vypsat	k5eAaPmAgFnS	vypsat
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Swine	Swin	k1gInSc5	Swin
<g/>
"	"	kIx"	"
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
"	"	kIx"	"
<g/>
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
třinácti	třináct	k4xCc7	třináct
výkony	výkon	k1gInPc7	výkon
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
na	na	k7c4	na
MTV	MTV	kA	MTV
Movie	Movie	k1gFnSc2	Movie
&	&	k?	&
TV	TV	kA	TV
Awards	Awards	k1gInSc1	Awards
za	za	k7c4	za
snímek	snímek	k1gInSc4	snímek
Gaga	Gag	k1gInSc2	Gag
<g/>
:	:	kIx,	:
Five	Five	k1gFnSc1	Five
Foot	Foot	k1gMnSc1	Foot
Two	Two	k1gMnSc1	Two
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
objevila	objevit	k5eAaPmAgFnS	objevit
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
při	při	k7c6	při
The	The	k1gFnSc6	The
Monster	monstrum	k1gNnPc2	monstrum
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
poté	poté	k6eAd1	poté
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
při	při	k7c6	při
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
turné	turné	k1gNnSc6	turné
artRAVE	artRAVE	k?	artRAVE
The	The	k1gMnSc1	The
ARTPOP	ARTPOP	kA	ARTPOP
Ball	Ball	k1gMnSc1	Ball
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Lenox	Lenox	k1gInSc1	Lenox
Hill	Hilla	k1gFnPc2	Hilla
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
italsko-americkým	italskomerický	k2eAgMnSc7d1	italsko-americký
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Joseph	Joseph	k1gMnSc1	Joseph
Germanotta	Germanotta	k1gMnSc1	Germanotta
podnikal	podnikat	k5eAaImAgMnS	podnikat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
internetu	internet	k1gInSc2	internet
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Cynthia	Cynthia	k1gFnSc1	Cynthia
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Bissett	Bissett	k1gInSc1	Bissett
<g/>
)	)	kIx)	)
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
bydlela	bydlet	k5eAaImAgFnS	bydlet
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
West	West	k2eAgInSc4d1	West
Side	Side	k1gInSc4	Side
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
ještě	ještě	k9	ještě
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Natali	Natali	k1gFnSc2	Natali
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
tam	tam	k6eAd1	tam
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
bydlí	bydlet	k5eAaImIp3nS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Lady	Lada	k1gFnSc2	Lada
Gaga	Gaga	k1gMnSc1	Gaga
Joe	Joe	k1gMnSc1	Joe
vlastní	vlastní	k2eAgFnSc4d1	vlastní
restauraci	restaurace	k1gFnSc4	restaurace
Joanne	Joann	k1gInSc5	Joann
Trattoria	Trattorium	k1gNnPc4	Trattorium
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Central	Central	k1gFnSc2	Central
Parku	park	k1gInSc2	park
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Cynthia	Cynthium	k1gNnSc2	Cynthium
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
nadaci	nadace	k1gFnSc6	nadace
Born	Borna	k1gFnPc2	Borna
This	This	k1gInSc1	This
Way	Way	k1gMnSc1	Way
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založila	založit	k5eAaPmAgFnS	založit
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Natali	Natali	k1gFnSc2	Natali
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Parsons	Parsons	k1gInSc4	Parsons
The	The	k1gMnPc3	The
New	New	k1gFnPc2	New
School	School	k1gInSc4	School
for	forum	k1gNnPc2	forum
Design	design	k1gInSc1	design
a	a	k8xC	a
Convent	Convent	k1gInSc1	Convent
of	of	k?	of
the	the	k?	the
Sacred	Sacred	k1gInSc4	Sacred
Heart	Hearta	k1gFnPc2	Hearta
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Haus	Haus	k1gInSc4	Haus
of	of	k?	of
Gaga	Gag	k1gInSc2	Gag
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
klipu	klip	k1gInSc6	klip
Telephone	Telephon	k1gInSc5	Telephon
v	v	k7c6	v
čase	čas	k1gInSc6	čas
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
a	a	k8xC	a
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
obě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
velmi	velmi	k6eAd1	velmi
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc7	on
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
ji	on	k3xPp3gFnSc4	on
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
Stefani	Stefan	k1gMnPc1	Stefan
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
dokázala	dokázat	k5eAaPmAgFnS	dokázat
hrát	hrát	k5eAaImF	hrát
podle	podle	k7c2	podle
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
složila	složit	k5eAaPmAgFnS	složit
svou	svůj	k3xOyFgFnSc4	svůj
úplně	úplně	k6eAd1	úplně
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
popěvek	popěvek	k1gInSc4	popěvek
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dollar	dollar	k1gInSc1	dollar
Bills	Bills	k1gInSc1	Bills
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dolarové	dolarový	k2eAgInPc4d1	dolarový
účty	účet	k1gInPc4	účet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Money	Mone	k2eAgFnPc4d1	Mone
<g/>
"	"	kIx"	"
od	od	k7c2	od
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
její	její	k3xOp3gMnSc1	její
tatínek	tatínek	k1gMnSc1	tatínek
často	často	k6eAd1	často
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
napsala	napsat	k5eAaPmAgFnS	napsat
baladu	balada	k1gFnSc4	balada
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
Love	lov	k1gInSc5	lov
Again	Again	k2eAgMnSc1d1	Again
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Znovu	znovu	k6eAd1	znovu
Milovat	milovat	k5eAaImF	milovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
v	v	k7c6	v
butiku	butik	k1gInSc6	butik
nedaleko	nedaleko	k7c2	nedaleko
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
zpívala	zpívat	k5eAaImAgFnS	zpívat
píseň	píseň	k1gFnSc1	píseň
od	od	k7c2	od
Backstreet	Backstreeta	k1gFnPc2	Backstreeta
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
oslovil	oslovit	k5eAaPmAgMnS	oslovit
majitel	majitel	k1gMnSc1	majitel
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
ji	on	k3xPp3gFnSc4	on
telefonní	telefonní	k2eAgNnSc1d1	telefonní
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
patřilo	patřit	k5eAaImAgNnS	patřit
strýci	strýc	k1gMnSc6	strýc
majitele	majitel	k1gMnSc4	majitel
butiku	butik	k1gInSc2	butik
<g/>
,	,	kIx,	,
učiteli	učitel	k1gMnSc3	učitel
zpěvu	zpěv	k1gInSc2	zpěv
Donu	Don	k1gMnSc3	Don
Lawrencovi	Lawrenec	k1gMnSc3	Lawrenec
mezi	mezi	k7c7	mezi
jehož	jehož	k3xOyRp3gMnPc7	jehož
žáky	žák	k1gMnPc7	žák
patří	patřit	k5eAaImIp3nP	patřit
Annie	Annie	k1gFnSc1	Annie
Lennox	Lennox	k1gInSc4	Lennox
<g/>
,	,	kIx,	,
Christina	Christin	k1gMnSc4	Christin
Aguilera	Aguiler	k1gMnSc4	Aguiler
<g/>
,	,	kIx,	,
Bono	bona	k1gFnSc5	bona
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
ji	on	k3xPp3gFnSc4	on
matka	matka	k1gFnSc1	matka
poprvé	poprvé	k6eAd1	poprvé
vzala	vzít	k5eAaPmAgFnS	vzít
do	do	k7c2	do
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c4	v
"	"	kIx"	"
<g/>
open	open	k1gNnSc4	open
mic	mic	k?	mic
nights	nights	k1gInSc1	nights
<g/>
"	"	kIx"	"
-	-	kIx~	-
večery	večer	k1gInPc1	večer
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
mikrofonu	mikrofon	k1gInSc3	mikrofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
dívčí	dívčí	k2eAgFnSc4d1	dívčí
církevní	církevní	k2eAgFnSc4d1	církevní
školu	škola	k1gFnSc4	škola
Convent	Convent	k1gMnSc1	Convent
of	of	k?	of
the	the	k?	the
Sacred	Sacred	k1gInSc1	Sacred
Heart	Hearta	k1gFnPc2	Hearta
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
udělala	udělat	k5eAaPmAgFnS	udělat
maturitu	maturita	k1gFnSc4	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
začala	začít	k5eAaPmAgFnS	začít
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tisch	Tisch	k1gInSc1	Tisch
School	School	k1gInSc1	School
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
dvacítky	dvacítka	k1gFnSc2	dvacítka
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Woody	Wooda	k1gFnPc1	Wooda
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
Whoopi	Whoop	k1gFnSc2	Whoop
Goldbergová	Goldbergový	k2eAgFnSc1d1	Goldbergová
<g/>
,	,	kIx,	,
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
designu	design	k1gInSc2	design
a	a	k8xC	a
také	také	k9	také
chodila	chodit	k5eAaImAgFnS	chodit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k6eAd1	Gaga
se	se	k3xPyFc4	se
na	na	k7c4	na
Tisch	Tisch	k1gInSc4	Tisch
School	Schoola	k1gFnPc2	Schoola
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
každoroční	každoroční	k2eAgFnPc4d1	každoroční
soutěže	soutěž	k1gFnPc4	soutěž
talentů	talent	k1gInPc2	talent
Ultra	ultra	k2eAgFnPc2d1	ultra
Violet	Violeta	k1gFnPc2	Violeta
Live	Liv	k1gFnSc2	Liv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pořádala	pořádat	k5eAaImAgFnS	pořádat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
místní	místní	k2eAgFnSc4d1	místní
charitativní	charitativní	k2eAgFnSc4d1	charitativní
organizaci	organizace	k1gFnSc4	organizace
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
porazila	porazit	k5eAaPmAgFnS	porazit
všechny	všechen	k3xTgMnPc4	všechen
studenty	student	k1gMnPc4	student
newyorské	newyorský	k2eAgFnSc2d1	newyorská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitu	univerzita	k1gFnSc4	univerzita
ale	ale	k9	ale
předčasně	předčasně	k6eAd1	předčasně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaPmF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
rozvíjející	rozvíjející	k2eAgFnSc3d1	rozvíjející
se	se	k3xPyFc4	se
kariéře	kariéra	k1gFnSc3	kariéra
hudební	hudební	k2eAgFnPc1d1	hudební
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
nápaditost	nápaditost	k1gFnSc1	nápaditost
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
už	už	k6eAd1	už
když	když	k8xS	když
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
školky	školka	k1gFnSc2	školka
<g/>
.	.	kIx.	.
</s>
<s>
Uměla	umět	k5eAaImAgFnS	umět
bavit	bavit	k5eAaImF	bavit
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
vrstevníky	vrstevník	k1gMnPc4	vrstevník
jako	jako	k8xC	jako
nejstarší	starý	k2eAgInSc1d3	nejstarší
kozlík	kozlík	k1gInSc1	kozlík
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Three	Three	k1gNnSc6	Three
Billy	Bill	k1gMnPc4	Bill
Goats	Goatsa	k1gFnPc2	Goatsa
Gruff	Gruff	k1gMnSc1	Gruff
(	(	kIx(	(
<g/>
Tři	tři	k4xCgMnPc1	tři
kozlíci	kozlík	k1gMnPc1	kozlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
pro	pro	k7c4	pro
kozlíka	kozlík	k1gMnSc4	kozlík
rohy	roh	k1gInPc7	roh
z	z	k7c2	z
alobalu	alobal	k1gInSc2	alobal
a	a	k8xC	a
z	z	k7c2	z
věšáku	věšák	k1gInSc2	věšák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Máš	mít	k5eAaImIp2nS	mít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
rok	rok	k1gInSc1	rok
<g/>
!	!	kIx.	!
</s>
<s>
Buď	buď	k8xC	buď
uspěješ	uspět	k5eAaPmIp2nS	uspět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
půjdeš	jít	k5eAaImIp2nS	jít
vydělávat	vydělávat	k5eAaImF	vydělávat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Joe	Joe	k1gMnSc1	Joe
Germanotta	Germanotta	k1gMnSc1	Germanotta
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
devatenácti	devatenáct	k4xCc2	devatenáct
letech	léto	k1gNnPc6	léto
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studium	studium	k1gNnSc1	studium
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tisch	Tisch	k1gInSc1	Tisch
School	School	k1gInSc1	School
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
předčasně	předčasně	k6eAd1	předčasně
ukončí	ukončit	k5eAaPmIp3nS	ukončit
a	a	k8xC	a
půjde	jít	k5eAaImIp3nS	jít
si	se	k3xPyFc3	se
tvrdohlavě	tvrdohlavě	k6eAd1	tvrdohlavě
za	za	k7c7	za
vysněnou	vysněný	k2eAgFnSc7d1	vysněná
kariérou	kariéra	k1gFnSc7	kariéra
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Začátky	začátek	k1gInPc1	začátek
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2005	[number]	k4	2005
nahrála	nahrát	k5eAaBmAgFnS	nahrát
pár	pár	k4xCyI	pár
písniček	písnička	k1gFnPc2	písnička
s	s	k7c7	s
hip-hopovým	hipopový	k2eAgMnSc7d1	hip-hopový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Grandmaster	Grandmastra	k1gFnPc2	Grandmastra
Melle	Melle	k1gInSc1	Melle
Mel	mlít	k5eAaImRp2nS	mlít
pro	pro	k7c4	pro
audio	audio	k2eAgFnSc4d1	audio
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
také	také	k9	také
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Stefani	Stefaň	k1gFnSc6	Stefaň
Germanotta	Germanott	k1gInSc2	Germanott
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
SGBand	SGBand	k1gInSc1	SGBand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
z	z	k7c2	z
NYU	NYU	kA	NYU
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Calvin	Calvina	k1gFnPc2	Calvina
Pia	Pius	k1gMnSc2	Pius
<g/>
,	,	kIx,	,
basa	basa	k1gFnSc1	basa
Eli	Eli	k1gMnSc1	Eli
Silverman	Silverman	k1gMnSc1	Silverman
<g/>
,	,	kIx,	,
bubny	buben	k1gInPc1	buben
Alex	Alex	k1gMnSc1	Alex
Beckham	Beckham	k1gInSc1	Beckham
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
manažerem	manažer	k1gMnSc7	manažer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Frank	Frank	k1gMnSc1	Frank
Frederics	Fredericsa	k1gFnPc2	Fredericsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydali	vydat	k5eAaPmAgMnP	vydat
dvě	dva	k4xCgFnPc4	dva
EP	EP	kA	EP
Words	Wordsa	k1gFnPc2	Wordsa
a	a	k8xC	a
Red	Red	k1gFnPc2	Red
and	and	k?	and
Blue	Blue	k1gFnPc2	Blue
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
hudebním	hudební	k2eAgMnSc7d1	hudební
producentem	producent	k1gMnSc7	producent
Robem	Robem	k?	Robem
Fusarim	Fusarim	k1gInSc1	Fusarim
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
Songwriters	Songwriters	k1gInSc4	Songwriters
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Famus	k1gMnSc5	Famus
Showcase	Showcas	k1gMnSc5	Showcas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
začala	začít	k5eAaPmAgFnS	začít
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Fusarim	Fusari	k1gNnSc7	Fusari
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
její	její	k3xOp3gInPc4	její
vokály	vokál	k1gInPc4	vokál
k	k	k7c3	k
Freddiemu	Freddiemo	k1gNnSc3	Freddiemo
Mercurymu	Mercurym	k1gInSc2	Mercurym
<g/>
,	,	kIx,	,
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
zpěvákovi	zpěvák	k1gMnSc3	zpěvák
Queenu	Queen	k1gMnSc3	Queen
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
písničce	písnička	k1gFnSc3	písnička
"	"	kIx"	"
<g/>
Radio	radio	k1gNnSc1	radio
Gaga	Gag	k1gInSc2	Gag
<g/>
"	"	kIx"	"
a	a	k8xC	a
omylu	omyl	k1gInSc2	omyl
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
na	na	k7c6	na
mobilu	mobil	k1gInSc6	mobil
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
její	její	k3xOp3gNnPc4	její
přezdívka	přezdívka	k1gFnSc1	přezdívka
Lady	Lada	k1gFnSc2	Lada
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
momentem	moment	k1gInSc7	moment
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
deset	deset	k4xCc1	deset
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgInPc2	který
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
napsala	napsat	k5eAaPmAgFnS	napsat
svůj	svůj	k3xOyFgInSc4	svůj
pozdější	pozdní	k2eAgInSc4d2	pozdější
průlomový	průlomový	k2eAgInSc4d1	průlomový
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
myšlenka	myšlenka	k1gFnSc1	myšlenka
nebyla	být	k5eNaImAgFnS	být
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
<g/>
,	,	kIx,	,
však	však	k9	však
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
tvůrčím	tvůrčí	k2eAgMnSc7d1	tvůrčí
kumpánem	kumpánem	k?	kumpánem
RedOnem	RedOn	k1gInSc7	RedOn
psala	psát	k5eAaImAgNnP	psát
s	s	k7c7	s
kocovinou	kocovina	k1gFnSc7	kocovina
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
song	song	k1gInSc1	song
o	o	k7c6	o
opilosti	opilost	k1gFnSc6	opilost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vzešel	vzejít	k5eAaPmAgMnS	vzejít
z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Napsala	napsat	k5eAaPmAgFnS	napsat
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
letělo	letět	k5eAaImAgNnS	letět
z	z	k7c2	z
mého	můj	k3xOp1gNnSc2	můj
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekla	říct	k5eAaPmAgFnS	říct
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
-	-	kIx~	-
tedy	tedy	k9	tedy
na	na	k7c4	na
koleni	kolit	k5eAaImNgMnP	kolit
za	za	k7c4	za
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
-	-	kIx~	-
později	pozdě	k6eAd2	pozdě
spíchla	spíchnout	k5eAaPmAgFnS	spíchnout
i	i	k9	i
další	další	k2eAgInPc4d1	další
velké	velký	k2eAgInPc4d1	velký
hity	hit	k1gInPc4	hit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc1	poker
Face	Fac	k1gInSc2	Fac
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Born	Born	k1gNnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
<g/>
"	"	kIx"	"
<g/>
Antonio	Antonio	k1gMnSc1	Antonio
"	"	kIx"	"
<g/>
L.A	L.A	k1gMnSc1	L.A
<g/>
"	"	kIx"	"
Reid	Reid	k1gMnSc1	Reid
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnPc2	součást
Def	Def	k1gMnPc2	Def
Jam	jáma	k1gFnPc2	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
však	však	k9	však
byla	být	k5eAaImAgNnP	být
z	z	k7c2	z
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
propuštěna	propuštěn	k2eAgFnSc1d1	propuštěna
-	-	kIx~	-
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
části	část	k1gFnPc1	část
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
věnována	věnovat	k5eAaImNgFnS	věnovat
písnička	písnička	k1gFnSc1	písnička
"	"	kIx"	"
<g/>
Marry	Marra	k1gFnPc1	Marra
the	the	k?	the
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Fusarim	Fusari	k1gNnSc7	Fusari
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
go-go	goo	k6eAd1	go-go
tanečnicí	tanečnice	k1gFnSc7	tanečnice
v	v	k7c6	v
St.	st.	kA	st.
Jerome	Jerom	k1gInSc5	Jerom
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
v	v	k7c6	v
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
umělkyní	umělkyně	k1gFnSc7	umělkyně
Lady	lady	k1gFnSc1	lady
Starlight	Starlight	k1gMnSc1	Starlight
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
spolu	spolu	k6eAd1	spolu
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc4	jejich
umělecká	umělecký	k2eAgNnPc4d1	umělecké
vystoupení	vystoupení	k1gNnPc4	vystoupení
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
and	and	k?	and
the	the	k?	the
Starlight	Starlight	k1gInSc1	Starlight
Revue	revue	k1gFnSc1	revue
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Dvojice	dvojice	k1gFnSc1	dvojice
byla	být	k5eAaImAgFnS	být
pozvaná	pozvaný	k2eAgFnSc1d1	pozvaná
na	na	k7c4	na
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Lollapalooza	Lollapalooz	k1gMnSc2	Lollapalooz
<g/>
.	.	kIx.	.
</s>
<s>
Fusari	Fusari	k6eAd1	Fusari
poslal	poslat	k5eAaPmAgMnS	poslat
její	její	k3xOp3gFnPc4	její
písničky	písnička	k1gFnPc4	písnička
svému	svůj	k1gMnSc3	svůj
kamarádovi	kamarád	k1gMnSc3	kamarád
<g/>
,	,	kIx,	,
producentovi	producent	k1gMnSc3	producent
Vincentovi	Vincent	k1gMnSc3	Vincent
Herbertovi	Herbert	k1gMnSc3	Herbert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
následně	následně	k6eAd1	následně
přijal	přijmout	k5eAaPmAgMnS	přijmout
pod	pod	k7c7	pod
svojí	svůj	k3xOyFgFnSc7	svůj
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
Streamline	Streamlin	k1gInSc5	Streamlin
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
pracovala	pracovat	k5eAaImAgFnS	pracovat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
najatá	najatý	k2eAgFnSc1d1	najatá
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
psala	psát	k5eAaImAgFnS	psát
písně	píseň	k1gFnPc4	píseň
zpěvačkám	zpěvačka	k1gFnPc3	zpěvačka
jako	jako	k8xS	jako
Britney	Britnea	k1gFnPc1	Britnea
Spears	Spears	k1gInSc1	Spears
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Kids	Kidsa	k1gFnPc2	Kidsa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Block	Block	k1gInSc1	Block
<g/>
,	,	kIx,	,
Fergie	Fergie	k1gFnSc1	Fergie
a	a	k8xC	a
Pussycat	Pussycat	k1gFnSc1	Pussycat
Dolls	Dollsa	k1gFnPc2	Dollsa
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
zpěvák	zpěvák	k1gMnSc1	zpěvák
Akon	Akon	k1gMnSc1	Akon
se	se	k3xPyFc4	se
však	však	k9	však
všiml	všimnout	k5eAaPmAgMnS	všimnout
jejího	její	k3xOp3gInSc2	její
krásného	krásný	k2eAgInSc2d1	krásný
hlasu	hlas	k1gInSc2	hlas
a	a	k8xC	a
přizval	přizvat	k5eAaPmAgMnS	přizvat
jí	jíst	k5eAaImIp3nS	jíst
k	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jí	jíst	k5eAaImIp3nS	jíst
připsal	připsat	k5eAaPmAgMnS	připsat
pod	pod	k7c7	pod
svojí	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc1d1	vlastní
společnost	společnost	k1gFnSc1	společnost
Kon	kon	k1gInSc4	kon
Live	Liv	k1gFnSc2	Liv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
textařem	textař	k1gMnSc7	textař
RedOnem	RedOn	k1gMnSc7	RedOn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
produkovala	produkovat	k5eAaImAgFnS	produkovat
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Boys	boy	k1gMnPc3	boy
Boys	boy	k1gMnPc3	boy
Boys	boy	k1gMnPc3	boy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
kolaborací	kolaborace	k1gFnSc7	kolaborace
s	s	k7c7	s
RedOnem	RedOn	k1gInSc7	RedOn
v	v	k7c6	v
nahrávacím	nahrávací	k2eAgNnSc6d1	nahrávací
studio	studio	k1gNnSc1	studio
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
Cherrytree	Cherrytree	k1gNnSc3	Cherrytree
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Fame	Fam	k1gFnSc2	Fam
a	a	k8xC	a
The	The	k1gFnSc2	The
Fame	Fam	k1gInSc2	Fam
Monster	monstrum	k1gNnPc2	monstrum
===	===	k?	===
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
The	The	k1gFnSc1	The
Fame	Fame	k1gFnSc1	Fame
vydala	vydat	k5eAaPmAgFnS	vydat
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
a	a	k8xC	a
za	za	k7c4	za
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc4	první
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
taneční	taneční	k2eAgFnSc1d1	taneční
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc1	poker
Face	Face	k1gInSc1	Face
<g/>
"	"	kIx"	"
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
prodáváným	prodáváný	k2eAgInSc7d1	prodáváný
singlem	singl	k1gInSc7	singl
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s	s	k7c7	s
9,8	[number]	k4	9,8
miliony	milion	k4xCgInPc7	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnSc7	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
na	na	k7c4	na
52	[number]	k4	52
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Grammy	Gramma	k1gFnSc2	Gramma
Award	Awarda	k1gFnPc2	Awarda
získala	získat	k5eAaPmAgFnS	získat
a	a	k8xC	a
odnesla	odnést	k5eAaPmAgFnS	odnést
si	se	k3xPyFc3	se
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Písničku	písnička	k1gFnSc4	písnička
roku	rok	k1gInSc2	rok
a	a	k8xC	a
Nahrávku	nahrávka	k1gFnSc4	nahrávka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Fame	Fame	k1gNnSc1	Fame
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
Album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získalo	získat	k5eAaPmAgNnS	získat
cenu	cena	k1gFnSc4	cena
Nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
taneční	taneční	k1gFnPc4	taneční
<g/>
/	/	kIx~	/
<g/>
electro	electro	k6eAd1	electro
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
si	se	k3xPyFc3	se
odnesla	odnést	k5eAaPmAgFnS	odnést
3	[number]	k4	3
ceny	cena	k1gFnSc2	cena
z	z	k7c2	z
9	[number]	k4	9
nominací	nominace	k1gFnPc2	nominace
<g/>
.7	.7	k4	.7
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
její	její	k3xOp3gFnSc1	její
koncertní	koncertní	k2eAgFnSc1d1	koncertní
šňůra	šňůra	k1gFnSc1	šňůra
The	The	k1gMnSc2	The
Fame	Fam	k1gMnSc2	Fam
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
203	[number]	k4	203
místech	místo	k1gNnPc6	místo
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
120	[number]	k4	120
<g/>
×	×	k?	×
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
64	[number]	k4	64
<g/>
×	×	k?	×
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
×	×	k?	×
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
turné	turné	k1gNnSc6	turné
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
přes	přes	k7c4	přes
227,4	[number]	k4	227,4
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
EP	EP	kA	EP
The	The	k1gFnSc1	The
Fame	Fam	k1gInSc2	Fam
Monster	monstrum	k1gNnPc2	monstrum
s	s	k7c7	s
8	[number]	k4	8
písničkami	písnička	k1gFnPc7	písnička
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
song	song	k1gInSc1	song
představoval	představovat	k5eAaImAgInS	představovat
její	její	k3xOp3gFnSc4	její
osobní	osobní	k2eAgFnSc4d1	osobní
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gMnSc5	Telephon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
spolupracovala	spolupracovat	k5eAaImAgNnP	spolupracovat
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Beyoncé	Beyoncá	k1gFnSc2	Beyoncá
Knowles	Knowles	k1gInSc1	Knowles
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Britney	Britnea	k1gFnSc2	Britnea
Spears	Spearsa	k1gFnPc2	Spearsa
<g/>
,	,	kIx,	,
té	ten	k3xDgFnSc2	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nelíbil	líbit	k5eNaImAgMnS	líbit
a	a	k8xC	a
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Alejandro	Alejandra	k1gFnSc5	Alejandra
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
videoklipem	videoklip	k1gInSc7	videoklip
lámala	lámat	k5eAaImAgFnS	lámat
rekordy	rekord	k1gInPc4	rekord
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k8xC	i
první	první	k4xOgFnSc7	první
osobou	osoba	k1gFnSc7	osoba
co	co	k8xS	co
měla	mít	k5eAaImAgFnS	mít
30	[number]	k4	30
miliónů	milión	k4xCgInPc2	milión
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c6	na
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
zvané	zvaný	k2eAgFnSc2d1	zvaná
Twitter	Twitter	k1gInSc4	Twitter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
tehdy	tehdy	k6eAd1	tehdy
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
večera	večer	k1gInSc2	večer
došla	dojít	k5eAaPmAgFnS	dojít
převzít	převzít	k5eAaPmF	převzít
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Cher	Chera	k1gFnPc2	Chera
ocenění	ocenění	k1gNnSc2	ocenění
za	za	k7c4	za
Videoklip	videoklip	k1gInSc4	videoklip
roku	rok	k1gInSc2	rok
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc1	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nejikoničtějším	ikonický	k2eAgInSc6d3	ikonický
kostýmu	kostým	k1gInSc6	kostým
z	z	k7c2	z
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
měli	mít	k5eAaImAgMnP	mít
tento	tento	k3xDgInSc4	tento
kousek	kousek	k1gInSc4	kousek
jen	jen	k9	jen
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
důkaz	důkaz	k1gInSc4	důkaz
její	její	k3xOp3gFnSc2	její
nechutnosti	nechutnost	k1gFnSc2	nechutnost
<g/>
,	,	kIx,	,
Gaga	Gaga	k1gFnSc1	Gaga
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
ale	ale	k9	ale
viděla	vidět	k5eAaImAgFnS	vidět
hlubší	hluboký	k2eAgNnSc4d2	hlubší
poselství	poselství	k1gNnSc4	poselství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gInSc1	její
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
kontroverznímu	kontroverzní	k2eAgInSc3d1	kontroverzní
americkému	americký	k2eAgInSc3d1	americký
zákonu	zákon	k1gInSc3	zákon
zvanému	zvaný	k2eAgInSc3d1	zvaný
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Ask	Ask	k1gMnSc1	Ask
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Tell	Tell	k1gInSc1	Tell
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
neumožňoval	umožňovat	k5eNaImAgMnS	umožňovat
homosexuálům	homosexuál	k1gMnPc3	homosexuál
vykonávat	vykonávat	k5eAaImF	vykonávat
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
orientaci	orientace	k1gFnSc3	orientace
veřejně	veřejně	k6eAd1	veřejně
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nepostavíme	postavit	k5eNaPmIp1nP	postavit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
v	v	k7c4	v
co	co	k3yQnSc4	co
věříme	věřit	k5eAaImIp1nP	věřit
<g/>
,	,	kIx,	,
když	když	k8xS	když
nebudeme	být	k5eNaImBp1nP	být
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
budeme	být	k5eAaImBp1nP	být
mít	mít	k5eAaImF	mít
tolik	tolik	k4xDc4	tolik
práv	právo	k1gNnPc2	právo
jako	jako	k8xC	jako
maso	maso	k1gNnSc1	maso
na	na	k7c6	na
našich	náš	k3xOp1gFnPc6	náš
kostech	kost	k1gFnPc6	kost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Řekla	říct	k5eAaPmAgFnS	říct
Gaga	Gaga	k1gFnSc1	Gaga
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
rozhovoru	rozhovor	k1gInSc6	rozhovor
Ellen	Ellen	k2eAgInSc4d1	Ellen
DeGeneres	DeGeneres	k1gInSc4	DeGeneres
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
kus	kus	k6eAd1	kus
masa	masa	k1gFnSc1	masa
nejsem	být	k5eNaImIp1nS	být
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodala	dodat	k5eAaPmAgFnS	dodat
odhodlaně	odhodlaně	k6eAd1	odhodlaně
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Ask	Ask	k1gMnSc1	Ask
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Tell	Tell	k1gInSc1	Tell
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gMnSc1	Gaga
si	se	k3xPyFc3	se
z	z	k7c2	z
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
odnesla	odnést	k5eAaPmAgFnS	odnést
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
cen	cena	k1gFnPc2	cena
z	z	k7c2	z
13	[number]	k4	13
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
The	The	k1gFnSc7	The
Fame	Fame	k1gFnPc2	Fame
Monster	monstrum	k1gNnPc2	monstrum
získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
6	[number]	k4	6
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
-	-	kIx~	-
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
Nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
popové	popový	k2eAgNnSc4d1	popové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
HBO	HBO	kA	HBO
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
televizní	televizní	k2eAgInSc1d1	televizní
speciál	speciál	k1gInSc1	speciál
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Lady	lady	k1gFnSc4	lady
Gaga	Gag	k1gInSc2	Gag
Presents	Presents	k1gInSc1	Presents
the	the	k?	the
Monster	monstrum	k1gNnPc2	monstrum
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
<g/>
:	:	kIx,	:
At	At	k1gFnSc1	At
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
fenoménu	fenomén	k1gInSc6	fenomén
Gaga	Gag	k1gInSc2	Gag
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
zpěvaččina	zpěvaččin	k2eAgFnSc1d1	zpěvaččina
kreativita	kreativita	k1gFnSc1	kreativita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
vystupování	vystupování	k1gNnSc2	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
bizarní	bizarní	k2eAgInPc1d1	bizarní
kostýmy	kostým	k1gInPc1	kostým
nebo	nebo	k8xC	nebo
provokativní	provokativní	k2eAgFnPc1d1	provokativní
televizní	televizní	k2eAgFnPc1d1	televizní
čísla	číslo	k1gNnSc2	číslo
způsobily	způsobit	k5eAaPmAgInP	způsobit
doslova	doslova	k6eAd1	doslova
Gagamánii	Gagamánie	k1gFnSc4	Gagamánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Born	Born	k1gNnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
===	===	k?	===
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Born	Born	k1gNnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
1,108	[number]	k4	1,108
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
místech	místo	k1gNnPc6	místo
světových	světový	k2eAgFnPc2d1	světová
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
tři	tři	k4xCgFnPc4	tři
nominace	nominace	k1gFnPc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
Nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
k	k	k7c3	k
albu	album	k1gNnSc3	album
byly	být	k5eAaImAgInP	být
"	"	kIx"	"
<g/>
Born	Born	k1gInSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Judas	Judas	k1gInSc1	Judas
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Edge	Edg	k1gFnSc2	Edg
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Born	Born	k1gInSc1	Born
this	this	k1gInSc1	this
way	way	k?	way
<g/>
"	"	kIx"	"
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
53	[number]	k4	53
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
Yoü	Yoü	k1gFnSc4	Yoü
and	and	k?	and
I	i	k9	i
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Marry	Marra	k1gFnSc2	Marra
the	the	k?	the
Night	Night	k1gInSc1	Night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
videoklipu	videoklip	k1gInSc2	videoklip
k	k	k7c3	k
písničce	písnička	k1gFnSc3	písnička
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
and	and	k?	and
I	I	kA	I
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
snoubencem	snoubenec	k1gMnSc7	snoubenec
hercem	herec	k1gMnSc7	herec
Taylorem	Taylor	k1gInSc7	Taylor
Kinneym	Kinneym	k1gInSc1	Kinneym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
čelilo	čelit	k5eAaImAgNnS	čelit
hodně	hodně	k6eAd1	hodně
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
Madonny	Madonna	k1gFnSc2	Madonna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
jejím	její	k3xOp3gInSc7	její
songem	song	k1gInSc7	song
"	"	kIx"	"
<g/>
Express	express	k1gInSc1	express
Yourself	Yourself	k1gInSc1	Yourself
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
klipu	klip	k1gInSc6	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Born	Borna	k1gFnPc2	Borna
This	This	k1gInSc1	This
Way	Way	k1gMnSc1	Way
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Gagy	gag	k1gInPc4	gag
blízký	blízký	k2eAgMnSc1d1	blízký
kamarád	kamarád	k1gMnSc1	kamarád
Rick	Rick	k1gMnSc1	Rick
Genest	Genest	k1gMnSc1	Genest
-	-	kIx~	-
nejpotetovanější	potetovaný	k2eAgMnSc1d3	nejpotetovanější
muž	muž	k1gMnSc1	muž
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byly	být	k5eAaImAgFnP	být
nejspíše	nejspíše	k9	nejspíše
velké	velký	k2eAgFnPc4d1	velká
deprese	deprese	k1gFnPc4	deprese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
umělcem	umělec	k1gMnSc7	umělec
Tonym	Tony	k1gMnSc7	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
a	a	k8xC	a
nahrály	nahrát	k5eAaPmAgInP	nahrát
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
verzi	verze	k1gFnSc4	verze
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Lady	lady	k1gFnSc1	lady
Is	Is	k1gFnSc1	Is
a	a	k8xC	a
Tramp	tramp	k1gInSc1	tramp
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nahrála	nahrát	k5eAaBmAgFnS	nahrát
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
s	s	k7c7	s
Eltonem	Elton	k1gMnSc7	Elton
Johnem	John	k1gMnSc7	John
pro	pro	k7c4	pro
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
Gnomeo	Gnomeo	k1gMnSc1	Gnomeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
Na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
ABC	ABC	kA	ABC
hostovala	hostovat	k5eAaImAgFnS	hostovat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
televizní	televizní	k2eAgInSc4d1	televizní
speciál	speciál	k1gInSc4	speciál
k	k	k7c3	k
Díkuvzdání	díkuvzdání	k1gNnSc3	díkuvzdání
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
A	A	kA	A
Very	Vera	k1gFnSc2	Vera
Gaga	Gag	k1gInSc2	Gag
Thanksgiving	Thanksgiving	k1gInSc1	Thanksgiving
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
kriticky	kriticky	k6eAd1	kriticky
uznáván	uznávat	k5eAaImNgInS	uznávat
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
5,749	[number]	k4	5,749
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
vydala	vydat	k5eAaPmAgFnS	vydat
vánoční	vánoční	k2eAgFnSc1d1	vánoční
EP	EP	kA	EP
A	a	k8xC	a
Very	Vera	k1gMnSc2	Vera
Gaga	Gagus	k1gMnSc2	Gagus
Holiday	Holidaa	k1gMnSc2	Holidaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
právě	právě	k6eAd1	právě
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
zakládá	zakládat	k5eAaImIp3nS	zakládat
Gaga	Gag	k2eAgFnSc1d1	Gaga
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
Cynthiou	Cynthia	k1gFnSc7	Cynthia
nadační	nadační	k2eAgInSc4d1	nadační
fond	fond	k1gInSc4	fond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
GaGa	GaG	k1gInSc2	GaG
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
seriálů	seriál	k1gInPc2	seriál
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gFnSc1	The
Simpsons	Simpsons	k1gInSc1	Simpsons
(	(	kIx(	(
<g/>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
508	[number]	k4	508
-	-	kIx~	-
Lisa	Lisa	k1gFnSc1	Lisa
Goes	Goes	k1gInSc1	Goes
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Springfieldu	Springfield	k1gInSc2	Springfield
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Lisa	Lisa	k1gFnSc1	Lisa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lady	lady	k1gFnSc7	lady
Gagou	Gaga	k1gFnSc7	Gaga
představili	představit	k5eAaPmAgMnP	představit
duet	duet	k1gInSc4	duet
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Lady	Lada	k1gFnSc2	Lada
Gagy	gag	k1gInPc4	gag
Lisa	Lisa	k1gFnSc1	Lisa
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
Springfield	Springfielda	k1gFnPc2	Springfielda
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
být	být	k5eAaImF	být
odlišný	odlišný	k2eAgInSc4d1	odlišný
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
být	být	k5eAaImF	být
jako	jako	k9	jako
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Yoü	Yoü	k1gFnSc1	Yoü
and	and	k?	and
I	i	k9	i
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
Gagy	gag	k1gInPc1	gag
alter	altra	k1gFnPc2	altra
ego	ego	k1gNnSc2	ego
zvané	zvaný	k2eAgFnPc4d1	zvaná
Jo	jo	k9	jo
Calderone	Calderon	k1gInSc5	Calderon
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
klipu	klip	k1gInSc6	klip
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
VMAs	VMAsa	k1gFnPc2	VMAsa
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mimochodem	mimochodem	k9	mimochodem
odnese	odnést	k5eAaPmIp3nS	odnést
i	i	k9	i
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
skupiny	skupina	k1gFnSc2	skupina
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
Brianem	Brian	k1gMnSc7	Brian
Mayem	May	k1gMnSc7	May
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k9	právě
na	na	k7c6	na
předávání	předávání	k1gNnSc6	předávání
těchto	tento	k3xDgFnPc2	tento
cen	cena	k1gFnPc2	cena
zazpívá	zazpívat	k5eAaPmIp3nS	zazpívat
onu	onen	k3xDgFnSc4	onen
píseň	píseň	k1gFnSc4	píseň
You	You	k1gFnSc2	You
and	and	k?	and
I.	I.	kA	I.
</s>
</p>
<p>
<s>
V	v	k7c6	v
klipu	klip	k1gInSc6	klip
k	k	k7c3	k
písničce	písnička	k1gFnSc3	písnička
Marry	Marra	k1gFnSc2	Marra
The	The	k1gMnPc1	The
Night	Nighta	k1gFnPc2	Nighta
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
Gagy	gag	k1gInPc7	gag
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
boty	bota	k1gFnSc2	bota
co	co	k9	co
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
sobě	se	k3xPyFc3	se
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
podpatky	podpatek	k1gInPc4	podpatek
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
39	[number]	k4	39
centimetrů	centimetr	k1gInPc2	centimetr
(	(	kIx(	(
<g/>
18	[number]	k4	18
palců	palec	k1gInPc2	palec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
podpatky	podpatek	k1gInPc7	podpatek
Gaga	Gagum	k1gNnSc2	Gagum
měřila	měřit	k5eAaImAgFnS	měřit
194	[number]	k4	194
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
ARTPOP	ARTPOP	kA	ARTPOP
a	a	k8xC	a
Cheek	Cheek	k6eAd1	Cheek
To	ten	k3xDgNnSc1	ten
Cheek	Cheek	k6eAd1	Cheek
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
písničkách	písnička	k1gFnPc6	písnička
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
Artpop	Artpop	k1gInSc1	Artpop
(	(	kIx(	(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1	stylizováno
jako	jako	k8xS	jako
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
258	[number]	k4	258
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Applause	Applause	k1gFnPc4	Applause
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Do	do	k7c2	do
What	Whata	k1gFnPc2	Whata
U	u	k7c2	u
Want	Wanta	k1gFnPc2	Wanta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
R.	R.	kA	R.
Kellym	Kellym	k1gInSc1	Kellym
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
ohlas	ohlas	k1gInSc4	ohlas
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nahrála	nahrát	k5eAaPmAgFnS	nahrát
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
píseň	píseň	k1gFnSc4	píseň
i	i	k8xC	i
upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
s	s	k7c7	s
Christinou	Christin	k2eAgFnSc7d1	Christina
Aguilerou	Aguilera	k1gFnSc7	Aguilera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
kromě	kromě	k7c2	kromě
R.	R.	kA	R.
Kellyho	Kelly	k1gMnSc4	Kelly
a	a	k8xC	a
Aguilery	Aguiler	k1gMnPc4	Aguiler
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k8xC	i
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgMnSc1d1	známý
umělec	umělec	k1gMnSc1	umělec
Jeff	Jeff	k1gMnSc1	Jeff
Koons	Koons	k1gInSc4	Koons
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
sochami	socha	k1gFnPc7	socha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pes	pes	k1gMnSc1	pes
z	z	k7c2	z
balónlů	balónl	k1gInPc2	balónl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
GaGu	gag	k1gInSc3	gag
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
právě	právě	k9	právě
obal	obal	k1gInSc4	obal
jejího	její	k3xOp3gNnSc2	její
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
písni	píseň	k1gFnSc6	píseň
Swine	Swin	k1gInSc5	Swin
na	na	k7c6	na
SXSW	SXSW	kA	SXSW
zvracela	zvracet	k5eAaImAgFnS	zvracet
Millie	Millie	k1gFnSc1	Millie
Brown	Browna	k1gFnPc2	Browna
<g/>
.	.	kIx.	.
</s>
<s>
Millie	Millie	k1gFnSc1	Millie
je	být	k5eAaImIp3nS	být
umělkyně	umělkyně	k1gFnSc1	umělkyně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pije	pít	k5eAaImIp3nS	pít
obarvené	obarvený	k2eAgNnSc4d1	obarvené
sójové	sójový	k2eAgNnSc4d1	sójové
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pak	pak	k6eAd1	pak
vyzvrací	vyzvracet	k5eAaPmIp3nS	vyzvracet
na	na	k7c4	na
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Roberta	Robert	k1gMnSc2	Robert
Rodrigueza	Rodriguez	k1gMnSc2	Rodriguez
Machete	Mache	k1gNnSc2	Mache
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
jako	jako	k9	jako
La	la	k1gNnSc1	la
Chameleon	chameleon	k1gMnSc1	chameleon
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
negativní	negativní	k2eAgFnSc4d1	negativní
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
malina	malina	k1gFnSc1	malina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
také	také	k9	také
moderovala	moderovat	k5eAaBmAgNnP	moderovat
show	show	k1gNnPc1	show
Saturday	Saturda	k2eAgInPc4d1	Saturda
Night	Night	k2eAgInSc4d1	Night
Live	Liv	k1gInPc4	Liv
a	a	k8xC	a
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
písničkou	písnička	k1gFnSc7	písnička
"	"	kIx"	"
<g/>
Do	do	k7c2	do
What	Whatum	k1gNnPc2	Whatum
You	You	k1gMnSc2	You
Want	Wanta	k1gFnPc2	Wanta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
hostovala	hostovat	k5eAaImAgFnS	hostovat
již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
televizní	televizní	k2eAgInSc4d1	televizní
speciál	speciál	k1gInSc4	speciál
k	k	k7c3	k
Díkuvzdání	díkuvzdání	k1gNnSc3	díkuvzdání
Lady	Lada	k1gFnSc2	Lada
Gaga	Gagum	k1gNnSc2	Gagum
&	&	k?	&
the	the	k?	the
Muppets	Muppets	k1gInSc1	Muppets
<g/>
'	'	kIx"	'
Holiday	Holidaa	k1gFnPc1	Holidaa
Spectacular	Spectacular	k1gInSc4	Spectacular
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
cameo	cameo	k1gMnSc1	cameo
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
filmu	film	k1gInSc6	film
Roberta	Robert	k1gMnSc2	Robert
Rodrigueze	Rodrigueze	k1gFnSc2	Rodrigueze
Sin	sin	kA	sin
City	City	k1gFnSc2	City
<g/>
:	:	kIx,	:
Ženská	ženská	k1gFnSc1	ženská
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
bych	by	kYmCp1nS	by
vraždil	vraždit	k5eAaImAgMnS	vraždit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůbec	vůbec	k9	vůbec
první	první	k4xOgInPc1	první
umělkyní	umělkyně	k1gFnPc2	umělkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
asociace	asociace	k1gFnSc2	asociace
RIAA	RIAA	kA	RIAA
získala	získat	k5eAaPmAgFnS	získat
Digital	Digital	kA	Digital
Diamond	Diamond	k1gInSc4	Diamond
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
hit	hit	k1gInSc4	hit
Bad	Bad	k1gFnSc2	Bad
Romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
a	a	k8xC	a
ve	v	k7c6	v
streamech	stream	k1gInPc6	stream
v	v	k7c6	v
USA	USA	kA	USA
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
manažera	manažer	k1gMnSc4	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Troye	Troye	k1gFnSc4	Troye
Cartera	Carter	k1gMnSc2	Carter
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Bobbyho	Bobby	k1gMnSc4	Bobby
Campella	Campell	k1gMnSc4	Campell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
vydala	vydat	k5eAaPmAgFnS	vydat
společně	společně	k6eAd1	společně
s	s	k7c7	s
jazzovým	jazzový	k2eAgMnSc7d1	jazzový
umělcem	umělec	k1gMnSc7	umělec
Tony	Tony	k1gFnSc2	Tony
Bennettem	Bennetto	k1gNnSc7	Bennetto
album	album	k1gNnSc4	album
Cheek	Cheek	k1gInSc1	Cheek
To	to	k9	to
Cheek	Cheek	k6eAd1	Cheek
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
131	[number]	k4	131
tisíc	tisíc	k4xCgInPc2	tisíc
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
nahrála	nahrát	k5eAaBmAgFnS	nahrát
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
Cher	Cher	k1gInSc4	Cher
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
nelíbila	líbit	k5eNaImAgFnS	líbit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
a	a	k8xC	a
řekla	říct	k5eAaPmAgFnS	říct
Cher	Cher	k1gInSc4	Cher
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
prozradila	prozradit	k5eAaPmAgFnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
RedOne	RedOn	k1gMnSc5	RedOn
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
italský	italský	k2eAgMnSc1d1	italský
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Giorgio	Giorgio	k1gMnSc1	Giorgio
Moroder	Moroder	k1gMnSc1	Moroder
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
na	na	k7c6	na
nadcházejícím	nadcházející	k2eAgNnSc6d1	nadcházející
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
American	American	k1gInSc1	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc1	story
===	===	k?	===
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
název	název	k1gInSc4	název
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
skladatelkou	skladatelka	k1gFnSc7	skladatelka
Diane	Dian	k1gInSc5	Dian
Warren	Warrno	k1gNnPc2	Warrno
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Til	til	k1gInSc1	til
It	It	k1gFnSc2	It
Happens	Happensa	k1gFnPc2	Happensa
To	to	k9	to
You	You	k1gMnPc2	You
a	a	k8xC	a
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
americký	americký	k2eAgInSc4d1	americký
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
The	The	k1gMnPc2	The
Hunting	Hunting	k1gInSc1	Hunting
Ground	Grounda	k1gFnPc2	Grounda
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Lovný	lovný	k2eAgInSc1d1	lovný
revír	revír	k1gInSc1	revír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
násilí	násilí	k1gNnSc2	násilí
v	v	k7c6	v
kampusech	kampus	k1gInPc6	kampus
univerzit	univerzita	k1gFnPc2	univerzita
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Song	song	k1gInSc1	song
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Ocara	Ocaro	k1gNnPc4	Ocaro
v	v	k7c6	v
kategirii	kategirie	k1gFnSc6	kategirie
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
však	však	k9	však
nezískal	získat	k5eNaPmAgInS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
večera	večer	k1gInSc2	večer
Gaga	Gag	k1gInSc2	Gag
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
také	také	k6eAd1	také
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
dále	daleko	k6eAd2	daleko
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
na	na	k7c4	na
Satellite	Satellit	k1gInSc5	Satellit
Award	Award	k1gMnSc1	Award
2015	[number]	k4	2015
jako	jako	k8xC	jako
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
původní	původní	k2eAgFnSc1d1	původní
písnička	písnička	k1gFnSc1	písnička
a	a	k8xC	a
Cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
ovšem	ovšem	k9	ovšem
převzala	převzít	k5eAaPmAgFnS	převzít
pouze	pouze	k6eAd1	pouze
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dle	dle	k7c2	dle
poroty	porota	k1gFnSc2	porota
Gaga	Gagum	k1gNnSc2	Gagum
pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
napsala	napsat	k5eAaBmAgFnS	napsat
příliš	příliš	k6eAd1	příliš
málo	málo	k6eAd1	málo
textu	text	k1gInSc2	text
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
předávání	předávání	k1gNnSc2	předávání
Cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
Tony	Tony	k1gFnSc7	Tony
Bennettem	Bennett	k1gInSc7	Bennett
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
společného	společný	k2eAgNnSc2d1	společné
alba	album	k1gNnSc2	album
Cheek	Cheek	k6eAd1	Cheek
To	to	k9	to
Cheek	Cheek	k6eAd1	Cheek
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
večera	večer	k1gInSc2	večer
také	také	k9	také
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
získali	získat	k5eAaPmAgMnP	získat
cenu	cena	k1gFnSc4	cena
v	v	k7c4	v
kategoriiBest	kategoriiBest	k1gFnSc4	kategoriiBest
Traditional	Traditional	k1gFnSc2	Traditional
Pop	pop	k1gInSc1	pop
Vocal	Vocal	k1gInSc1	Vocal
Album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
koncertní	koncertní	k2eAgInSc4d1	koncertní
speciál	speciál	k1gInSc4	speciál
Tony	Tony	k1gMnSc1	Tony
Bennett	Bennett	k1gMnSc1	Bennett
and	and	k?	and
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
:	:	kIx,	:
Cheek	Cheek	k6eAd1	Cheek
To	ten	k3xDgNnSc4	ten
Cheek	Cheek	k1gMnSc1	Cheek
LIVE	LIVE	kA	LIVE
<g/>
!	!	kIx.	!
</s>
<s>
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
televizní	televizní	k2eAgInSc4d1	televizní
stanicí	stanice	k1gFnSc7	stanice
PBS	PBS	kA	PBS
byli	být	k5eAaImAgMnP	být
Gaga	Gagum	k1gNnSc2	Gagum
a	a	k8xC	a
Bennett	Bennetta	k1gFnPc2	Bennetta
nominováni	nominován	k2eAgMnPc1d1	nominován
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Gaga	Gagum	k1gNnSc2	Gagum
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
I	i	k9	i
Wish	Wish	k1gInSc1	Wish
od	od	k7c2	od
Stevieho	Stevie	k1gMnSc2	Stevie
Wondera	Wonder	k1gMnSc2	Wonder
na	na	k7c6	na
speciálním	speciální	k2eAgInSc6d1	speciální
koncertu	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
<g/>
.	.	kIx.	.
<g/>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
87	[number]	k4	87
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
udílení	udílení	k1gNnSc2	udílení
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
svým	svůj	k3xOyFgNnSc7	svůj
vystoupením	vystoupení	k1gNnSc7	vystoupení
vzdala	vzdát	k5eAaPmAgFnS	vzdát
hold	hold	k1gInSc4	hold
Julii	Julie	k1gFnSc4	Julie
Andrews	Andrews	k1gInSc4	Andrews
a	a	k8xC	a
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
filmovému	filmový	k2eAgInSc3d1	filmový
muzikálu	muzikál	k1gInSc2	muzikál
The	The	k1gFnPc2	The
Sound	Sound	k1gMnSc1	Sound
of	of	k?	of
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
Za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
Gaga	Gaga	k1gFnSc1	Gaga
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
tyto	tento	k3xDgFnPc4	tento
následující	následující	k2eAgFnPc4d1	následující
písně	píseň	k1gFnPc4	píseň
The	The	k1gMnSc1	The
Sound	Sound	k1gMnSc1	Sound
Of	Of	k1gMnSc1	Of
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Favorite	favorit	k1gInSc5	favorit
Things	Things	k1gInSc1	Things
<g/>
,	,	kIx,	,
Edelweiss	Edelweiss	k1gInSc1	Edelweiss
<g/>
,	,	kIx,	,
a	a	k8xC	a
Climb	Climb	k1gMnSc1	Climb
Every	Evera	k1gFnSc2	Evera
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
velice	velice	k6eAd1	velice
kladně	kladně	k6eAd1	kladně
hodnotila	hodnotit	k5eAaImAgNnP	hodnotit
světová	světový	k2eAgNnPc1d1	světové
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
ostatní	ostatní	k2eAgFnPc4d1	ostatní
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
nejdiskutovanějším	diskutovaný	k2eAgInSc7d3	nejdiskutovanější
momentem	moment	k1gInSc7	moment
večera	večer	k1gInSc2	večer
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
i	i	k8xC	i
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napsali	napsat	k5eAaBmAgMnP	napsat
o	o	k7c4	o
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
Lady	Lada	k1gFnSc2	Lada
Gaga	Gagum	k1gNnSc2	Gagum
na	na	k7c6	na
Oscarech	Oscar	k1gInPc6	Oscar
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
předefinovat	předefinovat	k5eAaPmF	předefinovat
její	její	k3xOp3gFnSc4	její
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
oscarovém	oscarový	k2eAgNnSc6d1	oscarové
vystoupení	vystoupení	k1gNnSc6	vystoupení
nejvíce	hodně	k6eAd3	hodně
šokující	šokující	k2eAgMnSc1d1	šokující
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
málo	málo	k6eAd1	málo
šokující	šokující	k2eAgMnSc1d1	šokující
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
verzí	verze	k1gFnSc7	verze
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
tím	ten	k3xDgNnSc7	ten
vůbec	vůbec	k9	vůbec
nejpřekvapivějším	překvapivý	k2eAgInSc7d3	nejpřekvapivější
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
opřela	opřít	k5eAaPmAgFnS	opřít
se	se	k3xPyFc4	se
o	o	k7c4	o
samotnou	samotný	k2eAgFnSc4d1	samotná
píseň	píseň	k1gFnSc4	píseň
&	&	k?	&
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
prakticky	prakticky	k6eAd1	prakticky
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
současnou	současný	k2eAgFnSc4d1	současná
éru	éra	k1gFnSc4	éra
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
coby	coby	k?	coby
podívanou	podívaná	k1gFnSc4	podívaná
<g/>
.	.	kIx.	.
</s>
<s>
Katy	Katy	k1gFnSc4	Katy
Perry	Perr	k1gInPc4	Perr
a	a	k8xC	a
Miley	Milea	k1gFnPc4	Milea
Cyrusová	Cyrusový	k2eAgNnPc4d1	Cyrusový
jí	on	k3xPp3gFnSc3	on
obě	dva	k4xCgFnPc1	dva
něco	něco	k3yInSc4	něco
dluží	dlužit	k5eAaImIp3nS	dlužit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
časopis	časopis	k1gInSc1	časopis
Time	Time	k1gFnSc1	Time
<g/>
"	"	kIx"	"
<g/>
Gaga	Gaga	k1gFnSc1	Gaga
vytáhla	vytáhnout	k5eAaPmAgFnS	vytáhnout
každý	každý	k3xTgInSc4	každý
vysoký	vysoký	k2eAgInSc4d1	vysoký
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
''	''	k?	''
<g/>
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
"	"	kIx"	"
<g/>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
prostě	prostě	k6eAd1	prostě
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
letošní	letošní	k2eAgInSc4d1	letošní
Oscar	Oscar	k1gInSc4	Oscar
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
Mirror	Mirror	k1gInSc1	Mirror
<g/>
"	"	kIx"	"
<g/>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
zachránila	zachránit	k5eAaPmAgFnS	zachránit
Oscary	Oscara	k1gFnPc4	Oscara
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
Washington	Washington	k1gInSc1	Washington
PostGaga	PostGaga	k1gFnSc1	PostGaga
také	také	k9	také
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
Twitteru	Twitter	k1gInSc6	Twitter
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
krátké	krátký	k2eAgNnSc4d1	krátké
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
American	American	k1gInSc4	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
seriálu	seriál	k1gInSc6	seriál
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
nebo	nebo	k8xC	nebo
TV	TV	kA	TV
filmu	film	k1gInSc2	film
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
na	na	k7c6	na
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
historicky	historicky	k6eAd1	historicky
prvních	první	k4xOgFnPc2	první
Evropských	evropský	k2eAgFnPc2d1	Evropská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
<g/>
.	.	kIx.	.
</s>
<s>
Zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
píseň	píseň	k1gFnSc1	píseň
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
Imagine	Imagin	k1gInSc5	Imagin
<g/>
.18	.18	k4	.18
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
převzala	převzít	k5eAaPmAgFnS	převzít
v	v	k7c4	v
Songwriters	Songwriters	k1gInSc4	Songwriters
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
ocenění	ocenění	k1gNnSc2	ocenění
Contemporary	Contemporara	k1gFnSc2	Contemporara
Icon	Icon	k1gMnSc1	Icon
Award	Award	k1gMnSc1	Award
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
ikonický	ikonický	k2eAgInSc4d1	ikonický
status	status	k1gInSc4	status
v	v	k7c6	v
popové	popový	k2eAgFnSc6d1	popová
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
největší	veliký	k2eAgFnPc4d3	veliký
skladatelské	skladatelský	k2eAgFnPc4d1	skladatelská
legendy	legenda	k1gFnPc4	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
udělovala	udělovat	k5eAaImAgFnS	udělovat
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
ji	on	k3xPp3gFnSc4	on
předal	předat	k5eAaPmAgMnS	předat
její	její	k3xOp3gMnSc1	její
hudební	hudební	k2eAgMnSc1d1	hudební
partner	partner	k1gMnSc1	partner
Tony	Tony	k1gMnSc1	Tony
Bennett	Bennett	k1gMnSc1	Bennett
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k6eAd1	Gaga
na	na	k7c6	na
akci	akce	k1gFnSc6	akce
také	také	k9	také
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Up	Up	k1gFnSc7	Up
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
4	[number]	k4	4
Non	Non	k1gMnSc1	Non
Blondes	Blondes	k1gMnSc1	Blondes
jako	jako	k9	jako
poctu	pocta	k1gFnSc4	pocta
Lindě	Linda	k1gFnSc3	Linda
Perry	Perra	k1gMnSc2	Perra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
večer	večer	k1gInSc4	večer
také	také	k9	také
přebírala	přebírat	k5eAaImAgFnS	přebírat
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
<g/>
Únor	únor	k1gInSc1	únor
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
měsícem	měsíc	k1gInSc7	měsíc
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Super	super	k2eAgInSc6d1	super
Bowlu	Bowl	k1gInSc6	Bowl
50	[number]	k4	50
zpívala	zpívat	k5eAaImAgFnS	zpívat
americkou	americký	k2eAgFnSc4d1	americká
státní	státní	k2eAgFnSc4d1	státní
hymnu	hymna	k1gFnSc4	hymna
<g/>
,	,	kIx,	,
během	během	k7c2	během
Cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
s	s	k7c7	s
několika	několik	k4yIc7	několik
songy	song	k1gInPc7	song
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Bowieho	Bowie	k1gMnSc2	Bowie
a	a	k8xC	a
na	na	k7c6	na
Oscarech	Oscar	k1gInPc6	Oscar
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Til	til	k1gInSc4	til
It	It	k1gMnPc3	It
Happens	Happensa	k1gFnPc2	Happensa
To	to	k9	to
You	You	k1gMnPc2	You
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
Gaga	Gaga	k1gFnSc1	Gaga
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
chystá	chystat	k5eAaImIp3nS	chystat
vydání	vydání	k1gNnSc4	vydání
pilotního	pilotní	k2eAgInSc2d1	pilotní
singlu	singl	k1gInSc2	singl
Perfect	Perfect	k2eAgInSc1d1	Perfect
Illusion	Illusion	k1gInSc1	Illusion
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
pátého	pátý	k4xOgInSc2	pátý
studiového	studiový	k2eAgInSc2d1	studiový
alba	album	k1gNnPc4	album
Joanne	Joann	k1gInSc5	Joann
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Ronsonem	Ronson	k1gMnSc7	Ronson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
celého	celý	k2eAgNnSc2d1	celé
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Joanne	Joann	k1gInSc5	Joann
získalo	získat	k5eAaPmAgNnS	získat
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
hudebním	hudební	k2eAgInSc6d1	hudební
webu	web	k1gInSc6	web
iREPORT	iREPORT	k?	iREPORT
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
recenze	recenze	k1gFnSc2	recenze
však	však	k9	však
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
deska	deska	k1gFnSc1	deska
"	"	kIx"	"
<g/>
sotva	sotva	k6eAd1	sotva
rozsoudí	rozsoudit	k5eAaPmIp3nS	rozsoudit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k2eAgFnSc1d1	Gaga
padlá	padlý	k2eAgFnSc1d1	padlá
popová	popový	k2eAgFnSc1d1	popová
ikona	ikona	k1gFnSc1	ikona
nebo	nebo	k8xC	nebo
opěvovaná	opěvovaný	k2eAgFnSc1d1	opěvovaná
modla	modla	k1gFnSc1	modla
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
už	už	k6eAd1	už
mnohokrát	mnohokrát	k6eAd1	mnohokrát
prokázala	prokázat	k5eAaPmAgFnS	prokázat
svou	svůj	k3xOyFgFnSc4	svůj
univerzálnost	univerzálnost	k1gFnSc4	univerzálnost
a	a	k8xC	a
vizionářství	vizionářství	k1gNnSc4	vizionářství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Joanne	Joann	k1gMnSc5	Joann
<g/>
,	,	kIx,	,
Super	super	k6eAd1	super
Bowl	Bowl	k1gInSc1	Bowl
a	a	k8xC	a
Five	Five	k1gInSc1	Five
Foot	Foot	k1gMnSc1	Foot
Two	Two	k1gMnSc1	Two
===	===	k?	===
</s>
</p>
<p>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
desky	deska	k1gFnSc2	deska
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
tvůrčí	tvůrčí	k2eAgFnSc3d1	tvůrčí
pauze	pauza	k1gFnSc3	pauza
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
Perfect	Perfect	k2eAgInSc1d1	Perfect
Illusion	Illusion	k1gInSc1	Illusion
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
prestižního	prestižní	k2eAgInSc2d1	prestižní
amerického	americký	k2eAgInSc2d1	americký
hudebního	hudební	k2eAgInSc2d1	hudební
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
song	song	k1gInSc1	song
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gMnSc1	Gaga
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
neumístil	umístit	k5eNaPmAgMnS	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
A-YO	A-YO	k1gFnSc2	A-YO
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgFnSc4d1	nadcházející
desku	deska	k1gFnSc4	deska
Joanne	Joann	k1gInSc5	Joann
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
po	po	k7c6	po
sestře	sestra	k1gFnSc6	sestra
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
letech	let	k1gInPc6	let
na	na	k7c4	na
lupus	lupus	k1gInSc4	lupus
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
celého	celý	k2eAgNnSc2d1	celé
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
po	po	k7c6	po
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
uvážení	uvážení	k1gNnSc6	uvážení
však	však	k8xC	však
pozici	pozice	k1gFnSc4	pozice
druhého	druhý	k4xOgInSc2	druhý
singlu	singl	k1gInSc2	singl
získala	získat	k5eAaPmAgFnS	získat
balada	balada	k1gFnSc1	balada
Million	Million	k1gInSc1	Million
Reasons	Reasons	k1gInSc1	Reasons
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
a	a	k8xC	a
pozitivnější	pozitivní	k2eAgInSc4d2	pozitivnější
ohlas	ohlas	k1gInSc4	ohlas
než	než	k8xS	než
původně	původně	k6eAd1	původně
A-YO	A-YO	k1gFnSc1	A-YO
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
poprvé	poprvé	k6eAd1	poprvé
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
Japonska	Japonsko	k1gNnSc2	Japonsko
při	při	k7c6	při
propagaci	propagace	k1gFnSc6	propagace
nové	nový	k2eAgFnSc2d1	nová
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
udělat	udělat	k5eAaPmF	udělat
opak	opak	k1gInSc1	opak
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělám	udělat	k5eAaPmIp1nS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevím	objevit	k5eAaPmIp1nS	objevit
na	na	k7c6	na
nějakém	nějaký	k3yIgInSc6	nějaký
podělaném	podělaný	k2eAgInSc6d1	podělaný
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
v	v	k7c6	v
šatech	šat	k1gInPc6	šat
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
s	s	k7c7	s
devadesáti	devadesát	k4xCc7	devadesát
nahými	nahý	k2eAgMnPc7d1	nahý
chlapy	chlap	k1gMnPc7	chlap
a	a	k8xC	a
jednorožci	jednorožec	k1gMnPc1	jednorožec
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
udělám	udělat	k5eAaPmIp1nS	udělat
něco	něco	k3yInSc1	něco
šokujícího	šokující	k2eAgNnSc2d1	šokující
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
prozradila	prozradit	k5eAaPmAgFnS	prozradit
Lady	lady	k1gFnSc1	lady
GaGa	GaGa	k1gFnSc1	GaGa
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Five	Fiv	k1gInSc2	Fiv
Foot	Foot	k1gMnSc1	Foot
Two	Two	k1gMnSc1	Two
<g/>
.	.	kIx.	.
<g/>
Celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
Joanne	Joann	k1gInSc5	Joann
i	i	k8xC	i
Deluxe	Deluxe	k1gFnSc1	Deluxe
verzi	verze	k1gFnSc4	verze
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
vydala	vydat	k5eAaPmAgFnS	vydat
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
a	a	k8xC	a
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
americké	americký	k2eAgFnSc2d1	americká
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc1	billboard
Top	topit	k5eAaImRp2nS	topit
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Markem	Marek	k1gMnSc7	Marek
Ronsonem	Ronson	k1gMnSc7	Ronson
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
sledu	sled	k1gInSc6	sled
událostí	událost	k1gFnPc2	událost
Lady	lady	k1gFnSc4	lady
Gaga	Gagum	k1gNnSc2	Gagum
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
American	American	k1gInSc4	American
Music	Music	k1gMnSc1	Music
Awards	Awards	k1gInSc4	Awards
2016	[number]	k4	2016
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
na	na	k7c4	na
Victoria	Victorium	k1gNnPc4	Victorium
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Secret	Secret	k1gInSc1	Secret
Fashion	Fashion	k1gInSc4	Fashion
Show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
písně	píseň	k1gFnPc4	píseň
A-YO	A-YO	k1gFnSc2	A-YO
<g/>
,	,	kIx,	,
Million	Million	k1gInSc4	Million
Reasons	Reasonsa	k1gFnPc2	Reasonsa
a	a	k8xC	a
John	John	k1gMnSc1	John
Wayne	Wayn	k1gInSc5	Wayn
-	-	kIx~	-
všechny	všechen	k3xTgMnPc4	všechen
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
její	její	k3xOp3gFnSc2	její
novinky	novinka	k1gFnSc2	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
vystoupením	vystoupení	k1gNnSc7	vystoupení
pro	pro	k7c4	pro
Lady	lady	k1gFnSc4	lady
Gaga	Gag	k1gInSc2	Gag
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
pochyby	pochyba	k1gFnSc2	pochyba
show	show	k1gFnSc2	show
v	v	k7c6	v
poločase	poločas	k1gInSc6	poločas
Super	super	k2eAgInSc2d1	super
Bowlu	Bowl	k1gInSc2	Bowl
51	[number]	k4	51
(	(	kIx(	(
<g/>
Super	super	k2eAgInSc1d1	super
Bowl	Bowl	k1gInSc1	Bowl
Halftime	halftime	k1gInSc1	halftime
Show	show	k1gFnSc1	show
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
necelých	celý	k2eNgInPc2d1	necelý
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
zazněly	zaznít	k5eAaPmAgInP	zaznít
její	její	k3xOp3gInPc1	její
největší	veliký	k2eAgInPc1d3	veliký
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
Poker	poker	k1gInSc1	poker
Face	Fac	k1gFnSc2	Fac
<g/>
,	,	kIx,	,
Born	Born	k1gNnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
<g/>
,	,	kIx,	,
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
,	,	kIx,	,
Telephone	Telephon	k1gMnSc5	Telephon
<g/>
,	,	kIx,	,
Bad	Bad	k1gFnSc3	Bad
Romance	romance	k1gFnSc2	romance
a	a	k8xC	a
také	také	k9	také
Million	Million	k1gInSc1	Million
Reasons	Reasons	k1gInSc1	Reasons
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
na	na	k7c6	na
Super	super	k2eAgInSc6d1	super
Bowlu	Bowl	k1gInSc6	Bowl
umístil	umístit	k5eAaPmAgMnS	umístit
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
místo	místo	k1gNnSc4	místo
Beyoncé	Beyoncá	k1gFnSc2	Beyoncá
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
porodu	porod	k1gInSc3	porod
dvojčat	dvojče	k1gNnPc2	dvojče
nemohla	moct	k5eNaImAgFnS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
)	)	kIx)	)
na	na	k7c6	na
prestižním	prestižní	k2eAgInSc6d1	prestižní
americkém	americký	k2eAgInSc6d1	americký
festivalu	festival	k1gInSc6	festival
Coachella	Coachella	k1gFnSc1	Coachella
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
headlinerkou	headlinerka	k1gFnSc7	headlinerka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
vystoupení	vystoupení	k1gNnSc2	vystoupení
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
píseň	píseň	k1gFnSc1	píseň
The	The	k1gFnSc1	The
Cure	Cure	k1gFnSc1	Cure
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsala	napsat	k5eAaBmAgFnS	napsat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
DJ	DJ	kA	DJ
Whites	Whites	k1gInSc1	Whites
Shadowem	Shadowem	k1gInSc1	Shadowem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nazvala	nazvat	k5eAaBmAgFnS	nazvat
jednoduše	jednoduše	k6eAd1	jednoduše
Joanne	Joann	k1gInSc5	Joann
World	Worldo	k1gNnPc2	Worldo
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
60	[number]	k4	60
zastávek	zastávka	k1gFnPc2	zastávka
po	po	k7c6	po
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
však	však	k9	však
musela	muset	k5eAaImAgFnS	muset
zrušit	zrušit	k5eAaPmF	zrušit
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
následující	následující	k2eAgInPc1d1	následující
termíny	termín	k1gInPc1	termín
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
čelila	čelit	k5eAaImAgFnS	čelit
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
závažným	závažný	k2eAgInPc3d1	závažný
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
musela	muset	k5eAaImAgFnS	muset
zrušit	zrušit	k5eAaPmF	zrušit
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
část	část	k1gFnSc4	část
turné	turné	k1gNnSc2	turné
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnSc1	Way
Ball	Ball	k1gMnSc1	Ball
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c6	na
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
poranila	poranit	k5eAaPmAgFnS	poranit
kyčel	kyčel	k1gFnSc1	kyčel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zrušením	zrušení	k1gNnSc7	zrušení
koncertů	koncert	k1gInPc2	koncert
stojí	stát	k5eAaImIp3nP	stát
onemocnění	onemocnění	k1gNnPc1	onemocnění
zvané	zvaný	k2eAgFnSc2d1	zvaná
fibromyalgie	fibromyalgie	k1gFnSc2	fibromyalgie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
jí	jíst	k5eAaImIp3nS	jíst
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
chronickou	chronický	k2eAgFnSc4d1	chronická
únavu	únava	k1gFnSc4	únava
a	a	k8xC	a
bolesti	bolest	k1gFnPc4	bolest
svalů	sval	k1gInPc2	sval
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Slibuju	slibovat	k5eAaImIp1nS	slibovat
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátím	vrátit	k5eAaPmIp1nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
teď	teď	k6eAd1	teď
musím	muset	k5eAaImIp1nS	muset
upřednostnit	upřednostnit	k5eAaPmF	upřednostnit
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
svoje	svůj	k3xOyFgNnSc4	svůj
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Miluju	milovat	k5eAaImIp1nS	milovat
vás	vy	k3xPp2nPc4	vy
<g/>
,	,	kIx,	,
navždycky	navždycky	k6eAd1	navždycky
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedla	uvést	k5eAaPmAgFnS	uvést
zdrcená	zdrcený	k2eAgFnSc1d1	zdrcená
popová	popový	k2eAgFnSc1d1	popová
diva	diva	k1gFnSc1	diva
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2017	[number]	k4	2017
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
Netflix	Netflix	k1gInSc4	Netflix
100	[number]	k4	100
<g/>
minutový	minutový	k2eAgInSc4d1	minutový
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
alba	album	k1gNnSc2	album
Joanne	Joann	k1gInSc5	Joann
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnPc1	vystoupení
na	na	k7c4	na
Super	super	k2eAgFnSc4d1	super
Bowlu	Bowla	k1gFnSc4	Bowla
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
diváky	divák	k1gMnPc4	divák
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
i	i	k9	i
do	do	k7c2	do
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
samotné	samotný	k2eAgFnSc2d1	samotná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2018	[number]	k4	2018
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
Enigma	enigma	k1gFnSc1	enigma
a	a	k8xC	a
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
Gaga	Gaga	k1gFnSc1	Gaga
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
ji	on	k3xPp3gFnSc4	on
budeme	být	k5eAaImBp1nP	být
moct	moct	k5eAaImF	moct
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Bradleyho	Bradley	k1gMnSc2	Bradley
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
představitelku	představitelka	k1gFnSc4	představitelka
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
dívky	dívka	k1gFnSc2	dívka
Ally	Alla	k1gFnSc2	Alla
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stane	stanout	k5eAaPmIp3nS	stanout
hudební	hudební	k2eAgFnSc1d1	hudební
ikona	ikona	k1gFnSc1	ikona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
písních	píseň	k1gFnPc6	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Mark	Mark	k1gMnSc1	Mark
Nilan	Nilan	k1gMnSc1	Nilan
i	i	k8xC	i
textařka	textařka	k1gFnSc1	textařka
Diane	Dian	k1gInSc5	Dian
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
napsala	napsat	k5eAaPmAgFnS	napsat
písně	píseň	k1gFnPc4	píseň
Why	Why	k1gMnSc2	Why
Did	Did	k1gMnSc2	Did
You	You	k1gMnSc2	You
Do	do	k7c2	do
That	Thata	k1gFnPc2	Thata
<g/>
?	?	kIx.	?
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
premiéru	premiéra	k1gFnSc4	premiéra
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Zlomil	zlomit	k5eAaPmAgInS	zlomit
rekord	rekord	k1gInSc1	rekord
nejvýdělečnějšího	výdělečný	k2eAgInSc2d3	nejvýdělečnější
snímku	snímek	k1gInSc2	snímek
se	s	k7c7	s
zpevačkou	zpevačka	k1gFnSc7	zpevačka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgMnSc1d1	osobní
strážce	strážce	k1gMnSc1	strážce
vydělal	vydělat	k5eAaPmAgMnS	vydělat
411	[number]	k4	411
milionů	milion	k4xCgInPc2	milion
$	$	kIx~	$
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
překonal	překonat	k5eAaPmAgMnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
Golden	Goldna	k1gFnPc2	Goldna
Globes	Globesa	k1gFnPc2	Globesa
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
song	song	k1gInSc4	song
(	(	kIx(	(
<g/>
Shallow	Shallow	k1gFnSc4	Shallow
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
National	National	k1gFnSc2	National
Board	Board	k1gInSc1	Board
of	of	k?	of
Review	Review	k1gFnSc2	Review
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
Critics	Critics	k1gInSc4	Critics
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
také	také	k9	také
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
na	na	k7c4	na
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guilda	k1gFnPc2	Guilda
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
neodnesla	odnést	k5eNaPmAgFnS	odnést
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uchvátila	uchvátit	k5eAaPmAgFnS	uchvátit
ostatní	ostatní	k2eAgFnSc1d1	ostatní
svou	svůj	k3xOyFgFnSc7	svůj
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
bílou	bílý	k2eAgFnSc7d1	bílá
róbou	róba	k1gFnSc7	róba
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
8	[number]	k4	8
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
Shallow	Shallow	k1gFnSc4	Shallow
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
okamžikem	okamžik	k1gInSc7	okamžik
večera	večer	k1gInSc2	večer
bylo	být	k5eAaImAgNnS	být
vystoupení	vystoupení	k1gNnSc4	vystoupení
Gagy	gag	k1gInPc4	gag
a	a	k8xC	a
Bradleyho	Bradley	k1gMnSc4	Bradley
se	s	k7c7	s
zamilovanou	zamilovaný	k2eAgFnSc7d1	zamilovaná
písní	píseň	k1gFnSc7	píseň
Shallow	Shallow	k1gFnSc2	Shallow
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předávání	předávání	k1gNnSc6	předávání
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Gaga	Gagum	k1gNnPc1	Gagum
a	a	k8xC	a
Bradley	Bradlea	k1gFnPc1	Bradlea
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
jako	jako	k8xC	jako
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
Lady	lady	k1gFnSc1	lady
GaGa	GaGa	k1gFnSc1	GaGa
nominovaná	nominovaný	k2eAgFnSc1d1	nominovaná
na	na	k7c4	na
5	[number]	k4	5
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
4	[number]	k4	4
ceny	cena	k1gFnPc4	cena
pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
Shallow	Shallow	k1gFnSc2	Shallow
<g/>
:	:	kIx,	:
Song	song	k1gInSc1	song
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Year	Year	k1gMnSc1	Year
<g/>
,	,	kIx,	,
Record	Record	k1gMnSc1	Record
Of	Of	k1gFnSc2	Of
The	The	k1gMnSc1	The
Year	Year	k1gMnSc1	Year
<g/>
,	,	kIx,	,
Best	Best	k2eAgInSc1d1	Best
Pop	pop	k1gInSc1	pop
Duo	duo	k1gNnSc1	duo
<g/>
/	/	kIx~	/
<g/>
Group	Group	k1gInSc1	Group
Performance	performance	k1gFnSc2	performance
<g/>
,	,	kIx,	,
Best	Best	k2eAgInSc1d1	Best
Song	song	k1gInSc1	song
Written	Written	k2eAgInSc1d1	Written
For	forum	k1gNnPc2	forum
Visual	Visual	k1gMnSc1	Visual
Media	medium	k1gNnSc2	medium
a	a	k8xC	a
pak	pak	k6eAd1	pak
1	[number]	k4	1
nominace	nominace	k1gFnSc2	nominace
pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
Joanne	Joann	k1gInSc5	Joann
(	(	kIx(	(
<g/>
Where	Wher	k1gInSc5	Wher
Do	do	k7c2	do
You	You	k1gMnPc2	You
Think	Think	k1gMnSc1	Think
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Goin	Goin	k1gInSc1	Goin
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
Best	Best	k2eAgInSc1d1	Best
Pop	pop	k1gInSc1	pop
Solo	Solo	k6eAd1	Solo
Performance	performance	k1gFnSc2	performance
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
odnesla	odnést	k5eAaPmAgFnS	odnést
3	[number]	k4	3
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
předávání	předávání	k1gNnSc6	předávání
s	s	k7c7	s
rockovou	rockový	k2eAgFnSc7d1	rocková
verzí	verze	k1gFnSc7	verze
písně	píseň	k1gFnSc2	píseň
Shallow	Shallow	k1gFnSc2	Shallow
společně	společně	k6eAd1	společně
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Ronsonem	Ronson	k1gMnSc7	Ronson
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
slavila	slavit	k5eAaImAgFnS	slavit
GaGa	GaG	k1gInSc2	GaG
navíc	navíc	k6eAd1	navíc
také	také	k9	také
10	[number]	k4	10
let	léto	k1gNnPc2	léto
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
její	její	k3xOp3gFnSc1	její
songy	song	k1gInPc4	song
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
oslavili	oslavit	k5eAaPmAgMnP	oslavit
také	také	k6eAd1	také
10	[number]	k4	10
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
zahájila	zahájit	k5eAaPmAgFnS	zahájit
GaGa	GaG	k2eAgFnSc1d1	GaGa
svou	svůj	k3xOyFgFnSc4	svůj
show	show	k1gFnSc4	show
Enigma	enigma	k1gNnSc4	enigma
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncertní	koncertní	k2eAgNnSc4d1	koncertní
vystoupení	vystoupení	k1gNnSc4	vystoupení
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
jejími	její	k3xOp3gInPc7	její
hity	hit	k1gInPc7	hit
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
,	,	kIx,	,
The	The	k1gMnSc7	The
Edge	Edg	k1gMnSc2	Edg
of	of	k?	of
Glory	Glora	k1gMnSc2	Glora
<g/>
,	,	kIx,	,
Bad	Bad	k1gMnSc2	Bad
Romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
Poker	poker	k1gInSc4	poker
Face	Fac	k1gFnSc2	Fac
<g/>
,	,	kIx,	,
Alejandro	Alejandra	k1gFnSc5	Alejandra
<g/>
,	,	kIx,	,
Paparazzi	Paparazze	k1gFnSc6	Paparazze
<g/>
,	,	kIx,	,
Dance	Danka	k1gFnSc6	Danka
in	in	k?	in
the	the	k?	the
dark	dark	k1gInSc1	dark
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
...	...	k?	...
Show	show	k1gFnSc1	show
začala	začít	k5eAaPmAgFnS	začít
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
show	show	k1gFnSc2	show
se	se	k3xPyFc4	se
stihla	stihnout	k5eAaPmAgFnS	stihnout
GaGa	GaGa	k1gFnSc1	GaGa
převléct	převléct	k5eAaPmF	převléct
do	do	k7c2	do
několika	několik	k4yIc2	několik
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
při	při	k7c6	při
písni	píseň	k1gFnSc6	píseň
Scheibe	Scheib	k1gInSc5	Scheib
byla	být	k5eAaImAgNnP	být
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
velkém	velký	k2eAgMnSc6d1	velký
robotovi	robot	k1gMnSc6	robot
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
show	show	k1gNnSc4	show
se	se	k3xPyFc4	se
byly	být	k5eAaImAgFnP	být
podívat	podívat	k5eAaPmF	podívat
hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xS	jako
Katy	Kata	k1gFnPc1	Kata
Perry	Perra	k1gFnSc2	Perra
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
Celine	Celin	k1gInSc5	Celin
Dion	Dion	k1gMnSc1	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
show	show	k1gFnPc2	show
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Bradley	Bradle	k1gMnPc4	Bradle
Cooper	Cooper	k1gInSc4	Cooper
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
zaspíval	zaspívat	k5eAaImAgInS	zaspívat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
s	s	k7c7	s
Gagou	Gaga	k1gMnSc7	Gaga
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
společnou	společný	k2eAgFnSc4d1	společná
píseň	píseň	k1gFnSc4	píseň
Shallow	Shallow	k1gFnSc2	Shallow
z	z	k7c2	z
filmu	film	k1gInSc2	film
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
show	show	k1gFnSc2	show
Enigma	enigma	k1gNnSc1	enigma
si	se	k3xPyFc3	se
GaGa	GaGum	k1gNnSc2	GaGum
také	také	k6eAd1	také
připravila	připravit	k5eAaPmAgFnS	připravit
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
této	tento	k3xDgFnSc2	tento
show	show	k1gFnSc2	show
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
JAZZ	jazz	k1gInSc4	jazz
a	a	k8xC	a
PIANO	piano	k1gNnSc4	piano
Show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaspívala	zaspívat	k5eAaPmAgFnS	zaspívat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Cheek	Cheek	k6eAd1	Cheek
to	ten	k3xDgNnSc4	ten
Cheek	Cheek	k1gMnSc1	Cheek
např.	např.	kA	např.
Anything	Anything	k1gInSc1	Anything
goes	goes	k1gInSc1	goes
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Lady	lady	k1gFnSc1	lady
is	is	k?	is
a	a	k8xC	a
Tramp	tramp	k1gMnSc1	tramp
nebo	nebo	k8xC	nebo
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
hity	hit	k1gInPc4	hit
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Born	Born	k1gNnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
<g/>
,	,	kIx,	,
Paparazzi	Paparazze	k1gFnSc4	Paparazze
a	a	k8xC	a
Bad	Bad	k1gFnSc4	Bad
Romance	romance	k1gFnSc2	romance
v	v	k7c6	v
akustických	akustický	k2eAgNnPc6d1	akustické
aranžmá	aranžmá	k1gNnPc6	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
Nějaké	nějaký	k3yIgFnPc4	nějaký
písně	píseň	k1gFnPc4	píseň
si	se	k3xPyFc3	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zazpíval	zazpívat	k5eAaPmAgMnS	zazpívat
opět	opět	k6eAd1	opět
Tony	Tony	k1gMnSc1	Tony
Bennet	Bennet	k1gMnSc1	Bennet
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
u	u	k7c2	u
Cheek	Cheky	k1gFnPc2	Cheky
to	ten	k3xDgNnSc4	ten
Cheek	Cheek	k6eAd1	Cheek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
překvapením	překvapení	k1gNnSc7	překvapení
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
GaGu	gag	k1gInSc2	gag
od	od	k7c2	od
jejích	její	k3xOp3gMnPc2	její
fanoušků	fanoušek	k1gMnPc2	fanoušek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gInSc1	její
hit	hit	k1gInSc1	hit
Bad	Bad	k1gFnSc2	Bad
Romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
veřejném	veřejný	k2eAgInSc6d1	veřejný
nátlaku	nátlak	k1gInSc6	nátlak
na	na	k7c6	na
Lady	lady	k1gFnSc6	lady
Gaga	Gagus	k1gMnSc2	Gagus
ohledně	ohledně	k7c2	ohledně
spolupráce	spolupráce	k1gFnSc2	spolupráce
na	na	k7c6	na
songu	song	k1gInSc6	song
Do	do	k7c2	do
What	Whatum	k1gNnPc2	Whatum
You	You	k1gMnSc2	You
Want	Wanta	k1gFnPc2	Wanta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
nechala	nechat	k5eAaPmAgFnS	nechat
Gaga	Gaga	k1gFnSc1	Gaga
song	song	k1gInSc1	song
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
odstranit	odstranit	k5eAaPmF	odstranit
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
streamovacích	streamovací	k2eAgFnPc2d1	streamovací
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
R.	R.	kA	R.
Kelly	Kella	k1gFnSc2	Kella
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgInS	obvinit
z	z	k7c2	z
četného	četný	k2eAgNnSc2d1	četné
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
obtěžování	obtěžování	k1gNnSc2	obtěžování
nezletilých	nezletilých	k2eAgFnPc2d1	nezletilých
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
začalo	začít	k5eAaPmAgNnS	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
kdy	kdy	k6eAd1	kdy
vyjde	vyjít	k5eAaPmIp3nS	vyjít
LG	LG	kA	LG
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
její	její	k3xOp3gInSc1	její
šestém	šestý	k4xOgNnSc6	šestý
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
GaGa	GaGa	k1gFnSc1	GaGa
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
instagramovém	instagramový	k2eAgInSc6d1	instagramový
profilu	profil	k1gInSc6	profil
fanouškům	fanoušek	k1gMnPc3	fanoušek
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
už	už	k6eAd1	už
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Electric	Electrice	k1gFnPc2	Electrice
Lady	lady	k1gFnSc1	lady
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
společně	společně	k6eAd1	společně
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Ronsonem	Ronson	k1gMnSc7	Ronson
<g/>
,	,	kIx,	,
trumpetistou	trumpetista	k1gMnSc7	trumpetista
Brianem	Brian	k1gMnSc7	Brian
Newmanem	Newman	k1gMnSc7	Newman
a	a	k8xC	a
rapperem	rapper	k1gMnSc7	rapper
Tylerem	Tyler	k1gMnSc7	Tyler
<g/>
,	,	kIx,	,
the	the	k?	the
Creatorem	Creator	k1gInSc7	Creator
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
nedala	dát	k5eNaPmAgFnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
manažerů	manažer	k1gMnPc2	manažer
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
vyjde	vyjít	k5eAaPmIp3nS	vyjít
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
videoklipu	videoklip	k1gInSc2	videoklip
k	k	k7c3	k
písničce	písnička	k1gFnSc3	písnička
"	"	kIx"	"
<g/>
Yoü	Yoü	k1gFnSc1	Yoü
and	and	k?	and
I	i	k9	i
<g/>
"	"	kIx"	"
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Taylorem	Taylor	k1gMnSc7	Taylor
Kinneym	Kinneym	k1gInSc4	Kinneym
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
dvojice	dvojice	k1gFnSc1	dvojice
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
pár	pár	k1gInSc1	pár
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ukázala	ukázat	k5eAaPmAgFnS	ukázat
svého	svůj	k3xOyFgMnSc4	svůj
nového	nový	k2eAgMnSc4d1	nový
přítele	přítel	k1gMnSc4	přítel
Christiana	Christian	k1gMnSc4	Christian
Carino	Carin	k2eAgNnSc1d1	Carin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zásnuby	zásnub	k1gInPc4	zásnub
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2019	[number]	k4	2019
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
styl	styl	k1gInSc4	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
,	,	kIx,	,
Debbie	Debbie	k1gFnSc2	Debbie
Harry	Harra	k1gFnSc2	Harra
z	z	k7c2	z
Blondie	Blondie	k1gFnSc2	Blondie
či	či	k8xC	či
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
z	z	k7c2	z
Queenu	Queen	k1gInSc2	Queen
<g/>
,	,	kIx,	,
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Cyndi	Cynd	k1gMnPc1	Cynd
Lauper	Laupra	k1gFnPc2	Laupra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Beatles	Beatles	k1gFnPc1	Beatles
<g/>
,	,	kIx,	,
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
popové	popový	k2eAgFnPc4d1	popová
ikony	ikona	k1gFnPc4	ikona
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Madonna	Madonna	k1gFnSc1	Madonna
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vidí	vidět	k5eAaImIp3nS	vidět
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
srovnání	srovnání	k1gNnSc2	srovnání
s	s	k7c7	s
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
být	být	k5eAaImF	být
předvídatelná	předvídatelný	k2eAgFnSc1d1	předvídatelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
provést	provést	k5eAaPmF	provést
v	v	k7c4	v
popu	pop	k1gInSc2	pop
revoluci	revoluce	k1gFnSc3	revoluce
–	–	k?	–
ta	ten	k3xDgNnPc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
před	před	k7c7	před
25	[number]	k4	25
lety	léto	k1gNnPc7	léto
právě	právě	k6eAd1	právě
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepochybný	pochybný	k2eNgInSc1d1	nepochybný
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
malíř	malíř	k1gMnSc1	malíř
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
obrazech	obraz	k1gInPc6	obraz
<g/>
,	,	kIx,	,
když	když	k8xS	když
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c4	na
Grammy	Gramm	k1gMnPc4	Gramm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
obraz	obraz	k1gInSc1	obraz
"	"	kIx"	"
<g/>
Geopoliticus	Geopoliticus	k1gInSc1	Geopoliticus
Child	Child	k1gMnSc1	Child
Watching	Watching	k1gInSc1	Watching
the	the	k?	the
Birth	Birth	k1gInSc1	Birth
of	of	k?	of
the	the	k?	the
New	New	k1gMnSc1	New
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
když	když	k8xS	když
zpívala	zpívat	k5eAaImAgFnS	zpívat
pro	pro	k7c4	pro
britskou	britský	k2eAgFnSc4d1	britská
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
jakési	jakýsi	k3yIgFnSc2	jakýsi
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
pódiem	pódium	k1gNnSc7	pódium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Tentation	Tentation	k1gInSc1	Tentation
de	de	k?	de
saint	saint	k1gInSc1	saint
Antoine	Antoin	k1gInSc5	Antoin
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inspiraci	inspirace	k1gFnSc4	inspirace
můžeme	moct	k5eAaImIp1nP	moct
také	také	k9	také
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
parfém	parfém	k1gInSc4	parfém
FAME	FAME	kA	FAME
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
scéně	scéna	k1gFnSc6	scéna
vidět	vidět	k5eAaImF	vidět
prvky	prvek	k1gInPc4	prvek
surrealismu	surrealismus	k1gInSc2	surrealismus
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
"	"	kIx"	"
<g/>
Zjevení	zjevení	k1gNnSc1	zjevení
tváře	tvář	k1gFnSc2	tvář
a	a	k8xC	a
ovocné	ovocný	k2eAgFnPc4d1	ovocná
mísy	mísa	k1gFnPc4	mísa
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vokálně	vokálně	k6eAd1	vokálně
je	být	k5eAaImIp3nS	být
přirovnávána	přirovnáván	k2eAgFnSc1d1	přirovnávána
ke	k	k7c3	k
Gwen	Gwen	k1gInSc1	Gwen
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ke	k	k7c3	k
Kylie	Kylius	k1gMnPc4	Kylius
Minogue	Minogu	k1gMnSc4	Minogu
a	a	k8xC	a
vizuálně	vizuálně	k6eAd1	vizuálně
k	k	k7c3	k
Amy	Amy	k1gFnSc3	Amy
Winehouse	Winehouse	k1gFnSc2	Winehouse
či	či	k8xC	či
Christině	Christin	k2eAgFnSc3d1	Christina
Aguileře	Aguilera	k1gFnSc3	Aguilera
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
básníkem	básník	k1gMnSc7	básník
je	být	k5eAaImIp3nS	být
pražský	pražský	k2eAgMnSc1d1	pražský
rodák	rodák	k1gMnSc1	rodák
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Mario	k1gMnSc4	Mario
Rilke	Rilk	k1gMnSc4	Rilk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbožňuje	zbožňovat	k5eAaImIp3nS	zbožňovat
módu	móda	k1gFnSc4	móda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
všechno	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
módě	móda	k1gFnSc3	móda
a	a	k8xC	a
stylu	styl	k1gInSc2	styl
zdědila	zdědit	k5eAaPmAgFnS	zdědit
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
a	a	k8xC	a
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
skládá	skládat	k5eAaImIp3nS	skládat
novou	nový	k2eAgFnSc4d1	nová
písničku	písnička	k1gFnSc4	písnička
<g/>
,	,	kIx,	,
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
vystupování	vystupování	k1gNnSc2	vystupování
a	a	k8xC	a
herecké	herecký	k2eAgFnSc2d1	herecká
interpretace	interpretace	k1gFnSc2	interpretace
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
inspiraci	inspirace	k1gFnSc4	inspirace
u	u	k7c2	u
Grace	Grace	k1gFnSc2	Grace
Jones	Jonesa	k1gFnPc2	Jonesa
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
produkční	produkční	k2eAgInSc1d1	produkční
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Haus	Haus	k1gInSc1	Haus
of	of	k?	of
Gaga	Gag	k1gInSc2	Gag
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
připravuje	připravovat	k5eAaImIp3nS	připravovat
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
účesy	účes	k1gInPc4	účes
a	a	k8xC	a
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
propriety	proprieta	k1gFnPc4	proprieta
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GaGa	GaGa	k6eAd1	GaGa
se	se	k3xPyFc4	se
také	také	k9	také
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
show	show	k1gFnSc6	show
s	s	k7c7	s
Ellen	Ellen	k1gInSc1	Ellen
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
filmu	film	k1gInSc2	film
Zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tetování	tetování	k1gNnSc2	tetování
===	===	k?	===
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
celkem	celkem	k6eAd1	celkem
čtyřiadvacet	čtyřiadvacet	k4xCc4	čtyřiadvacet
tetování	tetování	k1gNnPc2	tetování
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgFnSc1	všechen
svá	svůj	k3xOyFgNnPc4	svůj
tetování	tetování	k1gNnPc4	tetování
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kromě	kromě	k7c2	kromě
not	nota	k1gFnPc2	nota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
skládají	skládat	k5eAaImIp3nP	skládat
slovo	slovo	k1gNnSc4	slovo
GAGA	GAGA	kA	GAGA
a	a	k8xC	a
trumpety	trumpeta	k1gFnSc2	trumpeta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Tony	Tony	k1gMnSc1	Tony
Bennett	Bennett	k1gMnSc1	Bennett
a	a	k8xC	a
Gaga	Gaga	k1gMnSc1	Gaga
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
vytetovat	vytetovat	k5eAaPmF	vytetovat
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
paži	paže	k1gFnSc4	paže
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pořadu	pořad	k1gInSc2	pořad
Musicians	Musicians	k1gInSc1	Musicians
<g/>
@	@	kIx~	@
<g/>
Google	Google	k1gInSc1	Google
Presents	Presents	k1gInSc1	Presents
<g/>
:	:	kIx,	:
Google	Google	k1gInSc1	Google
Goes	Goesa	k1gFnPc2	Goesa
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
důvod	důvod	k1gInSc4	důvod
proč	proč	k6eAd1	proč
má	mít	k5eAaImIp3nS	mít
tetování	tetování	k1gNnSc4	tetování
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
normální	normální	k2eAgMnSc1d1	normální
<g/>
.	.	kIx.	.
</s>
<s>
Ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
Iggy	Igga	k1gMnSc2	Igga
Pop	pop	k1gInSc1	pop
a	a	k8xC	a
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
tetování	tetování	k1gNnSc2	tetování
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
−	−	k?	−
The	The	k1gMnSc4	The
Fame	Fam	k1gMnSc4	Fam
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
−	−	k?	−
The	The	k1gMnSc1	The
Fame	Fam	k1gFnSc2	Fam
Monster	monstrum	k1gNnPc2	monstrum
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
−	−	k?	−
Born	Born	k1gInSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
−	−	k?	−
ARTPOP	ARTPOP	kA	ARTPOP
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
−	−	k?	−
Cheek	Cheek	k6eAd1	Cheek
To	to	k9	to
Cheek	Cheek	k6eAd1	Cheek
(	(	kIx(	(
<g/>
s	s	k7c7	s
Tony	Tony	k1gFnSc7	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
−	−	k?	−
Joanne	Joann	k1gInSc5	Joann
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
A	A	kA	A
Star	Star	kA	Star
Is	Is	k1gMnSc1	Is
Born	Born	k1gMnSc1	Born
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc4	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reklama	reklama	k1gFnSc1	reklama
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
==	==	k?	==
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
The	The	k1gFnSc2	The
Monster	monstrum	k1gNnPc2	monstrum
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
celkově	celkově	k6eAd1	celkově
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
201	[number]	k4	201
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gInSc4	její
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
udělala	udělat	k5eAaPmAgFnS	udělat
s	s	k7c7	s
koncertním	koncertní	k2eAgNnSc7d1	koncertní
turné	turné	k1gNnSc7	turné
artRAVE	artRAVE	k?	artRAVE
The	The	k1gFnSc2	The
ARTPOP	ARTPOP	kA	ARTPOP
Ball	Ball	k1gInSc4	Ball
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
také	také	k6eAd1	také
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Fame	Fam	k1gFnSc2	Fam
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
Monster	monstrum	k1gNnPc2	monstrum
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Born	Borna	k1gFnPc2	Borna
This	This	k1gInSc1	This
Way	Way	k1gMnSc1	Way
Ball	Ball	k1gMnSc1	Ball
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
artRAVE	artRAVE	k?	artRAVE
The	The	k1gMnSc1	The
ARTPOP	ARTPOP	kA	ARTPOP
Ball	Ball	k1gMnSc1	Ball
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Cheek	Cheek	k6eAd1	Cheek
To	ten	k3xDgNnSc4	ten
Cheek	Cheek	k1gMnSc1	Cheek
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
s	s	k7c7	s
Tonym	Tony	k1gMnSc7	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Joanne	Joann	k1gInSc5	Joann
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
Enigma	enigma	k1gFnSc1	enigma
a	a	k8xC	a
JAZZ	jazz	k1gInSc1	jazz
a	a	k8xC	a
PIANO	piano	k1gNnSc1	piano
Show	show	k1gFnSc2	show
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c7	za
svojí	svůj	k3xOyFgFnSc7	svůj
kariéru	kariéra	k1gFnSc4	kariéra
získala	získat	k5eAaPmAgFnS	získat
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
Billboard	billboard	k1gInSc1	billboard
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Stoupající	stoupající	k2eAgFnSc1d1	stoupající
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
speciální	speciální	k2eAgInPc1d1	speciální
efekty	efekt	k1gInPc1	efekt
a	a	k8xC	a
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
umělecká	umělecký	k2eAgFnSc1d1	umělecká
režie	režie	k1gFnSc1	režie
za	za	k7c4	za
"	"	kIx"	"
<g/>
Paparazzi	Paparazze	k1gFnSc4	Paparazze
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MuchMusic	MuchMusic	k1gMnSc1	MuchMusic
Video	video	k1gNnSc1	video
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
video	video	k1gNnSc1	video
roku	rok	k1gInSc2	rok
za	za	k7c4	za
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc4	poker
Face	Fac	k1gFnSc2	Fac
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Teen	Teen	k1gInSc1	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Music	Music	k1gMnSc1	Music
Hook	Hook	k1gMnSc1	Hook
Up	Up	k1gMnSc1	Up
za	za	k7c4	za
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
The	The	k?	The
Record	Record	k1gInSc1	Record
of	of	k?	of
the	the	k?	the
Year	Year	k1gMnSc1	Year
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nahrávka	nahrávka	k1gFnSc1	nahrávka
roku	rok	k1gInSc2	rok
za	za	k7c4	za
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc4	poker
Face	Fac	k1gFnSc2	Fac
<g/>
"	"	kIx"	"
<g/>
2010	[number]	k4	2010
<g/>
American	American	k1gMnSc1	American
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
(	(	kIx(	(
<g/>
pop	pop	k1gInSc1	pop
<g/>
/	/	kIx~	/
<g/>
rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BET	BET	kA	BET
Awards	Awards	k1gInSc1	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Video	video	k1gNnSc1	video
roku	rok	k1gInSc2	rok
za	za	k7c4	za
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gInSc5	Telephon
<g/>
"	"	kIx"	"
s	s	k7c7	s
Beyoncé	Beyoncé	k1gNnSc7	Beyoncé
</s>
</p>
<p>
<s>
BRIT	Brit	k1gMnSc1	Brit
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
album	album	k1gNnSc1	album
za	za	k7c4	za
The	The	k1gFnSc4	The
Fame	Fam	k1gInSc2	Fam
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
GLAAD	GLAAD	kA	GLAAD
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hudební	hudební	k2eAgMnSc1d1	hudební
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
Grammy	Gramma	k1gFnPc1	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
electro	electro	k1gNnSc1	electro
<g/>
/	/	kIx~	/
<g/>
taneční	taneční	k2eAgNnSc1d1	taneční
album	album	k1gNnSc1	album
za	za	k7c4	za
The	The	k1gFnSc4	The
Fame	Fame	k1gFnPc2	Fame
a	a	k8xC	a
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
taneční	taneční	k2eAgFnSc1d1	taneční
nahrávka	nahrávka	k1gFnSc1	nahrávka
za	za	k7c4	za
"	"	kIx"	"
<g/>
Poker	poker	k1gInSc4	poker
Face	Fac	k1gFnSc2	Fac
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
písnička	písnička	k1gFnSc1	písnička
za	za	k7c4	za
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc4	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Video	video	k1gNnSc1	video
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
ženské	ženský	k2eAgNnSc1d1	ženské
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
popové	popový	k2eAgNnSc1d1	popové
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
taneční	taneční	k2eAgNnSc1d1	taneční
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
choreografie	choreografie	k1gFnSc1	choreografie
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
za	za	k7c4	za
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc4	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kolaborace	kolaborace	k1gFnSc1	kolaborace
za	za	k7c4	za
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gInSc5	Telephon
<g/>
"	"	kIx"	"
s	s	k7c7	s
Beyoncé	Beyoncé	k1gNnSc7	Beyoncé
</s>
</p>
<p>
<s>
People	People	k6eAd1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choika	k1gFnSc6	Choika
Awards	Awards	k1gInSc1	Awards
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
popový	popový	k2eAgMnSc1d1	popový
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
Teen	Teen	k1gInSc1	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
ženská	ženský	k2eAgFnSc1d1	ženská
letní	letní	k2eAgFnSc1d1	letní
hudební	hudební	k2eAgFnSc1d1	hudební
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
2011	[number]	k4	2011
<g/>
Billboard	billboard	k1gInSc4	billboard
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
pop	pop	k1gMnSc1	pop
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
taneční	taneční	k2eAgMnSc1d1	taneční
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
electro	electro	k1gNnSc1	electro
<g/>
/	/	kIx~	/
<g/>
taneční	taneční	k2eAgNnSc1d1	taneční
album	album	k1gNnSc1	album
za	za	k7c4	za
The	The	k1gFnSc4	The
Fame	Fam	k1gFnSc2	Fam
</s>
</p>
<p>
<s>
Grammy	Gramma	k1gFnPc1	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
popové	popový	k2eAgNnSc1d1	popové
album	album	k1gNnSc1	album
za	za	k7c4	za
The	The	k1gFnSc4	The
Fame	Fam	k1gFnSc2	Fam
Monster	monstrum	k1gNnPc2	monstrum
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
ženské	ženský	k2eAgNnSc4d1	ženské
popové	popový	k2eAgNnSc4d1	popové
vystoupení	vystoupení	k1gNnSc4	vystoupení
za	za	k7c4	za
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc4	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
krátká	krátký	k2eAgFnSc1d1	krátká
forma	forma	k1gFnSc1	forma
videoklipu	videoklip	k1gInSc2	videoklip
za	za	k7c4	za
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc4	Bad
Romance	romance	k1gFnSc2	romance
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
fanouškové	fanoušek	k1gMnPc1	fanoušek
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
písnička	písnička	k1gFnSc1	písnička
za	za	k7c4	za
"	"	kIx"	"
<g/>
Born	Born	k1gNnSc4	Born
this	this	k6eAd1	this
way	way	k?	way
<g/>
"	"	kIx"	"
a	a	k8xC	a
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
videoklip	videoklip	k1gInSc4	videoklip
za	za	k7c4	za
"	"	kIx"	"
<g/>
Born	Born	k1gNnSc4	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ženské	ženský	k2eAgNnSc4d1	ženské
video	video	k1gNnSc4	video
a	a	k8xC	a
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
videoklip	videoklip	k1gInSc4	videoklip
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
za	za	k7c4	za
"	"	kIx"	"
<g/>
Born	Born	k1gInSc4	Born
This	Thisa	k1gFnPc2	Thisa
way	way	k?	way
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
MuchMusic	MuchMusic	k1gMnSc1	MuchMusic
Video	video	k1gNnSc1	video
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
video	video	k1gNnSc1	video
roku	rok	k1gInSc2	rok
za	za	k7c4	za
"	"	kIx"	"
<g/>
Judas	Judas	k1gInSc4	Judas
<g/>
"	"	kIx"	"
a	a	k8xC	a
Nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
umělec	umělec	k1gMnSc1	umělec
za	za	k7c7	za
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnSc1	Way
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
The	The	k?	The
Record	Record	k1gInSc1	Record
of	of	k?	of
the	the	k?	the
Year	Year	k1gMnSc1	Year
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nahrávka	nahrávka	k1gFnSc1	nahrávka
roku	rok	k1gInSc2	rok
za	za	k7c4	za
"	"	kIx"	"
<g/>
Born	Born	k1gNnSc4	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Trevor	Trevor	k1gInSc1	Trevor
Project	Project	k2eAgInSc4d1	Project
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hrdina	Hrdina	k1gMnSc1	Hrdina
</s>
</p>
<p>
<s>
VH1	VH1	k4	VH1
"	"	kIx"	"
<g/>
Do	do	k7c2	do
Something	Something	k1gInSc1	Something
<g/>
"	"	kIx"	"
Award	Award	k1gInSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Do	do	k7c2	do
Something	Something	k1gInSc1	Something
Facebook	Facebook	k1gInSc1	Facebook
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
Billboard	billboard	k1gInSc4	billboard
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejvlivnější	vlivný	k2eAgMnSc1d3	nejvlivnější
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
Nejlépe	dobře	k6eAd3	dobře
oblékaná	oblékaný	k2eAgFnSc1d1	oblékaná
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
taneční	taneční	k2eAgMnSc1d1	taneční
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
electro	electro	k1gNnSc1	electro
<g/>
/	/	kIx~	/
<g/>
taneční	taneční	k2eAgNnSc1d1	taneční
album	album	k1gNnSc1	album
za	za	k7c4	za
Born	Born	k1gInSc4	Born
This	This	k1gInSc4	This
Way	Way	k1gFnSc2	Way
</s>
</p>
<p>
<s>
GLAAD	GLAAD	kA	GLAAD
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hudební	hudební	k2eAgMnSc1d1	hudební
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
People	People	k6eAd1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choic	k1gMnPc4	Choic
Awards	Awards	k1gInSc1	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
za	za	k7c4	za
Born	Born	k1gNnSc4	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnPc2	Way
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
Glamour	Glamoura	k1gFnPc2	Glamoura
Women	Womna	k1gFnPc2	Womna
of	of	k?	of
the	the	k?	the
Year	Year	k1gMnSc1	Year
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Žena	žena	k1gFnSc1	žena
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
Grammy	Grammo	k1gNnPc7	Grammo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
tradiční	tradiční	k2eAgNnSc1d1	tradiční
popové	popový	k2eAgNnSc1d1	popové
album	album	k1gNnSc1	album
za	za	k7c4	za
Cheek	Cheek	k1gInSc4	Cheek
To	to	k9	to
Cheek	Cheek	k6eAd1	Cheek
(	(	kIx(	(
<g/>
s	s	k7c7	s
Tonym	Tony	k1gMnSc7	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Contemporary	Contemporar	k1gInPc1	Contemporar
Icon	Icona	k1gFnPc2	Icona
Award	Awarda	k1gFnPc2	Awarda
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
ikonický	ikonický	k2eAgInSc4d1	ikonický
status	status	k1gInSc4	status
v	v	k7c6	v
popové	popový	k2eAgFnSc6d1	popová
kultuře	kultura	k1gFnSc6	kultura
(	(	kIx(	(
<g/>
Songwriters	Songwriters	k1gInSc1	Songwriters
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tony	Tony	k1gMnSc1	Tony
Bennettem	Bennett	k1gMnSc7	Bennett
za	za	k7c4	za
jejich	jejich	k3xOp3gNnPc4	jejich
vystoupení	vystoupení	k1gNnPc4	vystoupení
Tony	Tony	k1gMnSc1	Tony
Bennett	Bennett	k1gMnSc1	Bennett
and	and	k?	and
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
:	:	kIx,	:
Cheek	Cheek	k6eAd1	Cheek
To	ten	k3xDgNnSc4	ten
Cheek	Cheek	k1gMnSc1	Cheek
LIVE	LIVE	kA	LIVE
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Billboard	billboard	k1gInSc1	billboard
zvolil	zvolit	k5eAaPmAgInS	zvolit
Lady	lady	k1gFnSc4	lady
Gaga	Gagum	k1gNnSc2	Gagum
jako	jako	k8xS	jako
Woman	Womana	k1gFnPc2	Womana
of	of	k?	of
the	the	k?	the
year	year	k1gInSc1	year
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
–	–	k?	–
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
minisérii	minisérie	k1gFnSc6	minisérie
či	či	k8xC	či
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
–	–	k?	–
za	za	k7c4	za
roli	role	k1gFnSc4	role
hraběnky	hraběnka	k1gFnSc2	hraběnka
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
American	American	k1gInSc1	American
Horror	horror	k1gInSc1	horror
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Hotel	hotel	k1gInSc1	hotel
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Til	til	k1gInSc1	til
It	It	k1gFnSc2	It
Happens	Happensa	k1gFnPc2	Happensa
to	ten	k3xDgNnSc1	ten
You	You	k1gFnSc1	You
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsala	napsat	k5eAaBmAgFnS	napsat
Diane	Dian	k1gInSc5	Dian
Warren	Warrno	k1gNnPc2	Warrno
a	a	k8xC	a
Lady	lady	k1gFnPc2	lady
Gaga	Gag	k1gInSc2	Gag
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
Grammy	Gramm	k1gMnPc4	Gramm
a	a	k8xC	a
Emmy	Emma	k1gFnPc4	Emma
</s>
</p>
<p>
<s>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Dalai	Dalai	k1gNnSc7	Dalai
Lamou	lama	k1gFnSc7	lama
v	v	k7c6	v
IndianapolisMTV	IndianapolisMTV	k1gFnSc6	IndianapolisMTV
Europe	Europ	k1gInSc5	Europ
Awards	Awards	k1gInSc1	Awards
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
2019	[number]	k4	2019
<g/>
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Glóbus	glóbus	k1gInSc4	glóbus
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
filmová	filmový	k2eAgFnSc1d1	filmová
píseň	píseň	k1gFnSc1	píseň
Shalow	Shalow	k1gFnSc1	Shalow
z	z	k7c2	z
filmu	film	k1gInSc2	film
A	a	k8xC	a
star	star	k1gFnSc2	star
is	is	k?	is
born	born	k1gMnSc1	born
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Critic	Critice	k1gFnPc2	Critice
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choice	k1gInPc4	Choice
Awards	Awards	k1gInSc1	Awards
-	-	kIx~	-
Best	Best	k2eAgInSc1d1	Best
Song	song	k1gInSc1	song
in	in	k?	in
a	a	k8xC	a
Motion	Motion	k1gInSc1	Motion
PictureBAFTA	PictureBAFTA	k1gFnSc2	PictureBAFTA
Awards	Awardsa	k1gFnPc2	Awardsa
-	-	kIx~	-
Hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
film	film	k1gInSc4	film
"	"	kIx"	"
<g/>
A	a	k9	a
star	star	k1gFnSc1	star
is	is	k?	is
born	born	k1gInSc1	born
<g/>
"	"	kIx"	"
<g/>
3	[number]	k4	3
Grammy	Gramma	k1gFnSc2	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Best	Best	k2eAgInSc4d1	Best
Song	song	k1gInSc4	song
Written	Written	k2eAgInSc4d1	Written
for	forum	k1gNnPc2	forum
Visual	Visual	k1gMnSc1	Visual
Media	medium	k1gNnSc2	medium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Shallow	Shallow	k1gFnSc1	Shallow
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Best	Best	k1gMnSc1	Best
pop	pop	k1gMnSc1	pop
<g />
.	.	kIx.	.
</s>
<s>
duo	duo	k1gNnSc1	duo
<g/>
/	/	kIx~	/
<g/>
group	group	k1gInSc1	group
performance	performance	k1gFnSc2	performance
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Shallow	Shallow	k1gFnSc1	Shallow
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Best	Best	k1gMnSc1	Best
pop	pop	k1gMnSc1	pop
solo	solo	k1gMnSc1	solo
performance	performance	k1gFnSc2	performance
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Joanne	Joann	k1gMnSc5	Joann
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
Píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Shallow	Shallow	k1gFnSc1	Shallow
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
napsanou	napsaný	k2eAgFnSc4d1	napsaná
pro	pro	k7c4	pro
film	film	k1gInSc4	film
"	"	kIx"	"
<g/>
A	a	k9	a
star	star	k1gFnSc1	star
is	is	k?	is
born	born	k1gInSc1	born
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lady	Lada	k1gFnSc2	Lada
Gaga	Gagum	k1gNnSc2	Gagum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
