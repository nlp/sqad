<s>
Secese	secese	k1gFnSc1	secese
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
art	art	k?	art
nouveau	nouveau	k5eAaPmIp1nS	nouveau
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
:	:	kIx,	:
<g/>
r	r	kA	r
nuvo	nuvo	k1gNnSc4	nuvo
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Jugendstil	Jugendstil	k1gFnSc1	Jugendstil
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
uměleckých	umělecký	k2eAgInPc2d1	umělecký
sloh	sloh	k1gInSc4	sloh
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
období	období	k1gNnSc2	období
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xC	jako
fin	fin	k?	fin
de	de	k?	de
siè	siè	k?	siè
<g/>
)	)	kIx)	)
a	a	k8xC	a
posledním	poslední	k2eAgInSc7d1	poslední
stylem	styl	k1gInSc7	styl
souhrnného	souhrnný	k2eAgNnSc2d1	souhrnné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
též	též	k9	též
Gesamtkunstwerk	Gesamtkunstwerk	k1gInSc1	Gesamtkunstwerk
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vtisknout	vtisknout	k5eAaPmF	vtisknout
svůj	svůj	k3xOyFgInSc4	svůj
umělecký	umělecký	k2eAgInSc4d1	umělecký
řád	řád	k1gInSc4	řád
všem	všecek	k3xTgInPc3	všecek
projevům	projev	k1gInPc3	projev
a	a	k8xC	a
věcem	věc	k1gFnPc3	věc
moderního	moderní	k2eAgInSc2d1	moderní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
secese	secese	k1gFnSc2	secese
neleží	ležet	k5eNaImIp3nS	ležet
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
v	v	k7c6	v
malbě	malba	k1gFnSc6	malba
a	a	k8xC	a
sochařství	sochařství	k1gNnSc6	sochařství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dekoraci	dekorace	k1gFnSc6	dekorace
a	a	k8xC	a
užitém	užitý	k2eAgNnSc6d1	užité
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Secese	secese	k1gFnSc1	secese
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
květinových	květinový	k2eAgFnPc2d1	květinová
koncepcí	koncepce	k1gFnPc2	koncepce
britského	britský	k2eAgMnSc2d1	britský
textilního	textilní	k2eAgMnSc2d1	textilní
designéra	designér	k1gMnSc2	designér
Williama	William	k1gMnSc2	William
Morrise	Morrise	k1gFnSc2	Morrise
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
Arts	Artsa	k1gFnPc2	Artsa
and	and	k?	and
Crafts	Craftsa	k1gFnPc2	Craftsa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
vracelo	vracet	k5eAaImAgNnS	vracet
k	k	k7c3	k
lidovým	lidový	k2eAgFnPc3d1	lidová
tradicím	tradice	k1gFnPc3	tradice
a	a	k8xC	a
romantismu	romantismus	k1gInSc2	romantismus
řemeslnému	řemeslný	k2eAgInSc3d1	řemeslný
fortelu	fortelu	k?	fortelu
středověkých	středověký	k2eAgMnPc2d1	středověký
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
stavělo	stavět	k5eAaImAgNnS	stavět
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
proti	proti	k7c3	proti
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
zpracování	zpracování	k1gNnSc2	zpracování
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
výrobků	výrobek	k1gInPc2	výrobek
hnaných	hnaný	k2eAgInPc2d1	hnaný
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
také	také	k9	také
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
anglickými	anglický	k2eAgInPc7d1	anglický
malíři	malíř	k1gMnSc6	malíř
označovanými	označovaný	k2eAgMnPc7d1	označovaný
jako	jako	k9	jako
Prerafaelité	prerafaelita	k1gMnPc1	prerafaelita
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
secese	secese	k1gFnSc1	secese
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1890	[number]	k4	1890
a	a	k8xC	a
1910	[number]	k4	1910
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Art	Art	k1gFnSc1	Art
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Jugendstil	Jugendstil	k1gFnSc2	Jugendstil
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
secese	secese	k1gFnSc1	secese
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Sezession	Sezession	k1gInSc1	Sezession
<g/>
,	,	kIx,	,
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
secessio	secessio	k6eAd1	secessio
odštěpení	odštěpení	k1gNnSc4	odštěpení
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
uměleckých	umělecký	k2eAgInPc2d1	umělecký
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
Wiener	Wiener	k1gInSc1	Wiener
Sezession	Sezession	k1gInSc1	Sezession
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
oddělily	oddělit	k5eAaPmAgFnP	oddělit
od	od	k7c2	od
konservativních	konservativní	k2eAgFnPc2d1	konservativní
akademií	akademie	k1gFnPc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgInPc4d1	hlavní
znaky	znak	k1gInPc4	znak
secesního	secesní	k2eAgInSc2d1	secesní
slohu	sloh	k1gInSc2	sloh
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
ornamentálnost	ornamentálnost	k1gFnSc1	ornamentálnost
<g/>
,	,	kIx,	,
plošnost	plošnost	k1gFnSc1	plošnost
a	a	k8xC	a
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
neobyčejných	obyčejný	k2eNgFnPc6d1	neobyčejná
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
estetickém	estetický	k2eAgNnSc6d1	estetické
využití	využití	k1gNnSc6	využití
rozličných	rozličný	k2eAgInPc2d1	rozličný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vláčný	vláčný	k2eAgInSc1d1	vláčný
secesní	secesní	k2eAgInSc1d1	secesní
ornament	ornament	k1gInSc1	ornament
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
kvalitu	kvalita	k1gFnSc4	kvalita
secesního	secesní	k2eAgNnSc2d1	secesní
cítění	cítění	k1gNnSc2	cítění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
citovým	citový	k2eAgInPc3d1	citový
výkyvům	výkyv	k1gInPc3	výkyv
i	i	k9	i
silnému	silný	k2eAgNnSc3d1	silné
vypětí	vypětí	k1gNnSc3	vypětí
vůle	vůle	k1gFnSc2	vůle
a	a	k8xC	a
tíhne	tíhnout	k5eAaImIp3nS	tíhnout
k	k	k7c3	k
vyrovnané	vyrovnaný	k2eAgFnSc3d1	vyrovnaná
náladovosti	náladovost	k1gFnSc3	náladovost
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgFnSc1d1	secesní
linie	linie	k1gFnSc1	linie
je	být	k5eAaImIp3nS	být
plynulá	plynulý	k2eAgFnSc1d1	plynulá
vlnící	vlnící	k2eAgFnSc1d1	vlnící
se	se	k3xPyFc4	se
křivka	křivka	k1gFnSc1	křivka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
v	v	k7c6	v
divákovi	divák	k1gMnSc6	divák
dojem	dojem	k1gInSc4	dojem
nenásilného	násilný	k2eNgInSc2d1	nenásilný
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jednotvárnost	jednotvárnost	k1gFnSc1	jednotvárnost
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
barevností	barevnost	k1gFnSc7	barevnost
<g/>
.	.	kIx.	.
</s>
<s>
Secese	secese	k1gFnSc1	secese
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
barevné	barevný	k2eAgInPc4d1	barevný
odstíny	odstín	k1gInPc4	odstín
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
je	on	k3xPp3gInPc4	on
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
harmonie	harmonie	k1gFnSc2	harmonie
a	a	k8xC	a
kontrastu	kontrast	k1gInSc2	kontrast
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
stylizace	stylizace	k1gFnSc1	stylizace
<g/>
,	,	kIx,	,
secese	secese	k1gFnSc1	secese
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
překonat	překonat	k5eAaPmF	překonat
gravidní	gravidní	k2eAgFnSc4d1	gravidní
tematiku	tematika	k1gFnSc4	tematika
historických	historický	k2eAgInPc2d1	historický
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
přírodním	přírodní	k2eAgInPc3d1	přírodní
tvarům	tvar	k1gInPc3	tvar
(	(	kIx(	(
<g/>
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc4d1	lidské
a	a	k8xC	a
zvířecí	zvířecí	k2eAgNnSc4d1	zvířecí
tělo	tělo	k1gNnSc4	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Secese	secese	k1gFnSc1	secese
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
v	v	k7c6	v
užitém	užitý	k2eAgNnSc6d1	užité
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
bytové	bytový	k2eAgNnSc1d1	bytové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
vitráže	vitráž	k1gFnPc1	vitráž
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
nositeli	nositel	k1gMnPc7	nositel
secesního	secesní	k2eAgInSc2d1	secesní
slohu	sloh	k1gInSc2	sloh
staly	stát	k5eAaPmAgInP	stát
proudy	proud	k1gInPc1	proud
označované	označovaný	k2eAgInPc1d1	označovaný
jako	jako	k8xS	jako
symbolismus	symbolismus	k1gInSc1	symbolismus
a	a	k8xC	a
dekadence	dekadence	k1gFnSc1	dekadence
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
secese	secese	k1gFnSc2	secese
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
tendence	tendence	k1gFnSc2	tendence
k	k	k7c3	k
ornamentálnosti	ornamentálnost	k1gFnSc3	ornamentálnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
jazykové	jazykový	k2eAgFnSc6d1	jazyková
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
rytmizace	rytmizace	k1gFnSc1	rytmizace
věty	věta	k1gFnSc2	věta
a	a	k8xC	a
verše	verš	k1gInPc1	verš
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
hláskových	hláskový	k2eAgFnPc2d1	hlásková
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
Otokar	Otokar	k1gMnSc1	Otokar
Březina	Březina	k1gMnSc1	Březina
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Karásek	Karásek	k1gMnSc1	Karásek
ze	z	k7c2	z
Lvovic	Lvovice	k1gFnPc2	Lvovice
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnPc7	představitel
architektury	architektura	k1gFnSc2	architektura
jsou	být	k5eAaImIp3nP	být
Belgičan	Belgičan	k1gMnSc1	Belgičan
Victor	Victor	k1gMnSc1	Victor
Horta	Hort	k1gMnSc2	Hort
(	(	kIx(	(
<g/>
Dům	dům	k1gInSc1	dům
Tasselových	Tasselová	k1gFnPc2	Tasselová
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francouz	Francouz	k1gMnSc1	Francouz
Hector	Hector	k1gMnSc1	Hector
Guimard	Guimard	k1gMnSc1	Guimard
(	(	kIx(	(
<g/>
vstupy	vstup	k1gInPc7	vstup
do	do	k7c2	do
pařížského	pařížský	k2eAgNnSc2d1	pařížské
metra	metro	k1gNnSc2	metro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
Antonín	Antonín	k1gMnSc1	Antonín
Balšánek	balšánek	k1gMnSc1	balšánek
a	a	k8xC	a
Osvald	Osvald	k1gMnSc1	Osvald
Polívka	Polívka	k1gMnSc1	Polívka
(	(	kIx(	(
<g/>
Obecní	obecní	k2eAgInSc1d1	obecní
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španěl	Španěl	k1gMnSc1	Španěl
Antoni	Anton	k1gMnPc1	Anton
Gaudí	Gaudí	k1gNnSc1	Gaudí
(	(	kIx(	(
<g/>
chrám	chrám	k1gInSc1	chrám
Sagrada	Sagrada	k1gFnSc1	Sagrada
Família	Família	k1gFnSc1	Família
<g/>
,	,	kIx,	,
Güellův	Güellův	k2eAgInSc1d1	Güellův
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Batllóův	Batllóův	k2eAgInSc1d1	Batllóův
dům	dům	k1gInSc1	dům
atd.	atd.	kA	atd.
–	–	k?	–
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakušan	Rakušan	k1gMnSc1	Rakušan
<g/>
,	,	kIx,	,
opavský	opavský	k2eAgMnSc1d1	opavský
rodák	rodák	k1gMnSc1	rodák
Joseph	Joseph	k1gMnSc1	Joseph
Maria	Mario	k1gMnSc2	Mario
Olbrich	Olbrich	k1gMnSc1	Olbrich
(	(	kIx(	(
<g/>
pavilon	pavilon	k1gInSc1	pavilon
Secese	secese	k1gFnSc2	secese
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
prosluli	proslout	k5eAaPmAgMnP	proslout
Čech	Čech	k1gMnSc1	Čech
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
(	(	kIx(	(
<g/>
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc4	cyklus
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
návrhy	návrh	k1gInPc1	návrh
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
nábytku	nábytek	k1gInSc2	nábytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakušan	Rakušan	k1gMnSc1	Rakušan
Gustav	Gustav	k1gMnSc1	Gustav
Klimt	Klimt	k1gMnSc1	Klimt
(	(	kIx(	(
<g/>
dekorativní	dekorativní	k2eAgFnPc1d1	dekorativní
malby	malba	k1gFnPc1	malba
<g/>
,	,	kIx,	,
plakáty	plakát	k1gInPc1	plakát
<g/>
)	)	kIx)	)
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
sochař	sochař	k1gMnSc1	sochař
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnSc3	představitel
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
jsou	být	k5eAaImIp3nP	být
Francouz	Francouz	k1gMnSc1	Francouz
Émile	Émile	k1gFnSc2	Émile
Gallé	Gallý	k2eAgFnSc2d1	Gallý
(	(	kIx(	(
<g/>
skleněné	skleněný	k2eAgFnSc2d1	skleněná
vázy	váza	k1gFnSc2	váza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Skot	Skot	k1gMnSc1	Skot
Charles	Charles	k1gMnSc1	Charles
Rennie	Rennie	k1gFnSc2	Rennie
Mackintosh	Mackintosh	k1gMnSc1	Mackintosh
(	(	kIx(	(
<g/>
bytové	bytový	k2eAgNnSc1d1	bytové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Secese	secese	k1gFnSc1	secese
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
životní	životní	k2eAgInSc4d1	životní
způsob	způsob	k1gInSc4	způsob
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
neopakovatelně	opakovatelně	k6eNd1	opakovatelně
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
i	i	k9	i
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
nebo	nebo	k8xC	nebo
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
nespočetně	spočetně	k6eNd1	spočetně
děl	dít	k5eAaImAgInS	dít
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
předmětů	předmět	k1gInPc2	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgFnSc1d1	secesní
architektura	architektura	k1gFnSc1	architektura
Gombrich	Gombrich	k1gMnSc1	Gombrich
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Hector	Hector	k1gMnSc1	Hector
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
416	[number]	k4	416
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
437	[number]	k4	437
<g/>
–	–	k?	–
<g/>
507	[number]	k4	507
<g/>
.	.	kIx.	.
</s>
<s>
Šabouk	Šabouk	k1gMnSc1	Šabouk
<g/>
,	,	kIx,	,
Sáva	Sáva	k1gMnSc1	Sáva
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
světového	světový	k2eAgNnSc2d1	světové
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
315	[number]	k4	315
<g/>
.	.	kIx.	.
</s>
<s>
Wittlich	Wittlich	k1gMnSc1	Wittlich
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Umění	umění	k1gNnSc1	umění
a	a	k8xC	a
život	život	k1gInSc1	život
–	–	k?	–
doba	doba	k1gFnSc1	doba
secese	secese	k1gFnSc1	secese
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Artia	Artia	k1gFnSc1	Artia
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
62	[number]	k4	62
<g/>
–	–	k?	–
<g/>
126	[number]	k4	126
<g/>
.	.	kIx.	.
</s>
<s>
Wittlich	Wittlich	k1gMnSc1	Wittlich
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
secese	secese	k1gFnSc1	secese
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Wittlich	Wittlich	k1gMnSc1	Wittlich
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Sochařství	sochařství	k1gNnSc1	sochařství
české	český	k2eAgFnSc2d1	Česká
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Pijoan	Pijoan	k1gInSc1	Pijoan
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
55	[number]	k4	55
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
Wilfried	Wilfried	k1gMnSc1	Wilfried
<g/>
:	:	kIx,	:
Evropská	evropský	k2eAgFnSc1d1	Evropská
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ikar	Ikar	k1gInSc1	Ikar
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7202	[number]	k4	7202
<g/>
-	-	kIx~	-
<g/>
388	[number]	k4	388
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
274	[number]	k4	274
<g/>
.	.	kIx.	.
</s>
<s>
Dvořáček	Dvořáček	k1gMnSc1	Dvořáček
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Architektura	architektura	k1gFnSc1	architektura
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Secese	secese	k1gFnSc1	secese
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
287	[number]	k4	287
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Sagnerová	Sagnerová	k1gFnSc1	Sagnerová
<g/>
,	,	kIx,	,
Karin	Karina	k1gFnPc2	Karina
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
poznáme	poznat	k5eAaPmIp1nP	poznat
<g/>
?	?	kIx.	?
</s>
<s>
Umění	umění	k1gNnSc1	umění
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1773	[number]	k4	1773
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Millerová	Millerová	k1gFnSc1	Millerová
<g/>
,	,	kIx,	,
Judith	Judith	k1gMnSc1	Judith
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
pro	pro	k7c4	pro
sběratele	sběratel	k1gMnPc4	sběratel
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Noxi	Noxi	k1gNnSc1	Noxi
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
89179	[number]	k4	89179
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
secese	secese	k1gFnSc2	secese
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Secese	secese	k1gFnSc1	secese
</s>
