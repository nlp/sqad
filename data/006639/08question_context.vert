<s>
Secese	secese	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
art	art	k?
nouveau	nouveau	k5eAaPmIp1nS
[	[	kIx(
<g/>
a	a	k8xC
<g/>
:	:	kIx,
<g/>
r	r	kA
nuvo	nuvo	k1gNnSc4
<g/>
:	:	kIx,
<g/>
]	]	kIx)
<g/>
,	,	kIx,
německy	německy	k6eAd1
Jugendstil	Jugendstil	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc1d1
uměleckých	umělecký	k2eAgInPc2d1
sloh	sloh	k1gInSc4
z	z	k7c2
přelomu	přelom	k1gInSc2
19	[number]	k4
<g/>
.	.	kIx.
a	a	k8xC
20	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
období	období	k1gNnSc2
označovaného	označovaný	k2eAgInSc2d1
jako	jako	k8xC
fin	fin	k?
de	de	k?
siè	siè	k?
<g/>
)	)	kIx)
a	a	k8xC
posledním	poslední	k2eAgInSc7d1
stylem	styl	k1gInSc7
souhrnného	souhrnný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
též	též	k9
Gesamtkunstwerk	Gesamtkunstwerk	k1gInSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vtisknout	vtisknout	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
umělecký	umělecký	k2eAgInSc4d1
řád	řád	k1gInSc4
všem	všecek	k3xTgInPc3
projevům	projev	k1gInPc3
a	a	k8xC
věcem	věc	k1gFnPc3
moderního	moderní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>