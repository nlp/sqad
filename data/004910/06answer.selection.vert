<s>
Katastrofa	katastrofa	k1gFnSc1	katastrofa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
následkem	následkem	k7c2	následkem
zatopení	zatopení	k1gNnSc2	zatopení
elektrárny	elektrárna	k1gFnSc2	elektrárna
ničivou	ničivý	k2eAgFnSc7d1	ničivá
vlnou	vlna	k1gFnSc7	vlna
tsunami	tsunami	k1gNnSc1	tsunami
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
mimořádně	mimořádně	k6eAd1	mimořádně
silným	silný	k2eAgNnSc7d1	silné
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tóhoku	Tóhok	k1gInSc2	Tóhok
<g/>
.	.	kIx.	.
</s>
