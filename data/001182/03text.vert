<s>
Varšava	Varšava	k1gFnSc1	Varšava
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Warszawa	Warszawa	k1gFnSc1	Warszawa
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
varˈ	varˈ	k?	varˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1596	[number]	k4	1596
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
1	[number]	k4	1
726	[number]	k4	726
581	[number]	k4	581
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
s	s	k7c7	s
okolní	okolní	k2eAgFnSc7d1	okolní
aglomerací	aglomerace	k1gFnSc7	aglomerace
3	[number]	k4	3
003	[number]	k4	003
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
Mazovsku	Mazovsko	k1gNnSc6	Mazovsko
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
Visly	Visla	k1gFnSc2	Visla
ve	v	k7c6	v
Varšavské	varšavský	k2eAgFnSc6d1	Varšavská
kotlině	kotlina	k1gFnSc6	kotlina
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
520	[number]	k4	520
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
250	[number]	k4	250
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
je	být	k5eAaImIp3nS	být
také	také	k9	také
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Mazovského	Mazovský	k2eAgNnSc2d1	Mazovský
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
zpracovatelský	zpracovatelský	k2eAgInSc4d1	zpracovatelský
<g/>
,	,	kIx,	,
ocelářský	ocelářský	k2eAgInSc4d1	ocelářský
<g/>
,	,	kIx,	,
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
a	a	k8xC	a
automobilový	automobilový	k2eAgInSc4d1	automobilový
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Uniwersytet	Uniwersytet	k1gInSc1	Uniwersytet
Warszawski	Warszawsk	k1gFnSc2	Warszawsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
kardinála	kardinál	k1gMnSc2	kardinál
Stefana	Stefan	k1gMnSc2	Stefan
Wyszyńského	Wyszyńský	k1gMnSc2	Wyszyńský
<g/>
,	,	kIx,	,
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Politechnika	Politechnika	k1gFnSc1	Politechnika
Warszawska	Warszawska	k1gFnSc1	Warszawska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Szkoła	Szkoł	k2eAgFnSc1d1	Szkoła
Główna	Główen	k2eAgFnSc1d1	Główen
Handlowa	Handlowa	k1gFnSc1	Handlowa
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
přes	přes	k7c4	přes
30	[number]	k4	30
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tu	tu	k6eAd1	tu
sídlo	sídlo	k1gNnSc4	sídlo
Národní	národní	k2eAgInSc1d1	národní
filharmonický	filharmonický	k2eAgInSc1d1	filharmonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
opevněné	opevněný	k2eAgNnSc1d1	opevněné
osídlení	osídlení	k1gNnSc1	osídlení
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Varšavy	Varšava	k1gFnSc2	Varšava
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
Bródno	Bródno	k1gNnSc4	Bródno
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
Jazdów	Jazdów	k1gFnSc2	Jazdów
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Płocku	Płock	k1gInSc2	Płock
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mazovský	Mazovský	k1gMnSc1	Mazovský
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1281	[number]	k4	1281
na	na	k7c6	na
Jazdów	Jazdów	k1gFnSc6	Jazdów
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založeno	založen	k2eAgNnSc4d1	založeno
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
malé	malý	k2eAgFnPc1d1	malá
rybářské	rybářský	k2eAgFnPc1d1	rybářská
vesničky	vesnička	k1gFnPc1	vesnička
Warzsowa	Warzsow	k1gInSc2	Warzsow
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
sídel	sídlo	k1gNnPc2	sídlo
Mazovského	Mazovský	k2eAgNnSc2d1	Mazovský
vojvodství	vojvodství	k1gNnSc2	vojvodství
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1413	[number]	k4	1413
pak	pak	k6eAd1	pak
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Mazovska	Mazovsko	k1gNnSc2	Mazovsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
místní	místní	k2eAgFnSc2d1	místní
vojvodské	vojvodský	k2eAgFnSc2d1	vojvodský
linie	linie	k1gFnSc2	linie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
bylo	být	k5eAaImAgNnS	být
vojvodství	vojvodství	k1gNnSc1	vojvodství
začleněno	začlenit	k5eAaPmNgNnS	začlenit
pod	pod	k7c4	pod
polskou	polský	k2eAgFnSc4d1	polská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1529	[number]	k4	1529
se	se	k3xPyFc4	se
Varšava	Varšava	k1gFnSc1	Varšava
stala	stát	k5eAaPmAgFnS	stát
poprvé	poprvé	k6eAd1	poprvé
sídlem	sídlo	k1gNnSc7	sídlo
polského	polský	k2eAgInSc2d1	polský
Sejmu	Sejm	k1gInSc2	Sejm
<g/>
,	,	kIx,	,
trvale	trvale	k6eAd1	trvale
zde	zde	k6eAd1	zde
Sejm	Sejm	k1gInSc1	Sejm
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
dala	dát	k5eAaPmAgFnS	dát
Varšava	Varšava	k1gFnSc1	Varšava
jméno	jméno	k1gNnSc4	jméno
Varšavské	varšavský	k2eAgFnSc6d1	Varšavská
konfederaci	konfederace	k1gFnSc6	konfederace
<g/>
,	,	kIx,	,
dohodě	dohoda	k1gFnSc6	dohoda
polské	polský	k2eAgFnSc2d1	polská
šlechty	šlechta	k1gFnSc2	šlechta
na	na	k7c4	na
toleranci	tolerance	k1gFnSc4	tolerance
různých	různý	k2eAgNnPc2d1	různé
náboženství	náboženství	k1gNnPc2	náboženství
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
výhodné	výhodný	k2eAgFnSc3d1	výhodná
centrální	centrální	k2eAgFnSc3d1	centrální
poloze	poloha	k1gFnSc3	poloha
mezi	mezi	k7c7	mezi
Vilniusem	Vilnius	k1gInSc7	Vilnius
a	a	k8xC	a
Krakovem	Krakov	k1gInSc7	Krakov
v	v	k7c6	v
polsko-litevském	polskoitevský	k2eAgNnSc6d1	polsko-litevské
společenství	společenství	k1gNnSc6	společenství
se	se	k3xPyFc4	se
Varšava	Varšava	k1gFnSc1	Varšava
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
tohoto	tento	k3xDgNnSc2	tento
společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
současně	současně	k6eAd1	současně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1596	[number]	k4	1596
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sem	sem	k6eAd1	sem
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vasa	Vasa	k1gMnSc1	Vasa
přesunul	přesunout	k5eAaPmAgMnS	přesunout
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
z	z	k7c2	z
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
byla	být	k5eAaImAgFnS	být
metropolí	metropol	k1gFnSc7	metropol
polsko-litevského	polskoitevský	k2eAgNnSc2d1	polsko-litevské
společenství	společenství	k1gNnSc2	společenství
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
soustátí	soustátí	k1gNnSc1	soustátí
napadeno	napaden	k2eAgNnSc1d1	napadeno
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Varšava	Varšava	k1gFnSc1	Varšava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Nové	Nové	k2eAgNnSc1d1	Nové
Východní	východní	k2eAgNnSc1d1	východní
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
město	město	k1gNnSc1	město
na	na	k7c6	na
Visle	Visla	k1gFnSc6	Visla
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
metropolí	metropol	k1gFnSc7	metropol
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgFnP	sejít
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
mocnosti	mocnost	k1gFnPc1	mocnost
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Prusko	Prusko	k1gNnSc1	Prusko
(	(	kIx(	(
<g/>
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
kongres	kongres	k1gInSc1	kongres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
Polsko	Polsko	k1gNnSc1	Polsko
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
imperiálního	imperiální	k2eAgNnSc2d1	imperiální
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
polskou	polský	k2eAgFnSc4d1	polská
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
ruské	ruský	k2eAgFnSc3d1	ruská
nadvládě	nadvláda	k1gFnSc3	nadvláda
v	v	k7c6	v
letech	let	k1gInPc6	let
1830	[number]	k4	1830
a	a	k8xC	a
1863	[number]	k4	1863
jen	jen	k6eAd1	jen
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
represe	represe	k1gFnSc2	represe
proti	proti	k7c3	proti
polskému	polský	k2eAgNnSc3d1	polské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovuzískání	znovuzískání	k1gNnSc6	znovuzískání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Polska	Polsko	k1gNnSc2	Polsko
po	po	k7c6	po
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Varšava	Varšava	k1gFnSc1	Varšava
stala	stát	k5eAaPmAgFnS	stát
znovu	znovu	k6eAd1	znovu
metropolí	metropol	k1gFnSc7	metropol
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
tvořícího	tvořící	k2eAgInSc2d1	tvořící
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
Polsko	Polsko	k1gNnSc1	Polsko
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
bolševickým	bolševický	k2eAgNnSc7d1	bolševické
Ruskem	Rusko	k1gNnSc7	Rusko
o	o	k7c4	o
historická	historický	k2eAgNnPc4d1	historické
území	území	k1gNnPc4	území
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varšavy	Varšava	k1gFnSc2	Varšava
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
polská	polský	k2eAgNnPc4d1	polské
vojska	vojsko	k1gNnPc4	vojsko
zcela	zcela	k6eAd1	zcela
zničila	zničit	k5eAaPmAgFnS	zničit
jednotky	jednotka	k1gFnPc4	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
útočící	útočící	k2eAgFnSc2d1	útočící
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
zmařila	zmařit	k5eAaPmAgFnS	zmařit
tak	tak	k9	tak
pokus	pokus	k1gInSc4	pokus
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
ruských	ruský	k2eAgMnPc2d1	ruský
bolševiků	bolševik	k1gMnPc2	bolševik
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
<g/>
,	,	kIx,	,
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
a	a	k8xC	a
maďarskými	maďarský	k2eAgMnPc7d1	maďarský
bolševiky	bolševik	k1gMnPc7	bolševik
a	a	k8xC	a
ovládnout	ovládnout	k5eAaPmF	ovládnout
tak	tak	k6eAd1	tak
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Německo	Německo	k1gNnSc1	Německo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
napadeno	napaden	k2eAgNnSc1d1	napadeno
Polsko	Polsko	k1gNnSc1	Polsko
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
Polska	Polsko	k1gNnSc2	Polsko
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
východ	východ	k1gInSc4	východ
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
Polska	Polsko	k1gNnSc2	Polsko
včetně	včetně	k7c2	včetně
Varšavy	Varšava	k1gFnSc2	Varšava
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
nacistické	nacistický	k2eAgFnSc2d1	nacistická
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
General-Gouvernment	General-Gouvernment	k1gInSc1	General-Gouvernment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
invazi	invaze	k1gFnSc6	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
Varšava	Varšava	k1gFnSc1	Varšava
bombardována	bombardovat	k5eAaImNgFnS	bombardovat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
<g/>
%	%	kIx~	%
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
významné	významný	k2eAgFnPc4d1	významná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
nacistických	nacistický	k2eAgInPc2d1	nacistický
plánů	plán	k1gInPc2	plán
město	město	k1gNnSc1	město
kompletně	kompletně	k6eAd1	kompletně
zlikvidováno	zlikvidovat	k5eAaPmNgNnS	zlikvidovat
a	a	k8xC	a
zachováno	zachovat	k5eAaPmNgNnS	zachovat
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
důležitý	důležitý	k2eAgInSc4d1	důležitý
přestupní	přestupní	k2eAgInSc4d1	přestupní
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
okupaci	okupace	k1gFnSc6	okupace
města	město	k1gNnSc2	město
nacisty	nacista	k1gMnPc7	nacista
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc4	všechen
vyšší	vysoký	k2eAgFnPc4d2	vyšší
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
instituce	instituce	k1gFnPc4	instituce
okamžitě	okamžitě	k6eAd1	okamžitě
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
a	a	k8xC	a
varšavská	varšavský	k2eAgFnSc1d1	Varšavská
židovská	židovská	k1gFnSc1	židovská
populace	populace	k1gFnSc2	populace
-	-	kIx~	-
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
nahnána	nahnat	k5eAaPmNgFnS	nahnat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
"	"	kIx"	"
<g/>
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
<g/>
"	"	kIx"	"
pokusili	pokusit	k5eAaPmAgMnP	pokusit
ghetto	ghetto	k1gNnSc4	ghetto
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
židovskému	židovský	k2eAgNnSc3d1	Židovské
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
těžkému	těžký	k2eAgNnSc3d1	těžké
ostřelování	ostřelování	k1gNnSc3	ostřelování
a	a	k8xC	a
přesile	přesila	k1gFnSc3	přesila
se	se	k3xPyFc4	se
ghetto	ghetto	k1gNnSc1	ghetto
udrželo	udržet	k5eAaPmAgNnS	udržet
bránit	bránit	k5eAaImF	bránit
skoro	skoro	k6eAd1	skoro
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
byli	být	k5eAaImAgMnP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
zmasakrováni	zmasakrovat	k5eAaPmNgMnP	zmasakrovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1944	[number]	k4	1944
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojska	vojsko	k1gNnSc2	vojsko
postupovala	postupovat	k5eAaImAgFnS	postupovat
přes	přes	k7c4	přes
polské	polský	k2eAgNnSc4d1	polské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
pak	pak	k6eAd1	pak
ustupovali	ustupovat	k5eAaImAgMnP	ustupovat
k	k	k7c3	k
Varšavě	Varšava	k1gFnSc3	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
si	se	k3xPyFc3	se
polská	polský	k2eAgFnSc1d1	polská
londýnská	londýnský	k2eAgFnSc1d1	londýnská
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalin	Stalin	k1gMnSc1	Stalin
není	být	k5eNaImIp3nS	být
nakloněn	nakloněn	k2eAgInSc1d1	nakloněn
myšlence	myšlenka	k1gFnSc3	myšlenka
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
chtěl	chtít	k5eAaImAgMnS	chtít
pouze	pouze	k6eAd1	pouze
hranice	hranice	k1gFnPc4	hranice
Polska	Polsko	k1gNnSc2	Polsko
posunout	posunout	k5eAaPmF	posunout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
rozkaz	rozkaz	k1gInSc4	rozkaz
ilegální	ilegální	k2eAgFnSc2d1	ilegální
polské	polský	k2eAgFnSc6d1	polská
Zemské	zemský	k2eAgFnSc3d1	zemská
armádě	armáda	k1gFnSc3	armáda
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Armia	Armia	k1gFnSc1	Armia
Krajowa	Krajowa	k1gFnSc1	Krajowa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
než	než	k8xS	než
tam	tam	k6eAd1	tam
dorazí	dorazit	k5eAaPmIp3nP	dorazit
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
k	k	k7c3	k
městu	město	k1gNnSc3	město
rychle	rychle	k6eAd1	rychle
blížila	blížit	k5eAaImAgFnS	blížit
<g/>
,	,	kIx,	,
Zemská	zemský	k2eAgFnSc1d1	zemská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
zahájila	zahájit	k5eAaPmAgFnS	zahájit
(	(	kIx(	(
<g/>
hrdinské	hrdinský	k2eAgNnSc4d1	hrdinské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velice	velice	k6eAd1	velice
ukvapené	ukvapený	k2eAgFnPc1d1	ukvapená
a	a	k8xC	a
bez	bez	k7c2	bez
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
)	)	kIx)	)
Varšavské	varšavský	k2eAgNnSc4d1	Varšavské
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
Stalinovu	Stalinův	k2eAgNnSc3d1	Stalinovo
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
k	k	k7c3	k
Polsku	Polsko	k1gNnSc6	Polsko
Poláci	Polák	k1gMnPc1	Polák
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
pomohou	pomoct	k5eAaPmIp3nP	pomoct
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gMnSc3	jejich
společnému	společný	k2eAgMnSc3d1	společný
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
Varšavě	Varšava	k1gFnSc3	Varšava
<g/>
,	,	kIx,	,
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
byla	být	k5eAaImAgFnS	být
náhle	náhle	k6eAd1	náhle
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
vysíleny	vysílit	k5eAaPmNgFnP	vysílit
<g/>
,	,	kIx,	,
potřebovaly	potřebovat	k5eAaImAgFnP	potřebovat
doplnit	doplnit	k5eAaPmF	doplnit
stavy	stav	k1gInPc7	stav
a	a	k8xC	a
také	také	k9	také
využít	využít	k5eAaPmF	využít
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
vyčkat	vyčkat	k5eAaPmF	vyčkat
<g/>
,	,	kIx,	,
až	až	k8xS	až
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
Němci	Němec	k1gMnPc1	Němec
s	s	k7c7	s
Poláky	Polák	k1gMnPc7	Polák
vyřídí	vyřídit	k5eAaPmIp3nS	vyřídit
a	a	k8xC	a
povstání	povstání	k1gNnSc4	povstání
potlačí	potlačit	k5eAaPmIp3nS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
proto	proto	k8xC	proto
mohli	moct	k5eAaImAgMnP	moct
povstání	povstání	k1gNnSc4	povstání
nemilosrdně	milosrdně	k6eNd1	milosrdně
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
povstání	povstání	k1gNnSc2	povstání
plánována	plánovat	k5eAaImNgFnS	plánovat
na	na	k7c4	na
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
obránci	obránce	k1gMnPc1	obránce
se	se	k3xPyFc4	se
udrželi	udržet	k5eAaPmAgMnP	udržet
63	[number]	k4	63
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
muselo	muset	k5eAaImAgNnS	muset
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Polska	Polsko	k1gNnSc2	Polsko
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
byli	být	k5eAaImAgMnP	být
převezeni	převézt	k5eAaPmNgMnP	převézt
do	do	k7c2	do
zajateckých	zajatecký	k2eAgInPc2d1	zajatecký
táborů	tábor	k1gInPc2	tábor
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
dojednané	dojednaný	k2eAgFnPc4d1	dojednaná
podmínky	podmínka	k1gFnPc4	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
srovnáno	srovnán	k2eAgNnSc1d1	srovnáno
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnSc2	knihovna
a	a	k8xC	a
muzea	muzeum	k1gNnSc2	muzeum
vyloupeny	vyloupit	k5eAaPmNgInP	vyloupit
nebo	nebo	k8xC	nebo
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
přecházeli	přecházet	k5eAaImAgMnP	přecházet
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
přes	přes	k7c4	přes
Vislu	Visla	k1gFnSc4	Visla
<g/>
,	,	kIx,	,
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Varšava	Varšava	k1gFnSc1	Varšava
téměř	téměř	k6eAd1	téměř
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
85	[number]	k4	85
%	%	kIx~	%
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
historického	historický	k2eAgNnSc2d1	historické
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
a	a	k8xC	a
Královského	královský	k2eAgInSc2d1	královský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc4d1	přeživší
bojovníky	bojovník	k1gMnPc4	bojovník
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
sovětské	sovětský	k2eAgFnSc2d1	sovětská
NKVD	NKVD	kA	NKVD
zabilo	zabít	k5eAaPmAgNnS	zabít
nebo	nebo	k8xC	nebo
poslalo	poslat	k5eAaPmAgNnS	poslat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
válečných	válečný	k2eAgFnPc2d1	válečná
událostí	událost	k1gFnPc2	událost
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
vyčíslen	vyčíslit	k5eAaPmNgInS	vyčíslit
na	na	k7c4	na
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
oběti	oběť	k1gFnPc4	oběť
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
moci	moc	k1gFnSc2	moc
ujala	ujmout	k5eAaPmAgFnS	ujmout
prostalinská	prostalinský	k2eAgFnSc1d1	prostalinský
vláda	vláda	k1gFnSc1	vláda
prezidenta	prezident	k1gMnSc2	prezident
Bolesława	Bolesławus	k1gMnSc2	Bolesławus
Bieruta	Bierut	k1gMnSc2	Bierut
a	a	k8xC	a
Varšava	Varšava	k1gFnSc1	Varšava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Polské	polský	k2eAgFnSc2d1	polská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
kladlo	klást	k5eAaImAgNnS	klást
naléhavou	naléhavý	k2eAgFnSc4d1	naléhavá
otázku	otázka	k1gFnSc4	otázka
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
obnovy	obnova	k1gFnSc2	obnova
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
sloganů	slogan	k1gInPc2	slogan
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
"	"	kIx"	"
<g/>
celý	celý	k2eAgInSc1d1	celý
národ	národ	k1gInSc1	národ
stavět	stavět	k5eAaImF	stavět
novou	nový	k2eAgFnSc4d1	nová
Varšavu	Varšava	k1gFnSc4	Varšava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
obnovit	obnovit	k5eAaPmF	obnovit
nejen	nejen	k6eAd1	nejen
historické	historický	k2eAgFnPc4d1	historická
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
řadové	řadový	k2eAgInPc4d1	řadový
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
úzké	úzký	k2eAgFnPc1d1	úzká
uličky	ulička	k1gFnPc1	ulička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vedeny	vést	k5eAaImNgFnP	vést
v	v	k7c6	v
trasách	trasa	k1gFnPc6	trasa
staletí	staletí	k1gNnSc2	staletí
starých	starý	k2eAgFnPc2d1	stará
cest	cesta	k1gFnPc2	cesta
nahradily	nahradit	k5eAaPmAgInP	nahradit
moderní	moderní	k2eAgInPc1d1	moderní
bulváry	bulvár	k1gInPc1	bulvár
vedené	vedený	k2eAgInPc1d1	vedený
v	v	k7c6	v
severo-jižním	severoižní	k2eAgInSc6d1	severo-jižní
a	a	k8xC	a
východo-západním	východoápadní	k2eAgInSc6d1	východo-západní
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
propojené	propojený	k2eAgInPc4d1	propojený
kruhovými	kruhový	k2eAgInPc7d1	kruhový
objezdy	objezd	k1gInPc7	objezd
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
původních	původní	k2eAgInPc2d1	původní
domů	dům	k1gInPc2	dům
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
starší	starý	k2eAgInSc4d2	starší
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
kostely	kostel	k1gInPc1	kostel
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
vzhledu	vzhled	k1gInSc6	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
dominantou	dominanta	k1gFnSc7	dominanta
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Palác	palác	k1gInSc1	palác
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
dar	dar	k1gInSc1	dar
národů	národ	k1gInPc2	národ
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
polskému	polský	k2eAgInSc3d1	polský
lidu	lid	k1gInSc3	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
zrekonstruované	zrekonstruovaný	k2eAgNnSc1d1	zrekonstruované
historické	historický	k2eAgNnSc1d1	historické
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
do	do	k7c2	do
historického	historický	k2eAgNnSc2d1	historické
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
i	i	k9	i
výstavba	výstavba	k1gFnSc1	výstavba
dopravních	dopravní	k2eAgFnPc2d1	dopravní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
důležité	důležitý	k2eAgFnSc6d1	důležitá
varšavské	varšavský	k2eAgFnSc6d1	Varšavská
dopravní	dopravní	k2eAgFnSc6d1	dopravní
tepně	tepna	k1gFnSc6	tepna
Aleje	alej	k1gFnSc2	alej
Jerozolimskie	Jerozolimskie	k1gFnSc2	Jerozolimskie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěné	umístěný	k2eAgNnSc1d1	umístěné
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
tratí	trať	k1gFnSc7	trať
<g/>
,	,	kIx,	,
vedenou	vedený	k2eAgFnSc4d1	vedená
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
metropole	metropol	k1gFnSc2	metropol
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
dopravní	dopravní	k2eAgFnSc4d1	dopravní
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
východními	východní	k2eAgInPc7d1	východní
a	a	k8xC	a
západními	západní	k2eAgInPc7d1	západní
vlakovými	vlakový	k2eAgInPc7d1	vlakový
spoji	spoj	k1gInPc7	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
Varšava	Varšava	k1gFnSc1	Varšava
již	již	k9	již
z	z	k7c2	z
předválečných	předválečný	k2eAgInPc2d1	předválečný
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zmodernizovala	zmodernizovat	k5eAaPmAgFnS	zmodernizovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
rychlostních	rychlostní	k2eAgFnPc6d1	rychlostní
komunikacích	komunikace	k1gFnPc6	komunikace
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
moderní	moderní	k2eAgInPc1d1	moderní
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Metropoli	metropole	k1gFnSc4	metropole
však	však	k9	však
čekal	čekat	k5eAaImAgInS	čekat
i	i	k8xC	i
přesto	přesto	k8xC	přesto
velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
z	z	k7c2	z
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
začaly	začít	k5eAaPmAgFnP	začít
brzy	brzy	k6eAd1	brzy
doplňovat	doplňovat	k5eAaImF	doplňovat
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začaly	začít	k5eAaPmAgFnP	začít
růst	růst	k5eAaImF	růst
první	první	k4xOgFnPc1	první
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
siluetu	silueta	k1gFnSc4	silueta
metropole	metropol	k1gFnSc2	metropol
pak	pak	k6eAd1	pak
doplnily	doplnit	k5eAaPmAgFnP	doplnit
stavby	stavba	k1gFnPc1	stavba
jako	jako	k8xC	jako
Hotel	hotel	k1gInSc1	hotel
Mariott	Mariotta	k1gFnPc2	Mariotta
či	či	k8xC	či
Blue	Blue	k1gFnPc2	Blue
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
doba	doba	k1gFnSc1	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pak	pak	k6eAd1	pak
přinesla	přinést	k5eAaPmAgFnS	přinést
úplné	úplný	k2eAgNnSc4d1	úplné
otevření	otevření	k1gNnSc4	otevření
západním	západní	k2eAgMnSc7d1	západní
investorům	investor	k1gMnPc3	investor
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
další	další	k2eAgFnPc1d1	další
kancelářské	kancelářský	k2eAgFnPc1d1	kancelářská
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
obchodní	obchodní	k2eAgNnPc1d1	obchodní
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
muzea	muzeum	k1gNnPc1	muzeum
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Muzeum	muzeum	k1gNnSc1	muzeum
dějin	dějiny	k1gFnPc2	dějiny
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
lež	lež	k1gFnSc1	lež
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
Visla	visnout	k5eAaImAgFnS	visnout
v	v	k7c6	v
středomazovské	středomazovský	k2eAgFnSc6d1	středomazovský
nížině	nížina	k1gFnSc6	nížina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
350	[number]	k4	350
km	km	kA	km
od	od	k7c2	od
Karpaty	Karpaty	k1gInPc1	Karpaty
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
evropské	evropský	k2eAgNnSc1d1	Evropské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
Kampinoský	Kampinoský	k2eAgInSc1d1	Kampinoský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Visly	Visla	k1gFnSc2	Visla
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
podél	podél	k7c2	podél
jejích	její	k3xOp3gInPc2	její
břehů	břeh	k1gInPc2	břeh
(	(	kIx(	(
<g/>
v	v	k7c6	v
šíři	šíř	k1gFnSc6	šíř
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
na	na	k7c4	na
sever-jih	severih	k1gInSc4	sever-jih
a	a	k8xC	a
cca	cca	kA	cca
28	[number]	k4	28
km	km	kA	km
na	na	k7c4	na
východ-západ	východápad	k1gInSc4	východ-západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgInPc4d1	mírný
<g/>
,	,	kIx,	,
přechodné	přechodný	k2eAgInPc4d1	přechodný
mezi	mezi	k7c7	mezi
oceánským	oceánský	k2eAgInSc7d1	oceánský
a	a	k8xC	a
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
nebývají	bývat	k5eNaImIp3nP	bývat
velké	velký	k2eAgInPc4d1	velký
meziroční	meziroční	k2eAgInPc4d1	meziroční
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Vegetační	vegetační	k2eAgFnSc1d1	vegetační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
200	[number]	k4	200
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
<g/>
%	%	kIx~	%
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnPc1d1	severovýchodní
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
<g/>
%	%	kIx~	%
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Varšavy	Varšava	k1gFnSc2	Varšava
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
tepleji	teple	k6eAd2	teple
než	než	k8xS	než
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
okraji	okraj	k1gInSc6	okraj
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
7-8	[number]	k4	7-8
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
o	o	k7c4	o
10	[number]	k4	10
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
-31	-31	k4	-31
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
znečištění	znečištění	k1gNnSc3	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
více	hodně	k6eAd2	hodně
oblačno	oblačno	k6eAd1	oblačno
než	než	k8xS	než
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
má	mít	k5eAaImIp3nS	mít
1600	[number]	k4	1600
hodin	hodina	k1gFnPc2	hodina
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
centru	centrum	k1gNnSc6	centrum
(	(	kIx(	(
<g/>
Śródmieście	Śródmieście	k1gFnSc1	Śródmieście
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
nejen	nejen	k6eAd1	nejen
mnoho	mnoho	k4c4	mnoho
národních	národní	k2eAgFnPc2d1	národní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
domácích	domácí	k2eAgFnPc2d1	domácí
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
268	[number]	k4	268
307	[number]	k4	307
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
spoluúčast	spoluúčast	k1gFnSc1	spoluúčast
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
650	[number]	k4	650
miliónů	milión	k4xCgInPc2	milión
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
%	%	kIx~	%
polského	polský	k2eAgInSc2d1	polský
národního	národní	k2eAgInSc2d1	národní
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
Varšavy	Varšava	k1gFnSc2	Varšava
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
kolem	kolem	k7c2	kolem
28	[number]	k4	28
000	[number]	k4	000
USD	USD	kA	USD
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
celopolský	celopolský	k2eAgInSc4d1	celopolský
průměr	průměr	k1gInSc4	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
zdrojů	zdroj	k1gInPc2	zdroj
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
města	město	k1gNnSc2	město
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
se	se	k3xPyFc4	se
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
Varšava	Varšava	k1gFnSc1	Varšava
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
továren	továrna	k1gFnPc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
patřily	patřit	k5eAaImAgFnP	patřit
varšavské	varšavský	k2eAgFnPc1d1	Varšavská
ocelárny	ocelárna	k1gFnPc1	ocelárna
Huta	Hutum	k1gNnSc2	Hutum
Warszawa	Warszawum	k1gNnSc2	Warszawum
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
automobilky	automobilka	k1gFnPc4	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jak	jak	k6eAd1	jak
komunistický	komunistický	k2eAgInSc1d1	komunistický
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
systém	systém	k1gInSc1	systém
upadal	upadat	k5eAaImAgInS	upadat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
komplexů	komplex	k1gInPc2	komplex
postupně	postupně	k6eAd1	postupně
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
po	po	k7c6	po
transformaci	transformace	k1gFnSc6	transformace
politického	politický	k2eAgInSc2d1	politický
i	i	k8xC	i
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
systému	systém	k1gInSc2	systém
mnoho	mnoho	k6eAd1	mnoho
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
firem	firma	k1gFnPc2	firma
zbankrotovalo	zbankrotovat	k5eAaPmAgNnS	zbankrotovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
firma	firma	k1gFnSc1	firma
Lucchini-Warzsawa	Lucchini-Warzsawa	k1gMnSc1	Lucchini-Warzsawa
Steel	Steel	k1gMnSc1	Steel
Mill	Mill	k1gMnSc1	Mill
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
původní	původní	k2eAgFnSc2d1	původní
továrny	továrna	k1gFnSc2	továrna
Huta	Huta	k1gMnSc1	Huta
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
.	.	kIx.	.
</s>
<s>
Automobilky	automobilka	k1gFnSc2	automobilka
Ursus	Ursus	k1gInSc1	Ursus
a	a	k8xC	a
FSO	FSO	kA	FSO
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Fabryka	Fabryek	k1gMnSc4	Fabryek
Samochodów	Samochodów	k1gMnSc4	Samochodów
Osobowych	Osobowych	k1gMnSc1	Osobowych
-	-	kIx~	-
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
<g/>
)	)	kIx)	)
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
vozy	vůz	k1gInPc4	vůz
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
center	centrum	k1gNnPc2	centrum
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nP	sídlet
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgFnPc4	čtyři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
přes	přes	k7c4	přes
62	[number]	k4	62
dalších	další	k2eAgNnPc2d1	další
vzdělávacích	vzdělávací	k2eAgNnPc2d1	vzdělávací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Uniwersytet	Uniwersytet	k1gInSc1	Uniwersytet
Warszawski	Warszawski	k1gNnSc1	Warszawski
<g/>
)	)	kIx)	)
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Politechnika	Politechnika	k1gFnSc1	Politechnika
Warszawska	Warszawska	k1gFnSc1	Warszawska
<g/>
)	)	kIx)	)
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Szkoła	Szkoł	k2eAgFnSc1d1	Szkoła
Główna	Główen	k2eAgFnSc1d1	Główen
Handlowa	Handlowa	k1gFnSc1	Handlowa
<g/>
)	)	kIx)	)
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Szkoła	Szkoł	k2eAgFnSc1d1	Szkoła
Główna	Główen	k2eAgFnSc1d1	Główen
Gospodarstwa	Gospodarstwa	k1gFnSc1	Gospodarstwa
Wiejskiego	Wiejskiego	k1gMnSc1	Wiejskiego
<g/>
)	)	kIx)	)
Univerzita	univerzita	k1gFnSc1	univerzita
Kardinála	kardinál	k1gMnSc2	kardinál
Stefana	Stefan	k1gMnSc2	Stefan
Wyszyńského	Wyszyńský	k1gMnSc2	Wyszyńský
(	(	kIx(	(
<g/>
Uniwersytet	Uniwersytet	k1gMnSc1	Uniwersytet
Kardynała	Kardynała	k1gMnSc1	Kardynała
<g />
.	.	kIx.	.
</s>
<s>
Stefana	Stefan	k1gMnSc4	Stefan
Wyszyńskiego	Wyszyńskiego	k1gMnSc1	Wyszyńskiego
<g/>
)	)	kIx)	)
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
lékařská	lékařský	k2eAgFnSc1d1	lékařská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Akademia	Akademia	k1gFnSc1	Akademia
Medyczna	Medyczna	k1gFnSc1	Medyczna
w	w	k?	w
Warszawie	Warszawie	k1gFnSc1	Warszawie
<g/>
)	)	kIx)	)
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
technická	technický	k2eAgFnSc1d1	technická
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
Wojskowa	Wojskow	k2eAgFnSc1d1	Wojskow
Akademia	Akademia	k1gFnSc1	Akademia
Techniczna	Techniczna	k1gFnSc1	Techniczna
<g/>
)	)	kIx)	)
Akademie	akademie	k1gFnSc1	akademie
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
Akademia	Akademia	k1gFnSc1	Akademia
Obrony	Obrona	k1gFnSc2	Obrona
Narodowej	Narodowej	k1gInSc1	Narodowej
<g/>
)	)	kIx)	)
Academy	Academa	k1gFnPc1	Academa
of	of	k?	of
Physical	Physical	k1gFnSc1	Physical
Education	Education	k1gInSc1	Education
in	in	k?	in
Warsaw	Warsaw	k1gMnSc2	Warsaw
(	(	kIx(	(
<g/>
Akademia	Akademium	k1gNnSc2	Akademium
Wychowania	Wychowanium	k1gNnSc2	Wychowanium
Fizycznego	Fizycznego	k1gMnSc1	Fizycznego
w	w	k?	w
Warszawie	Warszawie	k1gFnSc1	Warszawie
<g/>
)	)	kIx)	)
Hudební	hudební	k2eAgFnSc1d1	hudební
akademie	akademie	k1gFnSc1	akademie
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
(	(	kIx(	(
<g/>
Akademia	Akademia	k1gFnSc1	Akademia
Muzyczna	Muzyczna	k1gFnSc1	Muzyczna
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyka	Fryderyka	k1gMnSc1	Fryderyka
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
)	)	kIx)	)
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
divadelní	divadelní	k2eAgFnSc1d1	divadelní
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
Akademia	Akademia	k1gFnSc1	Akademia
Teatralna	Teatralna	k1gFnSc1	Teatralna
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
Aleksandra	Aleksandra	k1gFnSc1	Aleksandra
Zelwerowicza	Zelwerowicza	k1gFnSc1	Zelwerowicza
<g/>
)	)	kIx)	)
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
všech	všecek	k3xTgInPc2	všecek
stupňů	stupeň	k1gInPc2	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
500,000	[number]	k4	500,000
(	(	kIx(	(
<g/>
29.2	[number]	k4	29.2
<g/>
%	%	kIx~	%
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
populace	populace	k1gFnSc2	populace
<g/>
;	;	kIx,	;
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
univerzitních	univerzitní	k2eAgMnPc2d1	univerzitní
studentů	student	k1gMnPc2	student
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
255,000	[number]	k4	255,000
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
231	[number]	k4	231
m	m	kA	m
Warsaw	Warsaw	k1gFnPc2	Warsaw
Trade	Trad	k1gInSc5	Trad
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
208	[number]	k4	208
m	m	kA	m
(	(	kIx(	(
<g/>
WTT	WTT	kA	WTT
<g/>
)	)	kIx)	)
Złota	Złota	k1gFnSc1	Złota
44	[number]	k4	44
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
192	[number]	k4	192
m	m	kA	m
Rondo	rondo	k1gNnSc1	rondo
1	[number]	k4	1
<g/>
,	,	kIx,	,
2006,192	[number]	k4	2006,192
m	m	kA	m
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Hotel	hotel	k1gInSc1	hotel
<g />
.	.	kIx.	.
</s>
<s>
Marriott	Marriott	k1gMnSc1	Marriott
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
170	[number]	k4	170
m	m	kA	m
Warsaw	Warsaw	k1gMnSc1	Warsaw
Financial	Financial	k1gMnSc1	Financial
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
165	[number]	k4	165
m	m	kA	m
(	(	kIx(	(
<g/>
WFC	WFC	kA	WFC
<g/>
)	)	kIx)	)
Hotel	hotel	k1gInSc1	hotel
InterContinental	InterContinental	k1gMnSc1	InterContinental
Warszawa	Warszawa	k1gMnSc1	Warszawa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
164	[number]	k4	164
m	m	kA	m
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
Oxford	Oxford	k1gInSc1	Oxford
Tower	Towra	k1gFnPc2	Towra
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
150	[number]	k4	150
m	m	kA	m
TP	TP	kA	TP
S.	S.	kA	S.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
128	[number]	k4	128
m	m	kA	m
Błękitny	Błękitna	k1gFnPc1	Błękitna
Wieżowiec	Wieżowiec	k1gMnSc1	Wieżowiec
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
m	m	kA	m
Hotel	hotel	k1gInSc1	hotel
Westin	Westin	k1gInSc1	Westin
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
120	[number]	k4	120
m	m	kA	m
Milennium	Milennium	k1gNnSc1	Milennium
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
116	[number]	k4	116
m	m	kA	m
ORCO	ORCO	kA	ORCO
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
115	[number]	k4	115
m	m	kA	m
Novotel	Novotel	k1gMnSc1	Novotel
Warszawa	Warszawa	k1gMnSc1	Warszawa
Centrum	centrum	k1gNnSc1	centrum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
111	[number]	k4	111
m	m	kA	m
Złote	Złot	k1gInSc5	Złot
Tarasy	taras	k1gInPc1	taras
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
105	[number]	k4	105
m	m	kA	m
Babka	Babka	k1gMnSc1	Babka
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
105	[number]	k4	105
m	m	kA	m
Ilmet	Ilmeta	k1gFnPc2	Ilmeta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
103	[number]	k4	103
m	m	kA	m
stavět	stavět	k5eAaImF	stavět
Warsaw	Warsaw	k1gMnSc5	Warsaw
Spire	Spir	k1gMnSc5	Spir
<g/>
,	,	kIx,	,
220	[number]	k4	220
m	m	kA	m
Cosmopolitan	Cosmopolitan	k1gInSc1	Cosmopolitan
Twarda	Twarda	k1gFnSc1	Twarda
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
160	[number]	k4	160
m	m	kA	m
</s>
