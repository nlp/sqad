<p>
<s>
Android	android	k1gInSc1	android
(	(	kIx(	(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1	stylizováno
jako	jako	k9	jako
android	android	k1gInSc4	android
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgInSc4d1	mobilní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
jádře	jádro	k1gNnSc6	jádro
Linuxu	linux	k1gInSc2	linux
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
jako	jako	k8xC	jako
otevřený	otevřený	k2eAgInSc1d1	otevřený
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
open	open	k1gInSc1	open
source	sourec	k1gInSc2	sourec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
na	na	k7c6	na
smartphonech	smartphon	k1gInPc6	smartphon
<g/>
,	,	kIx,	,
tabletech	tablet	k1gInPc6	tablet
<g/>
,	,	kIx,	,
chytrých	chytrý	k2eAgFnPc6d1	chytrá
televizích	televize	k1gFnPc6	televize
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
vede	vést	k5eAaImIp3nS	vést
firma	firma	k1gFnSc1	firma
Google	Google	k1gFnSc1	Google
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
konsorcia	konsorcium	k1gNnSc2	konsorcium
firem	firma	k1gFnPc2	firma
Open	Opena	k1gFnPc2	Opena
Handset	Handset	k1gInSc1	Handset
Alliance	Allianec	k1gInSc2	Allianec
a	a	k8xC	a
výrobci	výrobce	k1gMnPc1	výrobce
různých	různý	k2eAgNnPc2d1	různé
zařízení	zařízení	k1gNnPc2	zařízení
mohou	moct	k5eAaImIp3nP	moct
Android	android	k1gInSc4	android
upravovat	upravovat	k5eAaImF	upravovat
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
stanovených	stanovený	k2eAgFnPc2d1	stanovená
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
název	název	k1gInSc4	název
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
sám	sám	k3xTgMnSc1	sám
vývojář	vývojář	k1gMnSc1	vývojář
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
MIUI	MIUI	kA	MIUI
od	od	k7c2	od
Xiaomi	Xiao	k1gFnPc7	Xiao
<g/>
,	,	kIx,	,
Touchwiz	Touchwiz	k1gInSc4	Touchwiz
od	od	k7c2	od
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
vynechán	vynechán	k2eAgInSc1d1	vynechán
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc1d1	velká
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
systémy	systém	k1gInPc4	systém
YunOS	YunOS	k1gMnSc2	YunOS
<g/>
,	,	kIx,	,
Fire	Fir	k1gMnSc2	Fir
OS	OS	kA	OS
nebo	nebo	k8xC	nebo
OxygenOS	OxygenOS	k1gFnSc2	OxygenOS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
Android	android	k1gInSc1	android
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc4d3	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tabletech	tablet	k1gInPc6	tablet
je	být	k5eAaImIp3nS	být
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
systémem	systém	k1gInSc7	systém
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
a	a	k8xC	a
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgNnSc1d1	dominantní
podle	podle	k7c2	podle
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kvartálu	kvartál	k1gInSc6	kvartál
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
měl	mít	k5eAaImAgInS	mít
Android	android	k1gInSc1	android
86,8	[number]	k4	86,8
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
prodaných	prodaný	k2eAgInPc2d1	prodaný
chytrých	chytrý	k2eAgInPc2d1	chytrý
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkových	celkový	k2eAgInPc6d1	celkový
prodejích	prodej	k1gInPc6	prodej
je	být	k5eAaImIp3nS	být
Android	android	k1gInSc1	android
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
následován	následován	k2eAgMnSc1d1	následován
systémy	systém	k1gInPc7	systém
iOS	iOS	k?	iOS
a	a	k8xC	a
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Android	android	k1gInSc1	android
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
konsorcium	konsorcium	k1gNnSc1	konsorcium
Open	Opena	k1gFnPc2	Opena
Handset	Handseta	k1gFnPc2	Handseta
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
progresivní	progresivní	k2eAgInSc4d1	progresivní
rozvoj	rozvoj	k1gInSc4	rozvoj
mobilních	mobilní	k2eAgFnPc2d1	mobilní
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgInPc4d2	nižší
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
přinese	přinést	k5eAaPmIp3nS	přinést
inovativní	inovativní	k2eAgNnSc1d1	inovativní
uživatelsky	uživatelsky	k6eAd1	uživatelsky
přívětivé	přívětivý	k2eAgNnSc1d1	přívětivé
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
systému	systém	k1gInSc2	systém
byla	být	k5eAaImAgFnS	být
brána	brát	k5eAaImNgFnS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
klasická	klasický	k2eAgNnPc1d1	klasické
mobilní	mobilní	k2eAgNnPc1d1	mobilní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
výdrž	výdrž	k1gFnSc1	výdrž
baterie	baterie	k1gFnSc1	baterie
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
výkonnost	výkonnost	k1gFnSc1	výkonnost
a	a	k8xC	a
málo	málo	k1gNnSc1	málo
dostupné	dostupný	k2eAgFnSc2d1	dostupná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
jádro	jádro	k1gNnSc1	jádro
Androidu	android	k1gInSc2	android
navrženo	navržen	k2eAgNnSc1d1	navrženo
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
na	na	k7c6	na
různém	různý	k2eAgInSc6d1	různý
hardwaru	hardware	k1gInSc6	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
použitou	použitý	k2eAgFnSc4d1	použitá
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
čipovou	čipový	k2eAgFnSc4d1	čipová
sadu	sada	k1gFnSc4	sada
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
nebo	nebo	k8xC	nebo
rozlišení	rozlišení	k1gNnSc4	rozlišení
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
.	.	kIx.	.
<g/>
Samotná	samotný	k2eAgFnSc1d1	samotná
platforma	platforma	k1gFnSc1	platforma
Android	android	k1gInSc1	android
dává	dávat	k5eAaImIp3nS	dávat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nejen	nejen	k6eAd1	nejen
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
s	s	k7c7	s
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
prostředím	prostředí	k1gNnSc7	prostředí
pro	pro	k7c4	pro
koncové	koncový	k2eAgMnPc4d1	koncový
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kompletní	kompletní	k2eAgNnSc1d1	kompletní
řešení	řešení	k1gNnSc1	řešení
nasazení	nasazení	k1gNnSc4	nasazení
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
specifikace	specifikace	k1gFnSc1	specifikace
ovladačů	ovladač	k1gInPc2	ovladač
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgMnPc4d1	mobilní
operátory	operátor	k1gMnPc4	operátor
a	a	k8xC	a
výrobce	výrobce	k1gMnPc4	výrobce
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
aplikací	aplikace	k1gFnSc7	aplikace
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
efektivní	efektivní	k2eAgInPc4d1	efektivní
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
–	–	k?	–
Software	software	k1gInSc1	software
Development	Development	k1gMnSc1	Development
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
SDK	SDK	kA	SDK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Android	android	k1gInSc1	android
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
Andym	Andym	k1gInSc1	Andym
Rubinem	Rubino	k1gNnSc7	Rubino
<g/>
,	,	kIx,	,
Richem	Rich	k1gInSc7	Rich
Minerem	Miner	k1gMnSc7	Miner
<g/>
,	,	kIx,	,
Nickem	Nicek	k1gMnSc7	Nicek
Searsem	Sears	k1gMnSc7	Sears
a	a	k8xC	a
Chrisem	Chris	k1gInSc7	Chris
Whitem	Whit	k1gMnSc7	Whit
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k1gFnSc1	Google
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nepříliš	příliš	k6eNd1	příliš
známou	známý	k2eAgFnSc4d1	známá
startup	startup	k1gInSc4	startup
společnost	společnost	k1gFnSc1	společnost
Android	android	k1gInSc1	android
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
svoji	svůj	k3xOyFgFnSc4	svůj
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
odkupu	odkup	k1gInSc6	odkup
společnosti	společnost	k1gFnSc2	společnost
tým	tým	k1gInSc1	tým
Googlu	Googl	k1gInSc2	Googl
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Andyho	Andy	k1gMnSc2	Andy
Rubina	Rubin	k1gMnSc2	Rubin
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
platformu	platforma	k1gFnSc4	platforma
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
Linuxovém	linuxový	k2eAgNnSc6d1	linuxové
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Google	Google	k1gNnPc2	Google
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc1	několik
patentů	patent	k1gInPc2	patent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mobilních	mobilní	k2eAgFnPc2d1	mobilní
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
veřejnost	veřejnost	k1gFnSc1	veřejnost
začala	začít	k5eAaPmAgFnS	začít
po	po	k7c6	po
akvizici	akvizice	k1gFnSc6	akvizice
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Google	Google	k1gInSc1	Google
chce	chtít	k5eAaImIp3nS	chtít
tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
vstoupit	vstoupit	k5eAaPmF	vstoupit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
"	"	kIx"	"
<g/>
chytrých	chytrý	k2eAgInPc2d1	chytrý
<g/>
"	"	kIx"	"
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
chystá	chystat	k5eAaImIp3nS	chystat
vydání	vydání	k1gNnSc4	vydání
vlastního	vlastní	k2eAgInSc2d1	vlastní
mobilu	mobil	k1gInSc2	mobil
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
uskupení	uskupení	k1gNnSc4	uskupení
Open	Opena	k1gFnPc2	Opena
Handset	Handseta	k1gFnPc2	Handseta
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
.	.	kIx.	.
</s>
<s>
Konsorcium	konsorcium	k1gNnSc1	konsorcium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
společnosti	společnost	k1gFnPc4	společnost
zabývající	zabývající	k2eAgFnPc4d1	zabývající
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
čipů	čip	k1gInPc2	čip
nebo	nebo	k8xC	nebo
mobilních	mobilní	k2eAgFnPc2d1	mobilní
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Google	Google	k1gInSc1	Google
<g/>
,	,	kIx,	,
HTC	HTC	kA	HTC
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
Motorola	Motorola	kA	Motorola
<g/>
,	,	kIx,	,
nVidia	nVidium	k1gNnSc2	nVidium
<g/>
,	,	kIx,	,
Qualcomm	Qualcomm	k1gMnSc1	Qualcomm
<g/>
,	,	kIx,	,
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
Texas	Texas	kA	Texas
Instruments	Instruments	kA	Instruments
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
25	[number]	k4	25
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgNnSc2	tento
konsorcia	konsorcium	k1gNnSc2	konsorcium
bylo	být	k5eAaImAgNnS	být
vyvinout	vyvinout	k5eAaPmF	vyvinout
otevřený	otevřený	k2eAgInSc4d1	otevřený
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
den	den	k1gInSc4	den
Open	Opena	k1gFnPc2	Opena
Handset	Handset	k1gMnSc1	Handset
Alliance	Alliance	k1gFnSc2	Alliance
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
Android	android	k1gInSc4	android
<g/>
,	,	kIx,	,
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
mobilní	mobilní	k2eAgFnSc4d1	mobilní
platformu	platforma	k1gFnSc4	platforma
postavenou	postavený	k2eAgFnSc4d1	postavená
na	na	k7c6	na
jádře	jádro	k1gNnSc6	jádro
Linux	Linux	kA	Linux
verze	verze	k1gFnSc1	verze
2.6	[number]	k4	2.6
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Schmidt	Schmidt	k1gMnSc1	Schmidt
dal	dát	k5eAaPmAgMnS	dát
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
Google	Google	k1gNnSc1	Google
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Android	android	k1gInSc4	android
velké	velký	k2eAgInPc1d1	velký
plány	plán	k1gInPc1	plán
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
oznámení	oznámení	k1gNnSc1	oznámení
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
ambicióznější	ambiciózní	k2eAgNnSc1d2	ambicióznější
<g/>
,	,	kIx,	,
než	než	k8xS	než
pouhý	pouhý	k2eAgInSc1d1	pouhý
Google	Google	k1gInSc1	Google
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
spekuloval	spekulovat	k5eAaImAgMnS	spekulovat
tisk	tisk	k1gInSc4	tisk
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Naší	náš	k3xOp1gFnSc7	náš
vizí	vize	k1gFnSc7	vize
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
platforma	platforma	k1gFnSc1	platforma
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
představujeme	představovat	k5eAaImIp1nP	představovat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgNnP	použít
na	na	k7c6	na
tisících	tisíc	k4xCgInPc6	tisíc
rozdílných	rozdílný	k2eAgInPc6d1	rozdílný
telefonních	telefonní	k2eAgInPc6d1	telefonní
modelech	model	k1gInPc6	model
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
Android	android	k1gInSc1	android
SDK	SDK	kA	SDK
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
open-source	openourka	k1gFnSc6	open-sourka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
uveden	uvést	k5eAaPmNgInS	uvést
první	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
G1	G1	k1gFnPc3	G1
(	(	kIx(	(
<g/>
HTC	HTC	kA	HTC
Dream	Dream	k1gInSc1	Dream
<g/>
)	)	kIx)	)
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Android	android	k1gInSc1	android
(	(	kIx(	(
<g/>
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
uvolněno	uvolnit	k5eAaPmNgNnS	uvolnit
SDK	SDK	kA	SDK
1.0	[number]	k4	1.0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
zařízení	zařízení	k1gNnSc2	zařízení
používající	používající	k2eAgInSc1d1	používající
Android	android	k1gInSc1	android
na	na	k7c4	na
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
dvacet	dvacet	k4xCc4	dvacet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Android	android	k1gInSc1	android
stal	stát	k5eAaPmAgInS	stát
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
platformou	platforma	k1gFnSc7	platforma
smartphonů	smartphon	k1gInPc2	smartphon
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
59	[number]	k4	59
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
už	už	k6eAd1	už
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dominoval	dominovat	k5eAaImAgInS	dominovat
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Architektura	architektura	k1gFnSc1	architektura
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
5	[number]	k4	5
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vrstva	vrstva	k1gFnSc1	vrstva
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
účel	účel	k1gInSc4	účel
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
přímo	přímo	k6eAd1	přímo
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
vrstva	vrstva	k1gFnSc1	vrstva
architektury	architektura	k1gFnSc2	architektura
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
abstraktní	abstraktní	k2eAgFnSc4d1	abstraktní
vrstvu	vrstva	k1gFnSc4	vrstva
mezi	mezi	k7c7	mezi
používaným	používaný	k2eAgInSc7d1	používaný
hardwarem	hardware	k1gInSc7	hardware
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
softwaru	software	k1gInSc2	software
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
Linux	linux	k1gInSc4	linux
<g/>
,	,	kIx,	,
firmy	firma	k1gFnPc1	firma
vyvíjející	vyvíjející	k2eAgFnPc1d1	vyvíjející
Android	android	k1gInSc4	android
tak	tak	k8xS	tak
jsou	být	k5eAaImIp3nP	být
předními	přední	k2eAgMnPc7d1	přední
přispěvateli	přispěvatel	k1gMnPc7	přispěvatel
linuxového	linuxový	k2eAgNnSc2d1	linuxové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
verzích	verze	k1gFnPc6	verze
Androidu	android	k1gInSc2	android
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
verze	verze	k1gFnPc4	verze
Linuxu	linux	k1gInSc2	linux
2.6	[number]	k4	2.6
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
verze	verze	k1gFnSc2	verze
využívají	využívat	k5eAaPmIp3nP	využívat
novější	nový	k2eAgNnPc1d2	novější
linuxová	linuxový	k2eAgNnPc1d1	linuxové
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využito	využít	k5eAaPmNgNnS	využít
mnoha	mnoho	k4c2	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
Linuxu	linux	k1gInSc2	linux
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
podpora	podpora	k1gFnSc1	podpora
správy	správa	k1gFnSc2	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
zabudované	zabudovaný	k2eAgInPc4d1	zabudovaný
ovladače	ovladač	k1gInPc4	ovladač
nebo	nebo	k8xC	nebo
správy	správa	k1gFnPc4	správa
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
souběžného	souběžný	k2eAgInSc2d1	souběžný
běhu	běh	k1gInSc2	běh
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
běží	běžet	k5eAaImIp3nP	běžet
jako	jako	k9	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
procesy	proces	k1gInPc1	proces
s	s	k7c7	s
oprávněním	oprávnění	k1gNnSc7	oprávnění
stanoveným	stanovený	k2eAgNnSc7d1	stanovené
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
stabilitě	stabilita	k1gFnSc3	stabilita
a	a	k8xC	a
také	také	k6eAd1	také
ochraně	ochrana	k1gFnSc3	ochrana
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
systém	systém	k1gInSc1	systém
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
X	X	kA	X
Window	Window	k1gMnSc2	Window
<g/>
,	,	kIx,	,
init	init	k2eAgInSc1d1	init
Systemd	Systemd	k1gInSc1	Systemd
a	a	k8xC	a
ani	ani	k8xC	ani
úplnou	úplný	k2eAgFnSc4d1	úplná
sadu	sada	k1gFnSc4	sada
GNU	gnu	k1gNnSc2	gnu
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
použití	použití	k1gNnSc2	použití
jádra	jádro	k1gNnSc2	jádro
Linux	Linux	kA	Linux
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vlastnost	vlastnost	k1gFnSc4	vlastnost
poměrně	poměrně	k6eAd1	poměrně
snadného	snadný	k2eAgNnSc2d1	snadné
sestavení	sestavení	k1gNnSc2	sestavení
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
zařízeních	zařízení	k1gNnPc6	zařízení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zaručená	zaručený	k2eAgFnSc1d1	zaručená
přenositelnost	přenositelnost	k1gFnSc1	přenositelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
vrstvou	vrstva	k1gFnSc7	vrstva
jsou	být	k5eAaImIp3nP	být
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
napsány	napsat	k5eAaBmNgInP	napsat
v	v	k7c6	v
C	C	kA	C
nebo	nebo	k8xC	nebo
C	C	kA	C
<g/>
++	++	k?	++
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
je	on	k3xPp3gInPc4	on
různé	různý	k2eAgInPc4d1	různý
komponenty	komponent	k1gInPc4	komponent
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
vývojářům	vývojář	k1gMnPc3	vývojář
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Android	android	k1gInSc1	android
Application	Application	k1gInSc1	Application
Framework	Framework	k1gInSc1	Framework
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc1	některý
příklady	příklad	k1gInPc1	příklad
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Media	medium	k1gNnPc4	medium
Libraries	Libraries	k1gInSc1	Libraries
–	–	k?	–
Knihovna	knihovna	k1gFnSc1	knihovna
podporuje	podporovat	k5eAaImIp3nS	podporovat
přehrávání	přehrávání	k1gNnSc4	přehrávání
video	video	k1gNnSc1	video
a	a	k8xC	a
audio	audio	k2eAgInPc2d1	audio
formátů	formát	k1gInPc2	formát
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
obrazové	obrazový	k2eAgInPc1d1	obrazový
soubory	soubor	k1gInPc1	soubor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
MPEG	MPEG	kA	MPEG
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
.264	.264	k4	.264
<g/>
,	,	kIx,	,
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
AAC	AAC	kA	AAC
<g/>
,	,	kIx,	,
AMR	AMR	kA	AMR
<g/>
,	,	kIx,	,
JPG	JPG	kA	JPG
a	a	k8xC	a
PNG	PNG	kA	PNG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LibWebCore	LibWebCor	k1gMnSc5	LibWebCor
–	–	k?	–
Knihovna	knihovna	k1gFnSc1	knihovna
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
vložené	vložený	k2eAgInPc4d1	vložený
náhledy	náhled	k1gInPc4	náhled
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Libc	Libc	k1gFnSc1	Libc
–	–	k?	–
Odvozená	odvozený	k2eAgFnSc1d1	odvozená
BSD	BSD	kA	BSD
standardní	standardní	k2eAgFnSc1d1	standardní
knihovna	knihovna	k1gFnSc1	knihovna
systému	systém	k1gInSc2	systém
C	C	kA	C
vyladěná	vyladěný	k2eAgFnSc1d1	vyladěná
pro	pro	k7c4	pro
embedded	embedded	k1gInSc4	embedded
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SQLite	SQLit	k1gMnSc5	SQLit
–	–	k?	–
Odlehčená	odlehčený	k2eAgFnSc1d1	odlehčená
relační	relační	k2eAgFnSc1d1	relační
databázová	databázový	k2eAgFnSc1d1	databázová
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OpenSSL	OpenSSL	k?	OpenSSL
–	–	k?	–
Secure	Secur	k1gMnSc5	Secur
sockets	sockets	k6eAd1	sockets
layer	layrat	k5eAaPmRp2nS	layrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FreeType	FreeTyp	k1gInSc5	FreeTyp
–	–	k?	–
Knihovna	knihovna	k1gFnSc1	knihovna
pro	pro	k7c4	pro
rendering	rendering	k1gInSc4	rendering
bitmapových	bitmapový	k2eAgInPc2d1	bitmapový
a	a	k8xC	a
vektorových	vektorový	k2eAgInPc2d1	vektorový
fontů	font	k1gInPc2	font
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OpenGL	OpenGL	k?	OpenGL
ES	ES	kA	ES
–	–	k?	–
Knihovna	knihovna	k1gFnSc1	knihovna
na	na	k7c4	na
vykreslování	vykreslování	k1gNnSc4	vykreslování
3D	[number]	k4	3D
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
akceleraci	akcelerace	k1gFnSc4	akcelerace
výpočtů	výpočet	k1gInPc2	výpočet
(	(	kIx(	(
<g/>
jako	jako	k9	jako
OpenGL	OpenGL	k1gFnSc4	OpenGL
a	a	k8xC	a
OpenCL	OpenCL	k1gFnSc4	OpenCL
na	na	k7c6	na
osobním	osobní	k2eAgInSc6d1	osobní
počítači	počítač	k1gInSc6	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Android	android	k1gInSc1	android
Runtime	runtime	k1gInSc1	runtime
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Do	do	k7c2	do
verze	verze	k1gFnSc2	verze
4.3	[number]	k4	4.3
====	====	k?	====
</s>
</p>
<p>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vrstvu	vrstva	k1gFnSc4	vrstva
obsahující	obsahující	k2eAgInSc4d1	obsahující
aplikační	aplikační	k2eAgInSc4d1	aplikační
virtuální	virtuální	k2eAgInSc4d1	virtuální
stroj	stroj	k1gInSc4	stroj
zvaný	zvaný	k2eAgInSc1d1	zvaný
Dalvik	Dalvik	k1gInSc1	Dalvik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
<g/>
,	,	kIx,	,
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
Googlu	Googlo	k1gNnSc6	Googlo
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Dana	Dan	k1gMnSc2	Dan
Bornsteina	Bornstein	k1gMnSc2	Bornstein
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Dalvik	Dalvik	k1gInSc1	Dalvik
Turbo	turba	k1gFnSc5	turba
od	od	k7c2	od
francouzsko-švýcarské	francouzsko-švýcarský	k2eAgFnSc2d1	francouzsko-švýcarská
firmy	firma	k1gFnSc2	firma
Myriad	Myriad	k1gInSc1	Myriad
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
úspornější	úsporný	k2eAgMnSc1d2	úspornější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
Dalvikem	Dalvik	k1gInSc7	Dalvik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
virtuálního	virtuální	k2eAgInSc2d1	virtuální
stroje	stroj	k1gInSc2	stroj
Dalvik	Dalvik	k1gMnSc1	Dalvik
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
současné	současný	k2eAgFnSc6d1	současná
existenci	existence	k1gFnSc6	existence
JVM	JVM	kA	JVM
od	od	k7c2	od
Oracle	Oracle	k1gFnSc2	Oracle
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgNnP	být
licenční	licenční	k2eAgNnPc1d1	licenční
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jazyk	jazyk	k1gInSc1	jazyk
Java	Javum	k1gNnSc2	Javum
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
knihovny	knihovna	k1gFnPc1	knihovna
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgInPc1d1	šiřitelný
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
JVM	JVM	kA	JVM
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
optimalizace	optimalizace	k1gFnSc1	optimalizace
virtuálního	virtuální	k2eAgInSc2d1	virtuální
stroje	stroj	k1gInSc2	stroj
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
poměru	poměr	k1gInSc2	poměr
úspory	úspora	k1gFnSc2	úspora
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
základní	základní	k2eAgFnPc1d1	základní
knihovny	knihovna	k1gFnPc1	knihovna
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Knihovny	knihovna	k1gFnSc2	knihovna
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
obsahem	obsah	k1gInSc7	obsah
blíží	blížit	k5eAaImIp3nS	blížit
platformě	platforma	k1gFnSc3	platforma
Java	Jav	k1gInSc2	Jav
Standard	standard	k1gInSc1	standard
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
knihoven	knihovna	k1gFnPc2	knihovna
pro	pro	k7c4	pro
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
Abstract	Abstract	k2eAgMnSc1d1	Abstract
Window	Window	k1gMnSc1	Window
Toolkit	Toolkit	k1gMnSc1	Toolkit
a	a	k8xC	a
Swing	swing	k1gInSc1	swing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
knihovnami	knihovna	k1gFnPc7	knihovna
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
nebo	nebo	k8xC	nebo
přidání	přidání	k1gNnSc4	přidání
knihovny	knihovna	k1gFnSc2	knihovna
Apache	Apach	k1gFnSc2	Apach
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Překlad	překlad	k1gInSc1	překlad
aplikace	aplikace	k1gFnSc2	aplikace
napsané	napsaný	k2eAgFnSc2d1	napsaná
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
než	než	k8xS	než
verze	verze	k1gFnSc1	verze
4.4	[number]	k4	4.4
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
zkompilováním	zkompilování	k1gNnSc7	zkompilování
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
Java	Jav	k1gInSc2	Jav
kódu	kód	k1gInSc2	kód
do	do	k7c2	do
Java	Jav	k1gInSc2	Jav
byte	byte	k1gInSc1	byte
kódu	kód	k1gInSc2	kód
pomocí	pomocí	k7c2	pomocí
stejného	stejný	k2eAgInSc2d1	stejný
kompilátoru	kompilátor	k1gInSc2	kompilátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
překladu	překlad	k1gInSc2	překlad
Java	Javum	k1gNnSc2	Javum
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
překompiluje	překompilovat	k5eAaImIp3nS	překompilovat
Java	Java	k1gFnSc1	Java
byte	byte	k1gInSc4	byte
kód	kód	k1gInSc1	kód
pomocí	pomoc	k1gFnPc2	pomoc
Dalvik	Dalvik	k1gInSc1	Dalvik
kompilátoru	kompilátor	k1gInSc2	kompilátor
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
Dalvik	Dalvik	k1gInSc1	Dalvik
byte	byte	k1gInSc1	byte
kód	kód	k1gInSc4	kód
je	být	k5eAaImIp3nS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c6	na
DVM	DVM	kA	DVM
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
spuštěná	spuštěný	k2eAgFnSc1d1	spuštěná
Android	android	k1gInSc1	android
aplikace	aplikace	k1gFnPc4	aplikace
běží	běžet	k5eAaImIp3nS	běžet
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
instancí	instance	k1gFnSc7	instance
DVM	DVM	kA	DVM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
4.4	[number]	k4	4.4
včetně	včetně	k7c2	včetně
(	(	kIx(	(
<g/>
ART	ART	kA	ART
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
Androidu	android	k1gInSc2	android
4.4	[number]	k4	4.4
(	(	kIx(	(
<g/>
a	a	k8xC	a
Android	android	k1gInSc1	android
5.0	[number]	k4	5.0
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
ART	ART	kA	ART
–	–	k?	–
Android	android	k1gInSc1	android
RunTime	runtime	k1gInSc1	runtime
<g/>
)	)	kIx)	)
dopředná	dopředný	k2eAgFnSc1d1	dopředná
kompilace	kompilace	k1gFnSc1	kompilace
(	(	kIx(	(
<g/>
AOT	AOT	kA	AOT
–	–	k?	–
Ahead-of-time	Aheadfim	k1gInSc5	Ahead-of-tim
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jsou	být	k5eAaImIp3nP	být
úspory	úspora	k1gFnPc4	úspora
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
výrazné	výrazný	k2eAgNnSc1d1	výrazné
zrychlení	zrychlení	k1gNnSc1	zrychlení
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopředná	Dopředný	k2eAgFnSc1d1	Dopředná
kompilace	kompilace	k1gFnSc1	kompilace
v	v	k7c6	v
Androidu	android	k1gInSc6	android
4.4	[number]	k4	4.4
<g/>
+	+	kIx~	+
funguje	fungovat	k5eAaImIp3nS	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
aplikace	aplikace	k1gFnSc1	aplikace
napsaná	napsaný	k2eAgFnSc1d1	napsaná
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
Javy	Java	k1gFnSc2	Java
přeloží	přeložit	k5eAaPmIp3nS	přeložit
pomocí	pomocí	k7c2	pomocí
kompilátoru	kompilátor	k1gInSc2	kompilátor
do	do	k7c2	do
Java	Jav	k1gInSc2	Jav
byte	byte	k1gInSc1	byte
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
přeloží	přeložit	k5eAaPmIp3nS	přeložit
do	do	k7c2	do
byte	byte	k1gInSc4	byte
kódu	kód	k1gInSc2	kód
Dalviku	Dalvik	k1gInSc2	Dalvik
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
DEX	DEX	kA	DEX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
instalaci	instalace	k1gFnSc6	instalace
do	do	k7c2	do
Androidu	android	k1gInSc2	android
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
pro	pro	k7c4	pro
vždy	vždy	k6eAd1	vždy
zkompiluje	zkompilovat	k5eAaPmIp3nS	zkompilovat
do	do	k7c2	do
nativního	nativní	k2eAgInSc2d1	nativní
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
procesoru	procesor	k1gInSc2	procesor
<g/>
)	)	kIx)	)
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
<g/>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
výkonnější	výkonný	k2eAgMnSc5d2	výkonnější
smartphone	smartphon	k1gMnSc5	smartphon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
cca	cca	kA	cca
o	o	k7c4	o
36	[number]	k4	36
%	%	kIx~	%
delší	dlouhý	k2eAgFnSc4d2	delší
výdrž	výdrž	k1gFnSc4	výdrž
<g/>
.	.	kIx.	.
</s>
<s>
Zpomalení	zpomalení	k1gNnSc1	zpomalení
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
jen	jen	k9	jen
instalace	instalace	k1gFnSc1	instalace
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
její	její	k3xOp3gFnSc1	její
finální	finální	k2eAgFnSc1d1	finální
optimalizace	optimalizace	k1gFnSc1	optimalizace
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
kompilátor	kompilátor	k1gInSc1	kompilátor
(	(	kIx(	(
<g/>
JIT	jit	k2eAgInSc1d1	jit
–	–	k?	–
Just-in-time	Justnim	k1gInSc5	Just-in-tim
compilation	compilation	k1gInSc1	compilation
<g/>
)	)	kIx)	)
Dalvik	Dalvik	k1gInSc1	Dalvik
prozatím	prozatím	k6eAd1	prozatím
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
6.0	[number]	k4	6.0
(	(	kIx(	(
<g/>
Marshmallow	Marshmallow	k1gFnSc1	Marshmallow
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc2	Google
zřejmě	zřejmě	k6eAd1	zřejmě
přichází	přicházet	k5eAaImIp3nS	přicházet
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
změnou	změna	k1gFnSc7	změna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řízení	řízení	k1gNnSc2	řízení
oprávnění	oprávnění	k1gNnSc2	oprávnění
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
nadcházejícím	nadcházející	k2eAgInSc6d1	nadcházející
systému	systém	k1gInSc6	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
systém	systém	k1gInSc4	systém
Android	android	k1gInSc1	android
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
aplikaci	aplikace	k1gFnSc4	aplikace
buď	buď	k8xC	buď
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
a	a	k8xC	a
spouštět	spouštět	k5eAaImF	spouštět
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
právy	právo	k1gNnPc7	právo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
instalaci	instalace	k1gFnSc4	instalace
aplikace	aplikace	k1gFnSc2	aplikace
zcela	zcela	k6eAd1	zcela
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
mění	měnit	k5eAaImIp3nS	měnit
<g/>
;	;	kIx,	;
ona	onen	k3xDgFnSc1	onen
změna	změna	k1gFnSc1	změna
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
rozšíření	rozšíření	k1gNnSc6	rozšíření
možností	možnost	k1gFnPc2	možnost
uživatele	uživatel	k1gMnSc2	uživatel
Androidu	android	k1gInSc2	android
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
aplikace	aplikace	k1gFnSc1	aplikace
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
skupiny	skupina	k1gFnPc1	skupina
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nezbytně	nezbytně	k6eAd1	nezbytně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
které	který	k3yRgFnPc1	který
jí	on	k3xPp3gFnSc3	on
uživatel	uživatel	k1gMnSc1	uživatel
dovolí	dovolit	k5eAaPmIp3nS	dovolit
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
sice	sice	k8xC	sice
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
dodatečná	dodatečný	k2eAgNnPc4d1	dodatečné
práva	právo	k1gNnPc4	právo
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
běhu	běh	k1gInSc6	běh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
jí	jíst	k5eAaImIp3nS	jíst
uživatel	uživatel	k1gMnSc1	uživatel
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
práva	právo	k1gNnPc4	právo
odepřít	odepřít	k5eAaPmF	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
například	například	k6eAd1	například
zamezit	zamezit	k5eAaPmF	zamezit
aplikaci	aplikace	k1gFnSc4	aplikace
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
mikrofonu	mikrofon	k1gInSc3	mikrofon
<g/>
,	,	kIx,	,
fotoaparátu	fotoaparát	k1gInSc3	fotoaparát
<g/>
,	,	kIx,	,
souborům	soubor	k1gInPc3	soubor
a	a	k8xC	a
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
telefonnímu	telefonní	k2eAgInSc3d1	telefonní
seznamu	seznam	k1gInSc3	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
Androidu	android	k1gInSc2	android
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
propracovanější	propracovaný	k2eAgFnSc4d2	propracovanější
správu	správa	k1gFnSc4	správa
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Application	Application	k1gInSc1	Application
framework	framework	k1gInSc1	framework
===	===	k?	===
</s>
</p>
<p>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
Application	Application	k1gInSc1	Application
framework	framework	k1gInSc1	framework
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
služby	služba	k1gFnPc1	služba
mohou	moct	k5eAaImIp3nP	moct
zpřístupňovat	zpřístupňovat	k5eAaImF	zpřístupňovat
data	datum	k1gNnPc1	datum
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc1	prvek
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
upozorňovací	upozorňovací	k2eAgInSc4d1	upozorňovací
stavový	stavový	k2eAgInSc4d1	stavový
řádek	řádek	k1gInSc4	řádek
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnPc4	aplikace
běžící	běžící	k2eAgFnPc4d1	běžící
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
hardware	hardware	k1gInSc4	hardware
používaného	používaný	k2eAgNnSc2d1	používané
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
sada	sada	k1gFnSc1	sada
služeb	služba	k1gFnPc2	služba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
především	především	k6eAd1	především
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sada	sada	k1gFnSc1	sada
prvků	prvek	k1gInPc2	prvek
View	View	k1gFnSc1	View
–	–	k?	–
Tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
widgety	widgeta	k1gFnPc4	widgeta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
jako	jako	k8xS	jako
seznamy	seznam	k1gInPc4	seznam
<g/>
,	,	kIx,	,
textové	textový	k2eAgNnSc4d1	textové
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
tlačítka	tlačítko	k1gNnPc1	tlačítko
<g/>
,	,	kIx,	,
checkboxy	checkbox	k1gInPc1	checkbox
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Content	Content	k1gInSc1	Content
providers	providers	k1gInSc1	providers
–	–	k?	–
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kontakty	kontakt	k1gInPc4	kontakt
<g/>
)	)	kIx)	)
jiných	jiný	k2eAgFnPc2d1	jiná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Resource	Resourka	k1gFnSc3	Resourka
manager	manager	k1gMnSc1	manager
–	–	k?	–
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přístup	přístup	k1gInSc1	přístup
"	"	kIx"	"
<g/>
nekódovým	kódový	k2eNgInPc3d1	kódový
<g/>
"	"	kIx"	"
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
řetězce	řetězec	k1gInPc1	řetězec
<g/>
,	,	kIx,	,
grafika	grafikon	k1gNnPc1	grafikon
<g/>
,	,	kIx,	,
přidané	přidaný	k2eAgInPc1d1	přidaný
soubory	soubor	k1gInPc1	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Notification	Notification	k1gInSc1	Notification
manager	manager	k1gMnSc1	manager
–	–	k?	–
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
všem	všecek	k3xTgFnPc3	všecek
aplikacím	aplikace	k1gFnPc3	aplikace
zobrazit	zobrazit	k5eAaPmF	zobrazit
vlastní	vlastní	k2eAgNnPc4d1	vlastní
upozornění	upozornění	k1gNnPc4	upozornění
ve	v	k7c6	v
stavovém	stavový	k2eAgInSc6d1	stavový
řádku	řádek	k1gInSc6	řádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Activity	Activita	k1gFnPc1	Activita
manager	manager	k1gMnSc1	manager
–	–	k?	–
Řídí	řídit	k5eAaImIp3nS	řídit
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
zásobníku	zásobník	k1gInSc6	zásobník
s	s	k7c7	s
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
<g/>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
vrstvu	vrstva	k1gFnSc4	vrstva
systému	systém	k1gInSc2	systém
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgFnPc4d1	základní
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
využívají	využívat	k5eAaPmIp3nP	využívat
běžní	běžný	k2eAgMnPc1d1	běžný
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
aplikace	aplikace	k1gFnPc4	aplikace
předinstalované	předinstalovaný	k2eAgFnPc4d1	předinstalovaná
nebo	nebo	k8xC	nebo
dodatečně	dodatečně	k6eAd1	dodatečně
stažené	stažený	k2eAgFnPc1d1	stažená
z	z	k7c2	z
Android	android	k1gInSc1	android
Marketu	market	k1gInSc6	market
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
e-mailový	eailový	k2eAgMnSc1d1	e-mailový
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
SMS	SMS	kA	SMS
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
<g/>
,	,	kIx,	,
kontakty	kontakt	k1gInPc1	kontakt
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
aplikace	aplikace	k1gFnPc1	aplikace
i	i	k9	i
od	od	k7c2	od
"	"	kIx"	"
<g/>
třetích	třetí	k4xOgFnPc2	třetí
<g/>
"	"	kIx"	"
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
verzí	verze	k1gFnPc2	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
několik	několik	k4yIc1	několik
aktualizací	aktualizace	k1gFnPc2	aktualizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
opravují	opravovat	k5eAaImIp3nP	opravovat
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
nové	nový	k2eAgFnPc4d1	nová
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
verze	verze	k1gFnPc1	verze
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
abecedně	abecedně	k6eAd1	abecedně
podle	podle	k7c2	podle
sladkostí	sladkost	k1gFnPc2	sladkost
(	(	kIx(	(
<g/>
Cupcake	Cupcake	k1gFnSc1	Cupcake
<g/>
,	,	kIx,	,
Donut	Donut	k1gMnSc1	Donut
<g/>
,	,	kIx,	,
Eclair	Eclair	k1gMnSc1	Eclair
<g/>
,	,	kIx,	,
Froyo	Froyo	k1gMnSc1	Froyo
<g/>
,	,	kIx,	,
Gingerbread	Gingerbread	k1gInSc1	Gingerbread
<g/>
,	,	kIx,	,
Honeycomb	Honeycomb	k1gInSc1	Honeycomb
<g/>
,	,	kIx,	,
Ice	Ice	k1gFnSc1	Ice
Cream	Cream	k1gInSc1	Cream
Sandwich	Sandwich	k1gInSc1	Sandwich
<g/>
,	,	kIx,	,
Jelly	Jella	k1gFnPc1	Jella
Bean	Bean	k1gInSc1	Bean
<g/>
,	,	kIx,	,
KitKat	KitKat	k1gInSc1	KitKat
<g/>
,	,	kIx,	,
Lollipop	Lollipop	k1gInSc1	Lollipop
<g/>
,	,	kIx,	,
Marshmallow	Marshmallow	k1gFnSc1	Marshmallow
<g/>
,	,	kIx,	,
Nougat	Nougat	k1gInSc1	Nougat
<g/>
,	,	kIx,	,
Oreo	Oreo	k1gNnSc4	Oreo
a	a	k8xC	a
Pie	Pius	k1gMnPc4	Pius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
API	API	kA	API
Vulkan	Vulkany	k1gInPc2	Vulkany
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
knihovna	knihovna	k1gFnSc1	knihovna
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lépe	dobře	k6eAd2	dobře
využít	využít	k5eAaPmF	využít
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
novějších	nový	k2eAgFnPc6d2	novější
architekturách	architektura	k1gFnPc6	architektura
(	(	kIx(	(
<g/>
GCN	GCN	kA	GCN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
API	API	kA	API
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
s	s	k7c7	s
Vulcanem	Vulcan	k1gInSc7	Vulcan
z	z	k7c2	z
desktopu	desktop	k1gInSc2	desktop
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
API	API	kA	API
Vulkan	Vulkan	k1gInSc4	Vulkan
3D	[number]	k4	3D
již	již	k6eAd1	již
využívá	využívat	k5eAaImIp3nS	využívat
Android	android	k1gInSc4	android
7.0	[number]	k4	7.0
Nougat	Nougat	k1gInSc4	Nougat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
podporované	podporovaný	k2eAgNnSc1d1	podporované
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
Android	android	k1gInSc4	android
je	on	k3xPp3gMnPc4	on
Eclipse	Eclips	k1gMnPc4	Eclips
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
ADT	ADT	kA	ADT
plugin	plugin	k1gInSc4	plugin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ulehčuje	ulehčovat	k5eAaImIp3nS	ulehčovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
Android	android	k1gInSc1	android
projektem	projekt	k1gInSc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
volby	volba	k1gFnSc2	volba
jiného	jiný	k2eAgNnSc2d1	jiné
IDE	IDE	kA	IDE
nebo	nebo	k8xC	nebo
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
textového	textový	k2eAgInSc2d1	textový
editoru	editor	k1gInSc2	editor
a	a	k8xC	a
kompilace	kompilace	k1gFnSc2	kompilace
aplikací	aplikace	k1gFnPc2	aplikace
pomocí	pomocí	k7c2	pomocí
příkazové	příkazový	k2eAgFnSc2d1	příkazová
řádky	řádka	k1gFnSc2	řádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Google	Google	k1gFnSc2	Google
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Android	android	k1gInSc1	android
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
prosazovat	prosazovat	k5eAaImF	prosazovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Android	android	k1gInSc1	android
Software	software	k1gInSc1	software
Development	Development	k1gInSc1	Development
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
SDK	SDK	kA	SDK
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
Android	android	k1gInSc1	android
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
SDK	SDK	kA	SDK
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
hlavní	hlavní	k2eAgFnPc4d1	hlavní
platformy	platforma	k1gFnPc4	platforma
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
i	i	k8xC	i
macOS	macOS	k?	macOS
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
SDK	SDK	kA	SDK
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
základní	základní	k2eAgInPc4d1	základní
(	(	kIx(	(
<g/>
nástroje	nástroj	k1gInPc4	nástroj
nezbytné	nezbytný	k2eAgInPc4d1	nezbytný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
a	a	k8xC	a
plná	plný	k2eAgFnSc1d1	plná
konfigurace	konfigurace	k1gFnSc1	konfigurace
vývojového	vývojový	k2eAgNnSc2d1	vývojové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
konfigurace	konfigurace	k1gFnSc1	konfigurace
SDK	SDK	kA	SDK
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
SDK	SDK	kA	SDK
Tools	Tools	k1gInSc1	Tools
–	–	k?	–
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
debugging	debugging	k1gInSc4	debugging
(	(	kIx(	(
<g/>
ddms	ddms	k6eAd1	ddms
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
testování	testování	k1gNnSc1	testování
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
Android	android	k1gInSc1	android
Virtual	Virtual	k1gMnSc1	Virtual
Devices	Devices	k1gMnSc1	Devices
(	(	kIx(	(
<g/>
AVD	AVD	kA	AVD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Android	android	k1gInSc1	android
emulátor	emulátor	k1gInSc4	emulátor
<g/>
,	,	kIx,	,
analýzu	analýza	k1gFnSc4	analýza
grafického	grafický	k2eAgInSc2d1	grafický
layoutu	layout	k1gInSc2	layout
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
potřebné	potřebný	k2eAgInPc4d1	potřebný
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SDK	SDK	kA	SDK
Platform-tool	Platformool	k1gInSc1	Platform-tool
–	–	k?	–
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
další	další	k2eAgInPc4d1	další
důležité	důležitý	k2eAgInPc4d1	důležitý
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
platformy	platforma	k1gFnSc2	platforma
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
aktualizovány	aktualizován	k2eAgInPc1d1	aktualizován
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
SDK	SDK	kA	SDK
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Android	android	k1gInSc1	android
Debug	Debuga	k1gFnPc2	Debuga
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
nahrávat	nahrávat	k5eAaImF	nahrávat
soubory	soubor	k1gInPc4	soubor
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Android	android	k1gInSc1	android
SDK	SDK	kA	SDK
platforms	platforms	k1gInSc1	platforms
–	–	k?	–
Každá	každý	k3xTgFnSc1	každý
platforma	platforma	k1gFnSc1	platforma
SDK	SDK	kA	SDK
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
systémového	systémový	k2eAgInSc2d1	systémový
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
ukázkových	ukázkový	k2eAgInPc2d1	ukázkový
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
skinů	skin	k1gMnPc2	skin
emulátoru	emulátor	k1gInSc2	emulátor
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kompilaci	kompilace	k1gFnSc3	kompilace
aplikace	aplikace	k1gFnSc2	aplikace
a	a	k8xC	a
pro	pro	k7c4	pro
nastavení	nastavení	k1gNnSc4	nastavení
a	a	k8xC	a
běh	běh	k1gInSc1	běh
AVD	AVD	kA	AVD
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
platforma	platforma	k1gFnSc1	platforma
<g/>
.	.	kIx.	.
<g/>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
konfigurace	konfigurace	k1gFnSc1	konfigurace
SDK	SDK	kA	SDK
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
USB	USB	kA	USB
Driver	driver	k1gInSc1	driver
–	–	k?	–
Komponenta	komponenta	k1gFnSc1	komponenta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
a	a	k8xC	a
testování	testování	k1gNnSc6	testování
aplikace	aplikace	k1gFnSc2	aplikace
nainstalované	nainstalovaný	k2eAgFnSc2d1	nainstalovaná
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Potřebné	potřebné	k1gNnSc1	potřebné
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc4	příklad
kódů	kód	k1gInPc2	kód
–	–	k?	–
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ukázkové	ukázkový	k2eAgInPc4d1	ukázkový
kódy	kód	k1gInPc4	kód
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
aktuální	aktuální	k2eAgFnPc1d1	aktuální
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
platformu	platforma	k1gFnSc4	platforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokumentace	dokumentace	k1gFnSc1	dokumentace
–	–	k?	–
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
lokální	lokální	k2eAgFnSc4d1	lokální
kopii	kopie	k1gFnSc4	kopie
dokumentace	dokumentace	k1gFnSc2	dokumentace
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgInSc4d1	aktuální
Android	android	k1gInSc4	android
framework	framework	k1gInSc1	framework
API	API	kA	API
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dokumentace	dokumentace	k1gFnSc1	dokumentace
je	být	k5eAaImIp3nS	být
také	také	k9	také
využita	využít	k5eAaPmNgFnS	využít
ve	v	k7c6	v
vývojovém	vývojový	k2eAgNnSc6d1	vývojové
prostředí	prostředí	k1gNnSc6	prostředí
Eclipse	Eclipse	k1gFnSc2	Eclipse
<g/>
.	.	kIx.	.
<g/>
Plné	plný	k2eAgFnSc2d1	plná
konfigurace	konfigurace	k1gFnSc2	konfigurace
SDK	SDK	kA	SDK
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
API	API	kA	API
–	–	k?	–
Knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
zpřístupňující	zpřístupňující	k2eAgNnSc1d1	zpřístupňující
rozhraní	rozhraní	k1gNnSc1	rozhraní
Google	Google	k1gFnSc2	Google
Maps	Mapsa	k1gFnPc2	Mapsa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatní	ostatní	k1gNnSc1	ostatní
SDK	SDK	kA	SDK
platformy	platforma	k1gFnSc2	platforma
–	–	k?	–
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Market	market	k1gInSc1	market
Licensing	Licensing	k1gInSc1	Licensing
package	package	k1gInSc4	package
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
knihovnu	knihovna	k1gFnSc4	knihovna
ověřující	ověřující	k2eAgFnSc4d1	ověřující
licenci	licence	k1gFnSc4	licence
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
nelegální	legální	k2eNgFnSc4d1	nelegální
kopii	kopie	k1gFnSc4	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emulátor	emulátor	k1gInSc4	emulátor
===	===	k?	===
</s>
</p>
<p>
<s>
Emulátor	emulátor	k1gInSc1	emulátor
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c4	v
Android	android	k1gInSc4	android
SDK	SDK	kA	SDK
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
testovat	testovat	k5eAaImF	testovat
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
aplikace	aplikace	k1gFnPc4	aplikace
bez	bez	k7c2	bez
fyzického	fyzický	k2eAgNnSc2d1	fyzické
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Android	android	k1gInSc4	android
SDK	SDK	kA	SDK
a	a	k8xC	a
AVD	AVD	kA	AVD
Manageru	manager	k1gMnSc6	manager
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
konfigurovat	konfigurovat	k5eAaBmF	konfigurovat
volbu	volba	k1gFnSc4	volba
síťového	síťový	k2eAgNnSc2d1	síťové
připojení	připojení	k1gNnSc2	připojení
<g/>
,	,	kIx,	,
SD	SD	kA	SD
karty	karta	k1gFnSc2	karta
atd.	atd.	kA	atd.
a	a	k8xC	a
spouštět	spouštět	k5eAaImF	spouštět
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
virtuální	virtuální	k2eAgNnPc4d1	virtuální
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
aplikací	aplikace	k1gFnPc2	aplikace
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
emulátoru	emulátor	k1gInSc6	emulátor
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c6	na
fyzickém	fyzický	k2eAgNnSc6d1	fyzické
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
situace	situace	k1gFnPc1	situace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
virtualizovat	virtualizovat	k5eAaImF	virtualizovat
nedají	dát	k5eNaPmIp3nP	dát
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přijímání	přijímání	k1gNnSc1	přijímání
hovorů	hovor	k1gInPc2	hovor
<g/>
,	,	kIx,	,
video	video	k1gNnSc1	video
<g/>
/	/	kIx~	/
<g/>
audio	audio	k2eAgInSc1d1	audio
vstup	vstup	k1gInSc1	vstup
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
nabité	nabitý	k2eAgFnPc1d1	nabitá
baterie	baterie	k1gFnPc1	baterie
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
bluetooth	bluetooth	k1gMnSc1	bluetooth
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
aplikace	aplikace	k1gFnSc2	aplikace
Android	android	k1gInSc1	android
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
stavební	stavební	k2eAgInPc1d1	stavební
kameny	kámen	k1gInPc1	kámen
v	v	k7c6	v
aplikacích	aplikace	k1gFnPc6	aplikace
Android	android	k1gInSc4	android
jsou	být	k5eAaImIp3nP	být
komponenty	komponenta	k1gFnPc1	komponenta
activity	activita	k1gFnSc2	activita
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
,	,	kIx,	,
service	service	k1gFnSc1	service
umožňující	umožňující	k2eAgFnSc1d1	umožňující
provádět	provádět	k5eAaImF	provádět
akce	akce	k1gFnPc4	akce
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
,	,	kIx,	,
content	content	k1gInSc4	content
providers	providers	k6eAd1	providers
poskytující	poskytující	k2eAgInSc4d1	poskytující
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
a	a	k8xC	a
broadcast	broadcast	k1gInSc1	broadcast
reciever	recievra	k1gFnPc2	recievra
reagující	reagující	k2eAgMnSc1d1	reagující
na	na	k7c4	na
příchozí	příchozí	k1gMnPc4	příchozí
oznámení	oznámení	k1gNnSc2	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
komponenty	komponenta	k1gFnPc1	komponenta
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
definovány	definovat	k5eAaBmNgInP	definovat
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
AndroidManifest	AndroidManifest	k1gFnSc1	AndroidManifest
<g/>
.	.	kIx.	.
<g/>
xml	xml	k?	xml
<g/>
,	,	kIx,	,
uloženém	uložený	k2eAgInSc6d1	uložený
v	v	k7c6	v
kořenovém	kořenový	k2eAgInSc6d1	kořenový
adresáři	adresář	k1gInSc6	adresář
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
content	content	k1gInSc4	content
provideru	providera	k1gFnSc4	providera
mohou	moct	k5eAaImIp3nP	moct
komponenty	komponent	k1gInPc4	komponent
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
kooperovat	kooperovat	k5eAaImF	kooperovat
pomocí	pomoc	k1gFnPc2	pomoc
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
intentů	intent	k1gInPc2	intent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Activity	Activit	k1gInPc4	Activit
===	===	k?	===
</s>
</p>
<p>
<s>
Activity	Activita	k1gFnPc4	Activita
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedné	jeden	k4xCgFnSc6	jeden
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
pro	pro	k7c4	pro
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obvykle	obvykle	k6eAd1	obvykle
více	hodně	k6eAd2	hodně
activit	activit	k5eAaPmF	activit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgMnPc7	který
je	být	k5eAaImIp3nS	být
uživatel	uživatel	k1gMnSc1	uživatel
schopen	schopen	k2eAgMnSc1d1	schopen
přepínat	přepínat	k5eAaImF	přepínat
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
activity	activita	k1gFnPc4	activita
může	moct	k5eAaImIp3nS	moct
předávat	předávat	k5eAaImF	předávat
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
activity	activita	k1gFnSc2	activita
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
náročná	náročný	k2eAgFnSc1d1	náročná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
alokovat	alokovat	k5eAaImF	alokovat
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
rozhraní	rozhraní	k1gNnPc2	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nP	rozložit
do	do	k7c2	do
layoutu	layout	k1gInSc2	layout
obrazovky	obrazovka	k1gFnSc2	obrazovka
a	a	k8xC	a
na	na	k7c4	na
připravenou	připravený	k2eAgFnSc4d1	připravená
obrazovku	obrazovka	k1gFnSc4	obrazovka
vyvolat	vyvolat	k5eAaPmF	vyvolat
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
zbytečnému	zbytečný	k2eAgNnSc3d1	zbytečné
plýtvání	plýtvání	k1gNnSc3	plýtvání
výpočetních	výpočetní	k2eAgInPc2d1	výpočetní
prostředků	prostředek	k1gInPc2	prostředek
např.	např.	kA	např.
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
zániku	zánik	k1gInSc2	zánik
a	a	k8xC	a
opětovného	opětovný	k2eAgInSc2d1	opětovný
vzniku	vznik	k1gInSc2	vznik
activity	activita	k1gFnSc2	activita
–	–	k?	–
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jednoduše	jednoduše	k6eAd1	jednoduše
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
při	při	k7c6	při
stisku	stisk	k1gInSc6	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
zařízení	zařízení	k1gNnSc6	zařízení
–	–	k?	–
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Activity	Activita	k1gFnPc1	Activita
Manager	manager	k1gMnSc1	manager
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
za	za	k7c4	za
vytváření	vytváření	k1gNnSc4	vytváření
<g/>
,	,	kIx,	,
rušení	rušení	k1gNnSc4	rušení
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
správu	správa	k1gFnSc4	správa
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
activity	activita	k1gFnSc2	activita
<g/>
.	.	kIx.	.
</s>
<s>
Activity	Activita	k1gFnSc2	Activita
Manager	manager	k1gMnSc1	manager
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
zásobníkem	zásobník	k1gInSc7	zásobník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
uchované	uchovaný	k2eAgFnPc4d1	uchovaná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
spuštěných	spuštěný	k2eAgFnPc6d1	spuštěná
activitách	activita	k1gFnPc6	activita
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
tohoto	tento	k3xDgInSc2	tento
zásobníku	zásobník	k1gInSc2	zásobník
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
zobrazované	zobrazovaný	k2eAgFnSc2d1	zobrazovaná
activity	activita	k1gFnSc2	activita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
activity	activita	k1gFnSc2	activita
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
stavech	stav	k1gInPc6	stav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Activity	Activita	k1gFnPc1	Activita
starts	starts	k1gInSc1	starts
–	–	k?	–
Počátek	počátek	k1gInSc1	počátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gInPc4	on
actvity	actvit	k1gInPc4	actvit
inicializováno	inicializován	k2eAgNnSc1d1	inicializován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Activity	Activit	k1gInPc1	Activit
is	is	k?	is
running	running	k1gInSc1	running
–	–	k?	–
Activity	Activita	k1gFnSc2	Activita
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
okamžiku	okamžik	k1gInSc6	okamžik
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
právě	právě	k9	právě
jedna	jeden	k4xCgFnSc1	jeden
activity	activita	k1gFnPc1	activita
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Process	Process	k1gInSc1	Process
is	is	k?	is
killed	killed	k1gInSc1	killed
–	–	k?	–
Activity	Activita	k1gFnSc2	Activita
Manager	manager	k1gMnSc1	manager
zrušil	zrušit	k5eAaPmAgMnS	zrušit
activity	activit	k1gInPc4	activit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
activity	activita	k1gFnPc1	activita
není	být	k5eNaImIp3nS	být
viditelné	viditelný	k2eAgNnSc1d1	viditelné
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možnost	možnost	k1gFnSc1	možnost
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
–	–	k?	–
activity	activit	k1gInPc4	activit
je	být	k5eAaImIp3nS	být
viditelné	viditelný	k2eAgNnSc1d1	viditelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uživatel	uživatel	k1gMnSc1	uživatel
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nemůže	moct	k5eNaImIp3nS	moct
navázat	navázat	k5eAaPmF	navázat
interakci	interakce	k1gFnSc4	interakce
(	(	kIx(	(
<g/>
nastává	nastávat	k5eAaImIp3nS	nastávat
například	například	k6eAd1	například
při	při	k7c6	při
dialogových	dialogový	k2eAgFnPc6d1	dialogová
hláškách	hláška	k1gFnPc6	hláška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Activity	Activita	k1gFnPc1	Activita
is	is	k?	is
shut	shut	k1gMnSc1	shut
down	down	k1gMnSc1	down
–	–	k?	–
Activity	Activita	k1gFnSc2	Activita
Managerem	manager	k1gMnSc7	manager
ukončil	ukončit	k5eAaPmAgMnS	ukončit
activity	activita	k1gFnSc2	activita
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
již	již	k6eAd1	již
nevyužívá	využívat	k5eNaImIp3nS	využívat
žádnou	žádný	k3yNgFnSc4	žádný
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Service	Service	k1gFnSc2	Service
===	===	k?	===
</s>
</p>
<p>
<s>
Komponenta	komponenta	k1gFnSc1	komponenta
service	service	k1gFnSc2	service
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
proces	proces	k1gInSc4	proces
běžící	běžící	k2eAgInSc4d1	běžící
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vykonávání	vykonávání	k1gNnSc3	vykonávání
dlouho	dlouho	k6eAd1	dlouho
trvajících	trvající	k2eAgInPc2d1	trvající
úkolů	úkol	k1gInPc2	úkol
nebo	nebo	k8xC	nebo
k	k	k7c3	k
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
vzdáleným	vzdálený	k2eAgInPc3d1	vzdálený
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
serveru	server	k1gInSc3	server
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Service	Service	k1gFnPc1	Service
můžeme	moct	k5eAaImIp1nP	moct
spustit	spustit	k5eAaPmF	spustit
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
startService	startService	k1gFnSc2	startService
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
service	service	k1gFnSc1	service
může	moct	k5eAaImIp3nS	moct
ukončit	ukončit	k5eAaPmF	ukončit
sama	sám	k3xTgFnSc1	sám
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
ukončit	ukončit	k5eAaPmF	ukončit
jiná	jiný	k2eAgFnSc1d1	jiná
komponenta	komponenta	k1gFnSc1	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
spuštění	spuštění	k1gNnSc2	spuštění
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
bindService	bindService	k1gFnSc2	bindService
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
jiná	jiný	k2eAgFnSc1d1	jiná
komponenta	komponenta	k1gFnSc1	komponenta
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
service	service	k1gFnSc2	service
může	moct	k5eAaImIp3nS	moct
ukončit	ukončit	k5eAaPmF	ukončit
pouze	pouze	k6eAd1	pouze
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
spustil	spustit	k5eAaPmAgInS	spustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
k	k	k7c3	k
service	service	k1gFnSc2	service
navázáno	navázat	k5eAaPmNgNnS	navázat
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
bindService	bindService	k1gFnSc2	bindService
i	i	k9	i
více	hodně	k6eAd2	hodně
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
service	service	k1gFnSc1	service
ukončena	ukončit	k5eAaPmNgFnS	ukončit
po	po	k7c4	po
odpojení	odpojení	k1gNnSc4	odpojení
všech	všecek	k3xTgMnPc2	všecek
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Service	Service	k1gFnSc1	Service
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stavech	stav	k1gInPc6	stav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Component	Component	k1gInSc1	Component
calls	calls	k1gInSc1	calls
–	–	k?	–
Inicializace	inicializace	k1gFnSc1	inicializace
service	service	k1gFnSc1	service
pouhým	pouhý	k2eAgNnSc7d1	pouhé
zavoláním	zavolání	k1gNnSc7	zavolání
nebo	nebo	k8xC	nebo
navázáním	navázání	k1gNnSc7	navázání
komponenty	komponenta	k1gFnSc2	komponenta
na	na	k7c4	na
service	service	k1gFnPc4	service
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Service	Service	k1gFnSc1	Service
is	is	k?	is
running	running	k1gInSc1	running
–	–	k?	–
Service	Service	k1gFnSc1	Service
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Service	Service	k1gFnSc1	Service
is	is	k?	is
shut	shutum	k1gNnPc2	shutum
down	downo	k1gNnPc2	downo
–	–	k?	–
Service	Service	k1gFnSc2	Service
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
sám	sám	k3xTgInSc1	sám
nebo	nebo	k8xC	nebo
komponentou	komponenta	k1gFnSc7	komponenta
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
formě	forma	k1gFnSc6	forma
spuštění	spuštění	k1gNnSc6	spuštění
service	service	k1gFnSc2	service
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Content	Content	k1gMnSc1	Content
provider	provider	k1gMnSc1	provider
===	===	k?	===
</s>
</p>
<p>
<s>
Content	Content	k1gInSc1	Content
provider	providra	k1gFnPc2	providra
je	být	k5eAaImIp3nS	být
aplikační	aplikační	k2eAgNnSc1d1	aplikační
rozhraní	rozhraní	k1gNnSc1	rozhraní
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
activitami	activita	k1gFnPc7	activita
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
může	moct	k5eAaImIp3nS	moct
uchovávat	uchovávat	k5eAaImF	uchovávat
data	datum	k1gNnPc4	datum
v	v	k7c6	v
souborech	soubor	k1gInPc6	soubor
<g/>
,	,	kIx,	,
SQLite	SQLit	k1gInSc5	SQLit
databázi	databáze	k1gFnSc6	databáze
nebo	nebo	k8xC	nebo
na	na	k7c6	na
webu	web	k1gInSc6	web
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
datům	datum	k1gNnPc3	datum
přístup	přístup	k1gInSc4	přístup
–	–	k?	–
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
povoleno	povolen	k2eAgNnSc4d1	povoleno
–	–	k?	–
jiné	jiný	k2eAgFnSc2d1	jiná
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Content	Content	k1gMnSc1	Content
provider	provider	k1gMnSc1	provider
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
rozhraní	rozhraní	k1gNnSc1	rozhraní
se	s	k7c7	s
standardními	standardní	k2eAgFnPc7d1	standardní
metodami	metoda	k1gFnPc7	metoda
(	(	kIx(	(
<g/>
insert	insert	k1gInSc1	insert
<g/>
,	,	kIx,	,
update	update	k1gInSc1	update
<g/>
,	,	kIx,	,
delete	dele	k1gNnSc2	dele
a	a	k8xC	a
query	quera	k1gFnSc2	quera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
klasické	klasický	k2eAgFnPc4d1	klasická
databázové	databázový	k2eAgFnPc4d1	databázová
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
dat	datum	k1gNnPc2	datum
od	od	k7c2	od
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc1	možnost
nahrazení	nahrazení	k1gNnSc2	nahrazení
výchozích	výchozí	k2eAgFnPc2d1	výchozí
aplikací	aplikace	k1gFnPc2	aplikace
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
může	moct	k5eAaImIp3nS	moct
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
aplikace	aplikace	k1gFnSc2	aplikace
využít	využít	k5eAaPmF	využít
uložených	uložený	k2eAgInPc2d1	uložený
uživatelských	uživatelský	k2eAgInPc2d1	uživatelský
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
tak	tak	k6eAd1	tak
defaultní	defaultní	k2eAgFnSc4d1	defaultní
aplikaci	aplikace	k1gFnSc4	aplikace
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
zobrazování	zobrazování	k1gNnSc4	zobrazování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Broadcast	Broadcast	k1gMnSc1	Broadcast
receiver	receiver	k1gMnSc1	receiver
===	===	k?	===
</s>
</p>
<p>
<s>
Broadcast	Broadcast	k1gFnSc1	Broadcast
reciever	recievra	k1gFnPc2	recievra
je	být	k5eAaImIp3nS	být
komponenta	komponenta	k1gFnSc1	komponenta
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
"	"	kIx"	"
<g/>
naslouchání	naslouchání	k1gNnSc3	naslouchání
<g/>
"	"	kIx"	"
oznámení	oznámení	k1gNnSc3	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
určení	určení	k1gNnSc2	určení
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
reaguje	reagovat	k5eAaBmIp3nS	reagovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výpisem	výpis	k1gInSc7	výpis
na	na	k7c4	na
stavový	stavový	k2eAgInSc4d1	stavový
řádek	řádek	k1gInSc4	řádek
nebo	nebo	k8xC	nebo
spuštěním	spuštění	k1gNnSc7	spuštění
jiné	jiný	k2eAgFnSc2d1	jiná
komponenty	komponenta	k1gFnSc2	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaImF	využívat
broadcasty	broadcast	k1gMnPc4	broadcast
systémové	systémový	k2eAgMnPc4d1	systémový
nebo	nebo	k8xC	nebo
vytvářet	vytvářet	k5eAaImF	vytvářet
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
service	service	k1gFnSc1	service
ani	ani	k8xC	ani
broadcast	broadcast	k1gFnSc1	broadcast
receiver	receivra	k1gFnPc2	receivra
nemá	mít	k5eNaImIp3nS	mít
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc2	použití
broadcast	broadcast	k1gInSc1	broadcast
recieveru	reciever	k1gInSc2	reciever
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
oznámení	oznámení	k1gNnSc4	oznámení
o	o	k7c6	o
nízkém	nízký	k2eAgInSc6d1	nízký
stavu	stav	k1gInSc6	stav
baterie	baterie	k1gFnSc2	baterie
<g/>
,	,	kIx,	,
o	o	k7c4	o
zachycení	zachycení	k1gNnSc4	zachycení
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
doručení	doručení	k1gNnSc2	doručení
SMS	SMS	kA	SMS
zprávy	zpráva	k1gFnSc2	zpráva
nebo	nebo	k8xC	nebo
stažení	stažení	k1gNnSc4	stažení
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Android	android	k1gInSc1	android
Developer	developer	k1gMnSc1	developer
Challenge	Challenge	k1gInSc1	Challenge
==	==	k?	==
</s>
</p>
<p>
<s>
Aby	aby	k9	aby
společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc1	Google
přilákala	přilákat	k5eAaPmAgFnS	přilákat
vývojáře	vývojář	k1gMnSc4	vývojář
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
novou	nový	k2eAgFnSc4d1	nová
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
Google	Google	k1gFnSc2	Google
Developer	developer	k1gMnSc1	developer
Challenge	Challenge	k1gNnSc1	Challenge
vložila	vložit	k5eAaPmAgFnS	vložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
celých	celý	k2eAgInPc2d1	celý
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Android	android	k1gInSc1	android
Developer	developer	k1gMnSc1	developer
Challenge	Challenge	k1gFnPc2	Challenge
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
vývojářů	vývojář	k1gMnPc2	vývojář
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
společností	společnost	k1gFnPc2	společnost
Google	Google	k1gFnPc4	Google
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
přihlašují	přihlašovat	k5eAaImIp3nP	přihlašovat
skupiny	skupina	k1gFnPc1	skupina
vývojářů	vývojář	k1gMnPc2	vývojář
či	či	k8xC	či
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
své	svůj	k3xOyFgFnPc4	svůj
aplikace	aplikace	k1gFnPc4	aplikace
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
Android	android	k1gInSc4	android
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
stanovených	stanovený	k2eAgNnPc2d1	stanovené
pravidel	pravidlo	k1gNnPc2	pravidlo
podělí	podělit	k5eAaPmIp3nS	podělit
o	o	k7c4	o
finanční	finanční	k2eAgFnPc4d1	finanční
odměny	odměna	k1gFnPc4	odměna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mohou	moct	k5eAaImIp3nP	moct
financovat	financovat	k5eAaBmF	financovat
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
lepší	dobrý	k2eAgFnSc4d2	lepší
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
kolech	kolo	k1gNnPc6	kolo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
užší	úzký	k2eAgInPc1d2	užší
výběr	výběr	k1gInSc1	výběr
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
řádově	řádově	k6eAd1	řádově
hodnotnější	hodnotný	k2eAgFnPc4d2	hodnotnější
finanční	finanční	k2eAgFnPc4d1	finanční
odměny	odměna	k1gFnPc4	odměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
výběrem	výběr	k1gInSc7	výběr
50	[number]	k4	50
projektů	projekt	k1gInPc2	projekt
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
odborné	odborný	k2eAgFnSc2d1	odborná
poroty	porota	k1gFnSc2	porota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obdržely	obdržet	k5eAaPmAgFnP	obdržet
každý	každý	k3xTgInSc1	každý
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
10	[number]	k4	10
vítězů	vítěz	k1gMnPc2	vítěz
(	(	kIx(	(
<g/>
každý	každý	k3xTgInSc4	každý
vítězný	vítězný	k2eAgInSc4d1	vítězný
projekt	projekt	k1gInSc4	projekt
dostal	dostat	k5eAaPmAgMnS	dostat
275	[number]	k4	275
tisíc	tisíc	k4xCgInSc4	tisíc
$	$	kIx~	$
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
deset	deset	k4xCc4	deset
týmů	tým	k1gInPc2	tým
dostalo	dostat	k5eAaPmAgNnS	dostat
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc4d1	podobné
druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Google	Googl	k1gInSc5	Googl
Play	play	k0	play
a	a	k8xC	a
instalace	instalace	k1gFnSc1	instalace
aplikací	aplikace	k1gFnPc2	aplikace
obecně	obecně	k6eAd1	obecně
==	==	k?	==
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Android	android	k1gInSc1	android
jsou	být	k5eAaImIp3nP	být
primárně	primárně	k6eAd1	primárně
dostupné	dostupný	k2eAgInPc4d1	dostupný
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
Google	Google	k1gInSc4	Google
Play	play	k0	play
(	(	kIx(	(
<g/>
do	do	k7c2	do
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
Android	android	k1gInSc1	android
Market	market	k1gInSc4	market
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
stahování	stahování	k1gNnSc4	stahování
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
přímo	přímo	k6eAd1	přímo
Google	Google	k1gInSc4	Google
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
již	již	k6eAd1	již
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
prvního	první	k4xOgInSc2	první
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
290	[number]	k4	290
000	[number]	k4	000
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
aplikace	aplikace	k1gFnPc1	aplikace
jsou	být	k5eAaImIp3nP	být
placené	placený	k2eAgInPc4d1	placený
–	–	k?	–
ty	ten	k3xDgInPc4	ten
lze	lze	k6eAd1	lze
zatím	zatím	k6eAd1	zatím
kupovat	kupovat	k5eAaImF	kupovat
jen	jen	k9	jen
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
množině	množina	k1gFnSc6	množina
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
omezení	omezení	k1gNnSc4	omezení
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
placené	placený	k2eAgFnPc4d1	placená
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
Android	android	k1gInSc1	android
Market	market	k1gInSc4	market
rovněž	rovněž	k9	rovněž
zpřístupněn	zpřístupněn	k2eAgInSc4d1	zpřístupněn
přes	přes	k7c4	přes
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
ze	z	k7c2	z
svého	své	k1gNnSc2	své
PC	PC	kA	PC
procházet	procházet	k5eAaImF	procházet
seznamy	seznam	k1gInPc4	seznam
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
rovněž	rovněž	k6eAd1	rovněž
si	se	k3xPyFc3	se
vybírat	vybírat	k5eAaImF	vybírat
ty	ten	k3xDgFnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
nainstalují	nainstalovat	k5eAaPmIp3nP	nainstalovat
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
tato	tento	k3xDgFnSc1	tento
funkcionalita	funkcionalita	k1gFnSc1	funkcionalita
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
mobilního	mobilní	k2eAgNnSc2d1	mobilní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
oficiálního	oficiální	k2eAgInSc2d1	oficiální
repozitáře	repozitář	k1gInSc2	repozitář
aplikací	aplikace	k1gFnPc2	aplikace
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
konkurenční	konkurenční	k2eAgInPc1d1	konkurenční
a	a	k8xC	a
neoficiální	neoficiální	k2eAgInPc1d1	neoficiální
repozitáře	repozitář	k1gInPc1	repozitář
–	–	k?	–
např.	např.	kA	např.
Amazon	amazona	k1gFnPc2	amazona
Appstore	Appstor	k1gMnSc5	Appstor
nebo	nebo	k8xC	nebo
F-Droid	F-Droid	k1gInSc1	F-Droid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
svobodné	svobodný	k2eAgFnPc4d1	svobodná
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
však	však	k9	však
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
platformou	platforma	k1gFnSc7	platforma
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnPc1	aplikace
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nahrávat	nahrávat	k5eAaImF	nahrávat
i	i	k9	i
přímo	přímo	k6eAd1	přímo
např.	např.	kA	např.
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
postupy	postup	k1gInPc1	postup
jsou	být	k5eAaImIp3nP	být
zdokumentované	zdokumentovaný	k2eAgInPc1d1	zdokumentovaný
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
je	být	k5eAaImIp3nS	být
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
Android	android	k1gInSc4	android
SDK	SDK	kA	SDK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
modifikovat	modifikovat	k5eAaBmF	modifikovat
i	i	k9	i
samotné	samotný	k2eAgFnPc4d1	samotná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
postupy	postup	k1gInPc1	postup
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
získat	získat	k5eAaPmF	získat
oprávnění	oprávnění	k1gNnSc4	oprávnění
administrátora	administrátor	k1gMnSc2	administrátor
v	v	k7c6	v
zařízení	zařízení	k1gNnSc6	zařízení
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
root	roota	k1gFnPc2	roota
[	[	kIx(	[
<g/>
ruː	ruː	k?	ruː
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
těmto	tento	k3xDgInPc3	tento
postupům	postup	k1gInPc3	postup
říká	říkat	k5eAaImIp3nS	říkat
rooting	rooting	k1gInSc1	rooting
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgFnPc1	takový
změny	změna	k1gFnPc1	změna
nejsou	být	k5eNaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
podporované	podporovaný	k2eAgFnPc1d1	podporovaná
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
porušit	porušit	k5eAaPmF	porušit
některá	některý	k3yIgNnPc4	některý
obchodní	obchodní	k2eAgNnPc4d1	obchodní
ustanovení	ustanovení	k1gNnPc4	ustanovení
a	a	k8xC	a
vést	vést	k5eAaImF	vést
např.	např.	kA	např.
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
záruky	záruka	k1gFnSc2	záruka
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zastoupení	zastoupení	k1gNnPc4	zastoupení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Canalys	Canalysa	k1gFnPc2	Canalysa
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
2009	[number]	k4	2009
měl	mít	k5eAaImAgInS	mít
Android	android	k1gInSc1	android
2,9	[number]	k4	2,9
%	%	kIx~	%
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
tzv.	tzv.	kA	tzv.
chytrých	chytrá	k1gFnPc2	chytrá
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
již	již	k6eAd1	již
ale	ale	k8xC	ale
Android	android	k1gInSc1	android
zřejmě	zřejmě	k6eAd1	zřejmě
předběhl	předběhnout	k5eAaPmAgInS	předběhnout
v	v	k7c6	v
USA	USA	kA	USA
iPhone	iPhon	k1gInSc5	iPhon
co	co	k3yInSc4	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
telefonů	telefon	k1gInPc2	telefon
svým	svůj	k3xOyFgInSc7	svůj
podílem	podíl	k1gInSc7	podíl
28	[number]	k4	28
%	%	kIx~	%
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
Google	Google	k1gFnSc2	Google
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
aktivováno	aktivován	k2eAgNnSc1d1	aktivováno
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
550	[number]	k4	550
tisíc	tisíc	k4xCgInPc2	tisíc
zařízení	zařízení	k1gNnPc2	zařízení
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
android	android	k1gInSc4	android
na	na	k7c4	na
48	[number]	k4	48
%	%	kIx~	%
prodaných	prodaný	k2eAgInPc2d1	prodaný
mobilů	mobil	k1gInPc2	mobil
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
už	už	k9	už
Android	android	k1gInSc1	android
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dominoval	dominovat	k5eAaImAgInS	dominovat
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
okolo	okolo	k7c2	okolo
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
firemní	firemní	k2eAgNnSc1d1	firemní
nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
webu	web	k1gInSc2	web
Gizmodo	Gizmoda	k1gFnSc5	Gizmoda
nasazení	nasazení	k1gNnSc1	nasazení
přístrojů	přístroj	k1gInPc2	přístroj
s	s	k7c7	s
Androidem	android	k1gInSc7	android
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
postupně	postupně	k6eAd1	postupně
zpomalovalo	zpomalovat	k5eAaImAgNnS	zpomalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vině	vina	k1gFnSc6	vina
jsou	být	k5eAaImIp3nP	být
chybějící	chybějící	k2eAgFnPc4d1	chybějící
funkce	funkce	k1gFnPc4	funkce
pro	pro	k7c4	pro
podnikové	podnikový	k2eAgNnSc4d1	podnikové
nasazení	nasazení	k1gNnSc4	nasazení
spojené	spojený	k2eAgNnSc4d1	spojené
zejména	zejména	k6eAd1	zejména
s	s	k7c7	s
dostatečným	dostatečný	k2eAgNnSc7d1	dostatečné
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
a	a	k8xC	a
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
Androidu	android	k1gInSc2	android
chybí	chybět	k5eAaImIp3nS	chybět
zejména	zejména	k9	zejména
podpora	podpora	k1gFnSc1	podpora
šifrování	šifrování	k1gNnSc2	šifrování
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
enkrypce	enkrypce	k1gFnSc2	enkrypce
SD	SD	kA	SD
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
také	také	k9	také
lepší	dobrý	k2eAgInSc4d2	lepší
systém	systém	k1gInSc4	systém
ochrany	ochrana	k1gFnSc2	ochrana
proti	proti	k7c3	proti
malwaru	malwar	k1gInSc3	malwar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
na	na	k7c4	na
přístroje	přístroj	k1gInPc4	přístroj
s	s	k7c7	s
Androidem	android	k1gInSc7	android
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
OS	OS	kA	OS
Android	android	k1gInSc1	android
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
snadný	snadný	k2eAgInSc1d1	snadný
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
vymazávání	vymazávání	k1gNnSc2	vymazávání
dat	datum	k1gNnPc2	datum
administrátory	administrátor	k1gMnPc7	administrátor
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
a	a	k8xC	a
také	také	k9	také
pokročilou	pokročilý	k2eAgFnSc4d1	pokročilá
schopnost	schopnost	k1gFnSc4	schopnost
sledování	sledování	k1gNnSc2	sledování
přístroje	přístroj	k1gInSc2	přístroj
při	při	k7c6	při
ztrátě	ztráta	k1gFnSc6	ztráta
či	či	k8xC	či
odcizení	odcizení	k1gNnSc6	odcizení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Androidu	android	k1gInSc3	android
příliš	příliš	k6eAd1	příliš
neprospěla	prospět	k5eNaPmAgFnS	prospět
ani	ani	k8xC	ani
šokující	šokující	k2eAgFnSc4d1	šokující
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
upozornil	upozornit	k5eAaPmAgInS	upozornit
web	web	k1gInSc1	web
The	The	k1gFnSc4	The
Register	registrum	k1gNnPc2	registrum
<g/>
,	,	kIx,	,
že	že	k8xS	že
totiž	totiž	k9	totiž
99	[number]	k4	99
%	%	kIx~	%
mobilů	mobil	k1gInPc2	mobil
používajících	používající	k2eAgInPc2d1	používající
platformu	platforma	k1gFnSc4	platforma
Android	android	k1gInSc1	android
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chyby	chyba	k1gFnPc4	chyba
dovolující	dovolující	k2eAgFnPc4d1	dovolující
například	například	k6eAd1	například
reklamním	reklamní	k2eAgFnPc3d1	reklamní
společnostem	společnost	k1gFnPc3	společnost
krádež	krádež	k1gFnSc4	krádež
přihlašovacích	přihlašovací	k2eAgInPc2d1	přihlašovací
údajů	údaj	k1gInPc2	údaj
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
kalendářům	kalendář	k1gInPc3	kalendář
<g/>
,	,	kIx,	,
kontaktům	kontakt	k1gInPc3	kontakt
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
citlivým	citlivý	k2eAgInPc3d1	citlivý
údajům	údaj	k1gInPc3	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
uživatel	uživatel	k1gMnSc1	uživatel
odešle	odeslat	k5eAaPmIp3nS	odeslat
přihlašovací	přihlašovací	k2eAgInPc1d1	přihlašovací
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
webové	webový	k2eAgFnSc3d1	webová
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
zpátky	zpátky	k6eAd1	zpátky
tzv.	tzv.	kA	tzv.
přihlašovací	přihlašovací	k2eAgInSc4d1	přihlašovací
token	token	k1gInSc4	token
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
si	se	k3xPyFc3	se
webová	webový	k2eAgFnSc1d1	webová
služba	služba	k1gFnSc1	služba
nadále	nadále	k6eAd1	nadále
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
identitu	identita	k1gFnSc4	identita
uživatele	uživatel	k1gMnSc2	uživatel
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
znovu	znovu	k6eAd1	znovu
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
heslo	heslo	k1gNnSc4	heslo
a	a	k8xC	a
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možné	možný	k2eAgNnSc1d1	možné
k	k	k7c3	k
přihlášení	přihlášení	k1gNnSc3	přihlášení
použít	použít	k5eAaPmF	použít
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
token	token	k1gInSc1	token
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
odesílán	odesílán	k2eAgInSc1d1	odesílán
nešifrovaný	šifrovaný	k2eNgInSc1d1	nešifrovaný
a	a	k8xC	a
tak	tak	k9	tak
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jej	on	k3xPp3gMnSc4	on
získá	získat	k5eAaPmIp3nS	získat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jednoduše	jednoduše	k6eAd1	jednoduše
webovou	webový	k2eAgFnSc4d1	webová
službu	služba	k1gFnSc4	služba
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
k	k	k7c3	k
přístupu	přístup	k1gInSc3	přístup
na	na	k7c4	na
účet	účet	k1gInSc4	účet
daného	daný	k2eAgMnSc2d1	daný
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
díra	díra	k1gFnSc1	díra
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
opravena	opravit	k5eAaPmNgFnS	opravit
v	v	k7c6	v
Androidu	android	k1gInSc6	android
verze	verze	k1gFnSc2	verze
2.3	[number]	k4	2.3
<g/>
.4	.4	k4	.4
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
stále	stále	k6eAd1	stále
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
podobný	podobný	k2eAgInSc4d1	podobný
útok	útok	k1gInSc4	útok
například	například	k6eAd1	například
při	při	k7c6	při
synchronizaci	synchronizace	k1gFnSc6	synchronizace
se	s	k7c7	s
službou	služba	k1gFnSc7	služba
Picasa	Picasa	k1gFnSc1	Picasa
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
webu	web	k1gInSc2	web
ICT	ICT	kA	ICT
manažer	manažer	k1gMnSc1	manažer
pak	pak	k6eAd1	pak
Google	Google	k1gFnSc2	Google
prý	prý	k9	prý
není	být	k5eNaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
sám	sám	k3xTgInSc1	sám
Android	android	k1gInSc1	android
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c4	v
platformu	platforma	k1gFnSc4	platforma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
požadavkům	požadavek	k1gInPc3	požadavek
korporátních	korporátní	k2eAgMnPc2d1	korporátní
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
zůstane	zůstat	k5eAaPmIp3nS	zůstat
tak	tak	k6eAd1	tak
spíše	spíše	k9	spíše
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
na	na	k7c4	na
koncové	koncový	k2eAgMnPc4d1	koncový
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaPmF	využívat
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gInSc4	on
uživatelé	uživatel	k1gMnPc1	uživatel
nebudou	být	k5eNaImBp3nP	být
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
server	server	k1gInSc1	server
The	The	k1gFnSc2	The
Hacker	hacker	k1gMnSc1	hacker
News	Newsa	k1gFnPc2	Newsa
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
700	[number]	k4	700
milionů	milion	k4xCgInPc2	milion
telefonů	telefon	k1gInPc2	telefon
s	s	k7c7	s
Androidem	android	k1gInSc7	android
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
backdoor	backdoor	k1gInSc1	backdoor
společnosti	společnost	k1gFnSc2	společnost
Shanghai	Shangha	k1gFnSc2	Shangha
AdUps	AdUpsa	k1gFnPc2	AdUpsa
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získává	získávat	k5eAaImIp3nS	získávat
data	datum	k1gNnSc2	datum
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Backdoor	Backdoor	k1gInSc1	Backdoor
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
obsah	obsah	k1gInSc4	obsah
SMS	SMS	kA	SMS
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc4	seznam
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc4	seznam
volání	volání	k1gNnSc4	volání
<g/>
,	,	kIx,	,
lokační	lokační	k2eAgInPc4d1	lokační
údaje	údaj	k1gInPc4	údaj
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
osobní	osobní	k2eAgFnPc4d1	osobní
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
je	být	k5eAaImIp3nS	být
každých	každý	k3xTgNnPc2	každý
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Backdoor	Backdoor	k1gInSc1	Backdoor
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
systémových	systémový	k2eAgFnPc6d1	systémová
aplikacích	aplikace	k1gFnPc6	aplikace
com	com	k?	com
<g/>
.	.	kIx.	.
<g/>
adups	adups	k1gInSc1	adups
<g/>
.	.	kIx.	.
<g/>
fota	foto	k1gNnSc2	foto
<g/>
.	.	kIx.	.
<g/>
sysoper	sysoper	k1gInSc1	sysoper
a	a	k8xC	a
com	com	k?	com
<g/>
.	.	kIx.	.
<g/>
adups	adups	k1gInSc1	adups
<g/>
.	.	kIx.	.
<g/>
fota	foto	k1gNnSc2	foto
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichjž	nichjž	k6eAd1	nichjž
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
nebo	nebo	k8xC	nebo
odinstalována	odinstalovat	k5eAaPmNgFnS	odinstalovat
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Licence	licence	k1gFnSc2	licence
==	==	k?	==
</s>
</p>
<p>
<s>
Android	android	k1gInSc1	android
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
otevřený	otevřený	k2eAgInSc1d1	otevřený
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otevřená	otevřený	k2eAgFnSc1d1	otevřená
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
od	od	k7c2	od
modulů	modul	k1gInPc2	modul
Linuxového	linuxový	k2eAgNnSc2d1	linuxové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
aplikačního	aplikační	k2eAgInSc2d1	aplikační
programové	programový	k2eAgNnSc1d1	programové
rozhraní	rozhraní	k1gNnSc1	rozhraní
až	až	k9	až
po	po	k7c4	po
základní	základní	k2eAgFnPc4d1	základní
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dává	dávat	k5eAaImIp3nS	dávat
výrobcům	výrobce	k1gMnPc3	výrobce
možnost	možnost	k1gFnSc4	možnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
systém	systém	k1gInSc1	systém
nasazenému	nasazený	k2eAgInSc3d1	nasazený
hardwaru	hardware	k1gInSc3	hardware
a	a	k8xC	a
také	také	k9	také
lépe	dobře	k6eAd2	dobře
integrovat	integrovat	k5eAaBmF	integrovat
své	svůj	k3xOyFgFnPc4	svůj
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
šířen	šířit	k5eAaImNgInS	šířit
pod	pod	k7c4	pod
"	"	kIx"	"
<g/>
business-friendly	businessriendnout	k5eAaPmAgFnP	business-friendnout
<g/>
"	"	kIx"	"
licencí	licence	k1gFnPc2	licence
(	(	kIx(	(
<g/>
Apache	Apache	k1gFnSc1	Apache
<g/>
/	/	kIx~	/
<g/>
MIT	MIT	kA	MIT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
třetím	třetí	k4xOgMnSc6	třetí
stranám	strana	k1gFnPc3	strana
využívat	využívat	k5eAaImF	využívat
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
technologii	technologie	k1gFnSc4	technologie
firmy	firma	k1gFnSc2	firma
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
Google	Google	k1gFnSc1	Google
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
jako	jako	k9	jako
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Android	android	k1gInSc1	android
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
patentovaných	patentovaný	k2eAgFnPc2d1	patentovaná
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
výrobci	výrobce	k1gMnPc1	výrobce
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
licencovat	licencovat	k5eAaBmF	licencovat
<g/>
.	.	kIx.	.
<g/>
Díky	díky	k7c3	díky
vydání	vydání	k1gNnSc3	vydání
produktu	produkt	k1gInSc2	produkt
jako	jako	k8xC	jako
open	open	k1gNnSc1	open
source	sourec	k1gInSc2	sourec
a	a	k8xC	a
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
použitém	použitý	k2eAgInSc6d1	použitý
hardwaru	hardware	k1gInSc6	hardware
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Android	android	k1gInSc1	android
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
vznik	vznik	k1gInSc4	vznik
velkého	velký	k2eAgNnSc2d1	velké
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
ekosystému	ekosystém	k1gInSc2	ekosystém
tvořeného	tvořený	k2eAgInSc2d1	tvořený
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
,	,	kIx,	,
vývojáři	vývojář	k1gMnPc7	vývojář
a	a	k8xC	a
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
Android	android	k1gInSc1	android
komunita	komunita	k1gFnSc1	komunita
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Android	android	k1gInSc4	android
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
první	první	k4xOgInSc4	první
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
G	G	kA	G
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
začalo	začít	k5eAaPmAgNnS	začít
fungovat	fungovat	k5eAaImF	fungovat
české	český	k2eAgNnSc1d1	české
fórum	fórum	k1gNnSc1	fórum
o	o	k7c6	o
Androidu	android	k1gInSc6	android
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
něhož	jenž	k3xRgNnSc2	jenž
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
velká	velký	k2eAgFnSc1d1	velká
komunita	komunita	k1gFnSc1	komunita
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
77	[number]	k4	77
000	[number]	k4	000
členy	člen	k1gInPc7	člen
(	(	kIx(	(
<g/>
k	k	k7c3	k
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
nízká	nízký	k2eAgFnSc1d1	nízká
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
zařízení	zařízení	k1gNnPc2	zařízení
běžících	běžící	k2eAgNnPc2d1	běžící
na	na	k7c6	na
Androidu	android	k1gInSc6	android
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
aktualitám	aktualita	k1gFnPc3	aktualita
kolem	kolem	k7c2	kolem
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
věnuje	věnovat	k5eAaPmIp3nS	věnovat
server	server	k1gInSc4	server
Svetandroida	Svetandroida	k1gFnSc1	Svetandroida
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
média	médium	k1gNnPc4	médium
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
právě	právě	k9	právě
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
</s>
</p>
<p>
<s>
Smartphone	Smartphon	k1gMnSc5	Smartphon
</s>
</p>
<p>
<s>
YunOS	YunOS	k?	YunOS
</s>
</p>
<p>
<s>
Vestavěný	vestavěný	k2eAgInSc1d1	vestavěný
Linux	linux	k1gInSc1	linux
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Android	android	k1gInSc1	android
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
Androida	Androida	k1gFnSc1	Androida
<g/>
,	,	kIx,	,
svetandroida	svetandroida	k1gFnSc1	svetandroida
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
