<s>
British	British	k1gMnSc1
Overseas	Overseas	k1gMnSc1
Airways	Airwaysa	k1gFnPc2
Corporation	Corporation	k1gInSc4
</s>
<s>
British	British	k1gMnSc1
Overseas	Overseas	k1gMnSc1
Airways	Airwaysa	k1gFnPc2
Corporation	Corporation	k1gInSc4
</s>
<s>
IATABA	IATABA	kA
</s>
<s>
ICAOBA	ICAOBA	kA
</s>
<s>
CALLSIGNSPEEDBIRD	CALLSIGNSPEEDBIRD	kA
</s>
<s>
Zahájení	zahájení	k1gNnSc1
činnosti	činnost	k1gFnSc2
<g/>
1939	#num#	k4
<g/>
Ukončení	ukončení	k1gNnSc2
činnosti	činnost	k1gFnSc2
<g/>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1974	#num#	k4
<g/>
SídloLondýnHlavní	SídloLondýnHlavní	k2eAgInSc4d1
základnaLondon	základnaLondon	k1gInSc4
HeathrowVelikost	HeathrowVelikost	k1gFnSc4
flotily	flotila	k1gFnSc2
<g/>
68	#num#	k4
(	(	kIx(
<g/>
stav	stav	k1gInSc1
z	z	k7c2
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bristol	Bristol	k1gInSc4
Britannia	Britannium	k1gNnSc2
312	#num#	k4
od	od	k7c2
BOAC	BOAC	kA
přistává	přistávat	k5eAaImIp3nS
na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
<g/>
,	,	kIx,
rok	rok	k1gInSc4
1959	#num#	k4
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.106	.106	k4
Comet	Comet	k1gInSc1
4	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
707-436	707-436	k4
na	na	k7c6
London	London	k1gMnSc1
Heathrow	Heathrow	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
</s>
<s>
Vickers	Vickers	k1gInSc1
VC10	VC10	k1gFnSc2
</s>
<s>
British	British	k2eAgFnSc1d1
Overseas	Overseas	k2eAgFnSc1d1
Airways	Airways	k2eAgFnSc1d1
Corporation	Corporation	k1gFnSc1
(	(	kIx(
<g/>
BOAC	BOAC	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
britská	britský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1939	#num#	k4
a	a	k8xC
pod	pod	k7c7
názvem	název	k1gInSc7
BOAC	BOAC	kA
fungovala	fungovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začala	začít	k5eAaPmAgFnS
spolupracovat	spolupracovat	k5eAaImF
se	se	k3xPyFc4
společností	společnost	k1gFnPc2
British	British	k1gInSc1
European	European	k1gMnSc1
Airways	Airways	k1gInSc1
(	(	kIx(
<g/>
BEA	BEA	kA
<g/>
)	)	kIx)
pod	pod	k7c7
názvem	název	k1gInSc7
British	Britisha	k1gFnPc2
Airways	Airwaysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
měla	mít	k5eAaImAgFnS
na	na	k7c6
londýnském	londýnský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
Heathrow	Heathrow	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Letadla	letadlo	k1gNnPc1
používaná	používaný	k2eAgNnPc1d1
společností	společnost	k1gFnPc2
BOAC	BOAC	kA
</s>
<s>
Airspeed	Airspeed	k1gInSc1
Consul	Consul	k1gInSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Airspeed	Airspeed	k1gInSc1
Oxford	Oxford	k1gInSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
Whitworth	Whitworth	k1gMnSc1
A.W	A.W	k1gMnSc1
<g/>
.38	.38	k4
Whitley	Whitlea	k1gFnSc2
5	#num#	k4
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
Whitworth	Whitworth	k1gMnSc1
Ensign	Ensign	k1gMnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avro	Avro	k6eAd1
683	#num#	k4
Lancaster	Lancastra	k1gFnPc2
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avro	Avro	k6eAd1
691	#num#	k4
Lancastrian	Lancastriana	k1gFnPc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avro	Avro	k1gMnSc1
688	#num#	k4
Tudor	tudor	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avro	Avro	k1gMnSc1
685	#num#	k4
York	York	k1gInSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bristol	Bristol	k1gInSc1
Britannia	Britannium	k1gNnSc2
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boeing	boeing	k1gInSc1
314A	314A	k4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boeing	boeing	k1gInSc1
377	#num#	k4
Stratocruiser	Stratocruisra	k1gFnPc2
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boeing	boeing	k1gInSc1
707	#num#	k4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boeing	boeing	k1gInSc1
747	#num#	k4
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Canadair	Canadair	k1gMnSc1
C-4	C-4	k1gMnSc1
Argonaut	argonaut	k1gMnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Consolidated	Consolidated	k1gMnSc1
Model	model	k1gInSc4
28	#num#	k4
Catalina	Catalina	k1gFnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Consolidated	Consolidated	k1gMnSc1
Model	model	k1gInSc4
32	#num#	k4
Liberator	Liberator	k1gInSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Curtis	Curtis	k1gInSc1
Wright	Wright	k1gMnSc1
CW-20	CW-20	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.91	.91	k4
Albatross	Albatrossa	k1gFnPc2
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.95	.95	k4
Flamingo	Flamingo	k6eAd1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.98	.98	k4
Mosquito	Mosquit	k2eAgNnSc1d1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.104	.104	k4
Dove	Dov	k1gFnPc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.106	.106	k4
Comet	Cometa	k1gFnPc2
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Douglas	Douglas	k1gMnSc1
DC-3	DC-3	k1gMnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Douglas	Douglas	k1gMnSc1
DC-7C	DC-7C	k1gMnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Focke-Wulf	Focke-Wulf	k1gInSc1
Fw	Fw	k1gFnSc2
200B	200B	k4
Condor	Condora	k1gFnPc2
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Handley	Handle	k2eAgFnPc1d1
Page	Pag	k1gFnPc1
Halifax	Halifax	k1gInSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Handley	Handle	k2eAgFnPc1d1
Page	Pag	k1gFnPc1
Halton	Halton	k1gInSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Handley	Handlea	k1gMnSc2
Page	Page	k1gNnSc1
Hermes	Hermes	k1gMnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lockheed	Lockheed	k1gInSc1
Constellation	Constellation	k1gInSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Hudson	Hudson	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Lodestar	Lodestar	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
S.	S.	kA
<g/>
23	#num#	k4
Empire	empir	k1gInSc5
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
S.	S.	kA
<g/>
25	#num#	k4
Sunderland	Sunderlando	k1gNnPc2
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
S.	S.	kA
<g/>
26	#num#	k4
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
S.	S.	kA
<g/>
30	#num#	k4
Empire	empir	k1gInSc5
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
Sandringham	Sandringham	k1gInSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Short	Short	k1gInSc1
Solent	Solent	k1gInSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vickers	Vickers	k1gInSc1
VC10	VC10	k1gFnSc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vickers	Vickers	k1gInSc1
Warwick	Warwick	k1gInSc1
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
British	British	k1gInSc1
Overseas	Overseas	k1gInSc1
Airways	Airways	k1gInSc1
Corporation	Corporation	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2018997804	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2182	#num#	k4
2896	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
149544253	#num#	k4
</s>
