<s>
Tubulin	Tubulin	k2eAgMnSc1d1	Tubulin
je	být	k5eAaImIp3nS	být
dimerní	dimerní	k2eAgInSc1d1	dimerní
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
podjednotek	podjednotka	k1gFnPc2	podjednotka
-	-	kIx~	-
alfa-tubulinu	alfaubulin	k2eAgFnSc4d1	alfa-tubulin
a	a	k8xC	a
beta-tubulinu	betaubulin	k2eAgFnSc4d1	beta-tubulin
<g/>
.	.	kIx.	.
</s>
