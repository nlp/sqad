<s>
Tubulin	Tubulin	k2eAgMnSc1d1	Tubulin
je	být	k5eAaImIp3nS	být
dimerní	dimerní	k2eAgInSc1d1	dimerní
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
podjednotek	podjednotka	k1gFnPc2	podjednotka
-	-	kIx~	-
alfa-tubulinu	alfaubulin	k2eAgFnSc4d1	alfa-tubulin
a	a	k8xC	a
beta-tubulinu	betaubulin	k2eAgFnSc4d1	beta-tubulin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
především	především	k9	především
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
mikrotubulů	mikrotubul	k1gInPc2	mikrotubul
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
tvoří	tvořit	k5eAaImIp3nP	tvořit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součást	součást	k1gFnSc4	součást
cytoskeletu	cytoskelet	k1gInSc2	cytoskelet
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
tubulinu	tubulin	k2eAgFnSc4d1	tubulin
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
gama-tubulin	gamaubulin	k2eAgInSc1d1	gama-tubulin
(	(	kIx(	(
<g/>
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
organizačním	organizační	k2eAgNnSc6d1	organizační
centru	centrum	k1gNnSc6	centrum
mikrotubulů	mikrotubul	k1gInPc2	mikrotubul
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
tubulinových	tubulinový	k2eAgInPc2d1	tubulinový
řetězců	řetězec	k1gInPc2	řetězec
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgInSc1d1	aktivní
proces	proces	k1gInSc1	proces
spotřebávající	spotřebávající	k2eAgFnSc2d1	spotřebávající
GTP	GTP	kA	GTP
<g/>
.	.	kIx.	.
</s>
<s>
Beta	beta	k1gNnSc1	beta
a	a	k8xC	a
alfa	alfa	k1gNnSc1	alfa
tubulinové	tubulinový	k2eAgFnSc2d1	tubulinový
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
tubulin	tubulin	k2eAgMnSc1d1	tubulin
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
množství	množství	k1gNnSc4	množství
izotypických	izotypický	k2eAgFnPc2d1	izotypický
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
také	také	k6eAd1	také
podléhat	podléhat	k5eAaImF	podléhat
rozličným	rozličný	k2eAgFnPc3d1	rozličná
posttranslačním	posttranslační	k2eAgFnPc3d1	posttranslační
modifikacím	modifikace	k1gFnPc3	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgInPc1d2	složitější
mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
organismy	organismus	k1gInPc1	organismus
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
sedm	sedm	k4xCc4	sedm
rozličných	rozličný	k2eAgFnPc2d1	rozličná
izoforem	izofor	k1gInSc7	izofor
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
tubulinů	tubulin	k1gInPc2	tubulin
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
kvasinka	kvasinka	k1gFnSc1	kvasinka
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
alfa	alfa	k1gNnPc2	alfa
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
beta	beta	k1gNnSc1	beta
tubulin	tubulin	k2eAgMnSc1d1	tubulin
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
izotypy	izotyp	k1gInPc1	izotyp
složitělších	složitělší	k2eAgInPc2d1	složitělší
organismů	organismus	k1gInPc2	organismus
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
či	či	k8xC	či
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomné	přítomný	k1gMnPc4	přítomný
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
fázi	fáze	k1gFnSc6	fáze
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
