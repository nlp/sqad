<s>
Pojem	pojem	k1gInSc1	pojem
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
bacterion	bacterion	k1gInSc1	bacterion
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
malý	malý	k2eAgInSc1d1	malý
klacek	klacek	k1gInSc1	klacek
či	či	k8xC	či
tyčka	tyčka	k1gFnSc1	tyčka
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
bakterie	bakterie	k1gFnSc2	bakterie
byly	být	k5eAaImAgFnP	být
tyčinky	tyčinka	k1gFnPc1	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
