<s>
Kleště	kleště	k1gFnPc1	kleště
jsou	být	k5eAaImIp3nP	být
nástroj	nástroj	k1gInSc4	nástroj
k	k	k7c3	k
uchopování	uchopování	k1gNnSc3	uchopování
a	a	k8xC	a
stlačování	stlačování	k1gNnSc3	stlačování
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
principu	princip	k1gInSc6	princip
oboustranné	oboustranný	k2eAgFnSc2d1	oboustranná
páky	páka	k1gFnSc2	páka
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
nástrojem	nástroj	k1gInSc7	nástroj
kleští	kleště	k1gFnPc2	kleště
jsou	být	k5eAaImIp3nP	být
nůžky	nůžky	k1gFnPc1	nůžky
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
kleště	kleště	k1gFnPc1	kleště
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
starý	starý	k2eAgInSc4d1	starý
<g/>
,	,	kIx,	,
praslovanský	praslovanský	k2eAgInSc4d1	praslovanský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
metalurgií	metalurgie	k1gFnSc7	metalurgie
(	(	kIx(	(
<g/>
lití	lití	k1gNnSc4	lití
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
kovářství	kovářství	k1gNnSc2	kovářství
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kleště	kleště	k1gFnPc1	kleště
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
souvislost	souvislost	k1gFnSc4	souvislost
(	(	kIx(	(
<g/>
etymologickou	etymologický	k2eAgFnSc4d1	etymologická
ani	ani	k8xC	ani
věcnou	věcný	k2eAgFnSc4d1	věcná
<g/>
)	)	kIx)	)
s	s	k7c7	s
klestěním	klestění	k1gNnSc7	klestění
(	(	kIx(	(
<g/>
kastrací	kastrace	k1gFnPc2	kastrace
<g/>
)	)	kIx)	)
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
ani	ani	k8xC	ani
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
kleštěnec	kleštěnec	k1gMnSc1	kleštěnec
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
<g/>
,	,	kIx,	,
nejpoužívanější	používaný	k2eAgInPc1d3	nejpoužívanější
druhy	druh	k1gInPc1	druh
kleští	kleště	k1gFnPc2	kleště
jsou	být	k5eAaImIp3nP	být
štípací	štípací	k2eAgFnPc1d1	štípací
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnPc1d1	určená
ke	k	k7c3	k
štípání	štípání	k1gNnSc3	štípání
(	(	kIx(	(
<g/>
střihání	střihání	k1gNnSc3	střihání
<g/>
)	)	kIx)	)
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
materiálů	materiál	k1gInPc2	materiál
malého	malý	k2eAgInSc2d1	malý
průřezu	průřez	k1gInSc2	průřez
<g/>
,	,	kIx,	,
a	a	k8xC	a
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
část	část	k1gFnSc4	část
čelistí	čelist	k1gFnPc2	čelist
plochou	plochý	k2eAgFnSc4d1	plochá
rýhovanou	rýhovaný	k2eAgFnSc4d1	rýhovaná
k	k	k7c3	k
uchopování	uchopování	k1gNnSc3	uchopování
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
určenou	určený	k2eAgFnSc4d1	určená
ke	k	k7c3	k
štípání	štípání	k1gNnSc3	štípání
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
také	také	k9	také
požívají	požívat	k5eAaImIp3nP	požívat
kleště	kleště	k1gFnPc1	kleště
sika	sika	k1gMnSc1	sika
(	(	kIx(	(
<g/>
sikovky	sikovka	k1gFnPc1	sikovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nastavitelnou	nastavitelný	k2eAgFnSc4d1	nastavitelná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
čelistí	čelist	k1gFnPc2	čelist
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
uchopení	uchopení	k1gNnSc4	uchopení
předmětů	předmět	k1gInPc2	předmět
většího	veliký	k2eAgInSc2d2	veliký
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
štípací	štípací	k2eAgFnPc1d1	štípací
kleště	kleště	k1gFnPc1	kleště
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
štípačky	štípačka	k1gFnSc2	štípačka
<g/>
)	)	kIx)	)
čelní	čelní	k2eAgFnPc1d1	čelní
štípací	štípací	k2eAgFnPc1d1	štípací
kleště	kleště	k1gFnPc1	kleště
(	(	kIx(	(
<g/>
tesařské	tesařský	k2eAgFnPc1d1	tesařská
<g/>
)	)	kIx)	)
stranové	stranový	k2eAgFnPc1d1	stranová
štípací	štípací	k2eAgFnPc1d1	štípací
kleště	kleště	k1gFnPc1	kleště
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
kleště	kleště	k1gFnPc1	kleště
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s>
kombinačky	kombinačka	k1gFnPc1	kombinačka
<g/>
)	)	kIx)	)
kleště	kleště	k1gFnPc1	kleště
sika	sika	k1gMnSc1	sika
či	či	k8xC	či
siko	sika	k1gMnSc5	sika
<g/>
,	,	kIx,	,
sikovky	sikovka	k1gFnPc1	sikovka
ploché	plochý	k2eAgFnPc1d1	plochá
kleště	kleště	k1gFnPc1	kleště
sklenářské	sklenářský	k2eAgFnPc1d1	sklenářská
kleště	kleště	k1gFnPc1	kleště
-	-	kIx~	-
zhotovené	zhotovený	k2eAgInPc1d1	zhotovený
z	z	k7c2	z
řemeslnických	řemeslnický	k2eAgInPc2d1	řemeslnický
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
čelist	čelist	k1gFnSc1	čelist
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
3	[number]	k4	3
mm	mm	kA	mm
posunutá	posunutý	k2eAgFnSc1d1	posunutá
elektrikářské	elektrikářský	k2eAgFnPc4d1	elektrikářská
kleště	kleště	k1gFnPc4	kleště
lisovací	lisovací	k2eAgFnPc4d1	lisovací
kleště	kleště	k1gFnPc4	kleště
na	na	k7c4	na
kabelová	kabelový	k2eAgNnPc4d1	kabelové
oka	oko	k1gNnPc4	oko
odizolovací	odizolovací	k2eAgFnSc2d1	odizolovací
(	(	kIx(	(
<g/>
stripovací	stripovací	k2eAgFnSc2d1	stripovací
<g/>
)	)	kIx)	)
kleště	kleště	k1gFnPc1	kleště
pro	pro	k7c4	pro
elektrikáře	elektrikář	k1gMnPc4	elektrikář
krimpovací	krimpovací	k2eAgFnPc1d1	krimpovací
kleště	kleště	k1gFnPc1	kleště
stahovací	stahovací	k2eAgFnPc4d1	stahovací
kleště	kleště	k1gFnPc4	kleště
segrovky	segrovka	k1gFnSc2	segrovka
pro	pro	k7c4	pro
instalaci	instalace	k1gFnSc4	instalace
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
sejmutí	sejmutí	k1gNnSc3	sejmutí
pojistných	pojistný	k2eAgInPc2d1	pojistný
kroužků	kroužek	k1gInPc2	kroužek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
segrovka	segrovka	k1gFnSc1	segrovka
<g/>
)	)	kIx)	)
kovářské	kovářský	k2eAgFnPc1d1	Kovářská
kleště	kleště	k1gFnPc1	kleště
instalatérské	instalatérský	k2eAgFnPc1d1	instalatérská
kleště	kleště	k1gFnPc1	kleště
dlaždičské	dlaždičský	k2eAgFnPc1d1	dlaždičská
kleště	kleště	k1gFnPc1	kleště
hutnické	hutnický	k2eAgFnPc1d1	Hutnická
kleště	kleště	k1gFnPc1	kleště
cihlářské	cihlářský	k2eAgFnPc1d1	Cihlářská
kleště-uchopí	kleštěchopit	k5eAaPmIp3nP	kleště-uchopit
naráz	naráz	k6eAd1	naráz
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
cihel	cihla	k1gFnPc2	cihla
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
zamečnické	zamečnický	k2eAgFnPc4d1	zamečnický
kleště	kleště	k1gFnPc4	kleště
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
tzv.	tzv.	kA	tzv.
hasák	hasák	k1gInSc1	hasák
<g/>
)	)	kIx)	)
kadeřnické	kadeřnický	k2eAgFnPc1d1	kadeřnická
kleště	kleště	k1gFnPc1	kleště
(	(	kIx(	(
<g/>
ondulační	ondulační	k2eAgFnPc1d1	ondulační
kleště	kleště	k1gFnPc1	kleště
<g/>
)	)	kIx)	)
kleště	kleště	k1gFnPc1	kleště
na	na	k7c4	na
brikety	briketa	k1gFnPc4	briketa
(	(	kIx(	(
<g/>
topičské	topičský	k2eAgFnPc4d1	topičský
<g />
.	.	kIx.	.
</s>
<s>
kleště	kleště	k1gFnPc1	kleště
<g/>
)	)	kIx)	)
fotografické	fotografický	k2eAgFnPc1d1	fotografická
klíšťky	klíšťky	k1gFnPc1	klíšťky
kleště	kleště	k1gFnPc1	kleště
průvodčího	průvodčí	k1gMnSc2	průvodčí
kancelářské	kancelářský	k2eAgFnSc2d1	kancelářská
pomůcky	pomůcka	k1gFnSc2	pomůcka
na	na	k7c6	na
principu	princip	k1gInSc6	princip
kleští	kleštit	k5eAaImIp3nP	kleštit
proštipovací	proštipovací	k2eAgFnPc1d1	proštipovací
kleště	kleště	k1gFnPc1	kleště
razítkovací	razítkovací	k2eAgFnPc1d1	razítkovací
kleště	kleště	k1gFnPc1	kleště
kleště	kleště	k1gFnPc1	kleště
na	na	k7c4	na
cukr	cukr	k1gInSc4	cukr
louskáček	louskáček	k1gInSc4	louskáček
drtič	drtič	k1gInSc1	drtič
česneku	česnek	k1gInSc2	česnek
(	(	kIx(	(
<g/>
mačkátko	mačkátko	k1gNnSc1	mačkátko
na	na	k7c4	na
česnek	česnek	k1gInSc4	česnek
<g/>
)	)	kIx)	)
kleště	kleště	k1gFnPc1	kleště
pro	pro	k7c4	pro
vytahování	vytahování	k1gNnSc4	vytahování
knedlíků	knedlík	k1gInPc2	knedlík
Pinzeta	pinzeta	k1gFnSc1	pinzeta
Peán	peán	k1gInSc1	peán
Zubařské	zubařský	k2eAgFnSc2d1	zubařská
kleště	kleště	k1gFnPc1	kleště
Porodnické	porodnický	k2eAgFnPc4d1	porodnická
kleště	kleště	k1gFnPc4	kleště
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kleště	kleště	k1gFnPc1	kleště
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kleště	kleště	k1gFnPc1	kleště
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
