<s>
Merkur	Merkur	k1gInSc1	Merkur
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
s	s	k7c7	s
rovníkovým	rovníkový	k2eAgInSc7d1	rovníkový
poloměrem	poloměr	k1gInSc7	poloměr
2439,7	[number]	k4	2439,7
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
38	[number]	k4	38
%	%	kIx~	%
průměru	průměr	k1gInSc2	průměr
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
