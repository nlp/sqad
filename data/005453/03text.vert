<s>
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1749	[number]	k4	1749
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
Výmar	Výmar	k1gInSc1	Výmar
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
krátce	krátce	k6eAd1	krátce
jako	jako	k8xC	jako
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
dvorním	dvorní	k2eAgMnSc7d1	dvorní
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
Sasko-výmarského	saskoýmarský	k2eAgNnSc2d1	sasko-výmarský
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Johannem	Johann	k1gMnSc7	Johann
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Herderem	Herder	k1gMnSc7	Herder
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
přátelství	přátelství	k1gNnSc1	přátelství
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
hnutí	hnutí	k1gNnSc2	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k6eAd1	Goethe
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
samostatné	samostatný	k2eAgFnPc4d1	samostatná
Sicílie	Sicílie	k1gFnPc4	Sicílie
(	(	kIx(	(
<g/>
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
antikou	antika	k1gFnSc7	antika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgNnSc4d1	antické
Řecko	Řecko	k1gNnSc4	Řecko
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c7	za
nedostižný	nedostižný	k?	nedostižný
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
západočeská	západočeský	k2eAgNnPc1d1	Západočeské
lázeňská	lázeňský	k2eAgNnPc1d1	lázeňské
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
především	především	k9	především
Teplice	Teplice	k1gFnPc1	Teplice
a	a	k8xC	a
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
Mariánské	mariánský	k2eAgFnPc4d1	Mariánská
Lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
poslední	poslední	k2eAgFnSc7d1	poslední
láskou	láska	k1gFnSc7	láska
Ulrikou	Ulrika	k1gFnSc7	Ulrika
von	von	k1gInSc1	von
Levetzow	Levetzow	k1gMnPc1	Levetzow
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgInS	sbírat
také	také	k9	také
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
žil	žít	k5eAaImAgInS	žít
a	a	k8xC	a
působil	působit	k5eAaImAgInS	působit
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
usadil	usadit	k5eAaPmAgMnS	usadit
také	také	k9	také
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Goethemu	Goethema	k1gFnSc4	Goethema
nablízku	nablízku	k6eAd1	nablízku
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Schiller	Schiller	k1gMnSc1	Schiller
byli	být	k5eAaImAgMnP	být
tehdy	tehdy	k6eAd1	tehdy
již	již	k9	již
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jejich	jejich	k3xOp3gInSc1	jejich
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
prošel	projít	k5eAaPmAgInS	projít
předtím	předtím	k6eAd1	předtím
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
poté	poté	k6eAd1	poté
<g/>
)	)	kIx)	)
několika	několik	k4yIc7	několik
většími	veliký	k2eAgFnPc7d2	veliký
či	či	k8xC	či
menšími	malý	k2eAgFnPc7d2	menší
krizemi	krize	k1gFnPc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
umělci	umělec	k1gMnPc1	umělec
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
významná	významný	k2eAgNnPc4d1	významné
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
rok	rok	k1gInSc4	rok
1797	[number]	k4	1797
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
rok	rok	k1gInSc1	rok
balad	balada	k1gFnPc2	balada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethat	k5eAaPmIp3nS	Goethat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
již	již	k6eAd1	již
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
slavným	slavný	k2eAgMnSc7d1	slavný
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
dramatikem	dramatik	k1gMnSc7	dramatik
uznávaným	uznávaný	k2eAgInPc3d1	uznávaný
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Johannův	Johannův	k2eAgMnSc1d1	Johannův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Caspar	Caspara	k1gFnPc2	Caspara
Goethe	Goethe	k1gFnPc2	Goethe
(	(	kIx(	(
<g/>
1710	[number]	k4	1710
<g/>
–	–	k?	–
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zástupcem	zástupce	k1gMnSc7	zástupce
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
Říšského	říšský	k2eAgInSc2d1	říšský
soudu	soud	k1gInSc2	soud
ve	v	k7c6	v
Wetzlaru	Wetzlar	k1gInSc6	Wetzlar
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zapustil	zapustit	k5eAaPmAgMnS	zapustit
kořeny	kořen	k1gInPc7	kořen
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
prostorném	prostorný	k2eAgInSc6d1	prostorný
domě	dům	k1gInSc6	dům
u	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
srnčí	srnčí	k2eAgFnSc2d1	srnčí
obory	obora	k1gFnSc2	obora
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
svým	svůj	k3xOyFgFnPc3	svůj
zálibám	záliba	k1gFnPc3	záliba
–	–	k?	–
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnSc3d1	jiná
sbírce	sbírka	k1gFnSc3	sbírka
naturálií	naturálie	k1gFnPc2	naturálie
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Johannova	Johannův	k2eAgFnSc1d1	Johannova
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Katharina	Katharina	k1gFnSc1	Katharina
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
Goethe	Goethe	k1gInSc1	Goethe
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Textor	Textor	k1gInSc1	Textor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
frankfurtského	frankfurtský	k2eAgMnSc2d1	frankfurtský
starosty	starosta	k1gMnSc2	starosta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
osmatřicetiletého	osmatřicetiletý	k2eAgMnSc4d1	osmatřicetiletý
radu	rada	k1gMnSc4	rada
Goetheho	Goethe	k1gMnSc4	Goethe
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
sestry	sestra	k1gFnSc2	sestra
Friederiky	Friederika	k1gFnSc2	Friederika
Christiany	Christian	k1gMnPc7	Christian
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1808	[number]	k4	1808
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
Cornelia	Cornelia	k1gFnSc1	Cornelia
Schlosser	Schlossra	k1gFnPc2	Schlossra
<g/>
)	)	kIx)	)
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
všichni	všechen	k3xTgMnPc1	všechen
další	další	k2eAgMnPc1d1	další
sourozenci	sourozenec	k1gMnPc1	sourozenec
velmi	velmi	k6eAd1	velmi
brzo	brzo	k6eAd1	brzo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
sám	sám	k3xTgMnSc1	sám
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
Goethe	Goethe	k1gInSc1	Goethe
byl	být	k5eAaImAgInS	být
vyučován	vyučovat	k5eAaImNgInS	vyučovat
svým	svůj	k1gMnSc7	svůj
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
soukromým	soukromý	k2eAgMnSc7d1	soukromý
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
vzdělání	vzdělání	k1gNnSc3	vzdělání
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
šermu	šerm	k1gInSc3	šerm
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvíce	hodně	k6eAd3	hodně
jej	on	k3xPp3gMnSc4	on
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Klopstock	Klopstock	k1gMnSc1	Klopstock
(	(	kIx(	(
<g/>
tenkrát	tenkrát	k6eAd1	tenkrát
velmi	velmi	k6eAd1	velmi
moderní	moderní	k2eAgMnSc1d1	moderní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Homér	Homér	k1gMnSc1	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Arkádské	Arkádský	k2eAgFnSc6d1	Arkádský
společnosti	společnost	k1gFnSc6	společnost
Phylandria	Phylandrium	k1gNnSc2	Phylandrium
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
divadlo	divadlo	k1gNnSc4	divadlo
–	–	k?	–
proto	proto	k8xC	proto
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
během	během	k7c2	během
francouzské	francouzský	k2eAgFnSc2d1	francouzská
okupace	okupace	k1gFnSc2	okupace
roku	rok	k1gInSc2	rok
1759	[number]	k4	1759
francouzské	francouzský	k2eAgNnSc4d1	francouzské
divadlo	divadlo	k1gNnSc4	divadlo
ve	v	k7c6	v
frankfurtském	frankfurtský	k2eAgInSc6d1	frankfurtský
Junghofu	Junghof	k1gInSc6	Junghof
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
zhlédl	zhlédnout	k5eAaPmAgInS	zhlédnout
koncert	koncert	k1gInSc1	koncert
tehdy	tehdy	k6eAd1	tehdy
sedmiletého	sedmiletý	k2eAgMnSc2d1	sedmiletý
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1765	[number]	k4	1765
opustil	opustit	k5eAaPmAgMnS	opustit
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
započal	započnout	k5eAaPmAgInS	započnout
studia	studio	k1gNnSc2	studio
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1765	[number]	k4	1765
až	až	k9	až
1768	[number]	k4	1768
studoval	studovat	k5eAaImAgInS	studovat
Goethe	Goethe	k1gInSc1	Goethe
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
přednášky	přednáška	k1gFnPc4	přednáška
poetiky	poetika	k1gFnSc2	poetika
Christiana	Christian	k1gMnSc4	Christian
Fürchtegotta	Fürchtegott	k1gMnSc4	Fürchtegott
Gellerta	Gellert	k1gMnSc4	Gellert
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
cvičení	cvičení	k1gNnSc4	cvičení
stylistiky	stylistika	k1gFnSc2	stylistika
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
malířského	malířský	k2eAgNnSc2d1	malířské
vyučování	vyučování	k1gNnSc2	vyučování
u	u	k7c2	u
Adama	Adam	k1gMnSc4	Adam
Friedricha	Friedrich	k1gMnSc2	Friedrich
Oesera	Oeser	k1gMnSc2	Oeser
<g/>
,	,	kIx,	,
ředitele	ředitel	k1gMnSc2	ředitel
Lipské	lipský	k2eAgFnSc2d1	Lipská
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Kätchen	Kätchna	k1gFnPc2	Kätchna
Schönkopf	Schönkopf	k1gInSc4	Schönkopf
a	a	k8xC	a
opěvoval	opěvovat	k5eAaImAgMnS	opěvovat
tuto	tento	k3xDgFnSc4	tento
lásku	láska	k1gFnSc4	láska
ve	v	k7c6	v
veselých	veselý	k2eAgInPc6d1	veselý
rozverných	rozverný	k2eAgInPc6d1	rozverný
verších	verš	k1gInPc6	verš
rokokové	rokokový	k2eAgFnSc2d1	rokoková
tradice	tradice	k1gFnSc2	tradice
(	(	kIx(	(
<g/>
básnický	básnický	k2eAgInSc1d1	básnický
cyklus	cyklus	k1gInSc1	cyklus
Annette	Annett	k1gInSc5	Annett
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hostinec	hostinec	k1gInSc1	hostinec
Auerbachs	Auerbachsa	k1gFnPc2	Auerbachsa
Keller	Keller	k1gMnSc1	Keller
a	a	k8xC	a
tamější	tamější	k2eAgFnSc1d1	tamější
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Faustovi	Fausta	k1gMnSc6	Fausta
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
udělaly	udělat	k5eAaPmAgFnP	udělat
takový	takový	k3xDgInSc4	takový
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
použil	použít	k5eAaPmAgInS	použít
hostinec	hostinec	k1gInSc1	hostinec
Auerbachs	Auerbachsa	k1gFnPc2	Auerbachsa
Keller	Keller	k1gMnSc1	Keller
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
existující	existující	k2eAgNnSc1d1	existující
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dramatu	drama	k1gNnSc6	drama
Faust	Faust	k1gFnSc1	Faust
I.	I.	kA	I.
Náhlá	náhlý	k2eAgFnSc1d1	náhlá
hematorea	hematorea	k1gFnSc1	hematorea
(	(	kIx(	(
<g/>
prudké	prudký	k2eAgNnSc1d1	prudké
krvácení	krvácení	k1gNnSc1	krvácení
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
donutilo	donutit	k5eAaPmAgNnS	donutit
přerušit	přerušit	k5eAaPmF	přerušit
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1768	[number]	k4	1768
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
jedenapůlleté	jedenapůlletý	k2eAgNnSc1d1	jedenapůlletý
<g/>
,	,	kIx,	,
několika	několik	k4yIc7	několik
zvraty	zvrat	k1gInPc4	zvrat
přerušované	přerušovaný	k2eAgNnSc4d1	přerušované
období	období	k1gNnSc4	období
zotavování	zotavování	k1gNnSc2	zotavování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
starala	starat	k5eAaImAgFnS	starat
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
matčina	matčin	k2eAgFnSc1d1	matčina
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
,	,	kIx,	,
Susanne	Susann	k1gInSc5	Susann
von	von	k1gInSc1	von
Klettenberg	Klettenberg	k1gInSc4	Klettenberg
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
pietistickými	pietistický	k2eAgInPc7d1	pietistický
(	(	kIx(	(
<g/>
proud	proud	k1gInSc1	proud
v	v	k7c6	v
protestantském	protestantský	k2eAgNnSc6d1	protestantské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
)	)	kIx)	)
názory	názor	k1gInPc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1770	[number]	k4	1770
opustil	opustit	k5eAaPmAgInS	opustit
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
splnil	splnit	k5eAaPmAgInS	splnit
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
poznává	poznávat	k5eAaImIp3nS	poznávat
Friedriku	Friedrika	k1gFnSc4	Friedrika
Brion	Briona	k1gFnPc2	Briona
–	–	k?	–
dceru	dcera	k1gFnSc4	dcera
faráře	farář	k1gMnSc2	farář
<g/>
.	.	kIx.	.
</s>
<s>
Jí	jíst	k5eAaImIp3nS	jíst
věnoval	věnovat	k5eAaImAgMnS	věnovat
několik	několik	k4yIc4	několik
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Willkommen	Willkommen	k1gInSc1	Willkommen
und	und	k?	und
Abschied	Abschied	k1gInSc1	Abschied
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sessenheimer	Sessenheimer	k1gMnSc1	Sessenheimer
Lieder	Lieder	k1gMnSc1	Lieder
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Heidenröslein	Heidenröslein	k2eAgMnSc1d1	Heidenröslein
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Přivítání	přivítání	k1gNnSc1	přivítání
a	a	k8xC	a
loučení	loučení	k1gNnSc1	loučení
<g/>
,	,	kIx,	,
Sessenheimské	Sessenheimský	k2eAgFnPc1d1	Sessenheimský
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
Šípková	šípkový	k2eAgFnSc1d1	šípková
růžička	růžička	k1gFnSc1	růžička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
domů	domů	k6eAd1	domů
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
advokát	advokát	k1gMnSc1	advokát
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
soudu	soud	k1gInSc6	soud
ve	v	k7c6	v
Wetzlaru	Wetzlar	k1gInSc6	Wetzlar
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
zanechal	zanechat	k5eAaPmAgInS	zanechat
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
výhradně	výhradně	k6eAd1	výhradně
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
novelu	novela	k1gFnSc4	novela
Utrpení	utrpení	k1gNnSc2	utrpení
mladého	mladý	k2eAgNnSc2d1	mladé
Werthera	Werthero	k1gNnSc2	Werthero
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inspirovanou	inspirovaný	k2eAgFnSc7d1	inspirovaná
údajně	údajně	k6eAd1	údajně
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
Charlottě	Charlotta	k1gFnSc3	Charlotta
Buffové	buffový	k2eAgFnSc3d1	buffový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
žil	žít	k5eAaImAgMnS	žít
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
sasko-výmarského	saskoýmarský	k2eAgMnSc2d1	sasko-výmarský
vévody	vévoda	k1gMnSc2	vévoda
Karla	Karel	k1gMnSc2	Karel
Augusta	August	k1gMnSc2	August
(	(	kIx(	(
<g/>
1757	[number]	k4	1757
<g/>
–	–	k?	–
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
malé	malý	k2eAgNnSc1d1	malé
vévodství	vévodství	k1gNnSc1	vévodství
kulturně	kulturně	k6eAd1	kulturně
pozvednout	pozvednout	k5eAaPmF	pozvednout
a	a	k8xC	a
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
různé	různý	k2eAgFnPc4d1	různá
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
vévody	vévoda	k1gMnSc2	vévoda
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
byl	být	k5eAaImAgInS	být
Goethe	Goethe	k1gInSc1	Goethe
povýšen	povýšit	k5eAaPmNgInS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
,	,	kIx,	,
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
hnutí	hnutí	k1gNnSc2	hnutí
"	"	kIx"	"
<g/>
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
Goethe	Goethe	k1gInSc1	Goethe
navázal	navázat	k5eAaPmAgInS	navázat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Charlottou	Charlotta	k1gFnSc7	Charlotta
von	von	k1gInSc4	von
Steinovou	Steinová	k1gFnSc7	Steinová
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc7d2	starší
<g/>
,	,	kIx,	,
vdanou	vdaný	k2eAgFnSc7d1	vdaná
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pouto	pouto	k1gNnSc1	pouto
trvalo	trvat	k5eAaImAgNnS	trvat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Goethe	Goethe	k1gInSc1	Goethe
náhle	náhle	k6eAd1	náhle
odjel	odjet	k5eAaPmAgInS	odjet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
italský	italský	k2eAgInSc4d1	italský
poloostrov	poloostrov	k1gInSc4	poloostrov
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
v	v	k7c6	v
letech	let	k1gInPc6	let
1786	[number]	k4	1786
<g/>
–	–	k?	–
<g/>
1788	[number]	k4	1788
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
jeho	jeho	k3xOp3gFnSc2	jeho
estetiky	estetika	k1gFnSc2	estetika
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
antickým	antický	k2eAgNnSc7d1	antické
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
evropské	evropský	k2eAgFnSc2d1	Evropská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
se	se	k3xPyFc4	se
Goethe	Goethe	k1gInSc1	Goethe
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Pruska	Prusko	k1gNnSc2	Prusko
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Valmy	Valma	k1gFnSc2	Valma
proti	proti	k7c3	proti
revoluční	revoluční	k2eAgFnSc3d1	revoluční
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
do	do	k7c2	do
Výmaru	Výmar	k1gInSc2	Výmar
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
v	v	k7c6	v
Goethově	Goethův	k2eAgFnSc6d1	Goethova
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
předtím	předtím	k6eAd1	předtím
už	už	k9	už
několik	několik	k4yIc4	několik
roků	rok	k1gInPc2	rok
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1798	[number]	k4	1798
spolu	spolu	k6eAd1	spolu
publikovali	publikovat	k5eAaBmAgMnP	publikovat
několik	několik	k4yIc4	několik
balad	balada	k1gFnPc2	balada
v	v	k7c6	v
ročence	ročenka	k1gFnSc6	ročenka
Musen-Almanach	Musen-Almanacha	k1gFnPc2	Musen-Almanacha
für	für	k?	für
das	das	k?	das
Jahr	Jahr	k1gInSc4	Jahr
1798	[number]	k4	1798
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
Goethe	Goethe	k1gInSc1	Goethe
legalizoval	legalizovat	k5eAaBmAgInS	legalizovat
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Christianou	Christiana	k1gFnSc7	Christiana
Vulpiusovou	Vulpiusový	k2eAgFnSc7d1	Vulpiusový
(	(	kIx(	(
<g/>
1765	[number]	k4	1765
<g/>
–	–	k?	–
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dospělosti	dospělost	k1gFnSc3	dospělost
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
jen	jen	k9	jen
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
Goethe	Goethe	k1gFnSc1	Goethe
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
poslední	poslední	k2eAgFnSc7d1	poslední
láskou	láska	k1gFnSc7	láska
Ulrikou	Ulrika	k1gFnSc7	Ulrika
von	von	k1gInSc4	von
Levetzow	Levetzow	k1gMnSc4	Levetzow
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženit	oženit	k5eAaPmF	oženit
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
zabránila	zabránit	k5eAaPmAgFnS	zabránit
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
Goethe	Goeth	k1gFnSc2	Goeth
pobýval	pobývat	k5eAaImAgMnS	pobývat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
a	a	k8xC	a
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc4	tento
města	město	k1gNnPc4	město
opouštěl	opouštět	k5eAaImAgMnS	opouštět
jen	jen	k6eAd1	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pro	pro	k7c4	pro
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
lázní	lázeň	k1gFnPc2	lázeň
(	(	kIx(	(
<g/>
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
větší	veliký	k2eAgFnSc4d2	veliký
cestu	cesta	k1gFnSc4	cesta
podnikl	podniknout	k5eAaPmAgMnS	podniknout
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
nepodnikal	podnikat	k5eNaImAgInS	podnikat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
dokončit	dokončit	k5eAaPmF	dokončit
a	a	k8xC	a
uspořádat	uspořádat	k5eAaPmF	uspořádat
rozpracovaná	rozpracovaný	k2eAgNnPc4d1	rozpracované
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
Fausta	Faust	k1gMnSc2	Faust
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1832	[number]	k4	1832
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
detailně	detailně	k6eAd1	detailně
propracovaná	propracovaný	k2eAgNnPc1d1	propracované
a	a	k8xC	a
komplikovaná	komplikovaný	k2eAgNnPc1d1	komplikované
<g/>
.	.	kIx.	.
</s>
<s>
Goethova	Goethův	k2eAgFnSc1d1	Goethova
tvorba	tvorba	k1gFnSc1	tvorba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1795	[number]	k4	1795
<g/>
–	–	k?	–
<g/>
1805	[number]	k4	1805
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
výmarská	výmarský	k2eAgFnSc1d1	Výmarská
klasika	klasika	k1gFnSc1	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
citovosti	citovost	k1gFnSc2	citovost
–	–	k?	–
Dramatický	dramatický	k2eAgInSc1d1	dramatický
vrtoch	vrtoch	k1gInSc1	vrtoch
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
sám	sám	k3xTgMnSc1	sám
Goethe	Goethe	k1gFnSc7	Goethe
nazval	nazvat	k5eAaPmAgInS	nazvat
<g/>
)	)	kIx)	)
Götz	Götz	k1gInSc1	Götz
z	z	k7c2	z
Berlichingen	Berlichingen	k1gInSc4	Berlichingen
se	s	k7c7	s
železnou	železný	k2eAgFnSc7d1	železná
rukou	ruka	k1gFnSc7	ruka
(	(	kIx(	(
<g/>
Götz	Götz	k1gInSc1	Götz
von	von	k1gInSc1	von
Berlichingen	Berlichingen	k1gInSc1	Berlichingen
mit	mit	k?	mit
der	drát	k5eAaImRp2nS	drát
eisernen	eisernen	k1gInSc4	eisernen
Hand	Hand	k1gInSc1	Hand
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drama	drama	k1gNnSc1	drama
psané	psaný	k2eAgNnSc1d1	psané
v	v	k7c6	v
shakespearovském	shakespearovský	k2eAgInSc6d1	shakespearovský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Goethův	Goethův	k2eAgInSc1d1	Goethův
Götz	Götz	k1gInSc1	Götz
je	být	k5eAaImIp3nS	být
načrtnut	načrtnout	k5eAaPmNgInS	načrtnout
podle	podle	k7c2	podle
ideálu	ideál	k1gInSc2	ideál
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
aktivní	aktivní	k2eAgMnSc1d1	aktivní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
hrdý	hrdý	k2eAgMnSc1d1	hrdý
a	a	k8xC	a
plný	plný	k2eAgInSc1d1	plný
vzdoru	vzdor	k1gInSc2	vzdor
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
,	,	kIx,	,
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ifigenie	Ifigenie	k1gFnSc1	Ifigenie
na	na	k7c6	na
Tauridě	Taurida	k1gFnSc6	Taurida
Torquato	Torquat	k2eAgNnSc1d1	Torquato
Tasso	Tassa	k1gFnSc5	Tassa
Egmont	Egmont	k1gMnSc1	Egmont
(	(	kIx(	(
<g/>
Beethoven	Beethoven	k1gMnSc1	Beethoven
byl	být	k5eAaImAgMnS	být
tímto	tento	k3xDgInSc7	tento
inspirován	inspirován	k2eAgInSc4d1	inspirován
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	)
Anethe	Anethe	k1gFnSc1	Anethe
Stella	Stella	k1gFnSc1	Stella
či	či	k8xC	či
Hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
milující	milující	k2eAgInSc4d1	milující
(	(	kIx(	(
<g/>
Stella	Stella	k1gFnSc1	Stella
:	:	kIx,	:
Ein	Ein	k1gMnSc1	Ein
Schauspiel	Schauspiel	k1gMnSc1	Schauspiel
für	für	k?	für
Liebende	Liebend	k1gMnSc5	Liebend
<g/>
,	,	kIx,	,
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
zdramatizována	zdramatizovat	k5eAaPmNgFnS	zdramatizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
Historie	historie	k1gFnSc1	historie
válečníka	válečník	k1gMnSc2	válečník
Faust	Faust	k1gFnSc1	Faust
je	být	k5eAaImIp3nS	být
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
dramatická	dramatický	k2eAgFnSc1d1	dramatická
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
raná	raný	k2eAgFnSc1d1	raná
nepublikovaná	publikovaný	k2eNgFnSc1d1	nepublikovaná
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Urfaust	Urfaust	k1gFnSc1	Urfaust
(	(	kIx(	(
<g/>
Prafaust	Prafaust	k1gInSc1	Prafaust
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
až	až	k9	až
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prometheus	Prometheus	k1gMnSc1	Prometheus
(	(	kIx(	(
<g/>
další	další	k2eAgInPc1d1	další
z	z	k7c2	z
Beethovenových	Beethovenových	k2eAgNnPc2d1	Beethovenových
děl	dělo	k1gNnPc2	dělo
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
antický	antický	k2eAgInSc4d1	antický
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
Prométheovi	Prométheus	k1gMnSc6	Prométheus
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
rebelujícího	rebelující	k2eAgMnSc4d1	rebelující
a	a	k8xC	a
triumfujícího	triumfující	k2eAgMnSc4d1	triumfující
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
duchů	duch	k1gMnPc2	duch
–	–	k?	–
balada	balada	k1gFnSc1	balada
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
učeň	učeň	k1gMnSc1	učeň
–	–	k?	–
balada	balada	k1gFnSc1	balada
Heřman	Heřman	k1gMnSc1	Heřman
a	a	k8xC	a
Dorotea	Dorotea	k1gMnSc1	Dorotea
–	–	k?	–
idyla	idyla	k1gFnSc1	idyla
Západovýchodní	západovýchodní	k2eAgFnPc1d1	západovýchodní
díván	díván	k1gInSc4	díván
Elegie	elegie	k1gFnSc2	elegie
z	z	k7c2	z
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
Život	život	k1gInSc4	život
slavím	slavit	k5eAaImIp1nS	slavit
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
Utrpení	utrpení	k1gNnSc4	utrpení
mladého	mladý	k2eAgMnSc2d1	mladý
Werthera	Werther	k1gMnSc2	Werther
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Leiden	Leidno	k1gNnPc2	Leidno
des	des	k1gNnSc7	des
jungen	jungen	k1gInSc1	jungen
Werthers	Werthers	k1gInSc1	Werthers
<g/>
,	,	kIx,	,
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
Goethe	Goethe	k1gInSc1	Goethe
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
Viléma	Vilém	k1gMnSc2	Vilém
Meistera	Meister	k1gMnSc2	Meister
léta	léto	k1gNnSc2	léto
učednická	učednický	k2eAgFnSc1d1	učednická
–	–	k?	–
vývojový	vývojový	k2eAgInSc1d1	vývojový
a	a	k8xC	a
výchovný	výchovný	k2eAgInSc1d1	výchovný
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
protipól	protipól	k1gInSc1	protipól
k	k	k7c3	k
Utrpení	utrpení	k1gNnSc3	utrpení
mladého	mladý	k2eAgMnSc2d1	mladý
Werthera	Werther	k1gMnSc2	Werther
Viléma	Vilém	k1gMnSc2	Vilém
Meistera	Meister	k1gMnSc2	Meister
léta	léto	k1gNnSc2	léto
tovaryšská	tovaryšský	k2eAgFnSc1d1	tovaryšská
Spříznění	spříznění	k1gNnSc2	spříznění
volbou	volba	k1gFnSc7	volba
(	(	kIx(	(
<g/>
Die	Die	k1gFnPc1	Die
Wahlverwandtschaften	Wahlverwandtschaftno	k1gNnPc2	Wahlverwandtschaftno
<g/>
,	,	kIx,	,
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
i	i	k8xC	i
pravda	pravda	k1gFnSc1	pravda
(	(	kIx(	(
<g/>
Aus	Aus	k1gMnSc7	Aus
meinem	mein	k1gMnSc7	mein
Leben	Leben	k2eAgMnSc1d1	Leben
<g/>
.	.	kIx.	.
</s>
<s>
Dichtung	Dichtung	k1gMnSc1	Dichtung
und	und	k?	und
Wahrheit	Wahrheit	k1gMnSc1	Wahrheit
<g/>
,	,	kIx,	,
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
–	–	k?	–
autobiografie	autobiografie	k1gFnSc1	autobiografie
Jako	jako	k8xC	jako
biolog	biolog	k1gMnSc1	biolog
Goethe	Goethe	k1gInSc4	Goethe
patřil	patřit	k5eAaImAgMnS	patřit
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
německé	německý	k2eAgFnSc2d1	německá
naturfilosofie	naturfilosofie	k1gFnSc2	naturfilosofie
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
ze	z	k7c2	z
zejména	zejména	k9	zejména
osteologií	osteologie	k1gFnPc2	osteologie
a	a	k8xC	a
botanikou	botanika	k1gFnSc7	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
časopisecky	časopisecky	k6eAd1	časopisecky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	on	k3xPp3gInSc4	on
esej	esej	k1gInSc4	esej
Versuch	Versuch	k1gInSc1	Versuch
die	die	k?	die
Metamorphose	Metamorphosa	k1gFnSc3	Metamorphosa
der	drát	k5eAaImRp2nS	drát
Pflanzen	Pflanzna	k1gFnPc2	Pflanzna
zu	zu	k?	zu
erklären	erklärna	k1gFnPc2	erklärna
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
<g/>
:	:	kIx,	:
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
proměny	proměna	k1gFnSc2	proměna
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
studie	studie	k1gFnSc1	studie
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
znovu	znovu	k6eAd1	znovu
až	až	k6eAd1	až
po	po	k7c6	po
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řady	řada	k1gFnSc2	řada
Zur	Zur	k1gFnSc2	Zur
Morphologie	Morphologie	k1gFnSc2	Morphologie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Die	Die	k1gFnSc2	Die
Metamorphose	Metamorphosa	k1gFnSc3	Metamorphosa
der	drát	k5eAaImRp2nS	drát
Pflanzen	Pflanzen	k2eAgInSc4d1	Pflanzen
(	(	kIx(	(
<g/>
Proměna	proměna	k1gFnSc1	proměna
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
Goethe	Goeth	k1gFnSc2	Goeth
m.	m.	k?	m.
<g/>
j.	j.	k?	j.
popisuje	popisovat	k5eAaImIp3nS	popisovat
rostlinu	rostlina	k1gFnSc4	rostlina
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
-	-	kIx~	-
naduť	naduť	k1gFnSc1	naduť
peřenou	peřena	k1gFnSc7	peřena
(	(	kIx(	(
<g/>
Bryophyllum	Bryophyllum	k1gInSc1	Bryophyllum
pinnatum	pinnatum	k1gNnSc1	pinnatum
či	či	k8xC	či
Kalanchoe	Kalanchoe	k1gFnSc1	Kalanchoe
pinnata	pinnata	k1gFnSc1	pinnata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Goetheho	Goethe	k1gMnSc4	Goethe
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
básník	básník	k1gMnSc1	básník
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gInSc1	Goethe
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
součásti	součást	k1gFnPc4	součást
květu	květ	k1gInSc2	květ
interpretoval	interpretovat	k5eAaBmAgInS	interpretovat
jako	jako	k9	jako
metamorfované	metamorfovaný	k2eAgInPc4d1	metamorfovaný
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
měl	mít	k5eAaImAgMnS	mít
primát	primát	k1gInSc4	primát
i	i	k9	i
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
-19	-19	k4	-19
let	léto	k1gNnPc2	léto
před	před	k7c7	před
formulováním	formulování	k1gNnSc7	formulování
lamarckismu	lamarckismus	k1gInSc2	lamarckismus
a	a	k8xC	a
48	[number]	k4	48
let	léto	k1gNnPc2	léto
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
Darwinovy	Darwinův	k2eAgFnSc2d1	Darwinova
vývojové	vývojový	k2eAgFnSc2d1	vývojová
teorie	teorie	k1gFnSc2	teorie
-	-	kIx~	-
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
reálné	reálný	k2eAgFnSc2d1	reálná
rostliny	rostlina	k1gFnSc2	rostlina
jako	jako	k8xC	jako
metamorfózy	metamorfóza	k1gFnSc2	metamorfóza
prarostliny	prarostlin	k2eAgFnSc2d1	prarostlin
(	(	kIx(	(
<g/>
Urpflanze	Urpflanze	k1gFnSc2	Urpflanze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethat	k5eAaPmIp3nS	Goethat
se	se	k3xPyFc4	se
také	také	k9	také
intenzivně	intenzivně	k6eAd1	intenzivně
zabýval	zabývat	k5eAaImAgInS	zabývat
geologií	geologie	k1gFnSc7	geologie
a	a	k8xC	a
mineralogií	mineralogie	k1gFnSc7	mineralogie
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgMnS	sbírat
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
poblíž	poblíž	k7c2	poblíž
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
například	například	k6eAd1	například
karlovarská	karlovarský	k2eAgNnPc4d1	Karlovarské
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
specifických	specifický	k2eAgInPc2d1	specifický
místních	místní	k2eAgInPc2d1	místní
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
odrůda	odrůda	k1gFnSc1	odrůda
vřídlovce	vřídlovec	k1gInSc2	vřídlovec
hrachovec	hrachovec	k1gInSc1	hrachovec
Jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
byl	být	k5eAaImAgInS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
minerál	minerál	k1gInSc1	minerál
goethit	goethit	k1gInSc1	goethit
<g/>
.	.	kIx.	.
</s>
<s>
Goethova	Goethův	k2eAgFnSc1d1	Goethova
sbírka	sbírka	k1gFnSc1	sbírka
minerálů	minerál	k1gInPc2	minerál
čítala	čítat	k5eAaImAgFnS	čítat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
17	[number]	k4	17
800	[number]	k4	800
exponátů	exponát	k1gInPc2	exponát
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gInSc1	Goethe
věnoval	věnovat	k5eAaPmAgInS	věnovat
mnoho	mnoho	k6eAd1	mnoho
úsilí	úsilí	k1gNnSc4	úsilí
práci	práce	k1gFnSc3	práce
Teorie	teorie	k1gFnSc2	teorie
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
Zur	Zur	k1gMnSc5	Zur
Farbenlehre	Farbenlehr	k1gMnSc5	Farbenlehr
<g/>
,	,	kIx,	,
Tübingen	Tübingen	k1gInSc1	Tübingen
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
popsat	popsat	k5eAaPmF	popsat
lidské	lidský	k2eAgNnSc4d1	lidské
vnímání	vnímání	k1gNnSc4	vnímání
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
polemizovat	polemizovat	k5eAaImF	polemizovat
s	s	k7c7	s
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
oceňována	oceňovat	k5eAaImNgFnS	oceňovat
přesnost	přesnost	k1gFnSc1	přesnost
Goethových	Goethův	k2eAgNnPc2d1	Goethovo
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
,	,	kIx,	,
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
teorie	teorie	k1gFnSc2	teorie
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
prediktivní	prediktivní	k2eAgFnSc4d1	prediktivní
platnost	platnost	k1gFnSc4	platnost
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
za	za	k7c4	za
fenomenologii	fenomenologie	k1gFnSc4	fenomenologie
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gInSc1	Goethe
působil	působit	k5eAaImAgInS	působit
též	též	k9	též
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
silničního	silniční	k2eAgMnSc2d1	silniční
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
technologii	technologie	k1gFnSc4	technologie
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
údržby	údržba	k1gFnSc2	údržba
některých	některý	k3yIgFnPc2	některý
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
Traktát	traktát	k1gInSc1	traktát
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
a	a	k8xC	a
stavbě	stavba	k1gFnSc6	stavba
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
GOETHE	GOETHE	kA	GOETHE
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
<g/>
.	.	kIx.	.
</s>
<s>
Ballady	Ballad	k1gInPc1	Ballad
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alfred	Alfred	k1gMnSc1	Alfred
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
102	[number]	k4	102
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
GOETHE	GOETHE	kA	GOETHE
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
<g/>
.	.	kIx.	.
</s>
<s>
Faust	Faust	k1gFnSc1	Faust
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
447	[number]	k4	447
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
GOETHE	GOETHE	kA	GOETHE
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
<g/>
.	.	kIx.	.
</s>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
mladého	mladý	k2eAgMnSc2d1	mladý
Werthera	Werther	k1gMnSc2	Werther
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Emanuel	Emanuel	k1gMnSc1	Emanuel
Miřiovský	Miřiovský	k1gMnSc1	Miřiovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
172	[number]	k4	172
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
