<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Matouš	Matouš	k1gMnSc1	Matouš
Evangelista	evangelista	k1gMnSc1	evangelista
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
dar	dar	k1gInSc1	dar
Boží	boží	k2eAgInSc1d1	boží
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Μ	Μ	k?	Μ
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
apoštol	apoštol	k1gMnSc1	apoštol
a	a	k8xC	a
údajný	údajný	k2eAgMnSc1d1	údajný
autor	autor	k1gMnSc1	autor
Evangelia	evangelium	k1gNnSc2	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Alfeus	Alfeus	k1gInSc4	Alfeus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
bratrem	bratr	k1gMnSc7	bratr
apoštola	apoštol	k1gMnSc2	apoštol
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bibli	bible	k1gFnSc6	bible
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
jako	jako	k8xS	jako
celník	celník	k1gInSc1	celník
či	či	k8xC	či
výběrčí	výběrčí	k1gFnSc1	výběrčí
daní	daň	k1gFnPc2	daň
v	v	k7c4	v
Kafarnaum	Kafarnaum	k1gInSc4	Kafarnaum
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lukášově	Lukášův	k2eAgMnSc6d1	Lukášův
a	a	k8xC	a
Markově	Markův	k2eAgNnSc6d1	Markovo
evangeliu	evangelium	k1gNnSc6	evangelium
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
Levi	Lev	k1gFnSc3	Lev
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbytek	zbytek	k1gInSc1	zbytek
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
osobám	osoba	k1gFnPc3	osoba
váže	vázat	k5eAaImIp3nS	vázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
evangeliích	evangelium	k1gNnPc6	evangelium
prakticky	prakticky	k6eAd1	prakticky
identický	identický	k2eAgInSc4d1	identický
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tutéž	týž	k3xTgFnSc4	týž
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
skutečně	skutečně	k6eAd1	skutečně
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Levi	Levi	k1gNnSc4	Levi
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Matouš	Matouš	k1gMnSc1	Matouš
přijal	přijmout	k5eAaPmAgInS	přijmout
až	až	k9	až
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
mezi	mezi	k7c4	mezi
Kristovy	Kristův	k2eAgMnPc4d1	Kristův
učedníky	učedník	k1gMnPc4	učedník
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
velkou	velký	k2eAgFnSc7d1	velká
hostinu	hostina	k1gFnSc4	hostina
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Skutcích	skutek	k1gInPc6	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
v	v	k7c6	v
apokryfních	apokryfní	k2eAgInPc6d1	apokryfní
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Tomášovo	Tomášův	k2eAgNnSc1d1	Tomášovo
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
velkém	velký	k2eAgInSc6d1	velký
významu	význam	k1gInSc6	význam
pro	pro	k7c4	pro
prvotní	prvotní	k2eAgFnSc4d1	prvotní
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
nadiktoval	nadiktovat	k5eAaPmAgInS	nadiktovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
Kristově	Kristův	k2eAgFnSc6d1	Kristova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Detaily	detail	k1gInPc4	detail
ohledně	ohledně	k7c2	ohledně
jeho	on	k3xPp3gNnSc2	on
umučení	umučení	k1gNnSc2	umučení
jsou	být	k5eAaImIp3nP	být
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
zcela	zcela	k6eAd1	zcela
neznámé	známý	k2eNgNnSc1d1	neznámé
(	(	kIx(	(
<g/>
svátek	svátek	k1gInSc1	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Matouše	Matouš	k1gMnSc2	Matouš
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
církvích	církev	k1gFnPc6	církev
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
.	.	kIx.	.
</s>
<s>
Učenec	učenec	k1gMnSc1	učenec
a	a	k8xC	a
církevní	církevní	k2eAgMnSc1d1	církevní
otec	otec	k1gMnSc1	otec
Epifanus	Epifanus	k1gMnSc1	Epifanus
klade	klást	k5eAaImIp3nS	klást
umučení	umučení	k1gNnSc4	umučení
evangelisty	evangelista	k1gMnSc2	evangelista
Matouše	Matouš	k1gMnSc2	Matouš
do	do	k7c2	do
Hierapolis	Hierapolis	k1gFnSc2	Hierapolis
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
dokument	dokument	k1gInSc1	dokument
ze	z	k7c2	z
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
místo	místo	k7c2	místo
hrobu	hrob	k1gInSc2	hrob
evangelisty	evangelista	k1gMnSc2	evangelista
Matouše	Matouš	k1gMnSc2	Matouš
jakési	jakýsi	k3yIgNnSc4	jakýsi
"	"	kIx"	"
<g/>
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
Issyk-Kul	Issyk-Kul	k1gInSc4	Issyk-Kul
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ztotožnění	ztotožnění	k1gNnSc1	ztotožnění
s	s	k7c7	s
jezerem	jezero	k1gNnSc7	jezero
Issyk-Kul	Issyk-Kula	k1gFnPc2	Issyk-Kula
v	v	k7c6	v
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nesprávné	správný	k2eNgFnSc2d1	nesprávná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
přeneseny	přenést	k5eAaPmNgInP	přenést
do	do	k7c2	do
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
Salerna	Salerna	k1gFnSc1	Salerna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
tamní	tamní	k2eAgFnSc2d1	tamní
katedrály	katedrála	k1gFnSc2	katedrála
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Matouš	Matouš	k1gMnSc1	Matouš
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
andělem	anděl	k1gMnSc7	anděl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gNnSc3	on
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
diktoval	diktovat	k5eAaImAgMnS	diktovat
slova	slovo	k1gNnPc4	slovo
jeho	jeho	k3xOp3gNnSc2	jeho
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
zobrazení	zobrazení	k1gNnSc2	zobrazení
je	být	k5eAaImIp3nS	být
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
obraz	obraz	k1gInSc4	obraz
Povolání	povolání	k1gNnSc2	povolání
svatého	svatý	k2eAgMnSc2d1	svatý
Matouše	Matouš	k1gMnSc2	Matouš
od	od	k7c2	od
Caravaggia	Caravaggium	k1gNnSc2	Caravaggium
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kostele	kostel	k1gInSc6	kostel
San	San	k1gFnSc2	San
Luigi	Luig	k1gFnSc2	Luig
dei	dei	k?	dei
Francesi	Francese	k1gFnSc4	Francese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Matouš	Matouš	k1gMnSc1	Matouš
je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
účetních	účetní	k1gFnPc2	účetní
<g/>
,	,	kIx,	,
celníků	celník	k1gMnPc2	celník
a	a	k8xC	a
výběrčích	výběrčí	k1gMnPc2	výběrčí
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
Salerna	Salerna	k1gFnSc1	Salerna
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Matthew	Matthew	k1gMnSc1	Matthew
the	the	k?	the
Evangelist	Evangelist	k1gMnSc1	Evangelist
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
