<s>
Chorvatská	chorvatský	k2eAgFnSc1d1
mužská	mužský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Žebříček	žebříček	k1gInSc1
FIBA	FIBA	kA
<g/>
:	:	kIx,
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
místo	místo	k6eAd1
Člen	člen	k1gMnSc1
FIBA	FIBA	kA
od	od	k7c2
<g/>
:	:	kIx,
</s>
<s>
1992	#num#	k4
Svaz	svaz	k1gInSc1
FIBA	FIBA	kA
<g/>
:	:	kIx,
</s>
<s>
FIBA	FIBA	kA
Europe	Europ	k1gInSc5
Národní	národní	k2eAgFnSc1d1
svaz	svaz	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Hrvatski	Hrvatski	k6eAd1
košarkaški	košarkaški	k6eAd1
savez	savez	k1gMnSc1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Jasmin	Jasmin	k2eAgMnSc1d1
Repeša	Repeša	k1gMnSc1
Letní	letní	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
Účasti	účast	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
Medaile	medaile	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
stříbro	stříbro	k1gNnSc1
<g/>
:	:	kIx,
1992	#num#	k4
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
Účasti	účast	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
Medaile	medaile	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
bronz	bronz	k1gInSc1
<g/>
:	:	kIx,
1994	#num#	k4
Mistrovství	mistrovství	k1gNnPc2
Evropy	Evropa	k1gFnSc2
Účasti	účast	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
11	#num#	k4
<g/>
×	×	k?
Medaile	medaile	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
bronz	bronz	k1gInSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1
mužská	mužský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
Chorvatsko	Chorvatsko	k1gNnSc4
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
v	v	k7c6
basketbalu	basketbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
v	v	k7c6
historii	historie	k1gFnSc6
je	být	k5eAaImIp3nS
zisk	zisk	k1gInSc1
stříbrné	stříbrný	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
z	z	k7c2
Letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
1992	#num#	k4
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
Rok	rok	k1gInSc1
<g/>
/	/	kIx~
<g/>
pořádající	pořádající	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Účast	účast	k1gFnSc1
</s>
<s>
MS	MS	kA
1950	#num#	k4
Argentina	Argentina	k1gFnSc1
Argentina	Argentina	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1954	#num#	k4
Brazílie	Brazílie	k1gFnSc2
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1959	#num#	k4
Chile	Chile	k1gNnSc2
Chile	Chile	k1gNnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1963	#num#	k4
Brazílie	Brazílie	k1gFnSc2
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1967	#num#	k4
Uruguay	Uruguay	k1gFnSc1
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1970	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1974	#num#	k4
Portoriko	Portoriko	k1gNnSc1
Portoriko	Portoriko	k1gNnSc5
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1978	#num#	k4
Filipíny	Filipíny	k1gFnPc4
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1982	#num#	k4
Kolumbie	Kolumbie	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1986	#num#	k4
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1990	#num#	k4
Argentina	Argentina	k1gFnSc1
Argentina	Argentina	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
1994	#num#	k4
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
</s>
<s>
MS	MS	kA
1998	#num#	k4
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
2002	#num#	k4
USA	USA	kA
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
2006	#num#	k4
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
2010	#num#	k4
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
MS	MS	kA
2014	#num#	k4
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
MS	MS	kA
2019	#num#	k4
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
MS	MS	kA
2023	#num#	k4
Filipíny	Filipíny	k1gFnPc4
Filipíny	Filipíny	k1gFnPc4
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc4
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Indonésie	Indonésie	k1gFnSc1
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
CelkemÚčast	CelkemÚčast	k1gFnSc1
–	–	k?
3	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
–	–	k?
1	#num#	k4
<g/>
×	×	k?
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
Rok	rok	k1gInSc1
<g/>
/	/	kIx~
<g/>
pořádající	pořádající	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Účast	účast	k1gFnSc1
</s>
<s>
ME	ME	kA
1935	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1937	#num#	k4
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc5
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1939	#num#	k4
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1946	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1947	#num#	k4
ČSR	ČSR	kA
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1949	#num#	k4
Egypt	Egypt	k1gInSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1951	#num#	k4
Francie	Francie	k1gFnSc2
Francie	Francie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1953	#num#	k4
SSSR	SSSR	kA
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1955	#num#	k4
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1957	#num#	k4
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1959	#num#	k4
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1961	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1963	#num#	k4
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1965	#num#	k4
SSSR	SSSR	kA
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1967	#num#	k4
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1969	#num#	k4
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1971	#num#	k4
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1973	#num#	k4
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1975	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1977	#num#	k4
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1979	#num#	k4
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1981	#num#	k4
ČSSR	ČSSR	kA
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1983	#num#	k4
Francie	Francie	k1gFnSc2
Francie	Francie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1985	#num#	k4
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1987	#num#	k4
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1989	#num#	k4
Jugoslávie	Jugoslávie	k1gFnSc2
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1991	#num#	k4
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
ME	ME	kA
1993	#num#	k4
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
ME	ME	kA
1995	#num#	k4
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
</s>
<s>
ME	ME	kA
1997	#num#	k4
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
1999	#num#	k4
Francie	Francie	k1gFnSc2
Francie	Francie	k1gFnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2001	#num#	k4
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2003	#num#	k4
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2005	#num#	k4
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2007	#num#	k4
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2009	#num#	k4
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2011	#num#	k4
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2013	#num#	k4
Slovinsko	Slovinsko	k1gNnSc4
Slovinsko	Slovinsko	k1gNnSc4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2015	#num#	k4
Chorvatsko	Chorvatsko	k1gNnSc4
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2017	#num#	k4
Finsko	Finsko	k1gNnSc1
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ME	ME	kA
2022	#num#	k4
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc1
</s>
<s>
Ano	ano	k9
</s>
<s>
CelkemÚčast	CelkemÚčast	k1gFnSc1
–	–	k?
13	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
–	–	k?
2	#num#	k4
<g/>
×	×	k?
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
<g/>
/	/	kIx~
<g/>
pořádající	pořádající	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Účast	účast	k1gFnSc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
1992	#num#	k4
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
1996	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sydney	Sydney	k1gNnSc1
2000	#num#	k4
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
Atény	Atény	k1gFnPc1
2004	#num#	k4
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
Peking	Peking	k1gInSc1
2008	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Londýn	Londýn	k1gInSc1
2012	#num#	k4
</s>
<s>
Bez	bez	k7c2
účasti	účast	k1gFnSc2
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
2016	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Tokio	Tokio	k1gNnSc1
2020	#num#	k4
</s>
<s>
CelkemÚčast	CelkemÚčast	k1gFnSc1
–	–	k?
4	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
–	–	k?
1	#num#	k4
<g/>
×	×	k?
–	–	k?
0	#num#	k4
<g/>
×	×	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chorvatská	chorvatský	k2eAgFnSc1d1
mužská	mužský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
na	na	k7c4
Eurobasket	Eurobasket	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mužské	mužský	k2eAgInPc1d1
basketbalové	basketbalový	k2eAgInPc1d1
týmy	tým	k1gInPc1
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
FIBA	FIBA	kA
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Herzegovina	Herzegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Skotsko	Skotsko	k1gNnSc1
</s>
<s>
Wales	Wales	k1gInSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Zaniklé	zaniklý	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
