<s>
Ribozom	Ribozom	k1gInSc1	Ribozom
je	být	k5eAaImIp3nS	být
ribonukleoprotein	ribonukleoprotein	k1gMnSc1	ribonukleoprotein
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vysokých	vysoký	k2eAgInPc6d1	vysoký
počtech	počet	k1gInPc6	počet
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
také	také	k9	také
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hrubého	hrubý	k2eAgNnSc2d1	hrubé
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
proteinů	protein	k1gInPc2	protein
-	-	kIx~	-
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
tzv.	tzv.	kA	tzv.
translace	translace	k1gFnPc1	translace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
řetězce	řetězec	k1gInSc2	řetězec
RNA	RNA	kA	RNA
syntetizován	syntetizován	k2eAgInSc4d1	syntetizován
polypeptid	polypeptid	k1gInSc4	polypeptid
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomy	Ribozom	k1gInPc1	Ribozom
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc1d1	velká
komplexní	komplexní	k2eAgFnPc1d1	komplexní
struktury	struktura	k1gFnPc1	struktura
složeny	složen	k2eAgFnPc1d1	složena
zejména	zejména	k9	zejména
z	z	k7c2	z
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
podjednotky	podjednotka	k1gFnPc4	podjednotka
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc4d2	menší
a	a	k8xC	a
větší	veliký	k2eAgInPc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ribozomu	ribozom	k1gInSc3	ribozom
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
mediátorová	mediátorový	k2eAgFnSc1d1	mediátorová
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
mRNA	mRNA	k?	mRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přepis	přepis	k1gInSc4	přepis
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
např.	např.	kA	např.
z	z	k7c2	z
jaderného	jaderný	k2eAgInSc2d1	jaderný
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
trojic	trojice	k1gFnPc2	trojice
bází	báze	k1gFnSc7	báze
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
genetické	genetický	k2eAgFnSc6d1	genetická
informaci	informace	k1gFnSc6	informace
přichází	přicházet	k5eAaImIp3nS	přicházet
k	k	k7c3	k
ribozomu	ribozom	k1gInSc3	ribozom
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
napojené	napojený	k2eAgFnPc1d1	napojená
na	na	k7c6	na
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc1	tento
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
díky	díky	k7c3	díky
katalytickým	katalytický	k2eAgFnPc3d1	katalytická
vlastnostem	vlastnost	k1gFnPc3	vlastnost
ribozomu	ribozom	k1gInSc2	ribozom
spojeny	spojit	k5eAaPmNgFnP	spojit
v	v	k7c6	v
jeden	jeden	k4xCgInSc1	jeden
polypeptid	polypeptid	k1gInSc1	polypeptid
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
jistých	jistý	k2eAgFnPc6d1	jistá
úpravách	úprava	k1gFnPc6	úprava
<g/>
)	)	kIx)	)
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výzvou	výzva	k1gFnSc7	výzva
vždy	vždy	k6eAd1	vždy
bylo	být	k5eAaImAgNnS	být
určit	určit	k5eAaPmF	určit
přesnou	přesný	k2eAgFnSc4d1	přesná
atomární	atomární	k2eAgFnSc4d1	atomární
strukturu	struktura	k1gFnSc4	struktura
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objevy	objev	k1gInPc4	objev
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
třem	tři	k4xCgMnPc3	tři
významným	významný	k2eAgMnPc3d1	významný
vědcům	vědec	k1gMnPc3	vědec
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
<g/>
:	:	kIx,	:
Venkatramanu	Venkatraman	k1gMnSc3	Venkatraman
Ramakrishnanovi	Ramakrishnan	k1gMnSc3	Ramakrishnan
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
Thomasovi	Thomas	k1gMnSc6	Thomas
Steitzovi	Steitz	k1gMnSc6	Steitz
z	z	k7c2	z
Yalu	Yalus	k1gInSc2	Yalus
a	a	k8xC	a
Adě	Adě	k1gFnPc6	Adě
Jonat	Jonat	k1gInSc1	Jonat
z	z	k7c2	z
izraelského	izraelský	k2eAgNnSc2d1	izraelské
WIS	WIS	kA	WIS
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
všem	všecek	k3xTgFnPc3	všecek
se	se	k3xPyFc4	se
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	se	k3xPyFc3	se
podařilo	podařit	k5eAaPmAgNnS	podařit
určit	určit	k5eAaPmF	určit
trojrozměrnou	trojrozměrný	k2eAgFnSc4d1	trojrozměrná
stavbu	stavba	k1gFnSc4	stavba
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
velké	velký	k2eAgFnPc1d1	velká
podjednotky	podjednotka	k1gFnPc1	podjednotka
prokaryotických	prokaryotický	k2eAgInPc2d1	prokaryotický
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
však	však	k9	však
využívali	využívat	k5eAaPmAgMnP	využívat
metodu	metoda	k1gFnSc4	metoda
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
krystalografie	krystalografie	k1gFnSc2	krystalografie
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
oříškem	oříšek	k1gInSc7	oříšek
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
nejprve	nejprve	k6eAd1	nejprve
získat	získat	k5eAaPmF	získat
krystaly	krystal	k1gInPc1	krystal
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
Ada	Ada	kA	Ada
Yonath	Yonatha	k1gFnPc2	Yonatha
pokoušela	pokoušet	k5eAaImAgNnP	pokoušet
již	již	k6eAd1	již
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Používala	používat	k5eAaImAgFnS	používat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ribozomy	ribozom	k1gInPc1	ribozom
z	z	k7c2	z
termofilních	termofilní	k2eAgFnPc2d1	termofilní
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
halofilních	halofilní	k2eAgFnPc2d1	halofilní
archeí	arche	k1gFnPc2	arche
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
postupně	postupně	k6eAd1	postupně
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
získáván	získávat	k5eAaImNgInS	získávat
stále	stále	k6eAd1	stále
kvalitnější	kvalitní	k2eAgInSc1d2	kvalitnější
obraz	obraz	k1gInSc1	obraz
difraktujícího	difraktující	k2eAgInSc2d1	difraktující
krystalu	krystal	k1gInSc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
analýze	analýza	k1gFnSc3	analýza
přispěli	přispět	k5eAaPmAgMnP	přispět
zejména	zejména	k6eAd1	zejména
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
laureáti	laureát	k1gMnPc1	laureát
<g/>
,	,	kIx,	,
Ramakrishnan	Ramakrishnan	k1gMnSc1	Ramakrishnan
a	a	k8xC	a
Steitz	Steitz	k1gMnSc1	Steitz
<g/>
.	.	kIx.	.
</s>
<s>
Ribozom	Ribozom	k1gInSc1	Ribozom
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
ribonukleové	ribonukleový	k2eAgFnSc2d1	ribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
tzv.	tzv.	kA	tzv.
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
pak	pak	k6eAd1	pak
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
organizmy	organizmus	k1gInPc1	organizmus
mají	mít	k5eAaImIp3nP	mít
stavbu	stavba	k1gFnSc4	stavba
ribozomů	ribozom	k1gInPc2	ribozom
podobnou	podobný	k2eAgFnSc4d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
základní	základní	k2eAgNnSc4d1	základní
rozdělení	rozdělení	k1gNnSc4	rozdělení
části	část	k1gFnSc2	část
ribozomu	ribozom	k1gInSc2	ribozom
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc4d1	malá
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
podjednotku	podjednotka	k1gFnSc4	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
lze	lze	k6eAd1	lze
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
prokaryotickým	prokaryotický	k2eAgInSc7d1	prokaryotický
a	a	k8xC	a
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
ribozomem	ribozom	k1gMnSc7	ribozom
nalézt	nalézt	k5eAaBmF	nalézt
určité	určitý	k2eAgInPc4d1	určitý
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
ribozomy	ribozom	k1gInPc7	ribozom
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
udávají	udávat	k5eAaImIp3nP	udávat
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
sedimentačního	sedimentační	k2eAgInSc2d1	sedimentační
koeficientu	koeficient	k1gInSc2	koeficient
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
veličiny	veličina	k1gFnPc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
v	v	k7c6	v
ultracentrifuze	ultracentrifuga	k1gFnSc6	ultracentrifuga
sedimentace	sedimentace	k1gFnSc2	sedimentace
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
Svedberg	Svedberg	k1gInSc1	Svedberg
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
představuje	představovat	k5eAaImIp3nS	představovat
čas	čas	k1gInSc4	čas
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotický	Prokaryotický	k2eAgInSc1d1	Prokaryotický
ribozom	ribozom	k1gInSc1	ribozom
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
70	[number]	k4	70
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
eukaryotický	eukaryotický	k2eAgMnSc1d1	eukaryotický
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
<g/>
S.	S.	kA	S.
Také	také	k9	také
obě	dva	k4xCgFnPc1	dva
podjednotky	podjednotka	k1gFnPc1	podjednotka
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
určité	určitý	k2eAgInPc4d1	určitý
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
sedimentační	sedimentační	k2eAgInPc1d1	sedimentační
koeficienty	koeficient	k1gInPc1	koeficient
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
a	a	k8xC	a
prokaryotických	prokaryotický	k2eAgInPc2d1	prokaryotický
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
podjednotka	podjednotka	k1gFnSc1	podjednotka
prokaryot	prokaryota	k1gFnPc2	prokaryota
má	mít	k5eAaImIp3nS	mít
koeficient	koeficient	k1gInSc1	koeficient
30	[number]	k4	30
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
podjednotka	podjednotka	k1gFnSc1	podjednotka
40	[number]	k4	40
<g/>
S.	S.	kA	S.
Velká	velký	k2eAgFnSc1d1	velká
podjednotka	podjednotka	k1gFnSc1	podjednotka
ribozomu	ribozom	k1gInSc2	ribozom
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
50	[number]	k4	50
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
60	[number]	k4	60
<g/>
S.	S.	kA	S.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
rRNA	rRNA	k?	rRNA
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomální	Ribozomální	k2eAgInSc1d1	Ribozomální
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
esenciální	esenciální	k2eAgFnSc7d1	esenciální
složkou	složka	k1gFnSc7	složka
ribozomů	ribozom	k1gInPc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
rRNA	rRNA	k?	rRNA
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
funkčnost	funkčnost	k1gFnSc4	funkčnost
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
schopnost	schopnost	k1gFnSc1	schopnost
přepisovat	přepisovat	k5eAaImF	přepisovat
mRNA	mRNA	k?	mRNA
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
ribozomální	ribozomální	k2eAgInSc1d1	ribozomální
RNA	RNA	kA	RNA
vlastně	vlastně	k9	vlastně
enzym	enzym	k1gInSc4	enzym
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
proto	proto	k8xC	proto
ribozym	ribozym	k1gInSc4	ribozym
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
překvapivá	překvapivý	k2eAgFnSc1d1	překvapivá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymatické	enzymatický	k2eAgFnPc1d1	enzymatická
aktivity	aktivita	k1gFnPc1	aktivita
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
jen	jen	k9	jen
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomální	Ribozomální	k2eAgFnSc1d1	Ribozomální
RNA	RNA	kA	RNA
však	však	k8xC	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
prostorové	prostorový	k2eAgFnPc1d1	prostorová
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
aktivním	aktivní	k2eAgInSc7d1	aktivní
místům	místo	k1gNnPc3	místo
proteinů	protein	k1gInPc2	protein
fungujících	fungující	k2eAgFnPc2d1	fungující
jako	jako	k8xS	jako
enzymy	enzym	k1gInPc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
schopná	schopný	k2eAgFnSc1d1	schopná
například	například	k6eAd1	například
správně	správně	k6eAd1	správně
navázat	navázat	k5eAaPmF	navázat
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
také	také	k9	také
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vznik	vznik	k1gInSc1	vznik
peptidových	peptidový	k2eAgFnPc2d1	peptidová
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
řetězce	řetězec	k1gInSc2	řetězec
(	(	kIx(	(
<g/>
druhou	druhý	k4xOgFnSc4	druhý
z	z	k7c2	z
jmenovaných	jmenovaná	k1gFnPc2	jmenovaná
ovládá	ovládat	k5eAaImIp3nS	ovládat
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
především	především	k9	především
23S	[number]	k4	23S
rRNA	rRNA	k?	rRNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotický	Prokaryotický	k2eAgInSc1d1	Prokaryotický
a	a	k8xC	a
eukaryotický	eukaryotický	k2eAgInSc1d1	eukaryotický
ribozom	ribozom	k1gInSc1	ribozom
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
obsahu	obsah	k1gInSc6	obsah
rRNA	rRNA	k?	rRNA
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
prokaryotický	prokaryotický	k2eAgMnSc1d1	prokaryotický
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
podjednotce	podjednotka	k1gFnSc6	podjednotka
16S	[number]	k4	16S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
5S	[number]	k4	5S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
120	[number]	k4	120
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
a	a	k8xC	a
23S	[number]	k4	23S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
2900	[number]	k4	2900
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eukaryotický	eukaryotický	k2eAgMnSc1d1	eukaryotický
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
podjednotce	podjednotka	k1gFnSc6	podjednotka
18S	[number]	k4	18S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
5S	[number]	k4	5S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
120	[number]	k4	120
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5,8	[number]	k4	5,8
<g/>
S	s	k7c7	s
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
160	[number]	k4	160
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
a	a	k8xC	a
28S	[number]	k4	28S
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
4700	[number]	k4	4700
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ribozomy	ribozom	k1gInPc1	ribozom
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotické	Prokaryotický	k2eAgInPc1d1	Prokaryotický
ribozomy	ribozom	k1gInPc1	ribozom
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
55	[number]	k4	55
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
21	[number]	k4	21
v	v	k7c6	v
malé	malá	k1gFnSc6	malá
a	a	k8xC	a
34	[number]	k4	34
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eukaryotické	eukaryotický	k2eAgInPc1d1	eukaryotický
ribozomy	ribozom	k1gInPc1	ribozom
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
82	[number]	k4	82
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
33	[number]	k4	33
v	v	k7c6	v
malé	malá	k1gFnSc6	malá
a	a	k8xC	a
49	[number]	k4	49
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
translace	translace	k1gFnSc2	translace
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc2	biologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
malá	malý	k2eAgFnSc1d1	malá
podjednotka	podjednotka	k1gFnSc1	podjednotka
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
S	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k6eAd1	především
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
ocitly	ocitnout	k5eAaPmAgInP	ocitnout
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
tRNA	trnout	k5eAaImSgInS	trnout
s	s	k7c7	s
přinášenými	přinášený	k2eAgFnPc7d1	přinášená
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
i	i	k8xC	i
translační	translační	k2eAgInPc1d1	translační
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
podjednotka	podjednotka	k1gFnSc1	podjednotka
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
S	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
peptidyltransferáza	peptidyltransferáza	k1gFnSc1	peptidyltransferáza
umožňující	umožňující	k2eAgFnSc1d1	umožňující
vznik	vznik	k1gInSc4	vznik
peptidové	peptidový	k2eAgFnSc2d1	peptidová
vazby	vazba	k1gFnSc2	vazba
ve	v	k7c6	v
vznikajícím	vznikající	k2eAgInSc6d1	vznikající
polypeptidu	polypeptid	k1gInSc6	polypeptid
<g/>
.	.	kIx.	.
</s>
<s>
Transferová	transferový	k2eAgFnSc1d1	transferová
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
svým	svůj	k3xOyFgMnSc7	svůj
antikodonem	antikodon	k1gMnSc7	antikodon
na	na	k7c4	na
kodon	kodon	k1gInSc4	kodon
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
rozeznána	rozeznán	k2eAgFnSc1d1	rozeznána
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměnám	záměna	k1gFnPc3	záměna
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
a	a	k8xC	a
chybnému	chybný	k2eAgNnSc3d1	chybné
čtení	čtení	k1gNnSc3	čtení
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
RNA	RNA	kA	RNA
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
podjednotce	podjednotka	k1gFnSc6	podjednotka
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
S	s	k7c7	s
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
molekulární	molekulární	k2eAgNnSc1d1	molekulární
pravítko	pravítko	k1gNnSc1	pravítko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nukleotidy	nukleotid	k1gInPc1	nukleotid
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
můstky	můstek	k1gInPc1	můstek
s	s	k7c7	s
nukleotidy	nukleotid	k1gInPc7	nukleotid
kodonu	kodon	k1gInSc2	kodon
i	i	k8xC	i
antikodonu	antikodon	k1gInSc2	antikodon
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
navázaly	navázat	k5eAaPmAgFnP	navázat
a	a	k8xC	a
prostorově	prostorově	k6eAd1	prostorově
zorientovaly	zorientovat	k5eAaPmAgFnP	zorientovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
krok	krok	k1gInSc1	krok
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
ribozom	ribozom	k1gInSc4	ribozom
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
nutné	nutný	k2eAgNnSc1d1	nutné
navázat	navázat	k5eAaPmF	navázat
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
na	na	k7c4	na
prodlužující	prodlužující	k2eAgInSc4d1	prodlužující
se	se	k3xPyFc4	se
polypeptid	polypeptid	k1gInSc4	polypeptid
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
"	"	kIx"	"
<g/>
peptidyl-transferázové	peptidylransferázový	k2eAgNnSc1d1	peptidyl-transferázový
centrum	centrum	k1gNnSc1	centrum
<g/>
"	"	kIx"	"
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
ribozomální	ribozomální	k2eAgFnSc6d1	ribozomální
podjednotce	podjednotka	k1gFnSc6	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
opět	opět	k6eAd1	opět
ribozomální	ribozomální	k2eAgMnSc1d1	ribozomální
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterium	k1gNnPc2	bakterium
23S	[number]	k4	23S
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
molekula	molekula	k1gFnSc1	molekula
tRNA	trnout	k5eAaImSgInS	trnout
nesoucí	nesoucí	k2eAgFnSc7d1	nesoucí
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
<g/>
,	,	kIx,	,
molekuly	molekula	k1gFnPc4	molekula
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
ribozomální	ribozomální	k2eAgInPc4d1	ribozomální
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
buňku	buňka	k1gFnSc4	buňka
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgNnSc1d1	zásadní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
dostatek	dostatek	k1gInSc4	dostatek
ribozomů	ribozom	k1gInPc2	ribozom
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
neustále	neustále	k6eAd1	neustále
syntetizovány	syntetizován	k2eAgFnPc1d1	syntetizována
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
stavebních	stavební	k2eAgFnPc2d1	stavební
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
