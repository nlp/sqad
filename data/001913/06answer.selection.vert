<s>
Prokaryotické	Prokaryotický	k2eAgInPc1d1	Prokaryotický
ribozomy	ribozom	k1gInPc1	ribozom
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
55	[number]	k4	55
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
21	[number]	k4	21
v	v	k7c6	v
malé	malá	k1gFnSc6	malá
a	a	k8xC	a
34	[number]	k4	34
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eukaryotické	eukaryotický	k2eAgInPc1d1	eukaryotický
ribozomy	ribozom	k1gInPc1	ribozom
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
82	[number]	k4	82
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
33	[number]	k4	33
v	v	k7c6	v
malé	malá	k1gFnSc6	malá
a	a	k8xC	a
49	[number]	k4	49
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
podjednotce	podjednotka	k1gFnSc6	podjednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
