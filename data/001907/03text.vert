<s>
Plastid	plastid	k1gInSc1	plastid
je	být	k5eAaImIp3nS	být
eukaryotická	eukaryotický	k2eAgFnSc1d1	eukaryotická
semiautonomní	semiautonomní	k2eAgFnSc1d1	semiautonomní
organela	organela	k1gFnSc1	organela
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
různých	různý	k2eAgFnPc2d1	různá
řas	řasa	k1gFnPc2	řasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
chloroplasty	chloroplast	k1gInPc7	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
však	však	k9	však
plastidy	plastid	k1gInPc1	plastid
ztrácí	ztrácet	k5eAaImIp3nP	ztrácet
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
se	se	k3xPyFc4	se
k	k	k7c3	k
funkcím	funkce	k1gFnPc3	funkce
jiným	jiný	k2eAgFnPc3d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
pak	pak	k6eAd1	pak
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
zásobní	zásobní	k2eAgFnSc1d1	zásobní
organela	organela	k1gFnSc1	organela
nebo	nebo	k8xC	nebo
odpovídat	odpovídat	k5eAaImF	odpovídat
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
zabarvení	zabarvení	k1gNnSc4	zabarvení
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obalen	obalen	k2eAgMnSc1d1	obalen
nejméně	málo	k6eAd3	málo
dvěma	dva	k4xCgFnPc7	dva
membránami	membrána	k1gFnPc7	membrána
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
té	ten	k3xDgFnSc2	ten
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stroma	stroma	k1gNnSc1	stroma
s	s	k7c7	s
thylakoidy	thylakoid	k1gInPc7	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
Plastid	plastid	k1gInSc1	plastid
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
typický	typický	k2eAgInSc1d1	typický
obsahem	obsah	k1gInSc7	obsah
tzv.	tzv.	kA	tzv.
plastidové	plastidový	k2eAgNnSc1d1	plastidový
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
pDNA	pDNA	k?	pDNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
důležitý	důležitý	k2eAgInSc1d1	důležitý
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
mimojaderné	mimojaderný	k2eAgFnSc6d1	mimojaderná
dědičnosti	dědičnost	k1gFnSc6	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
endosymbiotická	endosymbiotický	k2eAgFnSc1d1	endosymbiotická
teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
eukaryogeneze	eukaryogeneze	k1gFnSc1	eukaryogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
endosymbiotické	endosymbiotický	k2eAgFnSc2d1	endosymbiotická
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
bývalých	bývalý	k2eAgInPc2d1	bývalý
endosymbiontů	endosymbiont	k1gInPc2	endosymbiont
sinicového	sinicový	k2eAgInSc2d1	sinicový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
v	v	k7c6	v
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
případech	případ	k1gInPc6	případ
u	u	k7c2	u
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
druhů	druh	k1gInPc2	druh
řas	řasa	k1gFnPc2	řasa
však	však	k9	však
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
plastidy	plastid	k1gInPc1	plastid
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc4	potomek
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
symbiontů	symbiont	k1gInPc2	symbiont
typu	typ	k1gInSc2	typ
řas	řasa	k1gFnPc2	řasa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sekundární	sekundární	k2eAgFnSc1d1	sekundární
endosymbióza	endosymbióza	k1gFnSc1	endosymbióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
evoluční	evoluční	k2eAgFnSc6d1	evoluční
historii	historie	k1gFnSc6	historie
až	až	k9	až
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samotné	samotný	k2eAgFnSc2d1	samotná
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
samotné	samotný	k2eAgFnPc1d1	samotná
eukaryogeneze	eukaryogeneze	k1gFnPc1	eukaryogeneze
se	se	k3xPyFc4	se
neúčastnily	účastnit	k5eNaImAgFnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
se	se	k3xPyFc4	se
plastidy	plastid	k1gInPc1	plastid
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
u	u	k7c2	u
několika	několik	k4yIc6	několik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
nepříbuzných	příbuzný	k2eNgFnPc2d1	nepříbuzná
skupin	skupina	k1gFnPc2	skupina
eukaryot	eukaryot	k1gMnSc1	eukaryot
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
primární	primární	k2eAgInPc4d1	primární
plastidy	plastid	k1gInPc4	plastid
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
sinice	sinice	k1gFnSc2	sinice
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nejen	nejen	k6eAd1	nejen
zelené	zelený	k2eAgFnPc1d1	zelená
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Viridiplantae	Viridiplanta	k1gFnPc1	Viridiplanta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ruduchy	ruducha	k1gFnSc2	ruducha
(	(	kIx(	(
<g/>
Rhodophyta	Rhodophyt	k1gInSc2	Rhodophyt
<g/>
)	)	kIx)	)
a	a	k8xC	a
glaukofyty	glaukofyt	k1gInPc1	glaukofyt
(	(	kIx(	(
<g/>
Glaucophyta	Glaucophyta	k1gFnSc1	Glaucophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
řasy	řasa	k1gFnPc1	řasa
Paulinella	Paulinell	k1gMnSc2	Paulinell
chromatophora	chromatophor	k1gMnSc2	chromatophor
a	a	k8xC	a
Paulinella	Paulinell	k1gMnSc2	Paulinell
longichromatophora	longichromatophor	k1gMnSc2	longichromatophor
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
získaly	získat	k5eAaPmAgInP	získat
plastidy	plastid	k1gInPc1	plastid
nedávno	nedávno	k6eAd1	nedávno
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
fotosyntetizujících	fotosyntetizující	k2eAgFnPc2d1	fotosyntetizující
eukaryot	eukaryota	k1gFnPc2	eukaryota
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
především	především	k6eAd1	především
sekundární	sekundární	k2eAgFnSc7d1	sekundární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pohlcením	pohlcení	k1gNnSc7	pohlcení
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
terciární	terciární	k2eAgFnSc7d1	terciární
<g/>
.	.	kIx.	.
</s>
<s>
Pohlcením	pohlcení	k1gNnSc7	pohlcení
ruduchy	ruducha	k1gFnSc2	ruducha
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
plastidy	plastid	k1gInPc1	plastid
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
kvůli	kvůli	k7c3	kvůli
svému	své	k1gNnSc3	své
původu	původ	k1gInSc2	původ
zvané	zvaný	k2eAgInPc1d1	zvaný
rhodoplasty	rhodoplast	k1gInPc1	rhodoplast
<g/>
)	)	kIx)	)
např.	např.	kA	např.
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
heterokont	heterokonta	k1gFnPc2	heterokonta
(	(	kIx(	(
<g/>
Heterokonta	Heterokonta	k1gFnSc1	Heterokonta
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozsivek	rozsivka	k1gFnPc2	rozsivka
(	(	kIx(	(
<g/>
Bacillariophyceae	Bacillariophyceae	k1gFnSc1	Bacillariophyceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohlcením	pohlcení	k1gNnSc7	pohlcení
zelené	zelená	k1gFnSc2	zelená
řasy	řasa	k1gFnSc2	řasa
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
plastidy	plastid	k1gInPc1	plastid
u	u	k7c2	u
Chlorarachniophyta	Chlorarachniophyt	k1gInSc2	Chlorarachniophyt
<g/>
,	,	kIx,	,
některých	některý	k3yIgNnPc2	některý
krásnooček	krásnoočko	k1gNnPc2	krásnoočko
(	(	kIx(	(
<g/>
Euglenozoa	Euglenozo	k2eAgFnSc1d1	Euglenozo
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
obrněnky	obrněnka	k1gFnSc2	obrněnka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
prokázané	prokázaný	k2eAgFnSc2d1	prokázaná
terciární	terciární	k2eAgFnSc2d1	terciární
endosymbiózy	endosymbióza	k1gFnSc2	endosymbióza
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
obrněnky	obrněnka	k1gFnPc4	obrněnka
rodu	rod	k1gInSc2	rod
Kryptoperidinium	Kryptoperidinium	k1gNnSc1	Kryptoperidinium
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
plastidy	plastid	k1gInPc1	plastid
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
pohlcené	pohlcený	k2eAgFnSc2d1	pohlcená
rozsivky	rozsivka	k1gFnSc2	rozsivka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
proplastid	proplastid	k1gInSc1	proplastid
<g/>
.	.	kIx.	.
</s>
<s>
Proplastid	Proplastid	k1gInSc1	Proplastid
je	být	k5eAaImIp3nS	být
nezralý	zralý	k2eNgInSc1d1	nezralý
plastid	plastid	k1gInSc1	plastid
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
dělivých	dělivý	k2eAgNnPc2d1	dělivé
pletiv	pletivo	k1gNnPc2	pletivo
a	a	k8xC	a
v	v	k7c6	v
mladých	mladý	k2eAgFnPc6d1	mladá
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
diferenciace	diferenciace	k1gFnSc2	diferenciace
a	a	k8xC	a
zrání	zrání	k1gNnSc2	zrání
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
plastidů	plastid	k1gInPc2	plastid
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
chloroplast	chloroplast	k1gInSc1	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgInPc1d1	aktivní
plastidy	plastid	k1gInPc1	plastid
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgNnPc4d1	aktivní
barviva	barvivo	k1gNnPc4	barvivo
a	a	k8xC	a
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
tedy	tedy	k9	tedy
probíhá	probíhat	k5eAaImIp3nS	probíhat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zachycení	zachycení	k1gNnSc2	zachycení
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
chloroplast	chloroplast	k1gInSc1	chloroplast
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgInSc4d1	aktivní
zelený	zelený	k2eAgInSc4d1	zelený
plastid	plastid	k1gInSc4	plastid
obsahující	obsahující	k2eAgInPc4d1	obsahující
chlorofyly	chlorofyl	k1gInPc4	chlorofyl
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
jej	on	k3xPp3gMnSc4	on
např.	např.	kA	např.
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
obalové	obalový	k2eAgFnSc2d1	obalová
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
odškrcuje	odškrcovat	k5eAaImIp3nS	odškrcovat
váčky	váček	k1gInPc4	váček
tylakoidy	tylakoida	k1gFnSc2	tylakoida
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
třetí	třetí	k4xOgFnSc4	třetí
membránovou	membránový	k2eAgFnSc4d1	membránová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
grana	grana	k6eAd1	grana
(	(	kIx(	(
<g/>
sloupcovité	sloupcovitý	k2eAgInPc1d1	sloupcovitý
shluky	shluk	k1gInPc1	shluk
mincovitých	mincovitý	k2eAgFnPc2d1	mincovitý
tylakoidních	tylakoidní	k2eAgFnPc2d1	tylakoidní
váčku	váček	k1gInSc3	váček
<g/>
)	)	kIx)	)
a	a	k8xC	a
lamely	lamela	k1gFnSc2	lamela
neboli	neboli	k8xC	neboli
stromální	stromální	k2eAgFnSc2d1	stromální
tylakoidy	tylakoida	k1gFnSc2	tylakoida
(	(	kIx(	(
<g/>
propojující	propojující	k2eAgInPc1d1	propojující
můstky	můstek	k1gInPc1	můstek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
obsahu	obsah	k1gInSc2	obsah
jistých	jistý	k2eAgNnPc2d1	jisté
fotosyntetických	fotosyntetický	k2eAgNnPc2d1	fotosyntetické
barviv	barvivo	k1gNnPc2	barvivo
či	či	k8xC	či
výskytu	výskyt	k1gInSc2	výskyt
u	u	k7c2	u
určitých	určitý	k2eAgFnPc2d1	určitá
skupin	skupina	k1gFnPc2	skupina
organizmů	organizmus	k1gInPc2	organizmus
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivních	aktivní	k2eAgInPc2d1	aktivní
plastidů	plastid	k1gInPc2	plastid
<g/>
.	.	kIx.	.
</s>
<s>
Rodoplast	Rodoplast	k1gInSc1	Rodoplast
(	(	kIx(	(
<g/>
též	též	k9	též
rhodoplast	rhodoplast	k1gInSc1	rhodoplast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgInSc1d1	aktivní
červený	červený	k2eAgInSc1d1	červený
plastid	plastid	k1gInSc1	plastid
obsahující	obsahující	k2eAgInSc4d1	obsahující
fykoerytrin	fykoerytrin	k1gInSc4	fykoerytrin
(	(	kIx(	(
<g/>
červený	červený	k2eAgMnSc1d1	červený
<g/>
)	)	kIx)	)
a	a	k8xC	a
fykocyanin	fykocyanin	k2eAgInSc1d1	fykocyanin
(	(	kIx(	(
<g/>
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
třeba	třeba	k6eAd1	třeba
u	u	k7c2	u
ruduch	ruducha	k1gFnPc2	ruducha
(	(	kIx(	(
<g/>
Rhodophyta	Rhodophyta	k1gFnSc1	Rhodophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Feoplast	Feoplast	k1gInSc1	Feoplast
je	být	k5eAaImIp3nS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgInSc1d1	aktivní
plastid	plastid	k1gInSc1	plastid
obsahující	obsahující	k2eAgInSc4d1	obsahující
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
fukoxantin	fukoxantin	k1gInSc4	fukoxantin
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
u	u	k7c2	u
hnědých	hnědý	k2eAgFnPc2d1	hnědá
řas	řasa	k1gFnPc2	řasa
(	(	kIx(	(
<g/>
Phaeophyta	Phaeophyta	k1gFnSc1	Phaeophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chromoplast	chromoplast	k1gInSc1	chromoplast
je	být	k5eAaImIp3nS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
nebo	nebo	k8xC	nebo
červený	červený	k2eAgInSc1d1	červený
plastid	plastid	k1gInSc1	plastid
obsahující	obsahující	k2eAgInSc1d1	obsahující
pouze	pouze	k6eAd1	pouze
pomocné	pomocný	k2eAgInPc1d1	pomocný
fotosyntetické	fotosyntetický	k2eAgInPc1d1	fotosyntetický
pigmenty	pigment	k1gInPc1	pigment
(	(	kIx(	(
<g/>
karotenoidy-karoteny	karotenoidyaroten	k1gInPc1	karotenoidy-karoten
<g/>
,	,	kIx,	,
xantofyly	xantofyl	k1gInPc1	xantofyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
fotosyntetizovat	fotosyntetizovat	k5eAaPmF	fotosyntetizovat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
sběrače	sběrač	k1gInPc4	sběrač
fotonů	foton	k1gInPc2	foton
pro	pro	k7c4	pro
chloroplasty	chloroplast	k1gInPc4	chloroplast
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
bývalý	bývalý	k2eAgInSc1d1	bývalý
chloroplast	chloroplast	k1gInSc1	chloroplast
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postrádá	postrádat	k5eAaImIp3nS	postrádat
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
především	především	k9	především
ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zbarvit	zbarvit	k5eAaPmF	zbarvit
povrch	povrch	k1gInSc4	povrch
plodu	plod	k1gInSc2	plod
nápadnou	nápadný	k2eAgFnSc7d1	nápadná
barvou	barva	k1gFnSc7	barva
aby	aby	kYmCp3nS	aby
přilákal	přilákat	k5eAaPmAgMnS	přilákat
konzumenty	konzument	k1gMnPc4	konzument
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
tak	tak	k6eAd1	tak
šíření	šíření	k1gNnSc4	šíření
semen	semeno	k1gNnPc2	semeno
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
podzimní	podzimní	k2eAgFnSc4d1	podzimní
barvu	barva	k1gFnSc4	barva
listí	listí	k1gNnSc2	listí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
je	být	k5eAaImIp3nS	být
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
již	již	k6eAd1	již
rozložen	rozložit	k5eAaPmNgInS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Leukoplasty	leukoplast	k1gInPc1	leukoplast
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc4	plastid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádná	žádný	k3yNgNnPc4	žádný
barviva	barvivo	k1gNnPc4	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
<g/>
:	:	kIx,	:
Proteinoplast	Proteinoplast	k1gInSc1	Proteinoplast
-	-	kIx~	-
plastid	plastid	k1gInSc1	plastid
obsahující	obsahující	k2eAgInPc1d1	obsahující
proteiny	protein	k1gInPc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Amyloplast	Amyloplast	k1gInSc1	Amyloplast
-	-	kIx~	-
plastid	plastid	k1gInSc1	plastid
shromažďující	shromažďující	k2eAgInSc1d1	shromažďující
škrob	škrob	k1gInSc4	škrob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
tkáních	tkáň	k1gFnPc6	tkáň
díky	díky	k7c3	díky
nim	on	k3xPp3gNnPc3	on
rostlina	rostlina	k1gFnSc1	rostlina
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
kde	kde	k6eAd1	kde
dole	dole	k6eAd1	dole
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
statolit	statolit	k1gInSc1	statolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elaioplast	Elaioplast	k1gInSc1	Elaioplast
-	-	kIx~	-
plastid	plastid	k1gInSc1	plastid
shromažďující	shromažďující	k2eAgInPc1d1	shromažďující
oleje	olej	k1gInPc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
plastidová	plastidový	k2eAgFnSc1d1	plastidová
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plastidech	plastid	k1gInPc6	plastid
je	být	k5eAaImIp3nS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
přítomnost	přítomnost	k1gFnSc1	přítomnost
kruhových	kruhový	k2eAgFnPc2d1	kruhová
molekul	molekula	k1gFnPc2	molekula
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xC	jako
pDNA	pDNA	k?	pDNA
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
plastidových	plastidový	k2eAgNnPc2d1	plastidový
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
prokaryotického	prokaryotický	k2eAgInSc2d1	prokaryotický
genomu	genom	k1gInSc2	genom
sinice	sinice	k1gFnSc2	sinice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
eukaryogeneze	eukaryogeneze	k1gFnSc2	eukaryogeneze
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
eukaryotem	eukaryot	k1gMnSc7	eukaryot
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
někdy	někdy	k6eAd1	někdy
vznikají	vznikat	k5eAaImIp3nP	vznikat
plastidy	plastid	k1gInPc1	plastid
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
sekundární	sekundární	k2eAgFnSc7d1	sekundární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
rovněž	rovněž	k9	rovněž
genom	genom	k1gInSc4	genom
sinice	sinice	k1gFnSc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
skupin	skupina	k1gFnPc2	skupina
tzv.	tzv.	kA	tzv.
nukleomorf	nukleomorf	k1gInSc1	nukleomorf
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zbytkový	zbytkový	k2eAgInSc1d1	zbytkový
genom	genom	k1gInSc1	genom
jiného	jiný	k2eAgMnSc2d1	jiný
eukaryota	eukaryot	k1gMnSc2	eukaryot
po	po	k7c6	po
sekundární	sekundární	k2eAgFnSc6d1	sekundární
endosymbiotické	endosymbiotický	k2eAgFnSc6d1	endosymbiotická
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
pDNA	pDNA	k?	pDNA
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
redukované	redukovaný	k2eAgInPc1d1	redukovaný
plastidové	plastidový	k2eAgInPc1d1	plastidový
genomy	genom	k1gInPc1	genom
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
zejména	zejména	k9	zejména
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
fotosyntetickou	fotosyntetický	k2eAgFnSc4d1	fotosyntetická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvotní	prvotní	k2eAgFnSc4d1	prvotní
fázi	fáze	k1gFnSc4	fáze
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
procesu	proces	k1gInSc2	proces
endosymbiotického	endosymbiotický	k2eAgInSc2d1	endosymbiotický
vzniku	vznik	k1gInSc2	vznik
druhotných	druhotný	k2eAgInPc2d1	druhotný
plastidů	plastid	k1gInPc2	plastid
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
tzv.	tzv.	kA	tzv.
kleptoplastii	kleptoplastie	k1gFnSc4	kleptoplastie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zabudovávání	zabudovávání	k1gNnSc1	zabudovávání
živých	živý	k2eAgInPc2d1	živý
plastidů	plastid	k1gInPc2	plastid
získaných	získaný	k2eAgInPc2d1	získaný
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
hostitelského	hostitelský	k2eAgMnSc2d1	hostitelský
<g/>
,	,	kIx,	,
normálně	normálně	k6eAd1	normálně
heterotrofního	heterotrofní	k2eAgInSc2d1	heterotrofní
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
mořských	mořský	k2eAgMnPc2d1	mořský
plžů	plž	k1gMnPc2	plž
živících	živící	k2eAgMnPc2d1	živící
se	s	k7c7	s
zelenými	zelený	k2eAgFnPc7d1	zelená
řasami	řasa	k1gFnPc7	řasa
bylo	být	k5eAaImAgNnS	být
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
epitelových	epitelový	k2eAgFnPc2d1	epitelová
buněk	buňka	k1gFnPc2	buňka
zabudovávají	zabudovávat	k5eAaImIp3nP	zabudovávat
chloroplasty	chloroplast	k1gInPc4	chloroplast
z	z	k7c2	z
pozřených	pozřený	k2eAgFnPc2d1	pozřená
řas	řasa	k1gFnPc2	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
plže	plž	k1gMnSc4	plž
Elysia	elysium	k1gNnSc2	elysium
atroviridis	atroviridis	k1gFnSc2	atroviridis
<g/>
,	,	kIx,	,
Elysiella	Elysiella	k1gFnSc1	Elysiella
pusilla	pusilla	k1gFnSc1	pusilla
<g/>
,	,	kIx,	,
Elysia	elysium	k1gNnPc1	elysium
tuca	tuca	k1gFnSc1	tuca
<g/>
,	,	kIx,	,
Bosellia	Bosellia	k1gFnSc1	Bosellia
mimetica	mimetica	k1gFnSc1	mimetica
<g/>
,	,	kIx,	,
Oxynoe	Oxynoe	k1gFnSc1	Oxynoe
antillarum	antillarum	k1gInSc1	antillarum
či	či	k8xC	či
Plakobranchus	Plakobranchus	k1gMnSc1	Plakobranchus
ocellatus	ocellatus	k1gMnSc1	ocellatus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plastidy	plastid	k1gInPc1	plastid
přežívají	přežívat	k5eAaImIp3nP	přežívat
až	až	k9	až
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
kleptoplastidy	kleptoplastida	k1gFnSc2	kleptoplastida
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
kleptoplasty	kleptoplast	k1gInPc1	kleptoplast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
plžů	plž	k1gMnPc2	plž
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
každá	každý	k3xTgFnSc1	každý
generace	generace	k1gFnSc1	generace
znovu	znovu	k6eAd1	znovu
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
potravního	potravní	k2eAgInSc2d1	potravní
nedostatku	nedostatek	k1gInSc2	nedostatek
jsou	být	k5eAaImIp3nP	být
funkční	funkční	k2eAgInPc1d1	funkční
kleptoplastidy	kleptoplastid	k1gInPc1	kleptoplastid
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
plžů	plž	k1gMnPc2	plž
–	–	k?	–
ti	ten	k3xDgMnPc1	ten
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
mixotrofní	mixotrofní	k2eAgNnSc4d1	mixotrofní
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
autotrofní	autotrofní	k2eAgInSc4d1	autotrofní
způsob	způsob	k1gInSc4	způsob
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
výživě	výživa	k1gFnSc6	výživa
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
přirozené	přirozený	k2eAgFnSc2d1	přirozená
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
však	však	k9	však
prokázán	prokázán	k2eAgMnSc1d1	prokázán
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kleptoplastidy	Kleptoplastid	k1gInPc1	Kleptoplastid
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
hostitelských	hostitelský	k2eAgInPc2d1	hostitelský
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obrněnek	obrněnka	k1gFnPc2	obrněnka
Gymnodinium	Gymnodinium	k1gNnSc1	Gymnodinium
spp	spp	k?	spp
<g/>
.	.	kIx.	.
a	a	k8xC	a
Pfisteria	Pfisterium	k1gNnSc2	Pfisterium
piscicida	piscicida	k1gFnSc1	piscicida
jsou	být	k5eAaImIp3nP	být
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgMnPc1d1	aktivní
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
u	u	k7c2	u
Dinophysis	Dinophysis	k1gFnSc2	Dinophysis
spp	spp	k?	spp
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nálevník	nálevník	k1gMnSc1	nálevník
Myrionecta	Myrionect	k1gMnSc2	Myrionect
rubra	rubrum	k1gNnSc2	rubrum
získává	získávat	k5eAaImIp3nS	získávat
kleptoplastidy	kleptoplastid	k1gInPc4	kleptoplastid
z	z	k7c2	z
kryptomonády	kryptomonáda	k1gFnSc2	kryptomonáda
Geminigera	Geminiger	k1gMnSc2	Geminiger
cryophila	cryophil	k1gMnSc2	cryophil
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
dírkonošců	dírkonošec	k1gInPc2	dírkonošec
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Bulimina	Bulimin	k2eAgFnSc1d1	Bulimin
<g/>
,	,	kIx,	,
Elphidium	Elphidium	k1gNnSc1	Elphidium
<g/>
,	,	kIx,	,
Haynesina	Haynesina	k1gFnSc1	Haynesina
<g/>
,	,	kIx,	,
Nonion	Nonion	k1gInSc1	Nonion
<g/>
,	,	kIx,	,
Nonionella	Nonionella	k1gFnSc1	Nonionella
<g/>
,	,	kIx,	,
Nonionellina	Nonionellina	k1gFnSc1	Nonionellina
<g/>
,	,	kIx,	,
Reophax	Reophax	k1gInSc1	Reophax
a	a	k8xC	a
Stainforthia	Stainforthia	k1gFnSc1	Stainforthia
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
kleptoplastidy	kleptoplastida	k1gFnPc1	kleptoplastida
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
obrněnek	obrněnka	k1gFnPc2	obrněnka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
organely	organela	k1gFnSc2	organela
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
chloroplastu	chloroplast	k1gInSc2	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
některých	některý	k3yIgFnPc2	některý
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
syntéza	syntéza	k1gFnSc1	syntéza
kondenzovaných	kondenzovaný	k2eAgInPc2d1	kondenzovaný
taninů	tanin	k1gInPc2	tanin
-	-	kIx~	-
polyfenolů	polyfenol	k1gInPc2	polyfenol
<g/>
,	,	kIx,	,
využívaných	využívaný	k2eAgInPc2d1	využívaný
rostlinami	rostlina	k1gFnPc7	rostlina
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
býložravými	býložravý	k2eAgMnPc7d1	býložravý
škůdci	škůdce	k1gMnPc7	škůdce
a	a	k8xC	a
ultrafialovým	ultrafialový	k2eAgInSc7d1	ultrafialový
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
dostala	dostat	k5eAaPmAgFnS	dostat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
tanin	tanin	k1gInSc4	tanin
+	+	kIx~	+
lat.	lat.	k?	lat.
soma	soma	k1gFnSc1	soma
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tělísko	tělísko	k1gNnSc1	tělísko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tanozomy	Tanozom	k1gInPc1	Tanozom
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
jako	jako	k9	jako
malé	malý	k2eAgInPc4d1	malý
kulovité	kulovitý	k2eAgInPc4d1	kulovitý
váčky	váček	k1gInPc4	váček
(	(	kIx(	(
<g/>
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
kolem	kolem	k7c2	kolem
30	[number]	k4	30
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
uvnitř	uvnitř	k7c2	uvnitř
chloroplastu	chloroplast	k1gInSc2	chloroplast
z	z	k7c2	z
thylakoidů	thylakoid	k1gInPc2	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
Shluky	shluk	k1gInPc1	shluk
tanozomů	tanozom	k1gInPc2	tanozom
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostávají	dostávat	k5eAaImIp3nP	dostávat
ven	ven	k6eAd1	ven
z	z	k7c2	z
chloroplastu	chloroplast	k1gInSc2	chloroplast
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
transportních	transportní	k2eAgInPc2d1	transportní
váčků	váček	k1gInPc2	váček
obalených	obalený	k2eAgInPc2d1	obalený
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
fúzí	fúze	k1gFnSc7	fúze
dvou	dva	k4xCgFnPc2	dva
obalových	obalový	k2eAgFnPc2d1	obalová
chloroplastových	chloroplastův	k2eAgFnPc2d1	chloroplastův
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Transportní	transportní	k2eAgInPc1d1	transportní
váčky	váček	k1gInPc1	váček
přenášejí	přenášet	k5eAaImIp3nP	přenášet
taniny	tanin	k1gInPc4	tanin
v	v	k7c6	v
tanozomech	tanozom	k1gInPc6	tanozom
do	do	k7c2	do
vakuoly	vakuola	k1gFnSc2	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
váček	váček	k1gInSc1	váček
se	se	k3xPyFc4	se
vchlípí	vchlípit	k5eAaPmIp3nS	vchlípit
do	do	k7c2	do
vakuoly	vakuola	k1gFnSc2	vakuola
a	a	k8xC	a
z	z	k7c2	z
tonoplastu	tonoplast	k1gInSc2	tonoplast
tak	tak	k6eAd1	tak
získá	získat	k5eAaPmIp3nS	získat
další	další	k2eAgFnSc4d1	další
obalovou	obalový	k2eAgFnSc4d1	obalová
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Taninové	taninový	k2eAgFnPc1d1	taninová
akrece	akrece	k1gFnPc1	akrece
ve	v	k7c6	v
vakuolách	vakuola	k1gFnPc6	vakuola
tak	tak	k6eAd1	tak
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
taniny	tanin	k1gInPc1	tanin
uložené	uložený	k2eAgInPc1d1	uložený
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
membránách	membrána	k1gFnPc6	membrána
<g/>
.	.	kIx.	.
</s>
