<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
plastid	plastid	k1gInSc1	plastid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
fykoerytrin	fykoerytrin	k1gInSc4	fykoerytrin
a	a	k8xC	a
fykocyanin	fykocyanin	k2eAgInSc4d1	fykocyanin
<g/>
?	?	kIx.	?
</s>
