<s>
Játrovky	játrovka	k1gFnPc1	játrovka
(	(	kIx(	(
<g/>
Marchantiophyta	Marchantiophyta	k1gFnSc1	Marchantiophyta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
stélkaté	stélkatý	k2eAgFnPc1d1	stélkatý
zelené	zelený	k2eAgFnPc1d1	zelená
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
řadí	řadit	k5eAaImIp3nS	řadit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mechy	mech	k1gInPc7	mech
a	a	k8xC	a
hlevíky	hlevík	k1gInPc7	hlevík
mezi	mezi	k7c4	mezi
mechorosty	mechorost	k1gInPc4	mechorost
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
podle	podle	k7c2	podle
zástupců	zástupce	k1gMnPc2	zástupce
rodu	rod	k1gInSc2	rod
Marchantia	Marchantia	k1gFnSc1	Marchantia
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
porostnice	porostnice	k1gFnSc1	porostnice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
porostnice	porostnice	k1gFnSc1	porostnice
mnohotvárná	mnohotvárný	k2eAgFnSc1d1	mnohotvárná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejhojnějších	hojný	k2eAgFnPc2d3	nejhojnější
játrovek	játrovka	k1gFnPc2	játrovka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řady	řada	k1gFnSc2	řada
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zastaralých	zastaralý	k2eAgInPc2d1	zastaralý
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
skupinu	skupina	k1gFnSc4	skupina
užívaly	užívat	k5eAaImAgFnP	užívat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zmínit	zmínit	k5eAaPmF	zmínit
např.	např.	kA	např.
Hepaticae	Hepaticae	k1gFnSc1	Hepaticae
nebo	nebo	k8xC	nebo
Hepaticophyta	Hepaticophyta	k1gFnSc1	Hepaticophyta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
latinského	latinský	k2eAgMnSc2d1	latinský
hepar	hepar	k1gMnSc1	hepar
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
a	a	k8xC	a
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
tak	tak	k6eAd1	tak
na	na	k7c4	na
typický	typický	k2eAgInSc4d1	typický
vzhled	vzhled	k1gInSc4	vzhled
mnoha	mnoho	k4c2	mnoho
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Játrovky	játrovka	k1gFnPc1	játrovka
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
stélek	stélka	k1gFnPc2	stélka
<g/>
.	.	kIx.	.
</s>
<s>
Vývojově	vývojově	k6eAd1	vývojově
nejpůvodnější	původní	k2eAgFnSc1d3	nejpůvodnější
je	být	k5eAaImIp3nS	být
stélka	stélka	k1gFnSc1	stélka
lupenitá	lupenitý	k2eAgFnSc1d1	lupenitá
(	(	kIx(	(
<g/>
frondosní	frondosní	k2eAgFnSc1d1	frondosní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
rozlišená	rozlišený	k2eAgFnSc1d1	rozlišená
na	na	k7c4	na
stonek	stonek	k1gInSc4	stonek
a	a	k8xC	a
listy	list	k1gInPc4	list
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
ploše	plocha	k1gFnSc3	plocha
listovité	listovitý	k2eAgInPc4d1	listovitý
útvary	útvar	k1gInPc4	útvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Marchantia	Marchantia	k1gFnSc1	Marchantia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přichycené	přichycený	k2eAgNnSc4d1	přichycené
k	k	k7c3	k
substrátu	substrát	k1gInSc3	substrát
jednobuněčnými	jednobuněčný	k2eAgFnPc7d1	jednobuněčná
příchytnými	příchytný	k2eAgNnPc7d1	příchytné
vlákny	vlákno	k1gNnPc7	vlákno
(	(	kIx(	(
<g/>
rhizoidy	rhizoid	k1gInPc7	rhizoid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stélku	stélka	k1gFnSc4	stélka
vývojově	vývojově	k6eAd1	vývojově
nejpokročilejších	pokročilý	k2eAgFnPc2d3	nejpokročilejší
játrovek	játrovka	k1gFnPc2	játrovka
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
stélka	stélka	k1gFnSc1	stélka
listnatá	listnatý	k2eAgNnPc4d1	listnaté
(	(	kIx(	(
<g/>
foliosní	foliosní	k2eAgNnPc4d1	foliosní
<g/>
)	)	kIx)	)
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
diferencovaným	diferencovaný	k2eAgInSc7d1	diferencovaný
stonkem	stonek	k1gInSc7	stonek
(	(	kIx(	(
<g/>
kauloid	kauloid	k1gInSc4	kauloid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listy	list	k1gInPc4	list
(	(	kIx(	(
<g/>
fyloidy	fyloida	k1gFnPc4	fyloida
<g/>
)	)	kIx)	)
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
též	též	k9	též
s	s	k7c7	s
rhizoidy	rhizoid	k1gInPc7	rhizoid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Chiloscyphus	Chiloscyphus	k1gInSc4	Chiloscyphus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
přechodné	přechodný	k2eAgFnPc4d1	přechodná
formy	forma	k1gFnPc4	forma
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
předchozími	předchozí	k2eAgFnPc7d1	předchozí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Fossombronia	Fossombronium	k1gNnSc2	Fossombronium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
játrovek	játrovka	k1gFnPc2	játrovka
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
stinná	stinný	k2eAgNnPc4d1	stinné
či	či	k8xC	či
polostinná	polostinný	k2eAgNnPc4d1	polostinné
stanoviště	stanoviště	k1gNnPc4	stanoviště
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
druhového	druhový	k2eAgNnSc2d1	druhové
bohatství	bohatství	k1gNnSc2	bohatství
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
lesích	les	k1gInPc6	les
na	na	k7c6	na
tlejícím	tlející	k2eAgNnSc6d1	tlející
dřevě	dřevo	k1gNnSc6	dřevo
(	(	kIx(	(
<g/>
Chiloscyphus	Chiloscyphus	k1gMnSc1	Chiloscyphus
<g/>
,	,	kIx,	,
Lepidozia	Lepidozia	k1gFnSc1	Lepidozia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
mokvajících	mokvající	k2eAgFnPc6d1	mokvající
skalách	skála	k1gFnPc6	skála
různého	různý	k2eAgNnSc2d1	různé
geologického	geologický	k2eAgNnSc2d1	geologické
složení	složení	k1gNnSc2	složení
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
kůře	kůra	k1gFnSc6	kůra
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
Radula	Radula	k1gFnSc1	Radula
complanata	complanata	k1gFnSc1	complanata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
čerstvě	čerstvě	k6eAd1	čerstvě
rozrušené	rozrušený	k2eAgFnSc6d1	rozrušená
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
kamenech	kámen	k1gInPc6	kámen
koryt	koryto	k1gNnPc2	koryto
potoků	potok	k1gInPc2	potok
(	(	kIx(	(
<g/>
Conocephalum	Conocephalum	k1gInSc1	Conocephalum
conicum	conicum	k1gInSc1	conicum
<g/>
,	,	kIx,	,
Pellia	Pellia	k1gFnSc1	Pellia
epiphylla	epiphylla	k1gFnSc1	epiphylla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
i	i	k9	i
suché	suchý	k2eAgNnSc1d1	suché
xerofytní	xerofytný	k2eAgMnPc1d1	xerofytný
trávníky	trávník	k1gInPc1	trávník
(	(	kIx(	(
<g/>
Mannia	Mannium	k1gNnPc1	Mannium
fragrans	fragransa	k1gFnPc2	fragransa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
patří	patřit	k5eAaImIp3nS	patřit
játrovky	játrovka	k1gFnPc4	játrovka
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
stromové	stromový	k2eAgInPc4d1	stromový
epifyty	epifyt	k1gInPc4	epifyt
<g/>
.	.	kIx.	.
</s>
<s>
Játrovky	játrovka	k1gFnPc1	játrovka
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
skupin	skupina	k1gFnPc2	skupina
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
datují	datovat	k5eAaImIp3nP	datovat
jejich	jejich	k3xOp3gInSc4	jejich
vznik	vznik	k1gInSc4	vznik
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
svrchního	svrchní	k2eAgInSc2d1	svrchní
karbonu	karbon	k1gInSc2	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Játrovky	játrovka	k1gFnPc1	játrovka
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
stélky	stélka	k1gFnSc2	stélka
pohárky	pohárek	k1gInPc4	pohárek
(	(	kIx(	(
<g/>
thalidia	thalidium	k1gNnSc2	thalidium
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozmnožovacími	rozmnožovací	k2eAgNnPc7d1	rozmnožovací
tělísky	tělísko	k1gNnPc7	tělísko
(	(	kIx(	(
<g/>
gemy	gem	k1gInPc7	gem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
nepohlavnímu	pohlavní	k2eNgNnSc3d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
pohlavně	pohlavně	k6eAd1	pohlavně
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
spor	spora	k1gFnPc2	spora
produkovaných	produkovaný	k2eAgFnPc2d1	produkovaná
samičími	samičí	k2eAgInPc7d1	samičí
pohlavními	pohlavní	k2eAgInPc7d1	pohlavní
orgány	orgán	k1gInPc7	orgán
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
oplození	oplození	k1gNnSc6	oplození
bičíkatými	bičíkatý	k2eAgFnPc7d1	bičíkatá
samčími	samčí	k2eAgFnPc7d1	samčí
gametami	gameta	k1gFnPc7	gameta
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Játrovky	játrovka	k1gFnSc2	játrovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
