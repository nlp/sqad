<s>
Hendrik	Hendrik	k1gMnSc1	Hendrik
Antoon	Antoon	k1gMnSc1	Antoon
Lorentz	Lorentz	k1gMnSc1	Lorentz
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1853	[number]	k4	1853
Arnhem	Arnh	k1gInSc7	Arnh
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc4	Holandsko
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
Haarlem	Haarl	k1gInSc7	Haarl
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc4	Holandsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
laureát	laureát	k1gMnSc1	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
obdržel	obdržet	k5eAaPmAgMnS	obdržet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pieterem	Pieter	k1gMnSc7	Pieter
Zeemanem	Zeeman	k1gMnSc7	Zeeman
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
vlivu	vliv	k1gInSc2	vliv
magnetismu	magnetismus	k1gInSc2	magnetismus
na	na	k7c6	na
záření	záření	k1gNnSc6	záření
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1853	[number]	k4	1853
v	v	k7c6	v
Arnhemu	Arnhem	k1gInSc6	Arnhem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Gerrit	Gerrita	k1gFnPc2	Gerrita
Frederik	Frederik	k1gMnSc1	Frederik
Lorentz	Lorentz	k1gMnSc1	Lorentz
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Geertruida	Geertruida	k1gFnSc1	Geertruida
van	vana	k1gFnPc2	vana
Ginkel	Ginkela	k1gFnPc2	Ginkela
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1866-1869	[number]	k4	1866-1869
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
nově	nově	k6eAd1	nově
zřízenou	zřízený	k2eAgFnSc4d1	zřízená
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Arnhemu	Arnhem	k1gInSc6	Arnhem
<g/>
,	,	kIx,	,
a	a	k8xC	a
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
složil	složit	k5eAaPmAgMnS	složit
zkoušku	zkouška	k1gFnSc4	zkouška
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
učitelem	učitel	k1gMnSc7	učitel
astronomie	astronomie	k1gFnSc2	astronomie
Frederikem	Frederik	k1gMnSc7	Frederik
Kaiserem	Kaiser	k1gMnSc7	Kaiser
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Arnhemu	Arnhem	k1gInSc2	Arnhem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
letech	léto	k1gNnPc6	léto
obhájil	obhájit	k5eAaPmAgMnS	obhájit
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
disertační	disertační	k2eAgFnSc4d1	disertační
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Odraz	odraz	k1gInSc1	odraz
a	a	k8xC	a
lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
Maxwellovy	Maxwellův	k2eAgFnSc2d1	Maxwellova
teorie	teorie	k1gFnSc2	teorie
<g/>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Over	Over	k1gInSc1	Over
de	de	k?	de
theorie	theorie	k1gFnSc2	theorie
der	drát	k5eAaImRp2nS	drát
terugkaatsing	terugkaatsing	k1gInSc1	terugkaatsing
en	en	k?	en
breking	breking	k1gInSc1	breking
van	van	k1gInSc1	van
het	het	k?	het
licht	licht	k1gInSc1	licht
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
nová	nový	k2eAgFnSc1d1	nová
katedra	katedra	k1gFnSc1	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
úvodní	úvodní	k2eAgFnSc1d1	úvodní
přednáška	přednáška	k1gFnSc1	přednáška
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1878	[number]	k4	1878
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Molekulová	molekulový	k2eAgFnSc1d1	molekulová
teorie	teorie	k1gFnSc1	teorie
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
De	De	k?	De
moleculaire	moleculair	k1gInSc5	moleculair
theorien	theorien	k1gInSc1	theorien
v	v	k7c6	v
de	de	k?	de
natuurkunde	natuurkund	k1gInSc5	natuurkund
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
P.	P.	kA	P.
Zeemanem	Zeeman	k1gInSc7	Zeeman
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
objev	objev	k1gInSc4	objev
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
Zeemanova	Zeemanův	k2eAgInSc2d1	Zeemanův
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
výzkumu	výzkum	k1gInSc2	výzkum
Teylerova	Teylerův	k2eAgInSc2d1	Teylerův
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgMnS	zůstat
ale	ale	k9	ale
čestným	čestný	k2eAgMnSc7d1	čestný
profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
Haarlemu	Haarlem	k1gInSc6	Haarlem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pořádal	pořádat	k5eAaImAgInS	pořádat
jednou	jednou	k6eAd1	jednou
týdně	týdně	k6eAd1	týdně
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
v	v	k7c6	v
Haarlemu	Haarlem	k1gInSc6	Haarlem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
teorii	teorie	k1gFnSc4	teorie
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vztah	vztah	k1gInSc4	vztah
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
magnetismu	magnetismus	k1gInSc2	magnetismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
světla	světlo	k1gNnPc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
obory	obor	k1gInPc4	obor
fyziky	fyzika	k1gFnSc2	fyzika
-	-	kIx~	-
přispěl	přispět	k5eAaPmAgMnS	přispět
svými	svůj	k3xOyFgMnPc7	svůj
pracemi	práce	k1gFnPc7	práce
k	k	k7c3	k
mechanice	mechanika	k1gFnSc3	mechanika
<g/>
,	,	kIx,	,
termodynamice	termodynamika	k1gFnSc6	termodynamika
<g/>
,	,	kIx,	,
hydrodynamice	hydrodynamika	k1gFnSc6	hydrodynamika
<g/>
,	,	kIx,	,
kinetické	kinetický	k2eAgFnSc6d1	kinetická
teorii	teorie	k1gFnSc6	teorie
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
pevné	pevný	k2eAgFnSc2d1	pevná
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
příspěvky	příspěvek	k1gInPc1	příspěvek
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
<g/>
,	,	kIx,	,
elektronové	elektronový	k2eAgFnSc2d1	elektronová
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
Maxwellovu	Maxwellův	k2eAgFnSc4d1	Maxwellova
teorii	teorie	k1gFnSc4	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
a	a	k8xC	a
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základu	základ	k1gInSc6	základ
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zdroje	zdroj	k1gInPc4	zdroj
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
považoval	považovat	k5eAaImAgMnS	považovat
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
oscilující	oscilující	k2eAgFnPc1d1	oscilující
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
elektrony	elektron	k1gInPc1	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
silné	silný	k2eAgNnSc1d1	silné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
generovaného	generovaný	k2eAgNnSc2d1	generované
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
experimentálně	experimentálně	k6eAd1	experimentálně
prokázal	prokázat	k5eAaPmAgMnS	prokázat
jeho	jeho	k3xOp3gNnSc4	jeho
žák	žák	k1gMnSc1	žák
P.	P.	kA	P.
Zeeman	Zeeman	k1gMnSc1	Zeeman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
publikoval	publikovat	k5eAaBmAgMnS	publikovat
práci	práce	k1gFnSc4	práce
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
světelný	světelný	k2eAgInSc4d1	světelný
paprsek	paprsek	k1gInSc4	paprsek
šíří	šíř	k1gFnPc2	šíř
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
Lorentz-Lorenzovou	Lorentz-Lorenzový	k2eAgFnSc7d1	Lorentz-Lorenzový
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vyložil	vyložit	k5eAaPmAgMnS	vyložit
výsledek	výsledek	k1gInSc4	výsledek
Michelson-Morleyova	Michelson-Morleyův	k2eAgInSc2d1	Michelson-Morleyův
experimentu	experiment	k1gInSc2	experiment
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kontrakce	kontrakce	k1gFnSc2	kontrakce
délek	délka	k1gFnPc2	délka
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
transformaci	transformace	k1gFnSc4	transformace
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
nemění	měnit	k5eNaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
transformace	transformace	k1gFnPc4	transformace
popisující	popisující	k2eAgInSc1d1	popisující
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
navzájem	navzájem	k6eAd1	navzájem
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
se	se	k3xPyFc4	se
pohybujícími	pohybující	k2eAgFnPc7d1	pohybující
soustavami	soustava	k1gFnPc7	soustava
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
jako	jako	k9	jako
Lorentzovy	Lorentzův	k2eAgFnPc1d1	Lorentzova
transformace	transformace	k1gFnPc1	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
také	také	k9	také
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
Lorentzovou	Lorentzový	k2eAgFnSc7d1	Lorentzový
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
Cauchy-Lorentzovým	Cauchy-Lorentzův	k2eAgNnSc7d1	Cauchy-Lorentzův
rozdělením	rozdělení	k1gNnSc7	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
Michelson-Morleyův	Michelson-Morleyův	k2eAgInSc4d1	Michelson-Morleyův
experiment	experiment	k1gInSc4	experiment
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zkracování	zkracování	k1gNnSc3	zkracování
délek	délka	k1gFnPc2	délka
pohybujících	pohybující	k2eAgNnPc2d1	pohybující
se	se	k3xPyFc4	se
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
kontrakci	kontrakce	k1gFnSc3	kontrakce
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
závěru	závěr	k1gInSc3	závěr
došel	dojít	k5eAaPmAgMnS	dojít
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
George	George	k1gNnPc2	George
FitzGerald	FitzGeralda	k1gFnPc2	FitzGeralda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
zavedl	zavést	k5eAaPmAgInS	zavést
termín	termín	k1gInSc1	termín
místní	místní	k2eAgInSc4d1	místní
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
současností	současnost	k1gFnSc7	současnost
nepohyblivého	pohyblivý	k2eNgNnSc2d1	nepohyblivé
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
přidal	přidat	k5eAaPmAgMnS	přidat
časovou	časový	k2eAgFnSc4d1	časová
dilataci	dilatace	k1gFnSc4	dilatace
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
transformacím	transformace	k1gFnPc3	transformace
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k9	co
Poincaré	Poincarý	k2eAgInPc1d1	Poincarý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
nazval	nazvat	k5eAaPmAgMnS	nazvat
Lorentzovými	Lorentzův	k2eAgFnPc7d1	Lorentzova
transformacemi	transformace	k1gFnPc7	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Lorentz	Lorentz	k1gMnSc1	Lorentz
patrně	patrně	k6eAd1	patrně
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
časovou	časový	k2eAgFnSc4d1	časová
dilataci	dilatace	k1gFnSc4	dilatace
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
již	již	k6eAd1	již
Joseph	Joseph	k1gMnSc1	Joseph
Larmor	Larmor	k1gMnSc1	Larmor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
časovou	časový	k2eAgFnSc4d1	časová
dilataci	dilatace	k1gFnSc4	dilatace
při	při	k7c6	při
oběhu	oběh	k1gInSc6	oběh
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
identické	identický	k2eAgFnPc4d1	identická
transformace	transformace	k1gFnPc4	transformace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vypadají	vypadat	k5eAaPmIp3nP	vypadat
Larmorovy	Larmorův	k2eAgFnPc1d1	Larmorova
a	a	k8xC	a
Lorenzovy	Lorenzův	k2eAgFnPc1d1	Lorenzova
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
algebraicky	algebraicky	k6eAd1	algebraicky
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
shodné	shodný	k2eAgFnPc1d1	shodná
s	s	k7c7	s
rovnicemi	rovnice	k1gFnPc7	rovnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
publikoval	publikovat	k5eAaBmAgInS	publikovat
Henri	Henre	k1gFnSc4	Henre
Poincaré	Poincarý	k2eAgFnSc2d1	Poincarý
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
matematické	matematický	k2eAgFnPc1d1	matematická
formulace	formulace	k1gFnPc1	formulace
popisují	popisovat	k5eAaImIp3nP	popisovat
základní	základní	k2eAgInPc4d1	základní
jevy	jev	k1gInPc4	jev
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
-	-	kIx~	-
růst	růst	k1gInSc1	růst
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
kontrakce	kontrakce	k1gFnSc2	kontrakce
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
dilataci	dilatace	k1gFnSc4	dilatace
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Alettou	Alettý	k2eAgFnSc7d1	Alettý
Catharinou	Catharin	k2eAgFnSc7d1	Catharina
Kaiserovou	Kaiserová	k1gFnSc7	Kaiserová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Johanna	Johann	k1gMnSc2	Johann
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Kaisera	Kaiser	k1gMnSc2	Kaiser
a	a	k8xC	a
neteří	neteř	k1gFnSc7	neteř
Frederika	Frederik	k1gMnSc2	Frederik
Kaisera	Kaiser	k1gMnSc2	Kaiser
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
