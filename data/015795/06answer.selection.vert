<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
305	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
německy	německy	k6eAd1
Kuhberg	Kuhberg	k1gInSc1
či	či	k8xC
Urnberg	Urnberg	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
hantecu	hantecus	k1gInSc6
Monte	Mont	k1gInSc5
Bú	bú	k0
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
lidově	lidově	k6eAd1
též	též	k9
Kravák	kravák	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
s	s	k7c7
městským	městský	k2eAgInSc7d1
parkem	park	k1gInSc7
a	a	k8xC
hvězdárnou	hvězdárna	k1gFnSc7
a	a	k8xC
planetáriem	planetárium	k1gNnSc7
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>