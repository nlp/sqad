<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Bobravská	Bobravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
Park	park	k1gInSc1
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
305	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Bobravská	Bobravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
305	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
německy	německy	k6eAd1
Kuhberg	Kuhberg	k1gInSc1
či	či	k8xC
Urnberg	Urnberg	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
hantecu	hantecus	k1gInSc6
Monte	Mont	k1gInSc5
Bú	bú	k0
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
lidově	lidově	k6eAd1
též	též	k9
Kravák	kravák	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
s	s	k7c7
městským	městský	k2eAgInSc7d1
parkem	park	k1gInSc7
a	a	k8xC
hvězdárnou	hvězdárna	k1gFnSc7
a	a	k8xC
planetáriem	planetárium	k1gNnSc7
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
asi	asi	k9
kilometr	kilometr	k1gInSc4
severozápadně	severozápadně	k6eAd1
od	od	k7c2
vrchu	vrch	k1gInSc2
s	s	k7c7
hradem	hrad	k1gInSc7
Špilberk	Špilberk	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Veveří	veveří	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
sousedním	sousední	k2eAgInSc7d1
Žlutým	žlutý	k2eAgInSc7d1
kopcem	kopec	k1gInSc7
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
mělkým	mělký	k2eAgNnSc7d1
sedlem	sedlo	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
náměstí	náměstí	k1gNnSc1
Míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologicky	geomorfologicky	k6eAd1
patří	patřit	k5eAaImIp3nS
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
do	do	k7c2
Bobravské	Bobravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kopec	kopec	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
středověku	středověk	k1gInSc2
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
jako	jako	k8xS,k8xC
pastvina	pastvina	k1gFnSc1
krav	kráva	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
toho	ten	k3xDgNnSc2
nejspíše	nejspíše	k9
pochází	pocházet	k5eAaImIp3nS
název	název	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
roku	rok	k1gInSc2
1346	#num#	k4
psán	psán	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Chueberck	Chueberck	k1gMnSc1
čili	čili	k8xC
Kuhberg	Kuhberg	k1gMnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
kuh	kuh	k?
–	–	k?
kráva	kráva	k1gFnSc1
<g/>
,	,	kIx,
berg	berg	k1gInSc1
–	–	k?
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
vinicemi	vinice	k1gFnPc7
na	na	k7c6
kopci	kopec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
kopci	kopec	k1gInSc6
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
také	také	k9
vinice	vinice	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
již	již	k6eAd1
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xC,k8xS
Cimpl	Cimpl	k1gFnPc2
<g/>
,	,	kIx,
Cimpel	Cimpela	k1gFnPc2
či	či	k8xC
Zimpffen	Zimpffna	k1gFnPc2
<g/>
,	,	kIx,
Zimpelberg	Zimpelberg	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgInS,k5eAaImAgInS
pramen	pramen	k1gInSc1
Žabího	žabí	k2eAgInSc2d1
potoka	potok	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Czimpelquelle	Czimpelquelle	k1gFnSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
fonte	font	k1gInSc5
Czimpl	Czimpl	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
vytékal	vytékat	k5eAaImAgMnS
v	v	k7c6
údolí	údolí	k1gNnSc6
mezi	mezi	k7c7
Kraví	kraví	k2eAgFnSc7d1
horou	hora	k1gFnSc7
a	a	k8xC
Žlutým	žlutý	k2eAgInSc7d1
kopcem	kopec	k1gInSc7
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
Údolní	údolní	k2eAgFnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
napájel	napájet	k5eAaImAgInS
městský	městský	k2eAgInSc1d1
vodovod	vodovod	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
obléhání	obléhání	k1gNnSc2
Brna	Brno	k1gNnSc2
Švédy	švéda	k1gFnSc2
roku	rok	k1gInSc2
1645	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Kraví	kraví	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
a	a	k8xC
Žlutém	žlutý	k2eAgInSc6d1
kopci	kopec	k1gInSc6
nacházely	nacházet	k5eAaImAgFnP
<g />
.	.	kIx.
</s>
<s hack="1">
šance	šance	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1655	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gFnPc6
nacházely	nacházet	k5eAaImAgInP
prachárny	prachárna	k1gFnSc2
(	(	kIx(
<g/>
skladiště	skladiště	k1gNnSc1
střeliva	střelivo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Občas	občas	k6eAd1
je	být	k5eAaImIp3nS
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
spojována	spojovat	k5eAaImNgFnS
se	s	k7c7
starým	starý	k2eAgInSc7d1
názvem	název	k1gInSc7
Urnberg	Urnberg	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c4
ten	ten	k3xDgInSc4
je	být	k5eAaImIp3nS
však	však	k9
označován	označovat	k5eAaImNgInS
Žlutý	žlutý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
toho	ten	k3xDgNnSc2
starý	starý	k2eAgInSc4d1
název	název	k1gInSc4
ulice	ulice	k1gFnSc2
Tomešova	Tomešův	k2eAgInSc2d1
<g/>
)	)	kIx)
či	či	k8xC
přesněji	přesně	k6eAd2
vrchol	vrchol	k1gInSc4
u	u	k7c2
Wilsonova	Wilsonův	k2eAgInSc2d1
lesu	les	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Administrativně	administrativně	k6eAd1
patří	patřit	k5eAaImIp3nS
většina	většina	k1gFnSc1
Kraví	kraví	k2eAgFnSc2d1
hory	hora	k1gFnSc2
do	do	k7c2
brněnské	brněnský	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-střed	Brno-střed	k1gInSc1
<g/>
,	,	kIx,
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
Veveří	veveří	k2eAgNnPc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
severozápadní	severozápadní	k2eAgNnPc4d1
úbočí	úbočí	k1gNnPc4
náleží	náležet	k5eAaImIp3nP
městské	městský	k2eAgInPc1d1
části	část	k1gFnPc4
a	a	k8xC
k.	k.	k?
ú.	ú.	k?
Žabovřesky	Žabovřesky	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jediná	jediný	k2eAgFnSc1d1
část	část	k1gFnSc1
Kraví	kraví	k2eAgFnSc2d1
hory	hora	k1gFnSc2
využitá	využitý	k2eAgFnSc1d1
pro	pro	k7c4
obytnou	obytný	k2eAgFnSc4d1
zástavbu	zástavba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
zejména	zejména	k9
rekreaci	rekreace	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
park	park	k1gInSc4
<g/>
,	,	kIx,
několik	několik	k4yIc1
hřišť	hřiště	k1gNnPc2
a	a	k8xC
koupaliště	koupaliště	k1gNnSc2
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
kopce	kopec	k1gInSc2
pak	pak	k6eAd1
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Brno	Brno	k1gNnSc1
a	a	k8xC
dvojice	dvojice	k1gFnSc1
vodojemů	vodojem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
temene	temeno	k1gNnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
areály	areál	k1gInPc1
technického	technický	k2eAgNnSc2d1
zázemí	zázemí	k1gNnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
a	a	k8xC
provozy	provoz	k1gInPc4
soukromých	soukromý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
níže	nízce	k6eAd2
při	při	k7c6
úpatí	úpatí	k1gNnSc6
pak	pak	k6eAd1
stojí	stát	k5eAaImIp3nS
Gymnázium	gymnázium	k1gNnSc4
Matyáše	Matyáš	k1gMnSc2
Lercha	Lerch	k1gMnSc2
a	a	k8xC
budovy	budova	k1gFnSc2
stavební	stavební	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
VUT	VUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	nedaleko	k7c2
koupaliště	koupaliště	k1gNnSc2
se	se	k3xPyFc4
při	při	k7c6
ulici	ulice	k1gFnSc6
Údolní	údolní	k2eAgNnSc1d1
nachází	nacházet	k5eAaImIp3nS
Centrum	centrum	k1gNnSc1
léčivých	léčivý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
LF	LF	kA
MU	MU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
na	na	k7c6
celém	celý	k2eAgNnSc6d1
východním	východní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
kopce	kopec	k1gInSc2
pokrývá	pokrývat	k5eAaImIp3nS
zahrádkářská	zahrádkářský	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
konci	konec	k1gInSc6
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
prudce	prudko	k6eAd1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
kolmo	kolmo	k6eAd1
(	(	kIx(
<g/>
vysekaným	vysekaný	k2eAgInSc7d1
odřezem	odřez	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
ulic	ulice	k1gFnPc2
Grohova	Grohova	k1gFnSc1
a	a	k8xC
Úvoz	úvoz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
existoval	existovat	k5eAaImAgInS
plán	plán	k1gInSc1
vystavět	vystavět	k5eAaPmF
na	na	k7c6
Kraví	kraví	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
brněnský	brněnský	k2eAgInSc1d1
univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gFnSc3
realizaci	realizace	k1gFnSc3
však	však	k9
nedošlo	dojít	k5eNaPmAgNnS
a	a	k8xC
současný	současný	k2eAgInSc1d1
kampus	kampus	k1gInSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
Bohunicích	Bohunice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Kraví	kraví	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
mají	mít	k5eAaImIp3nP
svá	svůj	k3xOyFgNnPc4
hřiště	hřiště	k1gNnPc4
baseballový	baseballový	k2eAgInSc1d1
a	a	k8xC
softballový	softballový	k2eAgInSc1d1
klub	klub	k1gInSc1
VSK	VSK	kA
Technika	technik	k1gMnSc2
Brno	Brno	k1gNnSc1
a	a	k8xC
házenkářský	házenkářský	k2eAgInSc1d1
klub	klub	k1gInSc1
Draken	Drakna	k1gFnPc2
Brno	Brno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
centra	centrum	k1gNnSc2
Brna	Brno	k1gNnSc2
dostupná	dostupný	k2eAgFnSc1d1
tramvajovou	tramvajový	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
4	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
konečnou	konečný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Náměstí	náměstí	k1gNnSc2
Míru	mír	k1gInSc2
v	v	k7c6
jejím	její	k3xOp3gNnSc6
bezprostředním	bezprostřední	k2eAgNnSc6d1
sousedství	sousedství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
druhé	druhý	k4xOgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
Žabovřesk	Žabovřesky	k1gFnPc2
<g/>
,	,	kIx,
sem	sem	k6eAd1
zajíždí	zajíždět	k5eAaImIp3nS
autobusová	autobusový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
projíždí	projíždět	k5eAaImIp3nS
přes	přes	k7c4
sousední	sousední	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
Míru	mír	k1gInSc2
linka	linka	k1gFnSc1
N	N	kA
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GOLIS	GOLIS	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
;	;	kIx,
JANÍK	Janík	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
Monte	Mont	k1gInSc5
Bú	bú	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-04-30	2012-04-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FLODROVÁ	FLODROVÁ	kA
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názvy	název	k1gInPc1
brněnských	brněnský	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
<g/>
,	,	kIx,
náměstí	náměstí	k1gNnSc2
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
veřejných	veřejný	k2eAgNnPc2d1
prostranství	prostranství	k1gNnPc2
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Šimon	Šimon	k1gMnSc1
Ryšavý	Ryšavý	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
342	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7354	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
73	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
137	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
FLODROVÁ	FLODROVÁ	kA
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
jako	jako	k8xS,k8xC
fenomén	fenomén	k1gInSc1
<g/>
.	.	kIx.
kravihora	kravihora	k1gFnSc1
<g/>
.	.	kIx.
<g/>
hvezdarna	hvezdarna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POŠVÁŘ	POŠVÁŘ	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brněnské	brněnský	k2eAgFnPc4d1
vinice	vinice	k1gFnPc4
a	a	k8xC
brněnské	brněnský	k2eAgNnSc4d1
viniční	viniční	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
a	a	k8xC
dnes	dnes	k6eAd1
<g/>
:	:	kIx,
sborník	sborník	k1gInSc1
příspěvků	příspěvek	k1gInPc2
k	k	k7c3
dějinám	dějiny	k1gFnPc3
a	a	k8xC
výstavbě	výstavba	k1gFnSc3
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
97	#num#	k4
<g/>
–	–	k?
<g/>
112	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
JAN	Jana	k1gFnPc2
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středověké	středověký	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
1071	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86736	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
381	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zur	Zur	k1gMnSc1
Verlegung	Verlegung	k1gMnSc1
der	drát	k5eAaImRp2nS
Pulvermagazine	Pulvermagazin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brünner	Brünner	k1gInSc1
Morgenpost	Morgenpost	k1gInSc4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
1.09	1.09	k4
<g/>
.1923	.1923	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
58	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
199	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SAMEK	Samek	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
;	;	kIx,
HRUBÝ	Hrubý	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
-	-	kIx~
proměny	proměna	k1gFnPc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Blok	blok	k1gInSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
231	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Brünn	Brünn	k1gInSc1
<g/>
:	:	kIx,
Blatt	Blatt	k1gInSc1
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1835	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapa	mapa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
na	na	k7c4
Hvezdarna	Hvezdarno	k1gNnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vrchy	vrch	k1gInPc1
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Červený	červený	k2eAgInSc4d1
kopec	kopec	k1gInSc4
•	•	k?
Hády	hádes	k1gInPc4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
z	z	k7c2
části	část	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Kopeček	kopeček	k1gInSc1
•	•	k?
Kraví	kraví	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Medlánecký	Medlánecký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Mniší	mniší	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Palackého	Palacký	k1gMnSc2
vrch	vrch	k1gInSc1
•	•	k?
Petrov	Petrov	k1gInSc1
•	•	k?
Stránská	Stránská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Strom	strom	k1gInSc1
•	•	k?
Špilberk	Špilberk	k1gInSc1
•	•	k?
Žlutý	žlutý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
