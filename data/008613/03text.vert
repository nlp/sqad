<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magistra	k1gFnPc2	magistra
artis	artis	k1gFnSc1	artis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
označující	označující	k2eAgMnSc1d1	označující
absolventa	absolvent	k1gMnSc4	absolvent
vysoké	vysoká	k1gFnSc2	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
MgA.	MgA.	k1gFnSc1	MgA.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
existuje	existovat	k5eAaImIp3nS	existovat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magistra	k1gFnPc2	magistra
<g/>
)	)	kIx)	)
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Získá	získat	k5eAaPmIp3nS	získat
ho	on	k3xPp3gMnSc4	on
absolvent	absolvent	k1gMnSc1	absolvent
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
případě	případ	k1gInSc6	případ
navazujícího	navazující	k2eAgNnSc2d1	navazující
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
případě	případ	k1gInSc6	případ
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
samostatného	samostatný	k2eAgMnSc2d1	samostatný
<g/>
,	,	kIx,	,
celistvého	celistvý	k2eAgInSc2d1	celistvý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
souvislého	souvislý	k2eAgNnSc2d1	souvislé
a	a	k8xC	a
nenavazujícího	navazující	k2eNgNnSc2d1	nenavazující
studia	studio	k1gNnSc2	studio
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgInPc1	tento
souvislé	souvislý	k2eAgInPc1d1	souvislý
magisterské	magisterský	k2eAgInPc1d1	magisterský
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
právě	právě	k9	právě
typické	typický	k2eAgInPc4d1	typický
např.	např.	kA	např.
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc1	lékařství
apod.	apod.	kA	apod.
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
magisterské	magisterský	k2eAgInPc4d1	magisterský
umělecké	umělecký	k2eAgInPc4d1	umělecký
studijní	studijní	k2eAgInPc4d1	studijní
obory	obor	k1gInPc4	obor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
typický	typický	k2eAgInSc1d1	typický
čtyřletý	čtyřletý	k2eAgInSc1d1	čtyřletý
souvislý	souvislý	k2eAgInSc1d1	souvislý
(	(	kIx(	(
<g/>
nerozdělený	rozdělený	k2eNgInSc1d1	nerozdělený
<g/>
)	)	kIx)	)
magisterský	magisterský	k2eAgInSc1d1	magisterský
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
učitelů	učitel	k1gMnPc2	učitel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
prominuto	prominut	k2eAgNnSc1d1	prominuto
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
diplomant	diplomant	k1gMnSc1	diplomant
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
student	student	k1gMnSc1	student
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
student	student	k1gMnSc1	student
pracující	pracující	k1gMnSc1	pracující
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
diplomové	diplomový	k2eAgNnSc4d1	diplomové
(	(	kIx(	(
<g/>
magisterské	magisterský	k2eAgNnSc4d1	magisterské
<g/>
)	)	kIx)	)
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Magisterské	magisterský	k2eAgNnSc1d1	magisterské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
řádně	řádně	k6eAd1	řádně
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
státnice	státnice	k1gFnSc1	státnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
kvalifikační	kvalifikační	k2eAgFnSc2d1	kvalifikační
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
umělecké	umělecký	k2eAgFnPc4d1	umělecká
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
typický	typický	k2eAgInSc4d1	typický
určitý	určitý	k2eAgInSc4d1	určitý
umělecký	umělecký	k2eAgInSc4d1	umělecký
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tituly	titul	k1gInPc1	titul
této	tento	k3xDgFnSc2	tento
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
běžně	běžně	k6eAd1	běžně
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
profesních	profesní	k2eAgInPc2d1	profesní
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
tak	tak	k9	tak
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývají	bývat	k5eAaImIp3nP	bývat
užívány	užívat	k5eAaImNgInP	užívat
<g />
.	.	kIx.	.
</s>
<s>
až	až	k6eAd1	až
tituly	titul	k1gInPc1	titul
od	od	k7c2	od
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gNnSc4	jejich
formální	formální	k2eAgNnSc4d1	formální
užívání	užívání	k1gNnSc4	užívání
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
častější	častý	k2eAgInSc1d2	častější
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k9	jako
formálně	formálně	k6eAd1	formálně
správné	správný	k2eAgNnSc1d1	správné
oslovování	oslovování	k1gNnSc1	oslovování
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívá	užívat	k5eAaImIp3nS	užívat
pane	pan	k1gMnSc5	pan
magistře	magistr	k1gMnSc5	magistr
/	/	kIx~	/
paní	paní	k1gFnSc3	paní
magistro	magistra	k1gFnSc5	magistra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
z	z	k7c2	z
lat.	lat.	k?	lat.
magister	magister	k1gMnSc1	magister
–	–	k?	–
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
představený	představený	k2eAgMnSc1d1	představený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
byl	být	k5eAaImAgMnS	být
magister	magister	k1gMnSc1	magister
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mistr	mistr	k1gMnSc1	mistr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
původně	původně	k6eAd1	původně
prostě	prostě	k9	prostě
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
akademickým	akademický	k2eAgInSc7d1	akademický
titulem	titul	k1gInSc7	titul
<g/>
.	.	kIx.	.
</s>
<s>
Uděloval	udělovat	k5eAaImAgInS	udělovat
se	se	k3xPyFc4	se
bakalářům	bakalář	k1gMnPc3	bakalář
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
obhajobě	obhajoba	k1gFnSc3	obhajoba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
these	these	k1gFnPc1	these
<g/>
)	)	kIx)	)
na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
(	(	kIx(	(
<g/>
fakultě	fakulta	k1gFnSc6	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc1	jeho
celý	celý	k2eAgInSc1d1	celý
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
učitelského	učitelský	k2eAgNnSc2d1	učitelské
oprávnění	oprávnění	k1gNnSc2	oprávnění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
licentia	licentia	k1gFnSc1	licentia
docendi	docend	k1gMnPc1	docend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oprávněným	oprávněný	k2eAgInSc7d1	oprávněný
tj.	tj.	kA	tj.
zpravidla	zpravidla	k6eAd1	zpravidla
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
učitelům	učitel	k1gMnPc3	učitel
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říkalo	říkat	k5eAaImAgNnS	říkat
magister	magistra	k1gFnPc2	magistra
regens	regens	k1gMnSc1	regens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odborných	odborný	k2eAgFnPc6d1	odborná
fakultách	fakulta	k1gFnPc6	fakulta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
teologické	teologický	k2eAgFnSc2d1	teologická
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgFnSc2d1	právnická
a	a	k8xC	a
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uděloval	udělovat	k5eAaImAgMnS	udělovat
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc4	magister
nahradil	nahradit	k5eAaPmAgInS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Magistr	magistr	k1gMnSc1	magistr
zůstal	zůstat	k5eAaPmAgMnS	zůstat
pouze	pouze	k6eAd1	pouze
titulem	titul	k1gInSc7	titul
farmaceutů	farmaceut	k1gMnPc2	farmaceut
(	(	kIx(	(
<g/>
PhMr	PhMr	k1gMnSc1	PhMr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc2	magister
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
znovu	znovu	k6eAd1	znovu
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc3	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
české	český	k2eAgInPc1d1	český
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
lépe	dobře	k6eAd2	dobře
odpovídaly	odpovídat	k5eAaImAgInP	odpovídat
titulům	titul	k1gInPc3	titul
z	z	k7c2	z
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc2	magister
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zaveden	zavést	k5eAaPmNgInS	zavést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
univerzitní	univerzitní	k2eAgNnPc4d1	univerzitní
studia	studio	k1gNnPc4	studio
bez	bez	k7c2	bez
získání	získání	k1gNnSc2	získání
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
titul	titul	k1gInSc4	titul
magistra	magister	k1gMnSc2	magister
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
MgA.	MgA.	k1gMnSc1	MgA.
<g/>
)	)	kIx)	)
přiznán	přiznat	k5eAaPmNgMnS	přiznat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
paušálně	paušálně	k6eAd1	paušálně
nahradil	nahradit	k5eAaPmAgInS	nahradit
dříve	dříve	k6eAd2	dříve
používané	používaný	k2eAgInPc4d1	používaný
tituly	titul	k1gInPc4	titul
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgMnSc1d1	akademický
architekt	architekt	k1gMnSc1	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
dle	dle	k7c2	dle
§	§	k?	§
99	[number]	k4	99
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
téhož	týž	k3xTgInSc2	týž
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
titulem	titul	k1gInSc7	titul
magistra	magistra	k1gFnSc1	magistra
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
MgA.	MgA.	k1gFnSc1	MgA.
<g/>
)	)	kIx)	)
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
i	i	k9	i
titul	titul	k1gInSc4	titul
magistra	magistra	k1gFnSc1	magistra
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
získali	získat	k5eAaPmAgMnP	získat
podle	podle	k7c2	podle
§	§	k?	§
21	[number]	k4	21
staršího	starý	k2eAgInSc2d2	starší
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
172	[number]	k4	172
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
absolventi	absolvent	k1gMnPc1	absolvent
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
tohoto	tento	k3xDgInSc2	tento
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
vydá	vydat	k5eAaPmIp3nS	vydat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
magisterského	magisterský	k2eAgInSc2d1	magisterský
gradu	grad	k1gInSc2	grad
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
obsahově	obsahově	k6eAd1	obsahově
<g/>
)	)	kIx)	)
ucelená	ucelený	k2eAgFnSc1d1	ucelená
část	část	k1gFnSc1	část
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc1	titul
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
umění	umění	k1gNnSc2	umění
uděluje	udělovat	k5eAaImIp3nS	udělovat
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
"	"	kIx"	"
<g/>
Mgr.	Mgr.	kA	Mgr.
art	art	k?	art
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Magistr	magistr	k1gMnSc1	magistr
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
udělován	udělovat	k5eAaImNgInS	udělovat
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
na	na	k7c6	na
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
ústavech	ústav	k1gInPc6	ústav
a	a	k8xC	a
institutech	institut	k1gInPc6	institut
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
akreditovaný	akreditovaný	k2eAgMnSc1d1	akreditovaný
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
oborů	obor	k1gInPc2	obor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udělován	udělován	k2eAgMnSc1d1	udělován
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
architektům	architekt	k1gMnPc3	architekt
<g/>
,	,	kIx,	,
absolventům	absolvent	k1gMnPc3	absolvent
oboru	obor	k1gInSc6	obor
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
udělován	udělován	k2eAgInSc1d1	udělován
absolventům	absolvent	k1gMnPc3	absolvent
<g/>
:	:	kIx,	:
Fakulty	fakulta	k1gFnPc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Fakulty	fakulta	k1gFnPc4	fakulta
umění	umění	k1gNnSc2	umění
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
udělován	udělován	k2eAgInSc1d1	udělován
je	být	k5eAaImIp3nS	být
také	také	k9	také
absolventům	absolvent	k1gMnPc3	absolvent
Fakulty	fakulta	k1gFnPc1	fakulta
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
komunikací	komunikace	k1gFnPc2	komunikace
Univerzity	univerzita	k1gFnSc2	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
či	či	k8xC	či
absolventům	absolvent	k1gMnPc3	absolvent
některých	některý	k3yIgInPc2	některý
oborů	obor	k1gInPc2	obor
Fakulty	fakulta	k1gFnSc2	fakulta
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
Technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Udělován	udělován	k2eAgInSc1d1	udělován
je	být	k5eAaImIp3nS	být
také	také	k9	také
některým	některý	k3yIgMnPc3	některý
absolventům	absolvent	k1gMnPc3	absolvent
na	na	k7c6	na
Filozoficko-přírodovědecké	filozofickořírodovědecký	k2eAgFnSc6d1	filozoficko-přírodovědecký
fakultě	fakulta	k1gFnSc6	fakulta
Slezské	slezský	k2eAgFnSc2d1	Slezská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Institutu	institut	k1gInSc2	institut
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
)	)	kIx)	)
či	či	k8xC	či
například	například	k6eAd1	například
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
designu	design	k1gInSc2	design
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
Ladislava	Ladislav	k1gMnSc2	Ladislav
Sutnara	Sutnar	k1gMnSc2	Sutnar
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
získat	získat	k5eAaPmF	získat
i	i	k9	i
soukromých	soukromý	k2eAgFnPc6d1	soukromá
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
například	například	k6eAd1	například
na	na	k7c6	na
<g/>
:	:	kIx,	:
Literární	literární	k2eAgFnSc6d1	literární
akademii	akademie	k1gFnSc6	akademie
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Master	master	k1gMnSc1	master
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
</s>
</p>
<p>
<s>
Magister	magister	k1gMnSc1	magister
regens	regens	k1gMnSc1	regens
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
farmacie	farmacie	k1gFnSc2	farmacie
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
teologie	teologie	k1gFnSc2	teologie
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Magister	magistra	k1gFnPc2	magistra
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
MgA.	MgA.	k1gFnSc2	MgA.
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
