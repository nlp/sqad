<s>
Dolar	dolar	k1gInSc1	dolar
nebo	nebo	k8xC	nebo
také	také	k9	také
dollar	dollar	k1gInSc1	dollar
(	(	kIx(	(
<g/>
označovaný	označovaný	k2eAgInSc1d1	označovaný
znakem	znak	k1gInSc7	znak
$	$	kIx~	$
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgFnSc4d1	oficiální
měnu	měna	k1gFnSc4	měna
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
provinciích	provincie	k1gFnPc6	provincie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
světovou	světový	k2eAgFnSc7d1	světová
měnou	měna	k1gFnSc7	měna
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
německého	německý	k2eAgNnSc2d1	německé
jména	jméno	k1gNnSc2	jméno
českých	český	k2eAgFnPc2d1	Česká
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
tolarů	tolar	k1gInPc2	tolar
<g/>
,	,	kIx,	,
ražených	ražený	k2eAgInPc2d1	ražený
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1520	[number]	k4	1520
jako	jako	k8xC	jako
lokální	lokální	k2eAgFnSc1d1	lokální
měna	měna	k1gFnSc1	měna
z	z	k7c2	z
jáchymovského	jáchymovský	k2eAgNnSc2d1	Jáchymovské
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
nazývány	nazývat	k5eAaImNgFnP	nazývat
hornoněmecky	hornoněmecky	k6eAd1	hornoněmecky
joachimsthaler	joachimsthaler	k1gInSc4	joachimsthaler
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
thaler	thaler	k1gInSc1	thaler
nebo	nebo	k8xC	nebo
taler	taler	k1gInSc1	taler
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
německého	německý	k2eAgNnSc2d1	německé
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Joachimsthal	Joachimsthal	k1gMnSc1	Joachimsthal
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jáchymův	Jáchymův	k2eAgInSc1d1	Jáchymův
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
názvem	název	k1gInSc7	název
přešel	přejít	k5eAaPmAgInS	přejít
název	název	k1gInSc1	název
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
<g/>
:	:	kIx,	:
česky	česky	k6eAd1	česky
tolar	tolar	k1gInSc1	tolar
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
tallér	tallér	k1gInSc1	tallér
<g/>
,	,	kIx,	,
seversky	seversky	k6eAd1	seversky
daler	daler	k1gInSc1	daler
<g/>
:	:	kIx,	:
dánsky	dánsky	k6eAd1	dánsky
a	a	k8xC	a
norsky	norsky	k6eAd1	norsky
rigsdaler	rigsdaler	k1gInSc4	rigsdaler
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
riksdaler	riksdaler	k1gInSc1	riksdaler
<g/>
,	,	kIx,	,
islandsky	islandsky	k6eAd1	islandsky
dalur	dalur	k1gMnSc1	dalur
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
tallero	tallero	k1gNnSc1	tallero
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
talar	talar	k1gInSc1	talar
<g/>
,	,	kIx,	,
etiopsky	etiopsky	k6eAd1	etiopsky
(	(	kIx(	(
<g/>
amharsky	amharsky	k6eAd1	amharsky
<g/>
)	)	kIx)	)
ታ	ታ	k?	ታ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
talari	talari	k6eAd1	talari
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
persky	persky	k6eAd1	persky
dare	dar	k1gInSc5	dar
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemsky	nizozemsky	k6eAd1	nizozemsky
šlo	jít	k5eAaImAgNnS	jít
obdobně	obdobně	k6eAd1	obdobně
o	o	k7c4	o
(	(	kIx(	(
<g/>
rijks	rijks	k1gInSc4	rijks
<g/>
)	)	kIx)	)
<g/>
daalder	daalder	k1gInSc4	daalder
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
rijks	rijks	k1gInSc1	rijks
<g/>
)	)	kIx)	)
<g/>
daler	daler	k1gInSc1	daler
a	a	k8xC	a
nizozemským	nizozemský	k2eAgNnSc7d1	Nizozemské
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
název	název	k1gInSc1	název
přešel	přejít	k5eAaPmAgInS	přejít
také	také	k9	také
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xS	jako
dollar	dollar	k1gInSc1	dollar
<g/>
:	:	kIx,	:
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
mince	mince	k1gFnSc2	mince
se	se	k3xPyFc4	se
znakem	znak	k1gInSc7	znak
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
leeuwendaler	leeuwendaler	k1gInSc4	leeuwendaler
nebo	nebo	k8xC	nebo
leeuwendaalder	leeuwendaalder	k1gInSc4	leeuwendaalder
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
lví	lví	k2eAgInSc1d1	lví
daler	daler	k1gInSc1	daler
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
provincie	provincie	k1gFnPc4	provincie
nizozemské	nizozemský	k2eAgInPc1d1	nizozemský
produkovaly	produkovat	k5eAaImAgInP	produkovat
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
rozmachu	rozmach	k1gInSc2	rozmach
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Leeuwendalery	Leeuwendaler	k1gInPc1	Leeuwendaler
obíhaly	obíhat	k5eAaImAgInP	obíhat
po	po	k7c6	po
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
napodobovány	napodobovat	k5eAaImNgFnP	napodobovat
několika	několik	k4yIc7	několik
italskými	italský	k2eAgNnPc7d1	italské
a	a	k8xC	a
německými	německý	k2eAgNnPc7d1	německé
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Měny	měna	k1gFnPc1	měna
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
'	'	kIx"	'
<g/>
lvy	lev	k1gInPc4	lev
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
leu	leu	k?	leu
a	a	k8xC	a
leva	leva	k1gInSc2	leva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
peníz	peníz	k1gInSc1	peníz
byl	být	k5eAaImAgInS	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
také	také	k9	také
v	v	k7c6	v
Nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
východní	východní	k2eAgFnSc6d1	východní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
ovšem	ovšem	k9	ovšem
i	i	k9	i
v	v	k7c6	v
nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
kolonii	kolonie	k1gFnSc6	kolonie
Nové	Nové	k2eAgNnSc1d1	Nové
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
nynějším	nynější	k2eAgMnSc7d1	nynější
New	New	k1gMnSc7	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
proto	proto	k8xC	proto
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
i	i	k8xC	i
v	v	k7c6	v
13	[number]	k4	13
britských	britský	k2eAgFnPc6d1	britská
koloniích	kolonie	k1gFnPc6	kolonie
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
lion	lion	k?	lion
(	(	kIx(	(
<g/>
lyon	lyon	k1gMnSc1	lyon
<g/>
)	)	kIx)	)
dollar	dollar	k1gInSc1	dollar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Americko-anglická	americkonglický	k2eAgFnSc1d1	americko-anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
slova	slovo	k1gNnSc2	slovo
dollar	dollar	k1gInSc1	dollar
je	on	k3xPp3gMnPc4	on
stále	stále	k6eAd1	stále
zřetelně	zřetelně	k6eAd1	zřetelně
blízká	blízký	k2eAgFnSc1d1	blízká
nizozemské	nizozemský	k2eAgFnPc4d1	nizozemská
výslovnosti	výslovnost	k1gFnPc4	výslovnost
daler	dalra	k1gFnPc2	dalra
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
opotřebovaným	opotřebovaný	k2eAgFnPc3d1	opotřebovaná
mincím	mince	k1gFnPc3	mince
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
koloniích	kolonie	k1gFnPc6	kolonie
se	se	k3xPyFc4	se
posměšně	posměšně	k6eAd1	posměšně
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
psí	psí	k2eAgInPc4d1	psí
dolary	dolar	k1gInPc4	dolar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dog	doga	k1gFnPc2	doga
dollars	dollars	k1gInSc1	dollars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
také	také	k9	také
užívá	užívat	k5eAaImIp3nS	užívat
pojem	pojem	k1gInSc1	pojem
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
dolar	dolar	k1gInSc4	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
měnová	měnový	k2eAgFnSc1d1	měnová
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
čas	čas	k1gInSc4	čas
stejnou	stejný	k2eAgFnSc4d1	stejná
kupní	kupní	k2eAgFnSc4d1	kupní
sílu	síla	k1gFnSc4	síla
jako	jako	k8xC	jako
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dolar	dolar	k1gInSc1	dolar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dolar	dolar	k1gInSc1	dolar
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
