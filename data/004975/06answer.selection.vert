<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morfologické	morfologický	k2eAgFnSc2d1	morfologická
typologie	typologie	k1gFnSc2	typologie
se	se	k3xPyFc4	se
korjačtina	korjačtina	k1gFnSc1	korjačtina
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
aglutinačním	aglutinační	k2eAgMnPc3d1	aglutinační
jazykům	jazyk	k1gMnPc3	jazyk
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
sufixů	sufix	k1gInPc2	sufix
<g/>
,	,	kIx,	,
prefixů	prefix	k1gInPc2	prefix
i	i	k8xC	i
cirkumfixů	cirkumfix	k1gInPc2	cirkumfix
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
afixů	afix	k1gInPc2	afix
složených	složený	k2eAgInPc2d1	složený
z	z	k7c2	z
prefixové	prefixový	k2eAgFnSc2d1	prefixová
a	a	k8xC	a
sufixové	sufixový	k2eAgFnSc2d1	sufixový
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
