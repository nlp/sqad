<s>
Světové	světový	k2eAgFnPc1d1	světová
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc4	čtyři
základní	základní	k2eAgInPc4d1	základní
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
směr	směr	k1gInSc1	směr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
základní	základní	k2eAgInPc1d1	základní
směry	směr	k1gInPc1	směr
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
a	a	k8xC	a
poledníků	poledník	k1gInPc2	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Poledník	poledník	k1gInSc1	poledník
je	být	k5eAaImIp3nS	být
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
Rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
východo-západně	východoápadně	k6eAd1	východo-západně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
(	(	kIx(	(
<g/>
indiánské	indiánský	k2eAgNnSc4d1	indiánské
nebo	nebo	k8xC	nebo
buddhistické	buddhistický	k2eAgNnSc4d1	buddhistické
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
koncept	koncept	k1gInSc4	koncept
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c4	na
šest	šest	k4xCc4	šest
základních	základní	k2eAgFnPc2d1	základní
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
-	-	kIx~	-
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
směry	směr	k1gInPc4	směr
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Čtyři	čtyři	k4xCgFnPc1	čtyři
světové	světový	k2eAgFnPc1d1	světová
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
písmeny	písmeno	k1gNnPc7	písmeno
S	s	k7c7	s
nebo	nebo	k8xC	nebo
N	N	kA	N
(	(	kIx(	(
<g/>
Sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
Nord	Nord	k1gMnSc1	Nord
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
O	O	kA	O
nebo	nebo	k8xC	nebo
E	E	kA	E
(	(	kIx(	(
<g/>
Východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
J	J	kA	J
nebo	nebo	k8xC	nebo
S	s	k7c7	s
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
<g/>
,	,	kIx,	,
Süd	Süd	k1gMnSc1	Süd
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
<g/>
)	)	kIx)	)
a	a	k8xC	a
Z	Z	kA	Z
nebo	nebo	k8xC	nebo
W	W	kA	W
(	(	kIx(	(
<g/>
Západ	západ	k1gInSc1	západ
<g/>
,	,	kIx,	,
West	West	k1gInSc1	West
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
navigaci	navigace	k1gFnSc3	navigace
a	a	k8xC	a
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
orientaci	orientace	k1gFnSc3	orientace
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc4d1	horní
okraj	okraj	k1gInSc4	okraj
mapy	mapa	k1gFnSc2	mapa
vždy	vždy	k6eAd1	vždy
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sever	sever	k1gInSc1	sever
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
nebo	nebo	k8xC	nebo
také	také	k9	také
geografický	geografický	k2eAgInSc4d1	geografický
sever	sever	k1gInSc4	sever
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc1	směr
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
k	k	k7c3	k
geografickému	geografický	k2eAgNnSc3d1	geografické
severnímu	severní	k2eAgNnSc3d1	severní
pólu	pólo	k1gNnSc3	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
svým	svůj	k3xOyFgInSc7	svůj
koncem	konec	k1gInSc7	konec
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
navigaci	navigace	k1gFnSc6	navigace
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
"	"	kIx"	"
<g/>
severní	severní	k2eAgInPc1d1	severní
směry	směr	k1gInPc1	směr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
především	především	k9	především
"	"	kIx"	"
<g/>
magnetický	magnetický	k2eAgInSc4d1	magnetický
sever	sever	k1gInSc4	sever
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
střelka	střelka	k1gFnSc1	střelka
magnetického	magnetický	k2eAgInSc2d1	magnetický
kompasu	kompas	k1gInSc2	kompas
a	a	k8xC	a
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
geografického	geografický	k2eAgInSc2d1	geografický
severu	sever	k1gInSc2	sever
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
deklinaci	deklinace	k1gFnSc4	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
satelitní	satelitní	k2eAgFnSc6d1	satelitní
navigaci	navigace	k1gFnSc6	navigace
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
další	další	k2eAgInPc1d1	další
souřadnicové	souřadnicový	k2eAgInPc1d1	souřadnicový
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jih	jih	k1gInSc1	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
než	než	k8xS	než
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
východ	východ	k1gInSc1	východ
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vychází	vycházet	k5eAaImIp3nS	vycházet
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
před	před	k7c7	před
rozšířením	rozšíření	k1gNnSc7	rozšíření
kompasu	kompas	k1gInSc2	kompas
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
význam	význam	k1gInSc1	význam
východu	východ	k1gInSc2	východ
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
dnešním	dnešní	k2eAgInSc7d1	dnešní
severem	sever	k1gInSc7	sever
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
byly	být	k5eAaImAgFnP	být
mapy	mapa	k1gFnPc1	mapa
orientovány	orientovat	k5eAaBmNgFnP	orientovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
samo	sám	k3xTgNnSc1	sám
slovo	slovo	k1gNnSc1	slovo
orientace	orientace	k1gFnSc2	orientace
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
označení	označení	k1gNnSc2	označení
východu	východ	k1gInSc2	východ
oriens	oriensa	k1gFnPc2	oriensa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
orientovány	orientovat	k5eAaBmNgInP	orientovat
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Západ	západ	k1gInSc1	západ
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zapadá	zapadat	k5eAaImIp3nS	zapadat
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesnější	přesný	k2eAgNnSc4d2	přesnější
určení	určení	k1gNnSc4	určení
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ještě	ještě	k9	ještě
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
směry	směr	k1gInPc7	směr
hlavními	hlavní	k2eAgInPc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc1	jihozápad
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
směry	směr	k1gInPc4	směr
k	k	k7c3	k
sobě	se	k3xPyFc3	se
kolmé	kolmý	k2eAgFnPc1d1	kolmá
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
půlí	půlit	k5eAaImIp3nP	půlit
úhel	úhel	k1gInSc4	úhel
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgInPc7d1	hlavní
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
hlavními	hlavní	k2eAgInPc7d1	hlavní
směry	směr	k1gInPc7	směr
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
kompasu	kompas	k1gInSc6	kompas
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
větrnou	větrný	k2eAgFnSc4d1	větrná
růžici	růžice	k1gFnSc4	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
směry	směr	k1gInPc1	směr
spolu	spolu	k6eAd1	spolu
svírají	svírat	k5eAaImIp3nP	svírat
úhel	úhel	k1gInSc4	úhel
45	[number]	k4	45
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
růžice	růžice	k1gFnSc1	růžice
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
používá	používat	k5eAaImIp3nS	používat
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pro	pro	k7c4	pro
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
směry	směr	k1gInPc4	směr
samostatné	samostatný	k2eAgInPc4d1	samostatný
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
neodvozené	odvozený	k2eNgInPc1d1	neodvozený
z	z	k7c2	z
názvů	název	k1gInPc2	název
základních	základní	k2eAgInPc2d1	základní
směrů	směr	k1gInPc2	směr
-	-	kIx~	-
srovnání	srovnání	k1gNnSc2	srovnání
dvou	dva	k4xCgInPc2	dva
blízkých	blízký	k2eAgInPc2d1	blízký
baltofinských	baltofinský	k2eAgInPc2d1	baltofinský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
"	"	kIx"	"
<g/>
prohození	prohození	k1gNnSc1	prohození
<g/>
"	"	kIx"	"
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
jih	jih	k1gInSc4	jih
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Větrná	větrný	k2eAgFnSc1d1	větrná
růžice	růžice	k1gFnSc1	růžice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesnějšímu	přesný	k2eAgNnSc3d2	přesnější
určování	určování	k1gNnSc3	určování
směru	směr	k1gInSc2	směr
používají	používat	k5eAaImIp3nP	používat
námořníci	námořník	k1gMnPc1	námořník
ještě	ještě	k6eAd1	ještě
jemnější	jemný	k2eAgNnSc4d2	jemnější
dělení	dělení	k1gNnSc4	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
růžice	růžice	k1gFnSc1	růžice
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
16	[number]	k4	16
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spolu	spolu	k6eAd1	spolu
svírají	svírat	k5eAaImIp3nP	svírat
úhel	úhel	k1gInSc4	úhel
22,50	[number]	k4	22,50
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
čárka	čárka	k1gFnSc1	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
termínem	termín	k1gInSc7	termín
půl	půl	k1xP	půl
čárky	čárka	k1gFnSc2	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
směrů	směr	k1gInPc2	směr
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
čtyři	čtyři	k4xCgInPc1	čtyři
hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zkratku	zkratka	k1gFnSc4	zkratka
-	-	kIx~	-
S	s	k7c7	s
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
Z	z	k7c2	z
názvy	název	k1gInPc7	název
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
směrů	směr	k1gInPc2	směr
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
směrů	směr	k1gInPc2	směr
hlavních	hlavní	k2eAgInPc2d1	hlavní
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc4	slovo
sever	sever	k1gInSc1	sever
a	a	k8xC	a
jih	jih	k1gInSc1	jih
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
slovem	slovo	k1gNnSc7	slovo
východ	východ	k1gInSc1	východ
a	a	k8xC	a
západ	západ	k1gInSc1	západ
<g/>
:	:	kIx,	:
severozápad	severozápad	k1gInSc1	severozápad
<g/>
,	,	kIx,	,
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
,	,	kIx,	,
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
,	,	kIx,	,
zkratky	zkratka	k1gFnPc1	zkratka
SZ	SZ	kA	SZ
<g/>
,	,	kIx,	,
SV	sv	kA	sv
<g/>
,	,	kIx,	,
JZ	JZ	kA	JZ
<g/>
,	,	kIx,	,
JV	JV	kA	JV
<g/>
.	.	kIx.	.
další	další	k2eAgNnSc1d1	další
dělení	dělení	k1gNnSc1	dělení
se	se	k3xPyFc4	se
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
podle	podle	k7c2	podle
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
osminy	osmina	k1gFnSc2	osmina
a	a	k8xC	a
příslušného	příslušný	k2eAgInSc2d1	příslušný
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ZSZ	ZSZ	kA	ZSZ
je	být	k5eAaImIp3nS	být
západo-severozápad	západoeverozápad	k1gInSc1	západo-severozápad
<g/>
,	,	kIx,	,
JJZ	JJZ	kA	JJZ
je	být	k5eAaImIp3nS	být
jiho-jihozápad	jihoihozápad	k1gInSc4	jiho-jihozápad
<g/>
.	.	kIx.	.
azimut	azimut	k1gInSc4	azimut
orientace	orientace	k1gFnSc2	orientace
kompas	kompas	k1gInSc1	kompas
kurz	kurz	k1gInSc1	kurz
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
větrná	větrný	k2eAgFnSc1d1	větrná
růžice	růžice	k1gFnSc1	růžice
navigace	navigace	k1gFnSc2	navigace
časové	časový	k2eAgNnSc4d1	časové
pásmo	pásmo	k1gNnSc4	pásmo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
světová	světový	k2eAgFnSc1d1	světová
strana	strana	k1gFnSc1	strana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
světová	světový	k2eAgFnSc1d1	světová
strana	strana	k1gFnSc1	strana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
