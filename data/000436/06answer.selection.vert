<s>
Poledník	poledník	k1gInSc1	poledník
je	být	k5eAaImIp3nS	být
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
Rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
východo-západně	východoápadně	k6eAd1	východo-západně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
</s>
