<s>
Elysejská	elysejský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
Elysejská	elysejský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
je	být	k5eAaImIp3nS
dohoda	dohoda	k1gFnSc1
o	o	k7c6
těsné	těsný	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
ve	v	k7c6
všech	všecek	k3xTgFnPc6
oblastech	oblast	k1gFnPc6
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
podepsaná	podepsaný	k2eAgFnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavazují	zavazovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
k	k	k7c3
„	„	k?
<g/>
hledání	hledání	k1gNnSc2
pokud	pokud	k8xS
možno	možno	k6eAd1
společné	společný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
<g/>
“	“	k?
k	k	k7c3
veškerým	veškerý	k3xTgFnPc3
významným	významný	k2eAgFnPc3d1
ekonomickým	ekonomický	k2eAgFnPc3d1
<g/>
,	,	kIx,
politickým	politický	k2eAgFnPc3d1
a	a	k8xC
kulturním	kulturní	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
„	„	k?
<g/>
osu	osa	k1gFnSc4
Paříž	Paříž	k1gFnSc1
–	–	k?
Bonn	Bonn	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
stát	stát	k5eAaPmF,k5eAaImF
rozhodující	rozhodující	k2eAgFnSc7d1
silou	síla	k1gFnSc7
Evropského	evropský	k2eAgNnSc2d1
hospodářského	hospodářský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
procesu	proces	k1gInSc6
usmíření	usmíření	k1gNnSc2
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
západním	západní	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
sehrál	sehrát	k5eAaPmAgMnS
spolkový	spolkový	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
Konrad	Konrada	k1gFnPc2
Adenauer	Adenauer	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
snaha	snaha	k1gFnSc1
o	o	k7c6
překonání	překonání	k1gNnSc6
„	„	k?
<g/>
historického	historický	k2eAgNnSc2d1
nepřátelství	nepřátelství	k1gNnSc2
<g/>
“	“	k?
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
zeměmi	zem	k1gFnPc7
symbolicky	symbolicky	k6eAd1
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
1962	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
kancléř	kancléř	k1gMnSc1
během	během	k7c2
státní	státní	k2eAgFnSc2d1
návštěvy	návštěva	k1gFnSc2
Francie	Francie	k1gFnSc2
zúčastnil	zúčastnit	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
francouzským	francouzský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Charlesem	Charles	k1gMnSc7
de	de	k?
Gaullem	Gaull	k1gMnSc7
bohoslužby	bohoslužba	k1gFnSc2
v	v	k7c6
remešské	remešský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
ledna	leden	k1gInSc2
1963	#num#	k4
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
politici	politik	k1gMnPc1
sešli	sejít	k5eAaPmAgMnP
v	v	k7c6
Elysejském	elysejský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
slavnostně	slavnostně	k6eAd1
podepsali	podepsat	k5eAaPmAgMnP
Smlouvu	smlouva	k1gFnSc4
o	o	k7c6
německo-francouzské	německo-francouzský	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
jejím	její	k3xOp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
čelit	čelit	k5eAaImF
utvářejícímu	utvářející	k2eAgInSc3d1
se	se	k3xPyFc4
americko-britskému	americko-britský	k2eAgInSc3d1
bloku	blok	k1gInSc3
v	v	k7c6
Severoatlantické	severoatlantický	k2eAgFnSc6d1
alianci	aliance	k1gFnSc6
a	a	k8xC
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
vedoucím	vedoucí	k2eAgInSc7d1
politickým	politický	k2eAgInSc7d1
a	a	k8xC
vojenským	vojenský	k2eAgInSc7d1
činitelem	činitel	k1gInSc7
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BAUMANN	BAUMANN	kA
<g/>
,	,	kIx,
Ansbert	Ansbert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Begegnung	Begegnung	k1gInSc1
der	drát	k5eAaImRp2nS
Völker	Völker	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Élysée-Vertrag	Élysée-Vertrag	k1gInSc1
und	und	k?
die	die	k?
Bundesrepublik	Bundesrepublik	k1gMnSc1
Deutschland	Deutschlando	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsch-französische	Deutsch-französisch	k1gInSc2
Kulturpolitik	Kulturpolitik	k1gMnSc1
von	von	k1gInSc4
1963	#num#	k4
bis	bis	k?
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
:	:	kIx,
Peter	Peter	k1gMnSc1
Lang	Lang	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
631	#num#	k4
<g/>
-	-	kIx~
<g/>
50539	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BAUMANN	BAUMANN	kA
<g/>
,	,	kIx,
Ansbert	Ansbert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
organisierte	organisirat	k5eAaPmRp2nP
Zusammenarbeit	Zusammenarbeit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
deutsch-französischen	deutsch-französischna	k1gFnPc2
Beziehungen	Beziehungen	k1gInSc4
am	am	k?
Vorabend	Vorabend	k1gInSc1
des	des	k1gNnSc1
Élysée-Vertrags	Élysée-Vertrags	k1gInSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludwigsburg	Ludwigsburg	k1gInSc1
:	:	kIx,
DFI	DFI	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DEFRANCE	DEFRANCE	kA
<g/>
,	,	kIx,
Corine	Corin	k1gMnSc5
<g/>
;	;	kIx,
PFEIL	PFEIL	kA
<g/>
,	,	kIx,
Ulrich	Ulrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Élysée-Vertrag	Élysée-Vertrag	k1gInSc1
und	und	k?
die	die	k?
deutsch-französischen	deutsch-französischen	k2eAgInSc4d1
Beziehungen	Beziehungen	k1gInSc4
1945	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnichov	Mnichov	k1gInSc1
:	:	kIx,
Oldenbourg	Oldenbourg	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
486	#num#	k4
<g/>
-	-	kIx~
<g/>
57678	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
DEFRANCE	DEFRANCE	kA
<g/>
,	,	kIx,
Corine	Corin	k1gMnSc5
<g/>
;	;	kIx,
PFEIL	PFEIL	kA
<g/>
,	,	kIx,
Ulrich	Ulrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc7
France	Franc	k1gMnSc2
<g/>
,	,	kIx,
l	l	kA
<g/>
’	’	k?
<g/>
Allemagne	Allemagn	k1gInSc5
et	et	k?
le	le	k?
traité	traitý	k2eAgFnSc2d1
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
Élysée	Élysée	k1gInSc1
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
:	:	kIx,
CNRS	CNRS	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
271	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7488	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FRIEND	FRIEND	kA
<g/>
,	,	kIx,
Julius	Julius	k1gMnSc1
W.	W.	kA
The	The	k1gMnSc1
Linchpin	Linchpin	k1gMnSc1
<g/>
:	:	kIx,
French-German	French-German	k1gMnSc1
Relations	Relationsa	k1gFnPc2
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
..	..	k?
New	New	k1gFnSc1
York	York	k1gInSc1
:	:	kIx,
Praeger	Praeger	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LAPPENKÜPER	LAPPENKÜPER	kA
<g/>
,	,	kIx,
Ulrich	Ulrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnPc2
deutsch-französischen	deutsch-französischit	k5eAaBmNgInS,k5eAaImNgInS,k5eAaPmNgInS
Beziehungen	Beziehungen	k1gInSc1
1949	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Von	von	k1gInSc1
der	drát	k5eAaImRp2nS
„	„	k?
<g/>
Erbfeindschaft	Erbfeindschaft	k1gInSc1
<g/>
“	“	k?
zur	zur	k?
„	„	k?
<g/>
Entente	entente	k1gFnSc2
élémentaire	élémentair	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnichov	Mnichov	k1gInSc1
:	:	kIx,
Oldenbourg	Oldenbourg	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
486	#num#	k4
<g/>
-	-	kIx~
<g/>
56522	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZIEBURA	ZIEBURA	kA
<g/>
,	,	kIx,
Gilbert	Gilbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnPc2
deutsch-französischen	deutsch-französischit	k5eAaBmNgInS,k5eAaPmNgInS,k5eAaImNgInS
Beziehungen	Beziehungen	k1gInSc1
seit	seit	k2eAgInSc1d1
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mythen	Mythen	k2eAgInSc1d1
und	und	k?
Realitäten	Realitätno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuttgart	Stuttgart	k1gInSc1
:	:	kIx,
Neske	Neske	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7885	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
511	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Elysejská	elysejský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://leccos.com/index.php/clanky/elysejska-smlouva	http://leccos.com/index.php/clanky/elysejska-smlouva	k6eAd1
</s>
<s>
http://www.ceskatelevize.cz/ct24/svet/211904-elysejska-smlouva-sblizuje-francii-a-nemecko-uz-50-let/	http://www.ceskatelevize.cz/ct24/svet/211904-elysejska-smlouva-sblizuje-francii-a-nemecko-uz-50-let/	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4149322-9	4149322-9	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
217224272	#num#	k4
</s>
