<s>
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgInSc4d1	literární
pseudonym	pseudonym	k1gInSc4	pseudonym
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgMnSc7	který
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1867	[number]	k4	1867
Opava	Opava	k1gFnSc1	Opava
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1958	[number]	k4	1958
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
jediné	jediný	k2eAgFnSc2d1	jediná
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
původně	původně	k6eAd1	původně
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Slezské	slezský	k2eAgNnSc1d1	Slezské
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgFnPc4d1	známá
jako	jako	k8xS	jako
Slezské	slezský	k2eAgFnPc4d1	Slezská
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
verše	verš	k1gInPc1	verš
útočně	útočně	k6eAd1	útočně
promlouvající	promlouvající	k2eAgInPc1d1	promlouvající
za	za	k7c4	za
sociální	sociální	k2eAgInPc4d1	sociální
a	a	k8xC	a
národní	národní	k2eAgInPc4d1	národní
zájmy	zájem	k1gInPc4	zájem
českých	český	k2eAgMnPc2d1	český
obyvatel	obyvatel	k1gMnPc2	obyvatel
Slezska	Slezsko	k1gNnSc2	Slezsko
nemají	mít	k5eNaImIp3nP	mít
svým	svůj	k3xOyFgInSc7	svůj
vznikem	vznik	k1gInSc7	vznik
<g/>
,	,	kIx,	,
stylem	styl	k1gInSc7	styl
ani	ani	k8xC	ani
dalším	další	k2eAgInSc7d1	další
osudem	osud	k1gInSc7	osud
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
obdobu	obdoba	k1gFnSc4	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgMnPc1d1	literární
historici	historik	k1gMnPc1	historik
řadí	řadit	k5eAaImIp3nP	řadit
Bezruče	Bezruče	k1gNnSc4	Bezruče
do	do	k7c2	do
generace	generace	k1gFnSc2	generace
tzv.	tzv.	kA	tzv.
anarchistických	anarchistický	k2eAgMnPc2d1	anarchistický
buřičů	buřič	k1gMnPc2	buřič
(	(	kIx(	(
<g/>
řadíme	řadit	k5eAaImIp1nP	řadit
jej	on	k3xPp3gInSc4	on
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
téže	týž	k3xTgFnSc2	týž
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
neztotožňovaly	ztotožňovat	k5eNaImAgInP	ztotožňovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
buřiči	buřič	k1gMnPc7	buřič
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
symbolismem	symbolismus	k1gInSc7	symbolismus
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
modernou	moderna	k1gFnSc7	moderna
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorem	autor	k1gMnSc7	autor
většiny	většina	k1gFnSc2	většina
básní	báseň	k1gFnPc2	báseň
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
není	být	k5eNaImIp3nS	být
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Ondřej	Ondřej	k1gMnSc1	Ondřej
Boleslav	Boleslav	k1gMnSc1	Boleslav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Antonína	Antonín	k1gMnSc2	Antonín
Vaška	Vašek	k1gMnSc2	Vašek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Marie	Maria	k1gFnSc2	Maria
Vaškové	Vašková	k1gFnSc2	Vašková
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Brožkové	Brožková	k1gFnPc1	Brožková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Antonín	Antonín	k1gMnSc1	Antonín
Vašek	Vašek	k1gMnSc1	Vašek
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
slezský	slezský	k2eAgMnSc1d1	slezský
buditel	buditel	k1gMnSc1	buditel
<g/>
.	.	kIx.	.
</s>
<s>
Vydával	vydávat	k5eAaImAgMnS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
list	list	k1gInSc4	list
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Opavský	opavský	k2eAgInSc1d1	opavský
besedník	besedník	k1gInSc1	besedník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
uznat	uznat	k5eAaPmF	uznat
pravost	pravost	k1gFnSc4	pravost
Rukopisů	rukopis	k1gInPc2	rukopis
královedvorského	královedvorský	k2eAgNnSc2d1	královedvorský
a	a	k8xC	a
zelenohorského	zelenohorský	k2eAgNnSc2d1	zelenohorský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
filologického	filologický	k2eAgInSc2d1	filologický
rozboru	rozbor	k1gInSc2	rozbor
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorem	autor	k1gMnSc7	autor
jejich	jejich	k3xOp3gInSc2	jejich
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Hanka	Hanka	k1gFnSc1	Hanka
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
padělání	padělání	k1gNnSc2	padělání
<g/>
.	.	kIx.	.
</s>
<s>
Vaškova	Vaškův	k2eAgFnSc1d1	Vaškova
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Týnci	Týnec	k1gInSc6	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Vašek	Vašek	k1gMnSc1	Vašek
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
pročeské	pročeský	k2eAgFnPc4d1	pročeská
aktivity	aktivita	k1gFnPc4	aktivita
(	(	kIx(	(
<g/>
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
německé	německý	k2eAgFnSc6d1	německá
Opavě	Opava	k1gFnSc6	Opava
<g/>
)	)	kIx)	)
nucen	nucen	k2eAgMnSc1d1	nucen
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
léto	léto	k1gNnSc1	léto
však	však	k9	však
malý	malý	k2eAgMnSc1d1	malý
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
trávil	trávit	k5eAaImAgMnS	trávit
v	v	k7c6	v
Háji	háj	k1gInSc6	háj
u	u	k7c2	u
Opavy	Opava	k1gFnSc2	Opava
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
vracel	vracet	k5eAaImAgMnS	vracet
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
zálibou	záliba	k1gFnSc7	záliba
-	-	kIx~	-
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc1	dětství
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ovšem	ovšem	k9	ovšem
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
neshodami	neshoda	k1gFnPc7	neshoda
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
nevybíravými	vybíravý	k2eNgInPc7d1	nevybíravý
útoky	útok	k1gInPc7	útok
proti	proti	k7c3	proti
otci	otec	k1gMnSc3	otec
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
filologickou	filologický	k2eAgFnSc4d1	filologická
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
Rukopisech	rukopis	k1gInPc6	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Antonín	Antonín	k1gMnSc1	Antonín
Vašek	Vašek	k1gMnSc1	Vašek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vdova	vdova	k1gFnSc1	vdova
Marie	Marie	k1gFnSc1	Marie
Vašková	Vašková	k1gFnSc1	Vašková
zůstala	zůstat	k5eAaPmAgFnS	zůstat
se	s	k7c7	s
šesti	šest	k4xCc7	šest
dětmi	dítě	k1gFnPc7	dítě
sama	sám	k3xTgFnSc1	sám
v	v	k7c6	v
nelehké	lehký	k2eNgFnSc6d1	nelehká
finanční	finanční	k2eAgFnSc6d1	finanční
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
začal	začít	k5eAaPmAgMnS	začít
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
brněnské	brněnský	k2eAgNnSc4d1	brněnské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgInSc1d3	Nejhorší
prospěch	prospěch	k1gInSc1	prospěch
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudbě	hudba	k1gFnSc3	hudba
nikdy	nikdy	k6eAd1	nikdy
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
měšťance	měšťanka	k1gFnSc6	měšťanka
Vaškovi	Vaškův	k2eAgMnPc1d1	Vaškův
posměšně	posměšně	k6eAd1	posměšně
přezdíval	přezdívat	k5eAaImAgMnS	přezdívat
Anton	Anton	k1gMnSc1	Anton
(	(	kIx(	(
<g/>
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
ein	ein	k?	ein
Ton	Ton	k1gFnSc1	Ton
=	=	kIx~	=
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
tón	tón	k1gInSc1	tón
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechny	všechen	k3xTgFnPc4	všechen
písně	píseň	k1gFnPc4	píseň
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
tónu	tón	k1gInSc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
prospíval	prospívat	k5eAaImAgMnS	prospívat
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
gymnaziálních	gymnaziální	k2eAgNnPc2d1	gymnaziální
let	léto	k1gNnPc2	léto
již	již	k6eAd1	již
psal	psát	k5eAaImAgInS	psát
první	první	k4xOgInPc4	první
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
ironické	ironický	k2eAgFnPc4d1	ironická
veršovánky	veršovánka	k1gFnPc4	veršovánka
namířené	namířený	k2eAgFnPc1d1	namířená
proti	proti	k7c3	proti
profesorům	profesor	k1gMnPc3	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
četbou	četba	k1gFnSc7	četba
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
,	,	kIx,	,
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
Nerudy	Neruda	k1gMnSc2	Neruda
<g/>
,	,	kIx,	,
Hölderlina	Hölderlin	k2eAgMnSc2d1	Hölderlin
a	a	k8xC	a
Poea	Poeus	k1gMnSc2	Poeus
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
četl	číst	k5eAaImAgMnS	číst
pak	pak	k6eAd1	pak
rovněž	rovněž	k9	rovněž
Puškina	Puškin	k2eAgFnSc1d1	Puškina
a	a	k8xC	a
Lermontova	Lermontův	k2eAgFnSc1d1	Lermontova
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
prospěch	prospěch	k1gInSc1	prospěch
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
trvale	trvale	k6eAd1	trvale
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
)	)	kIx)	)
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
klasické	klasický	k2eAgFnSc2d1	klasická
filologie	filologie	k1gFnSc2	filologie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
studoval	studovat	k5eAaImAgMnS	studovat
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
klasickou	klasický	k2eAgFnSc4d1	klasická
filologii	filologie	k1gFnSc4	filologie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
během	během	k7c2	během
nich	on	k3xPp3gFnPc2	on
ale	ale	k8xC	ale
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
i	i	k8xC	i
etymologii	etymologie	k1gFnSc6	etymologie
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
učitele	učitel	k1gMnPc4	učitel
patřili	patřit	k5eAaImAgMnP	patřit
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Goll	Goll	k1gMnSc1	Goll
či	či	k8xC	či
Otakar	Otakar	k1gMnSc1	Otakar
Hostinský	hostinský	k1gMnSc1	hostinský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
trávil	trávit	k5eAaImAgMnS	trávit
nejvíce	nejvíce	k6eAd1	nejvíce
času	čas	k1gInSc2	čas
v	v	k7c6	v
hospodách	hospodách	k?	hospodách
a	a	k8xC	a
kavárnách	kavárna	k1gFnPc6	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
studiu	studio	k1gNnSc6	studio
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
kartám	karta	k1gFnPc3	karta
a	a	k8xC	a
pitkám	pitka	k1gFnPc3	pitka
se	s	k7c7	s
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Herbenem	Herben	k1gMnSc7	Herben
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
Mrštíkem	Mrštík	k1gMnSc7	Mrštík
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
však	však	k9	však
propadal	propadat	k5eAaImAgMnS	propadat
melancholii	melancholie	k1gFnSc4	melancholie
a	a	k8xC	a
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Neúčastníil	Neúčastníit	k5eAaImAgMnS	Neúčastníit
se	se	k3xPyFc4	se
žádných	žádný	k3yNgInPc2	žádný
politických	politický	k2eAgInPc2d1	politický
ani	ani	k8xC	ani
národních	národní	k2eAgFnPc2d1	národní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
ani	ani	k9	ani
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
ani	ani	k8xC	ani
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
rodiny	rodina	k1gFnSc2	rodina
kritická	kritický	k2eAgFnSc1d1	kritická
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
zadlužit	zadlužit	k5eAaPmF	zadlužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uživila	uživit	k5eAaPmAgFnS	uživit
pět	pět	k4xCc4	pět
Vaškových	Vaškových	k2eAgMnPc2d1	Vaškových
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
hledat	hledat	k5eAaImF	hledat
nějaké	nějaký	k3yIgNnSc4	nějaký
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejevil	jevit	k5eNaImAgMnS	jevit
žádnou	žádný	k3yNgFnSc4	žádný
snahu	snaha	k1gFnSc4	snaha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
pomohl	pomoct	k5eAaPmAgMnS	pomoct
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
místo	místo	k7c2	místo
písaře	písař	k1gMnSc2	písař
zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
jen	jen	k9	jen
díky	díky	k7c3	díky
přímluvám	přímluva	k1gFnPc3	přímluva
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Rodině	rodina	k1gFnSc3	rodina
však	však	k9	však
nijak	nijak	k6eAd1	nijak
nepomáhal	pomáhat	k5eNaImAgMnS	pomáhat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgMnS	mít
tu	ten	k3xDgFnSc4	ten
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
apatii	apatie	k1gFnSc6	apatie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
jej	on	k3xPp3gMnSc4	on
dokázal	dokázat	k5eAaPmAgInS	dokázat
dostat	dostat	k5eAaPmF	dostat
jen	jen	k9	jen
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
cigarety	cigareta	k1gFnPc4	cigareta
a	a	k8xC	a
první	první	k4xOgInPc4	první
příležitostné	příležitostný	k2eAgInPc4d1	příležitostný
výlety	výlet	k1gInPc4	výlet
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Počaly	počnout	k5eAaPmAgInP	počnout
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
projevovat	projevovat	k5eAaImF	projevovat
první	první	k4xOgInPc4	první
příznaky	příznak	k1gInPc4	příznak
plicní	plicní	k2eAgFnSc2d1	plicní
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
psát	psát	k5eAaImF	psát
prozaické	prozaický	k2eAgFnSc2d1	prozaická
črty	črta	k1gFnSc2	črta
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
Café	café	k1gNnSc2	café
Lustig	Lustiga	k1gFnPc2	Lustiga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
otištěna	otisknout	k5eAaPmNgFnS	otisknout
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Švanda	Švanda	k1gMnSc1	Švanda
dudák	dudák	k1gMnSc1	dudák
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Ratibor	Ratibor	k1gMnSc1	Ratibor
Suk	Suk	k1gMnSc1	Suk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgMnS	začít
Bezruč	Bezruč	k1gMnSc1	Bezruč
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
poště	pošta	k1gFnSc6	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
složil	složit	k5eAaPmAgMnS	složit
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zaručila	zaručit	k5eAaPmAgFnS	zaručit
platový	platový	k2eAgInSc4d1	platový
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
ihned	ihned	k6eAd1	ihned
přidělen	přidělit	k5eAaPmNgInS	přidělit
na	na	k7c4	na
poštu	pošta	k1gFnSc4	pošta
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
<g/>
.	.	kIx.	.
</s>
<s>
Vaškův	Vaškův	k2eAgInSc1d1	Vaškův
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
<g/>
.	.	kIx.	.
</s>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
městečku	městečko	k1gNnSc6	městečko
Místek	místko	k1gNnPc2	místko
jako	jako	k8xS	jako
poštovní	poštovní	k2eAgMnSc1d1	poštovní
úředník	úředník	k1gMnSc1	úředník
místními	místní	k2eAgFnPc7d1	místní
automaticky	automaticky	k6eAd1	automaticky
přiřazen	přiřadit	k5eAaPmNgMnS	přiřadit
k	k	k7c3	k
"	"	kIx"	"
<g/>
honoraci	honorace	k1gFnSc3	honorace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tam	tam	k6eAd1	tam
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
nevázaném	vázaný	k2eNgInSc6d1	nevázaný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
všímat	všímat	k5eAaImF	všímat
velké	velký	k2eAgFnPc4d1	velká
lidské	lidský	k2eAgFnPc4d1	lidská
bídy	bída	k1gFnPc4	bída
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Koloredov	Koloredov	k1gInSc4	Koloredov
u	u	k7c2	u
Místku	Místek	k1gInSc2	Místek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
řezníka	řezník	k1gMnSc2	řezník
Lyska	lyska	k1gFnSc1	lyska
na	na	k7c6	na
Ostravské	ostravský	k2eAgFnSc6d1	Ostravská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
Vašek	Vašek	k1gMnSc1	Vašek
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
manželů	manžel	k1gMnPc2	manžel
Sagonových	Sagonová	k1gFnPc2	Sagonová
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Boleslavem	Boleslav	k1gMnSc7	Boleslav
Petrem	Petr	k1gMnSc7	Petr
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
osudové	osudový	k2eAgNnSc1d1	osudové
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Vaška	Vašek	k1gMnSc4	Vašek
velmi	velmi	k6eAd1	velmi
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
O.	O.	kA	O.
B.	B.	kA	B.
Petr	Petr	k1gMnSc1	Petr
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
divadelník	divadelník	k1gMnSc1	divadelník
a	a	k8xC	a
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
život	život	k1gInSc1	život
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
promítal	promítat	k5eAaImAgInS	promítat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
básních	báseň	k1gFnPc6	báseň
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
teoriím	teorie	k1gFnPc3	teorie
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
možném	možný	k2eAgNnSc6d1	možné
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
<g/>
)	)	kIx)	)
<g/>
autorství	autorství	k1gNnSc1	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
literáti	literát	k1gMnPc1	literát
psali	psát	k5eAaImAgMnP	psát
verše	verš	k1gInPc4	verš
a	a	k8xC	a
společně	společně	k6eAd1	společně
vyráželi	vyrážet	k5eAaImAgMnP	vyrážet
na	na	k7c4	na
túry	túra	k1gFnPc4	túra
po	po	k7c6	po
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Petr	Petr	k1gMnSc1	Petr
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
Vaška	Vašek	k1gMnSc4	Vašek
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
slezský	slezský	k2eAgMnSc1d1	slezský
vlastenec	vlastenec	k1gMnSc1	vlastenec
O.	O.	kA	O.
B.	B.	kA	B.
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obrátil	obrátit	k5eAaPmAgMnS	obrátit
Vaškovu	Vaškův	k2eAgFnSc4d1	Vaškova
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
a	a	k8xC	a
národní	národní	k2eAgFnSc3d1	národní
otázce	otázka	k1gFnSc3	otázka
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
Vaška	Vašek	k1gMnSc4	Vašek
také	také	k9	také
s	s	k7c7	s
Maryčkou	Maryčka	k1gFnSc7	Maryčka
Sagonovou	Sagonová	k1gFnSc7	Sagonová
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Maryčky	Maryčka	k1gFnSc2	Maryčka
Sagonové	Sagonová	k1gFnSc2	Sagonová
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Dody	Doda	k1gFnSc2	Doda
Bezručové	Bezručová	k1gFnSc2	Bezručová
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Bezručová	Bezručová	k1gFnSc1	Bezručová
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
přáteli	přítel	k1gMnPc7	přítel
Vaška	Vašek	k1gMnSc2	Vašek
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
Vaškův	Vaškův	k2eAgInSc1d1	Vaškův
pseudonym	pseudonym	k1gInSc1	pseudonym
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Místku	místko	k1gNnSc6	místko
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
přerod	přerod	k1gInSc1	přerod
apatického	apatický	k2eAgMnSc2d1	apatický
Vaška	Vašek	k1gMnSc2	Vašek
ve	v	k7c4	v
vášnivého	vášnivý	k2eAgMnSc4d1	vášnivý
zastánce	zastánce	k1gMnSc4	zastánce
práv	právo	k1gNnPc2	právo
chudých	chudý	k2eAgMnPc2d1	chudý
slezských	slezský	k2eAgMnPc2d1	slezský
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
však	však	k9	však
žádné	žádný	k3yNgNnSc4	žádný
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
již	již	k6eAd1	již
za	za	k7c2	za
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
básně	báseň	k1gFnPc1	báseň
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
svými	svůj	k3xOyFgFnPc7	svůj
reáliemi	reálie	k1gFnPc7	reálie
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c4	před
rok	rok	k1gInSc4	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
jiné	jiná	k1gFnPc1	jiná
naopak	naopak	k6eAd1	naopak
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
napsány	napsat	k5eAaPmNgFnP	napsat
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
popisují	popisovat	k5eAaImIp3nP	popisovat
události	událost	k1gFnPc1	událost
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
spáchal	spáchat	k5eAaPmAgMnS	spáchat
jeho	jeho	k3xOp3gMnSc1	jeho
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
O.	O.	kA	O.
B.	B.	kA	B.
Petr	Petr	k1gMnSc1	Petr
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
si	se	k3xPyFc3	se
podal	podat	k5eAaPmAgMnS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
přeložení	přeložení	k1gNnSc4	přeložení
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
kladně	kladně	k6eAd1	kladně
vyřízena	vyřídit	k5eAaPmNgFnS	vyřídit
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
nádražní	nádražní	k2eAgFnSc4d1	nádražní
poštu	pošta	k1gFnSc4	pošta
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Marie	k1gFnSc1	Marie
Vašková	Vašková	k1gFnSc1	Vašková
a	a	k8xC	a
Vašek	Vašek	k1gMnSc1	Vašek
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
starat	starat	k5eAaImF	starat
o	o	k7c4	o
svého	svůj	k3xOyFgMnSc4	svůj
mladšího	mladý	k2eAgMnSc4d2	mladší
sourozence	sourozenec	k1gMnSc4	sourozenec
Antonína	Antonín	k1gMnSc4	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ladislavem	Ladislav	k1gMnSc7	Ladislav
zase	zase	k9	zase
jezdíval	jezdívat	k5eAaImAgInS	jezdívat
do	do	k7c2	do
Kostelce	Kostelec	k1gInSc2	Kostelec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
Hané	Haná	k1gFnSc3	Haná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1899	[number]	k4	1899
začal	začít	k5eAaPmAgMnS	začít
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
zasílat	zasílat	k5eAaImF	zasílat
své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
Janu	Jan	k1gMnSc3	Jan
Herbenovi	Herben	k1gMnSc3	Herben
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Čas	čas	k1gInSc4	čas
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
a	a	k8xC	a
objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
chrlení	chrlení	k1gNnSc1	chrlení
krve	krev	k1gFnSc2	krev
-	-	kIx~	-
příznak	příznak	k1gInSc1	příznak
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Herben	Herben	k2eAgMnSc1d1	Herben
ihned	ihned	k6eAd1	ihned
poznal	poznat	k5eAaPmAgMnS	poznat
kvalitu	kvalita	k1gFnSc4	kvalita
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
je	být	k5eAaImIp3nS	být
vydávat	vydávat	k5eAaImF	vydávat
v	v	k7c6	v
beletristické	beletristický	k2eAgFnSc6d1	beletristická
příloze	příloha	k1gFnSc6	příloha
Času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
první	první	k4xOgFnSc2	první
zásilky	zásilka	k1gFnSc2	zásilka
otiskl	otisknout	k5eAaPmAgMnS	otisknout
tři	tři	k4xCgFnPc4	tři
básně	báseň	k1gFnPc4	báseň
<g/>
:	:	kIx,	:
Den	den	k1gInSc4	den
Palackého	Palackého	k2eAgInSc4d1	Palackého
<g/>
,	,	kIx,	,
Škaredý	škaredý	k2eAgInSc4d1	škaredý
zjev	zjev	k1gInSc4	zjev
a	a	k8xC	a
Jen	jen	k9	jen
jedenkrát	jedenkrát	k6eAd1	jedenkrát
(	(	kIx(	(
<g/>
též	též	k9	též
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Zkazka	zkazka	k1gFnSc1	zkazka
-	-	kIx~	-
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
časopisu	časopis	k1gInSc2	časopis
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
cenzura	cenzura	k1gFnSc1	cenzura
pak	pak	k6eAd1	pak
dovolila	dovolit	k5eAaPmAgFnS	dovolit
uveřejnit	uveřejnit	k5eAaPmF	uveřejnit
pouze	pouze	k6eAd1	pouze
intimní	intimní	k2eAgFnSc4d1	intimní
lyrickou	lyrický	k2eAgFnSc4d1	lyrická
báseň	báseň	k1gFnSc4	báseň
Jen	jen	k9	jen
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
zaslal	zaslat	k5eAaPmAgMnS	zaslat
Vašek	Vašek	k1gMnSc1	Vašek
celkem	celek	k1gInSc7	celek
74	[number]	k4	74
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
39	[number]	k4	39
zásilkách	zásilka	k1gFnPc6	zásilka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
měly	mít	k5eAaImAgFnP	mít
veliký	veliký	k2eAgInSc4d1	veliký
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
kladný	kladný	k2eAgInSc4d1	kladný
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
vydal	vydat	k5eAaPmAgMnS	vydat
Herben	Herben	k2eAgMnSc1d1	Herben
1	[number]	k4	1
<g/>
.	.	kIx.	.
číslo	číslo	k1gNnSc1	číslo
Besed	beseda	k1gFnPc2	beseda
Času	čas	k1gInSc2	čas
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Slezské	slezský	k2eAgNnSc4d1	Slezské
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
vychází	vycházet	k5eAaImIp3nS	vycházet
sbírka	sbírka	k1gFnSc1	sbírka
knižně	knižně	k6eAd1	knižně
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
3000	[number]	k4	3000
výtisků	výtisk	k1gInPc2	výtisk
(	(	kIx(	(
<g/>
Besedy	beseda	k1gFnSc2	beseda
Času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Knihovnička	knihovnička	k1gFnSc1	knihovnička
Času	čas	k1gInSc2	čas
1903	[number]	k4	1903
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
vyšly	vyjít	k5eAaPmAgFnP	vyjít
Bezručovy	Bezručův	k2eAgFnPc1d1	Bezručova
básně	báseň	k1gFnPc1	báseň
poprvé	poprvé	k6eAd1	poprvé
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnSc2d1	vydaná
Spolkem	spolek	k1gInSc7	spolek
českých	český	k2eAgMnPc2d1	český
bibliofilů	bibliofil	k1gMnPc2	bibliofil
v	v	k7c6	v
grafické	grafický	k2eAgFnSc6d1	grafická
úpravě	úprava	k1gFnSc6	úprava
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Preissiga	Preissig	k1gMnSc2	Preissig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
na	na	k7c6	na
veřejné	veřejný	k2eAgFnSc6d1	veřejná
přednášce	přednáška	k1gFnSc6	přednáška
Adolfa	Adolf	k1gMnSc2	Adolf
Kubise	Kubise	k1gFnSc2	Kubise
byla	být	k5eAaImAgFnS	být
prozrazena	prozrazen	k2eAgFnSc1d1	prozrazena
pravá	pravý	k2eAgFnSc1d1	pravá
identita	identita	k1gFnSc1	identita
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dvě	dva	k4xCgNnPc4	dva
knižní	knižní	k2eAgNnPc4d1	knižní
vydání	vydání	k1gNnPc4	vydání
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
odbojovém	odbojový	k2eAgInSc6d1	odbojový
časopise	časopis	k1gInSc6	časopis
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Indépendance	Indépendanec	k1gMnSc2	Indépendanec
Tchéque	Tchéqu	k1gMnSc2	Tchéqu
básně	báseň	k1gFnSc2	báseň
oslavující	oslavující	k2eAgNnPc4d1	oslavující
ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
jako	jako	k8xS	jako
osvoboditele	osvoboditel	k1gMnSc2	osvoboditel
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
iniciálami	iniciála	k1gFnPc7	iniciála
P.	P.	kA	P.
B.	B.	kA	B.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
veřejností	veřejnost	k1gFnSc7	veřejnost
okamžitě	okamžitě	k6eAd1	okamžitě
připisovány	připisovat	k5eAaImNgInP	připisovat
Petru	Petr	k1gMnSc6	Petr
Bezručovi	Bezruč	k1gMnSc6	Bezruč
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
těchto	tento	k3xDgFnPc2	tento
básní	báseň	k1gFnPc2	báseň
byl	být	k5eAaImAgMnS	být
však	však	k9	však
Jan	Jan	k1gMnSc1	Jan
Grmela	Grmel	k1gMnSc2	Grmel
<g/>
,	,	kIx,	,
ostravský	ostravský	k2eAgMnSc1d1	ostravský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
básník	básník	k1gMnSc1	básník
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
básně	báseň	k1gFnPc1	báseň
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Vaška	Vašek	k1gMnSc4	Vašek
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
odvezen	odvézt	k5eAaPmNgInS	odvézt
do	do	k7c2	do
posádkového	posádkový	k2eAgNnSc2d1	posádkové
vězení	vězení	k1gNnSc2	vězení
na	na	k7c4	na
Hernalsergurtlu	Hernalsergurtla	k1gFnSc4	Hernalsergurtla
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
proces	proces	k1gInSc4	proces
u	u	k7c2	u
vojenského	vojenský	k2eAgInSc2d1	vojenský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
protokolu	protokol	k1gInSc2	protokol
výslechu	výslech	k1gInSc2	výslech
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Politické	politický	k2eAgNnSc1d1	politické
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
mých	můj	k3xOp1gFnPc6	můj
básních	báseň	k1gFnPc6	báseň
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
se	se	k3xPyFc4	se
nejčistšímu	čistý	k2eAgInSc3d3	nejčistší
demokratismu	demokratismus	k1gInSc3	demokratismus
<g/>
.	.	kIx.	.
</s>
<s>
Moje	můj	k3xOp1gFnPc1	můj
básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
především	především	k9	především
pro	pro	k7c4	pro
dělnictvo	dělnictvo	k1gNnSc4	dělnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
celé	celý	k2eAgNnSc1d1	celé
zaměření	zaměření	k1gNnSc1	zaměření
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
oslavováním	oslavování	k1gNnSc7	oslavování
cara	car	k1gMnSc2	car
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
dělnictvem	dělnictvo	k1gNnSc7	dělnictvo
a	a	k8xC	a
demokraty	demokrat	k1gMnPc7	demokrat
nenáviděn	nenáviděn	k2eAgInSc4d1	nenáviděn
dnes	dnes	k6eAd1	dnes
právě	právě	k9	právě
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
báseň	báseň	k1gFnSc1	báseň
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
tendence	tendence	k1gFnPc1	tendence
<g/>
,	,	kIx,	,
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
v	v	k7c6	v
kterémsi	kterýsi	k3yIgInSc6	kterýsi
francouzském	francouzský	k2eAgInSc6d1	francouzský
časopise	časopis	k1gInSc6	časopis
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ode	ode	k7c2	ode
mne	já	k3xPp1nSc2	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nezabýval	zabývat	k5eNaImAgMnS	zabývat
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
jen	jen	k9	jen
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Policii	policie	k1gFnSc3	policie
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zajistit	zajistit	k5eAaPmF	zajistit
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
svědčící	svědčící	k2eAgFnSc1d1	svědčící
proti	proti	k7c3	proti
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Vaškovi	Vašek	k1gMnSc3	Vašek
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
vypracovat	vypracovat	k5eAaPmF	vypracovat
posudek	posudek	k1gInSc4	posudek
dvou	dva	k4xCgMnPc2	dva
literárních	literární	k2eAgMnPc2d1	literární
vědců	vědec	k1gMnPc2	vědec
(	(	kIx(	(
<g/>
německého	německý	k2eAgMnSc2d1	německý
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
Vaškově	Vaškův	k2eAgFnSc6d1	Vaškova
nevině	nevina	k1gFnSc6	nevina
<g/>
.	.	kIx.	.
</s>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
brněnské	brněnský	k2eAgFnSc2d1	brněnská
věznice	věznice	k1gFnSc2	věznice
na	na	k7c6	na
Cejlu	Cejl	k1gInSc6	Cejl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
dále	daleko	k6eAd2	daleko
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
a	a	k8xC	a
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
za	za	k7c4	za
nejútočnější	útoční	k2eAgFnPc4d3	útoční
básně	báseň	k1gFnPc4	báseň
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
německý	německý	k2eAgInSc1d1	německý
překlad	překlad	k1gInSc1	překlad
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Die	Die	k1gFnSc2	Die
schlesischen	schlesischen	k2eAgMnSc1d1	schlesischen
Lieder	Lieder	k1gMnSc1	Lieder
des	des	k1gNnSc2	des
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
<g/>
)	)	kIx)	)
s	s	k7c7	s
předmluvou	předmluva	k1gFnSc7	předmluva
Franze	Franze	k1gFnSc2	Franze
Werfela	Werfel	k1gMnSc2	Werfel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc4	jeho
součást	součást	k1gFnSc4	součást
byly	být	k5eAaImAgFnP	být
otištěny	otištěn	k2eAgFnPc1d1	otištěna
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
nejútočnější	útoční	k2eAgFnPc1d3	útoční
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
Bezručovi	Bezruč	k1gMnSc3	Bezruč
přiznáno	přiznán	k2eAgNnSc1d1	přiznáno
důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
spory	spor	k1gInPc1	spor
(	(	kIx(	(
<g/>
následně	následně	k6eAd1	následně
se	s	k7c7	s
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
táhly	táhnout	k5eAaImAgInP	táhnout
až	až	k9	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Manifest	manifest	k1gInSc1	manifest
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
žádající	žádající	k2eAgNnSc1d1	žádající
sebeurčení	sebeurčení	k1gNnSc1	sebeurčení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
Vašek	Vašek	k1gMnSc1	Vašek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
nepodepsal	podepsat	k5eNaPmAgMnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
vycházely	vycházet	k5eAaImAgFnP	vycházet
Slezské	Slezská	k1gFnPc1	Slezská
písně	píseň	k1gFnSc2	píseň
tiskem	tisk	k1gInSc7	tisk
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
a	a	k8xC	a
Vašek	Vašek	k1gMnSc1	Vašek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
národním	národní	k2eAgMnSc7d1	národní
<g/>
"	"	kIx"	"
básníkem	básník	k1gMnSc7	básník
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
však	však	k9	však
stahoval	stahovat	k5eAaImAgInS	stahovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
do	do	k7c2	do
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
utíkal	utíkat	k5eAaImAgMnS	utíkat
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
jakýmikoliv	jakýkoliv	k3yIgFnPc7	jakýkoliv
ovacemi	ovace	k1gFnPc7	ovace
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
podnikal	podnikat	k5eAaImAgMnS	podnikat
túry	túra	k1gFnPc4	túra
(	(	kIx(	(
<g/>
sám	sám	k3xTgInSc4	sám
jim	on	k3xPp3gMnPc3	on
říkal	říkat	k5eAaImAgMnS	říkat
"	"	kIx"	"
<g/>
výplazy	výplaz	k1gInPc1	výplaz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
po	po	k7c6	po
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
kondici	kondice	k1gFnSc6	kondice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
udržoval	udržovat	k5eAaImAgMnS	udržovat
otužováním	otužování	k1gNnSc7	otužování
(	(	kIx(	(
<g/>
rád	rád	k2eAgMnSc1d1	rád
se	se	k3xPyFc4	se
koupal	koupat	k5eAaImAgMnS	koupat
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
nahý	nahý	k2eAgInSc1d1	nahý
<g/>
)	)	kIx)	)
a	a	k8xC	a
výlety	výlet	k1gInPc4	výlet
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
devadesáti	devadesát	k4xCc2	devadesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
opavském	opavský	k2eAgInSc6d1	opavský
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
Otické	Otický	k2eAgFnSc6d1	Otická
ulici	ulice	k1gFnSc6	ulice
pod	pod	k7c7	pod
náhrobkem	náhrobek	k1gInSc7	náhrobek
ze	z	k7c2	z
slezské	slezský	k2eAgFnSc2d1	Slezská
žuly	žula	k1gFnSc2	žula
se	s	k7c7	s
sedící	sedící	k2eAgFnSc7d1	sedící
sochou	socha	k1gFnSc7	socha
slezské	slezský	k2eAgFnSc2d1	Slezská
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gInSc2	Bezruč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
naprosto	naprosto	k6eAd1	naprosto
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
originalita	originalita	k1gFnSc1	originalita
netkví	tkvět	k5eNaImIp3nS	tkvět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
(	(	kIx(	(
<g/>
jediném	jediný	k2eAgInSc6d1	jediný
<g/>
)	)	kIx)	)
prudkém	prudký	k2eAgInSc6d1	prudký
básnickém	básnický	k2eAgInSc6d1	básnický
výboji	výboj	k1gInSc6	výboj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
sociálními	sociální	k2eAgInPc7d1	sociální
a	a	k8xC	a
národnostními	národnostní	k2eAgInPc7d1	národnostní
problémy	problém	k1gInPc7	problém
Slezska	Slezsko	k1gNnSc2	Slezsko
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
ho	on	k3xPp3gInSc4	on
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
autory	autor	k1gMnPc4	autor
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
buřičské	buřičský	k2eAgNnSc1d1	buřičské
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
především	především	k6eAd1	především
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vztah	vztah	k1gInSc4	vztah
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Vaška	Vašek	k1gMnSc4	Vašek
k	k	k7c3	k
vlastnímu	vlastní	k2eAgMnSc3d1	vlastní
"	"	kIx"	"
<g/>
jedinému	jediné	k1gNnSc3	jediné
<g/>
"	"	kIx"	"
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
tak	tak	k6eAd1	tak
prudce	prudko	k6eAd1	prudko
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
chudých	chudý	k2eAgMnPc2d1	chudý
a	a	k8xC	a
utlačovaných	utlačovaný	k2eAgMnPc2d1	utlačovaný
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
jedenkrát	jedenkrát	k6eAd1	jedenkrát
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgMnPc1d1	literární
teoretici	teoretik	k1gMnPc1	teoretik
přicházeli	přicházet	k5eAaImAgMnP	přicházet
s	s	k7c7	s
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
teoriemi	teorie	k1gFnPc7	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
mnoho	mnoho	k4c1	mnoho
nejasností	nejasnost	k1gFnPc2	nejasnost
ohledně	ohledně	k7c2	ohledně
života	život	k1gInSc2	život
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
jeho	jeho	k3xOp3gFnPc2	jeho
básní	báseň	k1gFnPc2	báseň
je	být	k5eAaImIp3nS	být
dramaticky	dramaticky	k6eAd1	dramaticky
vzdorná	vzdorný	k2eAgFnSc1d1	vzdorná
a	a	k8xC	a
útočná	útočný	k2eAgFnSc1d1	útočná
<g/>
,	,	kIx,	,
používáním	používání	k1gNnSc7	používání
výhrůžných	výhrůžný	k2eAgFnPc2d1	výhrůžná
apostrof	apostrofa	k1gFnPc2	apostrofa
(	(	kIx(	(
<g/>
zvolání	zvolání	k1gNnSc2	zvolání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hyperbol	hyperbola	k1gFnPc2	hyperbola
<g/>
,	,	kIx,	,
kontrastů	kontrast	k1gInPc2	kontrast
<g/>
,	,	kIx,	,
sžíravého	sžíravý	k2eAgInSc2d1	sžíravý
sarkasmu	sarkasmus	k1gInSc2	sarkasmus
<g/>
,	,	kIx,	,
záměrného	záměrný	k2eAgNnSc2d1	záměrné
porušování	porušování	k1gNnSc2	porušování
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
využívání	využívání	k1gNnSc4	využívání
nářečních	nářeční	k2eAgInPc2d1	nářeční
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
zvukomalby	zvukomalba	k1gFnSc2	zvukomalba
nás	my	k3xPp1nPc4	my
nutí	nutit	k5eAaImIp3nS	nutit
zamyslet	zamyslet	k5eAaPmF	zamyslet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
tématy	téma	k1gNnPc7	téma
jeho	jeho	k3xOp3gFnPc2	jeho
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
s	s	k7c7	s
národní	národní	k2eAgFnSc7d1	národní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc7d1	sociální
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
však	však	k9	však
např.	např.	kA	např.
i	i	k8xC	i
básně	báseň	k1gFnPc1	báseň
milostné	milostný	k2eAgFnPc1d1	milostná
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vydáno	vydat	k5eAaPmNgNnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slezské	slezský	k2eAgNnSc1d1	Slezské
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
v	v	k7c6	v
"	"	kIx"	"
<g/>
Knihovničce	knihovnička	k1gFnSc6	knihovnička
Času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
získalo	získat	k5eAaPmAgNnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
známý	známý	k2eAgInSc4d1	známý
název	název	k1gInSc4	název
Slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
básně	báseň	k1gFnPc1	báseň
ze	z	k7c2	z
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
intimní	intimní	k2eAgFnPc1d1	intimní
básně	báseň	k1gFnPc1	báseň
osobního	osobní	k2eAgInSc2d1	osobní
charakteru	charakter	k1gInSc2	charakter
<g/>
:	:	kIx,	:
Labutinka	labutinka	k1gFnSc1	labutinka
-	-	kIx~	-
posmrtně	posmrtně	k6eAd1	posmrtně
nalezená	nalezený	k2eAgFnSc1d1	nalezená
báseň	báseň	k1gFnSc1	báseň
<g/>
;	;	kIx,	;
formálně	formálně	k6eAd1	formálně
přesunuta	přesunut	k2eAgFnSc1d1	přesunuta
do	do	k7c2	do
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Jen	jen	k9	jen
jedenkrát	jedenkrát	k6eAd1	jedenkrát
Červený	červený	k2eAgInSc4d1	červený
květ	květ	k1gInSc4	květ
-	-	kIx~	-
úvodní	úvodní	k2eAgFnSc4d1	úvodní
báseň	báseň	k1gFnSc4	báseň
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
květina	květina	k1gFnSc1	květina
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
nitra	nitro	k1gNnSc2	nitro
<g/>
,	,	kIx,	,
kaktus	kaktus	k1gInSc1	kaktus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jako	jako	k8xS	jako
květina	květina	k1gFnSc1	květina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vykvete	vykvést	k5eAaPmIp3nS	vykvést
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
jeho	jeho	k3xOp3gFnSc4	jeho
životní	životní	k2eAgFnSc4d1	životní
osamělost	osamělost	k1gFnSc4	osamělost
básně	báseň	k1gFnSc2	báseň
reagující	reagující	k2eAgFnSc2d1	reagující
na	na	k7c4	na
sociální	sociální	k2eAgFnSc4d1	sociální
bídu	bída	k1gFnSc4	bída
a	a	k8xC	a
národnostní	národnostní	k2eAgInSc1d1	národnostní
útlak	útlak	k1gInSc1	útlak
(	(	kIx(	(
<g/>
problém	problém	k1gInSc1	problém
utlačování	utlačování	k1gNnSc2	utlačování
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
viní	vinit	k5eAaImIp3nS	vinit
z	z	k7c2	z
toho	ten	k3xDgMnSc4	ten
Němce	Němec	k1gMnSc4	Němec
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc4	Polák
<g/>
,	,	kIx,	,
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
lhostejnost	lhostejnost	k1gFnSc4	lhostejnost
Čechů	Čech	k1gMnPc2	Čech
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Maryčka	Maryčka	k1gFnSc1	Maryčka
Magdónova	Magdónův	k2eAgFnSc1d1	Magdónova
-	-	kIx~	-
Maryčka	Maryčka	k1gFnSc1	Maryčka
je	být	k5eAaImIp3nS	být
sirotek	sirotek	k1gMnSc1	sirotek
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
opilec	opilec	k1gMnSc1	opilec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zabil	zabít	k5eAaPmAgMnS	zabít
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
hospody	hospody	k?	hospody
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maryčka	Maryčka	k1gFnSc1	Maryčka
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
mladší	mladý	k2eAgMnPc4d2	mladší
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
krádeži	krádež	k1gFnSc6	krádež
dříví	dříví	k1gNnSc2	dříví
chytí	chytit	k5eAaPmIp3nS	chytit
Hochfelder	Hochfelder	k1gInSc1	Hochfelder
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
udá	udat	k5eAaPmIp3nS	udat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
odvádí	odvádět	k5eAaImIp3nS	odvádět
strážník	strážník	k1gMnSc1	strážník
do	do	k7c2	do
"	"	kIx"	"
<g/>
Frydku	frydka	k1gFnSc4	frydka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skok	k1gInSc7	skok
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
kriticky	kriticky	k6eAd1	kriticky
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
sloce	sloka	k1gFnSc6	sloka
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
ptá	ptat	k5eAaImIp3nS	ptat
Maryčky	Maryčka	k1gFnPc4	Maryčka
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
Kantor	Kantor	k1gMnSc1	Kantor
Halfar	Halfar	k1gMnSc1	Halfar
-	-	kIx~	-
příběh	příběh	k1gInSc1	příběh
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vzdoroval	vzdorovat	k5eAaImAgInS	vzdorovat
tlaku	tlak	k1gInSc3	tlak
odnárodňování	odnárodňování	k1gNnSc2	odnárodňování
a	a	k8xC	a
učil	učít	k5eAaPmAgMnS	učít
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
70	[number]	k4	70
000	[number]	k4	000
Bernard	Bernard	k1gMnSc1	Bernard
Žár	žár	k1gInSc1	žár
Návrat	návrat	k1gInSc1	návrat
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
soudobou	soudobý	k2eAgFnSc4d1	soudobá
českou	český	k2eAgFnSc4d1	Česká
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
<g/>
:	:	kIx,	:
Praga	Praga	k1gFnSc1	Praga
caput	caput	k2eAgInSc1d1	caput
regni	regeň	k1gFnSc3	regeň
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
estetická	estetický	k2eAgFnSc1d1	estetická
hodnota	hodnota	k1gFnSc1	hodnota
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
různorodých	různorodý	k2eAgInPc2d1	různorodý
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
tendencí	tendence	k1gFnPc2	tendence
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
poetik	poetik	k1gMnSc1	poetik
Škaredý	škaredý	k2eAgInSc4d1	škaredý
zjev	zjev	k1gInSc4	zjev
-	-	kIx~	-
nepřímým	přímý	k2eNgInSc7d1	nepřímý
obrazem	obraz	k1gInSc7	obraz
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
popisuje	popisovat	k5eAaImIp3nS	popisovat
těžký	těžký	k2eAgInSc4d1	těžký
život	život	k1gInSc4	život
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
za	za	k7c4	za
věčného	věčný	k2eAgMnSc4d1	věčný
tuláka	tulák	k1gMnSc4	tulák
<g/>
.	.	kIx.	.
básně	báseň	k1gFnSc2	báseň
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
obcím	obec	k1gFnPc3	obec
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
Polská	polský	k2eAgFnSc1d1	polská
Ostrava	Ostrava	k1gFnSc1	Ostrava
-	-	kIx~	-
báseň	báseň	k1gFnSc1	báseň
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
přejmenování	přejmenování	k1gNnSc3	přejmenování
Polské	polský	k2eAgFnSc2d1	polská
Ostravy	Ostrava	k1gFnSc2	Ostrava
na	na	k7c4	na
Slezskou	slezský	k2eAgFnSc4d1	Slezská
Ostravu	Ostrava	k1gFnSc4	Ostrava
Dombrová	Dombrová	k1gFnSc1	Dombrová
-	-	kIx~	-
protest	protest	k1gInSc1	protest
proti	proti	k7c3	proti
českým	český	k2eAgFnPc3d1	Česká
snahám	snaha	k1gFnPc3	snaha
přejmenovávat	přejmenovávat	k5eAaImF	přejmenovávat
vesnice	vesnice	k1gFnPc4	vesnice
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
Valčice	Valčice	k1gFnSc2	Valčice
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
připojení	připojení	k1gNnSc2	připojení
Valtic	Valtice	k1gFnPc2	Valtice
k	k	k7c3	k
Moravě	Morava	k1gFnSc3	Morava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Děrné	děrný	k2eAgInPc1d1	děrný
<g/>
,	,	kIx,	,
Melč	Melč	k1gInSc1	Melč
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
otevření	otevření	k1gNnSc2	otevření
nové	nový	k2eAgFnSc2d1	nová
české	český	k2eAgFnSc2d1	Česká
školy	škola	k1gFnSc2	škola
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
Mohelnice	Mohelnice	k1gFnSc1	Mohelnice
Některé	některý	k3yIgFnPc4	některý
básně	báseň	k1gFnPc4	báseň
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
a	a	k8xC	a
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
písničkář	písničkář	k1gMnSc1	písničkář
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
.	.	kIx.	.
</s>
<s>
Paralipomena	paralipomenon	k1gNnPc1	paralipomenon
I.	I.	kA	I.
Paralipomena	paralipomenon	k1gNnSc2	paralipomenon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stužkonoska	stužkonoska	k1gFnSc1	stužkonoska
modrá	modrý	k2eAgFnSc1d1	modrá
-	-	kIx~	-
druh	druh	k1gInSc1	druh
vzácné	vzácný	k2eAgFnSc2d1	vzácná
můry	můra	k1gFnSc2	můra
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
básníkova	básníkův	k2eAgInSc2d1	básníkův
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc1	hledání
smyslu	smysl	k1gInSc2	smysl
<g/>
,	,	kIx,	,
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
(	(	kIx(	(
<g/>
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
míře	míra	k1gFnSc6	míra
i	i	k9	i
kritika	kritika	k1gFnSc1	kritika
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
první	první	k4xOgFnSc6	první
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
nepřátelům	nepřítel	k1gMnPc3	nepřítel
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
milostnou	milostný	k2eAgFnSc4d1	milostná
lyriku	lyrika	k1gFnSc4	lyrika
a	a	k8xC	a
verše	verš	k1gInPc4	verš
přemýšlející	přemýšlející	k2eAgInPc4d1	přemýšlející
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
kafé	kafé	k?	kafé
Lustig	Lustig	k1gMnSc1	Lustig
Alois	Alois	k1gMnSc1	Alois
Adamus	Adamus	k1gMnSc1	Adamus
František	František	k1gMnSc1	František
Buriánek	Buriánek	k1gMnSc1	Buriánek
Miroslav	Miroslav	k1gMnSc1	Miroslav
Červenka	Červenka	k1gMnSc1	Červenka
Jaromír	Jaromír	k1gMnSc1	Jaromír
Dvořák	Dvořák	k1gMnSc1	Dvořák
Viktor	Viktor	k1gMnSc1	Viktor
Ficek	ficka	k1gFnPc2	ficka
Oldřich	Oldřich	k1gMnSc1	Oldřich
Králík	Králík	k1gMnSc1	Králík
Alois	Alois	k1gMnSc1	Alois
Miarka	Miarka	k1gMnSc1	Miarka
st.	st.	kA	st.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
Alois	Alois	k1gMnSc1	Alois
Sivek	sivka	k1gFnPc2	sivka
Drahomír	Drahomír	k1gMnSc1	Drahomír
Šajtar	Šajtar	k1gMnSc1	Šajtar
Jiří	Jiří	k1gMnSc1	Jiří
Urbanec	Urbanec	k1gMnSc1	Urbanec
Joža	Joža	k1gMnSc1	Joža
Vochala	Vochal	k1gMnSc4	Vochal
</s>
