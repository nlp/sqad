<p>
<s>
BMP	BMP	kA	BMP
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
Bitmap	bitmapa	k1gFnPc2	bitmapa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
DIB	DIB	kA	DIB
(	(	kIx(	(
<g/>
device-independent	devicendependent	k1gMnSc1	device-independent
bitmap	bitmapa	k1gFnPc2	bitmapa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počítačový	počítačový	k2eAgInSc1d1	počítačový
formát	formát	k1gInSc1	formát
pro	pro	k7c4	pro
ukládaní	ukládaný	k2eAgMnPc1d1	ukládaný
rastrové	rastrový	k2eAgMnPc4d1	rastrový
grafiky	grafik	k1gMnPc4	grafik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
BMP	BMP	kA	BMP
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
verze	verze	k1gFnSc1	verze
1.10	[number]	k4	1.10
SE	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
trochu	trochu	k6eAd1	trochu
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
jeho	jeho	k3xOp3gFnSc4	jeho
definici	definice	k1gFnSc4	definice
a	a	k8xC	a
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
tehdy	tehdy	k6eAd1	tehdy
nejprodávanějšího	prodávaný	k2eAgInSc2d3	nejprodávanější
16	[number]	k4	16
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
grafického	grafický	k2eAgNnSc2d1	grafické
operačního	operační	k2eAgNnSc2d1	operační
prostředí	prostředí	k1gNnSc2	prostředí
–	–	k?	–
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
první	první	k4xOgFnSc2	první
32	[number]	k4	32
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
systém	systém	k1gInSc1	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
vylepšenou	vylepšený	k2eAgFnSc4d1	vylepšená
variantu	varianta	k1gFnSc4	varianta
BMP	BMP	kA	BMP
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
strukturou	struktura	k1gFnSc7	struktura
pro	pro	k7c4	pro
uskladnění	uskladnění	k1gNnSc4	uskladnění
vícenásobných	vícenásobný	k2eAgFnPc2d1	vícenásobná
bitových	bitový	k2eAgFnPc2d1	bitová
map	mapa	k1gFnPc2	mapa
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
souboru	soubor	k1gInSc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
souboru	soubor	k1gInSc2	soubor
se	se	k3xPyFc4	se
často	často	k6eAd1	často
obecně	obecně	k6eAd1	obecně
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
bitmapové	bitmapový	k2eAgNnSc1d1	bitmapové
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgInSc2	tento
formátu	formát	k1gInSc2	formát
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
extrémní	extrémní	k2eAgFnSc1d1	extrémní
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
dokumentovanost	dokumentovanost	k1gFnSc1	dokumentovanost
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
volné	volný	k2eAgNnSc1d1	volné
použití	použití	k1gNnSc1	použití
není	být	k5eNaImIp3nS	být
znemožněno	znemožnit	k5eAaPmNgNnS	znemožnit
patentovou	patentový	k2eAgFnSc7d1	patentová
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
jej	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
snadno	snadno	k6eAd1	snadno
číst	číst	k5eAaImF	číst
i	i	k8xC	i
zapisovat	zapisovat	k5eAaImF	zapisovat
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
grafických	grafický	k2eAgInPc2d1	grafický
editorů	editor	k1gInPc2	editor
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
používá	používat	k5eAaImIp3nS	používat
podobný	podobný	k2eAgInSc1d1	podobný
formát	formát	k1gInSc1	formát
XBM	XBM	kA	XBM
pro	pro	k7c4	pro
jednobitové	jednobitový	k2eAgInPc4d1	jednobitový
černobílé	černobílý	k2eAgInPc4d1	černobílý
obrázky	obrázek	k1gInPc4	obrázek
a	a	k8xC	a
XPM	XPM	kA	XPM
pro	pro	k7c4	pro
barevné	barevný	k2eAgInPc4d1	barevný
obrázky	obrázek	k1gInPc4	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
BMP	BMP	kA	BMP
jsou	být	k5eAaImIp3nP	být
ukládány	ukládat	k5eAaImNgFnP	ukládat
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
pixelech	pixel	k1gInPc6	pixel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
bitů	bit	k1gInPc2	bit
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
reprezentaci	reprezentace	k1gFnSc4	reprezentace
každého	každý	k3xTgInSc2	každý
pixelu	pixel	k1gInSc2	pixel
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozlišit	rozlišit	k5eAaPmF	rozlišit
různé	různý	k2eAgNnSc4d1	různé
množství	množství	k1gNnSc4	množství
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
barevná	barevný	k2eAgFnSc1d1	barevná
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2	[number]	k4	2
barvy	barva	k1gFnPc1	barva
(	(	kIx(	(
<g/>
1	[number]	k4	1
bit	bit	k1gInSc1	bit
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
16	[number]	k4	16
(	(	kIx(	(
<g/>
4	[number]	k4	4
bity	bit	k1gInPc7	bit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
256	[number]	k4	256
(	(	kIx(	(
<g/>
8	[number]	k4	8
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
65	[number]	k4	65
536	[number]	k4	536
(	(	kIx(	(
<g/>
16	[number]	k4	16
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
16,7	[number]	k4	16,7
miliónů	milión	k4xCgInPc2	milión
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
24	[number]	k4	24
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osmibitové	osmibitový	k2eAgInPc1d1	osmibitový
obrázky	obrázek	k1gInPc1	obrázek
mohou	moct	k5eAaImIp3nP	moct
místo	místo	k7c2	místo
barev	barva	k1gFnPc2	barva
používat	používat	k5eAaImF	používat
šedou	šedý	k2eAgFnSc4d1	šedá
škálu	škála	k1gFnSc4	škála
(	(	kIx(	(
<g/>
256	[number]	k4	256
odstínů	odstín	k1gInPc2	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
barevnou	barevný	k2eAgFnSc4d1	barevná
paletu	paleta	k1gFnSc4	paleta
(	(	kIx(	(
<g/>
CLUT	CLUT	kA	CLUT
-	-	kIx~	-
Color	Color	k1gMnSc1	Color
LookUp	LookUp	k1gMnSc1	LookUp
Table	tablo	k1gNnSc6	tablo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soubory	soubor	k1gInPc1	soubor
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
BMP	BMP	kA	BMP
většinou	většinou	k6eAd1	většinou
nepoužívají	používat	k5eNaImIp3nP	používat
žádnou	žádný	k3yNgFnSc4	žádný
kompresi	komprese	k1gFnSc4	komprese
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
varianty	varianta	k1gFnPc1	varianta
používající	používající	k2eAgFnSc4d1	používající
kompresi	komprese	k1gFnSc4	komprese
RLE	RLE	kA	RLE
–	–	k?	–
run-length	runength	k1gInSc1	run-length
encoding	encoding	k1gInSc1	encoding
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
BMP	BMP	kA	BMP
soubory	soubor	k1gInPc1	soubor
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
obrázky	obrázek	k1gInPc1	obrázek
stejného	stejný	k2eAgInSc2d1	stejný
rozměru	rozměr	k1gInSc2	rozměr
uložené	uložený	k2eAgInPc1d1	uložený
ve	v	k7c6	v
formátech	formát	k1gInPc6	formát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kompresi	komprese	k1gFnSc4	komprese
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ukládání	ukládání	k1gNnSc4	ukládání
obrázků	obrázek	k1gInPc2	obrázek
vyžadujících	vyžadující	k2eAgInPc2d1	vyžadující
zachování	zachování	k1gNnSc4	zachování
všech	všecek	k3xTgFnPc2	všecek
informací	informace	k1gFnPc2	informace
používají	používat	k5eAaImIp3nP	používat
spíše	spíše	k9	spíše
novější	nový	k2eAgInPc4d2	novější
formáty	formát	k1gInPc4	formát
PNG	PNG	kA	PNG
<g/>
,	,	kIx,	,
GIF	GIF	kA	GIF
nebo	nebo	k8xC	nebo
také	také	k9	také
TIFF	TIFF	kA	TIFF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
nekomprimovaného	komprimovaný	k2eNgInSc2d1	nekomprimovaný
obrázku	obrázek	k1gInSc2	obrázek
v	v	k7c6	v
bajtech	bajt	k1gInPc6	bajt
lze	lze	k6eAd1	lze
přibližně	přibližně	k6eAd1	přibližně
vypočítat	vypočítat	k5eAaPmF	vypočítat
podle	podle	k7c2	podle
vzorce	vzorec	k1gInSc2	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
šířka	šířka	k1gFnSc1	šířka
v	v	k7c6	v
pixelech	pixel	k1gInPc6	pixel
<g/>
)	)	kIx)	)
*	*	kIx~	*
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
pixelech	pixel	k1gInPc6	pixel
<g/>
)	)	kIx)	)
*	*	kIx~	*
(	(	kIx(	(
<g/>
bitů	bit	k1gInPc2	bit
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
/	/	kIx~	/
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
K	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
obrázku	obrázek	k1gInSc2	obrázek
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ještě	ještě	k6eAd1	ještě
připočítat	připočítat	k5eAaPmF	připočítat
velikost	velikost	k1gFnSc4	velikost
hlavičky	hlavička	k1gFnSc2	hlavička
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gFnSc2	jeho
verze	verze	k1gFnSc2	verze
i	i	k9	i
dle	dle	k7c2	dle
použité	použitý	k2eAgFnSc2d1	použitá
barevné	barevný	k2eAgFnSc2d1	barevná
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
800	[number]	k4	800
<g/>
×	×	k?	×
<g/>
600	[number]	k4	600
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
s	s	k7c7	s
16,7	[number]	k4	16,7
miliony	milion	k4xCgInPc4	milion
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
3	[number]	k4	3
bajty	bajt	k1gInPc7	bajt
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
<g/>
)	)	kIx)	)
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
téměř	téměř	k6eAd1	téměř
1,4	[number]	k4	1,4
megabajtu	megabajt	k1gInSc2	megabajt
<g/>
.	.	kIx.	.
</s>
<s>
Formát	formát	k1gInSc1	formát
BMP	BMP	kA	BMP
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zcela	zcela	k6eAd1	zcela
nevhodný	vhodný	k2eNgMnSc1d1	nevhodný
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
4	[number]	k4	4
varianty	varianta	k1gFnPc4	varianta
formátu	formát	k1gInSc2	formát
BMP	BMP	kA	BMP
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
příslušným	příslušný	k2eAgNnSc7d1	příslušné
OS	OS	kA	OS
(	(	kIx(	(
<g/>
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
verze	verze	k1gFnPc1	verze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
and	and	k?	and
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
</s>
</p>
<p>
<s>
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
</s>
</p>
<p>
<s>
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
XZdroj	XZdroj	k1gInSc1	XZdroj
<g/>
:	:	kIx,	:
Murray	Murraa	k1gFnSc2	Murraa
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Vanryper	Vanryper	k1gMnSc1	Vanryper
W.	W.	kA	W.
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
grafických	grafický	k2eAgInPc2d1	grafický
formátů	formát	k1gInPc2	formát
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
922	[number]	k4	922
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7226	[number]	k4	7226
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
str	str	kA	str
<g/>
.	.	kIx.	.
491	[number]	k4	491
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
souboru	soubor	k1gInSc2	soubor
==	==	k?	==
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
BMP	BMP	kA	BMP
verze	verze	k1gFnSc1	verze
1	[number]	k4	1
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
souborovou	souborový	k2eAgFnSc4d1	souborová
hlavičku	hlavička	k1gFnSc4	hlavička
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
16	[number]	k4	16
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
a	a	k8xC	a
nekomprimovaná	komprimovaný	k2eNgNnPc4d1	nekomprimované
bitmapová	bitmapový	k2eAgNnPc4d1	bitmapové
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
BMP	BMP	kA	BMP
verze	verze	k1gFnSc1	verze
3	[number]	k4	3
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bitmapovou	bitmapový	k2eAgFnSc4d1	bitmapová
hlavičku	hlavička	k1gFnSc4	hlavička
(	(	kIx(	(
<g/>
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
14	[number]	k4	14
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
verzí	verze	k1gFnSc7	verze
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnSc4d1	informační
hlavičku	hlavička	k1gFnSc4	hlavička
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
40	[number]	k4	40
byte	byte	k1gInSc1	byte
<g/>
,	,	kIx,	,
paletu	paleta	k1gFnSc4	paleta
(	(	kIx(	(
<g/>
proměnná	proměnný	k2eAgFnSc1d1	proměnná
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
bitmapu	bitmapa	k1gFnSc4	bitmapa
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
paleta	paleta	k1gFnSc1	paleta
je	být	k5eAaImIp3nS	být
volitelná	volitelný	k2eAgFnSc1d1	volitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Palety	paleta	k1gFnSc2	paleta
===	===	k?	===
</s>
</p>
<p>
<s>
Barevné	barevný	k2eAgFnPc1d1	barevná
palety	paleta	k1gFnPc1	paleta
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
souborech	soubor	k1gInPc6	soubor
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
pod	pod	k7c7	pod
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
MS	MS	kA	MS
Windows	Windows	kA	Windows
a	a	k8xC	a
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
2.0	[number]	k4	2.0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
MS	MS	kA	MS
Windows	Windows	kA	Windows
verze	verze	k1gFnSc1	verze
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
paleta	paleta	k1gFnSc1	paleta
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
4	[number]	k4	4
<g/>
bytových	bytový	k2eAgFnPc2d1	bytová
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
RGBQUAD	RGBQUAD	kA	RGBQUAD
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
byte	byte	k1gInSc4	byte
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
barvě	barva	k1gFnSc6	barva
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
byte	byte	k1gInSc1	byte
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nastaven	nastaven	k2eAgMnSc1d1	nastaven
na	na	k7c4	na
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
definované	definovaný	k2eAgFnPc1d1	definovaná
v	v	k7c6	v
paletě	paleta	k1gFnSc6	paleta
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
obrázku	obrázek	k1gInSc6	obrázek
uloženo	uložit	k5eAaPmNgNnS	uložit
méně	málo	k6eAd2	málo
než	než	k8xS	než
16	[number]	k4	16
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
setřídit	setřídit	k5eAaPmF	setřídit
barvy	barva	k1gFnPc4	barva
podle	podle	k7c2	podle
četnosti	četnost	k1gFnSc2	četnost
a	a	k8xC	a
umístit	umístit	k5eAaPmF	umístit
barvy	barva	k1gFnPc4	barva
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
výskytem	výskyt	k1gInSc7	výskyt
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
aplikacích	aplikace	k1gFnPc6	aplikace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednobitové	jednobitový	k2eAgInPc4d1	jednobitový
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
invertují	invertovat	k5eAaBmIp3nP	invertovat
barevný	barevný	k2eAgInSc4d1	barevný
smysl	smysl	k1gInSc4	smysl
obrázku	obrázek	k1gInSc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
změněná	změněný	k2eAgFnSc1d1	změněná
na	na	k7c4	na
černou	černá	k1gFnSc4	černá
a	a	k8xC	a
černá	černat	k5eAaImIp3nS	černat
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
u	u	k7c2	u
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
znaky	znak	k1gInPc1	znak
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrazová	obrazový	k2eAgNnPc1d1	obrazové
data	datum	k1gNnPc1	datum
===	===	k?	===
</s>
</p>
<p>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
BMP	BMP	kA	BMP
souboru	soubor	k1gInSc2	soubor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
bitmapová	bitmapový	k2eAgNnPc4d1	bitmapové
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nekomprimovaná	komprimovaný	k2eNgFnSc1d1	nekomprimovaná
nebo	nebo	k8xC	nebo
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
RLE	RLE	kA	RLE
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
<s>
Obrazová	obrazový	k2eAgNnPc1d1	obrazové
data	datum	k1gNnPc1	datum
v	v	k7c6	v
BMP	BMP	kA	BMP
souboru	soubor	k1gInSc6	soubor
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
bitově	bitově	k6eAd1	bitově
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
čtena	číst	k5eAaImNgNnP	číst
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
bitů	bit	k1gInPc2	bit
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
komprimována	komprimován	k2eAgNnPc4d1	komprimováno
obrazová	obrazový	k2eAgNnPc4d1	obrazové
data	datum	k1gNnPc4	datum
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednobitových	jednobitový	k2eAgInPc6d1	jednobitový
obrázcích	obrázek	k1gInPc6	obrázek
představuje	představovat	k5eAaImIp3nS	představovat
každý	každý	k3xTgInSc4	každý
byte	byte	k1gInSc4	byte
osm	osm	k4xCc4	osm
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
bit	bit	k1gInSc1	bit
v	v	k7c6	v
bytu	byt	k1gInSc6	byt
(	(	kIx(	(
<g/>
MSB	MSB	kA	MSB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
pixelová	pixelový	k2eAgFnSc1d1	pixelová
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
obrázky	obrázek	k1gInPc1	obrázek
používají	používat	k5eAaImIp3nP	používat
tří	tři	k4xCgFnPc2	tři
byte	byte	k1gInSc1	byte
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
<g/>
,	,	kIx,	,
uložené	uložený	k2eAgInPc1d1	uložený
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
modrý	modrý	k2eAgInSc4d1	modrý
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
scanovací	scanovací	k2eAgFnSc1d1	scanovací
řádka	řádka	k1gFnSc1	řádka
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
násobkem	násobek	k1gInSc7	násobek
čtyř	čtyři	k4xCgNnPc2	čtyři
byte	byte	k1gInSc1	byte
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
doplněn	doplněn	k2eAgInSc1d1	doplněn
nulami	nula	k1gFnPc7	nula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
obrázky	obrázek	k1gInPc1	obrázek
používají	používat	k5eAaImIp3nP	používat
palety	paleta	k1gFnPc1	paleta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
24	[number]	k4	24
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
obrázky	obrázek	k1gInPc1	obrázek
nikdy	nikdy	k6eAd1	nikdy
paletu	paleta	k1gFnSc4	paleta
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gNnPc1	jejich
pixelová	pixelový	k2eAgNnPc1d1	pixelový
barevná	barevný	k2eAgNnPc1d1	barevné
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
uložena	uložit	k5eAaPmNgNnP	uložit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
obrazových	obrazový	k2eAgNnPc6d1	obrazové
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Obrazová	obrazový	k2eAgNnPc1d1	obrazové
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
od	od	k7c2	od
dolního	dolní	k2eAgInSc2d1	dolní
levého	levý	k2eAgInSc2d1	levý
rohu	roh	k1gInSc2	roh
(	(	kIx(	(
<g/>
pixel	pixel	k1gInSc1	pixel
[	[	kIx(	[
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
0	[number]	k4	0
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
levým	levý	k2eAgInSc7d1	levý
dolním	dolní	k2eAgInSc7d1	dolní
rohem	roh	k1gInSc7	roh
obrázku	obrázek	k1gInSc2	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rastrová	rastrový	k2eAgFnSc1d1	rastrová
grafika	grafika	k1gFnSc1	grafika
</s>
</p>
<p>
<s>
Vektorová	vektorový	k2eAgFnSc1d1	vektorová
grafika	grafika	k1gFnSc1	grafika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
BMP	BMP	kA	BMP
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Microsoftu	Microsoft	k1gInSc2	Microsoft
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
formátu	formát	k1gInSc2	formát
BMP	BMP	kA	BMP
-	-	kIx~	-
grafický	grafický	k2eAgInSc1d1	grafický
formát	formát	k1gInSc1	formát
BMP	BMP	kA	BMP
-	-	kIx~	-
používaný	používaný	k2eAgInSc4d1	používaný
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
</s>
</p>
