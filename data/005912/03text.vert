<s>
Planeta	planeta	k1gFnSc1	planeta
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
<g/>
,	,	kIx,	,
planétés	planétés	k1gInSc1	planétés
–	–	k?	–
"	"	kIx"	"
<g/>
tulák	tulák	k1gMnSc1	tulák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
oběžnice	oběžnice	k1gFnSc1	oběžnice
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
definice	definice	k1gFnSc2	definice
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
takové	takový	k3xDgNnSc4	takový
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gFnSc2	její
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g />
.	.	kIx.	.
</s>
<s>
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
do	do	k7c2	do
přibližně	přibližně	k6eAd1	přibližně
kulového	kulový	k2eAgInSc2d1	kulový
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
hydrostatické	hydrostatický	k2eAgFnSc6d1	hydrostatická
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgMnSc1d1	dominantní
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
své	svůj	k3xOyFgFnSc2	svůj
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vyčistila	vyčistit	k5eAaPmAgFnS	vyčistit
svou	svůj	k3xOyFgFnSc4	svůj
gravitací	gravitace	k1gFnSc7	gravitace
okolí	okolí	k1gNnSc2	okolí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
družicí	družice	k1gFnSc7	družice
(	(	kIx(	(
<g/>
měsícem	měsíc	k1gInSc7	měsíc
<g/>
)	)	kIx)	)
jiného	jiný	k2eAgNnSc2d1	jiné
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
planeta	planeta	k1gFnSc1	planeta
rozumíme	rozumět	k5eAaImIp1nP	rozumět
objekty	objekt	k1gInPc4	objekt
značného	značný	k2eAgInSc2d1	značný
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
80	[number]	k4	80
MJ	mj	kA	mj
(	(	kIx(	(
<g/>
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
které	který	k3yQgInPc1	který
neprodukují	produkovat	k5eNaImIp3nP	produkovat
žádnou	žádný	k3yNgFnSc4	žádný
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
málo	málo	k1gNnSc1	málo
energie	energie	k1gFnSc2	energie
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
planetologie	planetologie	k1gFnSc2	planetologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ze	z	k7c2	z
smršťující	smršťující	k2eAgFnSc2d1	smršťující
se	se	k3xPyFc4	se
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
mateřská	mateřský	k2eAgFnSc1d1	mateřská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnPc1d1	prvotní
planety	planeta	k1gFnPc1	planeta
(	(	kIx(	(
<g/>
protoplanety	protoplanet	k1gInPc1	protoplanet
<g/>
)	)	kIx)	)
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nashromážděním	nashromáždění	k1gNnSc7	nashromáždění
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
obíhajícího	obíhající	k2eAgInSc2d1	obíhající
protohvězdu	protohvězd	k1gInSc2	protohvězd
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
začala	začít	k5eAaPmAgFnS	začít
termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
reakce	reakce	k1gFnSc1	reakce
a	a	k8xC	a
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
odfoukl	odfouknout	k5eAaPmAgInS	odfouknout
zbylý	zbylý	k2eAgInSc4d1	zbylý
materiál	materiál	k1gInSc4	materiál
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
žádné	žádný	k3yNgFnPc4	žádný
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
produkovaly	produkovat	k5eAaImAgFnP	produkovat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Všechnu	všechen	k3xTgFnSc4	všechen
vyzařovanou	vyzařovaný	k2eAgFnSc4d1	vyzařovaná
energii	energie	k1gFnSc4	energie
získávají	získávat	k5eAaImIp3nP	získávat
planety	planeta	k1gFnPc1	planeta
z	z	k7c2	z
gravitačních	gravitační	k2eAgInPc2d1	gravitační
<g/>
,	,	kIx,	,
mechanických	mechanický	k2eAgInPc2d1	mechanický
a	a	k8xC	a
termodynamických	termodynamický	k2eAgInPc2d1	termodynamický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
rozpadů	rozpad	k1gInPc2	rozpad
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
shromažďování	shromažďování	k1gNnSc1	shromažďování
a	a	k8xC	a
odrážení	odrážení	k1gNnSc1	odrážení
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
nebyla	být	k5eNaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
pojmenované	pojmenovaný	k2eAgFnSc6d1	pojmenovaná
podle	podle	k7c2	podle
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
římských	římský	k2eAgMnPc2d1	římský
bohů	bůh	k1gMnPc2	bůh
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc4	některý
neevropské	evropský	k2eNgInPc4d1	neevropský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
odlišné	odlišný	k2eAgInPc1d1	odlišný
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
podle	podle	k7c2	podle
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
klasické	klasický	k2eAgInPc1d1	klasický
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
měsíce	měsíc	k1gInPc1	měsíc
Uranu	Uran	k1gInSc2	Uran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asteroidy	asteroida	k1gFnPc1	asteroida
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
nazvané	nazvaný	k2eAgFnPc4d1	nazvaná
podle	podle	k7c2	podle
uvážení	uvážení	k1gNnSc2	uvážení
svých	svůj	k3xOyFgMnPc2	svůj
objevitelů	objevitel	k1gMnPc2	objevitel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
téměř	téměř	k6eAd1	téměř
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
nebo	nebo	k8xC	nebo
čehokoliv	cokoliv	k3yInSc2	cokoliv
(	(	kIx(	(
<g/>
zakázaní	zakázaný	k2eAgMnPc1d1	zakázaný
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
podléhají	podléhat	k5eAaImIp3nP	podléhat
schválení	schválení	k1gNnSc4	schválení
terminologické	terminologický	k2eAgFnSc2d1	terminologická
komise	komise	k1gFnSc2	komise
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
planetární	planetární	k2eAgFnSc2d1	planetární
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přijaté	přijatý	k2eAgFnSc2d1	přijatá
valným	valný	k2eAgInSc7d1	valný
shromáždění	shromáždění	k1gNnSc4	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
osm	osm	k4xCc4	osm
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
dominantních	dominantní	k2eAgInPc2d1	dominantní
<g/>
"	"	kIx"	"
těles	těleso	k1gNnPc2	těleso
obíhajících	obíhající	k2eAgMnPc2d1	obíhající
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
vzestupně	vzestupně	k6eAd1	vzestupně
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Merkur	Merkur	k1gInSc1	Merkur
(	(	kIx(	(
<g/>
☿	☿	k?	☿
<g/>
)	)	kIx)	)
Venuše	Venuše	k1gFnSc2	Venuše
(	(	kIx(	(
<g/>
♀	♀	k?	♀
<g/>
)	)	kIx)	)
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
♁	♁	k?	♁
<g/>
)	)	kIx)	)
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
♂	♂	k?	♂
<g/>
)	)	kIx)	)
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
♃	♃	k?	♃
<g/>
)	)	kIx)	)
Saturn	Saturn	k1gMnSc1	Saturn
(	(	kIx(	(
<g/>
♄	♄	k?	♄
<g/>
)	)	kIx)	)
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
♅	♅	k?	♅
<g/>
)	)	kIx)	)
Neptun	Neptun	k1gMnSc1	Neptun
(	(	kIx(	(
<g/>
♆	♆	k?	♆
<g/>
)	)	kIx)	)
Astronomové	astronom	k1gMnPc1	astronom
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
malými	malý	k2eAgNnPc7d1	malé
tělesy	těleso	k1gNnPc7	těleso
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustavy	soustava	k1gFnPc1	soustava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
planetky	planetka	k1gFnPc1	planetka
<g/>
,	,	kIx,	,
komety	kometa	k1gFnPc1	kometa
a	a	k8xC	a
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
skutečnými	skutečný	k2eAgFnPc7d1	skutečná
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
v	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
do	do	k7c2	do
více	hodně	k6eAd2	hodně
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
terestrické	terestrický	k2eAgFnPc1d1	terestrická
nebo	nebo	k8xC	nebo
též	též	k6eAd1	též
kamenné	kamenný	k2eAgFnPc4d1	kamenná
–	–	k?	–
planety	planeta	k1gFnPc4	planeta
podobné	podobný	k2eAgFnPc4d1	podobná
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnPc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
<g/>
:	:	kIx,	:
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc4	Mars
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
nebo	nebo	k8xC	nebo
též	též	k6eAd1	též
joviální	joviální	k2eAgFnSc2d1	joviální
planety	planeta	k1gFnSc2	planeta
–	–	k?	–
planety	planeta	k1gFnSc2	planeta
podobné	podobný	k2eAgInPc4d1	podobný
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
plynů	plyn	k1gInPc2	plyn
<g/>
:	:	kIx,	:
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uran	Uran	k1gMnSc1	Uran
<g/>
,	,	kIx,	,
Neptun	Neptun	k1gMnSc1	Neptun
uranské	uranská	k1gFnSc2	uranská
nebo	nebo	k8xC	nebo
též	též	k9	též
ledoví	ledový	k2eAgMnPc1d1	ledový
obři	obr	k1gMnPc1	obr
–	–	k?	–
podkategorie	podkategorie	k1gFnSc1	podkategorie
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
lišící	lišící	k2eAgFnSc2d1	lišící
se	se	k3xPyFc4	se
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
významným	významný	k2eAgInSc7d1	významný
podílem	podíl	k1gInSc7	podíl
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
ledu	led	k1gInSc2	led
<g/>
:	:	kIx,	:
Uran	Uran	k1gMnSc1	Uran
<g/>
,	,	kIx,	,
Neptun	Neptun	k1gInSc4	Neptun
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
považují	považovat	k5eAaImIp3nP	považovat
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
Měsíc	měsíc	k1gInSc4	měsíc
za	za	k7c2	za
dvojplanetu	dvojplanet	k1gInSc2	dvojplanet
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
měřený	měřený	k2eAgInSc4d1	měřený
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
1,5	[number]	k4	1,5
<g/>
×	×	k?	×
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2,2	[number]	k4	2,2
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
skutečnost	skutečnost	k1gFnSc1	skutečnost
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
satelit	satelit	k1gInSc4	satelit
je	být	k5eAaImIp3nS	být
však	však	k9	však
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
měsíce	měsíc	k1gInPc1	měsíc
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
působí	působit	k5eAaImIp3nS	působit
Slunce	slunce	k1gNnSc2	slunce
větší	veliký	k2eAgFnSc7d2	veliký
gravitací	gravitace	k1gFnSc7	gravitace
než	než	k8xS	než
jejich	jejich	k3xOp3gFnSc1	jejich
mateřská	mateřský	k2eAgFnSc1d1	mateřská
planeta	planeta	k1gFnSc1	planeta
(	(	kIx(	(
<g/>
u	u	k7c2	u
planetek	planetka	k1gFnPc2	planetka
jejich	jejich	k3xOp3gFnSc4	jejich
hlavní	hlavní	k2eAgFnSc1d1	hlavní
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
J	J	kA	J
–	–	k?	–
1,5	[number]	k4	1,5
<g/>
×	×	k?	×
Nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
měsíc	měsíc	k1gInSc4	měsíc
Uranu	Uran	k1gInSc2	Uran
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
–	–	k?	–
1,2	[number]	k4	1,2
<g/>
×	×	k?	×
Dva	dva	k4xCgInPc4	dva
nejvzdálenější	vzdálený	k2eAgInPc4d3	nejvzdálenější
měsíce	měsíc	k1gInPc4	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
:	:	kIx,	:
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
N	N	kA	N
4	[number]	k4	4
a	a	k8xC	a
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
N	N	kA	N
1	[number]	k4	1
–	–	k?	–
2,1	[number]	k4	2,1
<g/>
×	×	k?	×
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
1	[number]	k4	1
Linus	Linus	k1gMnSc1	Linus
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
1,6	[number]	k4	1,6
<g/>
×	×	k?	×
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
1	[number]	k4	1
Petit-Prince	Petit-Prinec	k1gInSc2	Petit-Prinec
–	–	k?	–
2,8	[number]	k4	2,8
<g/>
×	×	k?	×
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
243	[number]	k4	243
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
1,3	[number]	k4	1,3
<g/>
×	×	k?	×
nejnápadnější	nápadní	k2eAgInSc1d3	nápadní
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
66391	[number]	k4	66391
<g/>
)	)	kIx)	)
–	–	k?	–
1	[number]	k4	1
625	[number]	k4	625
<g/>
×	×	k?	×
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
tzv.	tzv.	kA	tzv.
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
splňují	splňovat	k5eAaImIp3nP	splňovat
většinu	většina	k1gFnSc4	většina
charakteristik	charakteristika	k1gFnPc2	charakteristika
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
dominantní	dominantní	k2eAgMnPc1d1	dominantní
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
své	svůj	k3xOyFgFnPc4	svůj
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ceres	ceres	k1gInSc1	ceres
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
objevení	objevení	k1gNnSc6	objevení
označený	označený	k2eAgMnSc1d1	označený
jako	jako	k8xC	jako
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
mnoho	mnoho	k4c1	mnoho
podobných	podobný	k2eAgInPc2d1	podobný
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
překlasifikován	překlasifikovat	k5eAaPmNgInS	překlasifikovat
na	na	k7c4	na
planetku	planetka	k1gFnSc4	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gNnSc1	Pluto
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
řazeno	řadit	k5eAaImNgNnS	řadit
též	též	k9	též
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
však	však	k9	však
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
velikost	velikost	k1gFnSc1	velikost
kterékoliv	kterýkoliv	k3yIgFnSc2	kterýkoliv
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
podobá	podobat	k5eAaImIp3nS	podobat
ledovým	ledový	k2eAgInPc3d1	ledový
měsícům	měsíc	k1gInPc3	měsíc
Saturna	Saturn	k1gMnSc2	Saturn
<g/>
,	,	kIx,	,
než	než	k8xS	než
planetám	planeta	k1gFnPc3	planeta
(	(	kIx(	(
<g/>
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Marsu	Mars	k1gInSc2	Mars
4	[number]	k4	4
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
další	další	k2eAgNnPc1d1	další
tělesa	těleso	k1gNnPc1	těleso
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptunu	Neptun	k1gInSc2	Neptun
(	(	kIx(	(
<g/>
TNO	TNO	kA	TNO
<g/>
,	,	kIx,	,
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
složením	složení	k1gNnSc7	složení
podobná	podobný	k2eAgNnPc4d1	podobné
Plutu	plut	k1gInSc3	plut
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
hrouda	hrouda	k1gFnSc1	hrouda
kamení	kamení	k1gNnSc2	kamení
a	a	k8xC	a
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
ledové	ledový	k2eAgFnSc2d1	ledová
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
objeveno	objeven	k2eAgNnSc1d1	objeveno
těleso	těleso	k1gNnSc1	těleso
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
dostala	dostat	k5eAaPmAgFnS	dostat
definitivní	definitivní	k2eAgNnSc4d1	definitivní
jméno	jméno	k1gNnSc4	jméno
Eris	Eris	k1gFnSc1	Eris
a	a	k8xC	a
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
být	být	k5eAaImF	být
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
domněnka	domněnka	k1gFnSc1	domněnka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
dalších	další	k2eAgNnPc2d1	další
velkých	velký	k2eAgNnPc2d1	velké
těles	těleso	k1gNnPc2	těleso
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptuna	Neptun	k1gMnSc2	Neptun
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
diskutovat	diskutovat	k5eAaImF	diskutovat
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
definice	definice	k1gFnSc2	definice
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
objevů	objev	k1gInPc2	objev
těles	těleso	k1gNnPc2	těleso
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
Plutu	Pluto	k1gNnSc3	Pluto
svou	svůj	k3xOyFgFnSc7	svůj
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
usoudilo	usoudit	k5eAaPmAgNnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pluto	Pluto	k1gNnSc1	Pluto
není	být	k5eNaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
XXVI	XXVI	kA	XXVI
<g/>
.	.	kIx.	.
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
planet	planeta	k1gFnPc2	planeta
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
nová	nový	k2eAgFnSc1d1	nová
kategorie	kategorie	k1gFnSc1	kategorie
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
trpasličí	trpasličí	k2eAgFnPc1d1	trpasličí
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
planetek	planetka	k1gFnPc2	planetka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
přidělována	přidělován	k2eAgNnPc4d1	přidělováno
i	i	k8xC	i
katalogová	katalogový	k2eAgNnPc4d1	Katalogové
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
patří	patřit	k5eAaImIp3nP	patřit
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzestupné	vzestupný	k2eAgFnSc2d1	vzestupná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Ceres	ceres	k1gInSc1	ceres
(	(	kIx(	(
<g/>
134340	[number]	k4	134340
<g/>
)	)	kIx)	)
Pluto	Pluto	k1gNnSc4	Pluto
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
Haumea	Haume	k1gInSc2	Haume
(	(	kIx(	(
<g/>
136472	[number]	k4	136472
<g/>
)	)	kIx)	)
Makemake	Makemak	k1gInSc2	Makemak
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
vyloučené	vyloučený	k2eAgNnSc1d1	vyloučené
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
našlo	najít	k5eAaPmAgNnS	najít
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
např.	např.	kA	např.
Mars	Mars	k1gInSc1	Mars
nebo	nebo	k8xC	nebo
Merkur	Merkur	k1gInSc1	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
několika	několik	k4yIc6	několik
hypotetických	hypotetický	k2eAgFnPc6d1	hypotetická
planetách	planeta	k1gFnPc6	planeta
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Planeta	planeta	k1gFnSc1	planeta
X	X	kA	X
(	(	kIx(	(
<g/>
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
výskyt	výskyt	k1gInSc4	výskyt
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Vulcan	Vulcan	k1gInSc1	Vulcan
(	(	kIx(	(
<g/>
s	s	k7c7	s
možnou	možný	k2eAgFnSc7d1	možná
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
mezi	mezi	k7c7	mezi
Merkurem	Merkur	k1gInSc7	Merkur
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
předměty	předmět	k1gInPc4	předmět
intenzívního	intenzívní	k2eAgNnSc2d1	intenzívní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúspěšného	úspěšný	k2eNgNnSc2d1	neúspěšné
hledání	hledání	k1gNnSc2	hledání
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Exoplaneta	Exoplaneto	k1gNnSc2	Exoplaneto
<g/>
.	.	kIx.	.
</s>
<s>
Exoplanety	Exoplanet	k1gInPc1	Exoplanet
jsou	být	k5eAaImIp3nP	být
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mimo	mimo	k7c4	mimo
naši	náš	k3xOp1gFnSc4	náš
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
objevení	objevení	k1gNnSc1	objevení
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
nalezení	nalezení	k1gNnSc4	nalezení
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tehdy	tehdy	k6eAd1	tehdy
uznávaného	uznávaný	k2eAgNnSc2d1	uznávané
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
jen	jen	k8xS	jen
devět	devět	k4xCc4	devět
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc4d1	známo
235	[number]	k4	235
planet	planeta	k1gFnPc2	planeta
–	–	k?	–
všechny	všechen	k3xTgInPc4	všechen
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnPc1d1	objevená
byly	být	k5eAaImAgFnP	být
planety	planeta	k1gFnPc1	planeta
mimo	mimo	k7c4	mimo
naši	náš	k3xOp1gFnSc4	náš
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
exoplanety	exoplanet	k1gInPc1	exoplanet
<g/>
;	;	kIx,	;
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
již	již	k9	již
430	[number]	k4	430
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
<g/>
,	,	kIx,	,
učiněných	učiněný	k2eAgInPc2d1	učiněný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
družice	družice	k1gFnSc2	družice
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
kolem	kolem	k7c2	kolem
hvězd	hvězda	k1gFnPc2	hvězda
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
kroužit	kroužit	k5eAaImF	kroužit
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
až	až	k6eAd1	až
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
už	už	k6eAd1	už
nenalézají	nalézat	k5eNaImIp3nP	nalézat
jen	jen	k9	jen
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
celé	celý	k2eAgFnSc2d1	celá
exoplanetární	exoplanetární	k2eAgFnSc2d1	exoplanetární
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
popsal	popsat	k5eAaPmAgMnS	popsat
Scott	Scott	k1gInSc4	Scott
Gaudi	Gaud	k1gMnPc1	Gaud
u	u	k7c2	u
objektu	objekt	k1gInSc2	objekt
označovaného	označovaný	k2eAgNnSc2d1	označované
jako	jako	k8xC	jako
OGLE-	OGLE-	k1gFnSc1	OGLE-
<g/>
2006	[number]	k4	2006
<g/>
-BLG-	-BLG-	k?	-BLG-
<g/>
109	[number]	k4	109
<g/>
L.	L.	kA	L.
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
2	[number]	k4	2
exoplanety	exoplanet	k1gInPc1	exoplanet
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
jen	jen	k6eAd1	jen
nepatrně	patrně	k6eNd1	patrně
méně	málo	k6eAd2	málo
hmotnější	hmotný	k2eAgMnSc1d2	hmotnější
než	než	k8xS	než
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
studie	studie	k1gFnSc1	studie
dokládající	dokládající	k2eAgFnSc1d1	dokládající
výskyt	výskyt	k1gInSc4	výskyt
několika	několik	k4yIc2	několik
planet	planeta	k1gFnPc2	planeta
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
Kepler-	Kepler-	k1gFnSc2	Kepler-
<g/>
444	[number]	k4	444
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
11,2	[number]	k4	11,2
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
planety	planeta	k1gFnSc2	planeta
jsou	být	k5eAaImIp3nP	být
samotáři	samotář	k1gMnPc1	samotář
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
gravitačně	gravitačně	k6eAd1	gravitačně
spojeni	spojit	k5eAaPmNgMnP	spojit
se	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
žádná	žádný	k3yNgFnSc1	žádný
mezihvězdná	mezihvězdný	k2eAgFnSc1d1	mezihvězdná
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
však	však	k9	však
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledky	výsledek	k1gInPc1	výsledek
počítačových	počítačový	k2eAgFnPc2d1	počítačová
simulací	simulace	k1gFnPc2	simulace
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
planetárních	planetární	k2eAgInPc2d1	planetární
systémů	systém	k1gInPc2	systém
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
procesy	proces	k1gInPc1	proces
zformování	zformování	k1gNnSc2	zformování
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
odvrhnutí	odvrhnutí	k1gNnSc2	odvrhnutí
těles	těleso	k1gNnPc2	těleso
o	o	k7c6	o
značné	značný	k2eAgFnSc6d1	značná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelných	obyvatelný	k2eAgFnPc2d1	obyvatelná
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
</s>
