<s>
Antonín	Antonín	k1gMnSc1	Antonín
Leopold	Leopold	k1gMnSc1	Leopold
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1841	[number]	k4	1841
Nelahozeves	Nelahozevesa	k1gFnPc2	Nelahozevesa
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
nejproslulejším	proslulý	k2eAgMnSc7d3	nejproslulejší
a	a	k8xC	a
nejhranějším	hraný	k2eAgMnSc7d3	nejhranější
českým	český	k2eAgMnSc7d1	český
skladatelem	skladatel	k1gMnSc7	skladatel
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Dvořákova	Dvořákův	k2eAgNnPc1d1	Dvořákovo
symfonická	symfonický	k2eAgNnPc1d1	symfonické
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
součástí	součást	k1gFnSc7	součást
repertoáru	repertoár	k1gInSc2	repertoár
významných	významný	k2eAgInPc2d1	významný
orchestrů	orchestr	k1gInPc2	orchestr
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hrána	hrát	k5eAaImNgFnS	hrát
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
jeho	jeho	k3xOp3gFnSc2	jeho
melodické	melodický	k2eAgFnSc2d1	melodická
invence	invence	k1gFnSc2	invence
uchvacuje	uchvacovat	k5eAaImIp3nS	uchvacovat
dodnes	dodnes	k6eAd1	dodnes
odborníky	odborník	k1gMnPc4	odborník
i	i	k8xC	i
laiky	laik	k1gMnPc4	laik
a	a	k8xC	a
brala	brát	k5eAaImAgFnS	brát
dech	dech	k1gInSc4	dech
i	i	k8xC	i
skladatelovým	skladatelův	k2eAgMnPc3d1	skladatelův
současníkům	současník	k1gMnPc3	současník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
Brahmsův	Brahmsův	k2eAgInSc1d1	Brahmsův
výrok	výrok	k1gInSc1	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dvořákova	Dvořákův	k2eAgNnPc4d1	Dvořákovo
témata	téma	k1gNnPc4	téma
pro	pro	k7c4	pro
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
myšlenky	myšlenka	k1gFnPc4	myšlenka
by	by	kYmCp3nS	by
mně	já	k3xPp1nSc3	já
docela	docela	k6eAd1	docela
stačila	stačit	k5eAaBmAgFnS	stačit
i	i	k9	i
na	na	k7c4	na
myšlenky	myšlenka	k1gFnPc4	myšlenka
hlavní	hlavní	k2eAgFnPc4d1	hlavní
...	...	k?	...
<g/>
"	"	kIx"	"
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
symfoniemi	symfonie	k1gFnPc7	symfonie
a	a	k8xC	a
velkými	velký	k2eAgFnPc7d1	velká
vokálně-instrumentálními	vokálněnstrumentální	k2eAgFnPc7d1	vokálně-instrumentální
skladbami	skladba	k1gFnPc7	skladba
<g/>
,	,	kIx,	,
neméně	málo	k6eNd2	málo
však	však	k9	však
i	i	k9	i
komorní	komorní	k2eAgFnSc7d1	komorní
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
operami	opera	k1gFnPc7	opera
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
je	být	k5eAaImIp3nS	být
čelným	čelný	k2eAgMnSc7d1	čelný
světovým	světový	k2eAgMnSc7d1	světový
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
klasicistně-romantické	klasicistněomantický	k2eAgFnSc2d1	klasicistně-romantický
hudební	hudební	k2eAgFnSc2d1	hudební
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Praděd	praděd	k1gMnSc1	praděd
Antonína	Antonín	k1gMnSc4	Antonín
Dvořáka	Dvořák	k1gMnSc4	Dvořák
<g/>
,	,	kIx,	,
rolník	rolník	k1gMnSc1	rolník
Jan	Jan	k1gMnSc1	Jan
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k8xC	i
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Třeboradicích	Třeboradice	k1gFnPc6	Třeboradice
<g/>
.	.	kIx.	.
</s>
<s>
Antonínův	Antonínův	k2eAgMnSc1d1	Antonínův
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Třeboradicích	Třeboradice	k1gFnPc6	Třeboradice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1818	[number]	k4	1818
<g/>
–	–	k?	–
<g/>
1824	[number]	k4	1824
a	a	k8xC	a
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1838	[number]	k4	1838
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Nelahozevsi	Nelahozevse	k1gFnSc6	Nelahozevse
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
24	[number]	k4	24
pronajatý	pronajatý	k2eAgInSc4d1	pronajatý
řeznický	řeznický	k2eAgInSc4d1	řeznický
krám	krám	k1gInSc4	krám
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Bobkovou	Bobková	k1gFnSc7	Bobková
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
14	[number]	k4	14
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Nelahozevsi	Nelahozevse	k1gFnSc6	Nelahozevse
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
pronajaté	pronajatý	k2eAgNnSc4d1	pronajaté
řeznictví	řeznictví	k1gNnSc4	řeznictví
v	v	k7c6	v
Dřínově	dřínově	k6eAd1	dřínově
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
narodil	narodit	k5eAaPmAgMnS	narodit
František	František	k1gMnSc1	František
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
skladatelův	skladatelův	k2eAgMnSc1d1	skladatelův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgInS	vyučit
řezníkem	řezník	k1gMnSc7	řezník
a	a	k8xC	a
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
na	na	k7c4	na
"	"	kIx"	"
<g/>
zkušenou	zkušená	k1gFnSc4	zkušená
<g/>
"	"	kIx"	"
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
v	v	k7c6	v
Chržíně	Chržína	k1gFnSc6	Chržína
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Zdeňkovou	Zdeňkův	k2eAgFnSc7d1	Zdeňkova
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Uhů	Uhů	k1gFnSc2	Uhů
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
šafáře	šafář	k1gMnSc2	šafář
lobkovického	lobkovický	k2eAgNnSc2d1	Lobkovické
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
řeznictví	řeznictví	k1gNnSc2	řeznictví
si	se	k3xPyFc3	se
manželé	manžel	k1gMnPc1	manžel
pronajali	pronajmout	k5eAaPmAgMnP	pronajmout
v	v	k7c6	v
Nelahozevsi	Nelahozevse	k1gFnSc6	Nelahozevse
dům	dům	k1gInSc1	dům
č.	č.	k?	č.
24	[number]	k4	24
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
Engelhardtovu	Engelhardtův	k2eAgFnSc4d1	Engelhardtův
hospodu	hospodu	k?	hospodu
na	na	k7c4	na
č.	č.	k?	č.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nelahozevsi	Nelahozevse	k1gFnSc6	Nelahozevse
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
12	[number]	k4	12
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Františkovi	František	k1gMnSc3	František
a	a	k8xC	a
Anně	Anna	k1gFnSc3	Anna
Dvořákovým	Dvořákovi	k1gRnPc3	Dvořákovi
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1841	[number]	k4	1841
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
začal	začít	k5eAaPmAgMnS	začít
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
učitele	učitel	k1gMnSc2	učitel
Josefa	Josef	k1gMnSc2	Josef
Spitze	Spitze	k1gFnSc1	Spitze
také	také	k9	také
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
rovněž	rovněž	k9	rovněž
plánoval	plánovat	k5eAaImAgInS	plánovat
práci	práce	k1gFnSc4	práce
řezníka	řezník	k1gMnSc2	řezník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
mladý	mladý	k1gMnSc1	mladý
Antonín	Antonín	k1gMnSc1	Antonín
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Zlonicích	Zlonice	k1gFnPc6	Zlonice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
místní	místní	k2eAgMnSc1d1	místní
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
Antonín	Antonín	k1gMnSc1	Antonín
Liehmann	Liehmann	k1gMnSc1	Liehmann
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
se	se	k3xPyFc4	se
u	u	k7c2	u
Liehmanna	Liehmann	k1gInSc2	Liehmann
učil	učít	k5eAaPmAgMnS	učít
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
varhany	varhany	k1gFnPc4	varhany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
odešel	odejít	k5eAaPmAgMnS	odejít
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
varhanické	varhanický	k2eAgFnSc6d1	varhanická
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
violu	viola	k1gFnSc4	viola
v	v	k7c6	v
orchestru	orchestr	k1gInSc6	orchestr
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
dirigentem	dirigent	k1gMnSc7	dirigent
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
stal	stát	k5eAaPmAgMnS	stát
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
orchestru	orchestr	k1gInSc6	orchestr
setrval	setrvat	k5eAaPmAgMnS	setrvat
Dvořák	Dvořák	k1gMnSc1	Dvořák
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
svých	svůj	k3xOyFgInPc2	svůj
příjmů	příjem	k1gInPc2	příjem
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
mladý	mladý	k2eAgMnSc1d1	mladý
Dvořák	Dvořák	k1gMnSc1	Dvořák
hodiny	hodina	k1gFnSc2	hodina
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
žačky	žačka	k1gFnSc2	žačka
Josefíny	Josefína	k1gFnSc2	Josefína
Čermákové	Čermáková	k1gFnSc2	Čermáková
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
neopětovala	opětovat	k5eNaImAgFnS	opětovat
jeho	jeho	k3xOp3gFnSc4	jeho
city	city	k1gFnSc4	city
a	a	k8xC	a
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
hraběte	hrabě	k1gMnSc4	hrabě
Václava	Václav	k1gMnSc4	Václav
Roberta	Robert	k1gMnSc4	Robert
z	z	k7c2	z
Kounic	Kounice	k1gFnPc2	Kounice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
se	se	k3xPyFc4	se
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Josefy	Josefa	k1gFnSc2	Josefa
<g/>
,	,	kIx,	,
Annou	Anna	k1gFnSc7	Anna
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manželům	manžel	k1gMnPc3	manžel
Antonínovi	Antonín	k1gMnSc6	Antonín
a	a	k8xC	a
Anně	Anna	k1gFnSc6	Anna
Dvořákovým	Dvořákův	k2eAgMnPc3d1	Dvořákův
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc1	devět
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
Otakar	Otakar	k1gMnSc1	Otakar
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1874	[number]	k4	1874
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
onemocnění	onemocnění	k1gNnSc6	onemocnění
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
;	;	kIx,	;
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1875	[number]	k4	1875
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Růžena	Růžena	k1gFnSc1	Růžena
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
fosforem	fosfor	k1gInSc7	fosfor
<g/>
;	;	kIx,	;
Otilie	Otilie	k1gFnSc1	Otilie
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1878	[number]	k4	1878
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Dvořákova	Dvořákův	k2eAgMnSc2d1	Dvořákův
žáka	žák	k1gMnSc2	žák
<g/>
,	,	kIx,	,
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Josefa	Josef	k1gMnSc2	Josef
Suka	Suk	k1gMnSc2	Suk
<g/>
,	,	kIx,	,
1	[number]	k4	1
syn	syn	k1gMnSc1	syn
<g/>
;	;	kIx,	;
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1880	[number]	k4	1880
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1923	[number]	k4	1923
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Josef	Josef	k1gMnSc1	Josef
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
2	[number]	k4	2
děti	dítě	k1gFnPc1	dítě
<g/>
;	;	kIx,	;
Magdalena	Magdalena	k1gFnSc1	Magdalena
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1881	[number]	k4	1881
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgFnSc1d1	koncertní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Karel	Karel	k1gMnSc1	Karel
Šantrůček	Šantrůček	k1gMnSc1	Šantrůček
<g/>
,	,	kIx,	,
bezdětná	bezdětný	k2eAgFnSc1d1	bezdětná
<g/>
;	;	kIx,	;
Antonín	Antonín	k1gMnSc1	Antonín
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1883	[number]	k4	1883
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Julie	Julie	k1gFnSc1	Julie
Janoušková	Janoušková	k1gFnSc1	Janoušková
<g/>
,	,	kIx,	,
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
;	;	kIx,	;
Otakar	Otakar	k1gMnSc1	Otakar
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1885	[number]	k4	1885
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
Antonínu	Antonín	k1gMnSc6	Antonín
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Marie	Marie	k1gFnSc1	Marie
Šrámková	Šrámková	k1gFnSc1	Šrámková
<g/>
,	,	kIx,	,
2	[number]	k4	2
děti	dítě	k1gFnPc1	dítě
<g/>
;	;	kIx,	;
Aloisie	Aloisie	k1gFnPc1	Aloisie
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1888	[number]	k4	1888
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Josef	Josef	k1gMnSc1	Josef
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
4	[number]	k4	4
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zdokumentovat	zdokumentovat	k5eAaPmF	zdokumentovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
skladatelské	skladatelský	k2eAgInPc1d1	skladatelský
pokusy	pokus	k1gInPc1	pokus
se	se	k3xPyFc4	se
však	však	k9	však
nesetkaly	setkat	k5eNaPmAgFnP	setkat
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
veřejným	veřejný	k2eAgInSc7d1	veřejný
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
svatbě	svatba	k1gFnSc6	svatba
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
varhaník	varhaník	k1gMnSc1	varhaník
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
dráhy	dráha	k1gFnSc2	dráha
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
pražský	pražský	k2eAgMnSc1d1	pražský
rodák	rodák	k1gMnSc1	rodák
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
působící	působící	k2eAgMnSc1d1	působící
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Eduard	Eduard	k1gMnSc1	Eduard
Hanslick	Hanslick	k1gMnSc1	Hanslick
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
informoval	informovat	k5eAaBmAgMnS	informovat
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
pozornost	pozornost	k1gFnSc4	pozornost
tehdy	tehdy	k6eAd1	tehdy
výrazné	výrazný	k2eAgFnSc2d1	výrazná
osobnosti	osobnost	k1gFnSc2	osobnost
hudebního	hudební	k2eAgInSc2d1	hudební
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
skladatele	skladatel	k1gMnSc2	skladatel
Johannese	Johannese	k1gFnSc2	Johannese
Brahmse	Brahms	k1gMnSc2	Brahms
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gNnSc4	on
posléze	posléze	k6eAd1	posléze
doporučil	doporučit	k5eAaPmAgInS	doporučit
berlínskému	berlínský	k2eAgMnSc3d1	berlínský
nakladateli	nakladatel	k1gMnSc3	nakladatel
hudebních	hudební	k2eAgFnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
Fritzi	Frith	k1gMnPc1	Frith
Simrockovi	Simrockův	k2eAgMnPc1d1	Simrockův
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
Simrocka	Simrocko	k1gNnPc4	Simrocko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
napsal	napsat	k5eAaPmAgMnS	napsat
první	první	k4xOgFnSc4	první
řadu	řada	k1gFnSc4	řada
svých	svůj	k3xOyFgInPc2	svůj
Slovanských	slovanský	k2eAgInPc2d1	slovanský
tanců	tanec	k1gInPc2	tanec
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
kritiku	kritika	k1gFnSc4	kritika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
od	od	k7c2	od
Hanslicka	Hanslicko	k1gNnSc2	Hanslicko
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Brahmsem	Brahms	k1gMnSc7	Brahms
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Dvořák	Dvořák	k1gMnSc1	Dvořák
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
přátelském	přátelský	k2eAgInSc6d1	přátelský
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgMnS	navštívit
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jeho	on	k3xPp3gInSc2	on
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Dvořákových	Dvořáková	k1gFnPc2	Dvořáková
byla	být	k5eAaImAgFnS	být
častými	častý	k2eAgMnPc7d1	častý
hosty	host	k1gMnPc4	host
hraběte	hrabě	k1gMnSc4	hrabě
JUDr.	JUDr.	kA	JUDr.
Václava	Václav	k1gMnSc4	Václav
Roberta	Robert	k1gMnSc4	Robert
z	z	k7c2	z
Kounic	Kounice	k1gFnPc2	Kounice
na	na	k7c6	na
zámečku	zámeček	k1gInSc6	zámeček
ve	v	k7c6	v
Vysoké	vysoká	k1gFnSc6	vysoká
u	u	k7c2	u
Příbramě	Příbram	k1gFnSc2	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
bydlel	bydlet	k5eAaImAgMnS	bydlet
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
správcovském	správcovský	k2eAgInSc6d1	správcovský
domě	dům	k1gInSc6	dům
hraběte	hrabě	k1gMnSc2	hrabě
Kounice	Kounice	k1gFnSc2	Kounice
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
švagrem	švagr	k1gMnSc7	švagr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
koupil	koupit	k5eAaPmAgMnS	koupit
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kupní	kupní	k2eAgInSc1d1	kupní
obnos	obnos	k1gInSc1	obnos
musel	muset	k5eAaImAgInS	muset
uhradit	uhradit	k5eAaPmF	uhradit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
ovčín	ovčín	k1gInSc1	ovčín
se	s	k7c7	s
špýcharem	špýchar	k1gInSc7	špýchar
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Špýchar	špýchar	k1gInSc1	špýchar
nechal	nechat	k5eAaPmAgInS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
sadařil	sadařit	k5eAaImAgMnS	sadařit
<g/>
,	,	kIx,	,
choval	chovat	k5eAaImAgMnS	chovat
holuby	holub	k1gMnPc4	holub
a	a	k8xC	a
komponoval	komponovat	k5eAaImAgMnS	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
i	i	k9	i
jako	jako	k8xS	jako
dirigent	dirigent	k1gMnSc1	dirigent
svých	svůj	k3xOyFgFnPc2	svůj
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
svou	svůj	k3xOyFgFnSc4	svůj
Stabat	Stabat	k1gFnSc4	Stabat
Mater	Matra	k1gFnPc2	Matra
<g/>
,	,	kIx,	,
vokálně-instrumentální	vokálněnstrumentální	k2eAgNnSc1d1	vokálně-instrumentální
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ohromujícím	ohromující	k2eAgInSc7d1	ohromující
úspěchem	úspěch	k1gInSc7	úspěch
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k9	tak
silné	silný	k2eAgFnPc4d1	silná
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
anglickou	anglický	k2eAgFnSc4d1	anglická
hudební	hudební	k2eAgFnSc4d1	hudební
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vynikající	vynikající	k2eAgInPc4d1	vynikající
výkony	výkon	k1gInPc4	výkon
souborů	soubor	k1gInPc2	soubor
sborového	sborový	k2eAgInSc2d1	sborový
zpěvu	zpěv	k1gInSc2	zpěv
motivovaly	motivovat	k5eAaBmAgInP	motivovat
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
kompozičnímu	kompoziční	k2eAgNnSc3d1	kompoziční
úsilí	úsilí	k1gNnSc3	úsilí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
skladeb	skladba	k1gFnPc2	skladba
vokálně-instrumentálního	vokálněnstrumentální	k2eAgInSc2d1	vokálně-instrumentální
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
Rekviem	rekviem	k1gNnPc4	rekviem
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedené	uvedený	k2eAgFnPc1d1	uvedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
pod	pod	k7c7	pod
skladatelovou	skladatelův	k2eAgFnSc7d1	skladatelova
taktovkou	taktovka	k1gFnSc7	taktovka
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgMnPc2	svůj
hudebních	hudební	k2eAgMnPc2d1	hudební
úspěchů	úspěch	k1gInPc2	úspěch
získal	získat	k5eAaPmAgInS	získat
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Cambridgeské	cambridgeský	k2eAgFnSc6d1	Cambridgeská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
skladatelem	skladatel	k1gMnSc7	skladatel
Čajkovským	Čajkovský	k2eAgMnSc7d1	Čajkovský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
pozval	pozvat	k5eAaPmAgMnS	pozvat
koncertovat	koncertovat	k5eAaImF	koncertovat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
byl	být	k5eAaImAgMnS	být
Dvořák	Dvořák	k1gMnSc1	Dvořák
obeslán	obeslat	k5eAaPmNgMnS	obeslat
dopisem	dopis	k1gInSc7	dopis
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
americké	americký	k2eAgFnSc2d1	americká
národní	národní	k2eAgFnSc2d1	národní
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Jeanette	Jeanett	k1gInSc5	Jeanett
Thurberová	Thurberový	k2eAgNnPc1d1	Thurberový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snažila	snažit	k5eAaImAgFnS	snažit
získat	získat	k5eAaPmF	získat
jako	jako	k9	jako
ředitele	ředitel	k1gMnPc4	ředitel
této	tento	k3xDgFnSc2	tento
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
sice	sice	k8xC	sice
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
v	v	k7c6	v
letech	let	k1gInPc6	let
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgFnPc4d1	další
pocty	pocta	k1gFnPc4	pocta
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
i	i	k9	i
světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc4	proslulost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
Dvořákovým	Dvořákův	k2eAgInSc7d1	Dvořákův
úkolem	úkol	k1gInSc7	úkol
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
bylo	být	k5eAaImAgNnS	být
pomoci	pomoct	k5eAaPmF	pomoct
najít	najít	k5eAaPmF	najít
americké	americký	k2eAgFnSc3d1	americká
hudbě	hudba	k1gFnSc3	hudba
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
se	se	k3xPyFc4	se
tak	tak	k9	tak
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
především	především	k9	především
díky	díky	k7c3	díky
inspiraci	inspirace	k1gFnSc3	inspirace
indiánskou	indiánský	k2eAgFnSc7d1	indiánská
a	a	k8xC	a
afroamerickou	afroamerický	k2eAgFnSc7d1	afroamerická
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Harry	Harra	k1gFnSc2	Harra
Burleigh	Burleigh	k1gMnSc1	Burleigh
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
černošských	černošský	k2eAgMnPc2d1	černošský
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
Dvořákovi	Dvořákův	k2eAgMnPc1d1	Dvořákův
předvedl	předvést	k5eAaPmAgMnS	předvést
kouzlo	kouzlo	k1gNnSc4	kouzlo
amerických	americký	k2eAgInPc2d1	americký
spirituálů	spirituál	k1gInPc2	spirituál
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
vyplácením	vyplácení	k1gNnSc7	vyplácení
honoráře	honorář	k1gInSc2	honorář
se	se	k3xPyFc4	se
však	však	k9	však
Dvořák	Dvořák	k1gMnSc1	Dvořák
nakonec	nakonec	k6eAd1	nakonec
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
ale	ale	k9	ale
sehrála	sehrát	k5eAaPmAgFnS	sehrát
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
stále	stále	k6eAd1	stále
stoupající	stoupající	k2eAgFnSc4d1	stoupající
prestiž	prestiž	k1gFnSc4	prestiž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
stesk	stesk	k1gInSc4	stesk
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
Dvořák	Dvořák	k1gMnSc1	Dvořák
především	především	k9	především
odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ve	v	k7c6	v
Vysoké	vysoká	k1gFnSc6	vysoká
u	u	k7c2	u
Příbramě	Příbram	k1gFnSc2	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
složil	složit	k5eAaPmAgMnS	složit
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
oper	opera	k1gFnPc2	opera
–	–	k?	–
Rusalku	rusalka	k1gFnSc4	rusalka
a	a	k8xC	a
Armidu	Armida	k1gFnSc4	Armida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
tvorby	tvorba	k1gFnSc2	tvorba
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
inspirací	inspirace	k1gFnPc2	inspirace
také	také	k6eAd1	také
český	český	k2eAgInSc1d1	český
folklór	folklór	k1gInSc1	folklór
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
Dvořák	Dvořák	k1gMnSc1	Dvořák
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vychoval	vychovat	k5eAaPmAgInS	vychovat
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgMnPc2d1	významný
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Nedbal	Nedbal	k1gMnSc1	Nedbal
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
starší	starší	k1gMnSc1	starší
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Dvořákovou	Dvořákův	k2eAgFnSc7d1	Dvořákova
dcerou	dcera	k1gFnSc7	dcera
Otilií	Otilie	k1gFnPc2	Otilie
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gMnSc7	jeho
zetěm	zeť	k1gMnSc7	zeť
<g/>
.	.	kIx.	.
</s>
<s>
Dvořákovo	Dvořákův	k2eAgNnSc1d1	Dvořákovo
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
nyní	nyní	k6eAd1	nyní
uváděno	uvádět	k5eAaImNgNnS	uvádět
a	a	k8xC	a
oslavováno	oslavovat	k5eAaImNgNnS	oslavovat
doma	doma	k6eAd1	doma
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
a	a	k8xC	a
Hans	Hans	k1gMnSc1	Hans
Richter	Richter	k1gMnSc1	Richter
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
popularitě	popularita	k1gFnSc3	popularita
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Joachim	Joachim	k1gMnSc1	Joachim
a	a	k8xC	a
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
zpopularizovali	zpopularizovat	k5eAaPmAgMnP	zpopularizovat
Dvořákovu	Dvořákův	k2eAgFnSc4d1	Dvořákova
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Barnby	Barnba	k1gFnSc2	Barnba
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Dvořákovy	Dvořákův	k2eAgFnPc4d1	Dvořákova
šedesáté	šedesátý	k4xOgFnPc4	šedesátý
narozeniny	narozeniny	k1gFnPc4	narozeniny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
národní	národní	k2eAgFnSc7d1	národní
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
povýšil	povýšit	k5eAaPmAgInS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
jako	jako	k8xC	jako
rytíře	rytíř	k1gMnSc4	rytíř
(	(	kIx(	(
<g/>
Ritter	Rittra	k1gFnPc2	Rittra
von	von	k1gInSc1	von
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
členem	člen	k1gMnSc7	člen
Panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgInS	být
rytířem	rytíř	k1gMnSc7	rytíř
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
sněmovny	sněmovna	k1gFnSc2	sněmovna
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
také	také	k9	také
další	další	k2eAgMnSc1d1	další
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
Dvořák	Dvořák	k1gMnSc1	Dvořák
stal	stát	k5eAaPmAgMnS	stát
nástupcem	nástupce	k1gMnSc7	nástupce
Antonína	Antonín	k1gMnSc2	Antonín
Bennewitze	Bennewitze	k1gFnSc2	Bennewitze
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
Pražské	pražský	k2eAgFnSc2d1	Pražská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
pěti	pět	k4xCc6	pět
týdnech	týden	k1gInPc6	týden
nemoci	nemoc	k1gFnSc2	nemoc
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
velké	velký	k2eAgNnSc4d1	velké
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
také	také	k9	také
několik	několik	k4yIc1	několik
nedokončených	dokončený	k2eNgFnPc2d1	nedokončená
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
zbožný	zbožný	k2eAgMnSc1d1	zbožný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
laskavou	laskavý	k2eAgFnSc4d1	laskavá
a	a	k8xC	a
nekomplikovanou	komplikovaný	k2eNgFnSc4d1	nekomplikovaná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
velké	velký	k2eAgInPc4d1	velký
záliby	zálib	k1gInPc4	zálib
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
)	)	kIx)	)
patřila	patřit	k5eAaImAgFnS	patřit
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
chov	chov	k1gInSc1	chov
holubů	holub	k1gMnPc2	holub
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
agorafobií	agorafobie	k1gFnSc7	agorafobie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
stupňovala	stupňovat	k5eAaImAgFnS	stupňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kriminalistický	kriminalistický	k2eAgInSc1d1	kriminalistický
ústav	ústav	k1gInSc1	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozboru	rozbor	k1gInSc2	rozbor
DNA	DNA	kA	DNA
skladatelova	skladatelův	k2eAgMnSc4d1	skladatelův
vnuka	vnuk	k1gMnSc4	vnuk
Antonína	Antonín	k1gMnSc4	Antonín
Dvořáka	Dvořák	k1gMnSc4	Dvořák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
syna	syn	k1gMnSc2	syn
Otakara	Otakar	k1gMnSc2	Otakar
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkové	předek	k1gMnPc1	předek
Dvořáků	Dvořák	k1gMnPc2	Dvořák
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
nebo	nebo	k8xC	nebo
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaBmAgMnS	dít
Antonína	Antonín	k1gMnSc4	Antonín
Dvořáka	Dvořák	k1gMnSc4	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
osobitou	osobitý	k2eAgFnSc7d1	osobitá
klasicistně-romantickou	klasicistněomantický	k2eAgFnSc7d1	klasicistně-romantický
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc1d1	absolutní
jedinečnost	jedinečnost	k1gFnSc1	jedinečnost
skladatelova	skladatelův	k2eAgFnSc1d1	skladatelova
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
především	především	k9	především
v	v	k7c6	v
orchestraci	orchestrace	k1gFnSc6	orchestrace
a	a	k8xC	a
instrumentaci	instrumentace	k1gFnSc6	instrumentace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
vývoj	vývoj	k1gInSc1	vývoj
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
etapách	etapa	k1gFnPc6	etapa
<g/>
:	:	kIx,	:
první	první	k4xOgFnPc1	první
skladby	skladba	k1gFnPc1	skladba
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
odkazu	odkaz	k1gInSc2	odkaz
Beethovena	Beethoven	k1gMnSc2	Beethoven
a	a	k8xC	a
Schuberta	Schubert	k1gMnSc2	Schubert
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
jeho	jeho	k3xOp3gNnSc2	jeho
úsilí	úsilí	k1gNnSc2	úsilí
je	být	k5eAaImIp3nS	být
však	však	k9	však
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
jeho	jeho	k3xOp3gNnSc2	jeho
osobitého	osobitý	k2eAgNnSc2d1	osobité
cítění	cítění	k1gNnSc2	cítění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
projevuje	projevovat	k5eAaImIp3nS	projevovat
již	již	k6eAd1	již
naprosto	naprosto	k6eAd1	naprosto
originální	originální	k2eAgNnSc4d1	originální
a	a	k8xC	a
specifické	specifický	k2eAgNnSc4d1	specifické
hudební	hudební	k2eAgNnSc4d1	hudební
chápání	chápání	k1gNnSc4	chápání
jak	jak	k8xS	jak
po	po	k7c4	po
formální	formální	k2eAgNnSc4d1	formální
<g/>
,	,	kIx,	,
tak	tak	k9	tak
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
příklonem	příklon	k1gInSc7	příklon
k	k	k7c3	k
vlasteneckým	vlastenecký	k2eAgInPc3d1	vlastenecký
námětům	námět	k1gInPc3	námět
a	a	k8xC	a
českým	český	k2eAgFnPc3d1	Česká
hudebním	hudební	k2eAgFnPc3d1	hudební
inspiracím	inspirace	k1gFnPc3	inspirace
(	(	kIx(	(
<g/>
kantáta	kantáta	k1gFnSc1	kantáta
Hymnus	hymnus	k1gInSc1	hymnus
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
dvojzpěvy	dvojzpěv	k1gInPc1	dvojzpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
zabarvení	zabarvení	k1gNnSc2	zabarvení
dodalo	dodat	k5eAaPmAgNnS	dodat
jeho	jeho	k3xOp3gFnSc4	jeho
hudbě	hudba	k1gFnSc6	hudba
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
obecně	obecně	k6eAd1	obecně
slovanský	slovanský	k2eAgInSc4d1	slovanský
ráz	ráz	k1gInSc4	ráz
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
obohatil	obohatit	k5eAaPmAgMnS	obohatit
nejvýrazněji	výrazně	k6eAd3	výrazně
světovou	světový	k2eAgFnSc4d1	světová
hudební	hudební	k2eAgFnSc4d1	hudební
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
však	však	k9	však
opustil	opustit	k5eAaPmAgInS	opustit
české	český	k2eAgFnPc4d1	Česká
a	a	k8xC	a
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
amerického	americký	k2eAgInSc2d1	americký
pobytu	pobyt	k1gInSc2	pobyt
inspirován	inspirován	k2eAgInSc4d1	inspirován
hudbou	hudba	k1gFnSc7	hudba
černošskou	černošský	k2eAgFnSc7d1	černošská
a	a	k8xC	a
indiánskou	indiánský	k2eAgFnSc7d1	indiánská
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c4	v
jeho	jeho	k3xOp3gNnPc2	jeho
9	[number]	k4	9
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc4d1	zvaná
Novosvětská	novosvětský	k2eAgNnPc4d1	Novosvětské
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgNnPc3d3	nejlepší
dílům	dílo	k1gNnPc3	dílo
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
osobitě	osobitě	k6eAd1	osobitě
barvitému	barvitý	k2eAgInSc3d1	barvitý
projevu	projev	k1gInSc3	projev
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
inspirací	inspirace	k1gFnSc7	inspirace
českými	český	k2eAgFnPc7d1	Česká
pohádkami	pohádka	k1gFnPc7	pohádka
a	a	k8xC	a
bájemi	báj	k1gFnPc7	báj
(	(	kIx(	(
<g/>
opery	opera	k1gFnSc2	opera
Čert	čert	k1gMnSc1	čert
a	a	k8xC	a
Káča	Káča	k1gFnSc1	Káča
<g/>
,	,	kIx,	,
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
Jakobín	jakobín	k1gMnSc1	jakobín
<g/>
,	,	kIx,	,
Armida	Armida	k1gFnSc1	Armida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc4	devět
symfonií	symfonie	k1gFnPc2	symfonie
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc2d1	velká
skladby	skladba	k1gFnSc2	skladba
instrumentální	instrumentální	k2eAgInPc4d1	instrumentální
(	(	kIx(	(
<g/>
Slovanské	slovanský	k2eAgInPc4d1	slovanský
tance	tanec	k1gInPc4	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vokální	vokální	k2eAgFnSc1d1	vokální
a	a	k8xC	a
vokálně-instrumentální	vokálněnstrumentální	k2eAgFnSc1d1	vokálně-instrumentální
(	(	kIx(	(
<g/>
Stabat	Stabat	k1gFnSc1	Stabat
Mater	mater	k1gFnSc1	mater
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
,	,	kIx,	,
Rekviem	rekviem	k1gNnSc1	rekviem
<g/>
,	,	kIx,	,
Te	Te	k1gFnSc1	Te
Deum	Deum	k1gMnSc1	Deum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
koncertních	koncertní	k2eAgFnPc2d1	koncertní
ouvertur	ouvertura	k1gFnPc2	ouvertura
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
<g />
.	.	kIx.	.
</s>
<s>
komorních	komorní	k2eAgFnPc2d1	komorní
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
kvartet	kvartet	k1gInSc4	kvartet
F	F	kA	F
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Americký	americký	k2eAgInSc1d1	americký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncertů	koncert	k1gInPc2	koncert
(	(	kIx(	(
<g/>
houslový	houslový	k2eAgMnSc1d1	houslový
<g/>
,	,	kIx,	,
violoncellový	violoncellový	k2eAgMnSc1d1	violoncellový
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc4	píseň
(	(	kIx(	(
<g/>
Biblické	biblický	k2eAgFnPc4d1	biblická
písně	píseň	k1gFnPc4	píseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
Slovanských	slovanský	k2eAgInPc2d1	slovanský
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
Jakobín	jakobín	k1gMnSc1	jakobín
<g/>
,	,	kIx,	,
Čert	čert	k1gMnSc1	čert
a	a	k8xC	a
Káča	Káča	k1gFnSc1	Káča
<g/>
,	,	kIx,	,
Dimitrij	Dimitrij	k1gFnSc1	Dimitrij
a	a	k8xC	a
Armida	Armida	k1gFnSc1	Armida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uctíván	uctíván	k2eAgInSc1d1	uctíván
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
Nejrůznější	různý	k2eAgInPc4d3	nejrůznější
dokumenty	dokument	k1gInPc4	dokument
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
památky	památka	k1gFnPc4	památka
(	(	kIx(	(
<g/>
notové	notový	k2eAgInPc4d1	notový
autografy	autograf	k1gInPc4	autograf
<g/>
,	,	kIx,	,
korespondence	korespondence	k1gFnPc4	korespondence
<g/>
,	,	kIx,	,
listinné	listinný	k2eAgInPc1d1	listinný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
výtvarná	výtvarný	k2eAgNnPc1d1	výtvarné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
dobové	dobový	k2eAgFnPc1d1	dobová
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
plakáty	plakát	k1gInPc1	plakát
<g/>
)	)	kIx)	)
na	na	k7c4	na
tohoto	tento	k3xDgMnSc4	tento
výjimečného	výjimečný	k2eAgMnSc4d1	výjimečný
skladatele	skladatel	k1gMnSc4	skladatel
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgMnPc4d1	umístěn
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
letohrádku	letohrádek	k1gInSc6	letohrádek
Amerika	Amerika	k1gFnSc1	Amerika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
jak	jak	k8xC	jak
svým	svůj	k3xOyFgInSc7	svůj
počtem	počet	k1gInSc7	počet
<g/>
,	,	kIx,	,
tak	tak	k9	tak
rozsahem	rozsah	k1gInSc7	rozsah
forem	forma	k1gFnPc2	forma
–	–	k?	–
čítá	čítat	k5eAaImIp3nS	čítat
téměř	téměř	k6eAd1	téměř
120	[number]	k4	120
opusů	opus	k1gInPc2	opus
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
představuje	představovat	k5eAaImIp3nS	představovat
velká	velký	k2eAgFnSc1d1	velká
orchestrální	orchestrální	k2eAgFnSc3d1	orchestrální
<g/>
,	,	kIx,	,
vokálně-instrumentální	vokálněnstrumentální	k2eAgFnSc3d1	vokálně-instrumentální
či	či	k8xC	či
hudebně	hudebně	k6eAd1	hudebně
dramatická	dramatický	k2eAgNnPc1d1	dramatické
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Vysoké	vysoká	k1gFnSc6	vysoká
u	u	k7c2	u
Příbramě	Příbram	k1gFnSc2	Příbram
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
skladateli	skladatel	k1gMnSc6	skladatel
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
pobytech	pobyt	k1gInPc6	pobyt
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
impaktní	impaktní	k2eAgInSc1d1	impaktní
kráter	kráter	k1gInSc1	kráter
Dvořák	Dvořák	k1gMnSc1	Dvořák
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Merkur	Merkur	k1gInSc1	Merkur
a	a	k8xC	a
planetka	planetka	k1gFnSc1	planetka
2055	[number]	k4	2055
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zazněla	zaznít	k5eAaPmAgFnS	zaznít
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
amerických	americký	k2eAgMnPc2d1	americký
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
Novosvětská	novosvětský	k2eAgFnSc1d1	Novosvětská
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
vlaků	vlak	k1gInPc2	vlak
railjet	railjet	k5eAaImF	railjet
jezdící	jezdící	k2eAgInPc4d1	jezdící
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
Grazem	Graz	k1gInSc7	Graz
<g/>
.	.	kIx.	.
</s>
