<s>
Zakladatelka	zakladatelka	k1gFnSc1
americké	americký	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Jeanette	Jeanett	k1gInSc5
Thurberová	Thurberový	k2eAgNnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
snažila	snažit	k5eAaImAgFnS
získat	získat	k5eAaPmF
jako	jako	k9
ředitele	ředitel	k1gMnPc4
této	tento	k3xDgFnSc2
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>