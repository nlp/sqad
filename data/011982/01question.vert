<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
směs	směs	k1gFnSc1	směs
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
růstu	růst	k1gInSc2	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
?	?	kIx.	?
</s>
