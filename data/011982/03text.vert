<p>
<s>
Hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
směsi	směs	k1gFnPc1	směs
<g/>
,	,	kIx,	,
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
růstu	růst	k1gInSc2	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
aplikována	aplikován	k2eAgNnPc1d1	aplikováno
přes	přes	k7c4	přes
půdu	půda	k1gFnSc4	půda
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
kořeny	kořen	k2eAgFnPc1d1	Kořena
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
hnojením	hnojení	k1gNnPc3	hnojení
na	na	k7c4	na
list	list	k1gInSc4	list
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
listy	lista	k1gFnSc2	lista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnojiva	hnojivo	k1gNnPc1	hnojivo
obvykle	obvykle	k6eAd1	obvykle
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
poměrech	poměr	k1gInPc6	poměr
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
biogenní	biogenní	k2eAgInPc1d1	biogenní
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgInPc1d1	sekundární
biogenní	biogenní	k2eAgInPc1d1	biogenní
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
stopové	stopový	k2eAgInPc1d1	stopový
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
pro	pro	k7c4	pro
hnojení	hnojení	k1gNnSc4	hnojení
<g/>
:	:	kIx,	:
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
a	a	k8xC	a
molybden	molybden	k1gInSc1	molybden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
zemědělských	zemědělský	k2eAgNnPc2d1	zemědělské
hnojiv	hnojivo	k1gNnPc2	hnojivo
==	==	k?	==
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
statková	statkový	k2eAgFnSc1d1	statková
neboli	neboli	k8xC	neboli
organická	organický	k2eAgFnSc1d1	organická
(	(	kIx(	(
<g/>
hnůj	hnůj	k1gInSc1	hnůj
<g/>
,	,	kIx,	,
kompost	kompost	k1gInSc1	kompost
<g/>
,	,	kIx,	,
močůvka	močůvka	k1gFnSc1	močůvka
<g/>
,	,	kIx,	,
hnojůvka	hnojůvka	k1gFnSc1	hnojůvka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
neboli	neboli	k8xC	neboli
minerální	minerální	k2eAgNnPc1d1	minerální
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
</s>
</p>
<p>
<s>
jednosložková	jednosložkový	k2eAgFnSc1d1	jednosložková
(	(	kIx(	(
<g/>
močovina	močovina	k1gFnSc1	močovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vícesložková	vícesložkový	k2eAgFnSc1d1	vícesložková
neboli	neboli	k8xC	neboli
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
</s>
</p>
<p>
<s>
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
(	(	kIx(	(
<g/>
ledek	ledek	k1gInSc1	ledek
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
trojitá	trojitý	k2eAgFnSc1d1	trojitá
neboli	neboli	k8xC	neboli
plná	plný	k2eAgFnSc1d1	plná
(	(	kIx(	(
<g/>
NPK	NPK	kA	NPK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
půdní	půdní	k2eAgFnSc4d1	půdní
reakci	reakce	k1gFnSc4	reakce
</s>
</p>
<p>
<s>
kyselá	kyselá	k1gFnSc1	kyselá
(	(	kIx(	(
<g/>
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
neutrální	neutrální	k2eAgFnPc1d1	neutrální
(	(	kIx(	(
<g/>
ledek	ledek	k1gInSc1	ledek
amonný	amonný	k2eAgInSc1d1	amonný
s	s	k7c7	s
vápencem	vápenec	k1gInSc7	vápenec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zásaditá	zásaditý	k2eAgFnSc1d1	zásaditá
(	(	kIx(	(
<g/>
dusíkaté	dusíkatý	k2eAgNnSc1d1	dusíkaté
vápno	vápno	k1gNnSc1	vápno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
</s>
</p>
<p>
<s>
tuhá	tuhý	k2eAgFnSc1d1	tuhá
(	(	kIx(	(
<g/>
hnůj	hnůj	k1gInSc1	hnůj
<g/>
,	,	kIx,	,
kompost	kompost	k1gInSc1	kompost
<g/>
,	,	kIx,	,
superfosfát	superfosfát	k1gInSc1	superfosfát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kapalná	kapalný	k2eAgFnSc1d1	kapalná
(	(	kIx(	(
<g/>
močůvka	močůvka	k1gFnSc1	močůvka
<g/>
,	,	kIx,	,
kejda	kejda	k1gFnSc1	kejda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
účinnosti	účinnost	k1gFnSc2	účinnost
</s>
</p>
<p>
<s>
hnojiva	hnojivo	k1gNnPc4	hnojivo
přímá	přímý	k2eAgFnSc1d1	přímá
</s>
</p>
<p>
<s>
hnojiva	hnojivo	k1gNnPc4	hnojivo
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
</s>
</p>
<p>
<s>
==	==	k?	==
Hnojiva	hnojivo	k1gNnPc4	hnojivo
podle	podle	k7c2	podle
účinnosti	účinnost	k1gFnSc2	účinnost
==	==	k?	==
</s>
</p>
<p>
<s>
Přímá	přímý	k2eAgNnPc1d1	přímé
hnojiva	hnojivo	k1gNnPc1	hnojivo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
živin	živina	k1gFnPc2	živina
buďto	buďto	k8xC	buďto
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
nebo	nebo	k8xC	nebo
minerální	minerální	k2eAgFnSc6d1	minerální
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Dodávají	dodávat	k5eAaImIp3nP	dodávat
rostlinám	rostlina	k1gFnPc3	rostlina
makro-	makro-	k?	makro-
nebo	nebo	k8xC	nebo
mikroprvky	mikroprvka	k1gFnSc2	mikroprvka
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnPc4	on
hnojiva	hnojivo	k1gNnPc4	hnojivo
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
i	i	k8xC	i
statková	statkový	k2eAgNnPc4d1	statkové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepřímá	přímý	k2eNgNnPc1d1	nepřímé
hnojiva	hnojivo	k1gNnPc1	hnojivo
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
základní	základní	k2eAgFnPc4d1	základní
živiny	živina	k1gFnPc4	živina
v	v	k7c6	v
zásadním	zásadní	k2eAgNnSc6d1	zásadní
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
metabolismus	metabolismus	k1gInSc4	metabolismus
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
příjem	příjem	k1gInSc4	příjem
základních	základní	k2eAgFnPc2d1	základní
živin	živina	k1gFnPc2	živina
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyšším	vysoký	k2eAgInPc3d2	vyšší
výnosům	výnos	k1gInPc3	výnos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
půdní	půdní	k2eAgFnPc1d1	půdní
kondicionéry	kondicionér	k1gInPc4	kondicionér
<g/>
,	,	kIx,	,
regulátory	regulátor	k1gInPc4	regulátor
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
stimulátory	stimulátor	k1gInPc4	stimulátor
i	i	k8xC	i
inhibitory	inhibitor	k1gInPc4	inhibitor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bakteriální	bakteriální	k2eAgNnPc1d1	bakteriální
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
inhibitory	inhibitor	k1gInPc1	inhibitor
mikrobiotických	mikrobiotický	k2eAgInPc2d1	mikrobiotický
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnojiva	hnojivo	k1gNnPc4	hnojivo
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
snadnost	snadnost	k1gFnSc4	snadnost
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
přijatelnost	přijatelnost	k1gFnSc1	přijatelnost
živin	živina	k1gFnPc2	živina
rostlinou	rostlina	k1gFnSc7	rostlina
nebo	nebo	k8xC	nebo
druh	druh	k1gInSc1	druh
použité	použitý	k2eAgFnSc2d1	použitá
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
používán	používán	k2eAgMnSc1d1	používán
CO2	CO2	k1gMnSc1	CO2
pro	pro	k7c4	pro
přihnojení	přihnojení	k1gNnSc4	přihnojení
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
salátu	salát	k1gInSc2	salát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
rozlišovány	rozlišovat	k5eAaImNgFnP	rozlišovat
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
pouze	pouze	k6eAd1	pouze
-	-	kIx~	-
</s>
</p>
<p>
<s>
pevná	pevný	k2eAgNnPc4d1	pevné
hnojiva	hnojivo	k1gNnPc4	hnojivo
</s>
</p>
<p>
<s>
tekutá	tekutý	k2eAgNnPc1d1	tekuté
hnojiva	hnojivo	k1gNnPc1	hnojivo
</s>
</p>
<p>
<s>
===	===	k?	===
Pevná	pevný	k2eAgNnPc1d1	pevné
hnojiva	hnojivo	k1gNnPc1	hnojivo
===	===	k?	===
</s>
</p>
<p>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
také	také	k9	také
nazývána	nazýván	k2eAgNnPc1d1	nazýváno
tuhá	tuhý	k2eAgNnPc1d1	tuhé
<g/>
)	)	kIx)	)
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
aplikována	aplikován	k2eAgNnPc1d1	aplikováno
rozmetadly	rozmetadlo	k1gNnPc7	rozmetadlo
<g/>
.	.	kIx.	.
</s>
<s>
Minerální	minerální	k2eAgNnPc1d1	minerální
hnojiva	hnojivo	k1gNnPc1	hnojivo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
výroby	výroba	k1gFnSc2	výroba
krystalická	krystalický	k2eAgFnSc1d1	krystalická
nebo	nebo	k8xC	nebo
granulovaná	granulovaný	k2eAgFnSc1d1	granulovaná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
částic	částice	k1gFnPc2	částice
jsou	být	k5eAaImIp3nP	být
dělena	dělit	k5eAaImNgNnP	dělit
na	na	k7c4	na
-	-	kIx~	-
</s>
</p>
<p>
<s>
prášková	práškový	k2eAgFnSc1d1	prášková
(	(	kIx(	(
<g/>
převládají	převládat	k5eAaImIp3nP	převládat
částice	částice	k1gFnPc1	částice
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
1	[number]	k4	1
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zrnitá	zrnitý	k2eAgFnSc1d1	zrnitá
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
zpravidla	zpravidla	k6eAd1	zpravidla
1-4	[number]	k4	1-4
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
Mezi	mezi	k7c4	mezi
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
pevná	pevný	k2eAgNnPc4d1	pevné
hnojiva	hnojivo	k1gNnPc4	hnojivo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
draselná	draselný	k2eAgFnSc1d1	draselná
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
NPK	NPK	kA	NPK
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organická	organický	k2eAgNnPc1d1	organické
<g/>
,	,	kIx,	,
statková	statkový	k2eAgNnPc1d1	statkové
pevná	pevný	k2eAgNnPc1d1	pevné
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
homogení	homogenit	k5eAaPmIp3nP	homogenit
směsi	směs	k1gFnPc4	směs
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
obsahem	obsah	k1gInSc7	obsah
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
chlévský	chlévský	k2eAgInSc4d1	chlévský
hnůj	hnůj	k1gInSc4	hnůj
nebo	nebo	k8xC	nebo
komposty	kompost	k1gInPc4	kompost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tekutá	tekutý	k2eAgNnPc1d1	tekuté
hnojiva	hnojivo	k1gNnPc1	hnojivo
===	===	k?	===
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
tekutá	tekutý	k2eAgNnPc1d1	tekuté
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
vyráběna	vyráběn	k2eAgNnPc1d1	vyráběno
jako	jako	k8xC	jako
ředitelná	ředitelný	k2eAgNnPc1d1	ředitelné
jednosložková	jednosložkový	k2eAgNnPc1d1	jednosložkové
nebo	nebo	k8xC	nebo
vícesložková	vícesložkový	k2eAgNnPc1d1	vícesložkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
daným	daný	k2eAgInSc7d1	daný
obsahem	obsah	k1gInSc7	obsah
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
možnosti	možnost	k1gFnPc4	možnost
mísení	mísení	k1gNnSc1	mísení
jako	jako	k8xC	jako
například	například	k6eAd1	například
DAM	dáma	k1gFnPc2	dáma
(	(	kIx(	(
<g/>
např	např	kA	např
DAM	dáma	k1gFnPc2	dáma
390	[number]	k4	390
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
statková	statkový	k2eAgNnPc4d1	statkové
tekutá	tekutý	k2eAgNnPc4d1	tekuté
hnojiva	hnojivo	k1gNnPc4	hnojivo
patří	patřit	k5eAaImIp3nP	patřit
močůvka	močůvka	k1gFnSc1	močůvka
nebo	nebo	k8xC	nebo
kejda	kejda	k1gFnSc1	kejda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnojiva	hnojivo	k1gNnSc2	hnojivo
podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Statková	statkový	k2eAgNnPc1d1	statkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
statková	statkový	k2eAgNnPc4d1	statkové
jak	jak	k8xC	jak
bývají	bývat	k5eAaImIp3nP	bývat
nazývána	nazýván	k2eAgNnPc1d1	nazýváno
také	také	k6eAd1	také
organická	organický	k2eAgNnPc1d1	organické
hnojiva	hnojivo	k1gNnPc1	hnojivo
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
především	především	k9	především
jako	jako	k9	jako
vydatné	vydatný	k2eAgInPc4d1	vydatný
zdroje	zdroj	k1gInPc4	zdroj
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
živin	živina	k1gFnPc2	živina
chlévský	chlévský	k2eAgInSc1d1	chlévský
hnůj	hnůj	k1gInSc1	hnůj
<g/>
,	,	kIx,	,
kompost	kompost	k1gInSc1	kompost
a	a	k8xC	a
močůvka	močůvka	k1gFnSc1	močůvka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
použití	použití	k1gNnSc1	použití
směsí	směs	k1gFnPc2	směs
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
zbytků	zbytek	k1gInPc2	zbytek
s	s	k7c7	s
organickými	organický	k2eAgNnPc7d1	organické
nebo	nebo	k8xC	nebo
průmyslovými	průmyslový	k2eAgNnPc7d1	průmyslové
hnojivy	hnojivo	k1gNnPc7	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
posklizňové	posklizňový	k2eAgInPc4d1	posklizňový
zbytky	zbytek	k1gInPc4	zbytek
<g/>
,	,	kIx,	,
slámu	sláma	k1gFnSc4	sláma
a	a	k8xC	a
zelené	zelený	k2eAgNnSc4d1	zelené
hnojení	hnojení	k1gNnSc4	hnojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Chlévský	chlévský	k2eAgInSc1d1	chlévský
hnůj	hnůj	k1gInSc1	hnůj
====	====	k?	====
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
hnojení	hnojení	k1gNnSc3	hnojení
dobře	dobře	k6eAd1	dobře
vyzrálý	vyzrálý	k2eAgInSc4d1	vyzrálý
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jím	jíst	k5eAaImIp1nS	jíst
hnojit	hnojit	k5eAaImF	hnojit
na	na	k7c6	na
volných	volný	k2eAgFnPc6d1	volná
venkovních	venkovní	k2eAgFnPc6d1	venkovní
plochách	plocha	k1gFnPc6	plocha
i	i	k8xC	i
ve	v	k7c6	v
skleníkových	skleníkový	k2eAgInPc6d1	skleníkový
záhonech	záhon	k1gInPc6	záhon
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
pařenišť	pařeniště	k1gNnPc2	pařeniště
jako	jako	k8xS	jako
výhřevný	výhřevný	k2eAgInSc4d1	výhřevný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
především	především	k6eAd1	především
jako	jako	k8xS	jako
přísun	přísun	k1gInSc4	přísun
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
ale	ale	k8xC	ale
i	i	k9	i
nakypřovací	nakypřovací	k2eAgInSc4d1	nakypřovací
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kompost	kompost	k1gInSc4	kompost
====	====	k?	====
</s>
</p>
<p>
<s>
Kompost	kompost	k1gInSc1	kompost
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
hnojivem	hnojivo	k1gNnSc7	hnojivo
pro	pro	k7c4	pro
venkovní	venkovní	k2eAgFnPc4d1	venkovní
zahradní	zahradní	k2eAgFnPc4d1	zahradní
půdy	půda	k1gFnPc4	půda
i	i	k9	i
pro	pro	k7c4	pro
zeminy	zemina	k1gFnPc4	zemina
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
živin	živina	k1gFnPc2	živina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
humus	humus	k1gInSc1	humus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vhodný	vhodný	k2eAgMnSc1d1	vhodný
k	k	k7c3	k
hnojení	hnojení	k1gNnSc3	hnojení
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
hnojení	hnojení	k1gNnSc1	hnojení
chlévským	chlévský	k2eAgInSc7d1	chlévský
hnojem	hnůj	k1gInSc7	hnůj
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
humusu	humus	k1gInSc2	humus
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komposty	kompost	k1gInPc1	kompost
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
kompostováním	kompostování	k1gNnSc7	kompostování
organických	organický	k2eAgFnPc2d1	organická
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vhodný	vhodný	k2eAgInSc4d1	vhodný
poměr	poměr	k1gInSc4	poměr
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
N	N	kA	N
a	a	k8xC	a
mírnou	mírný	k2eAgFnSc4d1	mírná
vlhkost	vlhkost	k1gFnSc4	vlhkost
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
aerobní	aerobní	k2eAgFnPc1d1	aerobní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ornice	ornice	k1gFnSc2	ornice
a	a	k8xC	a
vápence	vápenec	k1gInSc2	vápenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
i	i	k8xC	i
přídavek	přídavek	k1gInSc1	přídavek
minerálních	minerální	k2eAgNnPc2d1	minerální
hnojiv	hnojivo	k1gNnPc2	hnojivo
pro	pro	k7c4	pro
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
obsahu	obsah	k1gInSc2	obsah
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Komposty	kompost	k1gInPc1	kompost
mohou	moct	k5eAaImIp3nP	moct
zrát	zrát	k5eAaImF	zrát
např.	např.	kA	např.
na	na	k7c6	na
hromadách	hromada	k1gFnPc6	hromada
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1,5	[number]	k4	1,5
<g/>
m	m	kA	m
vysokých	vysoký	k2eAgFnPc2d1	vysoká
až	až	k8xS	až
3	[number]	k4	3
roky	rok	k1gInPc7	rok
při	při	k7c6	při
občasném	občasný	k2eAgNnSc6d1	občasné
přehazování	přehazování	k1gNnSc6	přehazování
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
kompostů	kompost	k1gInPc2	kompost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
vstupních	vstupní	k2eAgFnPc2d1	vstupní
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hnojení	hnojení	k1gNnSc4	hnojení
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
komposty	kompost	k1gInPc1	kompost
propařovat	propařovat	k5eAaImF	propařovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zbavily	zbavit	k5eAaPmAgInP	zbavit
semen	semeno	k1gNnPc2	semeno
plevelů	plevel	k1gInPc2	plevel
a	a	k8xC	a
možných	možný	k2eAgMnPc2d1	možný
parazitů	parazit	k1gMnPc2	parazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Močůvka	močůvka	k1gFnSc1	močůvka
====	====	k?	====
</s>
</p>
<p>
<s>
Močůvka	močůvka	k1gFnSc1	močůvka
je	být	k5eAaImIp3nS	být
prokvašená	prokvašený	k2eAgFnSc1d1	prokvašená
moč	moč	k1gFnSc1	moč
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
produkována	produkovat	k5eAaImNgFnS	produkovat
v	v	k7c6	v
klasických	klasický	k2eAgFnPc6d1	klasická
vazných	vazný	k2eAgFnPc6d1	vazná
stájích	stáj	k1gFnPc6	stáj
s	s	k7c7	s
nastýlaným	nastýlaný	k2eAgNnSc7d1	nastýlaný
ložem	lože	k1gNnSc7	lože
<g/>
.	.	kIx.	.
</s>
<s>
Močůvka	močůvka	k1gFnSc1	močůvka
bývá	bývat	k5eAaImIp3nS	bývat
zředěna	zředit	k5eAaPmNgFnS	zředit
vodou	voda	k1gFnSc7	voda
používanou	používaný	k2eAgFnSc7d1	používaná
ve	v	k7c6	v
stájích	stáj	k1gFnPc6	stáj
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
a	a	k8xC	a
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Močůvka	močůvka	k1gFnSc1	močůvka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
podíl	podíl	k1gInSc1	podíl
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
hodnotným	hodnotný	k2eAgNnSc7d1	hodnotné
statkovým	statkový	k2eAgNnSc7d1	statkové
hnojivem	hnojivo	k1gNnSc7	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Močůvku	močůvka	k1gFnSc4	močůvka
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
deseti	deset	k4xCc6	deset
až	až	k6eAd1	až
dvacetinásobném	dvacetinásobný	k2eAgNnSc6d1	dvacetinásobné
zředění	zředění	k1gNnSc6	zředění
jako	jako	k8xC	jako
hnojivou	hnojivý	k2eAgFnSc4d1	hnojivá
zálivku	zálivka	k1gFnSc4	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
pozitivně	pozitivně	k6eAd1	pozitivně
působí	působit	k5eAaImIp3nP	působit
obsažené	obsažený	k2eAgInPc1d1	obsažený
auxiny	auxin	k1gInPc1	auxin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnojiva	hnojivo	k1gNnPc4	hnojivo
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
===	===	k?	===
</s>
</p>
<p>
<s>
Neboli	neboli	k8xC	neboli
minerální	minerální	k2eAgInSc1d1	minerální
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
spadají	spadat	k5eAaImIp3nP	spadat
všechny	všechen	k3xTgFnPc4	všechen
látky	látka	k1gFnPc4	látka
využívané	využívaný	k2eAgFnPc1d1	využívaná
ke	k	k7c3	k
hnojení	hnojení	k1gNnSc3	hnojení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
mimo	mimo	k7c4	mimo
zemědělský	zemědělský	k2eAgInSc4d1	zemědělský
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
produkty	produkt	k1gInPc4	produkt
chemického	chemický	k2eAgNnSc2d1	chemické
<g/>
,	,	kIx,	,
těžebního	těžební	k2eAgMnSc2d1	těžební
a	a	k8xC	a
stavebního	stavební	k2eAgInSc2d1	stavební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
se	se	k3xPyFc4	se
krom	krom	k7c2	krom
skupenského	skupenský	k2eAgNnSc2d1	skupenské
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	na
pevná	pevný	k2eAgNnPc4d1	pevné
a	a	k8xC	a
kapalná	kapalný	k2eAgFnSc1d1	kapalná
dělí	dělit	k5eAaImIp3nS	dělit
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
</s>
</p>
<p>
<s>
plynná	plynný	k2eAgFnSc1d1	plynná
</s>
</p>
<p>
<s>
kapalná	kapalný	k2eAgFnSc1d1	kapalná
</s>
</p>
<p>
<s>
pevná	pevný	k2eAgFnSc1d1	pevná
</s>
</p>
<p>
<s>
prášková	práškový	k2eAgFnSc1d1	prášková
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
do	do	k7c2	do
1	[number]	k4	1
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zrnitá	zrnitý	k2eAgFnSc1d1	zrnitá
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
krystalická	krystalický	k2eAgFnSc1d1	krystalická
</s>
</p>
<p>
<s>
granulovaná	granulovaný	k2eAgFnSc1d1	granulovaná
</s>
</p>
<p>
<s>
====	====	k?	====
Podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
====	====	k?	====
</s>
</p>
<p>
<s>
Hnojiva	hnojivo	k1gNnPc1	hnojivo
se	s	k7c7	s
stěžejním	stěžejní	k2eAgInSc7d1	stěžejní
obsahem	obsah	k1gInSc7	obsah
jedné	jeden	k4xCgFnSc2	jeden
živiny	živina	k1gFnSc2	živina
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
jednosložková	jednosložkový	k2eAgNnPc1d1	jednosložkové
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dusíkatá	dusíkatý	k2eAgNnPc4d1	dusíkaté
<g/>
,	,	kIx,	,
fosforečná	fosforečný	k2eAgNnPc1d1	fosforečné
<g/>
,	,	kIx,	,
draselná	draselný	k2eAgNnPc1d1	draselné
<g/>
,	,	kIx,	,
hořečnatá	hořečnatý	k2eAgNnPc1d1	hořečnaté
a	a	k8xC	a
vápenatá	vápenatý	k2eAgNnPc1d1	vápenaté
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
mikroprvky	mikroprvek	k1gInPc1	mikroprvek
a	a	k8xC	a
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
ionty	ion	k1gInPc1	ion
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
SO	So	kA	So
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
Cl-	Cl-	k1gMnSc1	Cl-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
hnojiva	hnojivo	k1gNnPc1	hnojivo
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Také	také	k9	také
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
jednosložková	jednosložkový	k2eAgNnPc1d1	jednosložkové
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
živinu	živina	k1gFnSc4	živina
–	–	k?	–
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
základní	základní	k2eAgNnPc4d1	základní
hnojiva	hnojivo	k1gNnPc4	hnojivo
nebo	nebo	k8xC	nebo
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
vhodného	vhodný	k2eAgNnSc2d1	vhodné
vícesložkového	vícesložkový	k2eAgNnSc2d1	vícesložkové
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
k	k	k7c3	k
jednosložkovým	jednosložkový	k2eAgNnPc3d1	jednosložkové
hnojivům	hnojivo	k1gNnPc3	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
hlavních	hlavní	k2eAgFnPc2d1	hlavní
živin	živina	k1gFnPc2	živina
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k8xC	i
hnojiva	hnojivo	k1gNnPc1	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
hořčíku	hořčík	k1gInSc2	hořčík
(	(	kIx(	(
<g/>
hořečnatá	hořečnatý	k2eAgNnPc1d1	hořečnaté
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hlavní	hlavní	k2eAgFnSc2d1	hlavní
živiny	živina	k1gFnSc2	živina
lze	lze	k6eAd1	lze
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
hnojiva	hnojivo	k1gNnPc4	hnojivo
rozčlenit	rozčlenit	k5eAaPmF	rozčlenit
na	na	k7c4	na
</s>
</p>
<p>
<s>
dusíkatá	dusíkatý	k2eAgNnPc1d1	dusíkaté
hnojiva	hnojivo	k1gNnPc1	hnojivo
</s>
</p>
<p>
<s>
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
nitrátovým	nitrátový	k2eAgInSc7d1	nitrátový
(	(	kIx(	(
<g/>
ledkovým	ledkový	k2eAgInSc7d1	ledkový
<g/>
,	,	kIx,	,
dusičnanovým	dusičnanový	k2eAgInSc7d1	dusičnanový
<g/>
)	)	kIx)	)
NO3-	NO3-	k1gMnSc3	NO3-
</s>
</p>
<p>
<s>
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
amonným	amonný	k2eAgInSc7d1	amonný
a	a	k8xC	a
amoniakálním	amoniakální	k2eAgMnSc7d1	amoniakální
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
NH3	NH3	k1gMnSc1	NH3
</s>
</p>
<p>
<s>
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
amidovým	amidový	k2eAgInSc7d1	amidový
(	(	kIx(	(
<g/>
organickým	organický	k2eAgFnPc3d1	organická
<g/>
)	)	kIx)	)
NH2	NH2	k1gFnPc3	NH2
</s>
</p>
<p>
<s>
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
i	i	k8xC	i
více	hodně	k6eAd2	hodně
formách	forma	k1gFnPc6	forma
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
NH2	NH2	k1gMnSc1	NH2
</s>
</p>
<p>
<s>
pomalu	pomalu	k6eAd1	pomalu
působící	působící	k2eAgInPc1d1	působící
<g/>
.	.	kIx.	.
<g/>
fosforečná	fosforečný	k2eAgNnPc1d1	fosforečné
hnojiva	hnojivo	k1gNnPc1	hnojivo
</s>
</p>
<p>
<s>
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
–	–	k?	–
superfosfáty	superfosfát	k1gInPc4	superfosfát
</s>
</p>
<p>
<s>
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
v	v	k7c6	v
citranu	citran	k1gInSc6	citran
amonném	amonný	k2eAgInSc6d1	amonný
–	–	k?	–
dikalciumfosfát	dikalciumfosfát	k5eAaPmF	dikalciumfosfát
</s>
</p>
<p>
<s>
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
ve	v	k7c6	v
2	[number]	k4	2
<g/>
%	%	kIx~	%
<g/>
ní	on	k3xPp3gFnSc3	on
kyselině	kyselina	k1gFnSc3	kyselina
citrónové	citrónový	k2eAgFnPc4d1	citrónová
–	–	k?	–
Thomasova	Thomasův	k2eAgFnSc1d1	Thomasova
moučka	moučka	k1gFnSc1	moučka
<g/>
,	,	kIx,	,
Dopofos	Dopofos	k1gInSc1	Dopofos
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
P	P	kA	P
z	z	k7c2	z
mletých	mletý	k2eAgInPc2d1	mletý
fosfátů	fosfát	k1gInPc2	fosfát
</s>
</p>
<p>
<s>
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
v	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
kyselinách	kyselina	k1gFnPc6	kyselina
–	–	k?	–
mleté	mletý	k2eAgInPc4d1	mletý
fosfáty	fosfát	k1gInPc4	fosfát
<g/>
,	,	kIx,	,
hyperfosfáty	hyperfosfát	k1gInPc4	hyperfosfát
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc2d1	kostní
moučky	moučka	k1gFnSc2	moučka
<g/>
.	.	kIx.	.
</s>
<s>
Dolofos	Dolofos	k1gInSc1	Dolofos
15	[number]	k4	15
<g/>
,	,	kIx,	,
Dolofos	Dolofos	k1gInSc1	Dolofos
26	[number]	k4	26
<g/>
,	,	kIx,	,
Donaukorn	Donaukorn	k1gInSc1	Donaukorn
26	[number]	k4	26
<g/>
%	%	kIx~	%
<g/>
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Fosmag	Fosmag	k1gInSc1	Fosmag
<g/>
,	,	kIx,	,
Hyperkorn	Hyperkorn	k1gInSc1	Hyperkorn
26	[number]	k4	26
<g/>
%	%	kIx~	%
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
%	%	kIx~	%
MgO	MgO	k1gFnSc1	MgO
<g/>
,	,	kIx,	,
Hyperkorn	Hyperkorn	k1gInSc1	Hyperkorn
26	[number]	k4	26
<g/>
%	%	kIx~	%
<g/>
+	+	kIx~	+
<g/>
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
MgO	MgO	k1gFnSc1	MgO
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc1d1	kostní
moučka	moučka	k1gFnSc1	moučka
na	na	k7c6	na
hnojení	hnojení	k1gNnSc6	hnojení
<g/>
.	.	kIx.	.
<g/>
draselná	draselný	k2eAgNnPc1d1	draselné
hnojiva	hnojivo	k1gNnPc1	hnojivo
</s>
</p>
<p>
<s>
síranový	síranový	k2eAgInSc1d1	síranový
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
sírany	síran	k1gInPc1	síran
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chloridový	chloridový	k2eAgInSc1d1	chloridový
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
chloridy	chlorid	k1gInPc1	chlorid
<g/>
)	)	kIx)	)
<g/>
vápenatá	vápenatý	k2eAgNnPc1d1	vápenaté
hnojiva	hnojivo	k1gNnPc1	hnojivo
</s>
</p>
<p>
<s>
s	s	k7c7	s
uhličitanovou	uhličitanový	k2eAgFnSc7d1	uhličitanová
formou	forma	k1gFnSc7	forma
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
žíravou	žíravý	k2eAgFnSc7d1	žíravá
formou	forma	k1gFnSc7	forma
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
CaO	CaO	k1gMnSc1	CaO
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
2	[number]	k4	2
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
se	s	k7c7	s
síranovou	síranový	k2eAgFnSc7d1	síranová
formou	forma	k1gFnSc7	forma
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
křemičitanovou	křemičitanový	k2eAgFnSc7d1	křemičitanová
formou	forma	k1gFnSc7	forma
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Vícesložková	vícesložkový	k2eAgNnPc1d1	vícesložkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
živiny	živina	k1gFnPc4	živina
(	(	kIx(	(
<g/>
makroprvky	makroprvka	k1gFnPc4	makroprvka
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
i	i	k9	i
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
živiny	živina	k1gFnPc1	živina
(	(	kIx(	(
<g/>
mikroprvky	mikroprvka	k1gFnPc1	mikroprvka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
používáním	používání	k1gNnSc7	používání
je	být	k5eAaImIp3nS	být
přihnojování	přihnojování	k1gNnSc1	přihnojování
zjednodušeno	zjednodušen	k2eAgNnSc1d1	zjednodušeno
a	a	k8xC	a
efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
výroby	výroba	k1gFnSc2	výroba
vícesložkových	vícesložkový	k2eAgNnPc2d1	vícesložkové
hnojiv	hnojivo	k1gNnPc2	hnojivo
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
</s>
</p>
<p>
<s>
vícesložková	vícesložkový	k2eAgNnPc1d1	vícesložkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
směsná	směsný	k2eAgNnPc1d1	směsné
(	(	kIx(	(
<g/>
smíšená	smíšený	k2eAgFnSc1d1	smíšená
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vícesložková	vícesložkový	k2eAgNnPc1d1	vícesložkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
kombinovaná	kombinovaný	k2eAgNnPc1d1	kombinované
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
hnojivu	hnojivo	k1gNnSc6	hnojivo
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
živin	živina	k1gFnPc2	živina
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
označení	označení	k1gNnSc4	označení
vícesložková	vícesložkový	k2eAgFnSc1d1	vícesložková
neboli	neboli	k8xC	neboli
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dvě	dva	k4xCgFnPc4	dva
živiny	živina	k1gFnPc4	živina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
trojitá	trojitý	k2eAgFnSc1d1	trojitá
čili	čili	k8xC	čili
plná	plný	k2eAgFnSc1d1	plná
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
živiny	živina	k1gFnPc4	živina
N	N	kA	N
<g/>
,	,	kIx,	,
P	P	kA	P
a	a	k8xC	a
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
ještě	ještě	k9	ještě
mikrohnojiva	mikrohnojivo	k1gNnPc1	mikrohnojivo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
převážně	převážně	k6eAd1	převážně
stopové	stopový	k2eAgInPc1d1	stopový
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sledovaným	sledovaný	k2eAgFnPc3d1	sledovaná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
vícesložkových	vícesložkový	k2eAgNnPc2d1	vícesložkové
hnojiv	hnojivo	k1gNnPc2	hnojivo
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
obsah	obsah	k1gInSc1	obsah
přijatelných	přijatelný	k2eAgFnPc2d1	přijatelná
a	a	k8xC	a
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
poměr	poměr	k1gInSc1	poměr
těchto	tento	k3xDgFnPc2	tento
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prodávaná	prodávaný	k2eAgNnPc1d1	prodávané
hnojiva	hnojivo	k1gNnPc1	hnojivo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dusíkatá	dusíkatý	k2eAgNnPc4d1	dusíkaté
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
komerčních	komerční	k2eAgInPc2d1	komerční
názvů	název	k1gInPc2	název
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Močovina	močovina	k1gFnSc1	močovina
(	(	kIx(	(
<g/>
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnojivo	hnojivo	k1gNnSc1	hnojivo
obsahující	obsahující	k2eAgFnSc4d1	obsahující
močovinu	močovina	k1gFnSc4	močovina
</s>
</p>
<p>
<s>
Ledek	ledek	k1gInSc1	ledek
amonný	amonný	k2eAgInSc1d1	amonný
27	[number]	k4	27
%	%	kIx~	%
N	N	kA	N
hnojivo	hnojivo	k1gNnSc1	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
dusičnanu	dusičnan	k1gInSc2	dusičnan
amonného	amonný	k2eAgInSc2d1	amonný
</s>
</p>
<p>
<s>
Ledek	ledek	k1gInSc1	ledek
amonný	amonný	k2eAgInSc1d1	amonný
34	[number]	k4	34
%	%	kIx~	%
N	N	kA	N
</s>
</p>
<p>
<s>
DAM	dáma	k1gFnPc2	dáma
390	[number]	k4	390
(	(	kIx(	(
<g/>
Dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
s	s	k7c7	s
močovinou	močovina	k1gFnSc7	močovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DASA	DASA	kA	DASA
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc4d1	amonný
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
amonným	amonný	k2eAgInSc7d1	amonný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc1	síran
amonný	amonný	k2eAgInSc1d1	amonný
-	-	kIx~	-
hnojivo	hnojivo	k1gNnSc1	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
</s>
</p>
<p>
<s>
SAM	Sam	k1gMnSc1	Sam
-	-	kIx~	-
hnojivo	hnojivo	k1gNnSc1	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
a	a	k8xC	a
močoviny	močovina	k1gFnPc4	močovina
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hnojivo	hnojivo	k1gNnSc4	hnojivo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hnojivo	hnojivo	k1gNnSc4	hnojivo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
výpočet	výpočet	k1gInSc4	výpočet
bilance	bilance	k1gFnSc2	bilance
organických	organický	k2eAgFnPc2d1	organická
hnojiv	hnojivo	k1gNnPc2	hnojivo
</s>
</p>
