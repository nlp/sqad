<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Christopher	Christophra	k1gFnPc2	Christophra
Leto	Leto	k1gMnSc1	Leto
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
především	především	k6eAd1	především
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
30	[number]	k4	30
Seconds	Secondsa	k1gFnPc2	Secondsa
to	ten	k3xDgNnSc4	ten
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Jaredem	Jared	k1gMnSc7	Jared
Leto	Leto	k6eAd1	Leto
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Thirty	Thirta	k1gFnSc2	Thirta
Seconds	Seconds	k1gInSc1	Seconds
to	ten	k3xDgNnSc1	ten
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
shledalo	shledat	k5eAaPmAgNnS	shledat
s	s	k7c7	s
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
světové	světový	k2eAgFnSc2d1	světová
slávy	sláva	k1gFnSc2	sláva
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
dočkala	dočkat	k5eAaPmAgFnS	dočkat
až	až	k9	až
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
A	a	k9	a
Beautiful	Beautiful	k1gInSc4	Beautiful
Lie	Lie	k1gFnSc2	Lie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
This	Thisa	k1gFnPc2	Thisa
is	is	k?	is
War	War	k1gFnPc2	War
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
ho	on	k3xPp3gMnSc4	on
následovalo	následovat	k5eAaImAgNnS	následovat
album	album	k1gNnSc1	album
Lust	Lust	k1gInSc1	Lust
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Faith	Faith	k1gInSc1	Faith
and	and	k?	and
Dreams	Dreams	k1gInSc1	Dreams
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
alba	album	k1gNnPc4	album
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
i	i	k9	i
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
základně	základna	k1gFnSc6	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
je	být	k5eAaImIp3nS	být
i	i	k9	i
počet	počet	k1gInSc1	počet
prodaných	prodaný	k2eAgNnPc2d1	prodané
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k6eAd1	Leto
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
zpěváka	zpěvák	k1gMnSc2	zpěvák
Jareda	Jareda	k1gMnSc1	Jareda
Leta	Leta	k1gMnSc1	Leta
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
bubeník	bubeník	k1gMnSc1	bubeník
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
30	[number]	k4	30
Seconds	Secondsa	k1gFnPc2	Secondsa
To	to	k9	to
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
příležitostný	příležitostný	k2eAgMnSc1d1	příležitostný
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k1gMnSc1	Leto
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
projektech	projekt	k1gInPc6	projekt
například	například	k6eAd1	například
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Antoine	Antoin	k1gInSc5	Antoin
Becksem	Becks	k1gMnSc7	Becks
<g/>
,	,	kIx,	,
nahrávání	nahrávání	k1gNnSc1	nahrávání
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
</s>
</p>
<p>
<s>
The	The	k?	The
Wondergirls	Wondergirls	k1gInSc1	Wondergirls
a	a	k8xC	a
nebo	nebo	k8xC	nebo
příležitostné	příležitostný	k2eAgNnSc1d1	příležitostné
hraní	hraní	k1gNnSc1	hraní
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Street	Streeta	k1gFnPc2	Streeta
Drum	Druma	k1gFnPc2	Druma
Corps	corps	k1gInSc1	corps
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
talent	talent	k1gInSc4	talent
získal	získat	k5eAaPmAgMnS	získat
kladnou	kladný	k2eAgFnSc4d1	kladná
kritiku	kritika	k1gFnSc4	kritika
od	od	k7c2	od
hudebníků	hudebník	k1gMnPc2	hudebník
i	i	k8xC	i
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k1gMnSc1	Leto
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Bossier	Bossier	k1gInSc4	Bossier
City	city	k1gNnSc1	city
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Constance	Constanec	k1gInSc2	Constanec
Leto	Leto	k6eAd1	Leto
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Metrejonová	Metrejonová	k1gFnSc1	Metrejonová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cajunského	cajunský	k2eAgInSc2d1	cajunský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
Shannon	Shannon	k1gInSc4	Shannon
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Jaredem	Jared	k1gMnSc7	Jared
zůstali	zůstat	k5eAaPmAgMnP	zůstat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
se	se	k3xPyFc4	se
k	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
Shannonovi	Shannonův	k2eAgMnPc1d1	Shannonův
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Letovi	Leta	k1gMnSc3	Leta
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
z	z	k7c2	z
rodné	rodný	k2eAgFnSc2d1	rodná
Louisiany	Louisiana	k1gFnSc2	Louisiana
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
měst	město	k1gNnPc2	město
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Shannon	Shannon	k1gInSc1	Shannon
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgMnSc2	svůj
vlastního	vlastní	k2eAgMnSc2d1	vlastní
bratra	bratr	k1gMnSc2	bratr
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
druhého	druhý	k4xOgInSc2	druhý
manželství	manželství	k1gNnPc2	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
Constance	Constance	k1gFnSc1	Constance
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
hippie	hippie	k1gMnPc4	hippie
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
v	v	k7c6	v
dělání	dělání	k1gNnSc4	dělání
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
bicí	bicí	k2eAgInSc4d1	bicí
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
jednou	jeden	k4xCgFnSc7	jeden
</s>
</p>
<p>
<s>
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Pocházím	pocházet	k5eAaImIp1nS	pocházet
z	z	k7c2	z
docela	docela	k6eAd1	docela
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
byly	být	k5eAaImAgFnP	být
plátna	plátno	k1gNnPc1	plátno
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgFnPc2d1	další
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
jsem	být	k5eAaImIp1nS	být
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
hrnce	hrnec	k1gInPc4	hrnec
a	a	k8xC	a
pánve	pánev	k1gFnPc4	pánev
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tak	tak	k9	tak
nějak	nějak	k6eAd1	nějak
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vyplynuly	vyplynout	k5eAaPmAgFnP	vyplynout
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
prostě	prostě	k6eAd1	prostě
přirozený	přirozený	k2eAgInSc1d1	přirozený
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
dostal	dostat	k5eAaPmAgMnS	dostat
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
učit	učit	k5eAaImF	učit
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
styl	styl	k1gInSc4	styl
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
popsal	popsat	k5eAaPmAgMnS	popsat
své	svůj	k3xOyFgNnSc4	svůj
dospívání	dospívání	k1gNnSc4	dospívání
jako	jako	k8xC	jako
neklidné	klidný	k2eNgNnSc4d1	neklidné
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
požíval	požívat	k5eAaImAgMnS	požívat
drogy	droga	k1gFnPc4	droga
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
,,	,,	k?	,,
<g/>
Nebyl	být	k5eNaImAgInS	být
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
myslel	myslet	k5eAaImAgMnS	myslet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikam	nikam	k6eAd1	nikam
nepatřím	patřit	k5eNaImIp1nS	patřit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
"	"	kIx"	"
<g/>
outsiderem	outsider	k1gMnSc7	outsider
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nenáviděl	nenávidět	k5eAaImAgInS	nenávidět
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
chopil	chopit	k5eAaPmAgMnS	chopit
se	se	k3xPyFc4	se
každe	každe	k6eAd1	každe
příležitosti	příležitost	k1gFnSc2	příležitost
je	být	k5eAaImIp3nS	být
zničit	zničit	k5eAaPmF	zničit
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Jared	Jared	k1gInSc1	Jared
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgInS	pomoct
vše	všechen	k3xTgNnSc1	všechen
urovnat	urovnat	k5eAaPmF	urovnat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupina	skupina	k1gFnSc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaredem	Jared	k1gMnSc7	Jared
založili	založit	k5eAaPmAgMnP	založit
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
kapelu	kapela	k1gFnSc4	kapela
Thirty	Thirta	k1gFnSc2	Thirta
Seconds	Secondsa	k1gFnPc2	Secondsa
to	ten	k3xDgNnSc1	ten
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
vznikalo	vznikat	k5eAaImAgNnS	vznikat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nahráno	nahrát	k5eAaPmNgNnS	nahrát
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
v	v	k7c6	v
Wyomingu	Wyoming	k1gInSc6	Wyoming
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
a	a	k8xC	a
i	i	k9	i
některé	některý	k3yIgFnSc2	některý
skladby	skladba	k1gFnSc2	skladba
byly	být	k5eAaImAgInP	být
nahrané	nahraný	k2eAgInPc1d1	nahraný
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
albech	album	k1gNnPc6	album
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
chtělo	chtít	k5eAaImAgNnS	chtít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
spoustu	spousta	k1gFnSc4	spousta
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
Immortal	Immortal	k1gFnSc4	Immortal
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Bobem	Bob	k1gMnSc7	Bob
Ezrinem	Ezrin	k1gMnSc7	Ezrin
a	a	k8xC	a
s	s	k7c7	s
Brianem	Brian	k1gMnSc7	Brian
Virtueem	Virtueus	k1gMnSc7	Virtueus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydané	vydaný	k2eAgFnPc4d1	vydaná
nahrávacími	nahrávací	k2eAgNnPc7d1	nahrávací
studii	studio	k1gNnPc7	studio
Immortal	Immortal	k1gFnSc2	Immortal
Records	Records	k1gInSc1	Records
a	a	k8xC	a
Virgin	Virgin	k1gInSc1	Virgin
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
107	[number]	k4	107
<g/>
.	.	kIx.	.
<g/>
místa	místo	k1gNnPc4	místo
v	v	k7c4	v
US	US	kA	US
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
příčky	příčka	k1gFnSc2	příčka
v	v	k7c6	v
US	US	kA	US
Top	topit	k5eAaImRp2nS	topit
Heatseekers	Heatseekers	k1gInSc4	Heatseekers
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávce	nahrávka	k1gFnSc3	nahrávka
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
velmi	velmi	k6eAd1	velmi
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
ohlasů	ohlas	k1gInPc2	ohlas
např.	např.	kA	např.
i	i	k9	i
od	od	k7c2	od
Megan	Megana	k1gFnPc2	Megana
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Toole	Tool	k1gInSc6	Tool
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
kopijí	kopít	k5eAaPmIp3nP	kopít
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
než	než	k8xS	než
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
A	a	k8xC	a
Beautiful	Beautiful	k1gInSc1	Beautiful
Lie	Lie	k1gFnSc2	Lie
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
dokonce	dokonce	k9	dokonce
procestovala	procestovat	k5eAaPmAgFnS	procestovat
4	[number]	k4	4
různé	různý	k2eAgInPc4d1	různý
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
CD	CD	kA	CD
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
opět	opět	k6eAd1	opět
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
platinovým	platinový	k2eAgMnSc7d1	platinový
v	v	k7c4	v
Recording	Recording	k1gInSc4	Recording
Industry	Industra	k1gFnSc2	Industra
Association	Association	k1gInSc4	Association
of	of	k?	of
America	Americ	k1gInSc2	Americ
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zlatým	zlatý	k2eAgInSc7d1	zlatý
a	a	k8xC	a
platinovým	platinový	k2eAgInSc7d1	platinový
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
podnikla	podniknout	k5eAaPmAgFnS	podniknout
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
alba	album	k1gNnSc2	album
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
několika	několik	k4yIc2	několik
význammných	význammný	k2eAgInPc2d1	význammný
festivalů	festival	k1gInPc2	festival
jako	jako	k8xC	jako
Roskilde	Roskild	k1gInSc5	Roskild
<g/>
,	,	kIx,	,
Pinkpop	Pinkpop	k1gInSc1	Pinkpop
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc1	rock
am	am	k?	am
Ring	ring	k1gInSc1	ring
nebo	nebo	k8xC	nebo
Download	Download	k1gInSc1	Download
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
podepsat	podepsat	k5eAaPmF	podepsat
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
neobešlo	obešnout	k5eNaPmAgNnS	obešnout
bez	bez	k7c2	bez
soudního	soudní	k2eAgNnSc2d1	soudní
líčení	líčení	k1gNnSc2	líčení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
roku	rok	k1gInSc2	rok
právních	právní	k2eAgFnPc2d1	právní
bitev	bitva	k1gFnPc2	bitva
kapela	kapela	k1gFnSc1	kapela
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Podepsali	podepsat	k5eAaPmAgMnP	podepsat
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
vydali	vydat	k5eAaPmAgMnP	vydat
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
album	album	k1gNnSc4	album
This	Thisa	k1gFnPc2	Thisa
Is	Is	k1gMnSc1	Is
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
top	topit	k5eAaImRp2nS	topit
desítky	desítka	k1gFnSc2	desítka
v	v	k7c6	v
několika	několik	k4yIc6	několik
národních	národní	k2eAgFnPc6d1	národní
hitparádách	hitparáda	k1gFnPc6	hitparáda
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
řadu	řada	k1gFnSc4	řada
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2002	[number]	k4	2002
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
Into	Into	k6eAd1	Into
the	the	k?	the
Wild	Wild	k1gInSc1	Wild
Tour	Toura	k1gFnPc2	Toura
a	a	k8xC	a
zařadili	zařadit	k5eAaPmAgMnP	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejvytíženější	vytížený	k2eAgInPc4d3	nejvytíženější
"	"	kIx"	"
<g/>
putovní	putovní	k2eAgInPc4d1	putovní
<g/>
"	"	kIx"	"
umělce	umělec	k1gMnPc4	umělec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2001	[number]	k4	2001
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
za	za	k7c2	za
nejvíce	nejvíce	k6eAd1	nejvíce
živých	živý	k2eAgNnPc2d1	živé
vystoupení	vystoupení	k1gNnPc2	vystoupení
(	(	kIx(	(
<g/>
300	[number]	k4	300
koncertů	koncert	k1gInPc2	koncert
<g/>
)	)	kIx)	)
během	běh	k1gInSc7	běh
období	období	k1gNnSc1	období
jednoho	jeden	k4xCgNnSc2	jeden
vydaného	vydaný	k2eAgNnSc2d1	vydané
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2002	[number]	k4	2002
vydal	vydat	k5eAaPmAgInS	vydat
Shannon	Shannon	k1gInSc1	Shannon
remix	remix	k1gInSc4	remix
skladby	skladba	k1gFnSc2	skladba
Night	Night	k1gInSc1	Night
of	of	k?	of
the	the	k?	the
Hunter	Hunter	k1gInSc1	Hunter
<g/>
,	,	kIx,	,
skladba	skladba	k1gFnSc1	skladba
z	z	k7c2	z
This	Thisa	k1gFnPc2	Thisa
Is	Is	k1gMnSc2	Is
War	War	k1gMnSc2	War
<g/>
,	,	kIx,	,
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
do	do	k7c2	do
písně	píseň	k1gFnSc2	píseň
vnést	vnést	k5eAaPmF	vnést
nové	nový	k2eAgNnSc4d1	nové
světlo	světlo	k1gNnSc4	světlo
pomocí	pomocí	k7c2	pomocí
tanečních	taneční	k2eAgInPc2d1	taneční
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
již	již	k6eAd1	již
své	své	k1gNnSc4	své
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lust	Lusta	k1gFnPc2	Lusta
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Faith	Faith	k1gInSc1	Faith
and	and	k?	and
Dreams	Dreams	k1gInSc1	Dreams
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
vydala	vydat	k5eAaPmAgFnS	vydat
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
Universal	Universal	k1gFnSc2	Universal
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
mělo	mít	k5eAaImAgNnS	mít
velice	velice	k6eAd1	velice
kladné	kladný	k2eAgInPc4d1	kladný
ohlasy	ohlas	k1gInPc4	ohlas
a	a	k8xC	a
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
top	topit	k5eAaImRp2nS	topit
desítky	desítka	k1gFnSc2	desítka
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
USA	USA	kA	USA
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Lust	Lust	k1gMnSc1	Lust
<g/>
,	,	kIx,	,
Faith	Faith	k1gMnSc1	Faith
and	and	k?	and
Dreams	Dreams	k1gInSc1	Dreams
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
the	the	k?	the
Carnivores	Carnivores	k1gMnSc1	Carnivores
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
jeli	jet	k5eAaImAgMnP	jet
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
30	[number]	k4	30
Seconds	Secondsa	k1gFnPc2	Secondsa
to	ten	k3xDgNnSc1	ten
Mars	Mars	k1gInSc4	Mars
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odešli	odejít	k5eAaPmAgMnP	odejít
od	od	k7c2	od
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Virgin	Virgin	k2eAgInSc1d1	Virgin
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hraní	hraní	k1gNnSc2	hraní
==	==	k?	==
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gInSc1	Shannon
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pro	pro	k7c4	pro
energetická	energetický	k2eAgNnPc4d1	energetické
živá	živý	k2eAgNnPc4d1	živé
vystoupení	vystoupení	k1gNnPc4	vystoupení
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
skloubit	skloubit	k5eAaPmF	skloubit
dohromady	dohromady	k6eAd1	dohromady
tradiční	tradiční	k2eAgNnSc4d1	tradiční
hraní	hraní	k1gNnSc4	hraní
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
s	s	k7c7	s
experimentálním	experimentální	k2eAgInSc7d1	experimentální
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tvorby	tvorba	k1gFnSc2	tvorba
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
alba	album	k1gNnSc2	album
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
hrál	hrát	k5eAaImAgInS	hrát
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
elektronické	elektronický	k2eAgNnSc4d1	elektronické
bicí	bicí	k2eAgInSc4d1	bicí
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přirovnáván	přirovnáván	k2eAgMnSc1d1	přirovnáván
ke	k	k7c3	k
kapelám	kapela	k1gFnPc3	kapela
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Cure	Cure	k1gFnSc1	Cure
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
nebo	nebo	k8xC	nebo
The	The	k1gFnPc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
alba	album	k1gNnSc2	album
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
...	...	k?	...
<g/>
Shannon	Shannon	k1gNnSc1	Shannon
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
invenčních	invenční	k2eAgMnPc2d1	invenční
bubeníků	bubeník	k1gMnPc2	bubeník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
jsem	být	k5eAaImIp1nS	být
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojí	spokojit	k5eNaPmIp3nS	spokojit
se	se	k3xPyFc4	se
s	s	k7c7	s
pouhým	pouhý	k2eAgNnSc7d1	pouhé
přidáním	přidání	k1gNnSc7	přidání
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
bubnové	bubnový	k2eAgFnPc1d1	bubnová
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
skladem	sklad	k1gInSc7	sklad
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
také	také	k9	také
skvělí	skvělý	k2eAgMnPc1d1	skvělý
bubeník	bubeník	k1gMnSc1	bubeník
na	na	k7c4	na
živo	živo	k6eAd1	živo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zábavné	zábavný	k2eAgNnSc1d1	zábavné
ho	on	k3xPp3gInSc4	on
sledovat	sledovat	k5eAaImF	sledovat
<g/>
....	....	k?	....
<g/>
"	"	kIx"	"
Napsal	napsat	k5eAaBmAgInS	napsat
také	také	k9	také
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
skladbu	skladba	k1gFnSc4	skladba
L490	L490	k1gFnSc2	L490
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
každý	každý	k3xTgInSc4	každý
nástroj	nástroj	k1gInSc4	nástroj
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
alba	album	k1gNnSc2	album
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
právě	právě	k9	právě
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
objevuje	objevovat	k5eAaImIp3nS	objevovat
velká	velký	k2eAgFnSc1d1	velká
škála	škála	k1gFnSc1	škála
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
byl	být	k5eAaImAgMnS	být
víceméně	víceméně	k9	víceméně
samouk	samouk	k1gMnSc1	samouk
co	co	k9	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
oddaným	oddaný	k2eAgMnSc7d1	oddaný
fanouškem	fanoušek	k1gMnSc7	fanoušek
progresivního	progresivní	k2eAgInSc2d1	progresivní
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
poslouchal	poslouchat	k5eAaImAgInS	poslouchat
Fleetwood	Fleetwood	k1gInSc1	Fleetwood
Mac	Mac	kA	Mac
<g/>
,	,	kIx,	,
Janis	Janis	k1gInSc4	Janis
Joplin	Joplina	k1gFnPc2	Joplina
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
,	,	kIx,	,
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
a	a	k8xC	a
Boz	Boz	k1gFnPc2	Boz
Scaggs	Scaggsa	k1gFnPc2	Scaggsa
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
se	se	k3xPyFc4	se
elektronickou	elektronický	k2eAgFnSc7d1	elektronická
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
Depeche	Depeche	k1gFnSc7	Depeche
Mode	modus	k1gInSc5	modus
a	a	k8xC	a
The	The	k1gMnSc4	The
Cure	Cur	k1gMnSc4	Cur
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
jazzová	jazzový	k2eAgFnSc1d1	jazzová
skupina	skupina	k1gFnSc1	skupina
Steely	Steela	k1gFnSc2	Steela
Dan	Dana	k1gFnPc2	Dana
nebo	nebo	k8xC	nebo
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
Mountain	Mountain	k1gInSc4	Mountain
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
a	a	k8xC	a
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Shannona	Shannon	k1gMnSc2	Shannon
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
umělci	umělec	k1gMnPc1	umělec
komerčně	komerčně	k6eAd1	komerčně
životaschopní	životaschopný	k2eAgMnPc1d1	životaschopný
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
také	také	k9	také
respektovaní	respektovaný	k2eAgMnPc1d1	respektovaný
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
řekli	říct	k5eAaPmAgMnP	říct
a	a	k8xC	a
nebo	nebo	k8xC	nebo
čím	čí	k3xOyRgNnSc7	čí
přispěli	přispět	k5eAaPmAgMnP	přispět
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
obdiv	obdiv	k1gInSc1	obdiv
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezaměřil	zaměřit	k5eNaPmAgMnS	zaměřit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
bubeníky	bubeník	k1gMnPc4	bubeník
<g/>
,	,	kIx,	,
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
hudbou	hudba	k1gFnSc7	hudba
než	než	k8xS	než
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
Shannon	Shannona	k1gFnPc2	Shannona
i	i	k8xC	i
Jared	Jared	k1gMnSc1	Jared
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
<g/>
,	,	kIx,	,
<g/>
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
specifický	specifický	k2eAgInSc4d1	specifický
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
proces	proces	k1gInSc1	proces
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
organický	organický	k2eAgInSc1d1	organický
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
spíše	spíše	k9	spíše
pocit	pocit	k1gInSc4	pocit
než	než	k8xS	než
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
připomínal	připomínat	k5eAaImAgInS	připomínat
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncerty	koncert	k1gInPc1	koncert
jsou	být	k5eAaImIp3nP	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
přímým	přímý	k2eAgInSc7d1	přímý
odrazem	odraz	k1gInSc7	odraz
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jejich	jejich	k3xOp3gNnSc2	jejich
dětsví	dětsví	k1gNnSc2	dětsví
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
Drummies	Drummies	k1gInSc4	Drummies
Awards	Awardsa	k1gFnPc2	Awardsa
cenu	cena	k1gFnSc4	cena
Indie	Indie	k1gFnSc2	Indie
Drummer	Drummer	k1gMnSc1	Drummer
Award	Award	k1gMnSc1	Award
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
a	a	k8xC	a
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Alternative	Alternativ	k1gInSc5	Alternativ
Drummer	Drummer	k1gMnSc1	Drummer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
v	v	k7c6	v
My	my	k3xPp1nPc1	my
So-called	Soalled	k1gMnSc1	So-called
Life	Lif	k1gFnPc4	Lif
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
Jared	Jared	k1gMnSc1	Jared
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Prefontaine	Prefontain	k1gInSc5	Prefontain
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sol	solit	k5eAaImRp2nS	solit
Goode	Good	k1gMnSc5	Good
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
Highway	Highway	k1gInPc1	Highway
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nahrál	nahrát	k5eAaBmAgMnS	nahrát
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Drop	drop	k1gMnSc1	drop
That	That	k2eAgMnSc1d1	That
Baby	baba	k1gFnPc4	baba
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Go	Go	k1gFnSc7	Go
All	All	k1gMnSc2	All
the	the	k?	the
Way	Way	k1gMnSc2	Way
<g/>
"	"	kIx"	"
s	s	k7c7	s
krátkotrvající	krátkotrvající	k2eAgFnSc7d1	krátkotrvající
kapelou	kapela	k1gFnSc7	kapela
The	The	k1gFnSc2	The
Wondergirls	Wondergirlsa	k1gFnPc2	Wondergirlsa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
občas	občas	k6eAd1	občas
hrál	hrát	k5eAaImAgMnS	hrát
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Street	Streeta	k1gFnPc2	Streeta
Drum	Druma	k1gFnPc2	Druma
Corps	corps	k1gInSc1	corps
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Habitat	Habitat	k1gFnSc2	Habitat
for	forum	k1gNnPc2	forum
Humanity	humanita	k1gFnSc2	humanita
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
kapely	kapela	k1gFnSc2	kapela
30	[number]	k4	30
Seconds	Secondsa	k1gFnPc2	Secondsa
to	ten	k3xDgNnSc4	ten
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
producentem	producent	k1gMnSc7	producent
americké	americký	k2eAgFnSc2d1	americká
elektronické	elektronický	k2eAgFnSc2d1	elektronická
kapely	kapela	k1gFnSc2	kapela
CB7	CB7	k1gFnPc2	CB7
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
hudebníkem	hudebník	k1gMnSc7	hudebník
Antoine	Antoin	k1gInSc5	Antoin
Becksem	Becks	k1gMnSc7	Becks
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
CB7	CB7	k4	CB7
se	se	k3xPyFc4	se
roku	rok	k1gInSc6	rok
2011	[number]	k4	2011
přidalo	přidat	k5eAaPmAgNnS	přidat
na	na	k7c4	na
tour	tour	k1gInSc4	tour
Into	Into	k1gMnSc1	Into
the	the	k?	the
Wild	Wild	k1gMnSc1	Wild
Tour	Tour	k1gMnSc1	Tour
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
společně	společně	k6eAd1	společně
s	s	k7c7	s
30	[number]	k4	30
Seconds	Seconds	k1gInSc1	Seconds
to	ten	k3xDgNnSc4	ten
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
vedlejšího	vedlejší	k2eAgInSc2d1	vedlejší
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
Antoine	Antoin	k1gInSc5	Antoin
Becksem	Becks	k1gInSc7	Becks
<g/>
,	,	kIx,	,
cestovali	cestovat	k5eAaImAgMnP	cestovat
spolu	spolu	k6eAd1	spolu
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
Shannon	Shannon	k1gMnSc1	Shannon
zahájil	zahájit	k5eAaPmAgMnS	zahájit
projekt	projekt	k1gInSc4	projekt
Black	Black	k1gMnSc1	Black
Fuel	Fuel	k1gMnSc1	Fuel
Trading	Trading	k1gInSc4	Trading
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
především	především	k9	především
na	na	k7c4	na
zodpovědné	zodpovědný	k2eAgInPc4d1	zodpovědný
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
kávou	káva	k1gFnSc7	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Shannon	Shannona	k1gFnPc2	Shannona
Leto	Leto	k6eAd1	Leto
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k1gMnSc1	Leto
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k1gMnSc1	Leto
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Shannon	Shannon	k1gMnSc1	Shannon
Leto	Leto	k1gMnSc1	Leto
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
