<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1270	[number]	k4	1270
Jihlava	Jihlava	k1gFnSc1	Jihlava
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
stavební	stavební	k2eAgInSc1d1	stavební
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vtiskl	vtisknout	k5eAaPmAgInS	vtisknout
historické	historický	k2eAgFnPc4d1	historická
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
půdorys	půdorys	k1gInSc4	půdorys
<g/>
,	,	kIx,	,
pravoúhlé	pravoúhlý	k2eAgFnPc1d1	pravoúhlá
sítě	síť	k1gFnPc1	síť
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
náměstím	náměstí	k1gNnSc7	náměstí
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
