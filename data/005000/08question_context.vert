<s>
Burušaskí	Burušaskí	k1gFnSc1
neboli	neboli	k8xC
chadžuná	chadžuná	k1gFnSc1
je	být	k5eAaImIp3nS
izolovaný	izolovaný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7
mluví	mluvit	k5eAaImIp3nS
Hunzové	Hunzový	k2eAgNnSc1d1
(	(	kIx(
<g/>
také	také	k9
zvaní	zvaní	k1gNnSc1
Burúšové	Burúšová	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
horských	horský	k2eAgNnPc6d1
údolích	údolí	k1gNnPc6
pohoří	pohořet	k5eAaPmIp3nS
Karákóram	Karákóram	k1gInSc1
<g/>
.	.	kIx.
</s>